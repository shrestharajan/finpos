package global.citytech.finpos.merchant.domain.repository.core.cashin

import global.citytech.finpos.merchant.domain.model.cashin.CashInResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.cashin.CashInRequestItem
import io.reactivex.Observable

interface CashInRepository {
    fun generateCashInRequest(
        configurationItem: ConfigurationItem,
        cashInRequestItem: CashInRequestItem
    ): Observable<CashInResponseEntity>

}