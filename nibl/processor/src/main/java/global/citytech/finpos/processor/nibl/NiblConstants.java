package global.citytech.finpos.processor.nibl;

/**
 * Created by Unique Shakya on 1/11/2021.
 */
public class NiblConstants {

    /**
     * Transaction Constants
     */
    public static final String PIN_PAD_CURRENCY = "NPR";
    public static final String PIN_PAD_MESSAGE = "CUSTOMER PIN ENTRY";
}
