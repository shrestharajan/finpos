package global.citytech.finpos.device.weipass.sound

import android.content.Context
import android.media.MediaPlayer
import global.citytech.finpos.device.weipass.R
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.io.sound.Sound
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.lang.ref.WeakReference

/**
 * Created by Rishav Chudal on 6/4/20.
 */
class WeipassSoundServiceImplTest {

    private lateinit var SUT: WeipassSoundServiceImpl

    @RelaxedMockK
    private lateinit var mp4: MediaPlayer

    @MockK
    private lateinit var mockedContext: Context


    @Before
    fun setUp(){
        MockKAnnotations.init(this)
        SUT = WeipassSoundServiceImpl(mockedContext)
        mockkMediaPlayer()
        mockkWeipassLogger()
    }

    @Test
    fun playSound_OnSoundTypeSuccessProvided_PlaysSuccessSound() {
        //Setup
        val weakReferenceContext = WeakReference(mockedContext)
        val sound = Sound.SUCCESS
        every { MediaPlayer.create(weakReferenceContext.get(), R.raw.success) }  returns mp4
        every { mp4.start() } just Runs
        every { mp4.release() } just Runs
        every { mp4.stop() } just Runs
        every { mp4.setOnCompletionListener {  }} answers {
            mp4.release()
        }

        //Run
        SUT.playSound(sound)

        //Verify
        assertEquals(Sound.SUCCESS, sound)
        verifyOrder {
            MediaPlayer.create(mockedContext, R.raw.success )
            mp4.start()
        }
        clearAllMocks()
    }

    @Test
    fun playSound_OnSoundTypeFailureProvidedAndContextIsNotNull_PlaysFailureSound() {
        //Setup
        val weakReferenceContext = WeakReference(mockedContext)
        val sound = Sound.FAILURE
        every { MediaPlayer.create(weakReferenceContext.get(), R.raw.failed) }  returns mp4
        every { mp4.start() } just Runs
        every { mp4.release() } just Runs
        every { mp4.stop() } just Runs
        every { mp4.setOnCompletionListener {  }} answers {
            mp4.release()
        }

        //Run
        SUT.playSound(sound)

        //Verify
        assertEquals(Sound.FAILURE, sound)
        verifyOrder {
            MediaPlayer.create(mockedContext, R.raw.failed )
            mp4.start()
        }
        clearAllMocks()
    }

    private fun mockkMediaPlayer() {
        mockkStatic(MediaPlayer::class)
    }

    private fun mockkWeipassLogger() {
        mockkObject(WeipassLogger)
        mockkObject(WeipassLogger.Companion)
        every { WeipassLogger.log(any()) } just Runs
    }

}