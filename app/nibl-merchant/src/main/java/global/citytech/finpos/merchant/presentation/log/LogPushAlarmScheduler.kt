package global.citytech.finpos.merchant.presentation.log

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import global.citytech.finpos.merchant.framework.broadcasts.LogPushAlarmReceiver
import java.util.*

class LogPushAlarmScheduler(val context: Context) {
    private val pendingIntentBroadcast: PendingIntent = assignPendingIntentBroadcast()
    private val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager?

    private fun assignPendingIntentBroadcast(): PendingIntent {
        val intent = Intent(context.applicationContext, LogPushAlarmReceiver::class.java)
        intent.action = "action.alarm.log.push"
        return PendingIntent.getBroadcast(context.applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    private fun setAlarmForLogPush(alarmManager: AlarmManager) {
        alarmManager.cancel(pendingIntentBroadcast)
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 5)
        calendar.set(Calendar.SECOND, 0)
        val triggerTimeInMillis = calendar.timeInMillis

        if (calendar.timeInMillis <= System.currentTimeMillis()) {
            calendar.add(Calendar.DAY_OF_YEAR, 1)
        }

        alarmManager.setRepeating(
            AlarmManager.RTC_WAKEUP,
            triggerTimeInMillis,
            0,
            pendingIntentBroadcast
        )
    }

    fun triggerAlarm() {
        alarmManager?.let {
            setAlarmForLogPush(it)
        }
    }
}