package global.citytech.finpos.merchant.framework.datasource.core

import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.app.db.AppDatabase
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.ActivityLog
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.repositories.ActivityLogRepository


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/11/08 - 12:42 PM
*/

public class ActivityLogRepositoryImpl : ActivityLogRepository {
    private val logger = Logger(ActivityLogRepositoryImpl::class.java.name)
    private var localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )
    override fun updateActivityLog(activityLog: global.citytech.finposframework.usecases.activity.ActivityLog) {
        val log = ActivityLog(
            System.currentTimeMillis().toString(),
            activityLog.stan,
            activityLog.type.name,
            activityLog.merchantId,
            activityLog.terminalId,
            activityLog.batchNumber,
            activityLog.amount,
            activityLog.responseCode,
            activityLog.isoRequest,
            activityLog.isoResponse,
            activityLog.status.name,
            activityLog.remarks,
            activityLog.posDateTime.toString(),
            activityLog.additionalData
        )
        logger.log(":::: ACTIVITY LOG INSERTED :::: " + Jsons.toJsonObj(activityLog))
        AppDatabase.getInstance(NiblMerchant.INSTANCE).getActivityLogDao().insert(log)

    }
}