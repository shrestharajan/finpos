package global.citytech.neps.domain.model

import android.content.ContentValues
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "aid_param")
data class AidParam(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_ID)
    var id :String,
    @ColumnInfo(name = COLUMN_AID)
    var aid :String,
    @ColumnInfo(name = COLUMN_LABEL)
    var label:String? = null,
    @ColumnInfo(name = COLUMN_TERMINAL_AID_VERSION)
    var terminalAidVersion :String? = null,
    @ColumnInfo(name = COLUMN_DEFAULT_TDOL)
    var defaultTDOL:String? = null,
    @ColumnInfo(name = COLUMN_DEFAULT_DDOL)
    var defaultDDOL :String? = null,
    @ColumnInfo(name = COLUMN_DENIAL_ACTION_CODE)
    var denialActionCode:String? = null,
    @ColumnInfo(name = COLUMN_ONLINE_ACTION_CODE)
    var onlineActionCode :String? = null,
    @ColumnInfo(name = COLUMN_DEFAULT_ACTION_CODE)
    var defaultActionCode:String? = null,
    @ColumnInfo(name = COLUMN_TERMINAL_FLOOR_LIMIT)
    var terminalFloorLimit:String? = null,
    @ColumnInfo(name = COLUMN_CONTACTLESS_FLOOR_LIMIT)
    var contactlessFloorLimit :String? = null,
    @ColumnInfo(name = COLUMN_CONTACTLESS_TRANSACTION_LIMIT)
    var contactlessTransactionLimit:String? = null,
    @ColumnInfo(name = COLUMN_CVM_LIMIT)
    var cvmLimit:String? = null

){
    companion object {
        const val COLUMN_ID = "id"
        const val COLUMN_AID = "aid"
        const val COLUMN_LABEL = "label"
        const val COLUMN_TERMINAL_AID_VERSION = "terminal_aid_version"
        const val COLUMN_DEFAULT_TDOL = "default_tdol"
        const val COLUMN_DEFAULT_DDOL = "default_ddol"
        const val COLUMN_DENIAL_ACTION_CODE = "denial_action_code"
        const val COLUMN_ONLINE_ACTION_CODE = "online_action_code"
        const val COLUMN_DEFAULT_ACTION_CODE = "default_action_code"
        const val COLUMN_TERMINAL_FLOOR_LIMIT = "terminal_floor_limit"
        const val COLUMN_CONTACTLESS_FLOOR_LIMIT = "contactless_floor_limit"
        const val COLUMN_CONTACTLESS_TRANSACTION_LIMIT = "contactless_transaction_limit"
        const val COLUMN_CVM_LIMIT = "cvm_limit"


        fun fromContentValues(values: ContentValues): AidParam {
            values.let {
                val aidParam = AidParam(
                    id = values.getAsString(COLUMN_ID), aid = values.getAsString(
                        COLUMN_AID
                    )
                )
                if (it.containsKey(COLUMN_LABEL))
                    aidParam.label = it.getAsString(COLUMN_LABEL)
                if (it.containsKey(COLUMN_TERMINAL_AID_VERSION))
                    aidParam.terminalAidVersion = it.getAsString(COLUMN_TERMINAL_AID_VERSION)
                if (it.containsKey(COLUMN_DEFAULT_TDOL))
                    aidParam.defaultTDOL = it.getAsString(COLUMN_DEFAULT_TDOL)
                if (it.containsKey(COLUMN_DEFAULT_DDOL))
                    aidParam.defaultDDOL = it.getAsString(COLUMN_DEFAULT_DDOL)
                if (it.containsKey(COLUMN_DENIAL_ACTION_CODE))
                    aidParam.denialActionCode = it.getAsString(COLUMN_DENIAL_ACTION_CODE)
                if (it.containsKey(COLUMN_ONLINE_ACTION_CODE))
                    aidParam.onlineActionCode = it.getAsString(COLUMN_ONLINE_ACTION_CODE)
                if (it.containsKey(COLUMN_DEFAULT_ACTION_CODE))
                    aidParam.defaultActionCode = it.getAsString(COLUMN_DEFAULT_ACTION_CODE)
                if (it.containsKey(COLUMN_TERMINAL_FLOOR_LIMIT))
                    aidParam.terminalFloorLimit = it.getAsString(COLUMN_TERMINAL_FLOOR_LIMIT)
                if (it.containsKey(COLUMN_CONTACTLESS_FLOOR_LIMIT))
                    aidParam.contactlessFloorLimit = it.getAsString(COLUMN_CONTACTLESS_FLOOR_LIMIT)
                if (it.containsKey(COLUMN_CONTACTLESS_TRANSACTION_LIMIT))
                    aidParam.contactlessTransactionLimit =
                        it.getAsString(COLUMN_CONTACTLESS_TRANSACTION_LIMIT)
                if (it.containsKey(COLUMN_CVM_LIMIT))
                    aidParam.cvmLimit = it.getAsString(COLUMN_CVM_LIMIT)
                return aidParam
            }
        }
    }
}