package global.citytech.finpos.merchant.presentation.admin

enum class DeviceConfigurationStatus(val message: String) {
    DownloadingConfiguration("Downloading Configuration"),
    ConfigurationDownloaded("Configuration Downloaded"),
    SavingParameters("Saving Parameters"),
    ParametersSaved("Parameters Saved"),
    InjectingKeys("Injecting Keys"),
    KeysInjectedSuccesfully("Keys Injected"),
    ConfigurationFailed("Configuration Failed"),
    ConfigurationCanceled("Configuration Canceled"),
    ConfiguringTerminal("Configuring Terminal"),
    TerminalConfigured("Terminal Configured")
}