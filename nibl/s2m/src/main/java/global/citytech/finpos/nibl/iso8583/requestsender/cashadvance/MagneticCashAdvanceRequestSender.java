package global.citytech.finpos.nibl.iso8583.requestsender.cashadvance;

import global.citytech.finpos.nibl.iso8583.PosEntryModeConfig;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.switches.PosEntryMode;

/**
 * Created by Unique Shakya on 2/4/2021.
 */
public class MagneticCashAdvanceRequestSender extends CashAdvanceRequestSender{

    public MagneticCashAdvanceRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
    }

    @Override
    protected Iso8583Msg addConditionalFields(Iso8583Msg msg, TransactionIsoRequest cashAdvanceRequest) {
        msg.addField(
                new DataElement(
                        DataElement.TRACK_II_DATA,
                        cashAdvanceRequest.getTrack2Data()
                )
        );
        PosEntryMode posEntryMode;
        if (cashAdvanceRequest.isFallbackFromIccToMag())
            posEntryMode = PosEntryMode.ICC_FALLBACK_TO_MAGNETIC;
        else
            posEntryMode = PosEntryMode.MAGNETIC_STRIPE;
        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(posEntryMode)
                )
        );
        return msg;
    }
}
