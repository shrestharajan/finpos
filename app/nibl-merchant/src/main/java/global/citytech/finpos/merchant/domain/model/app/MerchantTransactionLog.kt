package global.citytech.finpos.merchant.domain.model.app

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Saurav Ghimire on 4/26/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */

@Entity(tableName = MerchantTransactionLog.TABLE_NAME)
data class MerchantTransactionLog(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_STAN)
    var stan: String,

    @ColumnInfo(name = COLUMN_TERMINAL_ID)
    var terminalId: String,

    @ColumnInfo(name = COLUMN_MERCHANT_ID)
    var merchantId: String,

    @ColumnInfo(name = COLUMN_INVOICE_NUM)
    var invoiceNumber: String,

    @ColumnInfo(name = COLUMN_RRN)
    var rrn: String? = "",

    @ColumnInfo(name = COLUMN_TRANSACTION_DATE)
    var txnDate: String? = "",

    @ColumnInfo(name = COLUMN_TRANSACTION_TIME)
    var txnTime: String? = "",

    @ColumnInfo(name = COLUMN_TRANSACTION_TYPE)
    var txnType: String? = "",

    @ColumnInfo(name = COLUMN_TRANSACTION_AMOUNT)
    var amount: Long? = 0,

    @ColumnInfo(name = COLUMN_SCHEME)
    var scheme: String? = "",

    @ColumnInfo(name = COLUMN_TRANSACTION_STATUS)
    var txnStatus: String? = "",

    @ColumnInfo(name = COLUMN_LONGITUDE)
    var longitude: String? = "",

    @ColumnInfo(name = COLUMN_LATITUDE)
    var latitude: String? = "",

    @ColumnInfo(name = COLUMN_APPROVAL_CODE)
    var approvalCode: String? = "",

    @ColumnInfo(name = COLUMN_ORIGINAL_TXN_REFERENCE)
    var originalTxnReference: String? = "",

    @ColumnInfo(name = COLUMN_POS_ENTRY_MODE)
    var posEntryMode: String? = "",

    @ColumnInfo(name = COLUMN_REMARK)
    var remarks: String? = "",

    @ColumnInfo(name = COLUMN_BIN)
    var bin: String? = "",

    @ColumnInfo(name = COLUMN_EMI_INFO)
    var emiInfo: String? = "",

    @ColumnInfo(name = COLUMN_CURRENCY_CODE)
    var currencyCode: String? = "",

    @ColumnInfo(name = COLUMN_VAT_AMOUNT)
    var vatAmount: String? = "",

    @ColumnInfo(name = COLUMN_VAT_REFUND_AMOUNT)
    var vatRefundAmount: String? = "",

    @ColumnInfo(name = COLUMN_CVM_MESSAGE)
    var cvmMessage: String? = "",

    @ColumnInfo(name = COLUMN_TRANSACTION_EMV_INFO)
    var transactionEmvInfo: String? = ""

) {
    companion object {
        const val TABLE_NAME = "merchant_transaction_log"
        const val COLUMN_STAN = "stan"
        const val COLUMN_TERMINAL_ID = "terminal_id"
        const val COLUMN_MERCHANT_ID = "merchant_id"
        const val COLUMN_INVOICE_NUM = "invoice_num"
        const val COLUMN_RRN = "retrieval_reference_number"
        const val COLUMN_TRANSACTION_DATE = "transaction_date"
        const val COLUMN_TRANSACTION_TIME = "transaction_time"
        const val COLUMN_TRANSACTION_TYPE = "transaction_type"
        const val COLUMN_TRANSACTION_AMOUNT = "transaction_amount"
        const val COLUMN_SCHEME = "scheme"
        const val COLUMN_TRANSACTION_STATUS = "transaction_status"
        const val COLUMN_LATITUDE = "latitude"
        const val COLUMN_LONGITUDE = "longitude"
        const val COLUMN_APPROVAL_CODE = "approval_code"
        const val COLUMN_ORIGINAL_TXN_REFERENCE = "original_txn_reference"
        const val COLUMN_POS_ENTRY_MODE = "pos_entry_mode"
        const val COLUMN_REMARK = "remark"
        const val COLUMN_BIN = "bin"
        const val COLUMN_EMI_INFO = "emi_info"
        const val COLUMN_CURRENCY_CODE = "currency_code"
        const val COLUMN_VAT_AMOUNT = "vat_amount"
        const val COLUMN_VAT_REFUND_AMOUNT = "vat_refund_amount"
        const val COLUMN_CVM_MESSAGE = "cvm_message"
        const val COLUMN_TRANSACTION_EMV_INFO = "transaction_emv_info"

    }
}

