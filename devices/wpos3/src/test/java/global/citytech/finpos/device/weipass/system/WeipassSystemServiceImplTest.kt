package global.citytech.finpos.device.weipass.system

import global.citytech.finpos.device.weipass.utils.WeipassLogger
import io.mockk.MockKAnnotations
import io.mockk.mockkObject
import org.junit.Before

/**
 * Created by Rishav Chudal on 6/7/20.
 */
//TODO Write test cases
class WeipassSystemServiceImplTest {
    private lateinit var SUT: WeipassSystemServiceImpl

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        SUT = WeipassSystemServiceImpl()
        mockkWangPosManager()
        mockkWeipassLogger()
    }

    private fun mockkWangPosManager() {
        mockkObject(WangPosManager::class)
        mockkObject(WangPosManager.Companion)
    }

    private fun mockkWeipassLogger() {
        mockkObject(WeipassLogger)
        mockkObject(WeipassLogger.Companion)
    }
}