package global.citytech.finpos.neps.iso8583.requestsender.reversal;

import java.math.BigDecimal;

import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;

public class ReversalIsoRequest extends TransactionIsoRequest {
    String processingCode;
    String stan;

    public String getStan() {
        return stan;
    }

    public String getProcessingCode() {
        return processingCode;
    }


    public static final class Builder {
        private String pan;
        private BigDecimal transactionAmount;
        private String track2Data;
        private String additionalData;
        private String currencyCode;
        private String pinBlock;
        private String emvData;
        private boolean isFallbackFromIccToMag;
        private String localDateTime;
        private String processingCode;
        private String stan;


        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public Builder localDateTime(String localDataTime) {
            this.localDateTime = localDataTime;
            return this;
        }

        public Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public Builder additionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder pinBlock(String pinBlock) {
            this.pinBlock = pinBlock;
            return this;
        }

        public Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public Builder processingCode(String processingCode) {
            this.processingCode = processingCode;
            return this;
        }

        public Builder stan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder fallBackFromIccToMag(boolean isFallbackFromIccToMag) {
            this.isFallbackFromIccToMag = isFallbackFromIccToMag;
            return this;
        }

        public ReversalIsoRequest build() {
            ReversalIsoRequest reversalIsoRequest = new ReversalIsoRequest();
            reversalIsoRequest.pinBlock = this.pinBlock;
            reversalIsoRequest.currencyCode = this.currencyCode;
            reversalIsoRequest.pan = this.pan;
            reversalIsoRequest.track2Data = this.track2Data;
            reversalIsoRequest.transactionAmount = this.transactionAmount;
            reversalIsoRequest.emvData = this.emvData;
            reversalIsoRequest.additionalData = this.additionalData;
            reversalIsoRequest.isFallbackFromIccToMag = this.isFallbackFromIccToMag;
            reversalIsoRequest.localDateTime = this.localDateTime;
            reversalIsoRequest.processingCode = this.processingCode;
            reversalIsoRequest.stan = this.stan;
            return reversalIsoRequest;
        }
    }
}
