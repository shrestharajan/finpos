package global.citytech.finpos.nibl.iso8583;

import org.junit.Test;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.reversal.ReversalRequestSender;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Rishav Chudal on 9/28/20.
 */
public class ReversalRequestSenderTest {
    private Logger logger = Logger.getLogger(ReversalRequestSenderTest.class.getName());

    @Test
    public void reversalTransactionSendTest() {
        try {
            String originalTransactionStan = "000007";
            NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
            niblReversalRequest.setProcessingCode("000000");
            niblReversalRequest.setPan("4249720020000234");
            niblReversalRequest.setAmount(100);
            niblReversalRequest.setExpiryDate("2412");
            niblReversalRequest.setPosEntryMode(MockValues.getIccPosEntryMode());
            niblReversalRequest.setPosConditionCode("00");
            niblReversalRequest.setTrack2Data("4249720020000234D24122261393525399999");
            niblReversalRequest.setPinBlock("D5088D7BFE73A450");
            niblReversalRequest.setInvoiceNumber("000006");
            niblReversalRequest.setNii("0011");

            RequestContext requestContext = MockValues.getRequestContext(originalTransactionStan);
            requestContext.setRequest(niblReversalRequest);

            ReversalRequestSender requestSender = new ReversalRequestSender(
                    new NIBLSpecInfo(),
                    requestContext
            );
            IsoMessageResponse msg = requestSender.send();
            assertNotNull(msg);
            assertEquals("0410", msg.getMsg().getMti().toString());
            assertEquals(
                    niblReversalRequest.getProcessingCode(),
                    msg.getMsg()
                            .getDataElementByIndex(DataElement.PROCESSING_CODE)
                            .get()
                            .getAsString()
            );

            assertEquals(
                    originalTransactionStan,
                    msg.getMsg()
                            .getDataElementByIndex(DataElement.SYSTEM_TRACE_AUDIT_NUMBER)
                            .get()
                            .getAsString()
            );

            assertEquals(
                    niblReversalRequest.getNii(),
                    msg.getMsg()
                            .getDataElementByIndex(DataElement.FUNCTION_CODE_OR_NETWORK_INTERNATIONAL_IDENTIFIER)
                            .get()
                            .getAsString()
            );

            assertEquals(
                    MockValues.getTerminalInfo().getTerminalID(),
                    msg.getMsg()
                            .getDataElementByIndex(DataElement.CARD_ACCEPTOR_TERMINAL_ID)
                            .get()
                            .getAsString()
            );

        } catch (FinPosException ex) {
            logger.log(ex.getMessage());
            logger.log(ex.getDetailMessage());
        }
    }
}
