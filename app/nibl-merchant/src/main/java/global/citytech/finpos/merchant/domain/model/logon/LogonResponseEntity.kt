package global.citytech.finpos.merchant.domain.model.logon

/**
 * Created by Unique Shakya on 1/19/2021.
 */
data class LogonResponseEntity(val isSuccess: Boolean)