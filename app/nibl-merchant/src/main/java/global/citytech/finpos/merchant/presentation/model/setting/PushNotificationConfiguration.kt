package global.citytech.finpos.merchant.presentation.model.setting

data class PushNotificationConfiguration(
    val ip: String? = "",
    val port: String? = "",
    val requiresAuth: Boolean = false,
    val userId: String? = "",
    val password: String? = "",
    val connectionTimeOut: Int? = 60,
    val connectionKeepAliveInterval: Int? = 60,
)