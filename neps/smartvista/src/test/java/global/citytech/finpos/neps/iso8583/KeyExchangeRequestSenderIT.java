package global.citytech.finpos.neps.iso8583;

import global.citytech.finposframework.log.Logger;

import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.exceptions.FinPosException;

import static org.junit.Assert.assertNotNull;

/**
 * User: Surajchhetry Date: 2/28/20 Time: 4:13 PM
 */
public class KeyExchangeRequestSenderIT {

    private Logger logger = Logger.getLogger(MerchantLogonMessageSenderIT.class.getName());

    // @Test
    public void sendTest() {
        try {
            HostInfo primaryHost = NEPSHostConfig.getPrimaryConfig();
            HostInfo secondaryHost = NEPSHostConfig.getSecondaryConfig();
            ConnectionParam connectionParam = new ConnectionParam(secondaryHost, primaryHost);
            RequestContext context =
                    new RequestContext(
                            "000001", new TerminalInfo("41234027", "100512345678910",
                            "", "Citytech","Kamaladi"), connectionParam);
            KeyExchangeRequestSender sender = new KeyExchangeRequestSender(context);
            IsoMessageResponse msg = sender.send();
            assertNotNull(msg);
            logger.debug(msg.getUsedHost().toString());
        } catch (FinPosException e) {
            logger.debug(e.getMessage());
            logger.debug(e.getDetailMessage());
        }
    }
}
