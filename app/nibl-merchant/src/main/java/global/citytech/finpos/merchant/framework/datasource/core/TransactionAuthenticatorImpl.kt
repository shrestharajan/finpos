package global.citytech.finpos.merchant.framework.datasource.core

import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.service.AppDeviceFactory
import global.citytech.finposframework.hardware.DeviceServiceType
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse
import global.citytech.finposframework.hardware.keymgmt.pin.PinService
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator
import global.citytech.finposframework.usecases.transaction.data.TransactionAuthorizationResponse

class TransactionAuthenticatorImpl : TransactionAuthenticator {
    override fun authenticate(): TransactionAuthorizationResponse {
        return TransactionAuthorizationResponse(true) //TODO Change
    }

    override fun authenticateUser(p0: PinRequest?): PinResponse {
        val pinService = AppDeviceFactory.getCore(
            NiblMerchant.getActivityContext(),
            DeviceServiceType.PINPAD
        ) as PinService
        p0!!.packageName = NiblMerchant.INSTANCE.packageName
        return pinService.showPinPadAndCalculatePinBlock(p0)
    }
}