package global.citytech.finpos.processor.neps.transaction;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.transaction.TransactionResponseParameter;

/**
 * Created by Saurav Ghimire on 2/3/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class TransactionResponse implements UseCase.Response, TransactionResponseParameter {
    protected String debugRequestMessage;
    protected String debugResponseMessage;
    protected boolean approved;
    protected String message;
    protected String stan;
    protected Result result;
    protected String approvalCode;
    protected boolean shouldPrintCustomerCopy;
    protected ReadCardResponse readCardResponse;


    public String getStan() {
        return stan;
    }

    public boolean shouldPrintCustomerCopy() {
        return shouldPrintCustomerCopy;
    }

    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }

    public boolean isApproved() {
        return approved;
    }

    public String getMessage() {
        return message;
    }

    public Result getResult() {
        return result;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public ReadCardResponse getReadCardResponse() {
        return readCardResponse;
    }

    @Override
    public String toString() {
        return "TransactionResponseModel{" +
                "debugRequestMessage='" + debugRequestMessage + '\'' +
                ", debugResponseMessage='" + debugResponseMessage + '\'' +
                ", success=" + approved +
                ", message=" + message +
                ", result=" + result +
                ", approvalCode=" + approvalCode +
                '}';
    }
}
