package global.citytech.finpos.merchant.utils

/**
 * Created by Saurav Ghimire on 4/26/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


object ApiConstant {
    const val UPDATE_MERCHANT_TRANSACTION_LOG_URL = "/devices-tms/v1/logs"
    const val UPDATE_ACTIVITY_LOG_URL = "/device-tms-platform/v1/activityLogs"
    const val COMMAND_CHECK_URL = "/device-tms-platform/v1/device/commands"
    const val COMMAND_EXECUTION_ACK_URL = "/device-tms-platform/v1/device/commands/execution/acknowledge"
    const val COMMAND_RECEIVE_ACK_URL = "/device-tms-platform/v1/device/commands/acknowledge"
    const val GET_TERMINAL_BANNERS = "/device-tms-platform/v1/device/public/banners"
    const val CONFIG_URL = "/devices-terminal/v1/configs/public/switch"
    const val SYNC_SETTING = "/device-tms-platform/v1/device/settings"
    const val DISCLAIMER_ACK_URL = "/device-tms-platform/v1/device/disclaimer/acknowledge"
    const val CUSTOMER_RECEIPT_URL = "finpos/tms/#/receipt/"
    const val MERCHANT_APP_BASE_URL = "master"
}