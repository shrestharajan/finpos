package global.citytech.finposframework.comm;

import java.util.Objects;

import global.citytech.finposframework.supports.Context;

/** User: Surajchhetry Date: 2/20/20 Time: 2:10 PM */
public final class HostInfo {
  private final String ip;
  private final int port;
  private final int timeOut;
  private final int retryLimit;
  private final TPDU tpdu;
  private final ConnectionEventListener eventListener;
  private static final int DEFAULT_TIME_SECOND = 2;
  private boolean isSecureConnection = false;
  private TlsCertificate tlsCertificate;

  private HostInfo(Builder builder) {
    this.ip = builder.ip;
    this.port = builder.port;
    this.timeOut = (builder.timeOut <= 0) ? DEFAULT_TIME_SECOND : builder.timeOut;
    this.retryLimit = builder.retryLimit;
    this.tpdu = builder.tpdu;
    this.eventListener = builder.eventListener;
    this.isSecureConnection = builder.isSecureConnection;
    this.tlsCertificate = builder.tlsCertificate;
  }

  public TPDU getTpdu() {
    return tpdu;
  }

  public String getIp() {
    return ip;
  }

  public int getPort() {
    return port;
  }

  public int getTimeOut() {
    return timeOut;
  }

  public int getRetryLimit() {
    return retryLimit;
  }

  public void raiseEvent(ConnectionEvent event, Context context) {
    if (this.eventListener != null) {
      this.eventListener.listen(event, context);
    }
  }

  public boolean isSecureConnection() {
    return isSecureConnection;
  }

  public TlsCertificate getTlsCertificate() {
    return tlsCertificate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    HostInfo hostInfo = (HostInfo) o;
    return port == hostInfo.port && ip.equals(hostInfo.ip);
  }

  @Override
  public int hashCode() {
    return Objects.hash(ip, port);
  }

  @Override
  public String toString() {
    return "HostInfo{" +
            "ip='" + ip + '\'' +
            ", port=" + port +
            ", timeOut=" + timeOut +
            ", retryLimit=" + retryLimit +
            ", tpdu=" + tpdu +
            ", eventListener=" + eventListener +
            ", isSecureConnection=" + isSecureConnection +
            ", tlsCertificate=" + tlsCertificate +
            '}';
  }

  public static final class Builder {
    private String ip;
    private int port;
    private int timeOut;
    private int retryLimit;
    private TPDU tpdu;
    private ConnectionEventListener eventListener;
    private boolean isSecureConnection = false;
    private TlsCertificate tlsCertificate;

    private Builder(String ip, int port) {
      this.ip = ip;
      this.port = port;
    }

    public static Builder createDefaultBuilder(String ip, int port) {
      return new Builder(ip, port);
    }

    public Builder timeOut(int timeOut) {
      this.timeOut = timeOut;
      return this;
    }

    public Builder retryLimit(int retryLimit) {
      this.retryLimit = retryLimit;
      return this;
    }

    public Builder tpduString(String hexTpduString) {
      this.tpdu = TPDU.fromHexString(hexTpduString);
      return this;
    }

    public Builder addEventListener(ConnectionEventListener eventListener) {
      this.eventListener = eventListener;
      return this;
    }

    public Builder secureConnection(boolean isSecureConnection) {
      this.isSecureConnection = isSecureConnection;
      return this;
    }

    public Builder tlsCertificate(TlsCertificate tlsCertificate) {
      this.tlsCertificate = tlsCertificate;
      return this;
    }

    public HostInfo build() {
      return new HostInfo(this);
    }
  }
}
