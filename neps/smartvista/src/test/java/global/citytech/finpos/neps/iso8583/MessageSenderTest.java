package global.citytech.finpos.neps.iso8583;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.comm.Request;
import global.citytech.finposframework.comm.Response;
import global.citytech.finposframework.comm.SocketClient;
import global.citytech.finposframework.utility.EncodingUtils;
import global.citytech.finposframework.utility.HexDump;
import global.citytech.finposframework.utility.NumberUtils;
import global.citytech.finposframework.supports.MessagePayload;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/** User: Surajchhetry Date: 2/20/20 Time: 10:05 AM */
public class MessageSenderTest {

  private Logger LOGGER = Logger.getLogger(MessageSenderTest.class.getName());

 // @Test
  public void hostConnectionTest() {
    HostInfo primaryHost =
        HostInfo.Builder.createDefaultBuilder("127.0.0.1", 0)
            .addEventListener(
                (event, context) -> {
                  LOGGER.log(context.get("MSG").toString());
                })
            .build();
    int headerLength = 2;
    int dataLength = 80;
    byte[] sampleResponse =
        EncodingUtils.hex2Bytes(
            "0050600000030330383130222000000A8008003939303030303032323031373233333130303030303130303032383837313739383930303034313233343032373136339F0E9B180A1F00575785397BBA3C01");
    byte[] sampleLength = {sampleResponse[0], sampleResponse[1]};

    SocketClient tcpClient = mock(SocketClient.class);
    when(tcpClient.dial()).thenReturn(primaryHost);
    when(tcpClient.write(any(Request.class))).thenReturn(true);
    when(tcpClient.read(headerLength)).thenReturn(new Response(sampleLength));
    byte[] onlyResponse = new byte[dataLength];
    System.arraycopy(sampleResponse, headerLength, onlyResponse, 0, 80);
    when(tcpClient.read(dataLength)).thenReturn(new Response(onlyResponse));

    HostInfo result = tcpClient.dial();

    tcpClient.write(this.getRequest());
    Response lenResponse = tcpClient.read(headerLength);

    LOGGER.log(" Response Received ");
    int len = NumberUtils.fromHexASCIIBytesToInt(lenResponse.getData());
    LOGGER.log("Header Length : " + len);
    Response data = tcpClient.read(len);
    LOGGER.log(" ==== RESPONSE ====");
    LOGGER.log(HexDump.dumpHexString(data.getData()));
  }

  public Request getRequest() {
    MTI mti =
        new MTI(
            MTI.Version.Version_1987,
            MTI.MessageClass.Network_Management,
            MTI.MessageFunction.Request,
            MTI.Originator.Acquirer);
    Iso8583Msg msg = new Iso8583Msg(mti);
    msg.addField(new DataElement(3, "990000"));
    msg.addField(new DataElement(7, "1111111111"));
    msg.addField(new DataElement(11, "000001"));
    msg.addField(new DataElement(24, "811"));
    msg.addField(new DataElement(41, "41234027"));
    msg.addField(new DataElement(42, "100512345678910"));

    NepsMessagePayloadBuilder builder = new NepsMessagePayloadBuilder(new NepsSpecInfo(), msg);
    MessagePayload payload = builder.build();
    LOGGER.log(HexDump.dumpHexString(payload.getAsBytes()));
    return new Request(payload.getAsBytes());
  }
}
