package global.citytech.finpos.merchant.presentation.dtg

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import global.citytech.finpos.merchant.R
import kotlinx.android.synthetic.main.fragment_setgps.*

class SetGpsFragment : DialogFragment() {


    private var northSouth = "N"
    private var eastWest = "E"

    private var listener: SetGpsListener? = null

    companion object {
        fun newInstance(): SetGpsFragment {
            return SetGpsFragment()
        }
    }

    interface SetGpsListener {
        fun onSubmitButtonClicked(gpsLocation: GpsLocation?)
        fun onCancelButtonClicked()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as SetGpsListener
    }

    override fun onStart() {
        super.onStart()
        handleRadioGroupCheckChangedListener()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_setgps,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_submit.setOnClickListener {
            validateEditTextFields(
                et_latitude_degree.text.toString(),
                et_latitude_minute.text.toString(),
                et_latitude_second.text.toString(),
                et_longitude_degree.text.toString(),
                et_longitude_minute.text.toString(),
                et_longitude_second.text.toString()
            )
        }

        btn_cancel.setOnClickListener {
            listener!!.onCancelButtonClicked()
        }
    }

    private fun validateEditTextFields(
        latitudeDegree: String,
        latitudeMinute: String,
        latitudeSecond: String,
        longitudeDegree: String,
        longitudeMinute: String,
        longitudeSecond: String
    ) {
        if (TextUtils.isEmpty(latitudeDegree)) {
            showToast(getString(R.string.error_empty_latitude_degree))
            et_latitude_degree.requestFocus()
            return
        }
        if (TextUtils.isEmpty(latitudeMinute)) {
            showToast(getString(R.string.error_empty_latitude_minute))
            et_latitude_minute.requestFocus()
            return
        }
        if (TextUtils.isEmpty(latitudeSecond)) {
            showToast(getString(R.string.error_empty_latitude_second))
            et_latitude_second.requestFocus()
            return
        }
        if (TextUtils.isEmpty(longitudeDegree)) {
            showToast(getString(R.string.error_empty_longitude_degree))
            et_longitude_degree.requestFocus()
            return
        }
        if (TextUtils.isEmpty(longitudeMinute)) {
            showToast(getString(R.string.error_empty_longitude_minute))
            et_longitude_minute.requestFocus()
            return
        }
        if (TextUtils.isEmpty(longitudeSecond)) {
            showToast(getString(R.string.error_empty_longitude_second))
            et_longitude_second.requestFocus()
            return
        }
        val gpsLocation = GpsLocation(
            northSouth, latitudeDegree, latitudeMinute,
            latitudeSecond, eastWest, longitudeDegree, longitudeMinute, longitudeSecond
        )
        listener!!.onSubmitButtonClicked(gpsLocation)

    }

    private fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    private fun handleRadioGroupCheckChangedListener() {
        rg_north_south.setOnCheckedChangeListener { _: RadioGroup?, checkedId: Int ->
            when (checkedId) {
                R.id.rbtn_north -> northSouth = "N"
                R.id.rbtn_south -> northSouth = "S"
                else -> {
                }
            }
        }

        rg_east_west.setOnCheckedChangeListener { _: RadioGroup?, checkedId: Int ->
            when (checkedId) {
                R.id.rbtn_east -> eastWest = "E"
                R.id.rbtn_west -> eastWest = "W"
                else -> {
                }
            }
        }
    }

}