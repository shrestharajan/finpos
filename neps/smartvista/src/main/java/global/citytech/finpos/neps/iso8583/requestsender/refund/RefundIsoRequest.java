package global.citytech.finpos.neps.iso8583.requestsender.refund;


import java.math.BigDecimal;

import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;

/**
 * Created by Saurav Ghimire on 1/21/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class RefundIsoRequest extends TransactionIsoRequest {

    private String originalRrn;

    public String getOriginalRrn() {
        return originalRrn;
    }


    public static final class Builder {
        private String pan;
        private BigDecimal transactionAmount;
        private String track2Data;
        private String additionalData;
        private String currencyCode;
        private String pinBlock;
        private String emvData;
        private boolean isFallbackFromIccToMag;
        private String localDateTime;
        private String originalRrn;
        private String cvv;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public Builder cvv(String cvv) {
            this.cvv = cvv;
            return this;
        }

        public Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public Builder localDateTime(String localDataTime) {
            this.localDateTime = localDataTime;
            return this;
        }

        public Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public Builder additionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder pinBlock(String pinBlock) {
            this.pinBlock = pinBlock;
            return this;
        }

        public Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public Builder originalRrn(String originalRrn) {
            this.originalRrn = originalRrn;
            return this;
        }

        public Builder fallBackFromIccToMag(boolean isFallbackFromIccToMag) {
            this.isFallbackFromIccToMag = isFallbackFromIccToMag;
            return this;
        }

        public RefundIsoRequest build() {
            RefundIsoRequest refundIsoRequest = new RefundIsoRequest();
            refundIsoRequest.pinBlock = this.pinBlock;
            refundIsoRequest.currencyCode = this.currencyCode;
            refundIsoRequest.pan = this.pan;
            refundIsoRequest.track2Data = this.track2Data;
            refundIsoRequest.transactionAmount = this.transactionAmount;
            refundIsoRequest.emvData = this.emvData;
            refundIsoRequest.additionalData = this.additionalData;
            refundIsoRequest.isFallbackFromIccToMag = this.isFallbackFromIccToMag;
            refundIsoRequest.localDateTime = this.localDateTime;
            refundIsoRequest.originalRrn = this.originalRrn;
            refundIsoRequest.cvv = this.cvv;
            return refundIsoRequest;
        }
    }
}
