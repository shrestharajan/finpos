package global.citytech.finpos.nibl.iso8583.requestsender.purchase;

import org.junit.Before;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.NIBLHostConfig;
import global.citytech.finpos.nibl.iso8583.MockValues;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.utility.EncodingUtils;
import global.citytech.finposframework.utility.HexDump;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Rishav Chudal on 10/16/20.
 */
public class IccPurchaseRequestSenderTest {
    private Logger logger = Logger.getLogger(IccPurchaseRequestSenderTest.class.getName());

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void send() {
        try {
            String terminalID = "99991002";
            String axquirerID = "405635000000002";
            TerminalInfo terminalInfo = new TerminalInfo(terminalID, axquirerID,"","Citytech","Kamaladi");
            HostInfo primaryHost = NIBLHostConfig.getPrimaryConfig();
            HostInfo secondaryHost = NIBLHostConfig.getSecondaryConfig();
            ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);
            RequestContext context =
                    new RequestContext(
                            "000405", terminalInfo, connectionParam);
            PurchaseRequest request = new PurchaseRequest();
            request.setTransactionAmount(105.0);
            request.setPosConditionCode("00");
            request.setPinBlock("7C8DE0C78AEA2044");
            request.setInvoiceNumber("000108");
            request.setCardSequenceNumber("01");
            request.setTrack2Data(MockValues.TRACK_2_DATA);
            request.setEmvData("82023C009F02060000000000289F03060000000000004F07A0000000031010500B56495341204352454449549F120B56495341204352454449549F360200CA9F26085C4637D46AFA9A359F2701809F34034203008407A00000000310109F100706010A03A0A8009F1E0831313130303837379F3303E0F8C89F350122950508000400009F1A0205245F2A0205249A032008289C01009F370460C801959F090200969F4104000012345F340101");

            context.setRequest(request);
            IccPurchaseRequestSender sender = new IccPurchaseRequestSender(
                    new NIBLSpecInfo(),
                    context
            );
            IsoMessageResponse response = sender.send();
            assertNotNull(response);

        } catch (FinPosException ex) {
            logger.log(ex.getMessage());
            logger.log(ex.getDetailMessage());
        }
    }

    @Test
    public void trackTwoFormattingTest(){
        String track2 = "==";
        byte[] result = EncodingUtils.hex2Bytes(track2);
        logger.log("::: FORMATTED TRACK 2 DATA ::: " + HexDump.dumpHexString(result));
        logger.log("::: FORMATTED TRACK 2 DATA ::: " + HexDump.dumpHexString(track2.getBytes(StandardCharsets.UTF_8)));
    }
}