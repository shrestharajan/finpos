package global.citytech.finpos.processor.nibl.transaction.tipadjustment;

import global.citytech.finpos.processor.nibl.transaction.TransactionRequestModel;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;

/**
 * Created by Unique Shakya on 5/4/2021.
 */
public class TipAdjustmentRequestModel extends TransactionRequestModel {

    public static final class Builder {
        private TransactionRequest transactionRequest;
        private TransactionRepository transactionRepository;
        private DeviceController deviceController;
        private PrinterService printerService;
        private ApplicationRepository applicationRepository;
        private LedService ledService;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder transactionRequest(TransactionRequest transactionRequest) {
            this.transactionRequest = transactionRequest;
            return this;
        }

        public Builder transactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public Builder deviceController(DeviceController deviceController) {
            this.deviceController = deviceController;
            return this;
        }

        public Builder printerService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public Builder applicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public Builder ledService(LedService ledService) {
            this.ledService = ledService;
            return this;
        }

        public TipAdjustmentRequestModel build() {
            TipAdjustmentRequestModel tipAdjustmentRequestModel = new TipAdjustmentRequestModel();
            tipAdjustmentRequestModel.transactionRepository = this.transactionRepository;
            tipAdjustmentRequestModel.printerService = this.printerService;
            tipAdjustmentRequestModel.transactionRequest = this.transactionRequest;
            tipAdjustmentRequestModel.deviceController = this.deviceController;
            tipAdjustmentRequestModel.applicationRepository = this.applicationRepository;
            tipAdjustmentRequestModel.ledService = this.ledService;
            return tipAdjustmentRequestModel;
        }
    }
}
