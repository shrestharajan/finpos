package global.citytech.finpos.processor.nibl.reconciliation.clear;

import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reconciliation.clear.BatchClearResponseParameter;

/**
 * Created by Unique Shakya on 9/28/2021.
 */
public class BatchClearResponseModel implements BatchClearResponseParameter, UseCase.Response {

    private boolean isSuccess;
    private String message;

    public BatchClearResponseModel(boolean isSuccess, String message) {
        this.isSuccess = isSuccess;
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public boolean isSuccess() {
        return isSuccess;
    }
}
