package global.citytech.finpos.processor.neps.reversal;

import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;

/**
 * Created by Rishav Chudal on 9/29/20.
 */
public class ReversalRequest extends global.citytech.finpos.processor.neps.transaction.TransactionRequest {

    private String processingCode;
    private String stan;
    private String track2Data;
    private ReceiptLog receiptLog;
    private String currencyCode;
    private ReadCardResponse readCardResponse;

    public String getTrack2Data() {
        return track2Data;
    }

    public String getStan() {
        return stan;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public ReceiptLog getReceiptLog() {
        return receiptLog;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public ReadCardResponse getReadCardresponse() {
        return readCardResponse;
    }

    public static final class Builder {
        private TransactionRequest transactionRequest;
        private TransactionRepository transactionRepository;
        private PrinterService printerService;
        private ApplicationRepository applicationRepository;
        private String processingCode;
        private String stan;
        private String track2Data;
        private ReceiptLog receiptLog;
        private String currencyCode;
        private ReadCardResponse readCardResponse;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withTransactionRequest(TransactionRequest transactionRequest) {
            this.transactionRequest = transactionRequest;
            return this;
        }

        public Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public Builder withProcessingCode(String processingCode) {
            this.processingCode = processingCode;
            return this;
        }

        public Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder withTrack2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public Builder withReceiptLog(ReceiptLog receiptLog) {
            this.receiptLog = receiptLog;
            return this;
        }

        public Builder withCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder withReadCardResponse(ReadCardResponse readCardResponse) {
            this.readCardResponse = readCardResponse;
            return this;
        }


        public ReversalRequest build() {
            ReversalRequest reversalRequestModel = new ReversalRequest();
            reversalRequestModel.transactionRequest = this.transactionRequest;
            reversalRequestModel.transactionRepository = this.transactionRepository;
            reversalRequestModel.applicationRepository = this.applicationRepository;
            reversalRequestModel.printerService = this.printerService;
            reversalRequestModel.processingCode = this.processingCode;
            reversalRequestModel.stan = this.stan;
            reversalRequestModel.track2Data = this.track2Data;
            reversalRequestModel.receiptLog = this.receiptLog;
            reversalRequestModel.currencyCode = this.currencyCode;
            reversalRequestModel.readCardResponse = this.readCardResponse;
            return reversalRequestModel;
        }
    }
}
