package global.citytech.finpos.merchant.domain.model.device

/**
 * Created by Rishav Chudal on 6/25/20.
 */
data class LoadParameterRequest(val loadAllEMVConfigurationInHardware: Boolean)