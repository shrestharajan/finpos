package global.citytech.finpos.device.qualcomm.io.cards.handler

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import com.ftpos.library.smartpos.emv.CandidateAIDInfo
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.util.EncodeConversionUtil
import global.citytech.finpos.device.qualcomm.R
import global.citytech.finpos.device.qualcomm.io.cards.transaction.FeitianSingleton
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MULTI_AID_LIST_EMPTY
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MULTI_AID_SELECTION_CANCELLED
import java.lang.ref.WeakReference
import java.util.concurrent.CountDownLatch
import java.util.concurrent.atomic.AtomicInteger

/**
 * Created by Rishav Chudal on 4/7/21.
 */
class FeitianApplicationSelectionHandler constructor(
    private val context: Context,
    private val reselectApplication: Boolean,
    private val candidateAIDInfos: List<CandidateAIDInfo>
): FeitianLogger.Listener {

    private val contextWeakReference = WeakReference(context)
    private val emv = Emv.getInstance(context.applicationContext)
    private val logger = FeitianLogger(this)
    private var appSelectedIndex = 0

    fun getSelectedApplicationIndex(): Int {
        this.logger.logWithClassName(
            "processApplicationSelection ::: Is Application Selection repeated? ::: "
                .plus(reselectApplication)
        )
        if (candidateAIDInfos.isEmpty()) {
            this.onAidListEmpty()
        } else if (candidateAIDInfos.size == 1) {
            this.onSingleAidPresent()
        } else {
            this.onMultipleAidPresent()
        }
        return this.appSelectedIndex
    }

    private fun onAidListEmpty() {
        this.logger.logWithClassName("AID List is empty...")
        this.appSelectedIndex = MULTI_AID_LIST_EMPTY
    }

    private fun onSingleAidPresent() {
        this.logger.logWithClassName("AID List contains single AID...")
        appSelectedIndex = 0
    }

    private fun onMultipleAidPresent() {
        this.logger.logWithClassName("AID List contains Multiple AID...")
        this.logger.logWithClassName(
            "processApplicationSelection ::: AID List Size ::: ".plus(candidateAIDInfos.size)
        )
        val encodeConversionTableIndex: Byte = 1
        val applicationList = Array<String>(candidateAIDInfos.size) { position ->
            val str = EncodeConversionUtil.EncodeConversion(
                candidateAIDInfos[position].applicationLabel_tag50,
                encodeConversionTableIndex
            )
            this.logger.logWithClassName("onAppSelect ::: App Name ::: ".plus(str))
            str
        }
        this.showDialogForApplicationSelection(applicationList)
    }

    private fun showDialogForApplicationSelection(applicationList: Array<String>) {
        val countDownLatch = CountDownLatch(1)
        val selectItem = AtomicInteger(0)
        val runnable = Runnable {
            val alertDialog: AlertDialog =
                AlertDialog.Builder(contextWeakReference.get())
                    .setSingleChoiceItems(
                        applicationList,
                        -1
                    ) { dialog: DialogInterface, which: Int ->
                        this.logger.logWithClassName("::: SELECTED INDEX OF AID ::: ".plus(which))
                        this.logger.logWithClassName(
                            "::: SELECTED AID ::: ".plus(applicationList[which])
                        )
                        selectItem.set(which)
                        dialog.dismiss()
                        countDownLatch.countDown()
                    }
                    .setNegativeButton(R.string.action_cancel) { dialog, which ->
                        selectItem.set(MULTI_AID_SELECTION_CANCELLED)
                        onApplicationSelectionCancelled(dialog, countDownLatch)
                    }
                    .setCancelable(false)
                    .create()
            FeitianSingleton.alertDialogWeakReference = WeakReference(alertDialog)
            if (FeitianSingleton.alertDialogWeakReference != null) {
                if (FeitianSingleton.alertDialogWeakReference!!.get() != null) {
                    FeitianSingleton.alertDialogWeakReference!!.get()!!.show()
                } else {
                    onApplicationSelectionCancelled(alertDialog, countDownLatch)
                }
            } else {
                onApplicationSelectionCancelled(alertDialog, countDownLatch)
            }
        }
        (this.contextWeakReference.get() as Activity?)!!.runOnUiThread(runnable)
        try {
            countDownLatch.await()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        this.appSelectedIndex = selectItem.get()
    }

    private fun onApplicationSelectionCancelled(
        dialogInterface: DialogInterface,
        countDownLatch: CountDownLatch
    ) {
        dialogInterface.dismiss()
        countDownLatch.countDown()
        this.appSelectedIndex = MULTI_AID_SELECTION_CANCELLED
    }

    override fun getClassName() = FeitianApplicationSelectionHandler::class.java.simpleName
}