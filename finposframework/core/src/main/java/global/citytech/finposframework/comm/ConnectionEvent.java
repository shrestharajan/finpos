package global.citytech.finposframework.comm;

/** User: Surajchhetry Date: 2/20/20 Time: 2:11 PM */
public enum ConnectionEvent {
  CONNECTING,
  CONNECTED,
  CONNECTION_FAILED,
  CONNECTING_TO_SECONDARY_HOST,
  REDIALING
}
