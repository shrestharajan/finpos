package global.citytech.finpos.merchant.presentation.log

import android.os.Environment
import android.util.Log
import java.io.File
import java.io.IOException
import java.util.*

/**
 * Created by BikashShrestha on 2020-04-06.
 */
class LogManager {
    private val cmdBegin = "logcat -v time -f"
    private var logFileAbsolutePath: String? = null
    private val cmdEnd = ""
    private var isLogStarted = false
    private var process: Process? = null
    private val LOG_COUNTER_MILLISECOND = 30000.toLong()
    private var logCounter: LogCounter? = null

    companion object {
        private var INSTANCE: LogManager? = null
        private var DEBUG_DURATION: Int = 0
        private var APPLICATION_ID: String? = "UNKNOWN"
        var LOG_DIR_PATH: String =
            Environment.getExternalStorageDirectory().absolutePath + "/finPulse/log/"  //change deprecated api

        fun getInstance(applicationID: String, debugDuration: Int ): LogManager {
            clear()
            if (INSTANCE == null) {
                INSTANCE =
                    LogManager()
            }
            APPLICATION_ID = applicationID
            DEBUG_DURATION = debugDuration
            return INSTANCE!!
        }

        fun getLogDir(): File {
            val dir = File(LOG_DIR_PATH)
            if (!dir.exists())
                dir.mkdirs()
            return dir
        }

        fun clear(){
            if(INSTANCE != null) {
                INSTANCE!!.cancel()
                INSTANCE = null
            }
        }
    }

    fun initLog() {
        if(!isLogStarted) {
            startWritingLog()
            logCounter = LogCounter(
                DEBUG_DURATION * 60000.toLong(),
                this::onCountDownTick,
                this::onCountDownFinish
            )
            logCounter?.start()
        }
    }

    fun cancel(){
        stopWritingLog()
        logCounter?.cancel()
    }

    private fun stopWritingLog() {
        if (process != null) {
            process?.destroy()
            isLogStarted = false
            process = null
        }
    }

    private fun getCurrentTime(): String {
        return Date().time.toString()
    }

    private fun clearLog() {
        try {
            Runtime.getRuntime().exec("logcat -c")
        } catch (e: IOException) {
            log(e.localizedMessage)
        }

    }

    private fun log(message: String?) {
        Log.d("LOGGER", "LOG MANAGER :: $message")
    }

    private fun startWritingLog() {
        try {
            val fileName = "${APPLICATION_ID}_" + getCurrentTime() + ".txt"
            val outputFile = getLogDir()
            logFileAbsolutePath = outputFile.absolutePath + "/" + fileName
            clearLog()
            val prevLogFile = File(logFileAbsolutePath!!)
            prevLogFile.delete()
            process = Runtime.getRuntime().exec(cmdBegin + logFileAbsolutePath + cmdEnd)
            isLogStarted = true
        } catch (ignored: IOException) {
            this.log("initLogCat: failed")
        }
    }

    private fun onCountDownFinish() {
        this.log(":: LOG COUNTER onCountDownFinish :: ")
        cancel()
    }

    private fun onCountDownTick() {
        this.log(":: LOG COUNTER COUNT DOWN TICK :: ")
        stopWritingLog()
        startWritingLog()
    }
}