package global.citytech.finpos.merchant.presentation.splash

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.datasource.device.DeviceConfigurationSource
import global.citytech.finpos.merchant.data.repository.app.AppNotificationRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.AutoReversalQueueRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.CommandQueueRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.device.DeviceRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.*
import global.citytech.finpos.merchant.domain.model.device.DeviceResponse
import global.citytech.finpos.merchant.domain.model.device.DeviceResult
import global.citytech.finpos.merchant.domain.model.device.LoadParameterRequest
import global.citytech.finpos.merchant.domain.repository.device.DeviceRepository
import global.citytech.finpos.merchant.domain.usecase.app.AppNotificationUseCase
import global.citytech.finpos.merchant.domain.usecase.app.CommandQueueUseCase
import global.citytech.finpos.merchant.domain.usecase.app.CoreAutoReversalQueueUseCase
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.device.DeviceUseCase
import global.citytech.finpos.merchant.framework.datasource.app.*
import global.citytech.finpos.merchant.framework.datasource.device.DeviceConfigurationSourceImpl
import global.citytech.finpos.merchant.utils.AppConstant
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.utility.StringUtils
import io.reactivex.Single
import io.reactivex.SingleOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

/**
 * Created by Rishav Chudal on 8/10/20.
 */
class SplashViewModel(val context: Application) :
    BaseAndroidViewModel(context) {

    val isTerminalConfigured by lazy { MutableLiveData<Boolean>() }
    val errorMessage by lazy { MutableLiveData<String>() }
    val autoReversalPresent by lazy { MutableLiveData<Boolean>() }
    val askAppPermission by lazy { MutableLiveData<Boolean>() }

    var deviceConfigurationSource: DeviceConfigurationSource =
        DeviceConfigurationSourceImpl(context)
    var deviceRepository: DeviceRepository = DeviceRepositoryImpl(deviceConfigurationSource)
    var deviceConfigurationUseCase = DeviceUseCase(deviceRepository)

    private val localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    private val appNotificationUseCase = AppNotificationUseCase(
        AppNotificationRepositoryImpl(
            AppNotificationDataSourceImpl(context)
        )
    )

    private val coreAutoReversalQueueUseCase = CoreAutoReversalQueueUseCase(
        AutoReversalQueueRepositoryImpl(
            AutoReversalQueueDataSourceImpl(context)
        )
    )

    private val commandQueueUseCase = CommandQueueUseCase(
        CommandQueueRepositoryImpl(
            CommandQueueDataSourceImpl(context)
        )
    )

    fun initializeSdk() {
        isLoading.value = true
        compositeDisposable.add(
            deviceConfigurationUseCase.initSdk()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::onSdkInitializedResponse,
                    this::onErrorInSdkInitialize
                )
        )
    }

    fun checkForAutoReversal() {
        if (coreAutoReversalQueueUseCase.isQueueEmpty()) {
            Logger.getLogger("AutoReversal").log("::: AUTO REVERSAL ::: QUEUE IS EMPTY")
            autoReversalPresent.value = false
        } else {
            Logger.getLogger("AutoReversal").log("::: AUTO REVERSAL ::: QUEUE IS NOT EMPTY")
            autoReversalPresent.value = coreAutoReversalQueueUseCase.isActive()
        }
    }

    private fun onSdkInitializedResponse(deviceResponse: DeviceResponse) {
        if (deviceResponse.result == DeviceResult.SUCCESS) {
            checkQrDisclaimerNotification()
        } else {
            isLoading.value = false
        }
    }

    private fun onErrorInSdkInitialize(throwable: Throwable) {
        isLoading.value = false
        errorMessage.value = throwable.message
    }

    private fun configureTerminal() {
        isLoading.value = true
        val loadAllEMVConfigurationInHardware: Boolean =
            BuildConfig.DO_LOAD_EMV_CONFIGURATION.equals("true", true)
        compositeDisposable.add(
            deviceConfigurationUseCase.loadTerminalParameters(
                LoadParameterRequest(loadAllEMVConfigurationInHardware)
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::onConfigureTerminalResponse,
                    this::onConfigureTerminalError
                )
        )
    }

    private fun onConfigureTerminalResponse(deviceResponse: DeviceResponse) {
        isLoading.value = false
        isTerminalConfigured.value = deviceResponse.result == DeviceResult.SUCCESS
        NiblMerchant.INSTANCE.isTerminalConfigured.value =
            deviceResponse.result == DeviceResult.SUCCESS
    }

    private fun onConfigureTerminalError(throwable: Throwable) {
        isLoading.value = false
        errorMessage.value = throwable.message
    }

    private fun checkQrDisclaimerNotification() {
        isLoading.value = true
        compositeDisposable.add(Single.create(
            SingleOnSubscribe<Boolean> {
                if (localDataUseCase.getShowQrDisclaimerSetting()) {
                    appNotificationUseCase.addNotification(
                        AppNotification(
                            title = "",
                            body = context.getString(R.string.msg_qr_disclaimer_notification),
                            timeStamp = StringUtils.dateTimeStamp(),
                            action = Action.QR_DISCLAIMER,
                            read = false,
                            type = Type.WARNING
                        )
                    )
                } else {
                    appNotificationUseCase.removeNotificationByAction(Action.QR_DISCLAIMER)
                }
                it.onSuccess(true)
            }
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                isLoading.value = false
                configureTerminal()
            }, {
                isLoading.value = false
                configureTerminal()
            })
        )
    }

    fun checkForAppUpdate() {
        val storedAppVersion = localDataUseCase.getStoredAppVersion()
        val currentAppVersion = BuildConfig.VERSION_CODE
        if (currentAppVersion > storedAppVersion) {
            localDataUseCase.setSwitchConfigActive(true)
            localDataUseCase.saveAppVersion(currentAppVersion)

            if (currentAppVersion >= AppConstant.APP_VERSION_CODE_1_9_0) {
                localDataUseCase.saveVatRefundSupportedPref(false)
            }
        }
        askAppPermission.value = true
    }

}