package global.citytech.finpos.processor.neps.receipt.summmaryreport;

import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Saurav Ghimire on 3/30/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */

public class CardSchemeSummary {
    private long salesCount;
    private double salesAmount;
    private long voidCount;
    private double voidAmount;
    private long refundCount;
    private double refundAmount;

    public long getSalesCount() {
        return salesCount;
    }

    public double getSalesAmount() {
        return salesAmount;
    }

    public long getVoidCount() {
        return voidCount;
    }

    public double getVoidAmount() {
        return voidAmount;
    }

    public long getRefundCount() {
        return refundCount;
    }

    public double getRefundAmount() {
        return refundAmount;
    }

    public String getSalesCountInString() {
        return StringUtils.ofRequiredLength(salesCount, 4);
    }

    public String getSalesAmountInString() {
        return StringUtils.formatAmountTwoDecimal(HelperUtils.fromIsoAmount(String.valueOf(salesAmount)));
    }

    public String getVoidCountInString() {
        return StringUtils.ofRequiredLength(voidCount, 4);
    }

    public String getVoidAmountInString() {
        return StringUtils.formatAmountTwoDecimal(HelperUtils.fromIsoAmount(String.valueOf(voidAmount)));
    }

    public String getRefundCountInString() {
        return StringUtils.ofRequiredLength(refundCount, 4);
    }

    public String getRefundAmountInString() {
        return StringUtils.formatAmountTwoDecimal(HelperUtils.fromIsoAmount(String.valueOf(refundAmount)));
    }

    public long getTotalCount(){
        return salesCount + refundCount + voidCount;
    }

    public String getTotalAmountForReceipt() {
        return StringUtils.formatAmountTwoDecimal(HelperUtils.fromIsoAmount(String.valueOf(salesAmount)));
    }

    public String getTotalAmountInString() {
        return StringUtils.formatAmountTwoDecimal(HelperUtils.fromIsoAmount(String.valueOf(salesAmount + voidAmount + refundAmount)));
    }

    public String getTotalCountForReceipt() {
        return StringUtils.ofRequiredLength(salesCount, 4);
    }

    public static final class Builder {
        private long salesCount;
        private double salesAmount;
        private long voidCount;
        private double voidAmount;
        private long refundCount;
        private double refundAmount;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withSalesCount(long salesCount) {
            this.salesCount = salesCount;
            return this;
        }

        public Builder withSalesAmount(double salesAmount) {
            this.salesAmount = salesAmount;
            return this;
        }

        public Builder withVoidCount(long voidCount) {
            this.voidCount = voidCount;
            return this;
        }

        public Builder withVoidAmount(double voidAmount) {
            this.voidAmount = voidAmount;
            return this;
        }

        public Builder withRefundCount(long refundCount) {
            this.refundCount = refundCount;
            return this;
        }

        public Builder withRefundAmount(double refundAmount) {
            this.refundAmount = refundAmount;
            return this;
        }

        public CardSchemeSummary build() {
            CardSchemeSummary cardSchemeSummary = new CardSchemeSummary();
            cardSchemeSummary.refundAmount = this.refundAmount;
            cardSchemeSummary.salesCount = this.salesCount;
            cardSchemeSummary.voidAmount = this.voidAmount;
            cardSchemeSummary.salesAmount = this.salesAmount;
            cardSchemeSummary.refundCount = this.refundCount;
            cardSchemeSummary.voidCount = this.voidCount;
            return cardSchemeSummary;
        }
    }
}
