package global.citytech.finpos.processor.neps.greenpin.OtpGeneration;
import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;

public class OtpGenerationResponse extends TransactionResponse {

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private String message;
        private String stan;
        private boolean approved;
        private boolean shouldPrintCustomerCopy;
        private ReadCardResponse readCardResponse;

        private Builder() {
        }

        public static OtpGenerationResponse.Builder newInstance() {
            return new OtpGenerationResponse.Builder();
        }

        public OtpGenerationResponse.Builder withDebugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public OtpGenerationResponse.Builder withDebugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public OtpGenerationResponse.Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }

        public OtpGenerationResponse.Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public OtpGenerationResponse.Builder withShouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public OtpGenerationResponse.Builder withApproved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public OtpGenerationResponse.Builder withReadCardResponse(ReadCardResponse readCardResponse) {
            this.readCardResponse = readCardResponse;
            return this;
        }

        public OtpGenerationResponse build() {
            OtpGenerationResponse OtpGenerationResponseModel = new OtpGenerationResponse();
            OtpGenerationResponseModel.debugRequestMessage = this.debugRequestMessage;
            OtpGenerationResponseModel.approved = this.approved;
            OtpGenerationResponseModel.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            OtpGenerationResponseModel.stan = this.stan;
            OtpGenerationResponseModel.debugResponseMessage = this.debugResponseMessage;
            OtpGenerationResponseModel.message = this.message;
            OtpGenerationResponseModel.readCardResponse = this.readCardResponse;
            return OtpGenerationResponseModel;
        }
    }
}
