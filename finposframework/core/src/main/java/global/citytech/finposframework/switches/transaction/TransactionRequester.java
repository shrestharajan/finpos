package global.citytech.finposframework.switches.transaction;

import global.citytech.finposframework.switches.Request;

public interface TransactionRequester<
        T extends TransactionRequestParameter, E extends TransactionResponseParameter>
        extends Request<T, E> {
}