package global.citytech.finpos.merchant.presentation.utils

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import global.citytech.finpos.merchant.R

/**
 * Created by Unique Shakya on 6/18/2021.
 */
class CustomViewPager : ConstraintLayout {

    private lateinit var vp: ViewPager2
    private lateinit var tl: TabLayout

    constructor(context: Context) : super(context) {
        initView(null)
    }

    constructor(context: Context, attrs: AttributeSet?) :
            super(context, attrs) {
        initView(attrs)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initView(attrs)
    }

    private fun initView(attrs: AttributeSet?) {
        val view = inflate(context, R.layout.custom_view_pager, this)
        vp = view.findViewById(R.id.vp)
        tl = view.findViewById(R.id.tab_layout)
        TabLayoutMediator(tl, vp) { _, _ -> }.attach()
    }

    fun showItems(itemIdList: MutableList<Int>) {
        vp.adapter = AdvertisementsCustomViewPagerAdapter(context, itemIdList)
    }
}