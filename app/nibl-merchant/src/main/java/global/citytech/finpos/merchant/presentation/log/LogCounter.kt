package global.citytech.finpos.merchant.presentation.log

import android.os.CountDownTimer

/**
 * Created by BikashShrestha on 2020-04-06.
 */

class LogCounter(
    millisInFuture: Long,
    private val onCountDownTick :  ()->Unit,
    private val onCountDownFinish :  ()->Unit
    )
    : CountDownTimer(millisInFuture, 30000.toLong()) {

    override fun onTick(millisUntilFinished: Long) {
        onCountDownTick()
    }

    override fun onFinish() {
        onCountDownFinish()
    }
}