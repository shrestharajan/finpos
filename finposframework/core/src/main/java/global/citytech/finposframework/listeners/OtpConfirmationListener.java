package global.citytech.finposframework.listeners;

/*
 * @author RajanShrestha
 * @project finpos
 * @created 2023/07/07 - 10:05 AM
 */

import java.io.Serializable;

public interface OtpConfirmationListener extends Serializable {
    void onResult(boolean confirmed, String otp);
}
