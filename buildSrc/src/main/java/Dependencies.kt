object Versions {
    const val gradle_version = "4.1.3"
    const val kotlinVersion = "1.6.0"
    const val compileSdkVersion = 29
    const val buildToolVersion = "29.0.2"
    const val minSdkVersion = 21
    const val targetSdkVersion = 29

    //Libraries
    const val appCompatVersion = "1.1.0"
    const val easydroidCoreVersion = "1.0.3"
    const val ktxVersion = "1.3.0"
    const val constraintLayoutVersion = "1.1.3"
    const val lifecycleExtension = "1.1.1"
    const val extJunitVersion = "1.1.1"
    const val espressoVersion = "3.2.0"
    const val rxJavaVersion = "2.2.10"
    const val gsonVersion = "2.8.5"
    const val rxAndroidVersion = "2.1.1"
    const val roomVersion = "2.2.5"
    const val testRuleVersion = "1.1.0"
    const val testRunnerVersion = "1.1.0"
    const val junit = "4.12"
    const val assertjCore = "3.14.0"
    const val mockitoKotlin = "2.1.0"
    const val mockitoInline = "3.2.4"
    const val lifecycleTesting = "2.1.0"
    const val mockk = "1.9.3"
    const val localBroadcast = "1.0.0"
    const val material = "1.3.0"
    const val leakCanary = "2.6"
}

object AppPath {
    const val gradle = "com.android.tools.build:gradle:${Versions.gradle_version}"
    const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinVersion}"
}

object PluginId {
    const val application = "com.android.application"
    const val kotlinAndroid = "kotlin-android"
    const val kotlinExtensions = "kotlin-android-extensions"
}

object App {
    const val name = "finPOS Cashier"
    const val applicationId = "global.citytech.finpos.merchant"
    const val versionCode = 21010906
    const val versionName = "1.9.6"
}

object Neps {
    const val name = "NEPS"
    const val applicationSuffix = "neps"
}

object Nibl {
    const val applicationSuffix = "global.citytech.finpos.merchant"
    const val name = "NIBL"
}

object Common {
    const val versionCode = 21010003
    const val versionName = "1.0.3"
}

object Application {
    const val applicationId = "global.citytech.finposui"
    const val versionCode = 21000100
    const val versionName = "0.1.0"
}

object CoreWeipass {
    const val applicationId = "global.citytech.finpos.device.weipass"
    const val versionCode = 210000100
    const val versionName = "0.1.0"
}

object Test {
    const val testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    const val junit = "junit:junit:${Versions.junit}"
    val assertjCore = "org.assertj:assertj-core:${Versions.assertjCore}"
    val mockitoKotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:${Versions.mockitoKotlin}"
    val mockitoInline = "org.mockito:mockito-inline:${Versions.mockitoInline}"
    val lifecycleTesting = "androidx.arch.core:core-testing:${Versions.lifecycleTesting}"
}

object AndroidTest {
    const val extJunit = "androidx.test.ext:junit:${Versions.extJunitVersion}"
    const val espresso = "androidx.test.espresso:espresso-core:${Versions.espressoVersion}"
    const val testRunner = "androidx.test:runner:${Versions.testRunnerVersion}"
    const val testRule = "androidx.test:rules:${Versions.testRuleVersion}"
    const val testRoom = "androidx.room:room-testing:${Versions.roomVersion}"
}

object BuildType {
    const val release = "release"
}

object Libraries {
    const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlinVersion}"
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompatVersion}"
    const val kotlinKtx = "androidx.core:core-ktx:${Versions.ktxVersion}"
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayoutVersion}"
    const val easydroidCore =
        "global.citytech.easydroid:easydroid-core:${Versions.easydroidCoreVersion}"
    const val lifeCycleExtension =
        "android.arch.lifecycle:extensions:${Versions.lifecycleExtension}"
    const val rxJava = "io.reactivex.rxjava2:rxjava:${Versions.rxJavaVersion}"
    const val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroidVersion}"
    const val lifecycleKaptDependency =
        "android.arch.lifecycle:compiler:${Versions.lifecycleExtension}"
    const val gson = "com.google.code.gson:gson:${Versions.gsonVersion}"
    const val room =
        "androidx.room:room-runtime:${Versions.roomVersion}"
    const val roomCompiler =
        "androidx.room:room-compiler:${Versions.roomVersion}"
    const val roomRx =
        "androidx.room:room-rxjava2:${Versions.roomVersion}"
    const val localBroadCastManager =
        "androidx.localbroadcastmanager:localbroadcastmanager:${Versions.localBroadcast}"
    const val material = "com.google.android.material:material:${Versions.material}"
    const val qrCode
            = "me.dm7.barcodescanner:zxing:1.9.13"

}
