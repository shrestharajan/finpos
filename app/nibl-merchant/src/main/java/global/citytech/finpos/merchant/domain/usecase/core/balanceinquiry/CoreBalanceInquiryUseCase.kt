package global.citytech.finpos.merchant.domain.usecase.core.balanceinquiry

import global.citytech.finpos.merchant.domain.model.balanceinquiry.BalanceInquiryResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.balanceinquiry.BalanceInquiryRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.balanceinquiry.BalanceInquiryRequestItem
import io.reactivex.Observable

class CoreBalanceInquiryUseCase(private val generateBalanceInquiryRepository: BalanceInquiryRepository) {
    fun generateBalanceInquiryRequest(
        configurationItem: ConfigurationItem,
        balanceInquiryRequestItem: BalanceInquiryRequestItem
    ): Observable<BalanceInquiryResponseEntity> =
        generateBalanceInquiryRepository.generateBalanceInquiryRequest(
            configurationItem,
            balanceInquiryRequestItem
        )
}