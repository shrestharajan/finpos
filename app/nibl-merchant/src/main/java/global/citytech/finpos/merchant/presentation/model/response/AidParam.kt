package global.citytech.finpos.merchant.presentation.model.response

import global.citytech.common.data.LabelValue

data class AidParam(
    var aid: LabelValue? = null,
    var contactlessFloorLimit: LabelValue? = null,
    var contactlessTransactionLimit: LabelValue? = null,
    var cvmLimit: LabelValue? = null,
    var defaultActionCode: LabelValue? = null,
    var defaultDDOL: LabelValue? = null,
    var defaultTDOL: LabelValue? = null,
    var denialActionCode: LabelValue? = null,
    var label: LabelValue? = null,
    var onlineActionCode: LabelValue? = null,
    var terminalAidVersion: LabelValue? = null,
    var terminalFloorLimit: LabelValue? = null,
    var additionalData: Map<String, LabelValue>? = null
) {
    companion object {

        const val TABLE_NAME = "aid_param"

        const val COLUMN_ID = "id"
        const val COLUMN_AID = "aid"
        const val COLUMN_LABEL = "label"
        const val COLUMN_TERMINAL_AID_VERSION = "terminal_aid_version"
        const val COLUMN_DEFAULT_TDOL = "default_tdol"
        const val COLUMN_DEFAULT_DDOL = "default_ddol"
        const val COLUMN_DENIAL_ACTION_CODE = "denial_action_code"
        const val COLUMN_ONLINE_ACTION_CODE = "online_action_code"
        const val COLUMN_DEFAULT_ACTION_CODE = "default_action_code"
        const val COLUMN_TERMINAL_FLOOR_LIMIT = "terminal_floor_limit"
        const val COLUMN_CONTACTLESS_FLOOR_LIMIT = "contactless_floor_limit"
        const val COLUMN_CONTACTLESS_TRANSACTION_LIMIT = "contactless_transaction_limit"
        const val COLUMN_CVM_LIMIT = "cvm_limit"
        const val COLUMN_ADDITIONAL_DATA = "additional_data"
    }
}