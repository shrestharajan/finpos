package global.citytech.finpos.merchant.data.datasource.core.autoreversal

import global.citytech.finpos.merchant.domain.model.autoreversal.AutoReversalResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.processor.nibl.transaction.autoreversal.AutoReversalResponseModel
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 6/22/2021.
 */
interface AutoReversalDataSource {
    fun performAutoReversal(configurationItem: ConfigurationItem):
            Observable<AutoReversalResponseEntity>
}