package global.citytech.finpos.device.qualcomm.utils

import global.citytech.finposframework.utility.StringUtils
import java.util.*

/**
 * Created by Rishav Chudal on 4/8/21.
 */
enum class FeitianCustomEMVTags(
    private val tag: Int,
    private val length: Int
) {
    /**
     * Tag with lenght 0x00 denotes that the length is variable (Not Fixed).
     */
    CUSTOM_TAG_APPLICATION_SELECTED(0x1F66, 0x01),
    CUSTOM_TAG_RID(0x1F42, 0x00),
    CUSTOM_TAG_CAPK_INDEX(0x9F22, 0x01),
    CUSTOM_TAG_CERTIFICATE_SERIAL_NUMBER(0x1F43, 0x03),
    CUSTOM_TAG_PINPAD_RESULT(0x1F63, 0x01);

    fun tagInHex() = Integer.toHexString(tag).uppercase(Locale.getDefault())

    fun lengthInHex(): String {
        val hexLength = Integer.toHexString(length)
        return StringUtils.getDoubleDigitSize(hexLength)
    }

}