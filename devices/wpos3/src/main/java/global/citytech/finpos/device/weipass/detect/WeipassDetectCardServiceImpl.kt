package global.citytech.finpos.device.weipass.detect

import android.content.Context
import com.wiseasy.emvprocess.SDKInstance
import com.wiseasy.emvprocess.utils.ByteUtil
import global.citytech.finpos.device.weipass.card.handler.WeipassStateHandler
import global.citytech.finpos.device.weipass.utils.WeipassDeviceUtils.Companion.getCardMode
import global.citytech.finpos.device.weipass.utils.WeipassDeviceUtils.Companion.getCardReadTimeoutOrDefault
import global.citytech.finpos.device.weipass.utils.WeipassDeviceUtils.Companion.getCardType
import global.citytech.finpos.device.weipass.utils.WeipassConstant.PICC_CARD_DETECTED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.ICC_CARD_DETECTED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.DETECT_CARD_CANCEL
import global.citytech.finpos.device.weipass.utils.WeipassConstant.DETECT_CARD_TIMEOUT
import global.citytech.finpos.device.weipass.utils.WeipassConstant.DETECT_ERROR_CASE_1
import global.citytech.finpos.device.weipass.utils.WeipassConstant.DETECT_ERROR_CASE_2
import global.citytech.finpos.device.weipass.utils.WeipassConstant.ICC_CARD_DETECT_ERROR_OUTDATA_VALUE
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MAGNETIC_CARD_DETECT_ERROR_OUTDATA_VALUE
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MAG_CARD_DETECTED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_ICC_READER_CARD_DETECT_ERROR
import global.citytech.finpos.device.weipass.utils.WeipassConstant.NO_CARD_DETECTED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SLOT_OPEN_EMPTY
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SWIPE_CARD_AGAIN
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.notifier.Notifier
import wangpos.sdk4.libbasebinder.BankCard
import java.lang.ref.WeakReference

/**
 * Created by Rishav Chudal on 5/12/20.
 */
class WeipassDetectCardServiceImpl constructor(val context: Context, val notifier: Notifier) : DetectCardService {
    private lateinit var response: DetectCardResponse
    private var contextWeakReference = WeakReference(context)

    override fun detectCard(request: DetectCardRequest): DetectCardResponse {
        response = DetectCardResponse(
            Result.FAILURE,
            NO_CARD_DETECTED,
            CardType.UNKNOWN,
            null
        )
        if (request.slotsToOpen.isNotEmpty()) {
            proceedCardDetectInSdk(request)
        } else {
            response = DetectCardResponse(
                Result.FAILURE,
                SLOT_OPEN_EMPTY,
                CardType.UNKNOWN,
                null
            )
        }
        return response
    }

    private fun proceedCardDetectInSdk(request: DetectCardRequest) {
        WeipassLogger.log("Proceeding for Card Detect In SDK...")
        val outData = ByteArray(256)
        val outDataLen = IntArray(1)
        val cardMode = getCardMode(request.slotsToOpen)
        try {
            val result = SDKInstance.mBankCard.readCard(
                getCardType(cardMode),
                cardMode,
                getCardReadTimeoutOrDefault(request.timeoutInSecs),
                outData,
                outDataLen,
                request.packageName
            )
            if (result == 0) {
                proceedOnDetectionResult(outData)
            } else {
                manageForCardDetectError(outData, NO_CARD_DETECTED)
            }
        } catch (exception: Exception) {
            WeipassLogger.log("Exception in readCard, ".plus(exception.message))
        }
    }

    private fun proceedOnDetectionResult(outData: ByteArray): DetectCardResponse {
        when (outData[0]) {
            BankCard.CARD_READ_MAGENC -> manageForMagneticCardDetected(outData)
            BankCard.CARD_READ_FAIL -> manageForCardDetectError(outData, DETECT_ERROR_CASE_1)
            BankCard.CARD_READ_MAGENCFAIL -> manageForCardDetectError(outData, DETECT_ERROR_CASE_2)
            BankCard.CARD_READ_TIMEOUT -> manageForCardDetectTimeout()
            BankCard.CARD_READ_CANCELED -> manageForCardDetectCancel()
            BankCard.CARD_READ_ICDETACT -> manageForContactCardDetected()
            BankCard.CARD_READ_PICCDETACT -> manageForContactlessCardDetected()
        }
        return response
    }

    private fun manageForMagneticCardDetected(outData: ByteArray) {
        this.notifyCardDetectedToUi()
        WeipassLogger.log("Magnetic Card Detected...")
        WeipassStateHandler.CURRENT_CARD_TYPE = CardType.MAG
        response = DetectCardResponse(
            Result.SUCCESS, MAG_CARD_DETECTED,
            CardType.MAG,
            outData
        )
    }

    private fun manageForCardDetectError(outData: ByteArray, message: String) {
        this.notifyCardDetectErrorToUi()
        WeipassLogger.log("Card Detected Error")
        response = DetectCardResponse(Result.FAILURE, message, CardType.UNKNOWN, null)
        val outDataInHexString: String = ByteUtil.bytes2HexString(outData)
        when (outDataInHexString.substring(2, 4)) {
            MAGNETIC_CARD_DETECT_ERROR_OUTDATA_VALUE -> manageForSwipeAgainCase()
            ICC_CARD_DETECT_ERROR_OUTDATA_VALUE -> manageForIccCardDetectError()
        }
    }

    private fun manageForCardDetectTimeout() {
        this.notifyCardDetectErrorToUi()
        WeipassLogger.log("Card Detect Timeout...")
        WeipassStateHandler.CURRENT_CARD_TYPE = CardType.UNKNOWN
        response = DetectCardResponse(
            Result.TIMEOUT, DETECT_CARD_TIMEOUT,
            CardType.UNKNOWN,
            null
        )
    }

    private fun manageForCardDetectCancel() {
        this.notifyCardDetectErrorToUi()
        WeipassLogger.log("Card Detect Cancel...")
        WeipassStateHandler.CURRENT_CARD_TYPE = CardType.UNKNOWN
        response = DetectCardResponse(
            Result.CANCEL, DETECT_CARD_CANCEL,
            CardType.UNKNOWN,
            null
        )
    }

    private fun manageForContactCardDetected() {
        this.notifyCardDetectedToUi()
        WeipassLogger.log("Contact Card Detected...")
        WeipassStateHandler.CURRENT_CARD_TYPE = CardType.ICC
        response = DetectCardResponse(
            Result.SUCCESS, ICC_CARD_DETECTED,
            CardType.ICC,
            null
        )
    }

    private fun notifyCardDetectedToUi() {
        notifier.notify(
            Notifier.EventType.READING_CARD,
            "Reading Card..."
        )
    }

    private fun notifyCardDetectErrorToUi() {
        notifier.notify(Notifier.EventType.DETECT_CARD_ERROR, "Please wait..")
    }

    private fun manageForContactlessCardDetected() {
        this.notifyCardDetectedToUi()
        WeipassLogger.log("Contactless Card Detected...")
        WeipassStateHandler.CURRENT_CARD_TYPE = CardType.PICC
        response = DetectCardResponse(
            Result.SUCCESS, PICC_CARD_DETECTED,
            CardType.PICC,
            null
        )
    }

    private fun manageForSwipeAgainCase() {
        WeipassLogger.log("Magnetic Card Detect Error. Swipe Again...")
        WeipassStateHandler.CURRENT_CARD_TYPE = CardType.MAG
        response = DetectCardResponse(
            Result.SWIPE_AGAIN,
            SWIPE_CARD_AGAIN,
            CardType.MAG,
            null
        )
    }

    private fun manageForIccCardDetectError() {
        WeipassLogger.log("Contact Reader Detect Error...")
        WeipassStateHandler.CURRENT_CARD_TYPE = CardType.ICC
        response = DetectCardResponse(
            Result.ICC_CARD_DETECT_ERROR,
            MSG_ICC_READER_CARD_DETECT_ERROR,
            CardType.ICC,
            null
        )
    }

}