package global.citytech.finpos.merchant

import android.app.Activity
import android.app.Application
import android.content.*
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.IBinder
import androidx.lifecycle.MutableLiveData
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import global.citytech.common.Constants
import global.citytech.common.data.PosCallback
import global.citytech.common.data.PosMessage
import global.citytech.common.data.Response
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.data.repository.app.CommandQueueRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.Status
import global.citytech.finpos.merchant.domain.usecase.app.CommandQueueUseCase
import global.citytech.finpos.merchant.framework.broadcasts.AppResetReceiver
import global.citytech.finpos.merchant.framework.broadcasts.NetworkStatusBroadcastReceiver
import global.citytech.finpos.merchant.framework.broadcasts.SettlementReceiver
import global.citytech.finpos.merchant.framework.datasource.app.CommandQueueDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.mqtt.MqttHelper
import global.citytech.finpos.merchant.presentation.alarms.SettlementAlarmScheduler
import global.citytech.finpos.merchant.presentation.dashboard.temp.DashActivity
import global.citytech.finpos.merchant.presentation.idle.IdleActivity
import global.citytech.finpos.merchant.presentation.log.LogPushAlarmScheduler
import global.citytech.finpos.merchant.presentation.model.command.execution.CommandExecutionAcknowledgmentRequest
import global.citytech.finpos.merchant.presentation.model.command.pull.PullCommandResponse
import global.citytech.finpos.merchant.presentation.model.command.pull.mapToCommand
import global.citytech.finpos.merchant.presentation.model.command.receive.CommandReceiveAcknowledgmentRequest
import global.citytech.finpos.merchant.presentation.model.setting.PushNotificationConfiguration
import global.citytech.finpos.merchant.presentation.payment.PaymentSdkSingleton
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finpos.merchant.service.ActivityLogPushService
import global.citytech.finpos.merchant.service.FinposCommandService
import global.citytech.finpos.merchant.service.NetworkConnectionReceiver
import global.citytech.finpos.merchant.service.TransactionPushService
import global.citytech.finpos.merchant.utils.ApiConstant
import global.citytech.finpos.merchant.utils.AppUtility.getPushNotificationConfiguration
import global.citytech.finpos.merchant.utils.AppUtility.handleMqttConnection
import global.citytech.finpos.merchant.utils.AppUtility.isMqttUrlAvailable
import global.citytech.finpos.merchant.utils.IdleTimeCountDownTimer
import global.citytech.finposframework.log.AppState
import global.citytech.finposframework.log.Logger
import global.citytech.payment.sdk.api.PaymentResponse
import global.citytech.payment.sdk.api.PaymentResult
import global.citytech.readinglog.BatteryStatusBroadcastReceiver
import global.citytech.tms.sdk.IPlatformManager
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.exceptions.UndeliverableException
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.eclipse.paho.client.mqttv3.MqttMessage
import java.lang.ref.WeakReference


class NiblMerchant : Application(), Application.ActivityLifecycleCallbacks,
    IdleTimeCountDownTimer.IdleTimeCountDownTimerListener {

    val mqttClient by lazy {
        MqttHelper(this)
    }

    companion object {
        lateinit var INSTANCE: NiblMerchant
        private var activityContext: WeakReference<Context>? = null
        fun getActivityContext(): Context? {
            return activityContext!!.get()
        }

        val showScreenSaver by lazy { MutableLiveData<Boolean>() }
        val startScreenSaverTimer by lazy { MutableLiveData<Boolean>() }
    }

    private val logger = Logger(NiblMerchant::class.java)
    private var settlementAlarmScheduler: SettlementAlarmScheduler? = null
    lateinit var iPlatformManager: IPlatformManager
    lateinit var serviceConnection: ServiceConnection
    private lateinit var currentForegroundActivity: WeakReference<Activity>
    val networkConnectionAvailable = MutableLiveData<Boolean>(true)
    var disableNetworkPing: Boolean = false
    val isTerminalConfigured = MutableLiveData<Boolean>(false)
    private var idleTimeCountDownTimer: IdleTimeCountDownTimer? = null
    private var isIdleCountdownActive = false

    lateinit var commandQueueUseCase: CommandQueueUseCase
    var settlementReceiver: SettlementReceiver = SettlementReceiver()
    var appResetReceiver: AppResetReceiver = AppResetReceiver()
    var networkStatusReceiver: NetworkStatusBroadcastReceiver = NetworkStatusBroadcastReceiver()
    var batteryStatusReceiver: BatteryStatusBroadcastReceiver = BatteryStatusBroadcastReceiver()

    var dashActivityVisible: Boolean = false

    override fun onCreate() {
        super.onCreate()

        LogPushAlarmScheduler(this.applicationContext)
        val logPushAlarmScheduler = LogPushAlarmScheduler(this)
        logPushAlarmScheduler.triggerAlarm()

        RxJavaPlugins.setErrorHandler {
            if (it is UndeliverableException)
                logger.log("::: UNDELIVERABLE EXCEPTION >> ${it.cause}")
            else
                return@setErrorHandler
        }
        INSTANCE = this
        if (BuildConfig.DEBUG)
            AppState.getInstance().buildType = AppState.BuildType.DEBUG
        bindService()
        PreferenceManager.init(this)
        NotificationHandler.init(this)
        registerActivityLifecycleCallbacks(this)
        NetworkConnectionReceiver.enable()
        initLastSettlementSuccessTime()
        PreferenceManager.setForceSettlementPreference(false)
        FinposCommandService.start(INSTANCE)
        ActivityLogPushService.start(INSTANCE)
        TransactionPushService.start(INSTANCE)

        commandQueueUseCase = CommandQueueUseCase(
            CommandQueueRepositoryImpl(
                CommandQueueDataSourceImpl(INSTANCE)
            )
        )
        registerAppUpdateBroadcastReceiver()
        registerAppResetBroadcastReceiver()
        registerNetworkStatusBroadcastReceiver()
        registerBatteryStatusBroadcastReceiver()
    }

    private fun registerAppUpdateBroadcastReceiver() {
        val filter = IntentFilter(INSTANCE.packageName.plus(".UPDATE.APPLICATION"))
        registerReceiver(settlementReceiver, filter)
    }

    private fun registerAppResetBroadcastReceiver() {
        val appResetFilter = IntentFilter("android.intent.action.RESET.APPLICATION")
        registerReceiver(appResetReceiver, appResetFilter)
    }

    private fun registerNetworkStatusBroadcastReceiver() {
        val networkFilter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(networkStatusReceiver, networkFilter)
    }

    private fun registerBatteryStatusBroadcastReceiver() {
        val batteryStatusIntentFilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        registerReceiver(batteryStatusReceiver, batteryStatusIntentFilter)
    }


    fun setUpIdleTime(idleTime: Long, countDownInterval: Long) {
        idleTimeCountDownTimer?.cancel()
        idleTimeCountDownTimer = IdleTimeCountDownTimer(idleTime, countDownInterval)
        idleTimeCountDownTimer?.addListener(this)
    }

    fun startIdleTimeCountDown() {
        idleTimeCountDownTimer?.let {
            it.cancel()
            it.start()
            this.isIdleCountdownActive = true
        }
    }

    fun resetIdleTimeCountDown() {
        if (isIdleCountdownActive) startIdleTimeCountDown()
    }

    fun stopIdleTimeCountDown() {
        this.isIdleCountdownActive = false
        idleTimeCountDownTimer?.cancel()
    }

    override fun onIdleTimeCountDownReached() {
        stopIdleTimeCountDown()
        if (PaymentSdkSingleton.getInstance().isFromPaymentSdk) {
            returnPaymentResponse()
        }
    }

    private fun returnPaymentResponse() {
        var currentPaymentResponse = PaymentSdkSingleton.getInstance()
            .paymentResponse

        if (currentPaymentResponse == null) {
            currentPaymentResponse = PaymentResponse(PaymentResult.TIME_OUT)
        }

        val resultCode: Int = currentPaymentResponse.resultCode
        if (resultCode != PaymentResult.SUCCESS.resultCode
            && resultCode != PaymentResult.FAILED.resultCode
        ) {
            val paymentResponse = PaymentResponse(PaymentResult.TIME_OUT)
            PaymentSdkSingleton.getInstance().updatePaymentResponse(paymentResponse)
        }
        PaymentSdkSingleton.getInstance()
            .returnActivityResultWithExtraData(currentForegroundActivity.get())
    }


    private fun initLastSettlementSuccessTime() {
        if (PreferenceManager.getLastSuccessSettlementTimeInMillis(0) == 0.toLong())
            PreferenceManager.updateLastSuccessSettlementTimeInMillis(System.currentTimeMillis())
    }

    private val serviceConnections = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            INSTANCE.iPlatformManager = IPlatformManager.Stub.asInterface(service)
            INSTANCE.serviceConnection = this
            logger.log("cta pluginInterfaceConnected : " + true)

            val pushNotificationConfiguration: PushNotificationConfiguration =
                getPushNotificationConfiguration()

            if (isMqttUrlAvailable(pushNotificationConfiguration)) {
                handleMqttConnection(pushNotificationConfiguration)
            } else {
                logger.log("MQTT Server Uri not available")
            }
        }

        override fun onServiceDisconnected(name: ComponentName) {
            logger.log("cta pluginInterfaceConnected : " + false)
            bindService()
        }
    }

    fun getPlatformServiceConnection(): ServiceConnection {
        return serviceConnections
    }

    fun bindService() {
        val compositeDisposable = CompositeDisposable()
        compositeDisposable.add(
            Single.create(SingleOnSubscribe<Response> { _ ->
                val bindIntent = Intent()
                val packageName = "global.citytech.tms"
                val className = "global.citytech.tms.service.PlatformManagerService"
                bindIntent.setClassName(packageName, className)
                bindService(bindIntent, serviceConnections, BIND_AUTO_CREATE)
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                }, { error ->
                    onError(error)
                })
        )
    }

    private fun onError(throwable: Throwable) {
        throwable.printStackTrace()
    }


    override fun onActivityPaused(p0: Activity) {
        logger.log("Activity ::: ".plus(p0.javaClass.simpleName).plus(" is Paused..."))
        if (p0 is DashActivity) {
            dashActivityVisible = false
        }
    }

    override fun onActivityStarted(p0: Activity) {
        logger.log("Activity ::: ".plus(p0.javaClass.simpleName).plus("is Started..."))
        setCurrentForegroundActivity(p0)
        activityContext = WeakReference(p0)
        if (p0 is DashActivity) {
            dashActivityVisible = true
        }
    }

    override fun onActivityDestroyed(p0: Activity) {
        logger.log("Activity ::: ".plus(p0.javaClass.simpleName).plus(" is Destroyed..."))
        if (p0 is DashActivity) {
            dashActivityVisible = false
        }
    }

    override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {
        logger.log("Activity ::: ".plus(p0.javaClass.simpleName).plus("SaveInstanceState..."))
    }

    override fun onActivityStopped(p0: Activity) {
        logger.log("Activity ::: ".plus(p0.javaClass.simpleName).plus(" is Stopped..."))
        if (p0 is DashActivity) {
            dashActivityVisible = false
        }
    }

    override fun onActivityCreated(p0: Activity, p1: Bundle?) {
        logger.log("Activity ::: ".plus(p0.javaClass.simpleName).plus(" is Created..."))
        setCurrentForegroundActivity(p0)
        activityContext = WeakReference(p0)
        if (p0 is DashActivity) {
            dashActivityVisible = true
        }
    }

    override fun onActivityResumed(p0: Activity) {
        logger.log("Activity ::: ".plus(p0.javaClass.simpleName).plus(" is Resumed..."))
        setCurrentForegroundActivity(p0)
        activityContext = WeakReference(p0)
        if (p0 is DashActivity) {
            dashActivityVisible = true
        }
    }

    private fun setCurrentForegroundActivity(activity: Activity) {
        this.currentForegroundActivity = WeakReference(activity)
        if (activity is DashActivity) {
            dashActivityVisible = true
        }
    }

    fun idleActivityVisible(): Boolean {
        val visible = ((currentForegroundActivity.get() != null) &&
                (currentForegroundActivity.get() is IdleActivity))
        logger.log("Is Idle Activity Visible? ::: ".plus(visible))
        return visible
    }

    fun isDashActivityVisible(): Boolean {
        val visible = dashActivityVisible
        logger.log("Is DashActivity Visible? ::: ".plus(visible))
        return visible
    }

    fun executeMqttAfterCommandReceived(mqttMessage: MqttMessage) {
        val response = Jsons.fromJsonToObj(mqttMessage.toString(), Response::class.java)

        if (response != null) {
            val combinedCommandList: List<PullCommandResponse> = Jsons.fromJsonToList(
                response.data.toString(),
                Array<PullCommandResponse>::class.java
            )
            var qrCommandList: MutableList<PullCommandResponse> = mutableListOf()
            var commandList: MutableList<PullCommandResponse> = mutableListOf()

            combinedCommandList.forEach {
                commandList.add(it)
            }


            if (commandList.size > 0) {
                commandList?.toList()?.let {
                    addCommandToQueue(commandList)
                }
            }


        } else {
            logger.log("FINPOS COMMAND CHECK :: Error Code :: ${response?.code}. ${response?.message}")
        }

    }

    private fun addCommandToQueue(commandList: MutableList<PullCommandResponse>) {
        this.logger.debug(":: FINPOS COMMAND CHECK :: STARTED  ".plus(commandList.size))
        Observable.create(ObservableOnSubscribe<Void> { onSubscribe(it, commandList) })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({}, {}, {
                if (INSTANCE.isDashActivityVisible()) {
                    logger.log("::: DASH ACTIVITY VISIBLE ::: START COMMAND >>>")
                    val intent = Intent(Constants.INTENT_ACTION_POS_CALLBACK)
                    intent.putExtra(
                        Constants.KEY_BROADCAST_MESSAGE,
                        PosCallback(PosMessage.POS_MESSAGE_START_COMMAND)
                    )
                    LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
                }
            })
    }

    private fun onSubscribe(
        it: ObservableEmitter<Void>,
        commandList: MutableList<PullCommandResponse>
    ) {
        logger.debug("FINPOS COMMAND CHECK :: ")
        val serialNumber = INSTANCE.iPlatformManager.serialNumber
        serialNumber?.let {
            sendExecutionAcknowledgmentOfPreviousCommands(it)
            prepareResponse(
                commandList,
                INSTANCE.iPlatformManager.serialNumber
            )
        }
        it.onComplete()
    }

    private fun sendExecutionAcknowledgmentOfPreviousCommands(serialNumber: String) {
        val commandList = commandQueueUseCase.getAllCommands()
        if (commandList.isNotEmpty()) {
            commandList.forEach {
                if (it.status != Status.OPEN) {
                    val commandExecutionAcknowledgmentRequest =
                        CommandExecutionAcknowledgmentRequest(
                            serialNumber = serialNumber,
                            status = it.status.name,
                            terminalCommandId = it.id,
                            remarks = it.remarks
                        )
                    val jsonRequest = Jsons.toJsonObj(commandExecutionAcknowledgmentRequest)
                    logger.log("FINPOS COMMAND EXEC ACK :: REQUEST :: $jsonRequest")
                    val jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.post(
                        ApiConstant.COMMAND_EXECUTION_ACK_URL,
                        jsonRequest,
                        false
                    )
                    val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
                    this.logger.log("FINPOS COMMAND EXEC ACK :: Response :: ${response.code}. ${response.message}")
                    if (response.code == "0")
                        commandQueueUseCase.removeCommand(it)
                }
            }
        }
    }


    private fun prepareResponse(response: List<PullCommandResponse>, serialNumber: String) {
        val listOfReceivedId = mutableListOf<String>()
        response.forEach {
            logger.log("COMMAND QUEUE BEFORE >>> ${commandQueueUseCase.getAllCommands()}")
            commandQueueUseCase.addCommand(it.mapToCommand())
            listOfReceivedId.add(it.id)
        }
        if (listOfReceivedId.isNotEmpty())
            sendReceiveAcknowledgmentOfCommand(listOfReceivedId, serialNumber)
    }

    private fun sendReceiveAcknowledgmentOfCommand(it: MutableList<String>, serialNumber: String) {
        val commandReceiveAcknowledgmentRequest = CommandReceiveAcknowledgmentRequest(
            serialNumber = serialNumber,
            commands = it
        )
        val jsonRequest = Jsons.toJsonObj(commandReceiveAcknowledgmentRequest)
        logger.log("FINPOS COMMAND REC ACK :: Request :: $jsonRequest")
        val jsonResponse =
            INSTANCE.iPlatformManager.post(
                ApiConstant.COMMAND_RECEIVE_ACK_URL,
                jsonRequest,
                false
            )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            this.logger.log("FINPOS COMMAND REC ACK :: Response :: ${response.code}. ${response.message}")
        } else {
            this.logger.log("FINPOS COMMAND REC ACK :: Error Code :: ${response.code}. ${response.message}")
        }
    }

    fun cancelRescheduledSettlement() {
        if (settlementAlarmScheduler == null)
            settlementAlarmScheduler = getActivityContext()?.applicationContext?.let {
                SettlementAlarmScheduler(
                    it
                )
            }

        settlementAlarmScheduler?.cancelAlarm()
        settlementAlarmScheduler?.cancelAlarmForOnGoingSettlement()
    }
}