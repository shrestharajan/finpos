package global.citytech.finposframework.hardware.io.printer

import global.citytech.finposframework.hardware.common.DeviceService

/**
 * Created by Rishav Chudal on 6/19/20.
 */
interface PrinterService: DeviceService {
    fun print(printerRequest: PrinterRequest?): PrinterResponse

    fun feedPaper(numberOfEmptyLines: Int)
}