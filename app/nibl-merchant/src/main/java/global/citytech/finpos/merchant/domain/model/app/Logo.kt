package global.citytech.finpos.merchant.domain.model.app

import android.content.ContentValues
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = Logo.TABLE_NAME)
data class Logo(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_ID)
    var id: String,
    @ColumnInfo(name = COLUMN_APP_WALLPAPER)
    var appWallpaper: String? = null,
    @ColumnInfo(name = COLUMN_DISPLAY_LOGO)
    var displayLogo: String? = null,
    @ColumnInfo(name = COLUMN_PRINT_LOGO)
    var printLogo: String? = null
) {

    companion object {
        const val TABLE_NAME = "logos"
        const val COLUMN_ID = "id"
        const val COLUMN_APP_WALLPAPER = "app_wallpaper"
        const val COLUMN_DISPLAY_LOGO = "display_logo"
        const val COLUMN_PRINT_LOGO = "print_logo"

        fun fromContentValues(values: ContentValues): Logo {
            values.let {
                val logo =
                    Logo(
                        id = values.getAsString(COLUMN_ID)
                    )
                if (it.containsKey(COLUMN_ID)) {
                    logo.id = it.getAsString(COLUMN_ID)
                }
                if (it.containsKey(COLUMN_APP_WALLPAPER))
                    logo.appWallpaper = it.getAsString(COLUMN_APP_WALLPAPER)
                if (it.containsKey(COLUMN_DISPLAY_LOGO))
                    logo.displayLogo = it.getAsString(COLUMN_DISPLAY_LOGO)
                if (it.containsKey(COLUMN_PRINT_LOGO))
                    logo.printLogo = it.getAsString(COLUMN_PRINT_LOGO)
                return logo
            }
        }

    }
}
