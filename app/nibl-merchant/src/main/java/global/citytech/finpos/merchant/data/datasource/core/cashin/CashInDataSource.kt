package global.citytech.finpos.merchant.data.datasource.core.cashin

import global.citytech.finpos.merchant.domain.model.cashin.CashInResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.cashin.CashInRequestItem
import io.reactivex.Observable

interface CashInDataSource {
    fun generateCashInRequest(configurationItem: ConfigurationItem, cashInRequestItem: CashInRequestItem)
    :Observable<CashInResponseEntity>
}