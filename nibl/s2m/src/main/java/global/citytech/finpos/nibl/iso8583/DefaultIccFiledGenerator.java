package global.citytech.finpos.nibl.iso8583;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import global.citytech.finposframework.log.Logger;;

import global.citytech.finposframework.utility.EncodingUtils;
import global.citytech.finposframework.utility.HexDump;

public class DefaultIccFiledGenerator implements IccFiledGenerator {
    private Logger logger = Logger.getLogger(DefaultIccFiledGenerator.class.getName());


    @Override
    public byte[] generate() {
        try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
            stream.write(EncodingUtils.hex2Bytes("0136"));//length
            //9726084266DA1FF65DFD3F
            stream.write(EncodingUtils.hex2Bytes("9726084266DA1FF65DFD3F"));
            //9F270180
            stream.write(EncodingUtils.hex2Bytes("9F270180"));
            //9F100706011203A00000
            stream.write(EncodingUtils.hex2Bytes("9F100706011203A00000"));
            //9F3704FA0077CA
            stream.write(EncodingUtils.hex2Bytes("9F3704FA0077CA"));
            //9F36020002
            stream.write(EncodingUtils.hex2Bytes("9F36020002"));
            //95050280008800
            stream.write(EncodingUtils.hex2Bytes("95050280008800"));
            //9A03200811
            stream.write(EncodingUtils.hex2Bytes("9A03200811"));
            //9C0100
            stream.write(EncodingUtils.hex2Bytes("9C0100"));
            //9F0206000000120000
            stream.write(EncodingUtils.hex2Bytes("9F0206000000120000"));
            //5F2A020524
            stream.write(EncodingUtils.hex2Bytes("5F2A020524"));
            //82025800
            stream.write(EncodingUtils.hex2Bytes("82025800"));
            //9F1A020524
            stream.write(EncodingUtils.hex2Bytes("9F1A020524"));
            //9F0306000000000000
            stream.write(EncodingUtils.hex2Bytes("9F0306000000000000"));
            //9F3303E0F0C8
            stream.write(EncodingUtils.hex2Bytes("9F3303E0F0C8"));
            //9F34031E0300
            stream.write(EncodingUtils.hex2Bytes("9F34031E0300"));
            //9F350122
            stream.write(EncodingUtils.hex2Bytes("F350122"));
            //9F1E084530313935343839
            stream.write(EncodingUtils.hex2Bytes("9F1E084530313935343839"));
            //8407A0000000031010
            stream.write(EncodingUtils.hex2Bytes("8407A0000000031010"));
            //9F09020140
            stream.write(EncodingUtils.hex2Bytes("F09020140"));
            //9F410400000004
            stream.write(EncodingUtils.hex2Bytes("9F410400000004"));
            //5F340101
            stream.write(EncodingUtils.hex2Bytes("5F340101"));

            logger.debug(HexDump.dumpHexString(stream.toByteArray()));
            return stream.toByteArray();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    public static void main(String[] arg) {
        DefaultIccFiledGenerator generator = new DefaultIccFiledGenerator();
        generator.generate();
    }
}
