package global.citytech.finpos.merchant.framework.datasource.core

import com.google.gson.JsonParser
import global.citytech.common.Constants
import global.citytech.common.extensions.toJsonString
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.app.db.AppDatabase
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.HostItem
import global.citytech.finpos.merchant.presentation.model.response.TlsCertificate
import global.citytech.finpos.merchant.presentation.utils.StringUtils.Companion.getValueFromLabelValue
import global.citytech.finposframework.comm.ConnectionEvent
import global.citytech.finposframework.comm.HostInfo
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.repositories.TerminalRepository
import global.citytech.finposframework.supports.Context
import global.citytech.finposframework.usecases.activity.ActivityLog
import global.citytech.finposframework.usecases.terminal.TerminalInfo
import global.citytech.finposframework.utility.StringUtils

class TerminalRepositoryImpl(configurationItem: ConfigurationItem?) :
    TerminalRepository {

    private val logger = Logger(TerminalRepositoryImpl::class.java.name)
    val hosts = configurationItem!!.hosts
    val merchant = configurationItem!!.merchants!![0]


    override fun findSecondaryHost(): HostInfo {
        val host = hosts!![1]
        logger.log("SECONDARY HOST TPDU:::: ${host.nii}")
        return HostInfo.Builder.createDefaultBuilder(
            host.ip,
            host.port!!.toInt()
        ).timeOut(host.connectionTimeout!!.toInt()).retryLimit(host.retryLimit!!.toInt())
            .tpduString(this.getTPDUString(host))
            .secureConnection(isSecureConnection(host.tlsCertificate))
            .tlsCertificate(retrieveTlsCertificate(host.tlsCertificate))
            .addEventListener { event: ConnectionEvent?, context: Context ->
                logger.log(
                    context["MSG"].toString()
                )
            }.build()
    }

    override fun getReconciliationBatchNumber(): String {
        return LocalDatabaseSourceImpl().getExistingSettlementBatchNumber().blockingLast()
    }

    override fun getTerminalTransactionCurrencyName(): String {
        val emvParams = AppDatabase.getInstance(NiblMerchant.INSTANCE)
            .getEmvParamDao()
            .getEmvParams()
        if (emvParams != null && emvParams.isNotEmpty()) {
            val additionalData = emvParams[0].additionalData
            if (!additionalData.isNullOrEmptyOrBlank()) {
                return getValueFromAdditionalData(
                    additionalData,
                    Constants.KEY_TRANSACTION_CURRENCY_NAME
                )
            }
        }
        return ""
    }

    override fun isVatRefundSupported(): Boolean = PreferenceManager.getVatRefundSupportedPref(false)

    private fun getValueFromAdditionalData(
        additionalData: String?,
        parameterToRetrieve: String?
    ): String {
        if (parameterToRetrieve.isNullOrEmptyOrBlank() || additionalData.isNullOrEmptyOrBlank())
            return ""
        val jsonParser = JsonParser()
        try {
            val additionalDataObject = jsonParser.parse(additionalData).asJsonObject
            if (!additionalDataObject.has(parameterToRetrieve))
                return ""
            val labelValueDataObject = additionalDataObject.getAsJsonObject(parameterToRetrieve)
            return getValueFromLabelValue(labelValueDataObject.toJsonString())
        } catch (e: Exception) {
            return ""
        }
    }

    override fun incrementSystemTraceAuditNumber() {
        val count = AppDatabase.getInstance(NiblMerchant.INSTANCE)
            .getTransactionConfigDao()
            .incrementStan()
        logger.log("::: NIBL ::: INCREASE STAN === $count")
    }

    override fun incrementInvoiceNumber() {
        val count = AppDatabase.getInstance(NiblMerchant.INSTANCE)
            .getTransactionConfigDao()
            .incrementInvoiceNumber()
        logger.log("::: NIBL ::: INCREASE INVOICE NUMBER === $count")
    }

    override fun getRetrievalReferenceNumber(): String {
        val rrn = AppDatabase.getInstance(NiblMerchant.INSTANCE)
            .getTransactionConfigDao()
            .getRrn()
        logger.log("::: NIBL ::: RRN === $rrn")
        return StringUtils.ofRequiredLength(
            rrn.toString(), 12
        )
    }

    override fun getApplicationVersion(): String {
        return BuildConfig.VERSION_NAME
    }

    override fun getSystemTraceAuditNumber(): String {
        val stan = AppDatabase.getInstance(NiblMerchant.INSTANCE)
            .getTransactionConfigDao()
            .getStan()
        logger.log("::: NIBL ::: STAN === $stan")
        return StringUtils.ofRequiredLength(
            stan.toString(), 6
        )
    }

    override fun incrementReconciliationBatchNumber() {
        val count = AppDatabase.getInstance(NiblMerchant.INSTANCE)
            .getTransactionConfigDao()
            .incrementReconciliationBatchNumber()
        logger.log("::: NIBL ::: INCREASE BATCH NUMBER === $count")
    }

    override fun incrementRetrievalReferenceNumber() {
        val count = AppDatabase.getInstance(NiblMerchant.INSTANCE)
            .getTransactionConfigDao()
            .incrementRrn()
        logger.log("::: NIBL ::: INCREASE RRN === $count")
    }

    override fun findPrimaryHost(): HostInfo {
        val host = hosts!![0]
        logger.log("PRIMARY HOST TPDU:::: ${host.nii}")
        return HostInfo.Builder.createDefaultBuilder(
            host.ip,
            host.port!!.toInt()
        ).timeOut(host.connectionTimeout!!.toInt()).retryLimit(host.retryLimit!!.toInt())
            .tpduString(this.getTPDUString(host))
            .secureConnection(isSecureConnection(host.tlsCertificate))
            .tlsCertificate(retrieveTlsCertificate(host.tlsCertificate))
            .addEventListener { event: ConnectionEvent?, context: Context ->
                logger.log(
                    context["MSG"].toString()
                )
            }.build()

    }

    private fun retrieveTlsCertificate(tlsCertificate: TlsCertificate?):
            global.citytech.finposframework.comm.TlsCertificate? {
        return if (!isSecureConnection(tlsCertificate))
            null
        else
            global.citytech.finposframework.comm.TlsCertificate.Builder.createDefaultBuilder()
                .withCertificate(tlsCertificate!!.certificate)
                .withCertificateFactoryType(tlsCertificate.certificateFactoryType)
                .withCertificateKey(tlsCertificate.certificateKey)
                .withExpiryDate(tlsCertificate.expiryDate)
                .withTlsVersion(tlsCertificate.tlsVersion)
                .build()
    }

    private fun isSecureConnection(tlsCertificate: TlsCertificate?): Boolean {
        return if (tlsCertificate == null)
            false
        else !tlsCertificate.certificate.isNullOrEmptyOrBlank()
    }

    override fun findTerminalInfo(): TerminalInfo {
        return TerminalInfo(
            merchant.terminalId,
            merchant.switchId,
            DeviceConfiguration.get().merchantLogo,
            DeviceConfiguration.get().merchantName,
            DeviceConfiguration.get().retailerAddress,
            DeviceConfiguration.get().debugMode
        )
    }

    override fun getInvoiceNumber(): String {
        val invoiceNumber = AppDatabase.getInstance(NiblMerchant.INSTANCE)
            .getTransactionConfigDao()
            .getInvoiceNumber()
        logger.log("::: NIBL ::: INVOICE NUMBER === $invoiceNumber")
        return StringUtils.ofRequiredLength(
            invoiceNumber.toString(), 6
        )
    }

    private fun getTPDUString(hostItem: HostItem): String? {
        logger.log("NII::: ${hostItem.nii}")
//        return hostItem.nii
        return "00110000" // TODO change after issue fix in front end
    }

    override fun shouldShufflePinPad(): Boolean =
        PreferenceManager.getShouldShufflePinPadPref(false)

    override fun updateActivityLog(activityLog: ActivityLog?) {
        if (activityLog == null)
            return
        ActivityLogRepositoryImpl().updateActivityLog(activityLog)
    }

    override fun getTerminalSerialNumber(): String {
        return NiblMerchant.INSTANCE.iPlatformManager.serialNumber
    }
}