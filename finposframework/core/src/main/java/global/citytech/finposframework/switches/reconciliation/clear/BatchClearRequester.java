package global.citytech.finposframework.switches.reconciliation.clear;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 9/28/2021.
 */
public interface BatchClearRequester<T extends BatchClearRequestParameter, E extends BatchClearResponseParameter>
        extends Request<T, E> {
}
