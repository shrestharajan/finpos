package global.citytech.finpos.merchant.presentation.mobilenfcpayment

import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Handler
import androidx.lifecycle.MutableLiveData
import global.citytech.common.data.Response
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.MobileNfcPaymentRepositoryImpl
import global.citytech.finpos.merchant.data.repository.device.DeviceRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.MobileNfcPayment
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.app.MobilePaymentUseCase
import global.citytech.finpos.merchant.domain.usecase.device.DeviceUseCase
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.INITIATION_MOBILE_NFC
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.DECLINED_PAYMENT_MOBILE
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.ERROR_MOBILE_NFC
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.PROCESSING_PAYMENT_MOBILE
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.ERROR_PROCESSING_MOBILE
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.PROCESSING_MOBILE_NFC
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.APPROVED_PAYMENT_MOBILE
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.ERROR_PAYMENT_STATUS_MOBILE
import global.citytech.finpos.merchant.domain.usecase.mobile.request.MobileNfcPaymentGenerateRequest
import global.citytech.finpos.merchant.domain.usecase.mobile.request.MobileNfcPaymentStatusRequest
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.MobileNfcPaymentDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.mobilenfcpayment.receipt.MobileNfcPaymentReceiptGenerator
import global.citytech.finpos.merchant.presentation.mobilenfcpayment.receipt.MobileNfcTransactionReceiptPrintHandler
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.domain.usecase.mobile.response.MobileNfcPaymentStatusResponse
import global.citytech.finpos.merchant.framework.datasource.device.DeviceConfigurationSourceImpl
import global.citytech.finpos.merchant.presentation.dynamicqr.nqrsale.DynamicNQrActivity
import global.citytech.finpos.merchant.presentation.model.mobile.list.AidParameterResponse
import global.citytech.finpos.merchant.service.NetworkConnectionReceiver
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.SoundUtils
import global.citytech.finpos.merchant.utils.VoiceMessageUtils
import global.citytech.finposframework.hardware.io.led.LedLight
import global.citytech.finposframework.hardware.io.printer.PrinterResponse
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.repositories.TerminalRepository
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion
import global.citytech.finposframework.utility.StringUtils.formatAmountTwoDecimal
import global.citytech.payment.sdk.utils.StringUtils
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject

/**
 * @author SIDDHARTHA GHIMIRE
 */
class MobileNfcPaymentViewModel(val context: Application) : BaseAndroidViewModel(context) {
    private val logger = Logger.getLogger(MobileNfcPaymentViewModel::class.java.name)

    val appConfiguration by lazy { MutableLiveData<ConfigurationItem>() }
    val transactionConfirmationLiveData by lazy { MutableLiveData<TransactionConfirmation>() }
    var updateTransactionConfirmation = false
    var amount = ""
    val nfcStatusMessage by lazy { MutableLiveData<String>() }
    val showAlertDialog by lazy { MutableLiveData<Boolean>() }
    val showNfcSuccess by lazy { MutableLiveData<String>() }
    val showPiccLoading by lazy { MutableLiveData<Boolean>() }
    val showProgressMessage by lazy { MutableLiveData<String>() }
    val internetConnection by lazy { MutableLiveData<Boolean>() }
    val performWaveAgain by lazy { MutableLiveData<Boolean>() }
    val aidReceivedSuccessMesage by lazy { MutableLiveData<Boolean>() }
    var count = 0
    private var paymentStatusCheckCount = 0


    var localDataUseCase: LocalDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )
    private var mobilePaymentUseCase = MobilePaymentUseCase(
        MobileNfcPaymentRepositoryImpl(
            MobileNfcPaymentDataSourceImpl(context)
        )
    )

    private val deviceConfigurationUseCase = DeviceUseCase(
        DeviceRepositoryImpl(DeviceConfigurationSourceImpl(context))
    )


    lateinit var terminalRepository: TerminalRepository
    val printerService = PrinterSourceImpl(context)
    var mobileNfcPayment: MobileNfcPayment? = null

    fun getConfiguration(): Boolean {
        this.isLoading.value = true

        return compositeDisposable.add(
            localDataUseCase.getWholeConfiguration()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe({
                    isLoading.value = false
                    appConfiguration.value = it
                    terminalRepository = TerminalRepositoryImpl(appConfiguration.value)
                    NetworkConnectionReceiver.networkConfiguration = it
                },
                    {
                        this.isLoading.value = false
                        nfcStatusMessage.value = it.message
                    })
        )
    }

    fun bindService() {
        compositeDisposable.add(
            Single.create(SingleOnSubscribe<Response> {
                val bindIntent = Intent()
                val packageName = "global.citytech.tms"
                val className = "global.citytech.tms.service.PlatformManagerService"
                bindIntent.setClassName(packageName, className)
                context.bindService(
                    bindIntent,
                    NiblMerchant.INSTANCE.getPlatformServiceConnection(),
                    Context.BIND_AUTO_CREATE
                )
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                }, {
                    it.printStackTrace()
                    isLoading.value = false
                    nfcStatusMessage.value = it.message
                })
        )
    }

    fun prepareNfcData() {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<NfcResponse> {
                getNfcTask(it)
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgressMessage.value = PROCESSING_MOBILE_NFC
                    generateMobilePayRequest(it)
                }, {
                    waveAgainValidation(it.message)
                })
        )
    }

    private fun getNfcTask(it: ObservableEmitter<NfcResponse>) {
        mobilePaymentUseCase.getNfcData()?.let { response ->
            if (response.nfcData.decodeToString() == ERROR_MOBILE_NFC) {
                logger.debug(":::Error nfc response:::")
                it.onError(Exception(ERROR_MOBILE_NFC))
            } else {
                turnOffLedIfExist()
                logger.debug(":::Success nfc response:::")
                count = 0
                it.onNext(response)
            }
        }
    }


    private fun waveAgainValidation(message: String?) {
        logger.debug(":::Validating error:::".plus(message))
        if (count < 3) {
            count++
            logger.debug(":::Wave again required:::")
            mobilePaymentUseCase.displaySoundAndLedWithRespectiveDevice(LedLight.YELLOW)
            performWaveAgain.value = true
        } else {
            logger.debug(":::Wave again not required:::")
            mobilePaymentUseCase.displaySoundAndLedWithRespectiveDevice(LedLight.RED)
            count = 0
            nfcStatusMessage.value = message
        }
    }

    fun doCleanUp() = Completable
        .fromAction {
            deviceConfigurationUseCase.closeUpIO()
        }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            { this.onCleanUpComplete() },
            { this.onCleanUpError(it) }
        )

    private fun onCleanUpComplete() {
        logger.log("corelogger Clean Up Complete")
    }

    private fun onCleanUpError(throwable: Throwable) {
        logger.log("corelogger Clean Up Error ::: ".plus(throwable.message))
    }

    fun cancelNfcTimer() {
        mobilePaymentUseCase.cancelTimer()
    }

    private fun generateMobilePayRequest(nfcData: NfcResponse) {
        logger.debug("RESPONSE DATA: ${String(nfcData.nfcData)}")
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<MobileNfcPaymentGenerateRequest> {
                it.onNext(
                    if (nfcData.aid == "A000029524" || nfcData.aid == "F222222124" ||  nfcData.aid == "F222222123") {
                        decodeNfcData(nfcData.nfcData, nfcData.aid)
                    } else {
                        val jsonResponse = String(nfcData.nfcData)
                        val jsonObject = JSONObject(jsonResponse)
                        val nfcToken = jsonObject.getString("data")
                        MobileNfcPaymentGenerateRequest("", "", nfcToken)
                    }
                )
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it.amount = amount
                    it.merchantId = terminalRepository.findTerminalInfo().merchantID
                    it.terminalId = terminalRepository.findTerminalInfo().terminalID
                    it.initiationMethod = INITIATION_MOBILE_NFC
                    initiateMobileNfcPaymentTransaction(
                        mobilePaymentUseCase.prepareMobileNfcPaymentRequest(
                            it
                        )
                    )
                }, {
                    mobilePaymentUseCase.displaySoundAndLedWithRespectiveDevice(LedLight.YELLOW)
                    waveAgainValidation(ERROR_PROCESSING_MOBILE)
                })
        )
    }


    private fun initiateMobileNfcPaymentTransaction(mobileNfcPaymentStatusRequest: MobileNfcPaymentStatusRequest) {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Response> {
                mobilePaymentUseCase.incrementNfcPaymentIdentifiers()
                it.onNext(
                    mobilePaymentUseCase.requestMobileNfcPaymentTransaction(
                        mobileNfcPaymentStatusRequest
                    )
                )
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.data != null) {
                        it.data?.let { response ->
                            checkPaymentStatus(response)
                        }
                    } else if (it.message.isEmpty()) {
                        mobilePaymentUseCase.displaySoundAndLedWithRespectiveDevice(LedLight.YELLOW)
                        nfcStatusMessage.value = ERROR_PAYMENT_STATUS_MOBILE
                    } else if (it.message == context.getString(R.string.no_internet_transactions)) {
                        internetConnection.value = false;
                    } else {
                        mobilePaymentUseCase.displaySoundAndLedWithRespectiveDevice(LedLight.YELLOW)
                        nfcStatusMessage.value = it.message
                    }
                }, {
                    mobilePaymentUseCase.displaySoundAndLedWithRespectiveDevice(LedLight.YELLOW)
                    it.printStackTrace()
                    internetConnection.value = true;
                })
        )
    }

    private fun checkPaymentStatus(response: Any) {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<MobileNfcPaymentStatusResponse> {
                it.onNext(mobilePaymentUseCase.checkMobileNfcPaymentStatus(response))
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    when (it.paymentStatus) {
                        PROCESSING_PAYMENT_MOBILE -> {
                            recheckPaymentStatusAfterOneSecondDelayIfCompositeDisposableIsNotDisposed(
                                response
                            )
                        }
                        APPROVED_PAYMENT_MOBILE -> {
                            processApprovedResultAfterOneSecondDelay(it)
                        }
                        else -> {
                            mobilePaymentUseCase.displaySoundAndLedWithRespectiveDevice(LedLight.YELLOW)
                            nfcStatusMessage.value = it.message
                        }
                    }
                }, {
                    paymentStatusCheckCount++
                    if (paymentStatusCheckCount < 3) {
                        checkPaymentStatus(response)
                    } else {
                        paymentStatusCheckCount = 0;
                        mobilePaymentUseCase.displaySoundAndLedWithRespectiveDevice(LedLight.YELLOW)
                        nfcStatusMessage.value = ERROR_PAYMENT_STATUS_MOBILE
                        it.printStackTrace()
                    }
                })
        )
    }

    private fun recheckPaymentStatusAfterOneSecondDelayIfCompositeDisposableIsNotDisposed(
        response: Any
    ) {
        if (!compositeDisposable.isDisposed) {
            logger.debug("checkPaymentStatus ::: PROCESSING_PAYMENT_MOBILE")
            Handler().postDelayed({
                checkPaymentStatus(response)
            }, 1000)
        }
    }

    private fun processApprovedResultAfterOneSecondDelay(it: MobileNfcPaymentStatusResponse) {
        logger.debug("checkPaymentStatus ::: APPROVED_PAYMENT_MOBILE")
        Handler().postDelayed({
            onPaymentStatusApproved(it)
        }, 1000)
    }

    private fun onPaymentStatusApproved(it: MobileNfcPaymentStatusResponse) {
        mobileNfcPayment = it.billingInformation?.let { response ->
            MobileNfcPayment(
                merchantId = response.mid,
                terminalId = response.tid,
                referenceNumber = response.reference,
                invoiceNumber = "",
                transactionType = "SALE",
                transactionDate = response.txnDate,
                transactionTime = response.txnTime,
                transactionCurrency = response.currency,
                transactionAmount = response.amount,
                transactionStatus = response.transactionResult,
                approvalCode = response.approvalCode,
                paymentInitiator = response.paymentInitiator,
                initiatorId = response.payerMobileNumber
            )
        }
        showAlertDialog.value = false
        updateTransactionConfirmation = false
        val isApproved =
            !StringUtils.isEmpty(it.billingInformation?.approvalCode)
        transactionConfirmationLiveData.value =
            it.billingInformation?.let { response ->
                TransactionConfirmation.Builder()
                    .amount(formatAmountTwoDecimal(response.amount))
                    .imageId(if (isApproved) R.drawable.approved else R.drawable.declined)
                    .message(if (isApproved) "Approval Code: ${response.approvalCode}" else DECLINED_PAYMENT_MOBILE)
                    .title(if (isApproved) APPROVED_PAYMENT_MOBILE else DECLINED_PAYMENT_MOBILE)
                    .positiveLabel("PRINT CUSTOMER COPY")
                    .negativeLabel("RETURN TO DASHBOARD")
                    .isApprove(isApproved)
                    .qrImageString("")
                    .transactionStatus("")
                    .build()
            }
        playSound(isApproved, it.billingInformation!!.amount.toDouble())
        android.os.Handler().postDelayed({
            printPaymentReceipt(ReceiptVersion.MERCHANT_COPY)
        }, 1000)
    }

    private fun decodeNfcData(
        nfcResponseData: ByteArray,
        aid: String
    ): MobileNfcPaymentGenerateRequest {
        return mobilePaymentUseCase.parseNfcData(nfcResponseData, aid)
    }

    fun printPaymentReceipt(receiptVersion: ReceiptVersion) {
        if (receiptVersion == ReceiptVersion.CUSTOMER_COPY)
            isLoading.value = true
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<PrinterResponse> {
                val dynamicReceipt =
                    MobileNfcPaymentReceiptGenerator(terminalRepository).generate(
                        mobileNfcPayment!!
                    )
                val printerResponse =
                    MobileNfcTransactionReceiptPrintHandler(printerService).printReceipt(
                        dynamicReceipt,
                        receiptVersion
                    )
                it.onNext(printerResponse)
                it.onComplete()
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    updateTransactionConfirmation = true
                    if (receiptVersion == ReceiptVersion.MERCHANT_COPY) {
                        val transactionConfirmation = transactionConfirmationLiveData.value
                        transactionConfirmation!!.positiveLabel = "PRINT CUSTOMER COPY"
                        transactionConfirmation.negativeLabel = "RETURN TO DASHBOARD"
                        transactionConfirmationLiveData.value = transactionConfirmation
                    } else {
                        message.value = it.message
                    }
                }, {
                    isLoading.value = false
                })
        )
    }

    fun turnOffLedIfExist() {
        mobilePaymentUseCase.hideAlertLed()
    }

    fun enableAlertDialogFlag() {
        showAlertDialog.value = true
    }

    fun dispose() {
        compositeDisposable.clear()
        compositeDisposable.dispose()
    }

    fun cleanNfcTimerAndLatches() {
        mobilePaymentUseCase.cleanMobileNfcTask()
    }


    fun getAidListForNFCPayment() {
        compositeDisposable.add(
            callAPI().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    aidReceivedSuccessMesage.value = true
                    mobilePaymentUseCase = MobilePaymentUseCase(
                        MobileNfcPaymentRepositoryImpl(
                            MobileNfcPaymentDataSourceImpl(context)
                        )
                    )
                    logger.debug("CALLING API FOR AID ::::: :::: :::: " + aidReceivedSuccessMesage.value)
                },
                    {
                        aidReceivedSuccessMesage.value = true
                        logger.debug("CALLING API FOR AID 11::::: :::: :::: ")
                    }
                )
        )
    }

    fun callAPI(): Observable<AidParameterResponse> {
        return Observable.fromCallable {
            val jsonResponse =
                NiblMerchant.INSTANCE.iPlatformManager.get("/device-tms-platform/v1/device/public/aid-configuration")
            val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
            val responseJson = Jsons.toJsonObj(response.data)
            PreferenceManager.saveAidResponse(responseJson)
            Jsons.fromJsonToObj(responseJson, AidParameterResponse::class.java)
        }
    }

    private fun playSound(isApproved: Boolean, amount: Double) {
        if (isApproved) {
            /** APPROVED */
            if (localDataUseCase.isSoundMode(false))
                SoundUtils.play(
                    getApplication<Application?>().applicationContext,
                    amount,
                    "blank",
                    AppUtility.getCurrencyCode()
                )
            Logger.getLogger(DynamicNQrActivity::class.simpleName)
                .debug("transaction approved")
        } else {
            /** DECLINED */
            if (localDataUseCase.isSoundMode(false))
                VoiceMessageUtils.playFail(getApplication<Application?>().applicationContext)
            Logger.getLogger(DynamicNQrActivity::class.simpleName)
                .debug("transaction declined")
        }
    }
}
