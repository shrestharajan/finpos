package global.citytech.finpos.processor.nibl.transaction;

import global.citytech.finpos.nibl.iso8583.S2MActionCode;
import global.citytech.finpos.processor.nibl.NiblConstants;
import global.citytech.finpos.processor.nibl.transaction.authorizer.NiblTransactionAuthorizer;
import global.citytech.finpos.processor.nibl.transaction.initializer.NiblTransactionInitializer;
import global.citytech.finpos.processor.nibl.transaction.receipt.NiblIsoMessageReceiptHandler;
import global.citytech.finpos.processor.nibl.transaction.receipt.NiblTransactionReceiptPrintHandler;
import global.citytech.finpos.processor.nibl.transaction.receipt.TransactionReceiptHandler;
import global.citytech.finpos.processor.nibl.transaction.responsehandler.NiblInvalidResponseHandler;
import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils;
import global.citytech.finpos.processor.nibl.transaction.validator.NiblTransactionValidator;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardSummary;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.PinBlock;
import global.citytech.finposframework.hardware.io.cards.cvm.Cvm;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat;
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest;
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.TransactionStatusFromHost;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandler;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.TransactionAuthorizer;
import global.citytech.finposframework.usecases.transaction.TransactionInitializer;
import global.citytech.finposframework.usecases.transaction.TransactionValidator;
import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.usecases.transaction.data.TransactionAuthorizationResponse;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.PosResponse;
import global.citytech.finposframework.utility.PosResult;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finposframework.utility.TMSConfigurationConstants.APPROVED_PRINT_ONLY;

;

/**
 * Created by Rishav Chudal on 11/2/20.
 */
public abstract class ManualTransactionUseCase {

    protected final TerminalRepository terminalRepository;
    protected final Notifier notifier;
    protected final Logger logger;
    protected TransactionValidator transactionValidator;
    protected TransactionRepository transactionRepository;
    protected AutoReversal autoReversal;

    public ManualTransactionUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        this.terminalRepository = terminalRepository;
        this.notifier = notifier;
        this.logger = Logger.getLogger(getClassName());
    }

    protected boolean deviceIsReady(
            DeviceController deviceController,
            TransactionType transactionType
    ) {
        PosResponse deviceReadyResponse = deviceController.isReady(global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils
                .getAllowedCardEntryModes(transactionType));
        if (deviceReadyResponse.getPosResult() == PosResult.SUCCESS)
            return true;
        else
            throw new PosException(deviceReadyResponse.getPosError());
    }

    protected void initTransaction(
            ApplicationRepository applicationRepository,
            TransactionRequest transactionRequest
    ) {
        TransactionInitializer transactionInitializer
                = new NiblTransactionInitializer(applicationRepository);
        TransactionRequest transactionRequestAfterInit
                = transactionInitializer.initialize(transactionRequest);
        this.validateRequest(transactionRequestAfterInit, applicationRepository);
    }

    private void validateRequest(
            TransactionRequest transactionRequest,
            ApplicationRepository applicationRepository
    ) {
        this.transactionValidator = new global.citytech.finpos.processor.nibl.transaction.validator.NiblTransactionValidator(applicationRepository);
        this.transactionValidator.validateRequest(transactionRequest);
    }

    protected void identifyAndSetCardScheme(
            ReadCardResponse readCardResponse,
            TransactionRepository transactionRepository,
            ApplicationRepository applicationRepository
    ) {
        global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(
                transactionRepository,
                applicationRepository
        );
        cardIdentifier.identify(readCardResponse);
    }

    protected ReadCardResponse prepareManualReadCardResponse(
            TransactionRequest transactionRequest
    ) {
        CardDetails cardDetails = new CardDetails();
        cardDetails.setCardType(CardType.MANUAL);
        cardDetails.setPrimaryAccountNumber(transactionRequest.getCardNumber());
        cardDetails.setExpiryDate(transactionRequest.getExpiryDate());
        cardDetails.setAmount(transactionRequest.getAmount());
        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
        cardDetails.setCvv(transactionRequest.getCvv());
        return new ReadCardResponse(
                cardDetails,
                Result.SUCCESS,
                "Manual Read Card Response Success"
        );
    }

    protected boolean validTransaction(
            ApplicationRepository applicationRepository,
            TransactionRequest transactionRequest,
            ReadCardResponse readCardResponse
    ) {
        this.transactionValidator = new NiblTransactionValidator(applicationRepository);
        return this.transactionValidator.isValidTransaction(
                transactionRequest,
                readCardResponse,
                readCardResponse.getCardDetails().getCardScheme().getCardSchemeId()
        );
    }

    protected boolean transactionAuthorizationSucceeds(
            TransactionAuthenticator transactionAuthenticator,
            ApplicationRepository applicationRepository,
            TransactionType transactionType,
            ReadCardResponse readCardResponse
    ) {
        TransactionAuthorizer transactionAuthorizer = new NiblTransactionAuthorizer(
                transactionAuthenticator,
                applicationRepository
        );
        TransactionAuthorizationResponse transactionAuthorizationResponse = transactionAuthorizer
                .authorizeUser(
                        transactionType,
                        readCardResponse.getCardDetails().getCardScheme().getCardSchemeId()
                );
        return transactionAuthorizationResponse.isSuccess();
    }

    protected ReadCardResponse retrievePinBlockIfRequired(
            PurchaseRequest purchaseRequest,
            ApplicationRepository applicationRepository,
            TransactionAuthenticator transactionAuthenticator
    ) {
        PinBlock pinBlock = new PinBlock("", true);
        Cvm cvm = this.retrieveCvm(
                purchaseRequest,
                applicationRepository,
                transactionAuthenticator
        );
        boolean pinRequired = cvm.equals(Cvm.ENCIPHERED_PIN_ONLINE);
        String pinPadAmountMessage = global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils.retrievePinPadMessageWithAmount(purchaseRequest.getAmount(),
                this.terminalRepository.getTerminalTransactionCurrencyName());
        if (pinRequired) {
            pinBlock = this.retrievePinBlock(
                    purchaseRequest
                            .getCardDetails(),
                    transactionAuthenticator,
                    pinPadAmountMessage
            );
        }
        ReadCardResponse readCardResponse = new ReadCardResponse(
                purchaseRequest.getCardDetails(),
                Result.SUCCESS,
                "Successful"
        );
        readCardResponse.setCvm(cvm);
        readCardResponse.getCardDetails().setPinBlock(pinBlock);
        readCardResponse
                .getCardDetails()
                .setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
        readCardResponse.setFallbackIccToMag(purchaseRequest.isFallbackIccToMag());
        return readCardResponse;
    }

    private Cvm retrieveCvm(
            PurchaseRequest purchaseRequest,
            ApplicationRepository applicationRepository,
            TransactionAuthenticator transactionAuthenticator
    ) {
        if (purchaseRequest.getCardDetails() == null ||
                purchaseRequest.getCardDetails().getCardScheme() == null) {
            return Cvm.NO_CVM;
        }
        String shortCardScheme = purchaseRequest.getCardDetails().getCardScheme()
                .getCardSchemeId();
        TransactionSetTableVerifier verifier = new TransactionSetTableVerifier(
                applicationRepository,
                shortCardScheme,
                TransactionUtils
                        .mapPurchaseTypeWithTransactionType(purchaseRequest.getTransactionType()));
        String cvmToPerform = verifier.getCardHolderVerificationMethod();
        switch (cvmToPerform) {
            case "1":
                return Cvm.SIGNATURE;
            case "2":
            case "3":
                return Cvm.ENCIPHERED_PIN_ONLINE;
            default:
                return Cvm.NO_CVM;
        }
    }

    private PinBlock retrievePinBlock(
            CardDetails cardDetails,
            TransactionAuthenticator transactionAuthenticator,
            String pinPadAmountMessage
    ) {
        String primaryAccountNumber = cardDetails.getPrimaryAccountNumber();
        if (!StringUtils.isEmpty(primaryAccountNumber)) {
            PinRequest pinRequest = new PinRequest(
                    primaryAccountNumber,
                    NiblConstants.PIN_PAD_MESSAGE,
                    PinBlockFormat.ISO9564_FORMAT_0,
                    4,
                    12,
                    30,
                    pinPadAmountMessage,
                    !this.terminalRepository.shouldShufflePinPad(),
                    false,
                    "Enter PIN"
            );
            CardSummary cardSummary = new CardSummary(cardDetails.getCardSchemeLabel(),
                    primaryAccountNumber, cardDetails.getCardHolderName(), DateUtils.toYYMMFormat(cardDetails.getExpiryDate()));
            pinRequest.setCardSummary(cardSummary);
            PinResponse response = transactionAuthenticator.authenticateUser(pinRequest);
            this.logger.debug("::: PIN RESPONSE ::: " + response.toString());
            if (response.getResult() == Result.SUCCESS)
                return response.getPinBlock();
            else if (response.getResult() == Result.USER_CANCELLED)
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            else if (response.getResult() == Result.TIMEOUT)
                throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
            else
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    protected RequestContext prepareManualTemplateRequestContext(String stan) {
        HostInfo primaryHostInfo = this.terminalRepository.findPrimaryHost();
        HostInfo secondaryHostInfo = this.terminalRepository.findSecondaryHost();
        TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
        ConnectionParam connectionParam = new ConnectionParam(primaryHostInfo, secondaryHostInfo);
        return new RequestContext(stan, terminalInfo, connectionParam);
    }

    protected InvalidResponseHandlerResponse checkForInvalidResponseFromHost(
            IsoMessageResponse isoMessageResponse
    ) {
        InvalidResponseHandler invalidResponseHandler = new NiblInvalidResponseHandler();
        return invalidResponseHandler.isInvalidResponse(isoMessageResponse);
    }

    protected boolean printReceipt(
            String batchNumber,
            String stan,
            String invoiceNumber,
            PrinterService printerService,
            ApplicationRepository applicationRepository,
            TransactionRepository transactionRepository,
            TransactionRequest transactionRequest,
            ReadCardResponse readCardResponse,
            IsoMessageResponse isoMessageResponse
    ) {
        printDebugReceiptInDebugMode(
                printerService,
                isoMessageResponse
        );
        boolean approved = this.transactionApprovedByActionCode(isoMessageResponse);
        boolean approvePrintOnly = this.approvePrintOnly(applicationRepository);
        boolean shouldPrintReceipt = approved || !approvePrintOnly;
        logger.log("::: TRANSACTION USE CASE ::: SHOULD PRINT RECEIPT == " + shouldPrintReceipt);
        if (shouldPrintReceipt) {
            ReceiptHandler.TransactionReceiptHandler receiptHandler
                    = new NiblTransactionReceiptPrintHandler(printerService);
            ReceiptLog receiptLog = new global.citytech.finpos.processor.nibl.transaction.receipt.TransactionReceiptHandler(
                    transactionRequest,
                    readCardResponse,
                    isoMessageResponse,
                    this.terminalRepository,
                    applicationRepository
            ).prepare(
                    batchNumber,
                    stan,
                    invoiceNumber
            );
            transactionRepository.updateReceiptLog(receiptLog);
            receiptHandler.printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
        }
        return shouldPrintReceipt;
    }

    private void printDebugReceiptInDebugMode(
            PrinterService printerService,
            IsoMessageResponse isoMessageResponse
    ) {
        boolean isDebugVersion = this.terminalRepository.findTerminalInfo().isDebugModeEnabled();
        if (isDebugVersion) {
            ReceiptHandler.IsoMessageReceiptHandler isoMessageReceiptHandler
                    = new NiblIsoMessageReceiptHandler(printerService);
            isoMessageReceiptHandler.printIsoMessageReceipt(isoMessageResponse);
        }
    }

    protected boolean transactionApprovedByActionCode(IsoMessageResponse isoMessageResponse) {
        int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(
                isoMessageResponse,
                DataElement.RESPONSE_CODE
        );
        return S2MActionCode
                .getByActionCode(actionCode)
                .getTransactionStatusFromHost() == TransactionStatusFromHost.ONLINE_APPROVED;
    }

    protected void notifyTransactionStatus(
            boolean transactionApproved,
            IsoMessageResponse isoMessageResponse,
            ApplicationRepository applicationRepository
    ) {
        if (transactionApproved) {
            this.notifier.notify(
                    Notifier.EventType.TRANSACTION_APPROVED,
                    this.approvedMessage(isoMessageResponse)
            );
        } else
            this.notifier.notify(
                    Notifier.EventType.TRANSACTION_DECLINED,
                    this.declinedMessage(
                            isoMessageResponse,
                            applicationRepository
                    )
            );
    }

    protected void updateTransactionIds(boolean incrementInvoiceNumber) {
        this.terminalRepository.incrementSystemTraceAuditNumber();
        this.terminalRepository.incrementRetrievalReferenceNumber();
        if (incrementInvoiceNumber) {
            this.terminalRepository.incrementInvoiceNumber();
        }
    }

    private String approvedMessage(IsoMessageResponse isoMessageResponse) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Approval Code : ");
        stringBuilder.append(
                IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse, DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE)
        );
        return stringBuilder.toString();
    }

    private String declinedMessage(IsoMessageResponse isoMessageResponse,
                                   ApplicationRepository applicationRepository) {
        int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(
                isoMessageResponse,
                DataElement.RESPONSE_CODE
        );
        if (actionCode < 0)
            throw new IllegalArgumentException("Invalid Action Code");
        String messageText = applicationRepository.getMessageTextByActionCode(actionCode);
        if (StringUtils.isEmpty(messageText))
            messageText = S2MActionCode.getByActionCode(actionCode).getDescription();
        return messageText;
    }

    protected String retrieveMessage(IsoMessageResponse response, boolean isTransactionApproved,
                                     ApplicationRepository applicationRepository) {
        if (isTransactionApproved)
            return this.approvedMessage(response);
        else
            return this.declinedMessage(response, applicationRepository);
    }

    protected abstract String getClassName();

    protected boolean isResponseNotReceivedException(FinPosException fe) {
        return fe.getType() == FinPosException.ExceptionType.READ_TIME_OUT ||
                fe.getType() == FinPosException.ExceptionType.CONNECTION_ERROR ||
                fe.getType() == FinPosException.ExceptionType.WRITE_EXCEPTION;
    }


    protected boolean approvePrintOnly(ApplicationRepository applicationRepository) {
        String approvedPrintOnly = applicationRepository
                .retrieveFromAdditionalDataEmvParameters(APPROVED_PRINT_ONLY);
        logger.log("::: TRANSACTION USE CASE ::: APPROVED PRINT ONLY FROM EMV PARAMETERS === " + approvedPrintOnly);
        if (StringUtils.isEmpty(approvedPrintOnly))
            return false;
        return Boolean.parseBoolean(approvedPrintOnly);
    }

    protected ReceiptLog getReceiptLog(TransactionRequest transactionRequest, ReadCardResponse readCardResponse, IsoMessageResponse isoMessageResponse, ApplicationRepository applicationRepository, String batchNumber, String stan, String invoiceNumber) {
        return new TransactionReceiptHandler(transactionRequest, readCardResponse,
                isoMessageResponse, terminalRepository, applicationRepository).prepare(batchNumber, stan, invoiceNumber);
    }
}