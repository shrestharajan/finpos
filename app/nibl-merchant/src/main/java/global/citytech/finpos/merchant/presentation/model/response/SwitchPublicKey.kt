package global.citytech.finpos.nibl.merchant.presentation.model.response

data class SwitchPublicKey(
    var index: String? = null,
    var key: String? = null
)