package global.citytech.finpos.processor.neps.reconciliation.check;

import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reconciliation.check.CheckReconciliationRequestParameter;

public class CheckReconciliationRequestModel implements CheckReconciliationRequestParameter, UseCase.Request {
    private final ReconciliationRepository reconciliationRepository;
    private final ApplicationRepository applicationRepository;

    public CheckReconciliationRequestModel(ReconciliationRepository reconciliationRepository, ApplicationRepository applicationRepository) {
        this.reconciliationRepository = reconciliationRepository;
        this.applicationRepository = applicationRepository;
    }

    public ReconciliationRepository getReconciliationRepository() {
        return reconciliationRepository;
    }

    public ApplicationRepository getApplicationRepository() {
        return applicationRepository;
    }
}
