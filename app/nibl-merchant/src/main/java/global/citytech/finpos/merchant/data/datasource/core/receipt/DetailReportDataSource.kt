package global.citytech.finpos.merchant.data.datasource.core.receipt

import global.citytech.finpos.merchant.domain.model.receipt.detailreport.DetailReportResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportResponseModel
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 1/4/2021.
 */
interface DetailReportDataSource {
    fun printDetailReport(configurationItem: ConfigurationItem): Observable<DetailReportResponseEntity>
}