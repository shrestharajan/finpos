package global.citytech.finpos.merchant.presentation.admin.login

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import global.citytech.common.extensions.hideKeyboard
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.utils.handleDebounce
import global.citytech.finposframework.log.Logger
import kotlinx.android.synthetic.main.dialog_login.*

/**
 * Created by Rishav Chudal on 5/6/21.
 */
class LoginDialog : DialogFragment() {
    private var pageTitle: String? = null
    private var validCredential: String? = null
    private lateinit var listener: LoginDialogListener
    private val logger = Logger(LoginDialog::class.java)

    companion object {
        const val TAG = "LoginDialog"
        const val BUNDLE_DATA_PAGE_TITLE = "page title"
        const val BUNDLE_DATA_VALID_CREDENTIAL = "valid credential"

        fun newInstance(pageTitle: String, validCredential: String? = null): LoginDialog {
            val adminLoginDialog = LoginDialog()
            val bundle = Bundle()
            bundle.putString(BUNDLE_DATA_PAGE_TITLE, pageTitle)
            bundle.putString(BUNDLE_DATA_VALID_CREDENTIAL, validCredential)
            adminLoginDialog.arguments = bundle
            return adminLoginDialog
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        hideDialogTitle()
        return inflater.inflate(R.layout.dialog_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handlePageItems()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as LoginDialogListener
        } catch (e: ClassCastException) {
            this.logger.log("ClassCastException ::: Activity Should implement AdminLoginDialogListener")
        }
    }

    override fun onResume() {
        super.onResume()
        setDialogLayoutParams()
    }

    private fun hideDialogTitle() {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
    }

    private fun setDialogLayoutParams() {
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
        layoutParams.dimAmount = 0.5f
        dialog?.window?.attributes = layoutParams
        dialog?.window?.decorView?.setBackgroundColor(Color.TRANSPARENT)
        dialog?.window?.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
    }

    private fun handlePageItems() {
        getBundleData()
        initViews()
    }

    private fun getBundleData() {
        val bundle = arguments
        checkIfBundleIsNullAndProceed(bundle)
    }

    private fun checkIfBundleIsNullAndProceed(bundle: Bundle?) {
        if (bundle != null) {
            getPageTitleFromBundleData(bundle)
            getValidCredentialFromBundleData(bundle)
        } else {
            assignDefaultPageTitle()
        }
    }

    private fun getValidCredentialFromBundleData(bundle: Bundle) {
        validCredential = bundle.getString(
            BUNDLE_DATA_VALID_CREDENTIAL,
            null
        )
    }

    private fun getPageTitleFromBundleData(bundle: Bundle) {
        this.pageTitle = bundle.getString(
            BUNDLE_DATA_PAGE_TITLE,
            getString(R.string.title_enter_password)
        )
    }

    private fun assignDefaultPageTitle() {
        this.pageTitle = getString(R.string.title_enter_password)
    }

    private fun initViews() {
        initPageTitleTextView()
        text_input_et_admin_password.requestFocus()
        initPageButtons()
    }

    private fun initPageTitleTextView() {
        this.tv_title.text = this.pageTitle
    }

    private fun initPageButtons() {
        initAndHandleCancelButton()
        initAndHandleConfirmButton()
    }

    private fun initAndHandleCancelButton() {
        this.btn_cancel.setOnClickListener {
            btn_cancel.handleDebounce()
            this.activity?.hideKeyboard(text_input_et_admin_password)
            this.dismiss()
            this.listener.onLoginDialogCancelButtonClicked()
        }
    }

    private fun initAndHandleConfirmButton() {
        this.btn_confirm.setOnClickListener {
            btn_confirm.handleDebounce()
            this.validateCredentials(this.text_input_et_admin_password.text.toString())
        }
    }

    private fun validateCredentials(enteredCredential: String) {
        if (enteredCredential == validCredential) {
            listener.onLoginDialogCorrectCredentialEntered()
            this.activity?.hideKeyboard(text_input_et_admin_password)
            this.dismiss()
        }
        else {
            tv_title.text = getString(R.string.title_re_enter_password)
            text_input_et_admin_password.text!!.clear()
        }
    }

    fun updateDialogTitle(pageTitle: String) {
        this.tv_title.text = pageTitle
    }

    interface LoginDialogListener {
        fun onLoginDialogCorrectCredentialEntered()
        fun onLoginDialogCancelButtonClicked()
    }
}