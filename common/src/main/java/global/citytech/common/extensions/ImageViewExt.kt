package global.citytech.common.extensions

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.widget.ImageView


fun ImageView.loadBase64String(base64String: String?) {
    base64String?.let {
        val decodedString: ByteArray = Base64.decode(base64String, Base64.DEFAULT)
        val decodedByte: Bitmap =
            BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        setImageBitmap(decodedByte)
    }
}