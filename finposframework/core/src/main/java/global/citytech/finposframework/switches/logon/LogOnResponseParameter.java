package global.citytech.finposframework.switches.logon;

import global.citytech.finposframework.switches.ResponseParameter;

/** @author rajudhital on 3/29/20 */
public interface LogOnResponseParameter extends ResponseParameter {}
