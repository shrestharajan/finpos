package global.citytech.finpos.merchant.presentation.alertdialogs

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.DialogFragment
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.domain.model.balanceinquiry.BalanceInquiryModel
import global.citytech.finpos.merchant.domain.utils.Currency
import global.citytech.finpos.merchant.presentation.dashboard.temp.DashActivity
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finposframework.repositories.TerminalRepository
import global.citytech.finposframework.usecases.transaction.receipt.transaction.TransactionInfo
import kotlinx.android.synthetic.main.dialog_balance_result.*
import kotlin.concurrent.thread


class BalanceEnquiryDialog(
    val balanceInquiryModel: BalanceInquiryModel,
    val bankLogo: ByteArray,
    val dialogListener: BalanceEnquiryDialogListener,
    var currencyCode: String
) : DialogFragment() {
    val TAG = BalanceEnquiryDialog::class.java.name
    var bankLogoDisplay: ByteArray = byteArrayOf()
    var bankLogoBitMap: Bitmap

    companion object {
        const val TAG = "BalanceEnquiryDialog"
    }

    init {
        bankLogoDisplay = bankLogo
        bankLogoBitMap = BitmapFactory.decodeByteArray(bankLogoDisplay, 0, bankLogoDisplay.size)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        hideDialogTitle()
        return inflater.inflate(R.layout.dialog_balance_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bankLogoImageView = view.findViewById<ImageView>(R.id.iv_bank_logo)
        bankLogoImageView.setImageBitmap(bankLogoBitMap)
        view.findViewById<TextView>(R.id.tv_actual_balance_value).text =
            AppUtility.checkCurrencyCode(currencyCode).plus(". ")
                .plus(balanceInquiryModel.availableBalance)
        view.findViewById<TextView>(R.id.tv_current_balance_value).text =
            AppUtility.checkCurrencyCode(currencyCode).plus(". ")
                .plus(balanceInquiryModel.currentBalance)

        if (balanceInquiryModel.currentBalance.isEmpty()) {
            ll_currency_balance.visibility = View.GONE
        }
        view.findViewById<AppCompatImageView>(R.id.iv_back).setOnClickListener {
            context?.startActivity(Intent(view.context, DashActivity::class.java))
        }
        val button = view.findViewById<Button>(R.id.btn_balance_inquiry)
        button.setOnClickListener {
            dialogListener.printBalanceInquiry()
            dismiss()
        }

    }

    override fun onResume() {
        super.onResume()
        setDialogLayoutParams()
    }

    private fun hideDialogTitle() {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
    }

    private fun setDialogLayoutParams() {
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog?.window?.attributes = layoutParams
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    override fun getTheme(): Int {
        return R.style.FullScreenDialog
    }
}

interface BalanceEnquiryDialogListener {
    fun printBalanceInquiry()
}