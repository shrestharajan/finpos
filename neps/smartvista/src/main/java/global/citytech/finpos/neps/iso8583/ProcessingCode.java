package global.citytech.finpos.neps.iso8583;

/**
 * Created by Saurav Ghimire on 1/20/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public enum ProcessingCode {
    PURCHASE("000000", "Purchase"),
    LOG_ON("900000", "Log on"),
    KEY_EXCHANGE("990000", "Key Exchange"),
    PURCHASE_WITH_CASHBACK("090000", "Purchase With Cashback"),
    PRE_AUTHORIZATION("930000", "Pre Authorization"),
    REFUND("200000", "Refund"),
    CASH_ADVANCE("010000", "Cash Advance"),
    PRE_AUTHORIZATION_COMPLETION("940000", "Pre Authorization Completion"),
    REVERSAL("", "Reversal"),
    SETTLEMENT("910000", "Settlement"),
    VOID_SALE("020000", "Void Sale"),
    GREEN_PIN("890000", "Green Pin"),
    OTP_GENERATION("890000", "Green Pin"),
    PIN_SET("800000", "Green Pin"),
    PIN_CHANGE("790000", "Pin Change"),
    CASH_IN("250000","Cash In");


    private String code;
    private String description;

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

    private ProcessingCode(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public static ProcessingCode getByCode(String code) {
        ProcessingCode[] dataTypes = values();
        ProcessingCode[] var2 = dataTypes;
        int var3 = dataTypes.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            ProcessingCode dataType = var2[var4];
            if (dataType.getCode().equalsIgnoreCase(code)) {
                return dataType;
            }
        }

        throw new IllegalArgumentException(String.format(" Code %d has not been mapped ", code));
    }
}
