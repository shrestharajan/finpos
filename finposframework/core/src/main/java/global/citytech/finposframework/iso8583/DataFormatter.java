package global.citytech.finposframework.iso8583;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finposframework.utility.EncodingUtils;
import global.citytech.finposframework.utility.HexDump;
import global.citytech.finposframework.utility.StringUtils;

/***
 @author Surajchhetry
 ***/
public class DataFormatter {
    private Logger logger = Logger.getLogger(DataFormatter.class.getName());

    public byte[] formatAsBytes(DataElement element, DataElementConfig config) {
        String data = this.padDataIfRequired(element.getData().toString(), config);
        logger.debug(String.format("[%d] MODIFIED DATA - %s",element.getIndex(),  HexDump.dumpHexString(data.getBytes(StandardCharsets.UTF_8))));
        byte[] result = (config.getDataConfig().getEncodingType() == EncodingType.BCD) ?
                EncodingUtils.hex2Bytes(data)
                : data.getBytes(Charset.forName("UTF-8"));
        logger.debug(String.format("[%d] FORMATTED DATA - %s",element.getIndex(),  HexDump.dumpHexString(result)));
    //    logger.debug("::: FORMATTED BYTES ::: " + HexDump.dumpHexString(result));
        return result;
        /*
        switch (config.getDataConfig().getEncodingType()) {
            case BCD:
                return EncodingUtils.hex2Bytes(element.getData().toString());
        }
        return element.getData().toString().getBytes(Charset.forName("UTF-8"));

         */
    }

    private String padDataIfRequired(String originalData, DataElementConfig config) {
        if (config.getPaddingType() == PaddingType.POSTFIX_WITH_CHAR_F
                && StringUtils.isLengthOdd(originalData))
            return originalData + PaddingType.PaddingChar.CHAR_F;
        return originalData;
    }
    /*
    private String formatLengthIfLLVar(DataElement element, DataElementConfig config) {
        int length = element.getData().toString().length();
        if (length < 10)
            return String.format("0%d", length);
        return String.format("%d", length);
    }

    private String formatLengthIfLLLVar(DataElement element, DataElementConfig config) {
        int length = element.getData().toString().length();
        if (length < 10)
            return String.format("00%d", length);
        if (length > 10 && length < 100)
            return String.format("0%d", length);
        return String.format("%d", length);
    }


    public byte[] formatAccordingToType(DataElement element) {
        if (value == null) {
            return "ISOValue<null>".getBytes(Charset.forName("UTF-8"));
        }
        if (element.getType() == DataType.BINARY) return (byte[]) this.value;


        if (element.getType() == DataType.NUMERIC
                || element.getType() == DataType.AMOUNT) {
            if (element.getType() == DataType.AMOUNT) {
                return format(element, (BigDecimal) value, 12).getBytes(Charset.forName("UTF-8"));
            } else if (value instanceof Number) {
                return format(element, ((Number) value).longValue(), element.getSize())
                        .getBytes(Charset.forName("UTF-8"));
            } else {
                return format(element, value.toString(), element.getSize())
                        .getBytes(Charset.forName("UTF-8"));
            }
        } else if (element.getType() == DataType.ALPHA) {
            return format(element, value.toString(), element.getSize())
                    .getBytes(Charset.forName("UTF-8"));
        } else if (element.getLengthType() == LengthType.LLVAR
                || element.getLengthType() == LengthType.LLLVAR) {
            return value.toString().getBytes(Charset.forName("UTF-8"));
        } else if (value instanceof Date) {
            return format(element, (Date) value).getBytes(Charset.forName("UTF-8"));
        }
        return value.toString().getBytes(Charset.forName("UTF-8"));
    }

    /**
     * Formats a Date if the receiver is DATE10, DATE4, DATE_EXP or TIME; throws an exception
     * otherwise.

    private String format(DataElement element, Date value) {
        if (element.getType() == DataType.DATE10) {
            return new SimpleDateFormat("MMddHHmmss").format(value);
        } else if (element.getType() == DataType.DATE4) {
            return new SimpleDateFormat("MMdd").format(value);
        } else if (element.getType() == DataType.DATE_EXP) {
            return new SimpleDateFormat("yyMM").format(value);
        } else if (element.getType() == DataType.TIME) {
            return new SimpleDateFormat("HHmmss").format(value);
        } else if (element.getType() == DataType.DATE_YYYYMMDDHHMMSS) {
            return new SimpleDateFormat("yyyyMMddHHmmss").format(value);
        } else if (element.getType() == DataType.DATE_YYYYMMDD) {
            return new SimpleDateFormat("yyyyMMdd").format(value);
        }
        // DATE_YYYYMMDD
        throw new IllegalArgumentException("Cannot format date as " + this);
    }

    /**
     * Formats the string to the given length (length is only useful if type is ALPHA).

    private String format(DataElement element, String value, int length) {
        if (element.getType() == DataType.ALPHA) {
            if (value == null) {
                value = "";
            }
            if (value.length() > length) {
                return value.substring(0, length);
            }
            char[] c = new char[length];
            System.arraycopy(value.toCharArray(), 0, c, 0, value.length());
            for (int i = value.length(); i < c.length; i++) {
                c[i] = ' ';
            }
            return new String(c);
        } else if (element.getLengthType() == LengthType.LLVAR
                || element.getLengthType() == LengthType.LLLVAR) {
            return value;
        } else if (element.getType() == DataType.NUMERIC) {
            char[] c = new char[length];
            char[] x = value.toCharArray();
            if (x.length > length) {
                throw new IllegalArgumentException(
                        "Numeric value is larger than intended length: "
                                + value
                                + " LEN "
                                + length
                                + " For field "
                                + element.getIndex());
            }
            int lim = c.length - x.length;
            for (int i = 0; i < lim; i++) {
                c[i] = '0';
            }
            System.arraycopy(x, 0, c, lim, x.length);
            return new String(c);
        }
        throw new IllegalArgumentException("Cannot format String as " + this);
    }

    /**
     * Formats the integer value as a NUMERIC, an AMOUNT, or a String.

    private String format(DataElement element, long value, int length) {
        if (element.getType() == DataType.NUMERIC) {
            char[] c = new char[length];
            char[] x = Long.toString(value).toCharArray();
            if (x.length > length) {
                throw new IllegalArgumentException(
                        "Numeric value is larger than intended length: " + value + " LEN " + length);
            }
            int lim = c.length - x.length;
            for (int i = 0; i < lim; i++) {
                c[i] = '0';
            }
            System.arraycopy(x, 0, c, lim, x.length);
            return new String(c);
        } else if (element.getType() == DataType.ALPHA
                || element.getLengthType() == LengthType.LLVAR
                || element.getLengthType() == LengthType.LLLVAR) {
            return format(element, Long.toString(value), length);
        } else if (element.getType() == DataType.AMOUNT) {
            String v = Long.toString(value);
            char[] digits = new char[12];
            for (int i = 0; i < 12; i++) {
                digits[i] = '0';
            }
            System.arraycopy(v.toCharArray(), 0, digits, 10 - v.length(), v.length());
            return new String(digits);
        }
        throw new IllegalArgumentException("Cannot format number as " + this);
    }

    /**
     * Formats the BigDecimal as an AMOUNT, NUMERIC, or a String.

    private String format(DataElement element, BigDecimal value, int length) {
        if (element.getType() == DataType.AMOUNT) {
            String v =
                    new DecimalFormat(value.signum() >= 0 ? "0000000000.00" : "000000000.00").format(value);
            return v.substring(0, 10) + v.substring(11);
        } else if (element.getType() == DataType.NUMERIC) {
            return format(element, value.longValue(), length);
        } else if (element.getType() == DataType.ALPHA
                || element.getLengthType() == LengthType.LLVAR
                || element.getLengthType() == LengthType.LLLVAR) {
            return format(element, value.toString(), length);
        }
        throw new IllegalArgumentException("Cannot format BigDecimal as " + this);
    }

    public static DataElement getEmptyField() {
        return new DataElement(-1, -1, DataType.ALPHA, LengthType.FIXED);
    }
    */
}
