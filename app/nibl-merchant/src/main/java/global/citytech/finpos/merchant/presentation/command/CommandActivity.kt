package global.citytech.finpos.merchant.presentation.command

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityCommandBinding
import global.citytech.finpos.merchant.domain.model.app.CommandType
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.config.ConfigActivity
import global.citytech.finpos.merchant.presentation.settlement.SettlementActivity
import global.citytech.finpos.merchant.utils.AppUtility.printLog

/**
 * Created by Unique Shakya on 9/21/2021.
 */
class CommandActivity : AppBaseActivity<ActivityCommandBinding, CommandViewModel>() {

    val TAG = CommandActivity::class.java.name

    companion object {
        const val COMMAND_ACTIVITY_REQUEST_CODE = 4001
        const val DASH_ACTIVITY_NEED_RESTART_RESULT_CODE = 4002
        const val DASH_ACTIVITY_NOT_NEED_RESTART_RESULT_CODE = 4003
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel().retrieveCommands()
        initObservers()
    }

    private fun initObservers() {
        getViewModel().finishCommandTask.observe(this, Observer {
            if (it) {
                setResult(DASH_ACTIVITY_NOT_NEED_RESTART_RESULT_CODE)
                finish()
            }
        })

        getViewModel().startDashActivity.observe(this, Observer {
            if (it) {
                setResult(DASH_ACTIVITY_NEED_RESTART_RESULT_CODE)
                finish()
            }
        })

        getViewModel().startSettlementActivity.observe(this, Observer {
            if (it) {
                SettlementActivity.getLaunchIntent(
                    context = this,
                    isManualSettlement = false,
                    isSettlementSuccessConfirmationRequired = false,
                    isSettlementFromCommand = true
                )
            }
        })

        getViewModel().commandToExecute.observe(this, Observer {
            when (it.commandType) {
                CommandType.SWITCH_CONFIG_UPDATE -> startConfigActivity()
                CommandType.KEY_EXCHANGE -> getViewModel().performKeyExchange()
                CommandType.BANNER_UPDATE -> getViewModel().fetchBanners()
                CommandType.SYNC_SETTINGS -> getViewModel().syncSettings()
                CommandType.TRANSACTION_LOG_PUSH -> getViewModel().triggerTransactionLogPush()
                CommandType.SETTLEMENT -> getViewModel().performSettlement()
                CommandType.UNKNOWN -> getViewModel().handleUnknownCommand()

            }
        })

        getViewModel().isLoading.observe(this, Observer {
            if (it)
                showProgress()
            else
                hideProgress()
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ConfigActivity.REGISTER_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK)
                getViewModel().onCommandExecutionSuccess()
            else
                getViewModel().onCommandExecutionFailure(data!!.getStringExtra(ConfigActivity.EXTRA_ERROR_MESSAGE)!!)
        } else if (requestCode == SettlementActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val result = data?.getBooleanExtra("SETTLEMENT_RESULT", false)
                printLog(TAG,"SETTLEMENT RESULT " +  result.toString())
                if (result!!) {
                    getViewModel().onCommandExecutionSuccess()
                } else {
                    getViewModel().onCommandExecutionFailure(errorMessage = "Settlement failed")
                }
            }

        }

    }

    private fun startConfigActivity() {
        val intent = ConfigActivity.getLaunchIntent(
            this,
            getViewModel().isTerminalConfigured(),
            true
        )
        startActivityForResult(intent, ConfigActivity.REGISTER_REQUEST_CODE)
    }

    override fun getBindingVariable(): Int = BR.commandViewModel

    override fun getLayout(): Int = R.layout.activity_command

    override fun getViewModel(): CommandViewModel =
        ViewModelProviders.of(this)[CommandViewModel::class.java]
}