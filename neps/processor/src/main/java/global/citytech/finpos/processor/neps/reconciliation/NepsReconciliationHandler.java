package global.citytech.finpos.processor.neps.reconciliation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationHandler;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationTotals;

public class NepsReconciliationHandler implements ReconciliationHandler {

    private ReconciliationRepository reconciliationRepository;
    private String batchNumber;
    private ApplicationRepository applicationRepository;
    private TerminalRepository terminalRepository;

    public NepsReconciliationHandler(TerminalRepository terminalRepository,ReconciliationRepository reconciliationRepository, String batchNumber, ApplicationRepository applicationRepository) {
        this.reconciliationRepository = reconciliationRepository;
        this.batchNumber = batchNumber;
        this.applicationRepository = applicationRepository;
        this.terminalRepository = terminalRepository;
    }

    @Override
    public ReconciliationTotals prepareReconciliationTotals() {
        Map<CardSchemeType, NepsCardSchemeTotals> totalsMap = new HashMap<>();
        List<CardSchemeType> cardSchemeTypes = this.applicationRepository.getSupportedCardSchemes();
        for (CardSchemeType cardSchemeType : cardSchemeTypes) {
            NepsCardSchemeTotals nepsCardSchemeTotals
                    = prepareTotalsWithRespectToCardSchemeType(cardSchemeType);
            if (cardSchemeType != CardSchemeType.NONE  && nepsCardSchemeTotals.getTotalCount() > 0)
                totalsMap.put(cardSchemeType, nepsCardSchemeTotals);
        }
        return new NepsReconciliationTotals(this.terminalRepository.getTerminalTransactionCurrencyName(), totalsMap);
    }

    private NepsCardSchemeTotals prepareTotalsWithRespectToCardSchemeType(CardSchemeType cardSchemeType) {
        return NepsCardSchemeTotals.Builder.newInstance()
                .withTransactionCurrencyName(this.terminalRepository.getTerminalTransactionCurrencyName())
                .withCardSchemeType(cardSchemeType)
                .withSalesCount(prepareSalesCount(cardSchemeType))
                .withSalesAmount(prepareSalesAmount(cardSchemeType))
                .withSalesRefundCount(prepareSalesRefundCount(cardSchemeType))
                .withSalesRefundAmount(prepareSalesRefundAmount(cardSchemeType))
                .withAuthCount(prepareAuthCount(cardSchemeType))
                .withAuthAmount(prepareAuthAmount(cardSchemeType))
                .withAuthRefundCount(prepareAuthRefundCount(cardSchemeType))
                .withAuthRefundAmount(prepareAuthRefundAmount(cardSchemeType))
                .withVoidCount(prepareVoidCount(cardSchemeType))
                .withVoidAmount(prepareVoidAmount(cardSchemeType))
                .build();
    }

    private long prepareSalesCount(CardSchemeType cardScheme) {
        return this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.PURCHASE) +
                this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.CASH_ADVANCE) +
                this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.AUTH_COMPLETION) -
                this.reconciliationRepository.getCountByOriginalTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.VOID, TransactionType.PURCHASE) -
                this.reconciliationRepository.getCountByOriginalTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.VOID, TransactionType.CASH_ADVANCE);
    }

    private long prepareSalesAmount(CardSchemeType cardScheme) {
        return this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.PURCHASE) +
                this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.CASH_ADVANCE) +
                this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.AUTH_COMPLETION) -
                this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.VOID, TransactionType.PURCHASE) -
                this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.VOID, TransactionType.CASH_ADVANCE);
    }

    private long prepareSalesRefundCount(CardSchemeType cardScheme) {
        return this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.REFUND) -
                this.reconciliationRepository.getCountByOriginalTransactionTypeWithCardScheme(cardScheme,
                        this.batchNumber, TransactionType.VOID, TransactionType.REFUND);
    }

    private long prepareSalesRefundAmount(CardSchemeType cardScheme) {
        return this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme,
                this.batchNumber, TransactionType.REFUND) -
                this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardScheme,
                        this.batchNumber, TransactionType.VOID, TransactionType.REFUND);
    }

    private long prepareAuthCount(CardSchemeType cardScheme) {
        return this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardScheme,
                this.batchNumber, TransactionType.PRE_AUTH);
    }

    private long prepareAuthAmount(CardSchemeType cardScheme) {
        return this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme,
                this.batchNumber, TransactionType.PRE_AUTH);
    }

    private long prepareAuthRefundCount(CardSchemeType cardScheme) {
        return 0;
    }

    private long prepareAuthRefundAmount(CardSchemeType cardScheme) {
        return 0;
    }

    private long prepareVoidCount(CardSchemeType cardSchemeType) {
        return this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardSchemeType,
                this.batchNumber, TransactionType.VOID);
    }

    private long prepareVoidAmount(CardSchemeType cardSchemeType) {
        return this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardSchemeType,
                this.batchNumber, TransactionType.VOID, TransactionType.PURCHASE) +
                this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardSchemeType,
                        this.batchNumber, TransactionType.VOID, TransactionType.CASH_ADVANCE);
    }




}
