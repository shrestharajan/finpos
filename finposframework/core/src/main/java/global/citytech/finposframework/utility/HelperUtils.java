package global.citytech.finposframework.utility;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.BitSet;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;
import java.util.UUID;

/** User: Surajchhetry Date: 2/14/20 Time: 7:26 AM */
public final class HelperUtils {
  public static final String DATE_FORMAT_HHmmss = "HHmmss";
  public static final String DATE_FORMAT_MMdd = "MMdd";
  public static final String DATE_FORMAT_yyMMdd = "yyMMdd";
  public static final String DATE_FORMAT_yyMMddHHmmssSSS = "yyMMddHHmmssSSS";
  public static final String DATE_FORMAT_MMddHHmmss = "MMddHHmmss";
  public static final String DATE_FORMAT_yyMMddHHmmss = "yyMMddHHmmss";

  private HelperUtils() {}

  private static String parseToDoubleDigit(int digit) {
    String sValue = "";
    if (digit < 10) {
      sValue = "0" + digit;
    } else {
      sValue = String.valueOf(digit);
    }
    return sValue;
  }

  public static String getTransmissionDateAndTime() {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
            DATE_FORMAT_MMddHHmmss,
            Locale.ENGLISH
    );
    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
    return simpleDateFormat.format(new Date());
  }

  public static String getLocalTransactionTime() {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
            DATE_FORMAT_yyMMddHHmmss,
            Locale.ENGLISH
    );
    return simpleDateFormat.format(new Date());
  }

  public static String getDefaultLocaleTimeHhMmSs() {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
            DATE_FORMAT_HHmmss,
            Locale.getDefault()
    );
    return simpleDateFormat.format(new Date());
  }

  public static String getDefaultLocaleDateYyMmDd() {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
            DATE_FORMAT_yyMMdd,
            Locale.getDefault()
    );
    return simpleDateFormat.format(new Date());
  }

  public static String getDefaultLocaleDateWithoutYear() {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
            DATE_FORMAT_MMdd,
            Locale.getDefault()
    );
    return simpleDateFormat.format(new Date());
  }

  public static String toHexString(byte[] data) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < data.length; i++) {
      sb.append(Integer.toString((data[i] & 0xff) + 0x100, 16).substring(1));
    }
    return sb.toString().toUpperCase();
  }

  public static void validateRequiredSize(String data, int reqSize, String propertyName) {
    if (isBlank(data))
      throw new IllegalArgumentException(String.format("%s is empty", propertyName));
    if (reqSize < 0)
      throw new IllegalArgumentException(String.format("%s can't be negative", propertyName));
    if (data.length() != reqSize)
      throw new IllegalArgumentException(
          String.format(
              "%s has not valid length . Should be %d but found %d",
              propertyName, reqSize, data.length()));
  }

  public static void validateRequiredSize(
      String data, int minSize, int maxSize, String propertyName) {
    if (isBlank(data))
      throw new IllegalArgumentException(String.format("%s is empty", propertyName));
    if (minSize < 0)
      throw new IllegalArgumentException(String.format("%s can't be negative", propertyName));
    if (data.length() > maxSize)
      throw new IllegalArgumentException(
          String.format(
              "%s has not valid length . Should be between %d and %d  but found %d",
              propertyName, minSize, maxSize, data.length()));
  }

  public static void validateForMinimumSize(String data, int minSize, String propertyName) {
    if (isBlank(data))
      throw new IllegalArgumentException(String.format("%s is empty", propertyName));
    if (minSize < 0)
      throw new IllegalArgumentException(String.format("%s can't be negative", propertyName));
    if (data.length() < minSize)
      throw new IllegalArgumentException(
          String.format(
              "%s has not valid length . Should be minimum %d but found %d",
              propertyName, minSize, data.length()));
  }

  public static boolean isBlank(String str) {
    int strLen;
    if (str == null || (strLen = str.length()) == 0) {
      return true;
    }
    for (int i = 0; i < strLen; i++) {
      if ((Character.isWhitespace(str.charAt(i)) == false)) {
        return false;
      }
    }
    return true;
  }

  public static String bitSetToString(BitSet bitSet) {
    StringBuilder binary = new StringBuilder();
    for (int i = 0; i < bitSet.length(); i++) {
      if (bitSet.get(i)) binary.append("1");
      else binary.append("0");
    }
    return binary.toString();
  }

  /*public static String toISOAmount(double amount, int len) {
    // double newAmount = amount * 100;
    long amt = Math.round(amount * 100);
    String val = String.valueOf(amt);
    int newLen = len - val.length();
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < newLen; i++) {
      builder.append("0");
    }
    builder.append(val);
    System.out.println("STRING ::DEF::: " + builder.toString());
    return builder.toString();
  }*/

  public static String toISOAmount(double amount, int len) {
    String strAmount = String.valueOf(amount);
    // FOR SINGLE CHAR AFTER DECIMAL POINT
    int diffLength = strAmount.length() - strAmount.lastIndexOf(".");
    if (diffLength < 3) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(strAmount);
      stringBuilder.append("0");
      strAmount = stringBuilder.toString();
    }
    // REPLACE DECIMAL
    String afterReplace = strAmount.replace(".", "");
    int newLen = afterReplace.length();
    int diffLengthLoop = len - newLen;
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < diffLengthLoop; i++) {
      builder.append("0");
    }
    builder.append(afterReplace);
    return builder.toString();
  }

  public static String toISOAmount(BigDecimal amount, int len) {
    String strAmount = String.valueOf(amount);
    // FOR SINGLE CHAR AFTER DECIMAL POINT
    int diffLength = strAmount.length() - strAmount.lastIndexOf(".");
    if (diffLength < 3) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(strAmount);
      stringBuilder.append("0");
      strAmount = stringBuilder.toString();
    }
    // REPLACE DECIMAL
    String afterReplace = strAmount.replace(".", "");
    int newLen = afterReplace.length();
    int diffLengthLoop = len - newLen;
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < diffLengthLoop; i++) {
      builder.append("0");
    }
    builder.append(afterReplace);
    return builder.toString();
  }

  /*public static String toISOAmount(double amount,int len) {
    double newAmount = amount * 100;
    String val = String.valueOf((long) (newAmount));
    int newLen = len - val.length();
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < newLen; i++) {
      builder.append("0".toString());
    }
    builder.append(val);
    return builder.toString();
  }*/

  public static synchronized String generateRetrievalReferenceNumber() {
    UUID idOne = UUID.randomUUID();
    long traceAudit = idOne.getLeastSignificantBits();
    String trace = String.valueOf(traceAudit).trim();
    trace.replace(' ', '0');
    return trace.substring(1, 13);
  }

  public static double fromIsoAmount(String amount) {
    double amt = Double.parseDouble(amount);
    double actualAmount = amt;
    if (amt != 0) {
      actualAmount = (amt) / 100;
    }
    return actualAmount;
  }

  public static BigDecimal fromIsoAmountToBigDecimal(String amount) {
    BigDecimal amt = new BigDecimal(amount);
    if (!Objects.equals(amt, BigDecimal.ZERO)){
      return amt.divide(BigDecimal.valueOf(100.00), 2, RoundingMode.CEILING);
    }
    return BigDecimal.ZERO;
  }

  /*public static void main(String[] args){
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMddhhmmss");
      simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
      System.out.println(simpleDateFormat.format(new Date()));
  }

    public static String getStringValue(String str){
        if(isBlank(str))
            return "";
        return str;
    }*/
  public static String repeat(String str, int times) {
    StringBuilder builder = new StringBuilder();
    for (int count = 0; count < times; count++) {
      builder.append(str);
    }
    return builder.toString();
  }

  public static String getDateAfterConversion(
          String srcFormat,
          String srcDateInString,
          String destFormat
  ) {
    String destDateInString = "";
    try {
      DateFormat srcDateFormat = new SimpleDateFormat(srcFormat, Locale.getDefault());
      Date srcDate = srcDateFormat.parse(srcDateInString);

      DateFormat destDateFormat = new SimpleDateFormat(destFormat, Locale.getDefault());
      destDateInString = destDateFormat.format(srcDate);
      System.out.println("Converted Date is ::: " + destDateInString);
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
      destDateInString = srcDateInString;
    }
    return destDateInString;
  }

  public static String formatDateTime(String dateTime, String inputPattern, String outputPattern) {
    try {
      SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
      SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
      return outputFormat.format(inputFormat.parse(dateTime));
    } catch (ParseException pe) {
      pe.printStackTrace();
      return dateTime;
    }
  }
}
