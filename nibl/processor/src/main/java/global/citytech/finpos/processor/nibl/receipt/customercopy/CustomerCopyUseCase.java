package global.citytech.finpos.processor.nibl.receipt.customercopy;

import global.citytech.finpos.processor.nibl.transaction.receipt.NiblTransactionReceiptPrintHandler;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.customercopy.CustomerCopyRequester;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;

/**
 * Created by Unique Shakya on 9/2/2020.
 */
public class CustomerCopyUseCase implements UseCase<CustomerCopyRequestModel, CustomerCopyResponseModel>,
        CustomerCopyRequester<CustomerCopyRequestModel, CustomerCopyResponseModel> {

    @Override
    public CustomerCopyResponseModel execute(CustomerCopyRequestModel request) {
        ReceiptLog receiptLog = request.getTransactionRepository().getReceiptLog();
        NiblTransactionReceiptPrintHandler transactionReceiptHandler = new
                NiblTransactionReceiptPrintHandler(request.getPrinterService());
        PrinterResponse printerResponse = transactionReceiptHandler
                .printTransactionReceipt(receiptLog, ReceiptVersion.CUSTOMER_COPY);
        return new CustomerCopyResponseModel(
                printerResponse.getResult(),
                printerResponse.getMessage()
        );
    }
}
