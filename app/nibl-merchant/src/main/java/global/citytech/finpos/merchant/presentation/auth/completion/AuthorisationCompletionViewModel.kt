package global.citytech.finpos.merchant.presentation.auth.completion

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.finpos.merchant.data.repository.core.preauth.completion.AuthorisationCompletionRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.TransactionLog
import global.citytech.finpos.merchant.domain.model.app.mapToUseCase
import global.citytech.finpos.merchant.domain.model.auth.completion.AuthorisationCompletionResponseEntity
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.preauth.completion.AuthorisationCompletionDataSourceImpl
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.core.preauth.completion.CoreAuthorisationCompletionUseCase
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.preauth.completion.AuthorisationCompletionRequestItem
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finposframework.usecases.TransactionType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal

/**
 * Created by Unique Shakya on 9/29/2020.
 */
class AuthorisationCompletionViewModel(application: Application) :
    BaseTransactionViewModel(application) {

    val TAG = AuthorisationCompletionViewModel::class.java.name
    val validData by lazy { MutableLiveData<Boolean>() }
    val transactionPresent by lazy { MutableLiveData<Boolean>() }
    var preAuthAmount :Long= 0.00.toLong()

    private var authorisationCompletionUseCase = CoreAuthorisationCompletionUseCase(
        AuthorisationCompletionRepositoryImpl(
            AuthorisationCompletionDataSourceImpl()
        )
    )

    private var localDataUseCase =
        LocalDataUseCase(LocalRepositoryImpl(LocalDatabaseSourceImpl(), PreferenceManager))

    fun validateData(data: String) {
        if (AuthCompletionValidator.validateData(data)) {
            validData.value = true
        } else {
            isLoading.value = false
            validData.value = false
        }
    }

    fun checkForTransactionPresent(data: String) {
        isLoading.value = true
        compositeDisposable.add(
            AuthCompletionValidator.getTransactionByDataObservable(localDataUseCase, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter {
                    validateTransaction(it)
                }
                .subscribe({
                    isLoading.value = false
                    onCheckTransactionPresentNext(it)
                }, {
                    printLog(TAG, "VALIDATING PRE AUTH TRANSACTION ERROR ::: " + it.message)
                    isLoading.value = false
                    transactionPresent.value = false
                })
        )
    }

    private fun onCheckTransactionPresentNext(it: TransactionLog?) {
        if(it !=null){
            preAuthAmount = it.transactionAmount!!
            transactionPresent.value = it != null
        }
    }

    private fun validateTransaction(it: TransactionLog): Boolean {
        val transactionLog = it.mapToUseCase()
        return if (transactionLog.transactionType == TransactionType.PRE_AUTH) {
            if (transactionLog.isAuthorizationCompleted) {
                isLoading.value = false
                transactionPresent.value = false
                false
            } else {
                true
            }
        } else {
            isLoading.value = false
            transactionPresent.value = false
            false
        }
    }

    fun performAuthCompletion(
        it: ConfigurationItem?,
        amount: BigDecimal,
        originalAuthCode: String,
        purchaseType: TransactionType
    ) {
        isLoading.value = true
        compositeDisposable.add(
            authorisationCompletionUseCase.doAuthorisationCompletion(
                it!!,
                AuthorisationCompletionRequestItem(purchaseType, amount, originalAuthCode)
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    this.onAuthorisationCompletionResponse(it)
                }, {
                    this.onAuthorisationCompletionError(it)
                })
        )
    }

    private fun onAuthorisationCompletionError(it: Throwable?) {
        printLog(TAG,"PERFORM AUTH COMPLETION ERROR :::  "+ it?.message)
        it!!.printStackTrace()
        isLoading.value = false
        transactionComplete.value = false
        message.value = it.message
    }

    private fun onAuthorisationCompletionResponse(it: AuthorisationCompletionResponseEntity) {
        prepareBase64UrlToDisplayQr(
            it.isApproved!!,
            it.message!!,
            it.shouldPrintCustomerCopy!!,
            it.stan!!
        )
    }

    private fun showTransactionConfirmationDialog(it: AuthorisationCompletionResponseEntity?) {
        val transactionConfirmation = this.retrieveTransactionConfirmation(
            it!!.isApproved!!, it.message, it.shouldPrintCustomerCopy!!
        )
        transactionConfirmationData.postValue(transactionConfirmation)
    }

}