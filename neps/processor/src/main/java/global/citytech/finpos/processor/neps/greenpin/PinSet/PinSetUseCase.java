package global.citytech.finpos.processor.neps.greenpin.PinSet;

import java.math.BigDecimal;
import java.util.List;

import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.PosConditionCode;
import global.citytech.finpos.neps.iso8583.SmartVistaIccDataList;
import global.citytech.finpos.neps.iso8583.requestsender.greenpin.PinSetIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.greenpin.PinSetRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.processor.neps.NepsConstant;
import global.citytech.finpos.processor.neps.cardprocessor.ICCCardProcessor;
import global.citytech.finpos.processor.neps.cardprocessor.PICCCardProcessor;
import global.citytech.finpos.processor.neps.greenpin.OtpGeneration.OtpGenerationResponse;
import global.citytech.finpos.processor.neps.transaction.NepsInvalidResponseHandler;
import global.citytech.finpos.processor.neps.transaction.NepsIsoMessageReceiptHandler;
import global.citytech.finpos.processor.neps.transaction.TransactionExecutorTemplate;
import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finpos.processor.neps.transaction.TransactionUtils;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardSummary;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.PinBlock;
import global.citytech.finposframework.hardware.io.cards.cvm.Cvm;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat;
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest;
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandler;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.HexString;
import global.citytech.finposframework.utility.ServiceCodeUtils;
import global.citytech.finposframework.utility.StringUtils;

public class PinSetUseCase extends TransactionExecutorTemplate {

    protected final ReadCardRequest.MultipleAidSelectionListener multipleAidSelectionListener = aids -> {
        notifier.notify(Notifier.EventType.DETECTED_MULTIPLE_AID, "Processing...");
        return -1;
    };

    private String userPin = "";
    private String OTP = "";
    IsoMessageResponse isoMessageResponse;
    TransactionResponse transactionResponse;

    public PinSetUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    public void setCardResponse(ReadCardResponse readCardResponse, TransactionRequest transactionRequest, global.citytech.finpos.processor.neps.transaction.TransactionRequest request, String stan, String otp) {
        this.readCardResponse = readCardResponse;
        this.request = request;
        this.stan = stan;
        this.OTP = otp;
    }

    @Override
    public TransactionResponse execute(global.citytech.finpos.processor.neps.transaction.TransactionRequest request) {
        setUpRequiredTransactionFields(request);
        retrieveCardResponse();
        processCard();
        return transactionResponse;
    }

    @Override
    protected void setUpRequiredTransactionFields(global.citytech.finpos.processor.neps.transaction.TransactionRequest request) {
        this.request = request;
        stan = this.terminalRepository.getSystemTraceAuditNumber();
        transactionRequest = request.getTransactionRequest();
        applicationRepository = request.getApplicationRepository();

    }

    @Override
    protected void retrieveCardResponse() {
        List<CardType> allowedCardTypes = TransactionUtils
                .getAllowedCardEntryModes(transactionRequest.getTransactionType());
        ReadCardRequest readCardRequest = this.prepareReadCardRequest(
                allowedCardTypes,
                request.getApplicationRepository(),
                transactionRequest
        );
        performTransactionWithCard(readCardRequest);

    }


    private void performTransactionWithCard(ReadCardRequest readCardRequest) {
        readCardResponse = this.getCardResponse(readCardRequest, request.getReadCardService());
        this.notifier.notify(Notifier.EventType.PROCESSING, "Please wait...");
    }

    private ReadCardResponse getCardResponse(ReadCardRequest readCardRequest, ReadCardService readCardService) {
        ReadCardResponse readCardResponse = readCardService.readCardDetails(readCardRequest);
        this.readCardResponse = readCardResponse;
        this.retrieveCardProcessor();
        this.readCardResponse= this.cardProcessor.validateReadCardResponse(readCardRequest, this.readCardResponse);
        PurchaseRequest purchaseRequest = preparePurchaseRequest(
                transactionRequest,
                readCardResponse.getCardDetails()
        );
        readCardResponse = this.cardProcessor.processCard(this.stan, purchaseRequest);
        request.getReadCardService().cleanUp();
        return readCardResponse;
    }


    private void validateReadCardResponse(ReadCardResponse readCardResponse) {
        if (readCardResponse == null)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        if (readCardResponse.getResult() == Result.TIMEOUT)
            throw new PosException(PosError.DEVICE_ERROR_CARD_READ_TIMEOUT);
        if (readCardResponse.getResult() == Result.USER_CANCELLED ||
                readCardResponse.getResult() == Result.CANCEL)
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        if (readCardResponse.getResult() == Result.PROCESSING_ERROR)
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        if (readCardResponse.getResult() == Result.FAILURE)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
    }


    protected ReadCardRequest prepareReadCardRequest(
            List<CardType> allowedCardTypes,
            ApplicationRepository applicationRepository,
            global.citytech.finposframework.usecases.transaction.data.TransactionRequest
                    transactionRequest
    ) {
        ReadCardRequest readCardRequest = new ReadCardRequest(
                allowedCardTypes,
                transactionRequest.getTransactionType(),
                TransactionUtils.retrieveTransactionType9C(transactionRequest.getTransactionType()),
                applicationRepository.retrieveEmvParameterRequest()
        );
        readCardRequest.setStan(stan);
        readCardRequest.setPinpadRequired(false);
        readCardRequest.setAmount(transactionRequest.getAmount());
        readCardRequest.setCashBackAmount(transactionRequest.getAdditionalAmount());
        readCardRequest.setTransactionDate(DateUtils.yyMMddDate());
        readCardRequest.setTransactionTime(DateUtils.HHmmssTime());
        readCardRequest.setMultipleAidSelectionListener(this.multipleAidSelectionListener);
        readCardRequest.setIccDataList(SmartVistaIccDataList.get());
        readCardRequest.setCurrencyName(NepsConstant.PIN_PAD_CURRENCY);
        readCardRequest.setPinPadFixedLayout(!this.terminalRepository.shouldShufflePinPad());
        return readCardRequest;
    }

    @Override
    protected void processCard() {
        PurchaseRequest purchaseRequest = preparePurchaseRequest(
                transactionRequest,
                readCardResponse.getCardDetails()
        );
        this.notifier.notify(Notifier.EventType.PROCESSING, "Please wait...");
        PinBlock pinBlockResponse = this.retrieveInitialPinBlock(purchaseRequest);
        proceedConfirmPin(purchaseRequest, pinBlockResponse);
    }

    private void retrieveCardProcessor() {
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case ICC:
                this.cardProcessor = new ICCCardProcessor(
                        this.request.getTransactionRepository(),
                        this.request.getTransactionAuthenticator(),
                        this.request.getApplicationRepository(),
                        this.request.getReadCardService(),
                        this.notifier,
                        terminalRepository
                );
                break;
            case PICC:
                this.cardProcessor = new PICCCardProcessor(
                        this.request.getTransactionRepository(),
                        this.request.getTransactionAuthenticator(),
                        this.request.getApplicationRepository(),
                        this.request.getReadCardService(),
                        this.notifier,
                        this.request.getLedService(),
                        this.request.getSoundService(),
                        terminalRepository
                );
                break;
            default:
                break;
        }
    }

    protected PurchaseRequest preparePurchaseRequest(global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest, CardDetails cardDetails) {
        PurchaseRequest purchaseRequest = new PurchaseRequest(
                transactionRequest.getTransactionType(),
                transactionRequest.getAmount(),
                transactionRequest.getAdditionalAmount()
        );
        cardDetails.setAmount(transactionRequest.getAmount());
        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
        purchaseRequest.setCardDetails(cardDetails);
        return purchaseRequest;
    }

    void proceedConfirmPin(PurchaseRequest purchaseRequest, PinBlock pinBlock) {
        PinBlock confirmPinBlockResponse = this.retrieveConfirmPinBlock(purchaseRequest);
        logger.log("Pin 1 " + pinBlock.getPin());
        logger.log("Pin 2 " + confirmPinBlockResponse.getPin());
        if (confirmPinBlockResponse.getPin().equals(pinBlock.getPin())) {
            userPin = confirmPinBlockResponse.getPin();
            isoMessageResponse = sendIsoMessage();
            transactionResponse = checkForInvalidResponseAndProceed(isoMessageResponse);
            printDebugReceiptIfDebugMode(request.getPrinterService(), isoMessageResponse);
        } else {
            this.notifier.notify(Notifier.EventType.PIN_UNMATCH, "Please wait...");
            proceedConfirmPin(purchaseRequest, pinBlock);
        }
    }

    protected TransactionResponse checkForInvalidResponseAndProceed(IsoMessageResponse isoMessageResponse) {
        InvalidResponseHandlerResponse invalidResponseHandlerResponse = this.checkForInvalidResponseFromHost(isoMessageResponse);
        if (invalidResponseHandlerResponse.isInvalid()) {
            return this.handleInvalidResponseFromHost(invalidResponseHandlerResponse);
        } else {
            return proceedValidResponseFromHost(isoMessageResponse);
        }
    }

    protected InvalidResponseHandlerResponse checkForInvalidResponseFromHost(IsoMessageResponse isoMessageResponse) {
        InvalidResponseHandler invalidResponseHandler = new NepsInvalidResponseHandler();
        return invalidResponseHandler.isInvalidResponse(isoMessageResponse);
    }


    protected TransactionResponse handleInvalidResponseFromHost(InvalidResponseHandlerResponse invalidResponseHandlerResponse) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, invalidResponseHandlerResponse.getMessage());
        return prepareErrorResponse(invalidResponseHandlerResponse.getMessage(), false);
    }

    private TransactionResponse proceedValidResponseFromHost(IsoMessageResponse isoMessageResponse) {
        boolean transactionApproved = this.transactionApprovedByActionCode(isoMessageResponse);
        updateTransactionIds(false);
        return this.prepareResponse(isoMessageResponse, transactionApproved,
                request.getApplicationRepository(), false);
    }


    protected PinBlock retrieveInitialPinBlock(PurchaseRequest purchaseRequest) {
        PinBlock pinBlock = new PinBlock("", true);
        String pinPadAmountMessage = "";
        pinBlock = this.retrievePinBlock(
                purchaseRequest
                        .getCardDetails()
                        .getPrimaryAccountNumber(),
                pinPadAmountMessage,
                "Enter Pin"
        );
        Cvm cvm = this.retrieveCvm(
                purchaseRequest.getTransactionType(),
                purchaseRequest.getCardDetails()
        );

        ReadCardResponse readCardResponse = new ReadCardResponse(
                purchaseRequest.getCardDetails(),
                Result.SUCCESS,
                "Successful"
        );
        readCardResponse.setCvm(cvm);
        readCardResponse.getCardDetails().setPinBlock(pinBlock);
        readCardResponse
                .getCardDetails()
                .setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
        readCardResponse.setFallbackIccToMag(purchaseRequest.isFallbackIccToMag());

        return pinBlock;
    }

    private Cvm retrieveCvm(TransactionType transactionType, CardDetails cardDetails) {
        if (ServiceCodeUtils.isPinRequired(cardDetails.getTrackTwoData()))
            return Cvm.ENCIPHERED_PIN_ONLINE;
        return Cvm.SIGNATURE;
    }

    protected PinBlock retrieveConfirmPinBlock(PurchaseRequest purchaseRequest) {
        PinBlock pinBlock = new PinBlock("", true);
        String pinPadAmountMessage = "";
        pinBlock = this.retrievePinBlock(
                purchaseRequest
                        .getCardDetails()
                        .getPrimaryAccountNumber(),
                pinPadAmountMessage,
                "Confirm Pin"
        );
        return pinBlock;
    }


    private PinBlock retrievePinBlock(String primaryAccountNumber, String pinPadAmountMessage, String pinPadMessage) {
        if (!StringUtils.isEmpty(primaryAccountNumber)) {
            CardSummary cardSummary = new CardSummary(readCardResponse.getCardDetails().getCardSchemeLabel(),
                    primaryAccountNumber,
                    readCardResponse.getCardDetails().getCardHolderName(),
                    readCardResponse.getCardDetails().getExpiryDate());
            PinRequest pinRequest = new PinRequest(
                    primaryAccountNumber,
                    pinPadMessage,
                    PinBlockFormat.ISO9564_FORMAT_0,
                    4,
                    4,
                    30,
                    pinPadAmountMessage,
                    !this.terminalRepository.shouldShufflePinPad(),
                    false,
                    "Enter PIN"
            );
            pinRequest.setCardSummary(cardSummary);
            this.logger.debug("::: GREENPIN RESPONSE ::: ");
            PinResponse response = request.getTransactionAuthenticator().authenticateUser(pinRequest);
            if (response.getResult() == Result.SUCCESS)
                return response.getPinBlock();
            else if (response.getResult() == Result.USER_CANCELLED)
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            else if (response.getResult() == Result.TIMEOUT || response.getResult() == Result.FAILURE) {
                throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
            } else
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    @Override
    protected String getClassName() {
        return null;
    }

    @Override
    protected TransactionIsoRequest prepareIsoRequest(ReadCardResponse readCardResponse, TransactionRequest transactionRequest) {
        return PinSetIsoRequest.Builder
                .newInstance()
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .transactionAmount(BigDecimal.valueOf(0.00))
                .stan(stan)
                .localDateTime(DateUtils.yyMMddHHmmssDate())
                .expirationDate(readCardResponse.getCardDetails().getExpiryDate())
                .posEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
                .posConditionCode(PosConditionCode.ATTENDANT_TERMINAL.getValue())
                .track2Data(readCardResponse.getCardDetails().getTrackTwoData())
                .additionalData(
                        HexString.stringToHex("002") +
                                HexString.stringToHex("008")
                                + this.userPin + HexString.stringToHex("051") +
                                HexString.stringToHex("006") + HexString.stringToHex(OTP)
                )
                .currencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .emvData(readCardResponse.getCardDetails().getIccDataBlock())
                .build();
    }

    @Override
    protected NepsMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context) {
        return new PinSetRequestSender(context);
    }

    @Override
    protected void processOnlineResult(IsoMessageResponse isoMessageResponse) {

    }


    @Override
    protected void updateTransactionLog(boolean transactionApproved, TransactionRequest transactionRequest, TransactionRepository transactionRepository, IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse) {

    }

    @Override
    protected void updateTransactionIdsAndNotifyTransactionStatus(boolean transactionApproved, IsoMessageResponse isoMessageResponse, global.citytech.finpos.processor.neps.transaction.TransactionRequest request, ReadCardResponse readCardResponse) {

    }

    @Override
    protected TransactionResponse prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository, boolean shouldPrint) {
        return PinSetResponse.Builder.newInstance()
                .withStan(stan)
                .withApproved(transactionApproved)
                .withDebugRequestMessage(isoMessageResponse.getDebugRequestString())
                .withDebugResponseMessage(isoMessageResponse.getDebugResponseString())
                .withMessage(this.generateMessageFromResponse(isoMessageResponse)).build();
    }

    private String generateMessageFromResponse(IsoMessageResponse isoMessageResponse){
        if(isoMessageResponse == null)
            return "Invalid Response";
        var response= isoMessageResponse.getMsg().getDataElementByIndex(39);
        if (response.isEmpty())
            return "Response is missing";
        if(response.get().getValueAsInt() == 0 )
            return "Your Pin has been updated.";
        if(response.get().getValueAsInt() == 5)
            return "Transaction unsuccessful." ;
        return "System Error";
    }

    @Override
    protected String getProcessingCode() {
        return null;
    }

    @Override
    protected TransactionResponse prepareErrorResponse(String responseMessage, boolean shouldPrint) {
        return PinSetResponse.Builder.newInstance()
                .withStan(this.stan)
                .withMessage(responseMessage)
                .withApproved(false)
                .build();
    }

    @Override
    protected TransactionResponse promptToPerformIccTransaction() {
        return null;
    }
}
