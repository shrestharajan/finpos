package global.citytech.finpos.processor.nibl.transaction;

import org.junit.Before;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import global.citytech.finpos.processor.nibl.transaction.card.CardSchemeRetriever;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.CardSchemeRetrieveRequest;
import global.citytech.finposframework.repositories.CardSchemeRetrieveResponse;
import global.citytech.finposframework.repositories.FieldAttributes;
import global.citytech.finposframework.usecases.CardSchemeType;

import static org.junit.Assert.assertEquals;

/**
 * Created by Unique Shakya on 10/9/2020.
 */
public class CardSchemeRetrieverTest {

    ApplicationRepository applicationRepository;

    @Before
    public void setup() {
        applicationRepository = PowerMockito.mock(ApplicationRepository.class);
    }

    @Test
    public void retrieveCardSchemeTestForVisa() {
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("P1",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse("455708|455709|455707"));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("VC",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse("424972|424980|424090"));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("VD",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse("424656"));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("MC",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse("544123"));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("UP",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse("612333"));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("DM",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse(""));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("AX",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse(""));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("GN",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse(""));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse(""));


        global.citytech.finpos.processor.nibl.transaction.card.CardSchemeRetriever cardSchemeRetriever = new global.citytech.finpos.processor.nibl.transaction.card.CardSchemeRetriever(this.applicationRepository);
        CardSchemeType cardSchemeType = cardSchemeRetriever.retrieveCardScheme("4249720020000234");

        assertEquals(CardSchemeType.VISA_CREDIT, cardSchemeType);
    }

    @Test
    public void retrieveCardSchemeTestForMada() {
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("P1",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse("455725-455730"));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("VC",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse("424972|424980|424090"));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("VD",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse("424656"));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("MC",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse("544123"));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("UP",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse("612333"));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("DM",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse(""));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("AX",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse(""));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("GN",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse(""));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(
                new CardSchemeRetrieveRequest("",
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES)))
                .thenReturn(new CardSchemeRetrieveResponse(""));


        global.citytech.finpos.processor.nibl.transaction.card.CardSchemeRetriever cardSchemeRetriever = new CardSchemeRetriever(this.applicationRepository);
        CardSchemeType cardSchemeType = cardSchemeRetriever.retrieveCardScheme("4557280200003359");

        assertEquals(CardSchemeType.MADA, cardSchemeType);
    }
}
