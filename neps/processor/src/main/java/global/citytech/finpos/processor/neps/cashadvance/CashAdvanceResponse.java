package global.citytech.finpos.processor.neps.cashadvance;

import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finposframework.hardware.common.Result;

public class CashAdvanceResponse extends TransactionResponse {

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private boolean approved;
        private boolean shouldPrintCustomerCopy;
        private String message;
        private String stan;
        private Result result;
        private String approvalCode;

        private Builder() {
        }

        public static Builder createDefaultBuilder() {
            return new Builder();
        }

        public Builder debugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public Builder debugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public Builder approved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public Builder shouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder stan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder result(Result result) {
            this.result = result;
            return this;
        }

        public Builder approvalCode(String approvalCode) {
            this.approvalCode = approvalCode;
            return this;
        }

        public CashAdvanceResponse build() {
            CashAdvanceResponse cashAdvanceResponse = new CashAdvanceResponse();
            cashAdvanceResponse.debugRequestMessage = this.debugRequestMessage;
            cashAdvanceResponse.debugResponseMessage = this.debugResponseMessage;
            cashAdvanceResponse.approved = this.approved;
            cashAdvanceResponse.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            cashAdvanceResponse.message = this.message;
            cashAdvanceResponse.result = this.result;
            cashAdvanceResponse.approvalCode = this.approvalCode;
            cashAdvanceResponse.stan = this.stan;
            return cashAdvanceResponse;
        }
    }

}
