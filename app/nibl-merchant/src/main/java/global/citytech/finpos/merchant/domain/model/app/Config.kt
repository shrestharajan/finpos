package global.citytech.finpos.merchant.domain.model.app

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.domain.model.app.Config.Companion.TABLE_NAME
import global.citytech.finpos.merchant.presentation.model.ConfigResponse
import global.citytech.finposframework.utility.AmountUtils
import org.jetbrains.annotations.NotNull

/**
 * Created by Unique Shakya on 1/28/2021.
 */
@Entity(tableName = TABLE_NAME)
data class Config(
    @PrimaryKey
    @NotNull
    @ColumnInfo(name = COLUMN_ID)
    var id: String,
    @ColumnInfo(name = COLUMN_AMOUNT_PER_TRANSACTION)
    var amountPerTransaction: Long?,
    @ColumnInfo(name = COLUMN_RECONCILE_TIME)
    var reconcileTime: String?,
    @ColumnInfo(name = COLUMN_KEYS)
    var keys: String?,
    @ColumnInfo(name = COLUMN_ADDITIONAL_DATA)
    var additionalData: String?
) {

    companion object {
        const val TABLE_NAME = "configs"
        const val COLUMN_ID = "id"
        const val COLUMN_AMOUNT_PER_TRANSACTION = "amount_per_transaction"
        const val COLUMN_RECONCILE_TIME = "reconcile_time"
        const val COLUMN_KEYS = "keys"
        const val COLUMN_ADDITIONAL_DATA = "additional_data"
    }
}

fun ConfigResponse.mapToEntity(): Config {
    return Config(
        "1",
        getAmountInLowerCurrencyLong(amountLimitPerTransaction),
        this.reconcileTime,
        Jsons.toJsonObj(this.keys),
        ""
    )
}

fun getAmountInLowerCurrencyLong(amountInDouble: Double?): Long {
    var amountInLowerCurrencyLong = 0L
    amountInDouble?.apply {
        amountInLowerCurrencyLong = AmountUtils.toLowerCurrencyInLong(this)
    }
    return amountInLowerCurrencyLong
}