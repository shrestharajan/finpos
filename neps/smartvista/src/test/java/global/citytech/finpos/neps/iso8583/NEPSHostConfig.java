package global.citytech.finpos.neps.iso8583;

import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;

/** User: Surajchhetry Date: 2/25/20 Time: 9:19 AM */
public class NEPSHostConfig {

  /*
   Primary Connection
   ==================
  ip- 182.93.95.151
  port-9999

   Secondary Connection
   ===================
  ip- 202.166.206.197
  port-7777

   */
  public static HostInfo getPrimaryConfig() {
    return HostInfo.Builder.createDefaultBuilder("182.93.95.151", 9999)
        .timeOut(10)
        .retryLimit(3)
        .tpduString(getTPDUString())
        .addEventListener(
            (event, context) -> {
              System.out.println(context.get("MSG").toString());
            })
        .build();
  }

  public static HostInfo getSecondaryConfig() {
    return HostInfo.Builder.createDefaultBuilder("202.166.206.197", 7777)
        .timeOut(10)
        .retryLimit(3)
        .tpduString(getTPDUString())
        .addEventListener(
            (event, context) -> {
              System.out.println(context.get("MSG").toString());
            })
        .build();
  }

  public static String getTPDUString() {
    return "03030000";
  }

  public static TerminalInfo getTerminalInfo() {
    TerminalInfo terminalInfo = new TerminalInfo("41234027", "100512345678910",
            "Citytech","Kamaladi","");
    return terminalInfo;
  }
}
