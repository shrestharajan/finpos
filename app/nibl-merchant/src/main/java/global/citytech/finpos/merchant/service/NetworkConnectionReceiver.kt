
package global.citytech.finpos.merchant.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Handler
import global.citytech.common.Constants
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.utils.isConnectionAvailable
import global.citytech.finpos.merchant.utils.isConnectionAvailableWithHosts
import global.citytech.finposframework.log.Logger
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/08/06 - 10:23 AM
*/

class NetworkConnectionReceiver : BroadcastReceiver() {
    private val NETWORK_CHECK_TIME_ON_SUCCESS = 1 * 60000.toLong()
    private val NETWORK_CHECK_TIME_ON_FAILED = 5000.toLong()

    val handler = Handler()
    val runnable = Runnable {
        logger.log("Running Runnable NetworkConnectionReceiver...")
        checkConnectionWithHost()
    }

    val logger = Logger.getLogger(NetworkConnectionReceiver::class.java.name)

    private val compositeDisposable = CompositeDisposable()

    companion object {
        var networkConfiguration: ConfigurationItem? = null
        fun enable() {
            NiblMerchant.INSTANCE.registerReceiver(
                NetworkConnectionReceiver(),
                IntentFilter(Constants.INTENT_ACTION_CONNECTION_CHANGE)
            )
        }
    }

    var localDataUseCase: LocalDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    override fun onReceive(context: Context?, intent: Intent?) {
        this.logger.log(":: Network Broadcast Received ::")
        checkConnectionWithHost()
    }

    fun getConfiguration(): Boolean {
        this.logger.debug(":: GETTING APP CONFIGURATION ::")
        compositeDisposable.clear()
        return compositeDisposable.add(
            localDataUseCase.getWholeConfiguration()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe({
                    networkConfiguration = it
                    checkConnectionWithHost()
                },
                    {
                        Handler().postDelayed({ getConfiguration() }, NETWORK_CHECK_TIME_ON_SUCCESS)
                    })
        )
    }

    private fun checkConnectionWithHost() {
        if (NiblMerchant.INSTANCE.disableNetworkPing) {
            this.handler.postDelayed(runnable, NETWORK_CHECK_TIME_ON_SUCCESS)
        } else {
            if (networkConfiguration == null) {
                getConfiguration()
            } else {
                compositeDisposable.clear()
                compositeDisposable.add(
                    Observable.create(ObservableOnSubscribe<Boolean> {
                        onCheckConnectionSubscribe(it)
                    })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            NiblMerchant.INSTANCE.networkConnectionAvailable.value = it
                            this.handler.removeCallbacks(runnable)
                            if (it) {
                                this.handler.postDelayed(runnable, NETWORK_CHECK_TIME_ON_SUCCESS)
                            } else {
                                this.handler.postDelayed(runnable, NETWORK_CHECK_TIME_ON_FAILED)
                            }
                            this.logger.log(":: CHECKING CONNECTION WITH HOST :: $it")
                        }, {
                            it.printStackTrace()
                            NiblMerchant.INSTANCE.networkConnectionAvailable.value = false
                            this.handler.removeCallbacks(runnable)
                            this.handler.postDelayed(runnable, NETWORK_CHECK_TIME_ON_FAILED)
                            this.logger.log(":: CHECKING CONNECTION WITH HOST :: false")
                        })
                )
            }
        }
    }


    private fun onCheckConnectionSubscribe(emitter: ObservableEmitter<Boolean>) {
        val terminalRepository = TerminalRepositoryImpl(networkConfiguration)
        emitter.onNext(
            isConnectionAvailable() && isConnectionAvailableWithHosts(
                terminalRepository.findPrimaryHost(),
                terminalRepository.findSecondaryHost()
            )
        )
    }
}