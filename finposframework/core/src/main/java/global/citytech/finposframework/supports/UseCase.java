package global.citytech.finposframework.supports;

/** User: Surajchhetry Date: 2/28/20 Time: 6:00 PM */
@FunctionalInterface
public interface UseCase<Req extends UseCase.Request, Res extends UseCase.Response> {

  Res execute(Req request);

  interface Request {}

  interface Response {}
}