package global.citytech.finpos.processor.nibl.ministatement;

import global.citytech.finpos.processor.nibl.greenpin.GreenPinRequest;
import global.citytech.finpos.processor.nibl.transaction.TransactionRequestModel;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.io.sound.SoundService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;

public class MiniStatementRequest extends TransactionRequestModel {
    public static class Builder{
        private TransactionRepository transactionRepository;
        private ReadCardService readCardService;
        private DeviceController deviceController;
        private TransactionAuthenticator transactionAuthenticator;
        private PrinterService printerService;
        private ApplicationRepository applicationRepository;
        private global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest;
        private LedService ledService;
        private SoundService soundService;

        public static MiniStatementRequest.Builder newInstance() {
            return new MiniStatementRequest.Builder();
        }

        public MiniStatementRequest.Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public MiniStatementRequest.Builder withReadCardService(ReadCardService readCardService) {
            this.readCardService = readCardService;
            return this;
        }

        public MiniStatementRequest.Builder withDeviceController(DeviceController deviceController) {
            this.deviceController = deviceController;
            return this;
        }

        public MiniStatementRequest.Builder withTransactionAuthenticator(TransactionAuthenticator transactionAuthenticator) {
            this.transactionAuthenticator = transactionAuthenticator;
            return this;
        }

        public MiniStatementRequest.Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public MiniStatementRequest.Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public MiniStatementRequest.Builder withTransactionRequest(global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest) {
            this.transactionRequest = transactionRequest;
            return this;
        }

        public MiniStatementRequest.Builder withLedService(LedService ledService) {
            this.ledService = ledService;
            return this;
        }

        public MiniStatementRequest.Builder withSoundService(SoundService soundService) {
            this.soundService = soundService;
            return this;
        }

        public  MiniStatementRequest build(){
            MiniStatementRequest MiniStatementRequest = new MiniStatementRequest();
            MiniStatementRequest.transactionAuthenticator = this.transactionAuthenticator;
            MiniStatementRequest.transactionRepository = this.transactionRepository;
            MiniStatementRequest.readCardService = this.readCardService;
            MiniStatementRequest.deviceController = this.deviceController;
            MiniStatementRequest.transactionRequest = this.transactionRequest;
            MiniStatementRequest.printerService = this.printerService;
            MiniStatementRequest.applicationRepository = this.applicationRepository;
            MiniStatementRequest.ledService = this.ledService;
            MiniStatementRequest.soundService = this.soundService;
            return MiniStatementRequest;
        }

    }
}
