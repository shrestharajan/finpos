package global.citytech.finpos.processor.neps.transaction;

import global.citytech.finpos.processor.neps.receipt.ReceiptLabels;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.EmvTags;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.TransactionInfo;
import global.citytech.finposframework.utility.StringUtils;

public class ReversedTransactionReceiptHandler {
    private final TransactionRequest transactionRequest;
    private final ReadCardResponse readCardResponse;
    private final TerminalRepository terminalRepository;

    public ReversedTransactionReceiptHandler(TransactionRequest transactionRequest, ReadCardResponse readCardResponse, TerminalRepository terminalRepository) {
        this.transactionRequest = transactionRequest;
        this.readCardResponse = readCardResponse;
        this.terminalRepository = terminalRepository;
    }

    public ReceiptLog prepare(String batchNumber, String stan, String invoiceNumber, ReasonForReversal reasonForReversal) {
        return new ReceiptLog.Builder()
                .withRetailer(prepareRetailerInfo())
                .withPerformance(preparePerformanceInfo())
                .withTransaction(prepareTransactionInfo(batchNumber, stan, invoiceNumber, reasonForReversal))
                .withEmvTags(prepareEmvTags())
                .build();
    }

    private EmvTags prepareEmvTags() {
        return EmvTags.Builder.defaultBuilder()
                .withAc(this.getFromTagCollection(IccData.APPLICATION_CRYPTOGRAM.getTag()))
                .withResponseCode("")
                .withAid(this.getFromTagCollection(IccData.DF_NAME.getTag()))
                .withCardType(this.readCardResponse.getCardDetails().getCardType().getPosEntryMode())
                .withCid(this.getFromTagCollection(IccData.CID.getTag()))
                .withCvm(this.getFromTagCollection(IccData.CVM.getTag()))
                .withKernelId(this.getFromTagCollection(IccData.KERNEL_ID.getTag()))
                .withTvr(this.getFromTagCollection(IccData.TVR.getTag()))
                .withTsi(this.getFromTagCollection(IccData.TSI.getTag()))
                .withIin(this.getFromTagCollection(IccData.IIN.getTag()))
                .build();
    }

    private String getFromTagCollection(int tag) {
        try {
            return this.readCardResponse.getCardDetails().getTagCollection().get(tag).getData();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private TransactionInfo prepareTransactionInfo(String batchNumber, String stan, String invoiceNumber, ReasonForReversal reasonForReversal) {
        return new TransactionInfo.Builder()
                .withApprovalCode("")
                .withBatchNumber(batchNumber)
                .withTransactionCurrencyName(terminalRepository.getTerminalTransactionCurrencyName())
                .withCardNumber(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .withCardHolderName(readCardResponse.getCardDetails().getCardHolderName())
                .withCardScheme(readCardResponse.getCardDetails().getCardSchemeLabel())
                .withExpireDate(readCardResponse.getCardDetails().getExpiryDate())
                .withPurchaseAmount(StringUtils.formatAmountTwoDecimal(readCardResponse.getCardDetails().getAmount()))
                .withPurchaseType(this.transactionRequest.getTransactionType().getPrintName())
                .withRrn("")
                .withStan(stan)
                .withInvoiceNumber(invoiceNumber)
                .withTransactionMessage(reasonForReversal.getMessage())
                .withTransactionStatus("DECLINED")
                .build();
    }

    private Performance preparePerformanceInfo() {
        return new Performance.Builder()
                .withStartDateTime(
                        this.readCardResponse
                                .getCardDetails()
                                .getTransactionInitializeDateTime()
                )
                .build();
    }

    private Retailer prepareRetailerInfo() {
        return new Retailer.Builder()
                .withRetailerLogo(this.terminalRepository.findTerminalInfo().getMerchantPrintLogo())
                .withRetailerName(this.terminalRepository.findTerminalInfo().getMerchantName())
                .withRetailerAddress(this.terminalRepository.findTerminalInfo().getMerchantAddress())
                .withMerchantId(this.terminalRepository.findTerminalInfo().getMerchantID())
                .withTerminalId(this.terminalRepository.findTerminalInfo().getTerminalID())
                .build();
    }
}
