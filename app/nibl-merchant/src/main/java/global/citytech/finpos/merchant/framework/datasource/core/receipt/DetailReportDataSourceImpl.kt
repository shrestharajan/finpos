package global.citytech.finpos.merchant.framework.datasource.core.receipt

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.receipt.DetailReportDataSource
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.ReconciliationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.domain.model.receipt.detailreport.DetailReportRequestEntity
import global.citytech.finpos.merchant.domain.model.receipt.detailreport.DetailReportResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.presentation.data.mapToDetailReportResponseUIModel
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finposframework.switches.ProcessorManager
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 1/4/2021.
 */
class DetailReportDataSourceImpl : DetailReportDataSource {
    override fun printDetailReport(configurationItem: ConfigurationItem): Observable<DetailReportResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val transactionRepository =
            TransactionRepositoryImpl(
                NiblMerchant.INSTANCE
            )
        val printerService = PrinterSourceImpl(NiblMerchant.getActivityContext())
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
        val reconciliationRepository = ReconciliationRepositoryImpl()
        val detailReportRequestEntity = DetailReportRequestEntity(
            printerService = printerService,
            applicationRepository = applicationRepository,
            reconciliationRepository = reconciliationRepository,
            transactionRepository = transactionRepository,
            printSummaryReport = true
        )
        val detailReportRequester =
            ProcessorManager.getInterface(terminalRepository, NotificationHandler)
                .detailReportRequester
        return Observable.fromCallable {
            (detailReportRequester.execute(detailReportRequestEntity.mapToModel())).mapToDetailReportResponseUIModel()
        }
    }
}