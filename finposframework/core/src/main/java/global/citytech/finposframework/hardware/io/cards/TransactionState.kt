package global.citytech.finposframework.hardware.io.cards

/**
 * Created by Rishav Chudal on 6/8/20.
 */
enum class TransactionState {
    ONLINE_APPROVED,
    ONLINE_DECLINED,
    OFFLINE_APPROVED,
    OFFLINE_DECLINED,
    ACCEPTED
}