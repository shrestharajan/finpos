package global.citytech.neps.presentation.model.transaction

import global.citytech.posswitchintregator.card.data.PurchaseType

data class PurchaseRequestItem(
    val amount: Double? = null,
    val transactionType: PurchaseType? = null
)