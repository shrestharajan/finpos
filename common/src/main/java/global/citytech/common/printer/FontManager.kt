package global.citytech.common.printer

import android.content.res.AssetManager
import android.graphics.Typeface
import global.citytech.common.Constants.ARIAL_FONT_SIZE_LARGE
import global.citytech.common.Constants.ARIAL_FONT_SIZE_NORMAL
import global.citytech.common.Constants.ARIAL_FONT_SIZE_SMALL
import global.citytech.common.Constants.ARIAL_FONT_SIZE_XLARGE
import global.citytech.common.Constants.ARIAL_FONT_SIZE_XSMALL
import global.citytech.common.Constants.MONOSPACE_FONT_SIZE_LARGE
import global.citytech.common.Constants.MONOSPACE_FONT_SIZE_NORMAL
import global.citytech.common.Constants.MONOSPACE_FONT_SIZE_SMALL
import global.citytech.common.Constants.MONOSPACE_FONT_SIZE_XLARGE
import global.citytech.common.Constants.MONOSPACE_FONT_SIZE_XSMALL
import global.citytech.finposframework.hardware.io.printer.Style.FontType
import java.util.*

/**
 * Created by Rishav Chudal on 6/19/20.
 */
class FontManager {
    companion object {
        private var normalTypeFace: Typeface? = null
        private var boldTypeFace: Typeface? = null

        fun setArialFontAsCustomTypeFaceFromAssets(assetManager: AssetManager?) {
            if (normalTypeFace == null) { //normalTypeFace = Typeface.createFromAsset(assetManager, String.format(Locale.US, "%s", "arial-mt-normal.otf"));
                normalTypeFace = Typeface.createFromAsset(
                    assetManager, String.format(
                        Locale.US,
                        "%s", "nexa-book.ttf"
                    )
                )
                boldTypeFace = Typeface.createFromAsset(
                    assetManager, String.format(
                        Locale.US,
                        "%s", "nexa-bold.ttf"
                    )
                )
//                boldTypeFace = Typeface.createFromAsset(
//                    assetManager, String.format(
//                        Locale.US,
//                        "%s", "arial-condensed-bold.ttf"
//                    )
//                )
            }
        }

        @JvmStatic
        fun getTypeFace(fontType: FontType?, isBold: Boolean): Typeface {
            return when (fontType) {
                FontType.MONOSPACE -> Typeface.MONOSPACE
                else -> if (isBold) {
                    if (boldTypeFace == null) Typeface.DEFAULT_BOLD else boldTypeFace!!
                } else {
                    if (normalTypeFace == null) Typeface.DEFAULT else normalTypeFace!!
                }
            }
        }

        @JvmStatic
        fun getTextSize(style: global.citytech.finposframework.hardware.io.printer.Style): Int {
            return when (style.fontType) {
                FontType.MONOSPACE -> getMonospaceFontSize(
                    style.getFontSize()
                )
                else -> getArialFontSize(style.getFontSize())
            }
        }

        private fun getArialFontSize(fontSize: global.citytech.finposframework.hardware.io.printer.Style.FontSize?): Int {
            return when (fontSize) {
                global.citytech.finposframework.hardware.io.printer.Style.FontSize.SMALL -> ARIAL_FONT_SIZE_SMALL
                global.citytech.finposframework.hardware.io.printer.Style.FontSize.XSMALL -> ARIAL_FONT_SIZE_XSMALL
                global.citytech.finposframework.hardware.io.printer.Style.FontSize.LARGE -> ARIAL_FONT_SIZE_LARGE
                global.citytech.finposframework.hardware.io.printer.Style.FontSize.XLARGE -> ARIAL_FONT_SIZE_XLARGE
                else -> ARIAL_FONT_SIZE_NORMAL
            }
        }

        private fun getMonospaceFontSize(fontSize: global.citytech.finposframework.hardware.io.printer.Style.FontSize?): Int {
            return when (fontSize) {
                global.citytech.finposframework.hardware.io.printer.Style.FontSize.SMALL -> MONOSPACE_FONT_SIZE_SMALL
                global.citytech.finposframework.hardware.io.printer.Style.FontSize.XSMALL -> MONOSPACE_FONT_SIZE_XSMALL
                global.citytech.finposframework.hardware.io.printer.Style.FontSize.LARGE -> MONOSPACE_FONT_SIZE_LARGE
                global.citytech.finposframework.hardware.io.printer.Style.FontSize.XLARGE -> MONOSPACE_FONT_SIZE_XLARGE
                else -> MONOSPACE_FONT_SIZE_NORMAL
            }
        }
    }
}