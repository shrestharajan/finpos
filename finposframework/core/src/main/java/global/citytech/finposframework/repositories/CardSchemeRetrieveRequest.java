package global.citytech.finposframework.repositories;

public class CardSchemeRetrieveRequest {

    private String cardSchemeType;
    private FieldAttributes fieldAttributes;

    public CardSchemeRetrieveRequest(String cardSchemeType, FieldAttributes fieldAttributes) {
        this.cardSchemeType = cardSchemeType;
        this.fieldAttributes = fieldAttributes;
    }

    public String getCardSchemeType() {
        return cardSchemeType;
    }

    public FieldAttributes getFieldAttributes() {
        return fieldAttributes;
    }

    @Override
    public String toString() {
        return "========= CARD SCHEME RETRIEVE REQUEST ===== \n" +
                "CARD SCHEME TYPE :::: " + this.cardSchemeType +
                "FIELD ATTRIBUTES :::: " + this.fieldAttributes;
    }


    @Override
    public boolean equals( Object obj) {
        if (obj == null)
            return false;
        if (((CardSchemeRetrieveRequest) obj).getCardSchemeType().equals(this.cardSchemeType))
            return ((CardSchemeRetrieveRequest) obj).getFieldAttributes() == this.fieldAttributes;
        return false;
    }
}
