package global.citytech.finpos.merchant.presentation.settlement

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import global.citytech.finpos.merchant.R
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.utility.StringUtils
import kotlinx.android.synthetic.main.dialog_settlement_confirmation.*

/**
 * Created by Unique Shakya on 8/4/2021.
 */
class SettlementConfirmationDialog : DialogFragment() {

    private val logger = Logger.getLogger(SettlementConfirmationDialog::class.java.name)

    private lateinit var settlementConfirmationTotals: SettlementConfirmationTotals
    private lateinit var listener: Listener

    private val zeroLong = 0.toLong()

    companion object {
        const val TAG = "SettlementConfirmationDialog"
        const val BUNDLE_DATA_SETTLEMENT_CONFIRMATION_TOTALS = "settlement confirmation totals"

        fun newInstance(settlementConfirmationTotals: SettlementConfirmationTotals): SettlementConfirmationDialog {
            val settlementConfirmationDialog = SettlementConfirmationDialog()
            val bundle = Bundle()
            bundle.putParcelable(
                BUNDLE_DATA_SETTLEMENT_CONFIRMATION_TOTALS,
                settlementConfirmationTotals
            )
            settlementConfirmationDialog.arguments = bundle
            return settlementConfirmationDialog
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_settlement_confirmation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBundleData()
    }

    override fun onResume() {
        super.onResume()
        setAttributesToViews()
        handleButtonClicks()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as Listener
        } catch (e: ClassCastException) {
            logger.log("Activity should implement settlement confirmation dialog listener")
        }
    }

    private fun handleButtonClicks() {
        btn_confirm.setOnClickListener { listener.onConfirmButtonClicked() }
        btn_cancel.setOnClickListener { listener.onCancelButtonClicked() }
    }

    private fun setAttributesToViews() {
        setAmountLabelWithCurrencyName()
        setSalesValues()
        setRefundValues()
        setVoidValues()
        setAuthValues()
        setTotalValues()
    }

    private fun setAmountLabelWithCurrencyName() {
        tv_amount_label.text =
            getString(R.string.title_amount).plus(" (${settlementConfirmationTotals.currencyName})")
    }

    private fun setTotalValues() {
        if (settlementConfirmationTotals.totalCount.toLong() == zeroLong &&
            settlementConfirmationTotals.totalAmount.toLong() == zeroLong) {
            tv_totals_label.visibility = View.GONE
            tv_total_count.visibility = View.GONE
            tv_total_amount.visibility = View.GONE
            divider_totals.visibility = View.GONE
        } else {
            tv_totals_label.visibility = View.VISIBLE
            tv_total_count.visibility = View.VISIBLE
            tv_total_amount.visibility = View.VISIBLE
            tv_total_count.text = settlementConfirmationTotals.totalCount
            tv_total_amount.text =
                StringUtils.formatAmountTwoDecimal(settlementConfirmationTotals.totalAmount.toDouble()/100)
        }
    }

    private fun setVoidValues() {
        if (settlementConfirmationTotals.voidCount.toLong() == zeroLong &&
            settlementConfirmationTotals.voidAmount.toLong() == zeroLong) {
            tv_void_label.visibility = View.GONE
            tv_void_count.visibility = View.GONE
            tv_void_amount.visibility = View.GONE
        } else {
            tv_void_label.visibility = View.VISIBLE
            tv_void_count.visibility = View.VISIBLE
            tv_void_amount.visibility = View.VISIBLE
            tv_void_count.text = settlementConfirmationTotals.voidCount
            tv_void_amount.text =
                StringUtils.formatAmountTwoDecimal(settlementConfirmationTotals.voidAmount.toDouble()/100)
        }
    }

    private fun setAuthValues() {
        if (settlementConfirmationTotals.authCount.toLong() == zeroLong &&
            settlementConfirmationTotals.authAmount.toLong() == zeroLong) {
            tv_auth_label.visibility = View.GONE
            tv_auth_count.visibility = View.GONE
            tv_auth_amount.visibility = View.GONE
        } else {
            tv_auth_label.visibility = View.VISIBLE
            tv_auth_count.visibility = View.VISIBLE
            tv_auth_amount.visibility = View.VISIBLE
            tv_auth_count.text = settlementConfirmationTotals.authCount
            tv_auth_amount.text =
                StringUtils.formatAmountTwoDecimal(settlementConfirmationTotals.authAmount.toDouble()/100)
        }
    }

    private fun setRefundValues() {
        if (settlementConfirmationTotals.refundCount.toLong() == zeroLong &&
            settlementConfirmationTotals.refundAmount.toLong() == zeroLong) {
            tv_refund_label.visibility = View.GONE
            tv_refund_count.visibility = View.GONE
            tv_refund_amount.visibility = View.GONE
        } else {
            tv_refund_label.visibility = View.VISIBLE
            tv_refund_count.visibility = View.VISIBLE
            tv_refund_amount.visibility = View.VISIBLE
            tv_refund_count.text = settlementConfirmationTotals.refundCount
            tv_refund_amount.text =
                StringUtils.formatAmountTwoDecimal(settlementConfirmationTotals.refundAmount.toDouble()/100)
        }
    }

    private fun setSalesValues() {
        if (settlementConfirmationTotals.salesCount.toLong() == zeroLong &&
            settlementConfirmationTotals.salesAmount.toLong() == zeroLong) {
            tv_sales_label.visibility = View.GONE
            tv_sales_count.visibility = View.GONE
            tv_sales_amount.visibility = View.GONE
        } else {
            tv_sales_label.visibility = View.VISIBLE
            tv_sales_count.visibility = View.VISIBLE
            tv_sales_amount.visibility = View.VISIBLE
            tv_sales_count.text = settlementConfirmationTotals.salesCount
            tv_sales_amount.text =
                StringUtils.formatAmountTwoDecimal(settlementConfirmationTotals.salesAmount.toDouble()/100)
        }
    }

    private fun getBundleData() {
        arguments?.let {
            settlementConfirmationTotals = it.getParcelable(
                BUNDLE_DATA_SETTLEMENT_CONFIRMATION_TOTALS
            )!!
        }
    }

    interface Listener {
        fun onConfirmButtonClicked()
        fun onCancelButtonClicked()
    }
}