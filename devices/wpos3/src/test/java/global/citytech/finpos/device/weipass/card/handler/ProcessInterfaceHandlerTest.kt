package global.citytech.finpos.device.weipass.card.handler

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.wiseasy.emvprocess.bean.PINSetting
import com.wiseasy.emvprocess.bean.TransAmount
import global.citytech.common.data.PosCallback
import global.citytech.finpos.device.weipass.led.WeipassLedServiceImpl
import global.citytech.finpos.device.weipass.utils.*
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import global.citytech.finposframework.hardware.io.cards.CardDetails
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest
import io.mockk.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException
import java.lang.ref.WeakReference

/**
 * Created by Rishav Chudal on 6/4/20.
 */
class ProcessInterfaceHandlerTest {
    private lateinit var SUT: ProcessInterfaceHandler
    private lateinit var mockkedWeipassLedServiceImpl: WeipassLedServiceImpl
    private lateinit var mockkedIntent: Intent
    private lateinit var mockkedLocalBroadcastManager: LocalBroadcastManager
    private lateinit var mockkedContext: Context
    private lateinit var contextWeakReference: WeakReference<Context>
    private lateinit var mockkedPosCallback: PosCallback
    private lateinit var mockkedReadCardRequest: ReadCardRequest
    private lateinit var mockkedCardDetails: CardDetails

    @Rule
    @JvmField
    var expectedException = ExpectedException.none()

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        SUT = ProcessInterfaceHandler()
        mockkWeipassLogger()
        mockkEmvUtils()
        mockkWeipassStateHandler()
        mockTextUtils()
        mockDeviceUtils()
        mockkWeipassLedServiceImpl()
        mockkIntent()
        mockkLocalBroadcastManager()
        mockkContext()
        mockkPosCallback()
    }

    @Test
    fun ConfigTransTPK_OnMethodCall_ReturnsPINSetting() {
        //Setup
        val readCardRequest =
            ReadCardRequest(
                emptyList(),
                "00",
                prepareEmvParametersRequest()
            )
        readCardRequest.packageName = "Test Package"
        WeipassStateHandler.readCardRequest = readCardRequest
        every { EmvUtils.retrievePAN() } returns "1234567890"

        //Run
        val result = SUT.ConfigTransTPK()

        //Verify
        assertTrue(result is PINSetting)
        assertTrue(result != null)
        assertEquals( "Test Package", result.packageName)
        assertFalse(result.isBypass)
        assertEquals(WeipassConstant.MINM_PIN_LENGTH_DEFAULT, result.minPINLength)
        assertEquals(WeipassConstant.MAXM_PIN_LENGTH_DEFAULT, result.maxPINLength)
        assertEquals(WeipassConstant.DEFAULT_PINPAD_TIMEOUT, result.timeOut)
        assertFalse(result.isDUKPT)
        assertFalse(result.isClearButtonNotCannel)
        assertFalse(result.isAutoComfirm)
        assertFalse(result.isFixLayout)
        assertEquals("1234567890", result.pan)
    }

    @Test
    fun setTransAmount_OnMethodCall_ReturnsTransAmount() {
        //Setup
        every { WeipassStateHandler.initKernelCountDownLatch() } just Runs
        every { WeipassStateHandler.releaseServiceCountdownLatch() } just Runs
        every { WeipassStateHandler.awaitKernelCountDownLatch() } just Runs
        every { WeipassDeviceUtils.prepareTransAmount(any(), any(), any()) } returns prepareTransAmount()

        //Run
        val result = SUT.setTransAmount()

        //Verify
        assertTrue(result != null)
        assertTrue(result is TransAmount)
        verify { WeipassStateHandler.initKernelCountDownLatch() }
        assertEquals(Result.CALLBACK_AMOUNT, WeipassStateHandler.readCardResponse!!.result)
        assertEquals(WeipassConstant.MSG_CALLBACK_AMOUNT, WeipassStateHandler.readCardResponse!!.message)
        verify { WeipassStateHandler.releaseServiceCountdownLatch() }
        verify { WeipassStateHandler.awaitKernelCountDownLatch() }
    }

    @Test
    fun processOnline_OnMethodCall_PreparesReadCardResponseForOnlineProcssAndGoesOnlineAndReturnsOnlineResult() {
        //TODO mock Intent
    }

    @Test
    fun processAdvice_OnMethodCall_WriteLogs() {
        //Setup
        val byteArray = byteArrayOf(1,2)

        //Run
        SUT.processAdvice(byteArray)

        //Verify
        verify { WeipassLogger.log("ProcessInterfaceHandler ::: processAdvice") }
    }

    @Test
    fun displayOfflinePinVerifyResult_OnMethodCall_WriteLogs() {
        //Setup
        val byteArray = byteArrayOf(1,2)

        //Run
        SUT.displayOfflinePinVerifyResult()

        //Verify
        verify { WeipassLogger.log("ProcessInterfaceHandler ::: displayOfflinePinVerifyResult") }
        verify { WeipassLogger.log("ProcessInterfaceHandler ::: Pin Offline Verified") }
    }

    @Test
    fun displayPan_WhenPanIsNullBlankOrEmtpy_ThrowsExceptionInvalidCard() {
        //Setup
        contextWeakReference = WeakReference(mockkedContext)
        every { mockkedContext.applicationContext } returns mockkedContext
        every { WeipassStateHandler.contextWeakReference } returns contextWeakReference
        every { EmvUtils.getOutComeDF8129Msg() } returns "12345"
        every { WeipassDeviceUtils.callForSdkBreakOffCommand(any()) } just Runs

        //Run
        try {
            SUT.displayPan("")
        } catch (ex: Exception) {

            //Verify
            verify { WeipassLogger.log("ProcessInterfaceHandler ::: displayPan") }
            assertEquals(WeipassTransState.FAILURE, WeipassStateHandler.transState)
            verify { WeipassDeviceUtils.callForSdkBreakOffCommand(any()) }
            assertEquals(WeipassConstant.INVALID_CARD, ex.message)
        }
    }

    @Test
    fun displayPan_WhenPanIsNotNullBlankOrEmtpyButDifferentCardUsed_ThrowsExceptionInvalidCard() {
        //TODO mock the extension function
    }

    @Test
    fun displayPan_WhenPanIsNullBlankOrEmtpyAndUsedSameCard_CardIsValidatedAndLedGlowsAndSoundPlays() {
        //TODO mock the extension function
    }

    @Test
    fun doAfterCardDataReading_OnMethodCallAndEmvProcessIsHalt_Returns1() {
        //Setup
        contextWeakReference = WeakReference(mockkedContext)
        mockkReadCardRequest()
        every { WeipassStateHandler.contextWeakReference } returns contextWeakReference
        every { WeipassStateHandler.initKernelCountDownLatch() } just Runs
        every { WeipassStateHandler.CURRENT_CARD_TYPE } returns CardType.ICC
        every { WeipassStateHandler.readCardRequest } returns mockkedReadCardRequest
        every { mockkedReadCardRequest.amount } returns 900.00
        every { mockkedReadCardRequest.cashBackAmount } returns 90.00
        every { EmvUtils.prepareBasicEmvDataForCardDetails(any()) } just Runs
        every { WeipassStateHandler.haltEmvProcess } returns true

        //Run
        val result = SUT.doAfterCardDataReading()

        //Verify
        verify { WeipassStateHandler.initKernelCountDownLatch() }

        assertEquals(CardType.ICC, WeipassStateHandler.readCardResponse!!.cardDetails!!.cardType)
        assertEquals(900.00, WeipassStateHandler.readCardResponse!!.cardDetails!!.amount)
        assertEquals(90.00, WeipassStateHandler.readCardResponse!!.cardDetails!!.cashBackAmount)
        assertEquals(Result.SUCCESS, WeipassStateHandler.readCardResponse!!.result)
        assertEquals(WeipassConstant.MSG_READ_CARD_SUCCESS, WeipassStateHandler.readCardResponse!!.message)

        verify { WeipassStateHandler.releaseServiceCountdownLatch() }

        verify { WeipassStateHandler.awaitKernelCountDownLatch() }

        assertEquals(1, result)
        clearAllMocks()
    }

    @Test
    fun doAfterCardDataReading_OnMethodCallAndEmvProcessIsNotHalt_Returns0() {
        //Setup
        contextWeakReference = WeakReference(mockkedContext)
        mockkReadCardRequest()
        every { WeipassStateHandler.contextWeakReference } returns contextWeakReference
        every { WeipassStateHandler.initKernelCountDownLatch() } just Runs
        every { WeipassStateHandler.CURRENT_CARD_TYPE } returns CardType.ICC
        every { WeipassStateHandler.readCardRequest } returns mockkedReadCardRequest
        every { mockkedReadCardRequest.amount } returns 900.00
        every { mockkedReadCardRequest.cashBackAmount } returns 90.00
        every { EmvUtils.prepareBasicEmvDataForCardDetails(any()) } just Runs
        every { WeipassStateHandler.haltEmvProcess } returns false

        //Run
        val result = SUT.doAfterCardDataReading()

        //Verify
        verify { WeipassStateHandler.initKernelCountDownLatch() }

        assertEquals(CardType.ICC, WeipassStateHandler.readCardResponse!!.cardDetails!!.cardType)
        assertEquals(900.00, WeipassStateHandler.readCardResponse!!.cardDetails!!.amount)
        assertEquals(90.00, WeipassStateHandler.readCardResponse!!.cardDetails!!.cashBackAmount)
        assertEquals(Result.SUCCESS, WeipassStateHandler.readCardResponse!!.result)
        assertEquals(WeipassConstant.MSG_READ_CARD_SUCCESS, WeipassStateHandler.readCardResponse!!.message)

        verify { WeipassStateHandler.releaseServiceCountdownLatch() }

        verify { WeipassStateHandler.awaitKernelCountDownLatch() }

        assertEquals(0, result)
    }


    private fun mockkWeipassLogger() {
        mockkObject(WeipassLogger)
        mockkObject(WeipassLogger.Companion)
        every { WeipassLogger.log(any()) } just Runs
    }

    private fun mockkEmvUtils() {
        mockkObject(EmvUtils)
        mockkObject(EmvUtils.Companion)
    }

    private fun mockkWeipassStateHandler() {
        mockkObject(WeipassStateHandler)
    }

    private fun prepareEmvParametersRequest() =
        EmvParametersRequest(
            "0682",
            "0682",
            "01",
            "E0F0C8",
            "22",
            "7399",
            "06804344",
            "800150400566",
            "Bhat Bhateni Super Market",
            "35E04000",
            "D000F0A000",
            "01"
        )

    private fun prepareTransAmount(): TransAmount {
        val transAmount = TransAmount()
        transAmount.amount = 90000
        transAmount.otherAmount = 9000
        return transAmount
    }

    private fun mockTextUtils() {
        mockkStatic(TextUtils::class)
        every { TextUtils.isEmpty(any()) } returns false
    }

    private fun mockDeviceUtils() {
        mockkObject(WeipassDeviceUtils)
        mockkObject(WeipassDeviceUtils.Companion)
    }

    private fun mockkWeipassLedServiceImpl() {
        mockkedWeipassLedServiceImpl = mockk()
    }

    private fun mockkIntent() {
        mockkedIntent = spyk()
    }

    private fun mockkLocalBroadcastManager() {
        mockkedLocalBroadcastManager = mockk()
    }

    private fun mockkContext() {
        mockkedContext = mockk()
    }

    private fun mockkPosCallback() {
        mockkedPosCallback = mockk()
    }

    private fun mockkReadCardRequest() {
        mockkedReadCardRequest = mockk()
    }

    private fun mockkCardDetails() {
        mockkedCardDetails = mockk()
    }
}