package global.citytech.finpos.merchant.domain.repository.core.configuration

import global.citytech.finpos.merchant.domain.model.logon.LogonResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import io.reactivex.Observable

interface ConfigurationRepository {
    fun logOn(configurationItem: ConfigurationItem): Observable<LogonResponseEntity>
}