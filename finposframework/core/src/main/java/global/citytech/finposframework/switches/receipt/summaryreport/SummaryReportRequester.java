package global.citytech.finposframework.switches.receipt.summaryreport;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 1/5/2021.
 */
public interface SummaryReportRequester<T extends SummaryReportRequestParameter,
        E extends SummaryReportResponseParameter> extends Request<T, E> {
}
