package global.citytech.finposframework.hardware.io.cards.read

import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.CardDetails
import global.citytech.finposframework.hardware.io.cards.cvm.PICCCvm
import global.citytech.finposframework.hardware.io.cards.cvm.Cvm

/**
 * Created by Rishav Chudal on 6/8/20.
 */
class ReadCardResponse(
    val cardDetails: CardDetails?,
    val result: Result,
    val message: String
) {
    var piccCvm: PICCCvm? = PICCCvm.NO_CVM
    var cvm: Cvm? = Cvm.NO_CVM
    var fallbackIccToMag: Boolean? = false


    override fun toString(): String {
        return "ReadCardResponse(cardDetails=${cardDetails.toString()}, result=$result, message='$message', contactlessCVM=$piccCvm, fallbackIccToMag=$fallbackIccToMag)"
    }
}