package global.citytech.finpos.processor.nibl.greenpin;

import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;

public class GreenPinResponse extends TransactionResponseModel {
    private String debugRequestMessage;
    private String debugResponseMessage;
    private String message;
    private String stan;
    private boolean approved;
    private boolean shouldPrintCustomerCopy;


    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }


    public String getMessage() {
        return message;
    }

    public String getStan() {
        return stan;
    }


    public boolean isApproved() {
        return approved;
    }

    public boolean isShouldPrintCustomerCopy() {
        return shouldPrintCustomerCopy;
    }

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private String message;
        private String stan;
        private boolean approved;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withDebugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public Builder withDebugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public Builder withShouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }
        public Builder withApproved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }


        public GreenPinResponse build() {
            GreenPinResponse GreenPinResponse = new GreenPinResponse();
            GreenPinResponse.debugRequestMessage = this.debugRequestMessage;
            GreenPinResponse.debugResponseMessage = this.debugResponseMessage;
            GreenPinResponse.approved = this.approved;
            GreenPinResponse.message = this.message;
            GreenPinResponse.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            GreenPinResponse.stan = this.stan;
            return GreenPinResponse;
        }
    }


}
