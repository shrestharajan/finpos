package global.citytech.finposframework.hardware.io.sound

import global.citytech.finposframework.hardware.common.DeviceService

/**
 * Created by Rishav Chudal on 6/7/20.
 */
interface SoundService: DeviceService {
    fun playSound(sound: Sound?)
}