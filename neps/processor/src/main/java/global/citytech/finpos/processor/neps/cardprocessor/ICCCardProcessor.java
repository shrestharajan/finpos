package global.citytech.finpos.processor.neps.cardprocessor;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finpos.processor.neps.transaction.TransactionSetTableVerifier;
import global.citytech.finpos.processor.neps.transaction.TransactionUtils;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.cards.tvr.TvrResults;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.StringUtils;
import global.citytech.finposframework.utility.TvrResultParser;

/**
 * Created by Saurav Ghimire on 1/21/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class ICCCardProcessor extends ChipCardProcessor {

    public ICCCardProcessor(
            TransactionRepository transactionRepository,
            TransactionAuthenticator transactionAuthenticator,
            ApplicationRepository applicationRepository,
            ReadCardService readCardService,
            Notifier notifier,
            TerminalRepository terminalRepository
    ) {
        super(
                transactionRepository,
                transactionAuthenticator,
                applicationRepository,
                readCardService,
                notifier,
                terminalRepository
        );
    }

    @Override
    protected void validateByRespectiveProcessor(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse) {
        super.validateByRespectiveProcessor(readCardRequest,readCardResponse);
        readCardResponse.getResult();// TODO implement amount callback notify UI and HW SDK
        if (readCardResponse.getResult() == Result.FAILURE || readCardResponse.getCardDetails() == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (StringUtils.isEmpty(readCardResponse.getCardDetails().getPrimaryAccountNumber()) ||
                StringUtils.isEmpty(readCardResponse.getCardDetails().getTrackTwoData())) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }

    }

    @Override
    protected ReadCardResponse retryByProcessor(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse) {
        return null;
    }

    @Override
    protected boolean checkForRetry(ReadCardResponse readCardResponse) {
        return false;
    }


    @Override
    protected ReadCardResponse readCardFromFallbackProcessor(ReadCardRequest readCardRequest) {
        return this.handleContactToMagneticFallbackCases(readCardRequest);
    }

    @Override
    protected boolean checkFallback(ReadCardResponse readCardResponse) {
        return readCardResponse.getResult() == Result.ICC_FALLBACK_TO_MAGNETIC || readCardResponse.getResult() == Result.ICC_CARD_DETECT_ERROR;
    }

    @Override
    protected void handleTransactionDeclinedCases(ReadCardResponse readCardResponse) {
        if(this.transactionDeclined(readCardResponse)) {
            String tvr = readCardResponse.getCardDetails().getTagCollection().get(IccData.TVR.getTag()).getData();
            List<TvrResults> tvrResults = TvrResultParser.parseTvr(tvr);
            if (tvrResults.contains(TvrResults.BYTE_2_BIT_5))
                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_DECLINED_SERVICE_NOT_ALLOWED_FOR_CARD);
            else
                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_DECLINED);
        }
    }

    @Override
    protected void handleErrorsByProcessor(ReadCardResponse readCardResponse) {
        if (readCardResponse.getResult() == Result.USER_CANCELLED) {
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        } else if (readCardResponse.getResult() == Result.TIMEOUT) {
            throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
        }

    }

    private ReadCardResponse handleContactToMagneticFallbackCases(ReadCardRequest readCardRequest) {
        this.readCardService.cleanUp();
        this.notifier.notify(Notifier.EventType.SWITCH_ICC_TO_MAGNETIC,
                Notifier.EventType.SWITCH_ICC_TO_MAGNETIC.getDescription());
        ReadCardRequest contactFallbackReadCardRequest = this.prepareContactFallbackReadCardRequest(readCardRequest);
        ReadCardResponse readCardResponse = this.readCardService.readCardDetails(contactFallbackReadCardRequest);
        readCardResponse.setFallbackIccToMag(true);
        return new MagCardProcessor(
                transactionRepository,
                transactionAuthenticator,
                applicationRepository,
                readCardService,
                notifier,
                terminalRepository
        ).validateReadCardResponse(readCardRequest, readCardResponse);
    }

    private ReadCardRequest prepareContactFallbackReadCardRequest(ReadCardRequest readCardRequest) {
        List<CardType> cardTypeList = new ArrayList<>();
        cardTypeList.add(CardType.MAG);
        ReadCardRequest fallbackRequest = new ReadCardRequest(cardTypeList, readCardRequest.getTransactionType(),
                readCardRequest.getTransactionType9C(), readCardRequest.getEmvParametersRequest());
        fallbackRequest.setCashBackAmount(readCardRequest.getCashBackAmount());
        fallbackRequest.setAmount(readCardRequest.getAmount());
        fallbackRequest.setCardDetailsBeforeEmvNext(readCardRequest.getCardDetailsBeforeEmvNext());
        fallbackRequest.setCardReadTimeOut(readCardRequest.getCardReadTimeOut());
        fallbackRequest.setFallbackFromIccToMag(readCardRequest.getFallbackFromIccToMag());
        fallbackRequest.setForceOnlineForICC(readCardRequest.getForceOnlineForICC());
        fallbackRequest.setForceOnlineForPICC(readCardRequest.getForceOnlineForPICC());
        fallbackRequest.setIccDataList(readCardRequest.getIccDataList());
        fallbackRequest.setMaxmPinLength(readCardRequest.getMaxmPinLength());
        fallbackRequest.setMinmPinLength(readCardRequest.getMinmPinLength());
        fallbackRequest.setMultipleAidSelectionListener(readCardRequest.getMultipleAidSelectionListener());
        fallbackRequest.setPackageName(readCardRequest.getPackageName());
        fallbackRequest.setPinBlockFormat(readCardRequest.getPinBlockFormat());
        fallbackRequest.setPinpadRequired(readCardRequest.isPinpadRequired());
        fallbackRequest.setPinpadTimeout(readCardRequest.getPinpadTimeout());
        fallbackRequest.setStan(readCardRequest.getStan());
        fallbackRequest.setPrimaryAccountNumber(readCardRequest.getPrimaryAccountNumber());
        fallbackRequest.setTransactionDate(DateUtils.yyMMddDate());
        fallbackRequest.setTransactionTime(DateUtils.HHmmssTime());
        fallbackRequest.setCurrencyName(readCardRequest.getCurrencyName());
        fallbackRequest.setPinPadFixedLayout(!terminalRepository.shouldShufflePinPad());
        return fallbackRequest;
    }



    private boolean transactionDeclined(ReadCardResponse readCardResponse) {
        try {
            return readCardResponse.getCardDetails().getTagCollection().get(IccData.CID.getTag())
                    .getData().equals("00");
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }

    @Override
    protected Boolean checkIfPinRequired(TransactionType transactionType, String cardSchemeId) {
        return this.retrievePinRequired(transactionType, cardSchemeId);
    }


    private Boolean retrievePinRequired(TransactionType transactionType, String cardSchemeId) {
        TransactionSetTableVerifier verifier = new TransactionSetTableVerifier(
                this.applicationRepository,
                cardSchemeId,
                TransactionUtils.mapPurchaseTypeWithTransactionType(transactionType));
        return verifier.isPinVerificationNecessary();
    }
}
