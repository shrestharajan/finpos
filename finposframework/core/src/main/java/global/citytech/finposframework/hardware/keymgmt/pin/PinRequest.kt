package global.citytech.finposframework.hardware.keymgmt.pin

import global.citytech.finposframework.hardware.keymgmt.keys.KeyAlgorithm
import global.citytech.finposframework.hardware.keymgmt.keys.KeyType
import global.citytech.finposframework.hardware.keymgmt.keys.KeyType.KEY_TYPE_PEK
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.PEK_KEY_INDEX

import global.citytech.finposframework.hardware.io.cards.CardSummary

/**
 * Created by Rishav Chudal on 6/19/20.
 */
data class PinRequest constructor(
    val primaryAccountNumber: String,
    val pinPadMessage: String,
    val pinBlockFormat: PinBlockFormat,
    val pinMinLength: Int? = 0,
    val pinMaxLength: Int? = 0,
    val pinPadTimeout: Int? = 0,
    val amountMessage: String,
    val pinPadFixedLayout: Boolean = true,
    val showBackButton: Boolean = false,
    val dynamicTitle: String,

) {
    var isByPass:Boolean? = false
    var packageName: String? = null
    var cardSummary: CardSummary? = null
    var keyType: KeyType = KEY_TYPE_PEK
    var pinKeyIndex: Int = PEK_KEY_INDEX
    var keyAlgorithm: KeyAlgorithm = KeyAlgorithm.TYPE_3DES
}