package global.citytech.finpos.processor.nibl.transaction.voidsale;

import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finposframework.switches.voidsale.VoidSaleResponseParameter;

/**
 * Created by Rishav Chudal on 9/17/20.
 */

public class VoidSaleResponseModel extends TransactionResponseModel {

    private String debugRequestMessage;
    private String debugResponseMessage;
    private String message;
    private String stan;
    private boolean approved;
    private boolean shouldPrintCustomerCopy;

    private VoidSaleResponseModel() {

    }

    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }

    public String getMessage() {
        return message;
    }

    public String getStan() {
        return stan;
    }

    public boolean isApproved() {
        return approved;
    }

    public boolean shouldPrintCustomerCopy() {
        return shouldPrintCustomerCopy;
    }

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private String message;
        private String stan;
        private boolean approved;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withDebugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public Builder withDebugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder withApproved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public Builder withShouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public VoidSaleResponseModel build() {
            VoidSaleResponseModel voidSaleResponseModel = new VoidSaleResponseModel();
            voidSaleResponseModel.debugRequestMessage = this.debugRequestMessage;
            voidSaleResponseModel.debugResponseMessage = this.debugResponseMessage;
            voidSaleResponseModel.message = this.message;
            voidSaleResponseModel.stan = this.stan;
            voidSaleResponseModel.approved = this.approved;
            voidSaleResponseModel.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            return voidSaleResponseModel;
        }
    }
}
