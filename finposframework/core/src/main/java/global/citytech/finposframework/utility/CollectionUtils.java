package global.citytech.finposframework.utility;

import java.util.List;

/** User: Surajchhetry Date: 2/18/20 Time: 11:45 AM */
public class CollectionUtils {

  public static boolean isEmptyOrNull(List collections) {
    if (collections == null) return true;
    return collections.isEmpty();
  }
}
