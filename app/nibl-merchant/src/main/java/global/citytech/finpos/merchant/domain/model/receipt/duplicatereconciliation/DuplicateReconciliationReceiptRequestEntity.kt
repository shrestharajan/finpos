package global.citytech.finpos.merchant.domain.model.receipt.duplicatereconciliation

import global.citytech.finposframework.hardware.io.printer.PrinterService
import global.citytech.finposframework.repositories.ReconciliationRepository

/**
 * Created by Saurav Ghimire on 6/16/21.
 * sauravnghimire@gmail.com
 */


data class DuplicateReconciliationReceiptRequestEntity(
    val reconciliationRepository: ReconciliationRepository? = null,
    val printerService: PrinterService? = null
)