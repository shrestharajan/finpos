package global.citytech.finpos.merchant.presentation.appnotification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.finpos.merchant.data.repository.app.AppNotificationRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.Action
import global.citytech.finpos.merchant.domain.model.app.AppNotification
import global.citytech.finpos.merchant.domain.usecase.app.AppNotificationUseCase
import global.citytech.finpos.merchant.framework.datasource.app.AppNotificationDataSourceImpl
import global.citytech.finposframework.log.Logger

/**
 * Created by Unique Shakya on 5/31/2021.
 */
class AppNotificationViewModel(val context: Application) : BaseAndroidViewModel(context) {

    val appNotificationsLiveData by lazy { MutableLiveData<List<AppNotification>>() }
    val startSettlementLiveData by lazy { MutableLiveData<Boolean>() }
    val allNotificationsCleared by lazy { MutableLiveData<Boolean>() }

    private val appNotificationUseCase = AppNotificationUseCase(
        AppNotificationRepositoryImpl(
            AppNotificationDataSourceImpl(context.applicationContext)
        )
    )

    fun retrieveAppNotifications() {
        val appNotificationList = appNotificationUseCase.getAllNotifications()
        appNotificationsLiveData.value = appNotificationList
    }

    fun handleAppNotificationPositiveAction(appNotification: AppNotification) {
        when (appNotification.action) {
            Action.SETTLEMENT -> startSettlementLiveData.value = true
            else -> Logger.getLogger(AppNotificationViewModel::class.java.name)
                .log("Not yet implemented")
        }
    }

    fun checkForRemainingNotifications() {
        allNotificationsCleared.value = appNotificationUseCase.noNotifications()
    }
}