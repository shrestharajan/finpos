package global.citytech.finpos.nibl.iso8583.requestsender.tip;

import global.citytech.finpos.nibl.iso8583.NIBLMessagePayloadBuilder;
import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.PosEntryModeConfig;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Unique Shakya on 4/26/2021.
 */
public class TipAdjustmentRequestSender extends NIBLMessageSenderTemplate {

    private Logger logger = Logger.getLogger(TipAdjustmentRequestSender.class.getName());

    public TipAdjustmentRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti = new MTI(
                MTI.Version.Version_1987,
                MTI.MessageClass.Financial,
                MTI.MessageFunction.Advice,
                MTI.Originator.Acquirer
        );
        Iso8583Msg iso8583Msg = this.getTemplateIsoMessage(mti);
        TransactionIsoRequest request = (TransactionIsoRequest) this.context.getRequest();
        iso8583Msg.addField(new DataElement(DataElement.PRIMARY_ACCOUNT_NUMBER, request.getPan()));
        iso8583Msg.addField(new DataElement(DataElement.AMOUNT, HelperUtils.toISOAmount(request.getTransactionAmount(), 12)));
        iso8583Msg.addField(new DataElement(DataElement.LOCAL_TIME, request.getLocalTime()));
        iso8583Msg.addField(new DataElement(DataElement.LOCAL_DATE, request.getLocalDate()));
        iso8583Msg.addField(new DataElement(DataElement.PAN_EXPIRE_DATE, request.getExpireDate()));
        if (request.getPosEntryMode() != null)
            iso8583Msg.addField(new DataElement(DataElement.POS_ENTRY_MODE, PosEntryModeConfig.getValue(request.getPosEntryMode())));
        iso8583Msg.addField(new DataElement(DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE, request.getPosConditionCode()));
        iso8583Msg.addField(new DataElement(DataElement.RETRIEVAL_REFERENCE_NUMBER, request.getOriginalRetrievalReferenceNumber()));
        iso8583Msg.addField(new DataElement(DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE, request.getOriginalApprovalCode()));
//        iso8583Msg.addField(new DataElement(DataElement.RESPONSE_CODE, request.getOriginalResponseCode()));
        iso8583Msg.addField(new DataElement(DataElement.ADDITIONAL_AMOUNT, HelperUtils.toISOAmount(request.getTipAmount(), 12)));
//        if (!StringUtils.isEmpty(request.getIccDataBlock()))
//            iso8583Msg.addField(new DataElement(DataElement.ICC_SYSTEM_RELATED_DATA, request.getIccDataBlock()));
        iso8583Msg.addField(new DataElement(DataElement.PRIVATE_USE_FIELD_60, HelperUtils.toISOAmount(request.getOriginalAmount(), 12)));
        iso8583Msg.addField(new DataElement(DataElement.PRIVATE_USE_FIELD_62, request.getOriginalInvoiceNumber()));
        MessagePayload.Builder builder = new NIBLMessagePayloadBuilder(this.specInfo, iso8583Msg);
        logger.debug("<<<<<<< REQUEST >>>>>>>");
        logger.debug(iso8583Msg.getPrintableData());
        return builder.build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.TIP_ADJUSTMENT.getCode();
    }

    @Override
    protected String getFunctionCode() {
        return "0011";
    }
}
