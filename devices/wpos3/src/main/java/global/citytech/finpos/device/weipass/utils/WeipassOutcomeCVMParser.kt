package global.citytech.finpos.device.weipass.utils

import global.citytech.finposframework.hardware.io.cards.cvm.PICCCvm
import wangpos.sdk4.libbasebinder.HEX

/**
 * Created by Rishav Chudal on 6/15/20.
 */
class WeipassOutcomeCVMParser {
    companion object {
        fun retrieveCvmByTransactionOutcome(tagDF8129: String): PICCCvm {
            var contactlessCVM = PICCCvm.NO_CVM
            val tagDf8129InBytes = HEX.hexToBytes(tagDF8129)
            println("tagDF8129bytes[3] ::: ".plus(tagDf8129InBytes[3]))
            val cvmBitValue = (tagDf8129InBytes[3].toInt() ushr 4)
            println("CVM Bit Value of DF8129 ::: ".plus(Integer.toHexString(cvmBitValue)))
            when(cvmBitValue) {
                0x01 -> contactlessCVM = PICCCvm.SIGNATURE
                0x02 -> contactlessCVM = PICCCvm.ONLINE_PIN
                0x03 -> contactlessCVM = PICCCvm.CD_CVM
            }
            return contactlessCVM
        }
    }
}