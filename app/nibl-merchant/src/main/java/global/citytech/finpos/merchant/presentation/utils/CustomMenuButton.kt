package global.citytech.finpos.merchant.presentation.utils

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.button.MaterialButton
import global.citytech.finpos.merchant.R
import global.citytech.finposframework.log.Logger

/**
 * Created by Rishav Chudal on 8/11/20.
 */
class CustomMenuButton: ConstraintLayout {
    private val logger = Logger(CustomMenuButton::class.java.name)
    private var ivIcon: MaterialButton? = null
    private var tvMenuTitle: TextView? = null
    private var clMenu: LinearLayout? = null

    constructor(context: Context): super(context!!) {
        initView(null)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super (
        context!!,
        attrs
    ) {
        initView(attrs)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ): super (
        context!!,
        attrs,
        defStyleAttr
    ) {
        initView(attrs)
    }

    private fun initView(attrs: AttributeSet?) {
        val view = View.inflate(
            context,
            R.layout.custom_menu_button,
            this
        )
        ivIcon = view.findViewById(R.id.iv_icon)
        tvMenuTitle = view.findViewById(R.id.tv_menu_title)
        clMenu = view.findViewById(R.id.cl_menu)
        clMenu!!.layoutParams = LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        if (attrs != null) {
            val typedArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.CustomMenuButton
            )
            val menuIcon = typedArray.getResourceId(
                R.styleable.CustomMenuButton_imgSrc,
                0
            )
            val menuTitle = typedArray.getString(R.styleable.CustomMenuButton_title)
            val contentPadding = typedArray.getDimension(
                R.styleable.CustomMenuButton_contentPadding,
                0f
            ).toInt()
            ivIcon!!.setIconResource(menuIcon)
            tvMenuTitle!!.text = menuTitle
            clMenu!!.setPadding(
                0,
                contentPadding,
                0,
                contentPadding
            )
        }
    }


    override fun onDetachedFromWindow() {
        logger.log("memory onDetachedFromWindow")
        ivIcon = null
        tvMenuTitle = null
        clMenu = null
        super.onDetachedFromWindow()
    }


}