package global.citytech.finpos.nibl.iso8583.requestsender.refund;

import global.citytech.finpos.nibl.iso8583.PosEntryModeConfig;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.switches.PosEntryMode;

/**
 * Created by Rishav Chudal on 10/16/20.
 */
public class MagRefundRequestSender extends RefundRequestSender {

    public MagRefundRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
    }

    @Override
    public void addConditionalFields(Iso8583Msg msg, TransactionIsoRequest refundRequest) {
        msg.addField(
                new DataElement(
                        DataElement.TRACK_II_DATA,
                        refundRequest.getTrack2Data()
                )
        );
        PosEntryMode posEntryMode;
        if (refundRequest.isFallbackFromIccToMag())
            posEntryMode = PosEntryMode.ICC_FALLBACK_TO_MAGNETIC;
        else
            posEntryMode = PosEntryMode.MAGNETIC_STRIPE;
        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(posEntryMode)
                )
        );
    }
}
