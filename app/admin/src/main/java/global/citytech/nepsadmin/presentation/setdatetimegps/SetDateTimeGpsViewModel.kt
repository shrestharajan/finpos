package global.citytech.nepsadmin.presentation.setdatetimegps

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import java.util.*

class SetDateTimeGpsViewModel(context:Application):BaseAndroidViewModel(context) {

    val date by lazy { MutableLiveData<String>() }
    val time by lazy { MutableLiveData<String>() }
    val location by lazy { MutableLiveData<String>() }

    fun saveGps(latitude: String, longitude: String) {

    }

    fun retrieveDateTime() {
        date.value =  retrieveCurrentDate()
        time.value =  this.retrieveCurrentTime()
    }

    private fun retrieveCurrentTime(): String {
        val calendar = Calendar.getInstance()
        val year = calendar[Calendar.YEAR]
        val month = calendar[Calendar.MONTH]
        val day = calendar[Calendar.DAY_OF_MONTH]
        return this.prepareFormattedDate(year, month + 1, day)!!
    }

    private fun retrieveCurrentDate(): String {
        val calendar = Calendar.getInstance()
        val hour = calendar[Calendar.HOUR_OF_DAY]
        val minute = calendar[Calendar.MINUTE]
        return this.prepareFormattedTime(hour, minute)!!
    }

    private fun prepareFormattedTime(hour: Int, minute: Int): String? {
        return "$hour:$minute"
    }

    fun retrieveCurrentLocation() {

    }

    private fun prepareFormattedDate(year: Int, month: Int, day: Int): String? {
        val builder = StringBuilder()
        builder.append(this.numericToAlphabeticMonth(month))
        builder.append(" ")
        builder.append(day)
        builder.append(", ")
        builder.append(year)
        return builder.toString()
    }

    private fun numericToAlphabeticMonth(month: Int): String? {
        return when (month) {
            1 -> "January"
            2 -> "February"
            3 -> "March"
            4 -> "April"
            5 -> "May"
            6 -> "June"
            7 -> "July"
            8 -> "August"
            9 -> "September"
            10 -> "October"
            11 -> "November"
            12 -> "December"
            else -> ""
        }
    }
}