package global.citytech.finpos.device.weipass.utils

import com.wiseasy.emvprocess.SDKInstance
import global.citytech.finpos.device.weipass.card.handler.WeipassStateHandler
import global.citytech.finposframework.hardware.common.Result
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import wangpos.sdk4.libbasebinder.BankCard

/**
 * Created by Rishav Chudal on 6/8/20.
 */
class WeipassTransStateHandlerTest {

    @MockK
    private lateinit var mockkedBankCard: BankCard

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        mockkWeipassLogger()
        mockkSDKInstance()
    }

    /**
     * doProceedFurther test cases
     */
    @Test
    fun doProceedFurther_WhenTransStateIsNone_ReturnsTrue() {
        //Setup
        val transState = WeipassTransState.NONE

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler);

        //Verify
        assertTrue(result)
    }

    @Test
    fun doProceedFurther_WhenTransStateIsSuccess_ReturnsTrue() {
        //Setup
        val transState = WeipassTransState.SUCCESS

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler)

        //Verify
        assertTrue(result)
    }

    @Test
    fun doProceedFurther_WhenTransStateIsReadCard_ReturnsTrue() {
        //Setup
        val transState = WeipassTransState.READ_CARD

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler)

        //Verify
        assertTrue(result)
    }

    @Test
    fun doProceedFurther_WhenTransStateIsProcessEmv_ReturnsTrue() {
        //Setup
        val transState = WeipassTransState.PROCESS_EMV

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler)

        //Verify
        assertTrue(result)
    }

    @Test
    fun doProceedFurther_WhenTransStateIsSetOnlineResult_ReturnsTrue() {
        //Setup
        val transState = WeipassTransState.SET_ONLINE_RESULT

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler)

        //Verify
        assertTrue(result)
    }

    @Test
    fun doProceedFurther_WhenTransStateIsOnlineApproved_ReturnsTrue() {
        //Setup
        val transState = WeipassTransState.ONLINE_APPROVED

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler)

        //Verify
        assertTrue(result)
    }

    @Test
    fun doProceedFurther_WhenTransStateIsOnlineDeclined_ReturnsTrue() {
        //Setup
        val transState = WeipassTransState.ONLINE_DECLINED

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler)

        //Verify
        assertTrue(result)
    }

    @Test
    fun doProceedFurther_WhenTransStateIsOfflineApproved_ReturnsTrue() {
        //Setup
        val transState = WeipassTransState.OFFLINE_APPROVED

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler)

        //Verify
        assertTrue(result)
    }

    @Test
    fun doProceedFurther_WhenTransStateIsOfflineDeclined_ReturnsTrue() {
        //Setup
        val transState = WeipassTransState.OFFLINE_DECLINED

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler)

        //Verify
        assertTrue(result)
    }

    @Test
    fun doProceedFurther_WhenTransStateIsOnlineFailOfflineApproved_ReturnsTrue() {
        //Setup
        val transState = WeipassTransState.ONLINE_FAIL_OFFLINE_APPROVED

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler)

        //Verify
        assertTrue(result)
    }

    @Test
    fun doProceedFurther_WhenTransStateIsOnlineFailOfflineDeclined_ReturnsTrue() {
        //Setup
        val transState = WeipassTransState.ONLINE_FAIL_OFFLINE_DECLINED

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler)

        //Verify
        assertTrue(result)
    }

    @Test
    fun doProceedFurther_WhenTransStateIsPinPadSuccess_ReturnsTrue() {
        //Setup
        val transState = WeipassTransState.PINPAD_SUCCESS

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler)

        //Verify
        assertTrue(result)
    }

    @Test
    fun doProceedFurther_WhenTransStateIsScriptProcessing_ReturnsTrue() {
        //Setup
        val transState = WeipassTransState.SCRIPT_PROCESSING

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler)

        //Verify
        assertTrue(result)
    }

    @Test
    fun doProceedFurther_WhenTransStateIsFailure_ResultIsFailureInReadCardResponseOfWeipassStateHandlerAndReturnsFalse() {
        //Setup
        val transState = WeipassTransState.FAILURE

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler);

        //Verify
        assertEquals(null, WeipassStateHandler.readCardResponse?.cardDetails)
        assertEquals(Result.FAILURE, WeipassStateHandler.readCardResponse?.result)
        assertEquals(WeipassConstant.MSG_READ_CARD_FAIL, WeipassStateHandler.readCardResponse?.message)
        assertFalse(result)
        clearAllMocks()
    }

    @Test
    fun doProceedFurther_WhenTransStateIsSelectApplicationCancelled_ResultIsUserCancelledInReadCardResponseOfWeipassStateHandlerAndReturnsFalse() {
        //Setup
        val transState = WeipassTransState.SELECT_APPLICATION_CANCELLED

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler);

        //Verify
        assertEquals(null, WeipassStateHandler.readCardResponse?.cardDetails)
        assertEquals(Result.USER_CANCELLED, WeipassStateHandler.readCardResponse?.result)
        assertEquals(WeipassConstant.MSG_APP_SELECTION_CANCELLED, WeipassStateHandler.readCardResponse?.message)
        assertFalse(result)
        clearAllMocks()
    }

    @Test
    fun doProceedFurther_WhenTransStateIsPinpadCancel_ResultIsUserCancelledInReadCardResponseOfWeipassStateHandlerAndReturnsFalse() {
        //Setup
        val transState = WeipassTransState.PINPAD_CANCEL

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler);

        //Verify
        assertEquals(null, WeipassStateHandler.readCardResponse?.cardDetails)
        assertEquals(Result.USER_CANCELLED, WeipassStateHandler.readCardResponse?.result)
        assertEquals(WeipassConstant.MSG_PIN_ENTRY_CANCELLED, WeipassStateHandler.readCardResponse?.message)
        assertFalse(result)
        clearAllMocks()
    }

    @Test
    fun doProceedFurther_WhenTransStateIsPinpadError_ResultIsPinPadErrorInReadCardResponseOfWeipassStateHandlerAndReturnsFalse() {
        //Setup
        val transState = WeipassTransState.PINPAD_ERROR

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler);

        //Verify
        assertEquals(null, WeipassStateHandler.readCardResponse?.cardDetails)
        assertEquals(Result.PINPAD_ERROR, WeipassStateHandler.readCardResponse?.result)
        assertEquals(WeipassConstant.MSG_PINPAD_FAILURE, WeipassStateHandler.readCardResponse?.message)
        assertFalse(result)
        clearAllMocks()
    }

    @Test
    fun doProceedFurther_WhenTransStateIsPinpadTimeout_ResultIsTimeoutInReadCardResponseOfWeipassStateHandlerAndReturnsFalse() {
        //Setup
        val transState = WeipassTransState.PINPAD_TIMEOUT

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler);

        //Verify
        assertEquals(null, WeipassStateHandler.readCardResponse?.cardDetails)
        assertEquals(Result.TIMEOUT, WeipassStateHandler.readCardResponse?.result)
        assertEquals(WeipassConstant.MSG_PINPAD_TIMEOUT, WeipassStateHandler.readCardResponse?.message)
        assertFalse(result)
        clearAllMocks()
    }

    @Test
    fun doProceedFurther_WhenTransStateIsProcessingError_ResultIsProcessingErrorInReadCardResponseOfWeipassStateHandlerAndReturnsFalse() {
        //Setup
        val transState = WeipassTransState.PROCESSING_ERROR

        //Run
        val result = WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler);

        //Verify
        assertEquals(null, WeipassStateHandler.readCardResponse?.cardDetails)
        assertEquals(Result.PROCESSING_ERROR, WeipassStateHandler.readCardResponse?.result)
        assertEquals(WeipassConstant.MSG_PROCESSING_ERROR, WeipassStateHandler.readCardResponse?.message)
        assertFalse(result)
        clearAllMocks()
    }

    /**
     * checkForSdkErrorCode
     */
    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactlessFailedException_SetsTransStateProcessingErrorInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_FAILED_EXCEPTION

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PROCESSING_ERROR, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactlessWaveAgain_SetsTransStateContactlessWaveAgainInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_PRE_PROCESS_FAILED

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PICC_WAVE_AGAIN, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactlessAppSelectionFailed_SetsTransStateContactlessWaveAgainInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_APPLICATION_SELECT_FAILED

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PICC_WAVE_AGAIN, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndReadRecordFailed_SetsTransStateContactlessWaveAgainInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_READ_RECORD_FAILED

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PICC_WAVE_AGAIN, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactlessCardAuthenticateFailed_SetsTransStateContactlessWaveAgainInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_CARD_AUTHENTICATE_FAILED

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PROCESSING_ERROR, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactlessParameterNull_SetsTransStateProcessingErrorInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_PARAMETER_NULL

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PROCESSING_ERROR, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactlessParameterTerminalSettingNull_SetsTransStateProcessingErrorInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_PARAMETER_TERMINAL_SETTING_NULL

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PROCESSING_ERROR, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactlessParameterTerminalSettingError_SetsTransStateProcessingErrorInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_PARAMETER_TERMINALSETTING_ERROR

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PROCESSING_ERROR, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactlessNoApplication_SetsTransStateContactlessFallbackInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_NO_APPLICATION

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PICC_FALLBACK, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndApplicationBlocked_SetsTransStateApplicationBlockedInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_APPLICATION_BLOCK

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.APPLICATION_BLOCKED, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactlessCardBlock_SetsTransStateContactlessFallbackInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_CARD_BLOCK

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PICC_FALLBACK, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactlessUseOtherInterface_SetsTransStateContactlessFallbackInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_USE_OTHER_INTERFACE

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PICC_FALLBACK, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactlessNoDetectCard_SetsTransStateContactlessWaveAgainInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_NO_DETECT_CARD

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PICC_WAVE_AGAIN, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactlessUseContract_SetsTransStateContactlessFallbackInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_USE_CONTRACT

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PICC_FALLBACK, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactlessTryAgain_SetsTransStateContactlessWaveAgainInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_TRY_AGAIN

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PICC_WAVE_AGAIN, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactlessOtherError_SetsTransStateProcessingErrorInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_PICC_TRANS_OTHER_ERROR

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PROCESSING_ERROR, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactTransFailed_SetsTransStateFailureInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_ICC_TRANS_FAILED

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.FAILURE, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactAppSelectFailedAndIccPresentInDevice_SetsTransStateContactFallbackInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_ICC_TRANS_APPSEL_FAILED
        every { SDKInstance.mBankCard.iccDetect() } returns 1

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        verify { SDKInstance.mBankCard.iccDetect() }
        assertEquals(WeipassTransState.ICC_FALLBACK, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactAppSelectFailedAndIccNotPresentInDevice_SetsTransStateProcessingErrorInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_ICC_TRANS_APPSEL_FAILED
        every { SDKInstance.mBankCard.iccDetect() } returns 0

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        verify { SDKInstance.mBankCard.iccDetect() }
        assertEquals(WeipassTransState.PROCESSING_ERROR, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactReadAppDataFailedAndIccPresentInDevice_SetsTransStateContactFallbackInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_ICC_TRANS_READAPPDATA_FAILED
        every { SDKInstance.mBankCard.iccDetect() } returns 1

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        verify { SDKInstance.mBankCard.iccDetect() }
        assertEquals(WeipassTransState.ICC_FALLBACK, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactReadAppDataFailedAndIccNotPresentInDevice_SetsTransStateProcessingErrorInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_ICC_TRANS_READAPPDATA_FAILED
        every { SDKInstance.mBankCard.iccDetect() } returns 0

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        verify { SDKInstance.mBankCard.iccDetect() }
        assertEquals(WeipassTransState.PROCESSING_ERROR, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactCardAuthFailedAndIccPresentInDevice_SetsTransStateContactFallbackInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_ICC_TRANS_CARDAUTH_FAILED
        every { SDKInstance.mBankCard.iccDetect() } returns 1

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        verify { SDKInstance.mBankCard.iccDetect() }
        assertEquals(WeipassTransState.ICC_FALLBACK, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndContactCardAuthFailedAndIccNotPresentInDevice_SetsTransStateProcessingErrorInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_ICC_TRANS_CARDAUTH_FAILED
        every { SDKInstance.mBankCard.iccDetect() } returns 0

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        verify { SDKInstance.mBankCard.iccDetect() }
        assertEquals(WeipassTransState.PROCESSING_ERROR, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndGetPanFromEmvCardFailed_SetsTransStateProcessingErrorInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.TRANSPROCESS_GETPANFROMEMVCARD_GETPAN_FAILED

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PROCESSING_ERROR, WeipassStateHandler.transState)
    }

    @Test
    fun checkForSdkErrorCode_OnTransResultNonZeroAndOutcomeFailed_SetsTransStateProcessingErrorInWeipassStateHandler() {
        //Setup
        val result = SDKErrorCode.OUTCOME_FAILED

        //Run
        WeipassTransStateHandler.checkForSdkErrorCode(result.errorCode, WeipassStateHandler)

        //Verify
        assertTrue(result.errorCode != 0)
        assertEquals(WeipassTransState.PROCESSING_ERROR, WeipassStateHandler.transState)
    }

    private fun mockkWeipassLogger() {
        mockkObject(WeipassLogger)
        mockkObject(WeipassLogger.Companion)
        every { WeipassLogger.log(any()) } just Runs
    }

    private fun mockkSDKInstance() {
        mockkStatic(SDKInstance::class)
        SDKInstance.mBankCard = mockkedBankCard
    }

}