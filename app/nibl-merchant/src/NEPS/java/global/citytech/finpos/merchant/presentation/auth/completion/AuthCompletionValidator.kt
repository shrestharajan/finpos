package global.citytech.finpos.merchant.presentation.auth.completion

import global.citytech.common.extensions.isAlphaNumeric
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase


/**
 * Created by Saurav Ghimire on 2/15/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
object AuthCompletionValidator {
    fun validateData(rrn: String) = rrn.isAlphaNumeric() && rrn.length == 12
    fun getTransactionByDataObservable(localDataUseCase: LocalDataUseCase, rrn: String) =
        localDataUseCase.getTransactionByRRN(rrn)
}