package global.citytech.neps.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import global.citytech.neps.data.db.dao.*
import global.citytech.neps.domain.model.*


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
@Database(entities = arrayOf(Host::class,Merchant::class,Logo::class,AidParam::class,CardScheme::class,
            EmvKey::class,EmvParam::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getHostDao(): HostDao
    abstract fun getMerchantDao():MerchantDao
    abstract fun getLogoDao(): LogoDao
    abstract fun getAidParamDao(): AidParamsDao
    abstract fun getCardSchemeDao(): CardSchemeDao
    abstract fun getEmvKeyDao(): EmvKeyDao
    abstract fun getEmvParamDao(): EmvParamDao
    companion object {
        var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            INSTANCE.let { INSTANCE } ?: kotlin.run {
                Room.databaseBuilder(context, AppDatabase::class.java, "neps_db.db").build()
            }
    }
}