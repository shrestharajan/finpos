package global.citytech.finposframework.usecases.transaction;

import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;

public interface ChipCardProcessorInterface extends CardProcessorInterface {

    ReadCardResponse processOnlineResult(ReadCardResponse readCardResponse, IsoMessageResponse isoMessageResponse);
}
