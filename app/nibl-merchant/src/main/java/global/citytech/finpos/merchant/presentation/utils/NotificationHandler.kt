package global.citytech.finpos.merchant.presentation.utils

import android.content.Context
import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import global.citytech.common.Constants
import global.citytech.finposframework.hardware.io.cards.CardDetails
import global.citytech.finposframework.listeners.*
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.notifier.Notifier

/**
 * Created by Unique Shakya on 9/3/2020.
 */
object NotificationHandler : Notifier {

    private val logger = Logger(NotificationHandler::class.java.name)
    private lateinit var application: Context

    fun init(application: Context) {
        this.application = application
    }

    override fun notify(eventType: Notifier.EventType?, message: String?) {
        logger.log("BROADCAST :::: NOTIFY WITH MESSAGE ".plus(message))
        val intent = Intent(Constants.INTENT_ACTION_NOTIFICATION)
        intent.putExtra(Constants.EXTRA_EVENT_TYPE, eventType)
        message.let { intent.putExtra(Constants.EXTRA_NOTIFICATION_MESSAGE, message) }
        LocalBroadcastManager.getInstance(application).sendBroadcast(intent)
    }

    override fun notify(
        eventType: Notifier.EventType?,
        cardDetails: CardDetails?,
        cardConfirmationListener: CardConfirmationListener?
    ) {
        logger.log("BROADCAST :::: NOTIFY WITH CARD CONFIRMATION")
        val intent = Intent(Constants.INTENT_ACTION_NOTIFICATION)
        intent.putExtra(Constants.EXTRA_EVENT_TYPE, eventType)
        intent.putExtra(Constants.EXTRA_CARD_NUMBER, cardDetails?.primaryAccountNumber ?: "")
        intent.putExtra(Constants.EXTRA_CARD_SCHEME, cardDetails?.cardSchemeLabel ?: "")
        intent.putExtra(Constants.EXTRA_CARD_HOLDER_NAME, cardDetails?.cardHolderName ?: "")
        intent.putExtra(Constants.EXTRA_CARD_EXPIRY, cardDetails?.expiryDate ?: "")
        intent.putExtra(Constants.EXTRA_CARD_CONFIRMATION_LISTENER, cardConfirmationListener)
        LocalBroadcastManager.getInstance(application).sendBroadcast(intent)
    }


    override fun notify(
        eventType: Notifier.EventType?,
        cardDetails: CardDetails,
        otpConfirmationListener: OtpConfirmationListener?
    ) {
        logger.log("BROADCAST :::: NOTIFY WITH OTP CONFIRMATION")
        val intent = Intent(Constants.INTENT_ACTION_NOTIFICATION)
        intent.putExtra(Constants.EXTRA_EVENT_TYPE, eventType)
        intent.putExtra(Constants.EXTRA_CARD_NUMBER, cardDetails.primaryAccountNumber)
        intent.putExtra(Constants.EXTRA_CARD_SCHEME, cardDetails.cardSchemeLabel)
        intent.putExtra(Constants.EXTRA_CARD_HOLDER_NAME, cardDetails.cardHolderName)
        intent.putExtra(Constants.EXTRA_CARD_EXPIRY, cardDetails.expiryDate)
        intent.putExtra(Constants.EXTRA_OTP_CONFIRMATION_LISTENER, otpConfirmationListener)
        LocalBroadcastManager.getInstance(application).sendBroadcast(intent)
    }


    override fun notify(eventType: Notifier.EventType?, message: String?, transactionType: String?) {
        logger.log("BROADCAST :::: NOTIFY WITH MESSAGE ".plus(message))
        val intent = Intent(Constants.INTENT_ACTION_NOTIFICATION)
        intent.putExtra(Constants.EXTRA_EVENT_TYPE, eventType)
        intent.putExtra(Constants.EXTRA_TRANSACTION_TYPE, transactionType)
        message.let { intent.putExtra(Constants.EXTRA_NOTIFICATION_MESSAGE, message) }
        LocalBroadcastManager.getInstance(application).sendBroadcast(intent)
    }


    override fun notify(
        eventType: Notifier.EventType?,
        stan: String?,
        message: String?,
        isApproved: Boolean
    ) {
        logger.log("BROADCAST :::: NOTIFY FOR MERCHANT LOG PUSH")
        val intent = Intent(Constants.INTENT_ACTION_NOTIFICATION)
        intent.putExtra(Constants.EXTRA_EVENT_TYPE, eventType)
        intent.putExtra(Constants.KEY_STAN, stan)
        intent.putExtra(Constants.KEY_MESSAGE, message)
        intent.putExtra(Constants.KEY_IS_APPROVED, isApproved)
        LocalBroadcastManager.getInstance(application).sendBroadcast(intent)
    }

    override fun notify(eventType: Notifier.EventType?) {
        logger.log("BROADCAST :::: NOTIFY WITHOUT MESSAGE")
        val intent = Intent(Constants.INTENT_ACTION_NOTIFICATION)
        intent.putExtra(Constants.EXTRA_EVENT_TYPE, eventType)
        LocalBroadcastManager.getInstance(application).sendBroadcast(intent)
    }

    override fun notify(
        eventType: Notifier.EventType?,
        cardConfirmationListenerForGreenPin:  CardConfirmationListenerForGreenPin?
    ) {
        logger.log("BROADCAST :::: NOTIFY WITH CARDCONFIRMATION")
        val intent = Intent(Constants.INTENT_ACTION_NOTIFICATION)
        intent.putExtra(Constants.EXTRA_EVENT_TYPE, eventType)
        intent.putExtra(
            Constants.EXTRA_CARD_CONFIRMATION_LISTENER,
            cardConfirmationListenerForGreenPin
        )
        LocalBroadcastManager.getInstance(application).sendBroadcast(intent)
    }

    override fun notify(
        eventType: Notifier.EventType?,
        jsonData: String?,
        settlementConfirmationListener: SettlementConfirmationListener?
    ) {
        logger.log("BROADCAST :::: NOTIFY WITH SETTLEMENT CONFIRMATION")
        val intent = Intent(Constants.INTENT_ACTION_NOTIFICATION)
        intent.putExtra(Constants.EXTRA_EVENT_TYPE, eventType)
        intent.putExtra(Constants.EXTRA_SETTLEMENT_CONFIRMATION_JSON_DATA, jsonData)
        intent.putExtra(
            Constants.EXTRA_SETTLEMENT_CONFIRMATION_LISTENER,
            settlementConfirmationListener
        )
        LocalBroadcastManager.getInstance(application).sendBroadcast(intent)
    }
}