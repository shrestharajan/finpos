package global.citytech.finposframework.usecases;

import global.citytech.finposframework.hardware.io.cards.CardDetails;

public class IdentifyCardResponse {

    private CardDetails cardDetails;
    private boolean supervisorFunctionRequired;
    private boolean waveAgain;
    private boolean swipeAgain;
    private boolean isAmountCallBack;
    private boolean fallbackIccToMagnetic;

    public CardDetails getCardDetails() {
        return cardDetails;
    }


    public boolean isSupervisorFunctionRequired() {
        return supervisorFunctionRequired;
    }


    public boolean isWaveAgain() {
        return waveAgain;
    }

    public boolean isSwipeAgain() {
        return swipeAgain;
    }

    public boolean isAmountCallBack() {
        return isAmountCallBack;
    }

    public boolean isFallbackIccToMagnetic() {
        return fallbackIccToMagnetic;
    }


    public static final class Builder {
        private CardDetails cardDetails;
        private boolean supervisorFunctionRequired;
        private boolean waveAgain;
        private boolean swipeAgain;
        private boolean isAmountCallBack;
        private boolean fallbackIccToMagnetic;

        public Builder() {
        }

        public static Builder anIdentifyCardResponse() {
            return new Builder();
        }

        public Builder cardDetails(CardDetails cardDetails) {
            this.cardDetails = cardDetails;
            return this;
        }

        public Builder supervisorFunctionRequired(boolean supervisorFunctionRequired) {
            this.supervisorFunctionRequired = supervisorFunctionRequired;
            return this;
        }

        public Builder waveAgain(boolean waveAgain) {
            this.waveAgain = waveAgain;
            return this;
        }

        public Builder swipeAgain(boolean swipeAgain) {
            this.swipeAgain = swipeAgain;
            return this;
        }

        public Builder isAmountCallBack(boolean isAmountCallBack) {
            this.isAmountCallBack = isAmountCallBack;
            return this;
        }

        public Builder fallbackIccToMagnetic(boolean fallbackIccToMagnetic) {
            this.fallbackIccToMagnetic = fallbackIccToMagnetic;
            return this;
        }

        public IdentifyCardResponse build() {
            IdentifyCardResponse identifyCardResponse = new IdentifyCardResponse();
            identifyCardResponse.isAmountCallBack = this.isAmountCallBack;
            identifyCardResponse.fallbackIccToMagnetic = this.fallbackIccToMagnetic;
            identifyCardResponse.waveAgain = this.waveAgain;
            identifyCardResponse.supervisorFunctionRequired = this.supervisorFunctionRequired;
            identifyCardResponse.swipeAgain = this.swipeAgain;
            identifyCardResponse.cardDetails = this.cardDetails;
            return identifyCardResponse;
        }
    }
}