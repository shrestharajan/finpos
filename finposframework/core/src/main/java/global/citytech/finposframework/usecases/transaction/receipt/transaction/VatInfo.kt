package global.citytech.finposframework.usecases.transaction.receipt.transaction

class VatInfo(
    var vatAmount: String? = "",
    val vatRefundAmount: String? = ""
) {

    override fun toString(): String {
        return "vatAmount=$vatAmount, vatRefundAmount=$vatRefundAmount"
    }
}