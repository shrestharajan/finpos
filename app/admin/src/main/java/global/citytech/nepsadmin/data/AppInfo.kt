package global.citytech.nepsadmin.data

data class AppInfo(
    var id: String? = null,
    var version: Int? = null
)