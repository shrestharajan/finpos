package global.citytech.finpos.neps.iso8583.requestsender.voidsale;

import global.citytech.finpos.neps.iso8583.FunctionCodeConfig;
import global.citytech.finpos.neps.iso8583.NepsMessagePayloadBuilder;
import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.PosEntryModeConfig;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.switches.FunctionCode;
import global.citytech.finposframework.utility.HelperUtils;

public class VoidSaleRequestSender extends NepsMessageSenderTemplate {

    private final Logger logger = Logger.getLogger(VoidSaleRequestSender.class.getName());
    protected VoidSaleIsoRequest voidSaleIsoRequest;

    public VoidSaleRequestSender(RequestContext context) {
        super(context);
        voidSaleIsoRequest = (VoidSaleIsoRequest) context.getRequest();
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Reversal,
                        MTI.MessageFunction.Request,
                        MTI.Originator.Acquirer);
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);
        MessagePayload.Builder builder = new NepsMessagePayloadBuilder(this.specInfo, msg);
        msg.addField(
                new DataElement(
                        DataElement.PRIMARY_ACCOUNT_NUMBER,
                        voidSaleIsoRequest.getPan())
        );
        msg.addField(
                new DataElement(
                        DataElement.CURRENCY_CODE,
                        voidSaleIsoRequest.getCurrencyCode())
        );
        msg.addField(
                new DataElement(
                        DataElement.AMOUNT,
                        HelperUtils.toISOAmount(voidSaleIsoRequest.getTransactionAmount(), 12))
        );
        msg.addField(
                new DataElement(
                        DataElement.LOCAL_TIME,
                        voidSaleIsoRequest.getLocalDateTime())
        );
        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(voidSaleIsoRequest.getPosEntryMode()))
        );
        msg.addField(
                new DataElement(
                        DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE,
                        voidSaleIsoRequest.getPosConditionCode()
                )
        );

        if (voidSaleIsoRequest.getRrn() != null) {
            msg.addField(
                    new DataElement(DataElement.RETRIEVAL_REFERENCE_NUMBER,
                            voidSaleIsoRequest.getRrn()
                    )
            );
        }

        if (voidSaleIsoRequest.getEmvData() != null) {
            msg.addField(
                    new DataElement(DataElement.ICC_SYSTEM_RELATED_DATA,
                            voidSaleIsoRequest.getEmvData())
            );
        }

        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());

        return builder.build();
    }


    @Override
    protected String getFunctionCode() {
        return FunctionCodeConfig.getValue(FunctionCode.REVERSAL);
    }

    @Override
    protected String getProcessingCode() {
        return voidSaleIsoRequest.getProcessingCode();
    }

}
