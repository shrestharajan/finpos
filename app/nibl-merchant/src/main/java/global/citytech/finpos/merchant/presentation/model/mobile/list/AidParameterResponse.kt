package global.citytech.finpos.merchant.presentation.model.mobile.list

import com.google.gson.annotations.SerializedName

data class AidParameterResponse(
    val aids: List<String>,
    @SerializedName("aidToProviderMapping")
    val providerMapping: Map<String, String>,
    val networkConfig: Map<String, NetworkConfig>
)

data class NetworkConfig(
    val lcRequired: Boolean
)