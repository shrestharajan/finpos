package global.citytech.finposframework.switches.reconciliation.check;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 8/11/2021.
 */
public interface CheckReconciliationRequester<T extends CheckReconciliationRequestParameter,
        E extends CheckReconciliationResponseParameter> extends Request<T, E> {
}
