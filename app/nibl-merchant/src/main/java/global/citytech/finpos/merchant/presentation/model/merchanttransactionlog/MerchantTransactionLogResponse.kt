package global.citytech.finpos.merchant.presentation.model.merchanttransactionlog

data class MerchantTransactionLogResponse(
    var code: String? = null,
    var data: Any? = null,
    var message: String? = null
)