package global.citytech.finpos.processor.neps.purchases;

import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.ProcessingCode;
import global.citytech.finpos.neps.iso8583.requestsender.purchases.IccPurchaseRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.purchases.MagPurchaseRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.purchases.PICCPurchaseRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.purchases.PurchaseIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionRequestSender;
import global.citytech.finpos.processor.neps.transaction.CardTransactionUseCase;
import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.DateUtils;

/**
 * Created by Saurav Ghimire on 2/2/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class CardPurchaseUseCase extends CardTransactionUseCase {

    @Override
    protected TransactionType getTransactionType() {
        return TransactionType.PURCHASE;
    }

    @Override
    protected boolean getAuthCompletionStatus() {
        return true;
    }

    public CardPurchaseUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }


    @Override
    protected NepsMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context) {
        TransactionRequestSender sender;
        switch (readCardResponse.getCardDetails().getCardType()) {
            case MAG:
                sender = new MagPurchaseRequestSender(context);
                break;
            case PICC:
                sender = new PICCPurchaseRequestSender(context);
                break;
            case ICC:
            default:
                sender = new IccPurchaseRequestSender(context);
                break;
        }
        return sender;
    }

    @Override
    protected TransactionResponse prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository, boolean shouldPrint) {
        return PurchaseResponse.Builder.createDefaultBuilder()
                .approved(transactionApproved)
                .stan(stan)
                .shouldPrintCustomerCopy(shouldPrint)
                .debugRequestMessage(isoMessageResponse.getDebugRequestString())
                .debugResponseMessage(isoMessageResponse.getDebugResponseString())
                .message(this.retrieveMessage(isoMessageResponse, transactionApproved, applicationRepository))
                .build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.PURCHASE.getCode();
    }

    @Override
    protected TransactionResponse prepareErrorResponse(String responseMessage, boolean shouldPrint) {
        return PurchaseResponse.Builder.createDefaultBuilder()
                .message(responseMessage)
                .approved(false)
                .shouldPrintCustomerCopy(false)
                .stan(this.stan)
                .approvalCode("")
                .build();
    }


    @Override
    protected String getClassName() {
        return CardPurchaseUseCase.class.getName();
    }

    @Override
    protected TransactionIsoRequest prepareIsoRequest(ReadCardResponse readCardResponse, TransactionRequest transactionRequest) {
        String pin = shouldSkipPinBlockFromCard(readCardResponse) ? "" : readCardResponse.getCardDetails().getPinBlock().getPin();
        logger.debug("PIN BLOCK :::: " + pin);
        return PurchaseIsoRequest.Builder.newInstance()
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .transactionAmount(readCardResponse.getCardDetails().getAmount())
                .localDateTime(DateUtils.yyMMddHHmmssDate())
                .track2Data(readCardResponse.getCardDetails().getTrackTwoData())
                .currencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .pinBlock(shouldSkipPinBlockFromCard(readCardResponse) ? "" : readCardResponse.getCardDetails().getPinBlock().getPin())
                .emvData(readCardResponse.getCardDetails().getIccDataBlock())
                .fallBackFromIccToMag(readCardResponse.getFallbackIccToMag())
                .build();
    }
}
