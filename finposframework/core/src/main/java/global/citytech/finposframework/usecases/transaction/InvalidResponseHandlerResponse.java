package global.citytech.finposframework.usecases.transaction;

/**
 * Created by Unique Shakya on 10/2/2020.
 */
public class InvalidResponseHandlerResponse {

    private boolean invalid;
    private String message;

    public InvalidResponseHandlerResponse(boolean invalid, String message) {
        this.invalid = invalid;
        this.message = message;
    }

    public boolean isInvalid() {
        return invalid;
    }

    public String getMessage() {
        return message;
    }
}
