package global.citytech.finpos.merchant.domain.usecase.qr
/**
 * @author sachin
 */
data class LengthConfig (val length:Int, val lengthType: LengthType)