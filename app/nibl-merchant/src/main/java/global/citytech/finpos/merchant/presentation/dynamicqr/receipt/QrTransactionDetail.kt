package global.citytech.finpos.merchant.presentation.dynamicqr.receipt

data class QrTransactionDetail(
    val referenceNumber: String,
    val transactionType: String,
    val transactionCurrency: String,
    val transactionAmount: String,
    val transactionResult: String,
    val paymentInitiator: String,
    val initiatorId: String,
    val approvalCode: String
)
