package global.citytech.finposframework.hardware.emv.emvparam

/**
 * Created by Rishav Chudal on 4/22/20.
 */
data class EmvParametersRequest (
    val terminalCountryCode: String,
    val transactionCurrencyCode: String,
    val transactionCurrencyExponent: String,
    val terminalCapabilities: String,
    val terminalType: String,
    val merchantCategoryCode: String,
    val terminalId: String,
    val merchantIdentifier: String,
    val merchantName: String,
    val ttq: String,
    val additionalTerminalCapabilities: String,
    val forceOnlineFlag: String
)