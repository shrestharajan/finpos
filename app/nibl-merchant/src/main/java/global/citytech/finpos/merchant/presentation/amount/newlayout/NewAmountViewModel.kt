package global.citytech.finpos.merchant.presentation.amount.newlayout

import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseViewModel
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.service.NetworkConnectionReceiver
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.isConnectionAvailable
import global.citytech.finposframework.utility.AmountUtils
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class NewAmountViewModel : BaseViewModel() {

    val amount = MutableLiveData<String>()
    private var TAG = NewAmountViewModel::class.java.name
    val validAmount by lazy { MutableLiveData<Boolean>() }
    val maxAmountMessage by lazy { MutableLiveData<String>() }
    val internetConnected by lazy { MutableLiveData<Boolean>() }
    var amountLengthLimit: Int = 0

    private var isDotPressed: Boolean = false
    private var integerNumber: String = ""
    private var decimalNumber: String = ""
    private var includingBoth: String = ""

    var localDataUseCase: LocalDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    fun onNumericButtonPressedForNewLayout(buttonText: String, limit: Int) {
        checkAndAssignDotToNumbers(buttonText, limit)
        validateAndDisplayNumbersInView(limit)
    }

    private fun checkAndAssignDotToNumbers(buttonText: String, limit: Int) {
        if (buttonText != "." && !isDotPressed && integerNumber.length < limit) {
            if (buttonText == "0" && integerNumber.isEmpty()) {

            } else {
                integerNumber += buttonText
            }

        } else if (buttonText != "." && isDotPressed && decimalNumber.length < 2 && includingBoth.length < limit) {
            decimalNumber += buttonText
        } else {
            isDotPressed = true
        }
    }

    private fun validateAndDisplayNumbersInView(limit: Int) {
        if (includingBoth.length < limit) {
            if (isDotPressed) {
                if (integerNumber.isEmpty()) {
                    amount.value = "0.$decimalNumber"
                } else {
                    amount.value = "$integerNumber.$decimalNumber"
                }
            } else {
                amount.value = integerNumber
            }
            includingBoth = integerNumber + decimalNumber
        }
    }

    fun onClearButtonClickedForNewLayout() {
        validateAndReassignNumbers()
        amount.value = amount.value?.dropLast(1)
    }

    private fun validateAndReassignNumbers() {
        if (amount.value?.contains(".") == true) {
            if (decimalNumber.count() == 2) {
                decimalNumber = decimalNumber.dropLast(1)
            } else {
                amount.value = amount.value?.dropLast(1)
                decimalNumber = ""
                isDotPressed = false
            }
        } else {
            if (integerNumber.isNotEmpty() && integerNumber != "0") {
                integerNumber = integerNumber.dropLast(1)
            }
            isDotPressed = false
            decimalNumber = ""
        }
        includingBoth = integerNumber + decimalNumber
    }

    private fun onCheckConnectionSubscribe(emitter: ObservableEmitter<Boolean>) {
        emitter.onNext(
            isConnectionAvailable()
        )
    }

    fun retrieveAmountLengthLimit() {
        amountLengthLimit = localDataUseCase.getAmountLengthLimit()
    }

    fun checkInternetConnection() {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Boolean> {
                onCheckConnectionSubscribe(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    internetConnected.value = it
                }, {
                    AppUtility.printLog(TAG, it.message.toString())
                    it.printStackTrace()
                    internetConnected.value = false
                })
        )
    }

    fun getConfiguration(): Boolean {
        this.isLoading.value = true
        return compositeDisposable.add(
            localDataUseCase.getWholeConfiguration()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe({
                    NetworkConnectionReceiver.networkConfiguration = it
                    this.isLoading.value = false
                },
                    {
                        AppUtility.printLog(TAG, "Get Configuration error :: " + it.message)
                        this.isLoading.value = false
                        message.value = it.message
                    })
        )
    }

    private fun checkMaximumAmountAllowedByTms(amount: Double) {
        compositeDisposable.add(
            localDataUseCase.getAmountPerTransaction()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    maxAmountMessage.value =
                        "Max Allowed Amount: ".plus(AmountUtils.toHigherCurrency(it.toDouble()))
                    if (it == 0.toLong())
                        validAmount.value = true
                    else
                        validAmount.value = AmountUtils.toLongLowerCurrency(amount) <= it
                }, {
                    AppUtility.printLog(
                        TAG,
                        "Error in Maximum amount allowed by TMS :: " + it.message
                    )
                    validAmount.value = true
                })
        )
    }

    fun checkMaximumAmountAllowed(amount: Double, amountLimit: Double) {
        maxAmountMessage.value =
            "Max Allowed Amount: ".plus(AmountUtils.formatAmountTwoDecimal(amountLimit))
        if (amountLimit > 0) {
            validAmount.value = amount <= amountLimit
        } else {
            checkMaximumAmountAllowedByTms(amount)
        }
    }

    fun onClearButtonLongClicked() {
        includingBoth = ""
        integerNumber = ""
        decimalNumber = ""
        isDotPressed = false
        amount.value = "0"
    }

    fun prepareAmountToSendDuringConfirmation(amount: Double): String =
        AmountUtils.formatAmountTwoDecimal(amount)
}