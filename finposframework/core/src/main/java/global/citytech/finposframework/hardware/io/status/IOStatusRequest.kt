package global.citytech.finposframework.hardware.io.status

/**
 * Created by Rishav Chudal on 6/3/20.
 */
data class IOStatusRequest(val isPrinterOnly: Boolean)