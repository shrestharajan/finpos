package global.citytech.finpos.nibl.iso8583;

import org.junit.Test;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.NIBLHostConfig;
import global.citytech.finpos.nibl.iso8583.requestsender.settlement.SettlementRequestSender;
import global.citytech.finpos.nibl.iso8583.requestsender.voidsale.NiblVoidSaleRequest;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;

import static org.junit.Assert.assertNotNull;

public class SettlementRequestSenderTest {
    private Logger logger = Logger.getLogger(SettlementRequestSenderTest.class.getName());

    @Test
    public void sendTest() {
        try {
            String terminalID = "99991002";
            String axquirerID = "405635000000002";
            TerminalInfo terminalInfo = new TerminalInfo(terminalID, axquirerID,"","Citytech","Kamaladi");
            HostInfo primaryHost = NIBLHostConfig.getPrimaryConfig();
            HostInfo secondaryHost = NIBLHostConfig.getSecondaryConfig();
            ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);
            RequestContext context =
                    new RequestContext(
                            "000405", terminalInfo, connectionParam);
            NiblVoidSaleRequest request = new NiblVoidSaleRequest();
            request.setEntryMode(PosEntryMode.ICC);
            request.setPosConditionCode("00");
           // request.setApprovalCode("230084");
           // request.setInvoiceNumber("000108");
           // request.setRetrievalReferenceNumber("021711388850");
            context.setRequest(request);
            SettlementRequestSender sender = new SettlementRequestSender(new NIBLSpecInfo(), context, false);
            IsoMessageResponse msg = sender.send();
            assertNotNull(msg);
        } catch (FinPosException e) {
            logger.log(e.getMessage());
            logger.log(e.getDetailMessage());
        }
    }
}
