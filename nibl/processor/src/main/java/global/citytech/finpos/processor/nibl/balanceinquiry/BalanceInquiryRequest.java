package global.citytech.finpos.processor.nibl.balanceinquiry;

import global.citytech.finpos.processor.nibl.transaction.TransactionRequestModel;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.io.sound.SoundService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;

public class BalanceInquiryRequest extends TransactionRequestModel {
    public static  class Builder {
        private TransactionRepository transactionRepository;
        private ReadCardService readCardService;
        private DeviceController deviceController;
        private TransactionAuthenticator transactionAuthenticator;
        private PrinterService printerService;
        private ApplicationRepository applicationRepository;
        private global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest;
        private LedService ledService;
        private SoundService soundService;


        public static BalanceInquiryRequest.Builder newInstance() {
            return new BalanceInquiryRequest.Builder();
        }

        public BalanceInquiryRequest.Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public BalanceInquiryRequest.Builder withReadCardService(ReadCardService readCardService) {
            this.readCardService = readCardService;
            return this;
        }

        public BalanceInquiryRequest.Builder withDeviceController(DeviceController deviceController) {
            this.deviceController = deviceController;
            return this;
        }

        public BalanceInquiryRequest.Builder withTransactionAuthenticator(TransactionAuthenticator transactionAuthenticator) {
            this.transactionAuthenticator = transactionAuthenticator;
            return this;
        }

        public BalanceInquiryRequest.Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public BalanceInquiryRequest.Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public BalanceInquiryRequest.Builder withTransactionRequest(global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest) {
            this.transactionRequest = transactionRequest;
            return this;
        }

        public BalanceInquiryRequest.Builder withLedService(LedService ledService) {
            this.ledService = ledService;
            return this;
        }

        public BalanceInquiryRequest.Builder withSoundService(SoundService soundService) {
            this.soundService = soundService;
            return this;
        }

        public BalanceInquiryRequest build() {
            BalanceInquiryRequest BalanceInquiryRequest = new BalanceInquiryRequest();
            BalanceInquiryRequest.transactionAuthenticator = this.transactionAuthenticator;
            BalanceInquiryRequest.transactionRepository = this.transactionRepository;
            BalanceInquiryRequest.readCardService = this.readCardService;
            BalanceInquiryRequest.deviceController = this.deviceController;
            BalanceInquiryRequest.transactionRequest = this.transactionRequest;
            BalanceInquiryRequest.printerService = this.printerService;
            BalanceInquiryRequest.applicationRepository = this.applicationRepository;
            BalanceInquiryRequest.ledService = this.ledService;
            BalanceInquiryRequest.soundService = this.soundService;
            return BalanceInquiryRequest;
        }
    }
}
