package global.citytech.finpos.processor.neps.reconciliation.clear;

import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reconciliation.clear.BatchClearResponseParameter;

public class BatchClearResponseModel implements BatchClearResponseParameter, UseCase.Response {

    private final boolean isSuccess;
    private final String message;

    public BatchClearResponseModel(boolean isSuccess, String message) {
        this.isSuccess = isSuccess;
        this.message = message;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public String getMessage() {
        return message;
    }
}
