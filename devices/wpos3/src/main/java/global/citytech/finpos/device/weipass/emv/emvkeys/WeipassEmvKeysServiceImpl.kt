package global.citytech.finpos.device.weipass.emv.emvkeys

import com.wiseasy.emvprocess.KernelInit
import com.wiseasy.emvprocess.bean.CAPKData
import global.citytech.finpos.device.weipass.utils.WeipassConstant.EMPTY_EMV_KEYS_LIST
import global.citytech.finpos.device.weipass.utils.WeipassConstant.FAILURE_EMV_KEYS_ERASE
import global.citytech.finpos.device.weipass.utils.WeipassConstant.FAILURE_EMV_KEYS_INJECTION
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SUCCESS_EMV_KEYS_ERASE
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SUCCESS_EMV_KEYS_INJECTION
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finpos.device.weipass.utils.defaultOrEmptyValue
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result

/**
 * Created by Rishav Chudal on 4/29/20.
 */
class WeipassEmvKeysServiceImpl:
    global.citytech.finposframework.hardware.emv.emvkeys.EmvKeysService {

    private lateinit var deviceResponse: DeviceResponse

    /*
     * Emv Keys Injection
     */
    override fun injectEmvKeys(injectionRequest: global.citytech.finposframework.hardware.emv.emvkeys.EmvKeysInjectionRequest): DeviceResponse {
        deviceResponse = DeviceResponse(Result.FAILURE, FAILURE_EMV_KEYS_INJECTION)
        when {
            injectionRequest.emvKeyList.isEmpty() -> {
                deviceResponse = DeviceResponse(Result.FAILURE, EMPTY_EMV_KEYS_LIST)
            }
            else -> {
                eraseAllEmvKeys()
                injectEmvKeysInCore(injectionRequest.emvKeyList)
                deviceResponse = DeviceResponse(Result.SUCCESS, SUCCESS_EMV_KEYS_INJECTION)
                WeipassLogger.log("Emv Keys Injection Completed ...")
            }
        }
        return deviceResponse
    }

    private fun injectEmvKeysInCore(emvKeyList: List<global.citytech.finposframework.hardware.emv.emvkeys.EmvKey>): Int{
        WeipassLogger.log("=============== Injecting Emv Keys ==============")
        var finalResult = 0
        for (emvKey in emvKeyList) {
            try{
                WeipassLogger.log("========== Emv Key ::: ".plus(emvKey.rid).plus(" =========="))
                val coreCapkData = prepareCoreCapkData(emvKey)
                val result = KernelInit.addCAPK(coreCapkData)
                WeipassLogger.log("Emv Key ::: ".plus(emvKey.rid).plus(", injection result ::: "
                    .plus(result)))
                finalResult = finalResult + result
            } catch (exception: Exception) {
                WeipassLogger.log("Exception in Emv Key injection ::: ".plus(exception.message))
            }

        }
        WeipassLogger.log("Emv Keys injection final result ::: ".plus(finalResult))
        return finalResult
    }

    private fun prepareCoreCapkData(emvKey: global.citytech.finposframework.hardware.emv.emvkeys.EmvKey): CAPKData{
        val coreCapkData = CAPKData()

        val ridData = emvKey.rid.defaultOrEmptyValue()
        coreCapkData.rid = ridData
        WeipassLogger.debug("RID ::: ".plus(ridData))

        val keyIndexData = emvKey.index.defaultOrEmptyValue()
        coreCapkData.publicKeyIndex = keyIndexData
        WeipassLogger.debug("Key Index Data ::: ".plus(keyIndexData))

        val keyAlgorithmData = emvKey.keySignatureId.defaultOrEmptyValue()
        coreCapkData.publicKeyAlgorithm = keyAlgorithmData
        WeipassLogger.debug("Key Signature Id ::: ".plus(keyAlgorithmData))

        val keyHashAlgorithmData = emvKey.hashId.defaultOrEmptyValue()
        coreCapkData.publicKeyHashAlgorithm = keyHashAlgorithmData
        WeipassLogger.debug("Key Hash Id ::: ".plus(keyHashAlgorithmData))

        val keyModulusData = emvKey.modulus.defaultOrEmptyValue()
        coreCapkData.publicKeyModulus = keyModulusData
        WeipassLogger.debug("Key Modulus ::: ".plus(keyModulusData))

        val keyExponentData = emvKey.exponent.defaultOrEmptyValue()
        coreCapkData.publicKeyExponent = keyExponentData
        WeipassLogger.debug("Key Exponent ::: ".plus(keyExponentData))

        val keyExpiryData = emvKey.expiryDate.defaultOrEmptyValue()
        coreCapkData.publicKeyExpiredDate = keyExpiryData
        WeipassLogger.debug("Key Expiry Date ::: ".plus(keyExpiryData))

        val checkSumData = emvKey.checkSum.defaultOrEmptyValue()
        coreCapkData.checkValue = checkSumData
        WeipassLogger.debug("Check sum ::: ".plus(checkSumData))

        return coreCapkData
    }

    /*
     * Erase All Emv Keys
     */
    override fun eraseAllEmvKeys(): DeviceResponse {
        WeipassLogger.log("Erasing all the Emv keys in core ...")
        deviceResponse = DeviceResponse(Result.FAILURE, FAILURE_EMV_KEYS_ERASE)
        val result = KernelInit.deleteCAPK()
        WeipassLogger.log("Erase All Emv Keys result ::: ".plus(result))
        when (result) {
            0 -> {
                deviceResponse = DeviceResponse(Result.SUCCESS, SUCCESS_EMV_KEYS_ERASE)
            }
            else -> {
                WeipassLogger.log("Couldn't clear Emv Keys")
            }
        }
        return deviceResponse
    }
}