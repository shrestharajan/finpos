package global.citytech.finpos.processor.nibl.transaction;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import global.citytech.finpos.processor.nibl.transaction.receipt.TransactionReceiptHandler;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.EmvTag;
import global.citytech.finposframework.hardware.io.cards.EMVTagCollection;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
import global.citytech.finposframework.utility.IsoMessageUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

/**
 * Created by Unique Shakya on 10/8/2020.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({TransactionRequest.class, ReadCardResponse.class, TerminalRepository.class,
        EMVTagCollection.class, CardDetails.class, TerminalInfo.class, IsoMessageUtils.class,
        ApplicationRepository.class})
public class TransactionReceiptHandlerTest {

    TransactionRequest transactionRequest;
    ReadCardResponse readCardResponse;
    TerminalRepository terminalRepository;
    EMVTagCollection emvTagCollection;
    CardDetails cardDetails;
    TerminalInfo terminalInfo;
    IsoMessageResponse isoMessageResponse;
    ApplicationRepository applicationRepository;

    @Before
    public void setup() {
        transactionRequest = PowerMockito.mock(TransactionRequest.class);
        readCardResponse = PowerMockito.mock(ReadCardResponse.class);
        terminalRepository = PowerMockito.mock(TerminalRepository.class);
        emvTagCollection = PowerMockito.mock(EMVTagCollection.class);
        cardDetails = PowerMockito.mock(CardDetails.class);
        terminalInfo = PowerMockito.mock(TerminalInfo.class);
        PowerMockito.mockStatic(IsoMessageUtils.class);
        applicationRepository = PowerMockito.mock(ApplicationRepository.class);
    }

    @Test
    public void prepareTest() {
        String applicationCryptogram = "A1B2C3D4E5F67890";
        String aid = "A0000002282010";
        String cid = "40";
        String cvm = "442002";
        String kernelId = "02";
        String tvr = "000000000000";
        String transactionInitializeDate = "2008101127";
        String primaryAccountNumber = "4249720020000234";
        CardSchemeType cardScheme = CardSchemeType.VISA_CREDIT;
        String expireDate = "2412";
        double amount = 100.00;
        String merchantName = "CityTech";
        String merchantAddress = "Kamaladi";
        String merchantId = "1234567890123456";
        String terminalId = "12345678";
        String batchNumber = "001";
        String stan = "000111";
        String invoiceNumber = "000109";
        String rrn = "123456789012";
        String approvalCode = "123456";
        String actionCode = "00";

        global.citytech.finposframework.usecases.TransactionType transactionType = TransactionType.PURCHASE;
        PowerMockito.when(transactionRequest.getTransactionType()).thenReturn(transactionType);

        PowerMockito.when(emvTagCollection.get(IccData.APPLICATION_CRYPTOGRAM.getTag()))
                .thenReturn(new EmvTag(IccData.APPLICATION_CRYPTOGRAM.getTag(), applicationCryptogram, 8));
        PowerMockito.when(emvTagCollection.get(IccData.DF_NAME.getTag()))
                .thenReturn(new EmvTag(IccData.DF_NAME.getTag(), aid, 7));
        PowerMockito.when(emvTagCollection.get(IccData.CID.getTag()))
                .thenReturn(new EmvTag(IccData.CID.getTag(), cid, 1));
        PowerMockito.when(emvTagCollection.get(IccData.CVM.getTag()))
                .thenReturn(new EmvTag(IccData.CVM.getTag(), cvm, 3));
        PowerMockito.when(emvTagCollection.get(IccData.KERNEL_ID.getTag()))
                .thenReturn(new EmvTag(IccData.KERNEL_ID.getTag(), kernelId, 1));
        PowerMockito.when(emvTagCollection.get(IccData.TVR.getTag()))
                .thenReturn(new EmvTag(IccData.TVR.getTag(), tvr, 6));

        PowerMockito.when(cardDetails.getTransactionInitializeDateTime()).thenReturn(transactionInitializeDate);
        PowerMockito.when(cardDetails.getPrimaryAccountNumber()).thenReturn(primaryAccountNumber);
        PowerMockito.when(cardDetails.getCardScheme()).thenReturn(cardScheme);
        PowerMockito.when(cardDetails.getExpiryDate()).thenReturn(expireDate);
        PowerMockito.when(cardDetails.getAmount()).thenReturn(amount);
        PowerMockito.when(cardDetails.getTagCollection()).thenReturn(emvTagCollection);
        PowerMockito.when(cardDetails.getCardType()).thenReturn(CardType.ICC);

        PowerMockito.when(readCardResponse.getCardDetails()).thenReturn(cardDetails);

        PowerMockito.when(terminalInfo.getMerchantName()).thenReturn(merchantName);
        PowerMockito.when(terminalInfo.getMerchantAddress()).thenReturn(merchantAddress);
        PowerMockito.when(terminalInfo.getMerchantID()).thenReturn(merchantId);
        PowerMockito.when(terminalInfo.getTerminalID()).thenReturn(terminalId);

        PowerMockito.when(terminalRepository.findTerminalInfo()).thenReturn(terminalInfo);

        PowerMockito.when(IsoMessageUtils.retrieveFromDataElementsAsString(any(), eq(12)))
                .thenReturn("200810");
        PowerMockito.when(IsoMessageUtils.retrieveFromDataElementsAsString(any(), eq(37)))
                .thenReturn(rrn);
        PowerMockito.when(IsoMessageUtils.retrieveFromDataElementsAsString(any(), eq(38)))
                .thenReturn(approvalCode);
        PowerMockito.when(IsoMessageUtils.retrieveFromDataElementsAsString(any(), eq(39)))
                .thenReturn(actionCode);

        PowerMockito.when(applicationRepository.getMessageTextByActionCode(0)).thenReturn("APPROVED");

        global.citytech.finpos.processor.nibl.transaction.receipt.TransactionReceiptHandler transactionReceiptHandler = new
                TransactionReceiptHandler(transactionRequest, readCardResponse, isoMessageResponse,
                terminalRepository, applicationRepository);
        ReceiptLog receiptLog = transactionReceiptHandler
                .prepare(batchNumber, stan, invoiceNumber);

        assertNotNull(receiptLog);
        assertEquals(merchantName, receiptLog.getRetailer().getRetailerName());
        assertEquals(merchantAddress, receiptLog.getRetailer().getRetailerAddress());
        assertEquals(merchantId, receiptLog.getRetailer().getMerchantId());
        assertEquals(terminalId, receiptLog.getRetailer().getTerminalId());

        assertEquals(transactionInitializeDate, receiptLog.getPerformance().getStartDateTime());
        assertEquals("200810", receiptLog.getPerformance().getEndDateTime());

        assertEquals(batchNumber, receiptLog.getTransactionInfo().getBatchNumber());
        assertEquals(stan, receiptLog.getTransactionInfo().getStan());
        assertEquals(rrn, receiptLog.getTransactionInfo().getRrn());
        assertEquals(invoiceNumber, receiptLog.getTransactionInfo().getInvoiceNumber());
        assertEquals(cardScheme, receiptLog.getTransactionInfo().getCardScheme());
        assertEquals(transactionType.getPrintName(), receiptLog.getTransactionInfo().getPurchaseType());
        assertEquals(primaryAccountNumber, receiptLog.getTransactionInfo().getCardNumber());
        assertEquals(expireDate, receiptLog.getTransactionInfo().getExpireDate());
        assertEquals("100.00", receiptLog.getTransactionInfo().getPurchaseAmount());
        assertEquals("APPROVED", receiptLog.getTransactionInfo().getTransactionStatus());
        assertEquals("CARDHOLDER PIN VERIFIED", receiptLog.getTransactionInfo().getTransactionMessage());
        assertEquals(approvalCode, receiptLog.getTransactionInfo().getApprovalCode());

        assertEquals("DIPPED", receiptLog.getEmvTags().getCardType());
        assertEquals(actionCode, receiptLog.getEmvTags().getResponseCode());
        assertEquals(aid, receiptLog.getEmvTags().getAid());
        assertEquals(tvr, receiptLog.getEmvTags().getTvr());
        assertEquals(cvm, receiptLog.getEmvTags().getCvm());
        assertEquals(cid, receiptLog.getEmvTags().getCid());
        assertEquals(applicationCryptogram, receiptLog.getEmvTags().getAc());
        assertEquals(kernelId, receiptLog.getEmvTags().getKernelId());
    }
}
