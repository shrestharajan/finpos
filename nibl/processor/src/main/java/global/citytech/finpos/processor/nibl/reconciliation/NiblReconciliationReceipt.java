package global.citytech.finpos.processor.nibl.reconciliation;

import global.citytech.finposframework.switches.posmode.PosMode;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationTotals;
import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;

/**
 * Created by Unique Shakya on 9/22/2020.
 */
public class NiblReconciliationReceipt {

    private Retailer retailer;
    private Performance performance;
    private String reconciliationBatchNumber;
    private NiblSettlementTotals reconciliationTotals;
    private String reconciliationResult;
    private String host;
    private String applicationVersion;
    private PosMode posMode;

    private NiblReconciliationReceipt() {
    }

    public PosMode getPosMode() {
        return posMode;
    }

    public Retailer getRetailer() {
        return retailer;
    }

    public Performance getPerformance() {
        return performance;
    }

    public String getReconciliationBatchNumber() {
        return reconciliationBatchNumber;
    }

    public ReconciliationTotals getReconciliationTotals() {
        return reconciliationTotals;
    }

    public String getReconciliationResult() {
        return reconciliationResult;
    }

    public String getHost() {
        return host;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public static final class Builder {
        private Retailer retailer;
        private Performance performance;
        private String reconciliationBatchNumber;
        private NiblSettlementTotals reconciliationTotals;
        private String reconciliationResult;
        private String host;
        private String applicationVersion;
        private PosMode posMode;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withRetailer(Retailer retailer) {
            this.retailer = retailer;
            return this;
        }

        public Builder withPerformance(Performance performance) {
            this.performance = performance;
            return this;
        }

        public Builder withReconciliationBatchNumber(String reconciliationBatchNumber) {
            this.reconciliationBatchNumber = reconciliationBatchNumber;
            return this;
        }

        public Builder withReconciliationTotals(NiblSettlementTotals reconciliationTotals) {
            this.reconciliationTotals = reconciliationTotals;
            return this;
        }

        public Builder withReconciliationResult(String reconciliationResult) {
            this.reconciliationResult = reconciliationResult;
            return this;
        }

        public Builder withHost(String host) {
            this.host = host;
            return this;
        }

        public Builder withApplicationVersion(String applicationVersion) {
            this.applicationVersion = applicationVersion;
            return this;
        }

        public Builder withPosMode(PosMode posMode) {
            this.posMode = posMode;
            return this;
        }

        public NiblReconciliationReceipt build() {
            NiblReconciliationReceipt niblReconciliationReceipt = new NiblReconciliationReceipt();
            niblReconciliationReceipt.retailer = this.retailer;
            niblReconciliationReceipt.reconciliationResult = this.reconciliationResult;
            niblReconciliationReceipt.performance = this.performance;
            niblReconciliationReceipt.reconciliationTotals = this.reconciliationTotals;
            niblReconciliationReceipt.reconciliationBatchNumber = this.reconciliationBatchNumber;
            niblReconciliationReceipt.host = this.host;
            niblReconciliationReceipt.applicationVersion = this.applicationVersion;
            niblReconciliationReceipt.posMode = this.posMode;
            return niblReconciliationReceipt;
        }
    }
}
