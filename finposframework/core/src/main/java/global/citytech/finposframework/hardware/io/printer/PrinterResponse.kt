package global.citytech.finposframework.hardware.io.printer

import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result

class PrinterResponse(result: Result, message: String) :
    DeviceResponse(result, message)