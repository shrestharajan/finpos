plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("android.extensions")
}

android {
    compileSdkVersion(Versions.compileSdkVersion)
    buildToolsVersion(Versions.buildToolVersion)

    defaultConfig {
        minSdkVersion(Versions.minSdkVersion)
        targetSdkVersion(Versions.targetSdkVersion)
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    packagingOptions {
        exclude ("**/Request.class")
    }

}

dependencies {
    implementation(fileTree("dir" to "libs", "include" to listOf("*.jar")))
    api(Libraries.kotlinStdLib)
    api(Libraries.appCompat)
    api(Libraries.kotlinKtx)
    api(Libraries.rxJava)
    api(Libraries.rxAndroid)
    api(Libraries.gson)
    api(Libraries.constraintLayout)
    api("com.google.android.material:material:1.1.0")
    api(Libraries.easydroidCore)
    implementation("com.google.zxing:core:3.3.0")

    testImplementation("junit:junit:4.12")
    testImplementation("io.mockk:mockk:${Versions.mockk}")
    androidTestImplementation(AndroidTest.extJunit)
    androidTestImplementation(AndroidTest.espresso)
    implementation(project(":finposframework:core"))
}
