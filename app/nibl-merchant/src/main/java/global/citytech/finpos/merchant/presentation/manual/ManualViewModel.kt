package global.citytech.finpos.merchant.presentation.manual

import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseViewModel
import global.citytech.finposframework.utility.DateUtils
import java.util.*

/**
 * Created by Rishav Chudal on 11/1/20.
 */
class ManualViewModel: BaseViewModel() {
    var invalidCardNumber = MutableLiveData<Boolean>()
    var invalidExpiryDate = MutableLiveData<Boolean>()
    var invalidCVV = MutableLiveData<Boolean>()
    var validData = MutableLiveData<Boolean>()

    fun validateEnteredData(cardNumber: String, expiryDate: String, cvv: String) {
        if (cardNumber.isEmpty()) {
            this.invalidCardNumber.value = true
        } else if (isInvalidExpiryDate(expiryDate)) {
            this.invalidExpiryDate.value = true
        } else if (cvv.isEmpty() || cvv.length < 3) {
            this.invalidCVV.value = true
        } else {
            this.validData.value = true
        }
    }

    private fun isInvalidExpiryDate(expiryDate: String): Boolean {
        return (isInvalidExpiryDateLength(expiryDate) || isInvalidYear(expiryDate) ||
                isInvalidMonth(expiryDate))
    }

    private fun isInvalidExpiryDateLength(expiryDate: String): Boolean {
        return (expiryDate.isEmpty() || expiryDate.length < 4)
    }

    private fun isInvalidYear(expiryDate: String): Boolean {
        val enteredYear = Integer.valueOf(expiryDate.substring(2, 4))
        val currentYear = Integer.valueOf(DateUtils.getCurrentYearLastTwoDigits())
        return ((enteredYear == 0) || (enteredYear < currentYear))
    }

    private fun isInvalidMonth(expiryDate: String): Boolean {
        val enteredYear = Integer.valueOf(expiryDate.substring(2, 4))
        val currentYear = Integer.valueOf(DateUtils.getCurrentYearLastTwoDigits())
        val enteredMonth = Integer.valueOf(expiryDate.substring(0, 2))
        val currentMonth = Integer.valueOf(DateUtils.getCurrentMonth())
        return if(enteredYear == currentYear){
            ((enteredMonth == 0) || (enteredMonth > 12) || (enteredMonth < currentMonth))
        } else {
            ((enteredMonth == 0) || (enteredMonth > 12))
        }
    }

}