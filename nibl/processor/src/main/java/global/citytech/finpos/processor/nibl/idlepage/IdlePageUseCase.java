//package global.citytech.finpos.processor.nibl.idlepage;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
//import global.citytech.finpos.nibl.iso8583.ProcessingCode;
//import global.citytech.finpos.nibl.iso8583.S2MIccDataList;
//import global.citytech.finpos.nibl.iso8583.requestsender.purchase.IccPurchaseRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.purchase.PurchaseRequest;
//import global.citytech.finpos.nibl.iso8583.requestsender.purchase.PurchaseRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
//import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
//import global.citytech.finpos.processor.nibl.transaction.TransactionUseCase;
//import global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor;
//import global.citytech.finpos.processor.nibl.transaction.purchase.PurchaseRequestModel;
//import global.citytech.finpos.processor.nibl.transaction.purchase.PurchaseResponseModel;
//import global.citytech.finpos.processor.nibl.transaction.receipt.NiblTransactionReceiptPrintHandler;
//import global.citytech.finpos.processor.nibl.transaction.receipt.ReversedTransactionReceiptHandler;
//import global.citytech.finpos.processor.nibl.transaction.reversal.ReversalRequestModel;
//import global.citytech.finpos.processor.nibl.transaction.reversal.ReversalUseCase;
//import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils;
//import global.citytech.finposframework.exceptions.FinPosException;
//import global.citytech.finposframework.exceptions.PosError;
//import global.citytech.finposframework.exceptions.PosException;
//import global.citytech.finposframework.hardware.io.cards.CardDetails;
//import global.citytech.finposframework.hardware.io.cards.CardType;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
//import global.citytech.finposframework.hardware.io.led.LedService;
//import global.citytech.finposframework.iso8583.DataElement;
//import global.citytech.finposframework.iso8583.IsoMessageResponse;
//import global.citytech.finposframework.iso8583.RequestContext;
//import global.citytech.finposframework.notifier.Notifier;
//import global.citytech.finposframework.repositories.ApplicationRepository;
//import global.citytech.finposframework.repositories.TerminalRepository;
//import global.citytech.finposframework.repositories.TransactionRepository;
//import global.citytech.finposframework.supports.UseCase;
//import global.citytech.finposframework.switches.PosEntryMode;
//import global.citytech.finposframework.switches.transaction.TransactionRequester;
//import global.citytech.finposframework.usecases.TransactionType;
//import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
//import global.citytech.finposframework.usecases.transaction.TransactionLog;
//import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
//import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
//import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
//import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
//import global.citytech.finposframework.utility.HelperUtils;
//import global.citytech.finposframework.utility.IsoMessageUtils;
//import global.citytech.finposframework.utility.JsonUtils;
//import global.citytech.finposframework.utility.StringUtils;
//
///**
// * Created by Rishav Chudal on 11/23/20.
// */
//public class IdlePageUseCase extends TransactionUseCase
//        implements UseCase<PurchaseRequestModel, PurchaseResponseModel>,
//        TransactionRequester<PurchaseRequestModel, PurchaseResponseModel> {
//
//    private PurchaseRequest purchaseRequest;
//
//    public IdlePageUseCase(TerminalRepository terminalRepository, Notifier notifier) {
//        super(terminalRepository, notifier);
//    }
//
//    @Override
//    protected String getClassName() {
//        return IdlePageUseCase.class.getSimpleName();
//    }
//
//    @Override
//    public PurchaseResponseModel execute(PurchaseRequestModel purchaseRequestModel) {
//        try {
//            logger.log("::: REACHED IDLE PAGE PURCHASE USE CASE :::");
//            String stan = this.terminalRepository.getSystemTraceAuditNumber();
//            String invoiceNumber = this.terminalRepository.getInvoiceNumber();
//            String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
//            TransactionRequest transactionRequest = purchaseRequestModel.getTransactionRequest();
//            this.transactionRepository = purchaseRequestModel.getTransactionRepository();
//            ReadCardRequest readCardRequest = prepareIdleReadCardRequest(
//                    stan,
//                    purchaseRequestModel.getApplicationRepository(),
//                    transactionRequest
//            );
//            this.notifier.notify(
//                    Notifier.EventType.READING_CARD,
//                    "Reading Card..."
//            );
//            ReadCardResponse readCardResponse = this.readEmv(
//                    readCardRequest,
//                    purchaseRequestModel.getReadCardService()
//            );
//            CardType detectedCardType = readCardResponse.getCardDetails().getCardType();
//            this.logger.debug("::: IDLE PAGE USE CASE ::: CARD TYPE === " + detectedCardType);
//            if (detectedCardType == CardType.ICC) {
//                this.cardProcessor = new IccCardProcessor(
//                        purchaseRequestModel.getReadCardService(),
//                        purchaseRequestModel.getTransactionRepository(),
//                        purchaseRequestModel.getApplicationRepository(),
//                        purchaseRequestModel.getTransactionAuthenticator(),
//                        this.notifier,
//                        this.terminalRepository
//                );
//            } else {
//                throw new PosException(PosError.DEVICE_ERROR_INVALID_CARD);
//            }
//            readCardResponse = this.cardProcessor.validateReadCardResponse(readCardRequest, readCardResponse);
//            if (!isValidTransaction(
//                    purchaseRequestModel.getApplicationRepository(),
//                    transactionRequest,
//                    readCardResponse
//            )) {
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
//            }
//            if (!this.isTransactionAuthorizationSuccess(
//                    purchaseRequestModel.getTransactionAuthenticator(),
//                    purchaseRequestModel.getApplicationRepository(),
//                    transactionRequest.getTransactionType(),
//                    readCardResponse
//            ))
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
//            global.citytech.finposframework.usecases
//                    .transaction
//                    .data
//                    .PurchaseRequest purchaseRequest = preparePurchaseCardRequest(
//                    transactionRequest,
//                    readCardResponse.getCardDetails()
//            );
//            readCardResponse = this.cardProcessor.processCard(stan, purchaseRequest);
//            logger.debug("::: READ CARD RESPONSE ::: " + readCardResponse.toString());
//            this.notifier.notify(
//                    Notifier.EventType.TRANSMITTING_REQUEST,
//                    "Transmitting Request.."
//            );
//            try {
//                IsoMessageResponse purchaseIsoMessageResponse = this.sendIsoMessage(
//                        stan,
//                        invoiceNumber,
//                        readCardResponse,
//                        purchaseRequestModel
//                );
//                this.notifier.notify(
//                        Notifier.EventType.RESPONSE_RECEIVED,
//                        "Please wait.."
//                );
//                InvalidResponseHandlerResponse invalidResponseHandlerResponse
//                        = this.checkForInvalidResponseFromHost(purchaseIsoMessageResponse);
//                if (invalidResponseHandlerResponse.isInvalid())
//                    return this.onInvalidResponseFromHost(
//                            transactionRequest,
//                            invalidResponseHandlerResponse,
//                            batchNumber,
//                            stan,
//                            invoiceNumber,
//                            readCardResponse,
//                            purchaseRequestModel
//                    );
//                boolean isPurchaseApproved = this.isTransactionApprovedByActionCode(purchaseIsoMessageResponse);
//                this.notifier.notify(
//                        Notifier.EventType.PROCESSING_ISSUER_SCRIPT,
//                        "Processing..."
//                );
//                readCardResponse = this.cardProcessor.processOnlineResult(
//                        readCardResponse,
//                        purchaseIsoMessageResponse
//                );
//                if (this.isTransactionDeclineByCardEvenIfSuccessOnSwitch(purchaseIsoMessageResponse, readCardResponse))
//                    return this.handleApprovedTransactionDeclinedByCard(stan, batchNumber, invoiceNumber,
//                            readCardResponse, purchaseRequestModel, transactionRequest);
//                this.notifyTransactionCompletion(
//                        stan,
//                        batchNumber,
//                        invoiceNumber,
//                        isPurchaseApproved,
//                        transactionRequest,
//                        purchaseRequestModel.getTransactionRepository(),
//                        purchaseIsoMessageResponse,
//                        readCardResponse,
//                        purchaseRequestModel.getApplicationRepository(),
//                        purchaseRequestModel.getLedService()
//                );
//                boolean shouldPrint = this.printReceipt(batchNumber, stan, invoiceNumber,
//                        purchaseRequestModel.getPrinterService(),
//                        purchaseRequestModel.getTransactionRequest(),
//                        purchaseRequestModel.getTransactionRepository(),
//                        readCardResponse,
//                        purchaseIsoMessageResponse,
//                        purchaseRequestModel.getApplicationRepository()
//                );
//                return this.prepareResponse(stan,
//                        purchaseIsoMessageResponse,
//                        isPurchaseApproved,
//                        purchaseRequestModel.getApplicationRepository(),
//                        shouldPrint
//                );
//            } catch (FinPosException ex) {
//                if (this.isResponseNotReceivedException(ex))
//                    return this.onTransactionTimeout(
//                            transactionRequest,
//                            batchNumber,
//                            stan,
//                            invoiceNumber,
//                            readCardResponse,
//                            purchaseRequestModel
//                    );
//                else if (ex.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT) {
//                    logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED UNABLE TO CONNECT");
//                    transactionRepository.removeAutoReversal(this.autoReversal);
//                    throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
//                } else {
//                    throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//                }
//            }
//        } catch (
//                PosException ex) {
//            ex.printStackTrace();
//            throw ex;
//        } catch (
//                Exception ex) {
//            ex.printStackTrace();
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        }
//
//    }
//
//    private PurchaseResponseModel handleApprovedTransactionDeclinedByCard(String stan, String batchNumber,
//                                                                          String invoiceNumber,
//                                                                          ReadCardResponse readCardResponse,
//                                                                          PurchaseRequestModel purchaseRequestModel,
//                                                                          TransactionRequest transactionRequest) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, ReasonForReversal.Type.DECLINED_BY_CARD.getDescription());
//        this.performReversal(stan, readCardResponse, purchaseRequestModel);
//        boolean shouldPrint = !this.approvePrintOnly(purchaseRequestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest, readCardResponse,
//                    this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.DECLINED_BY_CARD,
//                            ReasonForReversal.Type.DECLINED_BY_CARD.getDescription()));
//            new NiblTransactionReceiptPrintHandler(purchaseRequestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            purchaseRequestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PurchaseResponseModel.Builder.createDefaultBuilder()
//                .isApproved(false)
//                .message(ReasonForReversal.Type.DECLINED_BY_CARD.getDescription())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private ReadCardRequest prepareIdleReadCardRequest(
//            String stan,
//            ApplicationRepository applicationRepository,
//            TransactionRequest transactionRequest
//    ) {
//        List<CardType> cardTypeList = new ArrayList<>();
//        cardTypeList.add(CardType.ICC);
//        ReadCardRequest readCardRequest = new ReadCardRequest(
//                cardTypeList,
//                transactionRequest.getTransactionType(),
//                TransactionUtils.retrieveTransactionType9C(transactionRequest.getTransactionType()),
//                applicationRepository.retrieveEmvParameterRequest()
//        );
//        readCardRequest.setStan(stan);
//        readCardRequest.setAmount(transactionRequest.getAmount());
//        readCardRequest.setCashBackAmount(transactionRequest.getAdditionalAmount());
//        readCardRequest.setTransactionDate(this.retrieveTransactionDate());
//        readCardRequest.setTransactionTime(this.retrieveTransactionTime());
//        readCardRequest.setMultipleAidSelectionListener(this.multipleAidSelectionListener);
//        readCardRequest.setIccDataList(S2MIccDataList.get());
//        readCardRequest.setCurrencyName(this.terminalRepository.getTerminalTransactionCurrencyName());
//        readCardRequest.setPinPadFixedLayout(!terminalRepository.shouldShufflePinPad());
//        return readCardRequest;
//    }
//
//    private ReadCardResponse readEmv(
//            ReadCardRequest readCardRequest,
//            ReadCardService readCardService
//    ) {
//        ReadCardResponse readCardResponse = readCardService.readEmv(readCardRequest);
//        this.validateReadCardResponse(readCardResponse);
//        return readCardResponse;
//    }
//
//    private global.citytech.finposframework.usecases.transaction.data.PurchaseRequest preparePurchaseCardRequest(
//            TransactionRequest transactionRequest,
//            CardDetails cardDetails
//    ) {
//        global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest
//                = new global.citytech.finposframework.usecases.transaction.data.PurchaseRequest(
//                transactionRequest.getTransactionType(),
//                transactionRequest.getAmount(),
//                transactionRequest.getAdditionalAmount()
//        );
//        cardDetails.setAmount(transactionRequest.getAmount());
//        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
//        purchaseRequest.setCardDetails(cardDetails);
//        purchaseRequest.setFallbackIccToMag(transactionRequest.isFallbackIccToMag());
//        return purchaseRequest;
//    }
//
//    private IsoMessageResponse sendIsoMessage(
//            String stan,
//            String invoiceNumber,
//            ReadCardResponse readCardResponse,
//            PurchaseRequestModel purchaseRequestModel) {
//        RequestContext context = this.prepareTemplateRequestContext(stan);
//        logger.debug(String.format(" Request Context ::%s", context));
//        this.purchaseRequest = preparePurchaseRequest(invoiceNumber, readCardResponse);
//        context.setRequest(this.purchaseRequest);
//        this.autoReversal = new AutoReversal(
//                TransactionType.PURCHASE,
//                JsonUtils.toJsonObj(prepareReversalRequest(stan, readCardResponse, purchaseRequestModel)),
//                0,
//                AutoReversal.Status.ACTIVE,
//                StringUtils.dateTimeStamp()
//        );
//        this.transactionRepository.saveAutoReversal(this.autoReversal);
//        PurchaseRequestSender sender = new IccPurchaseRequestSender(new NIBLSpecInfo(), context);
//        IsoMessageResponse isoMessageResponse = sender.send();
//        this.transactionRepository.removeAutoReversal(this.autoReversal);
//        return isoMessageResponse;
//    }
//
//    private PurchaseRequest preparePurchaseRequest(
//            String invoiceNumber,
//            ReadCardResponse readCardResponse
//    ) {
//        PurchaseRequest request = new PurchaseRequest();
//        request.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        request.setTransactionAmount(readCardResponse.getCardDetails().getAmount());
//        request.setPosEntryMode(PosEntryMode.ICC); //TODO Change Later
//        request.setPosConditionCode("00"); //TODO Change later
//        request.setTrack2Data(readCardResponse.getCardDetails().getTrackTwoData());
//        if (readCardResponse.getCardDetails().getPinBlock().getOfflinePin() ||
//                StringUtils.isEmpty(readCardResponse.getCardDetails().getPinBlock().getPin()) ||
//                readCardResponse.getCardDetails().getPinBlock().getPin().equalsIgnoreCase("offline"))
//            request.setPinBlock("");
//        else
//            request.setPinBlock(readCardResponse.getCardDetails().getPinBlock().getPin());
//        request.setEmvData(readCardResponse.getCardDetails().getIccDataBlock());
//        request.setExpireDate(readCardResponse.getCardDetails().getExpiryDate());
//        request.setCardSequenceNumber(
//                readCardResponse.getCardDetails().getPrimaryAccountNumberSerialNumber()
//        );
//        request.setLocalTime(
//                HelperUtils.getDateAfterConversion(
//                        HelperUtils.DATE_FORMAT_yyMMddHHmmssSSS,
//                        readCardResponse.getCardDetails().getTransactionInitializeDateTime(),
//                        HelperUtils.DATE_FORMAT_HHmmss
//                )
//        );
//        request.setLocalDate(
//                HelperUtils.getDateAfterConversion(
//                        HelperUtils.DATE_FORMAT_yyMMddHHmmssSSS,
//                        readCardResponse.getCardDetails().getTransactionInitializeDateTime(),
//                        HelperUtils.DATE_FORMAT_MMdd
//                )
//        );
//        request.setInvoiceNumber(invoiceNumber);
//        return request;
//    }
//
//    private PurchaseResponseModel onInvalidResponseFromHost(
//            TransactionRequest transactionRequest,
//            InvalidResponseHandlerResponse invalidResponseHandlerResponse,
//            String batchNumber,
//            String stan,
//            String invoiceNumber,
//            ReadCardResponse readCardResponse,
//            PurchaseRequestModel purchaseRequestModel
//    ) {
//        this.notifier.notify(
//                Notifier.EventType.TRANSACTION_DECLINED,
//                invalidResponseHandlerResponse.getMessage()
//        );
//        this.performReversal(stan, readCardResponse, purchaseRequestModel);
//        boolean shouldPrint = !this.approvePrintOnly(purchaseRequestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(
//                    transactionRequest,
//                    readCardResponse,
//                    this.terminalRepository
//            ).prepare(
//                    batchNumber,
//                    stan,
//                    invoiceNumber,
//                    new ReasonForReversal(
//                            ReasonForReversal.Type.INVALID_RESPONSE,
//                            invalidResponseHandlerResponse.getMessage()
//                    )
//            );
//            new NiblTransactionReceiptPrintHandler(purchaseRequestModel.getPrinterService())
//                    .printTransactionReceipt(
//                            receiptLog,
//                            ReceiptVersion.MERCHANT_COPY
//                    );
//            purchaseRequestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PurchaseResponseModel.Builder.createDefaultBuilder()
//                .isApproved(false)
//                .message(invalidResponseHandlerResponse.getMessage())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private void notifyTransactionCompletion(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean purchaseApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository,
//            LedService ledService
//    ) {
//        this.updateTransactionLog(
//                stan,
//                batchNumber,
//                invoiceNumber,
//                purchaseApproved,
//                transactionRequest,
//                transactionRepository,
//                isoMessageResponse,
//                readCardResponse,
//                applicationRepository
//        );
//        this.updateTransactionIds(purchaseApproved);
//        this.notifyTransactionStatus(
//                purchaseApproved,
//                isoMessageResponse,
//                applicationRepository,
//                ledService,
//                readCardResponse.getCardDetails().getCardType() == CardType.PICC
//        );
//    }
//
//    private void updateTransactionLog(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean purchaseApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository
//    ) {
//        this.logger.log("::: NIBL ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
//        this.logger.log("::: NIBL ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
//        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
//                .withStan(stan)
//                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
//                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
//                .withInvoiceNumber(invoiceNumber)
//                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
//                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
//                .withTransactionType(TransactionType.PURCHASE)
//                .withOriginalTransactionType(TransactionType.PURCHASE)
//                .withTransactionAmount(transactionRequest.getAmount())
//                .withOriginalTransactionAmount(transactionRequest.getAmount())
//                .withTransactionDate(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_DATE))
//                .withTransactionTime(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_TIME))
//                .withTransactionStatus(purchaseApproved ? "APPROVED" : "DECLINED")
//                .withPosEntryMode(PosEntryMode.ICC)
//                .withOriginalPosEntryMode(PosEntryMode.ICC)
//                .withPosConditionCode(this.purchaseRequest.getPosConditionCode())
//                .withOriginalPosConditionCode("")
//                .withReconcileStatus("")
//                .withReconcileTime("")
//                .withReconcileDate("")
//                .withReconcileBatchNo(batchNumber)
//                .withReadCardResponse(readCardResponse)
//                .withAuthorizationCompleted(true)
//                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
//                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse, applicationRepository, batchNumber, stan, invoiceNumber))
//                .withTransactionVoided(false)
//                .withTipAdjusted(false)
//                .build();
//        transactionRepository.updateTransactionLog(transactionLog);
//    }
//
//    private PurchaseResponseModel prepareResponse(
//            String stan,
//            IsoMessageResponse response,
//            boolean isPurchaseApproved,
//            ApplicationRepository applicationRepository,
//            boolean shouldPrint
//    ) {
//        PurchaseResponseModel.Builder builder = PurchaseResponseModel.Builder.createDefaultBuilder();
//        builder.stan(stan);
//        builder.isApproved(isPurchaseApproved);
//        builder.debugRequestMessage(response.getDebugRequestString());
//        builder.debugResponseMessage(response.getDebugResponseString());
//        builder.message(this.retrieveMessage(response, isPurchaseApproved, applicationRepository));
//        builder.shouldPrintCustomerCopy(shouldPrint);
//        return builder.build();
//    }
//
//    private PurchaseResponseModel onTransactionTimeout(
//            TransactionRequest transactionRequest,
//            String batchNumber,
//            String stan,
//            String invoiceNumber,
//            ReadCardResponse readCardResponse,
//            PurchaseRequestModel request
//    ) {
//        this.notifier.notify(
//                Notifier.EventType.TRANSACTION_DECLINED,
//                "TIMEOUT"
//        );
//        this.setUnableToGoOnlineResultToCard(readCardResponse, request.getReadCardService());
//        this.performReversal(stan, readCardResponse, request);
//        boolean shouldPrint = !this.approvePrintOnly(request.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(
//                    transactionRequest,
//                    readCardResponse,
//                    this.terminalRepository
//            ).prepare(
//                    batchNumber,
//                    stan,
//                    invoiceNumber,
//                    new ReasonForReversal(
//                            ReasonForReversal.Type.TIMEOUT,
//                            "Timeout"
//                    )
//            );
//            new NiblTransactionReceiptPrintHandler(request.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            request.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PurchaseResponseModel.Builder.createDefaultBuilder()
//                .message("TIMEOUT")
//                .isApproved(false)
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private void performReversal(
//            String stan,
//            ReadCardResponse readCardResponse,
//            PurchaseRequestModel purchaseRequestModel
//    ) {
//        ReversalUseCase reversalUseCase = new ReversalUseCase(
//                this.prepareReversalRequest(stan, readCardResponse, purchaseRequestModel),
//                this.terminalRepository,
//                this.notifier
//        );
//        try {
//            reversalUseCase.execute(
//                    prepareReversalRequestModel(
//                            purchaseRequestModel,
//                            readCardResponse
//                    )
//            );
//            this.transactionRepository.removeAutoReversal(this.autoReversal);
//        } catch (FinPosException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private NiblReversalRequest prepareReversalRequest(
//            String stan,
//            ReadCardResponse readCardResponse,
//            PurchaseRequestModel purchaseRequestModel
//    ) {
//        NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
//        niblReversalRequest.setProcessingCode(ProcessingCode.PURCHASE.getCode());
//        niblReversalRequest.setAmount(
//                purchaseRequestModel.getTransactionRequest().getAmount()
//        );
//        niblReversalRequest.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        niblReversalRequest.setExpiryDate(readCardResponse.getCardDetails().getExpiryDate());
//        niblReversalRequest.setPosEntryMode(this.purchaseRequest.getPosEntryMode());
//        niblReversalRequest.setPosConditionCode(this.purchaseRequest.getPosConditionCode());
//        niblReversalRequest.setTrack2Data(this.purchaseRequest.getTrack2Data());
//        niblReversalRequest.setPinBlock(this.purchaseRequest.getPinBlock());
//        niblReversalRequest.setInvoiceNumber(this.purchaseRequest.getInvoiceNumber());
//        niblReversalRequest.setStan(stan);
//        return niblReversalRequest;
//    }
//
//    private ReversalRequestModel prepareReversalRequestModel(
//            PurchaseRequestModel purchaseRequestModel,
//            ReadCardResponse readCardResponse
//    ) {
//        return ReversalRequestModel.Builder.newInstance()
//                .withTransactionRequest(
//                        purchaseRequestModel.getTransactionRequest()
//                )
//                .withTransactionRepository(
//                        purchaseRequestModel.getTransactionRepository()
//                )
//                .withPrinterService(
//                        purchaseRequestModel.getPrinterService()
//                )
//                .withApplicationRepository(
//                        purchaseRequestModel.getApplicationRepository()
//                )
//                .withReadCardResponse(readCardResponse)
//                .build();
//    }
//
//}
