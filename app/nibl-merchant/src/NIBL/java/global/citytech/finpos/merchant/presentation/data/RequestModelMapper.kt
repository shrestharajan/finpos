package global.citytech.finpos.merchant.presentation.data

import global.citytech.common.extensions.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.datasource.core.preauth.completion.AuthorisationUtils
import global.citytech.finpos.merchant.domain.model.auth.completion.AuthorisationCompletionRequestEntity
import global.citytech.finpos.merchant.domain.model.autoreversal.AutoReversalRequestEntity
import global.citytech.finpos.merchant.domain.model.balanceinquiry.BalanceInquiryRequestEntity
import global.citytech.finpos.merchant.domain.model.cashadvance.CashAdvanceRequestEntity
import global.citytech.finpos.merchant.domain.model.cashin.CashInRequestEntity
import global.citytech.finpos.merchant.domain.model.greenpin.GreenPinRequestEntity
import global.citytech.finpos.merchant.domain.model.logon.LogOnRequestEntity
import global.citytech.finpos.merchant.domain.model.pinchange.PinChangeRequestEntity
import global.citytech.finpos.merchant.domain.model.ministatement.MiniStatementRequestEntity
import global.citytech.finpos.merchant.domain.model.preauth.PreAuthRequestEntity
import global.citytech.finpos.merchant.domain.model.purchase.PurchaseRequestEntity
import global.citytech.finpos.merchant.domain.model.purchase.RefundRequestEntity
import global.citytech.finpos.merchant.domain.model.receipt.CustomerCopyRequestEntity
import global.citytech.finpos.merchant.domain.model.receipt.detailreport.DetailReportRequestEntity
import global.citytech.finpos.merchant.domain.model.receipt.duplicate.DuplicateReceiptRequestEntity
import global.citytech.finpos.merchant.domain.model.receipt.duplicatereconciliation.DuplicateReconciliationReceiptRequestEntity
import global.citytech.finpos.merchant.domain.model.receipt.summaryreport.SummaryReportRequestEntity
import global.citytech.finpos.merchant.domain.model.reconciliation.ReconciliationRequestEntity
import global.citytech.finpos.merchant.domain.model.reconciliation.check.CheckReconciliationRequestEntity
import global.citytech.finpos.merchant.domain.model.reconciliation.clear.BatchClearRequestEntity
import global.citytech.finpos.merchant.domain.model.tip.TipAdjustmentRequestEntity
import global.citytech.finpos.merchant.domain.model.voidsale.VoidSaleRequestEntity
import global.citytech.finpos.merchant.framework.datasource.core.TransactionAuthenticatorImpl
import global.citytech.finpos.processor.nibl.balanceinquiry.BalanceInquiryRequest
import global.citytech.finpos.processor.nibl.cashin.CashInRequest
import global.citytech.finpos.processor.nibl.greenpin.GreenPinRequest
import global.citytech.finpos.processor.nibl.logon.LogOnRequestModel
import global.citytech.finpos.processor.nibl.pinchange.PinChangeRequest
import global.citytech.finpos.processor.nibl.ministatement.MiniStatementRequest
import global.citytech.finpos.processor.nibl.posmode.PosModeRequestModel
import global.citytech.finpos.processor.nibl.receipt.customercopy.CustomerCopyRequestModel
import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportRequestModel
import global.citytech.finpos.processor.nibl.receipt.duplicatereceipt.DuplicateReceiptRequestModel
import global.citytech.finpos.processor.nibl.receipt.duplicatereconciliation.DuplicateReconciliationReceiptRequestModel
import global.citytech.finpos.processor.nibl.receipt.summaryreport.SummaryReportRequestModel
import global.citytech.finpos.processor.nibl.reconciliation.ReconciliationRequestModel
import global.citytech.finpos.processor.nibl.reconciliation.check.CheckReconciliationRequestModel
import global.citytech.finpos.processor.nibl.reconciliation.clear.BatchClearRequestModel
import global.citytech.finpos.processor.nibl.transaction.authorisation.completion.AuthorisationCompletionRequestModel
import global.citytech.finpos.processor.nibl.transaction.authorisation.preauth.PreAuthRequestModel
import global.citytech.finpos.processor.nibl.transaction.autoreversal.AutoReversalRequestModel
import global.citytech.finpos.processor.nibl.transaction.cash.advance.CashAdvanceRequestModel
import global.citytech.finpos.processor.nibl.transaction.purchase.PurchaseRequestModel
import global.citytech.finpos.processor.nibl.transaction.refund.RefundRequestModel
import global.citytech.finpos.processor.nibl.transaction.tipadjustment.TipAdjustmentRequestModel
import global.citytech.finpos.processor.nibl.transaction.voidsale.VoidSaleRequestModel
import global.citytech.finpos.processor.nibl.transactiontype.TransactionTypeRequestModel


/**
 * Created by Saurav Ghimire on 1/20/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
fun LogOnRequestEntity.mapToModel(): LogOnRequestModel {
    return LogOnRequestModel(printerService, hardwareKeyService, applicationPackageName)
}

fun CustomerCopyRequestEntity.mapToModel(): CustomerCopyRequestModel {
    return CustomerCopyRequestModel.Builder
        .newInstance()
        .withPrinterService(printerService)
        .withMiniStatementDate(statementList)
        .withTransactionRepository(transactionRepository)
        .build();
}

fun mapToModel(merchantCategoryCode: String): PosModeRequestModel {
    return PosModeRequestModel(merchantCategoryCode)
}

fun mapToModel(): TransactionTypeRequestModel {
    return TransactionTypeRequestModel()
}

fun PurchaseRequestEntity.mapToModel(): PurchaseRequestModel {
    return PurchaseRequestModel.Builder
        .newInstance()
        .withReadCardService(readCardService)
        .withDeviceController(deviceController)
        .withPrinterService(printerService)
        .withTransactionAuthenticator(transactionAuthenticator)
        .withTransactionRepository(transactionRepository)
        .withTransactionRequest(transactionRequest)
        .withApplicationRepository(applicationRepository)
        .withLedService(ledService)
        .withSoundService(soundService)
        .build()
}

fun GreenPinRequestEntity. mapToModel(): GreenPinRequest {

    return GreenPinRequest.Builder
        .newInstance()
        .withReadCardService(readCardService)
        .withDeviceController(deviceController)
        .withPrinterService(printerService)
        .withTransactionAuthenticator(transactionAuthenticator)
        .withTransactionRepository(transactionRepository)
        .withTransactionRequest(transactionRequest)
        .withApplicationRepository(applicationRepository)
        .withLedService(ledService)
        .withSoundService(soundService)
        .build()
}


fun PinChangeRequestEntity.mapToModel(): PinChangeRequest {

    return PinChangeRequest.Builder
        .newInstance()
        .withReadCardService(readCardService)
        .withDeviceController(deviceController)
        .withPrinterService(printerService)
        .withTransactionAuthenticator(transactionAuthenticator)
        .withTransactionRepository(transactionRepository)
        .withTransactionRequest(transactionRequest)
        .withApplicationRepository(applicationRepository)
        .withLedService(ledService)
        .withSoundService(soundService)
        .build()
}

fun MiniStatementRequestEntity.mapToModel(): MiniStatementRequest {

    return MiniStatementRequest.Builder
        .newInstance()
        .withReadCardService(readCardService)
        .withDeviceController(deviceController)
        .withPrinterService(printerService)
        .withTransactionAuthenticator(transactionAuthenticator)
        .withTransactionRepository(transactionRepository)
        .withTransactionRequest(transactionRequest)
        .withApplicationRepository(applicationRepository)
        .withLedService(ledService)
        .withSoundService(soundService)
        .build()
}

fun BalanceInquiryRequestEntity.mapToModel(): BalanceInquiryRequest {
    return BalanceInquiryRequest.Builder
        .newInstance()
        .withReadCardService(readCardService)
        .withDeviceController(deviceController)
        .withPrinterService(printerService)
        .withTransactionAuthenticator(transactionAuthenticator)
        .withTransactionRepository(transactionRepository)
        .withTransactionRequest(transactionRequest)
        .withApplicationRepository(applicationRepository)
        .withLedService(ledService)
        .withSoundService(soundService)
        .build()
}

fun CashInRequestEntity.mapToModel(): CashInRequest {
    return CashInRequest.Builder
        .newInstance()
        .withReadCardService(readCardService)
        .withDeviceController(deviceController)
        .withPrinterService(printerService)
        .withTransactionAuthenticator(transactionAuthenticator)
        .withTransactionRepository(transactionRepository)
        .withTransactionRequest(transactionRequest)
        .withApplicationRepository(applicationRepository)
        .withLedService(ledService)
        .withSoundService(soundService)
        .build()
}


fun PreAuthRequestEntity.mapToModel(): PreAuthRequestModel {
    return PreAuthRequestModel.Builder
        .newInstance()
        .withApplicationRepository(applicationRepository)
        .withDeviceController(deviceController)
        .withLedService(ledService)
        .withPrinterService(printerService)
        .withReadCardService(readCardService)
        .withSoundService(soundService)
        .withTransactionAuthenticator(transactionAuthenticator)
        .withTransactionRepository(transactionRepository)
        .withTransactionRequest(transactionRequest)
        .build()
}

fun VoidSaleRequestEntity.mapToModel(): VoidSaleRequestModel {
    return VoidSaleRequestModel.Builder
        .newInstance()
        .withApplicationRepository(applicationRepository)
        .withDeviceController(deviceController)
        .withLedService(ledService)
        .withPrinterService(printerService)
        .withReadCardService(readCardService)
        .withSoundService(soundService)
        .withTransactionAuthenticator(transactionAuthenticator)
        .withTransactionRepository(transactionRepository)
        .withTransactionRequest(transactionRequest)
        .build()
}

fun AuthorisationCompletionRequestEntity.mapToModel(): AuthorisationCompletionRequestModel {
    return AuthorisationCompletionRequestModel.Builder.newInstance()
        .withTransactionRepository(transactionRepository)
        .withTransactionRequest(
            AuthorisationUtils.prepareAuthorisationCompletionRequest(
                authorisationCompletionRequest!!
            )
        )
        .withDeviceController(deviceController)
        .withReadCardService(readCardService)
        .withTransactionAuthenticator(TransactionAuthenticatorImpl())
        .withPrinterService(printerService)
        .withApplicationRepository(applicationRepository)
        .withLedService(ledService)
        .withSoundService(soundService)
        .build()
}

fun RefundRequestEntity.mapToModel(): RefundRequestModel {
    return RefundRequestModel.Builder.newInstance()
        .withTransactionRepository(transactionRepository)
        .withTransactionRequest(transactionRequest)
        .withDeviceController(deviceController)
        .withReadCardService(readCardService)
        .withTransactionAuthenticator(transactionAuthenticator)
        .withPrinterService(printerService)
        .withApplicationRepository(applicationRepository)
        .withLedService(ledService)
        .withSoundService(soundService)
        .build()
}

fun CashAdvanceRequestEntity.mapToModel(): CashAdvanceRequestModel {
    return CashAdvanceRequestModel.Builder
        .newInstance()
        .withReadCardService(readCardService)
        .withDeviceController(deviceController)
        .withPrinterService(printerService)
        .withTransactionAuthenticator(transactionAuthenticator)
        .withTransactionRepository(transactionRepository)
        .withTransactionRequest(transactionRequest)
        .withApplicationRepository(applicationRepository)
        .withLedService(ledService)
        .withSoundService(soundService)
        .build()
}

fun TipAdjustmentRequestEntity.mapToModel(): TipAdjustmentRequestModel {
    return TipAdjustmentRequestModel.Builder.newInstance()
        .deviceController(deviceController)
        .printerService(printerService)
        .transactionRepository(transactionRepository)
        .transactionRequest(transactionRequest)
        .applicationRepository(applicationRepository)
        .ledService(ledService)
        .build()
}

fun DuplicateReceiptRequestEntity.mapToModel(): DuplicateReceiptRequestModel {
    return if (stan.isNullOrEmptyOrBlank())
        DuplicateReceiptRequestModel(transactionRepository, printerService)
    else
        DuplicateReceiptRequestModel(transactionRepository, printerService, stan)
}

fun DetailReportRequestEntity.mapToModel(): DetailReportRequestModel {
    return DetailReportRequestModel.Builder.newInstance()
        .transactionRepository(transactionRepository)
        .printerService(printerService)
        .printSummaryReport(printSummaryReport)
        .applicationRepository(applicationRepository)
        .reconciliationRepository(reconciliationRepository)
        .build()
}

fun SummaryReportRequestEntity.mapToModel(): SummaryReportRequestModel {
    return SummaryReportRequestModel.Builder.newInstance()
        .withApplicationRepository(applicationRepository)
        .withPrinterService(printerService)
        .withReconciliationRepository(reconciliationRepository)
        .build()
}

fun AutoReversalRequestEntity.mapToModel(): AutoReversalRequestModel {
    return AutoReversalRequestModel.Builder.newInstance()
        .applicationRepository(applicationRepository)
        .printerService(printerService)
        .transactionRepository(transactionRepository)
        .build()
}

fun ReconciliationRequestEntity.mapToModel(): ReconciliationRequestModel {
    val reconciliationRequestModel =
        ReconciliationRequestModel(
            reconciliationRepository,
            printerService,
            applicationRepository,
            transactionRepository,
            settlementActive!!
        )
    reconciliationRequestModel.applicationPackageName = applicationPackageName
    reconciliationRequestModel.hardwareKeyService = hardwareKeyService
    return reconciliationRequestModel
}

fun BatchClearRequestEntity.mapToModel(): BatchClearRequestModel {
    return BatchClearRequestModel(
        reconciliationRepository, printerService, transactionRepository
    )
}

fun DuplicateReconciliationReceiptRequestEntity.mapToModel(): DuplicateReconciliationReceiptRequestModel {
    return DuplicateReconciliationReceiptRequestModel(
        this.reconciliationRepository,
        this.printerService
    )
}

fun CheckReconciliationRequestEntity.mapToModel(): CheckReconciliationRequestModel {
    return CheckReconciliationRequestModel(reconciliationRepository, applicationRepository)
}

