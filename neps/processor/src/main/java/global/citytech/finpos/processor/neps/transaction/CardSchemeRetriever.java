package global.citytech.finpos.processor.neps.transaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.CardSchemeRetrieveRequest;
import global.citytech.finposframework.repositories.CardSchemeRetrieveResponse;
import global.citytech.finposframework.repositories.FieldAttributes;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Saurav Ghimire on 1/21/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
class CardSchemeRetriever {

    private ApplicationRepository applicationRepository;

    public CardSchemeRetriever(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    public CardSchemeType retrieveCardScheme(String primaryAccountNumber) {
        if (!StringUtils.isEmpty(primaryAccountNumber)) {
            Map<String, CardSchemeType> map = this.retrieveCardSchemeCardRangeMap();
            String bin = primaryAccountNumber.substring(0, 6);
            return this.matchingCardScheme(map, bin);
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    private CardSchemeType matchingCardScheme(Map<String, CardSchemeType> map, String bin) {
        if (!StringUtils.isEmpty(bin) && map != null) {
            for (String keyBin : map.keySet()) {
                if (this.compareBin(bin, keyBin))
                    return map.get(keyBin);
            }
        }
        throw new PosException(PosError.DEVICE_ERROR_CARD_NOT_SUPPORTED);
    }

    private boolean compareBin(String bin, String keyBin) {
        boolean result = false;
        if (!StringUtils.isEmpty(bin) && !StringUtils.isEmpty(keyBin)) {
            if (bin.length() == keyBin.length()) {
                for (int i = 0; i < keyBin.length(); i++) {
                    if (keyBin.charAt(i) != 'F') {
                        if (keyBin.charAt(i) != bin.charAt(i)) {
                            result = false;
                            break;
                        } else {
                            result = true;
                        }
                    } else {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    private Map<String, CardSchemeType> retrieveCardSchemeCardRangeMap() {
        Map<String, CardSchemeType> rangeMap = new HashMap<>();
        for (CardSchemeType cardSchemeType : CardSchemeType.values()) {
            String cardRanges = this.retrieveCardRanges(cardSchemeType.getCardSchemeId());
            System.out.println("::: CARD SCHEME ::: " + cardSchemeType.getCardSchemeId());
            System.out.println("::: CARD RANGES ::: " + cardRanges);
            List<String> rangeList = this.cardRangeListParser(cardRanges);
            for (String range : rangeList) {
                rangeMap.put(range, cardSchemeType);
            }
        }
        return this.sortedMap(rangeMap);
    }

    private Map<String, CardSchemeType> sortedMap(Map<String, CardSchemeType> rangeMap) {
        Map<String, CardSchemeType> sortedMap = new HashMap<>();
        TreeMap<String, CardSchemeType> schemeTypeTreeMap = new TreeMap<>(rangeMap);
        Set<Map.Entry<String, CardSchemeType>> mappings = schemeTypeTreeMap.entrySet();
        for (Map.Entry<String, CardSchemeType> mapping : mappings) {
            sortedMap.put(mapping.getKey(), mapping.getValue());
        }
        return sortedMap;
    }

    private List<String> cardRangeListParser(String cardRanges) {
        List<String> cardRangeList = new ArrayList<>();
        if (!StringUtils.isEmpty(cardRanges)) {
            if (cardRanges.contains("|")) {
                String[] splittedCardRanges = cardRanges.split("\\|");
                for (String cardRange : splittedCardRanges) {
                    if (cardRange.contains("-")) {
                        String[] splittedCardRangesTwo = cardRange.split("-");
                        long splittedIndexZero = Long.parseLong(splittedCardRangesTwo[0]);
                        long splittedIndexOne = Long.parseLong(splittedCardRangesTwo[1]);
                        if (splittedIndexOne < splittedIndexZero) {
                            for (long i = splittedIndexZero; i >= splittedIndexOne; i--) {
                                cardRangeList.add(String.valueOf(i));
                            }
                        } else {
                            for (long i = splittedIndexOne; i >= splittedIndexZero; i--) {
                                cardRangeList.add(String.valueOf(i));
                            }
                        }
                    } else {
                        cardRangeList.add(cardRange);
                    }
                }
            } else if (cardRanges.contains("-")) {
                String[] splittedCardRangesTwo = cardRanges.split("-");
                long splittedIndexZero = Long.parseLong(splittedCardRangesTwo[0]);
                long splittedIndexOne = Long.parseLong(splittedCardRangesTwo[1]);
                if (splittedIndexOne < splittedIndexZero) {
                    for (long i = splittedIndexZero; i >= splittedIndexOne; i--) {
                        cardRangeList.add(String.valueOf(i));
                    }
                } else {
                    for (long i = splittedIndexOne; i >= splittedIndexZero; i--) {
                        cardRangeList.add(String.valueOf(i));
                    }
                }
            } else {
                cardRangeList.add(cardRanges);
            }
        }
        return this.sortedCardRangeList(cardRangeList);
    }

    private List<String> sortedCardRangeList(List<String> cardRangeList) {
        Collections.sort(cardRangeList, String::compareTo);
        return cardRangeList;
    }

    private String retrieveCardRanges(String cardSchemeId) {
        CardSchemeRetrieveRequest cardSchemeRetrieveRequest =
                new CardSchemeRetrieveRequest(
                        cardSchemeId,
                        FieldAttributes.CARD_SCHEME_S3_F6_CARD_RANGES
                );
        CardSchemeRetrieveResponse response = this.applicationRepository
                .retrieveCardScheme(cardSchemeRetrieveRequest);
        return response.getData();
    }
}
