package global.citytech.finpos.processor.nibl.receipt.summaryreport;

/**
 * Created by Unique Shakya on 1/5/2021.
 */
public class SummaryReportReceiptLabels {

    private SummaryReportReceiptLabels() {
    }

    public static final String TERMINAL_ID = "TID";
    public static final String MERCHANT_ID = "MID";
    public static final String DATE = "DATE";
    public static final String TIME = "TIME";
    public static final String BATCH_NUMBER = "BATCH NO";
    public static final String HOST = "HOST";
    public static final String HEADER = "SUMMARY";
    public static final String TRANSACTION = "TRANSACTION";
    public static final String COUNT = "COUNT";
    public static final String AMOUNT = "AMOUNT";
    public static final String DIVIDER = "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --";
    public static final String LINE_BREAK = "   ";
    public static final String SALE = "SALE";
    public static final String CASH = "CASH";
    public static final String VOID = "VOID";
    public static final String CASH_VOID = "VOID CASH ADVANCE";
    public static final String REFUND = "REFUND";
    public static final String TOTAL = "TOTAL";
    public static final String CUMULATIVE = "CUMULATIVE";
}
