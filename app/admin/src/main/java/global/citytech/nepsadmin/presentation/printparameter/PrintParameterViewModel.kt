package global.citytech.nepsadmin.presentation.printparameter

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import global.citytech.common.extensions.toJsonObj
import global.citytech.common.extensions.toJsonString
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.finpos.core.api.common.DeviceResponse
import global.citytech.finpos.core.api.common.DeviceResult
import global.citytech.finpos.core.api.io.printer.PrintMessage
import global.citytech.finpos.core.api.io.printer.Printable
import global.citytech.finpos.core.api.io.printer.PrinterRequest
import global.citytech.finpos.core.api.io.printer.Style
import global.citytech.nepsadmin.NepsAdmin
import global.citytech.nepsadmin.constants.Parameter
import global.citytech.nepsadmin.data.response.Host
import io.reactivex.Single
import io.reactivex.SingleOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlin.collections.ArrayList


class PrintParameterViewModel(private val context: Application) : BaseAndroidViewModel(context) {

    val printResult by lazy { MutableLiveData<DeviceResult>() }

    fun printParameter(parameters: ArrayList<String>) {
        isLoading.value = true
        val printerRequest = preparePrintRequest(parameters)
        compositeDisposable.add(
            Single.create(SingleOnSubscribe<DeviceResponse> { emitter ->
                val responseInString =
                    NepsAdmin.INSTANCE.iPlatformManager.print(printerRequest.toJsonString())
                val response = responseInString.toJsonObj(DeviceResponse::class.java)
                if (response.result == DeviceResult.SUCCESS) {
                    emitter.onSuccess(response)
                } else {
                    emitter.onError(Throwable(response.message))
                }
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ _ ->
                    isLoading.value = false
                    printResult.value = DeviceResult.SUCCESS
                }, { _ ->
                    isLoading.value = false
                    printResult.value = DeviceResult.FAILURE
                })
        )

    }

    private fun preparePrintRequest(parameters: ArrayList<String>): PrinterRequest {
        val printables: ArrayList<Printable> = arrayListOf()
        for (parameter in parameters) {
            getParams(parameter, printables)
        }
        return PrinterRequest(printables)
    }

    private fun getParams(key: String, printables: ArrayList<Printable>) {
        when (key) {
            Parameter.SWITCH_PARAM -> {
                val hosts = getHostParam()
                for (host in hosts) {
                    printables.add(
                        PrintMessage.getInstance(
                            host.ip!!,
                            Style.Builder().fontSize(Style.FontSize.NORMAL)
                                .alignment(Style.Align.CENTER).build()
                        )
                    )
                    printables.add(
                        PrintMessage.getInstance(
                            host.port!!,
                            Style.Builder().fontSize(Style.FontSize.NORMAL)
                                .alignment(Style.Align.CENTER).build()
                        )
                    )
                    printables.add(
                        PrintMessage.getInstance(
                            host.nii!!,
                            Style.Builder().fontSize(Style.FontSize.NORMAL)
                                .alignment(Style.Align.CENTER).build()
                        )
                    )
                }
            }
        }
    }

    fun getHostParam(): ArrayList<Host> {
        val hosts = ArrayList<Host>()
        val cursor = context.contentResolver.query(Host.URI, null, null, null, null)
        if (cursor!!.moveToFirst()) {
            do {
                val ip = cursor.getString(cursor.getColumnIndex(Host.COLUMN_IP))
                val port =
                    cursor.getString(cursor.getColumnIndex(Host.COLUMN_PORT))
                val nii =
                    cursor.getString(cursor.getColumnIndex(Host.COLUMN_NII))
                val host = Host(ip = ip, port = port, nii = nii)
                Log.i("try", "$ip = $port")
                hosts.add(host)
            } while (cursor.moveToNext())
        }
        cursor.close()
        return hosts
    }


}