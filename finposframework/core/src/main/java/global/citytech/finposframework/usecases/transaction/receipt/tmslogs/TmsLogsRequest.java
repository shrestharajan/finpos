package global.citytech.finposframework.usecases.transaction.receipt.tmslogs;

import java.util.List;

import global.citytech.finposframework.switches.receipt.tmslogs.TmsLogsParameter;

/**
 * Created by Rishav Chudal on 11/6/20.
 */
public class TmsLogsRequest {
    private List<TmsLogsParameter> tmsLogsParameters;

    public TmsLogsRequest(List<TmsLogsParameter> tmsLogsParameters) {
        this.tmsLogsParameters = tmsLogsParameters;
    }

    public List<TmsLogsParameter> getTmsLogsParameters() {
        return tmsLogsParameters;
    }
}
