package global.citytech.finpos.merchant.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import global.citytech.finpos.merchant.R

class ConfirmationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)
    }
}