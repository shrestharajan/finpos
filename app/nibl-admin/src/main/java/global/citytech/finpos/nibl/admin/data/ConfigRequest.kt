package global.citytech.finpos.nibl.admin.data

data class ConfigRequest(
    var appInfo: AppInfo? = null,
    var deviceInfo: DeviceInfo? = null
)