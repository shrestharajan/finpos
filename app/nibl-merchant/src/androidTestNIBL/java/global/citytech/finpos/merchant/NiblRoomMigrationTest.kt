package global.citytech.finpos.merchant

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import androidx.room.Room
import androidx.room.testing.MigrationTestHelper
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import global.citytech.finpos.merchant.data.datasource.app.db.AppDatabase
import global.citytech.finpos.merchant.domain.model.app.TransactionLog
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Rishav Chudal on 9/15/20.
 */
@RunWith(AndroidJUnit4::class)
class NiblRoomMigrationTest {
    companion object {
        private const val TEST_DB = "migration_test"
    }

    @get:Rule
    val testHelper: MigrationTestHelper = MigrationTestHelper(
        InstrumentationRegistry.getInstrumentation(),
        AppDatabase::class.java.canonicalName,
        FrameworkSQLiteOpenHelperFactory()
    )

    @Test
    fun testRoomMigrationV2ToV3() {
        testHelper.createDatabase(TEST_DB, 3).apply {
            insert(
                "transactions",
                SQLiteDatabase.CONFLICT_REPLACE,
                dummyTransactionLog1ForDBVersion2()
            )
            insert(
                "transactions",
                SQLiteDatabase.CONFLICT_REPLACE,
                dummyTransactionLog2ForDBVersion2()
            )
            close()
        }

        testHelper.runMigrationsAndValidate(
            TEST_DB,
            4,
            true,
            AppDatabase.MIGRATION_V1_V2,
            AppDatabase.MIGRATION_V2_V3,
            AppDatabase.MIGRATION_V3_V4
        )

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        )
            .addMigrations(
                AppDatabase.MIGRATION_V1_V2,
                AppDatabase.MIGRATION_V2_V3,
                AppDatabase.MIGRATION_V3_V4
            )
            .build()

        val transactionLog = database.getTransactionLogDao().getRecentApprovedTransaction()
        assertEquals("000124", transactionLog!!.stan)
        assertEquals("APPROVED", transactionLog.transactionStatus)
        assertEquals(true, transactionLog.authorizationCompleted)
        assertEquals(true, transactionLog.transactionVoided)
        assertEquals(null, transactionLog.tipAdjusted)
    }


    @Test
    fun testRoomMigrationV10ToV11() {
        testHelper.createDatabase(TEST_DB, 10).apply {
            insert(
                "transactions",
                SQLiteDatabase.CONFLICT_REPLACE,
                dummyTransactionLog1ForDBVersion2()
            )
            insert(
                "merchant_transaction_log",
                SQLiteDatabase.CONFLICT_REPLACE,
                dummyTransactionLog2ForDBVersion11()
            )
            close()
        }

        testHelper.runMigrationsAndValidate(
            TEST_DB,
            11,
            true,
            AppDatabase.MIGRATION_V1_V2,
            AppDatabase.MIGRATION_V2_V3,
            AppDatabase.MIGRATION_V3_V4,
            AppDatabase.MIGRATION_V4_V5,
            AppDatabase.MIGRATION_V5_V6,
            AppDatabase.MIGRATION_V6_V7,
            AppDatabase.MIGRATION_V7_V8,
            AppDatabase.MIGRATION_V8_V9,
            AppDatabase.MIGRATION_V9_V10,
            AppDatabase.MIGRATION_V10_V11
        )

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        )
            .addMigrations(
                AppDatabase.MIGRATION_V1_V2,
                AppDatabase.MIGRATION_V2_V3,
                AppDatabase.MIGRATION_V3_V4,
                AppDatabase.MIGRATION_V4_V5,
                AppDatabase.MIGRATION_V5_V6,
                AppDatabase.MIGRATION_V6_V7,
                AppDatabase.MIGRATION_V7_V8,
                AppDatabase.MIGRATION_V8_V9,
                AppDatabase.MIGRATION_V9_V10,
                AppDatabase.MIGRATION_V10_V11
            )
            .build()

        val transactionLog = database.getTransactionLogDao().getRecentApprovedTransaction()
        assertEquals("000124", transactionLog!!.stan)
        assertEquals("APPROVED", transactionLog.transactionStatus)
        assertEquals(null, transactionLog.currencyCode)
    }

    private fun dummyTransactionLog1ForDBVersion2(): ContentValues = ContentValues().apply {
        put(TransactionLog.COLUMN_STAN, "000123")
        put(TransactionLog.COLUMN_TERMINAL_ID, "12345678")
        put(TransactionLog.COLUMN_MERCHANT_ID, "11223344")
        put(TransactionLog.COLUMN_INVOICE_NUM, "000103")
        put(TransactionLog.COLUMN_RRN, "123456789012")
        put(TransactionLog.COLUMN_AUTH_CODE, "123103")
        put(TransactionLog.COLUMN_TRANSACTION_TYPE, "Sale")
        put(TransactionLog.COLUMN_TRANSACTION_AMOUNT, "50")
        put(TransactionLog.COLUMN_TRANSACTION_DATE, "20210521")
        put(TransactionLog.COLUMN_TRANSACTION_TIME, "152533")
        put(TransactionLog.COLUMN_RECONCILE_BATCH_NO, "1")
        put(TransactionLog.COLUMN_READ_CARD_RESPONSE, "Read Card Response 1")
        put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
        put(TransactionLog.COLUMN_AUTHORIZATION_COMPLETED, 0)
        put(TransactionLog.COLUMN_TRANSACTION_VOIDED, 0)
        put(TransactionLog.COLUMN_RESPONSE_CODE, "00")
    }

    private fun dummyTransactionLog2ForDBVersion2(): ContentValues = ContentValues().apply {
        put(TransactionLog.COLUMN_STAN, "000124")
        put(TransactionLog.COLUMN_TERMINAL_ID, "12345678")
        put(TransactionLog.COLUMN_MERCHANT_ID, "11223344")
        put(TransactionLog.COLUMN_INVOICE_NUM, "000104")
        put(TransactionLog.COLUMN_RRN, "123456789012")
        put(TransactionLog.COLUMN_AUTH_CODE, "123104")
        put(TransactionLog.COLUMN_TRANSACTION_TYPE, "Void")
        put(TransactionLog.COLUMN_TRANSACTION_AMOUNT, "150")
        put(TransactionLog.COLUMN_TRANSACTION_DATE, "20210521")
        put(TransactionLog.COLUMN_TRANSACTION_TIME, "152533")
        put(TransactionLog.COLUMN_RECONCILE_BATCH_NO, "1")
        put(TransactionLog.COLUMN_READ_CARD_RESPONSE, "Read Card Response 1")
        put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
        put(TransactionLog.COLUMN_AUTHORIZATION_COMPLETED, 1)
        put(TransactionLog.COLUMN_TRANSACTION_VOIDED, 1)
        put(TransactionLog.COLUMN_RESPONSE_CODE, "05")
    }

    private fun dummyTransactionLog2ForDBVersion11(): ContentValues = ContentValues().apply {
        put(TransactionLog.COLUMN_STAN, "000124")
        put(TransactionLog.COLUMN_TERMINAL_ID, "12345678")
        put(TransactionLog.COLUMN_MERCHANT_ID, "11223344")
        put(TransactionLog.COLUMN_INVOICE_NUM, "000104")
        put(TransactionLog.COLUMN_RRN, "123456789012")
        put(TransactionLog.COLUMN_TRANSACTION_TYPE, "Void")
        put(TransactionLog.COLUMN_TRANSACTION_AMOUNT, "150")
        put(TransactionLog.COLUMN_TRANSACTION_DATE, "20210521")
        put(TransactionLog.COLUMN_TRANSACTION_TIME, "152533")
        put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
    }


    /*
    @Test
    fun testRoomMigrationV2ToV3() {
        val values = ContentValues().apply {
            put("id", "101")
            put("app_wallpaper", "app_wallpaper_101")
            put("display_logo", "display_logo_101")
            put("print_logo", "print_logo_101")
        }

        val responseCodeValues = ContentValues().apply {
            put("id", "202")
            put("message_code", "00")
            put("message_text", "APPROVED")
        }

        val responseCodeValues2 = ContentValues().apply {
            put("id", "203")
            put("message_code", "01")
            put("message_text", "DECLINED")
        }

        testHelper.createDatabase(TEST_DB, 2).apply {
            //inserting some values to the table "logo"
            insert("logo", SQLiteDatabase.CONFLICT_REPLACE, values)
            //inserting some values to the table "response_code"
            insert("response_code", SQLiteDatabase.CONFLICT_REPLACE, responseCodeValues)
            insert("response_code", SQLiteDatabase.CONFLICT_REPLACE, responseCodeValues2)
            close()
        }

        testHelper.runMigrationsAndValidate(
            TEST_DB,
            3,
            true,
            AppDatabase.MIGRATION_V1_V2,
            AppDatabase.MIGRATION_V2_V3
        )

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        )
            .addMigrations(AppDatabase.MIGRATION_V1_V2)
            .addMigrations(AppDatabase.MIGRATION_V2_V3)
            .build()

        //Verifying the data in table logo
        val tableLogo = database.getLogoDao().getLogoList()
        assertEquals(1, tableLogo.size)
        assertEquals("101", tableLogo[0].id)
        assertEquals("app_wallpaper_101", tableLogo[0].appWallpaper)
        assertEquals("display_logo_101", tableLogo[0].displayLogo)
        assertEquals("print_logo_101", tableLogo[0].printLogo)

        //Verifying the data in table response_code
        val responseCodeList = database.getMessageCodeDao().getAllMessageCodes()
        for (responseCode in responseCodeList) {
            logger.log("id ::: ".plus(responseCode.id))
            logger.log("messageCode ::: ".plus(responseCode.messageCode))
            logger.log("messageText ::: ".plus(responseCode.messageText))
        }
        assertEquals(2, responseCodeList.size)
        assertEquals("202", responseCodeList[0].id)
        assertEquals("00", responseCodeList[0].messageCode)
        assertEquals("APPROVED", responseCodeList[0].messageText)

        assertEquals("203", responseCodeList[1].id)
        assertEquals("01", responseCodeList[1].messageCode)
        assertEquals("DECLINED", responseCodeList[1].messageText)

        //Verifying the data in table transaction_log
        val transactionLog = database.getTransactionLogDao().getRecentApprovedTransaction()
        assertNull(transactionLog)
    }

    @Test
    fun testRoomMigrationV3ToV4() {
        val values = ContentValues().apply {
            put("id", "101")
            put("app_wallpaper", "app_wallpaper_101")
            put("display_logo", "display_logo_101")
            put("print_logo", "print_logo_101")
        }

        val responseCodeValues = ContentValues().apply {
            put("id", "202")
            put("message_code", "00")
            put("message_text", "APPROVED")
        }

        val responseCodeValues2 = ContentValues().apply {
            put("id", "203")
            put("message_code", "01")
            put("message_text", "DECLINED")
        }

        val transactionLogValues = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "10101")
            put(TransactionLog.COLUMN_RRN, "20202")
            put(TransactionLog.COLUMN_AUTH_CODE, "30303")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 12345)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PRE AUTH")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
        }

        testHelper.createDatabase(TEST_DB, 3).apply {
            //inserting some values to the table "logo"
            insert("logo", SQLiteDatabase.CONFLICT_REPLACE, values)
            //inserting some values to the table "response_code"
            insert("response_code", SQLiteDatabase.CONFLICT_REPLACE, responseCodeValues)
            insert("response_code", SQLiteDatabase.CONFLICT_REPLACE, responseCodeValues2)
            insert("transaction_log", SQLiteDatabase.CONFLICT_REPLACE, transactionLogValues)
            close()
        }

        testHelper.runMigrationsAndValidate(
            TEST_DB,
            4,
            true,
            AppDatabase.MIGRATION_V1_V2,
            AppDatabase.MIGRATION_V2_V3,
            AppDatabase.MIGRATION_V3_V4
        )

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        )
            .addMigrations(AppDatabase.MIGRATION_V1_V2)
            .addMigrations(AppDatabase.MIGRATION_V2_V3)
            .addMigrations(AppDatabase.MIGRATION_V3_V4)
            .build()

        //Verifying the data in table logo
        val tableLogo = database.getLogoDao().getLogoList()
        assertEquals(1, tableLogo.size)
        assertEquals("101", tableLogo[0].id)
        assertEquals("app_wallpaper_101", tableLogo[0].appWallpaper)
        assertEquals("display_logo_101", tableLogo[0].displayLogo)
        assertEquals("print_logo_101", tableLogo[0].printLogo)

        //Verifying the data in table response_code
        val responseCodeList = database.getMessageCodeDao().getAllMessageCodes()
        for (responseCode in responseCodeList) {
            logger.log("id ::: ".plus(responseCode.id))
            logger.log("messageCode ::: ".plus(responseCode.messageCode))
            logger.log("messageText ::: ".plus(responseCode.messageText))
        }
        assertEquals(2, responseCodeList.size)
        assertEquals("202", responseCodeList[0].id)
        assertEquals("00", responseCodeList[0].messageCode)
        assertEquals("APPROVED", responseCodeList[0].messageText)

        assertEquals("203", responseCodeList[1].id)
        assertEquals("01", responseCodeList[1].messageCode)
        assertEquals("DECLINED", responseCodeList[1].messageText)

        //Verifying the data in table transaction_log
        val transactionLog = database.getTransactionLogDao().getRecentTransaction()
        assertNotNull(transactionLog)
        assertEquals("10101", transactionLog!!.invoiceNumber)
        assertEquals("20202", transactionLog.rrn)
        assertEquals("30303", transactionLog.authCode)
        assertEquals("PRE AUTH", transactionLog.origTransactionType)
        assertEquals("APPROVED", transactionLog.transactionStatus)

        val stan = database.getTransactionConfigDao().getStan()
        assertEquals(0, stan)
        val rrn = database.getTransactionConfigDao().getRrn()
        assertEquals(0, rrn)
        val invoiceNumber = database.getTransactionConfigDao().getInvoiceNumber()
        assertEquals(0, invoiceNumber)
        val batchNumber = database.getTransactionConfigDao().getReconciliationBatchNumber()
        assertEquals(0, batchNumber)
    }*/

    /*@Test
    fun testRoomMigrationV4ToV5() {
        val values = ContentValues().apply {
            put("id", "101")
            put("app_wallpaper", "app_wallpaper_101")
            put("display_logo", "display_logo_101")
            put("print_logo", "print_logo_101")
        }

        val responseCodeValues = ContentValues().apply {
            put("id", "202")
            put("message_code", "00")
            put("message_text", "APPROVED")
        }

        val responseCodeValues2 = ContentValues().apply {
            put("id", "203")
            put("message_code", "01")
            put("message_text", "DECLINED")
        }

        val transactionLogValues = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "10101")
            put(TransactionLog.COLUMN_RRN, "20202")
            put(TransactionLog.COLUMN_AUTH_CODE, "30303")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 12345)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PRE AUTH")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
        }

        testHelper.createDatabase(TEST_DB, 4).apply {
            //inserting some values to the table "logo"
            insert("logo", SQLiteDatabase.CONFLICT_REPLACE, values)
            //inserting some values to the table "response_code"
            insert("response_code", SQLiteDatabase.CONFLICT_REPLACE, responseCodeValues)
            insert("response_code", SQLiteDatabase.CONFLICT_REPLACE, responseCodeValues2)
            insert("transaction_log", SQLiteDatabase.CONFLICT_REPLACE, transactionLogValues)
            close()
        }

        testHelper.runMigrationsAndValidate(
            TEST_DB,
            5,
            true
        )

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        )
            .build()

        //Verifying the data in table logo
        val tableLogo = database.getLogoDao().getLogoList()
        assertEquals(1, tableLogo.size)
        assertEquals("101", tableLogo[0].id)
        assertEquals("app_wallpaper_101", tableLogo[0].appWallpaper)
        assertEquals("display_logo_101", tableLogo[0].displayLogo)
        assertEquals("print_logo_101", tableLogo[0].printLogo)

        //Verifying the data in table response_code
        val responseCodeList = database.getMessageCodeDao().getAllMessageCodes()
        for (responseCode in responseCodeList) {
            logger.log("id ::: ".plus(responseCode.id))
            logger.log("messageCode ::: ".plus(responseCode.messageCode))
            logger.log("messageText ::: ".plus(responseCode.messageText))
        }
        assertEquals(2, responseCodeList.size)
        assertEquals("202", responseCodeList[0].id)
        assertEquals("00", responseCodeList[0].messageCode)
        assertEquals("APPROVED", responseCodeList[0].messageText)

        assertEquals("203", responseCodeList[1].id)
        assertEquals("01", responseCodeList[1].messageCode)
        assertEquals("DECLINED", responseCodeList[1].messageText)

        //Verifying the data in table transaction_log
        val transactionLog = database.getTransactionLogDao().getRecentTransaction()
        logger.log("::: TRANSACTION LOG ::: " + transactionLog.toString())
        assertNotNull(transactionLog)
        assertEquals("10101", transactionLog!!.invoiceNumber)
        assertEquals("20202", transactionLog.rrn)
        assertEquals("30303", transactionLog.authCode)
        assertEquals("PRE AUTH", transactionLog.origTransactionType)
        assertEquals("APPROVED", transactionLog.transactionStatus)

        val stan = database.getTransactionConfigDao().getStan()
        assertEquals(0, stan)
        val rrn = database.getTransactionConfigDao().getRrn()
        assertEquals(0, rrn)
        val invoiceNumber = database.getTransactionConfigDao().getInvoiceNumber()
        assertEquals(0, invoiceNumber)
        val batchNumber = database.getTransactionConfigDao().getReconciliationBatchNumber()
        assertEquals(0, batchNumber)


    }

    @Test
    fun testTransactionLogDaoGetRecentApprovedTransaction() {
        val values1 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "10101")
            put(TransactionLog.COLUMN_RRN, "20202")
            put(TransactionLog.COLUMN_AUTH_CODE, "30303")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 12345)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PRE AUTH")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
        }

        val values2 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "40404")
            put(TransactionLog.COLUMN_RRN, "50505")
            put(TransactionLog.COLUMN_AUTH_CODE, "60606")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 23456)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "DECLINED")
        }

        val values3 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "70707")
            put(TransactionLog.COLUMN_RRN, "80808")
            put(TransactionLog.COLUMN_AUTH_CODE, "90909")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 34567)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
        }

        testHelper.createDatabase(TEST_DB, 1).apply {
            //inserting some values to the table "transaction_log"
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values1)
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values2)
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values3)
            close()
        }

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        ).build()

        val transactionLog = database.getTransactionLogDao().getRecentApprovedTransaction()
        assertEquals("70707", transactionLog!!.invoiceNumber)
        assertEquals("80808", transactionLog.rrn)
        assertEquals("90909", transactionLog.authCode)
        assertEquals(34567.toLong(), transactionLog.origTransactionAmount)
        assertEquals("APPROVED", transactionLog.transactionStatus)
    }

    @Test
    fun testTransactionLogDaoGetTransactionLogByInvoiceNumber() {
        val values1 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "10101")
            put(TransactionLog.COLUMN_RRN, "20202")
            put(TransactionLog.COLUMN_AUTH_CODE, "30303")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 12345)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PRE AUTH")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
        }

        val values2 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "40404")
            put(TransactionLog.COLUMN_RRN, "50505")
            put(TransactionLog.COLUMN_AUTH_CODE, "60606")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 23456)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "DECLINED")
        }

        val values3 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "70707")
            put(TransactionLog.COLUMN_RRN, "80808")
            put(TransactionLog.COLUMN_AUTH_CODE, "90909")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 34567)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
        }

        testHelper.createDatabase(TEST_DB, 1).apply {
            //inserting some values to the table "transaction_log"
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values1)
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values2)
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values3)
            close()
        }

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        ).build()

        val transactionLog = database.getTransactionLogDao().getTransactionLogBy("40404")
        assertEquals("40404", transactionLog!!.invoiceNumber)
        assertEquals("50505", transactionLog.rrn)
        assertEquals("60606", transactionLog.authCode)
        assertEquals(23456.toLong(), transactionLog.origTransactionAmount)
        assertEquals("DECLINED", transactionLog.transactionStatus)
    }

    @Test
    fun testTransactionLogDaoGetRecentTransaction() {
        val values1 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "10101")
            put(TransactionLog.COLUMN_RRN, "20202")
            put(TransactionLog.COLUMN_AUTH_CODE, "30303")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 12345)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PRE AUTH")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
        }

        val values2 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "40404")
            put(TransactionLog.COLUMN_RRN, "50505")
            put(TransactionLog.COLUMN_AUTH_CODE, "60606")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 23456)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "DECLINED")
        }

        testHelper.createDatabase(TEST_DB, 1).apply {
            //inserting some values to the table "transaction_log"
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values1)
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values2)
            close()
        }

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        ).build()

        val transactionLog = database.getTransactionLogDao().getRecentTransaction()
        assertEquals("40404", transactionLog!!.invoiceNumber)
        assertEquals("50505", transactionLog.rrn)
        assertEquals("60606", transactionLog.authCode)
        assertEquals(23456.toLong(), transactionLog.origTransactionAmount)
        assertEquals("DECLINED", transactionLog.transactionStatus)
    }

    @Test
    fun testTransactionLogDaoGetRecentTransactionV5() {
        val values1 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "10101")
            put(TransactionLog.COLUMN_RRN, "20202")
            put(TransactionLog.COLUMN_AUTH_CODE, "30303")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 12345)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PRE AUTH")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
            put(TransactionLog.COLUMN_READ_CARD_RESPONSE, "")
        }

        val values2 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "40404")
            put(TransactionLog.COLUMN_RRN, "50505")
            put(TransactionLog.COLUMN_AUTH_CODE, "60606")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 23456)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "DECLINED")
            put(TransactionLog.COLUMN_READ_CARD_RESPONSE, "")
        }

        testHelper.createDatabase(TEST_DB, 5).apply {
            //inserting some values to the table "transaction_log"
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values1)
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values2)
            close()
        }

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        )
            .build()

        val transactionLog = database.getTransactionLogDao().getRecentTransaction()
        assertEquals("40404", transactionLog!!.invoiceNumber)
        assertEquals("50505", transactionLog.rrn)
        assertEquals("60606", transactionLog.authCode)
        assertEquals(23456.toLong(), transactionLog.origTransactionAmount)
        assertEquals("DECLINED", transactionLog.transactionStatus)
        assertEquals("", transactionLog.readCardResponse)
    }

    @Test
    fun testTransactionLogDaoGetCountByTransactionType() {
        val values1 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "10101")
            put(TransactionLog.COLUMN_RRN, "20202")
            put(TransactionLog.COLUMN_AUTH_CODE, "30303")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 12345)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PRE AUTH")
            put(TransactionLog.COLUMN_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
            put(TransactionLog.COLUMN_RECONCILE_BATCH_NO, "123456")
            put(TransactionLog.COLUMN_TRANSACTION_AMOUNT, 2500)
            put(
                TransactionLog.COLUMN_READ_CARD_RESPONSE,
                "{\"cardDetails\":{\"additionalAmount\":0,\"aid\":\"A0000000031010\",\"amount\":100,\"cardHolderName\":\"25P/PERCENT CREDIT        \",\"cardScheme\":\"VISA_CREDIT\",\"cardType\":\"ICC\",\"expiryDate\":\"2412\",\"iccDataBlock\":\"5F2A0205245F34010182023C008407A0000000031010950500000480009A032011129C01009F02060000000100009F03060000000000009F090200969F100706010A0320AC029F1A0205249F2608FB18EC9949CBB5ED9F2701009F3303E0F8C89F34034203009F3501229F360202569F3704FA0D5F449F410400000017\",\"pinBlock\":{\"offlinePin\":false,\"pin\":\"DFD1A47F4697F94A\"},\"primaryAccountNumber\":\"4249720020000234\",\"primaryAccountNumberSerialNumber\":\"01\",\"tag5F28\":\"0524\",\"tag9F42\":\"0524\",\"tagCollection\":{\"tags\":{\"130\":{\"data\":\"3C00\",\"size\":2,\"tag\":130},\"132\":{\"data\":\"A0000000031010\",\"size\":7,\"tag\":132},\"149\":{\"data\":\"0000048000\",\"size\":5,\"tag\":149},\"154\":{\"data\":\"201112\",\"size\":3,\"tag\":154},\"156\":{\"data\":\"00\",\"size\":1,\"tag\":156},\"24362\":{\"data\":\"0524\",\"size\":2,\"tag\":24362},\"24372\":{\"data\":\"01\",\"size\":1,\"tag\":24372},\"40706\":{\"data\":\"000000010000\",\"size\":6,\"tag\":40706},\"40707\":{\"data\":\"000000000000\",\"size\":6,\"tag\":40707},\"40713\":{\"data\":\"0096\",\"size\":2,\"tag\":40713},\"40720\":{\"data\":\"06010A0320AC02\",\"size\":7,\"tag\":40720},\"40730\":{\"data\":\"0524\",\"size\":2,\"tag\":40730},\"40742\":{\"data\":\"FB18EC9949CBB5ED\",\"size\":8,\"tag\":40742},\"40743\":{\"data\":\"00\",\"size\":1,\"tag\":40743},\"40755\":{\"data\":\"E0F8C8\",\"size\":3,\"tag\":40755},\"40756\":{\"data\":\"420300\",\"size\":3,\"tag\":40756},\"40757\":{\"data\":\"22\",\"size\":1,\"tag\":40757},\"40758\":{\"data\":\"0256\",\"size\":2,\"tag\":40758},\"40759\":{\"data\":\"FA0D5F44\",\"size\":4,\"tag\":40759},\"40769\":{\"data\":\"00000017\",\"size\":4,\"tag\":40769}}},\"trackTwoData\":\"4249720020000234D24122261393525399999\",\"transactionAcceptedByCard\":true,\"transactionInitializeDateTime\":\"201112181040447\",\"transactionState\":\"OFFLINE_DECLINED\"},\"contactlessCvm\":\"NO_CVM\",\"cvm\":\"NO_CVM\",\"fallbackIccToMag\":false,\"message\":\"Read Card Success\",\"result\":\"SUCCESS\"}"
            )
        }

        val values2 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "40404")
            put(TransactionLog.COLUMN_RRN, "50505")
            put(TransactionLog.COLUMN_AUTH_CODE, "60606")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 23456)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
            put(TransactionLog.COLUMN_RECONCILE_BATCH_NO, "123456")
            put(TransactionLog.COLUMN_TRANSACTION_AMOUNT, 1000)
            put(
                TransactionLog.COLUMN_READ_CARD_RESPONSE,
                "{\"cardDetails\":{\"additionalAmount\":0,\"aid\":\"A0000000031010\",\"amount\":100,\"cardHolderName\":\"25P/PERCENT CREDIT        \",\"cardScheme\":\"MADA\",\"cardType\":\"ICC\",\"expiryDate\":\"2412\",\"iccDataBlock\":\"5F2A0205245F34010182023C008407A0000000031010950500000480009A032011129C01009F02060000000100009F03060000000000009F090200969F100706010A0320AC029F1A0205249F2608FB18EC9949CBB5ED9F2701009F3303E0F8C89F34034203009F3501229F360202569F3704FA0D5F449F410400000017\",\"pinBlock\":{\"offlinePin\":false,\"pin\":\"DFD1A47F4697F94A\"},\"primaryAccountNumber\":\"4249720020000234\",\"primaryAccountNumberSerialNumber\":\"01\",\"tag5F28\":\"0524\",\"tag9F42\":\"0524\",\"tagCollection\":{\"tags\":{\"130\":{\"data\":\"3C00\",\"size\":2,\"tag\":130},\"132\":{\"data\":\"A0000000031010\",\"size\":7,\"tag\":132},\"149\":{\"data\":\"0000048000\",\"size\":5,\"tag\":149},\"154\":{\"data\":\"201112\",\"size\":3,\"tag\":154},\"156\":{\"data\":\"00\",\"size\":1,\"tag\":156},\"24362\":{\"data\":\"0524\",\"size\":2,\"tag\":24362},\"24372\":{\"data\":\"01\",\"size\":1,\"tag\":24372},\"40706\":{\"data\":\"000000010000\",\"size\":6,\"tag\":40706},\"40707\":{\"data\":\"000000000000\",\"size\":6,\"tag\":40707},\"40713\":{\"data\":\"0096\",\"size\":2,\"tag\":40713},\"40720\":{\"data\":\"06010A0320AC02\",\"size\":7,\"tag\":40720},\"40730\":{\"data\":\"0524\",\"size\":2,\"tag\":40730},\"40742\":{\"data\":\"FB18EC9949CBB5ED\",\"size\":8,\"tag\":40742},\"40743\":{\"data\":\"00\",\"size\":1,\"tag\":40743},\"40755\":{\"data\":\"E0F8C8\",\"size\":3,\"tag\":40755},\"40756\":{\"data\":\"420300\",\"size\":3,\"tag\":40756},\"40757\":{\"data\":\"22\",\"size\":1,\"tag\":40757},\"40758\":{\"data\":\"0256\",\"size\":2,\"tag\":40758},\"40759\":{\"data\":\"FA0D5F44\",\"size\":4,\"tag\":40759},\"40769\":{\"data\":\"00000017\",\"size\":4,\"tag\":40769}}},\"trackTwoData\":\"4249720020000234D24122261393525399999\",\"transactionAcceptedByCard\":true,\"transactionInitializeDateTime\":\"201112181040447\",\"transactionState\":\"OFFLINE_DECLINED\"},\"contactlessCvm\":\"NO_CVM\",\"cvm\":\"NO_CVM\",\"fallbackIccToMag\":false,\"message\":\"Read Card Success\",\"result\":\"SUCCESS\"}"
            )
        }

        val values3 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "40405")
            put(TransactionLog.COLUMN_RRN, "50505")
            put(TransactionLog.COLUMN_AUTH_CODE, "60606")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 23456)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_TYPE, "VOID")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
            put(TransactionLog.COLUMN_RECONCILE_BATCH_NO, "123456")
            put(TransactionLog.COLUMN_TRANSACTION_AMOUNT, 1000)
            put(
                TransactionLog.COLUMN_READ_CARD_RESPONSE,
                "{\"cardDetails\":{\"additionalAmount\":0,\"aid\":\"A0000000031010\",\"amount\":100,\"cardHolderName\":\"25P/PERCENT CREDIT        \",\"cardScheme\":\"VISA_CREDIT\",\"cardType\":\"ICC\",\"expiryDate\":\"2412\",\"iccDataBlock\":\"5F2A0205245F34010182023C008407A0000000031010950500000480009A032011129C01009F02060000000100009F03060000000000009F090200969F100706010A0320AC029F1A0205249F2608FB18EC9949CBB5ED9F2701009F3303E0F8C89F34034203009F3501229F360202569F3704FA0D5F449F410400000017\",\"pinBlock\":{\"offlinePin\":false,\"pin\":\"DFD1A47F4697F94A\"},\"primaryAccountNumber\":\"4249720020000234\",\"primaryAccountNumberSerialNumber\":\"01\",\"tag5F28\":\"0524\",\"tag9F42\":\"0524\",\"tagCollection\":{\"tags\":{\"130\":{\"data\":\"3C00\",\"size\":2,\"tag\":130},\"132\":{\"data\":\"A0000000031010\",\"size\":7,\"tag\":132},\"149\":{\"data\":\"0000048000\",\"size\":5,\"tag\":149},\"154\":{\"data\":\"201112\",\"size\":3,\"tag\":154},\"156\":{\"data\":\"00\",\"size\":1,\"tag\":156},\"24362\":{\"data\":\"0524\",\"size\":2,\"tag\":24362},\"24372\":{\"data\":\"01\",\"size\":1,\"tag\":24372},\"40706\":{\"data\":\"000000010000\",\"size\":6,\"tag\":40706},\"40707\":{\"data\":\"000000000000\",\"size\":6,\"tag\":40707},\"40713\":{\"data\":\"0096\",\"size\":2,\"tag\":40713},\"40720\":{\"data\":\"06010A0320AC02\",\"size\":7,\"tag\":40720},\"40730\":{\"data\":\"0524\",\"size\":2,\"tag\":40730},\"40742\":{\"data\":\"FB18EC9949CBB5ED\",\"size\":8,\"tag\":40742},\"40743\":{\"data\":\"00\",\"size\":1,\"tag\":40743},\"40755\":{\"data\":\"E0F8C8\",\"size\":3,\"tag\":40755},\"40756\":{\"data\":\"420300\",\"size\":3,\"tag\":40756},\"40757\":{\"data\":\"22\",\"size\":1,\"tag\":40757},\"40758\":{\"data\":\"0256\",\"size\":2,\"tag\":40758},\"40759\":{\"data\":\"FA0D5F44\",\"size\":4,\"tag\":40759},\"40769\":{\"data\":\"00000017\",\"size\":4,\"tag\":40769}}},\"trackTwoData\":\"4249720020000234D24122261393525399999\",\"transactionAcceptedByCard\":true,\"transactionInitializeDateTime\":\"201112181040447\",\"transactionState\":\"OFFLINE_DECLINED\"},\"contactlessCvm\":\"NO_CVM\",\"cvm\":\"NO_CVM\",\"fallbackIccToMag\":false,\"message\":\"Read Card Success\",\"result\":\"SUCCESS\"}"
            )
        }

        val values4 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "40406")
            put(TransactionLog.COLUMN_RRN, "50505")
            put(TransactionLog.COLUMN_AUTH_CODE, "60606")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 23456)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_TYPE, "VOID")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
            put(TransactionLog.COLUMN_RECONCILE_BATCH_NO, "123456")
            put(TransactionLog.COLUMN_TRANSACTION_AMOUNT, 1000)
            put(
                TransactionLog.COLUMN_READ_CARD_RESPONSE,
                "{\"cardDetails\":{\"additionalAmount\":0,\"aid\":\"A0000000031010\",\"amount\":100,\"cardHolderName\":\"25P/PERCENT CREDIT        \",\"cardScheme\":\"VISA_CREDIT\",\"cardType\":\"ICC\",\"expiryDate\":\"2412\",\"iccDataBlock\":\"5F2A0205245F34010182023C008407A0000000031010950500000480009A032011129C01009F02060000000100009F03060000000000009F090200969F100706010A0320AC029F1A0205249F2608FB18EC9949CBB5ED9F2701009F3303E0F8C89F34034203009F3501229F360202569F3704FA0D5F449F410400000017\",\"pinBlock\":{\"offlinePin\":false,\"pin\":\"DFD1A47F4697F94A\"},\"primaryAccountNumber\":\"4249720020000234\",\"primaryAccountNumberSerialNumber\":\"01\",\"tag5F28\":\"0524\",\"tag9F42\":\"0524\",\"tagCollection\":{\"tags\":{\"130\":{\"data\":\"3C00\",\"size\":2,\"tag\":130},\"132\":{\"data\":\"A0000000031010\",\"size\":7,\"tag\":132},\"149\":{\"data\":\"0000048000\",\"size\":5,\"tag\":149},\"154\":{\"data\":\"201112\",\"size\":3,\"tag\":154},\"156\":{\"data\":\"00\",\"size\":1,\"tag\":156},\"24362\":{\"data\":\"0524\",\"size\":2,\"tag\":24362},\"24372\":{\"data\":\"01\",\"size\":1,\"tag\":24372},\"40706\":{\"data\":\"000000010000\",\"size\":6,\"tag\":40706},\"40707\":{\"data\":\"000000000000\",\"size\":6,\"tag\":40707},\"40713\":{\"data\":\"0096\",\"size\":2,\"tag\":40713},\"40720\":{\"data\":\"06010A0320AC02\",\"size\":7,\"tag\":40720},\"40730\":{\"data\":\"0524\",\"size\":2,\"tag\":40730},\"40742\":{\"data\":\"FB18EC9949CBB5ED\",\"size\":8,\"tag\":40742},\"40743\":{\"data\":\"00\",\"size\":1,\"tag\":40743},\"40755\":{\"data\":\"E0F8C8\",\"size\":3,\"tag\":40755},\"40756\":{\"data\":\"420300\",\"size\":3,\"tag\":40756},\"40757\":{\"data\":\"22\",\"size\":1,\"tag\":40757},\"40758\":{\"data\":\"0256\",\"size\":2,\"tag\":40758},\"40759\":{\"data\":\"FA0D5F44\",\"size\":4,\"tag\":40759},\"40769\":{\"data\":\"00000017\",\"size\":4,\"tag\":40769}}},\"trackTwoData\":\"4249720020000234D24122261393525399999\",\"transactionAcceptedByCard\":true,\"transactionInitializeDateTime\":\"201112181040447\",\"transactionState\":\"OFFLINE_DECLINED\"},\"contactlessCvm\":\"NO_CVM\",\"cvm\":\"NO_CVM\",\"fallbackIccToMag\":false,\"message\":\"Read Card Success\",\"result\":\"SUCCESS\"}"
            )
        }

        testHelper.createDatabase(TEST_DB, 1).apply {
            //inserting some values to the table "transaction_log"
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values1)
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values2)
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values3)
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values4)
            close()
        }

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        ).build()

        val count = database.getTransactionLogDao().getCountByTransactionType("123456", "PURCHASE")
        val cardScheme = "%\"" + "cardScheme" + "\":\"" + CardSchemeType.VISA_CREDIT.name + "\"%"
        val sumByCardScheme = database.getTransactionLogDao()
            .getSumByOriginalTransactionTypeWithCardScheme(cardScheme, "123456", "VOID", "PURCHASE")
        val countByCardScheme = database.getTransactionLogDao()
            .getCountByOriginalTransactionTypeWithCardScheme(
                cardScheme,
                "123456",
                "VOID",
                "PURCHASE"
            )
        assertEquals(2, count)
        assertEquals(2, countByCardScheme)
        assertEquals(46912, sumByCardScheme)

        val originalCount = database.getTransactionLogDao()
            .getCountByOriginalTransactionType("123456", "VOID", "PURCHASE")
        assertEquals(2, originalCount)
    }

    @Test
    fun testTransactionLogDaoGetSumByTransactionType() {
        val values1 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "10101")
            put(TransactionLog.COLUMN_RRN, "20202")
            put(TransactionLog.COLUMN_AUTH_CODE, "30303")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 12345)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PRE AUTH")
            put(TransactionLog.COLUMN_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
            put(TransactionLog.COLUMN_RECONCILE_BATCH_NO, "123456")
            put(TransactionLog.COLUMN_TRANSACTION_AMOUNT, 2500)
        }

        val values2 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "40404")
            put(TransactionLog.COLUMN_RRN, "50505")
            put(TransactionLog.COLUMN_AUTH_CODE, "60606")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 23456)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
            put(TransactionLog.COLUMN_RECONCILE_BATCH_NO, "123456")
            put(TransactionLog.COLUMN_TRANSACTION_AMOUNT, 1000)
        }

        testHelper.createDatabase(TEST_DB, 1).apply {
            //inserting some values to the table "transaction_log"
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values1)
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values2)
            close()
        }

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        ).build()

        val sum = database.getTransactionLogDao().getSumByTransactionType("123456", "PURCHASE")
        assertEquals(3500, sum)
    }

    @Test
    fun testTransactionLogDaoUpdateReconcileStatus() {
        val values1 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "10101")
            put(TransactionLog.COLUMN_RRN, "20202")
            put(TransactionLog.COLUMN_AUTH_CODE, "30303")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 12345)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PRE AUTH")
            put(TransactionLog.COLUMN_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
            put(TransactionLog.COLUMN_RECONCILE_BATCH_NO, "123456")
            put(TransactionLog.COLUMN_TRANSACTION_AMOUNT, 2500)
        }

        val values2 = ContentValues().apply {
            put(TransactionLog.COLUMN_INVOICE_NUM, "40404")
            put(TransactionLog.COLUMN_RRN, "50505")
            put(TransactionLog.COLUMN_AUTH_CODE, "60606")
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_AMOUNT, 23456)
            put(TransactionLog.COLUMN_ORIGINAL_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_TYPE, "PURCHASE")
            put(TransactionLog.COLUMN_TRANSACTION_STATUS, "APPROVED")
            put(TransactionLog.COLUMN_RECONCILE_BATCH_NO, "123457")
            put(TransactionLog.COLUMN_TRANSACTION_AMOUNT, 1000)
        }

        testHelper.createDatabase(TEST_DB, 1).apply {
            //inserting some values to the table "transaction_log"
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values1)
            insert(TransactionLog.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values2)
            close()
        }

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        ).build()

        val count = database.getTransactionLogDao()
            .updateReconcileStatus("123456", "09/23/2020", "19:25:26")
        assertEquals(1, count)
    }

    @Test
    fun testTransactionLIdDaoStan() {
        val values1 = ContentValues().apply {
            put("stan", 1)
            put("rrn", 1)
            put("invoice_number", 1)
            put("reconciliation_batch_number", 1)
        }

        testHelper.createDatabase(TEST_DB, 1).apply {
            //inserting some values to the table "transaction_log"
            insert(TransactionConfig.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values1)
            close()
        }

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        ).build()

        val stan = database.getTransactionConfigDao().getStan()
        assertEquals(1, stan)
        val stanIncrementRowsAffected = database.getTransactionConfigDao().incrementStan()
        assertEquals(1, stanIncrementRowsAffected)
        val updatedStan = database.getTransactionConfigDao().getStan()
        assertEquals(2, updatedStan)
    }

    @Test
    fun testTransactionLIdDaoReconciliationBatchNumber() {
        val values1 = ContentValues().apply {
            put("stan", 1)
            put("rrn", 1)
            put("invoice_number", 1)
            put("reconciliation_batch_number", 1)
        }

        testHelper.createDatabase(TEST_DB, 1).apply {
            //inserting some values to the table "transaction_log"
            insert(TransactionConfig.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values1)
            close()
        }

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        ).build()

        val reconciliationBatchNumber =
            database.getTransactionConfigDao().getReconciliationBatchNumber()
        assertEquals(1, reconciliationBatchNumber)
        val reconciliationBatchNumberIncrementRowsAffected =
            database.getTransactionConfigDao().incrementReconciliationBatchNumber()
        assertEquals(1, reconciliationBatchNumberIncrementRowsAffected)
        val updatedReconciliationBatchNumber =
            database.getTransactionConfigDao().getReconciliationBatchNumber()
        assertEquals(2, updatedReconciliationBatchNumber)
    }

    @Test
    fun testTransactionLIdDaoRrn() {
        val values1 = ContentValues().apply {
            put("stan", 1)
            put("rrn", 1)
            put("invoice_number", 1)
            put("reconciliation_batch_number", 1)
        }

        testHelper.createDatabase(TEST_DB, 1).apply {
            //inserting some values to the table "transaction_log"
            insert(TransactionConfig.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values1)
            close()
        }

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        ).build()

        val rrn = database.getTransactionConfigDao().getRrn()
        assertEquals(1, rrn)
        val rrnIncrementRowsAffected = database.getTransactionConfigDao().incrementRrn()
        assertEquals(1, rrnIncrementRowsAffected)
        val updatedRrn = database.getTransactionConfigDao().getRrn()
        assertEquals(2, updatedRrn)
    }

    @Test
    fun testTransactionLIdDaoInvoiceNumber() {
        val values1 = ContentValues().apply {
            put("stan", 1)
            put("rrn", 1)
            put("invoice_number", 1)
            put("reconciliation_batch_number", 1)
        }

        testHelper.createDatabase(TEST_DB, 1).apply {
            //inserting some values to the table "transaction_log"
            insert(TransactionConfig.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values1)
            close()
        }

        val database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            TEST_DB
        ).build()

        val invoiceNumber = database.getTransactionConfigDao().getInvoiceNumber()
        assertEquals(1, invoiceNumber)
        var invoiceNumberIncrementRowsAffected =
            database.getTransactionConfigDao().incrementInvoiceNumber()
        assertEquals(1, invoiceNumberIncrementRowsAffected)
        var updatedInvoiceNumber = database.getTransactionConfigDao().getInvoiceNumber()
        assertEquals(2, updatedInvoiceNumber)
        invoiceNumberIncrementRowsAffected =
            database.getTransactionConfigDao().incrementInvoiceNumber()
        assertEquals(1, invoiceNumberIncrementRowsAffected)
        updatedInvoiceNumber = database.getTransactionConfigDao().getInvoiceNumber()
        assertEquals(3, updatedInvoiceNumber)
    }*/
}