package global.citytech.finpos.processor.nibl.receipt.detailReport;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Collections;
import java.util.List;

import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportReceipt;
import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportReceiptHandler;
import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportReceiptMaker;
import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportRequestModel;
import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportResponseModel;
import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportUseCase;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.utility.JsonUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

/**
 * Created by Unique Shakya on 1/5/2021.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(DetailReportUseCase.class)
public class DetailReportUseCaseTest {

    private TerminalRepository terminalRepository;
    private Notifier notifier;
    private TransactionRepository transactionRepository;
    private DetailReportReceiptMaker detailReportReceiptMaker;
    private DetailReportReceiptHandler detailReportReceiptHandler;
    private PrinterService printerService;

    @Before
    public void setUp() throws Exception {
        this.terminalRepository = PowerMockito.mock(TerminalRepository.class);
        this.notifier = PowerMockito.mock(Notifier.class);
        this.transactionRepository = PowerMockito.mock(TransactionRepository.class);
        this.detailReportReceiptMaker = PowerMockito.mock(DetailReportReceiptMaker.class);
        this.detailReportReceiptHandler = PowerMockito.mock(DetailReportReceiptHandler.class);
        this.printerService = PowerMockito.mock(PrinterService.class);
    }

    @Test
    public void executeTest() throws Exception {
        String batchNumber = "0001";
        List<TransactionLog> transactionLogs = this.mockTransactionLogs();
        DetailReportReceipt detailReportReceipt = this.mockDetailReportReceipt();
        PowerMockito.when(this.terminalRepository.getReconciliationBatchNumber()).thenReturn(batchNumber);

        PowerMockito.doNothing().when(this.notifier).notify(any());
        PowerMockito.doNothing().when(this.notifier).notify(any(), anyString());

        PowerMockito.when(this.transactionRepository.getTransactionLogsByBatchNumber(batchNumber))
                .thenReturn(transactionLogs);

        PowerMockito.whenNew(DetailReportReceiptMaker.class).withArguments(this.terminalRepository)
                .thenReturn(this.detailReportReceiptMaker);
        PowerMockito.when(this.detailReportReceiptMaker.prepare(batchNumber, transactionLogs))
                .thenReturn(detailReportReceipt);

        PowerMockito.whenNew(DetailReportReceiptHandler.class).withArguments(this.printerService)
                .thenReturn(this.detailReportReceiptHandler);
        PowerMockito.doNothing().when(this.detailReportReceiptHandler).print(detailReportReceipt);

        DetailReportRequestModel detailReportRequestModel = new DetailReportRequestModel();
        DetailReportUseCase detailReportUseCase = new DetailReportUseCase(this.terminalRepository,
                this.notifier);
        DetailReportResponseModel detailReportResponseModel = detailReportUseCase.execute(detailReportRequestModel);

        assertNotNull(detailReportResponseModel);
        assertEquals(Result.SUCCESS, detailReportResponseModel.getResult());
    }

    private DetailReportReceipt mockDetailReportReceipt() {
        String detailReportReceiptString = "{\"retailer\":{\"retailerName\":\"CityTech\",\"retailerAddress\":\"Kamaladi\",\"merchantId\":\"1122334455\",\"terminalId\":\"1234567890\"},\"performance\":{\"startDateTime\":\"210105151328172\",\"endDateTime\":\"210105151328172\"},\"reconciliationBatchNumber\":\"0001\",\"host\":\"000048\",\"detailReports\":[{\"cardNumber\":\"424942******2330\",\"cardScheme\":\"Visa Credit\",\"invoiceNumber\":\"111111\",\"approvalCode\":\"123342\",\"transactionType\":\"PURCHASE\",\"amount\":\"NPR 1500.00\"}]}";
        return JsonUtils.fromJsonToObj(detailReportReceiptString, DetailReportReceipt.class);
    }

    private List<TransactionLog> mockTransactionLogs() {
        CardDetails cardDetails = new CardDetails();
        cardDetails.setPrimaryAccountNumber("4249420012002330");
        cardDetails.setCardScheme(CardSchemeType.VISA_CREDIT);
        ReadCardResponse readCardResponse = new ReadCardResponse(cardDetails, Result.SUCCESS, "SUCCESS");
        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
                .withTransactionAmount(1500.00)
                .withAuthCode("123342")
                .withReadCardResponse(readCardResponse)
                .withInvoiceNumber("111111")
                .withTransactionType(TransactionType.PURCHASE)
                .build();
        return Collections.singletonList(transactionLog);
    }
}
