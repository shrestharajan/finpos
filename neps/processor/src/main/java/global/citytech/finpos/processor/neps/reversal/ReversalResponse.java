package global.citytech.finpos.processor.neps.reversal;

import global.citytech.finpos.processor.neps.transaction.TransactionResponse;

/**
 * Created by Rishav Chudal on 9/29/20.
 */
public class ReversalResponse extends TransactionResponse {

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private String message;
        private String stan;
        private boolean approved;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withDebugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public Builder withDebugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder withShouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public Builder withApproved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public ReversalResponse build() {
            ReversalResponse reversalResponseModel = new ReversalResponse();
            reversalResponseModel.debugRequestMessage = this.debugRequestMessage;
            reversalResponseModel.approved = this.approved;
            reversalResponseModel.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            reversalResponseModel.stan = this.stan;
            reversalResponseModel.debugResponseMessage = this.debugResponseMessage;
            reversalResponseModel.message = this.message;
            return reversalResponseModel;
        }
    }
}
