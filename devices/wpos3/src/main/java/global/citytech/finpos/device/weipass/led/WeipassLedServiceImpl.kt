package global.citytech.finpos.device.weipass.led

import com.wiseasy.emvprocess.SDKInstance
import global.citytech.finpos.device.weipass.utils.WeipassConstant.LED_ACTION_FAILURE
import global.citytech.finpos.device.weipass.utils.WeipassConstant.LED_ACTION_SUCCESS
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_LED_ACTION_OFF
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_LED_ACTION_ON
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.led.LedAction
import global.citytech.finposframework.hardware.io.led.LedLight
import global.citytech.finposframework.hardware.io.led.LedRequest
import global.citytech.finposframework.hardware.io.led.LedService

/**
 * Created by Rishav Chudal on 6/4/20.
 */
class WeipassLedServiceImpl:
    LedService {
    private var result = -1
    private lateinit var response: DeviceResponse

    override fun doTurnLedWith(request: LedRequest): DeviceResponse {
        response = DeviceResponse(Result.FAILURE, LED_ACTION_FAILURE)
        analyzeLedAction(request)
        if (result == 0) {
            response = DeviceResponse(Result.SUCCESS, LED_ACTION_SUCCESS)
        }
        return response
    }

    private fun analyzeLedAction(request: LedRequest) {
        if (request.ledAction == LedAction.ON) {
            proceedLedActionOnSdk(request.ledLight, SDK_LED_ACTION_ON)
        } else {
            proceedLedActionOnSdk(request.ledLight, SDK_LED_ACTION_OFF)
        }
    }

    private fun proceedLedActionOnSdk(ledLight: LedLight, action: Int) {
        WeipassLogger.log("Led Light ::: "
            .plus(ledLight)
            .plus(" Action (0 -> Off, 1 -> On) ::: ")
            .plus(action))
        try {
            when (ledLight) {
                LedLight.BLUE -> result = SDKInstance.mCore.led(
                    action,
                    SDK_LED_ACTION_OFF,
                    SDK_LED_ACTION_OFF,
                    SDK_LED_ACTION_OFF,
                    action)

                LedLight.YELLOW -> result = SDKInstance.mCore.led(
                    SDK_LED_ACTION_OFF,
                    action,
                    SDK_LED_ACTION_OFF,
                    SDK_LED_ACTION_OFF,
                    action)

                LedLight.GREEN -> result = SDKInstance.mCore.led(
                    SDK_LED_ACTION_OFF,
                    SDK_LED_ACTION_OFF,
                    action,
                    SDK_LED_ACTION_OFF,
                    action)

                LedLight.RED -> result = SDKInstance.mCore.led(
                    SDK_LED_ACTION_OFF,
                    SDK_LED_ACTION_OFF,
                    SDK_LED_ACTION_OFF,
                    action,
                    action)

                LedLight.ALL -> result = SDKInstance.mCore.led(action,
                    action,
                    action,
                    action,
                    action)
            }
        } catch (exception: Exception) {
            WeipassLogger.log("Exception ::: ".plus(exception.message))
        }
        WeipassLogger.log("Led Light Result ::: ".plus(result))
    }
}