package global.citytech.finpos.merchant.presentation.model.purchase

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

/**
 * Created by Rishav Chudal on 11/2/20.
 */
data class ManualPurchaseRequestItem(
    val amount: BigDecimal? = null,
    val transactionType: TransactionType? = null,
    val cardNumber: String = "",
    val expiryDate: String = "",
    val cvv: String = "",
    val vatInfo : String = ""
)