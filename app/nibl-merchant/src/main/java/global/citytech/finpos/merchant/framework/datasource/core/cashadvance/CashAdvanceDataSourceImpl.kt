package global.citytech.finpos.merchant.framework.datasource.core.cashadvance

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.cashadvance.CashAdvanceDataSource
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionAuthenticatorImpl
import global.citytech.finpos.merchant.framework.datasource.device.*

import global.citytech.finpos.merchant.domain.model.cashadvance.CashAdvanceRequestEntity
import global.citytech.finpos.merchant.domain.model.cashadvance.CashAdvanceResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.presentation.data.mapToCashAdvanceResponseUiModel
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.cashadvance.CashAdvanceRequestItem
import global.citytech.finpos.merchant.presentation.model.cashadvance.ManualCashAdvanceRequestItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finposframework.switches.ProcessorManager
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 2/4/2021.
 */
class CashAdvanceDataSourceImpl : CashAdvanceDataSource {
    override fun performCashAdvance(
        configurationItem: ConfigurationItem,
        cashAdvanceRequestItem: CashAdvanceRequestItem
    ): Observable<CashAdvanceResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val transactionRepository =
            TransactionRepositoryImpl(
                NiblMerchant.INSTANCE
            )
        val transactionRequest = prepareTransactionRequest(cashAdvanceRequestItem)
        val deviceController = DeviceControllerImpl(NiblMerchant.INSTANCE)
        val cardReader = CardSourceImpl(NiblMerchant.getActivityContext())
        val transactionAuthenticator =
            TransactionAuthenticatorImpl()
        val printerServiceImpl = PrinterSourceImpl(NiblMerchant.getActivityContext())
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
        val cashAdvanceRequestEntity = CashAdvanceRequestEntity(
            readCardService = cardReader,
            deviceController = deviceController,
            printerService = printerServiceImpl,
            transactionAuthenticator = transactionAuthenticator,
            transactionRepository = transactionRepository,
            transactionRequest = transactionRequest,
            applicationRepository = applicationRepository,
            ledService = LedSourceImpl(NiblMerchant.INSTANCE),
            soundService = SoundSourceImpl(NiblMerchant.INSTANCE)
        )
        val cashAdvanceRequester =
            ProcessorManager.getInterface(
                terminalRepository,
                NotificationHandler
            ).cashAdvanceRequester
        return Observable.fromCallable {
            cashAdvanceRequester.execute(cashAdvanceRequestEntity.mapToModel())
                .mapToCashAdvanceResponseUiModel()
        }
    }

    private fun prepareTransactionRequest(cashAdvanceRequestItem: CashAdvanceRequestItem): TransactionRequest? {
        val transactionRequest = TransactionRequest(cashAdvanceRequestItem.transactionType)
        transactionRequest.amount = cashAdvanceRequestItem.amount!!
        return transactionRequest
    }

    override fun performManualCashAdvance(
        configurationItem: ConfigurationItem,
        manualCashAdvanceRequestItem: ManualCashAdvanceRequestItem
    ): Observable<CashAdvanceResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val transactionRepository =
            TransactionRepositoryImpl(
                NiblMerchant.INSTANCE
            )
        val transactionRequest = prepareManualTransactionRequest(manualCashAdvanceRequestItem)
        val deviceController = DeviceControllerImpl(NiblMerchant.INSTANCE)
        val cardReader = CardSourceImpl(NiblMerchant.getActivityContext())
        val transactionAuthenticator =
            TransactionAuthenticatorImpl()
        val printerServiceImpl = PrinterSourceImpl(NiblMerchant.getActivityContext())
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
        val cashAdvanceRequestEntity = CashAdvanceRequestEntity(
            readCardService = cardReader,
            deviceController = deviceController,
            printerService = printerServiceImpl,
            transactionAuthenticator = transactionAuthenticator,
            transactionRepository = transactionRepository,
            transactionRequest = transactionRequest,
            applicationRepository = applicationRepository,
            ledService = LedSourceImpl(NiblMerchant.INSTANCE),
            soundService = SoundSourceImpl(NiblMerchant.INSTANCE)
        )
        val manualCashAdvanceRequester =
            ProcessorManager.getInterface(
                terminalRepository,
                NotificationHandler
            ).manualCashAdvanceRequester
        return Observable.fromCallable {
            manualCashAdvanceRequester.execute(cashAdvanceRequestEntity.mapToModel())
                .mapToCashAdvanceResponseUiModel()
        }
    }

    private fun prepareManualTransactionRequest(manualCashAdvanceRequestItem: ManualCashAdvanceRequestItem): TransactionRequest? {
        val transactionRequest = TransactionRequest(manualCashAdvanceRequestItem.transactionType)
        transactionRequest.amount = manualCashAdvanceRequestItem.amount!!
        transactionRequest.cardNumber = manualCashAdvanceRequestItem.cardNumber
        transactionRequest.expiryDate = manualCashAdvanceRequestItem.expiryDate
        return transactionRequest
    }
}