package global.citytech.finpos.merchant.presentation.payment;

/**
 * Created by Rishav Chudal on 6/20/21.
 */
public class PaymentFeature {
    private final String code;
    private final String label;

    public PaymentFeature(String code, String label) {
        this.code = code;
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }
}
