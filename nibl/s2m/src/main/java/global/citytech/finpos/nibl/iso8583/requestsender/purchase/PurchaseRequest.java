package global.citytech.finpos.nibl.iso8583.requestsender.purchase;

import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.transaction.Request;


/**
 * Modified by Rishav Chudal on 10/15/20.
 */
public class PurchaseRequest implements Request {
    private String pan;
    private double transactionAmount;
    private String localTime;
    private String localDate;
    private PosEntryMode entryMode;
    private String posConditionCode;
    private String track2Data;
    private String additionalData;
    private String currencyCode;
    private String pinBlock;
    private String emvData;
    private String expireDate;
    private String cardSequenceNumber;
    private String invoiceNumber;
    private String retrievalReferenceNumber;
    private boolean fallbackFromIccToMag;
    private String cvv;

    public boolean isFallbackFromIccToMag() {
        return fallbackFromIccToMag;
    }

    public void setFallbackFromIccToMag(boolean fallbackFromIccToMag) {
        this.fallbackFromIccToMag = fallbackFromIccToMag;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public String getLocalDate() {
        return localDate;
    }

    public void setLocalDate(String localDate) {
        this.localDate = localDate;
    }

    public PosEntryMode getPosEntryMode() {
        return entryMode;
    }

    public void setPosEntryMode(PosEntryMode posEntryMode) {
        this.entryMode = posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public void setPosConditionCode(String posConditionCode) {
        this.posConditionCode = posConditionCode;
    }

    public String getTrack2Data() {
        return track2Data;
    }

    public void setTrack2Data(String track2Data) {
        this.track2Data = track2Data;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getPinBlock() {
        return pinBlock;
    }

    public void setPinBlock(String pinBlock) {
        this.pinBlock = pinBlock;
    }

    public String getEmvData() {
        return emvData;
    }

    public void setEmvData(String emvData) {
        this.emvData = emvData;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public PosEntryMode getEntryMode() {
        return entryMode;
    }

    public void setEntryMode(PosEntryMode entryMode) {
        this.entryMode = entryMode;
    }

    public String getCardSequenceNumber() {
        return cardSequenceNumber;
    }

    public void setCardSequenceNumber(String cardSequenceNumber) {
        this.cardSequenceNumber = cardSequenceNumber;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    @Override
    public String toString() {
        return "PurchaseRequest{" +
                "pan='" + pan + '\'' +
                ", transactionAmount=" + transactionAmount +
                ", transmissionDateTime='" + localTime + '\'' +
                ", localDataTime='" + localDate + '\'' +
                ", entryMode=" + entryMode +
                ", posConditionCode='" + posConditionCode + '\'' +
                ", track2Data='" + track2Data + '\'' +
                ", additionalData='" + additionalData + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                ", pinBlock='" + pinBlock + '\'' +
                ", emvData='" + emvData + '\'' +
                ", expireDate='" + expireDate + '\'' +
                ", cardSequenceNumber='" + cardSequenceNumber + '\'' +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", cardCVV='" + cvv + '\'' +
                '}';
    }
}
