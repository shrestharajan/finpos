package global.citytech.finpos.merchant.presentation.model.preauth.completion

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

/**
 * Created by Unique Shakya on 9/29/2020.
 */
class AuthorisationCompletionRequestItem(
    val transactionType: TransactionType,
    val amount: BigDecimal,
    val originalData: String
) {
}