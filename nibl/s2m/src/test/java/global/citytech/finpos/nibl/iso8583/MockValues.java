package global.citytech.finpos.nibl.iso8583;

import global.citytech.finpos.nibl.NIBLHostConfig;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;

/**
 * Created by Rishav Chudal on 9/28/20.
 */
public class MockValues {
    public final static String TERMINAL_ID = "99994773";
    public final static String ACCQUIRER_ID = "405635629481338";
    public final static String PAN = "4359780000415378";
    public final static String EXPIRY_DATE = "2202";
    public final static double AMOUNT = 102.0;
    public final static String TRACK_2_DATA = "4249720020000234=24122261393525399999";
    public final static String PIN_BLOCK = "7C8DE0C78AEA2044";
    public final static String EMV_DATA = "82023C009F02060000000000289F03060000000000004F07A0000000031010500B56495341204352454449549F120B56495341204352454449549F360200CA9F26085C4637D46AFA9A359F2701809F34034203008407A00000000310109F100706010A03A0A8009F1E0831313130303837379F3303E0F8C89F350122950508000400009F1A0205245F2A0205249A032008289C01009F370460C801959F090200969F4104000012345F340101";
    public final static String STAN = "000043";
    public final static String INVOICE_NUMBER = "000043";
    public final static String POS_CONDITION_CODE_NORMAL_PRESENTMENT = "00";
    public final static String POS_CONDITION_CODE_CARD_NOT_PRESENT = "05";
    public final static String POS_CONDITION_CODE_PRE_AUTH_REQUEST = "06";
    public final static String PURCHASE_PROCESSING_CODE = "000000";
    public final static String PURCHASE_WITH_CASHBACK_PROCESSING_CODE = "090000";
    public final static String PRE_AUTH_PROCESSING_CODE = "300000";
    public final static String REFUND_PROCESSING_CODE = "200000";
    public final static String CASH_ADVANCE_PROCESSING_CODE = "010000";
    public final static String PRE_AUTH_COMPLETION_PROCESSING_CODE = "000000";
    public final static String VOID_PROCESSING_CODE = "020000";




    public static TerminalInfo getTerminalInfo() {
        return  new TerminalInfo(
                TERMINAL_ID,
                ACCQUIRER_ID,"",
                "Citytech",
                "Kamaladi"
        );
    }

    public static HostInfo getPrimaryHostInfo() {
        return NIBLHostConfig.getPrimaryConfig();
    }

    public static HostInfo getSecondaryHostInfo() {
        return NIBLHostConfig.getSecondaryConfig();
    }

    public static ConnectionParam getConnectionParam() {
        return new ConnectionParam(
                getPrimaryHostInfo(),
                getSecondaryHostInfo()
        );
    }

    public static PosEntryMode getIccPosEntryMode() {
        return PosEntryMode.ICC;
    }

    public static RequestContext getRequestContext(String stan) {
        return new RequestContext(
               getStan(stan),
               getTerminalInfo(),
               getConnectionParam()
        );
    }

    private static String getStan(String value) {
        if (value.isEmpty()) {
            return STAN;
        }
        return value;
    }


}
