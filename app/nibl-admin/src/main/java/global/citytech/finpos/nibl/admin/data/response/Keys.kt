package global.citytech.finpos.nibl.admin.data.response

data class Keys(
    var devicePrivateKey: List<DevicePrivateKey>? = null,
    var switchPublicKey: List<SwitchPublicKey>? = null
)