package global.citytech.finpos.merchant.presentation.preauth

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityBaseTransactionBinding
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.preauth.ManualPreAuthRequestItem
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionActivity
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finposframework.usecases.TransactionType

/**
 * Created by Rishav Chudal on 8/31/20.
 */
class PreAuthActivity :
    BaseTransactionActivity<ActivityBaseTransactionBinding, PreAuthViewModel>() {

    private lateinit var viewModel: PreAuthViewModel
    private var manualPurchase: Boolean = false
    private var cardNumber: String = ""
    private var expiryDate: String = ""
    private var cvv: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBaseViews(true)
        this.viewModel = getViewModel()
        initObservers()
        this.startAmountActivity()
    }

    private fun initObservers() {
        this.viewModel.configurationItem.observe(this, Observer { initiatePreAuth(it) })
        initBaseLiveDataObservers()
    }

    private fun initiatePreAuth(configurationItem: ConfigurationItem) {
        this.showProgressScreen("Initializing Transaction...")
        if (manualPurchase) {
            this.viewModel.doManualPreAuth(
                configurationItem,
                manualPreAuthRequestItem = preparePreAuthRequestItem()
            )
        } else {
            this.viewModel.doPreAuth(
                configurationItem,
                amount,
                getTransactionType()
            )
        }
    }

    private fun preparePreAuthRequestItem(): ManualPreAuthRequestItem {
        return ManualPreAuthRequestItem(
            amount, getTransactionType(), cardNumber, expiryDate, cvv
        )
    }

    override fun onPositiveButtonClick() {
        this.showProgressScreen("Printing Customer Copy...")
        this.viewModel.printCustomerCopy()
    }

    override fun onNegativeButtonClick() = checkForCardPresent()

    override fun onManualButtonClicked() {
        this.viewModel.shouldDispose = true
        this.viewModel.doCleanUp(BaseTransactionViewModel.Task.MANUAL_TRANSACTION)
        this.manualPurchase = true
    }

    override fun onCancelButtonClicked() {
        this.viewModel.shouldDispose = true
        this.viewModel.doCleanUp(BaseTransactionViewModel.Task.RETURN_TO_DASHBOARD)
    }

    override fun getBindingVariable(): Int = BR.preAuthViewModel

    override fun getLayout(): Int = R.layout.activity_base_transaction

    override fun getViewModel(): PreAuthViewModel =
        ViewModelProviders.of(this)[PreAuthViewModel::class.java]

    override fun getTransactionViewModel(): BaseTransactionViewModel = this.viewModel

    override fun getTransactionType(): TransactionType = TransactionType.PRE_AUTH

    override fun onAmountActivityResult(data: Intent?) {
        data?.let {
            val amountInString =
                data.getStringExtra(getString(R.string.intent_confirmed_amount))!!
            amount = amountInString.toBigDecimal()
            viewModel.getConfigurationItem()
        }
    }

    override fun onManualActivityResult(data: Intent?) {
        data?.let {
            this.cardNumber = data.getStringExtra(getString(R.string.title_card_number))!!
            this.expiryDate = data.getStringExtra(getString(R.string.title_expiry_date))!!
            this.cvv = data.getStringExtra(getString(R.string.title_cvv))!!
            this.viewModel.getConfigurationItem()
        }
    }
}