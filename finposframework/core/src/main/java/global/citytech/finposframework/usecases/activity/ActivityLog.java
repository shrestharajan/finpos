package global.citytech.finposframework.usecases.activity;


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/11/08 - 12:02 PM
 */

import java.sql.Timestamp;

public class ActivityLog {
    private String stan;
    private ActivityLogType type;
    private String merchantId;
    private String terminalId;
    private String batchNumber;
    private Long amount;
    private String responseCode;
    private String isoRequest;
    private String isoResponse;
    private ActivityLogStatus status;
    private String remarks;
    private Timestamp posDateTime;
    private String additionalData;

    private ActivityLog() {
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public ActivityLogType getType() {
        return type;
    }

    public void setType(ActivityLogType type) {
        this.type = type;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getIsoRequest() {
        return isoRequest;
    }

    public void setIsoRequest(String isoRequest) {
        this.isoRequest = isoRequest;
    }

    public String getIsoResponse() {
        return isoResponse;
    }

    public void setIsoResponse(String isoResponse) {
        this.isoResponse = isoResponse;
    }

    public ActivityLogStatus getStatus() {
        return status;
    }

    public void setStatus(ActivityLogStatus status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Timestamp getPosDateTime() {
        return posDateTime;
    }

    public void setPosDateTime(Timestamp posDateTime) {
        this.posDateTime = posDateTime;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }

    public static final class Builder {
        private String stan;
        private ActivityLogType type;
        private String merchantId;
        private String terminalId;
        private String batchNumber;
        private Long amount;
        private String responseCode;
        private String isoRequest;
        private String isoResponse;
        private ActivityLogStatus status;
        private String remarks;
        private Timestamp posDateTime;
        private String additionalData;

        public Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder withType(ActivityLogType type) {
            this.type = type;
            return this;
        }

        public Builder withMerchantId(String merchantId) {
            this.merchantId = merchantId;
            return this;
        }

        public Builder withTerminalId(String terminalId) {
            this.terminalId = terminalId;
            return this;
        }

        public Builder withBatchNumber(String batchNumber) {
            this.batchNumber = batchNumber;
            return this;
        }

        public Builder withAmount(Long amount) {
            this.amount = amount;
            return this;
        }

        public Builder withResponseCode(String responseCode) {
            this.responseCode = responseCode;
            return this;
        }

        public Builder withIsoRequest(String isoRequest) {
            this.isoRequest = isoRequest;
            return this;
        }

        public Builder withIsoResponse(String isoResponse) {
            this.isoResponse = isoResponse;
            return this;
        }

        public Builder withStatus(ActivityLogStatus status) {
            this.status = status;
            return this;
        }

        public Builder withRemarks(String remarks) {
            this.remarks = remarks;
            return this;
        }

        public Builder withPosDateTime(Timestamp posDateTime) {
            this.posDateTime = posDateTime;
            return this;
        }

        public Builder withAdditionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public ActivityLog build() {
            ActivityLog activityLog = new ActivityLog();
            activityLog.stan = this.stan;
            activityLog.type = this.type;
            activityLog.merchantId = this.merchantId;
            activityLog.terminalId = this.terminalId;
            activityLog.batchNumber = this.batchNumber;
            activityLog.amount = this.amount;
            activityLog.responseCode = this.responseCode;
            activityLog.isoRequest = this.isoRequest;
            activityLog.isoResponse = this.isoResponse;
            activityLog.status = this.status;
            activityLog.remarks = this.remarks;
            activityLog.posDateTime = this.posDateTime;
            activityLog.additionalData = this.additionalData;
            return activityLog;
        }
    }
}
