package global.citytech.finposframework.repositories;

import java.util.List;

import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
import global.citytech.finposframework.usecases.transaction.data.Bin;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;

public interface TransactionRepository {

    void updateReceiptLog(ReceiptLog receiptLog);

    ReceiptLog getReceiptLog();

    ReceiptLog getReceiptLogByStan(String stan);

    void updateTransactionLog(TransactionLog transactionLog);

    TransactionLog getTransactionLogWithInvoiceNumber(String invoiceNumber, String batchNumber);

    TransactionLog getTransactionLogWithInvoiceNumberWithoutBatchNumber(String invoiceNumber);

    TransactionLog getTipTransactionLogWithInvoiceNumber(String invoiceNumber, String batchNumber);

    TransactionLog getTransactionLogWithAuthCode(String authCode);

    TransactionLog getTransactionLogWithRRN(String rrn);

    TransactionLog getRecentTransactionLog();

    List<TransactionLog> getTransactionLogsByBatchNumber(String batchNumber);

    void updateAuthorizationCompletedStatusByAuthCode(boolean completed, TransactionType transactionType, String authCode);

    void updateAuthorizationCompletedStatusByRRN(boolean completed, TransactionType transactionType, String rrn);

    void deleteTransactionLogByStan(String stan);

    void updateVoidStatusForGivenTransactionDetails(
            boolean voided,
            TransactionType transactionType,
            String invoiceNumber
    );

    void updateTipAdjustedStatusForGivenTransactionDetails(
            boolean tipAdjusted,
            TransactionType transactionType,
            String invoiceNumber
    );

    void saveAutoReversal(AutoReversal autoReversal);

    AutoReversal getAutoReversal();

    List<AutoReversal> getAutoReversalList();

    boolean isAutoReversalPresent();

    void incrementAutoReversalRetryCount(AutoReversal autoReversal);

    void removeAutoReversal(AutoReversal autoReversal);

    void clearAutoReversal();

    void changeAutoReversalStatus(AutoReversal autoReversal, AutoReversal.Status status);

    List<Bin> getTransactionBins();

    void clearSharedPreference();
}
