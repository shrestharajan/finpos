package global.citytech.finposframework.usecases.transaction;

import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest;
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse;
import global.citytech.finposframework.usecases.transaction.data.TransactionAuthorizationResponse;

public interface TransactionAuthenticator {
    TransactionAuthorizationResponse authenticate();
    PinResponse authenticateUser(PinRequest pinRequest);
}
