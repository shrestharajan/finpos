package global.citytech.finpos.processor.nibl.reconciliation.clear;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reconciliation.clear.BatchClearRequestParameter;

/**
 * Created by Unique Shakya on 9/28/2021.
 */
public class BatchClearRequestModel implements BatchClearRequestParameter, UseCase.Request {

    private ReconciliationRepository reconciliationRepository;
    private PrinterService printerService;
    private TransactionRepository transactionRepository;

    public BatchClearRequestModel(ReconciliationRepository reconciliationRepository, PrinterService printerService, TransactionRepository transactionRepository) {
        this.reconciliationRepository = reconciliationRepository;
        this.printerService = printerService;
        this.transactionRepository = transactionRepository;
    }

    public ReconciliationRepository getReconciliationRepository() {
        return reconciliationRepository;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }
}
