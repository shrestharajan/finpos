package global.citytech.finpos.neps.iso8583.requestsender.batchupload;

import java.math.BigDecimal;

import global.citytech.finposframework.hardware.io.cards.PinBlock;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.transaction.Request;

/**
 * Created by Saurav Ghimire on 6/14/21.
 * sauravnghimire@gmail.com
 */


public class BatchUploadISORequest implements Request {

    private String pan;
    private String originalProcessingCode;
    private BigDecimal transactionAmount;
    private String localTxnDateAndTime;
    private String expirationDate;
    private PosEntryMode posEntryMode;
    private String posConditionCode;
    private String originalRrn;
    private String invoiceNumber;
    private String track2Data;
    private String emvData;
    private String currencyCode;
    private PinBlock pinBlock;

    public String getTrack2Data() {
        return track2Data;
    }

    public String getEmvData() {
        return emvData;
    }

    public String getPan() {
        return pan;
    }

    public String getOriginalProcessingCode() {
        return originalProcessingCode;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public String getLocalTxnDateAndTime() {
        return localTxnDateAndTime;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }


    public String getOriginalRrn() {
        return originalRrn;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public PinBlock getPinBlock() {
        return pinBlock;
    }

    public static final class Builder {
        private String pan;
        private String originalProcessingCode;
        private BigDecimal transactionAmount;
        private String localTxnDateAndTime;
        private String expirationDate;
        private PosEntryMode posEntryMode;
        private String posConditionCode;
        private String originalRrn;
        private String invoiceNumber;
        private String track2Data;
        private String emvData;
        private String currencyCode;
        private PinBlock pinBlock;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public Builder originalProcessingCode(String originalProcessingCode) {
            this.originalProcessingCode = originalProcessingCode;
            return this;
        }

        public Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public Builder localTxnDateAndTime(String localTime) {
            this.localTxnDateAndTime = localTime;
            return this;
        }

        public Builder expirationDate(String expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public Builder posEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public Builder posConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }


        public Builder originalRrn(String originalRrn) {
            this.originalRrn = originalRrn;
            return this;
        }


        public Builder invoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }
        public Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder pinBlock(PinBlock pinBlock) {
            this.pinBlock = pinBlock;
            return this;
        }

        public BatchUploadISORequest build() {
            BatchUploadISORequest batchUploadISORequest = new BatchUploadISORequest();
            batchUploadISORequest.originalRrn = this.originalRrn;
            batchUploadISORequest.originalProcessingCode = this.originalProcessingCode;
            batchUploadISORequest.localTxnDateAndTime = this.localTxnDateAndTime;
            batchUploadISORequest.pan = this.pan;
            batchUploadISORequest.transactionAmount = this.transactionAmount;
            batchUploadISORequest.invoiceNumber = this.invoiceNumber;
            batchUploadISORequest.posConditionCode = this.posConditionCode;
            batchUploadISORequest.expirationDate = this.expirationDate;
            batchUploadISORequest.posEntryMode = this.posEntryMode;
            batchUploadISORequest.track2Data = this.track2Data;
            batchUploadISORequest.emvData = this.emvData;
            batchUploadISORequest.currencyCode = this.currencyCode;
            batchUploadISORequest.pinBlock = this.pinBlock;
            return batchUploadISORequest;
        }
    }
}
