package global.citytech.finposframework.switches.autoreversal;

import global.citytech.finposframework.switches.RequestParameter;

/**
 * Created by Unique Shakya on 6/22/2021.
 */
public interface AutoReversalRequestParameter extends RequestParameter {
}
