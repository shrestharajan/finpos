package global.citytech.finpos.merchant.presentation.model.banner

import global.citytech.finpos.nibl.merchant.presentation.model.response.Logo

/**
 * Created by Rishav Chudal on 11/3/21.
 */
data class TerminalBannersResponse(
    val logo: Logo
)
