package global.citytech.finposframework.hardware.emv.emvkeys

import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.DeviceService

/**
 * Created by Rishav Chudal on 4/30/20.
 */
interface EmvKeysService: DeviceService {
    fun injectEmvKeys(injectionRequest: EmvKeysInjectionRequest): DeviceResponse

    fun eraseAllEmvKeys(): DeviceResponse

}