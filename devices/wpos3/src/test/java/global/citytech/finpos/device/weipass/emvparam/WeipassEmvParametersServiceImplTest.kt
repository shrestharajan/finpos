package global.citytech.finpos.device.weipass.emvparam

import android.content.Context
import com.wiseasy.emvprocess.SDKInstance
import com.wiseasy.emvprocess.TransProcess
import global.citytech.finpos.device.weipass.utils.WeipassConstant
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import sdk4.wangpos.libemvbinder.EmvCore
import sdk4.wangpos.libemvbinder.EmvParam

/**
 * Created by Rishav Chudal on 4/23/20.
 */
class WeipassEmvParametersServiceImplTest {

    @MockK
    private lateinit var mockedEmvParam: EmvParam

    @MockK
    private lateinit var mockedEmvCore: EmvCore

    @MockK
    private lateinit var mockedContext: Context

    private lateinit var SUT:WeipassEmvParametersServiceImpl
    private lateinit var deviceResponse: DeviceResponse
    private val exceptionMessage = "This is a test exception"

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        mockWeipassLogger()
        mockSDKInstanceAndEmvCore()
        mockTransProcess()
        SUT = WeipassEmvParametersServiceImpl(mockedContext)
    }

    private fun mockWeipassLogger() {
        mockkObject(WeipassLogger)
        mockkObject(WeipassLogger.Companion)
        every { WeipassLogger.log(any()) } just Runs
    }

    private fun mockSDKInstanceAndEmvCore() {
        mockkStatic(SDKInstance::class)
        SDKInstance.mEmvCore = mockedEmvCore
    }

    private fun mockTransProcess() {
        mockkStatic(TransProcess::class)
    }

    private fun prepareEmvParametersRequest() =
        EmvParametersRequest(
            "0682",
            "0682",
            "01",
            "E0F0C8",
            "22",
            "7399",
            "06804344",
            "800150400566",
            "Bhat Bhateni Super Market",
            "35E04000",
            "D000F0A000",
            "01"
        )

    @Test
    fun injectEmvParameters_ExceptionInEmvParamInjection_ReturnsFailureResponse() {
        //Setup
        every {
            TransProcess.getEMVParamFromTerminalSetting(any(), any(), any())
        } returns mockedEmvParam

        every { mockedEmvParam.toByteArray() } returns ByteArray(5)

        every {
            mockedEmvCore.setParam(any())
        } throws Exception(exceptionMessage)

        //Run
        this.deviceResponse = SUT.injectEmvParameters(prepareEmvParametersRequest())

        //Verify
        verify {
            TransProcess.getEMVParamFromTerminalSetting(any(), any(), any())
        }
        verify {
            WeipassLogger.log("Exception in injectEmvParamInCore ::: ".plus(exceptionMessage))
        }
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_EMV_PARAM_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectEmvParameters_EmvParamIsNull_ReturnsFailureResponse() {
        //Setup
        every {
            TransProcess.getEMVParamFromTerminalSetting(any(), any(), any())
        } returns null

        //Run
        this.deviceResponse = SUT.injectEmvParameters(prepareEmvParametersRequest())

        //Verify
        verify {
            TransProcess.getEMVParamFromTerminalSetting(any(), any(), any())
        }

        verify { WeipassLogger.log("Emv Param is Null") }
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_EMV_PARAM_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectEmvParameters_EmvParamInjectionResultIsZero_ReturnsSuccessResponse() {
        //Setup
        every {
            TransProcess.getEMVParamFromTerminalSetting(any(), any(), any())
        } returns mockedEmvParam

        every { mockedEmvParam.toByteArray() } returns ByteArray(5)

        every { mockedEmvCore.setParam(any()) } just Runs

        //Run
        this.deviceResponse = SUT.injectEmvParameters(prepareEmvParametersRequest())

        //Verify
        verify {
            TransProcess.getEMVParamFromTerminalSetting(any(), any(), any())
        }

        verify {
            WeipassLogger.log("Emv Param Injection Result ::: ".plus("0"))
        }
        assertEquals(Result.SUCCESS, this.deviceResponse.result)
        assertEquals(WeipassConstant.SUCCESS_EMV_PARAM_INJECT, this.deviceResponse.message)
    }
}