package global.citytech.finpos.nibl.admin.data

data class DeviceInfo(
    var serialNumber: String? = null
)