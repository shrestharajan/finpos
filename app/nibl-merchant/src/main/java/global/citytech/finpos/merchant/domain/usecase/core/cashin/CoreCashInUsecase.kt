package global.citytech.finpos.merchant.domain.usecase.core.cashin

import global.citytech.finpos.merchant.domain.model.cashin.CashInResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.cashin.CashInRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.cashin.CashInRequestItem
import io.reactivex.Observable

class CoreCashInUsecase(private val generateCashInRepository: CashInRepository){
    fun generateCashInRequest(
        configurationItem: ConfigurationItem,
        cashInRequestItem: CashInRequestItem
    ):Observable<CashInResponseEntity> = generateCashInRepository.generateCashInRequest(configurationItem, cashInRequestItem)
}