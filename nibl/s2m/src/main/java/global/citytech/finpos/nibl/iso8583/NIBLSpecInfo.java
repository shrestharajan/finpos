package global.citytech.finpos.nibl.iso8583;

import global.citytech.finposframework.iso8583.DataElementConfigs;
import global.citytech.finposframework.iso8583.SpecInfo;

public class NIBLSpecInfo implements SpecInfo {

    @Override
    public DataElementConfigs getDEConfig() {
        return new NiblDataElementConfig();
    }

    @Override
    public int getMessageHeaderLength() {
        return 2;
    }

    @Override
    public int getMTILength() {
        return 2;
    }
}
