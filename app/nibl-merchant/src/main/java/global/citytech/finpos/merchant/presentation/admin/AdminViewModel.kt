package global.citytech.finpos.merchant.presentation.admin

import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import global.citytech.common.data.Response
import global.citytech.common.extensions.hasInternetConnection
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.AppNotificationRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.MobileNfcPaymentRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.QrPaymentRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.posmode.PosModeRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.reconciliation.ReconciliationRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.*
import global.citytech.finpos.merchant.domain.model.device.DeviceResponse
import global.citytech.finpos.merchant.domain.model.device.DeviceResult
import global.citytech.finpos.merchant.domain.usecase.app.AppNotificationUseCase
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.app.MobilePaymentUseCase
import global.citytech.finpos.merchant.domain.usecase.app.QrPaymentUseCase
import global.citytech.finpos.merchant.domain.usecase.core.posmode.CorePosModeUseCase
import global.citytech.finpos.merchant.domain.usecase.core.reconciliation.CoreReconciliationUseCase
import global.citytech.finpos.merchant.framework.datasource.app.*
import global.citytech.finpos.merchant.framework.datasource.core.posmode.PosModeDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.reconciliation.ReconciliationDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.device.DeviceControllerImpl
import global.citytech.finpos.merchant.presentation.alarms.SettlementAlarmSetter
import global.citytech.finpos.merchant.presentation.model.*
import global.citytech.finpos.merchant.presentation.model.setting.TerminalSettingRequest
import global.citytech.finpos.merchant.presentation.model.setting.TerminalSettingResponse
import global.citytech.finpos.merchant.utils.ApiConstant
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finpos.merchant.utils.AppUtility.savePushNotificationConfiguration
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.exceptions.PosException
import global.citytech.finposframework.switches.posmode.PosMode
import global.citytech.finposframework.utility.StringUtils
import io.reactivex.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

/**
 * Created by Rishav Chudal on 8/13/20.
 */
class AdminViewModel(val context: Application) : BaseAndroidViewModel(context) {

    private val TAG = "AdminViewModel"
    val serviceLoaded by lazy { MutableLiveData<Boolean>() }
    val configResponse by lazy { MutableLiveData<ConfigResponse>() }
    val deviceConfigurationStatus by lazy { MutableLiveData<DeviceConfigurationStatus>() }
    val navigateToOperatorLoginMessage by lazy { MutableLiveData<String>() }
    val terminalConfigured by lazy { MutableLiveData<Boolean>() }
    val batchSettingPossibleLiveData by lazy { MutableLiveData<Boolean>() }
    private val configurationLiveData by lazy { MutableLiveData<ConfigurationItem>() }
    val existingBatchNumberLiveData by lazy { MutableLiveData<String>() }
    val successfulBatchUpdate by lazy { MutableLiveData<Boolean>() }
    val adminCredential by lazy { MutableLiveData<String>() }
    val resetPasswordSuccess by lazy { MutableLiveData<Boolean>() }
    val syncTerminalSettingsSuccessLiveData by lazy { MutableLiveData<Boolean>() }

    private val deviceController = DeviceControllerImpl(context)

    var localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    private val qrPaymentUseCase = QrPaymentUseCase(
        QrPaymentRepositoryImpl(
            QrPaymentDataSourceImpl(context)
        )
    )

    private val mobileNfcPaymentUseCase = MobilePaymentUseCase(
        MobileNfcPaymentRepositoryImpl(
            MobileNfcPaymentDataSourceImpl(
                context
            )
        )
    )

    var posModelUseCase = CorePosModeUseCase(PosModeRepositoryImpl(PosModeDataSourceImpl()))

    val reconciliationUseCase = CoreReconciliationUseCase(
        ReconciliationRepositoryImpl(
            ReconciliationDataSourceImpl()
        )
    )

    val appNotificationUseCase = AppNotificationUseCase(
        AppNotificationRepositoryImpl(
            AppNotificationDataSourceImpl(context)
        )
    )

    private val CONFIG_URL = "/devices-terminal/v1/configs/switch"
    private lateinit var configRequest: ConfigRequest

    companion object {
        val AUTHORITY = "global.citytech.finpos.merchant.provider.AdminProvider"
    }

    fun bindService() {
        compositeDisposable.add(
            Single.create(SingleOnSubscribe<Response> { _ ->
                val bindIntent = Intent()
                val packageName = "global.citytech.tms"
                val className = "global.citytech.tms.service.PlatformManagerService"
                bindIntent.setClassName(packageName, className)
                context.bindService(
                    bindIntent,
                    NiblMerchant.INSTANCE.getPlatformServiceConnection(),
                    Context.BIND_AUTO_CREATE
                )
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                }, { error ->
                    onError(error)
                })
        )
    }

    fun register() {
        deviceConfigurationStatus.value = DeviceConfigurationStatus.DownloadingConfiguration
        if (!context.hasInternetConnection()) {
            message.value = "No Internet Connection"
            deviceConfigurationStatus.value = DeviceConfigurationStatus.ConfigurationCanceled
            return
        }
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<ConfigResponse> {
                onSubscribe(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onNext, this::onError)
        )
    }

    private fun onSubscribe(emitter: ObservableEmitter<ConfigResponse>) {
        configRequest =
            ConfigRequest(
                AppInfo(
                    "NiblAdmin",
                    1
                ),
                DeviceInfo(
                    NiblMerchant.INSTANCE.iPlatformManager.serialNumber
                )
            )
        val jsonRequest = Jsons.toJsonObj(configRequest)
        val jsonResponse =
            NiblMerchant.INSTANCE.iPlatformManager.post(CONFIG_URL, jsonRequest, false)
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            prepareResponse(emitter, response.data as Any)
        } else {
            if (response.code == "401")
                emitter.onError(PosException(PosError.DEVICE_ERROR_OPERATOR_LOGIN_REQUIRED))
            else
                emitter.onError(Exception("Error Code :: ${response.code}. ${response.message}"))
        }
        emitter.onComplete()
    }

    private fun prepareResponse(emitter: ObservableEmitter<ConfigResponse>, data: Any) {
        val responseJson = Jsons.toJsonObj(data)
        val response = Jsons.fromJsonToObj(responseJson, ConfigResponse::class.java)
        emitter.onNext(response)
    }

    private fun onNext(configResponse: ConfigResponse) {
        deviceConfigurationStatus.value = DeviceConfigurationStatus.SavingParameters
        this.assignPosMode(configResponse)
        saveInMerchantDatabase(configResponse)
        configResponse.terminalSettings?.let {
            onTerminalSettingResponseReceived(it)
        }
    }

    private fun setSettlementTimeAlarm(reconcileTime: String?) {
        reconcileTime?.let {
            val settlementAlarmSetter = SettlementAlarmSetter(context.applicationContext)
            settlementAlarmSetter.setRepeatingAlarm(it)
            this.localDataUseCase.storeSettlementTime(it)
        }
    }

    private fun assignPosMode(configResponse: ConfigResponse) {
        val posMode =
            posModelUseCase.getPosMode(configResponse.terminalParameter!!.emvParameters!!.merchantCategoryCode!!.value as String)
        localDataUseCase.savePosMode(posMode)
    }

    private fun saveInMerchantDatabase(configResponse: ConfigResponse) {
        deviceConfigurationStatus.value = DeviceConfigurationStatus.ConfiguringTerminal
        compositeDisposable.add(
            localDataUseCase.saveConfigResponseAndInjectKey(configResponse)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onInjected, this::onError)
        )
    }

    private fun onInjected(deviceResponse: DeviceResponse) {
        isLoading.value = false
        deviceConfigurationStatus.value = DeviceConfigurationStatus.ParametersSaved
        if (deviceResponse.result == DeviceResult.SUCCESS) {
            deviceConfigurationStatus.value = DeviceConfigurationStatus.TerminalConfigured
        } else {
            deviceConfigurationStatus.value = DeviceConfigurationStatus.ConfigurationFailed
        }
    }

    private fun onError(throwable: Throwable) {
        throwable.printStackTrace()
        isLoading.value = false
        deviceConfigurationStatus.value = DeviceConfigurationStatus.ConfigurationCanceled
        if (throwable is PosException) {
            printLog(TAG, throwable.posError.errorMessage)
            val posError = throwable.posError
            if (posError == PosError.DEVICE_ERROR_OPERATOR_LOGIN_REQUIRED)
                navigateToOperatorLoginMessage.value = posError.errorMessage
            else
                message.value = posError.errorMessage
        } else {
            throwable.message?.let { printLog(TAG, it) }
            message.value = throwable.message
        }
    }

    fun saveDebugModePreference(isDebugModeOn: Boolean) {
        localDataUseCase.saveDebugModeStatus(isDebugModeOn)
    }

    fun getDebugModeStatus(): Boolean {
        return localDataUseCase.isDebugMode(false)
    }


    fun getPosMode(): PosMode {
        return localDataUseCase.getPosMode(PosMode.RETAILER)
    }

    fun storeTipLimit(value: Int) {
        localDataUseCase.saveTipLimit(value)
    }

    fun getAdminCredentialFromTms() {
        isLoading.value = true
        compositeDisposable.add(
            localDataUseCase.getAdminPassword()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        matchEnteredPasswordWithRetrievedFromTMSDataAndProceed(it)
                    },
                    this::onErrorRetrievingAdminPasswordFromTMSData
                )
        )
    }

    private fun matchEnteredPasswordWithRetrievedFromTMSDataAndProceed(
        retrievedPassword: String
    ) {
        printLog(TAG, "Retrieved Admin Password ::: ")
        printLog(TAG, "Retrieved Admin Password ::: ".plus(retrievedPassword), true)
        this.isLoading.value = false
        adminCredential.value = retrievedPassword
    }

    private fun onErrorRetrievingAdminPasswordFromTMSData(throwable: Throwable) {
        printLog(TAG, "Exception in retrieving Admin Password ::: ".plus(throwable.message))
        this.isLoading.value = false
        message.value = "Exception in retrieving Admin Password"
    }

    fun checkDeviceConfigured() {
        terminalConfigured.value = localDataUseCase.isTerminalConfigured(false)
    }

    fun getConfigurationItem(task: Task) {
        isLoading.value = true
        compositeDisposable.add(
            localDataUseCase.getWholeConfiguration()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe(
                    {
                        onConfigurationItemFetched(configurationItem = it, task = task)
                    },
                    {
                        onErrorFetchConfigurationItem(it, task)
                    }
                )
        )
    }

    fun isAppVariantNIBL(): Boolean {
        return BuildConfig.FLAVOR == "NIBL"
    }

    private fun onConfigurationItemFetched(configurationItem: ConfigurationItem, task: Task) {
        configurationLiveData.value = configurationItem
        if (task == Task.BATCH_NUMBER_CONFIGURE)
            checkForTransactionsPendingSettlement()
        else if (task == Task.CLEAR_BATCH)
            clearBatch(configurationItem)
    }

    private fun onErrorFetchConfigurationItem(
        throwable: Throwable,
        task: Task
    ) {
        throwable.printStackTrace()
        printLog(TAG, "Exception in fetching configuration item ::: ".plus(throwable.message))
        if (task == Task.BATCH_NUMBER_CONFIGURE)
            batchSettingPossibleLiveData.value = false
        else if (task == Task.CLEAR_BATCH)
            message.value = context.getString(R.string.error_msg_clear_batch)
    }

    private fun checkForTransactionsPendingSettlement() {
        isLoading.value = true
        compositeDisposable.add(
            localDataUseCase.getApprovedTransactions()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        checkTransactionLogsAndProceed(transactionLogs = it)
                    },
                    {
                        onErrorFetchTransactionLogs(throwable = it)
                    }
                )
        )
    }

    private fun checkTransactionLogsAndProceed(transactionLogs: List<TransactionLog>) {
        isLoading.value = false
        if (transactionLogs.isEmpty()) {
            getExistingSettlementBatchNumber()
        } else {
            batchSettingPossibleLiveData.value = false
        }
    }

    private fun onErrorFetchTransactionLogs(throwable: Throwable) {
        printLog(TAG, "Exception in fetching transaction logs ::: ".plus(throwable.message))
        batchSettingPossibleLiveData.value = false
    }

    private fun getExistingSettlementBatchNumber() {
        compositeDisposable.add(
            localDataUseCase.getExistingSettlementBatchNumber()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        onSettlementBatchNumberRetrieved(batchNumber = it)
                    },
                    {
                        onErrorRetrieveBatchNumber(throwable = it)
                    }
                )
        )
    }

    private fun onSettlementBatchNumberRetrieved(batchNumber: String) {
        isLoading.value = false
        existingBatchNumberLiveData.value = batchNumber
    }

    private fun onErrorRetrieveBatchNumber(throwable: Throwable) {
        printLog(
            TAG,
            "Exception in fetching existing batch number ::: ".plus(throwable.message)
        )
        isLoading.value = false
        batchSettingPossibleLiveData.value = false
    }

    fun configureWithInputBatchNumber(batchNumber: Int) {
        isLoading.value = true
        Completable
            .fromRunnable { localDataUseCase.updateReconciliationBatchNumber(batchNumber) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { onBatchNumberUpdated() },
                { onErrorUpdatingBatchNumber(it) }
            )
    }

    private fun onBatchNumberUpdated() {
        isLoading.value = false
        printLog(TAG, "Batch Number updated...")
        successfulBatchUpdate.value = true
    }

    private fun onErrorUpdatingBatchNumber(throwable: Throwable) {
        isLoading.value = false
        printLog(TAG, "Error in updating batch number ::: ".plus(throwable))
        message.value = "Could not configure batch number. Please try again"
    }

    fun retrieveConfiguredTipInPercentage() = localDataUseCase.retrieveTipLimit()


    fun enableHomeButton() {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<global.citytech.finposframework.hardware.common.DeviceResponse> {
                enableHomeButton(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    printLog(TAG, "Hardware Button Enabled", true)
                }, {
                    printLog(TAG, "Error in Hardware Button Enabled" + it.message)
                    it.printStackTrace()
                })
        )
    }

    private fun enableHomeButton(it: ObservableEmitter<global.citytech.finposframework.hardware.common.DeviceResponse>) {
        it.onNext(deviceController.enableHomeButton())
        it.onComplete()
    }

    fun getCardConfirmationRequiredStatus() =
        localDataUseCase.getCardConfirmationRequiredPref(false)

    fun saveCardConfirmationRequiredPreference(checked: Boolean) {
        localDataUseCase.saveCardConfirmationRequiredPref(checked)
    }

    fun getAutoSettlementTriggerStatus() =
        localDataUseCase.getTerminalSetting().enableAutoSettlement

    fun getVatRefundSupportedStatus() =
        localDataUseCase.getVatRefundSupportedPref(false)

    fun saveVatRefundSupportedPreference(checked: Boolean) {
        localDataUseCase.saveVatRefundSupportedPref(checked)
    }

    fun getShouldShufflePinPadStatus() =
        localDataUseCase.getShouldShufflePinPadPref(false)

    fun saveShouldShufflePinPadPreference(checked: Boolean) {
        localDataUseCase.saveShouldShufflePinPadPref(checked)
    }

    fun resetMerchantPassword() {
        localDataUseCase.saveMerchantCredential(BuildConfig.MERCHANT_DEFAULT_SANCHO)
        resetPasswordSuccess.value = true
    }

    private fun clearBatch(configurationItem: ConfigurationItem) {
        isLoading.value = true
        compositeDisposable.add(
            reconciliationUseCase.clearBatch(configurationItem)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    if (it.isSuccess) {
                        printLog(TAG, it.message)
                        localDataUseCase.saveSettlementActiveStatus(false)
                        appNotificationUseCase.removeNotificationByAction(Action.SETTLEMENT)
                        message.value = it.message
                        cancelRescheduledSettlement()
                        updateLastSettlementTimeStamp()
                    } else {
                        message.value = context.getString(R.string.error_msg_clear_batch)
                    }
                }, {
                    printLog(TAG, it.message.toString())
                    it.printStackTrace()
                    isLoading.value = false
                    message.value = context.getString(R.string.error_msg_clear_batch)
                })
        )
    }

    private fun updateLastSettlementTimeStamp() {
        val currentTimeInMillis = Calendar.getInstance().timeInMillis;
        localDataUseCase.updateLastSuccessSettlementTimeInMillis(currentTimeInMillis)
    }

    private fun cancelRescheduledSettlement() {
        NiblMerchant.INSTANCE.cancelRescheduledSettlement()
    }

    fun performSyncTerminalSettings() {
        isLoading.value = true
        compositeDisposable.add(
            postSyncSettings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        onTerminalSettingResponseReceived(it)
                    },
                    {
                        onErrorTerminalSettingResponse(it)
                    }
                )
        )
    }

    private fun postSyncSettings(): Observable<TerminalSettingResponse> {
        return Observable.fromCallable {
            val jsonRequest =
                Jsons.toJsonObj(TerminalSettingRequest(NiblMerchant.INSTANCE.iPlatformManager.serialNumber))
            val jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.post(
                ApiConstant.SYNC_SETTING,
                jsonRequest,
                false
            )
            val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
            val responseJson = Jsons.toJsonObj(response.data)
            Jsons.fromJsonToObj(responseJson, TerminalSettingResponse::class.java)
        }
    }

    private fun onTerminalSettingResponseReceived(it: TerminalSettingResponse) {
        isLoading.value = false
        localDataUseCase.saveTerminalSetting(it)
        it.qrBillNumber?.let { qrBillNumber ->
            qrPaymentUseCase.saveBillingInvoiceNumber(
                qrBillNumber
            )
        }
        it.nfcBillNumber?.let { mobileNfcPaymentUseCase.saveNfcPaymentIdentifiers(it) }
        if (it.enableAutoSettlement && !it.settlementTime.isNullOrEmpty()) {
            setSettlementTimeAlarm(it.settlementTime)
        } else {
            PreferenceManager.storeSettlementTime("")
            cancelRescheduledSettlement()
        }

        addQrDisclaimerNotification(it)
        handleDisclaimerList(it.disclaimers)
        localDataUseCase.saveDebugModeStatus(it.enableDebugLog)
        it.pushNotificationConfiguration?.let { it1
            ->
            savePushNotificationConfiguration(it1)
        }
        localDataUseCase.saveGreenPinStatus(it.enableGreenPin)
        localDataUseCase.saveSoundModeStatus(it.isSoundEnabled)
        syncTerminalSettingsSuccessLiveData.value = true
    }


    private fun handleDisclaimerList(disclaimerList: List<Disclaimer>?) {
        if (disclaimerList != null && disclaimerList.isNotEmpty()) {
            isLoading.value = true
            compositeDisposable.add(
                localDataUseCase.addListToDisclaimers(disclaimerList)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        isLoading.value = false
                        disclaimerList.forEach {
                            appNotificationUseCase.addNotification(
                                AppNotification(
                                    title = it.notificationTitle,
                                    body = it.notificationContent,
                                    type = Type.WARNING,
                                    action = Action.DISCLAIMER,
                                    read = false,
                                    timeStamp = StringUtils.dateTimeStamp(),
                                    referenceId = it.disclaimerId
                                )
                            )
                        }
                    }, {
                        printLog(TAG, it.message.toString())
                        isLoading.value = false
                    })
            )
        }
    }

    private fun addQrDisclaimerNotification(terminalSettings: TerminalSettingResponse) {
        if (terminalSettings.showQrDisclaimer) {
            appNotificationUseCase.addNotification(
                AppNotification(
                    title = "",
                    body = context.getString(R.string.msg_qr_disclaimer_notification),
                    timeStamp = StringUtils.dateTimeStamp(),
                    action = Action.QR_DISCLAIMER,
                    read = false,
                    type = Type.WARNING
                )
            )
        } else {
            appNotificationUseCase.removeNotificationByAction(Action.QR_DISCLAIMER)
        }
    }

    private fun onErrorTerminalSettingResponse(it: Throwable) {
        printLog(TAG, "Terminal Setting Response Error ::: ".plus(it.message))
        isLoading.value = false
        syncTerminalSettingsSuccessLiveData.value = false
    }

    fun retrieveConfiguredAmountLengthLimit(): Int = localDataUseCase.getAmountLengthLimit()

    fun onAmountLimitConfirmed(value: Int) = localDataUseCase.saveAmountLengthLimit(value)

    enum class Task {
        BATCH_NUMBER_CONFIGURE,
        CLEAR_BATCH
    }
}