package global.citytech.finpos.merchant.domain.usecase.qr.nqr

import global.citytech.finpos.merchant.domain.usecase.qr.LengthType
import global.citytech.finpos.merchant.domain.usecase.qr.QrDataObject
import global.citytech.finpos.merchant.domain.usecase.qr.QrField
/**
 * @author sachin
 */
data class NqrField(
    val qrField: QrField,
    val value: Any
) : QrDataObject {
    override fun getFieldId(): String {
        return qrField.id
    }

    override fun getFieldContentLength(): String {
        return getFieldContent().length.toString().padStart(2, '0')
    }

    override fun getFieldContent(): String {
        if (value is QrDataObject) {
            return value.getFieldContent().toString()
        }
        if (qrField.lengthConfig.lengthType == LengthType.FIXED) {
            return value.toString().padStart(qrField.lengthConfig.length, '0')
        }
        return value.toString()
    }
}