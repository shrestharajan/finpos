package global.citytech.finposframework.switches.receipt.duplicatereceipt;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 12/15/2020.
 */
public interface DuplicateReceiptRequester<T extends DuplicateReceiptRequestParameter,
        E extends DuplicateReceiptResponseParameter>
        extends Request<T, E> {
}
