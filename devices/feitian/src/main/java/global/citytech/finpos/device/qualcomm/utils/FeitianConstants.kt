package global.citytech.finpos.device.qualcomm.utils

import com.ftpos.library.smartpos.errcode.ErrCode

/**
 * Created by Rishav Chudal on 4/5/21.
 */
object FeitianConstants {
    const val LOG_TAG = "FinPos-Log-Feitian"
    const val SWITCH_DEBUG_FLAG = 3
    const val ERROR_CODE_AID_BLOCKED = 0x88

    /**
     * Valid Key Index Range
     */
    const val KEY_INDEX_RANGE_MIN = 0x00
    const val KEY_INDEX_RANGE_MAX = 0xFF

    /**
     * Card Detected Constants
     * updated: 2021-04-16
     */
    const val ICC_CARD_DETECTED = 0x01
    const val PICC_CARD_DETECTED = 0x02
    const val MAG_CARD_DETECTED = 0x08

    /**
     * Constants For Card Detection Mode
     */
    const val CARD_DETECTION_MODE_ICC = 0x01
    const val CARD_DETECTION_MODE_PICC = 0x02
    const val CARD_DETECTION_MODE_MAG = 0x08

    /**
     * PinPad Error Codes
     */
    const val ERROR_CODE_PIN_ENTRY_LAST = 0x63C1
    const val ERROR_CODE_OFFLINE_PIN_ENTRY_LIMIT_EXCEED = 0x63C0
    const val ERROR_CODE_OFFLINE_PIN_ENTRY_ERROR_6983 = 0x6983
    const val ERROR_CODE_PIN_BYPASS = ErrCode.ERR_PIN_BYPASS

    /**
     * PinPad Result TLV value
     */
    const val PIN_PAD_TLV_VALUE_ENTRY_EXCEED_LIMIT: Byte =
        4 //Value taken from Feitian Cashier Demo 2021-03-15

    const val PIN_PAD_TLV_VALUE_ERROR_6983: Byte =
        5 //Value taken from Feitian Cashier Demo 2021-03-15

    const val PIN_PAD_TLV_VALUE_PIN_BYPASS: Byte =
        1 //Value taken from Feitian Cashier Demo 2021-03-15

    const val PIN_PAD_TLV_VALUE_PIN_NORMAL: Byte =
        2 //Value taken from Feitian Cashier Demo 2021-03-15


    /**
     * Online Result Constants
     */
    const val ONLINE_RESULT_SUCCESS = 0 //Value taken from Web SDK document

    const val ONLINE_RESULT_FAILURE = 1 //Value taken from Web SDK document


    /**
     * MAC Flag
     */
    const val MAC_CALCULATION_FLAG_RESPONSE_MODE = 0
    const val MAC_CALCULATION_FLAG_REQUEST_MODE = 1

    /**
     * Flag Check and UnCheck
     */
    const val DO_NOT_CHECK_FLAG = "00"
    const val CHECK_FLAG = "01"

    /**
     * System API Invoke
     */
    const val SYSTEM_API_INVOKE_SUCCESS = 1

    /**
     * Card Exist in ICC reader
     */
    const val CARD_NOT_PRESENT_IN_ICC_READER = 0
    const val CARD_PRESENT_IN_ICC_READER_POWER_ON = 1
    const val CARD_PRESENT_IN_ICC_READER_POWER_OFF = 2

    /**
     * LED Actions
     */
    const val LED_ACTION_ON = true
    const val LED_ACTION_OFF = false
}