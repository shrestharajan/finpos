package global.citytech.finpos.device.weipass.keys

import com.wiseasy.emvprocess.Acquirer
import com.wiseasy.emvprocess.SDKInstance
import com.wiseasy.emvprocess.bean.AcquirerKey
import global.citytech.finpos.device.weipass.utils.WeipassDeviceUtils
import global.citytech.finpos.device.weipass.utils.WeipassConstant
import global.citytech.finpos.device.weipass.utils.WeipassConstant.FAILURE_INVALID_KEY
import global.citytech.finpos.device.weipass.utils.WeipassConstant.FAILURE_INVALID_PACKAGE_NAME
import global.citytech.finpos.device.weipass.utils.WeipassConstant.FAILURE_KEY_INJECT
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SUCCESS_KEY_INJECT
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.keymgmt.keys.KeyService
import global.citytech.finposframework.hardware.keymgmt.keys.KeyType
import global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyCheckRequest
import global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyInjectionRequest
import global.citytech.finposframework.hardware.utility.DeviceApiConstants

/**
 * Created by Rishav Chudal on 4/9/20.
 */
class WeipassKeyServiceImpl:
    KeyService {

    private lateinit var acquirerKey: AcquirerKey
    private lateinit var deviceResponse: DeviceResponse

    /*
     * Key Injection
     */
    override fun injectKey(keyInjectionRequest: KeyInjectionRequest): DeviceResponse {
        deviceResponse = DeviceResponse(Result.FAILURE, FAILURE_KEY_INJECT)
        when {
            keyInjectionRequest.packageName.isNullOrEmptyOrBlank() -> {
                deviceResponse = DeviceResponse(Result.FAILURE, FAILURE_INVALID_PACKAGE_NAME)
            }
            keyInjectionRequest.key.isNullOrEmptyOrBlank() -> {
                deviceResponse = DeviceResponse(Result.FAILURE, FAILURE_INVALID_KEY)
            }
            else -> {
                proceedForKeyInjection(keyInjectionRequest)
            }
        }
        return deviceResponse;
    }

    private fun proceedForKeyInjection(keyInjectionRequest: KeyInjectionRequest){
        deviceResponse = when (keyInjectionRequest.keyType) {
            KeyType.KEY_TYPE_TLK -> injectTLKInDevice(keyInjectionRequest)
            KeyType.KEY_TYPE_TMK -> injectTMKInDevice(keyInjectionRequest)
            KeyType.KEY_TYPE_DEK -> injectDEKInDevice(keyInjectionRequest)
            KeyType.KEY_TYPE_PEK -> injectPEKInDevice(keyInjectionRequest)
            KeyType.KEY_TYPE_IPEK -> injectIPEKInDevice(keyInjectionRequest)
            KeyType.KEY_TYPE_MAK -> injectMAKInDevice(keyInjectionRequest)
            KeyType.KEY_TYPE_KBPK -> injectKBPKInDevice(keyInjectionRequest)
            KeyType.KEY_TYPE_DDEK -> injectDDEKInDevice(keyInjectionRequest)
            KeyType.KEY_TYPE_DMAK -> injectDMAKInDevice(keyInjectionRequest)
            else -> throw Exception(DeviceApiConstants.MSG_KEY_NOT_SUPPORT)
        }
    }

    private fun injectTLKInDevice(keyInjectionRequest: KeyInjectionRequest): DeviceResponse {
        WeipassLogger.log("Injecting TLK in Device")
        prepareCommonKeyParameters(keyInjectionRequest)
        return injectKeyInCore()
    }

    private fun injectTMKInDevice(keyInjectionRequest: KeyInjectionRequest): DeviceResponse {
        WeipassLogger.log("Injecting TMK in Device")
        prepareCommonKeyParameters(keyInjectionRequest)
        return injectKeyInCore()
    }

    private fun injectDEKInDevice(keyInjectionRequest: KeyInjectionRequest): DeviceResponse {
        WeipassLogger.log("Injecting DEK in Device")
        prepareCommonKeyParameters(keyInjectionRequest)
        return injectKeyInCore()
    }

    private fun injectPEKInDevice(keyInjectionRequest: KeyInjectionRequest): DeviceResponse {
        WeipassLogger.log("Injecting PEK in Device")
        prepareCommonKeyParameters(keyInjectionRequest)
        return injectKeyInCore()
    }

    private fun injectIPEKInDevice(keyInjectionRequest: KeyInjectionRequest): DeviceResponse {
        WeipassLogger.log("Injecting IPEK in Device")
        if (keyInjectionRequest.ksn.isNullOrEmptyOrBlank()) {
            WeipassLogger.log("KSN is blank, null or empty")
            return DeviceResponse(Result.FAILURE, FAILURE_KEY_INJECT)
        }
        prepareCommonKeyParameters(keyInjectionRequest)
        acquirerKey.azKSN = keyInjectionRequest.ksn
        return injectKeyInCore()
    }

    private fun injectMAKInDevice(keyInjectionRequest: KeyInjectionRequest): DeviceResponse {
        WeipassLogger.log("Injecting MAK in Device")
        prepareCommonKeyParameters(keyInjectionRequest)
        return injectKeyInCore()
    }

    private fun injectKBPKInDevice(keyInjectionRequest: KeyInjectionRequest): DeviceResponse {
        WeipassLogger.log("Injecting KBPK in Device")
        prepareCommonKeyParameters(keyInjectionRequest)
        return injectKeyInCore()
    }

    private fun injectDDEKInDevice(keyInjectionRequest: KeyInjectionRequest): DeviceResponse {
        WeipassLogger.log("Injecting DDEK in Device")
        prepareCommonKeyParameters(keyInjectionRequest)
        return injectKeyInCore()
    }

    private fun injectDMAKInDevice(keyInjectionRequest: KeyInjectionRequest): DeviceResponse {
        WeipassLogger.log("Injecting DMAK in Device")
        prepareCommonKeyParameters(keyInjectionRequest)
        return injectKeyInCore()
    }

    private fun prepareCommonKeyParameters(keyInjectionRequest: KeyInjectionRequest) {
        WeipassLogger.log("Preparing common key parameters")
        acquirerKey = AcquirerKey()
        acquirerKey.packageName = keyInjectionRequest.packageName
        WeipassLogger.debug("Package Name ::: ".plus( keyInjectionRequest.packageName))

        acquirerKey.keyType = WeipassDeviceUtils.getKeyTypeForCore(keyInjectionRequest.keyType).toInt()
        WeipassLogger.debug("Key Type ::: ".plus(keyInjectionRequest.keyType))

        acquirerKey.algorithmType = WeipassDeviceUtils.getAlgorithmTypeForCore(keyInjectionRequest.algorithm)
        WeipassLogger.debug("Algorithm Type ::: ".plus(keyInjectionRequest.algorithm))

        acquirerKey.azKeyValue = keyInjectionRequest.key
        WeipassLogger.debug("Key ::: ".plus(keyInjectionRequest.key))

        acquirerKey.azKCVValue = keyInjectionRequest.keyKCV
        WeipassLogger.debug("Key KCV ::: ".plus(keyInjectionRequest.keyKCV))

        acquirerKey.setIsPlainText(keyInjectionRequest.isPlainText)
        WeipassLogger.debug("Is Plain Text ::: ".plus(keyInjectionRequest.isPlainText))
    }

    private fun injectKeyInCore(): DeviceResponse {
        WeipassLogger.log("Injecting KEY in Core")
        deviceResponse = DeviceResponse(Result.FAILURE, FAILURE_KEY_INJECT)
        val result = Acquirer.getInstance().addAcquireKey(acquirerKey)
        if (result == 0) {
            deviceResponse = DeviceResponse(Result.SUCCESS, SUCCESS_KEY_INJECT)
        }
        WeipassLogger.log("Key Injection Result ::: ".plus(result))
        return deviceResponse
    }

    /*
     * Erase Keys
     */
    override fun eraseAllKeys(): DeviceResponse {
        WeipassLogger.log("Clearing all keys in core")
        deviceResponse = DeviceResponse(Result.FAILURE, WeipassConstant.FAILURE_KEY_ERASE)
        if (clearAllKeysInCore() == 0) {
            WeipassLogger.log("All Keys Cleared")
            deviceResponse = DeviceResponse(Result.SUCCESS, WeipassConstant.SUCCESS_KEY_ERASE)
        } else {
            WeipassLogger.log("Could not clear keys")
        }
        return deviceResponse
    }

    private fun clearAllKeysInCore(): Int {
        var result = -1
        try {
            result = SDKInstance.mKey.erasePED()
        } catch (exception: Exception) {
            WeipassLogger.log("Exception in clearing keys ::: ".plus(exception.message))
        }
        WeipassLogger.log("Clear Keys result ::: ".plus(result))
        return result
    }

    /*
     * Key Exist Check
     */
    override fun checkKeyExist(keyCheckRequest: KeyCheckRequest): DeviceResponse {
        WeipassLogger.log("Checking for key in core")
        deviceResponse = DeviceResponse(Result.FAILURE, WeipassConstant.FAILURE_KEY_CHECK)
        if (keyCheckRequest.packageName.isNullOrEmptyOrBlank()) {
            deviceResponse = DeviceResponse(Result.FAILURE, FAILURE_INVALID_PACKAGE_NAME)
        } else {
            proceedForKeyCheck(keyCheckRequest)
        }
        return deviceResponse
    }

    private fun proceedForKeyCheck(keyCheckRequest: KeyCheckRequest) {
        if (checkForKeyInCore(keyCheckRequest) == 0) {
            WeipassLogger.log("Key already exist")
            deviceResponse = DeviceResponse(Result.SUCCESS, WeipassConstant.SUCCESS_KEY_CHECK)
        } else {
            WeipassLogger.log("Key do not exist")
        }
    }

    private fun checkForKeyInCore(keyCheckRequest: KeyCheckRequest): Int {
        var result = -1
        try {
            result = SDKInstance.mKey.checkKeyExist(keyCheckRequest.packageName,
                WeipassDeviceUtils.getKeyTypeForCore(keyCheckRequest.keyType).toInt())
        } catch (exception: Exception) {
            WeipassLogger.log("Exception in Key Check ::: ".plus(exception.message))
        }
        WeipassLogger.log("KEY Check Result ::: ".plus(result))
        return result
    }
}