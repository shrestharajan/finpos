package global.citytech.finposframework.switches.cashadvance;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 2/5/2021.
 */
public interface ManualCashAdvanceRequester<T extends CashAdvanceRequestParameter,
        E extends CashAdvanceResponseParameter> extends Request<T, E> {
}
