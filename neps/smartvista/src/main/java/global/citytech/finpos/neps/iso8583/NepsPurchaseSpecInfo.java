package global.citytech.finpos.neps.iso8583;

import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.DataElementConfigs;
import global.citytech.finposframework.iso8583.SpecInfo;

public class NepsPurchaseSpecInfo implements SpecInfo {

  ReadCardResponse readCardResponse;

  public NepsPurchaseSpecInfo(ReadCardResponse readCardResponse) {
    this.readCardResponse = readCardResponse;
  }

  @Override
  public DataElementConfigs getDEConfig() {
    DataElementConfigs collection = new NepsDataElementConfig();

    return collection;
  }

  /*
      @Override
      public TPDU getTPDU() {
          byte[] destination = {0x03, 0x03};
          byte[] originator = {0x00, 0x00};
          return new TPDU((byte) 0x60, destination, originator);
      }

      @Override
      public boolean containsTPDU() {
          return true;
      }
  */
  @Override
  public int getMessageHeaderLength() {
    return 2;
  }

  @Override
  public int getMTILength() {
    return 4;
  }
}
