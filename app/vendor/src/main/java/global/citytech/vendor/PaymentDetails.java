package global.citytech.vendor;

/**
 * Created by Rishav Chudal on 6/9/21.
 */
class PaymentDetails {
  private final String amount;
  private final String cashbackAmount;
  private final String paymentOriginator;
  private final String applicationIdentifier;
  private final String retrievalReferenceNumber;

  private PaymentDetails(Builder builder) {
    amount = builder.amount;
    cashbackAmount = builder.cashbackAmount;
    paymentOriginator = builder.paymentOriginator;
    applicationIdentifier = builder.applicationIdentifier;
    retrievalReferenceNumber = builder.retrievalReferenceNumber;
  }

  public String getAmount() {
    return amount;
  }

  public String getCashbackAmount() {
    return cashbackAmount;
  }

  public String getPaymentOriginator() {
    return paymentOriginator;
  }

  public String getApplicationIdentifier() {
    return applicationIdentifier;
  }

  public String getRetrievalReferenceNumber() {
    return retrievalReferenceNumber;
  }

  public static class Builder {
    private final String amount;
    private String cashbackAmount;
    private final String paymentOriginator;
    private final String applicationIdentifier;
    private String retrievalReferenceNumber;

    private Builder(String amount, String paymentOriginator, String applicationIdentifier) {
      this.amount = amount;
      this.paymentOriginator = paymentOriginator;
      this.applicationIdentifier = applicationIdentifier;
    }

    public static Builder getInstance(
        String amount,
        String paymentOriginator,
        String applicationIdentifier
    ) {
      return new Builder(amount, paymentOriginator, applicationIdentifier);
    }

    public Builder withCashbackAmount(String cashbackAmount) {
      this.cashbackAmount = cashbackAmount;
      return this;
    }

    public Builder withRetrievalReferenceNumber(String retrievalReferenceNumber) {
      this.retrievalReferenceNumber = retrievalReferenceNumber;
      return this;
    }

    public PaymentDetails build() {
      return new PaymentDetails(this);
    }
  }
}
