package global.citytech.finpos.merchant.presentation.model.voidsale

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

/**
 * Created by Rishav Chudal on 9/22/20.
 */
class VoidSaleRequestItem(
    val transactionType: TransactionType? = null,
    val amount: BigDecimal? = null,
    val originalInvoiceNumber: String? = null,
    val originalRRN: String? = null
)