package global.citytech.finpos.processor.nibl.batchupload;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finpos.nibl.iso8583.requestsender.batchupload.S2MBatchUploadRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.batchupload.S2MBatchUploadRequestSender;
import global.citytech.finpos.processor.nibl.transaction.TransactionType;
import global.citytech.finpos.processor.nibl.transaction.receipt.NiblIsoMessageReceiptHandler;
import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.AppState;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.batchupload.BatchUploadRequester;
import global.citytech.finposframework.usecases.activity.ActivityLog;
import global.citytech.finposframework.usecases.activity.ActivityLogStatus;
import global.citytech.finposframework.usecases.activity.ActivityLogType;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.utility.AmountUtils;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StackTraceUtil;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Unique Shakya on 5/11/2021.
 */
public class BatchUploadUseCase implements UseCase<BatchUploadRequestModel, BatchUploadResponseModel>,
        BatchUploadRequester<BatchUploadRequestModel, BatchUploadResponseModel> {

    private Logger logger = Logger.getLogger(BatchUploadUseCase.class.getName());
    private TerminalRepository terminalRepository;
    private Notifier notifier;

    public BatchUploadUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        this.terminalRepository = terminalRepository;
        this.notifier = notifier;
    }

    @Override
    public BatchUploadResponseModel execute(BatchUploadRequestModel request) {
        this.notifier.notify(Notifier.EventType.BATCH_UPLOADING, "Please wait...");
        String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
        List<TransactionLog> transactionLogs = request.getTransactionRepository().getTransactionLogsByBatchNumber(batchNumber);
        int totalSize = transactionLogs.size();
        int count = 1;
        for (TransactionLog transactionLog : transactionLogs) {
            if (this.isTransactionNotNeededForBatchUpload(transactionLog)) {
                logger.debug("::: NOT UPLOADING VOIDED SALE OR VOID TRANSACTION :::");
                totalSize--;
                /*if (transactionLog.getTransactionType() != global.citytech.finposframework.usecases.TransactionType.PRE_AUTH ||
                        transactionLog.isAuthorizationCompleted())
                    request.getTransactionRepository().deleteTransactionLogByStan(transactionLog.getStan());*/
            } else {
                this.notifier.notify(Notifier.EventType.BATCH_UPLOAD_PROGRESS,
                        this.retrieveBatchUploadMessage(batchNumber, count, totalSize));
                ConnectionParam connectionParam = new ConnectionParam(this.terminalRepository.findPrimaryHost(),
                        this.terminalRepository.findSecondaryHost());
                String stan = this.terminalRepository.getSystemTraceAuditNumber();
                RequestContext requestContext = new RequestContext(stan, this.terminalRepository.findTerminalInfo(),
                        connectionParam);
                requestContext.setRequest(this.prepareBatchUploadRequest(transactionLog));
                S2MBatchUploadRequestSender requestSender = new S2MBatchUploadRequestSender(new NIBLSpecInfo(),
                        requestContext);
                try {
                    IsoMessageResponse isoMessageResponse = requestSender.send(false);
                    logger.debug("::: BATCH UPLOAD RESPONSE RECEIVED :::");
                    logger.debug("::: DE 39 :::" + IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse,
                            DataElement.RESPONSE_CODE));
                    /*if (transactionLog.getTransactionType() != global.citytech.finposframework.usecases.TransactionType.PRE_AUTH ||
                            transactionLog.isAuthorizationCompleted())
                        request.getTransactionRepository().deleteTransactionLogByStan(transactionLog.getStan());*/
                    this.printBatchUploadReceipt(transactionLog, isoMessageResponse, request.getPrinterService(),
                            count == 1, count == totalSize);
                    this.terminalRepository.incrementSystemTraceAuditNumber();
                } catch (FinPosException fe) {
                    logger.error(fe.getDetailMessage());
                    requestSender.closeTcpClient();
                    updateActivityLog(requestContext, fe);
                    return new BatchUploadResponseModel(Result.FAILURE, "FAILURE");
                }catch (Exception e) {
                    logger.error(e.getLocalizedMessage());
                    requestSender.closeTcpClient();
                    updateActivityLog(requestContext, StackTraceUtil.getStackTraceAsString(e));
                    return new BatchUploadResponseModel(Result.FAILURE, "FAILURE");
                }
                count++;
            }
        }
        return new BatchUploadResponseModel(Result.SUCCESS, "SUCCESS");
    }

    private void updateActivityLog(RequestContext requestContext, FinPosException fe) {
        try {
            S2MBatchUploadRequest s2MBatchUploadRequest = (S2MBatchUploadRequest)requestContext.getRequest();
            TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.terminalRepository.getSystemTraceAuditNumber())
                    .withType(ActivityLogType.BATCH_ADVICE)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(this.terminalRepository.getReconciliationBatchNumber())
                    .withAmount(AmountUtils.toLowerCurrencyFromBigDecimalInLong(s2MBatchUploadRequest.getTransactionAmount()))
                    .withResponseCode("")
                    .withIsoRequest("")
                    .withIsoResponse(fe.getIsoResponse())
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(fe.getLocalizedMessage() + " : " + fe.getDetailMessage())
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(StringUtils.prepareAdditionalDataForActivityLog(this.terminalRepository.getTerminalSerialNumber()))
                    .build();
            this.terminalRepository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update BATCH ADVICE activity log :: " + e.getMessage());
        }
    }
    private void updateActivityLog(RequestContext requestContext, String remarks) {
        try {
            S2MBatchUploadRequest s2MBatchUploadRequest = (S2MBatchUploadRequest)requestContext.getRequest();
            TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.terminalRepository.getSystemTraceAuditNumber())
                    .withType(ActivityLogType.BATCH_ADVICE)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(this.terminalRepository.getReconciliationBatchNumber())
                    .withAmount(AmountUtils.toLowerCurrencyFromBigDecimalInLong(s2MBatchUploadRequest.getTransactionAmount()))
                    .withResponseCode("")
                    .withIsoRequest("")
                    .withIsoResponse("")
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(remarks)
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(StringUtils.prepareAdditionalDataForActivityLog(this.terminalRepository.getTerminalSerialNumber()))
                    .build();
            this.terminalRepository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update BATCH ADVICE activity log :: " + e.getMessage());
        }
    }

    private boolean isTransactionNotNeededForBatchUpload(TransactionLog transactionLog) {
        return this.isVoidedSaleTransaction(transactionLog) || this.isVoidTransaction(transactionLog) || this.isVoidedTipAdjustment(transactionLog)
                || this.isVoidedCashTransaction(transactionLog);
    }

    private boolean isVoidedCashTransaction(TransactionLog transactionLog) {
        return transactionLog.getTransactionType() == global.citytech.finposframework.usecases.TransactionType.CASH_ADVANCE &&
                transactionLog.isTransactionVoided();
    }

    private boolean isVoidedTipAdjustment(TransactionLog transactionLog) {
        return transactionLog.getTransactionType() == global.citytech.finposframework.usecases.TransactionType.TIP_ADJUSTMENT &&
                transactionLog.isTransactionVoided();
    }

    private boolean isVoidTransaction(TransactionLog transactionLog) {
        return transactionLog.getTransactionType() == global.citytech.finposframework.usecases.TransactionType.VOID ||
                transactionLog.getTransactionType() == global.citytech.finposframework.usecases.TransactionType.CASH_VOID;
    }

    private boolean isVoidedSaleTransaction(TransactionLog transactionLog) {
        return transactionLog.getTransactionType() == global.citytech.finposframework.usecases.TransactionType.PURCHASE &&
                transactionLog.isTransactionVoided();
    }

    private void printBatchUploadReceipt(TransactionLog transactionLog, IsoMessageResponse isoMessageResponse,
                                         PrinterService printerService, boolean printBatchUploadHeader,
                                         boolean feedPaperAfterFinish) {
        if (this.terminalRepository.findTerminalInfo().isDebugModeEnabled())
            this.printDebugReceiptIfDebugMode(printerService, isoMessageResponse);
        if (AppState.getInstance().isDebug()) {
            BatchUploadReceipt batchUploadReceipt = BatchUploadReceipt.Builder.newInstance()
                    .amount(transactionLog.getReceiptLog().getAmountWithCurrency())
                    .approvalCode(transactionLog.getAuthCode())
                    .cardNumber(StringUtils.encodeAccountNumber(transactionLog.getReadCardResponse().getCardDetails().getPrimaryAccountNumber()))
                    .cardScheme(transactionLog.getReadCardResponse().getCardDetails().getCardScheme().getCardScheme())
                    .invoiceNumber(transactionLog.getInvoiceNumber())
                    .responseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                    .transactionType(transactionLog.getTransactionType().getPrintName())
                    .transactionDate(transactionLog.getTransactionDate())
                    .transactionTime(transactionLog.getTransactionTime())
                    .build();
            new BatchUploadReceiptPrintHandler(printerService).printBatchUploadReceipt(batchUploadReceipt,
                    printBatchUploadHeader, feedPaperAfterFinish, false);
        }
    }

    protected void printDebugReceiptIfDebugMode(PrinterService printerService, IsoMessageResponse isoMessageResponse) {
        boolean isDebugVersion = this.terminalRepository.findTerminalInfo().isDebugModeEnabled();
        if (isDebugVersion) {
            ReceiptHandler.IsoMessageReceiptHandler isoMessageReceiptHandler
                    = new NiblIsoMessageReceiptHandler(printerService);
            isoMessageReceiptHandler.printIsoMessageReceipt(isoMessageResponse);
        }
    }

    private String retrieveBatchUploadMessage(String batchNumber, int count, int totalSize) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Batch Number: ");
        stringBuilder.append(batchNumber);
        stringBuilder.append("\n");
        stringBuilder.append("Uploading .. ");
        stringBuilder.append("\n");
        stringBuilder.append("[ ");
        stringBuilder.append(count);
        stringBuilder.append(" / ");
        stringBuilder.append(totalSize);
        stringBuilder.append(" ]");
        return stringBuilder.toString();
    }

    private S2MBatchUploadRequest prepareBatchUploadRequest(TransactionLog transactionLog) {
        TransactionType transactionType = TransactionUtils.mapPurchaseTypeWithTransactionType(transactionLog.getTransactionType());
        CardDetails cardDetails = transactionLog.getReadCardResponse().getCardDetails();
        return S2MBatchUploadRequest.Builder.newInstance()
                .pan(cardDetails.getPrimaryAccountNumber())
                .originalProcessingCode(transactionType.getProcessingCode())
                .transactionAmount(transactionLog.getTransactionAmount())
                .localTime(transactionLog.getTransactionTime())
                .localDate(transactionLog.getTransactionDate())
                .expirationDate(cardDetails.getExpiryDate())
                .posEntryMode(transactionLog.getPosEntryMode())
                .functionCode("0011")
                .posConditionCode(transactionLog.getPosConditionCode())
                .originalRrn(transactionLog.getRrn())
                .originalApprovalCode(transactionLog.getAuthCode())
                .originalResponseCode(transactionLog.getResponseCode())
                .originalData(this.prepareOriginalData(transactionLog, transactionType))
                .invoiceNumber(transactionLog.getInvoiceNumber())
                .build();
    }

    private String prepareOriginalData(TransactionLog transactionLog, TransactionType transactionType) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(TransactionUtils.retrieveMtiForTransactionType(transactionType));
        stringBuilder.append(transactionLog.getStan());
        stringBuilder.append(transactionLog.getRrn());
        return stringBuilder.toString();
    }
}