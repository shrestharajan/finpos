package global.citytech.finpos.merchant.data.repository.core.printparameter

import global.citytech.finpos.merchant.data.datasource.core.printparameter.PrintParameterDataSource
import global.citytech.finpos.merchant.domain.repository.core.printparameter.PrintParameterRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.printparameter.PrintParameterResponse
import global.citytech.finpos.processor.nibl.receipt.tmslogs.PrintParameterRequestModel
import global.citytech.finpos.processor.nibl.receipt.tmslogs.PrintParameterResponseModel
import global.citytech.finposframework.switches.receipt.tmslogs.TmsLogsParameter
import io.reactivex.Observable

/**
 * Created by Rishav Chudal on 11/5/20.
 */
class PrintParameterRepositoryImpl(
    private val printParameterDataSource: PrintParameterDataSource
): PrintParameterRepository {

    override fun prepareAndPrintLogs(
        configurationItem: ConfigurationItem,
        parameters: ArrayList<TmsLogsParameter>
    ): Observable<PrintParameterResponse> {
        return printParameterDataSource.prepareAndPrintLogs(
            configurationItem,
            parameters
        )
    }
}