package global.citytech.finpos.nibl.admin.data.response


data class TerminalParameter(
    var aidParameters: ArrayList<AidParam>? = null,
    var emvKeys: ArrayList<EmvKey>? = null,
    var emvParameters: EmvParam? = null,
    var cardSchemes: ArrayList<Any>? = null

)