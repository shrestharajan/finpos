package global.citytech.finposframework.hardware.io.cards

import java.util.*

/**
 * Created by Rishav Chudal on 6/8/20.
 */
class EmvTag(
    val tag: Int,
    var data: String,
    var size: Int
) {

    fun getTLV(): String {
        val stringBuilder = StringBuilder()
        stringBuilder.append(Integer.toHexString(tag))
        stringBuilder.append(getDoubleDigitSize())
        stringBuilder.append(data)
        return stringBuilder.toString()
    }

    private fun getDoubleDigitSize(): String {
        var formattedSize = "$size"
        when (size) {
            in 1..9 -> {
                formattedSize = "0$size"
            }
        }
        return formattedSize
    }

    fun tagInHex(): String {
        return Integer.toHexString(tag).toUpperCase(Locale.getDefault())
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val emvTag = other as EmvTag
        return tag == emvTag.tag
    }

    override fun hashCode(): Int {
        return Objects.hash(tag)
    }
}