package global.citytech.finpos.merchant.presentation.model

import global.citytech.finpos.merchant.domain.model.app.Merchant

data class MerchantItem(
    var id: String,
    var name: String? = null,
    var address: String? = null,
    var phoneNumber: String? = null,
    var switchId: String? = null,
    var terminalId: String? = null
)

fun Merchant.mapToPresentation(): MerchantItem =
    MerchantItem(id, name, address, phoneNumber, switchId, terminalId)

fun List<Merchant>.mapToPresentation(): List<MerchantItem> = map { it.mapToPresentation() }
