package global.citytech.finpos.processor.nibl.receipt.summaryreport;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.List;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.utility.JsonUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Unique Shakya on 1/5/2021.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(SummaryReportUseCase.class)
public class SummaryReportUseCaseTest {

    private TerminalRepository terminalRepository;
    private ApplicationRepository applicationRepository;
    private ReconciliationRepository reconciliationRepository;
    private PrinterService printerService;
    private SummaryReportReceiptMaker summaryReportReceiptMaker;
    private SummaryReportReceiptHandler summaryReportReceiptHandler;

    @Before
    public void setUp() throws Exception {
        this.terminalRepository = PowerMockito.mock(TerminalRepository.class);
        this.applicationRepository = PowerMockito.mock(ApplicationRepository.class);
        this.reconciliationRepository = PowerMockito.mock(ReconciliationRepository.class);
        this.printerService = PowerMockito.mock(PrinterService.class);
        this.summaryReportReceiptMaker = PowerMockito.mock(SummaryReportReceiptMaker.class);
        this.summaryReportReceiptHandler = PowerMockito.mock(SummaryReportReceiptHandler.class);
    }

    @Test
    public void executeTest() throws Exception {
        List<CardSchemeType> cardSchemeTypes = this.mockCardSchemeList();
        String batchNumber = "0001";
        SummaryReportReceipt summaryReportReceipt = this.mockSummaryReportReceipt();
        PowerMockito.when(this.terminalRepository.getReconciliationBatchNumber()).thenReturn(batchNumber);

        PowerMockito.whenNew(SummaryReportReceiptMaker.class).withArguments(this.terminalRepository,
                this.reconciliationRepository).thenReturn(this.summaryReportReceiptMaker);

        PowerMockito.when(this.applicationRepository.getSupportedCardSchemes()).thenReturn(cardSchemeTypes);

        PowerMockito.when(this.summaryReportReceiptMaker.prepare(batchNumber, cardSchemeTypes))
                .thenReturn(summaryReportReceipt);

        PowerMockito.whenNew(SummaryReportReceiptHandler.class).withArguments(this.printerService)
                .thenReturn(this.summaryReportReceiptHandler);
        PowerMockito.doNothing().when(this.summaryReportReceiptHandler).print(summaryReportReceipt);

        SummaryReportRequestModel summaryReportRequestModel = SummaryReportRequestModel.Builder.newInstance()
                .withApplicationRepository(this.applicationRepository)
                .withPrinterService(this.printerService)
                .withReconciliationRepository(this.reconciliationRepository)
                .build();

        SummaryReportUseCase summaryReportUseCase = new SummaryReportUseCase(this.terminalRepository);
        SummaryReportResponseModel summaryReportResponseModel = summaryReportUseCase.execute(summaryReportRequestModel);

        assertNotNull(summaryReportResponseModel);
        assertEquals(Result.SUCCESS, summaryReportResponseModel.getResult());
    }

    private SummaryReportReceipt mockSummaryReportReceipt() {
        String summaryReportReceiptString = "{\"retailer\":{\"retailerName\":\"CityTech\",\"retailerAddress\":\"Kamaladi\",\"merchantId\":\"123456123456\",\"terminalId\":\"1122334455\"},\"performance\":{\"startDateTime\":\"210105142659360\",\"endDateTime\":\"210105142659360\"},\"reconciliationBatchNumber\":\"0001\",\"host\":\"000048\",\"summaryReport\":{\"map\":{\"VISA_DEBIT\":{\"salesCount\":19,\"salesAmount\":1300,\"voidCount\":20,\"voidAmount\":2500,\"refundCount\":19,\"refundAmount\":1300},\"VISA_CREDIT\":{\"salesCount\":19,\"salesAmount\":1300,\"voidCount\":20,\"voidAmount\":2500,\"refundCount\":19,\"refundAmount\":1300},\"MASTERCARD\":{\"salesCount\":19,\"salesAmount\":1300,\"voidCount\":20,\"voidAmount\":2500,\"refundCount\":19,\"refundAmount\":1300}}}}";
        return JsonUtils.fromJsonToObj(summaryReportReceiptString, SummaryReportReceipt.class);
    }

    private List<CardSchemeType> mockCardSchemeList() {
        return Arrays.asList(CardSchemeType.VISA_CREDIT, CardSchemeType.VISA_DEBIT, CardSchemeType.MASTERCARD);
    }
}
