package global.citytech.finpos.merchant.data.datasource.core.balanceinquiry

import global.citytech.finpos.merchant.domain.model.balanceinquiry.BalanceInquiryResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.balanceinquiry.BalanceInquiryRequestItem
import io.reactivex.Observable

interface BalanceInquiryDataSource {
    fun generateBalanceInquiryRequest(
        configurationItem: ConfigurationItem,
        balanceInquiryRequestItem: BalanceInquiryRequestItem
    ):
            Observable<BalanceInquiryResponseEntity>
}