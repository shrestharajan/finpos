package global.citytech.neps.domain.usecase.device.configuration


import global.citytech.finpos.core.api.common.DeviceResponse
import global.citytech.neps.domain.device.configuration.Configuration
import global.citytech.neps.domain.model.LoadParameterRequest
import io.reactivex.Observable

class DeviceConfigurationUseCase(val configuration: Configuration) {
    fun injectKey(key: String): Observable<DeviceResponse> = configuration.inject(key)
    fun initSdk(): Observable<DeviceResponse> = configuration.initSdk()
    fun loadParameters(loadParameterRequest: LoadParameterRequest): Observable<DeviceResponse> =
        configuration.LoadTerminalParameters(loadParameterRequest)

}