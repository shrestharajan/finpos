package global.citytech.finposframework.usecases.transaction.data;

import java.math.BigDecimal;

import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest;
import global.citytech.finposframework.usecases.TransactionType;

/**
 * Created by Unique Shakya
 */

public class TransactionRequest {
    private final TransactionType transactionType;
    private BigDecimal amount = BigDecimal.ZERO;
    private BigDecimal additionalAmount = BigDecimal.ZERO;
    private boolean fallbackIccToMag;
    private EmvParametersRequest emvParameterRequest;
    private String originalInvoiceNumber;
    private String originalRetrievalReferenceNumber;
    private String originalApprovalCode;
    private String cardNumber;
    private String expiryDate;
    private String cvv;
    private boolean isEmiTransaction;
    private String emiInfo;
    private String vatInfo;

    public TransactionRequest(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public EmvParametersRequest getEmvParameterRequest() {
        return emvParameterRequest;
    }

    public void setEmvParameterRequest(EmvParametersRequest emvParameterRequest) {
        this.emvParameterRequest = emvParameterRequest;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAdditionalAmount() {
        return additionalAmount;
    }

    public void setAdditionalAmount(BigDecimal additionalAmount) {
        this.additionalAmount = additionalAmount;
    }

    public boolean isFallbackIccToMag() {
        return fallbackIccToMag;
    }

    public void setFallbackIccToMag(boolean fallbackIccToMag) {
        this.fallbackIccToMag = fallbackIccToMag;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public String getOriginalInvoiceNumber() {
        return originalInvoiceNumber;
    }

    public void setOriginalInvoiceNumber(String originalInvoiceNumber) {
        this.originalInvoiceNumber = originalInvoiceNumber;
    }

    public String getOriginalRetrievalReferenceNumber() {
        return originalRetrievalReferenceNumber;
    }

    public void setOriginalRetrievalReferenceNumber(String originalRetrievalReferenceNumber) {
        this.originalRetrievalReferenceNumber = originalRetrievalReferenceNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getOriginalApprovalCode() {
        return originalApprovalCode;
    }

    public void setOriginalApprovalCode(String originalApprovalCode) {
        this.originalApprovalCode = originalApprovalCode;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public boolean isEmiTransaction() {
        return isEmiTransaction;
    }

    public void setEmiTransaction(boolean emiTransaction) {
        isEmiTransaction = emiTransaction;
    }

    public String getEmiInfo() {
        return emiInfo;
    }

    public void setEmiInfo(String emiInfo) {
        this.emiInfo = emiInfo;
    }

    public String getVatInfo() {
        return vatInfo;
    }

    public void setVatInfo(String vatInfo) {
        this.vatInfo = vatInfo;
    }
}
