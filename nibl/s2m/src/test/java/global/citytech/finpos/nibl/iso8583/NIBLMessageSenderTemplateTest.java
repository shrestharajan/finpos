package global.citytech.finpos.nibl.iso8583;

import org.junit.Before;
import org.junit.Test;

import global.citytech.finposframework.iso8583.DefaultMessageParser;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.SpecInfo;

/**
 * Created by Saurav Ghimire on 1/26/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class NIBLMessageSenderTemplateTest {

    SpecInfo specInfo = new NIBLSpecInfo();
    DefaultMessageParser messageParser;

    @Before
    public void setup() {
        messageParser = new DefaultMessageParser(specInfo);
    }

    @Test
    public void testParse() {


        String hexData = "0210203801000A8002060000000000041615150126001131303236313637313735373839313030303031363738000000050003523000";

        Iso8583Msg message = messageParser.parse(hextToByte(hexData));
        System.out.println("ISO MESSAGE :::: " + message.getPrintableData());

    }

    public byte[] hextToByte(String str) {
        byte[] val = new byte[str.length() / 2];
        for (int i = 0; i < val.length; i++) {
            int index = i * 2;
            int j = Integer.parseInt(str.substring(index, index + 2), 16);
            val[i] = (byte) j;
        }
        return val;
    }
}