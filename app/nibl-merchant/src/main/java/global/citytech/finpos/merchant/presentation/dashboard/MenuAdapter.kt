package global.citytech.finpos.merchant.presentation.dashboard

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.menu.MenuItem
import kotlinx.android.synthetic.main.item_menu.view.*


/**
 * Created by Saurav Ghimire on 1/19/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
class MenuAdapter(val menus: List<MenuItem>) : RecyclerView.Adapter<MenuAdapter.MenuViewHolder>() {

    lateinit var context: Context

    class MenuViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        context = parent.context
        return MenuViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_menu, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        val menuItem = menus[position]
        holder.itemView.tv_menu_title.tv_menu_title.text =
            context.resources.getString(menuItem.menu!!.title)
        holder.itemView.iv_icon.setIconResource(menuItem.menu.icon)
        holder.itemView.setOnClickListener {
            menuItem.menuHandler.performOnClick(context = context as Activity)
        }

    }

    override fun getItemCount() = menus.size
}