package global.citytech.finpos.merchant.domain.usecase.core.printparameters

import global.citytech.finpos.merchant.domain.repository.core.printparameter.PrintParameterRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.printparameter.PrintParameterResponse
import global.citytech.finpos.processor.nibl.receipt.tmslogs.PrintParameterResponseModel
import global.citytech.finposframework.switches.receipt.tmslogs.TmsLogsParameter
import io.reactivex.Observable

/**
 * Created by Rishav Chudal on 11/6/20.
 */
class PrintParametersUseCase(private val printParameterRepository: PrintParameterRepository) {
    fun prepareAndPrintParameters(
        configurationItem: ConfigurationItem,
        parameters: ArrayList<TmsLogsParameter>
    ): Observable<PrintParameterResponse> = printParameterRepository.prepareAndPrintLogs(
        configurationItem,
        parameters
    )
}