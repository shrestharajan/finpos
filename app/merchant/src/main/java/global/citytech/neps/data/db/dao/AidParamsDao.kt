package global.citytech.neps.data.db.dao

import android.database.Cursor
import androidx.room.*
import global.citytech.neps.domain.model.AidParam
import global.citytech.neps.domain.model.Host

@Dao
interface AidParamsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(aidParams: List<AidParam>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(aidParam: AidParam): Long

    @Query("SELECT * FROM aid_param")
    fun get(): Cursor

    @Query("SELECT * FROM aid_param")
    fun getAidParamList(): List<AidParam>

    @Query("SELECT * FROM aid_param where id = :id")
    fun getById(id: Long): Cursor?

    @Update
    fun update(aidParam: AidParam):Int

    @Query("DELETE FROM aid_param WHERE id = :id")
    fun deleteById(id: Long): Int
}