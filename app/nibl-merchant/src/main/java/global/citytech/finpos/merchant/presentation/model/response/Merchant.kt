package global.citytech.finpos.merchant.presentation.model.response

data class Merchant(
    var address: String? = null,
    var name: String? = null,
    var phoneNumber: String? = null,
    var switchId: String? = null
) {
    companion object {
        const val TABLE_NAME = "merchant"
        const val COLUMN_ID = "id"
        const val COLUMN_NAME = "merchant_name"
        const val COLUMN_ADDRESS = "merchant_address"
        const val COLUMN_PHONE_NUMBER = "merchant_phone"
        const val COLUMN_SWITCH_ID = "merchant_switch_id"
        const val COLUMN_TERMINAL_ID = "merchant_terminal_id"
    }
}