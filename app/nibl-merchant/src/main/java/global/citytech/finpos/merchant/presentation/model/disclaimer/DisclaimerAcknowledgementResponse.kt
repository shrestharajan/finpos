package global.citytech.finpos.merchant.presentation.model.disclaimer

data class DisclaimerAcknowledgementResponse(
    val serviceId: String
)