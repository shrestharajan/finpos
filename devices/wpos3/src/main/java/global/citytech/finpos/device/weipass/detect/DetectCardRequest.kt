package global.citytech.finpos.device.weipass.detect

import global.citytech.finposframework.hardware.io.cards.CardType

/**
 * Created by Rishav Chudal on 5/12/20.
 */
data class DetectCardRequest(
    val slotsToOpen: List<CardType>,
    val timeoutInSecs: Int?,
    val packageName: String
)