package global.citytech.finposframework.hardware.io.cards.cvm;

public enum CardHolderVerification { // TODO remove all arabic translations

    PIN_VERIFIED("CARDHOLDER PIN VERIFIED", "تم التحقق من الرقم السري للعميل"),
    SIGNATURE_VERIFIED("CARDHOLDER VERIFIED BY SIGNATURE", "تم التحقق بتوقيع العميل"),
    NO_VERIFICATION("NO VERIFICATION REQUIRED", ""),
    DEVICE_OWNER_VERIFIED("DEVICE OWNER IDENTITY VERIFIED","تم التحقق من هوية حامل الجهاز");

    private String cardHolderVerificationEn;
    private String cardHolderVerificationAr;

    CardHolderVerification(String cardHolderVerificationEn, String cardHolderVerificationAr) {
        this.cardHolderVerificationEn = cardHolderVerificationEn;
        this.cardHolderVerificationAr = cardHolderVerificationAr;
    }

    public String getCardHolderVerificationEn() {
        return cardHolderVerificationEn;
    }

    public String getCardHolderVerificationAr() {
        return cardHolderVerificationAr;
    }
}
