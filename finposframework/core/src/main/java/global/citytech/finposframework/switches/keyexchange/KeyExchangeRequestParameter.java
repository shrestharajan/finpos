package global.citytech.finposframework.switches.keyexchange;

import global.citytech.finposframework.switches.RequestParameter;

/** @author rajudhital on 4/21/20 */
public interface KeyExchangeRequestParameter extends RequestParameter {}
