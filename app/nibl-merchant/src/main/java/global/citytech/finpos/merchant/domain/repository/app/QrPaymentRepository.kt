package global.citytech.finpos.merchant.domain.repository.app

import global.citytech.finpos.merchant.domain.model.app.QrPayment

interface QrPaymentRepository {
    fun getAllQrPayments(): MutableList<QrPayment>
    fun getCount(): Long
    fun addQrPayment(qrPayment: QrPayment)
    fun removeQrPayment(qrPayment: QrPayment)
    fun updateQrPaymentStatus(status: String, qrPayment: QrPayment)
    fun getLastUpdateDateTime(): String
    fun getBillingInvoiceNumber(): String
    fun incrementBillingInvoiceNumber()
    fun getLimitedQrPayments(pageNumber: Int, offset: Int): MutableList<QrPayment>
    fun getLimitedQrPaymentsBySearchParam(searchParam: String, pageNumber: Int, pageSize: Int): MutableList<QrPayment>
    fun getCountBySearchParam(searchParam: String): Long
    fun getQrPaymentByInvoiceNumber(invoiceNumber: String): QrPayment?
    fun updateQrPaymentRefunded(qrPayment: QrPayment)
    fun updateQrPaymentVoided(qrPayment: QrPayment)
    fun clear()
    fun saveBillingInvoiceNumber(invoiceNumber:String)
    fun generateFonepayQrIdentifiers(key: String): String
}