package global.citytech.finposframework.switches;

import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.switches.autoreversal.AutoReversalRequester;
import global.citytech.finposframework.switches.keyexchange.KeyExchangeRequester;
import global.citytech.finposframework.switches.logon.LogOnRequester;
import global.citytech.finposframework.switches.posmode.PosModeRequester;
import global.citytech.finposframework.switches.receipt.customercopy.CustomerCopyRequester;
import global.citytech.finposframework.switches.receipt.detailreport.DetailReportRequester;
import global.citytech.finposframework.switches.receipt.duplicatereceipt.DuplicateReceiptRequester;
import global.citytech.finposframework.switches.receipt.duplicatereconciliation.DuplicateReconciliationReceiptRequester;
import global.citytech.finposframework.switches.receipt.ministatement.StatementRequester;
import global.citytech.finposframework.switches.receipt.summaryreport.SummaryReportRequester;
import global.citytech.finposframework.switches.receipt.tmslogs.TmsLogsRequester;
import global.citytech.finposframework.switches.reconciliation.ReconciliationRequester;
import global.citytech.finposframework.switches.reconciliation.check.CheckReconciliationRequester;
import global.citytech.finposframework.switches.reconciliation.clear.BatchClearRequester;
import global.citytech.finposframework.switches.transaction.TransactionRequester;
import global.citytech.finposframework.switches.transactiontype.TransactionTypeRequester;

/**
 * @author rajudhital on 4/3/20
 * modified RishavChudal on 23/11/20
 **/
public interface Processor {

    void init(TerminalRepository terminalRepository, Notifier notifier);

    LogOnRequester getLogOnRequest();

    KeyExchangeRequester getKeyExchangeRequester();

    TransactionRequester getPurchaseRequester();

    TransactionRequester getManualPurchaseRequester();

    TransactionRequester getIdlePurchaseRequester();

    TransactionRequester getPreAuthRequester();

    TransactionRequester getManualPreAuthRequester();

    CustomerCopyRequester getCustomerCopyRequester();

    StatementRequester getStatementPrintRequester();

    ReconciliationRequester getReconciliationRequester();

    TransactionRequester getVoidSaleRequester();

    TransactionRequester getRefundRequester();

    TransactionRequester getManualRefundRequester();

    TransactionRequester getAuthorisationCompletionRequester();

    TmsLogsRequester getTmsLogsRequester();

    TransactionTypeRequester getTransactionTypeRequester();

    DuplicateReceiptRequester getDuplicateReceiptRequester();

    DuplicateReconciliationReceiptRequester getDuplicateReconciliationReceiptRequester();

    DetailReportRequester getDetailReportRequester();

    SummaryReportRequester getSummaryReportRequester();

    PosModeRequester getPosModeRequester();

    TransactionRequester getCashAdvanceRequester();

    TransactionRequester getManualCashAdvanceRequester();

    TransactionRequester getTipAdjustmentRequester();

    AutoReversalRequester getAutoReversalRequester();

    CheckReconciliationRequester getCheckReconciliationRequester();

    BatchClearRequester getBatchClearRequester();

    TransactionRequester getGreenPinRequester();

    TransactionRequester getPinChangeRequester();

    TransactionRequester getBalanceInquiryRequester();

    TransactionRequester getCashInRequester();

    TransactionRequester getMiniStatementRequester();
}
