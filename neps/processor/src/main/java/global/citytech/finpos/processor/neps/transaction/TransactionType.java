package global.citytech.finpos.processor.neps.transaction;


import global.citytech.finpos.neps.iso8583.ProcessingCode;

public enum TransactionType {

    PURCHASE(0, ProcessingCode.PURCHASE.getCode()),
    LOG_ON(1, ProcessingCode.LOG_ON.getCode()),
    PURCHASE_WITH_CASHBACK(2, ProcessingCode.PURCHASE_WITH_CASHBACK.getCode()),
    PRE_AUTH(3, ProcessingCode.PRE_AUTHORIZATION.getCode()),
    REFUND(4, ProcessingCode.REFUND.getCode()),
    CASH_ADVANCE(5, ProcessingCode.CASH_ADVANCE.getCode()),
    PRE_AUTH_COMPLETION(6, ProcessingCode.PRE_AUTHORIZATION_COMPLETION.getCode()),
    REVERSAL(7, ProcessingCode.REVERSAL.getCode()),
    SETTLEMENT(8, ProcessingCode.SETTLEMENT.getCode()),
    VOID(9, ProcessingCode.VOID_SALE.getCode()),
    GREEN_PIN(10, ProcessingCode.GREEN_PIN.getCode()),
    PIN_CHANGE(11, ProcessingCode.PIN_CHANGE.getCode());

    private int offset;
    private String processingCode;

    TransactionType(int offset, String processingCode) {
        this.offset = offset;
        this.processingCode = processingCode;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public int getOffset() {
        return offset;
    }
}