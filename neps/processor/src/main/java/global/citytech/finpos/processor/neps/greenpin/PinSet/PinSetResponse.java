package global.citytech.finpos.processor.neps.greenpin.PinSet;

import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;

public class PinSetResponse extends TransactionResponse {
    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private String message;
        private String stan;
        private boolean approved;
        private boolean shouldPrintCustomerCopy;
        private ReadCardResponse readCardResponse;

        private Builder() {
        }

        public static PinSetResponse.Builder newInstance() {
            return new PinSetResponse.Builder();
        }

        public PinSetResponse.Builder withDebugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public PinSetResponse.Builder withDebugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public PinSetResponse.Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }

        public PinSetResponse.Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public PinSetResponse.Builder withShouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public PinSetResponse.Builder withApproved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public PinSetResponse.Builder withReadCardResponse(ReadCardResponse readCardResponse) {
            this.readCardResponse = readCardResponse;
            return this;
        }

        public PinSetResponse build() {
            PinSetResponse PinSetResponseModel = new PinSetResponse();
            PinSetResponseModel.debugRequestMessage = this.debugRequestMessage;
            PinSetResponseModel.approved = this.approved;
            PinSetResponseModel.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            PinSetResponseModel.stan = this.stan;
            PinSetResponseModel.debugResponseMessage = this.debugResponseMessage;
            PinSetResponseModel.message = this.message;
            PinSetResponseModel.readCardResponse = this.readCardResponse;
            return PinSetResponseModel;
        }
    }
}
