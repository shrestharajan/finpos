package global.citytech.payment.sdk.core;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import global.citytech.payment.sdk.api.PaymentResult;
import global.citytech.payment.sdk.utils.HexUtils;

/**
 * Created by BikashShrestha on 5/31/19.
 */
public final class PaymentMacGenerator {

    private PaymentMacGenerator() {
    }

    public static String generateMacWithGivenKeyAndData(String key, String data) {
        try {
            String algorithm = "RawBytes";
            SecretKeySpec secretKeySpec = new SecretKeySpec(HexUtils.hex2byte(key), algorithm);
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(secretKeySpec);
            byte[] bytes = data.getBytes();
            byte[] macResult = mac.doFinal(bytes);
            return HexUtils.byte2hex(macResult);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            if (e instanceof NoSuchAlgorithmException)
                throw new PaymentException(PaymentResult.MAC_INVALID_ALGORITHM);
            throw new PaymentException(PaymentResult.MAC_INVALID_KEY);
        }
    }
}
