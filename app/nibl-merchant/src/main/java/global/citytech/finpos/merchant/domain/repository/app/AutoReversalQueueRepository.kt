package global.citytech.finpos.merchant.domain.repository.app

import global.citytech.finposframework.usecases.transaction.data.AutoReversal

/**
 * Created by Unique Shakya on 6/22/2021.
 */
interface AutoReversalQueueRepository {

    fun isQueueEmpty(): Boolean

    fun isActive(): Boolean

    fun getSize(): Long

    fun getAllQueueRequests(): MutableList<AutoReversal>

    fun getRecentRequest(): AutoReversal?

    fun removeRequest(autoReversal: AutoReversal)

    fun addRequest(autoReversal: AutoReversal)

    fun incrementRetryCount(autoReversal: AutoReversal)

    fun changeStatus(autoReversal: AutoReversal, status: AutoReversal.Status)

    fun clear()
}