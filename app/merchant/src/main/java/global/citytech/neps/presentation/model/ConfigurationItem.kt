package global.citytech.neps.presentation.model

import global.citytech.neps.domain.usecase.local.Configuration

data class ConfigurationItem(
    var hosts: List<HostItem>? = null,
    var merchants: List<MerchantItem>? = null,
    var logos:List<LogoItem>? = null
)

fun Configuration.mapToPresentation(): ConfigurationItem =
    ConfigurationItem(hosts = hostList.maptoPresentation(), merchants= merchantList.mapToPresentation(),logos = logos.mapToPresentation())

fun List<Configuration>.mapToPresentation(): List<ConfigurationItem> = map { it.mapToPresentation() }