package global.citytech.finpos.merchant.domain.model.logon

import global.citytech.finposframework.hardware.io.printer.PrinterService
import global.citytech.finposframework.hardware.keymgmt.keys.KeyService

/**
 * Created by Saurav Ghimire on 6/9/21.
 * sauravnghimire@gmail.com
 */


data class LogOnRequestEntity(
    val printerService: PrinterService? = null,
    val hardwareKeyService: KeyService? = null,
    val applicationPackageName: String? = null
)