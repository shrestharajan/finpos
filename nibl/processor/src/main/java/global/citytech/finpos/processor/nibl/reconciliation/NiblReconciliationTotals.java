package global.citytech.finpos.processor.nibl.reconciliation;

import global.citytech.finposframework.usecases.reconciliation.ReconciliationTotals;
import global.citytech.finposframework.utility.HelperUtils;

/**
 * Created by Unique Shakya on 9/22/2020.
 */
public class NiblReconciliationTotals implements ReconciliationTotals {

    private long salesCount;
    private long salesAmount;
    private long salesRefundCount;
    private long salesRefundAmount;
    private long debitCount;
    private long debitAmount;
    private long debitRefundCount;
    private long debitRefundAmount;
    private long authCount;
    private long authAmount;
    private long authRefundCount;
    private long authRefundAmount;

    public long getSalesCount() {
        return salesCount;
    }

    public long getSalesAmount() {
        return salesAmount;
    }

    public long getSalesRefundCount() {
        return salesRefundCount;
    }

    public long getSalesRefundAmount() {
        return salesRefundAmount;
    }

    public long getDebitCount() {
        return debitCount;
    }

    public long getDebitAmount() {
        return debitAmount;
    }

    public long getDebitRefundCount() {
        return debitRefundCount;
    }

    public long getDebitRefundAmount() {
        return debitRefundAmount;
    }

    public long getAuthCount() {
        return authCount;
    }

    public long getAuthAmount() {
        return authAmount;
    }

    public long getAuthRefundCount() {
        return authRefundCount;
    }

    public long getAuthRefundAmount() {
        return authRefundAmount;
    }

    public long getTotalCount() {
        return salesCount + salesRefundCount + debitCount + debitRefundCount + authCount + authRefundCount;
    }

    public long getTotalAmount() {
        return salesAmount + salesRefundAmount + debitAmount + debitRefundAmount + authAmount + authRefundAmount;
    }

    @Override
    public String toTotalsBlock() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(HelperUtils.toISOAmount(salesCount, 3));
        stringBuilder.append(HelperUtils.toISOAmount(salesAmount, 12));
        stringBuilder.append(HelperUtils.toISOAmount(salesRefundCount, 3));
        stringBuilder.append(HelperUtils.toISOAmount(salesRefundAmount, 12));
        stringBuilder.append(HelperUtils.toISOAmount(debitCount, 3));
        stringBuilder.append(HelperUtils.toISOAmount(debitAmount, 12));
        stringBuilder.append(HelperUtils.toISOAmount(debitRefundCount, 3));
        stringBuilder.append(HelperUtils.toISOAmount(debitRefundAmount, 12));
        stringBuilder.append(HelperUtils.toISOAmount(authCount, 3));
        stringBuilder.append(HelperUtils.toISOAmount(authAmount, 12));
        stringBuilder.append(HelperUtils.toISOAmount(authRefundCount, 3));
        stringBuilder.append(HelperUtils.toISOAmount(authRefundAmount, 12));
        return stringBuilder.toString();
    }

    @Override
    public String toString() {
        return "NiblReconciliationTotals{" +
                "salesCount=" + salesCount +
                ", salesAmount=" + salesAmount +
                ", salesRefundCount=" + salesRefundCount +
                ", salesRefundAmount=" + salesRefundAmount +
                ", debitCount=" + debitCount +
                ", debitAmount=" + debitAmount +
                ", debitRefundCount=" + debitRefundCount +
                ", debitRefundAmount=" + debitRefundAmount +
                ", authCount=" + authCount +
                ", authAmount=" + authAmount +
                ", authRefundCount=" + authRefundCount +
                ", authRefundAmount=" + authRefundAmount +
                '}';
    }

    public static final class Builder {
        private long salesCount;
        private long salesAmount;
        private long salesRefundCount;
        private long salesRefundAmount;
        private long debitCount;
        private long debitAmount;
        private long debitRefundCount;
        private long debitRefundAmount;
        private long authCount;
        private long authAmount;
        private long authRefundCount;
        private long authRefundAmount;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withSalesCount(long salesCount) {
            this.salesCount = salesCount;
            return this;
        }

        public Builder withSalesAmount(long salesAmount) {
            this.salesAmount = salesAmount;
            return this;
        }

        public Builder withSalesRefundCount(long salesRefundCount) {
            this.salesRefundCount = salesRefundCount;
            return this;
        }

        public Builder withSalesRefundAmount(long salesRefundAmount) {
            this.salesRefundAmount = salesRefundAmount;
            return this;
        }

        public Builder withDebitCount(long debitCount) {
            this.debitCount = debitCount;
            return this;
        }

        public Builder withDebitAmount(long debitAmount) {
            this.debitAmount = debitAmount;
            return this;
        }

        public Builder withDebitRefundCount(long debitRefundCount) {
            this.debitRefundCount = debitRefundCount;
            return this;
        }

        public Builder withDebitRefundAmount(long debitRefundAmount) {
            this.debitRefundAmount = debitRefundAmount;
            return this;
        }

        public Builder withAuthCount(long authCount) {
            this.authCount = authCount;
            return this;
        }

        public Builder withAuthAmount(long authAmount) {
            this.authAmount = authAmount;
            return this;
        }

        public Builder withAuthRefundCount(long authRefundCount) {
            this.authRefundCount = authRefundCount;
            return this;
        }

        public Builder withAuthRefundAmount(long authRefundAmount) {
            this.authRefundAmount = authRefundAmount;
            return this;
        }

        public NiblReconciliationTotals build() {
            NiblReconciliationTotals niblReconciliationTotals = new NiblReconciliationTotals();
            niblReconciliationTotals.authRefundAmount = this.authRefundAmount;
            niblReconciliationTotals.authRefundCount = this.authRefundCount;
            niblReconciliationTotals.salesCount = this.salesCount;
            niblReconciliationTotals.debitAmount = this.debitAmount;
            niblReconciliationTotals.debitRefundCount = this.debitRefundCount;
            niblReconciliationTotals.authCount = this.authCount;
            niblReconciliationTotals.authAmount = this.authAmount;
            niblReconciliationTotals.salesAmount = this.salesAmount;
            niblReconciliationTotals.debitRefundAmount = this.debitRefundAmount;
            niblReconciliationTotals.salesRefundCount = this.salesRefundCount;
            niblReconciliationTotals.salesRefundAmount = this.salesRefundAmount;
            niblReconciliationTotals.debitCount = this.debitCount;
            return niblReconciliationTotals;
        }
    }
}
