package global.citytech.finposframework.switches.keyexchange;

import global.citytech.finposframework.switches.Request;

/** @author rajudhital on 4/21/20 */
public interface KeyExchangeRequester<
        T extends KeyExchangeRequestParameter, E extends KeyExchangeResponseParameter>
    extends Request<T, E> {}
