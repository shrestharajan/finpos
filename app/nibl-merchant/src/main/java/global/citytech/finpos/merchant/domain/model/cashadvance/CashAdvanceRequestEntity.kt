package global.citytech.finpos.merchant.domain.model.cashadvance

import global.citytech.finposframework.hardware.io.cards.read.ReadCardService
import global.citytech.finposframework.hardware.io.led.LedService
import global.citytech.finposframework.hardware.io.printer.PrinterService
import global.citytech.finposframework.hardware.io.sound.SoundService
import global.citytech.finposframework.repositories.ApplicationRepository
import global.citytech.finposframework.repositories.ReconciliationRepository
import global.citytech.finposframework.repositories.TransactionRepository
import global.citytech.finposframework.usecases.controllers.DeviceController
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest

data class CashAdvanceRequestEntity(
        var transactionRequest: TransactionRequest? = null,
        val transactionRepository: TransactionRepository? = null,
        val readCardService: ReadCardService? = null,
        val deviceController: DeviceController? = null,
        val transactionAuthenticator: TransactionAuthenticator? = null,
        val printerService: PrinterService? = null,
        val applicationRepository: ApplicationRepository? = null,
        val reconciliationRepository: ReconciliationRepository? = null,
        val ledService: LedService? = null,
        val soundService: SoundService? = null
)
