package global.citytech.finpos.merchant.presentation.transactions.card

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import global.citytech.common.Constants.DEFAULT_PAGE_SIZE
import global.citytech.common.extensions.hideKeyboard
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.base.AppBaseFragment
import global.citytech.finpos.merchant.presentation.model.TransactionItem
import global.citytech.finpos.merchant.presentation.transactions.TransactionsViewModel
import kotlinx.android.synthetic.main.fragment_card_transactions.*

class CardTransactionFragment : AppBaseFragment() {

    private val adapter = CardTransactionsAdapter(mutableListOf())
    private var transactions = mutableListOf<TransactionItem>()
    lateinit var textWatcher: TextWatcher
    lateinit var onEditActionListener: TextView.OnEditorActionListener
    var pageNumber = 1
    var searchPageNumber = 1
    lateinit var scrollListener: RecyclerView.OnScrollListener
    var transactionsCount = 0
    var searchTransactionsCount = 0
    val pageSize = DEFAULT_PAGE_SIZE
    var searchParam = ""

//    companion object {
//
//        private var instance: CardTransactionFragment? = null
//
//        fun getInstance(): CardTransactionFragment {
//            if (instance == null)
//                instance = CardTransactionFragment()
//            return instance as CardTransactionFragment
//        }
//    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
        hideSoftKeyboard(requireActivity(),requireView())
    }

    override fun onResume() {
        super.onResume()
        getViewModel().countTransactions()
        et_invoice?.setText("")
    }

    fun getTransactions() {
        getViewModel().getTransactions(pageNumber, pageSize)
    }

    override fun getLayoutId(): Int = R.layout.fragment_card_transactions

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CardTransactionDetailActivity.REQUEST_CODE &&
            resultCode == CardTransactionDetailActivity.RESULT_FINISH_HOST_ACTIVITY
        )
            activity?.finish()
    }

    private fun initObservers() {
        getViewModel().transactionAvailable.observe(viewLifecycleOwner, Observer {
            if (it) {
                et_invoice.visibility = View.VISIBLE
                tv_no_txn.visibility = View.INVISIBLE
                cv_rv_wrapper.visibility = View.VISIBLE
                rv_transactions.visibility - View.VISIBLE
            } else {
                et_invoice.visibility = View.INVISIBLE
                tv_no_txn.visibility = View.VISIBLE
                cv_rv_wrapper.visibility = View.INVISIBLE
                rv_transactions.visibility - View.INVISIBLE
            }
        })

        getViewModel().transactions.observe(viewLifecycleOwner, Observer {
            transactions = it.toMutableList()
            adapter.updateAll(transactions)
            if (pageNumber == 1 && et_invoice.text?.isEmpty() == true && transactions.size < transactionsCount) {
                pageNumber++
                getViewModel().getTransactions(pageNumber, pageSize, true)
            }
        })

        getViewModel().isPaginating.observe(viewLifecycleOwner, Observer {
            if (it) {
                iv_progress_pagination.visibility = View.VISIBLE
            } else {
                iv_progress_pagination.visibility = View.GONE
            }
        })

        getViewModel().transactionsCount.observe(viewLifecycleOwner, Observer {
            transactionsCount = it
        })

        getViewModel().searchedTransactionsCount.observe(viewLifecycleOwner, Observer {
            searchTransactionsCount = it
        })

        getViewModel().isLoading.observe(viewLifecycleOwner, Observer {
            if (it) {
                showProgress()
            } else {
                hideProgress()
            }
        })
    }

    private fun initViews() {

        textWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchParam = s.toString()
                if ((s.toString()).isEmpty()) {
                    pageNumber = 1
                    getViewModel().getTransactions(pageNumber, pageSize, true)
                    return
                }
                searchPageNumber = 1
                pageNumber = 1
                adapter.update(transactions.filter { it.invoiceNumber!!.contains(s!!) }
                    .reversed()
                    .toMutableList())

                getViewModel().searchTransactions(s.toString(), searchPageNumber, pageSize)
            }

            override fun afterTextChanged(s: Editable?) {

            }
        }
        et_invoice.addTextChangedListener(textWatcher)

        onEditActionListener = object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_NUMPAD_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    activity?.hideKeyboard(et_invoice);
                }
                return false;
            }
        }

        Glide.with(this).load("file:///android_asset/images/loading.gif")
            .into(iv_progress_pagination)

        et_invoice.setOnEditorActionListener(onEditActionListener)

        val layoutManager = LinearLayoutManager(activity)
        rv_transactions.layoutManager = layoutManager
        scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) { //check for scroll down
                    val visibleItemCount = layoutManager.getChildCount();
                    val totalItemCount = layoutManager.getItemCount();
                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
                    var availablePages = transactionsCount / pageSize
                    var availableSearchPages = searchTransactionsCount / pageSize
                    if (transactionsCount % pageSize != 0) {
                        availablePages++
                    }
                    if (searchTransactionsCount % pageSize != 0) {
                        availableSearchPages++
                    }
                    if (et_invoice.text.toString().isEmpty()) {
                        if (((visibleItemCount + pastVisiblesItems) >= (totalItemCount - 5)) && !(getViewModel().isPaginating.value)!! && (++pageNumber <= availablePages)) {
                            getViewModel().getTransactions(pageNumber, pageSize)
                        }
                    } else {
                        if (((visibleItemCount + pastVisiblesItems) >= (totalItemCount - 5)) && !(getViewModel().isPaginating.value)!! && (++searchPageNumber <= availableSearchPages)) {
                            getViewModel().searchTransactions(
                                searchParam,
                                searchPageNumber,
                                pageSize
                            )
                        }
                    }


                }
            }

        }
        rv_transactions.addOnScrollListener(scrollListener)
        rv_transactions.adapter = adapter
    }

    fun getViewModel() = ViewModelProviders.of(requireActivity())[TransactionsViewModel::class.java]

    override fun onDestroyView(){
        et_invoice.removeTextChangedListener(textWatcher)
        et_invoice.setOnEditorActionListener(null)
        rv_transactions.removeOnScrollListener(scrollListener)
        super.onDestroyView()
    }
}