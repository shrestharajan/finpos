package global.citytech.finpos.merchant.domain.model.purchase

import global.citytech.finposframework.hardware.io.cards.read.ReadCardService
import global.citytech.finposframework.hardware.io.led.LedService
import global.citytech.finposframework.hardware.io.printer.PrinterService
import global.citytech.finposframework.hardware.io.sound.SoundService
import global.citytech.finposframework.repositories.ApplicationRepository
import global.citytech.finposframework.repositories.ReconciliationRepository
import global.citytech.finposframework.repositories.TransactionRepository
import global.citytech.finposframework.usecases.controllers.DeviceController
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest


/**
 * Created by Saurav Ghimire on 1/20/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
data class PurchaseRequestEntity(
    var transactionRequest: TransactionRequest? = null,
    var transactionRepository: TransactionRepository? = null,
    var readCardService: ReadCardService? = null,
    var deviceController: DeviceController? = null,
    var transactionAuthenticator: TransactionAuthenticator? = null,
    var printerService: PrinterService? = null,
    var applicationRepository: ApplicationRepository? = null,
    var reconciliationRepository: ReconciliationRepository? = null,
    var ledService: LedService? = null,
    var soundService: SoundService? = null
)