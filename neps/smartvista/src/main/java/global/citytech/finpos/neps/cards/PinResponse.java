package global.citytech.finpos.neps.cards;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.PinBlock;

public class PinResponse {

    private PinBlock pinBlock;
    private Result result;

    public PinResponse() {
        result = Result.FAILURE;
    }

    public void setPinBlock(PinBlock pinBlock) {
        this.pinBlock = pinBlock;
    }

    public PinBlock getPinBlock() {
        return pinBlock;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}