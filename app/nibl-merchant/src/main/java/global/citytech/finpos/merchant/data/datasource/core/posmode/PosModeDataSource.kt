package global.citytech.finpos.merchant.data.datasource.core.posmode

import global.citytech.finposframework.switches.posmode.PosMode

/**
 * Created by Unique Shakya on 1/7/2021.
 */
interface PosModeDataSource {
    fun getPosMode(merchantCategoryCode: String): PosMode
}