package global.citytech.finpos.device.weipass.detect

import android.content.Context
import android.os.RemoteException
import com.wiseasy.emvprocess.SDKInstance
import com.wiseasy.emvprocess.utils.ByteUtil
import global.citytech.finpos.device.weipass.utils.WeipassDeviceUtils
import global.citytech.finpos.device.weipass.utils.WeipassConstant.CARD_TYPE_MAG_PRIOR_TO_CL
import global.citytech.finpos.device.weipass.utils.WeipassConstant.DEFAULT_CARD_READ_TIMEOUT
import global.citytech.finpos.device.weipass.utils.WeipassConstant.DETECT_CARD_CANCEL
import global.citytech.finpos.device.weipass.utils.WeipassConstant.DETECT_CARD_TIMEOUT
import global.citytech.finpos.device.weipass.utils.WeipassConstant.DETECT_ERROR_CASE_1
import global.citytech.finpos.device.weipass.utils.WeipassConstant.DETECT_ERROR_CASE_2
import global.citytech.finpos.device.weipass.utils.WeipassConstant.NO_CARD_DETECTED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.NO_SUCH_SLOT_TO_OPEN
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SLOT_OPEN_EMPTY
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.CardType
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException
import wangpos.sdk4.libbasebinder.BankCard

/**
 * Created by Rishav Chudal on 5/12/20.
 */
class WeipassDetectCardServiceImplTest {

    @Rule
    @JvmField
    var expectedException = ExpectedException.none()

    private lateinit var SUT: WeipassDetectCardServiceImpl

    @MockK
    lateinit var mockkedContext: Context

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        SUT = WeipassDetectCardServiceImpl(mockkedContext)
        mockBankCard()
        mockWeipassLogger()
    }

    @Test
    fun detectCard_WhenDetectCardRequestHasEmptySlotsToOpenList_ReturnsFailureDetectCardResponse() {
        //Setup
        val request = DetectCardRequest(emptyList(), null, "empty")

        //Run
        val response = SUT.detectCard(request)

        //verify
        assertTrue(request.slotsToOpen.isEmpty())
        assertEquals(Result.FAILURE, response.result)
        assertEquals(SLOT_OPEN_EMPTY, response.message)
        assertEquals(CardType.MANUAL, response.cardDetected)

        //Finally
        clearAllMocks()
    }

    @Test
    fun detectCard_WhenSlotsToOpenIsMagneticOnlyAndSdkCardReadApiResultIsZeroAndOutDataResultsInMagneticCardDetected_ReturnsSuccessDetectCardResponse() {
        //Setup
        val slotsToOpen = listOf(CardType.MAG)
        val request = mockDetectCardRequest(slotsToOpen)
        val cardTypeArgument = slot<Byte>()
        val cardModeArgument = slot<Int>()
        val timeoutArgument = slot<Int>()
        val outDataArgument = slot<ByteArray>()
        val outDataLenArgument = slot<IntArray>()
        val packageNameArgument = slot<String>()
        every {
            SDKInstance.mBankCard.readCard(
                capture(cardTypeArgument),
                capture(cardModeArgument),
                capture(timeoutArgument),
                capture(outDataArgument),
                capture(outDataLenArgument),
                capture(packageNameArgument)
            )
        } answers {
            outDataArgument.captured[0] = BankCard.CARD_READ_MAGENC
            0
        }

        //Run
        val response = SUT.detectCard(request)

        //verify
        //TODO MORE
        assertFalse(request.slotsToOpen.isEmpty())
        assertEquals(request.slotsToOpen.get(0), CardType.MAG)
        verify { SDKInstance.mBankCard.readCard(any(), any(), any(), any(), any(), any()) }
        assertTrue(cardTypeArgument.isCaptured)
        assertTrue(cardModeArgument.isCaptured)
        assertTrue(timeoutArgument.isCaptured)
        assertTrue(packageNameArgument.isCaptured)
        assertEquals(BankCard.CARD_MODE_MAG, cardModeArgument.captured)
        assertEquals(BankCard.CARD_TYPE_NORMAL, cardTypeArgument.captured)
        assertEquals(request.packageName, packageNameArgument.captured)
        assertEquals(BankCard.CARD_READ_MAGENC, outDataArgument.captured[0])
        assertEquals(Result.SUCCESS, response.result)
        assertEquals(CardType.MAG, response.cardDetected)

        //Finally
        clearAllMocks()
    }

    @Test
    fun detectCard_WhenSlotsToOpenIsContactOnlyAndSdkCardReadApiResultIsZeroAndOutDataResultsInContactCardDetected_ReturnsSuccessDetectCardResponse() {
        //Setup
        mockkDeviceUtils()
        val slotsToOpen = listOf(CardType.ICC)
        val request = mockDetectCardRequest(slotsToOpen)
        val cardTypeArgument = slot<Byte>()
        val cardModeArgument = slot<Int>()
        val timeoutArgument = slot<Int>()
        val outDataArgument = slot<ByteArray>()
        val outDataLenArgument = slot<IntArray>()
        val packageNameArgument = slot<String>()
        every {
            SDKInstance.mBankCard.readCard(
                capture(cardTypeArgument),
                capture(cardModeArgument),
                capture(timeoutArgument),
                capture(outDataArgument),
                capture(outDataLenArgument),
                capture(packageNameArgument)
            )
        } answers {
            outDataArgument.captured[0] = BankCard.CARD_READ_ICDETACT
            0
        }

        //Run
        val response = SUT.detectCard(request)

        //verify
        //TODO MORE
        assertFalse(request.slotsToOpen.isEmpty())
        assertEquals(request.slotsToOpen.get(0), CardType.ICC)
        verify { SDKInstance.mBankCard.readCard(any(), any(), any(), any(), any(), any()) }
        assertTrue(cardTypeArgument.isCaptured)
        assertTrue(cardModeArgument.isCaptured)
        assertTrue(timeoutArgument.isCaptured)
        assertTrue(packageNameArgument.isCaptured)
        assertEquals(BankCard.CARD_READ_ICDETACT, outDataArgument.captured[0])
        assertEquals(BankCard.CARD_MODE_ICC, cardModeArgument.captured)
        assertEquals(BankCard.CARD_TYPE_NORMAL, cardTypeArgument.captured)
        assertEquals(request.packageName, packageNameArgument.captured)
        assertEquals(Result.SUCCESS, response.result)
        assertEquals(CardType.ICC, response.cardDetected)

        //Finally
        clearAllMocks()
    }

    @Test
    fun detectCard_WhenSlotsToOpenIsContactLessOnlyAndSdkCardReadApiResultIsZeroAndOutDataResultsInContactLessCardDetected_ReturnsSuccessDetectCardResponse() {
        //Setup
        val slotsToOpen = listOf(CardType.PICC)
        val request = mockDetectCardRequest(slotsToOpen)
        val cardTypeArgument = slot<Byte>()
        val cardModeArgument = slot<Int>()
        val timeoutArgument = slot<Int>()
        val outDataArgument = slot<ByteArray>()
        val outDataLenArgument = slot<IntArray>()
        val packageNameArgument = slot<String>()
        every {
            SDKInstance.mBankCard.readCard(
                capture(cardTypeArgument),
                capture(cardModeArgument),
                capture(timeoutArgument),
                capture(outDataArgument),
                capture(outDataLenArgument),
                capture(packageNameArgument)
            )
        } answers {
            outDataArgument.captured[0] = BankCard.CARD_READ_PICCDETACT
            0
        }

        //Run
        val response = SUT.detectCard(request)

        //verify
        //TODO MORE
        assertFalse(request.slotsToOpen.isEmpty())
        assertEquals(request.slotsToOpen.get(0), CardType.PICC)
        verify { SDKInstance.mBankCard.readCard(any(), any(), any(), any(), any(), any()) }
        assertTrue(cardTypeArgument.isCaptured)
        assertTrue(cardModeArgument.isCaptured)
        assertTrue(timeoutArgument.isCaptured)
        assertEquals(BankCard.CARD_READ_PICCDETACT, outDataArgument.captured[0])
        assertTrue(packageNameArgument.isCaptured)
        assertEquals(BankCard.CARD_MODE_NFC, cardModeArgument.captured)
        assertEquals(BankCard.CARD_TYPE_NORMAL, cardTypeArgument.captured)
        assertEquals(request.packageName, packageNameArgument.captured)
        assertEquals(Result.SUCCESS, response.result)
        assertEquals(CardType.PICC, response.cardDetected)

        //Finally
        clearAllMocks()
    }

    @Test
    fun detectCard_WhenSlotsToOpenContainsSlotTypeNone_ThrowsException() {
        //Setup
        val slotsToOpen = listOf(
            CardType.ICC, CardType.PICC,
            CardType.MANUAL
        )
        val request = mockDetectCardRequest(slotsToOpen)

        //Run and Verify Exception
        expectedException.expect(Exception::class.java)
        expectedException.expectMessage(NO_SUCH_SLOT_TO_OPEN)
        val response = SUT.detectCard(request)

        //Finally
        clearAllMocks()
    }

    @Test
    fun detectCard_WhenSlotsToOpenContainsMagneticAndContactlessExceptNoneAndSdkCardReadApiResultIsZeroAndOutDataResultsInContactLessCardDetected_ReturnsSuccessDetectCardResponse() {
        //Setup
        val slotsToOpen = listOf(CardType.MAG, CardType.ICC, CardType.PICC)
        val request = mockDetectCardRequest(slotsToOpen)
        val cardTypeArgument = slot<Byte>()
        val cardModeArgument = slot<Int>()
        val timeoutArgument = slot<Int>()
        val outDataArgument = slot<ByteArray>()
        val outDataLenArgument = slot<IntArray>()
        val packageNameArgument = slot<String>()
        every {
            SDKInstance.mBankCard.readCard(
                capture(cardTypeArgument),
                capture(cardModeArgument),
                capture(timeoutArgument),
                capture(outDataArgument),
                capture(outDataLenArgument),
                capture(packageNameArgument)
            )
        } answers {
            outDataArgument.captured[0] = BankCard.CARD_READ_PICCDETACT
            0
        }

        //Run
        val response = SUT.detectCard(request)

        //verify
        assertFalse(request.slotsToOpen.isEmpty())
        assertTrue(request.slotsToOpen.contains(CardType.MAG))
        assertTrue(request.slotsToOpen.contains(CardType.PICC))
        verify { SDKInstance.mBankCard.readCard(any(), any(), any(), any(), any(), any()) }
        assertTrue(cardTypeArgument.isCaptured)
        assertTrue(cardModeArgument.isCaptured)
        assertTrue(timeoutArgument.isCaptured)
        assertEquals(BankCard.CARD_READ_PICCDETACT, outDataArgument.captured[0])
        assertTrue(packageNameArgument.isCaptured)
        assertTrue(
            cardModeArgument.captured == (BankCard.CARD_MODE_MAG
                    or
                    BankCard.CARD_MODE_ICC
                    or
                    BankCard.CARD_MODE_NFC)
        )
        assertEquals(CARD_TYPE_MAG_PRIOR_TO_CL.toByte(), cardTypeArgument.captured)
        assertEquals(request.packageName, packageNameArgument.captured)
        assertEquals(Result.SUCCESS, response.result)
        assertEquals(CardType.PICC, response.cardDetected)

        //Finally
        clearAllMocks()
    }

    @Test
    fun detectCard_WhenSlotsToOpenIsAllExceptSlotTypeNoneAndSdkCardReadApiResultIsZeroAndOutDataResultsInDetectError1_ReturnsFailureDetectCardResponse() {
        //Setup
        val slotsToOpen = listOf(CardType.MAG, CardType.ICC, CardType.PICC)
        val request = mockDetectCardRequest(slotsToOpen)
        val cardTypeArgument = slot<Byte>()
        val cardModeArgument = slot<Int>()
        val timeoutArgument = slot<Int>()
        val outDataArgument = slot<ByteArray>()
        val outDataLenArgument = slot<IntArray>()
        val packageNameArgument = slot<String>()
        every {
            SDKInstance.mBankCard.readCard(
                capture(cardTypeArgument),
                capture(cardModeArgument),
                capture(timeoutArgument),
                capture(outDataArgument),
                capture(outDataLenArgument),
                capture(packageNameArgument)
            )
        } answers {
            outDataArgument.captured[0] = BankCard.CARD_READ_FAIL
            0
        }

        //Run
        val response = SUT.detectCard(request)

        //verify
        assertFalse(request.slotsToOpen.isEmpty())
        assertTrue(request.slotsToOpen.contains(CardType.MAG))
        assertTrue(request.slotsToOpen.contains(CardType.ICC))
        assertTrue(request.slotsToOpen.contains(CardType.PICC))
        verify { SDKInstance.mBankCard.readCard(any(), any(), any(), any(), any(), any()) }
        assertTrue(cardTypeArgument.isCaptured)
        assertTrue(cardModeArgument.isCaptured)
        assertTrue(timeoutArgument.isCaptured)
        assertEquals(BankCard.CARD_READ_FAIL, outDataArgument.captured[0])
        assertTrue(packageNameArgument.isCaptured)
        assertTrue(
            cardModeArgument.captured == (BankCard.CARD_MODE_MAG
                    or
                    BankCard.CARD_MODE_ICC
                    or
                    BankCard.CARD_MODE_NFC)
        )
        assertEquals(CARD_TYPE_MAG_PRIOR_TO_CL.toByte(), cardTypeArgument.captured)
        assertEquals(request.packageName, packageNameArgument.captured)
        assertEquals(Result.FAILURE, response.result)
        assertEquals(CardType.MANUAL, response.cardDetected)
        assertEquals(DETECT_ERROR_CASE_1, response.message)

        //Finally
        clearAllMocks()
    }

    @Test
    fun detectCard_WhenSlotsToOpenIsAllExceptSlotTypeNoneAndSdkCardReadApiResultIsZeroAndOutDataResultsInDetectError2_ReturnsFailureDetectCardResponse() {
        //Setup
        val slotsToOpen = listOf(CardType.MAG, CardType.ICC, CardType.PICC)
        val request = mockDetectCardRequest(slotsToOpen)
        val cardTypeArgument = slot<Byte>()
        val cardModeArgument = slot<Int>()
        val timeoutArgument = slot<Int>()
        val outDataArgument = slot<ByteArray>()
        val outDataLenArgument = slot<IntArray>()
        val packageNameArgument = slot<String>()
        every {
            SDKInstance.mBankCard.readCard(
                capture(cardTypeArgument),
                capture(cardModeArgument),
                capture(timeoutArgument),
                capture(outDataArgument),
                capture(outDataLenArgument),
                capture(packageNameArgument)
            )
        } answers {
            outDataArgument.captured[0] = BankCard.CARD_READ_MAGENCFAIL
            0
        }

        //Run
        val response = SUT.detectCard(request)

        //verify
        assertFalse(request.slotsToOpen.isEmpty())
        assertTrue(request.slotsToOpen.contains(CardType.MAG))
        assertTrue(request.slotsToOpen.contains(CardType.ICC))
        assertTrue(request.slotsToOpen.contains(CardType.PICC))
        verify { SDKInstance.mBankCard.readCard(any(), any(), any(), any(), any(), any()) }
        assertTrue(cardTypeArgument.isCaptured)
        assertTrue(cardModeArgument.isCaptured)
        assertTrue(timeoutArgument.isCaptured)
        assertEquals(BankCard.CARD_READ_MAGENCFAIL, outDataArgument.captured[0])
        assertTrue(packageNameArgument.isCaptured)
        assertTrue(
            cardModeArgument.captured == (BankCard.CARD_MODE_MAG
                    or
                    BankCard.CARD_MODE_ICC
                    or
                    BankCard.CARD_MODE_NFC)
        )
        assertEquals(CARD_TYPE_MAG_PRIOR_TO_CL.toByte(), cardTypeArgument.captured)
        assertEquals(request.packageName, packageNameArgument.captured)
        assertEquals(Result.FAILURE, response.result)
        assertEquals(CardType.MANUAL, response.cardDetected)
        assertEquals(DETECT_ERROR_CASE_2, response.message)

        //Finally
        clearAllMocks()
    }

    @Test
    fun detectCard_WhenSlotsToOpenIsAllExceptSlotTypeNoneAndSdkCardReadApiResultIsZeroAndOutDataResultsInDetectTimeout_ReturnsFailureDetectCardResponse() {
        //Setup
        val slotsToOpen = listOf(CardType.MAG, CardType.ICC, CardType.PICC)
        val request = mockDetectCardRequest(slotsToOpen)
        val cardTypeArgument = slot<Byte>()
        val cardModeArgument = slot<Int>()
        val timeoutArgument = slot<Int>()
        val outDataArgument = slot<ByteArray>()
        val outDataLenArgument = slot<IntArray>()
        val packageNameArgument = slot<String>()
        every {
            SDKInstance.mBankCard.readCard(
                capture(cardTypeArgument),
                capture(cardModeArgument),
                capture(timeoutArgument),
                capture(outDataArgument),
                capture(outDataLenArgument),
                capture(packageNameArgument)
            )
        } answers {
            outDataArgument.captured[0] = BankCard.CARD_READ_TIMEOUT
            0
        }

        //Run
        val response = SUT.detectCard(request)

        //verify
        assertFalse(request.slotsToOpen.isEmpty())
        assertTrue(request.slotsToOpen.contains(CardType.MAG))
        assertTrue(request.slotsToOpen.contains(CardType.ICC))
        assertTrue(request.slotsToOpen.contains(CardType.PICC))
        verify { SDKInstance.mBankCard.readCard(any(), any(), any(), any(), any(), any()) }
        assertTrue(cardTypeArgument.isCaptured)
        assertTrue(cardModeArgument.isCaptured)
        assertTrue(timeoutArgument.isCaptured)
        assertEquals(BankCard.CARD_READ_TIMEOUT, outDataArgument.captured[0])
        assertTrue(packageNameArgument.isCaptured)
        assertTrue(
            cardModeArgument.captured == (BankCard.CARD_MODE_MAG
                    or
                    BankCard.CARD_MODE_ICC
                    or
                    BankCard.CARD_MODE_NFC)
        )
        assertEquals(CARD_TYPE_MAG_PRIOR_TO_CL.toByte(), cardTypeArgument.captured)
        assertEquals(request.packageName, packageNameArgument.captured)
        assertEquals(Result.FAILURE, response.result)
        assertEquals(CardType.MANUAL, response.cardDetected)
        assertEquals(DETECT_CARD_TIMEOUT, response.message)

        //Finally
        clearAllMocks()
    }

    @Test
    fun detectCard_WhenSlotsToOpenIsAllExceptSlotTypeNoneAndSdkCardReadApiResultIsZeroAndOutDataResultsInDetectCancel_ReturnsFailureDetectCardResponse() {
        //Setup
        val slotsToOpen = listOf(CardType.MAG, CardType.ICC, CardType.PICC)
        val request = mockDetectCardRequest(slotsToOpen)
        val cardTypeArgument = slot<Byte>()
        val cardModeArgument = slot<Int>()
        val timeoutArgument = slot<Int>()
        val outDataArgument = slot<ByteArray>()
        val outDataLenArgument = slot<IntArray>()
        val packageNameArgument = slot<String>()
        every {
            SDKInstance.mBankCard.readCard(
                capture(cardTypeArgument),
                capture(cardModeArgument),
                capture(timeoutArgument),
                capture(outDataArgument),
                capture(outDataLenArgument),
                capture(packageNameArgument)
            )
        } answers {
            outDataArgument.captured[0] = BankCard.CARD_READ_CANCELED
            0
        }

        //Run
        val response = SUT.detectCard(request)

        //verify
        assertFalse(request.slotsToOpen.isEmpty())
        assertTrue(request.slotsToOpen.contains(CardType.MAG))
        assertTrue(request.slotsToOpen.contains(CardType.ICC))
        assertTrue(request.slotsToOpen.contains(CardType.PICC))
        verify { SDKInstance.mBankCard.readCard(any(), any(), any(), any(), any(), any()) }
        assertTrue(cardTypeArgument.isCaptured)
        assertTrue(cardModeArgument.isCaptured)
        assertTrue(timeoutArgument.isCaptured)
        assertEquals(BankCard.CARD_READ_CANCELED, outDataArgument.captured[0])
        assertTrue(packageNameArgument.isCaptured)
        assertTrue(
            cardModeArgument.captured == (BankCard.CARD_MODE_MAG
                    or
                    BankCard.CARD_MODE_ICC
                    or
                    BankCard.CARD_MODE_NFC)
        )
        assertEquals(CARD_TYPE_MAG_PRIOR_TO_CL.toByte(), cardTypeArgument.captured)
        assertEquals(request.packageName, packageNameArgument.captured)
        assertEquals(Result.FAILURE, response.result)
        assertEquals(CardType.MANUAL, response.cardDetected)
        assertEquals(DETECT_CARD_CANCEL, response.message)

        //Finally
        clearAllMocks()
    }

    @Test
    fun detectCard_WhenSlotToOpenIsAnyExceptNoneAndSdkCardReadApiThrowsException_ReturnsFailureDetectResponse() {
        //Setup
        val slotsToOpen = listOf(CardType.MAG)
        val request = mockDetectCardRequest(slotsToOpen)
        every {
            SDKInstance.mBankCard.readCard(any(), any(), any(), any(), any(), any())
        } throws RemoteException()

        //Run
        val response = SUT.detectCard(request)

        //Verify
        assertEquals(Result.FAILURE, response.result)
        assertEquals(NO_CARD_DETECTED, response.message)
        assertEquals(CardType.MANUAL, response.cardDetected)
        verify { SDKInstance.mBankCard.readCard(any(), any(), any(), any(), any(), any()) }

        //Finally
        clearAllMocks()
    }

    @Test
    fun detectCard_WhenSlotToOpenIsAnyExceptNoneAndSdkCardReadApiResultIsNonZero_ReturnsFailureDetectResponse() {
        //Setup
        val slotsToOpen = listOf(CardType.MAG)
        val request = mockDetectCardRequest(slotsToOpen)
        every {
            SDKInstance.mBankCard.readCard(any(), any(), any(), any(), any(), any())
        } returns -1

        //Run
        val response = SUT.detectCard(request)

        //Verify
        assertEquals(Result.FAILURE, response.result)
        assertEquals("No Card Detected", response.message)
        assertEquals(CardType.MANUAL, response.cardDetected)
        verify { SDKInstance.mBankCard.readCard(any(), any(), any(), any(), any(), any()) }

        //Finally
        clearAllMocks()
    }

    @Test
    fun detectCard_WhenSlotToOpenIsAnyExceptNoneAndSdkCardReadApiResultIsNotZeroAndOutDataResultsCardDetectError1WithSwipeAgain_ReturnsSwipeAgainDetectCardResponse() {
        //Setup
        mockByteUtil()
        val slotsToOpen = listOf(CardType.MAG, CardType.ICC, CardType.PICC)
        val request = mockDetectCardRequest(slotsToOpen)
        val outDataArg = slot<ByteArray>()
        every {
            SDKInstance.mBankCard.readCard(any(), any(), any(), capture(outDataArg), any(), any())
        } answers {
            outDataArg.captured[0] = 0x01
            0
        }

        every { ByteUtil.bytes2HexString(any()) } returns "01800000"

        //Run
        val response = SUT.detectCard(request)

        //Verify
        assertTrue(request.slotsToOpen.contains(CardType.MAG))
        assertTrue(request.slotsToOpen.contains(CardType.ICC))
        assertTrue(request.slotsToOpen.contains(CardType.PICC))
        verify { ByteUtil.bytes2HexString(any()) }
        assertEquals("80", ByteUtil.bytes2HexString(outDataArg.captured).substring(2, 4))
        assertEquals(Result.SWIPE_AGAIN, response.result)
        assertEquals(CardType.MAG, response.cardDetected)

        //Finally
        clearAllMocks()
    }

    @Test
    fun detectCard_WhenSlotToOpenIsAnyExceptNoneAndSdkCardReadApiResultIsNotZeroAndOutDataResultsCardDetectError2WithSwipeAgain_ReturnsSwipeAgainDetectCardResponse() {
        //Setup
        mockByteUtil()
        val slotsToOpen = listOf(CardType.MAG, CardType.ICC, CardType.PICC)
        val request = mockDetectCardRequest(slotsToOpen)
        val outDataArg = slot<ByteArray>()
        every {
            SDKInstance.mBankCard.readCard(any(), any(), any(), capture(outDataArg), any(), any())
        } answers {
            outDataArg.captured[0] = 0x02
            0
        }

        every { ByteUtil.bytes2HexString(any()) } returns "02800000"

        //Run
        val response = SUT.detectCard(request)

        //Verify
        assertTrue(request.slotsToOpen.contains(CardType.MAG))
        assertTrue(request.slotsToOpen.contains(CardType.ICC))
        assertTrue(request.slotsToOpen.contains(CardType.PICC))
        verify { ByteUtil.bytes2HexString(any()) }
        assertEquals("80", ByteUtil.bytes2HexString(outDataArg.captured).substring(2, 4))
        assertEquals(Result.SWIPE_AGAIN, response.result)
        assertEquals(CardType.MAG, response.cardDetected)

        //Finally
        clearAllMocks()
    }

    fun mockBankCard() {
        mockkStatic(SDKInstance::class)
        val mockedBankCard = mockk<BankCard>()
        SDKInstance.mBankCard = mockedBankCard
    }

    fun mockDetectCardRequest(slotsToOpen: List<CardType>): DetectCardRequest {
        return DetectCardRequest(slotsToOpen, DEFAULT_CARD_READ_TIMEOUT, "test")
    }

    fun mockWeipassLogger() {
        mockkObject(WeipassLogger::class)
        mockkObject(WeipassLogger.Companion)
        every { WeipassLogger.log(any()) } just Runs
    }

    fun mockByteUtil() {
        mockkStatic(ByteUtil::class)
    }

    fun mockkDeviceUtils() {
        mockkObject(WeipassDeviceUtils)
        mockkObject(WeipassDeviceUtils.Companion)
    }
}