package global.citytech.finpos.nibl.iso8583.requestsender.batchupload;

import org.junit.Test;

import global.citytech.finpos.nibl.NIBLHostConfig;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.AppState;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Unique Shakya on 5/11/2021.
 */
public class S2MBatchUploadRequestSenderTest {

    @Test
    public void executeTest() {
        AppState.getInstance().setBuildType(AppState.BuildType.DEBUG);
        try {
            String terminalId = "99991002";
            String acquirerId = "405635000000002";
            TerminalInfo terminalInfo = new TerminalInfo(terminalId, acquirerId, "", "Citytech", "Kamaladi");
            HostInfo primaryHost = NIBLHostConfig.getPrimaryConfig();
            HostInfo secondaryHost = NIBLHostConfig.getSecondaryConfig();
            ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);
            RequestContext context =
                    new RequestContext(
                            "000405", terminalInfo, connectionParam);
            context.setRequest(S2MBatchUploadRequest.Builder.newInstance()
                    .pan("4249720020000234")
                    .originalProcessingCode("000000")
                    .transactionAmount(100)
                    .localTime("142322")
                    .localDate("0404")
                    .expirationDate("1222")
                    .posEntryMode(PosEntryMode.ICC)
                    .functionCode("0011")
                    .posConditionCode("00")
                    .originalRrn("100000000012")
                    .originalApprovalCode("123456")
                    .originalResponseCode("00")
                    .originalData("0200000403100000000012")
                    .invoiceNumber("100023")
                    .build());
            S2MBatchUploadRequestSender s2MBatchUploadRequestSender = new S2MBatchUploadRequestSender(new NIBLSpecInfo(),
                    context);
            IsoMessageResponse isoMessageResponse = s2MBatchUploadRequestSender.send();
            assertNotNull(isoMessageResponse);
        } catch (FinPosException fe) {
            fe.printStackTrace();
        }
    }
}
