package global.citytech.finpos.merchant.data.datasource.app.db.dao

import androidx.room.*
import global.citytech.finpos.merchant.domain.model.app.MessageCode

/**
 * Created by Rishav Chudal on 9/14/20.
 */
@Dao
interface MessageCodesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(messageCodes: List<MessageCode>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(messageCode: MessageCode): Long

    @Query("SELECT id, message_code, message_text FROM message_codes")
    fun getAllMessageCodes(): List<MessageCode>

    @Update
    fun update(messageCode: MessageCode): Int

    @Query("DELETE FROM message_codes WHERE message_code = :code")
    fun deleteByMessageCode(code: String): Int

    @Query("SELECT message_text FROM message_codes WHERE message_code = :code")
    fun getMessageTextByActionCode(code: String): String

    @Query("DELETE FROM message_codes")
    fun nukeTableData()
}