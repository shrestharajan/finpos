package global.citytech.finpos.merchant.presentation.merchant.password

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.easydroid.core.extension.showToast
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityChangePasswordBinding
import kotlinx.android.synthetic.main.activity_change_password.*

/**
 * Created by Unique Shakya on 8/2/2021.
 */
class ChangeMerchantPasswordActivity :
    AppBaseActivity<ActivityChangePasswordBinding, ChangeMerchantPasswordViewModel>(),
    TextWatcher {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        initObservers()
        addTextWatchers()
    }

    override fun getBindingVariable(): Int = BR.changePasswordViewModel

    override fun getLayout(): Int = R.layout.activity_change_password

    override fun getViewModel(): ChangeMerchantPasswordViewModel = ViewModelProviders
        .of(this)[ChangeMerchantPasswordViewModel::class.java]

    override fun onBackPressed() {
        // do nothing
    }

    private fun addTextWatchers() {
        text_input_et_current_password.addTextChangedListener(this)
        text_input_et_new_password.addTextChangedListener(this)
        text_input_et_confirm_password.addTextChangedListener(this)
    }

    private fun initObservers() {
        getViewModel().invalidCurrentPassword.observe(this, Observer {
            text_input_layout_current_password.boxStrokeColor =
                ContextCompat.getColor(this, R.color.red)
            text_input_et_current_password.requestFocus()
            showToast(it)
        })

        getViewModel().invalidNewPassword.observe(this, Observer {
            text_input_layout_new_password.boxStrokeColor =
                ContextCompat.getColor(this, R.color.red)
            text_input_et_new_password.requestFocus()
            showToast(it)
        })

        getViewModel().invalidConfirmPassword.observe(this, Observer {
            text_input_layout_confirm_password.boxStrokeColor =
                ContextCompat.getColor(this, R.color.red)
            text_input_et_confirm_password.requestFocus()
            showToast(it)
        })

        getViewModel().passwordChangeSuccess.observe(this, Observer {
            showToast(getString(R.string.msg_success_change_password))
            finish()
        })
    }

    private fun initViews() {
        btn_confirm.setOnClickListener {
            getViewModel().changePassword(
                text_input_et_current_password.text.toString(),
                text_input_et_new_password.text.toString(),
                text_input_et_confirm_password.text.toString()
            )
        }

        btn_cancel.setOnClickListener {
            finish()
        }

        iv_back.setOnClickListener {
            finish()
        }
    }

    override fun afterTextChanged(p0: Editable?) {
        // do nothing
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        // do nothing
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        text_input_layout_current_password.boxStrokeColor =
            ContextCompat.getColor(this, R.color.colorPrimary)
        text_input_layout_new_password.boxStrokeColor =
            ContextCompat.getColor(this, R.color.colorPrimary)
        text_input_layout_confirm_password.boxStrokeColor =
            ContextCompat.getColor(this, R.color.colorPrimary)
    }

    override fun onDestroy() {
        super.onDestroy()
        text_input_et_confirm_password.removeTextChangedListener(this)
    }
}