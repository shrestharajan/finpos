package global.citytech.finpos.nibl.iso8583.requestsender.settlement;

import global.citytech.finposframework.usecases.reconciliation.ReconciliationTotals;
import global.citytech.finposframework.usecases.transaction.Request;

/**
 * Created by Unique Shakya on 9/22/2020.
 */
public class SettlementRequest implements Request {

    private String batchNumber;
    private String applicationVersion;
    private ReconciliationTotals reconciliationTotals;

    public String getBatchNumber() {
        return batchNumber;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public ReconciliationTotals getReconciliationTotals() {
        return reconciliationTotals;
    }

    public static final class Builder {
        private String batchNumber;
        private String applicationVersion;
        private ReconciliationTotals reconciliationTotals;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withBatchNumber(String batchNumber) {
            this.batchNumber = batchNumber;
            return this;
        }

        public Builder withApplicationVersion(String applicationVersion) {
            this.applicationVersion = applicationVersion;
            return this;
        }

        public Builder withReconciliationTotals(ReconciliationTotals reconciliationTotals) {
            this.reconciliationTotals = reconciliationTotals;
            return this;
        }

        public SettlementRequest build() {
            SettlementRequest settlementRequest = new SettlementRequest();
            settlementRequest.batchNumber = this.batchNumber;
            settlementRequest.applicationVersion = this.applicationVersion;
            settlementRequest.reconciliationTotals = this.reconciliationTotals;
            return settlementRequest;
        }
    }
}
