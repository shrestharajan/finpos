package global.citytech.finpos.processor.neps.reversal;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.SmartVistaActionCode;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.voidsale.VoidSaleIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.voidsale.VoidSaleRequestSender;
import global.citytech.finpos.processor.neps.transaction.NepsIsoMessageReceiptHandler;
import global.citytech.finpos.processor.neps.transaction.TransactionExecutorTemplate;
import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finpos.processor.neps.transaction.TransactionUtils;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.TransactionStatusFromHost;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.activity.ActivityLog;
import global.citytech.finposframework.usecases.activity.ActivityLogStatus;
import global.citytech.finposframework.usecases.activity.ActivityLogType;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.AmountUtils;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StackTraceUtil;
import global.citytech.finposframework.utility.StringUtils;

public class ReversalUseCase extends TransactionExecutorTemplate {

    private ReversalRequest reversalRequest;
    private final VoidSaleIsoRequest voidSaleIsoRequest;

    public ReversalUseCase(TerminalRepository terminalRepository, VoidSaleIsoRequest voidSaleIsoRequest, Notifier notifier) {
        super(terminalRepository, notifier);
        this.voidSaleIsoRequest = voidSaleIsoRequest;
    }

    @Override
    public TransactionResponse execute(global.citytech.finpos.processor.neps.transaction.TransactionRequest request) {
        try {
            this.reversalRequest = (ReversalRequest) request;
            this.stan = reversalRequest.getStan();
            this.currencyCode = reversalRequest.getCurrencyCode();
            this.receiptLog = reversalRequest.getReceiptLog();
            this.transactionRequest = request.getTransactionRequest();
            this.notifier.notify(
                    Notifier.EventType.PERFORMING_REVERSAL,
                    Notifier.EventType.PERFORMING_REVERSAL.getDescription()
            );

            IsoMessageResponse isoMessageResponse = sendIsoMessage();
            printDebugReceiptIfEnabled(request, isoMessageResponse);
            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait..");

            boolean isReversalApproved = this.transactionApprovedByActionCode(isoMessageResponse);
            this.notifyTransactionCompletion(isReversalApproved, request.getTransactionRequest(), reversalRequest, isoMessageResponse, readCardResponse);
            this.notifier.notify(Notifier.EventType.REVERSAL_COMPLETED,
                    Notifier.EventType.REVERSAL_COMPLETED.getDescription());

            String message = retrieveMessage(isoMessageResponse, isReversalApproved, request.getApplicationRepository());
            this.notifier.notify(Notifier.EventType.PUSH_MERCHANT_LOG,
                    this.stan, message, isReversalApproved);

            updateActivityLog(isoMessageResponse, isReversalApproved);
            return this.prepareResponse(isoMessageResponse, isReversalApproved,
                    request.getApplicationRepository(), false);
        } catch (FinPosException fe) {
            if (!fe.getMessage().equals(FinPosException.ExceptionType.UNABLE_TO_CONNECT.getMessage()) &&
                    !fe.getMessage().equals(FinPosException.ExceptionType.READ_TIME_OUT.getMessage())) {
                updateActivityLog(fe);
            }
            throw fe;
        } catch (Exception e) {
            updateActivityLog(StackTraceUtil.getStackTraceAsString(e));
            throw e;
        }
    }

    protected boolean transactionApprovedByActionCode(IsoMessageResponse isoMessageResponse) {
        int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, 39);
        //return (SmartVistaActionCode.getByActionCode(actionCode).getTransactionStatusFromHost() == TransactionStatusFromHost.ONLINE_APPROVED);
        return (actionCode == 000 || actionCode == 914);
    }

    private void updateActivityLog(FinPosException fe) {
        try {
            TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
            BigDecimal totalAmount = transactionRequest.getAmount().add(transactionRequest.getAdditionalAmount());
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.stan)
                    .withType(ActivityLogType.REVERSAL)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(this.terminalRepository.getReconciliationBatchNumber())
                    .withAmount(AmountUtils.toLowerCurrencyFromBigDecimalInLong(totalAmount))
                    .withResponseCode("")
                    .withIsoRequest(fe.getIsoRequest())
                    .withIsoResponse("")
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(fe.getLocalizedMessage() + " : " + fe.getDetailMessage())
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(StringUtils.prepareAdditionalDataForActivityLog(this.terminalRepository.getTerminalSerialNumber()))
                    .build();
            this.terminalRepository.updateActivityLog(activityLog);
        } catch (Exception ex) {
            this.logger.log("Unable to update REVERSAL activity log :: " + ex.getMessage());
        }
    }

    private void updateActivityLog(IsoMessageResponse isoMessageResponse, boolean isReversalApproved) {
        try {
            TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
            String responseCode = IsoMessageUtils.retrieveFromDataElementsAsString(
                    isoMessageResponse,
                    DataElement.RESPONSE_CODE
            );
            BigDecimal totalAmount = transactionRequest.getAmount().add(transactionRequest.getAdditionalAmount());
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.stan)
                    .withType(ActivityLogType.REVERSAL)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(this.terminalRepository.getReconciliationBatchNumber())
                    .withAmount(AmountUtils.toLowerCurrencyFromBigDecimalInLong(totalAmount))
                    .withResponseCode(responseCode)
                    .withIsoRequest("")
                    .withIsoResponse(isoMessageResponse.getDebugResponseString())
                    .withStatus(isReversalApproved ? ActivityLogStatus.SUCCESS : ActivityLogStatus.FAILED)
                    .withRemarks("")
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(StringUtils.prepareAdditionalDataForActivityLog(this.terminalRepository.getTerminalSerialNumber()))
                    .build();
            this.terminalRepository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update REVERSAL activity log :: " + e.getMessage());
        }
    }

    private void updateActivityLog(String remarks) {
        try {
            TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
            BigDecimal totalAmount = transactionRequest.getAmount().add(transactionRequest.getAdditionalAmount());
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.stan)
                    .withType(ActivityLogType.REVERSAL)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(this.terminalRepository.getReconciliationBatchNumber())
                    .withAmount(AmountUtils.toLowerCurrencyFromBigDecimalInLong(totalAmount))
                    .withResponseCode("")
                    .withIsoRequest("")
                    .withIsoResponse("")
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(remarks)
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(StringUtils.prepareAdditionalDataForActivityLog(this.terminalRepository.getTerminalSerialNumber()))
                    .build();
            this.terminalRepository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update REVERSAL activity log :: " + e.getMessage());
        }
    }

    private void printDebugReceiptIfEnabled(global.citytech.finpos.processor.neps.transaction.TransactionRequest request, IsoMessageResponse isoMessageResponse) {
        boolean isDebugVersion = this.terminalRepository.findTerminalInfo().isDebugModeEnabled();
        if (isDebugVersion) {
            ReceiptHandler.IsoMessageReceiptHandler isoMessageReceiptHandler
                    = new NepsIsoMessageReceiptHandler(request.getPrinterService());
            isoMessageReceiptHandler.printIsoMessageReceipt(isoMessageResponse);
        }
    }

    @Override
    protected void retrieveCardResponse() {
        // DO nothing
    }

    @Override
    protected void processCard() {
        //DO Nothing
    }

    @Override
    protected String getClassName() {
        return ReversalUseCase.class.getName();
    }

    @Override
    protected TransactionIsoRequest prepareIsoRequest(ReadCardResponse readCardResponse, TransactionRequest transactionRequest) {
        return VoidSaleIsoRequest.Builder
                .newInstance()
                .processingCode(voidSaleIsoRequest.getProcessingCode())
                .pan(voidSaleIsoRequest.getPan())
                .transactionAmount(voidSaleIsoRequest.getTransactionAmount())
                .localDateTime(DateUtils.yyMMddHHmmssDate())
                .posEntryMode(voidSaleIsoRequest.getPosEntryMode())
                .posConditionCode(voidSaleIsoRequest.getPosConditionCode())
                .track2Data(voidSaleIsoRequest.getTrack2Data())
                .rrn(voidSaleIsoRequest.getRrn())
                .currencyCode(voidSaleIsoRequest.getCurrencyCode())
                .emvData(voidSaleIsoRequest.getEmvData())
                .build();
    }

    @Override
    protected NepsMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context) {
        return new VoidSaleRequestSender(context);
    }

    @Override
    protected void processOnlineResult(IsoMessageResponse isoMessageResponse) {

    }

    @Override
    protected void updateTransactionLog(boolean transactionApproved, TransactionRequest transactionRequest, TransactionRepository transactionRepository, IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse) {
        this.logger.debug("REVERSAL ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
        this.logger.debug("REVERSAL ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
        String timeStamp = IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.LOCAL_TIME);
        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
                .withStan(stan)
                .withInvoiceNumber(terminalRepository.getInvoiceNumber())
                .withRrn(
                        IsoMessageUtils.retrieveFromDataElementsAsString(
                                isoMessageResponse,
                                DataElement.RETRIEVAL_REFERENCE_NUMBER
                        )
                )
                .withAuthCode(
                        IsoMessageUtils.retrieveFromDataElementsAsString(
                                isoMessageResponse,
                                DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE
                        )
                )
                .withTransactionType(TransactionType.REVERSAL)
                .withOriginalTransactionType(
                        reversalRequest.getTransactionRequest().getTransactionType()
                )
                .withTransactionAmount(
                        reversalRequest.getTransactionRequest().getAmount()
                )
                .withOriginalTransactionAmount(
                        reversalRequest.getTransactionRequest().getAmount()
                )
                .withTransactionDate(TransactionUtils.getDateOrDefaultFromTimeStamp(timeStamp))
                .withTransactionTime(TransactionUtils.getTimeOrDefaultFromTimeStamp(timeStamp))
                .withTransactionStatus(transactionApproved ? "ACCEPTED" : "DECLINED")
                .withPosEntryMode(voidSaleIsoRequest.getPosEntryMode())
                .withOriginalPosEntryMode(voidSaleIsoRequest.getPosEntryMode())
                .withPosConditionCode(voidSaleIsoRequest.getPosConditionCode())
                .withOriginalPosConditionCode(voidSaleIsoRequest.getPosConditionCode())
                .withReconcileStatus("") //TODO
                .withReconcileTime("") //TODO
                .withReconcileDate("") //TODO
                .withReconcileBatchNo(batchNumber)
                .withAuthorizationCompleted(true)
                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                .withProcessingCode(getProcessingCode())
                .withReadCardResponse(reversalRequest.getReadCardresponse())
                .withCurrencyCode(this.currencyCode)
                .withReceiptLog(this.receiptLog)
                .withVatInfo(reversalRequest.getTransactionRequest().getVatInfo())
                .build();

        reversalRequest.getTransactionRepository().updateTransactionLog(transactionLog);
    }

    @Override
    protected void updateTransactionIdsAndNotifyTransactionStatus(boolean transactionApproved, IsoMessageResponse isoMessageResponse, global.citytech.finpos.processor.neps.transaction.TransactionRequest request, ReadCardResponse readCardResponse) {
        terminalRepository.incrementSystemTraceAuditNumber();
        terminalRepository.incrementRetrievalReferenceNumber();
    }

    @Override
    protected TransactionResponse prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository, boolean shouldPrint) {
        return ReversalResponse.Builder.newInstance()
                .withStan(stan)
                .withShouldPrintCustomerCopy(shouldPrint)
                .withApproved(transactionApproved)
                .withDebugRequestMessage(isoMessageResponse.getDebugRequestString())
                .withDebugResponseMessage(isoMessageResponse.getDebugResponseString())
                .withMessage(
                        this.retrieveMessage(
                                isoMessageResponse,
                                transactionApproved,
                                reversalRequest.getApplicationRepository()
                        )
                ).build();
    }

    @Override
    protected String getProcessingCode() {
        return reversalRequest.getProcessingCode();
    }

    @Override
    protected TransactionResponse prepareErrorResponse(String responseMessage, boolean shouldPrint) {
        return null;
    }

    @Override
    protected TransactionResponse promptToPerformIccTransaction() {
        return null;
    }
}
