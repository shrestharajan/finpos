package global.citytech.finpos.neps.iso8583.requestsender.refund;

import global.citytech.finpos.neps.iso8583.requestsender.cardtypesenders.ICCISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.purchases.PurchaseRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.CardTypeISOMessageUpdater;
import global.citytech.finposframework.iso8583.RequestContext;

/**
 * Created by Saurav Ghimire on 1/21/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class IccRefundRequestSender extends RefundRequestSender {

    public IccRefundRequestSender(RequestContext context) {
        super( context);
    }

    @Override
    protected String getClassName() {
        return IccRefundRequestSender.class.getName();
    }

    @Override
    protected CardTypeISOMessageUpdater getCardTypeSender() {
        return new ICCISOMessageUpdater();
    }


}
