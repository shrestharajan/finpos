//package global.citytech.finpos.processor.nibl.voidsale;
//
//import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
//import global.citytech.finpos.nibl.iso8583.ProcessingCode;
//import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
//import global.citytech.finpos.nibl.iso8583.requestsender.voidsale.NiblVoidSaleRequest;
//import global.citytech.finpos.nibl.iso8583.requestsender.voidsale.VoidSaleRequestSender;
//import global.citytech.finpos.processor.nibl.reversal.ReversalRequestModel;
//import global.citytech.finpos.processor.nibl.reversal.ReversalUseCase;
//import global.citytech.finpos.processor.nibl.transaction.NiblTransactionReceiptPrintHandler;
//import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
//import global.citytech.finpos.processor.nibl.transaction.ReversedTransactionReceiptHandler;
//import global.citytech.finpos.processor.nibl.transaction.TransactionUseCase;
//import global.citytech.finpos.processor.nibl.transaction.TransactionUtils;
//import global.citytech.finposframework.exceptions.FinPosException;
//import global.citytech.finposframework.exceptions.PosError;
//import global.citytech.finposframework.exceptions.PosException;
//import global.citytech.finposframework.hardware.io.cards.CardDetails;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
//import global.citytech.finposframework.hardware.io.led.LedAction;
//import global.citytech.finposframework.hardware.io.led.LedLight;
//import global.citytech.finposframework.hardware.io.led.LedRequest;
//import global.citytech.finposframework.hardware.io.led.LedService;
//import global.citytech.finposframework.iso8583.DataElement;
//import global.citytech.finposframework.iso8583.IsoMessageResponse;
//import global.citytech.finposframework.iso8583.RequestContext;
//import global.citytech.finposframework.notifier.Notifier;
//import global.citytech.finposframework.repositories.ApplicationRepository;
//import global.citytech.finposframework.repositories.TerminalRepository;
//import global.citytech.finposframework.repositories.TransactionRepository;
//import global.citytech.finposframework.supports.UseCase;
//import global.citytech.finposframework.switches.voidsale.VoidSaleRequester;
//import global.citytech.finposframework.usecases.MagneticSwipe;
//import global.citytech.finposframework.usecases.PICCWave;
//import global.citytech.finposframework.usecases.TransactionType;
//import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
//import global.citytech.finposframework.usecases.transaction.TransactionLog;
//import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
//import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
//import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
//import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
//import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
//import global.citytech.finposframework.utility.DateUtils;
//import global.citytech.finposframework.utility.HelperUtils;
//import global.citytech.finposframework.utility.IsoMessageUtils;
//import global.citytech.finposframework.utility.JsonUtils;
//import global.citytech.finposframework.utility.PosResponse;
//import global.citytech.finposframework.utility.StringUtils;
//
///**
// * Created by Rishav Chudal on 9/17/20.
// */
//public class VoidSaleUseCase extends TransactionUseCase implements
//        UseCase<VoidSaleRequestModel, VoidSaleResponseModel>,
//        VoidSaleRequester<VoidSaleRequestModel, VoidSaleResponseModel> {
//
//    private VoidSaleRequestModel requestModel;
//    private TransactionLog transactionLogByInvoiceNumber;
//    private NiblVoidSaleRequest niblVoidSaleRequest;
//    private double voidAmount = 0.0;
//    private ReadCardResponse readCardResponse;
//
//    public VoidSaleUseCase(TerminalRepository terminalRepository, Notifier notifier) {
//        super(terminalRepository, notifier);
//    }
//
//    @Override
//    protected String getClassName() {
//        return VoidSaleUseCase.class.getName();
//    }
//
//    @Override
//    public VoidSaleResponseModel execute(VoidSaleRequestModel requestModel) {
//        this.requestModel = requestModel;
//        this.logger.log("::: VOID SALE USE CASE :::");
//        if (!isDeviceReady(requestModel.getDeviceController(),
//                requestModel.getTransactionRequest().getTransactionType())) {
//            throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
//        }
//        return initiateTransaction();
//    }
//
//    private PosResponse getDeviceStatus() {
//        return this.requestModel
//                .getDeviceController()
//                .isReady(TransactionUtils.getAllowedCardEntryModes(
//                        this.requestModel
//                                .getTransactionRequest()
//                                .getTransactionType()
//                        )
//                );
//    }
//
//    private VoidSaleResponseModel initiateTransaction() {
//        try {
//            String stan = this.terminalRepository.getSystemTraceAuditNumber();
//            String invoiceNumber = getInvoiceNumber();
//            String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
//            TransactionRequest transactionRequest = this.requestModel.getTransactionRequest();
//            this.transactionRepository = this.requestModel.getTransactionRepository();
//            this.initTransaction(this.requestModel.getApplicationRepository(), transactionRequest);
//            this.transactionLogByInvoiceNumber = getTransactionLogByInvoiceNumber(batchNumber, invoiceNumber);
//            if (this.transactionLogByInvoiceNumber.getTransactionType() == TransactionType.REVERSAL)
//                throw new PosException(PosError.DEVICE_ERROR_RECENT_TRANSACTION_CANNOT_VOID);
//            this.setVoidAmount(invoiceNumber, batchNumber);
//            this.setReadCardResponse();
//            this.notifier.notify(
//                    Notifier.EventType.TRANSMITTING_REQUEST,
//                    "Transmitting Request ..."
//            );
//            try {
//                IsoMessageResponse isoMessageResponse = sendIsoMessage(stan);
//                this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait...");
//                InvalidResponseHandlerResponse invalidResponseHandlerResponse
//                        = this.checkForInvalidResponseFromHost(isoMessageResponse);
//                if (invalidResponseHandlerResponse.isInvalid())
//                    return this.onHostInvalidResponse(transactionRequest,
//                            invalidResponseHandlerResponse, batchNumber, stan, invoiceNumber,
//                            requestModel);
//                boolean isVoidSaleApproved = isTransactionApprovedByActionCode(isoMessageResponse);
//                notifyTransactionCompletion(
//                        stan,
//                        batchNumber,
//                        invoiceNumber,
//                        isVoidSaleApproved,
//                        transactionRequest,
//                        this.requestModel.getTransactionRepository(),
//                        isoMessageResponse,
//                        this.requestModel.getApplicationRepository(),
//                        this.requestModel.getLedService()
//                );
//                boolean shouldPrint = printReceipt(
//                        batchNumber,
//                        stan,
//                        invoiceNumber,
//                        this.requestModel.getPrinterService(),
//                        transactionRequest,
//                        this.requestModel.getTransactionRepository(),
//                        this.readCardResponse,
//                        isoMessageResponse,
//                        this.requestModel.getApplicationRepository()
//                );
//                return prepareVoidSaleResponse(stan, isoMessageResponse, isVoidSaleApproved, shouldPrint);
//            } catch (FinPosException e) {
//                e.printStackTrace();
//                if (this.isResponseNotReceivedException(e)) {
//                    return this.onTransactionTimeout(
//                            transactionRequest,
//                            batchNumber,
//                            stan,
//                            invoiceNumber,
//                            requestModel
//                    );
//                } else if (e.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT) {
//                    logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED UNABLE TO CONNECT");
//                    transactionRepository.removeAutoReversal(this.autoReversal);
//                    throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
//                } else {
//                    throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//                }
//            }
//
//        } catch (PosException ex) {
//            ex.printStackTrace();
//            throw ex;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        } finally {
//            requestModel.getReadCardService().cleanUp();
//            requestModel.getLedService().doTurnLedWith(new LedRequest(LedLight.ALL, LedAction.OFF));
//            PICCWave.getInstance().clear();
//            MagneticSwipe.getInstance().clear();
//        }
//    }
//
//    private void setReadCardResponse() {
//        this.readCardResponse = this.transactionLogByInvoiceNumber.getReadCardResponse();
//        this.readCardResponse.getCardDetails().setAmount(this.voidAmount);
//        this.readCardResponse.getCardDetails().setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
//    }
//
//    private void setVoidAmount(String invoiceNumber, String batchNumber) {
//        if (this.transactionLogByInvoiceNumber.isTipAdjusted()) {
//            TransactionLog tipTransactionByInvoiceNumber = this.requestModel.getTransactionRepository()
//                    .getTipTransactionLogWithInvoiceNumber(invoiceNumber, batchNumber);
//            this.voidAmount = tipTransactionByInvoiceNumber.getTransactionAmount() +
//                    this.transactionLogByInvoiceNumber.getTransactionAmount();
//            logger.debug("::: VOID AMOUNT IF TIP ::: " + this.voidAmount);
//        } else {
//            this.voidAmount = this.transactionLogByInvoiceNumber.getTransactionAmount();
//            logger.debug("::: VOID AMOUNT IF NOT TIP ::: " + this.voidAmount);
//        }
//    }
//
//    private VoidSaleResponseModel onHostInvalidResponse(TransactionRequest transactionRequest,
//                                                        InvalidResponseHandlerResponse invalidResponseHandlerResponse,
//                                                        String batchNumber,
//                                                        String stan,
//                                                        String invoiceNumber,
//                                                        VoidSaleRequestModel requestModel) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, invalidResponseHandlerResponse.getMessage());
//        this.performReversal(stan, requestModel);
//        boolean shouldPrint = !this.approvePrintOnly(requestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(
//                    transactionRequest,
//                    this.readCardResponse,
//                    this.terminalRepository
//            )
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.INVALID_RESPONSE,
//                            invalidResponseHandlerResponse.getMessage()));
//            new NiblTransactionReceiptPrintHandler(requestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            requestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return VoidSaleResponseModel.Builder.newInstance()
//                .withApproved(false)
//                .withMessage(invalidResponseHandlerResponse.getMessage())
//                .withShouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private VoidSaleResponseModel onTransactionTimeout(
//            TransactionRequest transactionRequest,
//            String batchNumber,
//            String stan,
//            String invoiceNumber,
//            VoidSaleRequestModel requestModel
//    ) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, "TIMEOUT");
//        this.performReversal(stan, requestModel);
//        boolean shouldPrint = !this.approvePrintOnly(requestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(
//                    transactionRequest,
//                    this.readCardResponse,
//                    this.terminalRepository
//            )
//                    .prepare(
//                            batchNumber,
//                            stan,
//                            invoiceNumber,
//                            new ReasonForReversal(ReasonForReversal.Type.TIMEOUT, "Timeout")
//                    );
//            new NiblTransactionReceiptPrintHandler(requestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            requestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return VoidSaleResponseModel.Builder.newInstance()
//                .withMessage("TIMEOUT")
//                .withApproved(false)
//                .withShouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private PurchaseRequest preparePurchaseCardRequest(
//            TransactionRequest transactionRequest,
//            CardDetails cardDetails
//    ) {
//        PurchaseRequest purchaseRequest = new PurchaseRequest(
//                transactionRequest.getTransactionType(),
//                transactionRequest.getAmount(),
//                transactionRequest.getAdditionalAmount()
//        );
//        cardDetails.setAmount(transactionRequest.getAmount());
//        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
//        purchaseRequest.setCardDetails(cardDetails);
//        purchaseRequest.setFallbackIccToMag(transactionRequest.isFallbackIccToMag());
//        return purchaseRequest;
//    }
//
//    private IsoMessageResponse sendIsoMessage(String stan) {
//        RequestContext requestContext = this.prepareTemplateRequestContext(stan);
//        logger.debug(String.format(" Request Context ::%s", requestContext));
//        requestContext.setRequest(this.prepareVoidSaleRequest());
//        VoidSaleRequestSender requestSender = new VoidSaleRequestSender(
//                new NIBLSpecInfo(),
//                requestContext
//        );
//        this.autoReversal = new AutoReversal(
//                TransactionType.VOID,
//                JsonUtils.toJsonObj(prepareReversalRequest(stan, requestModel)),
//                0,
//                AutoReversal.Status.ACTIVE,
//                StringUtils.dateTimeStamp()
//        );
//        this.transactionRepository.saveAutoReversal(this.autoReversal);
//        IsoMessageResponse isoMessageResponse = requestSender.send();
//        this.transactionRepository.removeAutoReversal(this.autoReversal);
//        return isoMessageResponse;
//    }
//
//    private NiblVoidSaleRequest prepareVoidSaleRequest() {
//        this.niblVoidSaleRequest = new NiblVoidSaleRequest();
//        this.niblVoidSaleRequest.setPan(
//                this.transactionLogByInvoiceNumber
//                        .getReadCardResponse()
//                        .getCardDetails()
//                        .getPrimaryAccountNumber()
//        );
//        this.niblVoidSaleRequest.setTransactionAmount(
//                this.voidAmount
//        );
//        this.niblVoidSaleRequest.setExpireDate(
//                this.transactionLogByInvoiceNumber
//                        .getReadCardResponse()
//                        .getCardDetails()
//                        .getExpiryDate()
//        );
//        this.niblVoidSaleRequest.setEntryMode(this.transactionLogByInvoiceNumber.getPosEntryMode());
//        this.niblVoidSaleRequest.setCardSequenceNumber(
//                this.transactionLogByInvoiceNumber
//                        .getReadCardResponse()
//                        .getCardDetails()
//                        .getPrimaryAccountNumberSerialNumber()
//        );
//        this.niblVoidSaleRequest.setPosConditionCode(this.transactionLogByInvoiceNumber.getPosConditionCode());
//        this.niblVoidSaleRequest.setRetrievalReferenceNumber(this.transactionLogByInvoiceNumber.getRrn());
//        this.niblVoidSaleRequest.setApprovalCode(this.transactionLogByInvoiceNumber.getAuthCode());
//        this.niblVoidSaleRequest.setOriginalAmount(
//                this.voidAmount
//        );
//        this.niblVoidSaleRequest.setInvoiceNumber(
//                this.requestModel
//                        .getTransactionRequest()
//                        .getOriginalInvoiceNumber()
//        );
//        this.niblVoidSaleRequest.setLocalTime(HelperUtils.getDefaultLocaleTimeHhMmSs());
//        this.niblVoidSaleRequest.setLocalDate(HelperUtils.getDefaultLocaleDateWithoutYear());
//        return this.niblVoidSaleRequest;
//    }
//
//    private VoidSaleResponseModel prepareVoidSaleResponse(
//            String stan,
//            IsoMessageResponse response,
//            boolean isVoidSaleApproved,
//            boolean shouldPrint
//    ) {
//        return VoidSaleResponseModel.Builder.newInstance()
//                .withApproved(isVoidSaleApproved)
//                .withStan(stan)
//                .withDebugRequestMessage(response.getDebugRequestString())
//                .withDebugResponseMessage(response.getDebugResponseString())
//                .withMessage(retrieveMessage(response, isVoidSaleApproved, this.requestModel.getApplicationRepository()))
//                .withShouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private String getInvoiceNumber() {
//        if (this.requestModel != null) {
//            this.logger.log("::: ORIGINAL INVOICE NUMBER ::: " + this.requestModel.getTransactionRequest().getOriginalInvoiceNumber());
//            return this.requestModel.getTransactionRequest().getOriginalInvoiceNumber();
//        }
//        throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//    }
//
//    private TransactionLog getTransactionLogByInvoiceNumber(String batchNumber, String invoiceNumber) {
//        if (this.requestModel != null) {
//            TransactionLog transactionLog = this.requestModel
//                    .getTransactionRepository()
//                    .getTransactionLogWithInvoiceNumber(invoiceNumber, batchNumber);
//            if (transactionLog != null) {
//                return transactionLog;
//            }
//        }
//        throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//    }
//
//    private void performReversal(
//            String stan,
//            VoidSaleRequestModel voidSaleRequestModel
//    ) {
//        ReversalUseCase reversalUseCase = new ReversalUseCase(
//                this.prepareReversalRequest(stan, voidSaleRequestModel),
//                this.terminalRepository,
//                this.notifier
//        );
//        try {
//            reversalUseCase.execute(
//                    prepareReversalRequestModel(voidSaleRequestModel)
//            );
//            this.transactionRepository.removeAutoReversal(this.autoReversal);
//        } catch (FinPosException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private NiblReversalRequest prepareReversalRequest(
//            String stan,
//            VoidSaleRequestModel voidSaleRequestModel
//    ) {
//        NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
//        niblReversalRequest.setProcessingCode(ProcessingCode.VOID_SALE.getCode());
//        niblReversalRequest.setAmount(
//                this.niblVoidSaleRequest.getTransactionAmount()
//        );
//        niblReversalRequest.setPan(this.niblVoidSaleRequest.getPan());
//        niblReversalRequest.setExpiryDate(this.niblVoidSaleRequest.getExpireDate());
//        niblReversalRequest.setPosEntryMode(this.niblVoidSaleRequest.getEntryMode());
//        niblReversalRequest.setPosConditionCode(this.niblVoidSaleRequest.getPosConditionCode());
//        niblReversalRequest.setTrack2Data(this.readCardResponse.getCardDetails().getTrackTwoData());
//        niblReversalRequest.setPinBlock(this.readCardResponse.getCardDetails().getPinBlock().getPin());
//        niblReversalRequest.setInvoiceNumber(this.transactionLogByInvoiceNumber.getInvoiceNumber());
//        niblReversalRequest.setStan(stan);
//        return niblReversalRequest;
//    }
//
//    private ReversalRequestModel prepareReversalRequestModel(
//            VoidSaleRequestModel voidSaleRequestModel
//    ) {
//        return ReversalRequestModel.Builder.newInstance()
//                .withTransactionRequest(
//                        voidSaleRequestModel.getTransactionRequest()
//                )
//                .withTransactionRepository(
//                        voidSaleRequestModel.getTransactionRepository()
//                )
//                .withPrinterService(
//                        voidSaleRequestModel.getPrinterService()
//                )
//                .withApplicationRepository(
//                        voidSaleRequestModel.getApplicationRepository()
//                )
//                .withReadCardResponse(this.readCardResponse)
//                .build();
//    }
//
//    private void notifyTransactionCompletion(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean voidSaleApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ApplicationRepository applicationRepository,
//            LedService ledService
//    ) {
//        this.updateTransactionLog(
//                stan,
//                batchNumber,
//                invoiceNumber,
//                voidSaleApproved,
//                transactionRequest,
//                transactionRepository,
//                isoMessageResponse,
//                applicationRepository
//        );
//        this.updateTransactionIds(false);
//        this.notifyTransactionStatus(
//                voidSaleApproved,
//                isoMessageResponse,
//                applicationRepository,
//                ledService,
//                false
//        );
//    }
//
//    private void updateTransactionLog(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean voidSaleApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ApplicationRepository applicationRepository
//    ) {
//        this.logger.log("::: NIBL ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
//        this.logger.log("::: NIBL ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
//        this.logger.log("STAN >>> " + stan);
//        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
//                .withStan(stan)
//                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
//                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
//                .withInvoiceNumber(invoiceNumber)
//                .withRrn(
//                        IsoMessageUtils.retrieveFromDataElementsAsString(
//                                isoMessageResponse,
//                                DataElement.RETRIEVAL_REFERENCE_NUMBER
//                        )
//                )
//                .withAuthCode(
//                        IsoMessageUtils.retrieveFromDataElementsAsString(
//                                isoMessageResponse,
//                                DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE
//                        )
//                )
//                .withTransactionType(transactionRequest.getTransactionType())
//                .withOriginalTransactionType(
//                        this.transactionLogByInvoiceNumber.getTransactionType()
//                )
//                .withTransactionAmount(requestModel.getTransactionRequest().getAmount())
//                .withOriginalTransactionAmount(
//                        this.voidAmount
//                )
//                .withTransactionDate(
//                        IsoMessageUtils.retrieveFromDataElementsAsString(
//                                isoMessageResponse,
//                                DataElement.LOCAL_DATE
//                        )
//                )
//                .withTransactionTime(
//                        IsoMessageUtils.retrieveFromDataElementsAsString(
//                                isoMessageResponse,
//                                DataElement.LOCAL_TIME
//                        )
//                )
//                .withTransactionStatus(voidSaleApproved ? "APPROVED" : "DECLINED")
//                .withPosEntryMode(this.transactionLogByInvoiceNumber.getPosEntryMode())
//                .withOriginalPosEntryMode(this.transactionLogByInvoiceNumber.getPosEntryMode())
//                .withPosConditionCode(this.niblVoidSaleRequest.getPosConditionCode())
//                .withOriginalPosConditionCode(this.transactionLogByInvoiceNumber.getPosConditionCode())
//                .withReconcileStatus("")
//                .withReconcileTime("")
//                .withReconcileDate("")
//                .withReconcileBatchNo(batchNumber)
//                .withReadCardResponse(this.readCardResponse)
//                .withOriginalTransactionReferenceNumber(requestModel.getTransactionRequest().getOriginalInvoiceNumber())
//                .withReceiptLog(getReceiptLog(transactionRequest, this.readCardResponse, isoMessageResponse,
//                        applicationRepository, batchNumber, stan, invoiceNumber))
//                .withAuthorizationCompleted(true)
//                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
//                .withTipAdjusted(false)
//                .withTransactionVoided(false)
//                .build();
//        transactionRepository.updateTransactionLog(transactionLog);
//        transactionRepository.updateVoidStatusForGivenTransactionDetails(
//                voidSaleApproved,
//                this.transactionLogByInvoiceNumber.getTransactionType(),
//                this.transactionLogByInvoiceNumber.getInvoiceNumber()
//        );
//        if (this.transactionLogByInvoiceNumber.isTipAdjusted())
//            transactionRepository.updateVoidStatusForGivenTransactionDetails(
//                    voidSaleApproved,
//                    TransactionType.TIP_ADJUSTMENT,
//                    this.transactionLogByInvoiceNumber.getInvoiceNumber()
//            );
//    }
//}
