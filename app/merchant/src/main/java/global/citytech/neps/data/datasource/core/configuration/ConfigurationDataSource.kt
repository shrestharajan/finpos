package global.citytech.neps.data.datasource.core.configuration

import global.citytech.neps.datasource.model.logon.LogOnResponse
import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.neps.presentation.model.transaction.PurchaseRequestItem
import global.citytech.neps.presentation.model.transaction.PurchaseResponseItem
import global.citytech.posswitchintregator.api.keyexchange.KeyExchangeResponseParameter
import global.citytech.posswitchintregator.api.logon.LogOnResponseParameter
import global.citytech.posswitchintregator.neps.usecases.keyexchange.KeyExchangeResponseModel
import io.reactivex.Observable

interface ConfigurationDataSource {
    fun logOn(configurationItem: ConfigurationItem):Observable<LogOnResponseParameter>
    fun exchangeKey(configurationItem: ConfigurationItem):Observable<KeyExchangeResponseModel>
}