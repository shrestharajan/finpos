package global.citytech.finpos.merchant.presentation.transactions.mobile

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.common.data.Response
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.MobileNfcPayment
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.dynamicqr.nqrsale.DynamicNQrActivity
import global.citytech.finpos.merchant.presentation.mobilenfcpayment.receipt.MobileNfcPaymentReceiptGenerator
import global.citytech.finpos.merchant.presentation.mobilenfcpayment.receipt.MobileNfcTransactionReceiptPrintHandler
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.presentation.model.mobile.voidmobilepay.MobileNfcPaymentVoidRequest
import global.citytech.finpos.merchant.presentation.model.mobile.voidmobilepay.MobileNfcPaymentVoidResponse
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finpos.merchant.utils.SoundUtils
import global.citytech.finpos.merchant.utils.VoiceMessageUtils
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.printer.PrinterResponse
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.repositories.TerminalRepository
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion
import global.citytech.finposframework.utility.StringUtils
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * @author SIDDHARTHA GHIMIRE
 */
class MobileNfcTransactionDetailViewModel(val context: Application): BaseAndroidViewModel(context) {
    val TAG = MobileNfcTransactionDetailViewModel::class.java.name
    var configurationItem: ConfigurationItem? = null
    val transactionConfirmationLiveData by lazy { MutableLiveData<TransactionConfirmation>() }
    val finishActivity by lazy { MutableLiveData<Boolean>() }
    var updateTransactionConfirmation = false
    var voidedMobileNfcPayment: MobileNfcPayment? = null

    private val printerService = PrinterSourceImpl(context)
    private val localDataUseCase =
        LocalDataUseCase(LocalRepositoryImpl(LocalDatabaseSourceImpl(), PreferenceManager))
    private var terminalRepository: TerminalRepository? = null

    fun getConfigurationItem() {
        compositeDisposable.add(localDataUseCase.getWholeConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.mapToPresentation() }
            .subscribe({
                configurationItem = it
                terminalRepository = TerminalRepositoryImpl(configurationItem)
            }, {
                printLog(TAG, "Get Configuration error ::: "+it.message.toString())
            })
        )
    }

    fun printDuplicateReceipt(mobileNfcPayment: MobileNfcPayment) {
        isLoading.value = true
        compositeDisposable.add(
            onPrintDuplicateReceiptSubscribe(mobileNfcPayment)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onDuplicatePrinterResponseReceiptReceived(it)
                }, {
                    printLog(TAG, "PRINT DUPLICATE RECEIPT ::: "+it.message.toString())
                    isLoading.value = false
                    it.printStackTrace()
                    message.value = it.message
                })
        )
    }

    private fun onDuplicatePrinterResponseReceiptReceived(it: PrinterResponse) {
        isLoading.value = false
        if (it.result == Result.SUCCESS || it.result == Result.CANCEL) {
            return
        }
        message.value = it.message
    }

    private fun onPrintDuplicateReceiptSubscribe(
        mobileNfcPayment: MobileNfcPayment
    ): Observable<PrinterResponse> {
        val mobileReceipt = MobileNfcPaymentReceiptGenerator(terminalRepository!!).generate(mobileNfcPayment)
        val mobileNfcTransactionReceiptPrintHandler =
            MobileNfcTransactionReceiptPrintHandler(printerService)
        return Observable.fromCallable {
            mobileNfcTransactionReceiptPrintHandler.printReceipt(
                mobileReceipt,
                ReceiptVersion.DUPLICATE_COPY
            )
        }
    }

    fun performVoid(originalMobileNfcPayment: MobileNfcPayment) {
        isLoading.value = true
        compositeDisposable.add(Observable.create(
            ObservableOnSubscribe<MobileNfcPaymentVoidResponse> {
                onMobileNfcPaymentVoidSubscribe(it, originalMobileNfcPayment)
            }
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                val isApproved = true
                updateTransactionConfirmation = false
                transactionConfirmationLiveData.value = TransactionConfirmation.Builder()
                    .amount(StringUtils.formatAmountTwoDecimal(originalMobileNfcPayment.transactionAmount))
                    .imageId(if (isApproved) R.drawable.approved else R.drawable.declined)
                    .message(if (isApproved) "Approval Code: ".plus(it.requestNumber) else "DECLINED")
                    .title(if (isApproved) "APPROVED" else "DECLINED")
                    .positiveLabel("")
                    .negativeLabel("")
                    .qrImageString("")
                    .transactionStatus("")
                    .build()
                playSound(isApproved,originalMobileNfcPayment.transactionAmount.toDouble())
                storeTransactionLog(originalMobileNfcPayment, isApproved, it)

            }, {
                printLog(TAG, "PERFORM VOID ::: "+it.message.toString())
                isLoading.value = false
                message.value = it.message
            })
        )
    }

    private fun storeTransactionLog(
        originalMobileNfcPayment: MobileNfcPayment,
        isApproved: Boolean,
        mobileNfcPaymentVoidResponse: MobileNfcPaymentVoidResponse
    ) {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<MobileNfcPayment> {
                val responseDateTimeStamp = mobileNfcPaymentVoidResponse.localDateTime
                val transactionStatus = if (isApproved) "APPROVED" else "DECLINED"
                val voidMobileNfcPayment = MobileNfcPayment(
                    merchantId = originalMobileNfcPayment.merchantId,
                    terminalId = originalMobileNfcPayment.terminalId,
                    referenceNumber = originalMobileNfcPayment.referenceNumber,
                    invoiceNumber = originalMobileNfcPayment.invoiceNumber,
                    transactionType = "VOID SALE",
                    transactionDate = responseDateTimeStamp.substring(0, 10),
                    transactionTime = responseDateTimeStamp.substring(10),
                    transactionCurrency = originalMobileNfcPayment.transactionCurrency,
                    transactionAmount = originalMobileNfcPayment.transactionAmount,
                    transactionStatus = transactionStatus,
                    approvalCode = mobileNfcPaymentVoidResponse.requestNumber,
                    paymentInitiator = originalMobileNfcPayment.paymentInitiator,
                    initiatorId = originalMobileNfcPayment.initiatorId
                )
                it.onNext(voidMobileNfcPayment)
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    voidedMobileNfcPayment = it
                    printMobilePayReceipt(ReceiptVersion.MERCHANT_COPY)
                }, {
                    printLog(TAG, "STORE TRANSACTION LOG ::: "+it.message.toString())
                    isLoading.value = false
                    it.printStackTrace()
                })
        )
    }

    fun printMobilePayReceipt(receiptVersion: ReceiptVersion) {
        if (receiptVersion == ReceiptVersion.CUSTOMER_COPY)
            isLoading.value = true
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<PrinterResponse> {
                val dynamicReceipt =
                    MobileNfcPaymentReceiptGenerator(terminalRepository!!).generate(
                        voidedMobileNfcPayment!!
                    )
                val printerResponse =
                    MobileNfcTransactionReceiptPrintHandler(printerService).printReceipt(
                        dynamicReceipt,
                        receiptVersion
                    )
                it.onNext(printerResponse)
                it.onComplete()
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    updateTransactionConfirmation = true
                    if (receiptVersion == ReceiptVersion.MERCHANT_COPY) {
                        val transactionConfirmation = transactionConfirmationLiveData.value
                        transactionConfirmation!!.positiveLabel = "PRINT CUSTOMER COPY"
                        transactionConfirmation.negativeLabel = "RETURN TO TRANSACTIONS"
                        transactionConfirmationLiveData.value = transactionConfirmation
                    } else {
                        finishActivity.value = true
                    }
                }, {
                    printLog(TAG, "MOBILE PAY RECEIPT ::: "+it.message.toString())
                    isLoading.value = false
                    it.printStackTrace()
                })
        )
    }

    private fun onMobileNfcPaymentVoidSubscribe(
        it: ObservableEmitter<MobileNfcPaymentVoidResponse>,
        originalMobileNfcPayment: MobileNfcPayment
    ) {
        val qrVoidRequest = MobileNfcPaymentVoidRequest(
            originalMobileNfcPayment.merchantId,
            originalMobileNfcPayment.terminalId,
            originalMobileNfcPayment.referenceNumber,
            StringUtils.formatAmountTwoDecimal(originalMobileNfcPayment.transactionAmount)
        )
        val jsonRequest = Jsons.toJsonObj(qrVoidRequest)
        val jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.post(
            "/nfcengine/nfc/payments/void/",
            jsonRequest,
            false
        )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            val responseDataJson = Jsons.toJsonObj(response.data)
            it.onNext(Jsons.fromJsonToObj(responseDataJson, MobileNfcPaymentVoidResponse::class.java))
        } else {
            it.onError(Exception(response.message))
        }
    }

    fun getMerchantCredential(): String = localDataUseCase.getMerchantCredential()


    private fun playSound(isApproved: Boolean, amount: Double) {
        if (isApproved) {
            /** APPROVED */
            if (localDataUseCase.isSoundMode(false))
                SoundUtils.play(
                    getApplication<Application?>().applicationContext,
                    amount,
                    "radha",
                    AppUtility.getCurrencyCode()
                )
            printLog(TAG,"transaction approved")
        } else {
            /** DECLINED */
            if (localDataUseCase.isSoundMode(false))
                VoiceMessageUtils.playFail(getApplication<Application?>().applicationContext)
            printLog(TAG,"transaction declined")
        }
    }

}