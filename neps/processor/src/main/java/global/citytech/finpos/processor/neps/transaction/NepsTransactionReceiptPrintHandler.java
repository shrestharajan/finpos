package global.citytech.finpos.processor.neps.transaction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import global.citytech.finpos.processor.neps.receipt.ReceiptLabels;
import global.citytech.finposframework.hardware.io.printer.PrintMessage;
import global.citytech.finposframework.hardware.io.printer.Printable;
import global.citytech.finposframework.hardware.io.printer.PrinterRequest;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.io.printer.Style;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.data.StatementList;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.EmvTags;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finpos.processor.neps.receipt.ReceiptUtils.addBarCodeImage;
import static global.citytech.finpos.processor.neps.receipt.ReceiptUtils.addBase64Image;

public class NepsTransactionReceiptPrintHandler implements ReceiptHandler.TransactionReceiptHandler {

    private final static int AID_MAX_CHARACTERS_TO_MODIFY_ALIGNMENT = 20;
    private final PrinterService printerService;

    public NepsTransactionReceiptPrintHandler(PrinterService printerService) {
        this.printerService = printerService;
    }

    @Override
    public PrinterResponse printTransactionReceipt(ReceiptLog receiptLog, ReceiptVersion receiptVersion) {
        PrinterRequest printerRequest = this.preparePrinterRequest(receiptLog,receiptVersion);
        return printerService.print(printerRequest);
    }

    @Override
    public PrinterResponse printTransactionReceipt(ReceiptLog receiptLog, ReceiptVersion receiptVersion, StatementList statementList) {
        return null;
    }

    private PrinterRequest preparePrinterRequest(ReceiptLog receiptLog, ReceiptVersion receiptVersion) {
        List<Printable> printableList = new ArrayList<>();
        if (receiptLog.getRetailer().getRetailerLogo() != null)
            addBase64Image(printableList, receiptLog.getRetailer().getRetailerLogo());
        addSingleColumnString(printableList, receiptLog.getRetailer().getRetailerName(), TransactionReceiptStyle.RETAILER_NAME.getStyle());
        addSingleColumnString(printableList, receiptLog.getRetailer().getRetailerAddress(), TransactionReceiptStyle.RETAILER_ADDRESS.getStyle());
        addFeedPaper(printableList);
        addDoubleColumnString(printableList, this.prepareDateString(receiptLog.getPerformance().getStartDateTime()), this.prepareTimeString(receiptLog.getPerformance().getStartDateTime()), TransactionReceiptStyle.START_DATE_TIME.getStyle());
        addDoubleColumnString(printableList, this.merchantId(receiptLog.getRetailer().getMerchantId()), this.terminalId(receiptLog.getRetailer().getTerminalId()), TransactionReceiptStyle.MID_TID.getStyle());
        addDoubleColumnString(printableList, this.rrn(receiptLog.getTransactionInfo().getRrn()), this.stan(receiptLog.getTransactionInfo().getStan()), TransactionReceiptStyle.RRN_STAN.getStyle());
        addDoubleColumnString(printableList, ReceiptLabels.BATCH_NUMBER_LABEL, receiptLog.getTransactionInfo().getBatchNumber(), TransactionReceiptStyle.BATCH_NUMBER.getStyle());
        addDoubleColumnString(printableList, ReceiptLabels.INVOICE_NUMBER_LABEL, receiptLog.getTransactionInfo().getInvoiceNumber(), TransactionReceiptStyle.INVOICE_NUMBER.getStyle());
        addReceiptVersionToPrintableListIfOnlyDuplicateCopy(printableList, receiptVersion);
        addTransactionTypeLabelFromReceiptLogToPrintList(printableList, receiptLog);
        addSingleColumnString(printableList, ReceiptLabels.DIVIDER, TransactionReceiptStyle.DIVIDER.getStyle());
        addDoubleColumnString(printableList, ReceiptLabels.PURCHASE_AMOUNT_LABEL,
                this.amount(receiptLog),
                TransactionReceiptStyle.AMOUNT.getStyle());
        addSingleColumnString(printableList, ReceiptLabels.DIVIDER, TransactionReceiptStyle.DIVIDER.getStyle());
        addSingleColumnString(printableList, receiptLog.getTransactionInfo().getTransactionStatus(), TransactionReceiptStyle.TRANSACTION_STATUS.getStyle());
        addFeedPaper(printableList);
        if (!receiptLog.getTransactionInfo().isSignatureRequired()) {
            addSingleColumnString(printableList, receiptLog.getTransactionInfo().getTransactionMessage(), TransactionReceiptStyle.TRANSACTION_MESSAGE.getStyle());
        }
        if (receiptLog.getTransactionInfo().isSignatureRequired()) {
            addSingleColumnString(printableList, ReceiptLabels.SIGNATURE_MESSAGE_CUSTOMER, TransactionReceiptStyle.SIGNATURE_MESSAGE.getStyle());
            addSingleColumnString(printableList, ReceiptLabels.SIGNATURE_LINE, TransactionReceiptStyle.SIGNATURE_LINE.getStyle());
            if (receiptLog.getTransactionInfo().isDebitAcknowledgementRequired())
                addSingleColumnString(printableList, ReceiptLabels.DEBIT_ACCOUNT_ACKNOWLEDGEMENT, TransactionReceiptStyle.DEBIT_CREDIT_ACK.getStyle());
        }
        addSingleColumnString(printableList, receiptLog.getTransactionInfo().getCardHolderName(), TransactionReceiptStyle.CARD_HOLDER_NAME.getStyle());
        addFeedPaper(printableList);
        addSingleColumnString(printableList, StringUtils.arrangeEncodedPanInGivenGroupSize(receiptLog.getTransactionInfo().getCardNumber(), 4), TransactionReceiptStyle.CARD_NUMBER.getStyle());
        addDoubleColumnString(printableList, ReceiptLabels.CARD_TYPE, receiptLog.getTransactionInfo().getCardScheme(), TransactionReceiptStyle.CARD_SCHEME.getStyle());
        addDoubleColumnString(printableList, ReceiptLabels.AID, receiptLog.getEmvTags().getAid(), TransactionReceiptStyle.AID.getStyle());
        addApprovalCodeToPrintListIfPresent(printableList, receiptLog);
        addFeedPaper(printableList);
        addSingleColumnString(printableList, this.emvTagsString(receiptLog.getEmvTags()), TransactionReceiptStyle.EMV_TAG_DATA.getStyle());
        addFeedPaper(printableList);
        addSingleColumnString(printableList, ReceiptLabels.AGREEMENT_DISCLAIMER, TransactionReceiptStyle.AGREEMENT_DISCLAIMER.getStyle());
        addSingleColumnString(printableList, ReceiptLabels.THANK_YOU_MESSAGE, TransactionReceiptStyle.THANK_YOU_MESSAGE.getStyle());
        addReceiptVersionToPrintableListIfNotDuplicateCopy(printableList, receiptVersion);
        if (!StringUtils.isEmpty(receiptLog.getBarCodeString())) {
            addFeedPaper(printableList);
            addBarCode(printableList, receiptLog);
        }
        return new PrinterRequest(printableList);
    }

    private void addBarCode(List<Printable> printableList, ReceiptLog receiptLog) {
        addBarCodeImage(printableList, receiptLog.getBarCodeString());
        addSingleColumnString(printableList, receiptLog.getTransactionInfo().getRrn(), TransactionReceiptStyle.BAR_CODE_LABEL.getStyle());
    }

    private void addFeedPaper(List<Printable> printableList) {
        addSingleColumnString(printableList, "  ", TransactionReceiptStyle.DIVIDER.getStyle());
    }

    private void addTransactionTypeLabelFromReceiptLogToPrintList(List<Printable> printableList, ReceiptLog receiptLog) {
        addSingleColumnString(
                printableList,
                receiptLog.getTransactionInfo().getPurchaseType(),
                TransactionReceiptStyle.PURCHASE_TYPE.getStyle()
        );
    }

    private void addReceiptVersionToPrintableListIfNotDuplicateCopy(List<Printable> printableList, ReceiptVersion receiptVersion) {
        if (!isReceiptVersionDuplicateCopy(receiptVersion)) {
            addSingleColumnString(
                    printableList,
                    receiptVersion.getVersionLabel(),
                    TransactionReceiptStyle.RECEIPT_VERSION.getStyle()
            );
        }
    }

    private void addReceiptVersionToPrintableListIfOnlyDuplicateCopy(List<Printable> printableList, ReceiptVersion receiptVersion) {
        if (isReceiptVersionDuplicateCopy(receiptVersion)) {
            addSingleColumnString(
                    printableList,
                    receiptVersion.getVersionLabel(),
                    TransactionReceiptStyle.RECEIPT_VERSION.getStyle()
            );
            addSingleColumnString(
                    printableList,
                    "  ",
                    TransactionReceiptStyle.DIVIDER.getStyle()
            );
        }
    }

    private boolean isReceiptVersionDuplicateCopy(ReceiptVersion receiptVersion) {
        return (receiptVersion == ReceiptVersion.DUPLICATE_COPY);
    }

    private String emvTagsString(EmvTags emvTags) {
        StringBuilder stringBuilder = new StringBuilder();
        appendValidValueToStringBuilder(stringBuilder, emvTags.getCardType());
        appendValidValueToStringBuilder(stringBuilder, emvTags.getResponseCode());
        appendValidValueToStringBuilder(stringBuilder, emvTags.getAid());
        appendValidTvrToStringBuilder(stringBuilder, emvTags.getTvr());
        appendNextLineOrSpaceToStringBuilderBasedOnAidLength(stringBuilder, emvTags.getAid());
        appendValidValueToStringBuilder(stringBuilder, emvTags.getTsi());
        appendValidValueToStringBuilder(stringBuilder, emvTags.getCvm());
        appendValidValueToStringBuilder(stringBuilder, emvTags.getCid());
        appendValidValueToStringBuilder(stringBuilder, emvTags.getAc());
        appendValidValueToStringBuilder(stringBuilder, emvTags.getKernelId());
        return stringBuilder.toString();
    }

    private void appendValidValueToStringBuilder(StringBuilder stringBuilder, String value) {
        if (StringUtils.isEmpty(value)) {
            return;
        }
        stringBuilder.append(value);
        stringBuilder.append("  ");
    }

    private void appendValidTvrToStringBuilder(StringBuilder stringBuilder, String tvr) {
        if (StringUtils.isEmpty(tvr)) {
            return;
        }
        stringBuilder.append(tvr);
    }

    private void appendNextLineOrSpaceToStringBuilderBasedOnAidLength(
            StringBuilder stringBuilder,
            String aid
    ) {
        if (StringUtils.isEmpty(aid) || aid.length() > AID_MAX_CHARACTERS_TO_MODIFY_ALIGNMENT) {
            stringBuilder.append("\t");
            return;
        }
        stringBuilder.append("\n");
    }

    private String amount(ReceiptLog receiptLog) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(receiptLog.getTransactionInfo().getTransactionCurrencyName());
        stringBuilder.append("\t");
        stringBuilder.append(receiptLog.getTransactionInfo().getPurchaseAmount());
        return stringBuilder.toString();
    }

    private String rrn(String rrn) {
        return this.formatLabelWithValue(ReceiptLabels.RRN_LABEL, rrn);
    }

    private String formatLabelWithValue(String label, String value) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(label);
        stringBuilder.append(":\t");
        stringBuilder.append(value);
        return stringBuilder.toString();
    }

    private String stan(String stan) {
        return this.formatLabelWithValue(ReceiptLabels.STAN_LABEL, stan);
    }

    private String batchNumber(String batchNumber) {
        return this.formatLabelWithValue(ReceiptLabels.BATCH_NUMBER_LABEL, batchNumber);
    }

    private String merchantId(String merchantId) {
        return this.formatLabelWithValue(ReceiptLabels.MERCHANT_ID_LABEL, merchantId);
    }

    private String terminalId(String terminalId) {
        return this.formatLabelWithValue(ReceiptLabels.TERMINAL_ID_LABEL, terminalId);
    }

    private String prepareTimeString(String dateTime) {
        if (StringUtils.isEmpty(dateTime) || dateTime.length() < 15)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(dateTime, 6, 8);
        stringBuilder.append(":");
        stringBuilder.append(dateTime, 8, 10);
        stringBuilder.append(":");
        stringBuilder.append(dateTime, 10, 12);
        return this.formatLabelWithValue(ReceiptLabels.TIME_LABEL, stringBuilder.toString());
    }

    private String prepareDateString(String dateTime) {
        if (StringUtils.isEmpty(dateTime) || dateTime.length() < 15)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("20");
        stringBuilder.append(dateTime, 0, 2);
        stringBuilder.append("/");
        stringBuilder.append(dateTime, 2, 4);
        stringBuilder.append("/");
        stringBuilder.append(dateTime, 4, 6);
        return this.formatLabelWithValue(ReceiptLabels.DATE_LABEL, stringBuilder.toString());
    }

    private void addMultiColumnString(List<Printable> printableList, List<String> stringList, int[] columnWidths, Style style) {
        printableList.add(PrintMessage.Companion.getMultiColumnInstance(stringList, columnWidths, style));
    }

    private List<String> createStringList(String... args) {
        return new ArrayList<>(Arrays.asList(args));
    }

    private void addDoubleColumnString(List<Printable> printableList, String label, String value, Style style) {
        printableList.add(PrintMessage.Companion.getDoubleColumnInstance(label, value, style));
    }

    private void addSingleColumnString(List<Printable> printableList, String value, Style style) {
        printableList.add(PrintMessage.Companion.getInstance(value, style));
    }

    private void addApprovalCodeToPrintListIfPresent(
            List<Printable> printableList,
            ReceiptLog receiptLog
    ) {
        String approvalCode = receiptLog.getTransactionInfo().getApprovalCode();
        if (!StringUtils.isEmpty(approvalCode)) {
            addDoubleColumnString(
                    printableList,
                    ReceiptLabels.APPROVAL_CODE_LABEL,
                    approvalCode,
                    TransactionReceiptStyle.APPROVAL_CODE.getStyle()
            );
        }
    }
}
