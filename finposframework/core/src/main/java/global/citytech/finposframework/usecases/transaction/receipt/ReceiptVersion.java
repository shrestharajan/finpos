package global.citytech.finposframework.usecases.transaction.receipt;

/**
 * Created by Unique Shakya on 9/1/2020.
 */
public enum ReceiptVersion {
    MERCHANT_COPY("** MERCHANT COPY **"),
    CUSTOMER_COPY("** CUSTOMER COPY **"),
    DUPLICATE_COPY("** DUPLICATE COPY **");

    private String versionLabel;

    ReceiptVersion(String versionLabel) {
        this.versionLabel = versionLabel;
    }

    public String getVersionLabel() {
        return versionLabel;
    }
}
