package global.citytech.finpos.merchant.presentation.model.refund

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

/**
 * Created by Unique Shakya on 9/28/2020.
 */
data class RefundRequestItem(
    val amount: BigDecimal? = null,
    val transactionType: TransactionType? = null,
    val originalRetrievalReferenceNumber: String? = null
) {}