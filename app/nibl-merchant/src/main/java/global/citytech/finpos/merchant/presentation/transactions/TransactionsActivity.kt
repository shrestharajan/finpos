package global.citytech.finpos.merchant.presentation.transactions

import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.ViewModelProviders
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityTransactionsBinding
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.transactions.card.CardTransactionFragment
import global.citytech.finpos.merchant.presentation.transactions.mobile.MobileNfcTransactionFragment
import global.citytech.finpos.merchant.presentation.transactions.qr.QrTransactionFragment
import kotlinx.android.synthetic.main.activity_transactions.bnv_transactions
import kotlinx.android.synthetic.main.activity_transactions.iv_back


class TransactionsActivity : AppBaseActivity<ActivityTransactionsBinding, TransactionsViewModel>() {

    private val qrTransactionFragment: QrTransactionFragment  = QrTransactionFragment()
    private val cardTransactionFragment: CardTransactionFragment  = CardTransactionFragment()
    private val mobileNfcTransactionFragment: MobileNfcTransactionFragment  = MobileNfcTransactionFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initQrAndMobilePayOptionConfig()
        openCardTransactionFragment()
        observeLiveVisibilityForQrAndMobilePayOption()

        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(currentFocus?.windowToken, 0)

        bnv_transactions.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.menu_card_transaction -> {
                    openCardTransactionFragment()
                }
                R.id.menu_qr_transaction -> {
                    openQrTransactionFragment()
                }
                R.id.menu_mobile_pay_transaction -> {
                    openMobilePayTransactionFragment()
                }
                else -> {
                    return@setOnItemSelectedListener false
                }
            }
            true
        }

        iv_back.setOnClickListener {
            finish()
        }
    }

    private fun initQrAndMobilePayOptionConfig(){
        getViewModel().initMobilePayOptionConfig()
        getViewModel().initQrOptionConfig()
    }

    private fun observeLiveVisibilityForQrAndMobilePayOption(){
        getViewModel().showMobilePayOption.observe(this) {
            bnv_transactions.menu.findItem(R.id.menu_mobile_pay_transaction).isVisible = it
        }

        getViewModel().showQrOption.observe(this){
            bnv_transactions.menu.findItem(R.id.menu_qr_transaction).isVisible = it
        }
    }

    private fun openMobilePayTransactionFragment() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fl_transactions, mobileNfcTransactionFragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    private fun openQrTransactionFragment() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fl_transactions, qrTransactionFragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    private fun openCardTransactionFragment() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fl_transactions, cardTransactionFragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayout(): Int = R.layout.activity_transactions

    override fun getViewModel(): TransactionsViewModel =
        ViewModelProviders.of(this)[TransactionsViewModel::class.java]

    override fun onBackPressed() {
        // do nothing
    }
}