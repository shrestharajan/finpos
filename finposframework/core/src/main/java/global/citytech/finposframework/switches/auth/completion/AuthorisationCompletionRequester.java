package global.citytech.finposframework.switches.auth.completion;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 9/29/2020.
 */
public interface AuthorisationCompletionRequester<T extends AuthorisationCompletionRequestParameter,
        E extends AuthorisationCompletionResponseParameter> extends Request<T, E> {
}
