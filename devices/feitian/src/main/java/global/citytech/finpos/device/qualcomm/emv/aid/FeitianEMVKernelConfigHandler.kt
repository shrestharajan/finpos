package global.citytech.finpos.device.qualcomm.emv.aid

import android.content.Context
import com.ftpos.library.smartpos.bean.CEmvAidBean
import com.ftpos.library.smartpos.device.Device
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.emv.IActionFlag
import global.citytech.common.extensions.defaultOrEmptyValue
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.CHECK_FLAG
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.DO_NOT_CHECK_FLAG
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finpos.device.qualcomm.utils.FeitianUtils
import global.citytech.finposframework.hardware.emv.aid.AidParameters
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import global.citytech.finposframework.utility.AmountUtils

/**
 * Created by Rishav Chudal on 4/5/21.
 */
class FeitianEMVKernelConfigHandler constructor(
    private val context: Context,
    private val emv: Emv,
    private val aidParameters: AidParameters,
    private val emvParameters: EmvParametersRequest
) : FeitianLogger.Listener {

    private val logger = FeitianLogger(this)
    private val device: Device = Device.getInstance(context.applicationContext)


    fun injectEMVKernelConfigurations(): Int {
        val configurationsByteArray = prepareEMVKernelConfigurationsByteArray()
        val resultEMVAppParams = emv.manageEmvAppParameters(
            IActionFlag.ADD,
            configurationsByteArray
        )
        logger.logWithClassName(
            "EMV Kernel configurations injection result ::: ".plus(resultEMVAppParams)
        )
        return resultEMVAppParams
    }

    /**
     * Note: Parameter that are not set
     * - threshold value
     * - max target percentage
     * - target percentage
     *
     */
    private fun prepareEMVKernelConfigurationsByteArray(): ByteArray {
        logger.logWithClassName("***** Contact Params *****")

        val aid9F06 = aidParameters.aid.defaultOrEmptyValue()
        logger.debugWithClassName("AID ::: ".plus(aid9F06))
        val cEmvAidBean = CEmvAidBean(aid9F06)

        val appSelectionIndicator =
            DO_NOT_CHECK_FLAG //hardcoded as the AID parameters don't have this parameter
        logger.debugWithClassName("App Selection Indicator ::: ".plus(appSelectionIndicator))
        cEmvAidBean.setmASI_1F14(appSelectionIndicator)

        val defaultDDOL = aidParameters.defaultDDOL.defaultOrEmptyValue()
        logger.debugWithClassName("Default DDOL ::: ".plus(defaultDDOL))
        cEmvAidBean.setmDefaultDDOL_1F02(defaultDDOL)

        val appVersionNumber: String = aidParameters.terminalAidVersion.defaultOrEmptyValue()
        logger.debugWithClassName("Application Version Number ::: ".plus(appVersionNumber))
        cEmvAidBean.setmAppVersionNumber_9F09(appVersionNumber)

        var terminalFloorLimit = aidParameters.terminalFloorLimit.defaultOrEmptyValue()
        logger.debugWithClassName("Terminal Floor Limit ::: ".plus(terminalFloorLimit))
        terminalFloorLimit = AmountUtils.toISOFormattedLowerCurrency(terminalFloorLimit)
        logger.debugWithClassName(
            "Formatted Terminal Floor Limit ::: ".plus(terminalFloorLimit)
        )
        cEmvAidBean.setmTerminalFloorLimit_9F1B(terminalFloorLimit)

        val terminalTACDenial = aidParameters.denialActionCode.defaultOrEmptyValue()
        logger.debugWithClassName(
            "Terminal Action Code Denial ::: ".plus(terminalTACDenial)
        )
        cEmvAidBean.setmTerminalActionCodeDenial_1F05(terminalTACDenial)

        val terminalTACOnline = aidParameters.onlineActionCode.defaultOrEmptyValue()
        logger.debugWithClassName(
            "Terminal Action Code Online ::: ".plus(terminalTACOnline)
        )
        cEmvAidBean.setmTerminalActionCodeOnline_1F06(terminalTACOnline)

        val terminalTACDefault = aidParameters.defaultActionCode.defaultOrEmptyValue()
        logger.debugWithClassName(
            "Terminal Action Code Default ::: ".plus(terminalTACDefault)
        )
        cEmvAidBean.setmTerminalActionCodeDefault_1F04(terminalTACDefault)

        val terminalCapabilities = emvParameters.terminalCapabilities.defaultOrEmptyValue()
        logger.debugWithClassName("Terminal Capabilities ::: ".plus(terminalCapabilities))
        cEmvAidBean.setmTerminalCapabilities_9F33(terminalCapabilities)

        val addTerminalCapabilities: String =
            emvParameters.additionalTerminalCapabilities.defaultOrEmptyValue()
        logger.debugWithClassName(
            "Additional Terminal Capabilities ::: ".plus(addTerminalCapabilities)
        )
        cEmvAidBean.setmAdditionalTerminalCapabilities_9F40(addTerminalCapabilities)

        val acquirerId = aidParameters.acquirerId.defaultOrEmptyValue()
        logger.debugWithClassName("Acquirer Id ::: ".plus(acquirerId))
        cEmvAidBean.setmAcquirerID_9F01(acquirerId)

        val merchantCategoryCode = emvParameters.merchantCategoryCode.defaultOrEmptyValue()
        logger.debugWithClassName("Merchant Category Code ::: ".plus(merchantCategoryCode))
        cEmvAidBean.setmMerchantCategoryCode_9F15(merchantCategoryCode)

        val merchantId = emvParameters.merchantIdentifier.defaultOrEmptyValue()
        logger.debugWithClassName("Merchant Id ::: ".plus(merchantId))
        cEmvAidBean.setmMerchantID_9F16(merchantId)

        val merchantNameLocation: String = emvParameters.merchantName.defaultOrEmptyValue()
        logger.debugWithClassName("Merchant Name and Location ::: ".plus(merchantNameLocation))
        cEmvAidBean.setmMerchantNameLocation_9F4E(merchantNameLocation)

        val terminalId = emvParameters.terminalId.defaultOrEmptyValue()
        logger.debugWithClassName("Terminal Id ::: ".plus(terminalId))
        cEmvAidBean.setmTerminalID_9F1C(terminalId)

        val transCurrencyCode = emvParameters.transactionCurrencyCode.defaultOrEmptyValue()
        logger.debugWithClassName("Transaction Currency Code ::: ".plus(transCurrencyCode))
        cEmvAidBean.setmTransCurrencyCode_5F2A(transCurrencyCode)

        val transCurrencyExponent = emvParameters.transactionCurrencyExponent.defaultOrEmptyValue()
        logger.debugWithClassName("Transaction Currency Exponent ::: ".plus(transCurrencyExponent))
        cEmvAidBean.setmTransCurrencyExponent_5F36(transCurrencyExponent)

        val transReferenceCurrencyCode = emvParameters.transactionCurrencyCode.defaultOrEmptyValue()
        logger.debugWithClassName(
            "Transaction Reference Currency Code ::: ".plus(
                transReferenceCurrencyCode
            )
        )
        cEmvAidBean.setmTransRefCurrencyCode_9F3C(transReferenceCurrencyCode)

        val transReferenceCurrencyExponent =
            emvParameters.transactionCurrencyExponent.defaultOrEmptyValue()
        logger.debugWithClassName(
            "Transaction Reference Currency Exponent ::: ".plus(transReferenceCurrencyExponent)
        )
        cEmvAidBean.setmTransRefCurrencyExponent_9F3D(transReferenceCurrencyExponent)

        val terminalRiskMgmtData = aidParameters.terminalRiskManagementData.defaultOrEmptyValue()
        logger.debugWithClassName("Terminal Risk Management Data ::: ".plus(terminalRiskMgmtData))
        cEmvAidBean.setmTerminalRiskManageData_9F1D(terminalRiskMgmtData)

        val defaultTDOL = aidParameters.defaultTDOL.defaultOrEmptyValue()
        logger.debugWithClassName("Default TDOL ::: $defaultTDOL")
        cEmvAidBean.setmDefaultTDOL_1F03(defaultTDOL)

        val terminalCountryCode = emvParameters.terminalCountryCode.defaultOrEmptyValue()
        logger.debugWithClassName("Terminal Country Code ::: ".plus(terminalCountryCode))
        cEmvAidBean.setmTerminalCountryCode_9F1A(terminalCountryCode)

        val ifdSerialNumber = FeitianUtils.retrieveDeviceSerialNumber(device)
        logger.debugWithClassName("IFD Serial Number ::: ".plus(ifdSerialNumber))
        cEmvAidBean.setmIFDSerial_9F1E(ifdSerialNumber)

        val checkFloorLimit = CHECK_FLAG
        logger.debugWithClassName("Check Floor Limit ::: ".plus(checkFloorLimit))
        cEmvAidBean.transSupportFloorlimitCheck_1F12 = checkFloorLimit

//        val velocityCheck = CHECK_FLAG
//        logger.debugWithClassName("Velocity Check ::: ".plus(velocityCheck))
//        cEmvAidBean.transSupportVelocityCheck_1F16 = velocityCheck

//        val randomTransactionSelectionCheck = CHECK_FLAG
//        logger.debugWithClassName(
//            "Random Transaction Selection Check ::: ".plus(
//                randomTransactionSelectionCheck
//            )
//        )
//        cEmvAidBean.transSupportRandomTransSelection_1F15 = randomTransactionSelectionCheck

        val forceOnline = DO_NOT_CHECK_FLAG
        logger.debugWithClassName("Force Online Check ::: $forceOnline")
        cEmvAidBean.transForceOnline_1F0A = forceOnline

        return cEmvAidBean.toTlvByteArray()
    }

    override fun getClassName(): String {
        return FeitianEMVKernelConfigHandler::class.java.simpleName
    }
}