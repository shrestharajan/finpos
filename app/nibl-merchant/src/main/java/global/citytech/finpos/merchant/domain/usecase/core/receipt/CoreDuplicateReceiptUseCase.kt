package global.citytech.finpos.merchant.domain.usecase.core.receipt

import global.citytech.finpos.merchant.domain.model.receipt.duplicate.DuplicateReceiptResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.receipt.DuplicateReceiptRepository
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 12/15/2020.
 */
class CoreDuplicateReceiptUseCase(private val duplicateReceiptRepository: DuplicateReceiptRepository) {
    fun print(stan: String? = ""): Observable<DuplicateReceiptResponseEntity> {
        return duplicateReceiptRepository.print(stan)
    }
}