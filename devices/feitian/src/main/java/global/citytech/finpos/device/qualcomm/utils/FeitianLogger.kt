package global.citytech.finpos.device.qualcomm.utils

import android.util.Log
import global.citytech.finpos.device.qualcomm.BuildConfig
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.LOG_TAG
import global.citytech.finposframework.log.AppState
import global.citytech.finposframework.utility.StringUtils

/**
 * Created by Rishav Chudal on 4/5/21.
 */
class FeitianLogger constructor(private val listener: Listener) {

    companion object {
        @JvmStatic fun logStatic(message: String?) {
            if (!StringUtils.isEmpty(message)) {
                Log.d(LOG_TAG, message!!)
            }
        }

        @JvmStatic fun debugStatic(message: String?) {
            if (!StringUtils.isEmpty(message) && AppState.getInstance().isDebug) {
                Log.d(LOG_TAG, message!!)
            }
        }
    }

    fun logWithClassName(message: String?) {
        if (!StringUtils.isEmpty(message)) {
            Log.d(LOG_TAG, listener.getClassName().plus(" ::: ").plus(message!!))
        }
    }

    fun debugWithClassName(message: String?) {
        if (!StringUtils.isEmpty(message) && AppState.getInstance().isDebug) {
            Log.d(LOG_TAG, listener.getClassName().plus(" ::: ").plus(message!!))
        }
    }

    interface Listener {
        fun getClassName(): String
    }
}