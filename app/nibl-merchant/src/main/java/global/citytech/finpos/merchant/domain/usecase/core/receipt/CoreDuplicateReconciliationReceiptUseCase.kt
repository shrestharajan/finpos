package global.citytech.finpos.merchant.domain.usecase.core.receipt

import global.citytech.finpos.merchant.domain.model.receipt.duplicatereconciliation.DuplicateReconciliationReceiptResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.receipt.DuplicateReconciliationReceiptRepository
import global.citytech.finpos.processor.nibl.receipt.duplicatereconciliation.DuplicateReconciliationReceiptResponseModel
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 12/16/2020.
 */
class CoreDuplicateReconciliationReceiptUseCase(
    private val duplicateReconciliationReceiptRepository:
    DuplicateReconciliationReceiptRepository
) {
    fun printReceipt(): Observable<DuplicateReconciliationReceiptResponseEntity> {
        return duplicateReconciliationReceiptRepository.printReceipt()
    }
}