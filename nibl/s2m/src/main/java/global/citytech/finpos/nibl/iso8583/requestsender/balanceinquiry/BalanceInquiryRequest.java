package global.citytech.finpos.nibl.iso8583.requestsender.balanceinquiry;

import java.math.BigDecimal;

import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.switches.PosEntryMode;

public class BalanceInquiryRequest  extends TransactionIsoRequest {
    private String pan;
    private BigDecimal transactionAmount = BigDecimal.ZERO;
    private String localTime;
    private String localDate;
    private String expireDate;
    private PosEntryMode posEntryMode;
    private String posConditionCode;
    private String track2Data;
    private String additionalData;
    private String currencyCode;
    private String pinBlock;
    private String emvData;
    private String retrievalReferenceNumber;
    private String cardSequenceNumber;

    public String getPan() {
        return pan;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }


    public String getLocalTime() {
        return localTime;
    }

    public String getLocalDate() {
        return localDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public String getTrack2Data() {
        return track2Data;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getPinBlock() {
        return pinBlock;
    }

    public String getEmvData() {
        return emvData;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }
    public String getCardSequenceNumber() {
        return cardSequenceNumber;
    }

    public static final class Builder {
        private String pan;
        private BigDecimal transactionAmount = BigDecimal.ZERO;
        private String localTime;
        private String localDate;
        private String expireDate;
        private PosEntryMode posEntryMode;
        private String posConditionCode;
        private String track2Data;
        private String additionalData;
        private String currencyCode;
        private String pinBlock;
        private String emvData;
        private String retrievalReferenceNumber;
        private String cardSequenceNumber;

        private Builder() {
        }

        public static BalanceInquiryRequest.Builder newInstance() {
            return new BalanceInquiryRequest.Builder();
        }


        public BalanceInquiryRequest.Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public BalanceInquiryRequest.Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public BalanceInquiryRequest.Builder localTime(String localTime) {
            this.localTime = localTime;
            return this;
        }

        public BalanceInquiryRequest.Builder localDate(String localDate) {
            this.localDate = localDate;
            return this;
        }

        public BalanceInquiryRequest.Builder expireDate(String expiryDate) {
            this.expireDate = expiryDate;
            return this;
        }


        public BalanceInquiryRequest.Builder posEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public BalanceInquiryRequest.Builder posConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public BalanceInquiryRequest.Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public BalanceInquiryRequest.Builder additionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public BalanceInquiryRequest.Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public BalanceInquiryRequest.Builder pinBlock(String pinBlock) {
            this.pinBlock = pinBlock;
            return this;
        }

        public BalanceInquiryRequest.Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public BalanceInquiryRequest.Builder rrn(String rrn) {
            this.retrievalReferenceNumber = rrn;
            return this;
        }

        public BalanceInquiryRequest.Builder cardSequenceNumber(String cardSequenceNumber) {
            this.cardSequenceNumber = cardSequenceNumber;
            return this;
        }



        public BalanceInquiryRequest build() {
            BalanceInquiryRequest BalanceInquiryRequest = new BalanceInquiryRequest();
            BalanceInquiryRequest.pan = this.pan;
            BalanceInquiryRequest.transactionAmount = this.transactionAmount;
            BalanceInquiryRequest.localDate = this.localDate;
            BalanceInquiryRequest.localTime = this.localTime;
            BalanceInquiryRequest.expireDate = this.expireDate;
            BalanceInquiryRequest.posEntryMode = this.posEntryMode;
            BalanceInquiryRequest.posConditionCode = this.posConditionCode;
            BalanceInquiryRequest.track2Data = this.track2Data;
            BalanceInquiryRequest.additionalData = this.additionalData;
            BalanceInquiryRequest.currencyCode = this.currencyCode;
            BalanceInquiryRequest.pinBlock = this.pinBlock;
            BalanceInquiryRequest.emvData = this.emvData;
            BalanceInquiryRequest.retrievalReferenceNumber = this.retrievalReferenceNumber;
            BalanceInquiryRequest.cardSequenceNumber = this.cardSequenceNumber;

            return BalanceInquiryRequest;
        }
    }
}
