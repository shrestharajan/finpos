package global.citytech.finpos.nibl.iso8583.requestsender.preauth;

import org.junit.Test;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.NIBLHostConfig;
import global.citytech.finpos.nibl.iso8583.MockValues;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;

import static org.junit.Assert.*;

/**
 * Created by Rishav Chudal on 10/16/20.
 */
public class PICCPreAuthRequestSenderTest {
    private Logger logger = Logger.getLogger(PICCPreAuthRequestSenderTest.class.getName());

    @Test
    public void send() {
        try {
            String terminalID = "99991002";
            String acquirerID = "405635000000002";
            TerminalInfo terminalInfo = new TerminalInfo(
                    terminalID,
                    acquirerID,"",
                    "Citytech",
                    "Kamaladi"
            );
            HostInfo primaryHost = NIBLHostConfig.getPrimaryConfig();
            HostInfo secondaryHost = NIBLHostConfig.getSecondaryConfig();
            ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);
            RequestContext context = new RequestContext(
                    "000405",
                    terminalInfo,
                    connectionParam
            );

            PreAuthRequest request = new PreAuthRequest();
            request.setTransactionAmount(105.0);
            request.setPosEntryMode(PosEntryMode.PICC);
            request.setPosConditionCode("00");
            request.setPinBlock("c247220c967c1d");
            request.setInvoiceNumber(MockValues.INVOICE_NUMBER);
            request.setTrack2Data(MockValues.TRACK_2_DATA);
            request.setCardSequenceNumber("01");
            request.setEmvData("9F26084266DA1FF65DFD3F9F2701809F100706011203A000009F3704FA0077CA9F36020002950502800088009A032008119C01009F02060000001200005F2A020524820258009F1A0205249F03060000000000009F3303E0F0C89F34031E03009F3501229F1E0845303139353438398407A00000000310109F090201409F4104000000045F340101");


            context.setRequest(request);

            PICCPreAuthRequestSender sender = new PICCPreAuthRequestSender(
                    new NIBLSpecInfo(),
                    context
            );
            IsoMessageResponse response = sender.send();
            assertNotNull(response);
        } catch (FinPosException e) {
            logger.log(e.getMessage());
            logger.log(e.getDetailMessage());
        }
    }
}