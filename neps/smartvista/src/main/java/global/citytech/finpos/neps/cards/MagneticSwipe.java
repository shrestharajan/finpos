package global.citytech.finpos.neps.cards;

public class MagneticSwipe {

    private static MagneticSwipe INSTANCE = null;
    
    private int count;

    private MagneticSwipe() {
    }
    
    public static MagneticSwipe getInstance(){
        if (INSTANCE == null)
            INSTANCE = new MagneticSwipe();
        return INSTANCE;
    }

    public boolean isMagneticSwipeAgainAllowed() {
        return this.count < 3;
    }

    public void onMagneticSwipeFailure() {
        this.count++;
    }

    public void onMagneticSwipeSuccess() {
        this.count = 0;
    }

    public void onMagneticSwipeFailureLimitReached() {
        this.count = 0;
    }
}
