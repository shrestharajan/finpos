package global.citytech.neps.presentation.splash

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.common.extensions.openActivity
import global.citytech.easydroid.core.extension.showSuccessMessage
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.easydroid.core.utils.MessageConfig
import global.citytech.neps.BR
import global.citytech.neps.R
import global.citytech.neps.databinding.ActivitySplashScreenBinding
import global.citytech.neps.presentation.idle.IdleActivity
import kotlinx.android.synthetic.main.activity_splash_screen.*
import kotlin.system.exitProcess

class SplashScreenActivity : AppBaseActivity<ActivitySplashScreenBinding, SplashViewModel>() {

    private val SPLASH_TIME_OUT: Long = 3000
    private lateinit var viewModel:SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel()
        initObservers()
        initializeSdk()
    }

    private fun initObservers() {
        viewModel.isLoading.observe(this, Observer {
            if (!it) {
                makeConfiguringProgress(View.INVISIBLE)
            } else {
                makeConfiguringProgress(View.VISIBLE)
            }
        })

        viewModel.errorMessage.observe(this, Observer {
            showErrorMessageAndExit(it)
        })

        viewModel.isTerminalConfigured.observe(this, Observer {
            if (it) {
                Handler().postDelayed({
                    openActivity(IdleActivity::class.java)
                    finish()
                }, SPLASH_TIME_OUT)
            } else {
                showErrorMessageAndExit(getString(R.string.msg_not_configured))
            }
        })
    }

    private fun initializeSdk() {
        makeConfiguringProgress(View.VISIBLE)
        viewModel.initializeSdk()
    }

    private fun makeConfiguringProgress(visibility: Int) {
        tv_progress.visibility = visibility
        tv_progress.text = getString(R.string.msg_configure_terminal)
        pb_configure_terminal.visibility = visibility
    }

    private fun showErrorMessageAndExit(it: String?){
        if (!it.isNullOrEmpty()) {
            val messageConfig = MessageConfig.Builder()
                .message(getString(R.string.msg_not_configured))
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    dialogInterface, _ -> dialogInterface.dismiss()
                    closeApplication()
                }
            showSuccessMessage(messageConfig)
        }
    }

    private fun closeApplication() {
        finishAndRemoveTask()
        exitProcess(0)
    }

    override fun getBindingVariable() =  BR.splashViewModel

    override fun getLayout() = R.layout.activity_splash_screen

    override fun getViewModel(): SplashViewModel =
        ViewModelProviders.of(this).get(SplashViewModel::class.java)


}
