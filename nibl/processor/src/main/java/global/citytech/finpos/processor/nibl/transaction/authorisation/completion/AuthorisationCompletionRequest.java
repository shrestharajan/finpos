package global.citytech.finpos.processor.nibl.transaction.authorisation.completion;

import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;

/**
 * Created by Unique Shakya on 9/29/2020.
 */
public class AuthorisationCompletionRequest extends TransactionRequest {

    private String originalInvoiceNumber;

    public AuthorisationCompletionRequest(TransactionType transactionType) {
        super(transactionType);
    }

    public String getOriginalInvoiceNumber() {
        return originalInvoiceNumber;
    }

    public void setOriginalInvoiceNumber(String originalInvoiceNumber) {
        this.originalInvoiceNumber = originalInvoiceNumber;
    }
}
