package global.citytech.finpos.processor.nibl.transaction.tipadjustment;

import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.tip.TipAdjustmentRequestSender;
import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finpos.processor.nibl.transaction.template.CardLessTransactionUseCase;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.utility.IsoMessageUtils;

/**
 * Created by Unique Shakya on 6/11/2021.
 */
public class TipAdjustmentUseCase extends CardLessTransactionUseCase {

    public TipAdjustmentUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    protected String retrieveInvoiceNumber() {
        return this.originalTransactionLog.getInvoiceNumber();
    }



    @Override
    protected TransactionLog retrieveTransactionLogToStore(boolean isApproved, IsoMessageResponse isoMessageResponse) {
        return TransactionLog.Builder.newInstance()
                .withStan(this.stan)
                .withTerminalId(this.terminalRepository.findTerminalInfo().getTerminalID())
                .withMerchantId(this.terminalRepository.findTerminalInfo().getMerchantID())
                .withInvoiceNumber(this.invoiceNumber)
                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RETRIEVAL_REFERENCE_NUMBER))
                .withAuthCode(IsoMessageUtils.retrieveFromRequestDataElementAsString(isoMessageResponse, DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
                .withTransactionType(TransactionType.TIP_ADJUSTMENT)
                .withOriginalTransactionType(this.originalTransactionLog.getTransactionType())
                .withTransactionAmount(this.transactionRequest.getAdditionalAmount())
                .withOriginalTransactionAmount(this.originalTransactionLog.getTransactionAmount())
                .withTransactionDate(retrieveTransactionDate(isoMessageResponse))
                .withTransactionTime(retrieveTransactionTime(isoMessageResponse))
                .withTransactionStatus(isApproved ? APPROVED : DECLINED)
                .withPosEntryMode(this.originalTransactionLog.getPosEntryMode())
                .withOriginalPosEntryMode(this.originalTransactionLog.getPosEntryMode())
                .withPosConditionCode(this.transactionIsoRequest.getPosConditionCode())
                .withOriginalPosConditionCode(this.originalTransactionLog.getPosConditionCode())
                .withReconcileBatchNo(this.batchNumber)
                .withReadCardResponse(this.readCardResponse)
                .withAuthorizationCompleted(true)
                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                .withReceiptLog(this.receiptLog)
                .withTransactionVoided(false)
                .withTipAdjusted(false)
                .withOriginalTransactionReferenceNumber(this.originalTransactionLog.getInvoiceNumber())
                .withCurrencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .withVatInfo(originalTransactionLog.getVatInfo())
                .build();
    }

    @Override
    protected TransactionLog retrieveOriginalTransactionLog() {
        String originalInvoiceNumber = this.request.getTransactionRequest().getOriginalInvoiceNumber();
        String reconciliationBatchNumber = this.terminalRepository.getReconciliationBatchNumber();
        return this.request.getTransactionRepository()
                .getTransactionLogWithInvoiceNumber(originalInvoiceNumber,
                        reconciliationBatchNumber);
    }

    @Override
    protected void validateTransactionLog() {
        if (this.originalTransactionLog.getTransactionType() != TransactionType.PURCHASE)
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_FOUND);
        if (!this.originalTransactionLog.getTransactionStatus().equalsIgnoreCase(APPROVED))
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_FOUND);
    }

    @Override
    protected void setTotalTransactionAmount() {
        this.totalTransactionAmount = this.request.getTransactionRequest().getAmount();
    }

    @Override
    protected String getTransactionTypeClassName() {
        return TipAdjustmentUseCase.class.getName();
    }

    @Override
    protected TransactionIsoRequest prepareTransactionIsoRequest() {
        CardDetails cardDetails = this.readCardResponse.getCardDetails();
        return TransactionIsoRequest.Builder.newInstance()
                .pan(cardDetails.getPrimaryAccountNumber())
                .transactionAmount(this.transactionRequest.getAdditionalAmount().add(this.originalTransactionLog.getTransactionAmount()))
                .tipAmount(this.transactionRequest.getAdditionalAmount())
                .localDate(this.originalTransactionLog.getTransactionDate())
                .localTime(this.originalTransactionLog.getTransactionTime())
                .expireDate(cardDetails.getExpiryDate())
                .posEntryMode(this.originalTransactionLog.getPosEntryMode())
                .posConditionCode(this.originalTransactionLog.getPosConditionCode())
                .originalRetrievalReferenceNumber(this.originalTransactionLog.getRrn())
                .originalApprovalCode(this.originalTransactionLog.getAuthCode())
                .originalResponseCode(this.originalTransactionLog.getResponseCode())
                .emvData(cardDetails.getIccDataBlock())
                .originalAmount(this.originalTransactionLog.getTransactionAmount())
                .originalInvoiceNumber(this.transactionRequest.getOriginalInvoiceNumber())
                .build();
    }

    @Override
    protected NIBLMessageSenderTemplate getRequestSender(RequestContext context) {
        return new TipAdjustmentRequestSender(new NIBLSpecInfo(), context);
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.TIP_ADJUSTMENT.getCode();
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy, String miniStatementData) {
       return null;
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy) {
        return TipAdjustmentResponseModel.Builder.newInstance()
                .approved(isApproved)
                .stan(this.stan)
                .message(this.retrieveMessage(isoMessageResponse, isApproved))
                .debugRequestMessage(isoMessageResponse.getDebugRequestString())
                .debugResponseMessage(isoMessageResponse.getDebugResponseString())
                .shouldPrintCustomerCopy(shouldPrintCustomerCopy)
                .build();
    }

    @Override
    protected TransactionResponseModel prepareDeclinedTransactionResponse(String message, boolean shouldPrintReceipt) {
        return TipAdjustmentResponseModel.Builder.newInstance()
                .approved(false)
                .message(message)
                .stan(this.stan)
                .shouldPrintCustomerCopy(shouldPrintReceipt)
                .build();
    }

    @Override
    protected String getOriginalTransactionReferenceNumber() {
        return this.transactionIsoRequest.getOriginalRetrievalReferenceNumber();
    }

    @Override
    protected boolean isAuthorizationCompleted() {
        return true;
    }

    @Override
    protected void updateTransactionLogOfOriginalTransaction(boolean isApproved) {
        this.request.getTransactionRepository().updateTipAdjustedStatusForGivenTransactionDetails(
                isApproved, this.originalTransactionLog.getTransactionType(), this.originalTransactionLog.getInvoiceNumber()
        );
    }

    @Override
    protected boolean shouldIncrementInvoiceNumber(boolean isApproved) {
        return false;
    }
}
