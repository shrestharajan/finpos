package global.citytech.finpos.neps.iso8583.requestsender.purchases;


import java.math.BigDecimal;

import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;

/**
 * Created by Saurav Ghimire on 1/21/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class PurchaseIsoRequest extends TransactionIsoRequest {
    public static final class Builder {
        private String pan;
        private BigDecimal transactionAmount;
        private String track2Data;
        private String additionalData;
        private String currencyCode;
        private String pinBlock;
        private String emvData;
        private String cvv;
        private boolean isFallbackFromIccToMag;
        private String localDateTime;
        private String expiryDate;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public Builder localDateTime(String localDataTime) {
            this.localDateTime = localDataTime;
            return this;
        }

        public Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public Builder additionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder pinBlock(String pinBlock) {
            this.pinBlock = pinBlock;
            return this;
        }

        public Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }
        public Builder cvv(String cvv) {
            this.cvv = cvv;
            return this;
        }

        public Builder fallBackFromIccToMag(boolean isFallbackFromIccToMag) {
            this.isFallbackFromIccToMag = isFallbackFromIccToMag;
            return this;
        }

        public Builder expiryDate(String expiryDate) {
            this.expiryDate = expiryDate;
            return this;
        }

        public PurchaseIsoRequest build() {
            PurchaseIsoRequest purchaseIsoRequest = new PurchaseIsoRequest();
            purchaseIsoRequest.pinBlock = this.pinBlock;
            purchaseIsoRequest.currencyCode = this.currencyCode;
            purchaseIsoRequest.pan = this.pan;
            purchaseIsoRequest.track2Data = this.track2Data;
            purchaseIsoRequest.transactionAmount = this.transactionAmount;
            purchaseIsoRequest.emvData = this.emvData;
            purchaseIsoRequest.additionalData = this.additionalData;
            purchaseIsoRequest.isFallbackFromIccToMag = this.isFallbackFromIccToMag;
            purchaseIsoRequest.localDateTime = this.localDateTime;
            purchaseIsoRequest.cvv = this.cvv;
            purchaseIsoRequest.expiryDate = this.expiryDate;
            return purchaseIsoRequest;
        }
    }
}
