package global.citytech.finposframework.hardware.utility

/**
 * Created by Rishav Chudal on 6/3/20.
 */
object DeviceApiConstants {
    const val DEFAULT_CARD_TIMEOUT_IN_SECS = 30
    const val TRACK_2_SEPARATOR_TYPE_EQUALS_TO = "="
    const val TRACK_2_SEPARATOR_TYPE_D = "D"
    const val TRACK_1_SEPARATOR_TYPE_CARRET = "^"
    const val MULTI_AID_LIST_EMPTY = -1
    const val MULTI_AID_SELECTION_CANCELLED = -2
    const val TRACK_2_DATA_MAX_LENGTH = 37

    /**
     * KEY INDEX Constants
     */
    const val MASTER_KEY_INDEX = 0x01
    const val PEK_KEY_INDEX = 0x02
    const val IPEK_KEY_INDEX = 0x03

    /**
     * Kernel Identifiers
     */
    const val PAYPASS_KERNEL_ID = "02" //Visa
    const val PAYWAVE_KERNEL_ID = "03" //MasterCard
    const val QPBOC_KERNEL_ID = "07" //Union Pay

    /**
     *  Transaction Type 9C
     */
    const val TRANSACTION_TYPE_PURCHASE_SALE = "00"

    /**
     * Messages
     */
    const val MSG_SUCCESS_SDK_INIT = "Success in SDK Initialization"
    const val MSG_FAILURE_SDK_INIT = "Failure in SDK Initialization"
    const val MSG_SUCCESS_KEY_INJECT = "Success in KEY Injection"
    const val MSG_FAILURE_KEY_INJECT = "Failure in KEY Injection"
    const val MSG_SUCCESS_KEY_CHECK = "Success, KEY Exist"
    const val MSG_FAILURE_KEY_CHECK = "Failure, KEY Does Not Exist"
    const val MSG_SUCCESS_KEY_ERASE = "Success in KEY Erase"
    const val MSG_FAILURE_KEY_ERASE = "Failure in KEY Erase"
    const val MSG_FAILURE_INVALID_PACKAGE_NAME = "Provided Package Name is Not Valid"
    const val MSG_FAILURE_INVALID_KEY = "Provided Key is Not Valid"
    const val KEY_INJECTION_KEY_INDEX_INVALID = "Key Injection KEY INDEX is Invalid"
    const val KEY_INJECTION_PROTECT_KEY_INDEX_INVALID = "Key Injection PROTECT KEY INDEX is Invalid"
    const val MSG_FAILURE_EMV_PARAM_INJECT = "Failure in EMV Param Injection"
    const val MSG_SUCCESS_EMV_PARAM_INJECT = "Success in EMV Param Injection"
    const val MSG_MSG_NO_CARD_DETECTED = "No Card Detected"
    const val MSG_SUCCESS_EMV_KEYS_INJECTION = "Success in Emv Keys Injection"
    const val MSG_FAILURE_EMV_KEYS_INJECTION = "Failure in Emv Keys Injection"
    const val MSG_EMPTY_EMV_KEYS_LIST = "Emv Keys List is Empty"
    const val MSG_SUCCESS_EMV_KEYS_ERASE = "Success in Erasing All EMV Keys"
    const val MSG_FAILURE_EMV_KEYS_ERASE = "Failure in Erasing All EMV Keys"
    const val MSG_SUCCESS_REVOKED_KEYS_INJECTION = "Success in Revoked Keys Injection"
    const val MSG_FAILURE_REVOKED_KEYS_INJECTION = "Failure in Revoked Keys Injection"
    const val MSG_EMPTY_REVOKED_KEYS_LIST = "Revoked Keys List is Empty"
    const val MSG_SUCCESS_REVOKED_KEYS_ERASE = "Success in Erasing Revoked Keys"
    const val MSG_FAILURE_REVOKED_KEYS_ERASE = "Failure in Erasing Revoked Keys"
    const val MSG_EMPTY_AID_LIST = "AID List is Empty"
    const val MSG_SUCCESS_AID_INJECTION = "Success in AID Injection"
    const val MSG_FAILURE_AID_INJECTION = "Failure in AID Injection"
    const val MSG_FAILURE_AID_ERASE = "Failure in Erasing All AID Data"
    const val MSG_SUCCESS_AID_ERASE = "Success in Erasing All AID Data"
    const val MSG_NO_CARD_DETECTED = "No Card Detected"
    const val CARD_DETECT_FAILED = "Card Detection Failed"
    const val MSG_SLOT_OPEN_EMPTY = "Slots to open is emtpy"
    const val MSG_CARD_DETECT_TIMEOUT = "Card Detect TimeOut"
    const val MSG_MAG_CARD_DETECTED = "Magnetic Card Detected"
    const val MSG_CONTACT_CARD_DETECTED = "Contact Card Detected"
    const val MSG_CONTACTLESS_CARD_DETECTED = "ContactLess Card Detected"
    const val MSG_MAG_CARD_READ_SUCCESS = "Magnetic Card Read Success"
    const val MSG_CARD_READ_FAILURE = "Card Read Failure"
    const val MSG_PHASE_READ_CARD_SUCCESS = "Phase Read Card Success"
    const val MSG_PINPAD_CANCELLED = "PinPad Cancelled"
    const val MSG_PINPAD_PAN_ERROR = "PinPad PAN Error"
    const val MSG_PINPAD_FAILURE = "PinPad Failure"
    const val MSG_TRANSACTION_TIMEOUT = "Transaction TimeOut"
    const val MSG_PHASE_PROCESS_EMV_SUCCESS = "Phase Process Emv Success"
    const val MSG_EMV_PARAMETERS_INJECT_SUCCESS = "Emv Parameters Injection Success"
    const val MSG_TRANSACTION_COMPLETE_APPROVED = "Transaction Completed And Approved"
    const val MSG_TRANSACTION_COMPLETE_DECLINED = "Transaction Completed And Declined"
    const val MSG_APPLICATION_BLOCKED = "APPLICATION IS BLOCKED"
    const val MSG_CARD_BLOCKED = "CARD IS BLOCKED"
    const val MSG_PROCESSING_ERROR = "PROCESSING ERROR"
    const val MSG_CONTACT_FALLBACK_TO_MAGNETIC = "CONTACT FALLBACK TO MAGNETIC"
    const val MSG_CL_FALLBACK_TO_CONTACT = "CONTACTLESS FALLBACK TO CONTACT"
    const val MSG_TRANSACTION_PROCESSING_FAILED = "FAIL TO PROCESS TRANSACTION"
    const val MSG_WAVE_AGAIN = "PLEASE WAVE AGAIN"
    const val MSG_SEE_PHONE = "See Phone"
    const val MSG_SWIPE_CARD_AGAIN = "SWIPE CARD AGAIN"
    const val APPLICATION_SELECTION_CANCELLED = "Application Selection Cancelled"
    const val APPLICATION_SELECTION_FAILED = "Application Selection Failed"
    const val MSG_CALLBACK_AMOUNT = "CALLBACK FOR AMOUNT ENTRY"
    const val MSG_ICC_DATA_BLOCK_PREPARED = "ICC Data Block Prepared"
    const val MSG_PIN_INPUT_TIMEOUT = "Pin Input TimeOut"
    const val MSG_REVOKED_CERT_INJECT_FAILED = "Revoked Certificates List Injection Failed"
    const val MSG_REVOKED_CERT_LIST_EMPTY = "Revoked Certificates List is Empty"
    const val MSG_REVOKED_CERT_INJECT_SUCCESS = "Revoked Certificates List Injection Success"
    const val MSG_CLEAN_UP_COMPLETED = "Clean Up Completed"
    const val MSG_SYSTEM_SERVICE_SUCCESS = "System Service Success"
    const val MSG_SYSTEM_SERVICE_FAILURE = "System Service Failure"
    const val MSG_FEATURE_NOT_AVAILABLE = "Feature Not Available"
    const val MSG_KEY_NOT_SUPPORT = "Key Not Supported for Key Injection"
    const val MSG_ALG_NOT_SUPPORT = "Algorithm Not Supported for Key Injection"
    const val LED_ACTION_FAILURE = "Led Action Failure"
    const val LED_ACTION_SUCCESS = "Led Action Success"
    const val MSG_ONLINE_APPROVED_CARD_DECLINED = "Online Issuer Approved but Card Declined"
    const val MSG_READING_CARD = "Reading Card..."
    const val MSG_PLEASE_WAIT = "Please wait.."
    const val MSG_ICC_READER_CARD_DETECT_ERROR = " Error in detecting card in Icc reader"
    const val MSG_CARD_NOT_SUPPORTED = "Card Not Supported"

    /**
     * Printer
     */
    const val CORE_PRINTER_NOT_AVAILABLE = "0"
    const val CORE_PRINTER_OUT_OF_PAPER = "1"
    const val CORE_PRINTER_PLAIN_RECEIPT_PAPER = "2"
    /**
     * Mag Reader
     */
    const val CORE_MAG_READER_OK = "0"
    const val CORE_MAG_READER_OUT_OF_ORDER = "1"
    const val CORE_MAG_READER_NOT_SUPPORTED = "9" //Extra added, Not in Mada
    /**
     * Contact Reader
     */
    const val CORE_ICC_READER_OK = "0"
    const val CORE_ICC_READER_OUT_OF_ORDER = "1"
    const val CORE_ICC_READER_NOT_SUPPORTED = "9" //Extra added, Not in Mada
    /**
     * Contactless Reader
     */
    const val CORE_PICC_READER_OK = "0"
    const val CORE_PICC_READER_OUT_OF_ORDER = "1"
    const val CORE_PICC_READER_NOT_SUPPORTED = "9"
    /**
     * Pin Pad Min and Max Length
     */
    const val DEFAULT_PIN_MIN_LENGTH = 4
    const val DEFAULT_PIN_MAX_LENGTH = 12
}