package global.citytech.finposframework.repositories;

public enum SettlementStatus {
    OPEN,
    BATCH_UPLOAD_IN_PROGRESS,
    CLOSURE_IN_PROGRESS
}
