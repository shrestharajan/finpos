//package global.citytech.finpos.processor.nibl.tipadjusment;
//
//import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
//import global.citytech.finpos.nibl.iso8583.ProcessingCode;
//import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
//import global.citytech.finpos.nibl.iso8583.requestsender.tip.S2MTipAdjustmentRequest;
//import global.citytech.finpos.nibl.iso8583.requestsender.tip.TipAdjustmentRequestSender;
//import global.citytech.finpos.processor.nibl.reversal.ReversalRequestModel;
//import global.citytech.finpos.processor.nibl.reversal.ReversalUseCase;
//import global.citytech.finpos.processor.nibl.transaction.NiblTransactionReceiptPrintHandler;
//import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
//import global.citytech.finpos.processor.nibl.transaction.ReversedTransactionReceiptHandler;
//import global.citytech.finpos.processor.nibl.transaction.TransactionUseCase;
//import global.citytech.finposframework.exceptions.FinPosException;
//import global.citytech.finposframework.exceptions.PosError;
//import global.citytech.finposframework.exceptions.PosException;
//import global.citytech.finposframework.hardware.io.cards.CardDetails;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
//import global.citytech.finposframework.iso8583.DataElement;
//import global.citytech.finposframework.iso8583.IsoMessageResponse;
//import global.citytech.finposframework.iso8583.RequestContext;
//import global.citytech.finposframework.notifier.Notifier;
//import global.citytech.finposframework.repositories.TerminalRepository;
//import global.citytech.finposframework.supports.UseCase;
//import global.citytech.finposframework.switches.tipadjustment.TipAdjustmentRequester;
//import global.citytech.finposframework.usecases.TransactionType;
//import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
//import global.citytech.finposframework.usecases.transaction.TransactionLog;
//import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
//import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
//import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
//import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
//import global.citytech.finposframework.utility.DateUtils;
//import global.citytech.finposframework.utility.IsoMessageUtils;
//import global.citytech.finposframework.utility.JsonUtils;
//import global.citytech.finposframework.utility.StringUtils;
//
///**
// * Created by Unique Shakya on 5/4/2021.
// */
//public class TipAdjustmentUseCase extends TransactionUseCase implements TipAdjustmentRequester<TipAdjustmentRequestModel, TipAdjustmentResponseModel>,
//        UseCase<TipAdjustmentRequestModel, TipAdjustmentResponseModel> {
//
//    private TipAdjustmentRequestModel requestModel;
//    private TransactionLog transactionLogByInvoiceNumber;
//    private S2MTipAdjustmentRequest s2MTipAdjustmentRequest;
//    private String stan;
//    private String invoiceNumber;
//    private String reconciliationBatchNumber;
//    private TransactionRequest transactionRequest;
//    private ReadCardResponse readCardResponse;
//
//    public TipAdjustmentUseCase(TerminalRepository terminalRepository, Notifier notifier) {
//        super(terminalRepository, notifier);
//    }
//
//    @Override
//    protected String getClassName() {
//        return TipAdjustmentUseCase.class.getName();
//    }
//
//    @Override
//    public TipAdjustmentResponseModel execute(TipAdjustmentRequestModel tipAdjustmentRequestModel) {
//        logger.log("::: REACHED TIP ADJUSTMENT USE CASE :::");
//        this.requestModel = tipAdjustmentRequestModel;
//        try {
//            if (!this.isDeviceReady(this.requestModel.getDeviceController(),
//                    TransactionType.TIP_ADJUSTMENT))
//                throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
//            this.stan = this.terminalRepository.getSystemTraceAuditNumber();
//            this.reconciliationBatchNumber = this.terminalRepository.getReconciliationBatchNumber();
//            this.transactionRequest = this.requestModel.getTransactionRequest();
//            this.transactionRepository = this.requestModel.getTransactionRepository();
//            this.invoiceNumber = this.transactionRequest.getOriginalInvoiceNumber();
//            String originalInvoiceNumber = this.transactionRequest.getOriginalInvoiceNumber();
//            this.initTransaction(this.requestModel.getApplicationRepository(), this.transactionRequest);
//            this.transactionLogByInvoiceNumber = this.getTransactionLogByInvoiceNumber(originalInvoiceNumber);
//            this.validateTransactionLog();
//            this.prepareReadCardResponse();
//            return this.initiateTransactionWithHost();
//        } catch (PosException pe) {
//            pe.printStackTrace();
//            throw pe;
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        }
//    }
//
//    private void prepareReadCardResponse() {
//        this.readCardResponse = this.transactionLogByInvoiceNumber.getReadCardResponse();
//        this.readCardResponse.getCardDetails().setAmount(this.transactionRequest.getAmount());
//        this.readCardResponse.getCardDetails().setCashBackAmount(this.transactionRequest.getAdditionalAmount());
//        this.readCardResponse.getCardDetails().setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
//    }
//
//    private TipAdjustmentResponseModel initiateTransactionWithHost() {
//        this.notifier.notify(Notifier.EventType.TRANSMITTING_REQUEST,
//                Notifier.EventType.TRANSMITTING_REQUEST.getDescription());
//        try {
//            IsoMessageResponse isoMessageResponse = this.sendIsoMessage();
//            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait ...");
//            InvalidResponseHandlerResponse invalidResponseHandlerResponse =
//                    this.checkForInvalidResponseFromHost(isoMessageResponse);
//            if (invalidResponseHandlerResponse.isInvalid())
//                return this.onInvalidResponseReceivedFromHost(invalidResponseHandlerResponse);
//            boolean isTipAdjustmentApproved = this.isTransactionApprovedByActionCode(isoMessageResponse);
//            this.notifyTransactionCompletion(isTipAdjustmentApproved, isoMessageResponse);
//            boolean shouldPrint = this.printReceipt(this.reconciliationBatchNumber, this.stan,
//                    this.invoiceNumber, this.requestModel.getPrinterService(),
//                    this.transactionRequest, this.requestModel.getTransactionRepository(),
//                    this.readCardResponse,
//                    isoMessageResponse, this.requestModel.getApplicationRepository());
//            return this.prepareTipAdjustmentResponseModel(isoMessageResponse, isTipAdjustmentApproved,
//                    shouldPrint);
//        } catch (FinPosException fe) {
//            fe.printStackTrace();
//            if (isResponseNotReceivedException(fe)) {
//                return this.onTransactionUnableToGoOnline();
//            } else if (fe.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT) {
//                logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED UNABLE TO CONNECT");
//                transactionRepository.removeAutoReversal(this.autoReversal);
//                throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
//            } else {
//                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//            }
//        }
//    }
//
//    private TipAdjustmentResponseModel onTransactionUnableToGoOnline() {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, "TIMEOUT");
//        this.performReversal();
//        boolean shouldPrint = !this.approvePrintOnly(this.requestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(this.transactionRequest,
//                    this.readCardResponse, this.terminalRepository)
//                    .prepare(this.reconciliationBatchNumber, this.stan, this.invoiceNumber,
//                            new ReasonForReversal(ReasonForReversal.Type.TIMEOUT, "Timeout"));
//            new NiblTransactionReceiptPrintHandler(this.requestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            this.requestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return TipAdjustmentResponseModel.Builder.newInstance()
//                .message("TIMEOUT")
//                .approved(false)
//                .shouldPrintCustomerCopy(shouldPrint)
//                .stan(this.stan)
//                .build();
//    }
//
//    private void performReversal() {
//        ReversalUseCase reversalUseCase = new ReversalUseCase(this.prepareReversalRequest(),
//                this.terminalRepository, this.notifier);
//        try {
//            reversalUseCase.execute(this.prepareReversalRequestModel());
//            this.transactionRepository.removeAutoReversal(this.autoReversal);
//        } catch (FinPosException fe) {
//            fe.printStackTrace();
//        }
//    }
//
//    private ReversalRequestModel prepareReversalRequestModel() {
//        return ReversalRequestModel.Builder.newInstance()
//                .withTransactionRequest(this.transactionRequest)
//                .withTransactionRepository(this.requestModel.getTransactionRepository())
//                .withReadCardResponse(this.readCardResponse)
//                .withPrinterService(this.requestModel.getPrinterService())
//                .withApplicationRepository(this.requestModel.getApplicationRepository())
//                .build();
//    }
//
//    private NiblReversalRequest prepareReversalRequest() {
//        NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
//        niblReversalRequest.setProcessingCode(ProcessingCode.VOID_SALE.getCode());
//        niblReversalRequest.setAmount(this.s2MTipAdjustmentRequest.getAmount());
//        niblReversalRequest.setPan(this.s2MTipAdjustmentRequest.getPan());
//        niblReversalRequest.setExpiryDate(this.s2MTipAdjustmentRequest.getExpireDate());
//        niblReversalRequest.setPosEntryMode(this.s2MTipAdjustmentRequest.getPosEntryMode());
//        niblReversalRequest.setPosConditionCode(this.s2MTipAdjustmentRequest.getPosConditionCode());
//        niblReversalRequest.setTrack2Data(this.readCardResponse.getCardDetails().getTrackTwoData());
//        niblReversalRequest.setPinBlock(this.readCardResponse.getCardDetails().getPinBlock().getPin());
//        niblReversalRequest.setInvoiceNumber(this.transactionLogByInvoiceNumber.getInvoiceNumber());
//        niblReversalRequest.setStan(stan);
//        return niblReversalRequest;
//    }
//
//    private TipAdjustmentResponseModel prepareTipAdjustmentResponseModel(IsoMessageResponse isoMessageResponse, boolean isTipAdjustmentApproved, boolean shouldPrint) {
//        return TipAdjustmentResponseModel.Builder.newInstance()
//                .approved(isTipAdjustmentApproved)
//                .message(this.retrieveMessage(isoMessageResponse, isTipAdjustmentApproved, this.requestModel.getApplicationRepository()))
//                .stan(this.stan)
//                .shouldPrintCustomerCopy(shouldPrint)
//                .debugRequestMessage(isoMessageResponse.getDebugRequestString())
//                .debugResponseMessage(isoMessageResponse.getDebugResponseString())
//                .build();
//    }
//
//    private void notifyTransactionCompletion(boolean isTipAdjustmentApproved, IsoMessageResponse isoMessageResponse) {
//        this.updateTransactionLog(isTipAdjustmentApproved, isoMessageResponse);
//        this.updateTransactionIds(false);
//        this.notifyTransactionStatus(isTipAdjustmentApproved, isoMessageResponse,
//                this.requestModel.getApplicationRepository(), this.requestModel.getLedService(), false);
//    }
//
//    private void updateTransactionLog(boolean isTipAdjustmentApproved, IsoMessageResponse isoMessageResponse) {
//        logger.log("::: TIP ADJUSTMENT ::: BATCH NUMBER AFTER UPDATE >>> " + this.reconciliationBatchNumber);
//        logger.log("::: TIP ADJUSTMENT ::: INVOICE NUMBER AFTER UPDATE >>> " + this.invoiceNumber);
//
//        String transactionDate = IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.LOCAL_DATE);
//        if (StringUtils.isEmpty(transactionDate))
//            transactionDate = IsoMessageUtils.retrieveFromRequestDataElementAsString(isoMessageResponse, DataElement.LOCAL_DATE);
//
//        String transactionTime = IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.LOCAL_TIME);
//        if (StringUtils.isEmpty(transactionTime))
//            transactionTime = IsoMessageUtils.retrieveFromRequestDataElementAsString(isoMessageResponse, DataElement.LOCAL_TIME);
//
//        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
//                .withStan(this.stan)
//                .withTerminalId(this.terminalRepository.findTerminalInfo().getTerminalID())
//                .withMerchantId(this.terminalRepository.findTerminalInfo().getMerchantID())
//                .withInvoiceNumber(this.invoiceNumber)
//                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RETRIEVAL_REFERENCE_NUMBER))
//                .withAuthCode(IsoMessageUtils.retrieveFromRequestDataElementAsString(isoMessageResponse, DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
//                .withTransactionType(TransactionType.TIP_ADJUSTMENT)
//                .withOriginalTransactionType(this.transactionLogByInvoiceNumber.getTransactionType())
//                .withTransactionAmount(this.transactionRequest.getAdditionalAmount())
//                .withOriginalTransactionAmount(this.transactionLogByInvoiceNumber.getTransactionAmount())
//                .withTransactionDate(transactionDate)
//                .withTransactionTime(transactionTime)
//                .withTransactionStatus(isTipAdjustmentApproved ? "APPROVED" : "DECLINED")
//                .withPosEntryMode(this.s2MTipAdjustmentRequest.getPosEntryMode())
//                .withOriginalPosEntryMode(this.transactionLogByInvoiceNumber.getPosEntryMode())
//                .withPosConditionCode(this.s2MTipAdjustmentRequest.getPosConditionCode())
//                .withOriginalPosConditionCode(this.transactionLogByInvoiceNumber.getPosConditionCode())
//                .withReconcileBatchNo(this.reconciliationBatchNumber)
//                .withReadCardResponse(this.readCardResponse)
//                .withAuthorizationCompleted(true)
//                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
//                .withReceiptLog(getReceiptLog(transactionRequest, this.readCardResponse,
//                        isoMessageResponse, requestModel.getApplicationRepository(), reconciliationBatchNumber, stan, invoiceNumber))
//                .withTransactionVoided(false)
//                .withTipAdjusted(false)
//                .withOriginalTransactionReferenceNumber(this.transactionLogByInvoiceNumber.getInvoiceNumber())
//                .build();
//        this.requestModel.getTransactionRepository().updateTransactionLog(transactionLog);
//        this.requestModel.getTransactionRepository().updateTipAdjustedStatusForGivenTransactionDetails(
//                isTipAdjustmentApproved,
//                this.transactionLogByInvoiceNumber.getTransactionType(),
//                this.transactionLogByInvoiceNumber.getInvoiceNumber());
//    }
//
//    private TipAdjustmentResponseModel onInvalidResponseReceivedFromHost(InvalidResponseHandlerResponse invalidResponseHandlerResponse) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, invalidResponseHandlerResponse.getMessage());
//        this.performReversal();
//        boolean shouldPrint = !this.approvePrintOnly(this.requestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(this.transactionRequest,
//                    this.readCardResponse, this.terminalRepository)
//                    .prepare(this.reconciliationBatchNumber, this.stan, this.invoiceNumber,
//                            new ReasonForReversal(ReasonForReversal.Type.INVALID_RESPONSE, invalidResponseHandlerResponse.getMessage()));
//            new NiblTransactionReceiptPrintHandler(this.requestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            this.requestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return TipAdjustmentResponseModel.Builder.newInstance()
//                .approved(false)
//                .message(invalidResponseHandlerResponse.getMessage())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .stan(this.stan)
//                .build();
//    }
//
//    private IsoMessageResponse sendIsoMessage() {
//        RequestContext requestContext = this.prepareTemplateRequestContext(stan);
//        requestContext.setRequest(this.prepareTipAdjustmentRequest());
//        logger.debug(String.format("::: REQUEST CONTEXT ::: %s", requestContext.toString()));
//        TipAdjustmentRequestSender requestSender = new TipAdjustmentRequestSender(new NIBLSpecInfo(),
//                requestContext);
//        this.autoReversal = new AutoReversal(
//                TransactionType.TIP_ADJUSTMENT,
//                JsonUtils.toJsonObj(prepareReversalRequest()),
//                0,
//                AutoReversal.Status.ACTIVE,
//                StringUtils.dateTimeStamp()
//        );
//        this.transactionRepository.saveAutoReversal(this.autoReversal);
//        IsoMessageResponse isoMessageResponse = requestSender.send();
//        this.transactionRepository.removeAutoReversal(this.autoReversal);
//        return isoMessageResponse;
//    }
//
//    private S2MTipAdjustmentRequest prepareTipAdjustmentRequest() {
//        CardDetails cardDetails = this.readCardResponse.getCardDetails();
//        this.s2MTipAdjustmentRequest = S2MTipAdjustmentRequest.Builder.newInstance()
//                .pan(cardDetails.getPrimaryAccountNumber())
//                .amount(this.transactionLogByInvoiceNumber.getTransactionAmount() + this.transactionRequest.getAdditionalAmount())
//                .localDate(this.transactionLogByInvoiceNumber.getTransactionDate())
//                .localTime(this.transactionLogByInvoiceNumber.getTransactionTime())
//                .expireDate(cardDetails.getExpiryDate())
//                .posEntryMode(this.transactionLogByInvoiceNumber.getPosEntryMode())
//                .posConditionCode(this.transactionLogByInvoiceNumber.getPosConditionCode())
//                .originalRrn(this.transactionLogByInvoiceNumber.getRrn())
//                .originalAuthCode(this.transactionLogByInvoiceNumber.getAuthCode())
//                .originalResponseCode("00") // TODO may need to change
//                .tipAmount(transactionRequest.getAdditionalAmount())
//                .iccDataBlock(cardDetails.getIccDataBlock())
//                .originalAmount(this.transactionLogByInvoiceNumber.getTransactionAmount())
//                .originalInvoiceNumber(this.transactionRequest.getOriginalInvoiceNumber())
//                .build();
//        return this.s2MTipAdjustmentRequest;
//    }
//
//    private void validateTransactionLog() {
//        if (this.transactionLogByInvoiceNumber.getTransactionType() != TransactionType.PURCHASE)
//            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_FOUND);
//        if (!this.transactionLogByInvoiceNumber.getTransactionStatus().equals("APPROVED"))
//            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_FOUND);
//    }
//
//    private TransactionLog getTransactionLogByInvoiceNumber(String originalInvoiceNumber) {
//        TransactionLog transactionLog = this.requestModel.getTransactionRepository()
//                .getTransactionLogWithInvoiceNumber(originalInvoiceNumber, this.reconciliationBatchNumber);
//        if (transactionLog == null)
//            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_FOUND);
//        return transactionLog;
//    }
//}
