package global.citytech.finpos.nibl.merchant.presentation.model.response

data class Values(
    var key1: String? = null,
    var key2: String? = null
)