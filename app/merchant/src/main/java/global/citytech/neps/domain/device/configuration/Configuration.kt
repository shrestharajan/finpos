package global.citytech.neps.domain.device.configuration

import global.citytech.finpos.core.api.common.DeviceResponse
import global.citytech.neps.domain.model.LoadParameterRequest
import io.reactivex.Observable

interface Configuration {
    fun inject(key: String): Observable<DeviceResponse>
    fun initSdk(): Observable<DeviceResponse>
    fun LoadTerminalParameters(loadParameterRequest: LoadParameterRequest): Observable<DeviceResponse>
}