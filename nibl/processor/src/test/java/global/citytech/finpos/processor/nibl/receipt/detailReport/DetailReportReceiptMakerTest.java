package global.citytech.finpos.processor.nibl.receipt.detailReport;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Collections;
import java.util.List;

import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportReceipt;
import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportReceiptMaker;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.utility.JsonUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Unique Shakya on 1/5/2021.
 */
@RunWith(PowerMockRunner.class)
public class DetailReportReceiptMakerTest {

    private TerminalRepository terminalRepository;
    private TerminalInfo terminalInfo;

    @Before
    public void setUp() throws Exception {
        this.terminalRepository = PowerMockito.mock(TerminalRepository.class);
        this.terminalInfo = PowerMockito.mock(TerminalInfo.class);
    }

    @Test
    public void prepareTest() {
        PowerMockito.when(this.terminalRepository.findTerminalInfo())
                .thenReturn(this.terminalInfo);

        PowerMockito.when(this.terminalInfo.getMerchantName()).thenReturn("CityTech");
        PowerMockito.when(this.terminalInfo.getMerchantAddress()).thenReturn("Kamaladi");
        PowerMockito.when(this.terminalInfo.getMerchantID()).thenReturn("1122334455");
        PowerMockito.when(this.terminalInfo.getTerminalID()).thenReturn("1234567890");

        String batchNumber = "0001";
        List<TransactionLog> transactionLogs = this.mockTransactionLogs();

        DetailReportReceiptMaker detailReportReceiptMaker = new DetailReportReceiptMaker(this.terminalRepository);
        DetailReportReceipt detailReportReceipt = detailReportReceiptMaker.prepare(batchNumber, transactionLogs);

        assertNotNull(detailReportReceipt);
        Logger.getLogger(DetailReportReceiptMakerTest.class.getName()).log(":: DETAIL REPORT :: " + JsonUtils.toJsonObj(detailReportReceipt));
        assertEquals(1, detailReportReceipt.getDetailReports().size());
        assertEquals(batchNumber, detailReportReceipt.getReconciliationBatchNumber());
    }

    private List<TransactionLog> mockTransactionLogs() {
        CardDetails cardDetails = new CardDetails();
        cardDetails.setPrimaryAccountNumber("4249420012002330");
        cardDetails.setCardScheme(CardSchemeType.VISA_CREDIT);
        ReadCardResponse readCardResponse = new ReadCardResponse(cardDetails, Result.SUCCESS, "SUCCESS");
        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
                .withTransactionAmount(1500.00)
                .withAuthCode("123342")
                .withReadCardResponse(readCardResponse)
                .withInvoiceNumber("111111")
                .withTransactionType(TransactionType.PURCHASE)
                .build();
        return Collections.singletonList(transactionLog);
    }
}
