package global.citytech.finpos.merchant.presentation.transactions.mobile

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.domain.model.app.MobileNfcPayment
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.APPROVED_PAYMENT_MOBILE
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.TRANSACTION_VOID_SALE
import global.citytech.finpos.merchant.presentation.utils.StringUtils
import kotlinx.android.synthetic.main.item_transaction.view.*
import global.citytech.finpos.merchant.utils.retrieveCurrencyName
import global.citytech.finposframework.usecases.TransactionType

/**
 * @author SIDDHARTHA GHIMIRE
 */
class MobileNfcTransactionAdapter(
    private var mobileNfcPayment: MutableList<MobileNfcPayment>,
    private var listener: Listener
) :
    RecyclerView.Adapter<MobileNfcTransactionAdapter.MobilePayTransactionViewHolder>() {

    lateinit var context: Context

    class MobilePayTransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MobilePayTransactionViewHolder {
        context = parent.context
        return MobilePayTransactionViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_transaction, parent, false)
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MobilePayTransactionViewHolder, position: Int) {
        val mobileNfcPayment = mobileNfcPayment[position]
        holder.itemView.tv_txn_type.text =
            "Type: ${
                if (mobileNfcPayment.transactionType.lowercase() == TRANSACTION_VOID_SALE) {
                    TransactionType.VOID.displayName
                } else {
                    mobileNfcPayment.transactionType
                }
            }"

        holder.itemView.tv_card_number.text =
            "Reference # : ${mobileNfcPayment.referenceNumber}"
        holder.itemView.tv_invoice_number.visibility = View.GONE
        holder.itemView.tv_txn_time.text = mobileNfcPayment.transactionTime
        holder.itemView.tv_amount.text =
            "${retrieveCurrencyName(mobileNfcPayment.transactionCurrency)} ${
                StringUtils.decimalFormatterHigherCurrency(
                    mobileNfcPayment.transactionAmount
                )
            }"
        holder.itemView.setOnClickListener {
            listener.onItemClicked(mobileNfcPayment)
        }

        val drawableId =
            if (mobileNfcPayment.transactionStatus == APPROVED_PAYMENT_MOBILE) {
                if (mobileNfcPayment.transactionType.lowercase() == TRANSACTION_VOID_SALE) {
                    R.drawable.ic_check_circle_red_64dp
                }else{
                    R.drawable.ic_check_circle_green_64dp
                }
            }else {
                R.drawable.ic_failure_red_64dp
            }

        holder.itemView.iv_transaction_icon.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                drawableId
            )
        )
    }

    override fun getItemCount(): Int = mobileNfcPayment.size

    @SuppressLint("NotifyDataSetChanged")
    fun update(transactions: MutableList<MobileNfcPayment>) {
        this.mobileNfcPayment = transactions
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateAll(mobileNfcPayments: MutableList<MobileNfcPayment>) {
        this.mobileNfcPayment = mobileNfcPayments
        notifyDataSetChanged()
    }

    interface Listener {
        fun onItemClicked(mobileNfcPayment: MobileNfcPayment)
    }
}