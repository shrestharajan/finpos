package global.citytech.finpos.merchant.presentation.balanceinquiry

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import global.citytech.common.Base64
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityBalanceInquiryBinding
import global.citytech.finpos.merchant.domain.model.balanceinquiry.BalanceInquiryModel
import global.citytech.finpos.merchant.presentation.alertdialogs.BalanceEnquiryDialog
import global.citytech.finpos.merchant.presentation.alertdialogs.BalanceEnquiryDialogListener
//import global.citytech.finpos.merchant.presentation.alertdialogs.BalanceEnquiryDialogListener
import global.citytech.finpos.merchant.presentation.alertdialogs.PosEntryMode
import global.citytech.finpos.merchant.presentation.greenpin.GreenPinBaseTransactionActivity
import global.citytech.finpos.merchant.presentation.greenpin.GreenPinBaseTransactionViewModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finposframework.usecases.TransactionType
import kotlinx.android.synthetic.main.activity_green_pin.*
import kotlinx.android.synthetic.main.ministatement_list.*

class BalanceInquiryActivity :
    GreenPinBaseTransactionActivity<ActivityBalanceInquiryBinding, BalanceInquiryViewModel>(),
    View.OnClickListener,
    BalanceEnquiryDialogListener
{

    private lateinit var viewModel: BalanceInquiryViewModel
    private lateinit var configurationItem: ConfigurationItem
    private lateinit var balanceEnquiryDialog: BalanceEnquiryDialog
    private var bankLogo: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel()
        initBaseViews()
        initObservers()
        viewModel.getConfigurationItem()
        showPosEntrySelectionDialog()
        observeBankDisplayImage()

        viewModel.balanceInquirySuccess.observe(
            this,
            Observer {
                if(it){
                    viewModel.additionalAmount.observe(
                        this,
                        {
                            showBalanceEnquiryDialog(it)
                        }
                    )
                }

            }
        )
    }

    private fun initObservers() {
        this.viewModel.configurationItem.observe(this, Observer {
            this.configurationItem = it
            this.viewModel.generateBalanceInquiry(configurationItem, getTransactionType())
        })
    }

    private fun showPosEntrySelectionDialog() {
        Glide.with(this).load(PosEntryMode.ACCEPT_ALL.loaderImage).into(image)
    }

    override fun getBindingVariable(): Int = BR.balanceInquiryViewModel

    override fun getLayout(): Int = R.layout.activity_balance_inquiry

    override fun getViewModel(): BalanceInquiryViewModel =
        ViewModelProviders.of(this)[BalanceInquiryViewModel::class.java]

    override fun onManualButtonClicked() {
        TODO("Not yet implemented")
    }

    override fun onCancelButtonClicked() {
        this.viewModel.shouldDispose = true
        this.viewModel.doCleanUp(GreenPinBaseTransactionViewModel.Task.RETURN_TO_DASHBOARD)
    }


    override fun onAmountActivityResult(data: Intent?) {
        TODO("Not yet implemented")
    }

    override fun onManualActivityResult(data: Intent?) {
        TODO("Not yet implemented")
    }

    override fun getTransactionType(): TransactionType = TransactionType.BALANCE_INQUIRY

    override fun getGreenPinBaseTransactionViewModel(): GreenPinBaseTransactionViewModel =
        this.viewModel

    override fun onClick(p0: View?) {
    }


    private fun showBalanceEnquiryDialog(balanceInquiryModel: BalanceInquiryModel) {
        balanceEnquiryDialog = getViewModel().bankDisplayImage.value?.let {
            BalanceEnquiryDialog(balanceInquiryModel, it,this, getViewModel().currencyCode)
        }!!


        balanceEnquiryDialog.isCancelable = false
        balanceEnquiryDialog.show(
            supportFragmentManager,
            BalanceEnquiryDialog.TAG
        )
    }

    override fun printBalanceInquiry() {
        this.viewModel.printBalanceInquiry()
    }
    private fun observeBankDisplayImage() {
        getViewModel().bankDisplayImage.observe(this, Observer {
            this.bankLogo = Base64.encodeToString(it, false)
            Glide.with(this).load(it).into(iv_bank_logo_mini_statement)
        })

        getViewModel().defaultBankDisplayImage.observe(this, Observer {
            if (it)
                iv_bank_logo_mini_statement.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_full_finpos_logo
                    )
                )
        })
    }

}