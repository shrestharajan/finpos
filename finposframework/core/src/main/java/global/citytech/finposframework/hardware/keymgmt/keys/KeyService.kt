package global.citytech.finposframework.hardware.keymgmt.keys

import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.DeviceService
import global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyCheckRequest
import global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyInjectionRequest

/**
 * Created by Rishav Chudal on 4/16/20.
 */
interface KeyService: DeviceService {
    fun injectKey(keyInjectionRequest: KeyInjectionRequest) : DeviceResponse

    fun eraseAllKeys(): DeviceResponse

    fun checkKeyExist(keyCheckRequest: KeyCheckRequest): DeviceResponse
}