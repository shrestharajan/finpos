package global.citytech.finpos.merchant.framework.datasource.core.preauth

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.preauth.PreAuthDataSource
import global.citytech.finpos.merchant.data.datasource.device.CardSource
import global.citytech.finpos.merchant.data.datasource.device.PrinterSource
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionAuthenticatorImpl
import global.citytech.finpos.merchant.framework.datasource.device.*
import global.citytech.finpos.merchant.domain.model.preauth.PreAuthRequestEntity
import global.citytech.finpos.merchant.domain.model.preauth.PreAuthResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.data.mapToPreAuthResponseUiModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.preauth.ManualPreAuthRequestItem
import global.citytech.finpos.merchant.presentation.model.preauth.PreAuthRequestItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler

import global.citytech.finposframework.repositories.ApplicationRepository
import global.citytech.finposframework.usecases.controllers.DeviceController
import global.citytech.finposframework.repositories.TerminalRepository
import global.citytech.finposframework.repositories.TransactionRepository
import global.citytech.finposframework.switches.ProcessorManager
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest
import io.reactivex.Observable

/**
 * Created by Rishav Chudal on 8/31/20.
 */
class PreAuthDataSourceImpl: PreAuthDataSource {

    override fun doPreAuth(
        configurationItem: ConfigurationItem,
        preAuthRequestItem: PreAuthRequestItem
    ): Observable<PreAuthResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = prepareTerminalRepository(configurationItem)
        val transactionRequest = prepareTransactionRequest(preAuthRequestItem)

        val preAuthRequestEntity = PreAuthRequestEntity(
            transactionRepository = prepareTransactionRepository(),
            transactionRequest = transactionRequest,
            deviceController = prepareDeviceController(),
            readCardService = prepareCardService(),
            transactionAuthenticator = prepareTransactionAuthenticator(),
            printerService = preparePrinterService(),
            applicationRepository = prepareApplicationRepository(),
            ledService = LedSourceImpl(NiblMerchant.INSTANCE),
            soundService = SoundSourceImpl(NiblMerchant.INSTANCE)
        )


        val preAuthRequester
                = ProcessorManager.getInterface(terminalRepository, NotificationHandler).preAuthRequester
        return Observable.fromCallable {
            (preAuthRequester.execute(preAuthRequestEntity.mapToModel()).mapToPreAuthResponseUiModel())
        }
    }

    override fun doManualPreAuth(
        configurationItem: ConfigurationItem,
        manualPreAuthRequestItem: ManualPreAuthRequestItem
    ): Observable<PreAuthResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val transactionRepository =
            TransactionRepositoryImpl(
                NiblMerchant.INSTANCE
            )
        val transactionRequestForManual = prepareTransactionRequestForManual(manualPreAuthRequestItem)
        val deviceController = DeviceControllerImpl(NiblMerchant.INSTANCE)
        val transactionAuthenticator =
            TransactionAuthenticatorImpl()
        val printerServiceImpl = PrinterSourceImpl(NiblMerchant.getActivityContext())
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
        val preAuthResponseEntity = PreAuthRequestEntity(
            deviceController = deviceController,
            printerService = printerServiceImpl,
            transactionAuthenticator = transactionAuthenticator,
            transactionRepository = transactionRepository,
            transactionRequest = transactionRequestForManual,
            applicationRepository = applicationRepository
        )
        val manualPreAuthRequester
                = ProcessorManager.getInterface(
            terminalRepository,
            NotificationHandler
        ).manualPreAuthRequester
        return Observable.fromCallable {
            (manualPreAuthRequester.execute(preAuthResponseEntity.mapToModel()).mapToPreAuthResponseUiModel())
        }
    }

    private fun prepareTransactionRequest(
        preAuthRequestItem: PreAuthRequestItem
    ): TransactionRequest {
        val transactionRequest = TransactionRequest(preAuthRequestItem.transactionType)
        transactionRequest.amount = preAuthRequestItem.amount!!
        return transactionRequest
    }

    private fun prepareTransactionRequestForManual(
        manualPreAuthRequestItem: ManualPreAuthRequestItem
    ): TransactionRequest {
        val transactionRequest = TransactionRequest(
            manualPreAuthRequestItem.transactionType
        )
        transactionRequest.amount = manualPreAuthRequestItem.amount!!
        transactionRequest.cardNumber = manualPreAuthRequestItem.cardNumber
        transactionRequest.expiryDate = manualPreAuthRequestItem.expiryDate
        transactionRequest.cvv = manualPreAuthRequestItem.cvv
        return transactionRequest
    }

    private fun prepareTerminalRepository(
        configurationItem: ConfigurationItem
    ): TerminalRepository {
        return TerminalRepositoryImpl(configurationItem)
    }

    private fun prepareTransactionRepository(): TransactionRepository {
        return TransactionRepositoryImpl(
            NiblMerchant.INSTANCE
        )
    }

    private fun prepareDeviceController(): DeviceController {
        return DeviceControllerImpl(NiblMerchant.INSTANCE)
    }

    private fun prepareCardService(): CardSource {
        return CardSourceImpl(NiblMerchant.getActivityContext())
    }

    private fun prepareTransactionAuthenticator(): TransactionAuthenticator {
        return TransactionAuthenticatorImpl()
    }

    private fun preparePrinterService(): PrinterSource {
        return PrinterSourceImpl(NiblMerchant.INSTANCE)
    }

    private fun prepareApplicationRepository() : ApplicationRepository {
        return ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
    }
}