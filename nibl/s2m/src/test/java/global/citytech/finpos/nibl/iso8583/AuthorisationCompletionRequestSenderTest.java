package global.citytech.finpos.nibl.iso8583;

import org.junit.Test;

import global.citytech.finpos.nibl.NIBLHostConfig;
import global.citytech.finpos.nibl.iso8583.requestsender.authcompletion.AuthorisationCompletionRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.authcompletion.AuthorisationCompletionRequestSender;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.utility.HelperUtils;

import static junit.framework.TestCase.assertNotNull;

/**
 * Created by Unique Shakya on 9/29/2020.
 */
public class AuthorisationCompletionRequestSenderTest {

    @Test
    public void sendTest() {
        String terminalID = "99991002";
        String acquirerID = "405635000000002";
        TerminalInfo terminalInfo = new TerminalInfo(terminalID, acquirerID,"", "Citytech", "Kamaladi");
        HostInfo primaryHost = NIBLHostConfig.getPrimaryConfig();
        HostInfo secondaryHost = NIBLHostConfig.getSecondaryConfig();
        ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);
        RequestContext context =
                new RequestContext(
                        "000405", terminalInfo, connectionParam);
        AuthorisationCompletionRequest request = AuthorisationCompletionRequest.Builder.newInstance()
                .withExpirationDate("2212")
                .withLocalTransactionDate(HelperUtils.getDefaultLocaleDateWithoutYear())
                .withLocalTransactionTime(HelperUtils.getDefaultLocaleTimeHhMmSs())
                .withOriginalApprovalCode("000100")
                .withOriginalInvoiceNumber("001203")
                .withOriginalResponseCode("11")
                .withOriginalRrn("090990009220")
                .withPan("4249720020000234")
                .withPosConditionCode("00")
                .withPosEntryMode(PosEntryMode.ICC)
                .withTransactionAmount(10)
                .build();
        context.setRequest(request);
        AuthorisationCompletionRequestSender sender = new AuthorisationCompletionRequestSender(new NIBLSpecInfo(), context);
        IsoMessageResponse messageResponse = sender.send();
        assertNotNull(messageResponse);
    }
}
