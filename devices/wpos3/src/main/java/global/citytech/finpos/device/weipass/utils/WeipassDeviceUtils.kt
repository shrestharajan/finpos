package global.citytech.finpos.device.weipass.utils

import android.content.Context
import android.os.RemoteException
import android.text.Layout
import com.wiseasy.emvprocess.LibInit
import com.wiseasy.emvprocess.Process
import com.wiseasy.emvprocess.SDKInstance
import com.wiseasy.emvprocess.bean.TransAmount
import global.citytech.finpos.device.weipass.card.handler.WeipassStateHandler
import global.citytech.finpos.device.weipass.led.WeipassLedServiceImpl
import global.citytech.finpos.device.weipass.utils.WeipassConstant.CARD_TYPE_MAG_PRIOR_TO_CL
import global.citytech.finpos.device.weipass.utils.WeipassConstant.DEFAULT_CARD_READ_TIMEOUT
import global.citytech.finpos.device.weipass.utils.WeipassConstant.DEFAULT_PINPAD_TIMEOUT
import global.citytech.finpos.device.weipass.utils.WeipassConstant.EMPTY_STRING
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MAXM_PIN_LENGTH_DEFAULT
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MAX_CARD_READ_TIMEOUT
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MINM_PIN_LENGTH_DEFAULT
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_OFFLINE_APPROVED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_OFFLINE_DECLINED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_ONLINE_APPROVED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_ONLINE_DECLINED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_ONLINE_FAIL_OFFLINE_APPROVED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_ONLINE_FAIL_OFFLINE_DECLINED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.NO_SUCH_SLOT_TO_OPEN
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_TRANS_AUTH_VOID
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_TRANS_PREAUTH_AND_PREAUTH_EXTN
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_TRANS_PURCHASE_AND_PURCHASE_ADVICE
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_TRANS_REFUND
import global.citytech.finposframework.hardware.utility.DeviceApiConstants
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.TransactionState
import global.citytech.finposframework.hardware.io.led.LedAction
import global.citytech.finposframework.hardware.io.led.LedLight
import global.citytech.finposframework.hardware.io.led.LedRequest
import global.citytech.finposframework.hardware.io.printer.Style
import global.citytech.finposframework.hardware.keymgmt.keys.KeyAlgorithm
import global.citytech.finposframework.hardware.keymgmt.keys.KeyType
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat
import global.citytech.finposframework.usecases.TransactionType
import sdk4.wangpos.libemvbinder.utils.MoneyUtil
import wangpos.sdk4.libbasebinder.BankCard
import wangpos.sdk4.libbasebinder.Printer
import wangpos.sdk4.libkeymanagerbinder.Key
import java.math.BigDecimal

/**
 * Created by Rishav Chudal on 4/9/20.
 */
class WeipassDeviceUtils {
    companion object {
        fun getKeyTypeForCore(keyType: KeyType): Byte {
            when (keyType) {
                KeyType.KEY_TYPE_TLK -> return Key.KEY_REQUEST_TLK
                KeyType.KEY_TYPE_TMK -> return Key.KEY_REQUEST_TMK
                KeyType.KEY_TYPE_DEK -> return Key.KEY_REQUEST_DEK
                KeyType.KEY_TYPE_PEK -> return Key.KEY_REQUEST_PEK
                KeyType.KEY_TYPE_IPEK -> return Key.KEY_REQUEST_IPEK
                KeyType.KEY_TYPE_MAK -> return Key.KEY_REQUEST_MAK
                KeyType.KEY_TYPE_KBPK -> return Key.KEY_REQUEST_KBPK
                KeyType.KEY_TYPE_DDEK -> return Key.KEY_REQUEST_DDEK
                KeyType.KEY_TYPE_DMAK -> return Key.KEY_REQUEST_DMAK
                else -> throw Exception(DeviceApiConstants.MSG_KEY_NOT_SUPPORT)
            }
        }

        fun getAlgorithmTypeForCore(algorithm: KeyAlgorithm): Int {
            when (algorithm) {
                KeyAlgorithm.TYPE_3DES -> return Process.ALGORITHM_TYPE_3DES
                KeyAlgorithm.TYPE_AES -> return Process.ALGORITHM_TYPE_AES
                KeyAlgorithm.TYPE_SM4 -> return Process.ALGORITHM_TYPE_SM4
                KeyAlgorithm.TYPE_DES -> return Process.ALGORITHM_TYPE_DES
                else -> throw Exception(DeviceApiConstants.MSG_ALG_NOT_SUPPORT)
            }
        }

        fun getCardType(cardMode: Int): Byte {
            return when (cardMode){
                (BankCard.CARD_MODE_MAG or BankCard.CARD_MODE_ICC or BankCard.CARD_MODE_PICC) -> CARD_TYPE_MAG_PRIOR_TO_CL.toByte()
                (BankCard.CARD_MODE_MAG or BankCard.CARD_MODE_PICC) -> CARD_TYPE_MAG_PRIOR_TO_CL.toByte()
                else -> BankCard.CARD_TYPE_NORMAL
            }
        }

        fun getCardReadTimeoutOrDefault(timeout: Int?): Int {
            var var1 = timeout
            if (var1 == null || var1 == 0 || var1 > MAX_CARD_READ_TIMEOUT) {
                var1 = DEFAULT_CARD_READ_TIMEOUT
            }
            return var1
        }

        fun getCardMode(slotsToOpenList: List<CardType>): Int {
            var var1 = 0
            for (card: CardType in slotsToOpenList.distinct()) {
                var1 = when (card) {
                    CardType.MAG -> (var1 or BankCard.CARD_MODE_MAG)
                    CardType.ICC -> (var1 or BankCard.CARD_MODE_ICC)
                    CardType.PICC -> (var1 or BankCard.CARD_MODE_PICC)
                    else -> throw Exception(NO_SUCH_SLOT_TO_OPEN)
                }
            }
            return var1
        }

        fun isICCPresentInDevice(): Boolean {
            var result = false
            try {
                val result1 = SDKInstance.mBankCard.iccDetect()
                WeipassLogger.log("Icc status (0 --> Not detected, 1 --> Detected) ::: ".plus(result1))
                if (result1 == 1)
                    result = true
            } catch (ex: Exception) {
                WeipassLogger.log("Exception ::: ".plus(ex.message))
            }
            return result
        }

        fun callForSdkBreakOffCommand(context: Context) {
            try {
                if (SDKInstance.mBankCard == null) {
                    SDKInstance.initSDK(context.applicationContext)
                    LibInit.init(true)
                }
                val result = SDKInstance.mBankCard.breakOffCommand()
                WeipassLogger.log("Break Off Command Result ::: ".plus(result))
            } catch (exception: Exception) {
                WeipassLogger.log("Exception ::: ".plus(exception.message))
            }
        }

        fun getMinmPinLengthOrDefault(length: Int?): Int {
            var var1 = length
            if (var1 == null || var1 == 0) {
                var1 = MINM_PIN_LENGTH_DEFAULT
            }
            return var1
        }

        fun getMaxmPinLengthOrDefault(length: Int?): Int {
            var var1 = length
            if (var1 == null || var1 == 0) {
                var1 = MAXM_PIN_LENGTH_DEFAULT
            }
            return var1
        }

        fun getPinpadTimeoutOrDefault(length: Int?): Int {
            var var1 = length
            if (var1 == null || var1 == 0) {
                var1 = DEFAULT_PINPAD_TIMEOUT
            }
            return var1
        }

        fun prepareTransAmount(
            amount: BigDecimal?,
            additionalAmount: BigDecimal?,
            transType: TransactionType
        ): TransAmount? {
            val transAmount = TransAmount()
            val amountInTrans = amount.toString()
            if ((amountInTrans.isNullOrEmptyOrBlank() || amount!!.equals(0.0))
                && transType != TransactionType.VOID) {
                return null
            }
            val amountInLong = (MoneyUtil.toCent(amountInTrans)).toLong()
            transAmount.amount = amountInLong
            return prepareAdditionalAmount(transAmount, additionalAmount)
        }

        private fun prepareAdditionalAmount(transAmount: TransAmount,
                                             additionalAmount: BigDecimal?
        ): TransAmount {
            val additionalAmountInTrans = additionalAmount.toString()
            if (!additionalAmountInTrans.isNullOrEmptyOrBlank() ||
                    !additionalAmountInTrans.equals("0.0", true)) {
                val addAmountInLong = (MoneyUtil.toCent(additionalAmountInTrans)).toLong()
                transAmount.otherAmount = addAmountInLong
            }
            return transAmount
        }

        fun maskCardNumber(cardNumber: String): String {
            if (cardNumber.isNullOrEmptyOrBlank()) return "NOT PRESENT"
            val firstIndex = (cardNumber.length * .40).toInt()
            var defaultString = ""
            var i = 0
            while (i < cardNumber.length * .40) {
                defaultString = defaultString + "X"
                i++
            }
            val sb = StringBuilder()
            sb.append(cardNumber.substring(0, firstIndex))
            sb.append(defaultString)
            sb.append(cardNumber.substring(firstIndex + defaultString.length))
            return sb.toString()
        }

        fun getKernelTransType(transType: TransactionType): Int {
            var result = 0
            WeipassLogger.log("Transaction Type ::: ".plus(transType))
            when (transType) {
                TransactionType.PURCHASE -> result = SDK_TRANS_PURCHASE_AND_PURCHASE_ADVICE
                TransactionType.PRE_AUTH -> result = SDK_TRANS_PREAUTH_AND_PREAUTH_EXTN
                TransactionType.REFUND -> result = SDK_TRANS_REFUND
                TransactionType.VOID -> result = SDK_TRANS_AUTH_VOID
                else -> result = SDK_TRANS_PURCHASE_AND_PURCHASE_ADVICE
            }
            return result
        }

        fun getBooleanToBinaryAndViceVersa(data: Any?): String {
            var returnValue = ""
            if (data != null) {
                if (data is Int) {
                    returnValue = when (data) {
                        0 -> "false"
                        1 -> "true"
                        else -> "false"
                    }
                } else if (data is Boolean) {
                    returnValue = if (data) {
                        "1"
                    } else {
                        "0"
                    }
                }
            }
            return returnValue
        }

        fun getTransactionStateBasedOnWeipassTransState(
            transState: WeipassTransState
        ): TransactionState {
            return when (transState) {
                WeipassTransState.OFFLINE_APPROVED -> TransactionState.OFFLINE_APPROVED
                WeipassTransState.OFFLINE_DECLINED -> TransactionState.OFFLINE_DECLINED
                WeipassTransState.ONLINE_APPROVED -> TransactionState.ONLINE_APPROVED
                WeipassTransState.ONLINE_FAIL_OFFLINE_APPROVED -> TransactionState.OFFLINE_APPROVED
                WeipassTransState.ONLINE_FAIL_OFFLINE_DECLINED -> TransactionState.OFFLINE_DECLINED
                else -> TransactionState.ONLINE_DECLINED
            }
        }

        fun maskTrack2(value: String): String {
            if (value.isNullOrEmptyOrBlank()) return "NOT PRESENT"
            val firstIndex = (value.length * .15).toInt()
            var defaultString = ""
            var i = 0
            while (i < value.length * .70) {
                defaultString += "X"
                i++
            }
            val sb = java.lang.StringBuilder()
            sb.append(value.substring(0, firstIndex))
            sb.append(defaultString)
            sb.append(value.substring(firstIndex + defaultString.length))
            return sb.toString()
        }

        fun getResultBasedOnWeipassTransState(transState: WeipassTransState): Result {
            val result: Result
            when (transState) {
                WeipassTransState.OFFLINE_APPROVED,
                WeipassTransState.OFFLINE_DECLINED,
                WeipassTransState.ONLINE_APPROVED,
                WeipassTransState.ONLINE_FAIL_OFFLINE_APPROVED,
                WeipassTransState.ONLINE_FAIL_OFFLINE_DECLINED,
                WeipassTransState.ONLINE_DECLINED,
                WeipassTransState.SUCCESS -> result = Result.SUCCESS
                else -> result = Result.FAILURE
            }
            return result
        }

        fun getResponseBasedOnWeipassTransState(transState: WeipassTransState): String {
            val response: String
            when (transState) {
                WeipassTransState.OFFLINE_APPROVED -> response = MSG_OFFLINE_APPROVED
                WeipassTransState.OFFLINE_DECLINED -> response = MSG_OFFLINE_DECLINED
                WeipassTransState.ONLINE_APPROVED -> response = MSG_ONLINE_APPROVED
                WeipassTransState.ONLINE_FAIL_OFFLINE_APPROVED -> response = MSG_ONLINE_FAIL_OFFLINE_APPROVED
                WeipassTransState.ONLINE_FAIL_OFFLINE_DECLINED -> response = MSG_ONLINE_FAIL_OFFLINE_DECLINED
                WeipassTransState.ONLINE_DECLINED -> response = MSG_ONLINE_DECLINED
                else -> response = EMPTY_STRING
            }
            return response
        }

        fun interruptAliveThread(threadName: String) {
            for(thread in Thread.getAllStackTraces().keys) {
                if (thread.name.equals(threadName) && thread.isAlive) {
                    WeipassLogger.log("Thread ::: ".plus(threadName).plus(" ").plus("Interrupting..."))
                    thread.interrupt()
                }
            }
        }

        @Synchronized
        fun cleanUp(context: Context,
                    weipassStateHandler: WeipassStateHandler,
                    weipassLedServiceImpl: WeipassLedServiceImpl
        ) {
            WeipassLogger.log("=============== Cleaning up ===============")
            dismissAlertDialog(weipassStateHandler)
            callForSdkBreakOffCommand(context)
            weipassLedServiceImpl.doTurnLedWith(
                LedRequest(
                    LedLight.ALL,
                    LedAction.OFF
                )
            )
            openCloseCardReader(BankCard.CARD_NMODE_MAG
                    or BankCard.CARD_NMODE_ICC
                    or BankCard.CARD_NMODE_PICC,
                BankCard.CARD_READ_CLOSE.toInt()
            )
            closePrinter()
        }

        private fun dismissAlertDialog(weipassStateHandler: WeipassStateHandler) {
            val alertDialogWeakReference = weipassStateHandler.alertDialogWeakReference
            if (alertDialogWeakReference != null) {
                val alertDialog = alertDialogWeakReference.get()
                if (alertDialog != null && alertDialog.isShowing) {
                    WeipassLogger.log("Dismissing the alert dialog")
                    alertDialog.dismiss()
                }
            }
        }

        fun openCloseCardReader(readerType: Int, action: Int): Int {
            WeipassLogger.log("Open/Close Card Reader ::: "
                .plus(readerType)
                .plus(", Action (1--> OPEN, 2--> CLOSE) ::: ")
                .plus(action)
            )
            var result = -1
            try {
                result = SDKInstance.mBankCard.openCloseCardReader(readerType, action)
            } catch (ex: RemoteException) {
                WeipassLogger.log("Exception ::: ".plus(ex.message))
            }
            WeipassLogger.log("Opening/Closing Card Reader Result ::: $result")
            return result
        }

        fun closePrinter(): Int {
            WeipassLogger.log("Closing Printer...")
            var result = -1
            try {
                result = SDKInstance.mPrinter.printFinish()
            } catch (e: RemoteException) {
                e.printStackTrace()
            }
            WeipassLogger.log("Closing Printer Result ::: $result")
            return result
        }

        fun getAlignmentForBitmap(alignment: Style.Align?): Layout.Alignment {
            return when (alignment) {
                Style.Align.CENTER -> Layout.Alignment.ALIGN_CENTER
                Style.Align.RIGHT -> Layout.Alignment.ALIGN_OPPOSITE
                else -> Layout.Alignment.ALIGN_NORMAL
            }
        }

        fun getAlignmentForQRCode(alignment: Style.Align?): Printer.Align {
            return when (alignment) {
                Style.Align.LEFT -> Printer.Align.LEFT
                Style.Align.RIGHT -> Printer.Align.RIGHT
                else -> Printer.Align.CENTER
            }
        }

        fun getPinBlockFormatOrDefault(pinBlockFormat: PinBlockFormat): Int {
            return when(pinBlockFormat) {
                PinBlockFormat.ISO9564_FORMAT_0 -> 0x00
                PinBlockFormat.ISO9564_FORMAT_1 -> 0x01
                PinBlockFormat.ISO9564_FORMAT_2 -> 0x02
                else -> 0x00
            }
        }
    }
}