package global.citytech.finpos.merchant.data.datasource.app.db.dao

import androidx.room.*
import global.citytech.finpos.merchant.domain.model.app.EmvParam

@Dao
interface EmvParamsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(emvParams: List<EmvParam>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(emvParam: EmvParam): Long

    @Query("SELECT id, ttq, transaction_currency_exponent, transaction_currency_code, terminal_type, terminal_id, terminal_country_code, terminal_capabilities, merchant_name, merchant_identifier, merchant_category_code, force_online_flag, additional_terminal_capabilities, additional_data FROM emv_params")
    fun getEmvParams(): List<EmvParam>

    @Update
    fun update(emvParam: EmvParam):Int

    @Query("DELETE FROM emv_params WHERE id = :id")
    fun deleteById(id: Long): Int

    @Query("DELETE FROM emv_params")
    fun nukeTableData()
}