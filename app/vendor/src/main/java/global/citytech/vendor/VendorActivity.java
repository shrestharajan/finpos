package global.citytech.vendor;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.OnClick;
import global.citytech.easydroid.core.mvp.BaseActivity;
import global.citytech.easydroid.core.utils.MessageConfig;

public class VendorActivity extends BaseActivity implements VendorView {

  private final static String APPLICATION_IDENTIFIER = "global.citytech.vendor";
  private VendorPresenter vendorPresenter;
  private ProgressDialog progressDialog;

  @BindView(R.id.text_input_et_amount)
  TextInputEditText etAmount;

  @BindView(R.id.text_input_et_cashback_amount)
  TextInputEditText etCashbackAmount;

  @BindView(R.id.text_input_et_rrn)
  TextInputEditText etRetrievalReferenceNumber;

  @OnClick(R.id.btn_purchase)
  void onBtnPurchaseClicked() {
    getPaymentDetailsAndProceedForPurchasePayment();
  }

  @OnClick(R.id.btn_void)
  void onVoidButtonClicked(){
    PaymentDetails paymentDetails = preparePaymentDetailsForVoid();
    vendorPresenter.checkoutToCashierForVoidPayment(paymentDetails);
  }

  @Override protected int getLayout() {
    return R.layout.activity_payment;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    initializePageItems();
  }

  private void initializePageItems() {
    initPaymentPresenter();
  }

  private void initPaymentPresenter() {
    vendorPresenter = new VendorPresenter(this);
  }

  private void getPaymentDetailsAndProceedForPurchasePayment() {
    PaymentDetails paymentDetails = preparePaymentDetailsForPurchase();
    vendorPresenter.checkoutToCashierForPurchasePayment(paymentDetails);
  }

  private PaymentDetails preparePaymentDetailsForPurchase() {
    String amount = etAmount.getText().toString().trim();
    String paymentOriginator = getString(R.string.app_name);
    return PaymentDetails.Builder
        .getInstance(
            amount,
            paymentOriginator,
            APPLICATION_IDENTIFIER
         )
        .build();
  }

  private PaymentDetails preparePaymentDetailsForVoid() {
    String amount = etAmount.getText().toString().trim();
    String paymentOriginator = getString(R.string.app_name);
    String retrievalReferenceNumber = etRetrievalReferenceNumber.getText().toString().trim();
    return PaymentDetails.Builder
        .getInstance(
            amount,
            paymentOriginator,
            APPLICATION_IDENTIFIER
        )
        .withRetrievalReferenceNumber(retrievalReferenceNumber)
        .build();
  }

  @Override public void showProgressDialogWithMessage(String message) {
    if (progressDialog == null) {
      progressDialog = new ProgressDialog(this);
    }
    progressDialog.setMessage(message);
    progressDialog.show();
  }

  @Override public void hideProgressDialog() {
    if (progressDialog != null) {
      progressDialog.dismiss();
    }
  }

  @Override public Context getUiContext() {
    return VendorActivity.this;
  }

  @Override public String getStringWithGivenId(int stringId) {
    return getString(stringId);
  }


  @Override public void displayAlertMessage(MessageConfig.Builder errorMessageBuilder) {
    showMessage(errorMessageBuilder);
  }
}