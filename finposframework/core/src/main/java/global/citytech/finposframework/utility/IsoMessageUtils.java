package global.citytech.finposframework.utility;

import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.supports.Optional;

/**
 * Created by Unique Shakya on 8/31/2020.
 */
public class IsoMessageUtils {

    public static String retrieveFromDataElementsAsString(IsoMessageResponse isoMessageResponse, int index) {
        try {
            return isoMessageResponse.getMsg().getDataElementByIndex(index).get().getAsString();
        } catch (Exception e) {
            return "";
        }
    }

    public static int retrieveFromDataElementsAsInt(IsoMessageResponse isoMessageResponse, int index) {
        try {
            return isoMessageResponse.getMsg().getDataElementByIndex(index).get().getValueAsInt();
        } catch (Exception e) {
            return -99;
        }
    }

    public static String retrieveFromRequestDataElementAsString(IsoMessageResponse isoMessageResponse, int index){
        try {
            Optional<DataElement> dataElement = isoMessageResponse.getRequestPayload().getRequestMsg().getDataElementByIndex(index);
            if (dataElement.isEmpty())
                return "";
            return dataElement.get().getAsString();
        } catch (Exception e){
            return "";
        }
    }

    public static int retrieveFromRequestDataElementAsInt(IsoMessageResponse isoMessageResponse, int index){
        try {
            Optional<DataElement> dataElement = isoMessageResponse.getRequestPayload().getRequestMsg().getDataElementByIndex(index);
            if (dataElement.isEmpty())
                return -99;
            return dataElement.get().getValueAsInt();
        } catch (Exception e){
            return -99;
        }
    }

    public static String retrieveDateTime(IsoMessageResponse isoMessageResponse) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(retrieveFromDataElementsAsString(isoMessageResponse, 13));
        stringBuilder.append(retrieveFromDataElementsAsString(isoMessageResponse, 12));
        return stringBuilder.toString();
    }
}
