package global.citytech.finposframework.switches.receipt.customercopy;

import global.citytech.finposframework.switches.ResponseParameter;

/**
 * Created by Unique Shakya on 9/2/2020.
 */
public interface CustomerCopyResponseParameter extends ResponseParameter {
}
