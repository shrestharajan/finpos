package global.citytech.finpos.neps.iso8583.requestsender.cardtypesenders;

import global.citytech.finpos.neps.iso8583.PosEntryModeConfig;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.CardTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.switches.PosEntryMode;

public class ICCISOMessageUpdater implements CardTypeISOMessageUpdater {
    @Override
    public Iso8583Msg update(TransactionIsoRequest transactionIsoRequest,Iso8583Msg msg) {
        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(PosEntryMode.ICC)
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.TRACK_II_DATA,
                        transactionIsoRequest.getTrack2Data()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.ICC_SYSTEM_RELATED_DATA,
                        transactionIsoRequest.getEmvData()
                )
        );

        return msg;
    }
}
