package global.citytech.finposframework.switches.logon;

import global.citytech.finposframework.switches.Request;

/** @author rajudhital on 4/2/20 */
public interface LogOnRequester<T extends LogOnRequestParameter, E extends LogOnResponseParameter>
    extends Request<T, E> {}
