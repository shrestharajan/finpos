package global.citytech.finposframework.utility;

import global.citytech.finposframework.usecases.CardSchemeType;

public class CardUtils {

    public static CardSchemeType retrieveCardSchemeType(String aidLabel) {
        switch (aidLabel.toUpperCase()) {
            case "P1":
            case "SPAN VSDC":
            case "SPAN MCHIP":
            case "SPAN":
            case "MADA":
                return CardSchemeType.MADA;
            case "MC":
            case "MASTERCARD":
            case "MASTER CARD":
                return CardSchemeType.MASTERCARD;
            case "VC":
            case "VISA CREDIT":
                return CardSchemeType.VISA_CREDIT;
            case "VD":
            case "VISA DEBIT":
            case "VISA ELECTRON":
                return CardSchemeType.VISA_DEBIT;
            case "UP":
            case "UNION PAY":
            case "UNIONPAY":
                return CardSchemeType.UNION_PAY;
            case "DM":
            case "MAESTRO":
                return CardSchemeType.MAESTRO;
            case "AX":
            case "AMERICAN EXPRESS":
                return CardSchemeType.AMEX;
            case "GN":
            case "GCCNET":
                return CardSchemeType.GCCNET;
            default:
                return CardSchemeType.NONE;
        }
    }


    public static String getShortAidLabel(String aidLabel) {
        switch (aidLabel.toUpperCase()) {
            case "MASTERCARD":
            case "MC":
                return "MC";
            case "SPAN VSDC":
            case "SPAN MCHIP":
            case "SPAN":
            case "MADA":
            case "P1":
                return "P1";
            case "VISA CREDIT":
            case "VC":
                return "VC";
            case "VISA ELECTRON":
            case "VISA DEBIT":
            case "VD":
                return "VD";
            case "MAESTRO":
            case "DM":
                return "DM";
            case "UNION PAY":
            case "UNIONPAY":
            case "UP":
                return "UP";
            case "AMERICAN EXPRESS":
            case "AX":
                return "AX";
            case "GCCNET":
            case "GCC NET":
            case "GN":
                return "GN";
        }
        return "";
    }
}
