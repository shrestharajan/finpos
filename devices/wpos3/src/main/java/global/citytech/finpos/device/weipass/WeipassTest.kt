package global.citytech.finpos.device.weipass

import global.citytech.finpos.device.weipass.card.handler.WeipassStateHandler
import global.citytech.finpos.device.weipass.utils.WeipassTransState
import global.citytech.finpos.device.weipass.utils.defaultOrEmptyValue

/**
 * Created by Rishav Chudal on 4/9/20.
 */
fun main() {
    val test:String? = "   "
    val iss = test.defaultOrEmptyValue()
    println("The test is ::: " + iss)
}

fun main(args: Array<String>) {
    println(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid)
    Thread() {
        println("Thread 1 Started")
        test1()
    }.start()

    Thread {
        println("Thread 2 Started")
        test2()
    }.start()
}

fun test1() {
    WeipassStateHandler.transState = WeipassTransState.PINPAD_CANCEL
}

fun test2() {
    println(WeipassStateHandler.transState)
}

