package global.citytech.finpos.merchant.framework.broadcasts

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.AppUtility.printLog


class NetworkStatusBroadcastReceiver : BroadcastReceiver() {

    val TAG = NetworkStatusBroadcastReceiver::class.java.name

    companion object {
        const val TAG = "NetworkStatusBroadcastReceiver"
    }

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)?.state
            val mobileInfo =
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)?.state
            if (wifiInfo == NetworkInfo.State.CONNECTED || mobileInfo == NetworkInfo.State.CONNECTED) {
                printLog(TAG, "Network Status  >>>> CONNECTED")
                printLog(
                    TAG, "Network Connectivity Mode  >>>> ${AppUtility.getNetworkType(context)}"
                )
                if (wifiInfo == NetworkInfo.State.CONNECTED)
                    printLog(TAG, "IP ADDRESS  >>>> ${AppUtility.getIPAddress(true)}")
                if (mobileInfo == NetworkInfo.State.CONNECTED)
                    printLog(TAG, "IP ADDRESS  >>>> ${AppUtility.getIPAddress(true)}")
            } else if (wifiInfo == NetworkInfo.State.DISCONNECTED || mobileInfo == NetworkInfo.State.DISCONNECTED) {
                printLog(TAG, "Network Status  >>>> DISCONNECTED")
            }
        }
    }
}