package global.citytech.finpos.processor.neps.transaction;

import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.usecases.transaction.TransactionInitializer;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;

public class NepsTransactionInitializer implements TransactionInitializer {

    ApplicationRepository applicationRepository;

    public NepsTransactionInitializer(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    @Override
    public TransactionRequest initialize(TransactionRequest transactionRequest) {
        transactionRequest.setEmvParameterRequest(applicationRepository.retrieveEmvParameterRequest());
        return transactionRequest;
    }



}
