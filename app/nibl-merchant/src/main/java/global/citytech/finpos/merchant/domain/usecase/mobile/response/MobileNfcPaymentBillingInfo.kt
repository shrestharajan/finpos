package global.citytech.finpos.merchant.domain.usecase.mobile.response

import java.math.BigDecimal

/**
 * @author SIDDHARTHA GHIMIRE
 */
data class MobileNfcPaymentBillingInfo(
    val reference: String,
    val payerMobileNumber: String,
    val mid: String,
    val tid: String,
    val txnDate: String,
    val txnTime: String,
    val paymentInitiator: String,
    val merchantName: String,
    val approvalCode: String,
    val txnType: String,
    val merchantAddress: String,
    val amount: BigDecimal,
    val currency: String,
    val transactionResult: String,
)