package global.citytech.finpos.processor.neps;

/**
 * Created by Saurav Ghimire on 1/20/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class NepsConstant {

    public static final String PIN_PAD_CURRENCY = "NPR";
    public static final String PIN_PAD_MESSAGE = "CUSTOMER PIN ENTRY";
    public static final String ARQC_TRANSACTION_CODE = "80";
}
