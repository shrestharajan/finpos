package global.citytech.finpos.merchant.presentation.transactions.mobile

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import global.citytech.common.Constants
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityMobileNfcTransactionDetailBinding
import global.citytech.finpos.merchant.domain.model.app.MobileNfcPayment
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.FAILED_TRANSACTION
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.TRANSACTION_VOID_SALE
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.presentation.admin.login.LoginDialog
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmationDialog
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finpos.merchant.utils.retrieveCurrencyName
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion
import global.citytech.finposframework.utility.StringUtils
import kotlinx.android.synthetic.main.activity_mobile_nfc_transaction_detail.*

/**
 * @author SIDDHARTHA GHIMIRE
 */
class MobileNfcTransactionDetailActivity :
    AppBaseActivity<ActivityMobileNfcTransactionDetailBinding, MobileNfcTransactionDetailViewModel>(),
    TransactionConfirmationDialog.Listener, LoginDialog.LoginDialogListener {
    companion object {

        private const val EXTRA_MOBILE_PAYMENT = "extra.mobile.payment"

        fun getLaunchIntent(context: Context, mobileNfcPayment: MobileNfcPayment): Intent {
            val intent = Intent(context, MobileNfcTransactionDetailActivity::class.java)
            intent.putExtra(EXTRA_MOBILE_PAYMENT, mobileNfcPayment)
            return intent
        }
    }

    private var transactionConfirmationDialog: TransactionConfirmationDialog? = null
    private lateinit var loginDialog: LoginDialog
    private var originalMobileNfcPayment: MobileNfcPayment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel().getConfigurationItem()
        initObservers()
        (intent.getParcelableExtra(EXTRA_MOBILE_PAYMENT) as? MobileNfcPayment)?.let {
            originalMobileNfcPayment = it
            initViews(it)
            handleClickEvents(it)
        }
    }

    private fun initObservers() {
        getViewModel().isLoading.observe(this) {
            if (it)
                showProgress()
            else
                hideProgress()
        }

        getViewModel().message.observe(this) {
            showNormalMessage(
                MessageConfig.Builder().message(it).positiveLabel(getString(R.string.title_ok))
                    .onPositiveClick { })
        }

        getViewModel().finishActivity.observe(this) {
            if (it)
                finish()
        }

        getViewModel().transactionConfirmationLiveData.observe(this) {
            showTransactionConfirmationDialog(it, getViewModel().updateTransactionConfirmation)
        }
    }

    private fun showTransactionConfirmationDialog(it: TransactionConfirmation, update: Boolean) {
        if (transactionConfirmationDialog == null)
            transactionConfirmationDialog = TransactionConfirmationDialog(this, this, it)
        if (update)
            transactionConfirmationDialog!!.update(it, this)
        transactionConfirmationDialog!!.show()
    }

    private fun handleClickEvents(mobileNfcPayment: MobileNfcPayment) {
        btn_print_duplicate.setOnClickListener {
            getViewModel().printDuplicateReceipt(mobileNfcPayment)
        }

        btn_cancellation.setOnClickListener {
            showMerchantLoginDialog(
                getString(R.string.msg_merchant_password),
                getViewModel().getMerchantCredential()
            )
        }

        iv_back.setOnClickListener {
            finish()
        }
    }

    private fun showMerchantLoginDialog(pageTitle: String, validCredential: String) {
        this.loginDialog = LoginDialog.newInstance(
            pageTitle = pageTitle,
            validCredential = validCredential
        )
        this.loginDialog.isCancelable = false
        this.loginDialog.show(
            supportFragmentManager,
            LoginDialog.TAG
        )
    }

    private fun initViews(mobileNfcPayment: MobileNfcPayment) {
        val initiatorId = mobileNfcPayment.initiatorId.toString()
        tv_merchant_name.text = DeviceConfiguration.get().merchantName
        tv_merchant_id.text = mobileNfcPayment.merchantId
        tv_terminal_id.text = mobileNfcPayment.terminalId
        tv_date.text = mobileNfcPayment.transactionDate
        tv_time.text = mobileNfcPayment.transactionTime
        tv_reference_number.text = mobileNfcPayment.referenceNumber

        tv_transaction_type.text =
            when (mobileNfcPayment.transactionType.lowercase()) {
                TRANSACTION_VOID_SALE -> TransactionType.VOID.displayName
                else -> mobileNfcPayment.transactionType
            }

        tv_transaction_amount.text =
            retrieveCurrencyName(mobileNfcPayment.transactionCurrency).plus(" ")
                .plus(StringUtils.formatAmountTwoDecimal(mobileNfcPayment.transactionAmount))
        tv_transaction_status.text = mobileNfcPayment.transactionStatus
        tv_payment_initiator.text = mobileNfcPayment.paymentInitiator

        tv_initiator_id.text =
            when (initiatorId.length) {
                15 -> StringUtils.maskPayerPanNumber(initiatorId)
                14 -> maskInitiatorId(initiatorId,8)
                13 -> maskInitiatorId(initiatorId,7)
                12 -> maskInitiatorId(initiatorId,6)
                11 -> maskInitiatorId(initiatorId,5)
                10 -> maskInitiatorId(initiatorId,4)
                else -> initiatorId
            }

        rl_approval_code.visibility =
            when (mobileNfcPayment.transactionStatus.lowercase()) {
                FAILED_TRANSACTION -> View.GONE
                else -> View.VISIBLE
            }

        tv_approval_code.text = mobileNfcPayment.approvalCode

        btn_cancellation.visibility =
            if (isVoidOrVoidedMobilePay(mobileNfcPayment) || mobileNfcPayment.transactionStatus.lowercase() == FAILED_TRANSACTION) {
                View.GONE
            } else {
                View.VISIBLE
            }
    }

    private fun isVoidOrVoidedMobilePay(mobileNfcPayment: MobileNfcPayment): Boolean {
        return mobileNfcPayment.isVoided || mobileNfcPayment.transactionType.lowercase() == TRANSACTION_VOID_SALE
    }

    override fun getBindingVariable(): Int = BR.mobilePayTransactionViewModel

    override fun getLayout(): Int = R.layout.activity_mobile_nfc_transaction_detail

    override fun getViewModel(): MobileNfcTransactionDetailViewModel =
        ViewModelProviders.of(this)[MobileNfcTransactionDetailViewModel::class.java]

    override fun onPositiveButtonClick() {
        getViewModel().printMobilePayReceipt(ReceiptVersion.CUSTOMER_COPY)
    }

    override fun onNegativeButtonClick() {
        finish()
    }

    override fun onLoginDialogCorrectCredentialEntered() {
        getViewModel().performVoid(originalMobileNfcPayment!!)
    }

    override fun onLoginDialogCancelButtonClicked() {
        // do nothing
    }

    override fun onResume() {
        super.onResume()
        registerBroadCastReceivers()
    }

    private fun registerBroadCastReceivers() {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                reprintBroadcastReceiver,
                IntentFilter(Constants.INTENT_ACTION_REPRINT_UI)
            )
    }

    override fun onPause() {
        unregisterBroadcastReceivers()
        super.onPause()
    }

    private fun unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(reprintBroadcastReceiver)
    }
}