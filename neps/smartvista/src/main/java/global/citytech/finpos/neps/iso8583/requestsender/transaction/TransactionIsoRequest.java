package global.citytech.finpos.neps.iso8583.requestsender.transaction;


import java.math.BigDecimal;

import global.citytech.finposframework.usecases.transaction.Request;

/**
 * Created by Saurav Ghimire on 1/21/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class TransactionIsoRequest implements Request {
    protected String pan;
    protected BigDecimal transactionAmount;
    protected String localDateTime;
    protected String track2Data;
    protected String additionalData;
    protected String currencyCode;
    protected String pinBlock;
    protected String emvData;
    protected String cvv;
    protected boolean isFallbackFromIccToMag;
    protected String expiryDate;

    public String getPan() {
        return pan;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public String getLocalDateTime() {
        return localDateTime;
    }

    public String getTrack2Data() {
        return track2Data;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getPinBlock() {
        return pinBlock;
    }

    public String getEmvData() {
        return emvData;
    }

    public boolean isFallbackFromIccToMag() {
        return isFallbackFromIccToMag;
    }

    public String getCvv() {
        return cvv;
    }

    public String getExpiryDate() {
        return expiryDate;
    }
}
