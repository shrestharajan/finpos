package global.citytech.common.printer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.hardware.io.printer.Style;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;

/**
 * Created by BikashShrestha on 2/28/19.
 */


public class BitmapGenerator {
    public static final int PAPER_WIDTH = 384;


    public static Bitmap fromMultiColumn(List<String> list, Style style, int[] columns) {
        List<Bitmap> multiLineBitmapList = bmpMultiColumn(list, style, columns);
        Bitmap mergedBitmap = mergeMultipleBitmap(multiLineBitmapList);
        return removeBlackSpace(mergedBitmap);
    }

    public static Bitmap resize(Bitmap bm) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) 384) / width;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleWidth);
        return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
    }

    private static Bitmap mergeMultipleBitmap(List<Bitmap> multiLineBitmapList) {
        int totalWidth = 0;
        int greatestBitmapHeight = getHeightOfBiggestBitmap(multiLineBitmapList);
        List<Bitmap> bitmapsMergedWithEmptyBitmapList = new ArrayList<>();

        for (Bitmap bitmap : multiLineBitmapList) {
            totalWidth += bitmap.getWidth();
            Bitmap emptyBitmap = Bitmap.createBitmap(bitmap.getWidth(), greatestBitmapHeight,
                    Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(emptyBitmap);
            canvas.drawColor(0xfff);
            emptyBitmap.eraseColor(Color.WHITE);
            bitmapsMergedWithEmptyBitmapList.add(overlapBitmap(emptyBitmap, bitmap));
        }

        Bitmap comboBitmap = Bitmap.createBitmap(totalWidth, greatestBitmapHeight,
                Bitmap.Config.ARGB_8888);
        Canvas comboImage = new Canvas(comboBitmap);
        comboImage.drawBitmap(bitmapsMergedWithEmptyBitmapList.get(0), 0.0F, 0.0F,
                null);
        int appendWidth = bitmapsMergedWithEmptyBitmapList.get(0).getWidth();
        for (int i = 1; i < bitmapsMergedWithEmptyBitmapList.size(); i++) {
            comboImage.drawBitmap(bitmapsMergedWithEmptyBitmapList.get(i), appendWidth, 0.0F,
                    null);
            appendWidth += bitmapsMergedWithEmptyBitmapList.get(i).getWidth();
        }
        return removeBlackSpace(comboBitmap);
    }

    public static Bitmap fromDoubleColumn(String label, String value, Style style,
                                          Layout.Alignment masterAlignment) {
        double totalLength = label.length() + value.length();

        double labelPercentage = label.length()/totalLength;
        double valuePercentage = value.length()/totalLength;

        style.setAlignment(Style.Align.LEFT);
        Bitmap labelBitmap = fromText(label, style, masterAlignment, (int) (PAPER_WIDTH * labelPercentage));
        if (masterAlignment == Layout.Alignment.ALIGN_NORMAL)
            masterAlignment = Layout.Alignment.ALIGN_OPPOSITE;
        else
            masterAlignment = Layout.Alignment.ALIGN_NORMAL;
        Bitmap valueBitmap = fromText(value, style, masterAlignment, (int) (PAPER_WIDTH * valuePercentage));
        List<Bitmap> bitmaps = new ArrayList<>();
        bitmaps.add(labelBitmap);
        bitmaps.add(valueBitmap);
        Bitmap mergedBitmap = mergeMultipleBitmap(bitmaps);
        cleanupBitmap(bitmaps);
        return mergedBitmap;
    }

    private static void cleanupBitmap(List<Bitmap> bitmaps) {
        for(Bitmap bitmap: bitmaps)
            cleanupBitmap(bitmap);
    }

    private static int getHeightOfBiggestBitmap(List<Bitmap> list) {
        int greatestBitmapHeight = 0;
        for (int i = 0; i < list.size(); i++) {
            Bitmap bitmap = list.get(i);
            if (bitmap.getHeight() > greatestBitmapHeight) {
                greatestBitmapHeight = bitmap.getHeight();
            }
        }
        return greatestBitmapHeight;
    }

    private static List<Bitmap> bmpMultiColumn(List<String> list, Style style, int[] columns) {
        int noOfColumns = list.size();
        if (columns != null && columns.length != 0) {
            int totalWidth = 0;
            for (int i : columns) {
                totalWidth += i;
            }
            if (totalWidth > 100)
                throw new PosException(PosError.DEVICE_ERROR_SUM_COLUMNS_EXCEED);
        }
        List<Bitmap> bitmaps = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            String text = list.get(i);
            Bitmap bitmap;
            int width = getColumnWidth(i, columns, noOfColumns);
            if (!hasMinimumWidth(width, style))
                throw new PosException(PosError.DEVICE_ERROR_COLUMN_WIDTH_SMALL);
            //Log.v("test","Width: " + style.getFontSize().toString() + " | "+ list.get(i) +" | "+width);

            Layout.Alignment alignmentForEnglish = Layout.Alignment.ALIGN_NORMAL;
            Layout.Alignment alignmentForArabic = Layout.Alignment.ALIGN_OPPOSITE;
            if (style.getAlignment() == Style.Align.RIGHT) {
                alignmentForEnglish = Layout.Alignment.ALIGN_OPPOSITE;
                alignmentForArabic = Layout.Alignment.ALIGN_NORMAL;
            }

            if (isProbablyArabic(text) && !isProbablyArabicNumberOnly(text))
                bitmap = fromText(list.get(i), style, alignmentForArabic, width);
            else
                bitmap = fromText(list.get(i), style, alignmentForEnglish, width);

            bitmaps.add(removeBlackSpace(bitmap));
        }
        return bitmaps;
    }

    private static boolean hasMinimumWidth(int width, Style style) {
        switch (style.getFontSize()) {
            case XSMALL:
                return width > 12;
            case SMALL:
                return width > 14;
            case NORMAL:
                return width > 16;
            case LARGE:
                return width > 22;
            case XLARGE:
                return width > 28;
        }
        return false;
    }

    private static int getColumnWidth(int index, int[] columnsWidths, int noOfColumns) {
        if (columnsWidths == null)
            return PAPER_WIDTH / noOfColumns;

        if (columnsWidths.length > index)
            return (PAPER_WIDTH * columnsWidths[index]) / 100;

        int usedTotalWidth = 0;
        if (columnsWidths.length != 0) {
            for (int i : columnsWidths) {
                usedTotalWidth += i;
            }
        }
        int remainingWidth = 100 - usedTotalWidth;
        int remainingColumn = noOfColumns - columnsWidths.length;
        return ((PAPER_WIDTH * remainingWidth) / 100) / remainingColumn;
    }

    public static Bitmap fromText(String text, Style style, Layout.Alignment masterAlignment,
                                  int width) {
        if (width == 0) {
            width = 1;
        }
        if (text.trim().isEmpty()) {

            Bitmap bitmap = Bitmap.createBitmap(width, getHeightForEmptySpace(style),
                    Bitmap.Config.RGB_565);
            Canvas canvas1 = new Canvas(bitmap);
            canvas1.drawColor(0xfff);
            bitmap.eraseColor(Color.WHITE);
            return bitmap;
        }

        int fontSize = FontManager.getTextSize(style);
        boolean bold = style.isBold();

        if (isProbablyArabic(text) && !isProbablyArabicNumberOnly(text)) {
            if (bold)
                fontSize = (int) (fontSize * 1.1);
            else
                bold = true;
            if (masterAlignment == Layout.Alignment.ALIGN_NORMAL) {
                masterAlignment = Layout.Alignment.ALIGN_OPPOSITE;
            } else if (masterAlignment == Layout.Alignment.ALIGN_OPPOSITE) {
                masterAlignment = Layout.Alignment.ALIGN_NORMAL;
            }
        }
        int iWidth = width;
        if (text != null && text.length() != 0) {
            TextPaint textPaint = new TextPaint(65);
            textPaint.setStyle(Paint.Style.FILL);
            textPaint.setColor(-16777216);
            if(style.getFontType() == null)
                style.setFontType(Style.FontType.ARIAL);
            textPaint.setTypeface(FontManager.getTypeFace(style.getFontType(),true));
            textPaint.setTextScaleX(0.8f);
            textPaint.setFakeBoldText(bold);
            if (style.getFontSize() == Style.FontSize.LARGE ||
                    style.getFontSize() == Style.FontSize.XLARGE) {
                textPaint.setTextScaleX(0.7f);
            }
            textPaint.setTextSize((float) fontSize);
            textPaint.setTextSkewX(style.isItalic() ? -0.25F : 0);
            textPaint.setUnderlineText(style.isUnderline());

            Layout.Alignment al = masterAlignment;

            int iRtOffset = al == Layout.Alignment.ALIGN_OPPOSITE ? 7 : 0;

            StaticLayout mTextLayout = new StaticLayout(text, textPaint, iWidth - iRtOffset,
                    al, 1.0F, 0F, false);
            int iImageHeight = mTextLayout.getHeight();
            if (iImageHeight % 2 != 0) {
                ++iImageHeight;
            }
            if (style.getFontSize() == Style.FontSize.NORMAL && style.isBold()) {
                //iImageHeight += 7;
            }

            Bitmap b = Bitmap.createBitmap(iWidth, iImageHeight, Bitmap.Config.RGB_565);
            Canvas c = new Canvas(b);
            Paint paint = new Paint(65);
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(-1);
            c.drawPaint(paint);
            c.save();
            c.translate(0.0F, 0.0F);
            mTextLayout.draw(c);
            c.restore();


            Bitmap bTop = Bitmap.createBitmap(iWidth, 1, Bitmap.Config.RGB_565);
            Canvas cTop = new Canvas(bTop);
            cTop.drawPaint(paint);
            cTop.save();
            cTop.restore();
            Bitmap retBitmap = mergeBitmap(bTop, b);
            return cropWhiteSpace(retBitmap, style);
            //return retBitmap;
        } else {
            throw new PosException(PosError.DEVICE_ERROR_INPUT_TEXT_INVALID);
        }
    }

    private static int getHeightForEmptySpace(Style style) {
        switch (style.getFontSize()) {
            case XLARGE:
                return 60;
            case LARGE:
                return 45;
            case SMALL:
            case XSMALL:
                return 20;
            default:
                return 30;
        }
    }

    private static Bitmap cropWhiteSpace(Bitmap bitmap, Style style) {
        int topY = 0;
        int height = 0;

        if (style.getFontSize() == Style.FontSize.NORMAL) {
            topY = 0;
            height = 0;
        } else if (style.getFontSize() == Style.FontSize.SMALL ||
                style.getFontSize() == Style.FontSize.XSMALL ) {
            topY = 0;
            height = 2;
        } else {
            topY = 2;
            height = 4;
        }
        Bitmap croppedBitmap = Bitmap.createBitmap(
                bitmap,
                0,
                topY,
                bitmap.getWidth(),
                bitmap.getHeight() - height
        );
        cleanupBitmap(bitmap);
        return croppedBitmap;
    }

    private static boolean isProbablyArabic(String s) {
        for (int i = 0; i < s.length(); ) {
            int c = s.codePointAt(i);
            if (c >= 0x0600 && c <= 0x06E0)
                return true;
            i += Character.charCount(c);
        }
        return false;
    }

    private static boolean isProbablyArabicNumberOnly(String s) {
        boolean onlyNumber = true;
        for (int i = 0; i < s.length(); ) {
            int c = s.codePointAt(i);
            if (!(c >= 0x0660 && c <= 0x0669))
                onlyNumber = false;
            i += Character.charCount(c);
        }
        return onlyNumber;
    }



    private static Bitmap mergeBitmap(Bitmap bmpTop, Bitmap bmpBottom) {
        if (bmpTop.getWidth() != bmpBottom.getWidth()) {
            return null;
        } else {
            int width = bmpTop.getWidth();
            int height = bmpBottom.getHeight();
            Bitmap comboBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            Canvas comboImage = new Canvas(comboBitmap);
            comboImage.drawBitmap(bmpBottom, 0.0F, 0.1F, (Paint) null);
            cleanupBitmap(bmpTop);
            cleanupBitmap(bmpBottom);
            return comboBitmap;
        }
    }

    private static Bitmap overlapBitmap(Bitmap bmpTop, Bitmap bmpBottom) {
        if (bmpTop.getWidth() != bmpBottom.getWidth()) {
            return null;
        } else {
            int width = bmpTop.getWidth();
            int height = bmpTop.getHeight();
            height = bmpBottom.getHeight() > height ? bmpBottom.getHeight() : height;
            Bitmap comboBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            Canvas comboImage = new Canvas(comboBitmap);
            comboImage.drawBitmap(bmpTop, 0.0F, 0.0F, (Paint) null);
            comboImage.drawBitmap(bmpBottom, 0.0F, 0.0F, (Paint) null);
            cleanupBitmap(bmpTop);
            cleanupBitmap(bmpBottom);
            return comboBitmap;
        }
    }

    private static void cleanupBitmap(Bitmap bitmap) {
        if(bitmap!=null){
            bitmap.recycle();
            bitmap = null;
        }
    }

    public static Bitmap mergeBitmapsVertically(List<Bitmap> bitmaps) {
        int w = 0, h = 0;
        for (int i = 0; i < bitmaps.size(); i++) {
            if (w == 0)
                w = bitmaps.get(i).getWidth();
            if (i < bitmaps.size() - 1) {
                w = bitmaps.get(i).getWidth() > bitmaps.get(i + 1).getWidth() ?
                        bitmaps.get(i).getWidth() : bitmaps.get(i + 1).getWidth();
            }
            h += bitmaps.get(i).getHeight();
        }

        if (w == 0 || h == 0) {
            w = 1;
            h = 1;
        }

        Bitmap temp = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
        temp.eraseColor(Color.WHITE);
        Canvas canvas = new Canvas(temp);
        int top = 0;
        for (int i = 0; i < bitmaps.size(); i++) {
            Bitmap bitmap = bitmaps.get(i);
            canvas.drawBitmap(bitmap, 0f, i == 0 ? 0 : top, null);
            top = top + bitmaps.get(i).getHeight();
        }
        canvas.save();
        canvas.restore();
        Bitmap removedbitmap = removeBlackSpace(temp);
        cleanupBitmap(temp);
        return removedbitmap;
    }

    private static Bitmap removeBlackSpace(Bitmap bitmap) {
        Bitmap emptyBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas1 = new Canvas(emptyBitmap);
        canvas1.drawColor(0xfff);
        emptyBitmap.eraseColor(Color.WHITE);
        canvas1.save();
        canvas1.restore();
        Bitmap overlappedBitmap = overlapBitmap(emptyBitmap, bitmap);
        cleanupBitmap(emptyBitmap);
        cleanupBitmap(bitmap);
        return overlappedBitmap;
    }

    public static Bitmap updateAlignment(Bitmap bitmap, Style.Align alignment) {
        if (bitmap.getWidth() >= PAPER_WIDTH) {
            return bitmap;
        }
        Bitmap comboBitmap = Bitmap.createBitmap(PAPER_WIDTH, bitmap.getHeight(),
                Bitmap.Config.RGB_565);
        comboBitmap.eraseColor(Color.WHITE);
        Canvas comboImage = new Canvas(comboBitmap);
        int diff = PAPER_WIDTH - bitmap.getWidth();
        int leftPadding = 0;
        if (diff > 0) {
            switch (alignment) {
                case CENTER:
                    leftPadding = diff / 2;
                    break;
                case RIGHT:
                    leftPadding = diff;
                    break;
            }
        }
        comboImage.drawBitmap(bitmap, (float) leftPadding, 0.1F, (Paint) null);
        cleanupBitmap(bitmap);
        return comboBitmap;
    }

    @Nullable
    public static Bitmap base64ToImage(@NotNull String data) {
        byte[] decodedString = global.citytech.common.Base64.decode(data);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }
}
