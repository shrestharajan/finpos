package global.citytech.finpos.merchant.presentation.dynamicqr.receipt

import global.citytech.finpos.merchant.domain.model.app.QrPayment
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.TRANSACTION_VOID_SALE
import global.citytech.finpos.merchant.utils.retrieveCurrencyName
import global.citytech.finposframework.repositories.TerminalRepository
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.receipt.Performance
import global.citytech.finposframework.usecases.transaction.receipt.Retailer
import global.citytech.finposframework.utility.StringUtils

class DynamicQrReceiptGenerator(val terminalRepository: TerminalRepository) {
    private lateinit var initiatorId: String

    fun generate(qrPayment: QrPayment): DynamicQrReceipt = DynamicQrReceipt(
        prepareRetailerInfo(qrPayment),
        preparePerformanceInfo(qrPayment),
        prepareQrTransactionDetail(qrPayment),
        prepareQrCodeString(qrPayment),
        prepareThankYouMessage()
    )

    private fun prepareThankYouMessage(): String = "THANK YOU FOR USING QR\nPLEASE RETAIN RECEIPT"

    private fun prepareQrCodeString(qrPayment: QrPayment): String {
        val stringBuilder = StringBuilder()
        stringBuilder.append("MID:")
        stringBuilder.append("\t")
        stringBuilder.append(qrPayment.merchantId)
        stringBuilder.append("\n")
        stringBuilder.append("TID:")
        stringBuilder.append("\t")
        stringBuilder.append(qrPayment.terminalId)
        stringBuilder.append("\n")
        stringBuilder.append("REFERENCE NUMBER:")
        stringBuilder.append("\t")
        stringBuilder.append(qrPayment.referenceNumber)
        stringBuilder.append("\n")
        stringBuilder.append("PAYMENT INITIATOR:")
        stringBuilder.append("\t")
        stringBuilder.append(qrPayment.paymentInitiator)
        stringBuilder.append("\n")
        stringBuilder.append("INITIATOR ID:")
        stringBuilder.append("\t")
        stringBuilder.append(StringUtils.maskPayerPanNumber(qrPayment.initiatorId))
        stringBuilder.append("\n")
        stringBuilder.append("AMOUNT:")
        stringBuilder.append("\t")
        stringBuilder.append(qrPayment.transactionAmount)
        stringBuilder.append("\n")
        stringBuilder.append("DATE:")
        stringBuilder.append("\t")
        stringBuilder.append(qrPayment.transactionDate)
        stringBuilder.append("\n")
        stringBuilder.append("TIME:")
        stringBuilder.append("\t")
        stringBuilder.append(qrPayment.transactionTime)
        stringBuilder.append("\n")
        return stringBuilder.toString()
    }

    private fun prepareQrTransactionDetail(
        qrPayment: QrPayment
    ): QrTransactionDetail {
        val transactionType =
            if (qrPayment.transactionType.lowercase() == TRANSACTION_VOID_SALE) {
                TransactionType.VOID.printName
            } else {
                qrPayment.transactionType
            }
        initiatorId = qrPayment.initiatorId.toString()

        initiatorId =
            when (initiatorId.length) {
                15 -> StringUtils.maskPayerPanNumber(initiatorId)
                14 -> maskInitiatorId(8)
                13 -> maskInitiatorId(7)
                12 -> maskInitiatorId(6)
                11 -> maskInitiatorId(5)
                10 -> maskInitiatorId(4)
                else -> initiatorId
            }
        return QrTransactionDetail(
            qrPayment.referenceNumber,
            transactionType,
            retrieveCurrencyName(qrPayment.transactionCurrency),
            StringUtils.formatAmountTwoDecimal(qrPayment.transactionAmount.toDouble()),
            qrPayment.transactionStatus,
            qrPayment.paymentInitiator!!,
            initiatorId,
            qrPayment.approvalCode!!
        )
    }

    private fun maskInitiatorId(start: Int): String {
        return StringUtils.maskString(initiatorId, start, initiatorId.length - 2, 'X')
    }

    private fun preparePerformanceInfo(qrPayment: QrPayment): Performance =
        Performance.Builder()
            .withStartDateTime(qrPayment.transactionDate.plus(qrPayment.transactionTime))
            .withEndDateTime(qrPayment.transactionDate.plus(qrPayment.transactionTime))
            .build()

    private fun prepareRetailerInfo(qrPayment: QrPayment): Retailer = Retailer.Builder()
        .withRetailerLogo(this.terminalRepository.findTerminalInfo().merchantPrintLogo)
        .withRetailerName(this.terminalRepository.findTerminalInfo().merchantName)
        .withRetailerAddress(this.terminalRepository.findTerminalInfo().merchantAddress)
        .withMerchantId(qrPayment.merchantId)
        .withTerminalId(qrPayment.terminalId)
        .build()
}