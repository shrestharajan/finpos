package global.citytech.finpos.processor.nibl.receipt.detailreport;

/**
 * Created by Unique Shakya on 1/4/2021.
 */
public class DetailReportReceiptLabels {

    public static final String TERMINAL_ID = "TID";
    public static final String MERCHANT_ID = "MID";
    public static final String DATE = "DATE";
    public static final String TIME = "TIME";
    public static final String BATCH_NUMBER = "BATCH NO";
    public static final String HOST = "HOST";
    public static final String DETAIL_REPORT = "DETAIL REPORT";
    public static final String CARD_NUMBER = "CARD NUMBER";
    public static final String CARD_SCHEME = "CARD SCHEME";
    public static final String INVOICE_NUMBER = "INVOICE NUMBER";
    public static final String APPROVAL_CODE = "APPROVAL CODE";
    public static final String TRANSACTION_TYPE = "TRANSACTION";
    public static final String TRANSACTION_DATE = "TRANSACTION DATE";
    public static final String TRANSACTION_TIME = "TRANSACTION TIME";
    public static final String AMOUNT = "AMOUNT";
    public static final String DIVIDER = "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --";
    public static final String FOOTER = "END OF DETAIL REPORT";
    public static final String LINE_BREAK = "  ";
}
