package global.citytech.finpos.merchant.domain.usecase.qr.nqr

import global.citytech.finpos.merchant.domain.usecase.qr.*
import global.citytech.finpos.merchant.domain.usecase.qr.base.QrGenerator
import global.citytech.finpos.merchant.framework.qr.CrcCalculatorImpl
import global.citytech.finposframework.log.Logger

/**
 * @author sachin
 */
class NqrGenerator : QrGenerator<NqrGenerateRequest, NqrGenerateResponse> {
    var dataObjectContentDumper: QrDataObjectContentDumper
    var crcCalculator: CrcCalculator

    init {
        dataObjectContentDumper = QrDataObjectContentDumper()
        crcCalculator =
            CrcCalculatorImpl()
    }

    override fun generate(qrGenerateRequest: NqrGenerateRequest): NqrGenerateResponse {
        Logger.getLogger("NQRGenerator").log("$qrGenerateRequest")
        val qrContentBuilder: StringBuilder = StringBuilder("")
        val conventionFieldContent = getConventionalFieldsContent(qrGenerateRequest)
        Logger.getLogger("NQRGenerator").log("CONVENTION FIELD CONTENT >>>> $conventionFieldContent")
        qrContentBuilder.append(conventionFieldContent)
        val merchantAccountInfoContent = getMerchantAccountInfoContent(qrGenerateRequest)
        Logger.getLogger("NQRGenerator").log("MERCHANT ACCOUNT INFORMATION >>>> $merchantAccountInfoContent")
        qrContentBuilder.append(merchantAccountInfoContent)
        val additionalMerchantInfoContent =
            getAdditionalMerchantAccountInfoContent(qrGenerateRequest)
        Logger.getLogger("NQRGenerator").log("ADDITIONAL MERCHANT INFO >>>> $additionalMerchantInfoContent")
        qrContentBuilder.append(additionalMerchantInfoContent)
        val transactionInfoContent = getTransactionInfoContent(qrGenerateRequest)
        Logger.getLogger("NQRGenerator").log("TRANSACTION INFO CONTENT >>>> $transactionInfoContent")
        qrContentBuilder.append(transactionInfoContent)
        val additionalParametersContent = getAdditionalParametersContent(qrGenerateRequest)
        Logger.getLogger("NQRGenerator").log("ADDITIONAL PARAMETERS INFO >>>> $additionalParametersContent")
        qrContentBuilder.append(additionalParametersContent)
        val crcField =
            NqrField(QrField.CYCLIC_REDUNDANCY_CHECK, prepareCrcValue(qrContentBuilder.toString()))
        Logger.getLogger("NQRGenerator").log(
            "CRC >>>> ${
                dataObjectContentDumper.dumpQrContent(
                    arrayListOf(crcField)
                )
            }"
        )
        qrContentBuilder.append(dataObjectContentDumper.dumpQrContent(arrayListOf(crcField)))
        return NqrGenerateResponse(qrContentBuilder.toString())
    }

    private fun getAdditionalParametersContent(qrGenerateRequest: NqrGenerateRequest): String {
        val additionalParametersField = ArrayList<QrDataObject>()
        val additionalParameters = NqrField(QrField.ADDITIONAL_DATA,prepareAdditionalParametersValue(qrGenerateRequest))
        additionalParametersField.add(additionalParameters)
        return dataObjectContentDumper.dumpQrContent(additionalParametersField)
    }

    private fun prepareAdditionalParametersValue(qrGenerateRequest: NqrGenerateRequest): Any {
        val additionalSubParametersField = ArrayList<QrDataObject>()
        val merchantCode = NqrField(QrField.ADDITIONAL_DATA_MERCHANT_CODE, qrGenerateRequest.merchantCode)
        additionalSubParametersField.add(merchantCode)
        val terminalLabel = NqrField(QrField.ADDITIONAL_DATA_TERMINAL_LABEL, qrGenerateRequest.terminalId)
        additionalSubParametersField.add(terminalLabel)
        val billNumber = NqrField(QrField.ADDITIONAL_DATA_BILL_NUMBER, qrGenerateRequest.billNumber)
        additionalSubParametersField.add(billNumber)
        return  dataObjectContentDumper.dumpQrContent(additionalSubParametersField)
    }

    private fun getTransactionInfoContent(qrGenerateRequest: NqrGenerateRequest): String {
        val transactionFields = ArrayList<QrDataObject>()
        val transactionCurrencyField =
            NqrField(QrField.TRANSACTION_CURRENCY, qrGenerateRequest.transactionCurrency)
        transactionFields.add(transactionCurrencyField)
        if (qrGenerateRequest.initiationMethod.equals(NqrConstants.DYNAMIC_QR)) {
            val transactionAmountField =
                NqrField(QrField.TRANSACTION_AMOUNT, qrGenerateRequest.transactionAmount)
            transactionFields.add(transactionAmountField)
        }
        return dataObjectContentDumper.dumpQrContent(transactionFields)
    }

    private fun getAdditionalMerchantAccountInfoContent(qrGenerateRequest: NqrGenerateRequest): String {
        val additionalInfoFields = ArrayList<QrDataObject>()
        val merchantCategoryCodeField =
            NqrField(QrField.MERCHANT_CATEGORY_CODE, qrGenerateRequest.merchantCategoryCode)
        additionalInfoFields.add(merchantCategoryCodeField)
        val countryCodeField = NqrField(QrField.COUNTRY_CODE, qrGenerateRequest.countryCode)
        additionalInfoFields.add(countryCodeField)
        val merchantNameField = NqrField(QrField.MERCHANT_NAME, qrGenerateRequest.merchantName)
        additionalInfoFields.add(merchantNameField)
        val merchantCityField = NqrField(QrField.MERCHANT_CITY, qrGenerateRequest.merchantCity)
        additionalInfoFields.add(merchantCityField)
        return dataObjectContentDumper.dumpQrContent(additionalInfoFields)
    }

    private fun getMerchantAccountInfoContent(qrGenerateRequest: NqrGenerateRequest): String {
        val guidValue = prepareGuidValue(qrGenerateRequest)
        val guidField = NqrField(QrField.GUID, guidValue)
        val dumpedGuidField = dataObjectContentDumper.dumpQrContent(arrayListOf(guidField))
        val nchlMerchantInfoField = NqrField(QrField.NCHL_MERCHANT_INFO, dumpedGuidField)
        return dataObjectContentDumper.dumpQrContent(arrayListOf(nchlMerchantInfoField))
    }

    private fun prepareGuidValue(qrGenerateRequest: NqrGenerateRequest): Any {
        val guidBuilder: StringBuilder = StringBuilder("")
        guidBuilder.append(NqrConstants.NCHL_TAG)
        guidBuilder.append(qrGenerateRequest.acquirerCode)
        guidBuilder.append(qrGenerateRequest.merchantCode)
        return guidBuilder.toString()
    }

    private fun getConventionalFieldsContent(qrGenerateRequest: NqrGenerateRequest): String {
        var conventionFields = ArrayList<QrDataObject>()
        val payloadFormatIndicatorField = NqrField(QrField.PAYLOAD_FORMAT_INDICATOR, NqrConstants.PAYLOAD_FORMAT_INDICATOR)
        val pointOfInitiationMethodField =
            NqrField(QrField.INITIATION_METHOD, qrGenerateRequest.initiationMethod)
        conventionFields.add(payloadFormatIndicatorField)
        conventionFields.add(pointOfInitiationMethodField)
        val conventionContent = dataObjectContentDumper.dumpQrContent(conventionFields)
        return conventionContent
    }

    private fun prepareCrcValue(allDataObjectsContent: String): String {
        return crcCalculator.calcualteCrc(allDataObjectsContent.toByteArray())
    }

}