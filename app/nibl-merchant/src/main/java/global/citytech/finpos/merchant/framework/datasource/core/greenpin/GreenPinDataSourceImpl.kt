package global.citytech.finpos.merchant.framework.datasource.core.greenpin

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.greenpin.GreenPinDataSource
import global.citytech.finpos.merchant.domain.model.greenpin.GreenPinRequestEntity
import global.citytech.finpos.merchant.domain.model.greenpin.GreenPinResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionAuthenticatorImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.*
import global.citytech.finpos.merchant.presentation.data.mapToGreenPinResponseUiModel
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.greenpin.GreenPinRequestItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finposframework.switches.ProcessorManager
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest
import io.reactivex.Observable

class GreenPinDataSourceImpl : GreenPinDataSource {
    override fun generateOtpRequest(
        configurationItem: ConfigurationItem,
        greenPinRequestItem: GreenPinRequestItem
    ): Observable<GreenPinResponseEntity> {

        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val transactionRepository =
            TransactionRepositoryImpl(
                NiblMerchant.INSTANCE
            )
        val transactionRequest = prepareTransactionRequest(greenPinRequestItem)

        val deviceController = DeviceControllerImpl(NiblMerchant.INSTANCE)
        val cardReader = CardSourceImpl(NiblMerchant.getActivityContext())
        val transactionAuthenticator =
            TransactionAuthenticatorImpl()
        val printerServiceImpl = PrinterSourceImpl(NiblMerchant.INSTANCE)
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)

        val greenPinRequestEntity = GreenPinRequestEntity(
            readCardService = cardReader,
            deviceController = deviceController,
            printerService = printerServiceImpl,
            transactionAuthenticator = transactionAuthenticator,
            transactionRepository = transactionRepository,
            transactionRequest = transactionRequest,
            applicationRepository = applicationRepository,
            ledService = LedSourceImpl(NiblMerchant.INSTANCE),
            soundService = SoundSourceImpl(NiblMerchant.INSTANCE)
        )

        val greenPinRequester =
            ProcessorManager.getInterface(
                terminalRepository,
                NotificationHandler
            ).greenPinRequester

        return Observable.fromCallable {
            greenPinRequester.execute(
                greenPinRequestEntity.mapToModel()
            ).mapToGreenPinResponseUiModel()
        }
    }


    private fun prepareTransactionRequest(greenPinRequestItem: GreenPinRequestItem): TransactionRequest {
        val transactionRequest = TransactionRequest(
            greenPinRequestItem.transactionType,
        )
        transactionRequest.amount = greenPinRequestItem.amount
        return transactionRequest
    }
}