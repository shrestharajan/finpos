package global.citytech.finpos.processor.neps.pinchange;

import java.math.BigDecimal;
import java.util.List;
import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.PosConditionCode;
import global.citytech.finpos.neps.iso8583.SmartVistaIccDataList;
import global.citytech.finpos.neps.iso8583.requestsender.pinchange.PinChangeIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.pinchange.PinChangeRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.processor.neps.NepsConstant;
import global.citytech.finpos.processor.neps.cardprocessor.ICCCardProcessor;
import global.citytech.finpos.processor.neps.cardprocessor.PICCCardProcessor;
import global.citytech.finpos.processor.neps.transaction.NepsInvalidResponseHandler;
import global.citytech.finpos.processor.neps.transaction.NepsIsoMessageReceiptHandler;
import global.citytech.finpos.processor.neps.transaction.TransactionExecutorTemplate;
import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finpos.processor.neps.transaction.TransactionUtils;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardSummary;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.PinBlock;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedAction;
import global.citytech.finposframework.hardware.io.led.LedLight;
import global.citytech.finposframework.hardware.io.led.LedRequest;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat;
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest;
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandler;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.HexString;
import global.citytech.finposframework.utility.StringUtils;

public class PinChangeUseCase extends TransactionExecutorTemplate {
    IsoMessageResponse isoMessageResponse;
    private String userPin = "";
    TransactionResponse transactionResponse;
    private String oldPin = "";

    protected final ReadCardRequest.MultipleAidSelectionListener multipleAidSelectionListener = aids -> {
        notifier.notify(Notifier.EventType.DETECTED_MULTIPLE_AID, "Processing..");
        return -1;
    };

    public PinChangeUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }


    @Override
    public TransactionResponse execute(global.citytech.finpos.processor.neps.transaction.TransactionRequest request) {
        setUpRequiredTransactionFields(request);
        checkIfDeviceReady();
        this.initTransaction(this.request.getApplicationRepository(), transactionRequest);
        this.retrieveCardResponse();
        try {
            processCard();

        } catch (FinPosException e) {
            e.printStackTrace();
            if (e.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT){
                throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
            } else if(
                    e.getType() == FinPosException.ExceptionType.READ_TIME_OUT) {
                throw new PosException(PosError.DEVICE_ERROR_CARD_READ_TIMEOUT);
            }else if(
                    e.getType() == FinPosException.ExceptionType.CONNECTION_ERROR){
                throw new PosException(PosError.DEVICE_ERROR_SERVER_CONNECTION_ERROR);
            }

            else throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
        return transactionResponse;
    }

    protected void printDebugReceiptIfDebugMode(PrinterService printerService, IsoMessageResponse isoMessageResponse) {
        boolean isDebugVersion = this.terminalRepository.findTerminalInfo().isDebugModeEnabled();
        if (isDebugVersion) {
            ReceiptHandler.IsoMessageReceiptHandler isoMessageReceiptHandler
                    = new NepsIsoMessageReceiptHandler(printerService);
            isoMessageReceiptHandler.printIsoMessageReceipt(isoMessageResponse);
        }
    }


    @Override
    protected void setUpRequiredTransactionFields(global.citytech.finpos.processor.neps.transaction.TransactionRequest request) {
        this.request = request;
        transactionRequest = request.getTransactionRequest();
        stan = this.terminalRepository.getSystemTraceAuditNumber();
        applicationRepository = request.getApplicationRepository();
    }


    @Override
    protected void retrieveCardResponse() {
        List<CardType> allowedCardTypes = TransactionUtils
                .getAllowedCardEntryModes(transactionRequest.getTransactionType());
        ReadCardRequest readCardRequest = this.prepareReadCardRequest(
                allowedCardTypes,
                request.getApplicationRepository(),
                transactionRequest
        );
        this.notifier.notify(Notifier.EventType.STARTING_READ_CARD);
        performTransactionWithCard(readCardRequest);
    }

    protected ReadCardRequest prepareReadCardRequest(
            List<CardType> allowedCardTypes,
            ApplicationRepository applicationRepository,
            global.citytech.finposframework.usecases.transaction.data.TransactionRequest
                    transactionRequest
    ) {
        ReadCardRequest readCardRequest = new ReadCardRequest(
                allowedCardTypes,
                transactionRequest.getTransactionType(),
                TransactionUtils.retrieveTransactionType9C(transactionRequest.getTransactionType()),
                applicationRepository.retrieveEmvParameterRequest()
        );
        readCardRequest.setStan(stan);
        readCardRequest.setAmount(transactionRequest.getAmount());
        readCardRequest.setCashBackAmount(transactionRequest.getAdditionalAmount());
        readCardRequest.setTransactionDate(DateUtils.yyMMddDate());
        readCardRequest.setTransactionTime(DateUtils.HHmmssTime());
        readCardRequest.setMultipleAidSelectionListener(this.multipleAidSelectionListener);
        readCardRequest.setIccDataList(SmartVistaIccDataList.get());
        readCardRequest.setCurrencyName(NepsConstant.PIN_PAD_CURRENCY);
        readCardRequest.setPinPadFixedLayout(!this.terminalRepository.shouldShufflePinPad());
        return readCardRequest;
    }

    private void performTransactionWithCard(ReadCardRequest readCardRequest) {
        readCardResponse = this.detectCard(readCardRequest, request.getReadCardService(),
                request.getLedService());
        request.getReadCardService().cleanUp();
        this.notifier.notify(Notifier.EventType.READING_CARD, "Reading Card..");
    }

    private ReadCardResponse detectCard(ReadCardRequest readCardRequest, ReadCardService readCardService, LedService ledService) {
        ledService.doTurnLedWith(new LedRequest(LedLight.BLUE, LedAction.ON));
        ReadCardResponse readCardResponse = readCardService.readCardDetails(readCardRequest);
        this.readCardResponse = readCardResponse;
        if (readCardResponse.getResult().getCode() == 310) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        this.validateReadCardResponse(readCardResponse);
        this.readCardResponse= readCardResponse;
        this.retrieveCardProcessor();
        this.readCardResponse= this.cardProcessor.validateReadCardResponse(readCardRequest, this.readCardResponse);
        PurchaseRequest purchaseRequest = preparePurchaseRequest(
                transactionRequest,
                readCardResponse.getCardDetails()
        );
        readCardResponse = this.cardProcessor.processCard(this.stan, purchaseRequest);
        return readCardResponse;
    }

    private void validateReadCardResponse(ReadCardResponse readCardResponse) {
        if (readCardResponse == null)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        if (readCardResponse.getResult() == Result.TIMEOUT)
            throw new PosException(PosError.DEVICE_ERROR_CARD_READ_TIMEOUT);
        if (readCardResponse.getResult() == Result.USER_CANCELLED ||
                readCardResponse.getResult() == Result.CANCEL)
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        if (readCardResponse.getResult() == Result.PROCESSING_ERROR)
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        if (readCardResponse.getResult() == Result.FAILURE)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
    }

    private void retrieveCardProcessor() {
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case ICC:
                this.cardProcessor = new ICCCardProcessor(
                        this.request.getTransactionRepository(),
                        this.request.getTransactionAuthenticator(),
                        this.request.getApplicationRepository(),
                        this.request.getReadCardService(),
                        this.notifier,
                        terminalRepository
                );
                break;
            case PICC:
                this.cardProcessor = new PICCCardProcessor(
                        this.request.getTransactionRepository(),
                        this.request.getTransactionAuthenticator(),
                        this.request.getApplicationRepository(),
                        this.request.getReadCardService(),
                        this.notifier,
                        this.request.getLedService(),
                        this.request.getSoundService(),
                        terminalRepository
                );
                break;
            default:
                break;
        }
    }

    @Override
    protected void processCard() {
        PurchaseRequest purchaseRequest = preparePurchaseRequest(
                transactionRequest,
                readCardResponse.getCardDetails()
        );
        this.notifier.notify(Notifier.EventType.PROCESSING, "Please wait...");
        PinBlock oldPinBlockResponse = this.retrieveOldPinBlock(purchaseRequest);
        PinBlock newPinBlockResponse = this.retrieveInitialPinBlock(purchaseRequest);
        proceedConfirmPin(purchaseRequest, newPinBlockResponse);
    }

    protected PinBlock retrieveOldPinBlock(PurchaseRequest purchaseRequest) {
        PinBlock oldPinBlock = new PinBlock("", true);
        String pinPadAmountMessage = "";
        oldPinBlock = this.retrievePinBlock(
                purchaseRequest
                        .getCardDetails()
                        .getPrimaryAccountNumber(),
                pinPadAmountMessage,
                "Enter Old Pin",
                "Enter Old PIN"
        );
        oldPin = oldPinBlock.getPin();
        return oldPinBlock;

    }

    protected PinBlock retrieveInitialPinBlock(PurchaseRequest purchaseRequest) {
        PinBlock newPinBlock = new PinBlock("Online", false);
        String pinPadAmountMessage = "";
        newPinBlock = this.retrievePinBlock(
                purchaseRequest
                        .getCardDetails()
                        .getPrimaryAccountNumber(),
                pinPadAmountMessage,
                "Enter New Pin",
                "Enter PIN"
        );
        return newPinBlock;
    }

    protected PinBlock retrieveConfirmPinBlock(PurchaseRequest purchaseRequest) {
        PinBlock pinBlock = new PinBlock("Online", false);
        String pinPadAmountMessage = "";
        pinBlock = this.retrievePinBlock(
                purchaseRequest
                        .getCardDetails()
                        .getPrimaryAccountNumber(),
                pinPadAmountMessage,
                "Confirm Pin",
                "Confirm PIN"
        );
        return pinBlock;
    }

    private PinBlock retrievePinBlock(String primaryAccountNumber, String pinPadAmountMessage, String pinPadMessage, String title) {
        if (!StringUtils.isEmpty(primaryAccountNumber)) {
            CardSummary cardSummary = new CardSummary(readCardResponse.getCardDetails().getCardSchemeLabel(),
                    primaryAccountNumber,
                    readCardResponse.getCardDetails().getCardHolderName(),
                    readCardResponse.getCardDetails().getExpiryDate());
            PinRequest pinRequest = new PinRequest(
                    primaryAccountNumber,
                    pinPadMessage,
                    PinBlockFormat.ISO9564_FORMAT_0,
                    4,
                    4,
                    30,
                    pinPadAmountMessage,
                    !this.terminalRepository.shouldShufflePinPad(),
                    false,
                    title
            );
            pinRequest.setCardSummary(cardSummary);
            PinResponse response = request.getTransactionAuthenticator().authenticateUser(pinRequest);
            this.logger.debug("::: PIN RESPONSE ::: " + response.toString());
            if (response.getResult() == Result.SUCCESS) {
                return response.getPinBlock();
            } else if (response.getResult() == Result.USER_CANCELLED)
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            else if (response.getResult() == Result.TIMEOUT || response.getResult() == Result.FAILURE) {
                throw new PosException(PosError.DEVICE_ERROR_CARD_READ_TIMEOUT);
            } else
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }


    void proceedConfirmPin(PurchaseRequest purchaseRequest, PinBlock pinBlock) {
        PinBlock confirmPinBlockResponse = this.retrieveConfirmPinBlock(purchaseRequest);
        logger.log("Pin 1 " + pinBlock.getPin());
        logger.log("Pin 2 " + confirmPinBlockResponse.getPin());
        if (confirmPinBlockResponse.getPin().equals(pinBlock.getPin())) {
            userPin = confirmPinBlockResponse.getPin();
            this.notifier.notify(Notifier.EventType.PROCESSING, "Transmitting Request...");
            isoMessageResponse = sendIsoMessage();
            printDebugReceiptIfDebugMode(request.getPrinterService(), isoMessageResponse);
            transactionResponse = checkForInvalidResponseAndProceed(isoMessageResponse);
        } else {
            this.notifier.notify(Notifier.EventType.PIN_UNMATCH, "Please wait...");
            proceedConfirmPin(purchaseRequest, pinBlock);

        }
    }

    @Override
    protected String getClassName() {
        return null;
    }

    @Override
    protected TransactionIsoRequest prepareIsoRequest(ReadCardResponse readCardResponse, TransactionRequest transactionRequest) {
        return PinChangeIsoRequest.Builder
                .newInstance()
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .transactionAmount(BigDecimal.valueOf(0.00))
                .stan(stan)
                .localDateTime(DateUtils.yyMMddHHmmssDate())
                .expirationDate(readCardResponse.getCardDetails().getExpiryDate())
                .posEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
                .posConditionCode(PosConditionCode.ATTENDANT_TERMINAL.getValue())
                .track2Data(readCardResponse.getCardDetails().getTrackTwoData())
                .additionalData(HexString.stringToHex("002")+HexString.stringToHex("008")+ this.userPin)
                .currencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .emvData(readCardResponse.getCardDetails().getIccDataBlock())
                .pinBlock(oldPin)
                .build();
    }


    protected TransactionResponse checkForInvalidResponseAndProceed(IsoMessageResponse isoMessageResponse) {
        InvalidResponseHandlerResponse invalidResponseHandlerResponse = this.checkForInvalidResponseFromHost(isoMessageResponse);
        if (invalidResponseHandlerResponse.isInvalid()) {
            return this.handleInvalidResponseFromHost(invalidResponseHandlerResponse);
        } else {
            return proceedValidResponseFromHost(isoMessageResponse);
        }
    }


    protected TransactionResponse handleInvalidResponseFromHost(InvalidResponseHandlerResponse invalidResponseHandlerResponse) {
        return prepareErrorResponse(invalidResponseHandlerResponse.getMessage(), false);
    }

    private TransactionResponse proceedValidResponseFromHost(IsoMessageResponse isoMessageResponse) {
        boolean transactionApproved = this.transactionApprovedByActionCode(isoMessageResponse);
        updateTransactionIds(false);
        return this.prepareResponse(isoMessageResponse, transactionApproved,
                request.getApplicationRepository(), false);
    }

    protected InvalidResponseHandlerResponse checkForInvalidResponseFromHost(IsoMessageResponse isoMessageResponse) {
        InvalidResponseHandler invalidResponseHandler = new NepsInvalidResponseHandler();
        return invalidResponseHandler.isInvalidResponse(isoMessageResponse);
    }


    @Override
    protected NepsMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context) {
        return new PinChangeRequestSender(context);
    }

    @Override
    protected void processOnlineResult(IsoMessageResponse isoMessageResponse) {

    }

    @Override
    protected void updateTransactionLog(boolean transactionApproved, TransactionRequest transactionRequest, TransactionRepository transactionRepository, IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse) {

    }

    @Override
    protected void updateTransactionIdsAndNotifyTransactionStatus(boolean transactionApproved, IsoMessageResponse isoMessageResponse, global.citytech.finpos.processor.neps.transaction.TransactionRequest request, ReadCardResponse readCardResponse) {

    }

    @Override
    protected TransactionResponse prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository, boolean shouldPrint) {
        return PinChangeResponse.Builder.newInstance()
                .withStan(stan)
                .withApproved(transactionApproved)
                .withMessage(transactionApproved ? "Your Pin has been updated." : this.retrieveMessage(isoMessageResponse, transactionApproved, request.getApplicationRepository()
                ))
                .withDebugRequestMessage(isoMessageResponse.getDebugRequestString())
                .withDebugResponseMessage(isoMessageResponse.getDebugResponseString())
                       .build();
    }

    @Override
    protected String getProcessingCode() {
        return null;
    }

    @Override
    protected TransactionResponse prepareErrorResponse(String responseMessage, boolean shouldPrint) {
        return PinChangeResponse.Builder
                .newInstance()
                .withStan(stan)
                .withMessage(responseMessage)
                .build();
    }

    @Override
    protected TransactionResponse promptToPerformIccTransaction() {
        return null;
    }
}
