package global.citytech.finpos.merchant.presentation.model

/**
 * Created by Rishav Chudal on 11/26/20.
 */
data class CardReadError (
    val title: String,
    val errorMessage: String,
    val fromBroadcast: Boolean
)