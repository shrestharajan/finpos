package global.citytech.finpos.merchant.presentation.tipadjustment

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.common.extensions.getBundle
import global.citytech.common.extensions.hideKeyboard
import global.citytech.easydroid.core.extension.showToast
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityBaseTransactionBinding
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.TransactionItem
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionActivity
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finpos.merchant.utils.formatExpiryDate
import global.citytech.finpos.merchant.utils.maskCardNumber
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.utility.HelperUtils
import kotlinx.android.synthetic.main.activity_base_transaction.*
import kotlinx.android.synthetic.main.layout_auth_completion.*
import kotlinx.android.synthetic.main.layout_tip_adjustment.*
import kotlinx.android.synthetic.main.layout_void.*
import java.math.BigDecimal

/**
 * Created by Unique Shakya on 5/5/2021.
 */
class TipsAdjustmentActivity :
    BaseTransactionActivity<ActivityBaseTransactionBinding, TipAdjustmentViewModel>() {

    private var tipAmount = BigDecimal.ZERO
    private var transactionFound = false

    companion object{
        const val INVOICE = "invoice_number"
        const val FROM_TRANSACTION_DETAIL = "from_transaction_detail"
    }

    lateinit var textWatcher: TextWatcher

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadTipsLayout()
        initViews()
        initBaseViews()
        initObservers()
        getBundle()?.let {
            fromTransactionDetail = getBundle()!!.getBoolean(FROM_TRANSACTION_DETAIL, false)
            val invoiceNumber :String? = getBundle()?.getString(INVOICE)
            invoiceNumber?.let {
                et_tip_invoice.setText(invoiceNumber)
                btn_tips_confirm.performClick()
            }
        }
    }

    private fun loadTipsLayout() {
        layout_tips.visibility = View.VISIBLE
    }

    private fun hideTipsLayout() {
        layout_tips.visibility = View.GONE
    }

    private fun initObservers() {
        this.initBaseLiveDataObservers()

        this.getViewModel().configurationItem.observe(this, Observer {
            initTipAdjustment(it)
        })

        this.getViewModel().invalidInvoiceNumber.observe(this, Observer {
            if (it)
                showToast(getString(R.string.error_msg_invalid_invoice_number), Toast.LENGTH_SHORT)
        })

        this.getViewModel().transactionFound.observe(this, Observer {
            if (!it)
                showToast(getString(R.string.error_msg_transaction_not_found), Toast.LENGTH_SHORT)
        })

        this.getViewModel().tipAmountLimit.observe(this, Observer {
            startAmountActivityWithLimit(it)
        })

        this.getViewModel().transactionItem.observe(this, Observer {
            btn_tips_confirm.text = getString(R.string.action_confirm)
            this.transactionFound = true
            hideKeyboard(et_tip_invoice)
            showTransactionItems(it)
        })

        observeTransactionValidationLiveData()
    }

    private fun observeTransactionValidationLiveData() {
        this.getViewModel().transactionAlreadyVoided.observe(
            this,
            Observer {
                if (it) {
                    showToast(
                        getString(R.string.msg_transaction_already_voided),
                        Toast.LENGTH_SHORT
                    )
                }
            })

        getViewModel().tipAlreadyAdjusted.observe(this,
            Observer {
                if (it) {
                    showToast(
                        getString(R.string.msg_tip_already_adjusted),
                        Toast.LENGTH_SHORT
                    )
                }
            })
    }

    private fun showTransactionItems(it: TransactionItem) {
        val txnType = Jsons.fromJsonToObj(it.transactionType, TransactionType::class.java)
        tv_tip_txn_type.text = txnType.displayName
        tv_tip_card_number.text = it.pan!!.maskCardNumber()
        tv_tip_expiry.text = it.expiryDate!!.formatExpiryDate()
        tv_tip_amount.text = it.currencyName.plus(" ").plus(HelperUtils.fromIsoAmount(it.amount))
        ll_tip_txn_detail.visibility = View.VISIBLE
        btn_tips_confirm.text = resources.getString(R.string.action_confirm)
    }

    private fun initTipAdjustment(it: ConfigurationItem?) {
        this.getViewModel().performTipAdjustment(it!!, tipAmount, getTransactionType())
    }

    private fun initViews() {
        iv_tips_back.setOnClickListener {
            hideKeyboard(et_tip_invoice)
            onBackPressed()
        }

        btn_tips_cancel.setOnClickListener {
            hideKeyboard(et_tip_invoice)
            returnToDashboard()
        }

        textWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                ll_tip_txn_detail.visibility = View.GONE
                btn_tips_confirm.text = resources.getString(R.string.action_next)
            }

            override fun afterTextChanged(s: Editable?) {

            }
        }

        et_tip_invoice.addTextChangedListener(textWatcher)

        btn_tips_confirm.setOnClickListener {
            if (ll_tip_txn_detail.visibility == View.VISIBLE) {
                hideTipsLayout()
                this.getViewModel().retrieveTipAmountLimit()
            } else {
                this.showProgressScreen(getString(R.string.msg_finding_transaction))
                this.getViewModel().validateInvoiceNumber(et_tip_invoice.text.toString())
            }
        }
    }

    override fun getTransactionType(): TransactionType = TransactionType.TIP_ADJUSTMENT

    override fun getTransactionViewModel(): BaseTransactionViewModel = getViewModel()

    override fun onAmountActivityResult(data: Intent?) {
        data?.let {
            val amountInString = data.getStringExtra(getString(R.string.intent_confirmed_amount))
            tipAmount = amountInString!!.toBigDecimal()
            this.getViewModel().getConfigurationItem()
        }
    }

    override fun onManualActivityResult(data: Intent?) {
        // not required
    }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayout(): Int = R.layout.activity_base_transaction

    override fun getViewModel(): TipAdjustmentViewModel =
        ViewModelProviders.of(this)[TipAdjustmentViewModel::class.java]

    override fun onPositiveButtonClick() {
        this.showProgressScreen("Printing Customer Copy...")
        this.getViewModel().printCustomerCopy()
    }

    override fun onNegativeButtonClick() = this.returnToDashboard()

    override fun onManualButtonClicked() {
        // not required
    }

    override fun onCancelButtonClicked() {
        // not required
    }

    override fun onDestroy() {
        super.onDestroy()
        et_tip_invoice.removeTextChangedListener(textWatcher)
    }

}