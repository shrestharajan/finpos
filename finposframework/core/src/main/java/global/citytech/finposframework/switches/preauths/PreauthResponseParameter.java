package global.citytech.finposframework.switches.preauths;

import global.citytech.finposframework.switches.ResponseParameter;

public interface PreauthResponseParameter extends ResponseParameter {
}
