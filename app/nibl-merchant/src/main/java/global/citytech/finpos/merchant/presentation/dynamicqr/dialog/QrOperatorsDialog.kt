package global.citytech.finpos.merchant.presentation.dynamicqr.dialog

import android.app.Activity
import android.widget.Button
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.model.qroperator.QrOperatorItem

/**
 * @author sachin
 */
class QrOperatorsDialog constructor(
    activity: Activity,
    listener: Listener,
    qrOperatorItems: List<QrOperatorItem>
) {

    private var bottomSheetDialog = BottomSheetDialog(activity)
    private val adapterListener = object : Listener {
        override fun onSelectItem(qrOperatorItem: QrOperatorItem) {
            bottomSheetDialog.dismiss()
            listener.onSelectItem(qrOperatorItem)
        }

        override fun onDismiss() {
            bottomSheetDialog.dismiss()
            listener.onDismiss()
        }
    }

    init {
        val view = activity.layoutInflater.inflate(R.layout.dialog_qr_operators, null)
        val btnClose = view.findViewById<Button>(R.id.idBtnDismiss)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.setHasFixedSize(true)
        val adapter = QrOperatorOptionAdapter(qrOperatorItems, adapterListener)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                DividerItemDecoration.VERTICAL
            )
        )
        btnClose.setOnClickListener {
            bottomSheetDialog.dismiss()
            listener.onDismiss()
        }
        bottomSheetDialog.setCancelable(true)
        bottomSheetDialog.setContentView(view)
    }

    fun show() {
        bottomSheetDialog.show()
    }

    interface Listener {
        fun onSelectItem(qrOperatorItem: QrOperatorItem)
        fun onDismiss()
    }

}