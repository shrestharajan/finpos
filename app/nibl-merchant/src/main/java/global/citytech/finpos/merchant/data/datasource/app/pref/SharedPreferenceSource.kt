package global.citytech.finpos.merchant.data.datasource.app.pref

/**
 * Created by Unique Shakya on 9/1/2020.
 */

interface SharedPreferenceSource {
    fun saveSettlementActivePref(value: Boolean)
    fun getSettlementActivePref(defaultValue: Boolean): Boolean
    fun saveTerminalConfigPref(value: Boolean)
    fun getTerminalConfigPref(defaultValue: Boolean): Boolean
    fun saveDebugModePref(value: Boolean)
    fun saveSoundModePref(value: Boolean)
    fun getDebugModePref(defaultValue: Boolean): Boolean
    fun getSoundModePref(defaultValue: Boolean): Boolean
    fun saveCardConfirmationRequiredPref(value: Boolean)
    fun getCardConfirmationRequiredPref(defaultValue: Boolean): Boolean
    fun saveVatRefundSupportedPref(value: Boolean)
    fun getVatRefundSupportedPref(defaultValue: Boolean): Boolean
    fun saveShouldShufflePinPadPref(value: Boolean)
    fun getShouldShufflePinPadPref(defaultValue: Boolean): Boolean
    fun clearAllPreferences()
    fun setCardMonitoringPreference(cardMonitoring: Boolean)
    fun getCardMonitoringPreference(defaultValue: Boolean): Boolean
    fun setForceSettlementPreference(forceSettlement: Boolean)
    fun getForceSettlementPreference(defaultValue: Boolean): Boolean
    fun savePosMode(posMode: String)
    fun getPosMode(defaultPosMode: String): String
    fun saveTipLimit(value: Int)
    fun saveNumpadLayoutType(numpadLayoutType: String)
    fun getNumpadLayoutType(defaultValue: String): String
    fun retrieveTipLimit(): Int
    fun getSettlementTime(defaultValue: String): String
    fun storeSettlementTime(settlementTime: String)
    fun getLastSuccessSettlementTimeInMillis(defaultValue: Long): Long
    fun updateLastSuccessSettlementTimeInMillis(settledTime: Long)
    fun saveMerchantCredential(merchantCredential: String)
    fun getMerchantCredential(defaultValue: String): String
    fun saveBanners(banners: String)
    fun getBanners(defaultValue: String): String
    fun saveTerminalSetting(terminalSettingResponseJson: String)
    fun savePaymentInitiatiorItems(paymentListItems: String)
    fun updateShowQrDisclaimerSetting(showDisclaimer: Boolean)
    fun getShowQrDisclaimerSetting(): Boolean
    fun getTerminalSetting(): String
    fun getPaymentInitiatorItems(): String
    fun isDisclaimerShown(): Boolean
    fun saveDisclaimerShownStatusPref(value: Boolean)
    fun saveAppDownloadExtra(value: String)
    fun getAppDownloadExtra(): String
    fun getStoredAppVersion(): Int
    fun saveAppVersion(version: Int)
    fun isSwitchConfigActive(): Boolean
    fun setSwitchConfigActive(active: Boolean)
    fun isBroadcastAcknowledgeActive(): Boolean
    fun setBroadcastAcknowledgeActive(active: Boolean)
    fun isAppResetActive(defaultValue: Boolean): Boolean
    fun setAppResetActive(value: Boolean)
    fun setAmountLengthLimit(value: Int)
    fun getAmountLengthLimit():Int
    fun getFonePayLoginState(defaultValue: Boolean): Boolean
    fun saveFonePayLoginState(value: Boolean)
    fun getAidResponse(defaultValue: String): String
    fun saveAidResponse(value: String)
    fun saveGreenPinStatus(value:Boolean)
    fun getGreenPinStatus(): Boolean
}