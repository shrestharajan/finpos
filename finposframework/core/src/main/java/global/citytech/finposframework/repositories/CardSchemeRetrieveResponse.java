package global.citytech.finposframework.repositories;

public class CardSchemeRetrieveResponse {

    private String data;

    public CardSchemeRetrieveResponse(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }
}