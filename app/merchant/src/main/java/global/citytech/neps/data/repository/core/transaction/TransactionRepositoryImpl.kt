package global.citytech.neps.data.repository.core.transaction

import global.citytech.neps.data.datasource.core.transaction.TransactionDataSource
import global.citytech.neps.data.datasource.device.DeviceDataSource
import global.citytech.neps.domain.repository.core.transaction.TransactionRepository
import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.neps.presentation.model.transaction.PurchaseRequestItem
import global.citytech.neps.presentation.model.transaction.PurchaseResponseItem
import global.citytech.posswitchintregator.neps.usecases.purchase.PurchaseResponseModel
import io.reactivex.Observable

class TransactionRepositoryImpl(val transactionDataSource: TransactionDataSource) :
    TransactionRepository {

    override fun purchase(
        configurationItem: ConfigurationItem,
        purchaseRequestItem: PurchaseRequestItem
    ): Observable<PurchaseResponseModel> {
        return transactionDataSource.purchase(configurationItem, purchaseRequestItem)
    }

    override fun cleanUp() {
        transactionDataSource.cleanUp()
    }
}