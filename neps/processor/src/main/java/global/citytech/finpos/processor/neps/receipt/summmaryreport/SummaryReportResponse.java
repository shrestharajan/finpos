package global.citytech.finpos.processor.neps.receipt.summmaryreport;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.summaryreport.SummaryReportResponseParameter;

/**
 * Created by Saurav Ghimire on 3/30/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public class SummaryReportResponse implements UseCase.Response, SummaryReportResponseParameter {

    private Result result;
    private String message;

    public SummaryReportResponse(Result result, String message) {
        this.result = result;
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }

}
