package global.citytech.finpos.processor.nibl.transaction.autoreversal;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.autoreversal.AutoReversalRequestParameter;

/**
 * Created by Unique Shakya on 6/22/2021.
 */
public class AutoReversalRequestModel implements AutoReversalRequestParameter, UseCase.Request {

    private TransactionRepository transactionRepository;
    private PrinterService printerService;
    private ApplicationRepository applicationRepository;

    public PrinterService getPrinterService() {
        return printerService;
    }

    public ApplicationRepository getApplicationRepository() {
        return applicationRepository;
    }

    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }


    public static final class Builder {
        private TransactionRepository transactionRepository;
        private PrinterService printerService;
        private ApplicationRepository applicationRepository;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder transactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public Builder printerService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public Builder applicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public AutoReversalRequestModel build() {
            AutoReversalRequestModel autoReversalRequestModel = new AutoReversalRequestModel();
            autoReversalRequestModel.applicationRepository = this.applicationRepository;
            autoReversalRequestModel.transactionRepository = this.transactionRepository;
            autoReversalRequestModel.printerService = this.printerService;
            return autoReversalRequestModel;
        }
    }
}
