package global.citytech.finpos.device.qualcomm.io.cards.handler

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.pin.OnPinInputListener
import com.ftpos.library.smartpos.pin.PinSeting
import com.google.android.material.button.MaterialButton
import global.citytech.common.extensions.isNullOrEmptyOrBlank
import global.citytech.finpos.device.qualcomm.R
import global.citytech.finpos.device.qualcomm.io.cards.transaction.FeitianSingleton
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.ERROR_CODE_OFFLINE_PIN_ENTRY_ERROR_6983
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.ERROR_CODE_OFFLINE_PIN_ENTRY_LIMIT_EXCEED
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.ERROR_CODE_PIN_BYPASS
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.ERROR_CODE_PIN_ENTRY_LAST
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.PIN_PAD_TLV_VALUE_ENTRY_EXCEED_LIMIT
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.PIN_PAD_TLV_VALUE_ERROR_6983
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.PIN_PAD_TLV_VALUE_PIN_BYPASS
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.PIN_PAD_TLV_VALUE_PIN_NORMAL
import global.citytech.finpos.device.qualcomm.utils.FeitianCustomEMVTags.CUSTOM_TAG_PINPAD_RESULT
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finpos.device.qualcomm.utils.FeitianPinEntryResult
import global.citytech.finpos.device.qualcomm.utils.FeitianUtils
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.common.Result.FAILURE
import global.citytech.finposframework.hardware.common.Result.PIN_BYPASSED
import global.citytech.finposframework.hardware.io.cards.PinBlock
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_PINPAD_FAILURE
import global.citytech.finposframework.hardware.utility.DeviceBytesUtil
import global.citytech.finposframework.hardware.utility.DeviceUtils
import global.citytech.finposframework.utility.StringUtils
import java.lang.String.format
import java.lang.ref.WeakReference
import java.util.concurrent.CountDownLatch

/**
 * Created by Rishav Chudal on 4/8/21.
 */
class FeitianPinPadHandler constructor(
    private val context: Context,
    private val pinEntryType: Int,
    private val pinRequest: PinRequest
): FeitianLogger.Listener {

    private val contextWeakReference = WeakReference(context)
    private val emv = Emv.getInstance(context.applicationContext)
    private val logger = FeitianLogger(this)
    private val pinSetting = PinSeting(this.contextWeakReference.get(), this.emv)
    private val countDownLatch = CountDownLatch(1)
    private var pinLength :Int = 0
    private lateinit var buttonDigits: Array<MaterialButton>
    private lateinit var dialogView: View
    private lateinit var tvScheme: AppCompatTextView
    private lateinit var tvCardNumber: AppCompatTextView
    private lateinit var tvExpiryDate: AppCompatTextView
    private lateinit var tvCardHolder: AppCompatTextView
    private lateinit var textViewAmount: AppCompatTextView
    private lateinit var textViewTitle: AppCompatTextView
    private lateinit var textViewPinPadMessage: TextView
    private lateinit var imageButtonCancel: MaterialButton
    private lateinit var imageButtonClear: AppCompatImageView
    private lateinit var imageButtonConfirm: MaterialButton
    private lateinit var pinPadRootLayout: LinearLayout
    private lateinit var pinEntryResult: FeitianPinEntryResult
    private lateinit var backButton: ImageView


    fun showPinPadAndCalculatePinBlock(): FeitianPinEntryResult {
        this.logger.logWithClassName("showPinPadAndCalculatePinBlock...")
        this.preparePinPadResult(FAILURE, "")
        when (pinEntryType.toByte()) {
            Emv.EMV_CVMFLAG_OLPIN_SIGN -> this.onInputPinEntryTypeOnline(getPinEntryMessage())
            Emv.EMV_CVMFLAG_ENOFFLINE_PIN_SIGN -> this.onInputPinEntryTypeEncryptedOffline(
                getPinEntryMessage()
            )
            Emv.EMV_CVMFLAG_PLOFFLINE_PIN_SIGN -> this.onInputPinEntryTypePlainTextOffline(
                getPinEntryMessage()
            )
            else -> onPinEntryFailure()
        }

        return this.pinEntryResult
    }

    private fun onInputPinEntryTypeOnline(message: String) {
        this.logger.logWithClassName("Pin Entry is Online Pin..."+message)
        showPinPad(message)
    }

    private fun onInputPinEntryTypeEncryptedOffline(message: String) {
        this.logger.logWithClassName("Pin Entry is Encrypted Offline Pin...")
        showPinPad(message)
    }

    private fun onInputPinEntryTypePlainTextOffline(message: String) {
        this.logger.logWithClassName("Pin Entry is Plain Text Offline Pin...")
        showPinPad(message)
    }

    private fun onPinEntryFailure() {
        this.preparePinPadResult(FAILURE, MSG_PINPAD_FAILURE)
    }

    private fun showPinPad(pinPadMessage: String) {
        val activity = contextWeakReference.get() as Activity? ?: return
        activity.runOnUiThread {
            try {
                this.initializeUIAndSdkSettings(pinPadMessage)
                this.prepareDialogWindowAndDisplay()
            } catch (ex: Exception) {
                ex.printStackTrace()
                countDownLatch.countDown()
            }
        }
        try {
            countDownLatch.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    private fun initializeUIAndSdkSettings(pinPadMessage: String) {
        this.initializeDialogAndDialogView()
        this.initializeNumericButtons()
        this.initializeOtherWidgets(pinPadMessage)
        this.initializePinSetting()
    }

    private fun initializeDialogAndDialogView() {
        val pinPadDialogWeakReference = WeakReference(
            Dialog(contextWeakReference.get()!!, R.style.BaseDialog)
        )
        FeitianSingleton.dialogWeakReference = pinPadDialogWeakReference
        dialogView = LayoutInflater
            .from(contextWeakReference.get())
            .inflate(
                R.layout.layout_feitian_pinpad,
                null
            )
    }

    private fun initializeNumericButtons() {
        buttonDigits = Array(10) { MaterialButton(contextWeakReference.get()!!) }
        buttonDigits[0] = dialogView.findViewById(R.id.button0)
        buttonDigits[1] = dialogView.findViewById(R.id.button1)
        buttonDigits[2] = dialogView.findViewById(R.id.button2)
        buttonDigits[3] = dialogView.findViewById(R.id.button3)
        buttonDigits[4] = dialogView.findViewById(R.id.button4)
        buttonDigits[5] = dialogView.findViewById(R.id.button5)
        buttonDigits[6] = dialogView.findViewById(R.id.button6)
        buttonDigits[7] = dialogView.findViewById(R.id.button7)
        buttonDigits[8] = dialogView.findViewById(R.id.button8)
        buttonDigits[9] = dialogView.findViewById(R.id.button9)
    }

    private fun initializeOtherWidgets(pinPadMessage: String) {
        this.initAmountUI()
        this.initMsgUI(pinPadMessage)
        imageButtonCancel = dialogView.findViewById(R.id.imgbtn_cancel)
        imageButtonClear = dialogView.findViewById(R.id.imgbtn_clean)
        imageButtonConfirm = dialogView.findViewById(R.id.imgbtn_confirm)
        pinPadRootLayout = dialogView.findViewById(R.id.layoutPinpadRoot)
        backButton= dialogView.findViewById(R.id.iv_back)

        if(pinRequest.showBackButton){
            backButton.visibility= View.VISIBLE
        }else{
            backButton.visibility= View.GONE
        }
        backButton.setOnClickListener {
            FeitianSingleton.dialogWeakReference?.get()?.dismiss()
        }
    }

    private fun initAmountUI() {
        textViewAmount = dialogView.findViewById(R.id.tv_amount)
        tvScheme = dialogView.findViewById(R.id.tv_scheme)
        tvCardNumber = dialogView.findViewById(R.id.tv_card_number)
        tvCardHolder = dialogView.findViewById(R.id.tv_card_holder_name)
        tvExpiryDate = dialogView.findViewById(R.id.tv_expiry_date)
        textViewTitle = dialogView.findViewById(R.id.tv_txn_type)
        textViewTitle.text = pinRequest.dynamicTitle
        textViewAmount.text = pinRequest.amountMessage
        tvScheme.text = retrieveCardScheme(pinRequest.cardSummary?.cardSchemeLabel)
        tvCardNumber.text = retrieveCardNumber(pinRequest.cardSummary?.primaryAccountNumber)
        tvCardHolder.text = retrieveCardHolderName(pinRequest.cardSummary?.cardHolderName)
        tvExpiryDate.text = retrieveExpiryDate(pinRequest.cardSummary?.expiryDate)

    }


    private fun initMsgUI(pinPadMessage: String) {
        textViewPinPadMessage = dialogView.findViewById(R.id.tv_display)
        val message = pinRequest.pinPadMessage
        if (message.isNullOrEmptyOrBlank()) {
            textViewPinPadMessage.text = pinPadMessage
        } else {
            textViewPinPadMessage.text = message
        }
    }

    private fun initializePinSetting() {
        pinSetting.pan = pinRequest.primaryAccountNumber
        pinSetting.setButtonCancel(imageButtonCancel)
        pinSetting.setButtonNum(buttonDigits)
        pinSetting.setButtonDel(imageButtonClear)
        pinSetting.setButtonOK(imageButtonConfirm)
        pinSetting.setButtonKeyboard(pinPadRootLayout)
        pinSetting.onlinePinBlockFormat = FeitianUtils.getDevicePinBlockFormat(pinRequest.pinBlockFormat)
        pinSetting.onlinePinKeyIndex = pinRequest.pinKeyIndex
        pinSetting.onlinePinKeyType = FeitianUtils.getDeviceKeyType(pinRequest.keyType)
        pinSetting.timeout = DeviceUtils.getTimeOutOrDefault(pinRequest.pinPadTimeout!!)
        pinSetting.isRandomkeyboard = !pinRequest.pinPadFixedLayout
        pinSetting.minPinLen = DeviceUtils.getMinPinLengthOrDefault(pinRequest.pinMinLength!!)
        pinSetting.maxPinLen = DeviceUtils.getMaxPinLengthOrDefault(pinRequest.pinMaxLength!!)
        pinSetting.onlinePinByPass = false
    }

    private fun prepareDialogWindowAndDisplay() {
        val dialogWeakReference = FeitianSingleton.dialogWeakReference
        if (dialogWeakReference == null) {
            countDownLatch.countDown()
        }
        var dialog = dialogWeakReference!!.get()

        if(dialog == null){
            val pinPadDialogWeakReference = WeakReference(
                Dialog(contextWeakReference.get()!!, R.style.BaseDialog)
            )
            FeitianSingleton.dialogWeakReference = pinPadDialogWeakReference
            dialog = pinPadDialogWeakReference.get()
        }

        if (dialog == null) {
            countDownLatch.countDown()
        }
        val dialogWindow = dialog!!.window
        if (dialogWindow == null) {
            countDownLatch.countDown()
        }
        dialogWindow!!.setGravity(Gravity.BOTTOM)
        dialogWindow.setBackgroundDrawable(null)
        val layoutParams = dialogWindow.attributes
        layoutParams.x = 0
        layoutParams.y = 0
        dialogView.measure(0, 0)
        layoutParams.height = dialogView.measuredHeight
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        dialogWindow.attributes = layoutParams
        dialog.setContentView(dialogView)
        dialog.setOnShowListener { startSDKPinInput() }
        dialog.show()
    }

    private fun startSDKPinInput() {
        this.logger.logWithClassName("startSDKPinInput...")
        emv.StartPinInput(
            this.pinSetting,
            object : OnPinInputListener {
                override fun onDispalyPin(pinLength: Int, type: Int) {
                    logger.logWithClassName("OnPinInputListener ::: onDisplayPin...")
                    this@FeitianPinPadHandler.pinLength = pinLength
                    showPinEntryInDisplay(pinLength)
                }

                override fun onSuccess(pinBlockData: ByteArray?) {
                    logger.logWithClassName("OnPinInputListener ::: onSuccess...")
                    onPinInputSuccess(pinBlockData)
                }

                override fun onError(errCode: Int) {
                    logger.logWithClassName("OnPinInputListener ::: onError...")
                    logger.logWithClassName("OnPinInputListener ::: Error Code ::: $errCode")
                    onPinInputError(errCode)
                }

                override fun onTimeout() {
                    logger.logWithClassName("OnPinInputListener ::: onTimeout...")
                    onPinInputTimeOut()
                }

                override fun onCancel() {
                    logger.logWithClassName("OnPinInputListener ::: onCancel...")
                    onPinInputCancel()
                }

                override fun onSetDigits(anyObject: Any, character: Char) {
                    logger.logWithClassName("OnPinInputListener ::: onSetDigits")
                    logger.logWithClassName(
                        "OnPinInputListener ::: onSetDigits ::: character ::: ".plus(character)
                    )
                    setDigitsOnDisplay(anyObject, character)
                }
            }
        )
    }



    private fun onPinInputBack() {
        FeitianSingleton.dialogWeakReference?.get()?.dismiss()
        preparePinPadResult(Result.BACK_PRESSED, "")
        countDownLatch.countDown()
    }

    private fun showPinEntryInDisplay(pinLength: Int) {
        (this.contextWeakReference.get() as Activity?)!!.runOnUiThread {
            val chars = CharArray(pinLength)
            for (i in 0 until pinLength) {
                chars[i] = 'X'
            }
            this.textViewPinPadMessage.text = String(chars)
        }
    }

    private fun onPinInputSuccess(pinBlockData: ByteArray?) {
        if (pinBlockData != null) {
            this.logger.logWithClassName("Pin need to be verified online...")
            val pinBlockHex: String = DeviceBytesUtil.bytes2HexString(pinBlockData)
            this.logger.debugWithClassName("PinBlock ::: $pinBlockHex")
            FeitianSingleton.pinBlock = PinBlock(pinBlockHex, false)
        } else {
            this.logger.logWithClassName("Pin was offline verified...")
            FeitianSingleton.pinBlock = PinBlock("", true)
        }
        preparePinPadResult(
            Result.SUCCESS,
            preparePinPadTlvData(PIN_PAD_TLV_VALUE_PIN_NORMAL)!!
        )
        countDownLatch.countDown()
        FeitianSingleton.dialogWeakReference!!.get()!!.dismiss()
    }

    private fun onPinInputError(errCode: Int) {
        when (errCode) {
            ERROR_CODE_PIN_ENTRY_LAST -> this.onPinEntryLastTime()
            ERROR_CODE_OFFLINE_PIN_ENTRY_LIMIT_EXCEED -> this.onOfflinePinEntryLimitExceed()
            ERROR_CODE_OFFLINE_PIN_ENTRY_ERROR_6983 -> this.onPinEntryError6983()
            ERROR_CODE_PIN_BYPASS -> this.onPinEntryByPass()
            else -> this.onDefaultPinInputError(errCode)
        }
    }

    private fun onPinInputTimeOut() {
        FeitianSingleton.dialogWeakReference!!.get()!!.dismiss()
        preparePinPadResult(Result.TIMEOUT, "")
        countDownLatch.countDown()
    }

    private fun onPinInputCancel() {
        FeitianSingleton.dialogWeakReference!!.get()!!.dismiss()
        preparePinPadResult(Result.USER_CANCELLED, "")
        countDownLatch.countDown()
    }

    private fun setDigitsOnDisplay(anyObject: Any, c: Char) {
        (contextWeakReference.get() as Activity?)!!.runOnUiThread {
            val btn = anyObject as Button
            btn.text = c.toString()
        }
    }

    private fun onPinEntryLastTime() {
        this.logger.logWithClassName("Input Pin Entry Last Time...")
        FeitianSingleton.dialogWeakReference!!.get()!!.dismiss()
        onInputPinEntryTypeEncryptedOffline(getPinRetryMessage())
    }

    private fun onOfflinePinEntryLimitExceed() {
        this.logger.logWithClassName("Input Offline Pin Entry Limit Exceed...")
        FeitianSingleton.dialogWeakReference!!.get()!!.dismiss()
        preparePinPadResult(
            FAILURE,
            preparePinPadTlvData(PIN_PAD_TLV_VALUE_ENTRY_EXCEED_LIMIT)!!
        )
        countDownLatch.countDown()
    }

    private fun onPinEntryError6983() {
        this.logger.logWithClassName("Input Offline Pin Entry Error 6983...")
        FeitianSingleton.dialogWeakReference!!.get()!!.dismiss()
        preparePinPadResult(
            FAILURE,
            preparePinPadTlvData(PIN_PAD_TLV_VALUE_ERROR_6983)!!
        )
        countDownLatch.countDown()
    }

    private fun onPinEntryByPass() {
        this.logger.logWithClassName("Input Pin Entry ByPass...")
        FeitianSingleton.dialogWeakReference!!.get()!!.dismiss()
        preparePinPadResult(
            PIN_BYPASSED,
            preparePinPadTlvData(PIN_PAD_TLV_VALUE_PIN_BYPASS)!!
        )
        countDownLatch.countDown()
    }

    private fun onDefaultPinInputError(errCode: Int) {
        this.logger.logWithClassName("Input Pin Entry Default Error...")
        FeitianSingleton.dialogWeakReference!!.get()!!.dismiss()
        if (errCode and ERROR_CODE_OFFLINE_PIN_ENTRY_LIMIT_EXCEED
            == ERROR_CODE_OFFLINE_PIN_ENTRY_LIMIT_EXCEED
        ) {
            onInputPinEntryTypeEncryptedOffline(getPinRetryMessage())
        } else {
            preparePinPadResult(
                FAILURE,
                preparePinPadTlvData(errCode)!!
            )
            countDownLatch.countDown()
        }
    }

    private fun getPinEntryMessage(): String {
        return context.applicationContext.getString(R.string.text_customer_pin_entry)
    }

    private fun getPinRetryMessage(): String {
        return context.applicationContext.getString(R.string.text_customer_pin_retry)
    }

    private fun preparePinPadResult(result: Result, tlvData: String) {
        this.logger.logWithClassName("Preparing PinPadResult...")
        this.pinEntryResult = FeitianPinEntryResult(result, tlvData)
    }

    private fun preparePinPadTlvData(data: Any): String? {
        return format(
            CUSTOM_TAG_PINPAD_RESULT.tagInHex()
                .plus(CUSTOM_TAG_PINPAD_RESULT.lengthInHex())
                .plus("%02x"),
            data
        )
    }

    private fun retrieveCardNumber(cardNumber: String?): String {
        if (StringUtils.isEmpty(cardNumber)) {
            return ""
        }
        return StringUtils.arrangeEncodedPanInGivenGroupSize(cardNumber, 4)
    }

    private fun retrieveExpiryDate(expiryDate: String?): String {
        return if (!expiryDate.isNullOrEmptyOrBlank()) {
            expiryDate!!.substring(2, 4).plus("/").plus(expiryDate.substring(0, 2))
        } else {
            ""
        }
    }

    private fun retrieveCardScheme(cardScheme: String?): String {
        return if (!cardScheme.isNullOrEmptyOrBlank()) {
            cardScheme!!
        } else {
            ""
        }
    }

    private fun retrieveCardHolderName(cardHolderName: String?): String {
        return if (!cardHolderName.isNullOrEmptyOrBlank()) {
            if (cardHolderName!!.contains("/")) {
                val names = cardHolderName.split("/")
                var fullName = ""
                for (name in names) {
                    fullName = fullName.plus(name).plus(" ")
                }
                fullName
            } else {
                cardHolderName
            }
        } else {
            ""
        }

    }

    override fun getClassName() = FeitianPinPadHandler::class.java.simpleName
}