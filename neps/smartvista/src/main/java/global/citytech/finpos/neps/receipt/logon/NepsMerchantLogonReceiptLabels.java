package global.citytech.finpos.neps.receipt.logon;

public class NepsMerchantLogonReceiptLabels {

    private NepsMerchantLogonReceiptLabels() {
    }

    public static final String DIVIDER = "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --";
    public static final String SUCCESS_MESSAGE = "LOGON SUCCESSFUL";
    public static final String FAILURE_MESSAGE = "LOGON FAILURE";
}
