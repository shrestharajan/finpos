package global.citytech.common.data

data class Response(
    val code : String,
    val message : String,
    val data : Any?
)