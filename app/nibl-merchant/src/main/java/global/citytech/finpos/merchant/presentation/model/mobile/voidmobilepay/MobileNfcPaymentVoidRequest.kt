package global.citytech.finpos.merchant.presentation.model.mobile.voidmobilepay

import com.google.gson.annotations.SerializedName


data class MobileNfcPaymentVoidRequest(
    @SerializedName("merchant_pan")
    val merchantPan: String,
    @SerializedName("terminal_id")
    val terminalId: String,
    @SerializedName("txn_number")
    val transactionNumber: String,
    val amount: String
)