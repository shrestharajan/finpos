package global.citytech.payment.sdk.api;

import android.content.Context;

import global.citytech.payment.sdk.core.PaymentException;
import global.citytech.payment.sdk.core.PosPaymentImpl;

/**
 * Created by Rishav Chudal on 6/16/21.
 */
public final class PosPaymentApi implements PaymentService {
    private final PosPaymentImpl posPaymentImpl;

    private PosPaymentApi(Context uiContext, PosPaymentConfiguration posPaymentConfiguration) {
        posPaymentImpl = new PosPaymentImpl(uiContext, posPaymentConfiguration);
        posPaymentImpl.initialize();
    }

    public static PosPaymentApi getInstance(
            Context uiContext,
            PosPaymentConfiguration posPaymentConfiguration
    ) throws PaymentException {
        return new PosPaymentApi(uiContext, posPaymentConfiguration);
    }

    @Override
    public PaymentResponse doPurchase(PurchasePaymentRequest purchasePaymentRequest) {
        return posPaymentImpl.doPurchaseOnValidPaymentConfiguration(purchasePaymentRequest);
    }

    @Override
    public PaymentResponse performVoid(VoidPaymentRequest voidPaymentRequest) {
        return posPaymentImpl.performVoidOnValidPaymentConfiguration(voidPaymentRequest);
    }

    @Override
    public void doPaymentCancellation(PaymentCancellationRequest paymentCancellationRequest) {
        posPaymentImpl.cancelPaymentOnValidPaymentConfiguration(paymentCancellationRequest);
    }
}
