package global.citytech.finpos.merchant.framework.datasource.core.configuration

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.configuration.ConfigurationDataSource
import global.citytech.finpos.merchant.domain.model.logon.LogOnRequestEntity
import global.citytech.finpos.merchant.domain.model.logon.LogonResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.DeviceConfigurationSourceImpl
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.data.mapToUiModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finposframework.switches.ProcessorManager
import io.reactivex.Observable

class ConfigurationDataSourceImpl :
    ConfigurationDataSource {
    override fun logOn(configurationItem: ConfigurationItem): Observable<LogonResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepo =
            TerminalRepositoryImpl(
                configurationItem
            )
        val printerService =
            PrinterSourceImpl(NiblMerchant.getActivityContext(), provideOptionsToReprint = false)
        val hardwareKeyService = DeviceConfigurationSourceImpl(NiblMerchant.INSTANCE).keyService
        val packageName = NiblMerchant.INSTANCE.packageName
        val logOnRequester =
            ProcessorManager.getInterface(terminalRepo, NotificationHandler)!!.logOnRequest
        return Observable.fromCallable {
            logOnRequester.execute(
                LogOnRequestEntity(
                    printerService,
                    hardwareKeyService,
                    packageName
                ).mapToModel()
            ).mapToUiModel()
        }
    }
}