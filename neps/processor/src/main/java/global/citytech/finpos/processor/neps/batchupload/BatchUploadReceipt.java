package global.citytech.finpos.processor.neps.batchupload;

/**
 * Created by Saurav Ghimire on 6/14/21.
 * sauravnghimire@gmail.com
 */


public class BatchUploadReceipt {

    private String batchNumber;
    private String cardNumber;
    private String cardScheme;
    private String invoiceNumber;
    private String approvalCode;
    private String transactionType;
    private String amount;
    private String transactionDate;
    private String transactionTime;
    private String responseCode;

    public String getBatchNumber() {
        return batchNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getCardScheme() {
        return cardScheme;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public String getAmount() {
        return amount;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public static final class Builder {
        private String batchNumber;
        private String cardNumber;
        private String cardScheme;
        private String invoiceNumber;
        private String approvalCode;
        private String transactionType;
        private String amount;
        private String transactionDate;
        private String transactionTime;
        private String responseCode;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder batchNumber(String batchNumber) {
            this.batchNumber = batchNumber;
            return this;
        }

        public Builder cardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
            return this;
        }

        public Builder cardScheme(String cardScheme) {
            this.cardScheme = cardScheme;
            return this;
        }

        public Builder invoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }

        public Builder approvalCode(String approvalCode) {
            this.approvalCode = approvalCode;
            return this;
        }

        public Builder transactionType(String transactionType) {
            this.transactionType = transactionType;
            return this;
        }

        public Builder amount(String amount) {
            this.amount = amount;
            return this;
        }

        public Builder transactionDate(String transactionDate) {
            this.transactionDate = transactionDate;
            return this;
        }

        public Builder transactionTime(String transactionTime) {
            this.transactionTime = transactionTime;
            return this;
        }

        public Builder responseCode(String responseCode) {
            this.responseCode = responseCode;
            return this;
        }

        public BatchUploadReceipt build() {
            BatchUploadReceipt batchUploadReceipt = new BatchUploadReceipt();
            batchUploadReceipt.batchNumber = this.batchNumber;
            batchUploadReceipt.cardNumber = this.cardNumber;
            batchUploadReceipt.cardScheme = this.cardScheme;
            batchUploadReceipt.transactionTime = this.transactionTime;
            batchUploadReceipt.approvalCode = this.approvalCode;
            batchUploadReceipt.transactionDate = this.transactionDate;
            batchUploadReceipt.transactionType = this.transactionType;
            batchUploadReceipt.responseCode = this.responseCode;
            batchUploadReceipt.amount = this.amount;
            batchUploadReceipt.invoiceNumber = this.invoiceNumber;
            return batchUploadReceipt;
        }
    }
}
