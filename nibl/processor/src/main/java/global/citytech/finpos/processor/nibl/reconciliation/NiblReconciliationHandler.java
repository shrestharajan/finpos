package global.citytech.finpos.processor.nibl.reconciliation;

import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationHandler;

/**
 * Created by Unique Shakya on 9/22/2020.
 */
public class NiblReconciliationHandler implements ReconciliationHandler {

    private ReconciliationRepository reconciliationRepository;
    private Logger logger;
    private String batchNumber;

    public NiblReconciliationHandler(String batchNumber, ReconciliationRepository reconciliationRepository) {
        this.reconciliationRepository = reconciliationRepository;
        this.batchNumber = batchNumber;
        this.logger = new Logger(NiblReconciliationHandler.class.getName());
    }

    @Override
    public NiblReconciliationTotals prepareReconciliationTotals() {
        this.logger.log("::: NIBL ::: BATCH NUMBER IN NIBL RECONCILIATION HANDLER ::: " + this.batchNumber);
        return NiblReconciliationTotals.Builder.newInstance()
                .withSalesCount(this.prepareSalesCount())
                .withSalesAmount(this.prepareSalesAmount())
                .withSalesRefundCount(this.prepareSalesRefundCount())
                .withSalesRefundAmount(this.prepareSalesRefundAmount())
                .withDebitCount(this.prepareDebitCount())
                .withDebitAmount(this.prepareDebitAmount())
                .withDebitRefundCount(this.prepareDebitRefundCount())
                .withDebitRefundAmount(this.prepareDebitRefundAmount())
                .withAuthCount(this.prepareAuthCount())
                .withAuthAmount(this.prepareAuthAmount())
                .withAuthRefundCount(this.prepareAuthRefundCount())
                .withAuthRefundAmount(this.prepareAuthRefundAmount())
                .build();
    }

    private long prepareAuthRefundAmount() {
        return 0;
    }

    private long prepareAuthRefundCount() {
        return 0;
    }

    private long prepareAuthAmount() {
        return this.reconciliationRepository.getTotalAmountByTransactionType(this.batchNumber, TransactionType.PRE_AUTH);
    }

    private long prepareAuthCount() {
        return this.reconciliationRepository.getCountByTransactionType(this.batchNumber, TransactionType.PRE_AUTH);
    }

    private long prepareDebitRefundAmount() {
        return 0;
    }

    private long prepareDebitRefundCount() {
        return 0;
    }

    private long prepareDebitAmount() {
        return 0;
    }

    private long prepareDebitCount() {
        return 0;
    }

    private long prepareSalesRefundAmount() {
        return this.reconciliationRepository.getTotalAmountByTransactionType(this.batchNumber, TransactionType.REFUND) -
                this.reconciliationRepository.getTotalAmountByOriginalTransactionType(this.batchNumber, TransactionType.VOID, TransactionType.REFUND);
    }

    private long prepareSalesRefundCount() {
        return this.reconciliationRepository.getCountByTransactionType(this.batchNumber, TransactionType.REFUND) -
                this.reconciliationRepository.getCountByOriginalTransactionType(this.batchNumber, TransactionType.VOID, TransactionType.REFUND);
    }

    private long prepareSalesAmount() {
        return this.reconciliationRepository.getTotalAmountByTransactionType(this.batchNumber, TransactionType.PURCHASE) -
                this.reconciliationRepository.getTotalAmountByOriginalTransactionType(this.batchNumber, TransactionType.VOID, TransactionType.PURCHASE);
    }

    private long prepareSalesCount() {
        return this.reconciliationRepository.getCountByTransactionType(this.batchNumber, TransactionType.PURCHASE) -
                this.reconciliationRepository.getCountByOriginalTransactionType(this.batchNumber, TransactionType.VOID, TransactionType.PURCHASE);
    }
}
