package global.citytech.finpos.processor.nibl.cashin;

import global.citytech.finpos.processor.nibl.transaction.TransactionRequestModel;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.io.sound.SoundService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;

public class CashInRequest extends TransactionRequestModel {
    public static  class Builder {
        private TransactionRepository transactionRepository;
        private ReadCardService readCardService;
        private DeviceController deviceController;
        private TransactionAuthenticator transactionAuthenticator;
        private PrinterService printerService;
        private ApplicationRepository applicationRepository;
        private global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest;
        private LedService ledService;
        private SoundService soundService;


        public static CashInRequest.Builder newInstance() {
            return new CashInRequest.Builder();
        }

        public CashInRequest.Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public CashInRequest.Builder withReadCardService(ReadCardService readCardService) {
            this.readCardService = readCardService;
            return this;
        }

        public CashInRequest.Builder withDeviceController(DeviceController deviceController) {
            this.deviceController = deviceController;
            return this;
        }

        public CashInRequest.Builder withTransactionAuthenticator(TransactionAuthenticator transactionAuthenticator) {
            this.transactionAuthenticator = transactionAuthenticator;
            return this;
        }

        public CashInRequest.Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public CashInRequest.Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public CashInRequest.Builder withTransactionRequest(global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest) {
            this.transactionRequest = transactionRequest;
            return this;
        }

        public CashInRequest.Builder withLedService(LedService ledService) {
            this.ledService = ledService;
            return this;
        }

        public CashInRequest.Builder withSoundService(SoundService soundService) {
            this.soundService = soundService;
            return this;
        }

        public CashInRequest build() {
            CashInRequest CashInRequest = new CashInRequest();
            CashInRequest.transactionAuthenticator = this.transactionAuthenticator;
            CashInRequest.transactionRepository = this.transactionRepository;
            CashInRequest.readCardService = this.readCardService;
            CashInRequest.deviceController = this.deviceController;
            CashInRequest.transactionRequest = this.transactionRequest;
            CashInRequest.printerService = this.printerService;
            CashInRequest.applicationRepository = this.applicationRepository;
            CashInRequest.ledService = this.ledService;
            CashInRequest.soundService = this.soundService;
            return CashInRequest;
        }
    }
}
