package global.citytech.finpos.processor.nibl.receipt.summaryreport;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.summaryreport.SummaryReportResponseParameter;

/**
 * Created by Unique Shakya on 1/5/2021.
 */
public class SummaryReportResponseModel implements UseCase.Response, SummaryReportResponseParameter {

    private Result result;
    private String message;

    public SummaryReportResponseModel(Result result, String message) {
        this.result = result;
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }
}
