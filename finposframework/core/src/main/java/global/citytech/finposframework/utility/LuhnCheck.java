package global.citytech.finposframework.utility;

public class LuhnCheck {

    private static LuhnCheck INSTANCE = null;

    public static LuhnCheck getInstance() {
        if (INSTANCE == null)
            INSTANCE = new LuhnCheck();
        return INSTANCE;
    }

    private LuhnCheck() {
    }

    public boolean isValidCardNumber(String cardNumber) {
        if (StringUtils.isEmpty(cardNumber))
            return false;
        int length = cardNumber.length();
        int lastDigit = Integer.parseInt(cardNumber.substring(length - 1));
        int luhnDigit = this.getCheckNumber(cardNumber.substring(0, length - 1));
        return lastDigit == luhnDigit;
    }

    int getCheckNumber(String cardNumber) {
        int totalNumber = 0;
        int lastNumber = 0;
        for (int i = cardNumber.length() - 1; i >= 0; i -= 2) {
            int tmpNumber = calculate(Integer.parseInt(String.valueOf(cardNumber.charAt(i))) * 2);
            if (i == 0) {
                totalNumber += tmpNumber;
            } else {
                totalNumber += tmpNumber + Integer.parseInt(String.valueOf(cardNumber.charAt(i - 1)));
            }

        }
        if (totalNumber >= 0 && totalNumber < 9) {
            return (10 - totalNumber);
        } else {
            String str = String.valueOf(totalNumber);
            if (Integer.parseInt(String.valueOf(str.charAt(str.length() - 1))) == 0) {
                return 0;
            } else {
                return (10 - Integer.parseInt(String.valueOf(str.charAt(str.length() - 1))));
            }
        }

    }

    int calculate(int number) {
        String str = String.valueOf(number);
        int total = 0;
        for (int i = 0; i < str.length(); i++) {
            total += Integer.parseInt(String.valueOf(str.charAt(i)));
        }
        return total;
    }
}