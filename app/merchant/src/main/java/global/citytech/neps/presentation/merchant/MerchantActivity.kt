package global.citytech.neps.presentation.merchant

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.easydroid.core.extension.showSuccessMessage
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.easydroid.core.utils.MessageConfig
import global.citytech.neps.BR
import global.citytech.neps.R
import global.citytech.neps.databinding.ActivityMerchantBinding
import global.citytech.neps.presentation.model.ConfigurationItem
import kotlinx.android.synthetic.main.activity_merchant.*

class MerchantActivity : AppBaseActivity<ActivityMerchantBinding, MerchantViewModel>() {

    lateinit var configurationItem: ConfigurationItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_merchant)
        initViews()
        initObservers()
        getViewModel().getConfigurationItem()
    }

    private fun initObservers() {

        getViewModel().isLoading.observe(this, Observer {
            if (it) {
                iv_back.isEnabled = false
                rl_progress.visibility = View.VISIBLE
            } else {
                rl_progress.visibility = View.GONE
                iv_back.isEnabled = true
            }

        })

        getViewModel().message.observe(this, Observer {
            showSuccessMessage(MessageConfig.Builder()
                .message(it)
                .positiveLabel("RETRY")
                .onPositiveClick { dialog, which ->
                    btn_log_on.performClick()
                    dialog.dismiss()
                }.negativeLabel("CANCEL")
                .onNegativeClick { dialog, which ->
                    dialog.dismiss()
                })

        })

        getViewModel().configurationItem.observe(this, Observer {
            this.configurationItem = it
        })

        getViewModel().logonResponse.observe(this, Observer {
            if (it)
                showSuccessMessage(MessageConfig.Builder()
                    .message("LOG ON SUCCESSFUL")
                    .positiveLabel("OK")
                    .onPositiveClick { dialog, which ->
                        dialog.dismiss()
                    })
        })

        getViewModel().exchangeKeysResponse.observe(this, Observer {
            if (it)
                showSuccessMessage(MessageConfig.Builder()
                    .message("KEY EXCHANGE SUCCESSFUL")
                    .positiveLabel("OK")
                    .onPositiveClick { dialog, which ->
                        dialog.dismiss()
                    })
        })

    }

    private fun initViews() {
        btn_log_on.setOnClickListener {
            configurationItem.let {
                getViewModel().logOn(it)
            }
        }

        btn_exchange_key.setOnClickListener {
            configurationItem.let {
                getViewModel().exchangeKeys(it)
            }
        }

        iv_back.setOnClickListener {
            finish()
        }
    }

    override fun getBindingVariable(): Int = BR.viewModel
    override fun getLayout(): Int = R.layout.activity_merchant
    override fun getViewModel(): MerchantViewModel =
        ViewModelProviders.of(this)[MerchantViewModel::class.java]
}
