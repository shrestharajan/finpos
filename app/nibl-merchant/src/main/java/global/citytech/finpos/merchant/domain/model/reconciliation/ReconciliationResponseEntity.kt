package global.citytech.finpos.merchant.domain.model.reconciliation

/**
 * Created by Saurav Ghimire on 4/1/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


data class ReconciliationResponseEntity(
    val debugRequestString: String? = null,
    val debutResponseString: String? = null,
    val message: String? = null,
    val isSuccess:Boolean? = false
)
