package global.citytech.finpos.processor.nibl.reconciliation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import global.citytech.finpos.nibl.iso8583.requestsender.settlement.SettlementRequest;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationTotals;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.utility.IsoMessageUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Unique Shakya on 10/8/2020.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({SettlementRequest.class, IsoMessageResponse.class, ReconciliationTotals.class,
        TerminalInfo.class, RequestContext.class, IsoMessageUtils.class})
public class NiblNiblReconciliationReceiptMakerTest {

    SettlementRequest settlementRequest;
    IsoMessageResponse isoMessageResponse;
    ReconciliationTotals reconciliationTotals;
    TerminalInfo terminalInfo;
    RequestContext requestContext;

    @Before
    public void setup() {
        requestContext = PowerMockito.mock(RequestContext.class);
        settlementRequest = PowerMockito.mock(SettlementRequest.class);
        isoMessageResponse = PowerMockito.mock(IsoMessageResponse.class);
        PowerMockito.mockStatic(IsoMessageUtils.class);
        reconciliationTotals = PowerMockito.mock(ReconciliationTotals.class);
        terminalInfo = PowerMockito.mock(TerminalInfo.class);
    }

    @Test
    public void prepareTest() {
        String batchNumber = "001";

        PowerMockito.when(IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, 39))
                .thenReturn(0);
        PowerMockito.when(IsoMessageUtils.retrieveDateTime(this.isoMessageResponse))
                .thenReturn("1535231008");

        PowerMockito.when(terminalInfo.getMerchantName()).thenReturn("CityTech");
        PowerMockito.when(terminalInfo.getMerchantAddress()).thenReturn("Kamaladi");
        PowerMockito.when(terminalInfo.getMerchantID()).thenReturn("123456789012345");
        PowerMockito.when(terminalInfo.getTerminalID()).thenReturn("99991002");

        PowerMockito.when(settlementRequest.getBatchNumber()).thenReturn(batchNumber);
        PowerMockito.when(settlementRequest.getReconciliationTotals()).thenReturn(reconciliationTotals);

        PowerMockito.when(requestContext.getRequest()).thenReturn(settlementRequest);
        PowerMockito.when(requestContext.getTerminalInfo()).thenReturn(terminalInfo);

        NiblReconciliationReceipt niblReconciliationReceipt = new NiblReconciliationReceiptMaker(requestContext, isoMessageResponse)
                .prepare();

        assertNotNull(niblReconciliationReceipt);
        assertEquals("CityTech", niblReconciliationReceipt.getRetailer().getRetailerName());
        assertEquals("Kamaladi", niblReconciliationReceipt.getRetailer().getRetailerAddress());
        assertEquals("123456789012345", niblReconciliationReceipt.getRetailer().getMerchantId());
        assertEquals("99991002", niblReconciliationReceipt.getRetailer().getTerminalId());
        assertEquals("1535231008", niblReconciliationReceipt.getPerformance().getStartDateTime());
        assertEquals("1535231008", niblReconciliationReceipt.getPerformance().getEndDateTime());
        assertEquals("001", niblReconciliationReceipt.getReconciliationBatchNumber());
        assertEquals(this.reconciliationTotals, niblReconciliationReceipt.getReconciliationTotals());
        assertEquals(ReconciliationReceiptLabels.SETTLEMENT_SUCCESS, niblReconciliationReceipt.getReconciliationResult());
        assertEquals("000048", niblReconciliationReceipt.getHost());
    }
}
