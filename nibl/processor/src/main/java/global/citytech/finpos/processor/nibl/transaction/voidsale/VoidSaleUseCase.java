package global.citytech.finpos.processor.nibl.transaction.voidsale;


import java.util.List;
import java.util.Collections;

import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.voidsale.VoidSaleRequestSender;
import global.citytech.finpos.processor.nibl.transaction.TransactionRequestModel;
import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finpos.processor.nibl.transaction.template.CardLessTransactionUseCase;
import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils;
import global.citytech.finpos.processor.nibl.transaction.validator.NiblTransactionValidator;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.listeners.CardConfirmationListener;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.JsonUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Unique Shakya on 3/16/2021.
 */
public class VoidSaleUseCase extends CardLessTransactionUseCase {

    public VoidSaleUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    public TransactionResponseModel execute(TransactionRequestModel request) {
        this.request = request;
        this.originalTransactionLog = this.retrieveOriginalTransactionLog();
        if (this.originalTransactionLog == null)
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_FOUND);
        this.validateTransactionLog();
        this.setTotalTransactionAmount();

        this.request = request;
        this.transactionRequest = this.request.getTransactionRequest();
        if (this.deviceNotReady())
            throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
        this.transactionValidator = new NiblTransactionValidator(this.request.getApplicationRepository());
        this.stan = this.terminalRepository.getSystemTraceAuditNumber();
        this.invoiceNumber = this.retrieveInvoiceNumber();
        this.batchNumber = this.terminalRepository.getReconciliationBatchNumber();
        try {
            this.initTransaction();
            return this.initializeTransactionWithCard(TransactionUtils.getAllowedCardEntryModes(this.transactionRequest.getTransactionType()), false);
        } catch (PosException pe) {
            pe.printStackTrace();
            throw pe;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } finally {
            this.cleanup();
        }
    }

    private TransactionResponseModel initializeTransactionWithCard(List<CardType> allowedCardEntryModes, boolean isForceContactCardPrompt) throws InterruptedException {
        this.retrieveCardDetails(allowedCardEntryModes, isForceContactCardPrompt);
        if (this.invalidTransaction())
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
        this.validateIfEmiTransaction(readCardResponse);
        if (this.isTransactionAuthorizationFailure()) {
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
        }
        this.processCard();
        this.notifier.notify(Notifier.EventType.TRANSMITTING_REQUEST, Notifier.EventType.TRANSMITTING_REQUEST.getDescription());
        return this.performTransactionWithHost();
    }

    protected TransactionResponseModel performTransactionWithHost() {
        try {
            IsoMessageResponse isoMessageResponse = this.sendIsoMessage();
            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait..");
            InvalidResponseHandlerResponse invalidResponseHandlerResponse = this.checkForInvalidResponseFromHost(isoMessageResponse);
            if (invalidResponseHandlerResponse.isInvalid()) {
                return this.handleInvalidResponseFromHost(invalidResponseHandlerResponse);
            }
            boolean isApproved = this.isTransactionApprovedByActionCode(isoMessageResponse);
            this.processOnlineResultToCard(isoMessageResponse);
            boolean declinedByCard = this.checkTransactionDeclinedByCard(isoMessageResponse);
            if (declinedByCard) {
                return this.handleApprovedTransactionDeclinedByCard();
            }
            if (this.isForceContactCardPromptActionCode(isoMessageResponse)) {
                return handleForceContactCardPromptFromHost();
            }
            this.handleTransactionCompletion(isApproved, isoMessageResponse,Notifier.TransactionType.VOID.getDescription());
            boolean shouldPrintCustomerCopy = prepareReceiptLog(isoMessageResponse);
            this.updateTransactionLog(isApproved, isoMessageResponse);
            this.updateTransactionLogOfOriginalTransaction(isApproved);

            this.transactionResponseModel = this.prepareTransactionResponse(isoMessageResponse, isApproved, shouldPrintCustomerCopy);
            logger.log("transactionResponseModel >>> " + transactionResponseModel);
            String message = this.retrieveMessage(isoMessageResponse, isApproved);
            this.notifier.notify(Notifier.EventType.PUSH_MERCHANT_LOG, this.stan, message, isApproved);

            this.printReceipt(shouldPrintCustomerCopy);

            return this.prepareTransactionResponse(isoMessageResponse, isApproved, shouldPrintCustomerCopy);
        } catch (FinPosException fpe) {
            if (this.unableToGoOnlineException(fpe))
                return this.handleUnableToGoOnlineTransaction();
            else
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
    }

    private IsoMessageResponse sendIsoMessage() {
        RequestContext context = this.prepareTemplateRequestContext();
        this.transactionIsoRequest = this.prepareTransactionIsoRequest();
        context.setRequest(transactionIsoRequest);
        NIBLMessageSenderTemplate sender = this.getRequestSender(context);
        return sender.send();
    }

    @Override
    protected String retrieveInvoiceNumber() {
        return this.originalTransactionLog.getInvoiceNumber();
    }


    @Override
    protected TransactionLog retrieveTransactionLogToStore(boolean isApproved, IsoMessageResponse isoMessageResponse) {
        return TransactionLog.Builder.newInstance()
                .withStan(stan)
                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
                .withInvoiceNumber(invoiceNumber)
                .withRrn(
                        IsoMessageUtils.retrieveFromDataElementsAsString(
                                isoMessageResponse,
                                DataElement.RETRIEVAL_REFERENCE_NUMBER
                        )
                )
                .withAuthCode(
                        IsoMessageUtils.retrieveFromDataElementsAsString(
                                isoMessageResponse,
                                DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE
                        )
                )
                .withTransactionType(transactionRequest.getTransactionType())
                .withOriginalTransactionType(
                        this.originalTransactionLog.getTransactionType()
                )
                .withTransactionAmount(this.transactionIsoRequest.getTransactionAmount())
                .withOriginalTransactionAmount(this.totalTransactionAmount)
                .withTransactionDate(
                        IsoMessageUtils.retrieveFromDataElementsAsString(
                                isoMessageResponse,
                                DataElement.LOCAL_DATE
                        )
                )
                .withTransactionTime(
                        IsoMessageUtils.retrieveFromDataElementsAsString(
                                isoMessageResponse,
                                DataElement.LOCAL_TIME
                        )
                )
                .withTransactionStatus(isApproved ? APPROVED : DECLINED)
                .withPosEntryMode(this.originalTransactionLog.getPosEntryMode())
                .withOriginalPosEntryMode(this.originalTransactionLog.getPosEntryMode())
                .withPosConditionCode(this.transactionIsoRequest.getPosConditionCode())
                .withOriginalPosConditionCode(this.originalTransactionLog.getPosConditionCode())
                .withReconcileStatus("")
                .withReconcileTime("")
                .withReconcileDate("")
                .withReconcileBatchNo(batchNumber)
                .withReadCardResponse(this.readCardResponse)
                .withOriginalTransactionReferenceNumber(this.transactionRequest.getOriginalInvoiceNumber())
                .withReceiptLog(this.receiptLog)
                .withAuthorizationCompleted(true)
                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                .withTipAdjusted(false)
                .withTransactionVoided(false)
                .withCurrencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .withVatInfo(originalTransactionLog.getVatInfo())
                .build();
    }

    @Override
    protected TransactionLog retrieveOriginalTransactionLog() {
        String invoiceNumber = this.request.getTransactionRequest().getOriginalInvoiceNumber();
        String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
        return this.request.getTransactionRepository().getTransactionLogWithInvoiceNumber(invoiceNumber, batchNumber);
    }

    @Override
    protected void validateTransactionLog() {
        if (!(this.originalTransactionLog.getTransactionType() == TransactionType.PURCHASE || this.originalTransactionLog.getTransactionType() == TransactionType.CASH_ADVANCE))
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
        if (!this.originalTransactionLog.getTransactionStatus().equalsIgnoreCase(APPROVED))
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_FOUND);
    }

    @Override
    protected void setTotalTransactionAmount() {
        if (this.originalTransactionLog.isTipAdjusted()) {
            TransactionLog tipTransactionByInvoiceNumber = this.request.getTransactionRepository()
                    .getTipTransactionLogWithInvoiceNumber(retrieveInvoiceNumber(), terminalRepository.getReconciliationBatchNumber());
            this.totalTransactionAmount = tipTransactionByInvoiceNumber.getTransactionAmount()
                    .add(this.originalTransactionLog.getTransactionAmount());
            logger.debug("::: VOID AMOUNT IF TIP ::: " + this.totalTransactionAmount);
        } else {
            this.totalTransactionAmount = this.originalTransactionLog.getTransactionAmount();
            logger.debug("::: VOID AMOUNT IF NOT TIP ::: " + this.totalTransactionAmount);
        }
    }

    @Override
    protected String getTransactionTypeClassName() {
        return VoidSaleUseCase.class.getSimpleName();
    }

    @Override
    protected TransactionIsoRequest prepareTransactionIsoRequest() {
        return TransactionIsoRequest.Builder.newInstance()
                .transactionAmount(this.totalTransactionAmount)
                .pan(this.originalTransactionLog.getReadCardResponse().getCardDetails().getPrimaryAccountNumber())
                .expireDate(this.originalTransactionLog.getReadCardResponse().getCardDetails().getExpiryDate())
                .posEntryMode(this.originalTransactionLog.getPosEntryMode())
                .cardSequenceNumber(this.originalTransactionLog.getReadCardResponse().getCardDetails().getPrimaryAccountNumberSerialNumber())
                .posConditionCode(this.originalTransactionLog.getPosConditionCode())
                .retrievalReferenceNumber(this.originalTransactionLog.getRrn())
                .approvalCode(this.originalTransactionLog.getAuthCode())
                .originalAmount(this.totalTransactionAmount)
                .invoiceNumber(this.originalTransactionLog.getInvoiceNumber())
                .localDate(HelperUtils.getDefaultLocaleDateWithoutYear())
                .localTime(HelperUtils.getDefaultLocaleTimeHhMmSs())
                .build();
    }

    @Override
    protected NIBLMessageSenderTemplate getRequestSender(RequestContext context) {
        return new VoidSaleRequestSender(new NIBLSpecInfo(), context);
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.VOID_SALE.getCode();
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy, String miniStatementData) {
        return null;
    }


    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy) {
        return VoidSaleResponseModel.Builder.newInstance()
                .withApproved(isApproved)
                .withMessage(this.retrieveMessage(isoMessageResponse, isApproved))
                .withDebugRequestMessage(isoMessageResponse.getDebugRequestString())
                .withDebugResponseMessage(isoMessageResponse.getDebugResponseString())
                .withStan(this.stan)
                .withShouldPrintCustomerCopy(shouldPrintCustomerCopy)
                .build();
    }

    @Override
    protected TransactionResponseModel prepareDeclinedTransactionResponse(String message,
                                                                          boolean shouldPrintReceipt) {
        return VoidSaleResponseModel.Builder.newInstance()
                .withApproved(false)
                .withMessage(message)
                .withStan(this.stan)
                .withShouldPrintCustomerCopy(shouldPrintReceipt)
                .build();
    }

    @Override
    protected String getOriginalTransactionReferenceNumber() {
        return this.transactionIsoRequest.getOriginalRetrievalReferenceNumber();
    }

    @Override
    protected boolean isAuthorizationCompleted() {
        return true;
    }

    @Override
    protected void updateTransactionLogOfOriginalTransaction(boolean isApproved) {

        this.request.getTransactionRepository().updateVoidStatusForGivenTransactionDetails(
                isApproved,
                this.originalTransactionLog.getTransactionType(),
                this.originalTransactionLog.getInvoiceNumber()
        );

        if (isApproved)
            this.notifier.notify(
                    Notifier.EventType.TRANSACTION_APPROVED,
                    Notifier.TransactionType.VOID.name());

        else
            this.notifier.notify(
                    Notifier.EventType.TRANSACTION_DECLINED,
                    Notifier.TransactionType.VOID.name()
            );
    }

    @Override
    protected boolean shouldIncrementInvoiceNumber(boolean isApproved) {
        return false;
    }
}
