package global.citytech.finposframework.iso8583;

import org.junit.Test;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finposframework.utility.EncodingUtils;
import global.citytech.finposframework.supports.Optional;

public class DataElementTest {
    private Logger logger = Logger.getLogger(DataElementTest.class.getName());

    @Test
    public void parseASCIITest() {
        DataElementConfig de3Config = new DataElementConfig(
                39,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(4, LengthConfig.LengthType.FIXED)
        );
        //parse data now
        Optional<DataElement> de3 = this.formatIfAscii("3030", de3Config);
        logger.log(de3.get().toString());
    }

    private Optional<DataElement> formatIfAscii(String tobeData, DataElementConfig config) {
        byte[] bytes = EncodingUtils.asciiRepresentationAsByte(tobeData);
        return Optional.of(new DataElement(config.getIndex(), new String(bytes)));
    }
}
