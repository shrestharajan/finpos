package global.citytech.finposframework.hardware.io.status

/**
 * Created by Rishav Chudal on 6/3/20.
 */
data class IOStatusResponse(val printerStatus: String,
                              val magReaderStatus: String,
                              val iccReaderStatus: String,
                              val piccReaderStatus: String) {
}