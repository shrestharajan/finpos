package global.citytech.finposframework.switches.preauths;

import global.citytech.finposframework.switches.RequestParameter;

public interface PreauthRequestParameter extends RequestParameter {
}
