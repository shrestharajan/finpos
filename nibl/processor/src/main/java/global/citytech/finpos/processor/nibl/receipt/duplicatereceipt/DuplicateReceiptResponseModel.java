package global.citytech.finpos.processor.nibl.receipt.duplicatereceipt;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.duplicatereceipt.DuplicateReceiptResponseParameter;

/**
 * Created by Unique Shakya on 12/15/2020.
 */
public class DuplicateReceiptResponseModel implements UseCase.Response, DuplicateReceiptResponseParameter {

    private Result result;
    private String message;

    public DuplicateReceiptResponseModel(Result result, String message) {
        this.result = result;
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }
}
