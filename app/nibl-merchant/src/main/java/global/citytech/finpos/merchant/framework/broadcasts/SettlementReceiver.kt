package global.citytech.finpos.merchant.framework.broadcasts

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import global.citytech.common.Constants
import global.citytech.common.data.PosCallback
import global.citytech.common.data.PosMessage
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.repository.app.CommandQueueRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.CommandType
import global.citytech.finpos.merchant.domain.usecase.app.CommandQueueUseCase
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.CommandQueueDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.presentation.model.command.pull.PullCommandResponse
import global.citytech.finpos.merchant.presentation.model.command.pull.mapToCommand
import global.citytech.finpos.merchant.utils.AppConstant
import global.citytech.finposframework.log.Logger
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SettlementReceiver : BroadcastReceiver() {

    private val logger = Logger(SettlementReceiver::class.java)
    lateinit var commandQueueUseCase: CommandQueueUseCase
    lateinit var localDataUseCase: LocalDataUseCase

    override fun onReceive(context: Context?, intent: Intent?) {
        commandQueueUseCase = CommandQueueUseCase(
            CommandQueueRepositoryImpl(
                CommandQueueDataSourceImpl(NiblMerchant.INSTANCE)
            )
        )

        localDataUseCase = LocalDataUseCase(
            LocalRepositoryImpl(
                LocalDatabaseSourceImpl(),
                PreferenceManager
            )
        )

        commandQueueUseCase = CommandQueueUseCase(
            CommandQueueRepositoryImpl(
                CommandQueueDataSourceImpl(NiblMerchant.INSTANCE)
            )
        )

        intent?.let {
            if (it.action.equals(NiblMerchant.INSTANCE.packageName.plus(".UPDATE.APPLICATION"))) {
                val appDownloadExtra = it.getStringExtra(AppConstant.EXTRA_APP_DOWNLOAD)
                localDataUseCase.saveAppDownloadExtra(appDownloadExtra)
                prepareSettlementCommand(context)
            }
        }
    }

    private fun prepareSettlementCommand(context: Context?) {
        var commandList: MutableList<PullCommandResponse> = mutableListOf()
        commandList.add(
            PullCommandResponse(
                "TRANSACTION_001",
                CommandType.TRANSACTION_LOG_PUSH.toString()
            )
        )
        addCommandToQueue(context, commandList)
    }


    @SuppressLint("CheckResult")
    private fun addCommandToQueue(
        context: Context?,
        commandList: MutableList<PullCommandResponse>
    ) {
        this.logger.debug(":: FINPOS COMMAND CHECK :: STARTED  ".plus(commandList.size))
        Observable.create(ObservableOnSubscribe<Void> { onSubscribe(it, commandList) })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({}, {}, {
                if (NiblMerchant.INSTANCE.isDashActivityVisible()) {
                    logger.log("::: DASH ACTIVITY VISIBLE ::: START COMMAND >>>")
                    val intent = Intent(Constants.INTENT_ACTION_POS_CALLBACK)
                    intent.putExtra(
                        Constants.KEY_BROADCAST_MESSAGE,
                        PosCallback(PosMessage.POS_MESSAGE_START_COMMAND)
                    )
                    LocalBroadcastManager.getInstance(context!!).sendBroadcast(intent)
                }
            })
    }

    private fun onSubscribe(
        it: ObservableEmitter<Void>,
        commandList: MutableList<PullCommandResponse>
    ) {
        markForceSettlement()
        commandList.forEach {
            logger.log("COMMAND QUEUE BEFORE >>> ${commandQueueUseCase.getAllCommands()}")
            commandQueueUseCase.addCommand(it.mapToCommand())
        }
        it.onComplete()
    }

    private fun markForceSettlement() {
        logger.log("::: MARKING FORCE SETTLEMENT :::")
        PreferenceManager.setForceSettlementPreference(true)
        PreferenceManager.setBroadcastAcknowledgeActive(true)
    }
}