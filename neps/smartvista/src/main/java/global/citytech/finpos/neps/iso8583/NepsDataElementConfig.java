package global.citytech.finpos.neps.iso8583;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.iso8583.DataConfig;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.DataElementConfig;
import global.citytech.finposframework.iso8583.DataElementConfigs;
import global.citytech.finposframework.iso8583.EncodingType;
import global.citytech.finposframework.iso8583.LengthConfig;
import global.citytech.finposframework.iso8583.PaddingType;

/**
 * User: Surajchhetry Date: 2/18/20 Time: 8:36 PM
 */

public class NepsDataElementConfig extends DataElementConfigs {
    @Override
    protected List<DataElementConfig> config() {
        List<DataElementConfig> dataElementConfigs = new ArrayList<>();
        dataElementConfigs.add(new DataElementConfig(DataElement.PRIMARY_ACCOUNT_NUMBER,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(LengthConfig.LengthType.LLVAR, EncodingType.ASCII)));
        dataElementConfigs.add(new DataElementConfig(DataElement.PROCESSING_CODE,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(12, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.AMOUNT,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(24, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.AMOUNT_SETTLEMENT,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(26, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.TRANSMISSION_DATE_TIME,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(20, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.SYSTEM_TRACE_AUDIT_NUMBER,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(12, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.LOCAL_TIME,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(24, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.PAN_EXPIRE_DATE,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(6, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.SETTLEMENT_DATE,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(12, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.POS_ENTRY_MODE,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(6, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.FUNCTION_CODE_OR_NETWORK_INTERNATIONAL_IDENTIFIER,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(6, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(4, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.ORIGINAL_AMOUNT,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(26, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.TRACK_II_DATA,
                new DataConfig(DataConfig.DataType.ALPHA, EncodingType.ASCII, 37),
                new LengthConfig(LengthConfig.LengthType.LLVAR, EncodingType.ASCII)));
        dataElementConfigs.add(new DataElementConfig(DataElement.RETRIEVAL_REFERENCE_NUMBER,
                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC, EncodingType.ASCII),
                new LengthConfig(24, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE,
                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC, EncodingType.ASCII),
                new LengthConfig(12, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.RESPONSE_CODE,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(6, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.CARD_ACCEPTOR_TERMINAL_ID,
                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC, EncodingType.ASCII),
                new LengthConfig(16, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.CARD_ACCEPTOR_ACQUIRER_ID,
                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC, EncodingType.ASCII),
                new LengthConfig(30, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.ADDITIONAL_RESPONSE_DATA,
                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC, EncodingType.ASCII),
                new LengthConfig(LengthConfig.LengthType.LLVAR, EncodingType.ASCII)));
        dataElementConfigs.add(new DataElementConfig(DataElement.FEES_AMOUNT,
                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC, EncodingType.ASCII),
                new LengthConfig(LengthConfig.LengthType.LLLVAR, EncodingType.ASCII)));

        dataElementConfigs.add(new DataElementConfig(DataElement.ADDITIONAL_DATA_PRIVATE,
                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC, EncodingType.BCD),
                new LengthConfig(LengthConfig.LengthType.LLLVAR, EncodingType.ASCII)));

//        dataElementConfigs.add(new DataElementConfig(DataElement.ADDITIONAL_DATA_PRIVATE,
//                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC, EncodingType.ASCII),
//                new LengthConfig(LengthConfig.LengthType.LLLVAR, EncodingType.ASCII)));

        dataElementConfigs.add(new DataElementConfig(DataElement.CURRENCY_CODE,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(6, LengthConfig.LengthType.FIXED)));

        dataElementConfigs.add(new DataElementConfig(DataElement.PIN_BLOCK,
                new DataConfig(DataConfig.DataType.ALPHA, EncodingType.BCD),
                new LengthConfig(16, LengthConfig.LengthType.FIXED)));

        dataElementConfigs.add(new DataElementConfig(DataElement.SECURITY_RELATED_CONTROL_INFORMATION,
                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC, EncodingType.BCD, 16),
                new LengthConfig(LengthConfig.LengthType.LLVAR, EncodingType.ASCII)));
        dataElementConfigs.add(new DataElementConfig(DataElement.ADDITIONAL_AMOUNT,
                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC, EncodingType.ASCII,120),
                new LengthConfig(LengthConfig.LengthType.LLLVAR, EncodingType.ASCII)));
        dataElementConfigs.add(new DataElementConfig(DataElement.ICC_SYSTEM_RELATED_DATA,
                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC, EncodingType.BCD, 255),
                new LengthConfig(LengthConfig.LengthType.LLLVAR, EncodingType.BCD)));
        dataElementConfigs.add(new DataElementConfig(DataElement.PRIVATE_USE_FIELD_62,
                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC, EncodingType.ASCII, 999),
                new LengthConfig(LengthConfig.LengthType.LLLVAR, EncodingType.ASCII)));
        dataElementConfigs.add(new DataElementConfig(DataElement.PRIVATE_USE_FIELD_63,
                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC, EncodingType.ASCII, 999),
                new LengthConfig(LengthConfig.LengthType.LLLVAR, EncodingType.ASCII)));
        dataElementConfigs.add(new DataElementConfig(DataElement.PRIMARY_MAC,
                new DataConfig(DataConfig.DataType.ALPHA, EncodingType.BCD),
                new LengthConfig(8, LengthConfig.LengthType.FIXED)));
        dataElementConfigs.add(new DataElementConfig(DataElement.ACCOUNT_IDENTIFICATION_ONE,
                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC, EncodingType.ASCII),
                new LengthConfig(28, LengthConfig.LengthType.LLVAR)));
        dataElementConfigs.add(new DataElementConfig(DataElement.ACCOUNT_IDENTIFICATION_TWO,
                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC, EncodingType.ASCII),
                new LengthConfig(28, LengthConfig.LengthType.LLVAR)));
        dataElementConfigs.add(new DataElementConfig(DataElement.SECONDARY_MAC,
                new DataConfig(DataConfig.DataType.ALPHA, EncodingType.BCD),
                new LengthConfig(8, LengthConfig.LengthType.FIXED)));
        return dataElementConfigs;
    }

}