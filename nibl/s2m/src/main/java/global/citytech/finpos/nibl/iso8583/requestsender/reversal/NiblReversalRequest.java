package global.citytech.finpos.nibl.iso8583.requestsender.reversal;

import java.math.BigDecimal;

import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.transaction.Request;

/**
 * Created by Rishav Chudal on 9/28/20.
 */
public class NiblReversalRequest implements Request {
    private String processingCode;
    private String nii;
    private String pan;
    private BigDecimal amount = BigDecimal.ZERO;
    private BigDecimal additionalAmount = BigDecimal.ZERO;
    private String expiryDate;
    private PosEntryMode posEntryMode;
    private String posConditionCode;
    private String track2Data;
    private String pinBlock;
    private String invoiceNumber;
    private String stan;
    private String localTime;
    private String localDate;
    private String emvData;

    public String getProcessingCode() {
        return processingCode;
    }

    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getNii() {
        return nii;
    }

    public void setNii(String nii) {
        this.nii = nii;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public void setPosEntryMode(PosEntryMode posEntryMode) {
        this.posEntryMode = posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public void setPosConditionCode(String posConditionCode) {
        this.posConditionCode = posConditionCode;
    }

    public String getTrack2Data() {
        return track2Data;
    }

    public void setTrack2Data(String track2Data) {
        this.track2Data = track2Data;
    }

    public String getPinBlock() {
        return pinBlock;
    }

    public void setPinBlock(String pinBlock) {
        this.pinBlock = pinBlock;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public BigDecimal getAdditionalAmount() {
        return additionalAmount;
    }

    public void setAdditionalAmount(BigDecimal additionalAmount) {
        this.additionalAmount = additionalAmount;
    }
    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public String getLocalDate() {
        return localDate;
    }

    public void setLocalDate(String localDate) {
        this.localDate = localDate;
    }

    public String getEmvData() {
        return emvData;
    }

    public void setEmvData(String emvData) {
        this.emvData = emvData;
    }
}
