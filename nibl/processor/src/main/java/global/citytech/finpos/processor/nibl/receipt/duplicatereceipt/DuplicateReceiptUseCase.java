package global.citytech.finpos.processor.nibl.receipt.duplicatereceipt;

import global.citytech.finpos.processor.nibl.transaction.receipt.NiblTransactionReceiptPrintHandler;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.duplicatereceipt.DuplicateReceiptRequester;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;

/**
 * Created by Unique Shakya on 12/15/2020.
 * Modified by Rishav Chudal on 5/6/2021
 */
public class DuplicateReceiptUseCase implements DuplicateReceiptRequester<DuplicateReceiptRequestModel,
        DuplicateReceiptResponseModel>, UseCase<DuplicateReceiptRequestModel, DuplicateReceiptResponseModel> {

    @Override
    public DuplicateReceiptResponseModel execute(DuplicateReceiptRequestModel duplicateReceiptRequestModel) {
        ReceiptLog receiptLog;
        DuplicateReceiptResponseModel responseModel = new DuplicateReceiptResponseModel(Result.FAILURE, "Unable to print");
        if (duplicateReceiptRequestModel.getStan() == null) {
            receiptLog = duplicateReceiptRequestModel.getTransactionRepository().getReceiptLog();
        } else {
            receiptLog = duplicateReceiptRequestModel.getTransactionRepository().getReceiptLogByStan(duplicateReceiptRequestModel.getStan());
        }
        if (receiptLog == null)
            throw new PosException(PosError.DEVICE_ERROR_NO_RECENT_TRANSACTION);
        ReceiptHandler.TransactionReceiptHandler transactionReceiptHandler = new
                NiblTransactionReceiptPrintHandler(duplicateReceiptRequestModel.getPrinterService());
        try {
            PrinterResponse printerResponse = transactionReceiptHandler.printTransactionReceipt(receiptLog, ReceiptVersion.DUPLICATE_COPY);
            if (printerResponse.getResult() == Result.SUCCESS) {
                responseModel = new DuplicateReceiptResponseModel(printerResponse.getResult(), "Duplicate Receipt Printed");
            } else {
                responseModel = new DuplicateReceiptResponseModel(printerResponse.getResult(), printerResponse.getMessage());
            }
        } catch (PosException e) {
            throw e;
        } catch (Exception e) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_PRINT);
        }
        return responseModel;
    }
}
