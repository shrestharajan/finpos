package global.citytech.finpos.merchant.utils;

import static global.citytech.finpos.device.weipass.utils.WeipassLogger.debug;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.util.Log;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import global.citytech.finpos.merchant.R;
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration;
import global.citytech.finposframework.hardware.io.sound.Sound;
import global.citytech.finposframework.log.Logger;

public class SoundUtils {

    private static Context mActivity;


    public static void play(final Context activity, Double amount, String extraSound) {

        if (amount > 0 && amount < 1000000000) {
            mActivity = activity;

            double myDouble = amount;
            int lastDecimalDigit = (int) (myDouble * 100) % 100; // Extract the last decimal digit
            String lastDigitAsString = String.valueOf(lastDecimalDigit);

            String paisa = " paisa";
            if (new DeviceConfiguration().getCurrencyCode() == "524") {
                paisa = " paisa";
            } else if (new DeviceConfiguration().getCurrencyCode() == "840") {
                paisa = " cent";
            }

            String successVoiceMessage = "success ";
            String soundAmount = NumberToSoundConverterInNepali.convertIntoRupaiya((amount).longValue());
            String soundAmountWithDecimal = successVoiceMessage + soundAmount + " " + lastDigitAsString + "" + paisa + " only" + " bill";
            String soundAmountWithoutDecimal = successVoiceMessage + soundAmount + " only" + " bill";

            String soundAmountWithoutDecimalRadha = successVoiceMessage + soundAmount + " only" + " bill_vhuktani" + " radha" + " vayo";
            String soundAmountWithDecimalRadha = successVoiceMessage + soundAmount + " " + lastDigitAsString + "" + paisa + " only" + " bill_vhuktani" + " radha" + " vayo";

            if (lastDecimalDigit == 0) {

                if (extraSound == "radha") {
                    playSoundAll(soundAmountWithoutDecimalRadha);
                } else {
                    playSoundAll(soundAmountWithoutDecimal);
                }
                Logger.getLogger(SoundUtils.class.getName())
                        .debug(soundAmountWithoutDecimal);
            } else {
                if (extraSound == "radha") {
                    playSoundAll(soundAmountWithDecimalRadha);
                } else {
                    playSoundAll(soundAmountWithDecimal);
                }
                Logger.getLogger(SoundUtils.class.getName())
                        .debug(soundAmountWithDecimal);
            }
        }
    }

    public static void play(final Context activity, Double amount, String extraSound, String currencyCode) {

        if (amount > 0 && amount < 1000000000) {
            mActivity = activity;

            double myDouble = amount;
            int lastDecimalDigit = (int) (myDouble * 100) % 100; // Extract the last decimal digit
            String lastDigitAsString = String.valueOf(lastDecimalDigit);

            String paisa = " paisa";
            if (currencyCode == "NPR") {
                paisa = " paisa";
            } else if (currencyCode == "USD") {
                paisa = " cent";
            }

            String successVoiceMessage = "success ";
            String soundAmount = NumberToSoundConverterInNepali.convertIntoRupaiya((amount).longValue(), currencyCode);
            String soundAmountWithDecimal = successVoiceMessage + soundAmount + " " + lastDigitAsString + "" + paisa + " only" + " bill";
            String soundAmountWithoutDecimal = successVoiceMessage + soundAmount + " only" + " bill";

            String soundAmountWithoutDecimalRadha = successVoiceMessage + soundAmount + " only" + " bill_vhuktani" + " radha" + " vayo";
            String soundAmountWithDecimalRadha = successVoiceMessage + soundAmount + " " + lastDigitAsString + "" + paisa + " only" + " bill_vhuktani" + " radha" + " vayo";

            if (lastDecimalDigit == 0) {

                if (extraSound == "radha") {
                    playSoundAll(soundAmountWithoutDecimalRadha);
                } else {
                    playSoundAll(soundAmountWithoutDecimal);
                }
                Logger.getLogger(SoundUtils.class.getName())
                        .debug(soundAmountWithoutDecimal);
            } else {
                if (extraSound == "radha") {
                    playSoundAll(soundAmountWithDecimalRadha);
                } else {
                    playSoundAll(soundAmountWithDecimal);
                }
                Logger.getLogger(SoundUtils.class.getName())
                        .debug(soundAmountWithDecimal);
            }
        }
    }

    private static void playSoundAll(String resPrefix) {

        final String soundArray[];
        soundArray = resPrefix.split(" ");
        final MediaPlayer[] mp = new MediaPlayer[soundArray.length];

        for (int i = 0; i < soundArray.length; i++) {
            int soundResId = mActivity.getResources().getIdentifier("np_" + soundArray[i], "raw", mActivity.getPackageName());
            mp[i] = MediaPlayer.create(mActivity.getApplicationContext(), soundResId);

            MediaPlayerRegistry.mList.add(mp[i]);
            final int finalI = i;
            if (i == 0) {
                mp[i].start();
                Log.d("Playing :: np_", soundArray[i]);
            }

            mp[i].setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mpp) {
                    mp[finalI].release();
                    System.out.println("ON COMPLETION :::: "+ (finalI + 1));
                    if (finalI < soundArray.length - 1) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            mp[finalI + 1].setPlaybackParams(mp[finalI + 1].getPlaybackParams().setSpeed(1.2f)); // Reset to normal speed
                        }
                        mp[finalI + 1].start();
                    } else {
//                        MediaPlayerRegistry.stopAll();
                    }
                }
            });
        }
    }
}


