package global.citytech.finpos.merchant.presentation.mobilenfcpayment.receipt

import global.citytech.finposframework.usecases.transaction.receipt.Performance
import global.citytech.finposframework.usecases.transaction.receipt.Retailer

data class MobileNfcPaymentReceipt (
    val retailer: Retailer,
    val performance: Performance,
    val mobileNfcTransactionDetail: MobileNfcTransactionDetail,
    val thankYouMessage: String
)