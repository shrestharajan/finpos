package global.citytech.finpos.merchant.service

import android.content.Context
import android.content.Intent
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import global.citytech.common.AppCountDownTimer
import global.citytech.common.data.Response
import global.citytech.common.extensions.hasInternetConnection
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.MerchantTransactionLog
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.presentation.model.merchanttransactionlog.MerchantTransactionLogResponse
import global.citytech.finpos.merchant.utils.ApiConstant
import global.citytech.finpos.merchant.utils.AppConstant
import global.citytech.finpos.merchant.utils.AppConstant.MAX_TRANSACTIONS_COUNT_FOR_BATCH_PUSH
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finpos.merchant.utils.getMinutesAndSecondsDataFromMillis
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.exceptions.PosException
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Rishav Chudal on 29/08/2022.
 */
class TransactionPushService : LifecycleService() {
    val TAG = TransactionPushService::class.java.name
    private var localRepository = LocalRepositoryImpl(
        LocalDatabaseSourceImpl(),
        PreferenceManager
    )
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var countDownTimer = AppCountDownTimer(
        AppConstant.MERCHANT_TRANSACTION_LOG_PUSH_INTERVAL_MILLIS,
        1000,
        this::onCountDownTick,
        this::onCountDownFinish
    )

    companion object {
        var isLogPushInProgress: Boolean = false
        private val startPushingMerchantTransactionLog by lazy { MutableLiveData<Boolean>() }

        fun start(context: Context) {
            stop(context)
            printLog(
                TransactionPushService::class.java.name,
                "START PUSH MERCHANT TRANSACTION LOG SERVICE..."
            )
            context.startService(
                Intent(
                    context.applicationContext,
                    TransactionPushService::class.java
                )
            )
        }

        private fun stop(context: Context) {
            printLog(
                TransactionPushService::class.java.name,
                "STOP PUSH MERCHANT TRANSACTION LOG SERVICE..."
            )
            context.stopService(
                Intent(
                    context.applicationContext,
                    TransactionPushService::class.java
                )
            )
        }

        fun pushTransactionLog() {
            startPushingMerchantTransactionLog.value = true
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        printLog(TAG, "MerchantTransactionLogPushService ::: OnStartCommand...", true)
        observeStartPushingMerchantTransactionLogLiveData()
        onCountDownFinish()
        return super.onStartCommand(intent, flags, startId)
    }

    private fun observeStartPushingMerchantTransactionLogLiveData() {
        printLog(TAG, "Observing StartPushingMerchantTransactionLog Live Data...", true)
        startPushingMerchantTransactionLog.observe(this, Observer {
            if (it) {
                printLog(TAG, "startPushingMerchantTransactionLog is true...", true)
                pushTransactionLogIfFeasible()
            }
        })
    }

    private fun onCountDownTick(millisUntilFinished: Long) {
        printLog(
            TAG,
            "Time left to push next batch ::: "
                .plus(millisUntilFinished.getMinutesAndSecondsDataFromMillis()), true
        )
        if (!NiblMerchant.INSTANCE.iPlatformManager.asBinder().pingBinder()) {
            printLog(TAG, ":: PLATFORM MANAGER DISCONNECTED ::", true)
            NiblMerchant.INSTANCE.bindService()
        }
    }

    private fun onCountDownFinish() {
        pushTransactionLogIfFeasible()
        countDownTimer.start()
    }

    fun pushTransactionLogIfFeasible() {
        printLog(TAG, "Is Log Push Already in Progress ? ::: ".plus(isLogPushInProgress), true)
        if (isLogPushInProgress) {
            return
        }
        isLogPushInProgress = true
        getMerchantTransactionLogsToPush()
    }

    private fun getMerchantTransactionLogsToPush() {
        printLog(TAG, "Getting Merchant Transaction Logs...", true)
        compositeDisposable.add(
            localRepository.getMerchantTransactionLogsOfGivenCount(
                AppConstant.MAX_TRANSACTIONS_COUNT_FOR_BATCH_PUSH
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    pushAvailableTransactionLogs(it)
                }, {
                    it.printStackTrace()
                    isLogPushInProgress = false
                    printLog(TAG, "Error in getting Merchant Transaction Log...", true)
                })
        )
    }

    private fun pushAvailableTransactionLogs(transactionLogs: List<MerchantTransactionLog>?) {
        if (transactionLogs == null || transactionLogs.isEmpty()) {
            printLog(TAG, "No Transaction Logs to Push...", true)
            isLogPushInProgress = false
            return
        }
        printLog(TAG,"Merchant Transaction Log Size ::: ".plus(transactionLogs.size),false)
        updateTransactionLogsToServer(transactionLogs)
    }

    private fun updateTransactionLogsToServer(transactionLogs: List<MerchantTransactionLog>) {
        if (!NiblMerchant.INSTANCE.hasInternetConnection()) {
            printLog(TAG, "No Internet Connection...")
            isLogPushInProgress = false
            return
        }
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<MerchantTransactionLogResponse> {
                onSubscribe(transactionLogs, it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        onNext(it)
                    },
                    this::onError
                )
        )
    }

    private fun onSubscribe(
        merchantTransactionLogs: List<MerchantTransactionLog>?,
        emitter: ObservableEmitter<MerchantTransactionLogResponse>
    ) {
        val jsonRequest = Jsons.toJsonObj(merchantTransactionLogs)
        printLog(TAG, "Merchant Transaction Log Request::: $jsonRequest", true)
        val jsonResponse =
            NiblMerchant.INSTANCE.iPlatformManager.post(
                ApiConstant.UPDATE_MERCHANT_TRANSACTION_LOG_URL,
                jsonRequest,
                true
            )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            prepareResponse(emitter, response)
        } else {
            if (response.code == "401")
                emitter.onError(PosException(PosError.DEVICE_ERROR_OPERATOR_LOGIN_REQUIRED))
            else
                emitter.onError(Exception("Error Code :: ${response.code}. ${response.message}"))
        }
        emitter.onComplete()
    }

    private fun prepareResponse(
        emitter: ObservableEmitter<MerchantTransactionLogResponse>,
        data: Any
    ) {
        val responseJson = Jsons.toJsonObj(data)
        val response = Jsons.fromJsonToObj(responseJson, MerchantTransactionLogResponse::class.java)
        emitter.onNext(response)
    }

    private fun onNext(response: MerchantTransactionLogResponse?) {
        if (response == null) {
            printLog(TAG,"Response received after pushing Merchant Transaction Log is null...",false)
            isLogPushInProgress = false
            return
        }
        clearMerchantTransactionLogTableBasedOnMaxCountForLogUpload()
    }

    private fun onError(throwable: Throwable) {
        isLogPushInProgress = false
        printLog(TAG,"Error Received While Pushing Merchant Transaction Log...")
        printLog(TAG,throwable.message.toString())
    }

    private fun clearMerchantTransactionLogTableBasedOnMaxCountForLogUpload() {
        compositeDisposable.add(
            localRepository.deleteMerchantTransactionLogsOfGivenCountFromInitial(
                MAX_TRANSACTIONS_COUNT_FOR_BATCH_PUSH
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    printLog(TAG,"$MAX_TRANSACTIONS_COUNT_FOR_BATCH_PUSH Merchant Transaction Logs Cleared From Database...")
                    isLogPushInProgress = false
                }, {
                    printLog(TAG,it.message.toString())
                    isLogPushInProgress = false
                })
        )
    }
}