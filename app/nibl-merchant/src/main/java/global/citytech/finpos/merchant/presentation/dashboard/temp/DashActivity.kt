package global.citytech.finpos.merchant.presentation.dashboard.temp

import android.app.Activity
import android.content.*
import android.os.Build
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.button.MaterialButton
import com.wajahatkarim3.roomexplorer.RoomExplorer
import global.citytech.common.Base64.encodeToString
import global.citytech.common.Constants
import global.citytech.common.data.PosCallback
import global.citytech.common.data.PosMessage
import global.citytech.common.extensions.hasInternetConnection
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.datasource.app.db.AppDatabase
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.databinding.ActivityLandingPageBinding
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.presentation.admin.AdminActivity
import global.citytech.finpos.merchant.presentation.admin.login.LoginDialog
import global.citytech.finpos.merchant.presentation.appnotification.AppNotificationActivity
import global.citytech.finpos.merchant.presentation.autoreversal.AutoReversalActivity
import global.citytech.finpos.merchant.presentation.autoreversal.AutoReversalActivity.Companion.EXTRA_ERROR_MESSAGE
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.command.CommandActivity
import global.citytech.finpos.merchant.presentation.config.ConfigActivity
import global.citytech.finpos.merchant.presentation.dashboard.billing.BillingBroadcastMessage
import global.citytech.finpos.merchant.presentation.dashboard.billing.BillingBroadcastMessageType.*
import global.citytech.finpos.merchant.presentation.dashboard.menu.*
import global.citytech.finpos.merchant.presentation.dashboard.screensaver.ScreenSaver
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.settlement.SettlementActivity
import global.citytech.finpos.merchant.service.ScreenSaverService
import global.citytech.finpos.merchant.utils.AppConstant
import global.citytech.finpos.merchant.utils.AppConstant.EXTRA_BILLING_BROADCAST_MESSAGE
import global.citytech.finpos.merchant.utils.AppConstant.EXTRA_MESSAGE_CASHIER_BUSY
import global.citytech.finpos.merchant.utils.AppConstant.EXTRA_TRANSACTION_MESSAGE
import global.citytech.finpos.merchant.utils.AppConstant.INTENT_ACTION_PAYMENT_SERVICE_APP_BILLING_BROADCAST
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finpos.merchant.utils.CashierLogManager
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.log.AppState
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.switches.posmode.PosMode
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.activity_landing_page.*
import kotlinx.android.synthetic.main.drawer.*
import kotlin.system.exitProcess

/**
 * Created by Unique Shakya on 6/15/2021.
 */
class DashActivity : AppBaseActivity<ActivityLandingPageBinding, DashViewModel>(),
    LoginDialog.LoginDialogListener, ScreenSaver.ScreenSaverDialogListener {

    private val logger = Logger(DashActivity::class.java.simpleName)
    private val TAG = DashActivity::class.java.name
    private lateinit var posMode: PosMode
    private lateinit var dashItems: DashItems
    private var clickedMenuItem: MenuItem? = null
    private lateinit var configurationItem: ConfigurationItem
    private var clickable = true
    private lateinit var loginDialog: LoginDialog
    private var screenSaverDialog: ScreenSaver? = null
    private var bankLogo: String = ""
    private var isFromCommandActivity = false

    private var localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    private var broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            printLog(TAG, "BroadCast ::: DashActivity ::: ".plus(intent))
            getViewModel().setScreenSaverObserverVariable(
                startScreenSaverTimer = false,
                showScreenSaver = false
            )
            if (intent?.action == Constants.INTENT_ACTION_POS_CALLBACK) {
                printLog(TAG, "BROADCAST ::: POS CALLBACK RECEIVED >>>")
                handlePosCallbackBroadcast(intent)
            } else if (intent?.action == "global.citytech.finpos.payment.service.PAYMENT_SERVICE_CONNECTION") {
                getViewModel().isLoading.value = false
                val serviceConnected = intent.getBooleanExtra(
                    "global.citytech.finpos.payment.service.PAYMENT_SERVICE_CONNECTED",
                    false
                )
                printLog(TAG, "PAYMENT SERVICE CONNECTED >> $serviceConnected")
                updatePaymentServiceConnectionButton(serviceConnected)
            } else if (intent?.action.equals(
                    INTENT_ACTION_PAYMENT_SERVICE_APP_BILLING_BROADCAST,
                    true
                )
            ) {
                printLog(TAG, "BroadCast Received From Payment Service App...")
                handleBroadCastFromPaymentServiceApp(intent!!)
            }
        }
    }

    private fun updatePaymentServiceConnectionButton(serviceConnected: Boolean) {
        val label = if (serviceConnected) getString(R.string.msg_payment_service_connected)
        else getString(R.string.msg_payment_service_disconnected)
        val iconResourceId =
            if (serviceConnected) R.drawable.ic_connected_24 else R.drawable.ic_disconnected_24
        val iconDrawable = ContextCompat.getDrawable(this, iconResourceId)
        btn_payment_service_connection.visibility = View.VISIBLE
        btn_payment_service_connection.text = label
        btn_payment_service_connection.icon = iconDrawable
        btn_payment_service_connection.iconGravity = MaterialButton.ICON_GRAVITY_START
        startScreenSaverService()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        observeNotificationObserver()
        observeForceSettlement()
        observePosMode()
        observeAutoReversalObserver()
        initLiveDataObservers()
        getViewModel().getPosMode()
        registerBroadCastReceivers()
        initLoginDialog()
        observeScreenSaver()
        observeSwitchConfigActive()
        observeAppResetActive()
    }

    private fun observeAppResetActive() {
        getViewModel().appResetActive.observe(this) {
            printLog(TAG, "Is App Reset Active $it")
            if (it && !getViewModel().isAppResetInProgress) {
                getViewModel().isAppResetInProgress = true
                startSettlementActivity(
                    isManualSettlement = false,
                    isSettlementConfirmationRequired = false
                )
            }
        }

        getViewModel().readyToStartTransactionLogPush.observe(this) {
            if (it) {
                getViewModel().pushTransactionLogIfFeasible()
            }
        }

        getViewModel().readyToResetDevice.observe(this) {
            if (it) {
                getViewModel().resetDevice()
            }
        }

        getViewModel().canCloseApplication.observe(this) {
            if (it) {
                Thread.sleep(2000)
                AppUtility.sendAppResetBroadcastToTMSPlatform(this)
                closeApplication()
            }
        }
    }

    private fun closeApplication() {
        finishAndRemoveTask()
        exitProcess(0)
    }

    private fun observeSwitchConfigActive() {
        getViewModel().switchConfigActive.observe(this) {
            if (it) {
                startConfigActivity()
            } else {
                getViewModel().checkForNotifications()
                startScreenSaverService()
            }
        }
    }

    private fun startConfigActivity() {
        val intent =
            ConfigActivity.getLaunchIntent(
                this,
                isTerminalConfigured = true,
                fromCommand = false
            )
        startActivityForResult(intent, ConfigActivity.REGISTER_REQUEST_CODE)
    }

    override fun onResume() {
        printLog(TAG, "DashActivity is Resumed...", true)
        super.onResume()
        if (!isFromCommandActivity) {
            getViewModel().checkForCommands()
            ScreenSaverService.start(this)
        }
        loadEnabledTransactionsForGivenPosMode()
        Thread {
            CashierLogManager.getInstance().initLog()
        }.start()
        getViewModel().checkForNotifications()
        getViewModel().disableHomeButton()
        (rv_dashboard.adapter as DashRecyclerAdapter?)?.startSlideCountDown()
    }

    private fun registerBroadCastReceivers() {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                broadcastReceiver,
                IntentFilter(Constants.INTENT_ACTION_POS_CALLBACK)
            )
        registerReceiver(
            broadcastReceiver,
            IntentFilter("global.citytech.finpos.payment.service.PAYMENT_SERVICE_CONNECTION")
        )
        registerReceiver(
            broadcastReceiver,
            IntentFilter(INTENT_ACTION_PAYMENT_SERVICE_APP_BILLING_BROADCAST)
        )
    }

    private fun handlePosCallbackBroadcast(intent: Intent) {
        val posCallback =
            intent.getSerializableExtra(Constants.KEY_BROADCAST_MESSAGE) as PosCallback
        printLog(TAG, "::: POS CALLBACK >>> ${posCallback.posMessage}")
        if (posCallback.posMessage == PosMessage.POS_MESSAGE_START_SETTLEMENT) {
            startSettlementActivity(
                isManualSettlement = false,
                isSettlementConfirmationRequired = true
            )
        } else if (posCallback.posMessage == PosMessage.POS_MESSAGE_START_COMMAND) {
            getViewModel().checkForCommands()
        } else if (posCallback.posMessage == PosMessage.POS_MESSAGE_START_RESET) {
            getViewModel().checkForAppResetActive()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            SettlementActivity.REQUEST_CODE -> {
                if (PreferenceManager.isAppResetActive(false)) {
                    getViewModel().enableTransactionLogPush()
                }
                getViewModel().checkForSwitchConfigurationActive()
                startScreenSaverService()
            }
            AutoReversalActivity.REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    getViewModel().checkSettlementTime()
                    startScreenSaverService()
                } else {
                    val errorMessage = data?.getStringExtra(EXTRA_ERROR_MESSAGE)
                    showAutoReversalErrorMessage(errorMessage)
                }
            }
            ConfigActivity.REGISTER_REQUEST_CODE -> {
                if (resultCode == RESULT_OK) {
                    getViewModel().checkForNotifications()
                } else {
                    getViewModel().disableSwitchConfigActiveForOneSecond()
                }
                startScreenSaverService()
            }
            CommandActivity.COMMAND_ACTIVITY_REQUEST_CODE -> {
                if (resultCode == CommandActivity.DASH_ACTIVITY_NEED_RESTART_RESULT_CODE) {
                    printLog(TAG, "Restarting Dash Activity...", true)
                    isFromCommandActivity = true
                    restartDashActivityBasedOnApiLevel22()
                } else {
                    startScreenSaverService()
                    printLog(TAG, "No need to restart Dash Activity...", true)
                }
            }
        }
    }

    private fun showAutoReversalErrorMessage(errorMessage: String?) {
        var displayMessage = if (errorMessage.isNullOrEmptyOrBlank())
            getString(R.string.error_msg_auto_reversal_not_performed)
        else {
            if (!application.hasInternetConnection())
                errorMessage
            else if (PosError.DEVICE_ERROR_TIMEOUT.errorMessage == errorMessage) {
              errorMessage
            } else {
                getString(R.string.internet_not_working_message)
            }
        }
        showNormalMessage(MessageConfig.Builder()
            .message(displayMessage)
            .positiveLabel(getString(R.string.title_ok))
            .onPositiveClick {
                startScreenSaverService()
            })
    }

    protected fun startAppNotificationsActivity() {
        val intent = Intent(this, AppNotificationActivity::class.java)
        startActivity(intent)
    }

    private fun handlePageItems() {
        initViews()
        getViewModel().getAppVersion()
        getViewModel().getTerminalConfiguredStatusAndProceed()
    }

    private fun initLiveDataObservers() {
        observeLogon()
        observeMessage()
        observeLoading()
        observeAppConfigurationLiveData()
        observeAppVersionLiveData()
        observeTerminalIsConfiguredLiveData()
        observeMenusLiveData()
        observeConnectionLiveData()
        observeSettlementActiveLiveData()
        observeAdminCredential()
        observeCommandList()
        observeBankDisplayImage()
        observeTerminalBannersLiveData()
        observeSettlementBatchNumberLiveData()
    }



    private fun observeTerminalBannersLiveData() {
        getViewModel().terminalBannersLiveData.observe(
            this,
            Observer {
                dashItems.advertisements = it
                rv_dashboard.adapter = DashRecyclerAdapter(
                    this,
                    dashItems
                )
                (rv_dashboard.adapter as DashRecyclerAdapter).notifyDataSetChanged()
            }
        )
    }

    private fun observeSettlementBatchNumberLiveData() {
        getViewModel().settlementBatchNumber.observe(
            this,
            Observer {
                proceedBasedOnTheSettlementBatchNumber(it)
            }
        )
    }

    private fun proceedBasedOnTheSettlementBatchNumber(batchNumber: String) {
        if (batchNumber.isNullOrEmptyOrBlank()) {
            showNormalMessage(
                MessageConfig.Builder()
                    .message(getString(R.string.msg_unable_to_initiate_transaction))
                    .positiveLabel(getString(R.string.title_ok))
                    .onPositiveClick { }
            )
            return
        }
        val limitReached = getViewModel().isSettlementBatchLimitReached(batchNumber)
        if (!limitReached) {
            proceedWithMenuItemMenuHandler(clickedMenuItem!!)
            return
        }
        showNormalMessage(
            MessageConfig.Builder()
                .message(getString(R.string.msg_max_settlement_batch_reached))
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick { }
        )
    }

    private fun observeBankDisplayImage() {
        getViewModel().bankDisplayImage.observe(this, Observer {
            this.bankLogo = encodeToString(it, false)
            Glide.with(this).load(it).into(iv_bank_logo)
        })

        getViewModel().defaultBankDisplayImage.observe(this, Observer {
            if (it)
                iv_bank_logo.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_full_finpos_logo
                    )
                )
        })
    }

    private fun observeCommandList() {
        getViewModel().commandsPresent.observe(this, Observer {
            if (it) {
                printLog(TAG, "::: COMMANDS PRESENT >>>", true)
                startCommandsActivityIfInternetConnection()
            }
        })
    }

    private fun startCommandsActivityIfInternetConnection() {
        if (NiblMerchant.INSTANCE.hasInternetConnection()) {
            ScreenSaverService.stop(this)
            Intent(this, CommandActivity::class.java).also {
                startActivityForResult(it, CommandActivity.COMMAND_ACTIVITY_REQUEST_CODE)
            }
        }
    }

    private fun observeAdminCredential() {
        getViewModel().adminCredential.observe(this, Observer {
            showAdminLoginDialog(getString(R.string.title_enter_admin_password), it)
        })
    }

    private fun showAdminLoginDialog(pageTitle: String, validCredential: String?) {
        getViewModel().setScreenSaverObserverVariable(
            startScreenSaverTimer = false,
            showScreenSaver = false
        )

        this.loginDialog = LoginDialog.newInstance(
            pageTitle
            = pageTitle,
            validCredential = validCredential
        )
        this.loginDialog.isCancelable = false
        this.loginDialog.show(
            supportFragmentManager,
            LoginDialog.TAG
        )
    }

    private fun observeSettlementActiveLiveData() {
        getViewModel().settlementActive.observe(this, Observer {
            if (it) {
                showStartSettlementConfirmationDialog()
                startScreenSaverService()
            } else
                getViewModel().getSettlementBatchNumber()
        })
    }

    private fun showStartSettlementConfirmationDialog() {
        showConfirmationMessage(MessageConfig.Builder()
            .title(getString(R.string.msg_settlement_confirmation))
            .message(getString(R.string.msg_settlement_active))
            .positiveLabel(getString(R.string.action_confirm))
            .onPositiveClick {
                startSettlementActivity(
                    isManualSettlement = false,
                    isSettlementConfirmationRequired = true
                )
            }
            .negativeLabel(getString(R.string.action_cancel))
            .onNegativeClick {
                getViewModel().addSettlementPendingNotification()
                startScreenSaverService()
            })
    }

    private fun observeConnectionLiveData() {
        getViewModel().connectionAvailable.observe(this, Observer {
            if (it)
                getViewModel().checkForAutoReversal()
            else
                displayNoConnectionMessage()
        })
    }

    private fun displayNoConnectionMessage() {
        showFailureMessage(MessageConfig.Builder()
            .message(getString(R.string.error_msg_no_connection))
            .positiveLabel(getString(R.string.title_ok))
            .onPositiveClick {})
    }

    private fun observeMessage() {
        getViewModel().message.observe(this, Observer {
            if (drawer_dashboard.isDrawerOpen(GravityCompat.START)) {
                tv_message.visibility = View.VISIBLE
                tv_message.text = it
            } else {
                tv_message.visibility = View.GONE
            }
        })
    }

    private fun observeLoading() {
        getViewModel().isLoading.observe(this, Observer {
            clickable = !it
            btn_log_on.isEnabled = !it
            btn_payment_service_connection.isEnabled = !it
            if (drawer_dashboard.isDrawerOpen(GravityCompat.START)) {
                if (it) {
                    tv_message.visibility = View.GONE
                    progress_bar_log_on.visibility = View.VISIBLE
                    progress_bar.visibility = View.VISIBLE
                } else {
                    progress_bar_log_on.visibility = View.INVISIBLE
                    progress_bar.visibility = View.INVISIBLE
                }
            } else {
                if (it) {
                    progress_bar.visibility = View.VISIBLE
                    progress_bar_log_on.visibility = View.VISIBLE
                } else {
                    progress_bar_log_on.visibility = View.INVISIBLE
                    progress_bar.visibility = View.INVISIBLE
                }
            }
        })
    }

    private fun observeLogon() {
        getViewModel().logonResponse.observe(this, Observer {
            progress_bar_log_on.visibility = View.INVISIBLE
            if (drawer_dashboard.isDrawerOpen(GravityCompat.START)) {
                iv_logon_status.visibility = View.VISIBLE
            } else {
                iv_logon_status.visibility = View.INVISIBLE
            }
            if (it) {
                NiblMerchant.INSTANCE.networkConnectionAvailable.value = it
                iv_logon_status.setImageResource(R.drawable.ic_check_circle_green_64dp)
                tv_message.setTextColor(ContextCompat.getColor(this, R.color.green))
            } else {
                iv_logon_status.setImageResource(R.drawable.ic_failure_red_64dp)
                tv_message.setTextColor(ContextCompat.getColor(this, R.color.red))
            }
            if (!drawer_dashboard.isDrawerOpen(GravityCompat.START)) {
                showToast(getViewModel().message.value, Toast.LENGTH_SHORT)
            }
        })

    }

    private fun observeAutoReversalObserver() {
        getViewModel().autoReversalPresent.observe(this, Observer {
            if (it)
                startAutoReversalActivity()
            else
                getViewModel().checkSettlementTime()
        })
    }

    private fun initLoginDialog() {
        this.loginDialog = LoginDialog.newInstance(
            pageTitle = getString(R.string.title_enter_admin_password),
            validCredential = ""
        )
        this.loginDialog.isCancelable = false
    }

    private fun initScreenSaverDialog(bankLogo: String) {
        this.screenSaverDialog = ScreenSaver.newInstance(bankLogo)
    }

    private fun observeScreenSaver() {
        NiblMerchant.showScreenSaver.observe(this, Observer {
            if (it) {
                showScreenSaver()
            } else {
                hideScreenSaver()
            }

        })

    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (ev?.action?.equals(MotionEvent.ACTION_UP) == true) {
            logger.log("DashActivity dispatchTouchEvent........")
            startScreenSaverService()
        }
        return super.dispatchTouchEvent(ev)
    }

    private fun showScreenSaver() {
        hideScreenSaver()
        this.screenSaverDialog = ScreenSaver.newInstance(bankLogo)
        this.screenSaverDialog?.show(
            supportFragmentManager,
            ScreenSaver.TAG
        )
    }

    private fun hideScreenSaver() {
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()

        val prev: Fragment? =
            supportFragmentManager.findFragmentByTag(ScreenSaver.TAG)
        if (prev != null) {
            ft.remove(prev)
            if (screenSaverDialog != null) {
                screenSaverDialog?.dismiss()
            }
        }
        // ft.addToBackStack(null)

    }

    private fun startAutoReversalActivity() {
        val intent = Intent(this, AutoReversalActivity::class.java)
        startActivityForResult(intent, AutoReversalActivity.REQUEST_CODE)
    }

    private fun proceedWithMenuItemMenuHandler(menuItem: MenuItem) {
        menuItem.menuHandler.loadMenuItemPage(this as Activity)
    }

    private fun observeForceSettlement() {
        getViewModel().startSettlement.observe(this, Observer {
            if (it) {
                startSettlementActivity(
                    isManualSettlement = false,
                    isSettlementConfirmationRequired = true
                )
            } else {
                getViewModel().checkForSwitchConfigurationActive()
            }
        })
    }

    public fun startSettlementActivity(
        isManualSettlement: Boolean,
        isSettlementConfirmationRequired: Boolean
    ) {
        SettlementActivity.getLaunchIntent(
            this,
            isManualSettlement = isManualSettlement,
            isSettlementSuccessConfirmationRequired = isSettlementConfirmationRequired
        )
    }

    private fun observePosMode() {
        getViewModel().startDashboard.observe(this, Observer {
            posMode = it
            handlePageItems()
        })
    }


    private fun observeNotificationObserver() {
        getViewModel().noNotifications.observe(this, Observer {
            makeNotificationIconVisible(it)
        })
    }


    private fun observeAppConfigurationLiveData() {
        getViewModel().appConfiguration.observe(
            this,
            Observer {
                handleConfig(it)
            }
        )
    }

    private fun handleConfig(configurationItem: ConfigurationItem?) {
        this.configurationItem = configurationItem!!
        this.updateReadyAndTerminalIdInTextViews(configurationItem)
    }

    private fun loadEnabledTransactionsForGivenPosMode() {
        this.getViewModel().loadEnabledTransactionsForGivenPosMode(posMode)
    }

    private fun observeAppVersionLiveData() {
        getViewModel().appVersion.observe(
            this,
            Observer {
                updateAppVersionTextView(it)
            }
        )
    }

    private fun observeTerminalIsConfiguredLiveData() {
        getViewModel().terminalIsConfigured.observe(
            this,
            Observer { onObserveTerminalIsConfigured(it) }
        )
    }

    private fun onObserveTerminalIsConfigured(terminalIsConfigured: Boolean) {
        printLog(
            TAG,
            "onObserveTerminalIsConfigured ::: Terminal is configured? ::: ".plus(
                terminalIsConfigured
            )
        )
        if (terminalIsConfigured) {
            onTerminalIsConfigured()
        } else {
            onTerminalIsNotConfigured()
        }
    }

    private fun onTerminalIsConfigured() {
        printLog(TAG, "Terminal is configured...", true)
        this.getViewModel().getConfiguration()
    }

    private fun onTerminalIsNotConfigured() {
        this.logger.log("Terminal is not configured...")
        this.showNotConfiguredDialog()
    }

    private fun showNotConfiguredDialog() {
        showFailureMessage(
            MessageConfig.Builder()
                .message(getString(R.string.msg_not_configured))
                .positiveLabel("OK")
                .onPositiveClick {
                    startAdminActivity(false)
                })
    }

    private fun startAdminActivity(isTerminalConfigured: Boolean) {
        val intent = AdminActivity.getLaunchIntent(this, isTerminalConfigured)
        startActivity(intent)
        if (!isTerminalConfigured)
            finish()
    }

    private fun observeMenusLiveData() {
        this.getViewModel().menusMutableLiveData.observe(
            this,
            Observer { updateMenuRecyclerView(it) }
        )
    }

    private fun showForceSettlementMessage(message: String?) {
        if (!message.isNullOrEmptyOrBlank()) {
            val messageConfig = MessageConfig.Builder()
                .message(message)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {}
            showNormalMessage(messageConfig)
        }
    }

    fun initViews() {
        tv_application_version.setOnClickListener {
            if (BuildConfig.DEBUG || AppState.getInstance().isDebug) {
                RoomExplorer.show(this, AppDatabase::class.java, "nibl_db.db")
            }
        }

        img_btn_menu.setOnClickListener {
            drawer_dashboard.openDrawer(GravityCompat.START)
        }

        rv_dashboard.layoutManager = LinearLayoutManager(this)
        dashItems = DashItems(
            primaryTransaction = retrievePrimaryTransaction(),
            primaryMenuAdapterListener = object : MenuAdapter.MenuAdapterListener {
                override fun onMenuAdapterItemClicked(menuItem: MenuItem) {
                    if (clickable) {

                        clickedMenuItem = menuItem
                        getViewModel().checkForAutoReversal()
                    }
                }
            }
        )

        rv_drawer_menu.layoutManager = LinearLayoutManager(this)
        val drawerMenuItems = mutableListOf(
            DrawerMenuItem(
                R.drawable.ic_baseline_qr_code_scanner_24,
                "My QR Code",
                MerchantQrMenuHandler()
            ),
            DrawerMenuItem(
                R.drawable.ic_mini_statement,
                "Transactions",
                TransactionMenuHandler()
            ),
            DrawerMenuItem(R.drawable.ic_settlement, "Settlement", SettlementHandler()),
            DrawerMenuItem(
                R.drawable.ic_merchant_icon,
                "Merchant Menu",
               MerchantMenuHandler()
            ),
            DrawerMenuItem(R.drawable.ic_admin_menu_icon, "Admin Menu", AdminMenuHandler()),
            DrawerMenuItem(
                R.drawable.ic_baseline_local_phone_24,
                "Support",
                SupportMenuHandler()
            )
        )
        rv_drawer_menu.adapter = DrawerMenuAdapter(this, drawerMenuItems)

        iv_app_notification.setOnClickListener {
            startAppNotificationsActivity()
        }

        iv_drawer_back.setOnClickListener {
            closeDrawer()
        }

        drawer_dashboard.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerStateChanged(newState: Int) {

            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
            }

            override fun onDrawerClosed(drawerView: View) {
                tv_message.visibility = View.GONE
                iv_logon_status.visibility = View.GONE
                rv_drawer_menu.scrollToPosition(0)
            }

            override fun onDrawerOpened(drawerView: View) {
            }

        })

        iv_scheme.setImageResource(R.drawable.card_schemes)

        btn_log_on.setOnClickListener {
            getViewModel().logOn()
        }

        btn_payment_service_connection.setOnClickListener {
            getViewModel().isLoading.value = true
            val intent = Intent()
            intent.component = ComponentName(
                "global.citytech.finpos.payment.service",
                "global.citytech.finpos.payment.service.activity.ConnectionActivity"
            )
            startActivity(intent)
        }
    }

    private fun checkConnectionWithHostAndAutoReversal() {
        getViewModel().checkConnectionWithHost()
    }

    private fun retrievePrimaryTransaction(): MenuItem {
        return if (posMode == PosMode.RETAILER)
            MenuItem(Menu.SALES, PurchaseHandler())
        else
            MenuItem(Menu.CASH_ADVANCE, CashAdvanceMenuHandler())
    }

    private fun retrieveAdvertisements(): MutableList<Int> {
        val advertisements = mutableListOf<Int>()
        advertisements.add(R.drawable.finpos_banner_wallpaper)
        return advertisements
    }

    fun makeNotificationIconVisible(noNotifications: Boolean) {
        if (noNotifications)
            iv_app_notification.visibility = View.GONE
        else
            iv_app_notification.visibility = View.VISIBLE
    }

    private fun updateMenuRecyclerView(menus: List<MenuItem>) {
        dashItems.menus = menus.toMutableList()
        dashItems.menusAdapterListener = object : MenuAdapter.MenuAdapterListener {
            override fun onMenuAdapterItemClicked(menuItem: MenuItem) {
                if (clickable) {
                    clickedMenuItem = menuItem
                    if (nonCardTransactionMenuItem(clickedMenuItem!!))
                        proceedWithMenuItemMenuHandler(clickedMenuItem!!)
                    else
                        getViewModel().checkForAutoReversal()
                }
            }
        }
        getViewModel().getBannersForDisplay()
    }

    private fun nonCardTransactionMenuItem(clickedMenuItem: MenuItem): Boolean {
        return clickedMenuItem.menu == Menu.ADMIN_MENU || clickedMenuItem.menu == Menu.MERCHANT_MENU
                || clickedMenuItem.menu == Menu.TRANSACTIONS || clickedMenuItem.menu == Menu.SUPPORT
                || clickedMenuItem.menu == Menu.KEY_EXCHANGE || clickedMenuItem.menu == Menu.QR_CODE
                || clickedMenuItem.menu ==  Menu.PIN_CHANGE || clickedMenuItem.menu == Menu.GREEN_PIN



    }

    fun updateReadyAndTerminalIdInTextViews(configurationItem: ConfigurationItem?) {
        tv_merchant_name.text = configurationItem?.merchants!![0].name
        tv_merchant_address.text = configurationItem.merchants!![0].address
        tv_merchant_id.text = "Terminal ID : ${configurationItem.merchants!![0].terminalId}"
    }

    fun updateAppVersionTextView(appVersion: String) {
        tv_application_version.text = appVersion
    }

    override fun onBackPressed() {
        //disabled in Dashboard
        if (drawer_dashboard.isDrawerOpen(GravityCompat.START)) {
            drawer_dashboard.closeDrawer(GravityCompat.START)
        }
    }


    override fun getBindingVariable() = BR.viewModel

    override fun getLayout() = R.layout.activity_landing_page

    override fun getViewModel() =
        ViewModelProviders.of(this)[DashViewModel::class.java]

    private fun closeDrawer() {
        drawer_dashboard.closeDrawer(GravityCompat.START)
    }

    override fun onPause() {
        printLog(TAG, "DashActivity is Paused...", true)
        ScreenSaverService.stop(this)
        getViewModel().setScreenSaverObserverVariable(
            startScreenSaverTimer = false,
            showScreenSaver = false
        )
        (rv_dashboard.adapter as DashRecyclerAdapter?)?.stopSlideCountDown()
        super.onPause()
    }

    override fun onDestroy() {
        printLog(TAG, "onDestroy............", true)
        (rv_dashboard.adapter as DashRecyclerAdapter?)?.onDestroy()
        super.onDestroy()
        hideScreenSaver()
        screenSaverDialog = null
        ScreenSaverService.stop(this)
        unregisterBroadCastReceivers()
    }

    private fun unregisterBroadCastReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        unregisterReceiver(broadcastReceiver)
    }

    fun handleDrawerMenuItemClicked(drawerMenuItem: DrawerMenuItem) {
        if (!getViewModel().isLoading.value!!) {
            closeDrawer()
            drawerMenuItem.menuHandler.loadMenuItemPage(this)
        }
    }

    override fun onLoginDialogCorrectCredentialEntered() {
        hideAdminLoginDialog()
        startAdminActivity(true)
    }

    override fun onLoginDialogCancelButtonClicked() {
        startScreenSaverService()
        hideAdminLoginDialog()
    }

    private fun hideAdminLoginDialog() {
        this.loginDialog.dismiss()
        printLog(TAG, "Admin Dialog Dismissed...")
    }

    private fun handleBroadCastFromPaymentServiceApp(intent: Intent) {
        val billingBroadCastMessageJson =
            intent.getStringExtra(EXTRA_BILLING_BROADCAST_MESSAGE)
        billingBroadCastMessageJson?.let {
            try {
                val billingBroadcastMessage =
                    global.citytech.finposframework.utility.JsonUtils.fromJsonToObj(
                        billingBroadCastMessageJson,
                        BillingBroadcastMessage::class.java
                    )
                if (isCashierBusyWhilePurchaseOrVoidRequest(billingBroadcastMessage.messageType)) {
                    printLog(TAG, "Cashier is Busy...")
                    sendCashierBusyBroadCast(billingBroadcastMessage.messageType)
                    return@let
                }
                proceedBasedOnBillingBroadcastMessageType(billingBroadcastMessage)
            } catch (exception: Exception) {
                exception.printStackTrace()
                printLog(TAG, "Exception in handling payment...")
                sendCashierBusyBroadCast("")
            }
        }
    }

    private fun isCashierBusyWhilePurchaseOrVoidRequest(msgType: String): Boolean {
        if (isPurchaseOrVoid(msgType) and isDashActivityNotVisible()) {
            return true
        }
        return false
    }

    private fun isPurchaseOrVoid(msgType: String): Boolean {
        return (msgType.equals(PURCHASE.name, true) or
                msgType.equals(VOID.name, true))
    }

    private fun proceedBasedOnBillingBroadcastMessageType(
        billingBroadcastMessage: BillingBroadcastMessage
    ) {
        when (billingBroadcastMessage.messageType) {
            BILLING_SYSTEM_CONNECTED.name -> updatePaymentServiceConnectionButton(true)
            BILLING_SYSTEM_DISCONNECTED.name -> updatePaymentServiceConnectionButton(false)
            else -> startPaymentServiceActivityBasedOnBroadcastMessage(
                billingBroadcastMessage
            )
        }
    }

    private fun startPaymentServiceActivityBasedOnBroadcastMessage(
        billingBroadcastMessage: BillingBroadcastMessage
    ) {
        printLog(
            TAG,
            "Billing BroadCast Message Type::: ".plus(billingBroadcastMessage.messageType)
        )
        printLog(TAG, "Transaction Page Path ::: ".plus(billingBroadcastMessage.transactionPage))
        Intent().also {
            it.component = ComponentName(
                AppConstant.BILLING_TRANSACTION_TARGETED_PACKAGE,
                billingBroadcastMessage.transactionPage
            )
            it.putExtra(
                EXTRA_TRANSACTION_MESSAGE,
                billingBroadcastMessage.transactionMessage
            )
            startActivity(it)
        }
    }

    private fun sendCashierBusyBroadCast(messageType: String) {
        Intent().also {
            it.action = AppConstant.ACTION_CASHIER_BUSY_MESSAGE
            it.putExtra(EXTRA_MESSAGE_CASHIER_BUSY, messageType)
            NiblMerchant.INSTANCE.sendBroadcast(it)
        }
    }

    private fun isDashActivityNotVisible(): Boolean {
        if (NiblMerchant.INSTANCE.isDashActivityVisible()) {
            return false
        }
        return true
    }

    override fun onTouch() {
        startScreenSaverService()
    }

    private fun restartDashActivityBasedOnApiLevel22() {
        viewModelStore.clear()
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            logger.debug("Api level 22 or less (🍭🍭🍭)...")
            ActivityCompat.recreate(this)
        } else {
            logger.debug("Api level > 22...")
            recreate()
        }
    }

    private fun startScreenSaverService() {
        getViewModel().setScreenSaverObserverVariable(
            startScreenSaverTimer = true,
            showScreenSaver = false
        )
    }
}