package global.citytech.finposframework.hardware.io.cards.detect

import global.citytech.finposframework.hardware.io.cards.CardType

/**
 * Created by Rishav Chudal on 4/7/21.
 */
open class DetectCardRequest constructor(
    val slotsToOpen: List<CardType>,
    val timeOutInSecs: Int?
)