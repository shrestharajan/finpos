package global.citytech.finpos.merchant.framework.datasource.core.receipt

import global.citytech.common.extensions.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.receipt.DuplicateReceiptDataSource
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.domain.model.receipt.duplicate.DuplicateReceiptRequestEntity
import global.citytech.finpos.merchant.domain.model.receipt.duplicate.DuplicateReceiptResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.presentation.data.mapToDuplicateReceiptResponseUiModel
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finposframework.switches.ProcessorManager
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 12/15/2020.
 */
class DuplicateReceiptDataSourceImpl : DuplicateReceiptDataSource {
    override fun print(stan: String?): Observable<DuplicateReceiptResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val transactionRepository =
            TransactionRepositoryImpl(
                NiblMerchant.INSTANCE
            )
        val printerService = PrinterSourceImpl(NiblMerchant.INSTANCE)
        val duplicateReceiptRequestEntity =
            if (stan.isNullOrEmptyOrBlank())
                DuplicateReceiptRequestEntity(transactionRepository, printerService)
            else
                DuplicateReceiptRequestEntity(transactionRepository, printerService, stan)

        val duplicateReceiptRequester =
            ProcessorManager.getInterface(null, NotificationHandler).duplicateReceiptRequester
        return Observable.fromCallable {
            (duplicateReceiptRequester.execute(duplicateReceiptRequestEntity.mapToModel())).mapToDuplicateReceiptResponseUiModel()
        }
    }
}