package global.citytech.finpos.processor.nibl.reconciliation;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finpos.nibl.iso8583.requestsender.settlement.SettlementRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.settlement.SettlementRequestSender;
import global.citytech.finpos.processor.nibl.batchupload.BatchUploadRequestModel;
import global.citytech.finpos.processor.nibl.batchupload.BatchUploadResponseModel;
import global.citytech.finpos.processor.nibl.batchupload.BatchUploadUseCase;
import global.citytech.finpos.processor.nibl.logon.LogOnRequestModel;
import global.citytech.finpos.processor.nibl.logon.MerchantLogOnUseCase;
import global.citytech.finpos.processor.nibl.posmode.PosModeRequestModel;
import global.citytech.finpos.processor.nibl.posmode.PosModeResponseModel;
import global.citytech.finpos.processor.nibl.posmode.PosModeUseCase;
import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportRequestModel;
import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportUseCase;
import global.citytech.finpos.processor.nibl.transaction.receipt.NiblIsoMessageReceiptHandler;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.SettlementStatus;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reconciliation.ReconciliationRequester;
import global.citytech.finposframework.usecases.activity.ActivityLog;
import global.citytech.finposframework.usecases.activity.ActivityLogStatus;
import global.citytech.finposframework.usecases.activity.ActivityLogType;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationHandler;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationTotals;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.JsonUtils;
import global.citytech.finposframework.utility.StackTraceUtil;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Unique Shakya on 9/22/2020.
 * Updated by Bikash Shrestha on 11/09/2021
 */
public class ReconciliationUseCase implements UseCase<ReconciliationRequestModel, ReconciliationResponseModel>,
        ReconciliationRequester<ReconciliationRequestModel, ReconciliationResponseModel> {

    private final boolean[] isConfirm = {false};
    private IsoMessageResponse reconciliationIsoMessageResponse;
    private SettlementRequest settlementRequest;
    private ReconciliationRequestModel reconciliationRequestModel;
    private TerminalRepository terminalRepository;
    private Notifier notifier;
    private Logger logger;
    private CountDownLatch countDownLatch;

    public ReconciliationUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        this.terminalRepository = terminalRepository;
        this.notifier = notifier;
        this.logger = new Logger(ReconciliationUseCase.class.getName());
    }

    @Override
    public ReconciliationResponseModel execute(ReconciliationRequestModel request) {
        logger.debug("::: REACHED RECONCILIATION USE CASE :::");
        this.reconciliationRequestModel = request;
        String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
        if (request.getReconciliationRepository().getTransactionCountByBatchNumber(batchNumber) == 0)
            return ReconciliationResponseModel.Builder.newInstance()
                    .withMessage("No need for settlement")
                    .withIsSuccess(true)
                    .build();
        RequestContext requestContext = null;
        try {
            this.notifier.notify(Notifier.EventType.PREPARING_RECONCILIATION_TOTALS, Notifier.EventType.PREPARING_RECONCILIATION_TOTALS.getDescription());
            requestContext = this.prepareRequestContext(batchNumber, request.getReconciliationRepository(),
                    request.getApplicationRepository());
            return performSettlement(batchNumber, requestContext, request);
        } catch (PosException pe) {
            if (pe.getPosError() == PosError.DEVICE_ERROR_ZERO_AMOUNT_SETTLEMENT)
                return ReconciliationResponseModel.Builder.newInstance()
                        .withMessage("No need for settlement")
                        .withIsSuccess(true)
                        .build();
            else {
                throw pe;
            }
        } catch (FinPosException fe) {
            fe.printStackTrace();
            if (fe.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT ||
                    fe.getType() == FinPosException.ExceptionType.READ_TIME_OUT ||
                    fe.getType() == FinPosException.ExceptionType.CONNECTION_ERROR) {
                throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
            } else {
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
            }
        } catch (Exception e) {
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
    }

    private void updateParametersWhenSettlementSuccess(String batchNumber, ReconciliationRequestModel request) {
        this.terminalRepository.incrementReconciliationBatchNumber();
        request.getReconciliationRepository().updateBatch(batchNumber, "", "");
        request.getReconciliationRepository().saveSettlementStatus(SettlementStatus.OPEN);
    }

    private ReconciliationResponseModel performSettlement(String batchNumber, RequestContext requestContext,
                                                          ReconciliationRequestModel request) {
        boolean isReconciliationSuccess;
        SettlementRequestSender settlementRequestSender = null;
        try {
            this.notifier.notify(Notifier.EventType.CONNECTING_TO_SWITCH, Notifier.EventType.CONNECTING_TO_SWITCH.getDescription());
            settlementRequestSender = new SettlementRequestSender(new NIBLSpecInfo(),
                    requestContext, false);
            reconciliationIsoMessageResponse = settlementRequestSender.send(false);
            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait...");
            isReconciliationSuccess = IsoMessageUtils.retrieveFromDataElementsAsInt(reconciliationIsoMessageResponse, 39) == 0;
            if (!isReconciliationSuccess && terminalRepository.findTerminalInfo().isDebugModeEnabled()) {
                this.printReconciliationReceipt(requestContext, reconciliationIsoMessageResponse, request.getPrinterService(),
                        request.getReconciliationRepository(), false);
            }
        } catch (FinPosException fe) {
            updateFailedActivityLog(ActivityLogType.SETTLEMENT, requestContext, fe);
            closeSocket(settlementRequestSender);
            if (fe.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT ||
                    fe.getType() == FinPosException.ExceptionType.WRITE_EXCEPTION) {
                throw new PosException(PosError.DEVICE_ERROR_SETTLEMENT_REQUEST_CONNECTION_ERROR);
            } else {
                throw fe;
            }
        } catch (PosException pe) {
            updateFailedActivityLog(ActivityLogType.SETTLEMENT, requestContext, pe.getLocalizedMessage());
            closeSocket(settlementRequestSender);
            throw pe;
        } catch (Exception e) {
            updateFailedActivityLog(ActivityLogType.SETTLEMENT, requestContext, StackTraceUtil.getStackTraceAsString(e));
            closeSocket(settlementRequestSender);
            throw e;
        }
        updateActivityLog(ActivityLogType.SETTLEMENT, requestContext, isReconciliationSuccess);
        this.terminalRepository.incrementSystemTraceAuditNumber();
        if (IsoMessageUtils.retrieveFromDataElementsAsInt(reconciliationIsoMessageResponse, 39) == 95) {
            isReconciliationSuccess = performBatchUpload(request);
            if (isReconciliationSuccess) {
                isReconciliationSuccess = performSettlementClosure(request);
            }
        }
        if (isReconciliationSuccess) {
            this.notifier.notify(Notifier.EventType.SETTLEMENT_SUCCESS_EVENT, "Settlement Success");
            this.printDetailReport(this.terminalRepository, request, this.notifier);
            this.printReconciliationReceipt(requestContext, reconciliationIsoMessageResponse, request.getPrinterService(),
                    request.getReconciliationRepository(), true);
            updateParametersWhenSettlementSuccess(batchNumber, request);
            this.performMerchantLogon(request);
        }
        closeSocket(settlementRequestSender);
        return ReconciliationResponseModel.Builder.newInstance()
                .withIsSuccess(isReconciliationSuccess)
                .withMessage(this.prepareMessage(isReconciliationSuccess))
                .build();
    }

    private void printDetailReport(TerminalRepository terminalRepository, ReconciliationRequestModel request, Notifier notifier) {
        DetailReportUseCase detailReportUseCase = new DetailReportUseCase(terminalRepository, notifier);
        detailReportUseCase.execute(DetailReportRequestModel.Builder.newInstance()
                .printSummaryReport(false)
                .applicationRepository(request.getApplicationRepository())
                .printerService(request.getPrinterService())
                .reconciliationRepository(request.getReconciliationRepository())
                .transactionRepository(request.getTransactionRepository())
                .build());
    }

    private void closeSocket(SettlementRequestSender settlementRequestSender) {
        if (settlementRequestSender != null) {
            settlementRequestSender.closeTcpClient();
        }
    }

    private boolean performBatchUpload(ReconciliationRequestModel request) {
        BatchUploadUseCase batchUploadUseCase = new BatchUploadUseCase(this.terminalRepository, this.notifier);
        BatchUploadResponseModel batchUploadResponseModel = batchUploadUseCase
                .execute(new BatchUploadRequestModel(request.getTransactionRepository(),
                        request.getPrinterService()));
        return batchUploadResponseModel.getResult() == Result.SUCCESS;
    }

    private void performMerchantLogon(ReconciliationRequestModel request) {
        try {
            MerchantLogOnUseCase merchantLogOnUseCase = new MerchantLogOnUseCase(this.terminalRepository);
            merchantLogOnUseCase.execute(prepareLogOnRequestModel(request));
        } catch (FinPosException fe) {
            fe.printStackTrace();
            this.feedPaper();
        }
    }

    private void feedPaper() {
        reconciliationRequestModel.getPrinterService().feedPaper(5);
    }

    private boolean performSettlementClosure(ReconciliationRequestModel request) {
        RequestContext requestContext = null;
        try {
            HostInfo primaryHostInfo = this.terminalRepository.findPrimaryHost();
            HostInfo secondaryHostInfo = this.terminalRepository.findSecondaryHost();
            TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
            ConnectionParam connectionParam = new ConnectionParam(primaryHostInfo, secondaryHostInfo);
            requestContext = new RequestContext(this.terminalRepository.getSystemTraceAuditNumber(),
                    terminalInfo, connectionParam);
            requestContext.setRequest(this.settlementRequest);
            boolean isReconciliationSuccessAfterBatch = executeReconciliation(requestContext, true);
            this.terminalRepository.incrementSystemTraceAuditNumber();
            updateActivityLog(ActivityLogType.SETTLEMENT_CLOSURE, requestContext, isReconciliationSuccessAfterBatch);
            this.logger.log(":: RECONCILIATION AFTER BATCH :: RESULT: " + isReconciliationSuccessAfterBatch);
            return isReconciliationSuccessAfterBatch;
        } catch (FinPosException fe) {
            updateFailedActivityLog(ActivityLogType.SETTLEMENT_CLOSURE, requestContext, fe);
            fe.printStackTrace();
            if (fe.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT ||
                    fe.getType() == FinPosException.ExceptionType.READ_TIME_OUT ||
                    fe.getType() == FinPosException.ExceptionType.CONNECTION_ERROR) {
                throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
            } else {
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
            }
        } catch (Exception e) {
            updateFailedActivityLog(ActivityLogType.SETTLEMENT_CLOSURE, requestContext, StackTraceUtil.getStackTraceAsString(e));
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
    }

    private boolean executeReconciliation(RequestContext requestContext, boolean isSettlementClosure) {
        this.notifier.notify(Notifier.EventType.CONNECTING_TO_SWITCH, Notifier.EventType.CONNECTING_TO_SWITCH.getDescription());
        SettlementRequestSender settlementRequestSender = new SettlementRequestSender(new NIBLSpecInfo(),
                requestContext, isSettlementClosure);
        reconciliationIsoMessageResponse = settlementRequestSender.send(isSettlementClosure);
        this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait...");
        return IsoMessageUtils.retrieveFromDataElementsAsInt(reconciliationIsoMessageResponse, 39) == 0;
    }

    private void printReconciliationReceipt(RequestContext requestContext, IsoMessageResponse isoMessageResponse,
                                            PrinterService printerService, ReconciliationRepository reconciliationRepository,
                                            boolean isReconciliationSuccess) {
        this.notifier.notify(
                Notifier.EventType.PRINTING_SETTLEMENT_REPORT,
                Notifier.EventType.PRINTING_SETTLEMENT_REPORT.getDescription()
        );
        this.printDebugReceiptIfDebugMode(printerService, isoMessageResponse);
        if (isReconciliationSuccess) {
            String merchantCategoryCode = reconciliationRequestModel.getApplicationRepository().retrieveEmvParameterRequest().getMerchantCategoryCode();
            PosModeUseCase posModeUseCase = new PosModeUseCase();
            PosModeResponseModel posModeResponseModel = posModeUseCase.execute(new PosModeRequestModel(merchantCategoryCode));
            NiblReconciliationReceipt niblReconciliationReceipt = new
                    NiblReconciliationReceiptMaker(requestContext, isoMessageResponse).prepare(posModeResponseModel.getPosMode());
            String reconciliationReceiptString = JsonUtils.toJsonObj(niblReconciliationReceipt);
            reconciliationRepository.updateReconciliationReceipt(reconciliationReceiptString);
            NiblReconciliationReceiptPrintHandler reconciliationReceiptPrintHandler = new
                    NiblReconciliationReceiptPrintHandler(printerService);
            reconciliationReceiptPrintHandler.printReceipt(niblReconciliationReceipt, ReceiptVersion.MERCHANT_COPY);
        }
    }

    private void printDebugReceiptIfDebugMode(PrinterService printerService, IsoMessageResponse isoMessageResponse) {
        boolean isDebugVersion = this.terminalRepository.findTerminalInfo().isDebugModeEnabled();
        if (isDebugVersion) {
            ReceiptHandler.IsoMessageReceiptHandler isoMessageReceiptHandler
                    = new NiblIsoMessageReceiptHandler(printerService);
            isoMessageReceiptHandler.printIsoMessageReceipt(isoMessageResponse);
        }
    }

    private String prepareMessage(boolean isReconciliationSuccess) {
        return isReconciliationSuccess ? "SETTLEMENT SUCCESS" : "SETTLEMENT FAILURE";
    }

    private RequestContext prepareRequestContext(String batchNumber, ReconciliationRepository reconciliationRepository,
                                                 ApplicationRepository applicationRepository) {
        HostInfo primaryHostInfo = this.terminalRepository.findPrimaryHost();
        HostInfo secondaryHostInfo = this.terminalRepository.findSecondaryHost();
        TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
        ConnectionParam connectionParam = new ConnectionParam(primaryHostInfo, secondaryHostInfo);
        RequestContext context =
                new RequestContext(
                        this.terminalRepository.getSystemTraceAuditNumber(), terminalInfo, connectionParam);
        this.settlementRequest = this.prepareSettlementRequest(batchNumber, reconciliationRepository, applicationRepository);
        context.setRequest(this.settlementRequest);
        return context;
    }

    private SettlementRequest prepareSettlementRequest(String batchNumber, ReconciliationRepository reconciliationRepository,
                                                       ApplicationRepository applicationRepository) {
        ReconciliationHandler reconciliationHandler = new NiblSettlementHandler(this.terminalRepository,
                reconciliationRepository, batchNumber, applicationRepository);
        ReconciliationTotals reconciliationTotals = reconciliationHandler.prepareReconciliationTotals();
        if (StringUtils.isAllZero(reconciliationTotals.toTotalsBlock()))
            throw new PosException(PosError.DEVICE_ERROR_ZERO_AMOUNT_SETTLEMENT);
        this.logger.log("::: NIBL ::: RECONCILIATION TOTALS ::: " + reconciliationTotals.toString());
        this.notifySettlementConfirmationEvent((NiblSettlementTotals) reconciliationTotals);
        return SettlementRequest.Builder.newInstance()
                .withBatchNumber(batchNumber)
                .withApplicationVersion(this.terminalRepository.getApplicationVersion())
                .withReconciliationTotals(reconciliationTotals)
                .build();
    }

    private void notifySettlementConfirmationEvent(NiblSettlementTotals niblSettlementTotals) {
        String jsonData = this.prepareSettlementConfirmationJsonData(niblSettlementTotals);
        countDownLatch = new CountDownLatch(1);
        this.notifier.notify(Notifier.EventType.SETTLEMENT_CONFIRMATION_EVENT, jsonData,
                confirmed -> {
                    isConfirm[0] = confirmed;
                    countDownLatch.countDown();
                });
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!isConfirm[0])
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
    }

    private String prepareSettlementConfirmationJsonData(NiblSettlementTotals niblSettlementTotals) {
        Map<String, String> map = new HashMap<>();
        map.put("currencyName", niblSettlementTotals.getTransactionCurrencyName());
        map.put("salesCount", String.valueOf(niblSettlementTotals.getSalesCount()));
        map.put("salesAmount", String.valueOf(niblSettlementTotals.getSalesAmount()));
        map.put("refundCount", String.valueOf(niblSettlementTotals.getSalesRefundCount()));
        map.put("refundAmount", String.valueOf(niblSettlementTotals.getSalesRefundAmount()));
        map.put("voidCount", String.valueOf(niblSettlementTotals.getVoidCount()));
        map.put("voidAmount", String.valueOf(niblSettlementTotals.getVoidAmount()));
        map.put("authCount", String.valueOf(niblSettlementTotals.getAuthCount()));
        map.put("authAmount", String.valueOf(niblSettlementTotals.getAuthAmount()));
        map.put("totalCount", String.valueOf(niblSettlementTotals.getTotalCountReceipt()));
        map.put("totalAmount", String.valueOf(niblSettlementTotals.getTotalAmountReceipt()));
        return JsonUtils.toJsonObj(map);
    }

    private LogOnRequestModel prepareLogOnRequestModel(
            ReconciliationRequestModel reconciliationRequestModel
    ) {
        return new LogOnRequestModel(
                reconciliationRequestModel.getPrinterService(),
                reconciliationRequestModel.getHardwareKeyService(),
                reconciliationRequestModel.getApplicationPackageName()
        );
    }

    private void updateFailedActivityLog(ActivityLogType activityLogType, RequestContext requestContext, String remarks) {
        try {
            SettlementRequest settlementRequest = (SettlementRequest) requestContext.getRequest();
            TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.terminalRepository.getSystemTraceAuditNumber())
                    .withType(activityLogType)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(settlementRequest.getBatchNumber())
                    .withAmount(((NiblSettlementTotals) settlementRequest.getReconciliationTotals()).getTotalAmountReceipt())
                    .withResponseCode("")
                    .withIsoRequest("")
                    .withIsoResponse("")
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(remarks)
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(StringUtils.prepareAdditionalDataForActivityLog(this.terminalRepository.getTerminalSerialNumber()))
                    .build();
            this.terminalRepository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update " + activityLogType + " activity log :: " + e.getMessage());
        }
    }

    private void updateFailedActivityLog(ActivityLogType activityLogType, RequestContext requestContext, FinPosException fe) {
        try {
            SettlementRequest settlementRequest = (SettlementRequest) requestContext.getRequest();
            TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.terminalRepository.getSystemTraceAuditNumber())
                    .withType(activityLogType)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(settlementRequest.getBatchNumber())
                    .withAmount(((NiblSettlementTotals) settlementRequest.getReconciliationTotals()).getTotalAmountReceipt())
                    .withResponseCode("")
                    .withIsoRequest(fe.getIsoRequest())
                    .withIsoResponse(fe.getIsoResponse())
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(fe.getLocalizedMessage() + " : " + fe.getDetailMessage())
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(StringUtils.prepareAdditionalDataForActivityLog(this.terminalRepository.getTerminalSerialNumber()))
                    .build();
            this.terminalRepository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update " + activityLogType + " activity log :: " + e.getMessage());
        }

    }

    private void updateActivityLog(ActivityLogType activityLogType, RequestContext requestContext, boolean isReconciliationSuccess) {
        try {
            SettlementRequest settlementRequest = (SettlementRequest) requestContext.getRequest();
            TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
            String responseCode = IsoMessageUtils.retrieveFromDataElementsAsString(
                    reconciliationIsoMessageResponse,
                    DataElement.RESPONSE_CODE
            );
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.terminalRepository.getSystemTraceAuditNumber())
                    .withType(activityLogType)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(settlementRequest.getBatchNumber())
                    .withAmount(((NiblSettlementTotals) settlementRequest.getReconciliationTotals()).getTotalAmountReceipt())
                    .withResponseCode(responseCode)
                    .withIsoRequest(reconciliationIsoMessageResponse.getDebugRequestString())
                    .withIsoResponse(reconciliationIsoMessageResponse.getDebugResponseString())
                    .withStatus(isReconciliationSuccess ? ActivityLogStatus.SUCCESS : ActivityLogStatus.FAILED)
                    .withRemarks("")
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(StringUtils.prepareAdditionalDataForActivityLog(this.terminalRepository.getTerminalSerialNumber()))
                    .build();
            this.terminalRepository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update " + activityLogType + " activity log :: " + e.getMessage());
        }
    }
}
