package global.citytech.finpos.neps.iso8583;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.comm.Request;
import global.citytech.finposframework.comm.Response;
import global.citytech.finposframework.comm.TPDU;
import global.citytech.finposframework.comm.TcpClient;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.DefaultMessageParser;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.MessageParser;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.utility.ByteUtils;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.HexDump;
import global.citytech.finposframework.utility.NumberUtils;
import global.citytech.finposframework.utility.StackTraceUtil;

;

/** User: Surajchhetry Date: 2/28/20 Time: 3:50 PM */
public abstract class NepsMessageSenderTemplate {
  protected final SpecInfo specInfo;
  private Logger logger = Logger.getLogger(NepsMessageSenderTemplate.class.getName());

  protected final RequestContext context;

  public NepsMessageSenderTemplate(RequestContext context) {
    this.context = context;
    this.specInfo = new NepsSpecInfo();
  }

  protected abstract MessagePayload prepareRequestPayload();

  protected abstract String getProcessingCode();

  protected abstract String getFunctionCode();

  protected Iso8583Msg getTemplateIsoMessage(MTI mti) {
    String transmissionDateAndTime = HelperUtils.getTransmissionDateAndTime();
    Iso8583Msg msg = new Iso8583Msg(mti);
    msg.addField(new DataElement(DataElement.PROCESSING_CODE, this.getProcessingCode()));
    msg.addField(new DataElement(DataElement.TRANSMISSION_DATE_TIME, transmissionDateAndTime));
    getBaseTemplateIsoMessage(msg);
    return msg;
  }

  protected Iso8583Msg getTemplateIsoMessageForReconciliation(MTI mti) {
    Iso8583Msg msg = new Iso8583Msg(mti);
    getBaseTemplateIsoMessage(msg);
    return msg;
  }

  protected Iso8583Msg getTemplateIsoMessageForReconciliationTrailer(MTI mti) {
    Iso8583Msg msg = new Iso8583Msg(mti);
    msg.addField(new DataElement(DataElement.PROCESSING_CODE, this.getProcessingCode()));
    getBaseTemplateIsoMessage(msg);
    return msg;
  }

  private Iso8583Msg getBaseTemplateIsoMessage(Iso8583Msg msg) {
    msg.addField(new DataElement(DataElement.SYSTEM_TRACE_AUDIT_NUMBER, this.context.getStan()));
    msg.addField(new DataElement(DataElement.FUNCTION_CODE_OR_NETWORK_INTERNATIONAL_IDENTIFIER, this.getFunctionCode()));
    msg.addField(new DataElement(DataElement.CARD_ACCEPTOR_TERMINAL_ID, this.context.getTerminalInfo().getTerminalID()));
    msg.addField(new DataElement(DataElement.CARD_ACCEPTOR_ACQUIRER_ID, this.context.getTerminalInfo().getMerchantID()));
    return msg;
  }

  public IsoMessageResponse send() {
    try (TcpClient tcpClient = new TcpClient(context.getParam())) {
      HostInfo hostInfo = tcpClient.dial();
      logger.log("Connected with ::" + hostInfo);
      // Prepare ISO request payload
      MessagePayload payload = this.prepareRequestPayload();
      // Decorate request message with required other data tpdu and length
      Request requestPacket = this.prepareRequestPacket(payload, hostInfo);
      payload.setRequestPacket(requestPacket);
      logger.debug("Request");
      logger.debug(HexDump.dumpHexString(requestPacket.getData()));
      tcpClient.write(requestPacket);
      Response lenResponse = tcpClient.read(specInfo.getMessageHeaderLength());
      logger.log(" Response Received ");
      int len = NumberUtils.fromHexASCIIBytesToInt(lenResponse.getData());
      logger.debug("Header Length : " + len);
      Response data = tcpClient.read(len);
      logger.debug(" ==== RESPONSE ====");
      logger.debug(HexDump.dumpHexString(data.getData()));
      MessageParser messageParser = new DefaultMessageParser(this.specInfo);
      // Parse data after removing TPDU
      byte[] onlyMessage = this.removeTPDU(data.getData());
      Iso8583Msg msg = messageParser.parse(onlyMessage);
      logger.debug(msg.getPrintableData());
      IsoMessageResponse.Builder responseBuilder =
          IsoMessageResponse.Builder.createDefaultBuilder(msg, hostInfo, payload);
      responseBuilder.responseLength(len);
      responseBuilder.debugRequestString(HexDump.dumpHexString(requestPacket.getData()));
      responseBuilder.debugResponseString(HexDump.dumpHexString(data.getData()));
      return responseBuilder.build();
    } catch (Exception e) {
      if (e instanceof FinPosException) throw (FinPosException) e;
      throw new FinPosException(
          FinPosException.ExceptionType.SYSTEM_ERROR, StackTraceUtil.getStackTraceAsString(e));
    }
  }

  protected byte[] removeTPDU(byte[] data) {
    return ByteUtils.remove(data, TPDU.SIZE_IN_BYTES);
  }

  protected Request prepareRequestPacket(MessagePayload request, HostInfo hostInfo) {
    try (ByteArrayOutputStream messagePacket = new ByteArrayOutputStream()) {
      int totalMessageLength = hostInfo.getTpdu().getAsBytes().length + request.getAsBytes().length;
      // Add Message Length
      messagePacket.write(NumberUtils.toBytes(totalMessageLength, 2));
      // Add TPDU
      messagePacket.write(hostInfo.getTpdu().getAsBytes());
      // Add ISO message
      messagePacket.write(request.getAsBytes());
      // Prepare Request
      return new Request(messagePacket.toByteArray());

    } catch (IOException e) {
      throw new FinPosException(
          FinPosException.ExceptionType.SYSTEM_ERROR, StackTraceUtil.getStackTraceAsString(e));
    }
  }
}
