package global.citytech.finposframework.listeners;

import java.io.Serializable;

/**
 * Created by Unique Shakya on 8/4/2021.
 */
public interface SettlementConfirmationListener extends Serializable {
    void onResult(boolean confirmed);
}
