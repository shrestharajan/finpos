package global.citytech.finpos.merchant.presentation.model.common

class TerminalInfos(
    val merchantId: String,
    val terminalId: String
) {
}