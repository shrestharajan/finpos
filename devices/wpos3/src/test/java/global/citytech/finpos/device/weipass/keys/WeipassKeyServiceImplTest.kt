package global.citytech.finpos.device.weipass.keys

import com.wiseasy.emvprocess.Acquirer
import com.wiseasy.emvprocess.SDKInstance
import global.citytech.finpos.device.weipass.utils.WeipassConstant
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.keymgmt.keys.KeyAlgorithm
import global.citytech.finposframework.hardware.keymgmt.keys.KeyType
import global.citytech.finposframework.hardware.keymgmt.keys.KeyType.*
import global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyCheckRequest
import global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyInjectionRequest
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import wangpos.sdk4.libkeymanagerbinder.Key

/**
 * Created by Rishav Chudal on 4/21/20.
 */
class WeipassKeyServiceImplTest {

    @MockK
    private lateinit var mockedAcquirer: Acquirer

    @MockK
    private lateinit var mockedKey:Key

    private lateinit var SUT: WeipassKeyServiceImpl
    private lateinit var deviceResponse: DeviceResponse

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        mockWeipassLogger()
        mockAcquirer()
        SUT = WeipassKeyServiceImpl()
    }

    private fun mockWeipassLogger() {
        mockkObject(WeipassLogger)
        mockkObject(WeipassLogger.Companion)
        every { WeipassLogger.log(any()) } just Runs
    }

    private fun mockAcquirer() {
        mockkStatic(Acquirer::class)
        every { Acquirer.getInstance() } returns mockedAcquirer
    }

    private fun keyInjectionRequestWithEmptyPackageName(): KeyInjectionRequest {
        return KeyInjectionRequest(
            "",
            KEY_TYPE_TMK,
            KeyAlgorithm.TYPE_3DES,
            "12345678901234567890",
            null,
            null,
            true
        )
    }

    private fun keyInjectionRequestWithEmptyKey(): KeyInjectionRequest {
        return KeyInjectionRequest(
            "global.citytech.test",
            KEY_TYPE_TMK,
            KeyAlgorithm.TYPE_3DES,
            "",
            null,
            null,
            true
        )
    }

    private fun keyInjectionRequestWith(keyType: KeyType): KeyInjectionRequest {
        return KeyInjectionRequest(
            "global.citytech.test",
            keyType,
            KeyAlgorithm.TYPE_3DES,
            "12345678901234567890",
            null,
            null,
            true
        )
    }

    private fun keyInjectionRequestForIpekWith(ksn:String?): KeyInjectionRequest {
        return KeyInjectionRequest(
            "global.citytech.test",
            KEY_TYPE_IPEK,
            KeyAlgorithm.TYPE_3DES,
            "12345678901234567890",
            null,
            ksn,
            true
        )
    }

    private fun mockSDKInstanceAndKey() {
        mockkStatic(SDKInstance::class)
        SDKInstance.mKey = mockedKey
    }

    /*
     * Key Injection Tests
     */
    @Test
    fun injectKey_packageNameIsEmpty_ReturnsFailureResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWithEmptyPackageName()

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_INVALID_PACKAGE_NAME, this.deviceResponse.message)
    }

    @Test
    fun injectKey_keyIsEmpty_ReturnsFailureResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWithEmptyKey()

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_INVALID_KEY, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsTLKAndInjectionResultIsZero_ReturnSuccessResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_TLK)
        every { mockedAcquirer.addAcquireKey(any()) } returns 0

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        assertEquals(Result.SUCCESS, this.deviceResponse.result)
        assertEquals(WeipassConstant.SUCCESS_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsTLKAndInjectionResultIsNotZero_ReturnFailureResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_TLK)
        every { mockedAcquirer.addAcquireKey(any()) } returns -1

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        verify { mockedAcquirer.addAcquireKey(any()) }
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsTMKAndInjectionResultIsZero_ReturnSuccessResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_TMK)
        every { mockedAcquirer.addAcquireKey(any()) } returns 0

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        assertEquals(Result.SUCCESS, this.deviceResponse.result)
        assertEquals(WeipassConstant.SUCCESS_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsTMKAndInjectionResultIsNotZero_ReturnFailureResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_TMK)
        every { mockedAcquirer.addAcquireKey(any()) } returns -1

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        verify { mockedAcquirer.addAcquireKey(any()) }
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsDEKAndInjectionResultIsZero_ReturnSuccessResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_DEK)
        every { mockedAcquirer.addAcquireKey(any()) } returns 0

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        assertEquals(Result.SUCCESS, this.deviceResponse.result)
        assertEquals(WeipassConstant.SUCCESS_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsDEKAndInjectionResultIsNotZero_ReturnFailureResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_DEK)
        every { mockedAcquirer.addAcquireKey(any()) } returns -1

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        verify { mockedAcquirer.addAcquireKey(any()) }
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsPEKAndInjectionResultIsZero_ReturnSuccessResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_PEK)
        every { mockedAcquirer.addAcquireKey(any()) } returns 0

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        assertEquals(Result.SUCCESS, this.deviceResponse.result)
        assertEquals(WeipassConstant.SUCCESS_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsPEKAndInjectionResultIsNotZero_ReturnFailureResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_PEK)
        every { mockedAcquirer.addAcquireKey(any()) } returns -1

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        verify { mockedAcquirer.addAcquireKey(any()) }
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsIPEKAndKsnIsNull_ReturnFailureResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestForIpekWith(null)

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsIPEKAndKsnIsEmpty_ReturnFailureResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestForIpekWith("")

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsIPEKAndInjectionResultIsZero_ReturnSuccessResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestForIpekWith("1234567890ABCDEF")
        every { mockedAcquirer.addAcquireKey(any()) } returns 0

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        assertEquals(Result.SUCCESS, this.deviceResponse.result)
        assertEquals(WeipassConstant.SUCCESS_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsIPEKAndInjectionResultIsNotZero_ReturnFailureResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestForIpekWith("1234567890ABCDEF")
        every { mockedAcquirer.addAcquireKey(any()) } returns -1

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        verify { mockedAcquirer.addAcquireKey(any()) }
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsMAKAndInjectionResultIsZero_ReturnSuccessResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_MAK)
        every { mockedAcquirer.addAcquireKey(any()) } returns 0

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        assertEquals(Result.SUCCESS, this.deviceResponse.result)
        assertEquals(WeipassConstant.SUCCESS_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsMAKAndInjectionResultIsNotZero_ReturnFailureResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_MAK)
        every { mockedAcquirer.addAcquireKey(any()) } returns -1

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        verify { mockedAcquirer.addAcquireKey(any()) }
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsKBPKAndInjectionResultIsZero_ReturnSuccessResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_KBPK)
        every { mockedAcquirer.addAcquireKey(any()) } returns 0

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        assertEquals(Result.SUCCESS, this.deviceResponse.result)
        assertEquals(WeipassConstant.SUCCESS_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsKBPKAndInjectionResultIsNotZero_ReturnFailureResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_KBPK)
        every { mockedAcquirer.addAcquireKey(any()) } returns -1

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        verify { mockedAcquirer.addAcquireKey(any()) }
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsDDEKAndInjectionResultIsZero_ReturnSuccessResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_DDEK)
        every { mockedAcquirer.addAcquireKey(any()) } returns 0

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        assertEquals(Result.SUCCESS, this.deviceResponse.result)
        assertEquals(WeipassConstant.SUCCESS_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsDDEKAndInjectionResultIsNotZero_ReturnFailureResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_DDEK)
        every { mockedAcquirer.addAcquireKey(any()) } returns -1

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        verify { mockedAcquirer.addAcquireKey(any()) }
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsDMAKAndInjectionResultIsZero_ReturnSuccessResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_DMAK)
        every { mockedAcquirer.addAcquireKey(any()) } returns 0

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        assertEquals(Result.SUCCESS, this.deviceResponse.result)
        assertEquals(WeipassConstant.SUCCESS_KEY_INJECT, this.deviceResponse.message)
    }

    @Test
    fun injectKey_KeyTypeIsDMAKAndInjectionResultIsNotZero_ReturnFailureResponse() {
        //Setup
        val keyInjectionRequest = keyInjectionRequestWith(KEY_TYPE_DMAK)
        every { mockedAcquirer.addAcquireKey(any()) } returns -1

        //Run
        this.deviceResponse = SUT.injectKey(keyInjectionRequest)

        //Verify
        verify { mockedAcquirer.addAcquireKey(any()) }
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_KEY_INJECT, this.deviceResponse.message)
    }

    /*
     * Key Erase Tests
     */
    @Test
    fun eraseAllKeys_keyEraseResultIsZero_ReturnSuccessResponse() {
        //Setup
        mockSDKInstanceAndKey()
        every { mockedKey.erasePED() } returns 0

        //Run
        this.deviceResponse = SUT.eraseAllKeys()

        //Verify
        verify { mockedKey.erasePED() }
        assertEquals(Result.SUCCESS, this.deviceResponse.result)
        assertEquals(WeipassConstant.SUCCESS_KEY_ERASE, this.deviceResponse.message)
    }

    @Test
    fun eraseAllKeys_keyEraseResultIsNotZero_ReturnFailureResponse() {
        //Setup
        mockSDKInstanceAndKey()
        every { mockedKey.erasePED() } returns -1

        //Run
        this.deviceResponse = SUT.eraseAllKeys()

        //Verify
        verify { mockedKey.erasePED() }
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_KEY_ERASE, this.deviceResponse.message)
    }

    @Test
    fun eraseAllKeys_ExceptionInKeyErase_ReturnFailureResponse() {
        //Setup
        mockSDKInstanceAndKey()
        every { mockedKey.erasePED() } throws Exception("This is a test exception")

        //Run
        this.deviceResponse = SUT.eraseAllKeys()

        //Verify
        verify { mockedKey.erasePED() }
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_KEY_ERASE, this.deviceResponse.message)
    }

    /*
     * Key Check Tests
     */
    @Test
    fun checkKeyExist_packageNameIsEmpty_ReturnsFailureResponse() {
        //Setup
        val keyCheckRequest =
            KeyCheckRequest(
                "",
                KEY_TYPE_TMK
            )

        //Run
        this.deviceResponse = SUT.checkKeyExist(keyCheckRequest)

        //Verify
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_INVALID_PACKAGE_NAME, this.deviceResponse.message)
    }

    @Test
    fun checkKeyExist_KeyCheckResultIsZero_ReturnsSuccessResponse() {
        //Setup
        mockSDKInstanceAndKey()
        val keyCheckRequest =
            KeyCheckRequest(
                "global.citytech",
                KEY_TYPE_TMK
            )
        every { mockedKey.checkKeyExist(any(), any()) } returns 0

        //Run
        this.deviceResponse = SUT.checkKeyExist(keyCheckRequest)

        //Verify
        verify { mockedKey.checkKeyExist(any(), any()) }
        assertEquals(Result.SUCCESS, this.deviceResponse.result)
        assertEquals(WeipassConstant.SUCCESS_KEY_CHECK, this.deviceResponse.message)
    }

    @Test
    fun checkKeyExist_KeyCheckResultIsNotZero_ReturnsFailureResponse() {
        //Setup
        mockSDKInstanceAndKey()
        val keyCheckRequest =
            KeyCheckRequest(
                "global.citytech",
                KEY_TYPE_TMK
            )
        every { mockedKey.checkKeyExist(any(), any()) } returns -1

        //Run
        this.deviceResponse = SUT.checkKeyExist(keyCheckRequest)

        //Verify
        verify { mockedKey.checkKeyExist(any(), any()) }
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_KEY_CHECK, this.deviceResponse.message)
    }

    @Test
    fun checkKeyExist_ExceptionInKeyCheck_ResultFailureResponse() {
        //Setup
        mockSDKInstanceAndKey()
        val keyCheckRequest =
            KeyCheckRequest(
                "global.citytech",
                KEY_TYPE_TMK
            )
        every { mockedKey.checkKeyExist(any(), any()) } throws Exception("This is test exception")

        //Run
        this.deviceResponse = SUT.checkKeyExist(keyCheckRequest)

        //Verify
        verify { mockedKey.checkKeyExist(any(), any()) }
        assertEquals(Result.FAILURE, this.deviceResponse.result)
        assertEquals(WeipassConstant.FAILURE_KEY_CHECK, this.deviceResponse.message)
    }
}