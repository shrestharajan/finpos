package global.citytech.finposframework.utility;

import java.io.PrintWriter;
import java.io.StringWriter;

/** User: Surajchhetry Date: 2/27/20 Time: 3:38 PM */
public class StackTraceUtil {

  public static String getStackTraceAsString(Exception e) {
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    e.printStackTrace(pw);
    return sw.toString();
  }
}
