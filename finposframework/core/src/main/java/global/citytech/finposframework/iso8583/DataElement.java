package global.citytech.finposframework.iso8583;

import java.util.Objects;

import global.citytech.finposframework.exceptions.FinPosException;

/**
 * User: Surajchhetry Date: 2/14/20 Time: 7:16 AM
 */
public class DataElement {
    public static final int PRIMARY_ACCOUNT_NUMBER = 2;
    public static final int PROCESSING_CODE = 3;
    public static final int AMOUNT = 4;
    public static final int AMOUNT_SETTLEMENT = 5;
    public static final int TRANSMISSION_DATE_TIME = 7;
    public static final int SYSTEM_TRACE_AUDIT_NUMBER = 11;
    public static final int LOCAL_TIME = 12;
    public static final int LOCAL_DATE = 13;
    public static final int PAN_EXPIRE_DATE = 14;
    public static final int SETTLEMENT_DATE = 15;
    public static final int POS_ENTRY_MODE = 22;
    public static final int APPLICATION_PAN_SEQUENCE_NUMBER = 23;
    public static final int FUNCTION_CODE_OR_NETWORK_INTERNATIONAL_IDENTIFIER = 24;
    public static final int POINT_OF_SERVICE_CONDITIONAL_CODE = 25;
    public static final int ORIGINAL_AMOUNT = 30;
    public static final int TRACK_II_DATA = 35;
    public static final int RETRIEVAL_REFERENCE_NUMBER = 37;
    public static final int AUTHORIZATION_IDENTIFICATION_RESPONSE = 38;
    public static final int RESPONSE_CODE = 39;
    public static final int CARD_ACCEPTOR_TERMINAL_ID = 41;
    public static final int CARD_ACCEPTOR_ACQUIRER_ID = 42;
    public static final int ADDITIONAL_RESPONSE_DATA = 44;
    public static final int FEES_AMOUNT = 46;
    public static final int ADDITIONAL_DATA_PRIVATE = 48;
    public static final int CURRENCY_CODE = 49;
    public static final int PIN_BLOCK = 52;
    public static final int SECURITY_RELATED_CONTROL_INFORMATION = 53;
    public static final int ADDITIONAL_AMOUNT = 54;
    public static final int ICC_SYSTEM_RELATED_DATA = 55;
    public static final int PRIVATE_USE_FIELD_60 = 60;
    public static final int PRIVATE_USE_FIELD_62 = 62;
    public static final int PRIVATE_USE_FIELD_63 = 63;
    public static final int PRIMARY_MAC = 64;
    public static final int ACCOUNT_IDENTIFICATION_ONE = 102;
    public static final int ACCOUNT_IDENTIFICATION_TWO = 103;
    public static final int SECONDARY_MAC = 128;



    private final int index;
    private final Object data;

    public DataElement(int index, Object data) {
        this.index = index;
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return "[" +
                "index=" + index +
                ", data=" + data +
                ']';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataElement that = (DataElement) o;
        return index == that.index;
    }

    @Override
    public int hashCode() {
        return Objects.hash(index);
    }

    public int getValueAsInt() {
        if(this.data == null)
            throw new FinPosException(FinPosException.ExceptionType.INVALID_PARAMETER_SUPPLIED,"Unable to convert integer");
        return Integer.parseInt(this.data.toString().trim());
    }

    public String getAsString() {
        return data.toString();
    }

/*
            public DataElement(int index, Object value) {
                this.index = index;
                this.value = value;
            }
        */

/*

    public DataType getType() {
        return type;
    }


    public void setValue(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + index;
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DataElement other = (DataElement) obj;
        if (index != other.index) {
            return false;
        }
        if (size != other.size) {
            return false;
        }
        return type == other.type;
    }


     // Check whether size of data element is fixed or not.

     // @return <code>false</code> if variable length.

//    public boolean hasFixedLength() {
//        return this.lengthType != LengthInfo.LengthType.LLVAR && this.lengthType != LengthInfo.LengthType.LLLVAR;
//    }
//
//    public LengthInfo.LengthType getLengthType() {
//        return lengthType;
//    }



//    public enum DataType {
//        NUMERIC,
//        // A fixed-length alphanumeric value. It is filled with spaces to the right.
//
//        ALPHA,
//
//        // A date in format MMddHHmmss
//
//        DATE10,
//        // A date in format MMdd
//
//        DATE4,
//        // A date in format yyMM
//
//        DATE_EXP,
//        // Time of day in format HHmmss
//
//        TIME,
//        // An amount, expressed in cents with a fixed length of 12.
//
//        AMOUNT,
//        // A date in format yyyyMMddHHmmss
//        DATE_YYYYMMDDHHMMSS,
//        //A date in format yyyyMMdd
//        DATE_YYYYMMDD,
//        BINARY
//    }

    public enum EncodingType {
        // 30313233 (ASCII for "0123")
        ASCII,
        // "000000" is represented with just three bytes whose hex values are "00 00 00"
        BCD,
        EBCDIC,
        //     BINARY,
        DECIMAL
    }

    @Override
    public String toString() {
        return "FieldValue [index=" + index + ", value=" + value + "]";
    }

    public int getValueAsInt() {
        return Integer.parseInt(this.value.toString());
    }

    /*
    public byte[] formatAccordingToType(DataElement element) {
        if (value == null) {
            return "ISOValue<null>".getBytes(Charset.forName("UTF-8"));
        }
        if (element.getType() == DataType.BINARY) return (byte[]) this.value;


        if (element.getType() == DataType.NUMERIC
                || element.getType() == DataType.AMOUNT) {
            if (element.getType() == DataType.AMOUNT) {
                return format(element, (BigDecimal) value, 12).getBytes(Charset.forName("UTF-8"));
            } else if (value instanceof Number) {
                return format(element, ((Number) value).longValue(), element.getSize())
                        .getBytes(Charset.forName("UTF-8"));
            } else {
                return format(element, value.toString(), element.getSize())
                        .getBytes(Charset.forName("UTF-8"));
            }
        } else if (element.getType() == DataType.ALPHA) {
            return format(element, value.toString(), element.getSize())
                    .getBytes(Charset.forName("UTF-8"));
        } else if (element.getLengthType() == LengthType.LLVAR
                || element.getLengthType() == LengthType.LLLVAR) {
            return value.toString().getBytes(Charset.forName("UTF-8"));
        } else if (value instanceof Date) {
            return format(element, (Date) value).getBytes(Charset.forName("UTF-8"));
        }
        return value.toString().getBytes(Charset.forName("UTF-8"));
    }

    /**
     * Formats a Date if the receiver is DATE10, DATE4, DATE_EXP or TIME; throws an exception
     * otherwise.

    private String format(DataElement element, Date value) {
        if (element.getType() == DataType.DATE10) {
            return new SimpleDateFormat("MMddHHmmss").format(value);
        } else if (element.getType() == DataType.DATE4) {
            return new SimpleDateFormat("MMdd").format(value);
        } else if (element.getType() == DataType.DATE_EXP) {
            return new SimpleDateFormat("yyMM").format(value);
        } else if (element.getType() == DataType.TIME) {
            return new SimpleDateFormat("HHmmss").format(value);
        } else if (element.getType() == DataType.DATE_YYYYMMDDHHMMSS) {
            return new SimpleDateFormat("yyyyMMddHHmmss").format(value);
        } else if (element.getType() == DataType.DATE_YYYYMMDD) {
            return new SimpleDateFormat("yyyyMMdd").format(value);
        }
        // DATE_YYYYMMDD
        throw new IllegalArgumentException("Cannot format date as " + this);
    }

    /**
     * Formats the string to the given length (length is only useful if type is ALPHA).

    private String format(DataElement element, String value, int length) {
        if (element.getType() == DataType.ALPHA) {
            if (value == null) {
                value = "";
            }
            if (value.length() > length) {
                return value.substring(0, length);
            }
            char[] c = new char[length];
            System.arraycopy(value.toCharArray(), 0, c, 0, value.length());
            for (int i = value.length(); i < c.length; i++) {
                c[i] = ' ';
            }
            return new String(c);
        } else if (element.getLengthType() == LengthType.LLVAR
                || element.getLengthType() == LengthType.LLLVAR) {
            return value;
        } else if (element.getType() == DataType.NUMERIC) {
            char[] c = new char[length];
            char[] x = value.toCharArray();
            if (x.length > length) {
                throw new IllegalArgumentException(
                        "Numeric value is larger than intended length: "
                                + value
                                + " LEN "
                                + length
                                + " For field "
                                + element.getIndex());
            }
            int lim = c.length - x.length;
            for (int i = 0; i < lim; i++) {
                c[i] = '0';
            }
            System.arraycopy(x, 0, c, lim, x.length);
            return new String(c);
        }
        throw new IllegalArgumentException("Cannot format String as " + this);
    }

    /**
     * Formats the integer value as a NUMERIC, an AMOUNT, or a String.

    private String format(DataElement element, long value, int length) {
        if (element.getType() == DataType.NUMERIC) {
            char[] c = new char[length];
            char[] x = Long.toString(value).toCharArray();
            if (x.length > length) {
                throw new IllegalArgumentException(
                        "Numeric value is larger than intended length: " + value + " LEN " + length);
            }
            int lim = c.length - x.length;
            for (int i = 0; i < lim; i++) {
                c[i] = '0';
            }
            System.arraycopy(x, 0, c, lim, x.length);
            return new String(c);
        } else if (element.getType() == DataType.ALPHA
                || element.getLengthType() == LengthType.LLVAR
                || element.getLengthType() == LengthType.LLLVAR) {
            return format(element, Long.toString(value), length);
        } else if (element.getType() == DataType.AMOUNT) {
            String v = Long.toString(value);
            char[] digits = new char[12];
            for (int i = 0; i < 12; i++) {
                digits[i] = '0';
            }
            System.arraycopy(v.toCharArray(), 0, digits, 10 - v.length(), v.length());
            return new String(digits);
        }
        throw new IllegalArgumentException("Cannot format number as " + this);
    }

    /**
     * Formats the BigDecimal as an AMOUNT, NUMERIC, or a String.

    private String format(DataElement element, BigDecimal value, int length) {
        if (element.getType() == DataType.AMOUNT) {
            String v =
                    new DecimalFormat(value.signum() >= 0 ? "0000000000.00" : "000000000.00").format(value);
            return v.substring(0, 10) + v.substring(11);
        } else if (element.getType() == DataType.NUMERIC) {
            return format(element, value.longValue(), length);
        } else if (element.getType() == DataType.ALPHA
                || element.getLengthType() == LengthType.LLVAR
                || element.getLengthType() == LengthType.LLLVAR) {
            return format(element, value.toString(), length);
        }
        throw new IllegalArgumentException("Cannot format BigDecimal as " + this);
    }

    public static DataElement getEmptyField() {
        return new DataElement(-1, -1, DataType.ALPHA, LengthType.FIXED);
    }
    */
}
