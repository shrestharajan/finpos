package global.citytech.finpos.nibl.iso8583.requestsender.cashin;

import java.math.BigDecimal;

import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.switches.PosEntryMode;

public class CashInRequest extends TransactionIsoRequest {

    private String pan;
    private BigDecimal transactionAmount = BigDecimal.ZERO;
    private String localTime;
    private String localDate;
    private String expireDate;
    private PosEntryMode posEntryMode;
    private String posConditionCode;
    private String track2Data;
    private String additionalData;
    private String currencyCode;
    private String pinBlock;
    private String invoiceNumber;
    private String emvData;
    private String retrievalReferenceNumber;
    private String cardSequenceNumber;

    public String getPan() {
        return pan;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }


    public String getLocalTime() {
        return localTime;
    }

    public String getLocalDate() {
        return localDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public String getTrack2Data() {
        return track2Data;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getPinBlock() {
        return pinBlock;
    }

    public String getEmvData() {
        return emvData;
    }

    public String getInvoiceNumber() { return invoiceNumber;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }
    public String getCardSequenceNumber() {
        return cardSequenceNumber;
    }

    public static final class Builder {
        private String pan;
        private BigDecimal transactionAmount = BigDecimal.ZERO;
        private String localTime;
        private String localDate;
        private String expireDate;
        private PosEntryMode posEntryMode;
        private String posConditionCode;
        private String track2Data;
        private String additionalData;
        private String currencyCode;
        private String pinBlock;
        private String emvData;
        private String invoiceNumber;
        private String retrievalReferenceNumber;
        private String cardSequenceNumber;

        private Builder() {
        }

        public static CashInRequest.Builder newInstance() {
            return new CashInRequest.Builder();
        }


        public CashInRequest.Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public CashInRequest.Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public CashInRequest.Builder localTime(String localTime) {
            this.localTime = localTime;
            return this;
        }

        public CashInRequest.Builder localDate(String localDate) {
            this.localDate = localDate;
            return this;
        }

        public CashInRequest.Builder expireDate(String expiryDate) {
            this.expireDate = expiryDate;
            return this;
        }


        public CashInRequest.Builder posEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public CashInRequest.Builder posConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public CashInRequest.Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public CashInRequest.Builder additionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public CashInRequest.Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public CashInRequest.Builder pinBlock(String pinBlock) {
            this.pinBlock = pinBlock;
            return this;
        }

        public CashInRequest.Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public CashInRequest.Builder invoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }

        public CashInRequest.Builder rrn(String rrn) {
            this.retrievalReferenceNumber = rrn;
            return this;
        }

        public CashInRequest.Builder cardSequenceNumber(String cardSequenceNumber) {
            this.cardSequenceNumber = cardSequenceNumber;
            return this;
        }



        public CashInRequest build() {
            CashInRequest CashInRequest = new CashInRequest();
            CashInRequest.pan = this.pan;
            CashInRequest.transactionAmount = this.transactionAmount;
            CashInRequest.localDate = this.localDate;
            CashInRequest.localTime = this.localTime;
            CashInRequest.expireDate = this.expireDate;
            CashInRequest.posEntryMode = this.posEntryMode;
            CashInRequest.posConditionCode = this.posConditionCode;
            CashInRequest.track2Data = this.track2Data;
            CashInRequest.additionalData = this.additionalData;
            CashInRequest.currencyCode = this.currencyCode;
            CashInRequest.pinBlock = this.pinBlock;
            CashInRequest.emvData = this.emvData;
            CashInRequest.invoiceNumber = this.invoiceNumber;
            CashInRequest.retrievalReferenceNumber = this.retrievalReferenceNumber;
            CashInRequest.cardSequenceNumber = this.cardSequenceNumber;

            return CashInRequest;
        }
    }
}
