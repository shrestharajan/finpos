package global.citytech.finpos.merchant.framework.datasource.app

import android.app.Activity
import android.content.Context
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.IsoDep
import android.os.Build
import android.os.CountDownTimer
import global.citytech.common.data.Response
import global.citytech.easydroid.core.preference.SecurePreference
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.app.mobile.MobileNfcPaymentDataSource
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.DATA_PARSER_FIRST
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.DATA_PARSER_SECOND
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.DATA_PARSER_THIRD
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.DEVICE_WEIPASS
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.ERROR_MOBILE_NFC
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.ERROR_NOT_VALID_NFC
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.INVALID_NFC_CARD
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.INVALID_SOURCE_CODE_CARD
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.INVALID_SOURCE_CODE_MOBILE
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.PAYMENT_BILLING_NUMBER
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.PAYMENT_REFERENCE_NUMBER
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.PAYMENT_REQUEST_NUMBER
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.READ_NFC_MOBILE
import global.citytech.finpos.merchant.domain.usecase.mobile.request.Aid
import global.citytech.finpos.merchant.domain.usecase.mobile.request.MobileNfcPaymentGenerateRequest
import global.citytech.finpos.merchant.domain.usecase.mobile.request.MobileNfcPaymentStatusRequest
import global.citytech.finpos.merchant.domain.usecase.mobile.response.MobileNfcPaymentStatusResponse
import global.citytech.finpos.merchant.domain.utils.MobileNfcTlvDataParser
import global.citytech.finpos.merchant.framework.datasource.device.CardSourceImpl
import global.citytech.finpos.merchant.framework.datasource.device.LedSourceImpl
import global.citytech.finpos.merchant.framework.datasource.device.SoundSourceImpl
import global.citytech.finpos.merchant.presentation.mobilenfcpayment.NfcResponse
import global.citytech.finpos.merchant.presentation.model.mobile.list.AidParameterResponse
import global.citytech.finpos.merchant.presentation.utils.Constants
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finpos.merchant.utils.AppConstant.MOBILE_PAYMENT_TIME_OUT
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.getMinutesAndSecondsDataFromMillis
import global.citytech.finposframework.hardware.io.led.LedAction
import global.citytech.finposframework.hardware.io.led.LedLight
import global.citytech.finposframework.hardware.io.led.LedRequest
import global.citytech.finposframework.hardware.io.sound.Sound
import global.citytech.finposframework.hardware.utility.DeviceBytesUtil
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.notifier.Notifier.EventType.*
import global.citytech.finposframework.utility.StringUtils
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.CountDownLatch
import java.util.regex.Pattern
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

/**
 * @author SIDDHARTHA GHIMIRE
 */
class MobileNfcPaymentDataSourceImpl(
    val context: Context
) :
    MobileNfcPaymentDataSource {
    private var securePreference: SecurePreference
    private val preferenceName = "mobile_payment_pref"
    private val nfcPaymentIdentifiersKey = "req_billing_ref_number"
    private val logger = Logger.getLogger(MobileNfcPaymentDataSourceImpl::class.java.name)
    val notifier = NotificationHandler
    private val nfcAdapter = NfcAdapter.getDefaultAdapter(context)
    private var countDownLatch: CountDownLatch? = null
    private var responseData: ByteArray? = null
    private val manufacturer: String = Build.MANUFACTURER.toLowerCase(Locale.ROOT);
    private lateinit var isoDep: IsoDep
    private val patternToGetNumericOnlyFromAlphaNumeric = Pattern.compile("\\d+")
    var selectedAid: String? = null
    var listAid: List<String> = arrayListOf()

    init {
        securePreference = SecurePreference(context.applicationContext, preferenceName)
        listAid = if (AppUtility.isAppVariantNIBL()) {
            Constants.listOfAid
        } else {
            AppUtility.getAidList()
        }
    }

    private val mobilePayTransactionWaitingTimer =
        object : CountDownTimer(MOBILE_PAYMENT_TIME_OUT, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                logger.debug(
                    "Time left to show Nfc dialog ::: "
                        .plus(millisUntilFinished.getMinutesAndSecondsDataFromMillis())
                )
            }

            override fun onFinish() {
                notifier.notify(NFC_MOBILE_TIMEOUT)
            }
        }

    private val isoDepWaitingTimer = object : CountDownTimer(3000, 1000) {
        override fun onTick(p0: Long) {
            logger.debug(
                "Time left to disconnect nfc from the device ::: "
                    .plus(p0.getMinutesAndSecondsDataFromMillis())
            )
        }

        override fun onFinish() {
            CardSourceImpl(context).cleanUp()
        }
    }

    override fun getNfcData(): NfcResponse? {
        countDownLatch = CountDownLatch(1)
        mobilePayTransactionWaitingTimer.start()
        return try {
            enableNfcReaderMode()
        } catch (e: Exception) {
            logger.debug(":::Error while enabling nfc is:::".plus(e.localizedMessage))
            e.printStackTrace()
            disableNfcOnError()
            return NfcResponse("", responseData!!)
        }
    }

    private fun enableNfcReaderMode(): NfcResponse? {
        if (nfcAdapter != null) {
            nfcAdapter.enableReaderMode(
                NiblMerchant.getActivityContext() as Activity,
                { tag ->
                    onNfcDiscovered(tag)
                },
                NfcAdapter.FLAG_READER_NFC_A
                        or
                        NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK,
                null
            )
        } else {
            disableNfcOnError()
        }
        try {
            countDownLatch?.await()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (!nfcAdapter.isEnabled && responseData == null) {
            logger.debug(":::Response data is null and nfcAdapter is not enabled so return null:::")
            return null
        }

        if (responseData != null) {
            logger.debug(":::Response data is not null so validate source:::")
            return validateSource()
        } else {
            logger.debug(":::Response data null so display message:::")
            throw Exception(ERROR_MOBILE_NFC)
        }
    }

    private fun validateSource(): NfcResponse? {
        val responseCode = responseData?.let { DeviceBytesUtil.bytes2HexString(responseData) }
        responseCode.let {
            if (responseCode == INVALID_SOURCE_CODE_CARD || responseCode == INVALID_SOURCE_CODE_MOBILE) {
                displaySoundAndLedWithRespectiveDevice(LedLight.YELLOW)
                notifier.notify(NFC_SOURCE_INVALID, ERROR_NOT_VALID_NFC)
                return null
            } else {
                return selectedAid?.let { it1 ->
                    responseData?.let { it2 ->
                        NfcResponse(
                            aid = it1,
                            nfcData = it2
                        )
                    }
                }
            }
        }
    }

    private fun onNfcDiscovered(tag: Tag) {
        try {
            isoDep = IsoDep.get(tag)
            isoDep.connect()
            if (isoDep.isConnected) {
                getDataFromIsoDep(isoDep)
            } else {
                logger.debug(":::Tag technology closed:::")
                isoDep.close()
                cleanMobileNfcTask()
            }
        } catch (e: Exception) {
            logger.debug(":::Error occurred while getting nfc data is:::".plus(e.localizedMessage))
            e.printStackTrace()
            disableNfcOnError()
        }
    }

    private fun getDataFromIsoDep(isoDep: IsoDep) {
        notifier.notify(STARTING_READ_MOBILE_NFC, READ_NFC_MOBILE)
        isoDepWaitingTimer.start()

        for (aid in listAid) {
            responseData = try {
                isoDep.transceive(stringToByteArray(aid))

            } catch (e: Exception) {
                ERROR_MOBILE_NFC.toByteArray()
            }
            val responseCode = responseData?.let { DeviceBytesUtil.bytes2HexString(responseData) }
            logger.debug("RESPONSE DATA: ${responseCode.toString()}")
            logger.debug("RECEIVED NFC DATA: ${responseData?.contentToString()}")
            if (responseCode == INVALID_NFC_CARD || responseCode == INVALID_SOURCE_CODE_CARD) {
                continue
            } else {
                selectedAid = aid
                break
            }
        }
        cleanMobileNfcTask()
    }

    private fun disableNfcOnError() {
        cleanMobileNfcTask()
        throw Exception(ERROR_MOBILE_NFC)
        //responseData = ERROR_MOBILE_NFC.toByteArray()
    }

    override fun disableNfcReaderMode() {
        nfcAdapter?.disableReaderMode(NiblMerchant.getActivityContext() as Activity)
    }

    private fun stringToByteArray(aid: String): ByteArray {
        val lengthOfAid = aid.length / 2
        val formattedLength = String.format("%02d", lengthOfAid)


        if (AppUtility.isAppVariantNIBL()) {
            if (aid == "A000029524" || aid == "F222222124" || aid == "F222222123") {
                val len = ("00A40400$formattedLength$aid").length / 2
                val result = ByteArray(len)
                for (i in 0 until len) {
                    result[i] =
                        Integer.valueOf(
                            "00A40400$formattedLength$aid".substring(2 * i, 2 * i + 2),
                            16
                        )
                            .toByte()
                }
                return result
            } else {
                val len = ("00A40400$formattedLength$aid".plus("00")).length / 2
                val result = ByteArray(len)
                for (i in 0 until len) {
                    result[i] =
                        Integer.valueOf(
                            "00A40400$formattedLength$aid".plus("00").substring(2 * i, 2 * i + 2),
                            16
                        )
                            .toByte()
                }
                return result
            }
        } else {
            if (!AppUtility.isLcRequiredForNfcData(aid)) {
                val len = ("00A40400$formattedLength$aid").length / 2
                val result = ByteArray(len)
                for (i in 0 until len) {
                    result[i] =
                        Integer.valueOf(
                            "00A40400$formattedLength$aid".substring(2 * i, 2 * i + 2),
                            16
                        )
                            .toByte()
                }
                return result
            } else {
                val len = ("00A40400$formattedLength$aid".plus("00")).length / 2
                val result = ByteArray(len)
                for (i in 0 until len) {
                    result[i] =
                        Integer.valueOf(
                            "00A40400$formattedLength$aid".plus("00").substring(2 * i, 2 * i + 2),
                            16
                        )
                            .toByte()
                }
                return result
            }
        }
    }

    override fun addMobileNfcData(mobileNfcPaymentStatusRequest: MobileNfcPaymentStatusRequest): Response {
        logger.log("NFC PAYMENT REQUEST: " + Jsons.toJsonObj(mobileNfcPaymentStatusRequest))
        val jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.post(
            "/nfcengine/nfc/payments/",
            Jsons.toJsonObj(mobileNfcPaymentStatusRequest),
            false
        )
        logger.debug(jsonResponse)
        return Jsons.fromJsonToObj(jsonResponse, Response::class.java)
    }

    override fun checkMobileNfcPaymentStatus(
        response: Any
    ): MobileNfcPaymentStatusResponse {
        val jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.post(
            "/nfcengine/nfc/payments/status/",
            Jsons.toJsonObj(response),
            false
        )
        logger.debug(jsonResponse)
        return Jsons.fromJsonToObj(
            Jsons.toJsonObj(Jsons.fromJsonToObj(jsonResponse, Response::class.java).data),
            MobileNfcPaymentStatusResponse::class.java
        )
    }

    override fun parseNfcData(nfcData: ByteArray, aid: String): MobileNfcPaymentGenerateRequest {
        logger.debug("Selected AID:" + aid)
        if (aid == "F222222124") {
            val parser =
                MobileNfcTlvDataParser()
            val data = parser.parse(
                decryptData(
                    nfcData
                )
            )
            disableNfcReaderMode()
            return MobileNfcPaymentGenerateRequest(
                data[DATA_PARSER_FIRST].toString(),
                data[DATA_PARSER_SECOND].toString(),
                data[DATA_PARSER_THIRD].toString()
            )
        } else {
            val data = String(nfcData)
            disableNfcReaderMode()

            if (AppUtility.isAppVariantNIBL()) {
                return MobileNfcPaymentGenerateRequest("NFCISR0001", "NQR", data)
            } else {
                val response = Jsons.fromJsonToObj(
                    PreferenceManager.getAidResponse(""),
                    AidParameterResponse::class.java
                )

                val aidToProviderMapping = response.providerMapping

                val provider = aidToProviderMapping[aid]
                val enumAid = Aid.fromAid(aid)

                if (provider != null && enumAid != null) {
                    println("Aid: ${enumAid.aid}, Provider: $provider, lcRequired: ${response.networkConfig[provider]?.lcRequired}")
                }

                return MobileNfcPaymentGenerateRequest(enumAid!!.ISRCode, enumAid.payeerPan, data)
            }
        }
    }

    override fun prepareMobileNfcPaymentRequest(mobileNfcPaymentGenerateRequest: MobileNfcPaymentGenerateRequest): MobileNfcPaymentStatusRequest {
        var currencyCode: Int = ("" + DeviceConfiguration.get().currencyCode).toInt()
        if (currencyCode.toString().isEmpty()) {
            currencyCode = 524
        }
        return MobileNfcPaymentStatusRequest(
            mobileNfcPaymentGenerateRequest.merchantId,
            mobileNfcPaymentGenerateRequest.terminalId,
            BigDecimal(mobileNfcPaymentGenerateRequest.amount),
            generateNfcPaymentIdentifiers(PAYMENT_REQUEST_NUMBER),
            currencyCode,
            generateNfcPaymentIdentifiers(PAYMENT_BILLING_NUMBER),
            generateNfcPaymentIdentifiers(PAYMENT_REFERENCE_NUMBER),
            getSystemCurrentDate("yyyyMMddHHmmssSSS"),
            mobileNfcPaymentGenerateRequest.initiationMethod,
            "This is token",
            mobileNfcPaymentGenerateRequest.issuerCode,
            mobileNfcPaymentGenerateRequest.payeerPan,
            mobileNfcPaymentGenerateRequest.nfcToken
        )
    }

    private fun generateNfcPaymentIdentifiers(key: String): String {
        return when (key) {
            PAYMENT_REQUEST_NUMBER, PAYMENT_BILLING_NUMBER -> key.plus(
                generateNfcPaymentIdentifiersWithoutKey()
            )
            else -> generateNfcPaymentIdentifiersWithoutKey().plus(key)
        }
    }

    private fun generateNfcPaymentIdentifiersWithoutKey(): String {
        return getSystemCurrentDate("yyMMdd").plus(getNfcPaymentIdentifiers())
    }

    private fun getSystemCurrentDate(pattern: String): String {
        return SimpleDateFormat(pattern, Locale.getDefault()).format(Date())
    }

    private fun decryptData(cypherText: ByteArray): String {
        try {
            val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
            cipher.init(
                Cipher.DECRYPT_MODE,
                SecretKeySpec("fincp3ac_y=z++0!0hsf4x1i6u#23pos".toByteArray(), "AES"),
                IvParameterSpec("1e910b7598a1040f".toByteArray())
            )
            val decryptedData = cipher.doFinal(cypherText)
            return String(decryptedData)
        } catch (e: Exception) {
            return ""
        }
    }

    private fun getNfcPaymentIdentifiers(): String {
        return StringUtils.ofRequiredLength(
            securePreference.retrieveData(
                nfcPaymentIdentifiersKey,
                "1"
            ), 6
        )
    }

    override fun incrementNfcPaymentIdentifiers() {
        var nfcPaymentIdentifiers = getNfcPaymentIdentifiers().toLong()
        if (nfcPaymentIdentifiers == StringUtils.ofRequiredLength("9", 6, 9).toLong()) {
            nfcPaymentIdentifiers = StringUtils.ofRequiredLength("0", 6).toLong()
        }
        nfcPaymentIdentifiers++
        securePreference.saveData(
            nfcPaymentIdentifiersKey,
            StringUtils.ofRequiredLength(nfcPaymentIdentifiers, 6)
        )
    }

    override fun saveNfcPaymentIdentifiers(nfcPaymentIdentifiers: String) {
        var generatedNumericOnlyFromAlphaNumeric = ""
        val matchStringWithPattern =
            patternToGetNumericOnlyFromAlphaNumeric.matcher(nfcPaymentIdentifiers)
        while (matchStringWithPattern.find()) {
            generatedNumericOnlyFromAlphaNumeric = matchStringWithPattern.group()
        }

        generatedNumericOnlyFromAlphaNumeric =
            if (generatedNumericOnlyFromAlphaNumeric.count() == 12) {
                generatedNumericOnlyFromAlphaNumeric.substring(generatedNumericOnlyFromAlphaNumeric.length / 2)
            } else {
                StringUtils.ofRequiredLength("0", 6)
            }

        var num: Int = generatedNumericOnlyFromAlphaNumeric.toInt()
        num++
        generatedNumericOnlyFromAlphaNumeric = String.format("%06d", num)

        securePreference.saveData(
            nfcPaymentIdentifiersKey,
            generatedNumericOnlyFromAlphaNumeric
        )
    }

    override fun cancelTimer() {
        disableNfcReaderMode()
        mobilePayTransactionWaitingTimer.cancel()
    }

    override fun displaySoundAndLedWithRespectiveDevice(ledLight: LedLight) {
        if (manufacturer == DEVICE_WEIPASS) {
            SoundSourceImpl(context).playSound(Sound.FAILURE)
            showAlertLed(ledLight)
        } else {
            showAlertLed(ledLight)
        }
    }

    override fun cleanMobileNfcTask() {
        cancelTimer()
        if (countDownLatch != null && countDownLatch!!.count > 0) {
            countDownLatch!!.countDown()
        }
    }

    private fun showAlertLed(ledLight: LedLight) {
        LedSourceImpl(context).doTurnLedWith(LedRequest(ledLight, LedAction.ON))
    }

    override fun hideAlertLed() {
        LedSourceImpl(context).doTurnLedWith(LedRequest(LedLight.ALL, LedAction.OFF))
    }
}