package global.citytech.finpos.merchant.presentation.base

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import global.citytech.common.Constants
import global.citytech.easydroid.core.mvvm.BaseActivity
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.utils.CustomProgressDialog
import global.citytech.finpos.merchant.utils.CustomAlertDialog
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finposframework.utility.StringUtils
import java.lang.ref.WeakReference


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/09/07 - 3:18 PM
*/

abstract class AppBaseActivity<T : androidx.databinding.ViewDataBinding, V : androidx.lifecycle.ViewModel> :
    BaseActivity<T, V>() {
    private var customAlertDialog : CustomAlertDialog? = null
    private var customProgressDialog : CustomProgressDialog? = null
    private var toast: Toast? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        customAlertDialog = CustomAlertDialog(WeakReference(this).get())
        customProgressDialog = CustomProgressDialog(WeakReference(this))
        toast = Toast.makeText(this,"", Toast.LENGTH_LONG)
    }

    fun maskInitiatorId(initiatorId: String, start: Int): String {
        return StringUtils.maskString(initiatorId, start, initiatorId.length - 2, 'X')
    }

    fun showAmountConfirmationMessage(messageBuilder: MessageConfig.Builder) {
        customAlertDialog?.showAmountConfirmationDialog(messageBuilder.build())
    }

    fun showConfirmationMessage(messageBuilder: MessageConfig.Builder) {
        customAlertDialog?.showConfirmationDialog(messageBuilder.build())
    }
    fun hideConfirmationMessage() {
        customAlertDialog?.dismissDialog()
    }


    fun showFailureMessage(messageBuilder: MessageConfig.Builder) {
        customAlertDialog?.showFailureMessage(messageBuilder.build())
    }

    fun showGreenPinFailureMessage(messageBuilder: MessageConfig.Builder) {
        customAlertDialog?.showNewFailureMessage(messageBuilder.build())
    }

    fun showSuccessMessage(messageBuilder: MessageConfig.Builder) {
        customAlertDialog?.showSuccessMessage(messageBuilder.build())
    }

    fun showGreenPinSuccessMessage(messageBuilder: MessageConfig.Builder) {
        customAlertDialog?.showNewSuccessMessage(messageBuilder.build())
    }

    fun updateTimerMessage(message: String) {
        customAlertDialog?.updateTimerMessage(message)
    }

    fun updateButtonLabel(label: String) {
        customAlertDialog?.updateButtonLabel(label)
    }

    fun showNormalMessage(messageBuilder: MessageConfig.Builder) {
        customAlertDialog?.showNormalMessage(messageBuilder.build())
    }

    fun dismissDialog() {
        customAlertDialog?.dismissDialog()
    }

    fun showProgress(message: String? = this.getString(R.string.title_please_wait)) {
        customProgressDialog?.show(message!!)
    }

    fun showProgressIcon() {
        customProgressDialog?.show()
    }

    fun hideProgress() {
        customProgressDialog?.hide()
    }

    fun clearProgress() {
        this.hideProgress()
    }

    fun showToast(message:String?) {
        if (message.isNullOrEmpty()) {
            return
        }
        if (toast?.view?.isShown == true) {
            toast?.cancel()
        }
        toast?.setText(message)
        toast?.show()
    }

    fun showToast(message:String?,length: Int) {
        if (message.isNullOrEmpty()) {
            return
        }
        toast?.duration  = length
        if (toast?.view?.isShown == true) {
            toast?.cancel()
        }
        toast?.setText(message)
        toast?.show()
    }

    var reprintBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent!!.action == Constants.INTENT_ACTION_REPRINT_UI) {
                handleReprintReceiptBroadcast(intent)
            }
        }
    }

    protected fun handleReprintReceiptBroadcast(intent: Intent) {
        val message = intent.getStringExtra(Constants.KEY_REPRINT_MESSAGE)
        message.let {
            if (message.isNullOrEmptyOrBlank()) {
                getString(R.string.msg_default_reprint)
            }
        }
        val messageConfig: MessageConfig.Builder = MessageConfig.Builder()
            .title("REPRINT")
            .message(message)
            .positiveLabel(getString(R.string.action_reprint))
            .onPositiveClick {
                sendReprintBroadCast(true)
            }
            .negativeLabel(getString(R.string.action_cancel))
            .onNegativeClick {
                sendReprintBroadCast(false)
            }
        showConfirmationMessage(messageConfig)
    }

    private fun sendReprintBroadCast(reprint: Boolean) {
        val intent = Intent(Constants.INTENT_ACTION_REPRINT_SERVICE)
        intent.putExtra(Constants.KEY_BROADCAST_EXTRA_REPRINT, reprint)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    protected fun registerReprintBroadcastReceivers() {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(reprintBroadcastReceiver, IntentFilter(Constants.INTENT_ACTION_REPRINT_UI))
    }

    protected fun unregisterReprintBroadCastReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(reprintBroadcastReceiver)
    }

    override fun onDestroy() {
        unregisterReprintBroadCastReceivers()
        customAlertDialog?.dismissDialog()
        customAlertDialog = null
        customProgressDialog?.dismissDialog()
        customProgressDialog = null
        toast?.cancel()
        toast=null
        super.onDestroy()
    }

    fun hideSoftKeyboard(activity: Activity, view: View){
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}