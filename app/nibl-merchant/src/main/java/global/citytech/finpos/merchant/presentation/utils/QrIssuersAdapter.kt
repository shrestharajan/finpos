package global.citytech.finpos.merchant.presentation.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import global.citytech.finpos.merchant.R
import kotlinx.android.synthetic.main.item_qr_issuer.view.*

class QrIssuersAdapter(
    private var issuerUrls: List<*>
) : RecyclerView.Adapter<QrIssuersAdapter.QrIssuersViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QrIssuersViewHolder {
        context = parent.context
        return QrIssuersViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_qr_issuer, parent, false)
        )
    }

    override fun onBindViewHolder(holder: QrIssuersViewHolder, position: Int) {
        Glide.with(context).load(issuerUrls[position]).into(holder.itemView.iv_qr_issuer)
    }

    override fun getItemCount(): Int = issuerUrls.size

    inner class QrIssuersViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }
}