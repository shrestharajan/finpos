package global.citytech.finpos.merchant.presentation.dashboard.menu

import global.citytech.finpos.merchant.R
import global.citytech.finposframework.usecases.TransactionType


/**
 * Created by Saurav Ghimire on 1/19/21.
 * Citytech
 * saurav.ghimire@citytech.global
 *
 * Modified by Rishav Chudal on 5/10/21.
 * Citytech
 * rishav.chudal@citytech.global
 */

class MenuUtils {

    companion object {
        fun getMenuBasedOnEnabledTransactions(
            allowedTransactions: List<TransactionType>
        ): List<MenuItem> {
            val menus = ArrayList<MenuItem>()
            this.addAllowedTransactionsToMenus(menus, allowedTransactions)
            return menus
        }

        private fun addAllowedTransactionsToMenus(
            menus: ArrayList<MenuItem>,
            allowedTransactions: List<TransactionType>
        ) {
            for (transactionType in allowedTransactions) {
                when (transactionType) {
                    TransactionType.PRE_AUTH -> menus.add(menuItemPreAuth())
                    TransactionType.VOID -> menus.add(menuItemVoid())
                    TransactionType.REFUND -> menus.add(menuItemRefund())
                    TransactionType.AUTH_COMPLETION -> menus.add(menuItemAuthCompletion())
                    TransactionType.TIP_ADJUSTMENT -> menus.add(menuItemTipAdjustment())
                    TransactionType.CASH_IN -> menus.add(menuItemCashIn())
                    TransactionType.CASH_VOID -> menus.add(menuItemCashVoid())
                    TransactionType.BALANCE_INQUIRY -> menus.add(menuItemBalanceInquiry())
                    TransactionType.MINI_STATEMENT -> menus.add(menuItemMiniStatement())
                    TransactionType.PIN_CHANGE -> menus.add(menuItemPinChange())
                    TransactionType.GREEN_PIN -> menus.add(menuItemGreenPin())
                    else -> {
                    }
                }
            }
        }

        fun menuItemPurchase(): MenuItem =
            MenuItem(menu = Menu.SALES, menuHandler = PurchaseHandler())

        private fun menuItemPreAuth(): MenuItem =
            MenuItem(menu = Menu.PRE_AUTHORIZATION, menuHandler = PreAuthHandler())

        private fun menuItemVoid(): MenuItem =
            MenuItem(menu = Menu.VOID, menuHandler = VoidSaleHandler())

        private fun menuItemAuthCompletion(): MenuItem =
            MenuItem(menu = Menu.AUTHORIZATION_COMPLETION, menuHandler = AuthCompletionHandler())

        private fun menuItemRefund(): MenuItem =
            MenuItem(menu = Menu.REFUND, menuHandler = RefundHandler())

        private fun menuItemTipAdjustment(): MenuItem =
            MenuItem(menu = Menu.TIP_ADJUSTMENT, menuHandler = TipAdjustmentHandler())

        private fun menuItemMerchantMenu(): MenuItem =
            MenuItem(menu = Menu.MERCHANT_MENU, menuHandler = MerchantMenuHandler())

        private fun menuItemAdminMenu(): MenuItem =
            MenuItem(menu = Menu.ADMIN_MENU, menuHandler = AdminMenuHandler())

        private fun menuItemSupport(): MenuItem =
            MenuItem(menu = Menu.SUPPORT, menuHandler = SupportMenuHandler())

        private fun menuItemKeyExchange(): MenuItem =
            MenuItem(menu = Menu.KEY_EXCHANGE, menuHandler = KeyExchangeHandler())

        fun menuItemEmi(): MenuItem =
            MenuItem(menu = Menu.EMI, menuHandler = EMIHandler())

        private fun menuItemTransactions(): MenuItem =
            MenuItem(menu = Menu.TRANSACTIONS, menuHandler = TransactionsHandler())

        fun menuItemCashAdvance(): MenuItem =
            MenuItem(menu = Menu.CASH_ADVANCE, menuHandler = CashAdvanceMenuHandler())

        fun menuItemCashIn(): MenuItem =
            MenuItem(menu = Menu.CASH_IN, menuHandler = CashInHandler())

        private fun menuItemCashVoid(): MenuItem =
            MenuItem(menu = Menu.CASH_VOID, menuHandler = CashVoidHandler())

        fun menuItemBalanceInquiry(): MenuItem =
            MenuItem(menu = Menu.BALANCE_INQUIRY, menuHandler = BalanceInquiryHandler())

         fun menuItemMiniStatement(): MenuItem =
            MenuItem(menu = Menu.MINI_STATEMENT, menuHandler = MiniStatementHandler())

        fun menuItemQrCode(): MenuItem =
            MenuItem(menu = Menu.QR_CODE, menuHandler = QrCodeHandler())

        fun menuItemConnectFonePay(): MenuItem = MenuItem(
            menu = Menu.CONNECT_FONEPAY, menuHandler = ConnectFonePayHandler())

        fun menuItemPinChange(): MenuItem =
            MenuItem(menu = Menu.PIN_CHANGE, menuHandler = PinChangeHandler())

        fun menuItemGreenPin(): MenuItem =
            MenuItem(menu = Menu.GREEN_PIN, menuHandler = GreenPinHandler())


        fun addDefaultMenuItems(menus: MutableList<MenuItem>) {
            menus.add(menuItemTransactions())
            menus.add(menuItemMerchantMenu())
            menus.add(menuItemKeyExchange())
//          menus.add(menuItemSupport())
        }
    }
}

data class MenuItem(
    val menu: Menu? = null,
    val menuHandler: MenuHandler
)

/**
 * Note: Menu title should match with display name of enum Transaction Type
 */
enum class Menu(val title: Int, val icon: Int) {

    SALES(R.string.title_sale, R.drawable.ic_make_purchase_icon),
    PRE_AUTHORIZATION(R.string.title_pre_auth, R.drawable.ic_pre_authorization_icon),
    AUTHORIZATION_COMPLETION(R.string.title_auth_completion, R.drawable.ic_authorization_icon),
    REFUND(R.string.title_refund, R.drawable.ic_refund_icon),
    TIP_ADJUSTMENT(R.string.title_tip_adjustment, R.drawable.ic_tips),
    VOID(R.string.title_auth_void, R.drawable.ic_void_icon),

    MERCHANT_MENU(R.string.title_merchant_menu, R.drawable.ic_merchant_icon),
    ADMIN_MENU(R.string.title_admin_menu, R.drawable.ic_admin_menu_icon),
    SUPPORT(R.string.title_support, R.drawable.ic_baseline_local_phone_24),
    TRANSACTIONS(R.string.title_transactions, R.drawable.ic_tip_adjustment_icon),
    EMI(R.string.title_emi, R.drawable.ic_emi),
    KEY_EXCHANGE(R.string.title_key_exchange, R.drawable.ic_logon),

    CASH_ADVANCE(R.string.title_cash_advance, R.drawable.ic_cash_advance),
    CASH_IN(R.string.title_cash_in, R.drawable.ic_cash_in),
    BALANCE_INQUIRY(
        R.string.title_balance_inquiry,
        R.drawable.ic_baseline_account_balance_wallet_24
    ),
    MINI_STATEMENT(R.string.title_mini_statement, R.drawable.ic_mini_statement),
    CASH_VOID(R.string.title_cash_advance_void, R.drawable.ic_void_icon),

    QR_CODE(R.string.qr_code,R.drawable.ic_baseline_qr_code_scanner_24),
    CONNECT_FONEPAY(R.string.connect_fonepay, R.drawable.ic_fone_pay_connect),

    PIN_CHANGE(R.string.pin_change, R.drawable.ic_pin_change),
    GREEN_PIN(R.string.green_pin, R.drawable.ic_green_pin_icon)
}