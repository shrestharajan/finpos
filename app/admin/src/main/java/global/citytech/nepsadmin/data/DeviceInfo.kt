package global.citytech.nepsadmin.data

data class DeviceInfo(
    var serialNumber: String? = null
)