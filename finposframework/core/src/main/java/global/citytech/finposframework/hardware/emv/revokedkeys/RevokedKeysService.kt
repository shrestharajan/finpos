package global.citytech.finposframework.hardware.emv.revokedkeys

import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.DeviceService

/**
 * Created by Rishav Chudal on 4/30/20.
 */
interface RevokedKeysService: DeviceService {
    fun injectRevokedKeys(injectionRequest: RevokedKeysInjectionRequest): DeviceResponse

    fun eraseAllRevokedKeys(): DeviceResponse
}