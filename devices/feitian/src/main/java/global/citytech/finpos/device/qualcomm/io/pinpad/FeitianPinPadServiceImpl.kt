package global.citytech.finpos.device.qualcomm.io.pinpad

import android.content.Context
import com.ftpos.library.smartpos.emv.Emv
import global.citytech.finpos.device.qualcomm.io.cards.handler.FeitianPinPadHandler
import global.citytech.finpos.device.qualcomm.io.cards.transaction.FeitianSingleton
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finpos.device.qualcomm.utils.FeitianPinEntryResult
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse
import global.citytech.finposframework.hardware.keymgmt.pin.PinService

/**
 * Created by Rishav Chudal on 4/15/21.
 */
class FeitianPinPadServiceImpl(val context: Context): PinService, FeitianLogger.Listener {

    private lateinit var pinResponse: PinResponse
    private val logger = FeitianLogger(this)

    override fun showPinPadAndCalculatePinBlock(pinRequest: PinRequest): PinResponse {
        this.pinResponse = PinResponse(
            Result.PINPAD_ERROR,
            null
        )
        this.printLogsOfPinRequest(pinRequest)
        val pinPadHandler = FeitianPinPadHandler(
            this.context,
            Emv.EMV_CVMFLAG_OLPIN_SIGN.toInt(),
            pinRequest
        )
        val pinEntryResult = pinPadHandler.showPinPadAndCalculatePinBlock()
        this.analyzePinEntryResult(pinEntryResult)
        return this.pinResponse
    }

    private fun analyzePinEntryResult(pinEntryResult: FeitianPinEntryResult) {
        when (pinEntryResult.result) {
            Result.SUCCESS -> {
                pinResponse = PinResponse(
                    result = pinEntryResult.result,
                    pinBlock = FeitianSingleton.pinBlock
                )
            }

            else -> this.pinResponse = PinResponse(
                result = pinEntryResult.result,
                pinBlock = null
            )
        }
    }

    private fun printLogsOfPinRequest(pinRequest: PinRequest) {
        this.logger.logWithClassName("PAN ::: ".plus(pinRequest.primaryAccountNumber))
        this.logger.logWithClassName("Pin Block Format ::: ".plus(pinRequest.pinBlockFormat))
        this.logger.logWithClassName("Online Pin Key Index ::: ".plus(pinRequest.pinKeyIndex))
        this.logger.logWithClassName("Online Pin Key Type ::: ".plus(pinRequest.keyType))
        this.logger.logWithClassName("TimeOut ::: ".plus(pinRequest.pinPadTimeout))
        this.logger.logWithClassName("PinPad Message ::: ".plus(pinRequest.pinPadMessage))
        this.logger.logWithClassName("Amount Message ::: ".plus(pinRequest.amountMessage))
        this.logger.logWithClassName("Pin max length ::: ".plus(pinRequest.pinMaxLength))
        this.logger.logWithClassName("Pin min length ::: ".plus(pinRequest.pinMinLength))
    }

    override fun getClassName() = FeitianPinPadServiceImpl::class.java.simpleName
}