package global.citytech.finpos.merchant.framework.datasource.core.ministatement

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.ministatement.MiniStatementDataSource
import global.citytech.finpos.merchant.domain.model.ministatement.MiniStatementRequestEntity
import global.citytech.finpos.merchant.domain.model.ministatement.MiniStatementResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionAuthenticatorImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.*
import global.citytech.finpos.merchant.presentation.data.mapToGreenPinResponseUiModel
import global.citytech.finpos.merchant.presentation.data.mapToMiniStatementResponseUiModel
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.greenpin.GreenPinRequestItem
import global.citytech.finpos.merchant.presentation.model.ministatement.MiniStatementRequestItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finposframework.switches.ProcessorManager
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest
import io.reactivex.Observable

class MiniStatementDataSourceImpl : MiniStatementDataSource {
    override fun generateMiniStatementRequest(
        configurationItem: ConfigurationItem,
        miniStatementRequestItem: MiniStatementRequestItem
    ): Observable<MiniStatementResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val transactionRepository =
            TransactionRepositoryImpl(
                NiblMerchant.INSTANCE
            )
        val transactionRequest = prepareTransactionRequest(miniStatementRequestItem)

        val deviceController = DeviceControllerImpl(NiblMerchant.INSTANCE)
        val cardReader = CardSourceImpl(NiblMerchant.getActivityContext())
        val transactionAuthenticator =
            TransactionAuthenticatorImpl()
        val printerServiceImpl = PrinterSourceImpl(NiblMerchant.INSTANCE)
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)

        val miniStatementRequestEntity = MiniStatementRequestEntity(
            readCardService = cardReader,
            deviceController = deviceController,
            printerService = printerServiceImpl,
            transactionAuthenticator = transactionAuthenticator,
            transactionRepository = transactionRepository,
            transactionRequest = transactionRequest,
            applicationRepository = applicationRepository,
            ledService = LedSourceImpl(NiblMerchant.INSTANCE),
            soundService = SoundSourceImpl(NiblMerchant.INSTANCE)
        )

        val miniStatementRequester = ProcessorManager.getInterface(
            terminalRepository,
            NotificationHandler
        ).miniStatementRequester

        return Observable.fromCallable {
            miniStatementRequester.execute(
                miniStatementRequestEntity.mapToModel()
            ).mapToMiniStatementResponseUiModel()
        }

    }
    private fun prepareTransactionRequest(miniStatementRequestItem: MiniStatementRequestItem): TransactionRequest {
        val transactionRequest = TransactionRequest(
            miniStatementRequestItem.transactionType,
        )
        transactionRequest.amount = miniStatementRequestItem.amount
        return transactionRequest
    }
}