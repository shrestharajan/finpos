package global.citytech.finposframework.usecases;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.EMVTagCollection;
import global.citytech.finposframework.hardware.io.cards.TransactionState;

public class SetOnlineResultResponse {

    private Result result;
    private String message;
    private String iccDataBlock;
    private EMVTagCollection emvTagCollection;
    private TransactionState transactionState;

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }

    public String getIccDataBlock() {
        return iccDataBlock;
    }

    public EMVTagCollection getEmvTagCollection() {
        return emvTagCollection;
    }

    public TransactionState getTransactionState() {
        return transactionState;
    }

    public static final class SetOnlineResultResponseBuilder {
        private Result result;
        private String message;
        private String iccDataBlock;
        private EMVTagCollection emvTagCollection;
        private TransactionState transactionState;

        public SetOnlineResultResponseBuilder() {
        }

        public SetOnlineResultResponseBuilder withResult(Result result) {
            this.result = result;
            return this;
        }

        public SetOnlineResultResponseBuilder withMessage(String message) {
            this.message = message;
            return this;
        }

        public SetOnlineResultResponseBuilder withIccDataBlock(String iccDataBlock) {
            this.iccDataBlock = iccDataBlock;
            return this;
        }

        public SetOnlineResultResponseBuilder withEmvTagCollection(EMVTagCollection emvTagCollection) {
            this.emvTagCollection = emvTagCollection;
            return this;
        }

        public SetOnlineResultResponseBuilder withTransactionState(TransactionState transactionState) {
            this.transactionState = transactionState;
            return this;
        }

        public SetOnlineResultResponse build() {
            SetOnlineResultResponse setOnlineResultResponse = new SetOnlineResultResponse();
            setOnlineResultResponse.iccDataBlock = this.iccDataBlock;
            setOnlineResultResponse.emvTagCollection = this.emvTagCollection;
            setOnlineResultResponse.transactionState = this.transactionState;
            setOnlineResultResponse.result = this.result;
            setOnlineResultResponse.message = this.message;
            return setOnlineResultResponse;
        }
    }
}
