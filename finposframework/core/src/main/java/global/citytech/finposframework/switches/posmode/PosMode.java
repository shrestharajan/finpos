package global.citytech.finposframework.switches.posmode;

/**
 * Created by Unique Shakya on 1/7/2021.
 */
public enum PosMode {
    RETAILER,
    CASH
}
