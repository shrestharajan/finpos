package global.citytech.finposframework.switches.posmode;

import global.citytech.finposframework.switches.ResponseParameter;

/**
 * Created by Unique Shakya on 1/7/2021.
 */
public interface PosModeResponseParameter extends ResponseParameter {
}
