package global.citytech.finpos.merchant.presentation.model.response

import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank

/**
 * Created by Unique Shakya on 10/4/2021.
 */
data class TlsCertificate(
    var certificate: String?,
    var tlsVersion: String?,
    var certificateKey: String?,
    var certificateFactoryType: String?,
    var expiryDate: String?
) {
    companion object {
        const val COLUMN_TLS_CERTIFICATE = "tls_certificate"
    }

    fun mapToJson(): String {
        return Jsons.toJsonObj(this)
    }
}

fun String?.mapToTlsCertificate(): TlsCertificate? {
    return if (this.isNullOrEmptyOrBlank())
        null
    else if (!Jsons.isValidJsonString(this))
        null
    else {
        Jsons.fromJsonToObj(this, TlsCertificate::class.java)
    }
}