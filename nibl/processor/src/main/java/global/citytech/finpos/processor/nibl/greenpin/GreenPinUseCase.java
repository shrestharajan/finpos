package global.citytech.finpos.processor.nibl.greenpin;


import java.util.concurrent.CountDownLatch;
import java.util.List;

import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.S2MIccDataList;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finpos.processor.nibl.NiblConstants;
import global.citytech.finpos.processor.nibl.greenpin.pinset.PinSetUseCase;
import global.citytech.finpos.processor.nibl.transaction.TransactionRequestModel;
import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor;
import global.citytech.finpos.processor.nibl.transaction.card.MagneticCardProcessor;
import global.citytech.finpos.processor.nibl.transaction.card.PICCCardProcessor;
import global.citytech.finpos.processor.nibl.transaction.template.CardTransactionUseCase;
import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils;
import global.citytech.finpos.processor.nibl.transaction.validator.NiblTransactionValidator;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardSummary;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.PinBlock;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.led.LedAction;
import global.citytech.finposframework.hardware.io.led.LedLight;
import global.citytech.finposframework.hardware.io.led.LedRequest;
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat;
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest;
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.listeners.CardConfirmationListener;
import global.citytech.finposframework.listeners.OtpConfirmationListener;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.CardProcessor;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.PosResponse;
import global.citytech.finposframework.utility.PosResult;
import global.citytech.finposframework.utility.StringUtils;

public class GreenPinUseCase extends CardTransactionUseCase {
    ReadCardRequest readCardRequest;
    private String OTP ="";
    protected final ReadCardRequest.MultipleAidSelectionListener multipleAidSelectionListener = aids -> {
        notifier.notify(Notifier.EventType.DETECTED_MULTIPLE_AID, "Processing..");
        return -1;
    };

    private CardProcessor cardProcessor;

    public GreenPinUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy, String additionalAmount) {
        return null;
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy) {
        return null;
    }

    @Override
    protected String getTransactionTypeClassName() {
        return null;
    }

    @Override
    public TransactionResponseModel execute(TransactionRequestModel request) {
        this.request = request;
        this.transactionRequest = this.request.getTransactionRequest();
        if (this.deviceNotReady())
            throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
        this.transactionValidator = new NiblTransactionValidator(this.request.getApplicationRepository());
        this.stan = this.terminalRepository.getSystemTraceAuditNumber();
        try {
            this.initTransaction();
            return this.initializeTransactionWithCard(TransactionUtils.getAllowedCardEntryModes(this.transactionRequest.getTransactionType()), false);
        } catch (PosException pe) {
            pe.printStackTrace();
            throw pe;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } finally {
            this.cleanup();
        }
    }

    protected boolean deviceNotReady() {
        PosResponse deviceReadyResponse = this.request.getDeviceController().isReady(TransactionUtils
                .getAllowedCardEntryModes(this.transactionRequest.getTransactionType()));
        if (deviceReadyResponse.getPosResult() == PosResult.SUCCESS) {
            return false;
        }
        this.notifier.notify(Notifier.EventType.DETECT_CARD_ERROR);
        throw new PosException(deviceReadyResponse.getPosError());
    }

    protected PurchaseRequest preparePurchaseRequest(TransactionRequest transactionRequest, CardDetails cardDetails) {
        PurchaseRequest purchaseRequest = new PurchaseRequest(
                transactionRequest.getTransactionType(),
                transactionRequest.getAmount(),
                transactionRequest.getAdditionalAmount()
        );
        cardDetails.setAmount(transactionRequest.getAmount());
        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
        purchaseRequest.setCardDetails(cardDetails);
        return purchaseRequest;
    }


    private TransactionResponseModel initializeTransactionWithCard(List<CardType> allowedCardEntryModes, boolean isForceContactCardPrompt) throws InterruptedException {
        this.retrieveCardDetails(allowedCardEntryModes, isForceContactCardPrompt);
        if (this.invalidTransaction())
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
        if (this.isTransactionAuthorizationFailure()) {
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
        }
        if (isCardConfirmationRequired) {
            final boolean[] isConfirm = {false};
            CountDownLatch countDownLatch = new CountDownLatch(1);
            this.notifier.notify(Notifier.EventType.CARD_CONFIRMATION_EVENT,
                    readCardResponse.getCardDetails(),
                    (CardConfirmationListener) confirmed -> {
                        isConfirm[0] = confirmed;
                        countDownLatch.countDown();
                    });
            countDownLatch.await();
            if (!isConfirm[0]) {
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            }
        }

        this.notifier.notify(Notifier.EventType.PROCESSING, "Please wait...");
        proceedOtpConfirmation();
        PinSetUseCase pinSetUseCase = new PinSetUseCase(this.terminalRepository, this.notifier);
        pinSetUseCase.setCardResponse(this.readCardResponse, this.transactionRequest, this.request, this.stan, OTP, this.cardProcessor);
        TransactionResponseModel pinSetTransactionResponse = pinSetUseCase.execute(request);
        if (pinSetTransactionResponse.isApproved()) {
            IsoMessageResponse isoMessageResponse = null;
            return prepareResponse(isoMessageResponse, true, false);
        } else {
            return prepareDeclinedTransactionResponse(pinSetTransactionResponse.getMessage(), false);
        }
    }

    void proceedOtpConfirmation() throws InterruptedException {
        final boolean[] isConfirm = {false};
        CountDownLatch countDownLatch = new CountDownLatch(1);
        try {
            this.notifier.notify(Notifier.EventType.OTP_CONFIRMATION_EVENT,
                    readCardResponse.getCardDetails(),
                    (OtpConfirmationListener) (confirmed, otp) -> {
                        isConfirm[0] = confirmed;
                        this.OTP = otp;
                        countDownLatch.countDown();
                    });
        } catch (Exception e) {

        }
        countDownLatch.await();
        if (!isConfirm[0]) {
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        }
    }

    protected void retrieveCardDetails(List<CardType> allowedCardTypes, boolean isForceContactCardPrompt) {
         readCardRequest = this.prepareReadCardRequest(allowedCardTypes);
         readCardRequest.setPinpadRequired(false);
        if (isForceContactCardPrompt)
            this.notifier.notify(Notifier.EventType.FORCE_ICC_CARD, Notifier.EventType.FORCE_ICC_CARD.getDescription());
        else
            this.notifier.notify(Notifier.EventType.STARTING_READ_CARD);
        this.readCardResponse = this.detectCard(readCardRequest);
        this.retrieveCardProcessor();
        this.readCardResponse = this.cardProcessor.validateReadCardResponseForICC(readCardRequest, this.readCardResponse);
    }

    private void retrieveCardProcessor() {
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case MAG:
                this.isCardConfirmationRequired = true;
                this.cardProcessor = new MagneticCardProcessor(
                        this.request.getTransactionRepository(),
                        this.request.getTransactionAuthenticator(),
                        this.request.getApplicationRepository(),
                        this.request.getReadCardService(),
                        this.notifier,
                        terminalRepository
                );
                break;
            case ICC:
                this.isCardConfirmationRequired = true;
                this.cardProcessor = new IccCardProcessor(
                        this.request.getReadCardService(),
                        this.request.getTransactionRepository(),
                        this.request.getApplicationRepository(),
                        this.request.getTransactionAuthenticator(),
                        this.notifier,
                        terminalRepository
                );
                break;
            case PICC:
                this.cardProcessor = new PICCCardProcessor(
                        this.request.getTransactionRepository(),
                        this.request.getApplicationRepository(),
                        this.request.getReadCardService(),
                        this.notifier,
                        this.request.getTransactionAuthenticator(),
                        this.request.getLedService(),
                        this.request.getSoundService(),
                        terminalRepository
                );
                break;
            default:
                break;
        }
    }

    private ReadCardRequest prepareReadCardRequest(List<CardType> allowedCardTypes) {
        TransactionType transactionType = this.transactionRequest.getTransactionType();
        ReadCardRequest readCardRequest = new ReadCardRequest(
                allowedCardTypes,
                transactionType,
                TransactionUtils.retrieveTransactionType9C(transactionType),
                this.request.getApplicationRepository().retrieveEmvParameterRequest()
        );
        readCardRequest.setStan(this.stan);
        readCardRequest.setAmount(this.transactionRequest.getAmount());
        readCardRequest.setCashBackAmount(this.transactionRequest.getAdditionalAmount());
        readCardRequest.setTransactionDate(DateUtils.yyMMddDate());
        readCardRequest.setTransactionTime(DateUtils.HHmmssTime());
        readCardRequest.setMultipleAidSelectionListener(this.multipleAidSelectionListener);
        readCardRequest.setIccDataList(S2MIccDataList.get());
        readCardRequest.setCurrencyName(NiblConstants.PIN_PAD_CURRENCY);
        readCardRequest.setPinPadFixedLayout(!terminalRepository.shouldShufflePinPad());
        return readCardRequest;
    }

    private ReadCardResponse detectCard(ReadCardRequest readCardRequest) {
        this.request.getLedService().doTurnLedWith(new LedRequest(LedLight.BLUE, LedAction.ON));
        ReadCardResponse readCardResponse = this.request.getReadCardService().readCardDetails(readCardRequest);
        this.validateReadCardResponse(readCardResponse);
        return readCardResponse;
    }

    private void validateReadCardResponse(ReadCardResponse readCardResponse) {
        if (readCardResponse == null)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        if (readCardResponse.getResult() == Result.TIMEOUT)
            throw new PosException(PosError.DEVICE_ERROR_CARD_READ_TIMEOUT);
        if (readCardResponse.getResult() == Result.USER_CANCELLED)
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        if (readCardResponse.getResult() == Result.FAILURE)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
    }


    protected void processCard() {
        PurchaseRequest purchaseRequest = this.preparePurchaseRequest();
        this.readCardResponse = this.cardProcessor.processCard(this.stan, purchaseRequest);
    }

    protected String getClassName() {
        return null;
    }

    protected NIBLMessageSenderTemplate getRequestSender(RequestContext context) {
        return null;
    }

    protected String getOriginalPosConditionCode() {
        return null;
    }

    protected PosEntryMode getOriginalPosEntryMode() {
        return null;
    }

    protected void processOnlineResult(IsoMessageResponse isoMessageResponse) {

    }

    protected void updateTransactionLog(boolean transactionApproved, TransactionRequest transactionRequest, TransactionRepository transactionRepository, IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse) {

    }


    protected TransactionResponseModel prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, boolean shouldPrint) {
        return GreenPinResponse.Builder.newInstance()
                .withStan(stan)
                .withApproved(transactionApproved)
                .withMessage(transactionApproved ? "Your Pin has been updated." : "Cannot perform Task").build();
    }

    protected String getProcessingCode() {
        return null;
    }



    protected TransactionResponseModel prepareDeclinedTransactionResponse(String responseMessage, boolean shouldPrint) {
        return GreenPinResponse.Builder.newInstance()
                .withStan(this.stan)
                .withMessage(responseMessage)
                .withApproved(false)
                .build();
    }

    protected String getOriginalTransactionReferenceNumber() {
        return null;
    }

    protected boolean isAuthorizationCompleted() {
        return false;
    }

    protected void updateTransactionLogOfOriginalTransaction(boolean isApproved) {

    }

    protected boolean shouldIncrementInvoiceNumber(boolean isApproved) {
        return false;
    }

    protected TransactionLog retrieveTransactionLogToStore(boolean isApproved, IsoMessageResponse isoMessageResponse) {
        return null;
    }

}

