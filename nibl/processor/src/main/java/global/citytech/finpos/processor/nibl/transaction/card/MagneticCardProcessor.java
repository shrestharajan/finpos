package global.citytech.finpos.processor.nibl.transaction.card;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finpos.nibl.iso8583.S2MIccDataList;
import global.citytech.finpos.processor.nibl.NiblConstants;
import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardSummary;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.PinBlock;
import global.citytech.finposframework.hardware.io.cards.cvm.Cvm;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat;
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest;
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.CardSchemeRetrieveRequest;
import global.citytech.finposframework.repositories.CardSchemeRetrieveResponse;
import global.citytech.finposframework.repositories.FieldAttributes;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.IdentifyCardResponse;
import global.citytech.finposframework.usecases.MagneticSwipe;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.CardProcessor;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.ServiceCodeUtils;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finposframework.utility.TMSConfigurationConstants.*;

/**
 * Created by Unique Shakya on 10/16/2020.
 */
public class MagneticCardProcessor implements CardProcessor {

    private TransactionRepository transactionRepository;
    private TransactionAuthenticator transactionAuthenticator;
    private ApplicationRepository applicationRepository;
    private ReadCardService readCardService;
    private Notifier notifier;
    private TerminalRepository terminalRepository;
    private Logger logger = Logger.getLogger(MagneticCardProcessor.class.getName());

    public MagneticCardProcessor(
            TransactionRepository transactionRepository,
            TransactionAuthenticator transactionAuthenticator,
            ApplicationRepository applicationRepository,
            ReadCardService readCardService,
            Notifier notifier,
            TerminalRepository terminalRepository
    ) {
        this.transactionRepository = transactionRepository;
        this.transactionAuthenticator = transactionAuthenticator;
        this.applicationRepository = applicationRepository;
        this.readCardService = readCardService;
        this.notifier = notifier;
        this.terminalRepository = terminalRepository;
    }

    @Override
    public ReadCardResponse validateReadCardResponse(
            ReadCardRequest readCardRequest,
            ReadCardResponse readCardResponse
    ) {
        if (readCardResponse == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (readCardResponse.getResult() == Result.SWIPE_AGAIN) {
            readCardResponse = this.onReadCardResponseWaveAgain(readCardRequest, readCardResponse);
        }
        this.checkForExceptionCases(readCardResponse);
        if (this.checkForForceChipCardRead(readCardResponse)) {
            return this.forceChipCardRead(readCardRequest);
        }
        this.identifyCardScheme(readCardResponse);
        return readCardResponse;
    }

    @Override
    public ReadCardResponse validateReadCardResponseForICC(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse) {
        if (readCardResponse == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (readCardResponse.getResult() == Result.TIMEOUT) {
            throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
        }
        if (readCardResponse.getResult() == Result.APPLICATION_BLOCKED) {
            throw new PosException(PosError.DEVICE_ERROR_APPLICATION_BLOCKED);
        }
        if (readCardResponse.getResult() == Result.CARD_BLOCKED) {
            throw new PosException(PosError.DEVICE_ERROR_CARD_BLOCKED);
        }
        if (readCardResponse.getResult() == Result.USER_CANCELLED) {
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        }
        if (readCardResponse.getResult() == Result.PROCESSING_ERROR) {
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
        if (readCardResponse.getResult() == Result.ICC_FALLBACK_TO_MAGNETIC) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (readCardResponse.getResult() == Result.ICC_CARD_DETECT_ERROR) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (readCardResponse.getResult() == Result.CALLBACK_AMOUNT) {
            // TODO implement amount callback notify UI and HW SDK
        }
        if (readCardResponse.getResult() == Result.FAILURE || readCardResponse.getCardDetails() == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (StringUtils.isEmpty(readCardResponse.getCardDetails().getPrimaryAccountNumber()) ||
                StringUtils.isEmpty(readCardResponse.getCardDetails().getTrackTwoData())) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        CardIdentifier cardIdentifier = new CardIdentifier(transactionRepository, applicationRepository);
        IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(readCardResponse);
        readCardResponse.getCardDetails().setCardScheme(identifyCardResponse.getCardDetails().getCardScheme());
        return readCardResponse;
    }


    private ReadCardResponse forceChipCardRead(ReadCardRequest readCardRequest) {
        this.readCardService.cleanUp();
        this.notifier.notify(Notifier.EventType.SWITCH_MAGNETIC_TO_ICC,
                Notifier.EventType.SWITCH_MAGNETIC_TO_ICC.getDescription());
        ReadCardRequest contactlessFallbackReadCardRequest = this.prepareForceChipReadCardRequest(readCardRequest);
        ReadCardResponse readCardResponse = this.readCardService.readCardDetails(contactlessFallbackReadCardRequest);
        return new IccCardProcessor(this.readCardService, transactionRepository, applicationRepository,
                transactionAuthenticator, notifier, this.terminalRepository).validateReadCardResponse(readCardRequest, readCardResponse);
    }

    private ReadCardRequest prepareForceChipReadCardRequest(ReadCardRequest readCardRequest) {
        List<CardType> cardTypeList = new ArrayList<>();
        cardTypeList.add(CardType.ICC);
        ReadCardRequest forceChipReadRequest = new ReadCardRequest(cardTypeList, readCardRequest.getTransactionType(),
                readCardRequest.getTransactionType9C(), readCardRequest.getEmvParametersRequest());
        forceChipReadRequest.setCashBackAmount(readCardRequest.getCashBackAmount());
        forceChipReadRequest.setAmount(readCardRequest.getAmount());
        forceChipReadRequest.setCardDetailsBeforeEmvNext(readCardRequest.getCardDetailsBeforeEmvNext());
        forceChipReadRequest.setCardReadTimeOut(readCardRequest.getCardReadTimeOut());
        forceChipReadRequest.setFallbackFromIccToMag(readCardRequest.getFallbackFromIccToMag());
        forceChipReadRequest.setForceOnlineForICC(readCardRequest.getForceOnlineForICC());
        forceChipReadRequest.setForceOnlineForPICC(readCardRequest.getForceOnlineForPICC());
        forceChipReadRequest.setIccDataList(S2MIccDataList.get());
        forceChipReadRequest.setMaxmPinLength(readCardRequest.getMaxmPinLength());
        forceChipReadRequest.setMinmPinLength(readCardRequest.getMinmPinLength());
        forceChipReadRequest.setMultipleAidSelectionListener(readCardRequest.getMultipleAidSelectionListener());
        forceChipReadRequest.setPackageName(readCardRequest.getPackageName());
        forceChipReadRequest.setPinBlockFormat(readCardRequest.getPinBlockFormat());
        forceChipReadRequest.setPinpadRequired(readCardRequest.isPinpadRequired());
        forceChipReadRequest.setPinpadTimeout(readCardRequest.getPinpadTimeout());
        forceChipReadRequest.setStan(readCardRequest.getStan());
        forceChipReadRequest.setPrimaryAccountNumber(readCardRequest.getPrimaryAccountNumber());
        forceChipReadRequest.setTransactionDate(DateUtils.yyMMddDate());
        forceChipReadRequest.setTransactionTime(DateUtils.HHmmssTime());
        forceChipReadRequest.setCurrencyName(readCardRequest.getCurrencyName());
        forceChipReadRequest.setPinPadFixedLayout(readCardRequest.getPinPadFixedLayout());
        return forceChipReadRequest;
    }

    private boolean checkForForceChipCardRead(ReadCardResponse readCardResponse) {
        if (readCardResponse.getFallbackIccToMag()) {
            return false;
        } else {
            return ServiceCodeUtils.doesContainChipCard(readCardResponse.getCardDetails().getTrackTwoData());
        }
    }

    private void checkForExceptionCases(ReadCardResponse readCardResponse) {
        if (readCardResponse.getResult() == Result.TIMEOUT) {
            throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
        }

        if (readCardResponse.getResult() == Result.FAILURE ||
                readCardResponse.getCardDetails() == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (StringUtils.isEmpty(readCardResponse.getCardDetails().getTrackTwoData())) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    public void identifyCardScheme(ReadCardResponse readCardResponse) {
        global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new CardIdentifier(
                this.transactionRepository,
                this.applicationRepository
        );
        IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(readCardResponse);
        this.logger.debug("::: CARD SCHEME IN MAGNETIC PROCESSOR ::: " + identifyCardResponse.getCardDetails().getCardScheme());
        readCardResponse.getCardDetails().setCardScheme(
                identifyCardResponse
                        .getCardDetails()
                        .getCardScheme()
        );
    }

    @Override
    public ReadCardResponse processCard(String stan, PurchaseRequest purchaseRequest) {
        PinBlock pinBlock = new PinBlock("", true);
        Cvm cvm = this.retrieveCvm(
                purchaseRequest.getTransactionType(),
                purchaseRequest.getCardDetails()
        );
        boolean pinRequired = cvm.equals(Cvm.ENCIPHERED_PIN_ONLINE);
        String pinPadAmountMessage = TransactionUtils.retrievePinPadMessageWithAmount(
                purchaseRequest.getAmount(),
                this.applicationRepository.retrieveFromAdditionalDataEmvParameters(
                        KEY_TRANSACTION_CURRENCY_NAME
                )
        );
        if (pinRequired) {
            pinBlock = this.retrievePinBlock(
                    purchaseRequest
                            .getCardDetails(),
                    pinPadAmountMessage
            );
        }
        ReadCardResponse readCardResponse = new ReadCardResponse(
                purchaseRequest.getCardDetails(),
                Result.SUCCESS,
                "Successful"
        );
        readCardResponse.setCvm(cvm);
        readCardResponse.getCardDetails().setPinBlock(pinBlock);
        readCardResponse
                .getCardDetails()
                .setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
        readCardResponse.setFallbackIccToMag(purchaseRequest.isFallbackIccToMag());
        return readCardResponse;
    }

    @Override
    public ReadCardResponse processOnlineResult(
            ReadCardResponse readCardResponse,
            IsoMessageResponse isoMessageResponse
    ) {
        // not needed for magnetic card
        return readCardResponse;
    }

    @Override
    public ReadCardResponse processUnableToGoOnlineResult(ReadCardResponse readCardResponse) {
        return readCardResponse;
    }

    private Cvm retrieveCvm(TransactionType transactionType, CardDetails cardDetails) {
        /*String shortCardScheme = cardDetails.getCardScheme().getCardSchemeId();
        if (this.retrieveCardSchemeParameters(CardUtils.retrieveCardSchemeType(shortCardScheme),
                FieldAttributes.CARD_SCHEME_S1_F20_CHECK_SERVICE_CODE).equals("1")) {
            if (ServiceCodeUtils.isPinRequired(cardDetails.getTrackTwoData()))
                return Cvm.ENCIPHERED_PIN_ONLINE;
        }
        TransactionSetTableVerifier verifier = new TransactionSetTableVerifier(
                this.applicationRepository,
                shortCardScheme,
                TransactionUtils.mapPurchaseTypeWithTransactionType(transactionType));
        String cvmToPerform = verifier.getCardHolderVerificationMethod();
        switch (cvmToPerform) {
            case "1":
                return Cvm.SIGNATURE;
            case "2":
            case "3":
                return Cvm.ENCIPHERED_PIN_ONLINE;
            default:
                return Cvm.NO_CVM;
        }*/
        if (ServiceCodeUtils.isPinRequired(cardDetails.getTrackTwoData()))
            return Cvm.ENCIPHERED_PIN_ONLINE;
        return Cvm.SIGNATURE;
    }

    private String retrieveCardSchemeParameters(
            CardSchemeType cardSchemeType,
            FieldAttributes fieldAttributes
    ) {
        CardSchemeRetrieveRequest cardSchemeRetrieveRequest = new CardSchemeRetrieveRequest(
                cardSchemeType.getCardSchemeId(),
                fieldAttributes
        );
        CardSchemeRetrieveResponse cardSchemeRetrieveResponse
                = this.applicationRepository.retrieveCardScheme(cardSchemeRetrieveRequest);
        return cardSchemeRetrieveResponse.getData();
    }

    private PinBlock retrievePinBlock(CardDetails cardDetails, String pinPadAmountMessage) {
        String primaryAccountNumber = cardDetails.getPrimaryAccountNumber();
        if (!StringUtils.isEmpty(primaryAccountNumber)) {
            PinRequest pinRequest = new PinRequest(
                    primaryAccountNumber,
                    NiblConstants.PIN_PAD_MESSAGE,
                    PinBlockFormat.ISO9564_FORMAT_0,
                    4,
                    12,
                    30,
                    pinPadAmountMessage,
                    !this.terminalRepository.shouldShufflePinPad(),
                    false,
                    "Enter PIN"
            );
            CardSummary cardSummary = new CardSummary(cardDetails.getCardSchemeLabel(),
                    primaryAccountNumber, cardDetails.getCardHolderName(), cardDetails.getExpiryDate());
            pinRequest.setCardSummary(cardSummary);
            PinResponse response = transactionAuthenticator.authenticateUser(pinRequest);
            this.logger.debug("::: PIN RESPONSE ::: " + response.toString());
            if (response.getResult() == Result.SUCCESS)
                return response.getPinBlock();
            else if (response.getResult() == Result.USER_CANCELLED)
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            else if (response.getResult() == Result.TIMEOUT)
                throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
            else
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    private ReadCardResponse onReadCardResponseWaveAgain(
            ReadCardRequest readCardRequest,
            ReadCardResponse earlierReadCardResponse
    ) {
        if (MagneticSwipe.getInstance().isMagneticSwipeAgainAllowed()) {
            return onSwipeAgainAllowed(
                    readCardRequest,
                    earlierReadCardResponse
            );
        } else {
            MagneticSwipe.getInstance().onMagneticSwipeFailureLimitReached();
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    private ReadCardResponse onSwipeAgainAllowed(
            ReadCardRequest readCardRequest,
            ReadCardResponse earlierReadCardResponse
    ) {
        MagneticSwipe.getInstance().onMagneticSwipeFailure();
        this.notifier.notify(
                Notifier.EventType.SWIPE_AGAIN,
                Notifier.EventType.SWIPE_AGAIN.getDescription()
        );
        this.readCardService.cleanUp();
        ReadCardResponse readCardResponse = this.readCardService.readCardDetails(
                prepareSwipeAgainReadCardRequest(readCardRequest)
        );
        readCardResponse.setFallbackIccToMag(earlierReadCardResponse.getFallbackIccToMag());
        if (readCardResponse.getResult() == Result.SWIPE_AGAIN)
            return this.onReadCardResponseWaveAgain(readCardRequest, readCardResponse);
        if (readCardResponse.getResult() == Result.SUCCESS)
            MagneticSwipe.getInstance().onMagneticSwipeSuccess();
        this.checkForExceptionCases(readCardResponse);
        return readCardResponse;
    }

    private ReadCardRequest prepareSwipeAgainReadCardRequest(ReadCardRequest readCardRequest) {
        List<CardType> cardTypeList = new ArrayList<>();
        cardTypeList.add(CardType.MAG);
        ReadCardRequest swipeAgainReadCardRequest = new ReadCardRequest(
                cardTypeList,
                readCardRequest.getTransactionType(),
                readCardRequest.getTransactionType9C(),
                readCardRequest.getEmvParametersRequest()
        );
        swipeAgainReadCardRequest.setCashBackAmount(readCardRequest.getCashBackAmount());
        swipeAgainReadCardRequest.setAmount(readCardRequest.getAmount());
        swipeAgainReadCardRequest.setCardDetailsBeforeEmvNext(
                readCardRequest.getCardDetailsBeforeEmvNext()
        );
        swipeAgainReadCardRequest.setCardReadTimeOut(readCardRequest.getCardReadTimeOut());
        swipeAgainReadCardRequest.setFallbackFromIccToMag(
                readCardRequest.getFallbackFromIccToMag()
        );
        swipeAgainReadCardRequest.setForceOnlineForICC(
                readCardRequest.getForceOnlineForICC()
        );
        swipeAgainReadCardRequest.setForceOnlineForPICC(
                readCardRequest.getForceOnlineForPICC()
        );
        swipeAgainReadCardRequest.setIccDataList(readCardRequest.getIccDataList());
        swipeAgainReadCardRequest.setMaxmPinLength(readCardRequest.getMaxmPinLength());
        swipeAgainReadCardRequest.setMinmPinLength(readCardRequest.getMinmPinLength());
        swipeAgainReadCardRequest.setMultipleAidSelectionListener(
                readCardRequest.getMultipleAidSelectionListener()
        );
        swipeAgainReadCardRequest.setPackageName(readCardRequest.getPackageName());
        swipeAgainReadCardRequest.setPinBlockFormat(readCardRequest.getPinBlockFormat());
        swipeAgainReadCardRequest.setPinpadRequired(readCardRequest.isPinpadRequired());
        swipeAgainReadCardRequest.setPinpadTimeout(readCardRequest.getPinpadTimeout());
        swipeAgainReadCardRequest.setStan(readCardRequest.getStan());
        swipeAgainReadCardRequest.setPrimaryAccountNumber(readCardRequest.getPrimaryAccountNumber());
        swipeAgainReadCardRequest.setTransactionDate(DateUtils.yyMMddDate());
        swipeAgainReadCardRequest.setTransactionTime(DateUtils.HHmmssTime());
        swipeAgainReadCardRequest.setCurrencyName(readCardRequest.getCurrencyName());
        swipeAgainReadCardRequest.setPinPadFixedLayout(readCardRequest.getPinPadFixedLayout());
        return swipeAgainReadCardRequest;
    }
}
