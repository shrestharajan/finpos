package global.citytech.finpos.device.qualcomm.io.cards.transaction

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import global.citytech.common.SynchronizePropertyDelegate
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finposframework.hardware.common.Result.FAILURE
import global.citytech.finposframework.hardware.io.cards.CardSummary
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.PinBlock
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_PROCESSING_ERROR
import global.citytech.finposframework.hardware.utility.TransactionProcessedState
import java.lang.ref.WeakReference
import java.util.concurrent.CountDownLatch

/**
 * Created by Rishav Chudal on 4/7/21.
 */
object FeitianSingleton : FeitianLogger.Listener {

    private val logger = FeitianLogger(this)

    @get:Synchronized
    @set:Synchronized
    var transactionTimeOutInSecs: Int = 0

    var readCardRequest: ReadCardRequest? by SynchronizePropertyDelegate(null)
    var readCardResponse: ReadCardResponse? by SynchronizePropertyDelegate(null)
    var contextWeakReference: WeakReference<Context>? by SynchronizePropertyDelegate(null)
    var deviceCountDownLatch: CountDownLatch? by SynchronizePropertyDelegate(null)
    var currentCardDetected: CardType? by SynchronizePropertyDelegate(null)
    var alertDialogWeakReference: WeakReference<AlertDialog>? by SynchronizePropertyDelegate(null)
    var kernelCountDownLatch: CountDownLatch? by SynchronizePropertyDelegate(null)

    @get:Synchronized
    @set:Synchronized
    var pinEntryType: Int? = null

    var dialogWeakReference: WeakReference<Dialog>? by SynchronizePropertyDelegate(null)

    @get:Synchronized
    @set:Synchronized
    var onlineTransaction: Boolean = false

    @get:Synchronized
    @set:Synchronized
    var hostConnected: Boolean = false

    @get:Synchronized
    @set:Synchronized
    var pinBlock = PinBlock(
        "Offline",
        true
    )

    @get:Synchronized
    @set:Synchronized
    var cardSummary: CardSummary? by SynchronizePropertyDelegate(null)

    @get:Synchronized
    @set:Synchronized
    var transactionProcessedState: TransactionProcessedState by SynchronizePropertyDelegate(TransactionProcessedState.NONE)

    fun clearPropertyInstances() {
        logger.logWithClassName("FeitianSingleton ::: Clearing variable instances...")
        transactionTimeOutInSecs = 0
        readCardRequest = null
        readCardResponse =  ReadCardResponse(
            null,
            FAILURE,
            MSG_PROCESSING_ERROR
        )
        contextWeakReference = null
        deviceCountDownLatch = null
        currentCardDetected = CardType.UNKNOWN
        alertDialogWeakReference = null
        kernelCountDownLatch = null
        pinEntryType = null
        dialogWeakReference = null
        onlineTransaction = false
        hostConnected = false
        pinBlock = PinBlock("Offline", true)
        transactionProcessedState = TransactionProcessedState.NONE
    }

    override fun getClassName(): String = FeitianSingleton::class.java.simpleName
}