package global.citytech.finpos.merchant.presentation.model.response


data class TerminalParameter(
    var aidParameters: ArrayList<AidParam>? = null,
    var emvKeys: ArrayList<EmvKey>? = null,
    var emvParameters: EmvParam? = null,
    var cardSchemes: ArrayList<Any>? = null,
    var messageCodes: ArrayList<MessageCode>? = null
)