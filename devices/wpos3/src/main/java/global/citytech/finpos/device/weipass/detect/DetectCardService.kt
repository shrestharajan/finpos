package global.citytech.finpos.device.weipass.detect

/**
 * Created by Rishav Chudal on 5/29/20.
 */
interface DetectCardService {
    fun detectCard(request: DetectCardRequest): DetectCardResponse
}