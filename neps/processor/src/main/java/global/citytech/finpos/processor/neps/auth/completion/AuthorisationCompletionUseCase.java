package global.citytech.finpos.processor.neps.auth.completion;

import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.ProcessingCode;
import global.citytech.finpos.neps.iso8583.requestsender.authcompletion.AuthCompletionIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.authcompletion.AuthCompletionRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.processor.neps.transaction.CardTransactionUseCase;
import global.citytech.finpos.processor.neps.transaction.TransactionRequest;
import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finpos.processor.neps.transaction.TransactionUtils;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.led.LedAction;
import global.citytech.finposframework.hardware.io.led.LedLight;
import global.citytech.finposframework.hardware.io.led.LedRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;

/**
 * Created by Saurav Ghimire on 2/8/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class AuthorisationCompletionUseCase extends CardTransactionUseCase {

    TransactionLog transactionLog;
    private AuthorisationCompletionRequest authorisationCompletionRequest;

    public AuthorisationCompletionUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    public TransactionResponse execute(TransactionRequest request) {
        authorisationCompletionRequest = (AuthorisationCompletionRequest) request.getTransactionRequest();
        transactionLog = request.getTransactionRepository()
                .getTransactionLogWithRRN(authorisationCompletionRequest.getOriginalRRN());
        if (transactionLog == null)
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_FOUND);
        return super.execute(request);
    }


    @Override
    protected String getClassName() {
        return AuthorisationCompletionUseCase.class.getName();
    }

    @Override
    protected TransactionIsoRequest prepareIsoRequest(ReadCardResponse readCardResponse, global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest) {
        return AuthCompletionIsoRequest.Builder.newInstance()
                .transactionAmount(authorisationCompletionRequest.getAmount())
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .localDateTime(DateUtils.yyMMddHHmmssDate())
                .currencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .track2Data(readCardResponse.getCardDetails().getTrackTwoData())
                .pinBlock(readCardResponse.getCardDetails().getPinBlock().getPin())
                .emvData(readCardResponse.getCardDetails().getIccDataBlock())
                .originalRRN(transactionLog.getRrn())
                .build();
    }

    @Override
    protected NepsMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context) {
        return new AuthCompletionRequestSender(context);
    }

    @Override
    protected TransactionType getTransactionType() {
        return TransactionType.AUTH_COMPLETION;
    }

    @Override
    protected void updateTransactionLog(boolean transactionApproved, global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest, TransactionRepository transactionRepository, IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse) {
        System.out.println("::: NEPS ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
        System.out.println("::: NEPS ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
        String timeStamp = IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.LOCAL_TIME);
        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
                .withStan(stan)
                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
                .withInvoiceNumber(invoiceNumber)
                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
                .withTransactionType(TransactionType.AUTH_COMPLETION)
                .withOriginalTransactionType(TransactionType.PRE_AUTH)
                .withTransactionAmount(authorisationCompletionRequest.getAmount())
                .withOriginalTransactionAmount(authorisationCompletionRequest.getAmount())
                .withTransactionDate(TransactionUtils.getDateOrDefaultFromTimeStamp(timeStamp))
                .withTransactionTime(TransactionUtils.getTimeOrDefaultFromTimeStamp(timeStamp))
                .withTransactionStatus(transactionApproved ? "APPROVED" : "DECLINED")
                .withPosEntryMode(PosEntryMode.MANUAL) //TODO
                .withOriginalPosEntryMode(this.transactionLog.getPosEntryMode())
                .withPosConditionCode("00") //TODO
                .withOriginalPosConditionCode(this.transactionLog.getPosConditionCode()) //TODO
                .withReconcileStatus("") //TODO
                .withReconcileTime("") //TODO
                .withReconcileDate("") //TODO
                .withReconcileBatchNo(batchNumber)
                .withReadCardResponse(readCardResponse)
                .withProcessingCode(getProcessingCode()).withAuthorizationCompleted(true)
                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                .withOriginalTransactionReferenceNumber(authorisationCompletionRequest.getOriginalRRN())
                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse,
                        applicationRepository, batchNumber, stan, invoiceNumber))
                .withTransactionVoided(false)
                .withCurrencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .withVatInfo(transactionRequest.getVatInfo())
                .build();
        transactionRepository.updateTransactionLog(transactionLog);
        transactionRepository.updateAuthorizationCompletedStatusByAuthCode(transactionApproved, TransactionType.PRE_AUTH,
                authorisationCompletionRequest.getOriginalRRN());
    }

    @Override
    protected boolean getAuthCompletionStatus() {
        return true;
    }

    @Override
    protected void updateTransactionIdsAndNotifyTransactionStatus(boolean transactionApproved, IsoMessageResponse isoMessageResponse, TransactionRequest request, ReadCardResponse readCardResponse) {
        this.updateTransactionIds(transactionApproved);
        if (transactionApproved) {
            if (readCardResponse.getCardDetails().getCardType() == CardType.PICC)
                request.getLedService().doTurnLedWith(new LedRequest(LedLight.GREEN, LedAction.ON));
            this.notifier.notify(
                    Notifier.EventType.TRANSACTION_APPROVED,
                    this.approvedMessage(isoMessageResponse)
            );
        } else {
            if (readCardResponse.getCardDetails().getCardType() == CardType.PICC)
                request.getLedService().doTurnLedWith(new LedRequest(LedLight.RED, LedAction.ON));
            this.notifier.notify(
                    Notifier.EventType.TRANSACTION_DECLINED,
                    this.declinedMessage(
                            isoMessageResponse,
                            request.getApplicationRepository()
                    )
            );
        }
    }

    @Override
    protected TransactionResponse prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository, boolean shouldPrint) {

        return AuthCompletionResponseModel.Builder.createDefaultBuilder()
                .stan(stan)
                .approved(transactionApproved)
                .shouldPrintCustomerCopy(shouldPrint)
                .debugRequestMessage(isoMessageResponse.getDebugRequestString())
                .debugResponseMessage(isoMessageResponse.getDebugResponseString())
                .message(this.retrieveMessage(isoMessageResponse, transactionApproved, applicationRepository))
                .message(this.retrieveMessage(isoMessageResponse, transactionApproved, applicationRepository))
                .message(this.retrieveMessage(isoMessageResponse, transactionApproved, applicationRepository))
                .build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.PRE_AUTHORIZATION_COMPLETION.getCode();
    }

    @Override
    protected TransactionResponse prepareErrorResponse(String responseMessage, boolean shouldPrint) {
        return AuthCompletionResponseModel.Builder.createDefaultBuilder()
                .stan(this.stan)
                .message(responseMessage)
                .approved(false)
                .shouldPrintCustomerCopy(shouldPrint)
                .build();
    }
}
