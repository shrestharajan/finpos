package global.citytech.nepsadmin.data.response

data class DevicePrivateKey(
    var key1: String? = null,
    var key2: String? = null
)