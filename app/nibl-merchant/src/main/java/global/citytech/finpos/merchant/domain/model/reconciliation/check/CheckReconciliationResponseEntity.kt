package global.citytech.finpos.merchant.domain.model.reconciliation.check

data class CheckReconciliationResponseEntity(val isReconciliationRequired: Boolean)