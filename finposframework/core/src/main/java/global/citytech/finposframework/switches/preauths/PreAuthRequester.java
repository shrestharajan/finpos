package global.citytech.finposframework.switches.preauths;

import global.citytech.finposframework.switches.Request;

public interface PreAuthRequester<
        T extends PreauthRequestParameter, E extends PreauthResponseParameter>
        extends Request<T, E> {
}
