package global.citytech.finpos.neps.iso8583.requestsender.transactiontypesenders;

import global.citytech.finpos.neps.iso8583.ProcessingCode;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionTypeISOMessageUpdater;
import global.citytech.finposframework.iso8583.Iso8583Msg;

public class CashAdvanceISOMessageUpdater implements TransactionTypeISOMessageUpdater {
    @Override
    public Iso8583Msg update(TransactionIsoRequest transactionIsoRequest, Iso8583Msg iso8583Msg) {
        //Add any Cash Advance specific Data Elements
        return iso8583Msg;
    }

    @Override
    public String getProcessingCode() {
        return ProcessingCode.CASH_ADVANCE.getCode();
    }
}
