package global.citytech.finpos.device.weipass

import android.content.Context
import global.citytech.finpos.device.weipass.card.read.WeipassReadCardServiceImpl
import global.citytech.finpos.device.weipass.emv.aid.WeipassAidServiceImpl
import global.citytech.finpos.device.weipass.emv.emvkeys.WeipassEmvKeysServiceImpl
import global.citytech.finpos.device.weipass.emv.revokedkeys.WeipassRevokedKeysServiceImpl
import global.citytech.finpos.device.weipass.emvparam.WeipassEmvParametersServiceImpl
import global.citytech.finpos.device.weipass.hardware.WeipassIOStatusServiceImpl
import global.citytech.finpos.device.weipass.init.WeipassInitializeSDKServiceImpl
import global.citytech.finpos.device.weipass.keys.WeipassKeyServiceImpl
import global.citytech.finpos.device.weipass.led.WeipassLedServiceImpl
import global.citytech.finpos.device.weipass.pinpad.WeipassPinPadServiceImpl
import global.citytech.finpos.device.weipass.printer.WeipassPrinterServiceImpl
import global.citytech.finpos.device.weipass.sound.WeipassSoundServiceImpl
import global.citytech.finpos.device.weipass.system.WeipassSystemServiceImpl
import global.citytech.finposframework.hardware.DeviceServiceType.*
import global.citytech.finposframework.hardware.common.DeviceService
import global.citytech.finposframework.notifier.Notifier

/**
 * Created by Rishav Chudal on 4/9/20.
 */
class WeipassFactory {
    companion object {
        fun getService(
            context: Context,
            deviceServiceType: global.citytech.finposframework.hardware.DeviceServiceType,
            notifier: Notifier
        ): DeviceService {
            return when (deviceServiceType) {
                INIT -> WeipassInitializeSDKServiceImpl(context)
                KEYS -> WeipassKeyServiceImpl()
                EMV_PARAM -> WeipassEmvParametersServiceImpl(context)
                EMV_KEYS -> WeipassEmvKeysServiceImpl()
                REVOKED_KEYS -> WeipassRevokedKeysServiceImpl()
                AID_PARAM -> WeipassAidServiceImpl()
                HARDWARE_STATUS -> WeipassIOStatusServiceImpl(context)
                LED -> WeipassLedServiceImpl()
                SOUND -> WeipassSoundServiceImpl(context)
                CARD -> WeipassReadCardServiceImpl(context, notifier)
                PINPAD -> WeipassPinPadServiceImpl(context)
                PRINTER -> WeipassPrinterServiceImpl(context)
                SYSTEM -> WeipassSystemServiceImpl()
            }
        }
    }
}