package global.citytech.finpos.processor.nibl.reconciliation.check;

import global.citytech.finpos.processor.nibl.reconciliation.NiblSettlementHandler;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reconciliation.check.CheckReconciliationRequester;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationHandler;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationTotals;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Unique Shakya on 8/11/2021.
 */
public class CheckReconciliationUseCase implements CheckReconciliationRequester<CheckReconciliationRequestModel, CheckReconciliationResponseModel>,
        UseCase<CheckReconciliationRequestModel, CheckReconciliationResponseModel> {

    private static final long TWENTY_FOUR_HOURS = 86400000;

    private TerminalRepository terminalRepository;

    public CheckReconciliationUseCase(TerminalRepository terminalRepository) {
        this.terminalRepository = terminalRepository;
    }

    @Override
    public CheckReconciliationResponseModel execute(CheckReconciliationRequestModel request) {
        long lastSuccessfulSettlementTime = request.getReconciliationRepository().getLastSuccessfulSettlementTime();
        long currentTime = System.currentTimeMillis();
        long timeElapsed = currentTime - lastSuccessfulSettlementTime;
        if (timeElapsed > TWENTY_FOUR_HOURS) {
            String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
            if (request.getReconciliationRepository().getTransactionCountByBatchNumber(batchNumber) == 0) {
                request.getReconciliationRepository().updateLastSuccessfulSettlementTime(currentTime);
                return new CheckReconciliationResponseModel(false);
            }
            ReconciliationHandler reconciliationHandler = new NiblSettlementHandler(this.terminalRepository,
                    request.getReconciliationRepository(), batchNumber, request.getApplicationRepository());
            ReconciliationTotals reconciliationTotals = reconciliationHandler.prepareReconciliationTotals();
            if (StringUtils.isAllZero(reconciliationTotals.toTotalsBlock())){
                request.getReconciliationRepository().updateLastSuccessfulSettlementTime(currentTime);
                return new CheckReconciliationResponseModel(false);
            }
            return new CheckReconciliationResponseModel(true);
        } else {
            return new CheckReconciliationResponseModel(false);
        }
    }
}
