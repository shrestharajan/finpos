package global.citytech.finposframework.repositories;

import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.TransactionType;

/**
 * Created by Unique Shakya on 9/21/2020.
 */
public interface ReconciliationRepository {

    long getCountByTransactionType(String batchNumber, TransactionType transactionType);

    long getCountByTransactionTypeWithCardScheme(CardSchemeType cardScheme, String batchNumber, TransactionType transactionType);

    long getTotalAmountByTransactionType(String batchNumber, TransactionType transactionType);

    long getTotalAmountByTransactionTypeWithCardScheme(CardSchemeType cardScheme, String batchNumber, TransactionType transactionType);

    long getCountByOriginalTransactionType(String batchNumber, TransactionType transactionType, TransactionType originalTransactionType);

    long getCountByOriginalTransactionTypeWithCardScheme(CardSchemeType cardScheme, String batchNumber, TransactionType transactionType, TransactionType originalTransactionType);

    long getTotalAmountByOriginalTransactionType(String batchNumber, TransactionType transactionType, TransactionType originalTransactionType);

    long getTotalAmountByOriginalTransactionTypeWithCardScheme(CardSchemeType cardScheme, String batchNumber, TransactionType transactionType, TransactionType originalTransactionType);

    void updateBatch(String batchNumber, String reconciledDate, String reconciledTime);

    void updateReconciliationReceipt(String reconciliationReceipt);

    String getReconciliationReceipt();

    long getTransactionCountByBatchNumber(String batchNumber);

    long getLastSuccessfulSettlementTime();

    void updateLastSuccessfulSettlementTime(long timeInMillis);

    void saveSettlementStatus(SettlementStatus settlementStatus);

    void clear();

    SettlementStatus getSettlementStatus();
}
