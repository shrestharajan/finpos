package global.citytech.finpos.merchant.domain.model.app

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import global.citytech.easydroid.core.utils.Jsons

/**
 * Created by Rishav Chudal on 9/14/20.
 */
@Entity(tableName = MessageCode.TABLE_NAME)
data class MessageCode(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_ID)
    var id: String,

    @ColumnInfo(name = COLUMN_MESSAGE_CODE)
    var messageCode: String? = null,

    @ColumnInfo(name = COLUMN_MESSAGE_TEXT)
    var messageText: String? = null
) {
    companion object {
        const val TABLE_NAME = "message_codes"
        const val COLUMN_ID = "id"
        const val COLUMN_MESSAGE_CODE = "message_code"
        const val COLUMN_MESSAGE_TEXT = "message_text"
    }
}

fun global.citytech.finpos.merchant.presentation.model.response.MessageCode.mapToEntity(index: Int): MessageCode {
    return MessageCode(
        index.toString(),
        Jsons.toJsonObj(messageCode),
        Jsons.toJsonObj(messageText)
    )
}