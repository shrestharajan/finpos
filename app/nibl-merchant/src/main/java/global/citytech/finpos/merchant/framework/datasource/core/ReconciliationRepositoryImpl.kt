package global.citytech.finpos.merchant.framework.datasource.core

import global.citytech.easydroid.core.preference.SecurePreference
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.app.db.AppDatabase
import global.citytech.finpos.merchant.framework.datasource.app.AutoReversalQueueDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finposframework.repositories.ReconciliationRepository
import global.citytech.finposframework.repositories.SettlementStatus
import global.citytech.finposframework.usecases.CardSchemeType
import global.citytech.finposframework.usecases.TransactionType

/**
 * Created by Unique Shakya on 9/22/2020.
 */
class ReconciliationRepositoryImpl : ReconciliationRepository {

    val PREF_RECENT_RECONCILIATION: String = "pref_recent_reconciliation"
    val PREF_SETTLEMENT_STATUS: String = "pref_settlement_status"
    val KEY_RECENT_RECONCILIATION: String = "key_recent_reconciliation"
    val KEY_SETTLEMENT_STATUS: String = "key_settlement_status"

    override fun getCountByOriginalTransactionType(
        batchNumber: String?,
        purchaseType: TransactionType?,
        originalPurchaseType: TransactionType?
    ): Long {
        return AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .getCountByOriginalTransactionType(
                batchNumber!!,
                Jsons.toJsonObj(purchaseType!!),
                Jsons.toJsonObj(originalPurchaseType!!)
            )
    }

    override fun getCountByTransactionType(
        batchNumber: String?,
        purchaseType: TransactionType?
    ): Long {
        return AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .getCountByTransactionType(batchNumber!!, Jsons.toJsonObj(purchaseType!!))
    }

    override fun updateReconciliationReceipt(reconciliationReceipt: String) {
        val securePreference = SecurePreference(NiblMerchant.INSTANCE, PREF_RECENT_RECONCILIATION)
        securePreference.saveData(KEY_RECENT_RECONCILIATION, reconciliationReceipt)
    }

    override fun updateLastSuccessfulSettlementTime(timeInMillis: Long) {
        PreferenceManager.updateLastSuccessSettlementTimeInMillis(timeInMillis)
    }

    override fun getTotalAmountByOriginalTransactionTypeWithCardScheme(
        cardScheme: CardSchemeType?,
        batchNumber: String?,
        transactionType: TransactionType?,
        originalTransactionType: TransactionType?
    ): Long {
        return AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .getSumByOriginalTransactionTypeWithCardScheme(
                this.cardSchemeExpression(cardScheme),
                batchNumber!!,
                Jsons.toJsonObj(transactionType!!),
                Jsons.toJsonObj(originalTransactionType!!)
            )
    }

    override fun getLastSuccessfulSettlementTime(): Long =
        PreferenceManager.getLastSuccessSettlementTimeInMillis(0)

    override fun getTotalAmountByOriginalTransactionType(
        batchNumber: String?,
        purchaseType: TransactionType?,
        originalPurchaseType: TransactionType?
    ): Long {
        return AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .getSumByOriginalTransactionType(
                batchNumber!!,
                Jsons.toJsonObj(purchaseType!!),
                Jsons.toJsonObj(originalPurchaseType!!)
            )
    }

    override fun getCountByTransactionTypeWithCardScheme(
        cardScheme: CardSchemeType?,
        batchNumber: String?,
        transactionType: TransactionType?
    ): Long {
        return AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .getCountByTransactionTypeWithCardScheme(
                this.cardSchemeExpression(cardScheme),
                batchNumber!!, Jsons.toJsonObj(transactionType!!)
            )
    }

    override fun updateBatch(
        batchNumber: String?,
        reconciledDate: String?,
        reconciledTime: String?
    ) {
        AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .updateReconcileStatus(batchNumber!!, reconciledDate!!, reconciledTime!!)
        AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .deleteAll()
        AutoReversalQueueDataSourceImpl(NiblMerchant.INSTANCE).clear()
    }

    override fun getCountByOriginalTransactionTypeWithCardScheme(
        cardScheme: CardSchemeType?,
        batchNumber: String?,
        transactionType: TransactionType?,
        originalTransactionType: TransactionType?
    ): Long {
        return AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .getCountByOriginalTransactionTypeWithCardScheme(
                this.cardSchemeExpression(cardScheme),
                batchNumber!!,
                Jsons.toJsonObj(transactionType!!),
                Jsons.toJsonObj(originalTransactionType!!)
            )
    }

    override fun getTotalAmountByTransactionTypeWithCardScheme(
        cardScheme: CardSchemeType?,
        batchNumber: String?,
        transactionType: TransactionType?
    ): Long {
        return AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .getSumByTransactionTypeWithCardScheme(
                this.cardSchemeExpression(cardScheme),
                batchNumber!!, Jsons.toJsonObj(transactionType!!)
            )
    }

    override fun getTotalAmountByTransactionType(
        batchNumber: String?,
        purchaseType: TransactionType?
    ): Long {
        return AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .getSumByTransactionType(batchNumber!!, Jsons.toJsonObj(purchaseType!!))
    }

    override fun getTransactionCountByBatchNumber(batchNumber: String?): Long {
        return AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .getTransactionCountByBatchNumber(batchNumber!!)
    }

    override fun saveSettlementStatus(settlementStatus: SettlementStatus) {
        val securePreference = SecurePreference(NiblMerchant.INSTANCE, PREF_SETTLEMENT_STATUS)
        securePreference.saveData(KEY_SETTLEMENT_STATUS, settlementStatus.name)
    }

    override fun getSettlementStatus(): SettlementStatus {
        val securePreference = SecurePreference(NiblMerchant.INSTANCE, PREF_SETTLEMENT_STATUS)
        val stringSettlementStatus =
            securePreference.retrieveData(KEY_SETTLEMENT_STATUS, SettlementStatus.OPEN.name)
        return SettlementStatus.valueOf(stringSettlementStatus)
    }

    override fun clear() {
        val securePreference = SecurePreference(
            NiblMerchant.INSTANCE,
            PREF_RECENT_RECONCILIATION
        )
        securePreference.clear()
    }

    override fun getReconciliationReceipt(): String {
        val securePreference = SecurePreference(
            NiblMerchant.INSTANCE,
            PREF_RECENT_RECONCILIATION
        )
        return securePreference.retrieveData(
            KEY_RECENT_RECONCILIATION,
            ""
        )
    }

    private fun cardSchemeExpression(cardScheme: CardSchemeType?): String {
        return "%\"" + "cardScheme" + "\":\"" + cardScheme!!.name + "\"%"
    }
}