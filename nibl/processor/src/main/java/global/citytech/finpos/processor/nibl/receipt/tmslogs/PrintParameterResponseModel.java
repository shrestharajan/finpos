package global.citytech.finpos.processor.nibl.receipt.tmslogs;

import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.tmslogs.TmsLogsResponseParameter;

/**
 * Created by Rishav Chudal on 11/5/20.
 */
public class PrintParameterResponseModel implements UseCase.Response, TmsLogsResponseParameter {
    private boolean success;
    private String message;

    public PrintParameterResponseModel(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }
}
