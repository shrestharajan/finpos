package global.citytech.finpos.neps.repo;

import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.activity.ActivityLog;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;

/** @author rajudhital on 3/28/20 */
public class TerminalRepositoryDecorator implements TerminalRepository {
  private final TerminalRepository terminalRepository;

  public TerminalRepositoryDecorator(TerminalRepository terminalRepository) {
    this.terminalRepository = terminalRepository;
  }

  @Override
  public String getSystemTraceAuditNumber() {
    // DO SOME VALIDATION STUFF
    return this.terminalRepository.getSystemTraceAuditNumber();
  }

  @Override
  public void incrementSystemTraceAuditNumber() {
    this.terminalRepository.incrementSystemTraceAuditNumber();
  }

  @Override
  public TerminalInfo findTerminalInfo() {
    // TO DO : VALIDATION IF NEEDED
    return this.terminalRepository.findTerminalInfo();
  }

  @Override
  public HostInfo findPrimaryHost() {
    // TO DO : VALIDATION IF NEEDED
    return this.terminalRepository.findPrimaryHost();
  }

  @Override
  public HostInfo findSecondaryHost() {
    // TO DO : VALIDATION IF NEEDED
    return this.terminalRepository.findSecondaryHost();
  }

  @Override
  public String getReconciliationBatchNumber() {
    return this.terminalRepository.getReconciliationBatchNumber();
  }

  @Override
  public void incrementReconciliationBatchNumber() {
    this.terminalRepository.incrementReconciliationBatchNumber();
  }

  @Override
  public String getRetrievalReferenceNumber() {
    return this.terminalRepository.getRetrievalReferenceNumber();
  }

  @Override
  public void incrementRetrievalReferenceNumber() {
    this.incrementRetrievalReferenceNumber();
  }

  @Override
  public String getInvoiceNumber() {
    return this.getInvoiceNumber();
  }

  @Override
  public void incrementInvoiceNumber() {
    this.incrementInvoiceNumber();
  }

  @Override
  public String getTerminalTransactionCurrencyName() {
    return this.terminalRepository.getTerminalTransactionCurrencyName();
  }

  @Override
  public String getApplicationVersion() {
    return this.terminalRepository.getApplicationVersion();
  }

  @Override
  public boolean isVatRefundSupported() {
    return this.terminalRepository.isVatRefundSupported();
  }

  @Override
  public boolean shouldShufflePinPad() {
    return this.shouldShufflePinPad();
  }

  @Override
  public void updateActivityLog(ActivityLog activityLog) {

  }

  @Override
  public String getTerminalSerialNumber() {
    return null;
  }
}
