package global.citytech.finpos.processor.nibl.reconciliation;

import global.citytech.finposframework.usecases.CardSchemeType;

/**
 * Created by Unique Shakya on 11/12/2020.
 */
public class NiblCardSchemeTotals {

    private String transactionCurrencyName;
    private CardSchemeType cardSchemeType;
    private long salesCount;
    private long salesAmount;
    private long salesRefundCount;
    private long salesRefundAmount;
    private long debitCount;
    private long debitAmount;
    private long debitRefundCount;
    private long debitRefundAmount;
    private long authCount;
    private long authAmount;
    private long authRefundCount;
    private long authRefundAmount;
    private long voidCount;
    private long voidAmount;

    public String getTransactionCurrencyName() {
        return transactionCurrencyName;
    }

    public long getSalesCount() {
        return salesCount;
    }

    public long getSalesAmount() {
        return salesAmount;
    }

    public long getSalesRefundCount() {
        return salesRefundCount;
    }

    public long getSalesRefundAmount() {
        return salesRefundAmount;
    }

    public long getDebitCount() {
        return debitCount;
    }

    public long getDebitAmount() {
        return debitAmount;
    }

    public long getDebitRefundCount() {
        return debitRefundCount;
    }

    public long getDebitRefundAmount() {
        return debitRefundAmount;
    }

    public long getAuthCount() {
        return authCount;
    }

    public long getAuthAmount() {
        return authAmount;
    }

    public long getAuthRefundCount() {
        return authRefundCount;
    }

    public long getAuthRefundAmount() {
        return authRefundAmount;
    }

    public CardSchemeType getCardSchemeType() {
        return cardSchemeType;
    }

    public long getVoidAmount() {
        return voidAmount;
    }

    public long getVoidCount() {
        return voidCount;
    }

    public long getTotalCount(){
        return salesCount + salesRefundCount + voidCount + debitCount + debitRefundCount +
                authCount + authRefundCount;
    }

    public long getTotalCountReceipt() {
        return salesCount;
    }

    public long getTotalAmountReceipt() {
        return salesAmount;
    }

    public static final class Builder {
        private String transactionCurrencyName;
        private CardSchemeType cardSchemeType;
        private long salesCount;
        private long salesAmount;
        private long salesRefundCount;
        private long salesRefundAmount;
        private long debitCount;
        private long debitAmount;
        private long debitRefundCount;
        private long debitRefundAmount;
        private long authCount;
        private long authAmount;
        private long authRefundCount;
        private long authRefundAmount;
        private long voidCount;
        private long voidAmount;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withTransactionCurrencyName(String transactionCurrencyName) {
            this.transactionCurrencyName = transactionCurrencyName;
            return this;
        }

        public Builder withCardSchemeType(CardSchemeType cardSchemeType) {
            this.cardSchemeType = cardSchemeType;
            return this;
        }

        public Builder withSalesCount(long salesCount) {
            this.salesCount = salesCount;
            return this;
        }

        public Builder withSalesAmount(long salesAmount) {
            this.salesAmount = salesAmount;
            return this;
        }

        public Builder withSalesRefundCount(long salesRefundCount) {
            this.salesRefundCount = salesRefundCount;
            return this;
        }

        public Builder withSalesRefundAmount(long salesRefundAmount) {
            this.salesRefundAmount = salesRefundAmount;
            return this;
        }

        public Builder withDebitCount(long debitCount) {
            this.debitCount = debitCount;
            return this;
        }

        public Builder withDebitAmount(long debitAmount) {
            this.debitAmount = debitAmount;
            return this;
        }

        public Builder withDebitRefundCount(long debitRefundCount) {
            this.debitRefundCount = debitRefundCount;
            return this;
        }

        public Builder withDebitRefundAmount(long debitRefundAmount) {
            this.debitRefundAmount = debitRefundAmount;
            return this;
        }

        public Builder withAuthCount(long authCount) {
            this.authCount = authCount;
            return this;
        }

        public Builder withAuthAmount(long authAmount) {
            this.authAmount = authAmount;
            return this;
        }

        public Builder withAuthRefundCount(long authRefundCount) {
            this.authRefundCount = authRefundCount;
            return this;
        }

        public Builder withAuthRefundAmount(long authRefundAmount) {
            this.authRefundAmount = authRefundAmount;
            return this;
        }

        public Builder withVoidCount(long voidCount) {
            this.voidCount = voidCount;
            return this;
        }

        public Builder withVoidAmount(long voidAmount) {
            this.voidAmount = voidAmount;
            return this;
        }

        public NiblCardSchemeTotals build() {
            NiblCardSchemeTotals niblCardSchemeTotals = new NiblCardSchemeTotals();
            niblCardSchemeTotals.transactionCurrencyName = this.transactionCurrencyName;
            niblCardSchemeTotals.salesRefundCount = this.salesRefundCount;
            niblCardSchemeTotals.salesRefundAmount = this.salesRefundAmount;
            niblCardSchemeTotals.debitAmount = this.debitAmount;
            niblCardSchemeTotals.debitCount = this.debitCount;
            niblCardSchemeTotals.authRefundAmount = this.authRefundAmount;
            niblCardSchemeTotals.authRefundCount = this.authRefundCount;
            niblCardSchemeTotals.debitRefundCount = this.debitRefundCount;
            niblCardSchemeTotals.debitRefundAmount = this.debitRefundAmount;
            niblCardSchemeTotals.cardSchemeType = this.cardSchemeType;
            niblCardSchemeTotals.salesAmount = this.salesAmount;
            niblCardSchemeTotals.authCount = this.authCount;
            niblCardSchemeTotals.authAmount = this.authAmount;
            niblCardSchemeTotals.salesCount = this.salesCount;
            niblCardSchemeTotals.voidAmount = this.voidAmount;
            niblCardSchemeTotals.voidCount = this.voidCount;
            return niblCardSchemeTotals;
        }
    }
}
