package global.citytech.finpos.processor.neps.receipt.duplicate;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.duplicatereceipt.DuplicateReceiptResponseParameter;

public class DuplicateReceiptResponseModel implements UseCase.Response, DuplicateReceiptResponseParameter {

    private final Result result;
    private final String message;

    public DuplicateReceiptResponseModel(Result result, String message) {
        this.result = result;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public Result getResult() {
        return result;
    }
}
