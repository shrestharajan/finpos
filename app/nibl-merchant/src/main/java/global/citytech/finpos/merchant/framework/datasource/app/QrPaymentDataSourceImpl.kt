package global.citytech.finpos.merchant.framework.datasource.app

import android.content.Context
import global.citytech.easydroid.core.preference.SecurePreference
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.data.datasource.app.qr.QrPaymentDataSource
import global.citytech.finpos.merchant.domain.model.app.QrPayment
import global.citytech.finpos.merchant.presentation.dynamicqr.QrConst.FONEPAY_BILL_NUMBER
import global.citytech.finpos.merchant.presentation.dynamicqr.QrConst.FONEPAY_REQUEST_NUMBER
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.utility.DateUtils
import global.citytech.finposframework.utility.StringUtils
import java.text.SimpleDateFormat
import java.util.*

class QrPaymentDataSourceImpl(context: Context) : QrPaymentDataSource {

    private var securePreference: SecurePreference

    private val preferenceName = "qr_payment_pref"
    private val qrPaymentListKey = "key_qr_payment_list"
    private val lastUpdateKey = "key_last_update"
    private val billingInvoiceNumberKey = "key_billing_invoice_number"

    init {
        securePreference = SecurePreference(context.applicationContext, preferenceName)
    }

    override fun getAllQrPayments(): MutableList<QrPayment> {
        val data = getQrPaymentsAsString()
        if (data != null) {
            try {
                val list =
                    Jsons.fromJsonToList<QrPayment>(data, Array<QrPayment>::class.java)
                        .toMutableList()
                return list
            } catch (e: Exception) {
                e.printStackTrace()
                return emptyList<QrPayment>().toMutableList()
            }
        } else {
            return emptyList<QrPayment>().toMutableList()
        }
    }

    override fun getCount(): Long = getAllQrPayments().size.toLong()

    override fun addQrPayment(qrPayment: QrPayment) {
        var list = getAllQrPayments()
        if (list.isEmpty())
            list = mutableListOf()
        val copyList = copy(list)
        list.forEach {
            if (it.referenceNumber == qrPayment.referenceNumber && it.approvalCode == qrPayment.approvalCode)
                copyList.remove(qrPayment)
        }
        copyList.add(qrPayment)
        saveQrPaymentsAsString(copyList)
    }

    override fun removeQrPayment(qrPayment: QrPayment) {
        val list = getAllQrPayments()
        if (list.isNotEmpty()) {
            val copyList = copy(list)
            list.forEach {
                if (it.referenceNumber == qrPayment.referenceNumber && it.approvalCode == qrPayment.approvalCode)
                    copyList.remove(qrPayment)
            }
            saveQrPaymentsAsString(copyList)
        }
    }

    override fun getLastUpdateDateTime(): String = securePreference.retrieveData(lastUpdateKey, "")

    override fun getBillingInvoiceNumber(): String {
        return StringUtils.ofRequiredLength(
            securePreference.retrieveData(
                billingInvoiceNumberKey,
                "1"
            ), 6
        )
    }

    override fun incrementBillingInvoiceNumber() {
        var invoiceNumber = getBillingInvoiceNumber().toLong()
        if (invoiceNumber == StringUtils.ofRequiredLength("9", 6, 9).toLong()) {
            invoiceNumber = StringUtils.ofRequiredLength("0", 6).toLong()
        }
        invoiceNumber++
        securePreference.saveData(
            billingInvoiceNumberKey,
            StringUtils.ofRequiredLength(invoiceNumber, 6)
        )
    }

    override fun saveBillingInvoiceNumber(invoiceNumber: String) {
        securePreference.saveData(
            billingInvoiceNumberKey,
            invoiceNumber
        )
    }

    override fun generateFonepayQrIdentifiers(key: String): String {
        return when (key) {
            FONEPAY_REQUEST_NUMBER -> key.plus(DateUtils.yyMMddDate()).plus(getBillingInvoiceNumber())
            FONEPAY_BILL_NUMBER -> DateUtils.yyMMddDate().plus(getBillingInvoiceNumber())
            else -> DateUtils.yyMMddDate().plus(getBillingInvoiceNumber()).plus(key)
        }
    }

    override fun getLimitedQrPayments(limit: Int, offset: Int): MutableList<QrPayment> {
        Logger.getLogger("QrPaymentDataSourceImpl")
            .log("GET LIMITED QR PAYMENTS LIMIT >> $limit OFFSET >> $offset")
        val list = getAllQrPayments()
        val newList = mutableListOf<QrPayment>()
        var index = offset
        do {
            try {
                Logger.getLogger("QrPaymentDataSourceImpl")
                    .log(list[index].invoiceNumber)
            } catch (e: Exception) {
                e.printStackTrace()
                break
            }
            newList.add(list[index])
            index++
        } while (index < limit)
        return newList
    }

    override fun getLimitedQrPaymentsBySearchParam(
        searchParam: String,
        limit: Int,
        offset: Int
    ): MutableList<QrPayment> {
        Logger.getLogger("QrPaymentDataSourceImpl")
            .log("GET LIMITED QR PAYMENTS BY SEARCH PARAM >> $limit OFFSET >> $offset")
        val list = getAllQrPayments()
        val copyList = mutableListOf<QrPayment>()
        val newList = mutableListOf<QrPayment>()
        if (list.isNotEmpty()) {
            list.forEach {
                if (it.invoiceNumber.contains(searchParam))
                    copyList.add(it)
            }
        }
        var index = offset
        do {
            try {
                Logger.getLogger("QrPaymentDataSourceImpl")
                    .log(copyList[index].invoiceNumber)
            } catch (e: Exception) {
                e.printStackTrace()
                break
            }
            newList.add(copyList[index])
            index++
        } while (index < limit)
        return newList
    }

    override fun getCountBySearchParam(searchParam: String): Long {
        val list = getAllQrPayments()
        var count: Long = 0.toLong()
        if (list.isNotEmpty()) {
            list.forEach {
                if (it.invoiceNumber.contains(searchParam))
                    count++
            }
        }
        return count
    }

    override fun getQrPaymentByInvoiceNumber(invoiceNumber: String): QrPayment? {
        val list = getAllQrPayments()
        list.forEach {
            if (it.invoiceNumber == invoiceNumber)
                return it
        }
        return null
    }

    override fun updateQrPaymentRefunded(qrPayment: QrPayment) {
        var list = getAllQrPayments()
        if (list.isEmpty())
            list = mutableListOf()
        val copyList = copy(list)
        list.forEach {
            if (it.referenceNumber == qrPayment.referenceNumber && it.approvalCode == qrPayment.approvalCode)
                copyList.remove(qrPayment)
        }
        qrPayment.isRefunded = true
        copyList.add(qrPayment)
        saveQrPaymentsAsString(copyList)
    }

    override fun updateQrPaymentVoided(qrPayment: QrPayment) {
        var list = getAllQrPayments()
        if (list.isEmpty())
            list = mutableListOf()
        val copyList = copy(list)
        list.forEach {
            if (it.referenceNumber == qrPayment.referenceNumber && it.approvalCode == qrPayment.approvalCode)
                copyList.remove(qrPayment)
        }
        qrPayment.isVoided = true
        copyList.add(qrPayment)
        saveQrPaymentsAsString(copyList)
    }

    override fun clear() {
        securePreference.clear()
    }

    override fun updateQrPaymentStatus(status: String, qrPayment: QrPayment) {
        var list = getAllQrPayments()
        if (list.isEmpty())
            list = mutableListOf()
        val copyList = copy(list)
        list.forEach {
            if (it.referenceNumber == qrPayment.referenceNumber && it.approvalCode == qrPayment.approvalCode)
                copyList.remove(qrPayment)
        }
        qrPayment.transactionStatus = status
        copyList.add(qrPayment)
        saveQrPaymentsAsString(copyList)
    }

    private fun copy(list: MutableList<QrPayment>): MutableList<QrPayment> {
        val copyList = mutableListOf<QrPayment>()
        list.forEach {
            copyList.add(it)
        }
        return copyList
    }

    private fun getQrPaymentsAsString(): String? =
        securePreference.retrieveData(qrPaymentListKey, null)

    private fun saveQrPaymentsAsString(list: MutableList<QrPayment>) {
        val data = Jsons.toJsonList(list)
        securePreference.saveData(qrPaymentListKey, data)
        saveLastUpdateTime(StringUtils.dateTimeStampYYYYMMddHHmmss())
    }

    private fun saveLastUpdateTime(timeStamp: String) {
        securePreference.saveData(lastUpdateKey, timeStamp)
    }
}