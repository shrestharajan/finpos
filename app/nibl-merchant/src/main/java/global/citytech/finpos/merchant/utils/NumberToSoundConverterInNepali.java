package global.citytech.finpos.merchant.utils;

import android.util.Log;

import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration;

public class NumberToSoundConverterInNepali {
    static String FILE_EXTENSION = "";

    public static void main(String[] arg) {
        System.out.println(convertIntoRupaiya(100010));
    }

    public static String convertIntoRupaiya(long number) {
        String rupaiya = convertIntoSound(number);
        if (rupaiya.length() > 0) {
            if (new DeviceConfiguration().getCurrencyCode() == "524") {
                return convertIntoSound(number) +" rupaiya";
            }else if (new DeviceConfiguration().getCurrencyCode() == "840"){
                return convertIntoSound(number) +" dollar";
            }
        }
        return "";
    }

    static String convertIntoRupaiya(long number,String currencyCode) {
        String rupaiya = convertIntoSound(number);
        if (rupaiya.length() > 0) {
            if (currencyCode == "NPR") {
                return convertIntoSound(number) +" rupaiya";
            }else if (currencyCode == "USD"){
                return convertIntoSound(number) +" dollar";
            }
        }
        return "";
    }

    private static String getPostFixString(long divider) {
        if (divider <= 1000) {
            return " " + String.valueOf(divider) + FILE_EXTENSION;
        }
        if (divider == 100000) {
            return " " + String.valueOf("lakh") + FILE_EXTENSION;
        }
        if (divider == 10000000) {
            return " " + String.valueOf("karod") + FILE_EXTENSION;
        }
        return "";
    }

    public static String convertIntoSound(long number) {
        StringBuilder files = new StringBuilder();

        long divider = 0;
        if (number < 1000) {
            divider = 100;
        }
        if (number >= 1000 && number < 100000) {
            divider = 1000;
        }
        if (number >= 100000 && number < 10000000) {
            divider = 100000;
        }
        if (number >= 10000000 && number < 1000000000) {
            divider = 10000000;
        }
        if (number < 100) {
            files.append(number);
            files.append(FILE_EXTENSION);
            return files.toString();
        }
        long divider_result = number / divider; //12.523 for 12523
        long divider_mod = number % divider; //523

        files.append(divider_result);
        files.append(FILE_EXTENSION);
        files.append(getPostFixString(divider));
        if (divider_mod > 0) {
            if (divider_mod >= 100) {
                files.append(" ");
                files.append(convertIntoSound(divider_mod));
            } else {
                files.append(getPostFixString(divider_mod));
            }
        }
        return files.toString();
    }
}