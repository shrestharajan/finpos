package global.citytech.finpos.processor.neps.cardprocessor;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finpos.neps.iso8583.SmartVistaIccDataList;
import global.citytech.finpos.processor.neps.NepsConstant;
import global.citytech.finpos.processor.neps.transaction.TransactionSetTableVerifier;
import global.citytech.finpos.processor.neps.transaction.TransactionUtils;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardSummary;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.PinBlock;
import global.citytech.finposframework.hardware.io.cards.cvm.Cvm;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat;
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest;
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.CardSchemeRetrieveRequest;
import global.citytech.finposframework.repositories.CardSchemeRetrieveResponse;
import global.citytech.finposframework.repositories.FieldAttributes;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.MagneticSwipe;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.utility.CardUtils;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.ServiceCodeUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Saurav Ghimire on 1/27/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class MagCardProcessor extends CardProcessor {

    public MagCardProcessor(
            TransactionRepository transactionRepository,
            TransactionAuthenticator transactionAuthenticator,
            ApplicationRepository applicationRepository,
            ReadCardService readCardService,
            Notifier notifier,
            TerminalRepository terminalRepository
    ) {
        super(
                transactionRepository,
                transactionAuthenticator,
                applicationRepository,
                readCardService,
                notifier,
                terminalRepository
        );
    }

    private ReadCardResponse forceChipCardRead(ReadCardRequest readCardRequest) {
        readCardService.cleanUp();
        this.notifier.notify(Notifier.EventType.SWITCH_MAGNETIC_TO_ICC,
                Notifier.EventType.SWITCH_MAGNETIC_TO_ICC.getDescription());
        ReadCardRequest contactlessFallbackReadCardRequest = this.prepareForceChipReadCardRequest(readCardRequest);
        ReadCardResponse readCardResponse = this.readCardService.readCardDetails(contactlessFallbackReadCardRequest);
        return new ICCCardProcessor(
                transactionRepository,
                transactionAuthenticator,
                applicationRepository,
                readCardService,
                notifier,
                terminalRepository
        ).validateReadCardResponse(readCardRequest, readCardResponse);
    }

    private ReadCardRequest prepareForceChipReadCardRequest(ReadCardRequest readCardRequest) {
        List<CardType> cardTypeList = new ArrayList<>();
        cardTypeList.add(CardType.ICC);
        ReadCardRequest forceChipReadRequest = new ReadCardRequest(cardTypeList, readCardRequest.getTransactionType(),
                readCardRequest.getTransactionType9C(), readCardRequest.getEmvParametersRequest());
        forceChipReadRequest.setCashBackAmount(readCardRequest.getCashBackAmount());
        forceChipReadRequest.setAmount(readCardRequest.getAmount());
        forceChipReadRequest.setCardDetailsBeforeEmvNext(readCardRequest.getCardDetailsBeforeEmvNext());
        forceChipReadRequest.setCardReadTimeOut(readCardRequest.getCardReadTimeOut());
        forceChipReadRequest.setFallbackFromIccToMag(readCardRequest.getFallbackFromIccToMag());
        forceChipReadRequest.setForceOnlineForICC(readCardRequest.getForceOnlineForICC());
        forceChipReadRequest.setForceOnlineForPICC(readCardRequest.getForceOnlineForPICC());
        forceChipReadRequest.setIccDataList(SmartVistaIccDataList.get());
        forceChipReadRequest.setMaxmPinLength(readCardRequest.getMaxmPinLength());
        forceChipReadRequest.setMinmPinLength(readCardRequest.getMinmPinLength());
        forceChipReadRequest.setMultipleAidSelectionListener(readCardRequest.getMultipleAidSelectionListener());
        forceChipReadRequest.setPackageName(readCardRequest.getPackageName());
        forceChipReadRequest.setPinBlockFormat(readCardRequest.getPinBlockFormat());
        forceChipReadRequest.setPinpadRequired(readCardRequest.isPinpadRequired());
        forceChipReadRequest.setPinpadTimeout(readCardRequest.getPinpadTimeout());
        forceChipReadRequest.setStan(readCardRequest.getStan());
        forceChipReadRequest.setPrimaryAccountNumber(readCardRequest.getPrimaryAccountNumber());
        forceChipReadRequest.setTransactionDate(DateUtils.yyMMddDate());
        forceChipReadRequest.setTransactionTime(DateUtils.HHmmssTime());
        forceChipReadRequest.setCurrencyName(readCardRequest.getCurrencyName());
        forceChipReadRequest.setPinPadFixedLayout(!terminalRepository.shouldShufflePinPad());
        return forceChipReadRequest;
    }

    private boolean checkForForceChipCardRead(ReadCardResponse readCardResponse) {
        if (readCardResponse.getFallbackIccToMag()) {
            return false;
        } else {
            return ServiceCodeUtils.doesContainChipCard(readCardResponse.getCardDetails().getTrackTwoData());
        }
    }

    private ReadCardResponse onReadCardResponseWaveAgain(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse) {
        if (MagneticSwipe.getInstance().isMagneticSwipeAgainAllowed()) {
            return onSwipeAgainAllowed(
                    readCardRequest,
                    readCardResponse
            );
        } else {
            MagneticSwipe.getInstance().onMagneticSwipeFailureLimitReached();
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    private ReadCardResponse onSwipeAgainAllowed(ReadCardRequest readCardRequest, ReadCardResponse oldReadCardResponse) {
        MagneticSwipe.getInstance().onMagneticSwipeFailure();
        this.notifier.notify(
                Notifier.EventType.SWIPE_AGAIN,
                Notifier.EventType.SWIPE_AGAIN.getDescription()
        );
        this.readCardService.cleanUp();
        ReadCardResponse readCardResponse = this.readCardService.readCardDetails(
                prepareSwipeAgainReadCardRequest(readCardRequest)
        );
        readCardResponse.setFallbackIccToMag(oldReadCardResponse.getFallbackIccToMag());
        return validateReadCardResponse(readCardRequest, readCardResponse);
    }


    private ReadCardRequest prepareSwipeAgainReadCardRequest(ReadCardRequest readCardRequest) {
        List<CardType> cardTypeList = new ArrayList<>();
        cardTypeList.add(CardType.MAG);
        ReadCardRequest swipeAgainReadCardRequest = new ReadCardRequest(
                cardTypeList,
                readCardRequest.getTransactionType(),
                readCardRequest.getTransactionType9C(),
                readCardRequest.getEmvParametersRequest()
        );
        swipeAgainReadCardRequest.setCashBackAmount(readCardRequest.getCashBackAmount());
        swipeAgainReadCardRequest.setAmount(readCardRequest.getAmount());
        swipeAgainReadCardRequest.setCardDetailsBeforeEmvNext(
                readCardRequest.getCardDetailsBeforeEmvNext()
        );
        swipeAgainReadCardRequest.setCardReadTimeOut(readCardRequest.getCardReadTimeOut());
        swipeAgainReadCardRequest.setFallbackFromIccToMag(
                readCardRequest.getFallbackFromIccToMag()
        );
        swipeAgainReadCardRequest.setForceOnlineForICC(
                readCardRequest.getForceOnlineForICC()
        );
        swipeAgainReadCardRequest.setForceOnlineForPICC(
                readCardRequest.getForceOnlineForPICC()
        );
        swipeAgainReadCardRequest.setIccDataList(readCardRequest.getIccDataList());
        swipeAgainReadCardRequest.setMaxmPinLength(readCardRequest.getMaxmPinLength());
        swipeAgainReadCardRequest.setMinmPinLength(readCardRequest.getMinmPinLength());
        swipeAgainReadCardRequest.setMultipleAidSelectionListener(
                readCardRequest.getMultipleAidSelectionListener()
        );
        swipeAgainReadCardRequest.setPackageName(readCardRequest.getPackageName());
        swipeAgainReadCardRequest.setPinBlockFormat(readCardRequest.getPinBlockFormat());
        swipeAgainReadCardRequest.setPinpadRequired(readCardRequest.isPinpadRequired());
        swipeAgainReadCardRequest.setPinpadTimeout(readCardRequest.getPinpadTimeout());
        swipeAgainReadCardRequest.setStan(readCardRequest.getStan());
        swipeAgainReadCardRequest.setPrimaryAccountNumber(readCardRequest.getPrimaryAccountNumber());
        swipeAgainReadCardRequest.setTransactionDate(DateUtils.yyMMddDate());
        swipeAgainReadCardRequest.setTransactionTime(DateUtils.HHmmssTime());
        swipeAgainReadCardRequest.setCurrencyName(readCardRequest.getCurrencyName());
        swipeAgainReadCardRequest.setPinPadFixedLayout(!terminalRepository.shouldShufflePinPad());
        return swipeAgainReadCardRequest;
    }

    @Override
    protected void validateByRespectiveProcessor(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse) {
        // Add Magnetic Specific validation here
        if (readCardResponse.getResult() == Result.FAILURE ||
                readCardResponse.getCardDetails() == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (StringUtils.isEmpty(readCardResponse.getCardDetails().getTrackTwoData())) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    @Override
    protected ReadCardResponse retryByProcessor(ReadCardRequest readCardRequest, ReadCardResponse earlierReadCardResponse) {
        return onReadCardResponseWaveAgain(readCardRequest, earlierReadCardResponse);
    }

    @Override
    protected boolean checkForRetry(ReadCardResponse readCardResponse) {
        return readCardResponse.getResult() == Result.SWIPE_AGAIN;
    }

    @Override
    protected ReadCardResponse readCardFromFallbackProcessor(ReadCardRequest readCardRequest) {
        return this.forceChipCardRead(readCardRequest);
    }

    @Override
    protected boolean checkFallback(ReadCardResponse readCardResponse) {
        MagneticSwipe.getInstance().onMagneticSwipeSuccess();
        return this.checkForForceChipCardRead(readCardResponse);
    }

    @Override
    public ReadCardResponse processCard(String stan, PurchaseRequest purchaseRequest) {
        PinBlock pinBlock = new PinBlock("", true);
        Cvm cvm = this.retrieveCvm(
                purchaseRequest.getTransactionType(),
                purchaseRequest.getCardDetails()
        );
        boolean pinRequired = cvm.equals(Cvm.ENCIPHERED_PIN_ONLINE);
        String pinPadAmountMessage = TransactionUtils.retrievePinPadMessageWithAmount(purchaseRequest.getAmount());
        if (pinRequired) {
            pinBlock = this.retrievePinBlock(
                    purchaseRequest.getCardDetails(),
                    pinPadAmountMessage
            );
        }
        ReadCardResponse readCardResponse = new ReadCardResponse(
                purchaseRequest.getCardDetails(),
                Result.SUCCESS,
                "Successful"
        );
        readCardResponse.setCvm(cvm);
        readCardResponse.getCardDetails().setPinBlock(pinBlock);
        readCardResponse
                .getCardDetails()
                .setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
        readCardResponse.setFallbackIccToMag(purchaseRequest.isFallbackIccToMag());
        return readCardResponse;
    }

    private PinBlock retrievePinBlock(CardDetails cardDetails, String pinPadAmountMessage) {
        String primaryAccountNumber = cardDetails.getPrimaryAccountNumber();
        if (!StringUtils.isEmpty(primaryAccountNumber)) {
            PinRequest pinRequest = preparePinRequest(cardDetails, pinPadAmountMessage);
            PinResponse response = transactionAuthenticator.authenticateUser(pinRequest);
            System.out.println("::: PIN RESPONSE ::: " + response.toString());
            if (response.getResult() == Result.SUCCESS)
                return response.getPinBlock();
            else if (response.getResult() == Result.USER_CANCELLED)
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            else if (response.getResult() == Result.TIMEOUT)
                throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
            else
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    private PinRequest preparePinRequest(CardDetails cardDetails, String pinPadAmountMessage) {
        PinRequest pinRequest = new PinRequest(
                cardDetails.getPrimaryAccountNumber(),
                NepsConstant.PIN_PAD_MESSAGE,
                PinBlockFormat.ISO9564_FORMAT_0,
                4,
                12,
                30,
                pinPadAmountMessage,
                true,
               false,
                "Enter PIN"
        );
        CardSummary cardSummary = new CardSummary(
                cardDetails.getCardSchemeLabel(),
                cardDetails.getPrimaryAccountNumber(),
                cardDetails.getCardHolderName(),
                cardDetails.getExpiryDate()
        );
        pinRequest.setCardSummary(cardSummary);
        return pinRequest;
    }

    private Cvm retrieveCvm(TransactionType transactionType, CardDetails cardDetails) {
        String shortCardScheme = cardDetails.getCardScheme().getCardSchemeId();
        if (this.retrieveCardSchemeParameters(CardUtils.retrieveCardSchemeType(shortCardScheme),
                FieldAttributes.CARD_SCHEME_S1_F20_CHECK_SERVICE_CODE).equals("1")) {
            if (ServiceCodeUtils.isPinRequired(cardDetails.getTrackTwoData()))
                return Cvm.ENCIPHERED_PIN_ONLINE;
        }
        TransactionSetTableVerifier verifier = new TransactionSetTableVerifier(
                this.applicationRepository,
                shortCardScheme,
                TransactionUtils.mapPurchaseTypeWithTransactionType(transactionType));
        String cvmToPerform = verifier.getCardHolderVerificationMethod();
        switch (cvmToPerform) {
            case "1":
                return Cvm.SIGNATURE;
            case "2":
            case "3":
                return Cvm.ENCIPHERED_PIN_ONLINE;
            default:
                return Cvm.NO_CVM;
        }
    }

    private String retrieveCardSchemeParameters(CardSchemeType cardSchemeType, FieldAttributes fieldAttributes) {
        CardSchemeRetrieveRequest cardSchemeRetrieveRequest = new CardSchemeRetrieveRequest(
                cardSchemeType.getCardSchemeId(),
                fieldAttributes
        );
        CardSchemeRetrieveResponse cardSchemeRetrieveResponse
                = this.applicationRepository.retrieveCardScheme(cardSchemeRetrieveRequest);
        return cardSchemeRetrieveResponse.getData();
    }

}
