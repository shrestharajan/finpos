package global.citytech.finpos.merchant.data.datasource.app.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import global.citytech.finpos.merchant.domain.model.app.Config

/**
 * Created by Unique Shakya on 1/28/2021.
 */
@Dao
interface ConfigsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(config: Config)

    @Query("SELECT id, amount_per_transaction, reconcile_time, keys, additional_data FROM configs")
    fun getConfig():List<Config>

    @Query("SELECT amount_per_transaction FROM configs")
    fun getAmountPerTransaction(): Long

    @Query("SELECT reconcile_time FROM configs")
    fun getReconcileTime(): String

    @Query("DELETE FROM configs")
    fun nukeTableData()
}