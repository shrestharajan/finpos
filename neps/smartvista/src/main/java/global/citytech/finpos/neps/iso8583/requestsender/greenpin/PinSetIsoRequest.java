package global.citytech.finpos.neps.iso8583.requestsender.greenpin;

import java.math.BigDecimal;

import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finposframework.switches.PosEntryMode;

public class PinSetIsoRequest extends TransactionIsoRequest {
    private String stan;
    private PosEntryMode posEntryMode;
    private String posConditionCode;

    public String getStan() {
        return stan;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public static final class Builder {
        private String pan;
        private BigDecimal transactionAmount;
        private String stan;
        private String localDateTime;
        private String expirationDate;
        private PosEntryMode posEntryMode;
        private String posConditionCode;
        private String track2Data;
        private String additionalData;
        private String currencyCode;
        private String emvData;

        private Builder() {
        }

        public static PinSetIsoRequest.Builder newInstance() {
            return new PinSetIsoRequest.Builder();
        }

        public PinSetIsoRequest.Builder stan(String stan) {
            this.stan = stan;
            return this;
        }

        public PinSetIsoRequest.Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public PinSetIsoRequest.Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public PinSetIsoRequest.Builder localDateTime(String localDataTime) {
            this.localDateTime = localDataTime;
            return this;
        }

        public PinSetIsoRequest.Builder expirationDate(String expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public PinSetIsoRequest.Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public PinSetIsoRequest.Builder additionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public PinSetIsoRequest.Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }


        public PinSetIsoRequest.Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public PinSetIsoRequest.Builder posEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public PinSetIsoRequest.Builder posConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public PinSetIsoRequest build() {
            PinSetIsoRequest PinSetIsoRequest = new PinSetIsoRequest();
            PinSetIsoRequest.pan = this.pan;
            PinSetIsoRequest.transactionAmount = this.transactionAmount;
            PinSetIsoRequest.stan = this.stan;
            PinSetIsoRequest.localDateTime = this.localDateTime;
            PinSetIsoRequest.expiryDate = this.expirationDate;
            PinSetIsoRequest.posEntryMode = this.posEntryMode;
            PinSetIsoRequest.posConditionCode = this.posConditionCode;
            PinSetIsoRequest.track2Data = this.track2Data;
            PinSetIsoRequest.additionalData = this.additionalData;
            PinSetIsoRequest.currencyCode = this.currencyCode;
            PinSetIsoRequest.emvData = this.emvData;
            return PinSetIsoRequest;
        }
    }
}
