package global.citytech.neps.datasource.model

import global.citytech.neps.domain.model.Logo

data class LogoEntity(
    var id: String,
    var appWallpaper: String? = null,
    var displayLogo: String? = null,
    var printLogo: String? = null
)

fun LogoEntity.mapToDomain(): Logo = Logo(id, appWallpaper, displayLogo, printLogo)
fun List<LogoEntity>.mapToDomain(): List<Logo> = map { it.mapToDomain() }