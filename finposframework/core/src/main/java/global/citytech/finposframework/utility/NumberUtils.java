package global.citytech.finposframework.utility;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import global.citytech.finposframework.log.Logger;;

/** User: Surajchhetry Date: 2/19/20 Time: 4:49 PM */
public class NumberUtils {

  private static Logger logger = Logger.getLogger(NumberUtils.class.getName());

  public static byte[] toBytes(int data, int length) {
    if (data < 0) data = data * -1;
    byte[] iByte = new byte[length];
    if (length <= 0 || length > 4)
      throw new IllegalArgumentException("Invalid length. Must be less then equal to 4");
    if (length >= 4) {
      iByte[0] = (byte) (data >>> 24);
      iByte[1] = (byte) (data >>> 16);
      iByte[2] = (byte) (data >>> 8);
      iByte[3] = (byte) (data);
    }
    if (length == 3) {
      iByte[0] = (byte) ((data >>> 16) & 0xFF);
      iByte[1] = (byte) ((data >>> 8) & 0xFF);
      iByte[2] = (byte) (data & 0xFF);
    }

    if (length == 2) {
      iByte[0] = (byte) ((data >>> 8) & 0xFF);
      iByte[1] = (byte) (data & 0xFF);
    }

    if (length == 1) {
      iByte[0] = (byte) (data & 0xFF);
    }
    return iByte;
  }

  /**
   * Converts a byte array of hex into an integer
   *
   * @param bytes
   * @return integer representation of bytes
   */
  public static int fromHexASCIIBytesToInt(byte[] bytes) {
    if (bytes == null || bytes.length == 0) {
      return 0;
    }
    ByteBuffer byteBuffer = ByteBuffer.allocate(4);

    for (int i = 0; i < 4 - bytes.length; i++) {
      byteBuffer.put((byte) 0);
    }
    for (int i = 0; i < bytes.length; i++) {
      byteBuffer.put(bytes[i]);
    }
    ((Buffer) byteBuffer).position(0);
    return byteBuffer.getInt();
  }

  public static int toInt(byte[] data) {
    logger.log("Provided Data::" + HexDump.toHexString(data));
    String sData = HexDump.toHexString(data);
    if (sData.isEmpty())
      return -1;
    logger.log(" sDATA :: " + sData);
    return Integer.parseInt(sData);
  }
}
