package global.citytech.finpos.merchant.domain.usecase.core.reconciliation

import global.citytech.finpos.merchant.domain.model.reconciliation.ReconciliationResponseEntity
import global.citytech.finpos.merchant.domain.model.reconciliation.clear.BatchClearResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.reconciliation.ReconciliationRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/23/2020.
 */
class CoreReconciliationUseCase(private val reconciliationRepository: ReconciliationRepository) {

    fun reconcile(
        configurationItem: ConfigurationItem,
        settlementActive: Boolean
    ): Observable<ReconciliationResponseEntity> {
        return reconciliationRepository.reconcile(configurationItem, settlementActive)
    }

    fun checkReconciliationRequired(configurationItem: ConfigurationItem): Observable<Boolean> =
        reconciliationRepository.checkReconciliationRequired(configurationItem)

    fun clearBatch(configurationItem: ConfigurationItem): Observable<BatchClearResponseEntity> =
        reconciliationRepository.clearBatch(configurationItem)
}