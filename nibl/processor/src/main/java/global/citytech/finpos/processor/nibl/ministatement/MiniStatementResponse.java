package global.citytech.finpos.processor.nibl.ministatement;

import global.citytech.finpos.processor.nibl.greenpin.GreenPinResponse;
import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finposframework.iso8583.IsoMessageResponse;

public class MiniStatementResponse extends TransactionResponseModel {
    private String debugRequestMessage;
    private String debugResponseMessage;
    private String message;
    private String stan;
    private boolean approved;
    private boolean shouldPrintCustomerCopy;
    private String miniStatementData;


    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }


    public String getMessage() {
        return message;
    }

    public String getStan() {
        return stan;
    }


    public boolean isApproved() {
        return approved;
    }

    public String getMiniStatementData() {
        return miniStatementData;
    }

    public boolean isShouldPrintCustomerCopy() {
        return shouldPrintCustomerCopy;
    }

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private String message;
        private String stan;
        private boolean approved;
        private String miniStatement;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static MiniStatementResponse.Builder newInstance() {
            return new MiniStatementResponse.Builder();
        }

        public MiniStatementResponse.Builder withDebugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public MiniStatementResponse.Builder withDebugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public MiniStatementResponse.Builder withShouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public MiniStatementResponse.Builder withApproved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public MiniStatementResponse.Builder withMiniStatementData(String miniStatementData) {
            this.miniStatement = miniStatementData;
            return this;
        }

        public MiniStatementResponse.Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public MiniStatementResponse.Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }


        public MiniStatementResponse build() {
            MiniStatementResponse MiniStatementResponse = new MiniStatementResponse();
            MiniStatementResponse.debugRequestMessage = this.debugRequestMessage;
            MiniStatementResponse.debugResponseMessage = this.debugResponseMessage;
            MiniStatementResponse.approved = this.approved;
            MiniStatementResponse.miniStatementData = this.miniStatement;
            MiniStatementResponse.message = this.message;
            MiniStatementResponse.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            MiniStatementResponse.stan = this.stan;
            return MiniStatementResponse;
        }
    }
}
