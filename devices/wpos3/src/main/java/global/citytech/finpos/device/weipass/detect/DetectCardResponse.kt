package global.citytech.finpos.device.weipass.detect

import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.CardType

/**
 * Created by Rishav Chudal on 5/12/20.
 */
data class DetectCardResponse(
    val result: Result,
    val message: String,
    val cardDetected: CardType,
    val outData: ByteArray?
)