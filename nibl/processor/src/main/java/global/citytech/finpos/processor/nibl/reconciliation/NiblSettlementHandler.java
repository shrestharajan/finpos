package global.citytech.finpos.processor.nibl.reconciliation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationHandler;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationTotals;

/**
 * Created by Unique Shakya on 11/12/2020.
 */
public class NiblSettlementHandler implements ReconciliationHandler {

    private ReconciliationRepository reconciliationRepository;
    private String batchNumber;
    private ApplicationRepository applicationRepository;
    private TerminalRepository terminalRepository;

    public NiblSettlementHandler(TerminalRepository terminalRepository, ReconciliationRepository reconciliationRepository, String batchNumber,
                                 ApplicationRepository applicationRepository) {
        this.terminalRepository = terminalRepository;
        this.reconciliationRepository = reconciliationRepository;
        this.batchNumber = batchNumber;
        this.applicationRepository = applicationRepository;
    }

    @Override
    public ReconciliationTotals prepareReconciliationTotals() {
        Map<CardSchemeType, NiblCardSchemeTotals> totalsMap = new HashMap<>();
        List<CardSchemeType> cardSchemeTypes = this.applicationRepository.getSupportedCardSchemes();
        for (CardSchemeType cardSchemeType : cardSchemeTypes) {
            if (cardSchemeType != CardSchemeType.NONE)
                totalsMap.put(cardSchemeType, this.prepareTotalsWithRespectToCardSchemeType(cardSchemeType));
        }
        return new NiblSettlementTotals(this.terminalRepository.getTerminalTransactionCurrencyName(), totalsMap);
    }

    private NiblCardSchemeTotals prepareTotalsWithRespectToCardSchemeType(CardSchemeType cardSchemeType) {
        return NiblCardSchemeTotals.Builder.newInstance()
                .withTransactionCurrencyName(this.terminalRepository.getTerminalTransactionCurrencyName())
                .withCardSchemeType(cardSchemeType)
                .withSalesCount(this.prepareSalesCount(cardSchemeType))
                .withSalesAmount(this.prepareSalesAmount(cardSchemeType))
                .withSalesRefundCount(this.prepareSalesRefundCount(cardSchemeType))
                .withSalesRefundAmount(this.prepareSalesRefundAmount(cardSchemeType))
                .withDebitCount(this.prepareDebitCount(cardSchemeType))
                .withDebitAmount(this.prepareDebitAmount(cardSchemeType))
                .withDebitRefundCount(this.prepareDebitRefundCount(cardSchemeType))
                .withDebitRefundAmount(this.prepareDebitRefundAmount(cardSchemeType))
                .withAuthCount(this.prepareAuthCount(cardSchemeType))
                .withAuthAmount(this.prepareAuthAmount(cardSchemeType))
                .withAuthRefundCount(this.prepareAuthRefundCount(cardSchemeType))
                .withAuthRefundAmount(this.prepareAuthRefundAmount(cardSchemeType))
                .withVoidCount(this.prepareVoidCount(cardSchemeType))
                .withVoidAmount(this.prepareVoidAmount(cardSchemeType))
                .build();
    }

    private long prepareVoidAmount(CardSchemeType cardSchemeType) {
        return this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardSchemeType,
                this.batchNumber, TransactionType.VOID, TransactionType.PURCHASE) +
                this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardSchemeType,
                        this.batchNumber, TransactionType.CASH_VOID, TransactionType.CASH_ADVANCE);
    }

    private long prepareVoidCount(CardSchemeType cardSchemeType) {
        return this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardSchemeType,
                this.batchNumber, TransactionType.VOID) + this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardSchemeType,
                this.batchNumber, TransactionType.CASH_VOID) ;
    }

    private long prepareAuthRefundAmount(CardSchemeType cardScheme) {
        return 0;
    }

    private long prepareAuthRefundCount(CardSchemeType cardScheme) {
        return 0;
    }

    private long prepareAuthAmount(CardSchemeType cardScheme) {
        return this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme,
                this.batchNumber, TransactionType.PRE_AUTH);
    }

    private long prepareAuthCount(CardSchemeType cardScheme) {
        return this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardScheme,
                this.batchNumber, TransactionType.PRE_AUTH);
    }

    private long prepareDebitRefundAmount(CardSchemeType cardScheme) {
        return 0;
    }

    private long prepareDebitRefundCount(CardSchemeType cardScheme) {
        return 0;
    }

    private long prepareDebitAmount(CardSchemeType cardScheme) {
        return 0;
    }

    private long prepareDebitCount(CardSchemeType cardScheme) {
        return 0;
    }

    private long prepareSalesRefundAmount(CardSchemeType cardScheme) {
        return this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme,
                this.batchNumber, TransactionType.REFUND) -
                this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardScheme,
                        this.batchNumber, TransactionType.VOID, TransactionType.REFUND);
    }

    private long prepareSalesRefundCount(CardSchemeType cardScheme) {
        return this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.REFUND) -
                this.reconciliationRepository.getCountByOriginalTransactionTypeWithCardScheme(cardScheme,
                        this.batchNumber, TransactionType.VOID, TransactionType.REFUND);
    }

    private long prepareSalesAmount(CardSchemeType cardScheme) {
        return this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.PURCHASE) +
                this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.CASH_ADVANCE) +
                this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.TIP_ADJUSTMENT) +
                this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.AUTH_COMPLETION) -
                this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.VOID, TransactionType.PURCHASE) -
                this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.CASH_VOID, TransactionType.CASH_ADVANCE);
    }

    private long prepareSalesCount(CardSchemeType cardScheme) {
        return this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.PURCHASE) +
                this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.CASH_ADVANCE) +
                this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.AUTH_COMPLETION) -
                this.reconciliationRepository.getCountByOriginalTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.VOID, TransactionType.PURCHASE) -
                this.reconciliationRepository.getCountByOriginalTransactionTypeWithCardScheme(cardScheme, this.batchNumber, TransactionType.CASH_VOID, TransactionType.CASH_ADVANCE);
    }
}
