package global.citytech.finpos.merchant.presentation.greenpin

data class CardDataModel(

    val CardNumber : String,
    val CardScheme : String,
    val HolderName : String,
    val Expiry : String

)
