package global.citytech.finposframework.utility;

import org.junit.Test;

import java.util.List;

import global.citytech.finposframework.hardware.io.cards.tvr.TvrResults;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Unique Shakya on 11/24/2020.
 */
public class TvrResultsParserTest {

    @Test
    public void tvrParseTest() {
        List<TvrResults> results = TvrResultParser.parseTvr("2222222222");
        for (TvrResults tvrResults : results) {
            System.out.println("("+ tvrResults.name() + ")" + tvrResults.getDescription());
        }
        assertNotNull(results);
    }
}
