package global.citytech.payment.sdk.api;

/**
 * Created by Rishav Chudal on 6/11/21.
 */
public interface PaymentService {

    PaymentResponse doPurchase(PurchasePaymentRequest purchasePaymentRequest);

    PaymentResponse performVoid(VoidPaymentRequest voidPaymentRequest);

    void doPaymentCancellation(PaymentCancellationRequest paymentCancellationRequest);
}
