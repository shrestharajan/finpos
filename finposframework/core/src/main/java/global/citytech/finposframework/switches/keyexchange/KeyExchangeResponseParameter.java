package global.citytech.finposframework.switches.keyexchange;

import global.citytech.finposframework.switches.ResponseParameter;

/** @author rajudhital on 4/21/20 */
public interface KeyExchangeResponseParameter extends ResponseParameter {}
