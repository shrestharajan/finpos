package global.citytech.finpos.merchant.data.repository.core.idle

import global.citytech.finpos.merchant.data.datasource.core.idle.IdlePurchaseDataSource
import global.citytech.finpos.merchant.domain.model.purchase.PurchaseResponseEntity
import global.citytech.finpos.merchant.domain.model.purchase.RefundResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.idle.IdlePurchaseRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.purchase.PurchaseRequestItem
import io.reactivex.Observable

/**
 * Created by Rishav Chudal on 11/24/20.
 */
class IdlePurchaseRepositoryImpl(
    private val idlePurchaseDataSource: IdlePurchaseDataSource
) : IdlePurchaseRepository {
    override fun purchase(
        configurationItem: ConfigurationItem,
        purchaseRequestItem: PurchaseRequestItem
    ): Observable<PurchaseResponseEntity> {
        return idlePurchaseDataSource.purchase(configurationItem, purchaseRequestItem)
    }
}