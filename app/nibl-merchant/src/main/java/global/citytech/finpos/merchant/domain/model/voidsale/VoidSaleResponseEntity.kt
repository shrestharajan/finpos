package global.citytech.finpos.merchant.domain.model.voidsale

/**
 * Created by Saurav Ghimire on 4/19/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


data class VoidSaleResponseEntity(
    var debugRequestMessage: String? = null,
    val debugResponseMessage: String? = null,
    val stan: String? = null,
    val message: String? = null,
    val isApproved :Boolean= false,
    val shouldPrintCustomerCopy :Boolean= false
)

