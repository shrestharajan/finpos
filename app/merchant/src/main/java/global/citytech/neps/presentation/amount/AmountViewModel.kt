package global.citytech.neps.presentation.amount

import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseViewModel
import global.citytech.neps.presentation.utils.StringUtils

/**
 * Created by Rishav Chudal on 6/17/20.
 */
class AmountViewModel: BaseViewModel() {
    private var enteredAmount: Long = 0

    var integralAmount = MutableLiveData<String>()
    var fractionalAmount = MutableLiveData<String>()

    fun onNumericButtonPressed(buttonText: String, limit: Int) {
        if (enteredAmount.toString().length < limit) {
            enteredAmount = (enteredAmount.toString().plus(buttonText)).toLong()
            calculateAmount()
        }
    }

    fun calculateAmount() {
        val enteredMoneyInString = StringUtils.decimalFormatterFor(enteredAmount)
        val amountBeforeDecimal = enteredMoneyInString.substring(0, enteredMoneyInString.length - 2)
        val amountAfterDecimal = enteredMoneyInString.substring(enteredMoneyInString.length - 2)
        integralAmount.value = amountBeforeDecimal
        fractionalAmount.value = amountAfterDecimal
    }

    fun onClearButtonClicked() {
        if (enteredAmount > 0) {
            enteredAmount /= 10
            calculateAmount()
        }
    }

    fun onClearButtonLongClicked() {
        enteredAmount = 0
        calculateAmount()
    }
}