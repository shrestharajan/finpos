package global.citytech.finposframework.iso8583;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import global.citytech.finposframework.log.Logger;;

import global.citytech.finposframework.utility.EncodingUtils;
import global.citytech.finposframework.utility.HexDump;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/** User: Surajchhetry Date: 2/18/20 Time: 10:38 AM */
public class BitmapTest {

  private static final Logger LOGGER = Logger.getLogger(BitmapTest.class.getName());

  public interface Messages {

    String getMessage(String code);
  }

  public class NEPSEMessages implements Messages {

    private Map<String, String> messageStore = new HashMap<>();

    @Override
    public String getMessage(String key) {
      if (messageStore.containsKey(key)) return messageStore.get(key);
      return "";
    }
  }

  @Test
  public void testPrimaryBinaryBitMap() {
    BitMap bitMap = new BitMap();
    bitMap.set(3);
    bitMap.set(7);
    bitMap.set(11);
    bitMap.set(24);
    bitMap.set(41);
    bitMap.set(42);
    String binary = bitMap.asBinary();
    assertEquals("0010001000100000000000010000000000000000110000000000000000000000", binary);
  }

  @Test
  public void testSecondaryBinaryBitMap() {
    BitMap bitMap = new BitMap();
    bitMap.set(3);
    bitMap.set(7);
    bitMap.set(11);
    bitMap.set(24);
    bitMap.set(41);
    bitMap.set(42);
    bitMap.set(70);
    String binary = bitMap.asBinary();
    assertEquals(
        "10100010001000000000000100000000000000001100000000000000000000000000010000000000000000000000000000000000000000000000000000000000",
        binary);
  }

  @Test
  public void testHexEncodedPrimaryBitMap() {
    BitMap bitMap = new BitMap();
    bitMap.set(3);
    bitMap.set(7);
    bitMap.set(11);
    bitMap.set(24);
    bitMap.set(41);
    bitMap.set(42);
    String hex = bitMap.asHexEncoded();
    LOGGER.log(hex);
    assertEquals(
        "30303130303031303030313030303030303030303030303130303030303030303030303030303030313130303030303030303030303030303030303030303030",
        hex);
  }

  @Test
  public void testHexEncodedBytePrimaryBitMap() {
    ByteArrayOutputStream expected = new ByteArrayOutputStream();
    try {
      BitMap bitMap = new BitMap();
      bitMap.set(3);
      bitMap.set(7);
      bitMap.set(11);
      bitMap.set(24);
      bitMap.set(41);
      bitMap.set(42);
      byte[] hex = bitMap.asHexASCII();
      LOGGER.log(HexDump.dumpHexString(hex));
      LOGGER.log(bitMap.asHexValue());
      expected.write(EncodingUtils.hex2Bytes("2220010000C00000"));
      assertArrayEquals(expected.toByteArray(), hex);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testBinaryBitMapParser() {
    BitMap bitMap =
        BitMap.parseFromBinaryString(
            "0010001000100000000000010000000000000000110000000000000000000000");
    Integer[] indexes = {3, 7, 11, 24, 41, 42};
    assertArrayEquals(indexes, bitMap.getFields().toArray());
  }

  @Test
  public void testHexEncodedBitMapParser() {
    BitMap bitMap =
        BitMap.parseFromHexEncoded(
            "30303130303031303030313030303030303030303030303130303030303030303030303030303030313130303030303030303030303030303030303030303030");
    Integer[] indexes = {3, 7, 11, 24, 41, 42};
    assertArrayEquals(indexes, bitMap.getFields().toArray());
  }

  @Test
  public void testHexedPrimaryBitMapParser() {
    String hexedBitMap = "2220010000C00000";
    BitMap bitMap = BitMap.parseFromHexValue(hexedBitMap);
    Integer[] indexes = {3, 7, 11, 24, 41, 42};
    assertArrayEquals(indexes, bitMap.getFields().toArray());
  }

  @Test
  public void testHexedSecondaryBitMapParser() {
    String hexedBitMap = "E0000000000000020100000000000003";
    BitMap bitMap = BitMap.parseFromHexValue(hexedBitMap);
    Integer[] indexes = {1, 2, 3, 63, 72, 127, 128};
    assertArrayEquals(indexes, bitMap.getFields().toArray());
  }

  @Test
  public void testHexedToBinary() {
    String hexedBitMap = "2220010000C00000";
    String binary = EncodingUtils.fromHexValueToBinary(hexedBitMap);
    assertEquals("10001000100000000000010000000000000000110000000000000000000000", binary);
  }
}
