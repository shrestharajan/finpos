package global.citytech.finpos.merchant.presentation.merchantqr

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityMerchantQrBinding
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import kotlinx.android.synthetic.main.activity_merchant_qr.*

class MerchantQrActivity : AppBaseActivity<ActivityMerchantQrBinding, MerchantQrViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObservers()
        initViews()
        getViewModel().retrieveQrData()
    }

    private fun initViews() {
        iv_back.setOnClickListener {
            finish()
        }
    }

    private fun initObservers() {
        getViewModel().merchantQr.observe(this, Observer {
            Glide.with(this).load(it.merchantQrBitmap).into(iv_merchant_qr)
            tv_merchant_name.text = it.retailerName
            tv_merchant_address.text = it.retailerAddress
            tv_msg_register.visibility = View.VISIBLE
        })
    }

    override fun getBindingVariable(): Int = BR.merchantQrViewModel

    override fun getLayout(): Int = R.layout.activity_merchant_qr

    override fun getViewModel(): MerchantQrViewModel =
        ViewModelProviders.of(this)[MerchantQrViewModel::class.java]
}