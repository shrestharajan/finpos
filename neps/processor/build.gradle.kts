plugins{
    id("java-library")
    id("kotlin")
}

dependencies {
    implementation (fileTree("dir" to "libs", "include" to listOf ("*.jar")))
    implementation(project(":neps:smartvista"))
    implementation(project(":finposframework:core"))
    implementation ("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlinVersion}")
    testImplementation ("junit:junit:4.13")
    testImplementation ("org.mockito:mockito-core:3.4.6")
    testImplementation ("org.powermock:powermock-core:2.0.7")
    testImplementation ("org.powermock:powermock-module-junit4:2.0.7")
    testImplementation ("org.powermock:powermock-api-mockito2:2.0.7")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
    kotlinOptions.jvmTarget = "1.8"
}


