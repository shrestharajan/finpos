package global.citytech.finpos.merchant.domain.usecase.core.cashadvance

import global.citytech.finpos.merchant.domain.model.cashadvance.CashAdvanceResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.cashadvance.CashAdvanceRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.cashadvance.CashAdvanceRequestItem
import global.citytech.finpos.merchant.presentation.model.cashadvance.ManualCashAdvanceRequestItem
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 2/4/2021.
 */
class CoreCashAdvanceUseCase(private val cashAdvanceRepository: CashAdvanceRepository) {
    fun performCashAdvance(
        configurationItem: ConfigurationItem,
        cashAdvanceRequestItem: CashAdvanceRequestItem
    ): Observable<CashAdvanceResponseEntity> {
        return cashAdvanceRepository.performCashAdvance(configurationItem, cashAdvanceRequestItem)
    }

    fun performManualCashAdvance(
        configurationItem: ConfigurationItem,
        manualCashAdvanceRequestItem: ManualCashAdvanceRequestItem
    ): Observable<CashAdvanceResponseEntity> {
        return cashAdvanceRepository.performManualCashAdvance(
            configurationItem,
            manualCashAdvanceRequestItem
        )
    }
}