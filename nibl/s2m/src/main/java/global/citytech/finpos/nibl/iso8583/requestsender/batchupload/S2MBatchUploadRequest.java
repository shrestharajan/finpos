package global.citytech.finpos.nibl.iso8583.requestsender.batchupload;

import java.math.BigDecimal;

import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.transaction.Request;

/**
 * Created by Unique Shakya on 5/11/2021.
 */
public class S2MBatchUploadRequest implements Request {

    private String pan;
    private String originalProcessingCode;
    private BigDecimal transactionAmount;
    private String localTime;
    private String localDate;
    private String expirationDate;
    private PosEntryMode posEntryMode;
    private String posConditionCode;
    private String functionCode;
    private String originalRrn;
    private String originalApprovalCode;
    private String originalResponseCode;
    private String originalData;
    private String invoiceNumber;

    public String getPan() {
        return pan;
    }

    public String getOriginalProcessingCode() {
        return originalProcessingCode;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public String getLocalTime() {
        return localTime;
    }

    public String getLocalDate() {
        return localDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public String getFunctionCode() {
        return functionCode;
    }

    public String getOriginalRrn() {
        return originalRrn;
    }

    public String getOriginalApprovalCode() {
        return originalApprovalCode;
    }

    public String getOriginalResponseCode() {
        return originalResponseCode;
    }

    public String getOriginalData() {
        return originalData;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }


    public static final class Builder {
        private String pan;
        private String originalProcessingCode;
        private BigDecimal transactionAmount;
        private String localTime;
        private String localDate;
        private String expirationDate;
        private PosEntryMode posEntryMode;
        private String posConditionCode;
        private String functionCode;
        private String originalRrn;
        private String originalApprovalCode;
        private String originalResponseCode;
        private String originalData;
        private String invoiceNumber;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public Builder originalProcessingCode(String originalProcessingCode) {
            this.originalProcessingCode = originalProcessingCode;
            return this;
        }

        public Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public Builder localTime(String localTime) {
            this.localTime = localTime;
            return this;
        }

        public Builder localDate(String localDate) {
            this.localDate = localDate;
            return this;
        }

        public Builder expirationDate(String expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public Builder posEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public Builder posConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public Builder functionCode(String functionCode) {
            this.functionCode = functionCode;
            return this;
        }

        public Builder originalRrn(String originalRrn) {
            this.originalRrn = originalRrn;
            return this;
        }

        public Builder originalApprovalCode(String originalApprovalCode) {
            this.originalApprovalCode = originalApprovalCode;
            return this;
        }

        public Builder originalResponseCode(String originalResponseCode) {
            this.originalResponseCode = originalResponseCode;
            return this;
        }

        public Builder originalData(String originalData) {
            this.originalData = originalData;
            return this;
        }

        public Builder invoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }

        public S2MBatchUploadRequest build() {
            S2MBatchUploadRequest s2MBatchUploadRequest = new S2MBatchUploadRequest();
            s2MBatchUploadRequest.originalRrn = this.originalRrn;
            s2MBatchUploadRequest.originalApprovalCode = this.originalApprovalCode;
            s2MBatchUploadRequest.originalProcessingCode = this.originalProcessingCode;
            s2MBatchUploadRequest.originalData = this.originalData;
            s2MBatchUploadRequest.localDate = this.localDate;
            s2MBatchUploadRequest.localTime = this.localTime;
            s2MBatchUploadRequest.pan = this.pan;
            s2MBatchUploadRequest.transactionAmount = this.transactionAmount;
            s2MBatchUploadRequest.invoiceNumber = this.invoiceNumber;
            s2MBatchUploadRequest.posConditionCode = this.posConditionCode;
            s2MBatchUploadRequest.originalResponseCode = this.originalResponseCode;
            s2MBatchUploadRequest.expirationDate = this.expirationDate;
            s2MBatchUploadRequest.functionCode = this.functionCode;
            s2MBatchUploadRequest.posEntryMode = this.posEntryMode;
            return s2MBatchUploadRequest;
        }
    }
}
