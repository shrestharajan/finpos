package global.citytech.finpos.neps.iso8583;

//global.citytech.finposframework.log.Logger;

import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.utility.HexDump;

/**
 * User: Surajchhetry Date: 2/19/20 Time: 5:25 PM
 */
public class NepsMessageBuilderTest {

    private final Logger logger = Logger.getLogger(NepsMessageBuilderTest.class.getName());

    // @Test
    public void isoMsgClientTest() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Network_Management,
                        MTI.MessageFunction.Request,
                        MTI.Originator.Acquirer);
        Iso8583Msg msg = new Iso8583Msg(mti);
        msg.addField(new DataElement(3, "990000"));
        msg.addField(new DataElement(7, "1111111111"));
        msg.addField(new DataElement(11, "000001"));
        msg.addField(new DataElement(24, "811"));
        msg.addField(new DataElement(41, "41234027"));
        msg.addField(new DataElement(42, "100512345678910"));

        NepsMessagePayloadBuilder builder = new NepsMessagePayloadBuilder(new NepsSpecInfo(), msg);
        MessagePayload payload = builder.build();
        logger.debug(HexDump.dumpHexString(payload.getAsBytes()));
    }
}
