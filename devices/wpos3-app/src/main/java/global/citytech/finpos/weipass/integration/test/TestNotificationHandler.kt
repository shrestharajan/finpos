package global.citytech.finpos.weipass.integration.test

import global.citytech.finposframework.hardware.io.cards.CardDetails
import global.citytech.finposframework.listeners.*
import global.citytech.finposframework.notifier.Notifier

/**
 * Created by Unique Shakya on 9/20/2021.
 */
class TestNotificationHandler: Notifier {
    override fun notify(eventType: Notifier.EventType?, message: String?) {
        TODO("Not yet implemented")
    }
    
    override fun notify(
        eventType: Notifier.EventType?,
        message: String?,
        transactionType: String?
    ) {
        TODO("Not yet implemented")
    }

    override fun notify(
        eventType: Notifier.EventType?,
        cardDetails: CardDetails?,
        cardConfirmationListener: CardConfirmationListener?
    ) {
        TODO("Not yet implemented")
    }

    override fun notify(
        eventType: Notifier.EventType?,
        cardDetails: CardDetails?,
        otpConfirmationListener: OtpConfirmationListener?
    ) {
        TODO("Not yet implemented")
    }

    override fun notify(
        eventType: Notifier.EventType?,
        stan: String?,
        message: String?,
        isApproved: Boolean
    ) {
        TODO("Not yet implemented")
    }


    override fun notify(eventType: Notifier.EventType?) {
        TODO("Not yet implemented")
    }

    override fun notify(
        eventType: Notifier.EventType?,
        cardConfirmationListenerForGreenPin: CardConfirmationListenerForGreenPin?
    ) {
        TODO("Not yet implemented")
    }

    override fun notify(
        eventType: Notifier.EventType?,
        jsonData: String?,
        settlementConfirmationListener: SettlementConfirmationListener?
    ) {
        TODO("Not yet implemented")
    }
}