package global.citytech.nepsadmin

import android.app.Application
import android.content.ServiceConnection
import global.citytech.tms.sdk.IPlatformManager

class NepsAdmin:Application() {
    companion object{
        lateinit var INSTANCE :NepsAdmin
    }
    lateinit var iPlatformManager: IPlatformManager
    lateinit var serviceConnection: ServiceConnection

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }

    fun unbindService(){
        unbindService(serviceConnection)
    }
}