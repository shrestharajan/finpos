package global.citytech.finpos.merchant.presentation.merchantqr

import android.graphics.Bitmap

data class MerchantQr(
        val retailerLogo: ByteArray,
        val retailerName: String,
        val retailerAddress: String,
        val merchantQrBitmap: Bitmap
)