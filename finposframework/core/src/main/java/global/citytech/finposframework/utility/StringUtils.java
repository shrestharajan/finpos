package global.citytech.finposframework.utility;

import java.nio.charset.StandardCharsets;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class StringUtils {
    public static String TAG_EMPTY_STRING = "";
    private static final String HexChars = "1234567890abcdefABCDEF";

    public static String hextToString(String hexString){
        int l = hexString.length();
        byte[] data = new byte[l / 2];
        for (int i = 0; i < l; i += 2) {
            data[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4)
                    + Character.digit(hexString.charAt(i + 1), 16));
        }
        return new String(data, StandardCharsets.UTF_8);
    }

    public static boolean isEmpty(String data) {
        boolean is;
        if (data != null && !"".equals(data) && !"<EMPTY>".equals(data)) {
            is = false;
        } else {
            is = true;
        }
        return is;
    }

    public static String encodeAccountNumber(String charSeq) {
        if (StringUtils.isEmpty(charSeq)) {
            return "";
        }
        try {
            return maskString(charSeq, 6, charSeq.length() - 4, 'X');
        } catch (Exception e) {
            e.printStackTrace();
            return charSeq;
        }
    }

    public static String maskPayerPanNumber(String charSeq) {
        System.out.println("INSIDE MASK PAYER PAN NUMBER");
        if (StringUtils.isEmpty(charSeq)) {
            return "";
        }
        try {
            return maskString(charSeq, 9, charSeq.length() - 2, 'X');
        } catch (Exception e) {
            e.printStackTrace();
            return charSeq;
        }
    }

    public static String maskString(String strText, int start, int end, char maskChar) throws Exception {
        if (strText == null || strText.equals(""))
            return "";

        if (start < 0)
            start = 0;

        if (end > strText.length())
            end = strText.length();

        if (start > end)
            throw new Exception("End index cannot be greater than start index");

        int maskLength = end - start;

        if (maskLength == 0)
            return strText;

        StringBuilder sbMaskString = new StringBuilder(maskLength);

        for (int i = 0; i < maskLength; i++) {
            sbMaskString.append(maskChar);
        }

        return strText.substring(0, start)
                + sbMaskString.toString()
                + strText.substring(start + maskLength);
    }

    public static String formatAmountTwoDecimal(double amount) {
        try {
            DecimalFormat df = new DecimalFormat("0.00");
            return df.format(amount);
        } catch (Exception e) {
            e.printStackTrace();
            return String.valueOf(amount);
        }
    }

    public static String formatAmountTwoDecimal(BigDecimal amount) {
        try {
            DecimalFormat df = new DecimalFormat("0.00");
            return df.format(amount);
        } catch (Exception e) {
            e.printStackTrace();
            return String.valueOf(amount);
        }
    }

    public static String ofRequiredLength(String stringValue, int length) {
        if (stringValue.length() < length) {
            int requiredZeros = length - stringValue.length();
            StringBuilder beforeZeros = new StringBuilder();
            for (int i = 0; i < requiredZeros; i++) {
                beforeZeros.append("0");
            }
            beforeZeros.append(stringValue);
            return beforeZeros.toString();
        }
        return stringValue.substring(0, length);
    }

    public static String ofRequiredLength(String stringValue, int length, int fillTheWhiteSpaceWithSpecificValue) {
        if (stringValue.length() < length) {
            int requiredZeros = length - stringValue.length();
            StringBuilder beforeZeros = new StringBuilder();
            for (int i = 0; i < requiredZeros; i++) {
                beforeZeros.append(fillTheWhiteSpaceWithSpecificValue);
            }
            beforeZeros.append(stringValue);
            return beforeZeros.toString();
        }
        return stringValue.substring(0, length);
    }

    public static String ofRequiredLength(long longValue, int length) {
        String stringValue = String.valueOf(longValue);
        if (stringValue.length() < length) {
            int requiredZeros = length - stringValue.length();
            StringBuilder beforeZeros = new StringBuilder();
            for (int i = 0; i < requiredZeros; i++) {
                beforeZeros.append("0");
            }
            beforeZeros.append(stringValue);
            return beforeZeros.toString();
        }
        return stringValue.substring(0, length);
    }

    public static String toHexaDecimal(String text) {
        StringBuilder hexData = new StringBuilder();

        for (int index = 0; index < text.length(); ++index) {
            hexData.append(Long.toHexString((long) text.charAt(index)));
        }

        return hexData.toString().toUpperCase();
    }

    public static boolean isLengthOdd(String data) {
        boolean isOdd = false;
        if ((data != null) && (data.length() % 2 != 0)) {
            isOdd = true;
        }
        return isOdd;
    }

    public static String dateTimeStamp() {
        return new SimpleDateFormat("yyMMddHHmmss").format(new Date());
    }

    public static String dateTimeStampUpToMillis() {
        return new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date());
    }

    /**
     * @param s source string (with Hex representation)
     * @return byte array
     */
    public static byte[] hexString2bytes (String s)
    {
        if (null == s)
            return null;

        s = trimSpace(s);

        if (false == isHexChar(s, false))
            return null;

        return hex2byte (s, 0, s.length() >> 1);
    }

    public static boolean isHexChar(String hexString, boolean trimSpaceFlag)
    {
        if (null == hexString || 0 == hexString.length())
            return false;

        if (trimSpaceFlag)
            hexString = trimSpace(hexString);

        if (hexString.length() % 2 != 0)
            return false;
        int hexLen = hexString.length();
        for(int i = 0; i < hexLen; i++)
        {
            if (HexChars.indexOf(hexString.charAt(i)) < 0)
                return false;
        }

        return true;
    }

    public static String trimSpace(String oldString)
    {
        if (null == oldString)
            return null;
        if (0 == oldString.length())
            return "";

        StringBuffer sbuf = new StringBuffer();
        int oldLen = oldString.length();
        for(int i = 0; i < oldLen; i++)
        {
            if (' ' != oldString.charAt(i))
                sbuf.append(oldString.charAt(i));
        }
        String returnString = sbuf.toString();
        sbuf = null;
        return returnString;
    }

    /**
     * @param   s       source string
     * @param   offset  starting offset
     * @param   len     number of bytes in destination (processes len*2)
     * @return  byte[len]
     */
    public static byte[] hex2byte (String s, int offset, int len) {
        byte[] d = new byte[len];
        int byteLen = len * 2;
        for (int i=0; i < byteLen; i++) {
            int shift = (i%2 == 1) ? 0 : 4;
            d[i>>1] |= Character.digit(s.charAt(offset+i), 16) << shift;
        }
        return d;
    }

    public static String maskCardNumber(String value) {
        if (isEmpty(value))
            return "NOT PRESENT";
        int firstIndex = (int) (value.length() * .40);
        StringBuilder defaultString = new StringBuilder();
        for (int i = 0; i < value.length() * .40; i++) {
            defaultString.append("X");
        }
        return value.substring(0, firstIndex) +
                defaultString +
                value.substring(firstIndex + defaultString.length());
    }


    public static String maskTrack2(String value) {
        if (isEmpty(value))
            return "NOT PRESENT";
        int firstIndex = (int) (value.length() * .15);
        StringBuilder defaultString = new StringBuilder();
        for (int i = 0; i < value.length() * .70; i++) {
            defaultString.append("X");
        }
        return value.substring(0, firstIndex) +
                defaultString +
                value.substring(firstIndex + defaultString.length());
    }

    public static String getDoubleDigitSize(int size) {
        if (size < 10 && size > 0)
            return "0" + size;
        return "" + size;
    }

    public static String getDoubleDigitSize(String data) {
        String value = "";
        if (!isEmpty(data)) {
            if (data.length() == 1) {
                value = "0".concat(data);
            } else {
                value = data;
            }
        }
        return value;
    }

    public static String dateTimeStampYYYYMMddHHmmss() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }

    public static boolean isAllZero(String str) {
        for (char c : str.toCharArray())
            if (c != '0') {
                return false;
            }
        return true;
    }

    public static String arrangeEncodedPanInGivenGroupSize(String pan, int groupSize) {
        try {
            String encodedPan = encodeAccountNumber(pan);
            StringBuilder stringBuilder = new StringBuilder(encodedPan);
            int index = groupSize;
            while (index <= stringBuilder.length()) {
                stringBuilder.insert(index, " ");
                index += 1;
                index += groupSize;
            }
            return stringBuilder.toString();
        } catch (Exception exception) {
            exception.printStackTrace();
            return "";
        }
    }

    public static String prepareAdditionalDataForActivityLog(String terminalSerialNumber) {
        Map<String, String> map = new HashMap<>();
        map.put("serialNumber", terminalSerialNumber);
        return JsonUtils.toJsonObj(map);
    }
}