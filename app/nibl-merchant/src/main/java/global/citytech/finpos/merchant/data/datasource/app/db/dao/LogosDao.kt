package global.citytech.finpos.merchant.data.datasource.app.db.dao

import androidx.room.*
import global.citytech.finpos.merchant.domain.model.app.Logo


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
@Dao
interface LogosDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(logos: List<Logo>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(logo: Logo): Long

    @Query("SELECT id, app_wallpaper, display_logo, print_logo FROM logos")
    fun getLogoList(): List<Logo>

    @Update
    fun update(logo: Logo):Int

    @Query("DELETE FROM logos WHERE id = :id")
    fun deleteById(id: Long): Int

    @Query("DELETE FROM logos")
    fun nukeTableData()
}