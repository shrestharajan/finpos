package global.citytech.finpos.merchant.presentation.model.setting


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/11/19 - 11:05 AM
*/

data class TerminalSettingRequest(val serialNumber: String)
