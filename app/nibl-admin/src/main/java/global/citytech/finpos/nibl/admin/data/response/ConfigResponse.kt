package global.citytech.finpos.nibl.admin.data.response

data class ConfigResponse(
    var amountLimitPerTransaction: Double? = null,
    var crypto: Crypto? = null,
    var hosts: List<Host>? = null,
    var keys: Keys? = null,
    var logo: Logo? = null,
    var merchant: Merchant? = null,
    var reconcileTime: String? = null,
    var terminal: Terminal? = null,
    var terminalParameter: TerminalParameter? = null
)