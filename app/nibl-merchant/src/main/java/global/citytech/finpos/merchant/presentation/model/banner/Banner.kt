package global.citytech.finpos.merchant.presentation.model.banner

/**
 * Created by Rishav Chudal on 11/3/21.
 */
data class Banner(
    val id: String,
    val title: String,
    val banner: String
)
