package global.citytech.finpos.merchant.domain.usecase.app


import global.citytech.common.data.Response
import global.citytech.finpos.merchant.domain.repository.app.MobilePaymentRepository
import global.citytech.finpos.merchant.domain.usecase.mobile.request.MobileNfcPaymentGenerateRequest
import global.citytech.finpos.merchant.domain.usecase.mobile.request.MobileNfcPaymentStatusRequest
import global.citytech.finpos.merchant.domain.usecase.mobile.response.MobileNfcPaymentStatusResponse
import global.citytech.finpos.merchant.presentation.mobilenfcpayment.NfcResponse
import global.citytech.finposframework.hardware.io.led.LedLight

/**
 * @author SIDDHARTHA GHIMIRE
 */
class MobilePaymentUseCase(private val mobilePaymentRepository: MobilePaymentRepository) {

    fun requestMobileNfcPaymentTransaction(mobileNfcPaymentStatusRequest: MobileNfcPaymentStatusRequest): Response =
        mobilePaymentRepository.requestMobileNfcPaymentTransaction(mobileNfcPaymentStatusRequest)

    fun incrementNfcPaymentIdentifiers() =
        mobilePaymentRepository.incrementNfcPaymentIdentifiers()

    fun parseNfcData(nfcData: ByteArray, aid:String) = mobilePaymentRepository.parseNfcData(nfcData, aid)

    fun prepareMobileNfcPaymentRequest(mobileNfcPaymentGenerateRequest: MobileNfcPaymentGenerateRequest) =
        mobilePaymentRepository.prepareMobileNfcPaymentRequest(mobileNfcPaymentGenerateRequest)

    fun checkMobileNfcPaymentStatus(response: Any): MobileNfcPaymentStatusResponse =
        mobilePaymentRepository.checkMobileNfcPaymentStatus(response)

    fun getNfcData(): NfcResponse? = mobilePaymentRepository.getNfcData()

    fun cancelTimer() = mobilePaymentRepository.cancelTimer()

    fun displaySoundAndLedWithRespectiveDevice(ledLight: LedLight) =
        mobilePaymentRepository.displaySoundAndLedWithRespectiveDevice(ledLight)

    fun hideAlertLed() = mobilePaymentRepository.hideAlertLed()

    fun cleanMobileNfcTask() = mobilePaymentRepository.cleanMobileNfcTask()

    fun disableNfcReaderMode() = mobilePaymentRepository.disableNfcReaderMode()

    fun saveNfcPaymentIdentifiers(nfcPaymentIdentifiers: String) =
        mobilePaymentRepository.saveNfcPaymentIdentifiers(nfcPaymentIdentifiers)
}