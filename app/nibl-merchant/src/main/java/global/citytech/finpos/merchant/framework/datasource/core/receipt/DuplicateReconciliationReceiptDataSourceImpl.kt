package global.citytech.finpos.merchant.framework.datasource.core.receipt

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.receipt.DuplicateReconciliationReceiptDataSource
import global.citytech.finpos.merchant.domain.model.receipt.duplicatereconciliation.DuplicateReconciliationReceiptRequestEntity
import global.citytech.finpos.merchant.domain.model.receipt.duplicatereconciliation.DuplicateReconciliationReceiptResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.ReconciliationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.presentation.data.mapToDuplicateReconciliationReceiptUIModel
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finpos.processor.nibl.receipt.duplicatereconciliation.DuplicateReconciliationReceiptRequestModel
import global.citytech.finpos.processor.nibl.receipt.duplicatereconciliation.DuplicateReconciliationReceiptResponseModel
import global.citytech.finposframework.switches.ProcessorManager
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 12/16/2020.
 */
class DuplicateReconciliationReceiptDataSourceImpl : DuplicateReconciliationReceiptDataSource {
    override fun printReceipt(): Observable<DuplicateReconciliationReceiptResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val reconciliationRepository =
            ReconciliationRepositoryImpl()
        val printerService = PrinterSourceImpl(NiblMerchant.INSTANCE)

        val duplicateReconciliationReceiptRequestEntity =
            DuplicateReconciliationReceiptRequestEntity(reconciliationRepository, printerService)
        val duplicateReconciliationReceiptRequester =
            ProcessorManager.getInterface(
                null,
                NotificationHandler
            ).duplicateReconciliationReceiptRequester
        return Observable.fromCallable {
            (duplicateReconciliationReceiptRequester.execute(
                duplicateReconciliationReceiptRequestEntity.mapToModel()
            )).mapToDuplicateReconciliationReceiptUIModel()
        }
    }
}