package global.citytech.finpos.merchant.presentation.transactions

import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import global.citytech.common.data.Response
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.AutoReversalQueueRepositoryImpl
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.INITIATOR_QR
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.receipt.DuplicateReceiptRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.reconciliation.ReconciliationRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.TransactionLog
import global.citytech.finpos.merchant.domain.model.receipt.duplicate.DuplicateReceiptResponseEntity
import global.citytech.finpos.merchant.domain.usecase.app.CoreAutoReversalQueueUseCase
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.core.receipt.CoreDuplicateReceiptUseCase
import global.citytech.finpos.merchant.domain.usecase.core.reconciliation.CoreReconciliationUseCase
import global.citytech.finpos.merchant.framework.datasource.app.AutoReversalQueueDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.receipt.DuplicateReceiptDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.reconciliation.ReconciliationDataSourceImpl
import global.citytech.finpos.merchant.presentation.dynamicqr.QrType
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.TransactionItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.presentation.model.qr.list.QrPaymentListRequest
import global.citytech.finpos.merchant.presentation.model.common.PaymentListResponse
import global.citytech.finpos.merchant.presentation.model.common.TerminalInfos
import global.citytech.finpos.merchant.presentation.model.mobile.list.MobileNfcPaymentListRequest
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finpos.merchant.presentation.model.qr.list.QrTerminalInfos
import global.citytech.finpos.merchant.presentation.model.qroperator.QrOperatorItem
import global.citytech.finpos.merchant.presentation.model.setting.QrConfiguration
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog
import io.reactivex.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Saurav Ghimire on 5/7/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */

class TransactionsViewModel(val context: Application) : BaseAndroidViewModel(context) {

    val TAG = TransactionsViewModel::class.java.name
    val transactions by lazy { MutableLiveData<List<TransactionItem>>() }
    val transaction by lazy { MutableLiveData<TransactionLog>() }
    val voidAllowedInTerminal = MutableLiveData<Boolean>(false)
    val tipAdjustmentAllowedInTerminal = MutableLiveData<Boolean>(false)
    val autoReversalPresent by lazy { MutableLiveData<Boolean>() }
    val settlementActive by lazy { MutableLiveData<Boolean>() }
    val isLoadingMessage by lazy { MutableLiveData<String>() }
    var configurationItem: ConfigurationItem? = null
    var isPaginating = MutableLiveData<Boolean>(false)
    val transactionsCount by lazy { MutableLiveData<Int>() }
    val searchedTransactionsCount by lazy { MutableLiveData<Int>() }
    val canDeleteAuthorization by lazy { MutableLiveData<Boolean>() }
    val authorizationDeleted by lazy { MutableLiveData<Boolean>() }
    val transactionAvailable by lazy { MutableLiveData<Boolean>() }

    val appConfigurationForMobilePay by lazy { MutableLiveData<ConfigurationItem>() }
    val paymentsEitherQrOrMobilePay by lazy { MutableLiveData<MutableList<Any>>() }
    val transactionAvailableEitherQrOrMobilePay by lazy { MutableLiveData<Boolean>() }
    val noInternet by lazy { MutableLiveData<Boolean>() }
    var isPaginatingEitherQrOrMobilePay = MutableLiveData<Boolean>(false)
    val paymentCountEitherQrOrMobilePay by lazy { MutableLiveData<Int>() }
    val searchedPaymentCountEitherQrOrMobilePay by lazy { MutableLiveData<Int>() }
    val showMobilePayOption by lazy { MutableLiveData<Boolean>(false) }
    val showQrOption by lazy { MutableLiveData<Boolean>(false) }

    private var localDataUseCase: LocalDataUseCase =
        LocalDataUseCase(
            LocalRepositoryImpl(
                LocalDatabaseSourceImpl(),
                PreferenceManager
            )
        )

    var duplicateReceiptUseCase = CoreDuplicateReceiptUseCase(
        DuplicateReceiptRepositoryImpl(
            DuplicateReceiptDataSourceImpl()
        )
    )

    private val logger = Logger(TransactionsViewModel::class.java.name)

    private var applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)

    private val coreReconciliationUseCase = CoreReconciliationUseCase(
        ReconciliationRepositoryImpl(
            ReconciliationDataSourceImpl()
        )
    )

    private val coreAutoReversalQueueUseCase = CoreAutoReversalQueueUseCase(
        AutoReversalQueueRepositoryImpl(
            AutoReversalQueueDataSourceImpl(context)
        )
    )

    /*private val qrPaymentUseCase = QrPaymentUseCase(
        QrPaymentRepositoryImpl(
            QrPaymentDataSourceImpl(context)
        )
    )*/

    fun bindService() {
        compositeDisposable.add(
            Single.create(SingleOnSubscribe<Response> { _ ->
                val bindIntent = Intent()
                val packageName = "global.citytech.tms"
                val className = "global.citytech.tms.service.PlatformManagerService"
                bindIntent.setClassName(packageName, className)
                context.bindService(
                    bindIntent,
                    NiblMerchant.INSTANCE.getPlatformServiceConnection(),
                    Context.BIND_AUTO_CREATE
                )
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                }, { error ->
                    printLog(TAG, error.message.toString())
                    error.printStackTrace()
                })
        )
    }

    fun getConfigurationForMobilePay() {
        compositeDisposable.add(
            localDataUseCase.getWholeConfiguration()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe({
                    appConfigurationForMobilePay.value = it
                }, {
                    printLog(TAG, "Configuration for mobile pay " + it.message.toString())
                    isLoading.value = false
                    message.value = it.message
                })
        )
    }

    private fun onRetrieveAdditionalPaymentsSubscribe(
        emitter: ObservableEmitter<PaymentListResponse>,
        pageSize: Int,
        offset: Int,
        initiator: String
    ) {
        val listOfTerminalInfos: Any
        val request: Any

        if (initiator == INITIATOR_QR) {
            listOfTerminalInfos = mutableListOf<QrTerminalInfos>()
            localDataUseCase.getTerminalSetting().qrConfigs?.forEach {
                generateQrTransactionRequest(QrType.FONEPAY, it)?.let { qrTerminalInfos ->
                    listOfTerminalInfos.add(
                        qrTerminalInfos
                    )
                }
                generateQrTransactionRequest(QrType.NQR, it)?.let { qrTerminalInfos ->
                    listOfTerminalInfos.add(
                        qrTerminalInfos
                    )
                }
            }
            request = QrPaymentListRequest(listOfTerminalInfos)
        } else {
            val terminalRepositoryImpl =
                TerminalRepositoryImpl(appConfigurationForMobilePay.value).findTerminalInfo()
            listOfTerminalInfos =
                TerminalInfos(terminalRepositoryImpl.merchantID, terminalRepositoryImpl.terminalID)
            request = MobileNfcPaymentListRequest(listOfTerminalInfos, "NFC")
        }
        fetchQrEitherMobilePayTransactionList(request, emitter, initiator, pageSize, offset)
    }

    private fun generateQrTransactionRequest(
        qrType: QrType,
        qrConfiguration: QrConfiguration
    ): QrTerminalInfos? {
        qrType.codes.forEach {
            if (qrConfiguration.qrOperatorInfo.name.equals(it, true)) {
                return QrTerminalInfos(
                    qrConfiguration.merchantId,
                    qrConfiguration.terminalId,
                    qrType.codes[0].lowercase()
                )
            }
        }
        return null
    }

    private fun fetchQrEitherMobilePayTransactionList(
        request: Any,
        emitter: ObservableEmitter<PaymentListResponse>,
        initiator: String,
        pageSize: Int,
        offset: Int
    ) {
        val jsonResponse: String
        val jsonRequest = Jsons.toJsonObj(request)
        jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.post(
            "/device-payment-processor-endpoints/reports/transaction/detail?limit=$pageSize&offset=$offset",
            jsonRequest,
            false
        )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            val dataJson = Jsons.toJsonObj(response.data)
            val paymentListResponse =
                Jsons.fromJsonToObj(dataJson, PaymentListResponse::class.java)
            emitter.onNext(paymentListResponse)
        } else {
            emitter.onError(Exception(response.message))
        }
        emitter.onComplete()
    }

    fun countTransactions() {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Int> { emitter ->
                emitter.onNext(localDataUseCase.countApprovedTransactions())

            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    transactionsCount.value = it
                }, {
                    printLog(TAG, "Transaction count ::: " + it.message.toString())
                    transactionsCount.value = 0
                })
        )
    }

    private fun countSearchTransactions(searchParam: String) {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Int> { emitter ->
                emitter.onNext(localDataUseCase.countSearchApprovedTransactions(searchParam))

            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    searchedTransactionsCount.value = it
                }, {
                    printLog(TAG, "Transaction search count ::: " + it.message.toString())
                    searchedTransactionsCount.value = 0
                })
        )
    }

    fun searchTransactions(
        searchParam: String,
        pageNumber: Int,
        pageSize: Int,
        isSilent: Boolean = false
    ) {
        if (pageNumber > 1 && !isSilent) {
            isPaginating.value = true
        } else {
            countSearchTransactions(searchParam)
        }
        val offset = pageSize * (pageNumber - 1)
        compositeDisposable.add(
            localDataUseCase.searchApprovedTransactions(searchParam, pageSize, offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe({
                    isPaginating.value = false
                    if (!it.isNullOrEmpty()) {
                        if (pageNumber > 1) {
                            val list = transactions.value!!.toMutableList()
                            list.addAll(it)
                            list.reversed()
                            transactions.value = list
                        } else {
                            val list = it.toMutableList()
                            list.reversed()
                            transactions.value = list
                        }
                    }
                }, {
                    printLog(TAG, "Search Transactions ::: " + it.message.toString())
                    isPaginating.value = false
                })
        )
    }

    fun getTransactions(pageNumber: Int, pageSize: Int, isSilent: Boolean = false) {
        if (pageNumber > 1 && !isSilent) {
            isPaginating.value = true
        } else {
            if (!isSilent)
                isLoading.value = true
            countTransactions()
        }
        val offset = pageSize * (pageNumber - 1)
        compositeDisposable.add(
            localDataUseCase.getApprovedTransactions(pageSize, offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe({
                    isLoading.value = false
                    isPaginating.value = false
                    if (it.isNullOrEmpty()) {
                        transactionAvailable.value = false
                    } else {
                        transactionAvailable.value = true
                        isLoading.value = false
                        if (pageNumber > 1) {
                            val list = transactions.value!!.toMutableList()
                            list.addAll(it)
                            list.reversed()
                            transactions.value = list
                        } else {
                            val list = it.toMutableList()
                            list.reversed()
                            transactions.value = list
                        }
                    }
                }, {
                    printLog(TAG, "Get Transactions ::: " + it.message.toString())
                    isLoading.value = false
                    it.printStackTrace()
                    isPaginating.value = false
                    transactionAvailable.value = false
                })
        )
    }

    fun getTransaction(stan: String) {
        transactionAvailable.value = true
        compositeDisposable.add(
            localDataUseCase.getTransactionByStan(stan)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it != null) {
                        transaction.value = it
                    } else {
                        transactionAvailable.value = false
                    }
                }, {
                    printLog(TAG, "Get Transaction by stan ::: " + it.message.toString())
                    it.printStackTrace()
                    transactionAvailable.value = false
                })
        )
    }

    fun duplicateReceipt(stan: String) {
        isLoadingMessage.value = context.getString(R.string.title_duplicate_receipt_printing)
        compositeDisposable.add(
            duplicateReceiptUseCase.print(stan)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onDuplicateReceiptResponseEntityReceived(it)
                }, {
                    printLog(TAG, "Duplicate receipt print ::: " + it.message.toString())
                    isLoading.value = false
                    it.printStackTrace()
                    message.value = it.message
                })
        )
    }

    private fun onDuplicateReceiptResponseEntityReceived(it: DuplicateReceiptResponseEntity) {
        isLoading.value = false
        if (it.result == Result.SUCCESS || it.result == Result.CANCEL) {
            return
        }
        message.value = it.message
    }

    fun checkAdditionalTransactionsAllowed() {
        compositeDisposable.addAll(
            Observable.create(ObservableOnSubscribe<Boolean> {
                onSubscribeVoidAllowed(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    voidAllowedInTerminal.value = it
                }, {
                    printLog(TAG, it.message.toString())
                    voidAllowedInTerminal.value = false
                }),
            Observable.create(ObservableOnSubscribe<Boolean> {
                onSubscribeTipAllowed(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    tipAdjustmentAllowedInTerminal.value = it
                }, {
                    printLog(TAG, it.message.toString())
                    tipAdjustmentAllowedInTerminal.value = false
                })
        )
    }

    private fun onSubscribeTipAllowed(it: ObservableEmitter<Boolean>) {
        it.onNext(applicationRepository.isTransactionTypeEnabled(TransactionType.TIP_ADJUSTMENT))
    }

    private fun onSubscribeVoidAllowed(it: ObservableEmitter<Boolean>) {
        it.onNext(
            applicationRepository.isTransactionTypeEnabled(TransactionType.VOID)
                    || applicationRepository.isTransactionTypeEnabled(TransactionType.CASH_VOID)
        )
    }

    fun getConfiguration(): Boolean {
        isLoadingMessage.value = context.getString(R.string.please_wait)
        return compositeDisposable.add(
            localDataUseCase.getWholeConfiguration()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe({
                    configurationItem = it
                    checkAutoReversal()
                }, {
                    printLog(TAG, "Get configurations error :::" + it.message.toString())
                    isLoading.value = false
                    message.value = it.message
                })
        )
    }

    private fun checkAutoReversal() {
        isLoadingMessage.value = context.getString(R.string.please_wait)
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Boolean> {
                onCheckAutoReversalSubscribe(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    autoReversalPresent.value = it
                    isLoading.value = false
                }, {
                    printLog(TAG, "CHECK AUTO REVERSAL ::: " + it.message.toString())
                    autoReversalPresent.value = false
                    isLoading.value = false
                })
        )
    }

    private fun onCheckAutoReversalSubscribe(it: ObservableEmitter<Boolean>) {
        if (coreAutoReversalQueueUseCase.isQueueEmpty()) {
            printLog(TAG, "::: AUTO REVERSAL ::: QUEUE IS EMPTY")
            it.onNext(false)
        } else {
            printLog(TAG, "::: AUTO REVERSAL ::: QUEUE IS NOT EMPTY")
            it.onNext(coreAutoReversalQueueUseCase.isActive())
        }
    }

    fun checkSettlementTime() {
        isLoadingMessage.value = context.getString(R.string.please_wait)
        compositeDisposable.add(
            coreReconciliationUseCase.checkReconciliationRequired(configurationItem!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    settlementActive.value = it!!
                }, {
                    printLog(TAG, "CHECK SETTLEMENT TIME ::: " + it.message.toString())
                    isLoading.value = false
                    settlementActive.value = false
                })
        )
    }

    fun deletePreAuthorization(transactionLog: TransactionLog) {
        isLoadingMessage.value = context.getString(R.string.please_wait)
        compositeDisposable.add(
            localDataUseCase.deleteTransactionByStan(transactionLog.stan)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    authorizationDeleted.value = it
                }, {
                    printLog(TAG, "DELETE PRE AUTHORIZATION ::: " + it.message.toString())
                    it.printStackTrace()
                    isLoading.value = false
                    authorizationDeleted.value = false
                })
        )
    }

    fun isPreAuthorizationExpired(
        receiptLog: ReceiptLog?
    ) {
        isLoadingMessage.value = context.getString(R.string.please_wait)
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Boolean> {
                if (receiptLog != null) {
                    val transactionTimeStamp = receiptLog.performance.startDateTime
                    printLog(TAG, "transaction time stamp >> $transactionTimeStamp")
                    val simpleDateFormat = SimpleDateFormat("yyMMddHHmmssSSS")
                    val transactionDate = simpleDateFormat.parse(transactionTimeStamp)
                    val currentDate = Date()
                    val difference = currentDate.time - transactionDate!!.time
                    printLog(TAG, "difference >> $difference")
                    val differenceInDays = difference / 1000 / 60 / 60 / 24
                    printLog(TAG, "difference in days >> $differenceInDays")
                    it.onNext(differenceInDays >= 31)
                } else {
                    it.onNext(false)
                }
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    canDeleteAuthorization.value = it
                }, {
                    printLog(TAG, "PRE AUTHORIZATION EXPIRED ::: " + it.message.toString())
                    it.printStackTrace()
                    isLoading.value = false
                    canDeleteAuthorization.value = false
                })
        )
    }

    fun getTransactionsEitherQrOrMobilePay(
        pageNumber: Int,
        pageSize: Int,
        isSilent: Boolean = false,
        initiator: String
    ) {
        printLog(
            TAG,
            "getTransactionsEitherQrOrMobilePay >>> pageNumber == $pageNumber >>> pageSize == $pageSize",
            true
        )
        if (pageNumber > 1 && !isSilent) {
            isPaginatingEitherQrOrMobilePay.value = true
        } else {
            if (!isSilent)
                isLoading.value = true
        }
        val offset = pageSize * (pageNumber - 1)
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<PaymentListResponse> {
                onRetrieveAdditionalPaymentsSubscribe(it, pageSize, offset, initiator)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    isPaginatingEitherQrOrMobilePay.value = false
                    paymentCountEitherQrOrMobilePay.value = it.totalRecord.toInt()
                    if (it.data.isEmpty()) {
                        noInternet.value = false;
                        transactionAvailableEitherQrOrMobilePay.value = false
                    } else {
                        isLoading.value = false
                        transactionAvailableEitherQrOrMobilePay.value = true
                        if (pageNumber > 1) {
                            val list = paymentsEitherQrOrMobilePay.value!!.toMutableList()
                            list.addAll(it.data)
                            //list.addAll(it.data.reversed())
                            //list.reversed()
                            paymentsEitherQrOrMobilePay.value = list
                        } else {
                            val list = it.data.toMutableList()
                            //list.reversed()
                            paymentsEitherQrOrMobilePay.value = list
                        }
                    }
                }, {
                    printLog(TAG, "GET TRANSACTION QR/MobilePay ::: " + it.message.toString())
                    isLoading.value = false
                    it.printStackTrace()
                    noInternet.value =
                        it.message.equals(context.getString(R.string.no_internet_transactions))
                    isPaginatingEitherQrOrMobilePay.value = false
                    transactionAvailableEitherQrOrMobilePay.value = false
                    paymentCountEitherQrOrMobilePay.value = 0
                })
        )
    }

    fun searchTransactionsEitherQrOrMobilePay(
        searchParam: String,
        pageNumber: Int,
        pageSize: Int,
        isSilent: Boolean = false,
        initiator: String
    ) {
        printLog(
            TAG,
            "searchTransactionsEitherQrOrMobilePay >>> pageNumber == $pageNumber >>> pageSize == $pageSize",
            true
        )
        if (pageNumber > 1 && !isSilent) {
            isPaginatingEitherQrOrMobilePay.value = true
        }
        val offset = pageSize * (pageNumber - 1)
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<PaymentListResponse> {
                onRetrieveAdditionalPaymentsSubscribe(it, pageSize, offset, initiator)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isPaginatingEitherQrOrMobilePay.value = false
                    if (!it.data.isEmpty()) {
                        if (pageNumber > 1) {
                            val list = paymentsEitherQrOrMobilePay.value!!.toMutableList()
                            list.addAll(it.data)
                            //list.reversed()
                            paymentsEitherQrOrMobilePay.value = list
                        } else {
                            val list = it.data.toMutableList()
                            //list.reversed()
                            paymentsEitherQrOrMobilePay.value = list
                        }
                    }
                    searchedPaymentCountEitherQrOrMobilePay.value = it.totalRecord.toInt()
                }, {
                    printLog(TAG, "SEARCH TRANSACTION QR/MobilePay ::: " + it.message.toString())
                    it.printStackTrace()
                    isPaginatingEitherQrOrMobilePay.value = false
                    searchedPaymentCountEitherQrOrMobilePay.value = 0
                })
        )
    }

    fun initMobilePayOptionConfig() {
        showMobilePayOption.value = this.localDataUseCase.getTerminalSetting().enableNfcTapAndPay
    }

    fun initQrOptionConfig() {
        val settings = this.localDataUseCase.getTerminalSetting()
        val qrOperatorList = ArrayList<QrOperatorItem>()
        settings.qrConfigs
            ?.forEach { it ->
                run {
                    if (it.enableDynamicQr && it.qrOperatorInfo.active) {
                        qrOperatorList.add(it.qrOperatorInfo)
                    }
                }
            }
        showQrOption.value = qrOperatorList.size > 0
    }
}