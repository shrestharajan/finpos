package global.citytech.finpos.merchant.utils;

import android.media.MediaPlayer;

import java.util.ArrayList;
import java.util.List;

public class MediaPlayerRegistry {
    public static List<MediaPlayer> mList = new ArrayList<MediaPlayer>();

    public static void stopAll() {
        for (MediaPlayer player : MediaPlayerRegistry.mList) {
            player.release();
            if (player != null) {
                player.stop();
                //player.release();
            }
        }
        mList.clear();
    }

    public static void register(MediaPlayer mediaPlayer){
        mList.add(mediaPlayer);
    }
}