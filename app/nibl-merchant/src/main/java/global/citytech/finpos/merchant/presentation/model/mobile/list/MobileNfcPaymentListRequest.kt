package global.citytech.finpos.merchant.presentation.model.mobile.list

import global.citytech.finpos.merchant.presentation.model.common.TerminalInfos

data class MobileNfcPaymentListRequest(
    val terminalInfos: TerminalInfos,
    val paymentType: String
)
