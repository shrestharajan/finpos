package global.citytech.finpos.merchant.data.datasource.app.db

import global.citytech.finpos.merchant.domain.model.app.*
import global.citytech.finpos.merchant.domain.model.device.DeviceResponse
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.presentation.model.ConfigResponse
import global.citytech.finposframework.switches.posmode.PosMode
import global.citytech.finposframework.usecases.TransactionType
import io.reactivex.Observable


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
interface LocalDatabaseSource {
    fun getHost(): Observable<List<Host>>
    fun getMerchant(): Observable<List<Merchant>>
    fun getLogo(): Observable<List<Logo>>
    fun getAidParams(): Observable<List<AidParam>>
    fun getCardScheme(): Observable<List<CardScheme>>
    fun getEmvKeys(): Observable<List<EmvKey>>
    fun getEmvParams(): Observable<List<EmvParam>>
    fun getMerchantTransactionLog(): Observable<List<MerchantTransactionLog>>
    fun getActivityLog(): Observable<List<ActivityLog>>
    fun getTransactionLogStan(stan: String): Observable<TransactionLog>
    fun getTransactionLogByRRN(rrn: String): Observable<TransactionLog>
    fun getTransactionLogByInvoice(
        invoiceNumber: String,
        ignoreBatchNumber: Boolean
    ): Observable<TransactionLog>

    fun getTransactionLogs(): Observable<List<TransactionLog>>
    fun getTransactionLogs(limit: Int, offset: Int): Observable<List<TransactionLog>>
    fun searchTransactionLogs(
        searchParam: String,
        limit: Int,
        offset: Int
    ): Observable<List<TransactionLog>>

    fun countSearchApprovedTransactions(searchParam: String): Int
    fun countApprovedTransactions(): Int
    fun saveMerchantTransactionLog(merchantTransactionLog: MerchantTransactionLog): Observable<DatabaseReponse>
    fun nukeMerchantTransactionLog(): Observable<DatabaseReponse>
    fun nukeActivityLog(): Observable<DatabaseReponse>
    fun deleteMerchantTransactionLogByStans(stans: List<String>): Observable<DatabaseReponse>
    fun saveConfigResponseAndInjectKey(configResponse: ConfigResponse): Observable<DeviceResponse>
    fun isTransactionPresent(invoiceNumber: String): Observable<Boolean>
    fun isTransactionPresentByAuthCode(authCode: String): Observable<Boolean>
    fun isTransactionPresentByRrn(rrn: String): Observable<Boolean>
    fun getAdminPassword(): Observable<String>
    fun getEnabledTransactionsForThePosMode(posMode: PosMode): Observable<List<TransactionType>>
    fun updateReconciliationBatchNumber(batchNumber: Int)
    fun getExistingSettlementBatchNumber(): Observable<String>
    fun getTipAdjustedTransactionLogOfInvoiceNumber(invoiceNumber: String): Observable<TransactionLog>
    fun getDeviceConfiguration(): DeviceConfiguration
    fun deleteTransactionByStan(stan: String): Observable<Boolean>
    fun getInvoiceNumberByRrn(rrn: String): Observable<String>
    fun insertToActivityLog(activityLog: ActivityLog): Observable<Boolean>
    fun addToDisclaimers(disclaimer: Disclaimer): Observable<Boolean>
    fun addListToDisclaimers(disclaimers: List<Disclaimer>): Observable<Boolean>
    fun retrieveAllDisclaimers(): Observable<List<Disclaimer>>
    fun retrieveDisclaimerById(disclaimerId: String): Observable<Disclaimer>
    fun updateRemarksById(disclaimerId: String, remarks: String): Observable<Boolean>
    fun deleteById(disclaimerId: String): Observable<Boolean>
    fun deleteMerchantTransactionLogsOfGivenCountFromInitial(count: Int): Observable<DatabaseReponse>
    fun nukeAllDatabaseTable():Observable<DatabaseReponse>
    fun getAmountPerTransaction(): Observable<Long>
    fun getMerchantTransactionLogsOfGivenCount(count: Int): Observable<List<MerchantTransactionLog>>
}