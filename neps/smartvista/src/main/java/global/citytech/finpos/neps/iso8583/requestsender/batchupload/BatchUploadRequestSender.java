package global.citytech.finpos.neps.iso8583.requestsender.batchupload;

import global.citytech.finpos.neps.iso8583.FunctionCodeConfig;
import global.citytech.finpos.neps.iso8583.NepsMessagePayloadBuilder;
import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.PosEntryModeConfig;
import global.citytech.finposframework.hardware.io.cards.PinBlock;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.switches.FunctionCode;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Saurav Ghimire on 6/14/21.
 * sauravnghimire@gmail.com
 */


public class BatchUploadRequestSender extends NepsMessageSenderTemplate {
    private final BatchUploadISORequest request;
    private final Logger logger = Logger.getLogger(BatchUploadRequestSender.class.getName());

    public BatchUploadRequestSender( RequestContext context) {
        super(context);
        request = (BatchUploadISORequest) context.getRequest();
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti = new MTI(MTI.Version.Version_1987, MTI.MessageClass.File_Actions,
                MTI.MessageFunction.Advice, MTI.Originator.Acquirer);
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);
        msg.addField(new DataElement(DataElement.PRIMARY_ACCOUNT_NUMBER, this.request.getPan()));
        msg.addField(new DataElement(DataElement.AMOUNT, HelperUtils.toISOAmount(this.request.getTransactionAmount(),12)));
        msg.addField(new DataElement(DataElement.LOCAL_TIME, this.request.getLocalTxnDateAndTime()));
        msg.addField(new DataElement(DataElement.PAN_EXPIRE_DATE, this.request.getExpirationDate()));
        msg.addField(new DataElement(DataElement.POS_ENTRY_MODE, PosEntryModeConfig.getValue(this.request.getPosEntryMode())));
        msg.addField(new DataElement(DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE, this.request.getPosConditionCode()));
        msg.addField(new DataElement(DataElement.RETRIEVAL_REFERENCE_NUMBER, this.request.getOriginalRrn()));
        msg.addField(new DataElement(DataElement.TRACK_II_DATA,this.request.getTrack2Data()));
        msg.addField(new DataElement(DataElement.ICC_SYSTEM_RELATED_DATA,this.request.getEmvData()));
        addValidCurrencyOnlyToTheRequest(msg);
        addValidPinBlockOnlyToTheRequest(msg);
        MessagePayload.Builder builder = new NepsMessagePayloadBuilder(this.specInfo, msg);
        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());
        return builder.build();
    }

    @Override
    protected String getProcessingCode() {
        return this.request.getOriginalProcessingCode();
    }

    @Override
    protected String getFunctionCode() {
        return FunctionCodeConfig.getValue(FunctionCode.RECONCILICATION);
    }

    private void addValidCurrencyOnlyToTheRequest(Iso8583Msg iso8583Msg) {
        if (!StringUtils.isEmpty(request.getCurrencyCode())) {
            DataElement currencyCodeDataElement = new DataElement(
                    DataElement.CURRENCY_CODE,
                    this.request.getCurrencyCode()
            );
            iso8583Msg.addField(currencyCodeDataElement);
        }
    }

    private void addValidPinBlockOnlyToTheRequest(Iso8583Msg iso8583Msg) {
        PinBlock pinBlock = request.getPinBlock();
        if (pinBlock == null) {
            return;
        }
        if (pinBlock.getOfflinePin() || StringUtils.isEmpty(pinBlock.getPin())) {
            return;
        }
        DataElement pinBlockDataElement = new DataElement(
                DataElement.PIN_BLOCK,
                this.request.getPinBlock().getPin()
        );
        iso8583Msg.addField(pinBlockDataElement);
    }
}
