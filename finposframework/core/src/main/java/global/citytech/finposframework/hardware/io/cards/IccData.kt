package global.citytech.finposframework.hardware.io.cards

import java.util.*

/**
 * Created by Rishav Chudal on 6/8/20.
 */
enum class IccData(
    val tag: Int,
    val description: String
) {
    AIP(0x82, "Application Interchange Profile"),
    AMOUNT_AUTHORISED(0x9F02, "Authorised Amount"),
    AMOUNT_OTHER(0x9F03, "Additional Amount"),
    ADF(0x4F, "Application Identifier"),
    APPLICATION_LABEL(0x50, "Application Label"),
    APPLICATION_PREFERRED_NAME(0x9F12, "Application Preferred Name"),
    ATC(0x9F36, "Application Transaction Counter"),
    APPLICATION_CRYPTOGRAM(0x9F26, "Application Cryptogram"),
    CID(0x9F27, "Cryptogram Information Data"),
    CVM(0x9F34, "Cardholder Verification Method Results"),
    CFI(0x86, "Command For ICC"),
    RC(0x8A, "Response Code"),
    DF_NAME(0x84, "Dedicated File Name"),
    IAD(0x9F10, "Issuer Application Data"),
    ISSUER_AUTH_DATA(0x91, "Issuer Authentication Data"),
    IST1(0x71, "Issuer Script Template 1"),
    IST2(0x72, "Issuer Script Template 2"),
    ISI(0x9F18, "Issuer Script Identifier"),
    ISR(0x9F5B, "Issuer Script Result"),
    IFD(0x9F1E, "Interface Device Serial Number"),
    PIN_BLOCK(0xDF01, "Encrypted PIN BLOCK"),
    APPLICATION_PAN(0x5A, "Application PAN"),
    TRACK_2(0x57, "Track 2 Data"),
    TERMINAL_CAPABILITIES(0x9F33, "Terminal Capabilities"),
    TERMINAL_TYPE(0x9F35, "Terminal Type"),
    TVR(0x95, "Terminal Verification Results"),
    TSI(0x9B, "Terminal Status Information"),
    TERMINAL_COUNTRY_CODE(0x9F1A, "Terminal Country Code"),
    TRANSACTION_CURRENCY_CODE(0x5F2A, "Transaction Country Code"),
    TRANSACTION_DATE(0x9A, "Transaction Date"),
    TRANSACTION_TYPE(0x9C, "Transaction Type"),
    TRANSACTION_CATEGORY_CODE(0x9F53, "Transaction Category Code"),
    UNPREDICTABLE_NUMBER(0x9F37, "Unpredictable Number"),
    APP_VERSION_NUMBER(0x9F09,"Application Version Number"),
    TRANSACTION_SEQUENCE_COUNTER(0x9F41, "Transaction Sequence Counter"),
    PSN(0x5F34, "PAN Sequence Number"),
    FORM_FACTOR_INDICATOR(0x9F6E, "Form Factor Indicator"),
    PAYMENT_ACCOUNT_REFERENCE(0x9F24, "Payment Account Reference"),
    FPAN_SUFFIX(0x9F25, "FPAN Suffix"),
    KERNEL_ID(0x9F2A, "Kernel ID"),
    TTQ(0x9F66, "Terminal Transaction Qualifiers"),
    CTQ(0x9F6C, "Card Transaction Qualifiers"),
    TRI(0x9F19, "Token Requestor Identifier"),
    CVM_LIST(0x8E, "CVM List"),
    IIN(0x42, "Issuer Identification Number"),
    RISK_MGMT_DATA(0x9F1D, "Risk Management Data");

    fun tagInHex() = Integer.toHexString(tag).toUpperCase(Locale.getDefault())
}