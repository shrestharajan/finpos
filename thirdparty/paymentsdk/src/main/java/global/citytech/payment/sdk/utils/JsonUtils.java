package global.citytech.payment.sdk.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class JsonUtils {

    private static Gson gson = new GsonBuilder().serializeNulls().create();

    private JsonUtils() {
    }

    /**
     * To Json EntityConverter using Goolge's Gson Package<br>
     * this method converts a simple object to a json string<br>
     *
     * @param obj
     * @return a json string
     */
    public static <T> String toJsonObj(T obj) {
        return gson.toJson(obj);
    }

    /**
     * Converts a collection of objects using Google's Gson Package
     *
     * @param objCol
     * @return a json string array
     */
    public static <T> String toJsonList(List<T> objCol) {
        return gson.toJson(objCol);
    }

    /**
     * Returns the specific object given the Json String
     *
     * @param <T>
     * @param jsonString
     * @param obj
     * @return a specific object as defined by the user calling the method
     */
    public static <T> T fromJsonToObj(String jsonString, Class<T> obj) {
        return gson.fromJson(jsonString, obj);
    }

    /**
     * Returns a list of specified object from the given json array
     *
     * @param <T>
     * @param jsonString
     * @param t          the type defined by the user
     * @return a list of specified objects as given in the json array
     */
    public static <T> List<T> fromJsonToList(String jsonString, Type t) {
        return (List<T>) Arrays.asList(gson.fromJson(jsonString, t));
    }


    public static String toJsonTree(Object src) {
        return gson.toJsonTree(src).toString();
    }

    public static boolean isValidJsonString(String jsonString) {
        if (StringUtils.isEmpty(jsonString)) {
            return false;
        } else {
            try {
                gson.fromJson(jsonString, Object.class);
                return true;
            } catch (Exception var2) {
                return false;
            }
        }
    }

}


