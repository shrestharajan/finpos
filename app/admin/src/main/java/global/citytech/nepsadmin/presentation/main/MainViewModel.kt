package global.citytech.nepsadmin.presentation.main

import android.app.Application
import android.content.*
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.lifecycle.MutableLiveData
import global.citytech.common.data.Response
import global.citytech.common.extensions.hasInternetConnection
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.core.api.DeviceServiceType
import global.citytech.finpos.core.api.common.DeviceResponse
import global.citytech.finpos.core.api.common.DeviceResult
import global.citytech.finpos.core.api.keys.KeyAlgorithm
import global.citytech.finpos.core.api.keys.KeyService
import global.citytech.finpos.core.api.keys.KeyType
import global.citytech.finpos.core.api.keys.requests.KeyInjectionRequest
import global.citytech.nepsadmin.DeviceConfigurationStatus
import global.citytech.nepsadmin.NepsAdmin
import global.citytech.nepsadmin.data.AppInfo
import global.citytech.nepsadmin.data.ConfigRequest
import global.citytech.nepsadmin.data.DeviceInfo
import global.citytech.nepsadmin.data.response.*
import global.citytech.nepsadmin.data.response.AidParam.Companion.COLUMN_AID
import global.citytech.nepsadmin.data.response.AidParam.Companion.COLUMN_CONTACTLESS_FLOOR_LIMIT
import global.citytech.nepsadmin.data.response.AidParam.Companion.COLUMN_CONTACTLESS_TRANSACTION_LIMIT
import global.citytech.nepsadmin.data.response.AidParam.Companion.COLUMN_CVM_LIMIT
import global.citytech.nepsadmin.data.response.AidParam.Companion.COLUMN_DEFAULT_ACTION_CODE
import global.citytech.nepsadmin.data.response.AidParam.Companion.COLUMN_DEFAULT_DDOL
import global.citytech.nepsadmin.data.response.AidParam.Companion.COLUMN_DEFAULT_TDOL
import global.citytech.nepsadmin.data.response.AidParam.Companion.COLUMN_DENIAL_ACTION_CODE
import global.citytech.nepsadmin.data.response.AidParam.Companion.COLUMN_LABEL
import global.citytech.nepsadmin.data.response.AidParam.Companion.COLUMN_ONLINE_ACTION_CODE
import global.citytech.nepsadmin.data.response.AidParam.Companion.COLUMN_TERMINAL_AID_VERSION
import global.citytech.nepsadmin.data.response.AidParam.Companion.COLUMN_TERMINAL_FLOOR_LIMIT
import global.citytech.nepsadmin.data.response.EmvKey.Companion.COLUMN_CHECKSUM
import global.citytech.nepsadmin.data.response.EmvKey.Companion.COLUMN_EXPIRY_DATE
import global.citytech.nepsadmin.data.response.EmvKey.Companion.COLUMN_EXPONENT
import global.citytech.nepsadmin.data.response.EmvKey.Companion.COLUMN_HASH_ID
import global.citytech.nepsadmin.data.response.EmvKey.Companion.COLUMN_INDEX
import global.citytech.nepsadmin.data.response.EmvKey.Companion.COLUMN_KEY_SIGNATURE_ID
import global.citytech.nepsadmin.data.response.EmvKey.Companion.COLUMN_LENGTH
import global.citytech.nepsadmin.data.response.EmvKey.Companion.COLUMN_MODULES
import global.citytech.nepsadmin.data.response.EmvKey.Companion.COLUMN_RID
import global.citytech.nepsadmin.data.response.EmvParam.Companion.COLUMN_ADDITIONAL_TERMINAL_CAPABILITIES
import global.citytech.nepsadmin.data.response.EmvParam.Companion.COLUMN_FORCE_ONLINE_FLAG
import global.citytech.nepsadmin.data.response.EmvParam.Companion.COLUMN_MERCHANT_CATEGORY_CODE
import global.citytech.nepsadmin.data.response.EmvParam.Companion.COLUMN_MERCHANT_IDENTIFIER
import global.citytech.nepsadmin.data.response.EmvParam.Companion.COLUMN_MERCHANT_NAME
import global.citytech.nepsadmin.data.response.EmvParam.Companion.COLUMN_TERMINAL_CAPABILITIES
import global.citytech.nepsadmin.data.response.EmvParam.Companion.COLUMN_TERMINAL_COUNTRY_CODE
import global.citytech.nepsadmin.data.response.EmvParam.Companion.COLUMN_TERMINAL_ID
import global.citytech.nepsadmin.data.response.EmvParam.Companion.COLUMN_TERMINAL_TYPE
import global.citytech.nepsadmin.data.response.EmvParam.Companion.COLUMN_TRANSACTION_CURRENCY_CODE
import global.citytech.nepsadmin.data.response.EmvParam.Companion.COLUMN_TRANSACTION_CURRENCY_EXPONENT
import global.citytech.nepsadmin.data.response.EmvParam.Companion.COLUMN_TTQ
import global.citytech.nepsadmin.data.response.Host.Companion.COLUMN_CONNECTION_TIME_OUT
import global.citytech.nepsadmin.data.response.Host.Companion.COLUMN_IP
import global.citytech.nepsadmin.data.response.Host.Companion.COLUMN_NII
import global.citytech.nepsadmin.data.response.Host.Companion.COLUMN_ORDER
import global.citytech.nepsadmin.data.response.Host.Companion.COLUMN_PORT
import global.citytech.nepsadmin.data.response.Host.Companion.COLUMN_RETRY_LIMIT
import global.citytech.nepsadmin.service.AppCoreFactory
import global.citytech.nepsadmin.utils.JsonUtils
import global.citytech.tms.sdk.IPlatformManager
import io.reactivex.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import java.util.*

class MainViewModel(val context: Application) : BaseAndroidViewModel(context) {

    val serviceLoaded by lazy { MutableLiveData<Boolean>() }
    val configResponse by lazy { MutableLiveData<ConfigResponse>() }
    val deviceConfigurationStatus by lazy { MutableLiveData<DeviceConfigurationStatus>() }

    private val CONFIG_URL = "/devices-terminal/v1/configs/switch"
    private val configRequest = ConfigRequest(AppInfo("NepsAdmin", 1), DeviceInfo(Build.SERIAL))

    companion object {
        val AUTHORITY = "global.citytech.finpos.nepsmerchant.provider.AdminProvider"
    }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            NepsAdmin.INSTANCE.iPlatformManager = IPlatformManager.Stub.asInterface(service)
            NepsAdmin.INSTANCE.serviceConnection = this
            serviceLoaded.value = true
            Log.d("cta", "pluginInterfaceConnected : " + true)
        }

        override fun onServiceDisconnected(name: ComponentName) {
            Log.d("cta", "pluginInterfaceConnected : " + false)
        }
    }

    fun bindService() {
        compositeDisposable.add(
            Single.create(SingleOnSubscribe<Response> { _ ->
                val bindIntent = Intent()
                val packageName = "global.citytech.tms"
                val className = "global.citytech.tms.service.PlatformManagerService"
                bindIntent.setClassName(packageName, className)
                context.bindService(bindIntent, serviceConnection, Context.BIND_AUTO_CREATE)
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                }, { error ->
                })
        )
    }

    fun register() {
        deviceConfigurationStatus.value = DeviceConfigurationStatus.DownloadingConfiguration
        if (!context.hasInternetConnection()) {
            message.value = "No Internet Connection"
            deviceConfigurationStatus.value = DeviceConfigurationStatus.ConfigurationFailed
            return
        }
        isLoading.value = true
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<ConfigResponse> {
                onSubscribe(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onNext, this::onError)
        )
    }

    private fun onSubscribe(emitter: ObservableEmitter<ConfigResponse>) {
        val jsonRequest = Jsons.toJsonObj(configRequest)
        val jsonResponse =
            NepsAdmin.INSTANCE.iPlatformManager.post(CONFIG_URL, jsonRequest, true)
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            prepareResponse(emitter, response.data as Any)
        } else {
            emitter.onError(Exception("Error Code :: ${response.code}. ${response.message}"))
        }
        emitter.onComplete()
    }

    public fun prepareResponse(emitter: ObservableEmitter<ConfigResponse>, data: Any) {
        val responseJson = Jsons.toJsonObj(data)
        val response = Jsons.fromJsonToObj(responseJson, ConfigResponse::class.java)
        emitter.onNext(response)
    }

    private fun onNext(configResponse: ConfigResponse) {
        deviceConfigurationStatus.value = DeviceConfigurationStatus.SavingParameters
        saveInMerchantDatabase(configResponse)
    }

    private fun saveInMerchantDatabase(configResponse: ConfigResponse) {
        saveHosts(configResponse.hosts)
        saveMerchant(configResponse.merchant!!, configResponse.terminal!!)
        saveLogo(configResponse.logo!!)
        saveAidParam(configResponse.terminalParameter!!.aidParameters)
        saveEmvParam(configResponse.terminalParameter!!.emvParameters!!)
        saveEmvkeys(configResponse.terminalParameter!!.emvKeys!!)
        saveCardScheme(configResponse.terminalParameter!!.cardSchemes!!)
        deviceConfigurationStatus.value = DeviceConfigurationStatus.ParametersSaved
        injectKeys(configResponse.crypto!!)
    }

    private fun saveCardScheme(cardSchemeParameters: ArrayList<Any>) {
        val cardSchemeMaps = this.getSchemeAsMap(cardSchemeParameters)
        var id = 0
        for (cardSchemeMap in cardSchemeMaps) {
            val contentValues = ContentValues()
            for ((key, value) in cardSchemeMap) {
                val cardSchemeId = cardSchemeMap[key]!!["cardSchemeId"]!!["value"]
                contentValues.put(CardScheme.COLUMN_CARD_SCHEME_ID, cardSchemeId!!)
                for ((innerKey, innerValue) in value) {
                    contentValues.put(CardScheme.COLUMN_ID, "${cardSchemeId}_${innerKey}")
                    contentValues.put(CardScheme.COLUMN_CARD_SCHEME_ID, cardSchemeId)
                    contentValues.put(CardScheme.COLUMN_ATTRIBUTE, innerKey)
                    contentValues.put(CardScheme.COLUMN_DISPLAY_LABEL, innerValue["displayLabel"])
                    contentValues.put(CardScheme.COLUMN_PRINT_LABEL, innerValue["printLabel"])
                    contentValues.put(CardScheme.COLUMN_VALUE, innerValue["value"])
                    context.contentResolver.insert(CardScheme.URI, contentValues)
                    id++
                }
            }
        }

    }

    private fun getSchemeAsMap(cardSchemeParameters: ArrayList<Any>): List<Map<String, Map<String, HashMap<String, String>>>> {
        val cardSchemeMapList =
            arrayListOf<HashMap<String, HashMap<String, HashMap<String, String>>>>()
        for (cardSchemeParameter in cardSchemeParameters) {
            val cardSchemeParametersInString = Jsons.toJsonObj(cardSchemeParameter)
            val jsonObject = JSONObject(cardSchemeParametersInString)
            val cardSchemeSegmentsMap =
                JsonUtils.toMap(jsonObject) as HashMap<String, HashMap<String, HashMap<String, String>>>
            cardSchemeMapList.add(cardSchemeSegmentsMap)
        }
        return cardSchemeMapList
    }

    private fun saveEmvkeys(emvKeys: ArrayList<EmvKey>) {
        for ((index, emvKey) in emvKeys.withIndex()) {
            val contentValues = ContentValues()
            contentValues.put(EmvKey.COLUMN_ID, index.toString())
            contentValues.put(COLUMN_RID, Jsons.toJsonObj(emvKey.rid))
            contentValues.put(COLUMN_INDEX, Jsons.toJsonObj(emvKey.index))
            contentValues.put(COLUMN_LENGTH, Jsons.toJsonObj(emvKey.length))
            contentValues.put(COLUMN_EXPONENT, Jsons.toJsonObj(emvKey.exponent))
            contentValues.put(COLUMN_MODULES, Jsons.toJsonObj(emvKey.modules))
            contentValues.put(COLUMN_HASH_ID, Jsons.toJsonObj(emvKey.hashId))
            contentValues.put(COLUMN_KEY_SIGNATURE_ID, Jsons.toJsonObj(emvKey.keySignatureId))
            contentValues.put(COLUMN_CHECKSUM, Jsons.toJsonObj(emvKey.checkSum))
            contentValues.put(COLUMN_EXPIRY_DATE, Jsons.toJsonObj(emvKey.expiryDate))
            context.contentResolver.insert(EmvKey.URI, contentValues)
        }
    }

    private fun saveEmvParam(emvParameter: EmvParam) {
        val contentValues = ContentValues()
        contentValues.put(Merchant.COLUMN_ID, "0")
        contentValues.put(COLUMN_TTQ, Jsons.toJsonObj(emvParameter.ttq))
        contentValues.put(
            COLUMN_TRANSACTION_CURRENCY_EXPONENT,
            Jsons.toJsonObj(emvParameter.transactionCurrencyExponent)
        )
        contentValues.put(
            COLUMN_TRANSACTION_CURRENCY_CODE,
            Jsons.toJsonObj(emvParameter.transactionCurrencyCode)
        )
        contentValues.put(COLUMN_TERMINAL_TYPE, Jsons.toJsonObj(emvParameter.terminalType))
        contentValues.put(COLUMN_TERMINAL_ID, Jsons.toJsonObj(emvParameter.terminalId))
        contentValues.put(
            COLUMN_TERMINAL_COUNTRY_CODE,
            Jsons.toJsonObj(emvParameter.terminalCountryCode)
        )
        contentValues.put(
            COLUMN_TERMINAL_CAPABILITIES,
            Jsons.toJsonObj(emvParameter.terminalCapabilities)
        )
        contentValues.put(COLUMN_MERCHANT_NAME, Jsons.toJsonObj(emvParameter.merchantName))
        contentValues.put(
            COLUMN_MERCHANT_IDENTIFIER,
            Jsons.toJsonObj(emvParameter.merchantIdentifier)
        )
        contentValues.put(
            COLUMN_MERCHANT_CATEGORY_CODE,
            Jsons.toJsonObj(emvParameter.merchantCategoryCode)
        )
        contentValues.put(COLUMN_FORCE_ONLINE_FLAG, Jsons.toJsonObj(emvParameter.forceOnlineFlag))
        contentValues.put(
            COLUMN_ADDITIONAL_TERMINAL_CAPABILITIES,
            Jsons.toJsonObj(emvParameter.additionalTerminalCapabilities)
        )
        context.contentResolver.insert(EmvParam.URI, contentValues)
    }

    private fun injectKeys(crypto: Crypto) {
        deviceConfigurationStatus.value = DeviceConfigurationStatus.InjectingKeys
        val keyService = AppCoreFactory.getCore(
            context,
            DeviceServiceType.KEYS
        ) as KeyService
        val key = crypto.values

        val observable = Observable.fromCallable {
            keyService.injectKey(
                prepareKeyInjectionRequest(
                    key!!.key1.plus(
                        key.key2
                    )
                )
            )
        }
        compositeDisposable.add(
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onInjected, this::onError)
        )
    }

    fun onInjected(coreResponse: DeviceResponse) {
        isLoading.value = false
        if (coreResponse.result == DeviceResult.SUCCESS) {
            deviceConfigurationStatus.value = DeviceConfigurationStatus.KeysInjectedSuccesfully
        } else {
            deviceConfigurationStatus.value = DeviceConfigurationStatus.ConfigurationFailed
        }
    }

    private fun prepareKeyInjectionRequest(key: String): KeyInjectionRequest {
        return KeyInjectionRequest(
            "global.citytech.finpos.nepsmerchant",
            KeyType.KEY_TYPE_TMK,
            KeyAlgorithm.TYPE_3DES,
            key.toUpperCase(),
            null,
            null,
            true
        )
    }

    private fun saveLogo(logo: Logo) {
        val contentValues = ContentValues()
        contentValues.put(Logo.COLUMN_ID, "0")
        contentValues.put(Logo.COLUMN_APP_WALLPAPER, logo.appWallpaper)
        contentValues.put(Logo.COLUMN_DISPLAY_LOGO, logo.displayLogo)
        contentValues.put(Logo.COLUMN_PRINT_LOGO, logo.printLogo)
        context.contentResolver.insert(Logo.URI, contentValues)
    }

    private fun saveMerchant(merchant: Merchant, terminal: Terminal) {
        val contentValues = ContentValues()
        contentValues.put(Merchant.COLUMN_ID, "0")
        contentValues.put(Merchant.COLUMN_ADDRESS, merchant.address)
        contentValues.put(Merchant.COLUMN_NAME, merchant.name)
        contentValues.put(Merchant.COLUMN_SWITCH_ID, merchant.switchId)
        contentValues.put(Merchant.COLUMN_PHONE_NUMBER, merchant.phoneNumber)
        contentValues.put(Merchant.COLUMN_TERMINAL_ID, terminal.switchId)
        context.contentResolver.insert(Merchant.URI, contentValues)
    }

    private fun saveHosts(hosts: List<Host>?) {
        for ((index, host) in hosts!!.withIndex()) {
            val contentValues = ContentValues()
            contentValues.put(Host.COLUMN_ID, index.toString())
            contentValues.put(COLUMN_IP, host.ip)
            contentValues.put(COLUMN_PORT, host.port)
            contentValues.put(COLUMN_NII, host.nii)
            contentValues.put(COLUMN_CONNECTION_TIME_OUT, host.connectionTimeout)
            contentValues.put(COLUMN_ORDER, host.order)
            contentValues.put(COLUMN_RETRY_LIMIT, host.retryLimit)
            context.contentResolver.insert(Host.URI, contentValues)
        }
    }

    private fun saveAidParam(aidParams: List<AidParam>?) {
        for ((index, aidParam) in aidParams!!.withIndex()) {
            val contentValues = ContentValues()
            contentValues.put(AidParam.COLUMN_ID, index.toString())
            contentValues.put(COLUMN_AID, Jsons.toJsonObj(aidParam.aid))
            contentValues.put(COLUMN_LABEL, Jsons.toJsonObj(aidParam.label))
            contentValues.put(
                COLUMN_TERMINAL_AID_VERSION,
                Jsons.toJsonObj(aidParam.terminalAidVersion)
            )
            contentValues.put(COLUMN_DEFAULT_TDOL, Jsons.toJsonObj(aidParam.defaultTDOL))
            contentValues.put(COLUMN_DEFAULT_DDOL, Jsons.toJsonObj(aidParam.defaultDDOL))
            contentValues.put(COLUMN_DENIAL_ACTION_CODE, Jsons.toJsonObj(aidParam.denialActionCode))
            contentValues.put(COLUMN_ONLINE_ACTION_CODE, Jsons.toJsonObj(aidParam.onlineActionCode))
            contentValues.put(
                COLUMN_DEFAULT_ACTION_CODE,
                Jsons.toJsonObj(aidParam.defaultActionCode)
            )
            contentValues.put(
                COLUMN_TERMINAL_FLOOR_LIMIT,
                Jsons.toJsonObj(aidParam.terminalFloorLimit)
            )
            contentValues.put(
                COLUMN_CONTACTLESS_FLOOR_LIMIT,
                Jsons.toJsonObj(aidParam.contactlessFloorLimit)
            )
            contentValues.put(
                COLUMN_CONTACTLESS_TRANSACTION_LIMIT,
                Jsons.toJsonObj(aidParam.contactlessTransactionLimit)
            )
            contentValues.put(COLUMN_CVM_LIMIT, Jsons.toJsonObj(aidParam.cvmLimit))
            context.contentResolver.insert(AidParam.URI, contentValues)
        }
    }

    private fun onError(throwable: Throwable) {
        isLoading.value = false
        deviceConfigurationStatus.value = DeviceConfigurationStatus.ConfigurationCanceled
        message.value = throwable.message
    }


}
