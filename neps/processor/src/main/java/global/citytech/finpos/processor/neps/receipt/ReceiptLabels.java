package global.citytech.finpos.processor.neps.receipt;

public class ReceiptLabels {
    public static final String TERMINAL_ID = "TID ";
    public static final String MERCHANT_ID = "MID ";
    public static final String DATE = "DATE";
    public static final String TIME = "TIME";
    public static final String BATCH_NUMBER = "BATCH NO";
    public static final String HOST = "HOST";
    public static final String HEADER = "SUMMARY";
    public static final String DETAIL_REPORT = "DETAIL REPORT";
    public static final String CARD_NUMBER = "CARD NUMBER";
    public static final String CARD_SCHEME = "CARD SCHEME";
    public static final String INVOICE_NUMBER = "INVOICE NUMBER";
    public static final String APPROVAL_CODE = "APPROVAL CODE";
    public static final String TRANSACTION_TYPE = "TRANSACTION";
    public static final String AMOUNT = "AMOUNT";
    public static final String DIVIDER = "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --";
    public static final String FOOTER = "END OF DETAIL REPORT";
    public static final String LINE_BREAK = "  ";
    public static final String SALE = "SALE";
    public static final String CURRENCY = "NPR";
    public static final String VOID = "VOID";
    public static final String REFUND = "REFUND";
    public static final String COUNT = "COUNT";
    public static final String TOTAL = "TOTAL";
    public static final String TRANSACTION = "TRANSACTION";
    public static final String CUMULATIVE = "CUMULATIVE";

    public static final String SIGNATURE_MESSAGE_CUSTOMER = "CUSTOMER SIGN BELOW";
    public static final String SIGNATURE_LINE = "______________________X";
    public static final String DEBIT_ACCOUNT_ACKNOWLEDGEMENT = "DEBIT MY ACCOUNT FOR THE AMOUNT";
    public static final String APPROVAL_CODE_LABEL = "APPROVAL CODE";
    public static final String THANK_YOU_MESSAGE = "(THANK YOU FOR USING OUR SERVICE)";
    public static final String RETAIN_RECEIPT = "PLEASE RETAIN YOUR RECEIPT";
    public static final String CUSTOMER_COPY = "** CUSTOMER COPY **";
    public static final String PURCHASE_AMOUNT_LABEL = "AMOUNT";
    public static final String CURRENCY_CODE = "NPR ";
    public static final String DATE_LABEL = "DATE";
    public static final String TIME_LABEL = "TIME";
    public static final String TERMINAL_ID_LABEL = "TID";
    public static final String MERCHANT_ID_LABEL = "MID";
    public static final String BATCH_NUMBER_LABEL = "BATCH NO";
    public static final String STAN_LABEL = "TRACE NO";
    public static final String RRN_LABEL = "RRN";
    public static final String INVOICE_NUMBER_LABEL = "Invoice Number: ";
    public static final String TXN_CARD_NUMBER = "Card No:";
    public static final String CARD_TYPE = "Card Type:";
    public static final String AID = "AID:";

    public static final String CARD_TYPE_ICC = "CHIP";
    public static final String CARD_TYPE_MAGNETIC = "SWIPED";
    public static final String CARD_TYPE_MANUAL = "MANUAL";
    public static final String CARD_TYPE_PICC = "TAPPED";

    public static final String AGREEMENT_DISCLAIMER = "I AGREE TO PAY THE ABOVE TOTAL AMOUNT ACCORDING TO CARD ISSUER AGREEMENT";

    public static final String TRANSACTION_DATE = "TRANSACTION DATE";
    public static final String TRANSACTION_TIME = "TRANSACTION TIME";
}