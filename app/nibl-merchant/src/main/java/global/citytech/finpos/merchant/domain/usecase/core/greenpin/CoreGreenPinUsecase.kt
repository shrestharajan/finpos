package global.citytech.finpos.merchant.domain.usecase.core.greenpin

import global.citytech.finpos.merchant.domain.model.greenpin.GreenPinResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.greenpin.GreenPinRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.greenpin.GreenPinRequestItem
import io.reactivex.Observable

class CoreGreenPinUseCase(private val generateOtpRepository: GreenPinRepository) {
    fun generateOtpRequest(
        configurationItem: ConfigurationItem,
        greenPinRequestItem: GreenPinRequestItem
    ): Observable<GreenPinResponseEntity> =
        generateOtpRepository.generateOtpRequest(configurationItem,greenPinRequestItem)

}