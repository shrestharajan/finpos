package global.citytech.finpos.processor.neps.voidsale;

import global.citytech.finpos.processor.neps.transaction.TransactionResponse;

/**
 * Created by Saurav Ghimire on 4/19/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public class VoidSaleResponse extends TransactionResponse {
    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private String message;
        private String stan;
        private boolean approved;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withDebugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public Builder withDebugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }
        public Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder withApproved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public Builder withShouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public VoidSaleResponse build() {
            VoidSaleResponse voidSaleResponseModel = new VoidSaleResponse();
            voidSaleResponseModel.debugRequestMessage = this.debugRequestMessage;
            voidSaleResponseModel.debugResponseMessage = this.debugResponseMessage;
            voidSaleResponseModel.message = this.message;
            voidSaleResponseModel.approved = this.approved;
            voidSaleResponseModel.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            voidSaleResponseModel.stan = this.stan;
            return voidSaleResponseModel;
        }
    }
}
