package global.citytech.neps.presentation.model

import global.citytech.neps.domain.model.Host

data class HostItem(
    val id: String,
    val connectionTimeout: String? = null,
    val ip: String? = null,
    val nii: String? = null,
    val order: Int? = null,
    val port: String? = null,
    val retryLimit: String? = null
)

fun Host.mapToPresentation(): HostItem = HostItem(id,connectionTimeout, ip, nii, order, port, retryLimit)
fun List<Host>.maptoPresentation(): List<HostItem> = map { it.mapToPresentation() }