package global.citytech.finpos.merchant.domain.model.autoreversal

import global.citytech.finposframework.hardware.common.Result

/**
 * Created by Saurav Ghimire on 7/12/21.
 * sauravnghimire@gmail.com
 */


data class AutoReversalResponseEntity(
    var result: Result? = null,
    val message: String? = null
)