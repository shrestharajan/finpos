package global.citytech.finpos.neps.iso8583.requestsender.reversal;

import global.citytech.finposframework.log.Logger;

import global.citytech.finpos.neps.iso8583.FunctionCodeConfig;
import global.citytech.finpos.neps.iso8583.NepsMessagePayloadBuilder;
import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.PosEntryModeConfig;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.switches.FunctionCode;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.utility.HelperUtils;

public class ReversalRequestSender extends NepsMessageSenderTemplate {

    private final Logger logger = Logger.getLogger(ReversalRequestSender.class.getName());
    protected ReversalIsoRequest transactionIsoRequest;

    public ReversalRequestSender(RequestContext context) {
        super(context);
        transactionIsoRequest = (ReversalIsoRequest) context.getRequest();
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Reversal,
                        MTI.MessageFunction.Request,
                        MTI.Originator.Acquirer);
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);
        MessagePayload.Builder builder = new NepsMessagePayloadBuilder(this.specInfo, msg);
        msg.addField(
                new DataElement(
                        DataElement.AMOUNT,
                        HelperUtils.toISOAmount(transactionIsoRequest.getTransactionAmount(), 12))
        );
        msg.addField(
                new DataElement(
                        DataElement.LOCAL_TIME,
                        transactionIsoRequest.getLocalDateTime())
        );
        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(PosEntryMode.MAGNETIC_STRIPE))
        );
        msg.addField(
                new DataElement(
                        DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE,
                        "00")
        );

        msg.addField(
                new DataElement(
                        DataElement.TRACK_II_DATA,
                        transactionIsoRequest.getTrack2Data()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.CURRENCY_CODE,
                        transactionIsoRequest.getCurrencyCode()
                )
        );
        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());

        return builder.build();
    }


    @Override
    protected String getFunctionCode() {
        return FunctionCodeConfig.getValue(FunctionCode.REVERSAL);
    }

    @Override
    protected String getProcessingCode() {
        return transactionIsoRequest.getProcessingCode();
    }

}
