package global.citytech.finpos.processor.nibl.transactiontype;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.transactiontype.TransactionTypeRequester;

/**
 * Created by Unique Shakya on 12/1/2020.
 */
public class TransactionTypeUseCase implements UseCase<TransactionTypeRequestModel, TransactionTypeResponseModel>,
        TransactionTypeRequester<TransactionTypeRequestModel, TransactionTypeResponseModel> {

    @Override
    public TransactionTypeResponseModel execute(TransactionTypeRequestModel request) {
        List<String> supportedTransactionTypes = new ArrayList<>();
        supportedTransactionTypes.add(ProcessingCode.PURCHASE.getCode().substring(0,2));
        supportedTransactionTypes.add(ProcessingCode.PURCHASE_WITH_CASHBACK.getCode().substring(0,2));
        supportedTransactionTypes.add(ProcessingCode.PRE_AUTHORIZATION.getCode().substring(0,2));
        supportedTransactionTypes.add(ProcessingCode.REFUND.getCode().substring(0,2));
        supportedTransactionTypes.add(ProcessingCode.CASH_ADVANCE.getCode().substring(0,2));
        supportedTransactionTypes.add(ProcessingCode.VOID_SALE.getCode().substring(0,2));
        return new TransactionTypeResponseModel(supportedTransactionTypes);
    }
}
