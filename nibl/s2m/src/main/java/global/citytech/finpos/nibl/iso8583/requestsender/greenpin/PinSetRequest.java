package global.citytech.finpos.nibl.iso8583.requestsender.greenpin;

import java.math.BigDecimal;

import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.tip.S2MTipAdjustmentRequest;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.transaction.Request;

public class PinSetRequest extends TransactionIsoRequest {

    private String pan;
    private BigDecimal transactionAmount = BigDecimal.ZERO;
    private String localTime;
    private String localDate;
    private String expireDate;
    private PosEntryMode posEntryMode;
    private String posConditionCode;
    private String track2Data;
    private String additionalData;
    private String currencyCode;
    private String pinBlock;
    private String otp;
    private String emvData;
    private String retrievalReferenceNumber;
    private String cardSequenceNumber;

    public String getPan() {
        return pan;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }


    public String getLocalTime() {
        return localTime;
    }

    public String getLocalDate() {
        return localDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public String getTrack2Data() {
        return track2Data;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getPinBlock() {
        return pinBlock;
    }
    public String getOtp() {
        return otp;
    }

    public String getEmvData() {
        return emvData;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }
    public String getCardSequenceNumber() {
        return cardSequenceNumber;
    }

    public static final class Builder {
        private String pan;
        private BigDecimal transactionAmount = BigDecimal.ZERO;
        private String localTime;
        private String localDate;
        private String expireDate;
        private PosEntryMode posEntryMode;
        private String posConditionCode;
        private String track2Data;
        private String additionalData;
        private String currencyCode;
        private String pinBlock;
        private String otp;
        private String emvData;
        private String retrievalReferenceNumber;
        private String cardSequenceNumber;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }


        public Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public Builder localTime(String localTime) {
            this.localTime = localTime;
            return this;
        }

        public Builder localDate(String localDate) {
            this.localDate = localDate;
            return this;
        }

        public Builder expireDate(String expiryDate) {
            this.expireDate = expiryDate;
            return this;
        }


        public Builder posEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public Builder posConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public Builder additionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder pinBlock(String pinBlock) {
            this.pinBlock = pinBlock;
            return this;
        }

        public Builder otp(String otp) {
            this.otp = otp;
            return this;
        }


        public Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public Builder rrn(String rrn) {
            this.retrievalReferenceNumber = rrn;
            return this;
        }

        public Builder cardSequenceNumber(String cardSequenceNumber) {
            this.cardSequenceNumber = cardSequenceNumber;
            return this;
        }



        public PinSetRequest build() {
            PinSetRequest pinSetRequest = new PinSetRequest();
            pinSetRequest.pan = this.pan;
            pinSetRequest.transactionAmount = this.transactionAmount;
            pinSetRequest.localDate = this.localDate;
            pinSetRequest.localTime = this.localTime;
            pinSetRequest.expireDate = this.expireDate;
            pinSetRequest.posEntryMode = this.posEntryMode;
            pinSetRequest.posConditionCode = this.posConditionCode;
            pinSetRequest.track2Data = this.track2Data;
            pinSetRequest.additionalData = this.additionalData;
            pinSetRequest.currencyCode = this.currencyCode;
            pinSetRequest.pinBlock = this.pinBlock;
            pinSetRequest.otp = this.otp;
            pinSetRequest.emvData = this.emvData;
            pinSetRequest.retrievalReferenceNumber = this.retrievalReferenceNumber;
            pinSetRequest.cardSequenceNumber = this.cardSequenceNumber;

            return pinSetRequest;
        }
    }
}
