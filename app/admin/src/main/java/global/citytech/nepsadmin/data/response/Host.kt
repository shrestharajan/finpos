package global.citytech.nepsadmin.data.response

import android.net.Uri
import global.citytech.nepsadmin.presentation.main.MainViewModel

data class Host(
    var connectionTimeout: String? = null,
    var ip: String? = null,
    var nii: String? = null,
    var order: Int? = null,
    var port: String? = null,
    var retryLimit: String? = null
){
    companion object{

        val TABLE_NAME = "host"
        val URI = Uri.parse("content://${MainViewModel.AUTHORITY}/$TABLE_NAME")
        const val COLUMN_ID = "id"
        const val COLUMN_CONNECTION_TIME_OUT = "connection_time_out"
        const val COLUMN_IP = "ip"
        const val COLUMN_NII = "nii"
        const val COLUMN_ORDER = "order"
        const val COLUMN_PORT = "port"
        const val COLUMN_RETRY_LIMIT = "retry_limit"
    }
}