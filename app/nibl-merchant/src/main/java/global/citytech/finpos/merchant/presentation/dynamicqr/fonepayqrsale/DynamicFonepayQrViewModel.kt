package global.citytech.finpos.merchant.presentation.dynamicqr.fonepayqrsale

import android.app.Application
import android.content.Context
import android.content.Intent
import global.citytech.common.data.Response
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.domain.model.app.QrStatus
import global.citytech.finpos.merchant.domain.usecase.qr.fonepayqr.FonepayQrGenerateRequest
import global.citytech.finpos.merchant.domain.usecase.qr.fonepayqr.FonepayQrGenerateResponse
import global.citytech.finpos.merchant.presentation.dynamicqr.base.DynamicQrBaseViewModel
import global.citytech.finpos.merchant.utils.AppUtility
import io.reactivex.Single
import io.reactivex.SingleEmitter
import io.reactivex.SingleOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DynamicFonepayQrViewModel(val context: Application) : DynamicQrBaseViewModel(context) {
    var fonepayQrGenerateRequest: FonepayQrGenerateRequest? = null

    fun getFonepayQrIdentifier(key: String): String {
        return qrPaymentUseCase.generateFonepayQrIdentifiers(key)
    }

    fun bindService() {
        compositeDisposable.add(
            Single.create(SingleOnSubscribe<Response> {
                val bindIntent = Intent()
                val packageName = "global.citytech.tms"
                val className = "global.citytech.tms.service.PlatformManagerService"
                bindIntent.setClassName(packageName, className)
                context.bindService(
                    bindIntent,
                    NiblMerchant.INSTANCE.getPlatformServiceConnection(),
                    Context.BIND_AUTO_CREATE
                )
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                }, {
                    it.printStackTrace()
                    isLoading.value = false
                    message.value = it.message
                })
        )
    }

    fun getFonepayQr(fonepayQrGenerateRequest: FonepayQrGenerateRequest) {
        compositeDisposable.add(
            Single.create(SingleOnSubscribe<FonepayQrGenerateResponse> {
                qrPaymentUseCase.incrementBillingInvoiceNumber()
                requestQr(it, fonepayQrGenerateRequest)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    qrStatusMessage.value = QrStatus.QR000.message
                    qrStatusCode.value = QrStatus.QR000.code
                    showAlertDialog.value = false
                    noInternetConnection.value = false
                    qrData.value = it.qrContent
                }, {
                    it.printStackTrace()
                    when {
                        it.localizedMessage.contains(QrStatus.QR001.code) -> {
                            qrStatusMessage.value = QrStatus.QR001.message
                            qrStatusCode.value = QrStatus.QR001.code
                        }
                        it.localizedMessage.contains(QrStatus.QR002.code) -> {
                            qrStatusMessage.value = QrStatus.QR002.message
                            qrStatusCode.value = QrStatus.QR002.code
                        }
                        it.localizedMessage.contains(QrStatus.QR003.code) -> {
                            qrStatusMessage.value = QrStatus.QR003.message
                            qrStatusCode.value = QrStatus.QR003.code
                        }
                    }
                    noInternetConnection.value = it.localizedMessage?.contains("404")!!
                    if (it.localizedMessage.contains(context.getString(R.string.invalid_credential_fonepay))) {
                        localDataUseCase.saveFonePayLoginState(false)
                        message.value = if (AppUtility.isAppVariantNIBL()) {
                            context.getString(R.string.expired_fonepay_contact_support)
                        } else {
                            context.getString(R.string.expired_fonepay)
                        }
                    } else {
                        message.value = it.message
                    }
                })
        )
    }

    private fun requestQr(
        it: SingleEmitter<FonepayQrGenerateResponse>,
        fonepayQrGenerateRequest: FonepayQrGenerateRequest
    ) {
        val jsonRequest = Jsons.toJsonObj(fonepayQrGenerateRequest)
        val jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.post(
            "/device-payment-processor-endpoints/qr/api/getQr/",
            jsonRequest,
            false
        )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)

        when {
            response.data != null -> {
                val responseDataJson = Jsons.toJsonObj(response.data)
                it.onSuccess(
                    Jsons.fromJsonToObj(
                        responseDataJson,
                        FonepayQrGenerateResponse::class.java
                    )
                )
            }
            response.message == "Please check your internet connection and try again." -> {
                it.onError(Exception("404 ${response.message}"))
            }
            response.code == QrStatus.QR001.code -> {
                it.onError(Exception("${response.code}. ${response.message}"))
            }
            response.code == QrStatus.QR002.code -> {
                it.onError(Exception("${response.code}. ${response.message}"))
            }
            response.code == QrStatus.QR003.code -> {
                it.onError(Exception("${response.code}. ${response.message}"))
            }
            (response.code == "" && response.data == null) -> {
                it.onError(Exception("Error Code :: ${404}. ${context.getString(R.string.invalid_credential_fonepay)}"))
            }
            else -> {
                it.onError(Exception("Error Code :: ${response.code}. ${response.message}"))
            }
        }
    }
}