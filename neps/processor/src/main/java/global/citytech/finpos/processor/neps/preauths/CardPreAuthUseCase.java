package global.citytech.finpos.processor.neps.preauths;

import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.ProcessingCode;
import global.citytech.finpos.neps.iso8583.requestsender.preauths.IccPreAuthRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.preauths.MagPreAuthRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.preauths.PICCPreAuthRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.preauths.PreAuthIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionRequestSender;
import global.citytech.finpos.processor.neps.transaction.CardTransactionUseCase;
import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.DateUtils;

/**
 * Created by Saurav Ghimire on 2/3/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class CardPreAuthUseCase extends CardTransactionUseCase {

    @Override
    protected TransactionType getTransactionType() {
        return TransactionType.PRE_AUTH;
    }

    @Override
    protected boolean getAuthCompletionStatus() {
        return false;
    }

    public CardPreAuthUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    protected String getClassName() {
        return CardPreAuthUseCase.class.getName();
    }

    @Override
    protected TransactionIsoRequest prepareIsoRequest(ReadCardResponse readCardResponse, TransactionRequest transactionRequest) {
        String pin = shouldSkipPinBlockFromCard(readCardResponse) ? "" : readCardResponse.getCardDetails().getPinBlock().getPin();
        logger.debug("PIN BLOCK :::: " + pin);
        return PreAuthIsoRequest.Builder.newInstance()
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .transactionAmount(readCardResponse.getCardDetails().getAmount())
                .localDateTime(DateUtils.yyMMddHHmmssDate())
                .track2Data(readCardResponse.getCardDetails().getTrackTwoData())
                .currencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .pinBlock(shouldSkipPinBlockFromCard(readCardResponse) ? "" : readCardResponse.getCardDetails().getPinBlock().getPin())
                .emvData(readCardResponse.getCardDetails().getIccDataBlock())
                .fallBackFromIccToMag(readCardResponse.getFallbackIccToMag())
                .build();
    }


    @Override
    protected NepsMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context) {
        TransactionRequestSender sender;
        switch (readCardResponse.getCardDetails().getCardType()) {
            case MAG:
                sender = new MagPreAuthRequestSender(context);
                break;
            case PICC:
                sender = new PICCPreAuthRequestSender(context);
                break;
            case ICC:
            default:
                sender = new IccPreAuthRequestSender(context);
                break;
        }
        return sender;
    }

    @Override
    protected TransactionResponse prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository, boolean shouldPrint) {
        return PreAuthResponse.Builder.createDefaultBuilder()
                .approved(transactionApproved)
                .stan(stan)
                .shouldPrintCustomerCopy(shouldPrint)
                .debugRequestMessage(isoMessageResponse.getDebugRequestString())
                .debugResponseMessage(isoMessageResponse.getDebugResponseString())
                .message(this.retrieveMessage(isoMessageResponse, transactionApproved, applicationRepository))
                .build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.PRE_AUTHORIZATION.getCode();
    }

    @Override
    protected TransactionResponse prepareErrorResponse(String responseMessage, boolean shouldPrint) {
        return PreAuthResponse.Builder.createDefaultBuilder()
                .stan(this.stan)
                .message(responseMessage)
                .approved(false)
                .shouldPrintCustomerCopy(shouldPrint)
                .build();
    }

}
