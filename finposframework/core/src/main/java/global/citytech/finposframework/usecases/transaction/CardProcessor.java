package global.citytech.finposframework.usecases.transaction;

import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;

public interface CardProcessor {

    ReadCardResponse validateReadCardResponse(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse);

    ReadCardResponse validateReadCardResponseForICC(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse);

    ReadCardResponse processCard(String stan, PurchaseRequest purchaseRequest);

    ReadCardResponse processOnlineResult(ReadCardResponse readCardResponse, IsoMessageResponse isoMessageResponse);

    ReadCardResponse processUnableToGoOnlineResult(ReadCardResponse readCardResponse);
}
