package global.citytech.finpos.merchant.presentation.config

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.common.AppCountDownTimer
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityConfigBinding
import global.citytech.finpos.merchant.presentation.admin.DeviceConfigurationStatus
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.settlement.SettlementActivity
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finposframework.log.Logger

class ConfigActivity : AppBaseActivity<ActivityConfigBinding, ConfigViewModel>() {

    private val logger = Logger(ConfigActivity::class.java.name)

    companion object {
        const val REGISTER_REQUEST_CODE = 1000
        private const val EXTRA_IS_TERMINAL_CONFIGURED = "extra_is_terminal_configured"
        private const val EXTRA_FROM_COMMAND = "extra_from_command"
        const val EXTRA_ERROR_MESSAGE = "extra_error_message"

        fun getLaunchIntent(
            context: Context,
            isTerminalConfigured: Boolean,
            fromCommand: Boolean
        ): Intent {
            val intent = Intent(context, ConfigActivity::class.java)
            intent.putExtra(EXTRA_IS_TERMINAL_CONFIGURED, isTerminalConfigured)
            intent.putExtra(EXTRA_FROM_COMMAND, fromCommand)
            return intent
        }
    }

    var count = 10

    private val appCountDownTimer = AppCountDownTimer(
        10000, 1000,
        this::onCountDownTick,
        this::onCountDownFinish
    )

    private fun onCountDownTick(millisUntilFinished: Long) {
        count--
        updateTimerMessage(
            count.toString().plus(" ").plus(
                if (count == 1) getString(R.string.txt_second)
                else getString(R.string.txt_seconds)
            )
        )
    }

    private fun onCountDownFinish() {
        appCountDownTimer.cancel()
        finishWithSuccess()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObservers()
        if (intent.getBooleanExtra(EXTRA_IS_TERMINAL_CONFIGURED, true))
            startSettlementActivity()
        else
            getViewModel().register()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SettlementActivity.REQUEST_CODE)
            if (resultCode == Activity.RESULT_OK)
                Handler().postDelayed({
                    getViewModel().register()
                }, 200)
            else
                showErrorMessage(getString(R.string.error_msg_settlement_error_before_configuration))
    }

    private fun startSettlementActivity() {
        SettlementActivity.getLaunchIntent(
            this,
            isManualSettlement = false,
            isSettlementSuccessConfirmationRequired = false
        )
    }

    private fun initObservers() {
        getViewModel().isLoading.observe(this, Observer { isLoading ->
            if (isLoading) {
                showProgress()
            } else {
                hideProgress()
            }
        })

        getViewModel().message.observe(this, Observer {
            showErrorMessage(it)
        })



        getViewModel().deviceConfigurationStatus.observe(this, Observer { status ->
            when (status) {
                DeviceConfigurationStatus.TerminalConfigured -> {
                    hideProgress()
                    getViewModel().setSwitchConfigActive(false)
                    count = 10
                    showSuccessMessage(
                        MessageConfig.Builder()
                            .message(getString(R.string.terminal_configured_successfully))
                            .positiveLabel(getString(R.string.return_to_dashboard))
                            .onPositiveClick {
                                appCountDownTimer.cancel()
                                finishWithSuccess()
                            })
                    appCountDownTimer.start()
                }
                DeviceConfigurationStatus.ConfigurationCanceled -> {
                    hideProgress()
                }
                DeviceConfigurationStatus.ConfigurationFailed -> {
                    hideProgress()
                    showSwitchConfigurationErrorMessage(getString(R.string.terminal_configuration_failed))
                }
                else -> {
                    showProgress(status.message)
                }
            }
        })

        getViewModel().navigateToOperatorLoginMessage.observe(this, Observer {
            if (intent.getBooleanExtra(EXTRA_FROM_COMMAND, true))
                finishWithFailure(getString(R.string.error_msg_operator_not_logged_in))
            else
                showConfirmationMessage(
                    MessageConfig.Builder()
                        .message(it)
                        .positiveLabel(getString(R.string.title_ok))
                        .onPositiveClick {
                            this.navigateToOperatorLogin()
                        }
                        .negativeLabel(getString(R.string.action_cancel))
                        .onNegativeClick {
                            finishWithFailure(getString(R.string.error_msg_operator_not_logged_in))
                        }
                )
        })

        getViewModel().registrationComplete.observe(this, Observer {
            setResult(RESULT_OK)
        })
    }


    private fun showErrorMessage(message: String?) {
        showNormalMessage(
            MessageConfig.Builder()
                .message(message)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    finishWithFailure(message!!)
                }
        )
    }

    private fun finishWithSuccess() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun finishWithFailure(message: String) {
        val intent = Intent()
        intent.putExtra(EXTRA_ERROR_MESSAGE, message)
        setResult(Activity.RESULT_CANCELED, intent)
        finish()
    }

    private fun navigateToOperatorLogin() {
        val i = Intent("global.citytech.action.OPERATOR")
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(i)
    }

    private fun showSwitchConfigurationErrorMessage(message: String?) {
        showConfirmationMessage(MessageConfig.Builder()
            .message(message)
            .positiveLabel(getString(R.string.action_retry))
            .onPositiveClick {
                getViewModel().register()
            }
            .negativeLabel(getString(R.string.action_cancel))
            .onNegativeClick {
                finishWithFailure(message!!)
            })
    }

    override fun getBindingVariable(): Int = BR.configViewModel

    override fun getLayout(): Int = R.layout.activity_config

    override fun getViewModel(): ConfigViewModel =
        ViewModelProviders.of(this)[ConfigViewModel::class.java]
}