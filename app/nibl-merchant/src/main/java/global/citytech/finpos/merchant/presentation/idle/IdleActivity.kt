package global.citytech.finpos.merchant.presentation.idle

import android.app.Activity
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.wajahatkarim3.roomexplorer.RoomExplorer
import global.citytech.common.Constants
import global.citytech.common.data.PosCallback
import global.citytech.common.data.PosMessage
import global.citytech.common.extensions.openActivity
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.datasource.app.db.AppDatabase
import global.citytech.finpos.merchant.databinding.ActivityIdleBinding
import global.citytech.finpos.merchant.presentation.TextToSpeech.TextToSpeech
import global.citytech.finpos.merchant.presentation.admin.AdminActivity
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmationDialog
import global.citytech.finpos.merchant.presentation.amount.newlayout.NewAmountActivity
import global.citytech.finpos.merchant.presentation.amount.oldlayout.AmountActivity
import global.citytech.finpos.merchant.presentation.appnotification.AppNotificationActivity
import global.citytech.finpos.merchant.presentation.card.check.CardCheckActivity
import global.citytech.finpos.merchant.presentation.dashboard.cashmode.CashModeActivity
import global.citytech.finpos.merchant.presentation.dashboard.retailer.RetailerModeActivity
import global.citytech.finpos.merchant.presentation.model.CardReadError
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.LogoItem
import global.citytech.finpos.merchant.presentation.model.MerchantItem
import global.citytech.finpos.merchant.presentation.settlement.SettlementActivity
import global.citytech.finpos.merchant.presentation.splash.SplashScreenActivity
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.notifier.Notifier
import global.citytech.finposframework.switches.posmode.PosMode
import global.citytech.finposframework.utility.StringUtils
import kotlinx.android.synthetic.main.activity_idle.*
import java.math.BigDecimal
import kotlin.system.exitProcess


class IdleActivity : AppBaseActivity<ActivityIdleBinding, IdleViewModel>(),
    TransactionConfirmationDialog.Listener {

    private val logger = Logger(IdleActivity::class.java.name)
    private lateinit var viewModel: IdleViewModel
    private var cleanUpOnPause: Boolean = true
    private var amount: BigDecimal = BigDecimal.ZERO
    private var progressDialog: ProgressDialog? = null
    private var transactionConfirmationDialog: TransactionConfirmationDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.viewModel = getViewModel()
        initViews()
        initObserver()
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    override fun onResume() {
        super.onResume()
        logger.log("IdleActivity ::: onResume...")
        registerBroadCastReceivers()
        if (!this.viewModel.cardAlreadyRead) {
            hideToolBarItems()
            checkMemoryAndExecute(callback = Runnable {
                this.viewModel.disableCardMonitoring(IdleViewModel.TASK.CHECK_CARD_AND_FORCE_REQUEST_FLAG)
            })
        }
        getViewModel().checkForNotifications()
    }

    override fun onPause() {
        logger.log("IdleActivity ::: onPause...")
        this.viewModel.compositeDisposable.clear()
        unregisterBroadCastReceivers()
        if ((!this.viewModel.cardAlreadyRead) && this.cleanUpOnPause) {
            logger.log("cardAlreadyRead ::: ".plus(this.viewModel.cardAlreadyRead))
            logger.log("cleanUpOnPause ::: ".plus(this.cleanUpOnPause))
            this.viewModel.cardAlreadyRead = false
            this.viewModel.disableCardMonitoring(IdleViewModel.TASK.NONE)
        }

        if (!this.cleanUpOnPause)
            this.cleanUpOnPause = true
        super.onPause()
    }

    private fun registerBroadCastReceivers() {
        registerReprintBroadcastReceivers()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, IntentFilter(Constants.INTENT_ACTION_NOTIFICATION))
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, IntentFilter(Constants.INTENT_ACTION_POS_CALLBACK))
    }

    private fun unregisterBroadCastReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        unregisterReprintBroadCastReceivers()
    }

    private var broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            logger.log("BroadCast ::: IdleActivity ::: ".plus(intent))
            when {
                intent!!.action == Constants.INTENT_ACTION_NOTIFICATION -> {
                    handleNotificationBroadcast(intent)
                }
                intent.action == Constants.INTENT_ACTION_POS_CALLBACK -> {
                    logger.log("BROADCAST ::: POS CALLBACK RECEIVED >>>")
                    handlePosCallbackBroadcast(intent)
                }
            }
        }
    }

    private fun handlePosCallbackBroadcast(intent: Intent) {
        val posCallback =
            intent.getSerializableExtra(Constants.KEY_BROADCAST_MESSAGE) as PosCallback
        if (posCallback.posMessage == PosMessage.POS_MESSAGE_START_IDLE_ACTIVITY) {
            logger.log("::: POS MESSAGE START IDLE ACTIVITY :::")
            if (!this.viewModel.cardAlreadyRead) {
                hideToolBarItems()
                checkMemoryAndExecute(callback = Runnable {
                    this.viewModel.disableCardMonitoring(IdleViewModel.TASK.CHECK_CARD_AND_FORCE_REQUEST_FLAG)
                })
            }
        } else {
            logger.log("::: POS MESSAGE EVERYTHING ELSE :::")
            showProgressScreen(posCallback.posMessage.message)
        }
    }

    private fun handleNotificationBroadcast(intent: Intent) {
        val eventType =
            intent.getSerializableExtra(Constants.EXTRA_EVENT_TYPE) as Notifier.EventType
        logger.log("BROADCAST :::: EVENT TYPE IN BROADCAST ::: $eventType.name")
        val message = intent.getStringExtra(Constants.EXTRA_NOTIFICATION_MESSAGE)
        logger.log("BROADCAST :::: MESSAGE IN BROADCAST ::: $message")
        when (eventType) {
            Notifier.EventType.READING_CARD,
            Notifier.EventType.SWITCH_ICC_TO_MAGNETIC,
            Notifier.EventType.CONNECTING_TO_SWITCH,
            Notifier.EventType.TRANSMITTING_REQUEST,
            Notifier.EventType.RESPONSE_RECEIVED,
            Notifier.EventType.PROCESSING_ISSUER_SCRIPT,
            Notifier.EventType.DETECTED_MULTIPLE_AID,
            Notifier.EventType.PREPARING_RECONCILIATION_TOTALS,
            Notifier.EventType.PREPARING_DETAIL_REPORT,
            Notifier.EventType.BATCH_UPLOADING,
            Notifier.EventType.BATCH_UPLOAD_PROGRESS,
            Notifier.EventType.PRINTING_DETAIL_REPORT,
            Notifier.EventType.PRINTING_SETTLEMENT_REPORT -> {
                this.showProgressScreen(message!!)
            }
            Notifier.EventType.TRANSACTION_APPROVED -> {
                this.hideProgressScreen()
                this.showTransactionApprovedDialog(message!!)
            }
            Notifier.EventType.TRANSACTION_DECLINED -> {
                this.hideProgressScreen()
                this.showTransactionDeclinedDialog(message!!)
            }
            Notifier.EventType.PERFORMING_REVERSAL -> {
                this.showProgressScreen(message!!)
            }
            Notifier.EventType.REVERSAL_COMPLETED -> {
                this.hideProgressScreen()
            }
            else -> this.hideProgressScreen()
        }
    }

    private fun showProgressScreen(message: String) {
        if (progressDialog == null) {
            progressDialog = ProgressDialog(this)
        }
        progressDialog!!.setCancelable(false)
        progressDialog!!.setMessage(message)
        progressDialog!!.show()
    }

    private fun hideProgressScreen() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    private fun showTransactionApprovedDialog(message: String) {
        val transactionConfirmation = TransactionConfirmation.Builder()
            .title("APPROVED")
            .message(message)
            .imageId(R.drawable.ic_check_circle_green_64dp)
            .positiveLabel("")
            .negativeLabel("")
            .qrImageString("")
            .transactionStatus("")
            .build()
        this.showTransactionConfirmationDialog(transactionConfirmation, false)
    }

    private fun showTransactionDeclinedDialog(message: String) {
        val transactionConfirmation = TransactionConfirmation.Builder()
            .title("DECLINED")
            .message(message)
            .imageId(R.drawable.ic_failure_red_64dp)
            .positiveLabel("")
            .negativeLabel("")
            .build()
        this.showTransactionConfirmationDialog(transactionConfirmation, false)
    }

    private fun showTransactionConfirmationDialog(it: TransactionConfirmation, update: Boolean) {
        if (transactionConfirmationDialog == null)
            transactionConfirmationDialog = TransactionConfirmationDialog(this, this, it)
        if (update){
            transactionConfirmationDialog!!.update(it, this)
        }
        transactionConfirmationDialog!!.show()
    }

    private fun hideTransactionConfirmationDialog() {
        transactionConfirmationDialog?.hide()
        transactionConfirmationDialog = null
    }

    override fun onPositiveButtonClick() {
        this.showProgressScreen("Printing Customer Copy..")
        this.hideTransactionConfirmationDialog()
        this.viewModel.printCustomerCopy()
    }

    override fun onNegativeButtonClick() {
        this.hideTransactionConfirmationDialog()
        this.viewModel.disableCardMonitoring(IdleViewModel.TASK.CHECK_CARD_AND_FORCE_REQUEST_FLAG)
    }

    private fun initObserver() {
        getViewModel().configuration.observe(this, Observer { configurationItem ->
            checkConfigurationItemAndProceed(configurationItem)
        })

        viewModel.showActionMenu.observe(
            this,
            Observer { onObserveShowActionMenu(it) }
        )

        viewModel.hideTerminalId.observe(
            this,
            Observer { onObserveHideTerminalId(it) }
        )

        viewModel.loadCardCheckActivity.observe(
            this,
            Observer { onObserveLoadCardCheckActivity(it) }
        )

        viewModel.tvReadyMessage.observe(
            this,
            Observer { onObserveTvReadyMessage(it) }
        )

        viewModel.cardReadError.observe(
            this,
            Observer {
                onObserveCardReadError(it)
            }
        )

        viewModel.loadAmountPage.observe(
            this,
            Observer { onObserveLoadAmountPage(it) }
        )

        viewModel.message.observe(
            this,
            Observer { onObserveMessage(it) }
        )

        viewModel.transactionComplete.observe(
            this,
            Observer { onObserveTransactionComplete(it) }
        )

        viewModel.transactionConfirmationData.observe(
            this,
            Observer {
//                LogManager.clear()
                showTransactionConfirmationDialog(it, true)
            }
        )

        viewModel.customerCopyPrintMessage.observe(
            this,
            Observer { showCustomerCopyResponseDialog(it) }
        )

        viewModel.isLoading.observe(
            this,
            Observer { onObserveIsLoading(it) }
        )

        viewModel.startDashboard.observe(this, Observer {
            when (it) {
                PosMode.RETAILER -> startRetailerModeActivity()
                PosMode.CASH -> startCashModeActivity()
                else -> logger.log("::: <<< NO SUCH MODE >>> ::: ")
            }
        })

        viewModel.tvPosStatusMessage.observe(this, Observer {
            tv_pos_status.text = it
            if (it.isNullOrEmptyOrBlank())
                tv_pos_status.visibility = View.GONE
            else
                tv_pos_status.visibility = View.VISIBLE
        })

        viewModel.startSettlement.observe(this, Observer {
            startSettlementActivity()
        })

        viewModel.noNotifications.observe(this, Observer {
            if (it)
                iv_app_notification.visibility = View.GONE
            else
                iv_app_notification.visibility = View.VISIBLE
        })
    }

    private fun startSettlementActivity() {
        val intent = Intent(this, SettlementActivity::class.java)
        startActivityForResult(intent, SettlementActivity.REQUEST_CODE)
    }

    private fun startCashModeActivity() {
        openActivity(CashModeActivity::class.java)
        finish()
    }

    private fun startRetailerModeActivity() {
        openActivity(RetailerModeActivity::class.java)
        finish()
    }

    private fun checkConfigurationItemAndProceed(configurationItem: ConfigurationItem?) {
        if (isTerminalStatusConfigured(configurationItem)) {
            onTerminalStatusIsConfigured(configurationItem!!)
        } else {
            onTerminalStatusIsNotConfigured()
        }
    }

    private fun isTerminalStatusConfigured(configurationItem: ConfigurationItem?): Boolean {
        val configured = getViewModel().getTerminalConfigurationStatus();
        return (configurationItem?.merchants != null && !configurationItem.merchants!!.isNullOrEmpty() && configured)
    }

    private fun onTerminalStatusIsConfigured(configurationItem: ConfigurationItem) {
        val merchant: MerchantItem = configurationItem.merchants!![0]
        val logo: LogoItem = configurationItem.logos!![0]
//            iv_wallpaper.loadBase64String(logo.appWallpaper)
        tv_merchant_id.text = getString(R.string.title_terminal_id).plus(merchant.terminalId)
        tv_ready.visibility = View.VISIBLE
        iv_menu.visibility = View.VISIBLE
    }

    private fun onTerminalStatusIsNotConfigured() {
        getViewModel().setTerminalConfigurationStatus(false)
        showNotConfiguredDialog()
    }

    private fun onObserveShowActionMenu(show: Boolean) {
        if (show) {
            iv_menu.visibility = View.VISIBLE
        } else {
            iv_menu.visibility = View.INVISIBLE
        }
    }

    private fun onObserveHideTerminalId(hide: Boolean) {
        if (hide) {
            iv_menu.visibility = View.INVISIBLE
        } else {
            iv_menu.visibility = View.VISIBLE
        }
    }

    private fun onObserveLoadCardCheckActivity(load: Boolean) {
        if (load) {
            val intent = Intent(this, CardCheckActivity::class.java)
            startActivityForResult(intent, CardCheckActivity.REQUEST_CODE)
        }
    }

    private fun onObserveTvReadyMessage(message: String?) {
        if (StringUtils.isEmpty(message)) {
            tv_ready.visibility = View.INVISIBLE
        } else {
            tv_ready.visibility = View.VISIBLE
            tv_ready.text = message
        }
    }

    private fun onObserveCardReadError(cardReadError: CardReadError) {
        val transactionConfirmation = TransactionConfirmation
            .Builder()
            .title(cardReadError.title)
            .message(cardReadError.errorMessage)
            .imageId(R.drawable.ic_failure_red_64dp)
            .positiveLabel(getString(R.string.title_ok))
            .build()
        transactionConfirmationDialog = TransactionConfirmationDialog(
            this,
            cardReadErrorDialogListener(),
            transactionConfirmation
        )

        if (!cardReadError.fromBroadcast) {
            transactionConfirmationDialog!!.update(
                cardReadError.title,
                cardReadError.errorMessage,
                updatedCardReadErrorDialogListener()
            )
        }
    }

    private fun onObserveMessage(message: String?) {
        if (!message.isNullOrEmpty()) {
            val messageConfig = MessageConfig.Builder()
                .message(message)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    viewModel.disableCardMonitoring(IdleViewModel.TASK.CHECK_CARD_AND_FORCE_REQUEST_FLAG)
                }
            showNormalMessage(messageConfig)
        }
    }

    private fun onObserveTransactionComplete(transactionComplete: Boolean) {
        if (!transactionComplete) {
            hideProgressScreen()
            hideTransactionConfirmationDialog()
        }
    }

    private fun onObserveIsLoading(it: Boolean) {
        if (!it) {
            hideProgressScreen()
        }
    }

    private fun cardReadErrorDialogListener(): TransactionConfirmationDialog.Listener =
        object : TransactionConfirmationDialog.Listener {
            override fun onPositiveButtonClick() {
                transactionConfirmationDialog!!.hide()
            }

            override fun onNegativeButtonClick() {
                transactionConfirmationDialog!!.hide()
            }
        }

    private fun updatedCardReadErrorDialogListener(): TransactionConfirmationDialog.Listener =
        object : TransactionConfirmationDialog.Listener {
            override fun onPositiveButtonClick() {
                transactionConfirmationDialog!!.hide()
                if (viewModel.getCardMonitoringEnabledPreference()) {
                    viewModel.cleanUpAndCheckIfCardInserted()
                }
            }

            override fun onNegativeButtonClick() {
                transactionConfirmationDialog!!.hide()
            }
        }

    private fun onObserveLoadAmountPage(load: Boolean) {
        if (load) {
            viewModel.loadAmountPage.value = false
            if (viewModel.getNumpadType(getString(R.string.title_old_layout)) == getString(R.string.title_old_layout)){
                val intent = Intent(this, AmountActivity::class.java)
                startActivityForResult(intent, AmountActivity.REQUEST_CODE)
            }else{
                val intent = Intent(this, NewAmountActivity::class.java)
                startActivityForResult(intent, NewAmountActivity.REQUEST_CODE)
            }
        }
    }

    private fun showNotConfiguredDialog() {
        showFailureMessage(
            MessageConfig.Builder()
                .message(getString(R.string.msg_not_configured))
                .positiveLabel("OK")
                .onPositiveClick {
                    openActivity(AdminActivity::class.java)
                    finish()
                })
    }

    override fun onBackPressed() {
        showSuccessMessageAndExit()
    }

    private fun showSuccessMessageAndExit() {
        val messageConfig = MessageConfig.Builder()
            .title(getString(R.string.title_exit))
            .message(getString(R.string.msg_confirm_exit))
            .positiveLabel(getString(R.string.action_yes))
            .negativeLabel(getString(R.string.action_no))
            .onPositiveClick {
                viewModel.disableCardMonitoring(IdleViewModel.TASK.NONE)
                closeApplication()
            }
            .onNegativeClick { }
        showConfirmationMessage(messageConfig)
    }

    private fun closeApplication() {
        finishAndRemoveTask()
        exitProcess(0)
    }

    private fun initViews() {
        iv_menu.setOnClickListener {
            this.viewModel.cardAlreadyRead = false
            this.viewModel.disableCardMonitoring(IdleViewModel.TASK.START_DASHBOARD)
            this.cleanUpOnPause = false
        }

        tv_ready.setOnClickListener {
            if (BuildConfig.DEBUG)
                RoomExplorer.show(this, AppDatabase::class.java, "nibl_db.db")
        }

        iv_app_notification.setOnClickListener {
            startAppNotificationActivity()
        }
    }

    private fun startAppNotificationActivity() {
        val intent = Intent(this, AppNotificationActivity::class.java)
        startActivity(intent)
    }

    private fun hideToolBarItems() {
        iv_menu.visibility = View.INVISIBLE
    }

    private fun checkMemoryAndExecute(callback: Runnable) {
        val maxMemory: Long
        val usedMemory: Long
        var availableMemoryPercentage = 1.0
        val minAvailableMemoryInPercentage = 0.20

        val runtime: Runtime = Runtime.getRuntime()
        maxMemory = runtime.maxMemory()
        usedMemory = runtime.totalMemory() - runtime.freeMemory()
        availableMemoryPercentage = (1 - (usedMemory / maxMemory)).toDouble()

        logger.log("Max Memory ::: ".plus(maxMemory))
        logger.log("Used Memory ::: ".plus(usedMemory))
        logger.log("Available Memory Percentage ::: ".plus(availableMemoryPercentage))

        if (availableMemoryPercentage < minAvailableMemoryInPercentage) {
            tv_ready.text = getString(R.string.please_wait)
            Handler().postDelayed(
                {
                    val packageManager = packageManager
                    val intent = Intent(this, SplashScreenActivity::class.java)
                    intent.flags =
                        (Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    runtime.exit(0)
                },
                4000
            )
        } else {
            callback.run()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                AmountActivity.REQUEST_CODE, NewAmountActivity.REQUEST_CODE -> onAmountActivityResult(data)
                SettlementActivity.REQUEST_CODE -> onSettlementActivityResult()
            }
        } else {
            this.viewModel.disableCardMonitoring(IdleViewModel.TASK.CHECK_IF_CARD_PRESENT)
        }
    }

    private fun onSettlementActivityResult() {
        getViewModel().checkForNotifications()
        viewModel.enableHardwareButtons()
        viewModel.enableCardMonitoring()
    }

    private fun onAmountActivityResult(data: Intent?) {
        data?.let {
            val amountInString = data.getStringExtra(getString(R.string.intent_confirmed_amount))
            amount = amountInString.toBigDecimal()
            showProgressScreen(getString(R.string.title_processing))
            Handler().postDelayed(
                { this.viewModel.startTransaction(amount) },
                1000
            )
        }
    }

    private fun showCustomerCopyResponseDialog(it: String?) {
        val messageConfig = MessageConfig.Builder()
            .message(it).positiveLabel(getString(R.string.title_ok))
            .onPositiveClick {
                viewModel.disableCardMonitoring(IdleViewModel.TASK.CHECK_CARD_AND_FORCE_REQUEST_FLAG)
            }
        showNormalMessage(messageConfig)
    }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayout(): Int = R.layout.activity_idle

    override fun getViewModel(): IdleViewModel =
        ViewModelProviders.of(this)[IdleViewModel::class.java]

}