package global.citytech.finposframework.usecases.transaction;

import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.usecases.transaction.data.StatementList;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.usecases.transaction.receipt.keyexchange.KeyExchangeReceipt;
import global.citytech.finposframework.usecases.transaction.receipt.logon.MerchantLogonReceipt;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;

/**
 * Created by Unique Shakya
 */

public interface ReceiptHandler {

    interface TransactionReceiptHandler {
        PrinterResponse printTransactionReceipt(
                ReceiptLog receiptLog,
                ReceiptVersion receiptVersion
        );
        PrinterResponse printTransactionReceipt(
                ReceiptLog receiptLog,
                ReceiptVersion receiptVersion,
                StatementList statementList
        );
    }

    interface MerchantLogonReceiptHandler {
        void printLogonReceipt(MerchantLogonReceipt merchantLogonReceipt);
    }

    interface KeyExchangeReceiptHandler {
        void printKeyExchangeReceipt(KeyExchangeReceipt keyExchangeReceipt);
    }

    interface IsoMessageReceiptHandler {
        void printIsoMessageReceipt(IsoMessageResponse isoMessageResponse);
    }
}
