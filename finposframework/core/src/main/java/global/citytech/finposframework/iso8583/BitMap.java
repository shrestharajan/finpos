package global.citytech.finposframework.iso8583;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import global.citytech.finposframework.log.Logger;;

import global.citytech.finposframework.utility.ByteUtils;
import global.citytech.finposframework.utility.EncodingUtils;
import global.citytech.finposframework.utility.HelperUtils;

/**
 * User: Surajchhetry Date: 2/18/20 Time: 8:36 PM
 */
public class BitMap {
    public final int SECONDARY_BIT_MAP_LENGTH = 128;
    private final BitSet bits;
    public static final int PRIMARY_BIT_MAP_LENGTH = 64;
    public static final int LENGTH_IN_BYTES = 8;
    private static Logger logger = Logger.getLogger(BitMap.class.getName());

    public BitMap() {
        bits = new BitSet(PRIMARY_BIT_MAP_LENGTH);
    }

    enum Type {
        Hex,
        HexEncoded,
        HexASCII,
        NONE
    }

    public void set(int index) {
        if (index > PRIMARY_BIT_MAP_LENGTH) bits.set(0);
        // Since index start with 0
        bits.set(index - 1);
    }

    public void unSet(int index) {
        bits.clear(index);
    }

    private boolean hasSecondaryBitMap() {
        return bits.get(0);
    }

    public List<Integer> getFields() {
        List<Integer> fields = new ArrayList<>();
        for (int index = 0; index < this.bits.size(); index++)
            if (this.bits.get(index)) fields.add(index + 1);

        return Collections.unmodifiableList(fields);
    }

    // Encodings
    public String asBinary() {
        int length =
                this.hasSecondaryBitMap() == true ? SECONDARY_BIT_MAP_LENGTH : PRIMARY_BIT_MAP_LENGTH;
        return toString(this.bits, length);
    }

    private static String toString(BitSet bits, int length) {
        StringBuilder binary = new StringBuilder();
        // int length = this.hasSecondaryBitMap() == true ? SECONDARY_BIT_MAP_LENGTH :
        // PRIMARY_BIT_MAP_LENGTH;
        for (int i = 0; i < length; i++) {
            if (bits.get(i)) binary.append("1");
            else binary.append("0");
        }
        return binary.toString();
    }

    public String asHexEncoded() {
        return EncodingUtils.asciiRepresentation(asBinary());
    }

    public String asHexValue() {
        return new BigInteger(asBinary(), 2).toString(16).toUpperCase();
    }

    public byte[] asHexASCII() {
        return EncodingUtils.hex2Bytes(asHexValue());
    }

    public String printField() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (Integer index : this.getFields())
          builder.append(index).append(",");
        builder.delete(builder.length() - 1, builder.length());
        builder.append("]");
        return builder.toString();
    }

    /*
       Factory Method
    */
    public static BitMap parseFromBinaryString(String binaryString) {
        BitMap bitMap = new BitMap();
        for (int index = 0; index < binaryString.length(); index++) {
            if (binaryString.charAt(index) == '1')
                bitMap.set(index + 1); // Since index start from 0 but starting  position is 1
        }
        return bitMap;
    }

    public static BitMap parseFromHexEncoded(String hexEncoded) {
        String binaryRepresentation = EncodingUtils.hexEncodedToBinaryString(hexEncoded);
        return parseFromBinaryString(binaryRepresentation);
    }

    public static BitMap parseFromHexValue(String hexValue) {
        StringBuilder adjusted = new StringBuilder();
        String binaryRepresentation = EncodingUtils.fromHexValueToBinary(hexValue);
        if (binaryRepresentation.length() < PRIMARY_BIT_MAP_LENGTH) {
            adjusted.append(
                    HelperUtils.repeat("0", PRIMARY_BIT_MAP_LENGTH - binaryRepresentation.length()));
        }
        adjusted.append(binaryRepresentation);
        return parseFromBinaryString(adjusted.toString());
    }

    public static BitMap parseFromHexASCII(byte[] bitMap) {
        if (bitMap.length < BitMap.LENGTH_IN_BYTES)
            throw new IllegalArgumentException("Invalid length");
        byte[] onlyBitMap = Arrays.copyOfRange(bitMap, 0, BitMap.LENGTH_IN_BYTES);
        String binaryString = ByteUtils.toBinary(Arrays.copyOfRange(bitMap, 0, BitMap.LENGTH_IN_BYTES));
        logger.debug(binaryString);
        return parseFromBinaryString(binaryString);
    }
}
