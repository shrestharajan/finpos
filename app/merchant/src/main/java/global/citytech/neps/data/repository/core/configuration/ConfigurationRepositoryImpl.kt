package global.citytech.neps.data.repository.core.configuration

import global.citytech.neps.data.datasource.core.configuration.ConfigurationDataSource
import global.citytech.neps.domain.repository.core.configuration.ConfigurationRepository
import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.posswitchintregator.api.logon.LogOnResponseParameter
import global.citytech.posswitchintregator.neps.usecases.keyexchange.KeyExchangeResponseModel
import io.reactivex.Observable

class ConfigurationRepositoryImpl(val isoDataSource: ConfigurationDataSource):
    ConfigurationRepository {
    override fun logOn(configurationItem: ConfigurationItem) : Observable<LogOnResponseParameter> = isoDataSource.logOn(configurationItem)
    override fun exchangeKey(configurationItem: ConfigurationItem ) : Observable<KeyExchangeResponseModel> = isoDataSource.exchangeKey(configurationItem)
}