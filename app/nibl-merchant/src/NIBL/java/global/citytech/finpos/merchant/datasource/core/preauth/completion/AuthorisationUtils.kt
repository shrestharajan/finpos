package global.citytech.finpos.merchant.datasource.core.preauth.completion

import global.citytech.finpos.merchant.presentation.model.preauth.completion.AuthorisationCompletionRequestItem
import global.citytech.finpos.processor.nibl.transaction.authorisation.completion.AuthorisationCompletionRequest

/**
 * Created by Unique Shakya on 4/7/2021.
 */
object AuthorisationUtils {
    fun prepareAuthorisationCompletionRequest(authorisationCompletionRequestItem: AuthorisationCompletionRequestItem): AuthorisationCompletionRequest? {
        val authorisationCompletionRequest =
            AuthorisationCompletionRequest(
                authorisationCompletionRequestItem.transactionType
            )
        authorisationCompletionRequest.amount = authorisationCompletionRequestItem.amount
        authorisationCompletionRequest.originalInvoiceNumber =
            authorisationCompletionRequestItem.originalData
        return authorisationCompletionRequest
    }
}