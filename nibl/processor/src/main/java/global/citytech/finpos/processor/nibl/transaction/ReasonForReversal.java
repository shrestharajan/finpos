package global.citytech.finpos.processor.nibl.transaction;

/**
 * Created by Unique Shakya on 10/5/2020.
 */
public class ReasonForReversal {

    private Type type;
    private String message;

    public ReasonForReversal(Type type, String message) {
        this.type = type;
        this.message = message;
    }

    public Type getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public enum Type {
        TIMEOUT("Timeout"),
        INVALID_RESPONSE("Invalid Response"),
        DECLINED_BY_CARD("Declined by Card");

        private String description;

        Type(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }
    }
}
