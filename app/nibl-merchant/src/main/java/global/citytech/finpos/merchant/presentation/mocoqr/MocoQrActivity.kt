package global.citytech.finpos.merchant.presentation.mocoqr

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityMocoQrBinding
import global.citytech.finpos.merchant.framework.qr.QrHelper
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import kotlinx.android.synthetic.main.activity_moco_qr.*


class MocoQrActivity : AppBaseActivity<ActivityMocoQrBinding, MocoQrViewModel>() {

    private lateinit var qrHelper: QrHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
        getViewModel().getTerminalSettings()
        getViewModel().isDisclaimerShown()
        initListeners()
        observeTerminalSettings()
        observeDisclaimerStatus()
    }

    private fun initData() {
        qrHelper = QrHelper()
    }

    private fun observeDisclaimerStatus() {
        getViewModel().isDisclaimerShown.observe(this, Observer {
            if (!it) {
                startMocoDisclaimerActivity()
            }
        })
    }

    private fun startMocoDisclaimerActivity() {
        val intent = Intent(this, MocoQrDisclaimerActivity::class.java)
        startActivityForResult(intent, MocoQrDisclaimerActivity.REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MocoQrDisclaimerActivity.REQUEST_CODE && resultCode == Activity.RESULT_CANCELED) {
            finish()
        }
    }

    private fun initListeners() {
        iv_back.setOnClickListener {
            finish()
        }
    }

    private fun observeTerminalSettings() {
        getViewModel().terminalSettings.observe(this, Observer {
            if (!it.qrCode.isNullOrEmptyOrBlank()) {
                it.qrCode?.let { it1 -> setQrCode(it1) }
            }
        })
    }

    private fun setQrCode(qrData: String) {
        val qrBitmap = qrHelper.generateQrBitmap(qrData)
        if (qrBitmap != null) {
            iv_moco_qr.setImageBitmap(qrBitmap)
        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayout(): Int {
        return R.layout.activity_moco_qr
    }

    override fun getViewModel(): MocoQrViewModel {
        return ViewModelProviders.of(this)[MocoQrViewModel::class.java]
    }
}