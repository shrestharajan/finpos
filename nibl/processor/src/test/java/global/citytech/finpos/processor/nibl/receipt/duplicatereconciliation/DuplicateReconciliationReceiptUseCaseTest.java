package global.citytech.finpos.processor.nibl.receipt.duplicatereconciliation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import global.citytech.finpos.processor.nibl.reconciliation.NiblReconciliationReceiptPrintHandler;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ReconciliationRepository;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

/**
 * Created by Unique Shakya on 12/16/2020.
 */
@RunWith(PowerMockRunner.class)
public class DuplicateReconciliationReceiptUseCaseTest {

    PrinterService printerService;
    ReconciliationRepository reconciliationRepository;
    NiblReconciliationReceiptPrintHandler niblReconciliationReceiptPrintHandler;

    @Before
    public void setup() {
        printerService = PowerMockito.mock(PrinterService.class);
        reconciliationRepository = PowerMockito.mock(ReconciliationRepository.class);
        niblReconciliationReceiptPrintHandler = PowerMockito.mock(NiblReconciliationReceiptPrintHandler.class);
    }

    @Test
    public void printTest() throws Exception {
        PowerMockito.when(printerService.print(any())).thenReturn(new PrinterResponse(Result.SUCCESS, "SUCCESS"));
        String reconciliationReceiptString = "{\"host\":\"000048\",\"performance\":{\"endDateTime\":\"1216115051\",\"startDateTime\":\"1216115051\"},\"reconciliationBatchNumber\":\"042\",\"reconciliationResult\":\"SETTLEMENT UNSUCCESSFUL\",\"reconciliationTotals\":{\"authAmount\":0,\"authCount\":0,\"authRefundAmount\":0,\"authRefundCount\":0,\"debitAmount\":0,\"debitCount\":0,\"debitRefundAmount\":0,\"debitRefundCount\":0,\"salesAmount\":20000,\"salesCount\":2,\"salesRefundAmount\":10000,\"salesRefundCount\":1,\"totalsMap\":{\"VISA_CREDIT\":{\"authAmount\":0,\"authCount\":0,\"authRefundAmount\":0,\"authRefundCount\":0,\"cardSchemeType\":\"VISA_CREDIT\",\"debitAmount\":0,\"debitCount\":0,\"debitRefundAmount\":0,\"debitRefundCount\":0,\"salesAmount\":20000,\"salesCount\":2,\"salesRefundAmount\":0,\"salesRefundCount\":0},\"MADA\":{\"authAmount\":0,\"authCount\":0,\"authRefundAmount\":0,\"authRefundCount\":0,\"cardSchemeType\":\"MADA\",\"debitAmount\":0,\"debitCount\":0,\"debitRefundAmount\":0,\"debitRefundCount\":0,\"salesAmount\":0,\"salesCount\":0,\"salesRefundAmount\":10000,\"salesRefundCount\":1},\"VISA_DEBIT\":{\"authAmount\":0,\"authCount\":0,\"authRefundAmount\":0,\"authRefundCount\":0,\"cardSchemeType\":\"VISA_DEBIT\",\"debitAmount\":0,\"debitCount\":0,\"debitRefundAmount\":0,\"debitRefundCount\":0,\"salesAmount\":0,\"salesCount\":0,\"salesRefundAmount\":0,\"salesRefundCount\":0}}},\"retailer\":{\"merchantId\":\"405635000007367\",\"retailerAddress\":\"Kamaladi,Kathmandu\",\"retailerName\":\"Citytech\",\"terminalId\":\"00001678\"}}";
        PowerMockito.when(reconciliationRepository.getReconciliationReceipt()).thenReturn(reconciliationReceiptString);

        PowerMockito.whenNew(NiblReconciliationReceiptPrintHandler.class).withArguments(printerService)
                .thenReturn(niblReconciliationReceiptPrintHandler);
        PowerMockito.doNothing().when(niblReconciliationReceiptPrintHandler).printReceipt(any());

        DuplicateReconciliationReceiptRequestModel duplicateReconciliationReceiptRequestModel =
                new DuplicateReconciliationReceiptRequestModel(reconciliationRepository, printerService);
        DuplicateReconciliationReceiptUseCase duplicateReconciliationReceiptUseCase = new
                DuplicateReconciliationReceiptUseCase();
        DuplicateReconciliationReceiptResponseModel duplicateReconciliationReceiptResponseModel =
                duplicateReconciliationReceiptUseCase.execute(duplicateReconciliationReceiptRequestModel);

        assertNotNull(duplicateReconciliationReceiptResponseModel);
    }
}
