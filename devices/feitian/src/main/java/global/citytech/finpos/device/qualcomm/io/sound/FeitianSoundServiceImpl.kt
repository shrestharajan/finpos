package global.citytech.finpos.device.qualcomm.io.sound

import android.content.Context
import android.media.MediaPlayer
import global.citytech.finpos.device.qualcomm.R
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finposframework.hardware.io.sound.Sound
import global.citytech.finposframework.hardware.io.sound.SoundService

/**
 * Created by Rishav Chudal on 4/15/21.
 */
class FeitianSoundServiceImpl(val context: Context): SoundService, FeitianLogger.Listener {

    private val logger = FeitianLogger(this)

    override fun playSound(sound: Sound?) {
        if (sound != null) {
            when (sound) {
                Sound.SUCCESS -> playSuccessSound()
                Sound.FAILURE -> playFailureSound()
            }
        }
    }

    private fun playSuccessSound() {
        this.logger.logWithClassName("Playing Success Sound...")
        val mediaPlayer = MediaPlayer.create(
            this.context.applicationContext,
            R.raw.success
        )
        mediaPlayer.start()
        mediaPlayer.setOnCompletionListener { mediaPlayer.release() }
    }

    private fun playFailureSound() {
        this.logger.logWithClassName("Playing Failure Sound...")
        val mediaPlayer = MediaPlayer.create(
            this.context.applicationContext,
            R.raw.failed
        )
        mediaPlayer.start()
        mediaPlayer.setOnCompletionListener { mediaPlayer.release() }
    }

    override fun getClassName() = FeitianSoundServiceImpl::class.java.simpleName
}