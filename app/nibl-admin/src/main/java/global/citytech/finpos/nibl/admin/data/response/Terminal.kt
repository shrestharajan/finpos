package global.citytech.finpos.nibl.admin.data.response

data class Terminal(
    var switchId: String? = null
)