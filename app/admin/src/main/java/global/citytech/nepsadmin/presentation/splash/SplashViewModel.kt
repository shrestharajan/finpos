package global.citytech.nepsadmin.presentation.splash

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.finpos.core.api.DeviceServiceType
import global.citytech.finpos.core.api.common.DeviceResponse
import global.citytech.finpos.core.api.common.DeviceResult
import global.citytech.finpos.core.api.init.InitializeSdkService
import global.citytech.nepsadmin.service.AppCoreFactory
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SplashViewModel(val context: Application) :BaseAndroidViewModel(context){

    val sdkInitialized by lazy{MutableLiveData<Boolean>()}
    val errorMessage by lazy{MutableLiveData<String>()}

    fun initSdk(){
        val observable = Observable.fromCallable {
            val initializeSdkService = AppCoreFactory.getCore(context, DeviceServiceType.INIT) as InitializeSdkService
            initializeSdkService.initializeSdk()

        }
        compositeDisposable.add(observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::onNext,this::onError)
        )
    }

    private fun onNext(coreResponse: DeviceResponse){
        this.sdkInitialized.value = coreResponse.result == DeviceResult.SUCCESS
    }

    private fun onError(throwable: Throwable){
        this.errorMessage.value = throwable.message
    }

}