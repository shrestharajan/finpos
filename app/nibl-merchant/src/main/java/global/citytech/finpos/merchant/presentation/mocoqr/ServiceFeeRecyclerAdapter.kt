package global.citytech.finpos.merchant.presentation.mocoqr

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import global.citytech.finpos.merchant.R

class ServiceFeeRecyclerAdapter(
    private val context: Context,
    private val serviceFeeItems: List<ServiceFeeItem>
) :
    RecyclerView.Adapter<ServiceFeeRecyclerAdapter.ServiceFeeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceFeeViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_service_fee, parent, false)
        return ServiceFeeViewHolder(view)
    }

    override fun onBindViewHolder(holder: ServiceFeeViewHolder, position: Int) {
        val tvPaymentNetwork = holder.itemView.findViewById<AppCompatTextView>(R.id.tv_payment_network)
        val tvDomesticRate = holder.itemView.findViewById<AppCompatTextView>(R.id.tv_domestic_rate)
        val tvInternationalRate = holder.itemView.findViewById<AppCompatTextView>(R.id.tv_international_rate)
        tvPaymentNetwork.text = serviceFeeItems[position].paymentNetwork
        tvDomesticRate.text = serviceFeeItems[position].domesticRate
        tvInternationalRate.text = serviceFeeItems[position].internationalRate
        if (position == 0) {
            tvPaymentNetwork.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
            tvDomesticRate.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
            tvInternationalRate.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
            tvPaymentNetwork.setTextColor(ContextCompat.getColor(context, R.color.white))
            tvDomesticRate.setTextColor(ContextCompat.getColor(context, R.color.white))
            tvInternationalRate.setTextColor(ContextCompat.getColor(context, R.color.white))
        }
    }

    override fun getItemCount(): Int = serviceFeeItems.size

    class ServiceFeeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
