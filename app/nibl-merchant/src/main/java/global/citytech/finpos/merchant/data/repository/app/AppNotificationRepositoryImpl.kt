package global.citytech.finpos.merchant.data.repository.app

import global.citytech.finpos.merchant.data.datasource.app.alert.AppNotificationDataSource
import global.citytech.finpos.merchant.domain.model.app.Action
import global.citytech.finpos.merchant.domain.model.app.AppNotification
import global.citytech.finpos.merchant.domain.repository.app.AppNotificationRepository

/**
 * Created by Unique Shakya on 5/28/2021.
 */
class AppNotificationRepositoryImpl(private val appNotificationDataSource: AppNotificationDataSource): AppNotificationRepository {
    override fun noNotifications(): Boolean {
        return appNotificationDataSource.noNotifications()
    }

    override fun getNotificationCount(): Long {
        return appNotificationDataSource.getNotificationCount()
    }

    override fun getAllNotifications(): List<AppNotification> {
        return appNotificationDataSource.getAllNotifications()
    }

    override fun removeNotification(appNotification: AppNotification) {
        appNotificationDataSource.removeNotification(appNotification)
    }

    override fun removeNotificationByAction(action: Action) {
        appNotificationDataSource.removeNotificationByAction(action)
    }

    override fun addNotification(appNotification: AppNotification) {
        appNotificationDataSource.addNotification(appNotification)
    }

    override fun removeNotificationByReferenceId(referenceId: String) {
        appNotificationDataSource.removeNotificationByReferenceId(referenceId)
    }

    override fun clearSharedPreference() {
        appNotificationDataSource.clearSharedPreference()
    }
}