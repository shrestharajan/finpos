package global.citytech.finposframework.iso8583;

import java.util.Objects;

public class DataElementConfig {
    private int index;
    private DataConfig dataConfig;
    private LengthConfig lengthConfig;
    private PaddingType paddingType;

    public DataElementConfig(int index, DataConfig dataConfig, LengthConfig lengthConfig) {
        this.index = index;
        this.dataConfig = dataConfig;
        this.lengthConfig = lengthConfig;
        this.paddingType = PaddingType.NONE;
    }

    public DataElementConfig(int index, DataConfig dataConfig, LengthConfig lengthConfig, PaddingType paddingType) {
        this.index = index;
        this.dataConfig = dataConfig;
        this.lengthConfig = lengthConfig;
        this.paddingType = paddingType;
    }

    public int getIndex() {
        return index;
    }

    public DataConfig getDataConfig() {
        return dataConfig;
    }

    public LengthConfig getLengthConfig() {
        return lengthConfig;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataElementConfig that = (DataElementConfig) o;
        return index == that.index;
    }

    public PaddingType getPaddingType() {
        return paddingType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(index);
    }

}
