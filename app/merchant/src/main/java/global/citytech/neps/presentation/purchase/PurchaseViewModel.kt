package global.citytech.neps.presentation.purchase

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.neps.R
import global.citytech.neps.data.repository.core.transaction.TransactionRepositoryImpl
import global.citytech.neps.data.repository.local.LocalRepositoryImpl
import global.citytech.neps.datasource.core.transaction.TransactionDataSourceImpl
import global.citytech.neps.datasource.local.LocalConfigurationDataSourceImpl
import global.citytech.neps.domain.usecase.core.transaction.CoreTransactionUseCase
import global.citytech.neps.domain.usecase.local.LocalDataUseCase
import global.citytech.neps.presentation.alertdialogs.TransactionConfirmation
import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.neps.presentation.model.mapToPresentation
import global.citytech.neps.presentation.model.transaction.PurchaseRequestItem
import global.citytech.posswitchintregator.card.data.PurchaseType
import global.citytech.posswitchintregator.card.data.Result
import global.citytech.posswitchintregator.neps.usecases.purchase.PurchaseResponseModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PurchaseViewModel(context: Application) : BaseAndroidViewModel(context) {

    val configurationItem by lazy { MutableLiveData<ConfigurationItem>() }
    val isManualButtonClickedInDialog by lazy { MutableLiveData<Boolean>() }
    val isCancelButtonClickedInDialog by lazy { MutableLiveData<Boolean>() }
    val transactionConfirmationDialog by lazy { MutableLiveData<TransactionConfirmation>() }

    var transactionUseCase: CoreTransactionUseCase =
        CoreTransactionUseCase(
            TransactionRepositoryImpl(TransactionDataSourceImpl())
        )

    var localDataUseCase: LocalDataUseCase =
        LocalDataUseCase(
            LocalRepositoryImpl(LocalConfigurationDataSourceImpl())
        )

    fun getConfigurationItem() {
        compositeDisposable.add(localDataUseCase.getWholeConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.mapToPresentation() }
            .subscribe({ configurationItem.value = it }, { message.value = it.message })
        )
    }

    fun purchase(configurationItem: ConfigurationItem, amount: Double, purchaseType: PurchaseType) {
        isLoading.value = true
        compositeDisposable.add(
            transactionUseCase.purchase(
                configurationItem, preparePurchaseRequest(
                    amount,
                    purchaseType
                )
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    handleResponse(it)
                }, {
                    it.printStackTrace()
                    isLoading.value = false
                    message.value = it.message
                })
        )
    }

    private fun handleResponse(purchaseResponseModel: PurchaseResponseModel) {
        if (purchaseResponseModel.result == Result.SUCCESS)
            showTransactionConfirmationDialog(purchaseResponseModel)
        else
            showTransactionExceptionDialog(purchaseResponseModel)
    }

    private fun showTransactionExceptionDialog(purchaseResponseModel: PurchaseResponseModel) {
        message.value = purchaseResponseModel.message
    }

    private fun retrieveMessage(purchaseResponseModel: PurchaseResponseModel): String {
        val stringBuilder = StringBuilder()
        if (purchaseResponseModel.isApproved) {
            stringBuilder.append("APPROVAL CODE")
            stringBuilder.append("\n")
            stringBuilder.append(purchaseResponseModel.approvalCode)
        } else {
            stringBuilder.append(purchaseResponseModel.message)
        }
        return stringBuilder.toString()
    }

    private fun retrieveTitle(approved: Boolean): String {
        return if (approved)
            "APPROVED"
        else
            "DECLINED"
    }

    private fun retrieveImageId(approved: Boolean): Int {
        return if (approved)
            R.drawable.ic_check_circle_green_64dp
        else
            R.drawable.ic_failure_red_64dp
    }

    private fun showTransactionConfirmationDialog(purchaseResponseModel: PurchaseResponseModel) {
        val transactionConfirmation = TransactionConfirmation.Builder()
            .imageId(retrieveImageId(purchaseResponseModel.isApproved))
            .title(retrieveTitle(purchaseResponseModel.isApproved))
            .message(retrieveMessage(purchaseResponseModel))
            .positiveLabel("PRINT")
            .negativeLabel("CANCEL")
            .qrImageString("")
            .transactionStatus("")
            .build()
        transactionConfirmationDialog.postValue(transactionConfirmation)
    }

    private fun preparePurchaseRequest(
        amount: Double,
        purchaseType: PurchaseType
    ): PurchaseRequestItem {
        return PurchaseRequestItem(amount, purchaseType)
    }

}