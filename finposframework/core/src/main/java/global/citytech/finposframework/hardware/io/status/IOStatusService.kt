package global.citytech.finposframework.hardware.io.status

import global.citytech.finposframework.hardware.common.DeviceService

/**
 * Created by Rishav Chudal on 6/4/20.
 */
interface IOStatusService: DeviceService {
    fun retrieveHardwareStatus(request: IOStatusRequest): IOStatusResponse
}