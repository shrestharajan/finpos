package global.citytech.finposframework.usecases.transaction.receipt;

public class Performance {

    private String startDateTime;
    private String endDateTime;

    public String getStartDateTime() {
        return startDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public static final class Builder {
        private String startDateTime;
        private String endDateTime;

        public Builder() {
        }

        public static Builder defaultBuilder() {
            return new Builder();
        }

        public Builder withStartDateTime(String startDateTime) {
            this.startDateTime = startDateTime;
            return this;
        }

        public Builder withEndDateTime(String endDateTime) {
            this.endDateTime = endDateTime;
            return this;
        }

        public Performance build() {
            Performance performance = new Performance();
            performance.endDateTime = this.endDateTime;
            performance.startDateTime = this.startDateTime;
            return performance;
        }
    }
}
