package global.citytech.finpos.device.weipass.card.read

import android.content.Context
import com.wiseasy.emvprocess.TransProcess
import com.wiseasy.emvprocess.bean.TrackData
import global.citytech.finpos.device.weipass.card.handler.WeipassStateHandler
import global.citytech.finpos.device.weipass.detect.DetectCardResponse
import global.citytech.finpos.device.weipass.detect.WeipassDetectCardServiceImpl
import global.citytech.finpos.device.weipass.utils.*
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest
import io.mockk.*
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.lang.ref.WeakReference

/**
 * Created by Rishav Chudal on 6/16/20.
 */
class WeipassReadCardServiceImplTest {
    private lateinit var SUT: WeipassReadCardServiceImpl
    private lateinit var mockkedContext: Context
    private lateinit var contextWeakReference: WeakReference<Context>
    private lateinit var mockkedWeipassDetectCardServiceImpl: WeipassDetectCardServiceImpl
    private lateinit var mockkedTrackData: TrackData

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        mockkContext()
        SUT = WeipassReadCardServiceImpl(mockkedContext)
        mockkWeipassStateHandler()
        mockkWeipassDetectCardServiceImpl()
        mockkWeipassLogger()
        mockkMagneticCardUtil()
        mockkTransProcess()
        mockkTrackData()
        mockkEmvUtils()
    }

    /**
     * func readCardDetails
     */
    @Test
    fun readCardDetails_OnMagCardDetected_ReturnsReadCardResponseWithResultSuccess() {
        //Setup
        contextWeakReference = WeakReference(mockkedContext)

        val slotsToOpen = listOf(
            CardType.MAG,
            CardType.ICC,
            CardType.PICC)

        val readCardRequest =
            ReadCardRequest(
                slotsToOpen,
                "00",
                prepareEmvParametersRequest()
            )
        readCardRequest.packageName = "test"

        every { WeipassStateHandler.clearPropertyInstances() } just Runs

        every { WeipassStateHandler.contextWeakReference } returns contextWeakReference

        val detectCardResponse = DetectCardResponse(
            Result.SUCCESS, WeipassConstant.MAG_CARD_DETECTED,
            CardType.MAG, byteArrayOf(1,2))

        every {
            anyConstructed<WeipassDetectCardServiceImpl>().detectCard(any())
        } returns detectCardResponse

        every { WeipassStateHandler.CURRENT_CARD_TYPE } returns CardType.MAG

        every { TransProcess.getInstance().unpackTrackData(any()) } returns mockkedTrackData

        every { mockkedTrackData.track2 } returns "test_track_2"

        every { mockkedTrackData.track1 } returns "test_track_1"

        every { MagneticCardUtil.getMagPan(any()) } returns "test_pan"
        every { MagneticCardUtil.getMagExpiryDate(any()) } returns "test_expiry_date"
        every { MagneticCardUtil.getCardHolderName(any()) } returns "test_cardholder_name"

        //Run
        val result = SUT.readCardDetails(readCardRequest)

        //Verify
        verify { WeipassStateHandler.clearPropertyInstances() }
        verify { WeipassStateHandler.transState = WeipassTransState.READ_CARD }
        verify { WeipassDetectCardServiceImpl(mockkedContext).detectCard(any()) }

        assertEquals("test_pan", result.cardDetails!!.primaryAccountNumber)
        assertEquals("test_expiry_date", result.cardDetails!!.expiryDate)
        assertEquals("test_cardholder_name", result.cardDetails!!.cardHolderName)

        assertEquals(Result.SUCCESS, result.result)

        assertEquals(WeipassConstant.MSG_MAG_CARD_READ_SUCCESS, result.message)
    }

    @Test
    fun readCardDetails_OnMagCardDetectedAndTrack2DataIsNull_ReturnsReadCardResponseWithResultFailure() {
        //Setup
        contextWeakReference = WeakReference(mockkedContext)

        val slotsToOpen = listOf(
            CardType.MAG,
            CardType.ICC,
            CardType.PICC)

        val readCardRequest =
            ReadCardRequest(
                slotsToOpen,
                "00",
                prepareEmvParametersRequest()
            )
        readCardRequest.packageName = "test"

        every { WeipassStateHandler.clearPropertyInstances() } just Runs

        every { WeipassStateHandler.contextWeakReference } returns contextWeakReference

        val detectCardResponse = DetectCardResponse(
            Result.SUCCESS, WeipassConstant.MAG_CARD_DETECTED,
            CardType.MAG, byteArrayOf(1,2))

        every {
            anyConstructed<WeipassDetectCardServiceImpl>().detectCard(any())
        } returns detectCardResponse

        every { WeipassStateHandler.CURRENT_CARD_TYPE } returns CardType.MAG

        every { TransProcess.getInstance().unpackTrackData(any()) } returns mockkedTrackData

        every { mockkedTrackData.track2 } returns ""

        every { mockkedTrackData.track1 } returns "test_track_1"

        every { MagneticCardUtil.getMagPan(any()) } returns "test_pan"
        every { MagneticCardUtil.getMagExpiryDate(any()) } returns "test_expiry_date"
        every { MagneticCardUtil.getCardHolderName(any()) } returns "test_cardholder_name"

        //Run
        val result = SUT.readCardDetails(readCardRequest)

        //Verify
        verify { WeipassStateHandler.clearPropertyInstances() }
        verify { WeipassStateHandler.transState = WeipassTransState.READ_CARD }
        verify { WeipassDetectCardServiceImpl(mockkedContext).detectCard(any()) }

        assertEquals(Result.FAILURE, result.result)

        assertEquals(WeipassConstant.MSG_MAG_CARD_READ_FAIL, result.message)
    }

    @Test
    fun readCardDetails_OnDetectResponseTimeout_ReturnsReadCardResponseWithResultTimeout() {
        //Setup
        contextWeakReference = WeakReference(mockkedContext)

        val slotsToOpen = listOf(
            CardType.MAG,
            CardType.ICC,
            CardType.PICC)

        val readCardRequest =
            ReadCardRequest(
                slotsToOpen,
                "00",
                prepareEmvParametersRequest()
            )
        readCardRequest.packageName = "test"

        every { WeipassStateHandler.clearPropertyInstances() } just Runs

        every { WeipassStateHandler.contextWeakReference } returns contextWeakReference

        val detectCardResponse = DetectCardResponse(
            Result.TIMEOUT, WeipassConstant.DETECT_CARD_TIMEOUT,
            CardType.MANUAL,
            null)

        every {
            anyConstructed<WeipassDetectCardServiceImpl>().detectCard(any())
        } returns detectCardResponse

        every { WeipassStateHandler.CURRENT_CARD_TYPE } returns CardType.MANUAL

        //Run
        val result = SUT.readCardDetails(readCardRequest)

        //Verify
        verify { WeipassStateHandler.clearPropertyInstances() }
        verify { WeipassStateHandler.transState = WeipassTransState.READ_CARD }
        verify { WeipassDetectCardServiceImpl(mockkedContext).detectCard(any()) }

        assertEquals(Result.TIMEOUT, result.result)

        assertEquals(WeipassConstant.DETECT_CARD_TIMEOUT, result.message)
    }


    /**
     * Methods for mockking classes
     */

    private fun prepareEmvParametersRequest() =
        EmvParametersRequest(
            "0682",
            "0682",
            "01",
            "E0F0C8",
            "22",
            "7399",
            "06804344",
            "800150400566",
            "Bhat Bhateni Super Market",
            "35E04000",
            "D000F0A000",
            "01"
        )

    private fun mockkWeipassStateHandler() {
        mockkObject(WeipassStateHandler)
    }

    private fun mockkContext() {
        mockkedContext = mockk()
    }

    private fun mockkWeipassDetectCardServiceImpl() {
        mockkConstructor(WeipassDetectCardServiceImpl::class)
        mockkedWeipassDetectCardServiceImpl = mockk()
    }

    private fun mockkWeipassLogger() {
        mockkObject(WeipassLogger)
        mockkObject(WeipassLogger.Companion)
        every { WeipassLogger.log(any()) } just Runs
    }

    private fun mockkMagneticCardUtil() {
        mockkObject(MagneticCardUtil)
        mockkObject(MagneticCardUtil.Companion)
    }

    private fun mockkTransProcess() {
        mockkStatic(TransProcess::class)
    }

    private fun mockkTrackData() {
        mockkedTrackData = mockk()
    }

    private fun mockkEmvUtils() {
        mockkObject(EmvUtils)
        mockkObject(EmvUtils.Companion)
    }
}