package global.citytech.finposframework.usecases.transaction.receipt.transaction;

import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Unique Shakya
 */

public class ReceiptLog {

    private Retailer retailer;
    private Performance performance;
    private TransactionInfo transactionInfo;
    private EmvTags emvTags;
    private String barCodeString;

    public ReceiptLog() {
    }

    public Retailer getRetailer() {
        return retailer;
    }

    public Performance getPerformance() {
        return performance;
    }

    public TransactionInfo getTransactionInfo() {
        return transactionInfo;
    }

    public EmvTags getEmvTags() {
        return emvTags;
    }

    public String getBarCodeString() {
        return barCodeString;
    }

    public static final class Builder {
        private Retailer retailer;
        private Performance performance;
        private TransactionInfo transactionInfo;
        private EmvTags emvTags;
        private String barCodeString;

        public Builder() {
        }

        public static Builder defaultBuilder() {
            return new Builder();
        }

        public Builder withRetailer(Retailer retailer) {
            this.retailer = retailer;
            return this;
        }

        public Builder withPerformance(Performance performance) {
            this.performance = performance;
            return this;
        }

        public Builder withTransaction(TransactionInfo transactionInfo) {
            this.transactionInfo = transactionInfo;
            return this;
        }

        public Builder withEmvTags(EmvTags emvTags) {
            this.emvTags = emvTags;
            return this;
        }

        public Builder withBarCodeString(String barCodeString) {
            this.barCodeString = barCodeString;
            return this;
        }

        public ReceiptLog build() {
            ReceiptLog receiptLog = new ReceiptLog();
            receiptLog.retailer = this.retailer;
            receiptLog.transactionInfo = this.transactionInfo;
            receiptLog.performance = this.performance;
            receiptLog.emvTags = this.emvTags;
            receiptLog.barCodeString = this.barCodeString;
            return receiptLog;
        }
    }

    public String getDate() {
        String dateTime = this.getPerformance().getStartDateTime();
        if (StringUtils.isEmpty(dateTime) || dateTime.length() < 15)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("20");
        stringBuilder.append(dateTime, 0, 2);
        stringBuilder.append("/");
        stringBuilder.append(dateTime, 2, 4);
        stringBuilder.append("/");
        stringBuilder.append(dateTime, 4, 6);
        return stringBuilder.toString();
    }

    public String getTime() {
        String dateTime = this.performance.getStartDateTime();
        if (StringUtils.isEmpty(dateTime) || dateTime.length() < 15)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(dateTime, 6, 8);
        stringBuilder.append(":");
        stringBuilder.append(dateTime, 8, 10);
        stringBuilder.append(":");
        stringBuilder.append(dateTime, 10, 12);
        return stringBuilder.toString();
    }

    public String getAmountWithCurrency() {
        return this.transactionInfo.getTransactionCurrencyName().concat(" ").concat(this.transactionInfo.getPurchaseAmount());
    }
}


