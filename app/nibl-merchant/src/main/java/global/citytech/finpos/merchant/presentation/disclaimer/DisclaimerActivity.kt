package global.citytech.finpos.merchant.presentation.disclaimer

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityDisclaimerBinding
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.utils.MessageConfig
import kotlinx.android.synthetic.main.activity_disclaimer.*

class DisclaimerActivity : AppBaseActivity<ActivityDisclaimerBinding, DisclaimerViewModel>() {

    companion object {
        const val REQUEST_CODE = 3473
        const val EXTRA_DISCLAIMER_ID = "extra.disclaimer.id"

        fun getLaunchIntent(context: Context, disclaimer: String): Intent {
            val intent = Intent(context, DisclaimerActivity::class.java)
            intent.putExtra(EXTRA_DISCLAIMER_ID, disclaimer)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val disclaimerId = intent.getStringExtra(EXTRA_DISCLAIMER_ID)
        getViewModel().bindService()
        if (!disclaimerId?.isNullOrEmptyOrBlank()!!) {
            initObservers()
            initViews()
            handleClickEvents()
            getViewModel().retrieveDisclaimer(disclaimerId)
        } else {
            showFailureMessage(MessageConfig.Builder()
                .message(getString(R.string.error_generic_msg))
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    finish()
                })
        }
    }

    private fun handleClickEvents() {
        btn_confirm.setOnClickListener {
            if (wv_disclaimer_content.visibility == View.VISIBLE) {
                getViewModel().acceptDisclaimer()
            } else {
                wv_disclaimer_info.visibility = View.GONE
                wv_disclaimer_content.visibility = View.VISIBLE
                tv_title.text = getString(R.string.disclaimer)
            }
        }

        btn_later.setOnClickListener {
            getViewModel().acceptLater()
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        btn_cancel.setOnClickListener {
            showConfirmationMessage(
                MessageConfig.Builder()
                    .message(getString(R.string.msg_confirmation_cancel_disclaimer))
                    .positiveLabel(getString(R.string.action_yes))
                    .onPositiveClick {
                        getViewModel().declineDisclaimer()
                    }
                    .negativeLabel(getString(R.string.action_no))
                    .onNegativeClick {
                    })

        }
    }

    private fun initViews() {
        wv_disclaimer_info.setOnLongClickListener { true }
        wv_disclaimer_info.settings.javaScriptEnabled = false
        wv_disclaimer_info.isLongClickable = false
        wv_disclaimer_content.setOnLongClickListener { true }
        wv_disclaimer_content.settings.javaScriptEnabled = false
        wv_disclaimer_content.isLongClickable = false
    }

    private fun initObservers() {
        getViewModel().message.observe(this, Observer {
            it?.let {
                wv_disclaimer_content.visibility = View.INVISIBLE
                ll_actions.visibility = View.INVISIBLE
                showSuccessMessage(
                    MessageConfig.Builder()
                        .message(it)
                        .positiveLabel(getString(R.string.title_ok))
                        .onPositiveClick {
                            finish()
                        })
            }
        })

        getViewModel().isLoading.observe(this, Observer {
            if (it)
                showProgress()
            else
                hideProgress()
        })

        getViewModel().closeActivity.observe(this, Observer {
            finish()
        })

        getViewModel().disclaimer.observe(this, Observer {
            tv_title.text = it.serviceName
            wv_disclaimer_content.loadData(
                it!!.disclaimerContent,
                "text/html",
                "utf-8"
            )

            wv_disclaimer_info.loadData(
                it.disclaimerInfo,
                "text/html",
                "utf-8"
            )

            if (it.disclaimerInfo.isNullOrEmptyOrBlank()) {
                wv_disclaimer_info.visibility = View.GONE
                wv_disclaimer_content.visibility = View.VISIBLE
            } else {
                wv_disclaimer_info.visibility = View.VISIBLE
                wv_disclaimer_content.visibility = View.GONE
            }
        })

        getViewModel().errorMessage.observe(this, Observer {
            showFailureMessage(MessageConfig.Builder()
                .message(it)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    finish()
                })
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(Activity.RESULT_CANCELED)
    }

    override fun getBindingVariable(): Int = BR.disclaimerViewModel

    override fun getLayout(): Int = R.layout.activity_disclaimer

    override fun getViewModel(): DisclaimerViewModel =
        ViewModelProviders.of(this)[DisclaimerViewModel::class.java]
}