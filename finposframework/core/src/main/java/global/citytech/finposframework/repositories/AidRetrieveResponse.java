package global.citytech.finposframework.repositories;

public class AidRetrieveResponse {

    private String data;

    public AidRetrieveResponse(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }
}
