package global.citytech.finposframework.utility;

import global.citytech.finposframework.hardware.io.cards.cvm.Cvm;
import global.citytech.finposframework.hardware.io.cards.cvm.CvmCondition;
import global.citytech.finposframework.hardware.io.cards.cvm.CvmResult;
import global.citytech.finposframework.hardware.io.cards.cvm.CvmResults;
import global.citytech.finposframework.log.Logger;

public class CvmResultsParser {

    public static CvmResults retrieveCvmResults(String s) { // for tag 9F34
        if (s.length() != 6)
            throw new IllegalArgumentException("Invalid CVM");
        StringBuilder sb = new StringBuilder();
        int io = 0;
        sb.append(io + ":" + s + " ");
        Cvm cvm = retrieveCvm(sb, s);
        CvmCondition cvmCondition = retrieveCvmCondition(sb, s);
        CvmResult cvmResult = retrieveCvmResult(sb, s);
        Logger.getLogger(CvmResultsParser.class.getName()).debug(sb.toString());
        return new CvmResults(cvm, cvmCondition, cvmResult);
    }

    private static CvmResult retrieveCvmResult(StringBuilder sb, String sub) {
        CvmResult cvmResult;
        String result = sub.substring(4, 6);
        if ("00".equals(result)) {
            cvmResult = CvmResult.UNKNOWN;
            sb.append("[Unknown]");
        } else if ("01".equals(result)) {
            cvmResult = CvmResult.FAILED;
            sb.append("[Failed]");
        } else if ("02".equals(result)) {
            cvmResult = CvmResult.SUCCESSFUL;
            sb.append("[Successful]");
        } else {
            cvmResult = CvmResult.UNKNOWN;
        }
        return cvmResult;
    }

    private static CvmCondition retrieveCvmCondition(StringBuilder sb, String sub) {
        CvmCondition cvmCondition;
        String condition = sub.substring(2, 4);
        if ("00".equals(condition)) {
            cvmCondition = CvmCondition.ALWAYS;
            sb.append("[Always]");
        } else if ("01".equals(condition)) {
            cvmCondition = CvmCondition.IF_UNATTENDED_CASH;
            sb.append("[If unattended cash]");
        } else if ("02".equals(condition)) {
            cvmCondition = CvmCondition.IF_NOT_UNATTENDED_NOT_MANUAL_NOT_CASHBACK;
            sb.append("[If not unattended cash and not manual cash and not purchase with cashback]");
        } else if ("03".equals(condition)) {
            cvmCondition = CvmCondition.IF_TERMINAL_SUPPORTS_CVM;
            sb.append("[If terminal supports the Cvm]");
        } else if ("04".equals(condition)) {
            cvmCondition = CvmCondition.IF_MANUAL_CASH;
            sb.append("[If manual cash]");
        } else if ("05".equals(condition)) {
            cvmCondition = CvmCondition.IF_CASHBACK;
            sb.append("[If purchase with cashback]");
        } else if ("06".equals(condition)) {
            cvmCondition = CvmCondition.IF_UNDER_X;
            sb.append("[If transaction is in the application currency 21 and is under X value (see section 10.5 for a discussion of \"X\")]");
        } else if ("07".equals(condition)) {
            cvmCondition = CvmCondition.IF_OVER_X;
            sb.append("[If transaction is in the application currency and is over X value]");
        } else if ("08".equals(condition)) {
            cvmCondition = CvmCondition.IF_UNDER_Y;
            sb.append("[If transaction is in the application currency and is under Y value ]");
        } else if ("09".equals(condition)) {
            cvmCondition = CvmCondition.IF_OVER_Y;
            sb.append("[If transaction is in the application currency and is over Y value]");
        } else if (condition.compareTo("0A") >= 0 && condition.compareTo("7F") <= 0) {
            cvmCondition = CvmCondition.RFU;
            sb.append("[RFU]");
        } else if (condition.compareTo("80") >= 0 && condition.compareTo("FF") <= 0) {
            cvmCondition = CvmCondition.RFU_PAYMENT_SYSTEMS;
            sb.append("[Reserved for various payment systems]");
        } else {
            cvmCondition = CvmCondition.UNKNOWN;
            sb.append("[Unknown]");
        }
        sb.append("\n");
        return cvmCondition;
    }

    private static Cvm retrieveCvm(StringBuilder sb, String sub) {
        Cvm cvm;
        byte[] bs = HEX.hexToBytes(sub);
        if ((bs[0] & (1 << 7)) != 0) {
            sb.append("[Apply succeeding CV Rule if this Cvm is unsuccessful]");
        } else {
            sb.append("[Fail cardholder verification if this Cvm is unsuccessful]");
        }
        String type = Integer.toBinaryString((bs[0] & 0xFF) | (0xC0));
        type = type.substring(2);
        if ("000000".equals(type)) {
            cvm = Cvm.FAILED_CVM_PROCESSING;
            sb.append("[Fail Cvm processing]");
        } else if ("000001".equals(type)) {
            cvm = Cvm.PLAIN_TEXT_PIN_ICC;
            sb.append("[Plaintext PIN verification performed by ICC]");
        } else if ("000010".equals(type)) {
            cvm = Cvm.ENCIPHERED_PIN_ONLINE;
            sb.append("[Enciphered PIN verified online]");
        } else if ("000011".equals(type)) {
            cvm = Cvm.PLAIN_TEXT_PIN_ICC_SIGNATURE;
            sb.append("[Plaintext PIN verification performed by ICC and signature (paper)]");
        } else if ("000100".equals(type)) {
            cvm = Cvm.ENCIPHERED_PIN_ICC;
            sb.append("[Enciphered PIN verification performed by ICC]");
        } else if ("000101".equals(type)) {
            cvm = Cvm.ENCIPHERED_PIN_ICC_SIGNATURE;
            sb.append("[Enciphered PIN verification performed by ICC and signature (paper)]");
        } else if ("011110".equals(type)) {
            cvm = Cvm.SIGNATURE;
            sb.append("[Signature (paper)]");
        } else if ("011111".equals(type)) {
            cvm = Cvm.NO_CVM;
            sb.append("[No Cvm required]");
        } else if (type.compareTo("000110") >= 0 && type.compareTo("011101") <= 0) {
            cvm = Cvm.RFU_EMV;
            sb.append("[Values in the range 000110-011101 reserved for future use by this specification]");
        } else if (type.compareTo("100000") >= 0 && type.compareTo("101111") <= 0) {
            cvm = Cvm.RFU_PAYMENT_SYSTEMS;
            sb.append("[Values in the range 100000-101111 reserved for use by the individual payment systems]");
        } else if (type.compareTo("110000") >= 0 && type.compareTo("111110") <= 0) {
            cvm = Cvm.RFU_ISSUER;
            sb.append("[Values in the range 110000-111110 reserved for use by the issuer ]");
        } else if ("111111".equals(type)) {
            cvm = Cvm.NOT_USE;
            sb.append("[RFU]");
        } else if ("100000".equals(type)) {
            cvm = Cvm.FAILED_CVM_PROCESSING;
            sb.append("[Present cardholder ID]");
        } else {
            cvm = Cvm.UNKNOWN;
            sb.append("[Unknown]");
        }
        return cvm;
    }
}