package global.citytech.finpos.merchant.presentation.tipadjustment

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.common.extensions.isAlphaNumeric
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.data.repository.core.tipadjustment.TipAdjustmentRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.tipadjustment.TipAdjustmentDataSourceImpl
import global.citytech.finpos.merchant.domain.model.app.TransactionLog
import global.citytech.finpos.merchant.domain.model.tip.TipAdjustmentResponseEntity
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.core.tipadjustment.CoreTipAdjustmentUseCase
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.TransactionItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.presentation.model.tipadjustment.TipAdjustmentRequestItem
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finpos.processor.nibl.transaction.tipadjustment.TipAdjustmentResponseModel
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.utility.StringUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Created by Unique Shakya on 5/5/2021.
 */
class TipAdjustmentViewModel(val instance: Application) : BaseTransactionViewModel(instance) {

    val invalidInvoiceNumber by lazy { MutableLiveData<Boolean>() }
    val transactionFound by lazy { MutableLiveData<Boolean>() }
    val tipAmountLimit by lazy { MutableLiveData<BigDecimal>() }
    val transactionItem by lazy { MutableLiveData<TransactionItem>() }
    val transactionAlreadyVoided by lazy { MutableLiveData<Boolean>() }
    val tipAlreadyAdjusted by lazy { MutableLiveData<Boolean>() }

    private var invoiceNumber: String? = null
    private var originalAmount = BigDecimal.ZERO

    private val localDataUseCase =
        LocalDataUseCase(LocalRepositoryImpl(LocalDatabaseSourceImpl(), PreferenceManager))

    private val coreTipAdjustmentUseCase =
        CoreTipAdjustmentUseCase(TipAdjustmentRepositoryImpl(TipAdjustmentDataSourceImpl()))

    fun validateInvoiceNumber(invoiceNumber: String?) {
        if (invoiceNumber.isNullOrEmptyOrBlank() ||
            !invoiceNumber!!.isAlphaNumeric()
        ) {
            isLoading.value = false
            invalidInvoiceNumber.value = true
        } else {
            checkForTransactionPresent(StringUtils.ofRequiredLength(invoiceNumber, 6))
        }
    }

    fun performTipAdjustment(
        configurationItem: ConfigurationItem,
        tipAmount: BigDecimal,
        transactionType: TransactionType
    ) {
        isLoading.value = true
        compositeDisposable.add(
            coreTipAdjustmentUseCase.performTipAdjustment(
                configurationItem,
                TipAdjustmentRequestItem(
                    originalAmount,
                    tipAmount,
                    transactionType,
                    invoiceNumber!!
                )
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onTipAdjustmentNext(it!!)
                }, {
                    onTipAdjustmentError(it!!)
                })
        )
    }

    private fun onTipAdjustmentError(it: Throwable) {
        it.printStackTrace()
        isLoading.value = false
        transactionComplete.value = false
        message.value = it.message
    }

    private fun onTipAdjustmentNext(it: TipAdjustmentResponseEntity) {
        prepareBase64UrlToDisplayQr(
            it.approved,
            it.message,
            it.shouldPrintCustomerCopy,
            it.stan
        )
    }

    private fun showTransactionConfirmationDialog(it: TipAdjustmentResponseModel) {
        val transactionConfirmation = this.retrieveTransactionConfirmation(
            it.isApproved,
            it.message,
            it.isShouldPrintCustomerCopy
        )
        transactionConfirmationData.value = transactionConfirmation
    }

    private fun checkForTransactionPresent(invoiceNumber: String) {
        compositeDisposable.add(
            localDataUseCase.getTransactionByInvoiceNumber(invoiceNumber, false)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter {
                    validPurchaseTransactionForTipAdjustment(it)
                }
                .subscribe({
                    onCheckTransactionPresentNext(it, invoiceNumber)
                }, {
                    isLoading.value = false
                    this.transactionFound.value = false
                })
        )
    }

    private fun validPurchaseTransactionForTipAdjustment(
        transactionLog: TransactionLog
    ): Boolean {
        return if (isPurchaseTransaction(transactionLog)) {
            if (isTipAlreadyAdjusted(transactionLog))
                false
            else
                !isTransactionAlreadyVoided(transactionLog)
        } else {
            updateLiveDataIfNotPurchaseTransaction()
            false
        }
    }

    private fun isTipAlreadyAdjusted(transactionLog: TransactionLog): Boolean {
        var tipAdjusted: Boolean
        transactionLog.tipAdjusted!!.let {
            this.isLoading.value = !it
            this.tipAlreadyAdjusted.value = it
            tipAdjusted = it
        }
        return tipAdjusted
    }

    private fun updateLiveDataIfNotPurchaseTransaction() {
        this.isLoading.value = false
        this.transactionFound.value = false
    }

    private fun isPurchaseTransaction(
        transactionLog: TransactionLog
    ): Boolean {
        return transactionLog.transactionType.equals(
            Jsons.toJsonObj(TransactionType.PURCHASE),
            true
        )
    }

    private fun isTransactionAlreadyVoided(
        transactionLog: TransactionLog
    ): Boolean {
        var alreadyVoided: Boolean
        transactionLog.transactionVoided!!.let {
            this.isLoading.value = !it
            this.transactionAlreadyVoided.value = it
            alreadyVoided = it
        }
        return alreadyVoided
    }

    private fun onCheckTransactionPresentNext(it: TransactionLog?, invoiceNumber: String) {
        isLoading.value = false
        if (it != null) {
            this.invoiceNumber = invoiceNumber
            this.transactionItem.value = it.mapToPresentation()
        } else {
            isLoading.value = false
            this.transactionFound.value = false
        }
    }

    fun retrieveTipAmountLimit() {
        this.originalAmount = this.transactionItem.value!!.amount!!.toBigDecimal()
            .divide(BigDecimal.valueOf(100.00), 2, RoundingMode.CEILING)
        val tipPercentageLimit = localDataUseCase.retrieveTipLimit()
        if (tipPercentageLimit == 0)
            tipAmountLimit.value = this.originalAmount
        else
            tipAmountLimit.value = this.originalAmount.multiply(tipPercentageLimit.toBigDecimal()).divide(
                BigDecimal.valueOf(100.00), 2, RoundingMode.CEILING)
    }
}