package global.citytech.finpos.device.qualcomm.io.cards.detect

import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.detect.DetectCardRequest

/**
 * Created by Rishav Chudal on 4/7/21.
 */
class FeitianDetectCardRequest(
    slotsToOpen: List<CardType>,
    timeOutInSecs: Int
): DetectCardRequest(slotsToOpen, timeOutInSecs)