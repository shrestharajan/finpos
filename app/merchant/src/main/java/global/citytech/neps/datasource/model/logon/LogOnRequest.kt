package global.citytech.neps.datasource.model.logon

import global.citytech.posswitchintregator.api.logon.LogOnRequestParameter
import global.citytech.posswitchintregator.supports.UseCase

class LogOnRequest: UseCase.Request, LogOnRequestParameter{

}