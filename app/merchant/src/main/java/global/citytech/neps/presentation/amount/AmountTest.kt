package global.citytech.neps.presentation.amount

/**
 * Created by Rishav Chudal on 6/17/20.
 */
fun main() {
    onNumericButtonPressed("5", 8)
}

fun onNumericButtonPressed(buttonText: String, limit: Int) {
    var enteredAmount = 0L
    if (enteredAmount.toString().length < limit) {
        enteredAmount = (enteredAmount.toString().plus(buttonText)).toLong()
    }
    println("entered Amount ::: ".plus(enteredAmount))
}