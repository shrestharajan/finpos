package global.citytech.readinglog

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import global.citytech.finpos.merchant.utils.AppUtility.printLog


class BatteryStatusBroadcastReceiver : BroadcastReceiver() {
    val TAG = BatteryStatusBroadcastReceiver::class.java.name

    companion object {
        const val TAG = "BatteryStatusBroadcastReceiver"
    }

    private var batteryLevel: Int = 0
    private var previousBatteryLevel: Int = 0
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action.equals(Intent.ACTION_BATTERY_CHANGED)) {
            batteryLevel = intent.getIntExtra("level", 0)
            if (previousBatteryLevel != batteryLevel) {
                val batteryPercentage: Int = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
                printLog(TAG, "Battery Level in Device is  >>>> $batteryPercentage%")

                // Intent to check the actions on battery
                val batteryStatus: Intent? =
                    IntentFilter(Intent.ACTION_BATTERY_CHANGED).let { ifilter ->
                        context.registerReceiver(null, ifilter)
                    }

                // isCharging if true indicates charging is ongoing and vice-versa
                val status: Int = batteryStatus?.getIntExtra(BatteryManager.EXTRA_STATUS, -1) ?: -1
                val isCharging: Boolean = status == BatteryManager.BATTERY_STATUS_CHARGING
                        || status == BatteryManager.BATTERY_STATUS_FULL

                if (isCharging) {
                    printLog(TAG, " Device is in charging Mode")
                } else {
                    printLog(TAG, " Device is not charging")
                }

            }
            previousBatteryLevel = batteryLevel

        }
    }
}