package global.citytech.finpos.processor.neps.transaction;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finpos.neps.iso8583.SmartVistaIccDataList;
import global.citytech.finpos.processor.neps.NepsConstant;
import global.citytech.finpos.processor.neps.cardprocessor.ICCCardProcessor;
import global.citytech.finpos.processor.neps.cardprocessor.MagCardProcessor;
import global.citytech.finpos.processor.neps.cardprocessor.PICCCardProcessor;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedAction;
import global.citytech.finposframework.hardware.io.led.LedLight;
import global.citytech.finposframework.hardware.io.led.LedRequest;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.ChipCardProcessorInterface;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;

/**
 * Created by Saurav Ghimire on 2/2/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public abstract class CardTransactionUseCase extends TransactionExecutorTemplate {

    protected abstract TransactionType getTransactionType();


    @Override
    protected void retrieveCardResponse() {
        List<CardType> allowedCardTypes = TransactionUtils
                .getAllowedCardEntryModes(transactionRequest.getTransactionType());
        ReadCardRequest readCardRequest = this.prepareReadCardRequest(
                allowedCardTypes,
                request.getApplicationRepository(),
                transactionRequest
        );
        this.notifier.notify(Notifier.EventType.STARTING_READ_CARD);
        performTransactionWithCard(readCardRequest);
    }

    private void performTransactionWithCard(ReadCardRequest readCardRequest) {
        readCardResponse = this.detectCard(readCardRequest, request.getReadCardService(),
                request.getLedService());
        this.notifier.notify(Notifier.EventType.READING_CARD, "Reading Card..");
        System.out.println("::: PURCHASE USE CASE ::: CARD TYPE === " + readCardResponse.getCardDetails().getCardType());
        System.out.println("::: PURCHASE USE CASE ::: READ CARD RESPONSE === " + readCardResponse.toString());
        getRespectiveCardProcessor(readCardResponse.getCardDetails().getCardType(), request);
        if(this.cardProcessor == null && readCardResponse.getCardDetails().getCardType() == CardType.UNKNOWN){
                throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        readCardResponse = this.cardProcessor.validateReadCardResponse(readCardRequest, readCardResponse);
        System.out.println("::: CARD TYPE AFTER VALIDATE READ CARD RESPONSE === " + readCardResponse.getCardDetails().getCardType());
        System.out.println("::: FALLBACK TO ICC VALIDATE READ CARD RESPONSE === " + readCardResponse.getFallbackIccToMag());
        System.out.println("::: CARD SCHEME AFTER VALIDATE READ CARD RESPONSE ::: " + readCardResponse.getCardDetails().getCardScheme());
        this.checkForCardTypeChange(readCardResponse.getCardDetails().getCardType(), request);
    }

    @Override
    protected void processCard() {
        PurchaseRequest purchaseRequest =
                this.preparePurchaseRequest(transactionRequest, readCardResponse.getCardDetails(),
                        readCardResponse.getFallbackIccToMag());
        readCardResponse = this.cardProcessor.processCard(stan, purchaseRequest);
        logger.debug("::: FALLBACK TO ICC VALIDATE READ CARD RESPONSE === " + readCardResponse.getFallbackIccToMag());
        logger.debug("::: READ CARD RESPONSE ::: " + readCardResponse.toString());
    }

    private void checkForCardTypeChange(CardType cardType, TransactionRequest request) {
        getRespectiveCardProcessor(cardType, request);
    }

    @Override
    protected void updateTransactionLog(
            boolean transactionApproved,
            global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest,
            TransactionRepository transactionRepository,
            IsoMessageResponse isoMessageResponse,
            ReadCardResponse readCardResponse
    ) {
        System.out.println("::: NEPS ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
        System.out.println("::: NEPS ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
        String timeStamp = IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.LOCAL_TIME);
        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
                .withStan(stan)
                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
                .withInvoiceNumber(invoiceNumber)
                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
                .withTransactionType(getTransactionType())
                .withOriginalTransactionType(getTransactionType())
                .withTransactionAmount(transactionRequest.getAmount())
                .withOriginalTransactionAmount(transactionRequest.getAmount())
                .withTransactionDate(TransactionUtils.getDateOrDefaultFromTimeStamp(timeStamp))
                .withTransactionTime(TransactionUtils.getTimeOrDefaultFromTimeStamp(timeStamp))
                .withTransactionStatus(transactionApproved ? "APPROVED" : "DECLINED")
                .withPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
                .withOriginalPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
                .withPosConditionCode("00") //TODO
                .withOriginalPosConditionCode("00") //TODO
                .withReconcileStatus("") //TODO
                .withReconcileTime("") //TODO
                .withReconcileDate("") //TODO
                .withReconcileBatchNo(batchNumber)
                .withReadCardResponse(readCardResponse)
                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse,
                        applicationRepository,batchNumber,stan,invoiceNumber))
                .withAuthorizationCompleted(getAuthCompletionStatus())
                .withProcessingCode(getProcessingCode())
                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                .withOriginalTransactionReferenceNumber(transactionRequest.getOriginalRetrievalReferenceNumber())
                .withTransactionVoided(false)
                .withCurrencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .withVatInfo(transactionRequest.getVatInfo())
                .build();
        transactionRepository.updateTransactionLog(transactionLog);
    }

    protected abstract boolean getAuthCompletionStatus();


    @Override
    protected void updateTransactionIdsAndNotifyTransactionStatus(
            boolean transactionApproved,
            IsoMessageResponse isoMessageResponse,
            TransactionRequest request,
            ReadCardResponse readCardResponse
    ) {
        this.updateTransactionIds(transactionApproved);

        if (transactionApproved) {
            if (readCardResponse.getCardDetails().getCardType() == CardType.PICC)
                request.getLedService().doTurnLedWith(new LedRequest(LedLight.GREEN, LedAction.ON));
            this.notifier.notify(
                    Notifier.EventType.TRANSACTION_APPROVED,
                    this.approvedMessage(isoMessageResponse),
                    Notifier.TransactionType.CARD.name()
            );
        } else {
            if (readCardResponse.getCardDetails().getCardType() == CardType.PICC)
                request.getLedService().doTurnLedWith(new LedRequest(LedLight.RED, LedAction.ON));
            this.notifier.notify(
                    Notifier.EventType.TRANSACTION_DECLINED,
                    this.declinedMessage(
                            isoMessageResponse,
                            request.getApplicationRepository()
                    ),
                    Notifier.TransactionType.CARD.name()

            );
        }

    }

    private void getRespectiveCardProcessor(CardType cardType, TransactionRequest request) {
        switch (cardType) {
            case ICC:
                this.isCardConfirmationRequired = true;
                this.cardProcessor = new ICCCardProcessor(
                        request.getTransactionRepository(),
                        request.getTransactionAuthenticator(),
                        request.getApplicationRepository(),
                        request.getReadCardService(),
                        notifier,
                        terminalRepository
                );
                break;
            case MAG:
                this.isCardConfirmationRequired = true;
                this.cardProcessor = new MagCardProcessor(
                        request.getTransactionRepository(),
                        request.getTransactionAuthenticator(),
                        request.getApplicationRepository(),
                        request.getReadCardService(),
                        notifier,
                        terminalRepository
                );
                break;
            case PICC:
                this.isCardConfirmationRequired = false;
                this.cardProcessor = new PICCCardProcessor(
                        request.getTransactionRepository(),
                        request.getTransactionAuthenticator(),
                        request.getApplicationRepository(),
                        request.getReadCardService(),
                        notifier,
                        request.getLedService(),
                        request.getSoundService(),
                        terminalRepository
                );
                break;
            default:
                this.isCardConfirmationRequired = true;
                break;
        }
    }

    private ReadCardResponse detectCard(ReadCardRequest readCardRequest, ReadCardService readCardService, LedService ledService) {
        ledService.doTurnLedWith(new LedRequest(LedLight.BLUE, LedAction.ON));
        ReadCardResponse readCardResponse = readCardService.readCardDetails(readCardRequest);
        this.validateReadCardResponse(readCardResponse);
        return readCardResponse;
    }

    private void validateReadCardResponse(ReadCardResponse readCardResponse) {
        if (readCardResponse == null)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        if (readCardResponse.getResult() == Result.TIMEOUT)
            throw new PosException(PosError.DEVICE_ERROR_CARD_READ_TIMEOUT);
        if (readCardResponse.getResult() == Result.USER_CANCELLED ||
                readCardResponse.getResult() == Result.CANCEL)
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        if (readCardResponse.getResult() == Result.FAILURE)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
    }

    protected final ReadCardRequest.MultipleAidSelectionListener multipleAidSelectionListener = aids -> {
        notifier.notify(Notifier.EventType.DETECTED_MULTIPLE_AID, "Processing..");
        return -1;
    };

    protected CardTransactionUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    protected ReadCardRequest prepareReadCardRequest(
            List<CardType> allowedCardTypes,
            ApplicationRepository applicationRepository,
            global.citytech.finposframework.usecases.transaction.data.TransactionRequest
                    transactionRequest
    ) {
        ReadCardRequest readCardRequest = new ReadCardRequest(
                allowedCardTypes,
                transactionRequest.getTransactionType(),
                TransactionUtils.retrieveTransactionType9C(transactionRequest.getTransactionType()),
                applicationRepository.retrieveEmvParameterRequest()
        );
        readCardRequest.setStan(stan);
        readCardRequest.setAmount(transactionRequest.getAmount());
        readCardRequest.setCashBackAmount(transactionRequest.getAdditionalAmount());
        readCardRequest.setTransactionDate(DateUtils.yyMMddDate());
        readCardRequest.setTransactionTime(DateUtils.HHmmssTime());
        readCardRequest.setMultipleAidSelectionListener(this.multipleAidSelectionListener);
        readCardRequest.setIccDataList(SmartVistaIccDataList.get());
        readCardRequest.setCurrencyName(NepsConstant.PIN_PAD_CURRENCY);
        readCardRequest.setPinPadFixedLayout(!this.terminalRepository.shouldShufflePinPad());
        return readCardRequest;
    }

    protected PurchaseRequest preparePurchaseRequest(
            global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest,
            CardDetails cardDetails,
            Boolean fallbackIccToMag
    ) {
        PurchaseRequest purchaseRequest = super.preparePurchaseRequest(transactionRequest, cardDetails);
        purchaseRequest.setFallbackIccToMag(fallbackIccToMag);
        return purchaseRequest;
    }

    @Override
    protected void processOnlineResult(IsoMessageResponse isoMessageResponse) {
        this.notifier.notify(Notifier.EventType.PROCESSING_ISSUER_SCRIPT, "Processing..");
        readCardResponse = ((ChipCardProcessorInterface) this.cardProcessor).processOnlineResult(readCardResponse, isoMessageResponse);
    }

    @Override
    protected TransactionResponse promptToPerformIccTransaction() {
        updateTransactionIds(false);
        request.readCardService.cleanUp();
        assignTransactionIds();
        List<CardType> allowedCardTypes = new ArrayList<>();
        allowedCardTypes.add(CardType.ICC);
        ReadCardRequest forceIccTransactionReadCardRequest = prepareReadCardRequest(
                allowedCardTypes,
                applicationRepository,
                transactionRequest
        );
        this.notifier.notify(
                Notifier.EventType.FORCE_ICC_CARD,
                Notifier.EventType.FORCE_ICC_CARD.getDescription()
        );
        performTransactionWithCard(forceIccTransactionReadCardRequest);
        return checkTransactionValidity();
    }

    private void assignTransactionIds() {
        stan = terminalRepository.getSystemTraceAuditNumber();
        invoiceNumber = terminalRepository.getInvoiceNumber();
        batchNumber = terminalRepository.getReconciliationBatchNumber();
    }
}
