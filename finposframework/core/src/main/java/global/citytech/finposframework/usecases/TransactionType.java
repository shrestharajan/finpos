package global.citytech.finposframework.usecases;

public enum TransactionType {
    PURCHASE("SALE", "SALE"),
    PRE_AUTH("PRE AUTHORIZATION", "PRE-AUTHORIZATION"),
    VOID("VOID SALE", "VOID SALE"),
    REFUND("REFUND", "REFUND"),
    REVERSAL("REVERSAL", "REVERSAL"),
    AUTH_COMPLETION("AUTH COMPLETION", "SALE COMPLETION"),
    CASH_ADVANCE("CASH ADVANCE", "CASH ADVANCE"),
    TIP_ADJUSTMENT("TIP ADJUSTMENT", "TIP ADJUSTMENT"),
    CASH_IN("CASH IN", "CASH IN"),
    BALANCE_INQUIRY("BALANCE INQUIRY", "BALANCE INQUIRY"),
    MINI_STATEMENT("MINI STATEMENT", "MINI STATEMENT"),
    EMI_PURCHASE("EMI PURCHASE", "EMI PURCHASE"),
    CASH_VOID("VOID CASH ADVANCE", "VOID CASH ADVANCE"),
    PIN_CHANGE("PIN CHANGE", "PIN CHANGE"),
    GREEN_PIN("GREEN PIN" , "GREEN PIN");

    private final String displayName;
    private final String printName;

    TransactionType(String displayName, String printName) {
        this.displayName = displayName;
        this.printName = printName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getPrintName() {
        return printName;
    }
}
