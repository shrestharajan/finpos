package global.citytech.finpos.merchant.data.repository.core.receipt

import global.citytech.finpos.merchant.data.datasource.core.receipt.DetailReportDataSource
import global.citytech.finpos.merchant.domain.model.receipt.detailreport.DetailReportResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.receipt.DetailReportRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportResponseModel
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 1/4/2021.
 */
class DetailReportRepositoryImpl(private val detailReportDataSource: DetailReportDataSource) :
    DetailReportRepository {
    override fun printDetailReport(configurationItem: ConfigurationItem): Observable<DetailReportResponseEntity> {
        return detailReportDataSource.printDetailReport(configurationItem)
    }
}