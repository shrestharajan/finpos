package global.citytech.finpos.merchant.domain.usecase.core.configuration

import global.citytech.finpos.merchant.domain.repository.core.configuration.ConfigurationRepository
import global.citytech.finpos.merchant.domain.model.logon.LogonResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import io.reactivex.Observable

class CoreConfigurationUseCase(val repository: ConfigurationRepository) {
    fun logOn(configurationItem: ConfigurationItem): Observable<LogonResponseEntity> =
        repository.logOn(configurationItem)
}