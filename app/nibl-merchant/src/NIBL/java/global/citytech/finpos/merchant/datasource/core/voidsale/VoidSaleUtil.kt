package global.citytech.finpos.merchant.datasource.core.voidsale

import global.citytech.finpos.merchant.presentation.model.TransactionItem
import global.citytech.finpos.merchant.presentation.model.voidsale.VoidSaleRequestItem
import global.citytech.finpos.merchant.presentation.voidsale.VoidViewModel
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest
import global.citytech.finposframework.utility.HelperUtils
import java.math.BigDecimal

/**
 * Created by Unique Shakya on 6/11/2021.
 */
object VoidSaleUtil {

    fun prepareVoidSaleTransactionRequest(voidSaleRequestItem: VoidSaleRequestItem): TransactionRequest{
        val transactionRequest = TransactionRequest(voidSaleRequestItem.transactionType)
        transactionRequest.amount = voidSaleRequestItem.amount!!
        transactionRequest.originalInvoiceNumber = voidSaleRequestItem.originalInvoiceNumber
        return transactionRequest
    }

    fun getTotalAmount(
        transactionItem: TransactionItem,
        voidSaleViewModel: VoidViewModel? = null
    ): BigDecimal {
        val transactionAmount = HelperUtils.fromIsoAmountToBigDecimal(transactionItem.amount)
        val tipAmount = voidSaleViewModel!!.tipAdjustedAmount.value
        return transactionAmount.add(tipAmount!!)
    }
}