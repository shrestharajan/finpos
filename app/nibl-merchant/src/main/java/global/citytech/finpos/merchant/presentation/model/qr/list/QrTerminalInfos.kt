package global.citytech.finpos.merchant.presentation.model.qr.list

class QrTerminalInfos(
    val merchantId: String,
    val terminalId: String,
    val network: String
) {

}