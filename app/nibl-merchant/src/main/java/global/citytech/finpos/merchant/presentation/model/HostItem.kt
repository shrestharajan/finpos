package global.citytech.finpos.merchant.presentation.model

import global.citytech.finpos.merchant.domain.model.app.Host
import global.citytech.finpos.merchant.presentation.model.response.TlsCertificate
import global.citytech.finpos.merchant.presentation.model.response.mapToTlsCertificate

data class HostItem(
    val id: String,
    val connectionTimeout: String? = null,
    val ip: String? = null,
    val nii: String? = null,
    val order: Int? = null,
    val port: String? = null,
    val retryLimit: String? = null,
    val tlsCertificate: TlsCertificate? = null
)

fun Host.mapToPresentation(): HostItem =
    HostItem(id, connectionTimeout, ip, nii, order, port, retryLimit, tlsCertificate.mapToTlsCertificate())

fun List<Host>.mapToPresentation(): List<HostItem> = map { it.mapToPresentation() }