package global.citytech.finpos.merchant.domain.model.tip

data class TipAdjustmentResponseEntity(
    var approved: Boolean,
    var message: String,
    var shouldPrintCustomerCopy: Boolean,
    var stan: String,
    var debugRequestMessage: String? = null,
    var debugResponseMessage: String? = null
) {
}