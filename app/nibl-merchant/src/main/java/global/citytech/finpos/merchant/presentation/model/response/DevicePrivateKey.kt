package global.citytech.finpos.nibl.merchant.presentation.model.response

data class DevicePrivateKey(
    var key1: String? = null,
    var key2: String? = null
)