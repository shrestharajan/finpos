import com.android.build.gradle.api.ApplicationVariant
import com.android.build.gradle.api.BaseVariantOutput

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
    id("kotlin-android")
}

android {
    compileSdkVersion(Versions.compileSdkVersion)
    buildToolsVersion(Versions.buildToolVersion)

    signingConfigs {
        getByName("debug") {
            keyAlias = "debugKey"
            keyPassword = "keyPassword"
            storeFile = file("debugKeyStore.jks")
            storePassword = "storePassword"
        }
    }

    defaultConfig {
        applicationId = App.applicationId
        minSdkVersion(Versions.minSdkVersion)
        targetSdkVersion(Versions.targetSdkVersion)
        versionCode = App.versionCode
        versionName = App.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        kapt {
            arguments {
                arg("room.schemaLocation", "$projectDir/schemas")
            }
        }

        buildConfigField("String", "ADMIN_DEFAULT_SANCHO", "\"557788\"")
        buildConfigField("String", "MERCHANT_DEFAULT_SANCHO", "\"9771\"")
        buildConfigField("String", "MANUAL_TRANSACTION_ALLOWED", "\"true\"")
        buildConfigField("String", "TRANSACTION_ENABLED", "\"1\"")
    }

    buildTypes {
        getByName("debug") {
            isMinifyEnabled = false
            signingConfig = signingConfigs.getByName("debug")
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        getByName("release") {
            isMinifyEnabled = false
            signingConfig = signingConfigs.getByName("debug")
            resValue("string", "app_name", App.name)
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            lintOptions {
                isAbortOnError  = false // Set to false to ignore Lint errors
            }
        }
    }

    flavorDimensions("switch")
    productFlavors {
        register("NIBL") {
            dimension = "switch"
            applicationIdSuffix = ""
            buildConfigField(
                "String",
                "PROCESSOR_CLASS_NAME",
                "\"global.citytech.finpos.processor.nibl.NiblProcessor\""
            )
            buildConfigField("String", "DO_LOAD_EMV_CONFIGURATION", "\"false\"")
        }

        register("NEPS") {
            dimension = "switch"
            applicationIdSuffix = ".${Neps.applicationSuffix}"
            versionCode = App.versionCode
            versionName = App.versionName
            this.buildConfigField(
                "String",
                "PROCESSOR_CLASS_NAME",
                "\"global.citytech.finpos.processor.neps.NepsProcessor\""
            )
            buildConfigField("String", "DO_LOAD_EMV_CONFIGURATION", "\"true\"")
        }
    }

    dataBinding {
        isEnabled = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    sourceSets {
        val androidTest by getting
        androidTest.assets.srcDirs("$projectDir/schemas")
    }

    packagingOptions {
        exclude("META-INF/DEPENDENCIES")
        exclude("META-INF/LICENSE")
        exclude("META-INF/LICENSE.txt")
        exclude("META-INF/license.txt")
        exclude("META-INF/NOTICE")
        exclude("META-INF/NOTICE.txt")
        exclude("META-INF/notice.txt")
        exclude("META-INF/ASL2.0")
        exclude("META-INF/*.kotlin_module")
    }

    applicationVariants.all(object : Action<ApplicationVariant> {
            override fun execute(variant: ApplicationVariant) {
                println("variant: ${variant}")
                variant.outputs.all(object : Action<BaseVariantOutput> {
                    override fun execute(output: BaseVariantOutput) {
                        val outputImpl = output as com.android.build.gradle.internal.api.BaseVariantOutputImpl
                        val fileName = "finPOS-Cashier-v${variant.versionName}-${variant.name}.apk"
                        println("output file name: ${fileName}")
                        outputImpl.outputFileName = fileName
                    }
                })
            }
        }
    )


}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(project(":common"))

    implementation(files("./libs/tms-sdk-v1.0.6.aar"))

    implementation(project(":devices:wpos3"))
    implementation(project(":devices:feitian"))
    implementation(project(":nibl:processor"))
    implementation(project(":neps:processor"))
    implementation(project(":finposframework:core"))
    implementation(project(":thirdparty:paymentsdk"))
//
//    implementation('global.citytech.posswitchintregator:device-test:1.0.1')

    implementation(Libraries.lifeCycleExtension)
    implementation(Libraries.appCompat)
    implementation(Libraries.constraintLayout)
    implementation("com.wajahatkarim3:roomexplorer:0.0.2")
    implementation("global.citytech.easydroid:easydroid-concurrency:0.1.15")
    implementation("androidx.appcompat:appcompat:1.3.0")
    implementation("org.jetbrains.kotlin:kotlin-stdlib:${rootProject.extra["kotlin_version"]}")
    implementation("androidx.constraintlayout:constraintlayout:2.0.4")
    implementation("com.github.bumptech.glide:glide:4.12.0")
    kapt("com.github.bumptech.glide:compiler:4.12.0")
    kapt(Libraries.lifecycleKaptDependency)
    implementation(Libraries.rxJava)
    implementation(Libraries.rxAndroid)
    implementation("io.requery:sqlite-android:3.33.0")
    implementation("androidx.viewpager2:viewpager2:1.0.0")
    implementation("com.google.android.material:material:1.4.0-rc01")
    implementation("de.hdodenhof:circleimageview:3.1.0")
    implementation("com.google.android.flexbox:flexbox:3.0.0")
    implementation("androidx.swiperefreshlayout:swiperefreshlayout:1.1.0")

    implementation("com.squareup.okhttp3:okhttp:4.10.0")

    implementation(Libraries.qrCode)


    implementation(Libraries.room)
    kapt(Libraries.roomCompiler)
    implementation(Libraries.roomRx)

    testImplementation("junit:junit:4.12")
    testImplementation("org.assertj:assertj-core:${Versions.assertjCore}")
    testImplementation("io.mockk:mockk:${Versions.mockk}")
    testImplementation("androidx.arch.core:core-testing:${Versions.lifecycleTesting}")

    androidTestImplementation(AndroidTest.extJunit)
    androidTestImplementation(AndroidTest.espresso)
    androidTestImplementation(AndroidTest.testRunner)
    androidTestImplementation(AndroidTest.testRule)
    androidTestImplementation(AndroidTest.testRoom)

    implementation("org.eclipse.paho:org.eclipse.paho.client.mqttv3:1.2.4")
    implementation("org.eclipse.paho:org.eclipse.paho.android.service:1.1.1")

    implementation ("com.android.support:support-compat:28.0.0")

    implementation ("com.google.android.gms:play-services-tasks:17.2.1")
    implementation(files("libs/FTSDK_api_V1.0.0.70_20220325.jar"))
    implementation ("com.squareup.retrofit2:retrofit:2.9.0")
    implementation ("com.squareup.retrofit2:converter-gson:2.5.0")
    implementation("com.squareup.retrofit2:converter-simplexml:2.9.0")

}
