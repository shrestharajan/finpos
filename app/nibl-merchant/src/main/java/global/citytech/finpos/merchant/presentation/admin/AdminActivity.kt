package global.citytech.finpos.merchant.presentation.admin

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import global.citytech.common.extensions.openActivity
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityAdminBinding
import global.citytech.finpos.merchant.presentation.admin.batchconfigure.SettlementBatchConfigureDialog
import global.citytech.finpos.merchant.presentation.admin.login.LoginDialog
import global.citytech.finpos.merchant.presentation.alertdialogs.SliderAttributes
import global.citytech.finpos.merchant.presentation.alertdialogs.SliderDialog
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.config.ConfigActivity
import global.citytech.finpos.merchant.presentation.dashboard.temp.DashActivity
import global.citytech.finpos.merchant.presentation.dtg.SetDateTimeGpsActivity
import global.citytech.finpos.merchant.presentation.log.LogManager
import global.citytech.finpos.merchant.presentation.printparamter.PrintParameterActivity
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finpos.merchant.utils.handleDebounce
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.utility.TMSConfigurationConstants.*
import kotlinx.android.synthetic.main.activity_admin.*

/**
 * Created by Rishav Chudal on 8/13/20.
 */
class AdminActivity : AppBaseActivity<ActivityAdminBinding, AdminViewModel>(),
    LoginDialog.LoginDialogListener,
    SettlementBatchConfigureDialog.SettlementBatchConfigureDialogListener {

    private val logger = Logger(AdminActivity::class.java.name)
    private lateinit var loginDialog: LoginDialog
    private lateinit var settlementBatchConfigureDialog: SettlementBatchConfigureDialog
    private var fromDashboard: Boolean = false
    private var isClearBatch: Boolean = false

    companion object {
        private const val EXTRA_FROM_DASHBOARD = "extra_from_dashboard"

        fun getLaunchIntent(context: Context, fromDashboard: Boolean): Intent {
            val intent = Intent(context, AdminActivity::class.java)
            intent.putExtra(EXTRA_FROM_DASHBOARD, fromDashboard)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel().bindService()
        initObservers()
        initViews()
        Glide.with(this).load("file:///android_asset/images/loading.gif")
            .into(iv_loading)
        fromDashboard = intent.getBooleanExtra(EXTRA_FROM_DASHBOARD, false)
        if (fromDashboard) {
            getViewModel().enableHomeButton()
            showAdminMenuLayout()
        } else {
            iv_loading.visibility = View.VISIBLE
            getViewModel().getAdminCredentialFromTms()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ConfigActivity.REGISTER_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                finishAllAndOpenDashboard()
            }
        }
    }

    private fun initViews() {
        hideAdminMenuLayout()
        iv_back.setOnClickListener {
            returnToDashboard()
        }

        btn_network_registration.setOnClickListener {
            btn_network_registration.handleDebounce()
            showConfirmationMessage(MessageConfig.Builder()
                .message(getString(R.string.msg_switch_configuration_confirmation))
                .positiveLabel(getString(R.string.action_yes))
                .onPositiveClick { getViewModel().checkDeviceConfigured() }
                .negativeLabel(getString(R.string.action_no))
                .onNegativeClick { })
        }

        btn_set_date_time_gps.setOnClickListener {
            btn_set_date_time_gps.handleDebounce()
            openActivity(SetDateTimeGpsActivity::class.java)
        }

        btn_tms_parameters.setOnClickListener {
            btn_tms_parameters.handleDebounce()
            openActivity(PrintParameterActivity::class.java)
        }

        btn_tip_configuration.setOnClickListener {
            btn_tip_configuration.handleDebounce()
            this.showTipLimitSelectorDialog()
        }

        btn_batch_number_configuration.setOnClickListener {
            btn_batch_number_configuration.handleDebounce()
            getViewModel().getConfigurationItem(AdminViewModel.Task.BATCH_NUMBER_CONFIGURE)
        }

        btn_reset_merchant_password.setOnClickListener {
            btn_reset_merchant_password.handleDebounce()
            showConfirmationMessage(MessageConfig.Builder()
                .message(getString(R.string.msg_reset_merchant_password_confirmation))
                .positiveLabel(getString(R.string.action_yes))
                .onPositiveClick { getViewModel().resetMerchantPassword() }
                .negativeLabel(getString(R.string.action_no))
                .onNegativeClick { })
        }

        btn_clear_batch.setOnClickListener {
            isClearBatch = true
            getViewModel().getAdminCredentialFromTms()
        }

        btn_sync_terminal_settings.setOnClickListener {
            btn_sync_terminal_settings.handleDebounce()
            showConfirmationMessage(MessageConfig.Builder()
                .message(getString(R.string.msg_sync_terminal_settings))
                .positiveLabel(getString(R.string.action_yes))
                .onPositiveClick { getViewModel().performSyncTerminalSettings() }
                .negativeLabel(getString(R.string.action_no))
                .onNegativeClick { })
        }

        btn_amount_length_configuration.setOnClickListener {
            btn_amount_length_configuration.handleDebounce()
            showAmountLengthLimitSelectorDialog()
        }

        /*btn_gprs.setOnClickListener {
            if(BuildConfig.DEBUG) {
                val settlementAlarmSetter = SettlementAlarmSetter(this.applicationContext)
                val calendar = Calendar.getInstance()
                calendar.timeInMillis = System.currentTimeMillis() + 120000
                logger.log("::: CALENDAR TIME IN MILLIS === " + calendar.timeInMillis)
                val hour = calendar.get(Calendar.HOUR_OF_DAY)
                logger.log("::: CALENDAR HOUR OF DAY === $hour")
                val minute = calendar.get(Calendar.MINUTE)
                logger.log("::: CALENDAR MINUTE === $minute")
                settlementAlarmSetter.setRepeatingAlarm(
                StringUtils.ofRequiredLength(hour.toString(), 2)
                    .plus(":")
                    .plus(StringUtils.ofRequiredLength(minute.toString(), 2))
                )
            }
        }*/

        initDebugModeSwitch()
        initCardConfirmationRequiredSwitch()
        initAutoSettlementSwitch()
        initVatRefundSwitch()
        initShufflePinPadSwitch()
    }

    private fun showAmountLengthLimitSelectorDialog() {
        val amountLengthLimit = getViewModel().retrieveConfiguredAmountLengthLimit()
        val sliderDialogAttributes = SliderAttributes(
            title = getString(R.string.msg_select_amount_length_limit),
            minLimit = MIN_AMOUNT_LENGTH,
            maxLimit = MAX_AMOUNT_LENGTH,
            stepSize = AMOUNT_LENGTH_STEP_SIZE,
            configuredValue = amountLengthLimit,
            invalidInputMessage = getString(R.string.error_msg_invalid_amount_length),
            emptyInputMessage = getString(R.string.error_msg_empty_amount_length),
            defaultConfigurationMessage = getString(R.string.error_msg_default_amount_length),
            textInputHint = getString(R.string.hint_amount_length_limit)
        )
        val sliderDialog = SliderDialog(this, object : SliderDialog.Listener {
            override fun onValueConfirmed(value: Int) {
                getViewModel().onAmountLimitConfirmed(value)
            }

            override fun onCancel() {
                // not needed
            }
        }, sliderDialogAttributes)
        sliderDialog.show()
    }

    private fun startConfigActivity(it: Boolean) {
        val intent = ConfigActivity.getLaunchIntent(this, it, false)
        startActivityForResult(intent, ConfigActivity.REGISTER_REQUEST_CODE)
    }


    private fun hideAdminMenuLayout() {
        this.ll_admin_menu.visibility = View.GONE
    }

    private fun showTipLimitSelectorDialog() {
        val configuredTipPercentage = getViewModel().retrieveConfiguredTipInPercentage()
        val sliderDialogAttributes = SliderAttributes(
            title = getString(R.string.msg_select_tip_limit),
            minLimit = DEFAULT_TIP_LIMIT_CONFIGURATION_IN_PERCENTAGE,
            maxLimit = MAX_TIP_LIMIT_CONFIGURATION_IN_PERCENTAGE,
            stepSize = DEFAULT_STEP_SIZE_FOR_TIP_LIMIT_CONFIGURATION,
            configuredValue = configuredTipPercentage,
            invalidInputMessage = getString(R.string.error_msg_tip_limit_invalid),
            emptyInputMessage = getString(R.string.error_msg_tip_limit_empty),
            defaultConfigurationMessage = getString(R.string.msg_current_configured_tip_percentage),
            textInputHint = getString(R.string.hint_tip_limit)
        )
        val sliderDialog = SliderDialog(this, object : SliderDialog.Listener {
            override fun onValueConfirmed(value: Int) {
                onTipLimitConfirmed(value)
            }

            override fun onCancel() {
                // not needed
            }
        }, sliderDialogAttributes)
        sliderDialog.show()
    }

    private fun onTipLimitConfirmed(value: Int) {
        this.getViewModel().storeTipLimit(value)
    }

    private fun initDebugModeSwitch() {
        val isDebugModeOn = getViewModel().getDebugModeStatus()
        switch_debug_mode.isChecked = isDebugModeOn
        switch_debug_mode.setOnCheckedChangeListener { _, isChecked ->
            getViewModel().saveDebugModePreference(isChecked)
        }
    }

    private fun initCardConfirmationRequiredSwitch() {
        val isCardConfirmationRequiredOn = getViewModel().getCardConfirmationRequiredStatus()
        switch_card_confirmation.isChecked = isCardConfirmationRequiredOn
        switch_card_confirmation.setOnCheckedChangeListener { _,
                                                              isChecked ->
            getViewModel().saveCardConfirmationRequiredPreference(isChecked)
        }
    }

    private fun initAutoSettlementSwitch() {
        val isAutoSettlementRequiredOn = getViewModel().getAutoSettlementTriggerStatus()
        switch_auto_settlement.isChecked = isAutoSettlementRequiredOn
    }

    private fun initVatRefundSwitch() {
        val isVatRefundSupported = getViewModel().getVatRefundSupportedStatus()
        switch_vat_refund.isChecked = isVatRefundSupported
        switch_vat_refund.setOnCheckedChangeListener { _,
                                                       isChecked ->
            getViewModel().saveVatRefundSupportedPreference(isChecked)
        }
    }

    private fun initShufflePinPadSwitch() {
        val isShufflePinPadSupported = getViewModel().getShouldShufflePinPadStatus()
        switch_shuffle_pin_pad.isChecked = isShufflePinPadSupported
        switch_shuffle_pin_pad.setOnCheckedChangeListener { _,
                                                            isChecked ->
            getViewModel().saveShouldShufflePinPadPreference(isChecked)
        }
    }

    private fun initObservers() {

        getViewModel().successfulBatchUpdate.observe(this, Observer {
            showSuccessMessage(MessageConfig.Builder()
                .message("Batch number configured successfully")
                .positiveLabel("OK")
                .onPositiveClick {
                })
        })

        getViewModel().serviceLoaded.observe(this, Observer {
            logger.log("Service Bound")
        })

        getViewModel().isLoading.observe(this, Observer { isLoading ->
            if (isLoading) {
                showProgress()
            } else {
                hideProgress()
            }
        })

        getViewModel().deviceConfigurationStatus.observe(this, Observer { status ->
            when (status) {
                DeviceConfigurationStatus.TerminalConfigured -> {
                    hideProgress()
                    showSuccessMessage(
                        MessageConfig.Builder()
                            .message(getString(R.string.terminal_configured_successfully))
                            .positiveLabel(getString(R.string.return_to_dashboard))
                            .onPositiveClick {
                                returnToDashboard()
                            })
                }
                DeviceConfigurationStatus.ConfigurationCanceled -> {
                    hideProgress()
                }
                DeviceConfigurationStatus.ConfigurationFailed -> {
                    hideProgress()
                    showSwitchConfigurationErrorMessage(getString(R.string.terminal_configuration_failed))
                }
                else -> {
                    showProgress(status.message)
                }
            }
        })

        getViewModel().navigateToOperatorLoginMessage.observe(this, Observer {
            showConfirmationMessage(MessageConfig.Builder()
                .message(it)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    this.navigateToOperatorLogin()
                }
                .negativeLabel(getString(R.string.action_cancel))
                .onNegativeClick {
                }
            )
        })

        getViewModel().terminalConfigured.observe(this, Observer {
            startConfigActivity(it!!)
        })

        getViewModel().adminCredential.observe(this, Observer {
            val message: String = if (isClearBatch)
                getString(R.string.title_enter_admin_password_for_clear_batch)
            else
                getString(R.string.title_enter_admin_password)
            showAdminLoginDialog(message, it!!)
        })

        getViewModel().resetPasswordSuccess.observe(this, Observer {
            showToast(getString(R.string.msg_success_reset_password))
        })

        observeBaseViewModelMessageLiveData()
        observeBatchSettingPossibleLiveData()
        observeExistingBatchNumberLiveData()
        observeSyncSettingsSuccessLiveData()
    }

    private fun navigateToOperatorLogin() {
        val i = Intent("global.citytech.action.OPERATOR")
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(i)
    }

    private fun showSwitchConfigurationErrorMessage(it: String?) {
        showConfirmationMessage(MessageConfig.Builder()
            .message(it)
            .positiveLabel(getString(R.string.action_retry))
            .onPositiveClick {
                getViewModel().register()
            }
            .negativeLabel(getString(R.string.action_cancel))
            .onNegativeClick {
            })
    }

    private fun showErrorMessage(it: String?) {
        showNormalMessage(
            MessageConfig.Builder()
                .message(it)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                }
                .negativeLabel(getString(R.string.action_cancel))
                .onNegativeClick {
                }
        )
    }

    private fun returnToDashboard() {
        if (!fromDashboard) {
            finishAffinity()
            openActivity(DashActivity::class.java)
        }
        finish()
    }

    private fun finishAllAndOpenDashboard() {
        finishAffinity()
        openActivity(DashActivity::class.java)
    }

    private fun showAdminLoginDialog(pageTitle: String, validCredential: String) {
        this.loginDialog = LoginDialog.newInstance(
            pageTitle = pageTitle,
            validCredential = validCredential
        )
        this.loginDialog.isCancelable = false
        this.loginDialog.show(
            supportFragmentManager,
            LoginDialog.TAG
        )
    }

    override fun onLoginDialogCorrectCredentialEntered() {
        if (isClearBatch)
            getViewModel().getConfigurationItem(AdminViewModel.Task.CLEAR_BATCH)
        else
            hideLoginDialogAndShowAdminMenu()
    }

    override fun onLoginDialogCancelButtonClicked() {
        if (isClearBatch)
            hideAdminLoginDialog()
        else
            returnToDashboard()
    }

    override fun onBackPressed() {
        returnToDashboard()
    }

    private fun hideAdminLoginDialog() {
        this.loginDialog.dismiss()
        this.logger.log("Admin Dialog Dismissed...")
    }

    private fun observeBatchSettingPossibleLiveData() {
        getViewModel().batchSettingPossibleLiveData.observe(
            this,
            Observer {
                if (!it) {
                    buildMessageConfigForSettlementFirst()
                }
            }
        )
    }

    private fun buildMessageConfigForSettlementFirst() {
        val messageConfig = MessageConfig.Builder()
        messageConfig.title(getString(R.string.title_batch_number_configuration))
        messageConfig.message(getString(R.string.msg_perform_settlement_first_for_batch_configure))
        messageConfig.positiveLabel(getString(R.string.title_ok))
        messageConfig.onPositiveClick { }
        showFailureMessage(messageConfig)
    }

    private fun observeExistingBatchNumberLiveData() {
        getViewModel().existingBatchNumberLiveData.observe(
            this,
            Observer {
                showBatchConfigurationDialog(it)
            }
        )
    }

    private fun observeBaseViewModelMessageLiveData() {
        getViewModel().message.observe(
            this,
            Observer {
                showMessageInDialog(it)
            }
        )
    }

    private fun showMessageInDialog(message: String) {
        if (!message.isNullOrEmptyOrBlank()) {
            val messageConfig = MessageConfig.Builder()
            messageConfig.message(message)
            messageConfig.positiveLabel(getString(R.string.title_ok))
            messageConfig.onPositiveClick { }
            showNormalMessage(messageConfig)
        }
    }

    private fun showBatchConfigurationDialog(existingBatchNumber: String) {
        settlementBatchConfigureDialog = SettlementBatchConfigureDialog.newInstance(
            currentBatchNumber = existingBatchNumber
        )
        settlementBatchConfigureDialog.isCancelable = false
        settlementBatchConfigureDialog.show(
            supportFragmentManager,
            SettlementBatchConfigureDialog.TAG
        )
    }

    private fun hideLoginDialogAndShowAdminMenu() {
        hideAdminLoginDialog()
        getViewModel().enableHomeButton()
        showAdminMenuLayout()
    }

    private fun showAdminMenuLayout() {
        iv_loading.visibility = View.GONE
        this.ll_admin_menu.visibility = View.VISIBLE
    }

    private fun observeSyncSettingsSuccessLiveData() {
        getViewModel().syncTerminalSettingsSuccessLiveData.observe(
            this,
            Observer {
                if (it) {
                    fromDashboard = false
                    onSuccessToSyncSettings()
                } else {
                    onFailureToSyncSettings()
                }
            }
        )
    }

    private fun onSuccessToSyncSettings() {
        val messageConfig = MessageConfig.Builder()
        messageConfig.title(getString(R.string.title_sync_terminal_settings))
        messageConfig.message(getString(R.string.msg_success_sync_terminal_settings))
        messageConfig.positiveLabel(getString(R.string.title_ok))
        messageConfig.onPositiveClick { }
        showSuccessMessage(messageConfig)
    }

    private fun onFailureToSyncSettings() {
        val messageConfig = MessageConfig.Builder()
        messageConfig.title(getString(R.string.title_sync_terminal_settings))
        messageConfig.message(getString(R.string.msg_error_sync_terminal_settings))
        messageConfig.positiveLabel(getString(R.string.title_ok))
        messageConfig.onPositiveClick { }
        showFailureMessage(messageConfig)
    }

    override fun getBindingVariable(): Int = BR.adminViewModel

    override fun getLayout(): Int = R.layout.activity_admin

    override fun getViewModel(): AdminViewModel =
        ViewModelProviders.of(this)[AdminViewModel::class.java]

    override fun onDestroy() {
//        LogManager.clear()
        super.onDestroy()
    }

    override fun onBatchConfigureConfirmButtonClicked(batchNumber: Int) {
        getViewModel().configureWithInputBatchNumber(batchNumber)
    }
}