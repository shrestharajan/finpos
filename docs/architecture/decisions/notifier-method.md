# Title
Notifier Method

## Status

Decided. We are open to change to alternative if found any issue with selected one.

## Context

We discussed two methods Callbacks and Broadcast Receiver for notifier. So we have to choose one of them for notifier.

## Decision

We have selected Broadcast Receiver for notifier

## Arguments

- We achieve a loose coupling between your components.

- We can have a 1-to-n relationship (including 1-to-0).

- The onReceive() method is always executed on the main thread.

- We can notify components in your entire application, so the communicating components do not have to "see" eachother. 

- The client for the receiver has not to be propagated through various methods/components if it is not directly accessible by the caller of the service

- For message broadcasting between UI- and core-layer if we use the BroadcastReceiver- approach that let us decouple your UI layer from the logic layer


## Consequences

- This is a very generic approach, so marshalling and unmarshalling of data transported by the Intent is an additional error source

- it will cause performance issues if you transport a large amount (i.e about 504 KB )of data with your Intent. But we have various solution this issue too.

