package global.citytech.finpos.merchant.domain.repository.core.preauth.completion

import global.citytech.finpos.merchant.domain.model.auth.completion.AuthorisationCompletionResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.preauth.completion.AuthorisationCompletionRequestItem
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/29/2020.
 */
interface AuthorisationCompletionRepository {
    fun doAuthorisationCompletion(
        configurationItem: ConfigurationItem,
        authorisationCompletionRequestItem: AuthorisationCompletionRequestItem
    ):
            Observable<AuthorisationCompletionResponseEntity>
}