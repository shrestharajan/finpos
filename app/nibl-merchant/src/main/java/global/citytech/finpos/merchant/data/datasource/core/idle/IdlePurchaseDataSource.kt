package global.citytech.finpos.merchant.data.datasource.core.idle

import global.citytech.finpos.merchant.domain.model.purchase.PurchaseResponseEntity
import global.citytech.finpos.merchant.domain.model.purchase.RefundResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.purchase.PurchaseRequestItem

import io.reactivex.Observable

/**
 * Created by Rishav Chudal on 11/24/20.
 */
interface IdlePurchaseDataSource {
    fun purchase(
        configurationItem: ConfigurationItem,
        purchaseRequestItem: PurchaseRequestItem
    ): Observable<PurchaseResponseEntity>
}