package global.citytech.finpos.processor.neps.keyexchange;

import global.citytech.finposframework.switches.keyexchange.KeyExchangeResponseParameter;
import global.citytech.finposframework.supports.UseCase;

/** User: Surajchhetry Date: 2/28/20 Time: 6:03 PM */
public final class KeyExchangeResponseModel
    implements UseCase.Response, KeyExchangeResponseParameter {
  private String debugRequestMessage;
  private String debugResponseMessage;
  private boolean success;
  private String communicationKey;

  public KeyExchangeResponseModel(Builder builder) {
    this.debugRequestMessage = builder.debugRequestMessage;
    this.debugResponseMessage = builder.debugResponseMessage;
    this.success = builder.success;
    this.communicationKey = builder.communicationKey;
  }

  public String getDebugRequestMessage() {
    return debugRequestMessage;
  }

  public String getDebugResponseMessage() {
    return debugResponseMessage;
  }

  public boolean isSuccess() {
    return success;
  }

  public String getCommunicationKey() {
    return communicationKey;
  }

  @Override
  public String toString() {
    return "KeyExchangeResponseModel{"
        + "debugRequestMessage='"
        + debugRequestMessage
        + '\''
        + ", debugResponseMessage='"
        + debugResponseMessage
        + '\''
        + ", success="
        + success
        + ", communicationKey='"
        + communicationKey
        + '\''
        + '}';
  }

  public static final class Builder {
    private String debugRequestMessage;
    private String debugResponseMessage;
    private boolean success;
    private String communicationKey;

    private Builder() {}

    public static Builder createDefaultBuilder() {
      return new Builder();
    }

    public Builder debugRequestMessage(String debugRequestString) {
      this.debugRequestMessage = debugRequestString;
      return this;
    }

    public Builder debugResponseMessage(String debugResponseString) {
      this.debugResponseMessage = debugResponseString;
      return this;
    }

    public Builder success(boolean success) {
      this.success = success;
      return this;
    }

    public Builder communicationKey(String communicationKey) {
      this.communicationKey = communicationKey;
      return this;
    }

    public KeyExchangeResponseModel build() {
      return new KeyExchangeResponseModel(this);
    }
  }
}
