package global.citytech.finpos.merchant.presentation.dashboard.temp

import android.app.Application
import android.os.Handler
import androidx.lifecycle.MutableLiveData
import global.citytech.common.Base64
import global.citytech.common.Constants
import global.citytech.common.data.Response
import global.citytech.common.extensions.hasInternetConnection
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.*
import global.citytech.finpos.merchant.data.repository.core.configuration.ConfigurationRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.reconciliation.ReconciliationRepositoryImpl
import global.citytech.finpos.merchant.data.repository.device.DeviceRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.Action
import global.citytech.finpos.merchant.domain.model.app.AppNotification
import global.citytech.finpos.merchant.domain.model.app.MerchantTransactionLog
import global.citytech.finpos.merchant.domain.model.app.Type
import global.citytech.finpos.merchant.domain.model.logon.LogonResponseEntity
import global.citytech.finpos.merchant.domain.usecase.app.*
import global.citytech.finpos.merchant.domain.usecase.core.configuration.CoreConfigurationUseCase
import global.citytech.finpos.merchant.domain.usecase.core.reconciliation.CoreReconciliationUseCase
import global.citytech.finpos.merchant.domain.usecase.device.DeviceUseCase
import global.citytech.finpos.merchant.framework.datasource.app.*
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.configuration.ConfigurationDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.reconciliation.ReconciliationDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.device.DeviceConfigurationSourceImpl
import global.citytech.finpos.merchant.framework.datasource.device.DeviceControllerImpl
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuItem
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuUtils
import global.citytech.finpos.merchant.presentation.dynamicqr.QrType
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.banner.Banner
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.presentation.model.merchanttransactionlog.MerchantTransactionLogResponse
import global.citytech.finpos.merchant.presentation.model.qroperator.QrOperatorItem
import global.citytech.finpos.merchant.presentation.model.setting.TerminalSettingResponse
import global.citytech.finpos.merchant.service.ActivityLogPushService
import global.citytech.finpos.merchant.service.NetworkConnectionReceiver
import global.citytech.finpos.merchant.service.TransactionPushService
import global.citytech.finpos.merchant.utils.*
import global.citytech.finpos.merchant.utils.AppConstant.FONE_PAY_ID
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.exceptions.PosException
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.switches.posmode.PosMode
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.utility.StringUtils
import global.citytech.sendlog.SendLogInterface.LogPushInterface
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


/**
 * Created by Rishav Chudal on 5/11/21.
 */
class DashViewModel(val context: Application) : BaseAndroidViewModel(context) {

    private val TAG = DashViewModel::class.java.name
    val showPurchaseTransactionLiveData by lazy { MutableLiveData<Boolean>() }
    val startDashboard by lazy { MutableLiveData<PosMode>() }
    val appConfiguration by lazy { MutableLiveData<ConfigurationItem>() }
    val appVersion by lazy { MutableLiveData<String>() }
    val terminalIsConfigured by lazy { MutableLiveData<Boolean>() }
    val menusMutableLiveData by lazy { MutableLiveData<List<MenuItem>>() }
    val noNotifications by lazy { MutableLiveData<Boolean>() }
    val startSettlement by lazy { MutableLiveData<Boolean>() }
    val autoReversalPresent by lazy { MutableLiveData<Boolean>() }
    val logonResponse by lazy { MutableLiveData<Boolean>() }
    private val deviceController = DeviceControllerImpl(context)
    val connectionAvailable by lazy { MutableLiveData<Boolean>() }
    val settlementActive by lazy { MutableLiveData<Boolean>() }
    val adminCredential by lazy { MutableLiveData<String>() }
    val commandsPresent by lazy { MutableLiveData<Boolean>() }
    val bankDisplayImage by lazy { MutableLiveData<ByteArray>() }
    val defaultBankDisplayImage by lazy { MutableLiveData<Boolean>() }
    val terminalBannersLiveData by lazy { MutableLiveData<List<Banner>?>() }
    val settlementBatchNumber by lazy { MutableLiveData<String>() }
    val switchConfigActive by lazy { MutableLiveData<Boolean>() }
    val appResetActive by lazy { MutableLiveData<Boolean>() }
    val readyToStartTransactionLogPush by lazy { MutableLiveData<Boolean>() }
    val readyToResetDevice by lazy { MutableLiveData<Boolean>() }
    val canCloseApplication by lazy { MutableLiveData<Boolean>() }
    var isAppResetInProgress = false

    var configurationUseCase: CoreConfigurationUseCase =
        CoreConfigurationUseCase(
            ConfigurationRepositoryImpl(
                ConfigurationDataSourceImpl()
            )
        )

    var localDataUseCase: LocalDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )
    var sendLogUseCase: LogSendUseCase = LogSendUseCase(
        LogPushRepositoryImpl(
            LogPushDataSourceImpl()
        )
    )

    private val deviceConfigurationUseCase = DeviceUseCase(
        DeviceRepositoryImpl(
            DeviceConfigurationSourceImpl(context)
        )
    )

    private val transactionRepository =
        TransactionRepositoryImpl(
            context
        )

    private val appNotificationUseCase = AppNotificationUseCase(
        AppNotificationRepositoryImpl(
            AppNotificationDataSourceImpl(context.applicationContext)
        )
    )
    val reconciliationRepository =
        global.citytech.finpos.merchant.framework.datasource.core.ReconciliationRepositoryImpl()

    private val qrPaymentUseCase = QrPaymentUseCase(
        QrPaymentRepositoryImpl(
            QrPaymentDataSourceImpl(context)
        )
    )


    private val coreAutoReversalQueueUseCase = CoreAutoReversalQueueUseCase(
        AutoReversalQueueRepositoryImpl(
            AutoReversalQueueDataSourceImpl(context)
        )
    )

    private val coreReconciliationUseCase = CoreReconciliationUseCase(
        ReconciliationRepositoryImpl(
            ReconciliationDataSourceImpl()
        )
    )

    private val coreReconciliationUseCase1 = CoreReconciliationUseCase(
        ReconciliationRepositoryImpl(
            ReconciliationDataSourceImpl()
        )
    )


    private val commandQueueUseCase = CommandQueueUseCase(
        CommandQueueRepositoryImpl(
            CommandQueueDataSourceImpl(context)
        )
    )

    fun logOn() {
        isLoading.value = true
        if (!context.hasInternetConnection()) {
            if (isAirplaneModeOn(context)) {
                printLog(TAG, context.getString(R.string.msg_turn_off_airplane))
                message.value = context.getString(R.string.msg_turn_off_airplane)
            } else {
                printLog(TAG, context.getString(R.string.msg_turn_on_data_or_wifi))
                message.value = context.getString(R.string.msg_turn_on_data_or_wifi)
            }
            isLoading.value = false
            logonResponse.value = false
            return
        }
        compositeDisposable.add(
            configurationUseCase.logOn(appConfiguration.value!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    this.handleMerchantLogonResponse(it)
                    ActivityLogPushService.pushActivityLog()
                }, {
                    printLog(TAG, "LOGON ERROR :: " + it.message.toString())
                    printLog(TAG, context.getString(R.string.error_msg_key_exchange_failed))
                    isLoading.value = false
                    it.printStackTrace()
                    message.value = context.getString(R.string.error_msg_key_exchange_failed)
                    logonResponse.value = false
                    ActivityLogPushService.pushActivityLog()
                })
        )
    }

    private fun handleMerchantLogonResponse(it: LogonResponseEntity?) {
        if (it?.isSuccess!!) {
            message.value = context.getString(R.string.msg_key_exchange_success)
            logonResponse.value = true
        } else {
            message.value = context.getString(R.string.error_msg_key_exchange_failed)
            logonResponse.value = false
        }
    }

    fun checkForAutoReversal() {
        if (coreAutoReversalQueueUseCase.isQueueEmpty()) {
            printLog(TAG, "::: AUTO REVERSAL ::: QUEUE IS EMPTY")
            autoReversalPresent.value = false
        } else {
            printLog(TAG, "::: AUTO REVERSAL ::: QUEUE IS NOT EMPTY")
            autoReversalPresent.value = coreAutoReversalQueueUseCase.isActive()
        }
    }

    fun checkForCommands() {
        printLog(TAG, "CHECKING FOR COMMANDS")
        if (commandQueueUseCase.isQueueEmpty())
            checkForForceRequestFlag()
        else
            if (commandQueueUseCase.hasActiveCommands())
                commandsPresent.value = true
            else
                checkForForceRequestFlag()
    }

    private fun checkForForceRequestFlag() {
        printLog(TAG, "checkForForceRequestFlag...")
        if (localDataUseCase.getForceSettlementPreference(false)) {
            printLog(TAG, "::: FORCE SETTLEMENT ::: ")
            startSettlement.value = true
        } else {
            startSettlement.value = false
        }

    }

    fun getPosMode() {
        this.startDashboard.value =
            localDataUseCase.getPosMode(PosMode.RETAILER)
    }

    fun checkForNotifications() {
        noNotifications.value = appNotificationUseCase.noNotifications()
    }

    fun getAppVersion() {
        val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        val version = packageInfo.versionName
        appVersion.postValue("v".plus(version))
    }

    fun pushLogFileToServer(filePaths: List<String>) {
        val serialNumber = NiblMerchant.INSTANCE.iPlatformManager.serialNumber.toString()
        for (filePath in filePaths) {
            val file = File(filePath)
            if (!file.exists()) {
                continue
            }
            val currentDateTime = Date()
            val pattern = "yyyy-MM-dd"
            val simpleDateFormat = SimpleDateFormat(pattern, Locale.US)
            val formattedDate = simpleDateFormat.format(currentDateTime)

            val fileName = filePath.substringAfterLast("/").substringBeforeLast(".txt")
            val datePartFromFilePath = fileName.removePrefix("logcat_")

            val dateFromFilePath = simpleDateFormat.parse(datePartFromFilePath)
            val currentDate = simpleDateFormat.parse(formattedDate)

            if (dateFromFilePath.before(currentDate)) {
                compositeDisposable.add(
                    Observable.create<LogResponse> { emitter ->
                        emitter.onNext(sendLogUseCase.getLogsList(serialNumber, filePath))
                    }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                            {
                                Logger.getLogger("RESPONSE FROM API::::" + it.data.toString())
                                if (it.code == "0") {
                                    val properties = it.data.properties
                                    val requestFile = RequestBody.create(
                                        "application/octet-stream".toMediaTypeOrNull(),
                                        file
                                    )
                                    val body = MultipartBody.Part.createFormData(
                                        "file",
                                        file.name,
                                        requestFile
                                    )
                                    val retrofitService =
                                        getRetrofit(it.data.url + "/").create(
                                            LogPushInterface::class.java
                                        )
                                    val params: MutableMap<String, RequestBody> = HashMap()
                                    params["x-amz-date"] =
                                        createPartFromString(properties.xAmzDate)
                                    params["success_action_status"] =
                                        createPartFromString(properties.successActionStatus)
                                    params["x-amz-signature"] =
                                        createPartFromString(properties.xAmzSignature)
                                    params["response-content-type"] =
                                        createPartFromString(properties.responseContentType)
                                    params["key"] = createPartFromString(properties.key)
                                    params["x-amz-algorithm"] =
                                        createPartFromString(properties.xAmzAlgorithm)
                                    params["Content-Type"] =
                                        createPartFromString(properties.contentType)
                                    params["x-amz-credential"] =
                                        createPartFromString(properties.xAmzCredential)
                                    params["policy"] = createPartFromString(properties.policy)

                                    val call = retrofitService.uploadLogFileToServer("", params, body)
                                    call.enqueue(object : Callback<LogFileUploadResponse> {
                                        override fun onResponse(
                                            call: Call<LogFileUploadResponse>,
                                            response: retrofit2.Response<LogFileUploadResponse>
                                        ) {
                                            if (response.isSuccessful) {
                                                compositeDisposable.add(
                                                    Observable.create<Response> { emitter ->
                                                        response.body()?.let { it1 ->
                                                            sendLogUseCase.saveLogToDB(
                                                                it1,
                                                                filePath,
                                                                serialNumber
                                                            )
                                                        }?.let { it2 -> emitter.onNext(it2) }
                                                    }
                                                        .subscribeOn(Schedulers.io())
                                                        .observeOn(AndroidSchedulers.mainThread())
                                                        .subscribe(
                                                            {
                                                                if (it.code == "0") {
                                                                    Logger.getLogger("RESPONSE FROM API:::: SUCCESS::::" + it.data.toString())
                                                                    deleteFileAndParentFolder(
                                                                        filePath
                                                                    )
                                                                } else {
                                                                    Logger.getLogger("RESPONSE FROM API:::: FAILURE")
                                                                }

                                                            },
                                                            { error ->
                                                            })
                                                )
                                            } else {
                                                Logger.getLogger("ON-RESPONSE::::: FAILURE - HTTP Error: ${response.code()}")
                                                // Handle the error here
                                            }
                                        }

                                        override fun onFailure(
                                            call: Call<LogFileUploadResponse>,
                                            t: Throwable
                                        ) {
                                            Logger.getLogger("ON-RESPONSE::::: FAILURE - Request Failed")
                                            t.printStackTrace()
                                            // Handle the failure here
                                        }
                                    })
                                } else {
                                    Logger.getLogger("ON-REPSPONSE::::::API-FAILURE")
                                }
                            },
                            { error ->
                                // Handle the error
                            }
                        )
                )

            } else {
                Logger.getLogger("ON-REPSPONSE::::::There's no other log file other than today's log")
            }
        }
    }

    fun deleteFileAndParentFolder(filePath: String) {
        val file = File(filePath)

        if (file.exists()) {
            file.delete()
            val parentDir = file.parentFile

            if (parentDir != null && parentDir.exists()) {
                if (parentDir.list()?.isEmpty() == true) {
                    parentDir.delete()
                }
            }
        }
    }

    fun createPartFromString(value: String): RequestBody {
        return RequestBody.create("text/plain".toMediaTypeOrNull(), value)
    }

    fun getRetrofit(url: String): Retrofit {
        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(SimpleXmlConverterFactory.create()) // Use Simple XML Converter
            .build()
        return retrofit
    }

    fun getConfiguration(): Boolean {
        this.isLoading.value = true
        return compositeDisposable.add(
            localDataUseCase.getWholeConfiguration()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe({
                    appConfiguration.value = it
                    NetworkConnectionReceiver.networkConfiguration = it
                    doCleanUpAtStart()
                    retrieveBankDisplayImage(it)
                },
                    {
                        printLog(TAG, "Get Configuration error ::: " + it.message)
                        this.isLoading.value = false
                        message.value = it.message
                    })
        )
    }

    private fun retrieveBankDisplayImage(it: ConfigurationItem?) {
        it?.let { configItem ->
            configItem.logos?.let {
                if (it.isNotEmpty()) {
                    it[0].displayLogo?.let { s ->
                        retrieveBankDisplayImageByteArray(s)
                    } ?: run {
                        defaultBankDisplayImage.value = true
                    }
                } else {
                    defaultBankDisplayImage.value = true
                }
            } ?: run {
                defaultBankDisplayImage.value = true
            }
        } ?: run {
            defaultBankDisplayImage.value = true
        }
    }

    private fun retrieveBankDisplayImageByteArray(s: String) {
        isLoading.value = true
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<ByteArray> {
                it.onNext(Base64.decode(s))
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    bankDisplayImage.value = it
                }, {
                    isLoading.value = false
                    printLog(TAG, ":: UNABLE TO PARSE BANK DISPLAY IMAGE :: ${it.message}")
                    defaultBankDisplayImage.value = true
                })
        )
    }

    private fun doCleanUpAtStart() = Completable
        .fromAction { deviceConfigurationUseCase.closeUpIO() }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            this::onCleanUpComplete,
            this::onCleanUpError
        )

    private fun onCleanUpComplete() {
        this.isLoading.value = false
        printLog(TAG, "Clean Up Complete")
    }

    private fun onCleanUpError(throwable: Throwable) {
        this.isLoading.value = false
        printLog(TAG, "Clean Up Error ::: ".plus(throwable.message))
    }

    fun getTerminalConfiguredStatusAndProceed() {
        this.terminalIsConfigured.value =
            localDataUseCase.isTerminalConfigured(defaultValue = false)
    }

    fun loadEnabledTransactionsForGivenPosMode(posMode: PosMode) {
        this.isLoading.value = true
        this.compositeDisposable.add(
            this.localDataUseCase.getEnabledTransactionsForGivenPosMode(posMode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        onNextLoadingEnabledTransactions(it, posMode)
                    },
                    {
                        printLog(TAG, "TRANSACTION FOR GIVEN POS MODE ::: " + it.message)
                        onErrorLoadingEnabledTransactions(it)
                    }
                )
        )
    }

    private fun onNextLoadingEnabledTransactions(
        enabledTransactions: List<TransactionType>,
        posMode: PosMode
    ) {
        this.proceedWithEnabledTransactions(enabledTransactions, posMode)
        val menus = MenuUtils.getMenuBasedOnEnabledTransactions(enabledTransactions).toMutableList()
        addOtherTransactions(menus)
        this.menusMutableLiveData.value = menus.toList()
        this.isLoading.value = false
    }

    private fun addOtherTransactions(menus: MutableList<MenuItem>) {
        addEmiTransactionToMenuItemListIfEnabled(menus)
        addQrMenuIfExists(menus as ArrayList<MenuItem>)
        val poseMode = localDataUseCase.getPosMode(PosMode.RETAILER)
        if (poseMode == PosMode.CASH && AppUtility.isAppVariantNIBL()) {
            addCashAdvanceFeature(menus)
        } else if (!AppUtility.isAppVariantNIBL()) {
            addCashAdvanceFeatureForNEPS(menus)
            addConnectFonePayIfExist(menus)

        }
        MenuUtils.addDefaultMenuItems(menus)
    }

    private fun addConnectFonePayIfExist(menus: MutableList<MenuItem>) {
        val qrOperators = getQrOperatorItems()
        var count = 0
        for (i in qrOperators.indices) {
            if (QrType.FONEPAY == QrType.getByCodes(qrOperators[i].name)) {
                count++
                if (!localDataUseCase.getFonePayLoginState(false)) {
                    menus.add(MenuUtils.menuItemConnectFonePay())
                } else {
                    menus.remove(MenuUtils.menuItemConnectFonePay())
                }
            }
        }
        if (count == 0) {
            localDataUseCase.saveFonePayLoginState(false)
            menus.remove(MenuUtils.menuItemConnectFonePay())
        }
    }

    private fun getQrOperatorItems(): ArrayList<QrOperatorItem> {
        val settings = this.localDataUseCase.getTerminalSetting()
        val qrOperatorList = ArrayList<QrOperatorItem>()
        settings.qrConfigs
            ?.forEach { it ->
                run {
                    if (it.enableDynamicQr && it.qrOperatorInfo.active) {
                        qrOperatorList.add(it.qrOperatorInfo)
                    }
                }
            }
        return qrOperatorList
    }


    private fun addCashAdvanceFeatureForNEPS(menus: MutableList<MenuItem>) {
        if(localDataUseCase.getGreenPinStatus()){
            addPinChangeTransactionToMenuItemListIfEnabled(menus)
            addGreenPinTransactionToMenuItemListIfEnabled(menus)
        }
    }

    private fun addCashAdvanceFeature(menus: MutableList<MenuItem>) {
        menus.add(MenuUtils.menuItemGreenPin())
        menus.add(MenuUtils.menuItemPinChange())
        menus.add(MenuUtils.menuItemMiniStatement())
        menus.add(MenuUtils.menuItemBalanceInquiry())
        menus.add(MenuUtils.menuItemCashIn())
    }

    private fun addQrMenuIfExists(menus: ArrayList<MenuItem>) {
        val settings = this.getTerminalSettings()
        val qrOperatorList = ArrayList<QrOperatorItem>()
        if (!settings.qrCode.isNullOrEmptyOrBlank()) {
            qrOperatorList.add(
                QrOperatorItem(
                    id = FONE_PAY_ID,
                    name = "FonePay QR",
                    logo = "",
                    acquirerCode = "FONE PAY CODE",
                    host = "",
                    active = true,
                    issuers = emptyList()
                )
            )
        }
        settings.qrConfigs
            ?.forEach { it ->
                run {
                    if (it.enableStaticQr && it.qrOperatorInfo.active) {
                        qrOperatorList.add(it.qrOperatorInfo)
                    }
                }
            }
        if (qrOperatorList.size > 0) {
            menus.add(MenuUtils.menuItemQrCode())
        }
    }

    private fun onErrorLoadingEnabledTransactions(throwable: Throwable) {
        printLog(
            TAG,
            "Exception in Loading Allowed Transactions ::: ".plus(throwable.message),
            true
        )
        val menus = MenuUtils.getMenuBasedOnEnabledTransactions(emptyList()).toMutableList()
        addOtherTransactions(menus)
        this.menusMutableLiveData.value = menus.toList()
        this.isLoading.value = false
    }


    private fun proceedWithEnabledTransactions(
        enabledTransactions: List<TransactionType>,
        posMode: PosMode
    ) {
        var transactionType = TransactionType.PURCHASE
        if (posMode == PosMode.CASH)
            transactionType = TransactionType.CASH_ADVANCE
        showPurchaseTransactionLiveData.value = enabledTransactions.contains(transactionType)
    }

    fun disableHomeButton() {
        isLoading.value = true
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<DeviceResponse> {
                disableHomeButton(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    printLog(TAG, "Hardware Button Disabled", true)
                }, {
                    printLog(TAG, "Hardware Button Disabled error ::: " + it.message.toString())
                    isLoading.value = false
                    it.printStackTrace()
                })
        )
    }

    private fun disableHomeButton(it: ObservableEmitter<DeviceResponse>) {
        if (BuildConfig.DEBUG)
            it.onNext(deviceController.enableHomeButton())
        else
            it.onNext(deviceController.disableHomeButton())
        it.onComplete()
    }

    fun checkConnectionWithHost() {
        isLoading.value = true
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Boolean> {
                onCheckConnectionSubscribe(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    NiblMerchant.INSTANCE.networkConnectionAvailable.value = it
                    connectionAvailable.value = it
                }, {
                    printLog(TAG, it.message.toString())
                    it.printStackTrace()
                    isLoading.value = false
                    NiblMerchant.INSTANCE.networkConnectionAvailable.value = false
                    connectionAvailable.value = false
                })
        )
    }


    private fun onCheckConnectionSubscribe(emitter: ObservableEmitter<Boolean>) {
        val terminalRepository = TerminalRepositoryImpl(appConfiguration.value)
        emitter.onNext(
            isConnectionAvailable() && isConnectionAvailableWithHosts(
                terminalRepository.findPrimaryHost(),
                terminalRepository.findSecondaryHost()
            )
        )
    }

    fun checkSettlementTime() {
        isLoading.value = true
        compositeDisposable.add(
            coreReconciliationUseCase.checkReconciliationRequired(appConfiguration.value!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    settlementActive.value = it!!
                }, {
                    printLog(TAG, "SETTLEMENT TIME :::" + it.message.toString())
                    isLoading.value = false
                    settlementActive.value = false
                })
        )
    }

    fun addSettlementPendingNotification() {
        appNotificationUseCase.addNotification(
            AppNotification(
                title = context.getString(R.string.title_settlement_pending),
                body = context.getString(R.string.msg_settlement_active),
                action = Action.SETTLEMENT,
                read = false,
                type = Type.WARNING,
                timeStamp = StringUtils.dateTimeStamp()
            )
        )
        checkForNotifications()
    }

    fun retrieveAdminCredential() {
        isLoading.value = true
        compositeDisposable.add(
            localDataUseCase.getAdminPassword()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        onAdminCredentialRetrieved(it)
                    }, {
                        onAdminCredentialError(it)
                    }
                )
        )
    }

    private fun onAdminCredentialError(it: Throwable?) {
        printLog(TAG, "Exception in retrieving Admin Password ::: ".plus(it?.message))
        this.isLoading.value = false
        message.value = context.getString(R.string.error_msg_unable_to_fetch_credential)
    }

    private fun onAdminCredentialRetrieved(it: String?) {
        printLog(TAG, "Retrieved Admin Password ::: ")
        this.isLoading.value = false
        adminCredential.value = it
    }

    fun getBannersForDisplay() {
        val banners = localDataUseCase.getBanners()
        if (banners.isEmpty()) {
            terminalBannersLiveData.value = null
        } else {
            terminalBannersLiveData.value = banners
        }
    }

    private fun addEmiTransactionToMenuItemListIfEnabled(menus: MutableList<MenuItem>) {
        if (isEmiTransactionEnabled()) {
            menus.add(MenuUtils.menuItemEmi())
        }
    }

    private fun addPinChangeTransactionToMenuItemListIfEnabled(menus: MutableList<MenuItem>) {
        if (isPinChangeEnabled()) {
            menus.add(MenuUtils.menuItemPinChange())
        }
    }

    private fun addGreenPinTransactionToMenuItemListIfEnabled(menus: MutableList<MenuItem>) {
        if (isGreenPinEnabled()) {
            menus.add(MenuUtils.menuItemGreenPin())
        }

    }

    private fun addBalanceInquiryToMenuItemListIfEnabled(menus: MutableList<MenuItem>) {
        if (isBalanceInquiryEnabled()) {
            menus.add(MenuUtils.menuItemBalanceInquiry())
        }

    }

    private fun addCashInToMenuItemListIfEnabled(menus: MutableList<MenuItem>) {
        if (isCashInEnabled()) {
            menus.add(MenuUtils.menuItemCashIn())
        }
    }

    private fun addMiniStatementToMenuItemList(menus: MutableList<MenuItem>) {
        if(isMiniStatementEnabled()){
            menus.add(MenuUtils.menuItemMiniStatement())
        }

    }


    private fun isEmiTransactionEnabled(): Boolean {
        val terminalSettingsJson = PreferenceManager.getTerminalSetting()
        return try {
            val terminalSettings = Jsons.fromJsonToObj(
                terminalSettingsJson,
                TerminalSettingResponse::class.java
            )
            terminalSettings.enableEmiScheme
        } catch (ex: Exception) {
            printLog(TAG, "EMI Transaction enabled " + ex.message)
            ex.printStackTrace()
            false
        }
    }

    private fun isPinChangeEnabled(): Boolean {
        return localDataUseCase.getGreenPinStatus()
    }

    private fun isGreenPinEnabled(): Boolean {
        return localDataUseCase.getGreenPinStatus()
    }

    private fun isBalanceInquiryEnabled(): Boolean {
        return localDataUseCase.getGreenPinStatus()
    }

    private fun isCashInEnabled(): Boolean {
        return localDataUseCase.getGreenPinStatus()
    }

    private fun isMiniStatementEnabled(): Boolean {
        return localDataUseCase.getGreenPinStatus()
    }

    private fun getTerminalSettings(): TerminalSettingResponse {
        return this.localDataUseCase.getTerminalSetting()
    }

    fun setScreenSaverObserverVariable(startScreenSaverTimer: Boolean, showScreenSaver: Boolean) {
        NiblMerchant.startScreenSaverTimer.value = startScreenSaverTimer
        NiblMerchant.showScreenSaver.value = showScreenSaver
    }

    fun getSettlementBatchNumber() {
        isLoading.value = true
        compositeDisposable.add(
            localDataUseCase.getExistingSettlementBatchNumber()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        isLoading.value = false
                        settlementBatchNumber.value = it
                    },
                    {
                        onErrorRetrievingSettlementBatchNumber(it)
                    }
                )
        )
    }

    private fun onErrorRetrievingSettlementBatchNumber(throwable: Throwable) {
        isLoading.value = false
        printLog(TAG, "Error in retrieving settlement batch ::: ".plus(throwable.message))
        message.value = context.getString(R.string.msg_unable_to_initiate_transaction)
    }

    fun isSettlementBatchLimitReached(existingBatchNumber: String): Boolean {
        if (existingBatchNumber.toInt() > Constants.MAX_BATCH_NUMBER_FOR_SETTLEMENT) {
            return true
        }
        return false
    }

    fun checkForSwitchConfigurationActive() {
        switchConfigActive.value = localDataUseCase.isSwitchConfigActive()
    }

    fun checkForAppResetActive() {
        appResetActive.value = localDataUseCase.isAppResetActive(false)
    }

    fun disableSwitchConfigActiveForOneSecond() {
        if (localDataUseCase.isSwitchConfigActive()) {
            localDataUseCase.setSwitchConfigActive(false)
            Handler().postDelayed({ localDataUseCase.setSwitchConfigActive(true) }, 1000)
        }
    }

    fun enableTransactionLogPush() {
        readyToStartTransactionLogPush.value = true
    }

    fun resetDevice() {
        compositeDisposable.add(
            localDataUseCase.nukeAllDatabaseTable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    localDataUseCase.clearAllPreferences()
                    transactionRepository.clearSharedPreference()
                    appNotificationUseCase.clearSharedPreference()
                    coreAutoReversalQueueUseCase.clear()
                    commandQueueUseCase.clear()
                    reconciliationRepository.clear()
                    qrPaymentUseCase.clear()

                    PreferenceManager.saveTerminalConfigPref(false)

                    canCloseApplication.value = true
                    isAppResetInProgress = false
                }, {
                    printLog(TAG, "Error in reset device and clearing device data" + it.message)
                    isAppResetInProgress = false
                })
        )
    }

    /*================== Merchant Transaction log push starts =======================*/

    private var localRepository = LocalRepositoryImpl(
        LocalDatabaseSourceImpl(),
        PreferenceManager
    )

    fun pushTransactionLogIfFeasible() {
        printLog(
            TAG,
            "Is Log Push Already in Progress ? ::: ".plus(TransactionPushService.isLogPushInProgress)
        )
        if (TransactionPushService.isLogPushInProgress) {
            isAppResetInProgress = false
            return
        }
        TransactionPushService.isLogPushInProgress = true
        getMerchantTransactionLogs()
    }

    private fun getMerchantTransactionLogs() {
        printLog(TAG, "Getting Merchant Transaction Logs...", true)
        compositeDisposable.add(
            localRepository.getMerchantTransactionLog()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    pushAvailableTransactionLogs(it)
                }, {
                    it.printStackTrace()
                    TransactionPushService.isLogPushInProgress = false
                    isAppResetInProgress = false
                    printLog(TAG, "Error in getting Merchant Transaction Log...")
                })
        )
    }

    private fun pushAvailableTransactionLogs(transactionLogs: List<MerchantTransactionLog>?) {
        if (transactionLogs == null || transactionLogs.isEmpty()) {
            printLog(TAG, "No Transaction Logs to Push...")
            TransactionPushService.isLogPushInProgress = false
            PreferenceManager.setAppResetActive(false)
            readyToResetDevice.value = true
            return
        }
        updateTransactionLogsToServer(transactionLogs)
    }

    private fun updateTransactionLogsToServer(transactionLogs: List<MerchantTransactionLog>) {
        if (!NiblMerchant.INSTANCE.hasInternetConnection()) {
            printLog(TAG, "No Internet Connection...")
            TransactionPushService.isLogPushInProgress = false
            return
        }
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<MerchantTransactionLogResponse> {
                onSubscribe(transactionLogs, it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        onNext(transactionLogs, it)
                    },
                    this::onError
                )
        )
    }

    private fun onSubscribe(
        merchantTransactionLogs: List<MerchantTransactionLog>?,
        emitter: ObservableEmitter<MerchantTransactionLogResponse>
    ) {
        val jsonRequest = Jsons.toJsonObj(merchantTransactionLogs)
        printLog(TAG, "Merchant Transaction Log Request::: $jsonRequest", true)
        val jsonResponse =
            NiblMerchant.INSTANCE.iPlatformManager.post(
                ApiConstant.UPDATE_MERCHANT_TRANSACTION_LOG_URL,
                jsonRequest,
                true
            )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            prepareResponse(emitter, response)
        } else {
            if (response.code == "401")
                emitter.onError(PosException(PosError.DEVICE_ERROR_OPERATOR_LOGIN_REQUIRED))
            else
                emitter.onError(Exception("Error Code :: ${response.code}. ${response.message}"))
        }
        emitter.onComplete()
    }

    private fun prepareResponse(
        emitter: ObservableEmitter<MerchantTransactionLogResponse>,
        data: Any
    ) {
        val responseJson = Jsons.toJsonObj(data)
        val response = Jsons.fromJsonToObj(responseJson, MerchantTransactionLogResponse::class.java)
        emitter.onNext(response)
    }

    private fun onNext(
        transactionLogs: List<MerchantTransactionLog>,
        response: MerchantTransactionLogResponse?
    ) {
        if (response == null) {
            printLog(TAG, "Response received after pushing Merchant Transaction Log is null...")
            TransactionPushService.isLogPushInProgress = false
            isAppResetInProgress = false
            return
        }
        clearMerchantTransactionLogTable()
    }

    private fun onError(throwable: Throwable) {
        TransactionPushService.isLogPushInProgress = false
        printLog(TAG, "Error Received While Pushing Merchant Transaction Log...")
        isAppResetInProgress = false
    }

    private fun clearMerchantTransactionLogTable() {
        compositeDisposable.add(
            localRepository.nukeMerchantTransactionLog()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    printLog(TAG, "Merchant Transaction Log Database Cleared", true)
                    TransactionPushService.isLogPushInProgress = false
                    PreferenceManager.setAppResetActive(false)
                    readyToResetDevice.value = true
                }, {
                    printLog(
                        TAG,
                        "Error in clearing Merchant Transaction Log Table" + it.message,
                        true
                    )
                    TransactionPushService.isLogPushInProgress = false
                    isAppResetInProgress = false
                })
        )
    }
}