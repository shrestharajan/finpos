package global.citytech.finpos.merchant.data.datasource.core.preauth.completion

import global.citytech.finpos.merchant.domain.model.auth.completion.AuthorisationCompletionResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.preauth.completion.AuthorisationCompletionRequestItem
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/29/2020.
 */
interface AuthorisationCompletionDataSource {
    fun doAuthorisationCompletion(
        configurationItem: ConfigurationItem,
        authorisationCompletionRequestItem: AuthorisationCompletionRequestItem
    ): Observable<AuthorisationCompletionResponseEntity>
}