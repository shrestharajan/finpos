package global.citytech.finposframework.switches;

/**
 * Created by Saurav Ghimire on 4/6/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public enum FunctionCode {
    LOG_ON,
    KEY_EXCHANGE,
    TRANSACTION,
    REVERSAL,
    RECONCILICATION,
    GREEN_PIN,
    PIN_CHANGE,
    BALANCE_INQUIRY,
    CASH_IN,
    MINI_STATEMENT
}
