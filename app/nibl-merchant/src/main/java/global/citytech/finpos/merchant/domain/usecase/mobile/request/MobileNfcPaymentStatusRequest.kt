package global.citytech.finpos.merchant.domain.usecase.mobile.request

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

/**
 * @author SIDDHARTHA GHIMIRE
 */
data class MobileNfcPaymentStatusRequest (
    @SerializedName("merchant_id")
    val merchantId: String,
    @SerializedName("terminal_id")
    val terminalId: String,
    val amount: BigDecimal,
    @SerializedName("request_number")
    val requestNumber: String,
    @SerializedName("currency_code")
    val currencyCode: Int,
    @SerializedName("merchant_bill_no")
    val merchantBillNumber: String,
    @SerializedName("merchant_txn_ref")
    val merchantTransactionReference: String,
    @SerializedName("local_txn_date_time")
    val localTransactionDateTime: String,
    val narration: String,
    val token: String,
    @SerializedName("issuer_code")
    val issuerCode: String,
    @SerializedName("payer_pan")
    val payerPan: String,
    @SerializedName("nfc_token")
    val nfcToken: String
)
