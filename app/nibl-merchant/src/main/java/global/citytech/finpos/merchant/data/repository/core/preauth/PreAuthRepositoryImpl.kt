package global.citytech.finpos.merchant.data.repository.core.preauth

import global.citytech.finpos.merchant.data.datasource.core.preauth.PreAuthDataSource
import global.citytech.finpos.merchant.domain.model.preauth.PreAuthResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.preauth.PreAuthRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.preauth.ManualPreAuthRequestItem
import global.citytech.finpos.merchant.presentation.model.preauth.PreAuthRequestItem
import io.reactivex.Observable

/**
 * Created by Rishav Chudal on 8/31/20.
 */
class PreAuthRepositoryImpl(private val preAuthDataSource: PreAuthDataSource): PreAuthRepository {
    override fun doPreAuth(
        configurationItem: ConfigurationItem,
        preAuthRequestItem: PreAuthRequestItem
    ): Observable<PreAuthResponseEntity> {
        return preAuthDataSource.doPreAuth(
            configurationItem,
            preAuthRequestItem
        )
    }

    override fun doManualPreAuth(
        configurationItem: ConfigurationItem,
        manualPreAuthRequestItem: ManualPreAuthRequestItem
    ): Observable<PreAuthResponseEntity> {
        return preAuthDataSource.doManualPreAuth(
            configurationItem,
            manualPreAuthRequestItem
        )
    }
}