package global.citytech.finpos.processor.neps.pinchange;

import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;

public class PinChangeResponse extends TransactionResponse {
    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private boolean approved;
        private String message;
        private String stan;
        private Result result;
        private String approvalCode;
        private boolean shouldPrintCustomerCopy;
        private ReadCardResponse readCardResponse;

        private Builder() {
        }

        public static PinChangeResponse.Builder newInstance() {
            return new PinChangeResponse.Builder();
        }

        public PinChangeResponse.Builder withDebugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public PinChangeResponse.Builder withDebugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public PinChangeResponse.Builder withShouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }
        public PinChangeResponse.Builder withApproved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public PinChangeResponse.Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public PinChangeResponse.Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }

        public PinChangeResponse.Builder withResult(Result result) {
            this.result = result;
            return this;
        }

        public PinChangeResponse.Builder withApprovalCode(String approvalCode) {
            this.approvalCode = approvalCode;
            return this;
        }

        public PinChangeResponse.Builder withReadCardResponse(ReadCardResponse readCardResponse){
            this.readCardResponse= readCardResponse;
            return this;
        }

        public PinChangeResponse build() {
            PinChangeResponse PinChangeResponse = new PinChangeResponse();
            PinChangeResponse.debugRequestMessage = this.debugRequestMessage;
            PinChangeResponse.debugResponseMessage = this.debugResponseMessage;
            PinChangeResponse.approved = this.approved;
            PinChangeResponse.message = this.message;
            PinChangeResponse.result = this.result;
            PinChangeResponse.approvalCode = this.approvalCode;
            PinChangeResponse.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            PinChangeResponse.stan = this.stan;
            PinChangeResponse.readCardResponse= this.readCardResponse;
            return PinChangeResponse;
        }
    }
}
