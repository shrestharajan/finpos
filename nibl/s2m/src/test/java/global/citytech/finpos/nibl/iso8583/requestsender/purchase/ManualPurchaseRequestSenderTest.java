package global.citytech.finpos.nibl.iso8583.requestsender.purchase;

import org.junit.Test;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.NIBLHostConfig;
import global.citytech.finpos.nibl.iso8583.MockValues;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Rishav Chudal on 10/16/20.
 */
public class ManualPurchaseRequestSenderTest {
    private Logger logger = Logger.getLogger(ManualPurchaseRequestSenderTest.class.getName());

    @Test
    public void send() {
        try {
            String terminalID = "99994773";
            String axquirerID = "405635629481338";
            TerminalInfo terminalInfo = new TerminalInfo(terminalID, axquirerID,"","Citytech","Kamaladi");
            HostInfo primaryHost = NIBLHostConfig.getPrimaryConfig();
            HostInfo secondaryHost = NIBLHostConfig.getSecondaryConfig();
            ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);
            RequestContext context =
                    new RequestContext(
                            "000490", terminalInfo, connectionParam);
            PurchaseRequest request = new PurchaseRequest();
            request.setTransactionAmount(1.0);
            request.setPosConditionCode("08");
//            request.setPinBlock("c247220c967c1d");
            request.setInvoiceNumber("000130");
//            request.setTrack2Data(MockValues.TRACK_2_DATA);
            request.setPosEntryMode(PosEntryMode.MANUAL);
            request.setPan(MockValues.PAN);
            request.setExpireDate(MockValues.EXPIRY_DATE);
            request.setCvv("610");
            context.setRequest(request);

            ManualPurchaseRequestSender sender = new ManualPurchaseRequestSender(
                    new NIBLSpecInfo(),
                    context
            );
            IsoMessageResponse response = sender.send();
            assertNotNull(response);

        } catch (FinPosException ex) {
            logger.log(ex.getMessage());
            logger.log(ex.getDetailMessage());
        }
    }
}