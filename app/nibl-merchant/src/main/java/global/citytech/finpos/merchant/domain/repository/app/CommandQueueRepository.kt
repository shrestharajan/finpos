package global.citytech.finpos.merchant.domain.repository.app

import global.citytech.finpos.merchant.domain.model.app.Command
import global.citytech.finpos.merchant.domain.model.app.Status
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/20/2021.
 */
interface CommandQueueRepository {

    fun isQueueEmpty(): Boolean

    fun hasActiveCommands(): Boolean

    fun getCount(): Int

    fun getAllCommands(): MutableList<Command>

    fun removeCommand(command: Command)

    fun addCommand(command: Command)

    fun updateCommandStatus(command: Command, status: Status, remarks: String)

    fun clear()
}