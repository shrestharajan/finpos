package global.citytech.finposframework.switches.receipt.summaryreport;

import global.citytech.finposframework.switches.ResponseParameter;

/**
 * Created by Unique Shakya on 1/5/2021.
 */
public interface SummaryReportResponseParameter extends ResponseParameter {
}
