package global.citytech.finpos.processor.nibl.transaction;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import global.citytech.finpos.processor.nibl.transaction.initializer.NiblTransactionInitializer;
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;

import static org.junit.Assert.assertEquals;

/**
 * Created by Unique Shakya on 10/9/2020.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ApplicationRepository.class, EmvParametersRequest.class})
public class NiblTransactionInitializerTest {

    ApplicationRepository applicationRepository;
    EmvParametersRequest emvParametersRequest;

    @Before
    public void setup(){
        applicationRepository = PowerMockito.mock(ApplicationRepository.class);
        emvParametersRequest = PowerMockito.mock(EmvParametersRequest.class);
    }

    @Test
    public void initializeTest(){
        PowerMockito.when(emvParametersRequest.component1()).thenReturn("12345");
        PowerMockito.when(applicationRepository.retrieveEmvParameterRequest())
                .thenReturn(this.emvParametersRequest);

        TransactionRequest transactionRequest = new TransactionRequest(TransactionType.PURCHASE);
        global.citytech.finpos.processor.nibl.transaction.initializer.NiblTransactionInitializer niblTransactionInitializer = new
                NiblTransactionInitializer(this.applicationRepository);
        TransactionRequest responseTransactionRequest = niblTransactionInitializer
                .initialize(transactionRequest);

        assertEquals("12345", responseTransactionRequest.getEmvParameterRequest().component1());
    }
}
