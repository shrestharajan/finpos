package global.citytech.finpos.merchant.domain.usecase.qr.fonepayqr

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

/**
 * @author siddhartha
 */
data class FonepayQrGenerateResponse(
    @SerializedName("request_number")
    val requestNumber: String,
    @SerializedName("merchant_id")
    val merchantId: String,
    @SerializedName("terminal_id")
    val terminalId: String,
    @SerializedName("merchant_bill_no")
    val merchantBillNumber: String,
    @SerializedName("merchant_txn_ref")
    val merchantTransactionReference: String,
    @SerializedName("provider_code")
    val providerCode: String,
    val amount: BigDecimal,
    @SerializedName("qr_content")
    val qrContent: String
)