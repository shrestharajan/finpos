package global.citytech.neps.presentation.alertdialogs

class TransactionConfirmation constructor(builder: Builder) {

    var imageId: Int = 0
    var title: String
    var message: String
    var positiveLabel: String
    var negativeLabel: String

    init {
        this.title = builder.title
        this.message = builder.message
        this.positiveLabel = builder.positiveLabel
        this.negativeLabel = builder.negativeLabel
        this.imageId = builder.imageId
    }

    class Builder {

        lateinit var title: String
        lateinit var message: String
        lateinit var positiveLabel: String
        lateinit var negativeLabel: String
        var imageId: Int = 0

        fun title(title: String) = apply { this.title = title }
        fun message(message: String) = apply { this.message = message }
        fun positiveLabel(positiveLabel: String) = apply { this.positiveLabel = positiveLabel }
        fun negativeLabel(negativeLabel: String) = apply { this.negativeLabel = negativeLabel }
        fun imageId(imageId: Int) = apply { this.imageId = imageId }

        fun build(): TransactionConfirmation {
            return TransactionConfirmation(this)
        }
    }

    override fun toString(): String {
        return "TransactionConfirmation(imageId=$imageId, title='$title', message='$message', positiveLabel='$positiveLabel', negativeLabel='$negativeLabel')"
    }
}