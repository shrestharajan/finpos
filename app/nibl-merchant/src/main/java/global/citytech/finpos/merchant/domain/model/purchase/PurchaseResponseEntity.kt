package global.citytech.finpos.merchant.domain.model.purchase


/**
 * Created by Saurav Ghimire on 1/20/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
data class PurchaseResponseEntity(
    val stan:String? = null,
    val message: String = "",
    val isApproved: Boolean = false,
    val shouldPrintCustomerCopy: Boolean = false
)