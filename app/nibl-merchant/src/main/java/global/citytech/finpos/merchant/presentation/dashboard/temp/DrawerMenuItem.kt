package global.citytech.finpos.merchant.presentation.dashboard.temp

import android.app.Activity
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuHandler

/**
 * Created by Unique Shakya on 6/18/2021.
 */
data class DrawerMenuItem(val iconId: Int, val title: String,val menuHandler: MenuHandler)
