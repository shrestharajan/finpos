package global.citytech.finpos.processor.neps.keyexchange;

import global.citytech.finpos.neps.iso8583.KeyExchangeRequestSender;
import global.citytech.finpos.neps.receipt.keyexchange.NepsKeyExchangeReceiptMaker;
import global.citytech.finpos.neps.receipt.keyexchange.NepsKeyExchangeReceiptPrintHandler;
import global.citytech.finpos.processor.neps.transaction.NepsIsoMessageReceiptHandler;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.hardware.common.DeviceResponse;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.keymgmt.keys.KeyAlgorithm;
import global.citytech.finposframework.hardware.keymgmt.keys.KeyService;
import global.citytech.finposframework.hardware.keymgmt.keys.KeyType;
import global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyInjectionRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.keyexchange.KeyExchangeRequester;
import global.citytech.finposframework.usecases.activity.ActivityLog;
import global.citytech.finposframework.usecases.activity.ActivityLogStatus;
import global.citytech.finposframework.usecases.activity.ActivityLogType;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.receipt.keyexchange.KeyExchangeReceipt;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finposframework.hardware.utility.DeviceApiConstants.MASTER_KEY_INDEX;
import static global.citytech.finposframework.hardware.utility.DeviceApiConstants.PEK_KEY_INDEX;

import java.sql.Timestamp;
import java.util.Date;

/**
 * User: Surajchhetry Date: 2/28/20 Time: 6:02 PM
 * Modified : Rishav Chudal Date: 10/04/21 Time: 12:44 PM
 */
public class MerchantKeyExchangeUseCase
        implements UseCase<KeyExchangeRequestModel, KeyExchangeResponseModel>,
        KeyExchangeRequester<KeyExchangeRequestModel, KeyExchangeResponseModel> {

    private final TerminalRepository repository;
    private final Logger logger = Logger.getLogger(MerchantKeyExchangeUseCase.class.getName());
    private IsoMessageResponse keyExchangeIsoMessageResponse;
    private KeyExchangeRequestModel keyExchangeRequestModel;
    private KeyExchangeResponseModel keyExchangeResponseModel;

    public MerchantKeyExchangeUseCase(TerminalRepository repository) {
        this.repository = repository;
    }

    private RequestContext prepareKeyExchangeRequestContext() {
        HostInfo primaryHostInfo = this.repository.findPrimaryHost();
        HostInfo secondaryHostInfo = this.repository.findSecondaryHost();
        TerminalInfo terminalInfo = this.repository.findTerminalInfo();
        ConnectionParam connectionParam = new ConnectionParam(primaryHostInfo, secondaryHostInfo);
        return new RequestContext(this.repository.getSystemTraceAuditNumber(), terminalInfo, connectionParam);
    }

    @Override
    public KeyExchangeResponseModel execute(KeyExchangeRequestModel keyExchangeRequestModel) {
        try {
            this.keyExchangeRequestModel = keyExchangeRequestModel;
            sendKeyExchangeRequestToHost();
            updateActivityLogWithIsoMessageResponse(keyExchangeIsoMessageResponse);
            increaseSystemTraceAuditNumber();
            validateKeyExchangeIsoResponse();
            return keyExchangeResponseModel;
        }
        catch (FinPosException finPosException) {
            finPosException.printStackTrace();
            updateActivityLogFailedWithFinPosException(finPosException);
        } catch (Exception exception) {
            exception.printStackTrace();
            updateActivityLogFailedWithRemarks(exception.getMessage());
        }
        assignFailedResponseModel();
        return keyExchangeResponseModel;
    }

    private void sendKeyExchangeRequestToHost() {
        RequestContext keyExchangeRequestContext = prepareKeyExchangeRequestContext();
        logger.debug(String.format("Key Exchange Request Context ::%s", keyExchangeRequestContext));
        KeyExchangeRequestSender sender = new KeyExchangeRequestSender(keyExchangeRequestContext);
        keyExchangeIsoMessageResponse = sender.send();
    }

    private void increaseSystemTraceAuditNumber() {
        repository.incrementSystemTraceAuditNumber();
    }

    private void validateKeyExchangeIsoResponse() {
        String pinEncryptionKey = getPekFromKeyExchangeResponse();
        this.logger.debug("PEK ::: " + pinEncryptionKey);
        if (isSuccessKeyExchangeResponse() && !StringUtils.isEmpty(pinEncryptionKey)) {
            injectPinEncryptionKey(pinEncryptionKey);
        } else {
            onFailureKeyExchange();
        }
    }

    private void injectPinEncryptionKey(String pinEncryptionKey) {
        KeyInjectionRequest keyInjectionRequest = prepareKeyInjectionRequest(pinEncryptionKey);
        KeyService hardwareKeyService = keyExchangeRequestModel.getHardwareKeyService();
        DeviceResponse keyInjectionResponse = hardwareKeyService.injectKey(keyInjectionRequest);
        validateKeyInjectionResponse(keyInjectionResponse);
    }

    private KeyInjectionRequest prepareKeyInjectionRequest(String pinEncryptionKey) {
        KeyInjectionRequest keyInjectionRequest = new KeyInjectionRequest(
                this.keyExchangeRequestModel.getApplicationPackageName(),
                KeyType.KEY_TYPE_PEK,
                KeyAlgorithm.TYPE_3DES,
                pinEncryptionKey.toUpperCase(),
                false
        );
        keyInjectionRequest.setKeyIndex(PEK_KEY_INDEX);
        keyInjectionRequest.setProtectKeyType(KeyType.KEY_TYPE_TMK);
        keyInjectionRequest.setProtectKeyIndex(MASTER_KEY_INDEX);
        return keyInjectionRequest;
    }

    private void validateKeyInjectionResponse(DeviceResponse keyInjectionResponse) {
        this.logger.debug("Key Injection Result ::: " + keyInjectionResponse.getResult());
        this.logger.debug("Key Injection Message ::: " + keyInjectionResponse.getMessage());
        if (keyInjectionResponse.getResult() == Result.SUCCESS) {
            onSuccessKeyInjectionResponse();
        } else {
            onFailureKeyExchange();
        }
    }

    private void onSuccessKeyInjectionResponse() {
        prepareAndPrintReceipt();
        assignSuccessResponseModel();
    }

    private void assignSuccessResponseModel() {
        KeyExchangeResponseModel.Builder builder = KeyExchangeResponseModel
                .Builder.createDefaultBuilder()
                .success(true)
                .debugRequestMessage(keyExchangeIsoMessageResponse.getDebugRequestString())
                .debugResponseMessage(keyExchangeIsoMessageResponse.getDebugResponseString());
        keyExchangeResponseModel = builder.build();
    }

    private void onFailureKeyExchange() {
        prepareAndPrintReceipt();
        assignFailedResponseModel();
    }

    private void prepareAndPrintReceipt() {
        printDebugReceiptIfDebugMode();
        KeyExchangeReceipt keyExchangeReceipt =
                new NepsKeyExchangeReceiptMaker(keyExchangeIsoMessageResponse).prepare();
        ReceiptHandler.KeyExchangeReceiptHandler receiptHandler =
                new NepsKeyExchangeReceiptPrintHandler(keyExchangeRequestModel.getPrinterService());
        receiptHandler.printKeyExchangeReceipt(keyExchangeReceipt);
    }

    private void printDebugReceiptIfDebugMode() {
        if (isAdminDebugMode()) {
            ReceiptHandler.IsoMessageReceiptHandler debugReceiptHandler =
                    new NepsIsoMessageReceiptHandler(keyExchangeRequestModel.getPrinterService());
            debugReceiptHandler.printIsoMessageReceipt(keyExchangeIsoMessageResponse);
        }
    }

    private void assignFailedResponseModel() {
        KeyExchangeResponseModel.Builder builder = KeyExchangeResponseModel
                .Builder.createDefaultBuilder()
                .success(false)
                .debugRequestMessage(keyExchangeIsoMessageResponse.getDebugRequestString())
                .debugResponseMessage(keyExchangeIsoMessageResponse.getDebugResponseString());
        keyExchangeResponseModel = builder.build();
    }

    private boolean isSuccessKeyExchangeResponse() {
        int responseCode = IsoMessageUtils.retrieveFromDataElementsAsInt(
                keyExchangeIsoMessageResponse,
                DataElement.RESPONSE_CODE
        );
        return responseCode == 0;
    }

    private String getPekFromKeyExchangeResponse() {
        return IsoMessageUtils.retrieveFromDataElementsAsString(
                keyExchangeIsoMessageResponse,
                DataElement.SECURITY_RELATED_CONTROL_INFORMATION
        );
    }

    private boolean isAdminDebugMode() {
        return repository.findTerminalInfo().isDebugModeEnabled();
    }


    private void updateActivityLogWithIsoMessageResponse(IsoMessageResponse isoMessageResponse) {
        try {
            TerminalInfo terminalInfo = this.repository.findTerminalInfo();
            String responseCode = IsoMessageUtils.retrieveFromDataElementsAsString(
                    isoMessageResponse,
                    DataElement.RESPONSE_CODE
            );
            String isoResponse = "";
            ActivityLogStatus status = ActivityLogStatus.SUCCESS;
            if (!responseCode.equals("000")) {
                status = ActivityLogStatus.FAILED;
                isoResponse = isoMessageResponse.getDebugRequestString();
            }
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.repository.getSystemTraceAuditNumber())
                    .withType(ActivityLogType.KEY_EXCHANGE)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber("")
                    .withAmount(0L)
                    .withResponseCode(responseCode)
                    .withIsoRequest(isoMessageResponse.getDebugRequestString())
                    .withIsoResponse(isoResponse)
                    .withStatus(status)
                    .withRemarks("")
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(
                            StringUtils.prepareAdditionalDataForActivityLog(
                                    this.repository.getTerminalSerialNumber()
                            )
                    )
                    .build();
            this.repository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update KEY EXCHANGE activity log :: " + e.getMessage());
        }
    }

    private void updateActivityLogFailedWithFinPosException(FinPosException finPosException) {
        try {
            TerminalInfo terminalInfo = this.repository.findTerminalInfo();
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.repository.getSystemTraceAuditNumber())
                    .withType(ActivityLogType.KEY_EXCHANGE)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber("")
                    .withAmount(0L)
                    .withResponseCode("")
                    .withIsoRequest(finPosException.getIsoRequest())
                    .withIsoResponse("")
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(
                            finPosException.getLocalizedMessage() +
                                    " : " +
                                    finPosException.getDetailMessage()
                    )
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(
                            StringUtils.prepareAdditionalDataForActivityLog(
                                    this.repository.getTerminalSerialNumber()
                            )
                    )
                    .build();
            this.repository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update KEY EXCHANGE activity log :: " + e.getMessage());
            updateActivityLogFailedWithRemarks(e.getMessage());
        }
    }

    private void updateActivityLogFailedWithRemarks(String remarks) {
        try {
            TerminalInfo terminalInfo = this.repository.findTerminalInfo();
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.repository.getSystemTraceAuditNumber())
                    .withType(ActivityLogType.KEY_EXCHANGE)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber("")
                    .withAmount(0L)
                    .withResponseCode("")
                    .withIsoRequest("")
                    .withIsoResponse("")
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(remarks)
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(
                            StringUtils.prepareAdditionalDataForActivityLog(
                                    this.repository.getTerminalSerialNumber()
                            )
                    )
                    .build();
            this.repository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update KEY EXCHANGE activity log :: " + e.getMessage());
        }
    }

}
