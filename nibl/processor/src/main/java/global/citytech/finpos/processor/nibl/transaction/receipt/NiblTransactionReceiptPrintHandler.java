package global.citytech.finpos.processor.nibl.transaction.receipt;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.hardware.io.printer.Printable;
import global.citytech.finposframework.hardware.io.printer.PrinterRequest;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.data.EmiInfo;
import global.citytech.finposframework.usecases.transaction.data.StatementList;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.EmvTags;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.TransactionInfo;
import global.citytech.finposframework.utility.JsonUtils;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finposframework.utility.ReceiptUtils.addBarCodeImage;
import static global.citytech.finposframework.utility.ReceiptUtils.addBase64Image;
import static global.citytech.finposframework.utility.ReceiptUtils.addDoubleColumnString;
import static global.citytech.finposframework.utility.ReceiptUtils.addSingleColumnString;

public class NiblTransactionReceiptPrintHandler implements ReceiptHandler.TransactionReceiptHandler {

    private PrinterService printerService;

    public NiblTransactionReceiptPrintHandler(PrinterService printerService) {
        this.printerService = printerService;
    }

    @Override
    public PrinterResponse printTransactionReceipt(ReceiptLog receiptLog, ReceiptVersion receiptVersion) {
        PrinterRequest printerRequest = this.preparePrinterRequest(receiptLog, receiptVersion);
        return printerService.print(printerRequest);
    }

    @Override
    public PrinterResponse printTransactionReceipt(ReceiptLog receiptLog, ReceiptVersion receiptVersion, StatementList statementList) {
        return null;
    }

    private PrinterRequest preparePrinterRequest(ReceiptLog receiptLog, ReceiptVersion receiptVersion) {
        List<Printable> printableList = new ArrayList<>();
        if (receiptLog.getRetailer().getRetailerLogo() != null)
            addBase64Image(printableList, receiptLog.getRetailer().getRetailerLogo());
        addSingleColumnString(printableList, receiptLog.getRetailer().getRetailerName(), TransactionReceiptStyle.RETAILER_NAME.getStyle());
        addSingleColumnString(printableList, receiptLog.getRetailer().getRetailerAddress(), TransactionReceiptStyle.RETAILER_ADDRESS.getStyle());
        addFeedPaper(printableList);
        addDoubleColumnString(printableList, this.prepareDateString(receiptLog.getPerformance().getStartDateTime()), this.prepareTimeString(receiptLog.getPerformance().getStartDateTime()), TransactionReceiptStyle.START_DATE_TIME.getStyle());
        addDoubleColumnString(printableList, this.merchantId(receiptLog.getRetailer().getMerchantId()), this.terminalId(receiptLog.getRetailer().getTerminalId()), TransactionReceiptStyle.MID_TID.getStyle());
        addDoubleColumnString(printableList, this.rrn(receiptLog.getTransactionInfo().getRrn()), this.stan(receiptLog.getTransactionInfo().getStan()), TransactionReceiptStyle.RRN_STAN.getStyle());
        addDoubleColumnString(printableList, TransactionReceiptLabels.BATCH_NUMBER_LABEL, receiptLog.getTransactionInfo().getBatchNumber(), TransactionReceiptStyle.BATCH_NUMBER.getStyle());
        addDoubleColumnString(printableList, TransactionReceiptLabels.INVOICE_NUMBER_LABEL, receiptLog.getTransactionInfo().getInvoiceNumber(), TransactionReceiptStyle.INVOICE_NUMBER.getStyle());
        addReceiptVersionToPrintableListIfOnlyDuplicateCopy(printableList, receiptVersion);
        addTransactionTypeLabelFromReceiptLogToPrintList(printableList, receiptLog);
        this.addAmountLine(printableList, receiptLog);
        addSingleColumnString(printableList, receiptLog.getTransactionInfo().getTransactionStatus(), TransactionReceiptStyle.TRANSACTION_STATUS.getStyle());
        if (!receiptLog.getTransactionInfo().isSignatureRequired()) {
            addSingleColumnString(printableList, receiptLog.getTransactionInfo().getTransactionMessage(), TransactionReceiptStyle.TRANSACTION_MESSAGE.getStyle());
        }
        if (receiptLog.getTransactionInfo().isSignatureRequired()) {
            addSingleColumnString(printableList, TransactionReceiptLabels.SIGNATURE_MESSAGE_CUSTOMER, TransactionReceiptStyle.SIGNATURE_MESSAGE.getStyle());
            addSingleColumnString(printableList, TransactionReceiptLabels.SIGNATURE_LINE, TransactionReceiptStyle.SIGNATURE_LINE.getStyle());
            if (receiptLog.getTransactionInfo().isDebitAcknowledgementRequired())
                addSingleColumnString(printableList, TransactionReceiptLabels.DEBIT_ACCOUNT_ACKNOWLEDGEMENT, TransactionReceiptStyle.DEBIT_CREDIT_ACK.getStyle());
        }
        addSingleColumnString(printableList, receiptLog.getTransactionInfo().getCardHolderName(), TransactionReceiptStyle.CARD_HOLDER_NAME.getStyle());
        addDoubleColumnString(printableList, TransactionReceiptLabels.CARD_NUMBER, StringUtils.encodeAccountNumber(receiptLog.getTransactionInfo().getCardNumber()), TransactionReceiptStyle.CARD_NUMBER.getStyle());
        addDoubleColumnString(printableList, TransactionReceiptLabels.CARD_TYPE, receiptLog.getTransactionInfo().getCardScheme(), TransactionReceiptStyle.CARD_SCHEME.getStyle());
        addDoubleColumnString(printableList, TransactionReceiptLabels.AID, receiptLog.getEmvTags().getAid(), TransactionReceiptStyle.AID.getStyle());
        addDoubleColumnString(printableList, TransactionReceiptLabels.APPROVAL_CODE_LABEL, receiptLog.getTransactionInfo().getApprovalCode(), TransactionReceiptStyle.APPROVAL_CODE.getStyle());
        addEmiLabelToPrintListIfEmiTransaction(printableList, receiptLog.getTransactionInfo());
        addSingleColumnString(printableList, this.emvTagsString(receiptLog.getEmvTags()), TransactionReceiptStyle.EMV_TAG_DATA.getStyle());
        addSingleColumnString(printableList, TransactionReceiptLabels.THANK_YOU_MESSAGE, TransactionReceiptStyle.THANK_YOU_MESSAGE.getStyle());
        addReceiptVersionToPrintableListIfNotDuplicateCopy(printableList, receiptVersion);
        if (!StringUtils.isEmpty(receiptLog.getBarCodeString()))
            addBarCode(printableList, receiptLog);
        return new PrinterRequest(printableList);
    }

    private void addEmiLabelToPrintListIfEmiTransaction(
            List<Printable> printableList,
            TransactionInfo transactionInfo
    ) {
        if (transactionInfo.isEmiTransaction()) {
            addSingleColumnString(
                    printableList,
                    TransactionReceiptLabels.EMI_TRANSACTION_LABEL,
                    TransactionReceiptStyle.RECEIPT_VERSION.getStyle()
            );
            addEmiInfo(printableList, transactionInfo);
        } else {
            addFeedPaper(printableList);
        }
    }

    private void addEmiInfo(List<Printable> printableList, TransactionInfo transactionInfo) {
        try {
            EmiInfo emiInfo = JsonUtils.fromJsonToObj(transactionInfo.getEmiInfoJson(), EmiInfo.class);
            addSingleColumnString(printableList, TransactionReceiptLabels.DIVIDER, TransactionReceiptStyle.DIVIDER.getStyle());
            addDoubleColumnString(printableList, TransactionReceiptLabels.EMI_MONTHLY_EMI_AMOUNT_LABEL, transactionInfo.getTransactionCurrencyName()
                    + " " + StringUtils.formatAmountTwoDecimal(emiInfo.getEmiAmount()), TransactionReceiptStyle.EMI_DETAIL.getStyle());
            addDoubleColumnString(printableList, TransactionReceiptLabels.EMI_INTEREST_RATE_LABEL, String.valueOf(emiInfo.getRate() + "%"),
                    TransactionReceiptStyle.EMI_DETAIL.getStyle());
            addDoubleColumnString(printableList, TransactionReceiptLabels.EMI_TIME_PERIOD_LABEL,
                    (int) emiInfo.getTimePeriod() + " months", TransactionReceiptStyle.EMI_DETAIL.getStyle());
            addSingleColumnString(printableList, TransactionReceiptLabels.DIVIDER, TransactionReceiptStyle.DIVIDER.getStyle());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addBarCode(List<Printable> printableList, ReceiptLog receiptLog) {
        addBarCodeImage(printableList, receiptLog.getBarCodeString());
        addSingleColumnString(printableList, receiptLog.getTransactionInfo().getRrn(), TransactionReceiptStyle.BAR_CODE_LABEL.getStyle());
    }

    private void addFeedPaper(List<Printable> printableList) {
        addSingleColumnString(printableList, "  ", TransactionReceiptStyle.DIVIDER.getStyle());
    }

    private void addAmountLine(List<Printable> printableList, ReceiptLog receiptLog) {
        if (StringUtils.isEmpty(receiptLog.getTransactionInfo().getBaseAmount())) {
            addSingleColumnString(printableList, TransactionReceiptLabels.DIVIDER, TransactionReceiptStyle.DIVIDER.getStyle());
            addDoubleColumnString(printableList, TransactionReceiptLabels.AMOUNT_LABEL,
                    this.amountWithCurrencyName(receiptLog.getTransactionInfo().getTransactionCurrencyName(),
                            receiptLog.getTransactionInfo().getPurchaseAmount()),
                    TransactionReceiptStyle.AMOUNT.getStyle());
            if (receiptLog.getTransactionInfo().isTipEnabled()) {
                addDoubleColumnString(printableList, TransactionReceiptLabels.TIP_AMOUNT_LABEL,
                        "  ",
                        TransactionReceiptStyle.AMOUNT.getStyle());
            }
            addSingleColumnString(printableList, TransactionReceiptLabels.DIVIDER, TransactionReceiptStyle.DIVIDER.getStyle());
        } else {
            addSingleColumnString(printableList, TransactionReceiptLabels.DIVIDER, TransactionReceiptStyle.DIVIDER.getStyle());
            addDoubleColumnString(printableList, TransactionReceiptLabels.AMOUNT_LABEL,
                    this.amountWithCurrencyName(receiptLog.getTransactionInfo().getTransactionCurrencyName(),
                            receiptLog.getTransactionInfo().getBaseAmount()),
                    TransactionReceiptStyle.AMOUNT.getStyle());
            addDoubleColumnString(printableList, TransactionReceiptLabels.TIP_AMOUNT_LABEL,
                    this.amountWithCurrencyName(receiptLog.getTransactionInfo().getTransactionCurrencyName(),
                            receiptLog.getTransactionInfo().getPurchaseAmount()),
                    TransactionReceiptStyle.AMOUNT.getStyle());
            addSingleColumnString(printableList, TransactionReceiptLabels.DIVIDER, TransactionReceiptStyle.DIVIDER.getStyle());
            addDoubleColumnString(printableList, TransactionReceiptLabels.TOTAL_AMOUNT_LABEL,
                    this.amountWithCurrencyName(receiptLog.getTransactionInfo().getTransactionCurrencyName(),
                            receiptLog.getTransactionInfo().getTotalAmount()),
                    TransactionReceiptStyle.AMOUNT.getStyle());
            addSingleColumnString(printableList, TransactionReceiptLabels.DIVIDER, TransactionReceiptStyle.DIVIDER.getStyle());
        }
    }

    private void addTransactionTypeLabelFromReceiptLogToPrintList(
            List<Printable> printableList,
            ReceiptLog receiptLog
    ) {
        addSingleColumnString(
                printableList,
                receiptLog.getTransactionInfo().getPurchaseType(),
                TransactionReceiptStyle.PURCHASE_TYPE.getStyle()
        );
    }

    private String emvTagsString(EmvTags emvTags) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(emvTags.getCardType());
        stringBuilder.append("\t");
        stringBuilder.append(emvTags.getResponseCode());
        stringBuilder.append("\t");
        stringBuilder.append(emvTags.getAid());
        stringBuilder.append("\t");
        stringBuilder.append(emvTags.getIin());
        stringBuilder.append("\t");
        stringBuilder.append(emvTags.getTvr());
        stringBuilder.append("\t");
        stringBuilder.append(emvTags.getTsi());
        stringBuilder.append("\t");
        stringBuilder.append(emvTags.getCvm());
        stringBuilder.append("\t");
        stringBuilder.append(emvTags.getCid());
        stringBuilder.append("\t");
        stringBuilder.append(emvTags.getAc());
        stringBuilder.append("\t");
        stringBuilder.append(emvTags.getKernelId());
        return stringBuilder.toString();
    }

    private String amountWithCurrencyName(String currencyName, String amount) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(currencyName);
        stringBuilder.append("\t");
        stringBuilder.append(amount);
        return stringBuilder.toString();
    }

    private String rrn(String rrn) {
        return this.formatLabelWithValue(TransactionReceiptLabels.RRN_LABEL, rrn);
    }

    private String invoiceNumber(String invoiceNumber) {
        return this.formatLabelWithValue(TransactionReceiptLabels.INVOICE_NUMBER_LABEL, invoiceNumber);
    }

    private String formatLabelWithValue(String label, String value) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(label);
        stringBuilder.append(": ");
        stringBuilder.append(value);
        return stringBuilder.toString();
    }

    private String stan(String stan) {
        return this.formatLabelWithValue(TransactionReceiptLabels.STAN_LABEL, stan);
    }

    private String batchNumber(String batchNumber) {
        return this.formatLabelWithValue(TransactionReceiptLabels.BATCH_NUMBER_LABEL, batchNumber);
    }

    private String merchantId(String merchantId) {
        return this.formatLabelWithValue(TransactionReceiptLabels.MERCHANT_ID_LABEL, merchantId);
    }

    private String terminalId(String terminalId) {
        return this.formatLabelWithValue(TransactionReceiptLabels.TERMINAL_ID_LABEL, terminalId);
    }

    private String prepareTimeString(String dateTime) {
        if (StringUtils.isEmpty(dateTime) || dateTime.length() < 15)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(dateTime, 6, 8);
        stringBuilder.append(":");
        stringBuilder.append(dateTime, 8, 10);
        stringBuilder.append(":");
        stringBuilder.append(dateTime, 10, 12);
        return this.formatLabelWithValue(TransactionReceiptLabels.TIME_LABEL, stringBuilder.toString());
    }

    private String prepareDateString(String dateTime) {
        if (StringUtils.isEmpty(dateTime) || dateTime.length() < 15)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(dateTime, 4, 6);
        stringBuilder.append("/");
        stringBuilder.append(dateTime, 2, 4);
        stringBuilder.append("/");
        stringBuilder.append("20");
        stringBuilder.append(dateTime, 0, 2);
        return this.formatLabelWithValue(TransactionReceiptLabels.DATE_LABEL, stringBuilder.toString());
    }

    private void addReceiptVersionToPrintableListIfOnlyDuplicateCopy(
            List<Printable> printableList,
            ReceiptVersion receiptVersion
    ) {
        if (isReceiptVersionDuplicateCopy(receiptVersion)) {
            addSingleColumnString(
                    printableList,
                    receiptVersion.getVersionLabel(),
                    TransactionReceiptStyle.RECEIPT_VERSION.getStyle()
            );
//            addFeedPaper(printableList);
        }
    }

    private void addReceiptVersionToPrintableListIfNotDuplicateCopy(
            List<Printable> printableList,
            ReceiptVersion receiptVersion
    ) {
        if (!isReceiptVersionDuplicateCopy(receiptVersion)) {
            addSingleColumnString(
                    printableList,
                    receiptVersion.getVersionLabel(),
                    TransactionReceiptStyle.RECEIPT_VERSION.getStyle()
            );
        } else {
            addSingleColumnString(
                    printableList,
                    "* * * *",
                    TransactionReceiptStyle.RECEIPT_VERSION.getStyle()
            );
        }
    }

    private boolean isReceiptVersionDuplicateCopy(ReceiptVersion receiptVersion) {
        return (receiptVersion == ReceiptVersion.DUPLICATE_COPY);
    }
}
