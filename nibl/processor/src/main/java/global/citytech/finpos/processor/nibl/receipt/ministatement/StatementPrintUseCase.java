package global.citytech.finpos.processor.nibl.receipt.ministatement;

import global.citytech.finpos.processor.nibl.receipt.customercopy.CustomerCopyRequestModel;
import global.citytech.finpos.processor.nibl.receipt.customercopy.CustomerCopyResponseModel;
import global.citytech.finpos.processor.nibl.transaction.receipt.NiblStatementPrintHandler;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.ministatement.StatementRequester;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;

public class StatementPrintUseCase implements UseCase<CustomerCopyRequestModel, CustomerCopyResponseModel>,
        StatementRequester<CustomerCopyRequestModel, CustomerCopyResponseModel> {

    @Override
    public CustomerCopyResponseModel execute(CustomerCopyRequestModel request) {
        ReceiptLog receiptLog = request.getTransactionRepository().getReceiptLog();
        NiblStatementPrintHandler transactionReceiptHandler = new
                NiblStatementPrintHandler(request.getPrinterService());
        PrinterResponse printerResponse = transactionReceiptHandler
                .printTransactionReceipt(receiptLog, ReceiptVersion.CUSTOMER_COPY, request.getStatementList());
        return new CustomerCopyResponseModel(
                printerResponse.getResult(),
                printerResponse.getMessage()
        );
    }
}