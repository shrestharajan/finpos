package global.citytech.finpos.merchant.presentation.utils

import global.citytech.common.data.LabelValue
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finposframework.utility.StringUtils
import java.math.BigDecimal
import java.text.DecimalFormat

/**
 * Created by Rishav Chudal on 8/11/20.
 */
class StringUtils {
    companion object {
        fun decimalFormatterFor(enteredAmount: Long): String {
            val doubleDF = DecimalFormat("#0.00")
            return doubleDF.format(enteredAmount / 100.00)
        }

        fun decimalFormatterHigherCurrency(amount: BigDecimal): String {
            val decimalFormat = DecimalFormat("#0.00")
            return decimalFormat.format(amount)
        }

        fun createStringList(vararg args: String): MutableList<String> {
            return java.util.ArrayList(listOf(*args))
        }

        fun getLabelFromLabelValue(item: String?): String {
            var label = ""
            if (item != null) {
                val labelValue = Jsons.fromJsonToObj(item, LabelValue::class.java)
                if (labelValue?.printLabel != null) {
                    label = labelValue.printLabel.toString()
                }
            }
            return label
        }

        fun getValueFromLabelValue(
            item: String?
        ): String {
            var value = ""
            if (item != null) {
                val labelValue = Jsons.fromJsonToObj(item, LabelValue::class.java)
                if (labelValue?.value != null) {
                    value = labelValue.value.toString()
                }
            }
            return value
        }

        fun getValueOrEmpty(value: String?): String {
            if (StringUtils.isEmpty(value)) {
                return StringUtils.TAG_EMPTY_STRING
            }
            return value!!
        }

        fun getDividerFor(label: String?): String {
            val singleDash = "-"
            var multiDash = ""
            if (StringUtils.isEmpty(label)) {
                for (x in 1..24) {
                    multiDash = multiDash.plus(singleDash)
                }
                return multiDash
            } else {
                val reqLength = label!!.length.plus(4)
                for (x in 1..reqLength) {
                    multiDash = multiDash.plus(singleDash)
                }
            }
            return multiDash
        }
    }
}