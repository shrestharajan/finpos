package global.citytech.finposframework.hardware.io.led

/**
 * Created by Rishav Chudal on 6/4/20.
 */
enum class LedAction {
    ON, OFF
}