package global.citytech.finposframework.switches.receipt.tmslogs;

import global.citytech.finposframework.switches.ResponseParameter;

/**
 * Created by Rishav Chudal on 11/5/20.
 */
public interface TmsLogsResponseParameter extends ResponseParameter {
}
