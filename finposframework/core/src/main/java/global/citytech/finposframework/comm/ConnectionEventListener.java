package global.citytech.finposframework.comm;

import global.citytech.finposframework.supports.Context;

/** User: Surajchhetry Date: 2/20/20 Time: 2:12 PM */
public interface ConnectionEventListener {
  void listen(ConnectionEvent event, Context context);
}
