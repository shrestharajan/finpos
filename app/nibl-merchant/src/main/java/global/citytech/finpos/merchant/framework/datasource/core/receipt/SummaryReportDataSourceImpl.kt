package global.citytech.finpos.merchant.framework.datasource.core.receipt

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.receipt.SummaryReportDataSource
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.ReconciliationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.domain.model.receipt.summaryreport.SummaryReportRequestEntity
import global.citytech.finpos.merchant.domain.model.receipt.summaryreport.SummaryReportResponseEntity
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.data.mapToSummaryReportResponseUIModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finpos.processor.nibl.receipt.summaryreport.SummaryReportRequestModel
import global.citytech.finpos.processor.nibl.receipt.summaryreport.SummaryReportResponseModel
import global.citytech.finposframework.switches.ProcessorManager
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 1/5/2021.
 */
class SummaryReportDataSourceImpl : SummaryReportDataSource {
    override fun print(configurationItem: ConfigurationItem): Observable<SummaryReportResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val reconciliationRepository = ReconciliationRepositoryImpl()
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
        val printerService = PrinterSourceImpl(NiblMerchant.INSTANCE)
        val summaryRequestEntity = SummaryReportRequestEntity(
            applicationRepository,
            reconciliationRepository, printerService
        )
        val summaryReportRequester =
            ProcessorManager.getInterface(terminalRepository, NotificationHandler)
                .summaryReportRequester
        return Observable.fromCallable {
            (summaryReportRequester.execute(summaryRequestEntity.mapToModel())).mapToSummaryReportResponseUIModel()
        }
    }
}