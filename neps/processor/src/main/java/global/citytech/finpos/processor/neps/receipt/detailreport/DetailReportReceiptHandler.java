package global.citytech.finpos.processor.neps.receipt.detailreport;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finpos.processor.neps.receipt.ReceiptLabels;
import global.citytech.finposframework.hardware.io.printer.Printable;
import global.citytech.finposframework.hardware.io.printer.PrinterRequest;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finpos.processor.neps.receipt.ReceiptUtils.addBase64Image;
import static global.citytech.finpos.processor.neps.receipt.ReceiptUtils.addDoubleColumnString;
import static global.citytech.finpos.processor.neps.receipt.ReceiptUtils.addNullableValueDoubleColumnString;
import static global.citytech.finpos.processor.neps.receipt.ReceiptUtils.addSingleColumnString;

/**
 * Created by Saurav Ghimire on 3/29/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public class DetailReportReceiptHandler {
    private final PrinterService printerService;

    public DetailReportReceiptHandler(PrinterService printerService) {
        this.printerService = printerService;
    }


    public PrinterResponse print(DetailReportReceipt detailReportReceipt) {
        PrinterRequest printerRequest = this.preparePrinterRequest(detailReportReceipt);
        return this.printerService.print(printerRequest);
    }

    private PrinterRequest preparePrinterRequest(DetailReportReceipt detailReportReceipt) {
        List<Printable> printableList = new ArrayList<>();
        if (detailReportReceipt.getRetailer().getRetailerLogo() != null)
            addBase64Image(printableList, detailReportReceipt.getRetailer().getRetailerLogo());
        addSingleColumnString(printableList, detailReportReceipt.getRetailer().getRetailerName(), DetailReportReceiptStyle.RETAILER_NAME.getStyle());
        addSingleColumnString(printableList, detailReportReceipt.getRetailer().getRetailerAddress(), DetailReportReceiptStyle.RETAILER_ADDRESS.getStyle());
        addSingleColumnString(printableList, ReceiptLabels.LINE_BREAK, DetailReportReceiptStyle.LINE_BREAK.getStyle());
        addDoubleColumnString(printableList,
                this.prepareDateString(detailReportReceipt.getPerformance().getStartDateTime()),
                this.prepareTimeString(detailReportReceipt.getPerformance().getStartDateTime()),
                DetailReportReceiptStyle.START_DATE_TIME.getStyle());
        addDoubleColumnString(printableList,
                this.merchantId(detailReportReceipt.getRetailer().getMerchantId()),
                this.terminalId(detailReportReceipt.getRetailer().getTerminalId()),
                DetailReportReceiptStyle.TID_MID.getStyle());
        addDoubleColumnString(printableList,
                ReceiptLabels.BATCH_NUMBER,
                detailReportReceipt.getReconciliationBatchNumber(),
                DetailReportReceiptStyle.BATCH_NUMBER.getStyle());
        addDoubleColumnString(printableList,
                ReceiptLabels.HOST,
                detailReportReceipt.getHost(),
                DetailReportReceiptStyle.HOST.getStyle());
        addSingleColumnString(printableList, ReceiptLabels.LINE_BREAK, DetailReportReceiptStyle.LINE_BREAK.getStyle());
        addSingleColumnString(printableList, ReceiptLabels.DETAIL_REPORT, DetailReportReceiptStyle.HEADER.getStyle());
        addSingleColumnString(printableList, ReceiptLabels.LINE_BREAK, DetailReportReceiptStyle.LINE_BREAK.getStyle());
        this.addReportLabels(printableList);
        this.addReportValues(printableList, detailReportReceipt.getDetailReports());
        addSingleColumnString(printableList, ReceiptLabels.FOOTER, DetailReportReceiptStyle.FOOTER.getStyle());
        return new PrinterRequest(printableList);
    }

    private String prepareTimeString(String dateTime) {
        if (StringUtils.isEmpty(dateTime) || dateTime.length() < 15)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(dateTime, 6, 8);
        stringBuilder.append(":");
        stringBuilder.append(dateTime, 8, 10);
        stringBuilder.append(":");
        stringBuilder.append(dateTime, 10, 12);
        return this.formatLabelWithValue(ReceiptLabels.TIME, stringBuilder.toString());
    }

    private String prepareDateString(String dateTime) {
        if (StringUtils.isEmpty(dateTime) || dateTime.length() < 15)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("20");
        stringBuilder.append(dateTime, 0, 2);
        stringBuilder.append("/");
        stringBuilder.append(dateTime, 2, 4);
        stringBuilder.append("/");
        stringBuilder.append(dateTime, 4, 6);
        return this.formatLabelWithValue(ReceiptLabels.DATE, stringBuilder.toString());
    }

    private void addReportLabels(List<Printable> printableList) {
        addDoubleColumnString(printableList, ReceiptLabels.CARD_NUMBER, ReceiptLabels.CARD_SCHEME, DetailReportReceiptStyle.REPORT_LABEL.getStyle());
        addDoubleColumnString(printableList, ReceiptLabels.INVOICE_NUMBER, ReceiptLabels.APPROVAL_CODE, DetailReportReceiptStyle.REPORT_LABEL.getStyle());
        addDoubleColumnString(printableList, ReceiptLabels.TRANSACTION_TYPE, ReceiptLabels.AMOUNT, DetailReportReceiptStyle.REPORT_LABEL.getStyle());
        addDoubleColumnString(printableList, ReceiptLabels.TRANSACTION_DATE, ReceiptLabels.TRANSACTION_TIME, DetailReportReceiptStyle.REPORT_LABEL.getStyle());
        addSingleColumnString(printableList, ReceiptLabels.DIVIDER, DetailReportReceiptStyle.DIVIDER.getStyle());
    }

    private void addReportValues(List<Printable> printableList, List<DetailReport> detailReports) {
        for (DetailReport detailReport : detailReports) {
            addNullableValueDoubleColumnString(printableList, detailReport.getCardNumber(), detailReport.getCardScheme(), DetailReportReceiptStyle.REPORT_VALUE.getStyle());
            addNullableValueDoubleColumnString(printableList, detailReport.getInvoiceNumber(), detailReport.getApprovalCode(), DetailReportReceiptStyle.REPORT_VALUE.getStyle());
            addNullableValueDoubleColumnString(printableList, detailReport.getTransactionType(), detailReport.getAmount(), DetailReportReceiptStyle.REPORT_VALUE.getStyle());
            addNullableValueDoubleColumnString(printableList, detailReport.getTransactionDate(), formatOriginalTimeForEveryTransaction(detailReport.getTransactionTime()), DetailReportReceiptStyle.REPORT_VALUE.getStyle());
            addSingleColumnString(printableList, ReceiptLabels.LINE_BREAK, DetailReportReceiptStyle.LINE_BREAK.getStyle());
        }
    }

    private String merchantId(String merchantId) {
        return this.formatLabelWithValue(ReceiptLabels.MERCHANT_ID, merchantId);
    }

    private String terminalId(String terminalId) {
        return this.formatLabelWithValue(ReceiptLabels.TERMINAL_ID, terminalId);
    }

    private String formatLabelWithValue(String label, String value) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(label);
        stringBuilder.append(":\t");
        stringBuilder.append(value);
        return stringBuilder.toString();
    }

    private String formatOriginalTimeForEveryTransaction(String originalTime) {
        if (StringUtils.isEmpty(originalTime) || originalTime.length() < 6)
            return "";
        return originalTime.substring(0, 2) +
                ":" +
                originalTime.substring(2, 4) +
                ":" +
                originalTime.substring(4, 6);
    }
}
