package global.citytech.finposframework.switches.receipt.detailreport;

import global.citytech.finposframework.switches.ResponseParameter;

/**
 * Created by Unique Shakya on 1/4/2021.
 */
public interface DetailReportResponseParameter extends ResponseParameter {
}
