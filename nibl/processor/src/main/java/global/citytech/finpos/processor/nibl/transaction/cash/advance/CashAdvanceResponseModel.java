package global.citytech.finpos.processor.nibl.transaction.cash.advance;

import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;

/**
 * Created by Unique Shakya on 2/4/2021.
 */

public class CashAdvanceResponseModel extends TransactionResponseModel {

    private String message;
    private String stan;
    private boolean isApproved;
    private String debugRequestMessage;
    private String debugResponseMessage;
    private boolean shouldPrintCustomerCopy;

    public String getMessage() {
        return message;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }

    public String getStan() {
        return stan;
    }

    public boolean shouldPrintCustomerCopy() {
        return shouldPrintCustomerCopy;
    }

    public static final class Builder {
        private String message;
        private String stan;
        private boolean isApproved;
        private String debugRequestMessage;
        private String debugResponseMessage;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder stan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder isApproved(boolean isApproved) {
            this.isApproved = isApproved;
            return this;
        }

        public Builder shouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public Builder debugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public Builder debugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public CashAdvanceResponseModel build() {
            CashAdvanceResponseModel cashAdvanceResponseModel = new CashAdvanceResponseModel();
            cashAdvanceResponseModel.debugRequestMessage = this.debugRequestMessage;
            cashAdvanceResponseModel.debugResponseMessage = this.debugResponseMessage;
            cashAdvanceResponseModel.message = this.message;
            cashAdvanceResponseModel.stan = this.stan;
            cashAdvanceResponseModel.isApproved = this.isApproved;
            cashAdvanceResponseModel.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            return cashAdvanceResponseModel;
        }
    }
}
