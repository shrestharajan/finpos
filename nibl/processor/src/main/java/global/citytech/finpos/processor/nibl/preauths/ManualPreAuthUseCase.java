//package global.citytech.finpos.processor.nibl.preauths;
//
//import java.util.concurrent.CountDownLatch;
//
//import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
//import global.citytech.finpos.nibl.iso8583.ProcessingCode;
//import global.citytech.finpos.nibl.iso8583.requestsender.preauth.ManualPreAuthRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.preauth.PreAuthRequest;
//import global.citytech.finpos.nibl.iso8583.requestsender.preauth.PreAuthRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
//import global.citytech.finpos.processor.nibl.reversal.ReversalRequestModel;
//import global.citytech.finpos.processor.nibl.reversal.ReversalUseCase;
//import global.citytech.finpos.processor.nibl.transaction.ManualTransactionUseCase;
//import global.citytech.finpos.processor.nibl.transaction.NiblTransactionReceiptPrintHandler;
//import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
//import global.citytech.finpos.processor.nibl.transaction.ReversedTransactionReceiptHandler;
//import global.citytech.finposframework.exceptions.FinPosException;
//import global.citytech.finposframework.exceptions.PosError;
//import global.citytech.finposframework.exceptions.PosException;
//import global.citytech.finposframework.hardware.io.cards.CardDetails;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
//import global.citytech.finposframework.iso8583.DataElement;
//import global.citytech.finposframework.iso8583.IsoMessageResponse;
//import global.citytech.finposframework.iso8583.RequestContext;
//import global.citytech.finposframework.listeners.CardConfirmationListener;
//import global.citytech.finposframework.notifier.Notifier;
//import global.citytech.finposframework.repositories.ApplicationRepository;
//import global.citytech.finposframework.repositories.TerminalRepository;
//import global.citytech.finposframework.repositories.TransactionRepository;
//import global.citytech.finposframework.supports.UseCase;
//import global.citytech.finposframework.switches.PosEntryMode;
//import global.citytech.finposframework.switches.preauths.ManualPreAuthRequester;
//import global.citytech.finposframework.usecases.TransactionType;
//import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
//import global.citytech.finposframework.usecases.transaction.TransactionLog;
//import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
//import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
//import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
//import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
//import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
//import global.citytech.finposframework.utility.HelperUtils;
//import global.citytech.finposframework.utility.IsoMessageUtils;
//import global.citytech.finposframework.utility.JsonUtils;
//import global.citytech.finposframework.utility.StringUtils;
//
///**
// * Created by Rishav Chudal on 11/3/20.
// */
//public class ManualPreAuthUseCase extends ManualTransactionUseCase
//        implements UseCase<PreAuthRequestModel, PreAuthResponseModel>,
//        ManualPreAuthRequester<PreAuthRequestModel, PreAuthResponseModel> {
//
//    private PreAuthRequest isoPreAuthRequest;
//    static CountDownLatch countDownLatch;
//    PreAuthRequestModel preAuthRequestModel;
//    ReadCardResponse readCardResponse;
//    String stan;
//    String invoiceNum;
//    TransactionRequest transactionRequest;
//    String batchNumber;
//    private global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest;
//
//    public ManualPreAuthUseCase(
//            TerminalRepository terminalRepository,
//            Notifier notifier
//    ) {
//        super(terminalRepository, notifier);
//    }
//
//    @Override
//    protected String getClassName() {
//        return ManualPreAuthUseCase.class.getSimpleName();
//    }
//
//    @Override
//    public PreAuthResponseModel execute(PreAuthRequestModel preAuthRequestModel) {
//        try {
//            this.logger.log("::: MANUAL PRE AUTH USE CASE :::");
//            this.preAuthRequestModel = preAuthRequestModel;
//            stan = this.terminalRepository.getSystemTraceAuditNumber();
//            invoiceNum = this.terminalRepository.getInvoiceNumber();
//            batchNumber = this.terminalRepository.getReconciliationBatchNumber();
//            transactionRequest = preAuthRequestModel.getTransactionRequest();
//            this.transactionRepository = preAuthRequestModel.getTransactionRepository();
//            if (!this.deviceIsReady(
//                    preAuthRequestModel.getDeviceController(),
//                    transactionRequest.getTransactionType()
//            )) {
//                throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
//            }
//            this.initTransaction(
//                    preAuthRequestModel.getApplicationRepository(),
//                    transactionRequest
//            );
//            ReadCardResponse readCardResponse
//                    = this.prepareManualReadCardResponse(transactionRequest);
//            if (readCardResponse.getCardDetails() == null)
//                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//            this.identifyAndSetCardScheme(
//                    readCardResponse,
//                    preAuthRequestModel.getTransactionRepository(),
//                    preAuthRequestModel.getApplicationRepository()
//            );
//            if (!this.validTransaction(
//                    preAuthRequestModel.getApplicationRepository(),
//                    transactionRequest,
//                    readCardResponse
//            )) {
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
//            }
//            if (!transactionAuthorizationSucceeds(
//                    preAuthRequestModel.getTransactionAuthenticator(),
//                    preAuthRequestModel.getApplicationRepository(),
//                    transactionRequest.getTransactionType(),
//                    readCardResponse
//            )) {
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
//            }
//            purchaseRequest = preparePurchaseRequestForPreAuth(
//                    transactionRequest,
//                    readCardResponse.getCardDetails()
//            );
//
//            final boolean[] isConfirm = {false};
//            countDownLatch = new CountDownLatch(1);
//            this.notifier.notify(Notifier.EventType.CARD_CONFIRMATION_EVENT,
//                    readCardResponse.getCardDetails(),
//                    (CardConfirmationListener) confirmed -> {
//                        isConfirm[0] = confirmed;
//                        countDownLatch.countDown();
//                    });
//            countDownLatch.await();
//            if (!isConfirm[0]) {
//                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
//            }
//            return processCardAndProceed();
//        } catch (PosException ex) {
//            ex.printStackTrace();
//            throw ex;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        }
//    }
//
//    private PreAuthResponseModel processCardAndProceed() {
//        readCardResponse = this.retrievePinBlockIfRequired(
//                purchaseRequest,
//                preAuthRequestModel.getApplicationRepository(),
//                preAuthRequestModel.getTransactionAuthenticator()
//        );
//        this.logger.debug("::: READ CARD RESPONSE ::: " + readCardResponse.toString());
//        this.notifier.notify(
//                Notifier.EventType.TRANSMITTING_REQUEST,
//                Notifier.EventType.TRANSMITTING_REQUEST.getDescription()
//        );
//        return this.initiateIsoMessageTransmission(
//                preAuthRequestModel,
//                stan,
//                invoiceNum,
//                batchNumber,
//                transactionRequest,
//                readCardResponse,
//                preAuthRequestModel.getApplicationRepository()
//        );
//    }
//
//    private PurchaseRequest preparePurchaseRequestForPreAuth(
//            TransactionRequest transactionRequest,
//            CardDetails cardDetails
//    ) {
//        PurchaseRequest purchaseRequest = new PurchaseRequest(
//                transactionRequest.getTransactionType(),
//                transactionRequest.getAmount(),
//                transactionRequest.getAdditionalAmount()
//        );
//        cardDetails.setAmount(transactionRequest.getAmount());
//        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
//        purchaseRequest.setCardDetails(cardDetails);
//        purchaseRequest.setFallbackIccToMag(transactionRequest.isFallbackIccToMag());
//        return purchaseRequest;
//    }
//
//    private PreAuthResponseModel initiateIsoMessageTransmission(
//            PreAuthRequestModel preAuthRequestModel,
//            String stan,
//            String invoiceNum,
//            String batchNumber,
//            TransactionRequest transactionRequest,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository
//    ) {
//        try {
//            IsoMessageResponse isoMessageResponse = this.sendIsoMessage(
//                    stan,
//                    invoiceNum,
//                    readCardResponse,
//                    preAuthRequestModel
//            );
//            InvalidResponseHandlerResponse invalidResponseHandlerResponse
//                    = this.checkForInvalidResponseFromHost(isoMessageResponse);
//            if (invalidResponseHandlerResponse.isInvalid()) {
//                return this.onInvalidResponseFromHost(
//                        transactionRequest,
//                        invalidResponseHandlerResponse,
//                        batchNumber,
//                        stan,
//                        invoiceNum,
//                        readCardResponse,
//                        preAuthRequestModel
//                );
//            }
//            boolean preAuthApproved = this.transactionApprovedByActionCode(isoMessageResponse);
//            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait...");
//            this.notifyTransactionCompletion(
//                    stan,
//                    batchNumber,
//                    invoiceNum,
//                    preAuthApproved,
//                    transactionRequest,
//                    preAuthRequestModel,
//                    isoMessageResponse,
//                    readCardResponse,
//                    applicationRepository
//            );
//            boolean shouldPrint = this.printReceipt(
//                    batchNumber,
//                    stan,
//                    invoiceNum,
//                    preAuthRequestModel.getPrinterService(),
//                    preAuthRequestModel.getApplicationRepository(),
//                    preAuthRequestModel.getTransactionRepository(),
//                    transactionRequest,
//                    readCardResponse,
//                    isoMessageResponse
//            );
//            return this.preparePreAuthResponse(stan,
//                    isoMessageResponse,
//                    preAuthApproved,
//                    preAuthRequestModel.getApplicationRepository(),
//                    shouldPrint
//            );
//        } catch (FinPosException ex) {
//            ex.printStackTrace();
//            return onFinPosException(
//                    ex,
//                    transactionRequest,
//                    batchNumber,
//                    stan,
//                    invoiceNum,
//                    readCardResponse,
//                    preAuthRequestModel
//            );
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        }
//    }
//
//    private IsoMessageResponse sendIsoMessage(
//            String stan,
//            String invoiceNumber,
//            ReadCardResponse readCardResponse,
//            PreAuthRequestModel requestModel) {
//        RequestContext requestContext = this.prepareManualTemplateRequestContext(stan);
//        this.logger.debug(String.format(" Request Context ::%s", requestContext));
//        this.isoPreAuthRequest = prepareIsoPreAuthRequest(
//                invoiceNumber,
//                readCardResponse
//        );
//        PreAuthRequestSender preAuthRequestSender = new ManualPreAuthRequestSender(
//                new NIBLSpecInfo(),
//                requestContext
//        );
//        requestContext.setRequest(this.isoPreAuthRequest);
//        this.autoReversal = new AutoReversal(
//                TransactionType.PRE_AUTH,
//                JsonUtils.toJsonObj(prepareReversalRequest(stan, readCardResponse, requestModel)),
//                0,
//                AutoReversal.Status.ACTIVE,
//                StringUtils.dateTimeStamp()
//        );
//        this.transactionRepository.saveAutoReversal(this.autoReversal);
//        IsoMessageResponse isoMessageResponse = preAuthRequestSender.send();
//        this.transactionRepository.removeAutoReversal(this.autoReversal);
//        return isoMessageResponse;
//    }
//
//    private PreAuthRequest prepareIsoPreAuthRequest(
//            String invoiceNumber,
//            ReadCardResponse readCardResponse
//    ) {
//        PreAuthRequest preAuthRequest = new PreAuthRequest();
//        preAuthRequest.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        preAuthRequest.setExpireDate(readCardResponse.getCardDetails().getExpiryDate());
//        preAuthRequest.setEntryMode(PosEntryMode.MANUAL);
//        preAuthRequest.setPosConditionCode("05");
//        if (readCardResponse.getCardDetails().getPinBlock().getOfflinePin() ||
//                StringUtils.isEmpty(readCardResponse.getCardDetails().getPinBlock().getPin()) ||
//                readCardResponse.getCardDetails().getPinBlock().getPin().equalsIgnoreCase("offline"))
//            preAuthRequest.setPinBlock("");
//        else
//            preAuthRequest.setPinBlock(readCardResponse.getCardDetails().getPinBlock().getPin());
//        preAuthRequest.setLocalTime(HelperUtils.getDefaultLocaleTimeHhMmSs());
//        preAuthRequest.setLocalDate(HelperUtils.getDefaultLocaleDateWithoutYear());
//        preAuthRequest.setInvoiceNumber(invoiceNumber);
//        preAuthRequest.setTransactionAmount(readCardResponse.getCardDetails().getAmount());
//        preAuthRequest.setCvv(readCardResponse.getCardDetails().getCvv());
//        return preAuthRequest;
//    }
//
//    private PreAuthResponseModel onInvalidResponseFromHost(
//            TransactionRequest transactionRequest,
//            InvalidResponseHandlerResponse invalidResponseHandlerResponse,
//            String batchNumber,
//            String stan,
//            String invoiceNumber,
//            ReadCardResponse readCardResponse,
//            PreAuthRequestModel requestModel
//    ) {
//        this.notifier.notify(
//                Notifier.EventType.TRANSACTION_DECLINED,
//                invalidResponseHandlerResponse.getMessage()
//        );
//        this.performReversal(stan, readCardResponse, requestModel);
//        boolean shouldPrint = !this.approvePrintOnly(requestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(
//                    transactionRequest,
//                    readCardResponse,
//                    this.terminalRepository
//            ).prepare(
//                    batchNumber,
//                    stan,
//                    invoiceNumber,
//                    new ReasonForReversal(
//                            ReasonForReversal.Type.INVALID_RESPONSE,
//                            invalidResponseHandlerResponse.getMessage()
//                    )
//            );
//            new NiblTransactionReceiptPrintHandler(requestModel.getPrinterService())
//                    .printTransactionReceipt(
//                            receiptLog,
//                            ReceiptVersion.MERCHANT_COPY
//                    );
//            requestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PreAuthResponseModel.Builder.createDefaultBuilder()
//                .approved(false)
//                .message(invalidResponseHandlerResponse.getMessage())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private void performReversal(
//            String stan,
//            ReadCardResponse readCardResponse,
//            PreAuthRequestModel preAuthRequestModel
//    ) {
//        ReversalUseCase reversalUseCase = new ReversalUseCase(
//                this.prepareReversalRequest(stan, readCardResponse, preAuthRequestModel),
//                this.terminalRepository,
//                this.notifier
//        );
//        try {
//            reversalUseCase.execute(
//                    prepareReversalRequestModel(
//                            preAuthRequestModel,
//                            readCardResponse
//                    )
//            );
//            this.transactionRepository.removeAutoReversal(this.autoReversal);
//        } catch (FinPosException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private NiblReversalRequest prepareReversalRequest(
//            String stan,
//            ReadCardResponse readCardResponse,
//            PreAuthRequestModel preAuthRequestModel
//    ) {
//        NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
//        niblReversalRequest.setProcessingCode(ProcessingCode.PRE_AUTHORIZATION.getCode());
//        niblReversalRequest.setAmount(
//                preAuthRequestModel.getTransactionRequest().getAmount()
//        );
//        niblReversalRequest.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        niblReversalRequest.setExpiryDate(readCardResponse.getCardDetails().getExpiryDate());
//        niblReversalRequest.setPosEntryMode(this.isoPreAuthRequest.getPosEntryMode());
//        niblReversalRequest.setPosConditionCode(this.isoPreAuthRequest.getPosConditionCode());
//        niblReversalRequest.setPinBlock(this.isoPreAuthRequest.getPinBlock());
//        niblReversalRequest.setInvoiceNumber(this.isoPreAuthRequest.getInvoiceNumber());
//        niblReversalRequest.setStan(stan);
//        return niblReversalRequest;
//    }
//
//    private ReversalRequestModel prepareReversalRequestModel(
//            PreAuthRequestModel preAuthRequestModel,
//            ReadCardResponse readCardResponse
//    ) {
//        return ReversalRequestModel.Builder.newInstance()
//                .withTransactionRequest(
//                        preAuthRequestModel.getTransactionRequest()
//                )
//                .withTransactionRepository(
//                        preAuthRequestModel.getTransactionRepository()
//                )
//                .withPrinterService(
//                        preAuthRequestModel.getPrinterService()
//                )
//                .withApplicationRepository(
//                        preAuthRequestModel.getApplicationRepository()
//                )
//                .withReadCardResponse(readCardResponse)
//                .build();
//    }
//
//    private void notifyTransactionCompletion(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean preAuthApproved,
//            TransactionRequest transactionRequest,
//            PreAuthRequestModel preAuthRequestModel,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository
//    ) {
//        this.updateTransactionLog(
//                stan,
//                batchNumber,
//                invoiceNumber,
//                preAuthApproved,
//                transactionRequest,
//                preAuthRequestModel.getTransactionRepository(),
//                isoMessageResponse,
//                readCardResponse,
//                applicationRepository
//        );
//        this.updateTransactionIds(preAuthApproved);
//        this.notifyTransactionStatus(
//                preAuthApproved,
//                isoMessageResponse,
//                preAuthRequestModel.getApplicationRepository()
//        );
//    }
//
//    private void updateTransactionLog(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean preAuthApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository
//    ) {
//        this.logger.log("::: NIBL ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
//        this.logger.log("::: NIBL ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
//        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
//                .withStan(stan)
//                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
//                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
//                .withInvoiceNumber(invoiceNumber)
//                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
//                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
//                .withTransactionType(TransactionType.PRE_AUTH)
//                .withOriginalTransactionType(TransactionType.PRE_AUTH)
//                .withTransactionAmount(transactionRequest.getAmount())
//                .withOriginalTransactionAmount(transactionRequest.getAmount())
//                .withTransactionDate(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_DATE))
//                .withTransactionTime(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_TIME))
//                .withTransactionStatus(preAuthApproved ? "APPROVED" : "DECLINED")
//                .withPosEntryMode(PosEntryMode.MANUAL)
//                .withOriginalPosEntryMode(PosEntryMode.MANUAL)
//                .withPosConditionCode(this.isoPreAuthRequest.getPosConditionCode())
//                .withOriginalPosConditionCode("")
//                .withReconcileStatus("")
//                .withReconcileTime("")
//                .withReconcileDate("")
//                .withReconcileBatchNo(batchNumber)
//                .withReadCardResponse(readCardResponse)
//                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse,
//                        applicationRepository, batchNumber, stan, invoiceNumber))
//                .withAuthorizationCompleted(false)
//                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
//                .withTransactionVoided(false)
//                .withTipAdjusted(false)
//                .build();
//        transactionRepository.updateTransactionLog(transactionLog);
//    }
//
//    private PreAuthResponseModel preparePreAuthResponse(
//            String stan,
//            IsoMessageResponse isoMessageResponse,
//            boolean preAuthApproved,
//            ApplicationRepository applicationRepository,
//            boolean shouldPrint
//    ) {
//        PreAuthResponseModel.Builder builder = PreAuthResponseModel.Builder.createDefaultBuilder();
//        builder.approved(preAuthApproved);
//        builder.stan(stan);
//        builder.debugRequestMessage(isoMessageResponse.getDebugRequestString());
//        builder.debugResponseMessage(isoMessageResponse.getDebugResponseString());
//        builder.message(
//                this.retrieveMessage(
//                        isoMessageResponse,
//                        preAuthApproved,
//                        applicationRepository
//                )
//        );
//        builder.shouldPrintCustomerCopy(shouldPrint);
//        return builder.build();
//    }
//
//    private PreAuthResponseModel onFinPosException(
//            FinPosException ex,
//            TransactionRequest transactionRequest,
//            String batchNumber,
//            String stan,
//            String invoiceNum,
//            ReadCardResponse readCardResponse,
//            PreAuthRequestModel preAuthRequestModel
//    ) {
//        if (this.isResponseNotReceivedException(ex)) {
//            return onTransactionTimeOut(
//                    transactionRequest,
//                    batchNumber,
//                    stan,
//                    invoiceNum,
//                    readCardResponse,
//                    preAuthRequestModel
//            );
//        } else if (ex.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT) {
//            logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED UNABLE TO CONNECT");
//            transactionRepository.removeAutoReversal(this.autoReversal);
//            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
//        } else {
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        }
//    }
//
//    private PreAuthResponseModel onTransactionTimeOut(
//            TransactionRequest transactionRequest,
//            String batchNumber,
//            String stan,
//            String invoiceNumber,
//            ReadCardResponse readCardResponse,
//            PreAuthRequestModel preAuthRequestModel
//    ) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, "TIMEOUT");
//        this.performReversal(
//                stan,
//                readCardResponse,
//                preAuthRequestModel
//        );
//        boolean shouldPrint = !this.approvePrintOnly(preAuthRequestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(
//                    transactionRequest,
//                    readCardResponse,
//                    this.terminalRepository
//            ).prepare(
//                    batchNumber,
//                    stan,
//                    invoiceNumber,
//                    new ReasonForReversal(
//                            ReasonForReversal.Type.TIMEOUT,
//                            ReasonForReversal.Type.TIMEOUT.getDescription()
//                    )
//            );
//            new NiblTransactionReceiptPrintHandler(preAuthRequestModel.getPrinterService())
//                    .printTransactionReceipt(
//                            receiptLog,
//                            ReceiptVersion.MERCHANT_COPY
//                    );
//            preAuthRequestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PreAuthResponseModel.Builder.createDefaultBuilder()
//                .message(ReasonForReversal.Type.TIMEOUT.getDescription())
//                .approved(false)
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//}
