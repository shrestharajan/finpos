package global.citytech.finpos.neps.iso8583.requestsender.cashadvance;

import global.citytech.finpos.neps.iso8583.requestsender.cardtypesenders.ICCISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.CardTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transactiontypesenders.CashAdvanceISOMessageUpdater;
import global.citytech.finposframework.iso8583.RequestContext;

public class ICCCashAdvanceRequestSender extends CashAdvanceRequestSender {

    public ICCCashAdvanceRequestSender( RequestContext context) {
        super(context);
    }

    @Override
    protected String getClassName() {
        return this.getClass().getName();
    }

    @Override
    protected CardTypeISOMessageUpdater getCardTypeSender() {
        return new ICCISOMessageUpdater();
    }

}
