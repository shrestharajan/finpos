package global.citytech.finposframework.usecases.transaction.receipt.logon;

import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;

/**
 * Created by Unique Shakya
 */
public class MerchantLogonReceipt {

    private Retailer retailer;
    private Performance performance;
    private String message;

    public Retailer getRetailer() {
        return retailer;
    }

    public Performance getPerformance() {
        return performance;
    }

    public String getMessage() {
        return message;
    }

    public static final class Builder {
        private Retailer retailer;
        private Performance performance;
        private String message;

        private Builder() {
        }

        public static Builder aMerchantLogonReceipt() {
            return new Builder();
        }

        public Builder withRetailer(Retailer retailer) {
            this.retailer = retailer;
            return this;
        }

        public Builder withPerformance(Performance performance) {
            this.performance = performance;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public MerchantLogonReceipt build() {
            MerchantLogonReceipt merchantLogonReceipt = new MerchantLogonReceipt();
            merchantLogonReceipt.retailer = this.retailer;
            merchantLogonReceipt.performance = this.performance;
            merchantLogonReceipt.message = this.message;
            return merchantLogonReceipt;
        }
    }
}
