package global.citytech.finposframework.repositories;

public class AidRetrieveRequest  {

    private String aid;
    private FieldAttributes fieldAttributes;

    public AidRetrieveRequest(String aid, FieldAttributes fieldAttributes) {
        this.aid = aid;
        this.fieldAttributes = fieldAttributes;
    }

    public String getAid() {
        return aid;
    }

    public FieldAttributes getFieldAttributes() {
        return fieldAttributes;
    }
}
