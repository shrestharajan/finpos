package global.citytech.finpos.merchant.presentation.transactions.card

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.model.TransactionItem
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.utility.HelperUtils
import global.citytech.finposframework.utility.StringUtils
import kotlinx.android.synthetic.main.item_transaction.view.*

/**
 * Created by Saurav Ghimire on 5/7/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


class CardTransactionsAdapter(private var transactions: MutableList<TransactionItem>) :
    RecyclerView.Adapter<CardTransactionsAdapter.TransactionViewHolder>() {

    lateinit var context: Context

    class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        context = parent.context
        return TransactionViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_transaction, parent, false)
        )
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val transactionItem = transactions[position]
        val transactionType =
            Jsons.fromJsonToObj(transactionItem.transactionType, TransactionType::class.java)
        if (transactionType == TransactionType.VOID || transactionType == TransactionType.CASH_VOID || transactionType == TransactionType.REFUND) {
            holder.itemView.iv_transaction_icon.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.red
                ), android.graphics.PorterDuff.Mode.SRC_IN
            );
        } else {
            holder.itemView.iv_transaction_icon.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.green
                ), android.graphics.PorterDuff.Mode.SRC_IN
            );

        }
        holder.itemView.tv_txn_type.text = "Type : ${transactionType.displayName}"
        holder.itemView.tv_card_number.text =
            "Card : ${StringUtils.encodeAccountNumber(transactionItem.pan)}"
        holder.itemView.tv_invoice_number.text = "Invoice : ${transactionItem.invoiceNumber}"
        holder.itemView.tv_txn_time.text = "${transactionItem.time!!}"
        holder.itemView.tv_amount.text =
            "${transactionItem.currencyName} ${HelperUtils.fromIsoAmount(transactionItem.amount)}"
        holder.itemView.setOnClickListener {
            val bundle = Bundle()
            bundle.putString(CardTransactionDetailActivity.STAN, transactionItem.stan)
            val intent = Intent(context, CardTransactionDetailActivity::class.java)
            intent.putExtra("BUNDLE", bundle)
            (context as Activity).startActivityForResult(
                intent,
                CardTransactionDetailActivity.REQUEST_CODE
            )
        }
    }


    override fun getItemCount() = transactions.size

    fun update(transactions: MutableList<TransactionItem>) {
        this.transactions = transactions
        notifyDataSetChanged()
    }

    fun updateAll(transactions: List<TransactionItem>) {
        this.transactions = transactions as MutableList<TransactionItem>
        notifyDataSetChanged()
    }
}