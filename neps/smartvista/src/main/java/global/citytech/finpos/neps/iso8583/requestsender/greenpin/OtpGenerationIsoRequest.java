package global.citytech.finpos.neps.iso8583.requestsender.greenpin;

import java.math.BigDecimal;

import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finposframework.switches.PosEntryMode;

public class OtpGenerationIsoRequest extends TransactionIsoRequest {
    private String stan;
    private PosEntryMode posEntryMode;
    private String posConditionCode;

    public String getStan() {
        return stan;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public static final class Builder {
        private String pan;
        private BigDecimal transactionAmount;
        private String stan;
        private String localDateTime;
        private String expirationDate;
        private PosEntryMode posEntryMode;
        private String posConditionCode;
        private String track2Data;
        private String currencyCode;
        private String emvData;

        private Builder() {
        }

        public static OtpGenerationIsoRequest.Builder newInstance() {
            return new OtpGenerationIsoRequest.Builder();
        }

        public OtpGenerationIsoRequest.Builder stan(String stan) {
            this.stan = stan;
            return this;
        }

        public OtpGenerationIsoRequest.Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public OtpGenerationIsoRequest.Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public OtpGenerationIsoRequest.Builder localDateTime(String localDataTime) {
            this.localDateTime = localDataTime;
            return this;
        }

        public OtpGenerationIsoRequest.Builder expirationDate(String expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public OtpGenerationIsoRequest.Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public OtpGenerationIsoRequest.Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public OtpGenerationIsoRequest.Builder posEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public OtpGenerationIsoRequest.Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public OtpGenerationIsoRequest.Builder posConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public OtpGenerationIsoRequest build() {
            OtpGenerationIsoRequest OtpGenerationIsoRequest = new OtpGenerationIsoRequest();
            OtpGenerationIsoRequest.pan = this.pan;
            OtpGenerationIsoRequest.transactionAmount = this.transactionAmount;
            OtpGenerationIsoRequest.stan = this.stan;
            OtpGenerationIsoRequest.localDateTime = this.localDateTime;
            OtpGenerationIsoRequest.expiryDate = this.expirationDate;
            OtpGenerationIsoRequest.posEntryMode = this.posEntryMode;
            OtpGenerationIsoRequest.posConditionCode = this.posConditionCode;
            OtpGenerationIsoRequest.track2Data = this.track2Data;
            OtpGenerationIsoRequest.currencyCode = this.currencyCode;
            OtpGenerationIsoRequest.emvData = this.emvData;
            return OtpGenerationIsoRequest;
        }
    }
}
