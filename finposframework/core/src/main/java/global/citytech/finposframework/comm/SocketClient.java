package global.citytech.finposframework.comm;

import global.citytech.finposframework.exceptions.FinPosException;

/** User: Surajchhetry Date: 2/24/20 Time: 10:29 AM */
public interface SocketClient extends AutoCloseable {

  HostInfo dial() throws FinPosException;

  boolean write(Request data) throws FinPosException;

  Response read(int numberOfBytes) throws FinPosException;
}
