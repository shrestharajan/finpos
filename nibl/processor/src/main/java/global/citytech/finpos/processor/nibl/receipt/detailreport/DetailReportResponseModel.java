package global.citytech.finpos.processor.nibl.receipt.detailreport;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.detailreport.DetailReportResponseParameter;

/**
 * Created by Unique Shakya on 1/4/2021.
 */
public class DetailReportResponseModel implements UseCase.Response, DetailReportResponseParameter {

    private Result result;
    private String message;

    public DetailReportResponseModel(Result result, String message) {
        this.result = result;
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }
}
