package global.citytech.finpos.merchant.framework.datasource.device

import global.citytech.finpos.merchant.service.AppDeviceFactory
import global.citytech.finposframework.hardware.DeviceServiceType
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.io.led.LedRequest
import global.citytech.finposframework.hardware.io.led.LedService

/**
 * Created by Unique Shakya on 11/26/2020.
 */
class LedSourceImpl(val context: android.content.Context?) : LedService{

    private var service = AppDeviceFactory.getCore(
        context,
        DeviceServiceType.LED
    ) as LedService

    override fun doTurnLedWith(request: LedRequest): DeviceResponse {
        return service.doTurnLedWith(request)
    }
}