package global.citytech.finpos.device.weipass.sound

import android.content.Context
import android.media.MediaPlayer
import global.citytech.finpos.device.weipass.R
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.io.sound.Sound
import global.citytech.finposframework.hardware.io.sound.SoundService
import java.lang.ref.WeakReference

/**
 * Created by Rishav Chudal on 6/4/20.
 */
class WeipassSoundServiceImpl(context: Context):
    SoundService {
    private val weakReferenceContext: WeakReference<Context> = WeakReference(context)

    override fun playSound(sound: Sound?) {
        if (sound != null) {
            when (sound) {
                Sound.SUCCESS -> manageForSuccessSound()
                Sound.FAILURE -> manageForFailureSound()
            }
        }
    }

    private fun manageForSuccessSound() {
        WeipassLogger.log("Playing Success Sound ...")
        val mp3 = MediaPlayer.create(weakReferenceContext.get(), R.raw.success)
        mp3.start()
        mp3.setOnCompletionListener { mp3.release() }
    }

    private fun manageForFailureSound() {
        WeipassLogger.log("Playing Failure Sound ...")
        val mp3 = MediaPlayer.create(weakReferenceContext.get(), R.raw.failed)
        mp3.start()
        mp3.setOnCompletionListener { mp3.release() }
    }
}