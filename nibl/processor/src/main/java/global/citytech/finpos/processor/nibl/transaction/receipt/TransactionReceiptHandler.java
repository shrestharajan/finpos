package global.citytech.finpos.processor.nibl.transaction.receipt;

import global.citytech.finpos.nibl.iso8583.S2MActionCode;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.cvm.CardHolderVerification;
import global.citytech.finposframework.hardware.io.cards.cvm.Cvm;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.CardSchemeRetrieveRequest;
import global.citytech.finposframework.repositories.FieldAttributes;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.EmvTags;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.TransactionInfo;
import global.citytech.finposframework.utility.CvmResultsParser;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Rishav Chudal on 7/6/20.
 */
public class TransactionReceiptHandler {
    private TransactionRequest transactionRequest;
    private ReadCardResponse readCardResponse;
    private IsoMessageResponse isoMessageResponse;
    private TerminalRepository terminalRepository;
    private ApplicationRepository applicationRepository;

    public TransactionReceiptHandler(
            TransactionRequest transactionRequest,
            ReadCardResponse readCardResponse,
            IsoMessageResponse isoMessageResponse,
            TerminalRepository terminalRepository,
            ApplicationRepository applicationRepository
    ) {
        this.transactionRequest = transactionRequest;
        this.readCardResponse = readCardResponse;
        this.isoMessageResponse = isoMessageResponse;
        this.terminalRepository = terminalRepository;
        this.applicationRepository = applicationRepository;
    }

    public ReceiptLog prepare(String batchNumber, String stan, String invoiceNumber) {
        return new ReceiptLog.Builder()
                .withRetailer(prepareRetailerInfo())
                .withPerformance(preparePerformanceInfo())
                .withTransaction(prepareTransactionInfo(batchNumber, stan, invoiceNumber))
                .withEmvTags(prepareEmvTags())
                .withBarCodeString(prepareBarCodeString())
                .build();
    }

    private String prepareBarCodeString() {
        if (!terminalRepository.isVatRefundSupported())
            return "";
        if (transactionRequest.getTransactionType() == TransactionType.PURCHASE)
            return this.retrieveRrn();
        else
            return "";
    }

    private Retailer prepareRetailerInfo() {
        return new Retailer.Builder()
                .withRetailerLogo(this.terminalRepository.findTerminalInfo().getMerchantPrintLogo())
                .withRetailerName(this.terminalRepository.findTerminalInfo().getMerchantName())
                .withRetailerAddress(this.terminalRepository.findTerminalInfo().getMerchantAddress())
                .withMerchantId(this.terminalRepository.findTerminalInfo().getMerchantID())
                .withTerminalId(this.terminalRepository.findTerminalInfo().getTerminalID())
                .build();
    }

    private Performance preparePerformanceInfo() {
        return new Performance.Builder()
                .withStartDateTime(
                        this.readCardResponse
                                .getCardDetails()
                                .getTransactionInitializeDateTime()
                )
                .withEndDateTime(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, 12))
                .build();
    }

    private TransactionInfo prepareTransactionInfo(String batchNumber, String stan, String invoiceNumber) {
        boolean isApproved = this.isApprovedByActionCode(Integer.parseInt(this.retrieveResponseCode()));
        return new TransactionInfo.Builder()
                .withApprovalCode(this.retrieveApprovalCode())
                .withTransactionCurrencyName(this.terminalRepository.getTerminalTransactionCurrencyName())
                .withBatchNumber(batchNumber)
                .withCardHolderName(!StringUtils.isEmpty(readCardResponse.getCardDetails().getCardHolderName()) ? readCardResponse.getCardDetails().getCardHolderName().trim() : "")
                .withCardNumber(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .withCardScheme(readCardResponse.getCardDetails().getCardSchemeLabel())
                .withExpireDate(readCardResponse.getCardDetails().getExpiryDate())
                .withPurchaseAmount(this.retrieveAmount())
                .withBaseAmount(this.retrieveBaseAmount())
                .withPurchaseType(this.transactionRequest.getTransactionType().getPrintName())
                .withRrn(this.retrieveRrn())
                .withStan(stan)
                .withInvoiceNumber(invoiceNumber)
                .withTransactionMessage(this.retrieveTransactionMessage(isApproved, Integer.parseInt(this.retrieveResponseCode())))
                .withSignatureRequired(this.retrieveSignatureRequired(isApproved))
                .withDebitAcknowledgementRequired(this.retrieveDebitAcknowledgementRequired())
                .withTransactionStatus(this.retrieveTransactionStatus(isApproved))
                .withTipEnabled(this.retrieveTipEnabled())
                .isEmiTransaction(transactionRequest.isEmiTransaction())
                .withEmiInfo(transactionRequest.getEmiInfo())
                .build();
    }

    private boolean retrieveTipEnabled() {
        if (transactionRequest.getTransactionType() == TransactionType.PURCHASE)
            return applicationRepository.isTransactionTypeEnabled(TransactionType.TIP_ADJUSTMENT);
        else
            return false;
    }

    private String retrieveBaseAmount() {
        if (this.transactionRequest.getTransactionType() == TransactionType.TIP_ADJUSTMENT)
            return StringUtils.formatAmountTwoDecimal(readCardResponse.getCardDetails().getAmount());
        return "";
    }

    private boolean retrieveDebitAcknowledgementRequired() {
        return !this.isSignatureRequiredTransaction();
    }

    private String retrieveAmount() {
        if (this.transactionRequest.getTransactionType() == TransactionType.TIP_ADJUSTMENT)
            return StringUtils.formatAmountTwoDecimal(readCardResponse.getCardDetails().getCashBackAmount());
        else
            return StringUtils.formatAmountTwoDecimal(readCardResponse.getCardDetails().getAmount());
    }

    private boolean retrieveSignatureRequired(boolean isApproved) {
        if (isApproved) {
            if (this.isSignatureRequiredTransaction())
                return true;
            else if(this.transactionRequest.getTransactionType() == TransactionType.TIP_ADJUSTMENT){
                return false;
            }
            switch (this.readCardResponse.getCardDetails().getCardType()) {
                case ICC:
                    return CvmResultsParser.retrieveCvmResults(readCardResponse.getCardDetails().getTagCollection()
                            .get(0x9F34).getData()).getCvm() == Cvm.SIGNATURE;
                case PICC:
                    return this.cvmForPICC() == CardHolderVerification.SIGNATURE_VERIFIED;
                case MAG:
                case MANUAL:
                    return this.readCardResponse.getCvm() == Cvm.SIGNATURE;
                default:
                    return false;
            }
        } else {
            return false;
        }
    }

    private String retrieveRrn() {
        return IsoMessageUtils.retrieveFromDataElementsAsString(this.isoMessageResponse, DataElement.RETRIEVAL_REFERENCE_NUMBER);
    }

    private String retrieveApprovalCode() {
        if (this.transactionRequest.getTransactionType() == TransactionType.AUTH_COMPLETION ||
                this.transactionRequest.getTransactionType() == TransactionType.TIP_ADJUSTMENT)
            return IsoMessageUtils.retrieveFromRequestDataElementAsString(this.isoMessageResponse, DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE);
        return IsoMessageUtils.retrieveFromDataElementsAsString(this.isoMessageResponse, DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE);
    }

    private String retrieveTransactionMessage(boolean isApproved, int responseCode) {
        if (isApproved)
            return this.retrieveCvmMessage();
        else {
            String messageText = this.applicationRepository.getMessageTextByActionCode(responseCode);
            if (StringUtils.isEmpty(messageText))
                messageText = S2MActionCode.getByActionCode(responseCode).getDescription();
            return messageText;
        }
    }

    private String retrieveCvmMessage() {
        if (this.isSignatureRequiredTransaction())
            return CardHolderVerification.SIGNATURE_VERIFIED.getCardHolderVerificationEn();
        else if(this.transactionRequest.getTransactionType() == TransactionType.TIP_ADJUSTMENT){
            return CardHolderVerification.NO_VERIFICATION.getCardHolderVerificationEn();
        }
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case ICC:
                return CvmResultsParser.retrieveCvmResults(readCardResponse.getCardDetails().getTagCollection()
                        .get(0x9F34).getData()).getCvm().getCardHolderVerification().getCardHolderVerificationEn();
            case PICC:
                return this.cvmForPICC().getCardHolderVerificationEn();
            case MAG:
            case MANUAL:
                return this.readCardResponse.getCvm().getCardHolderVerification().getCardHolderVerificationEn();
            default:
                return "";
        }
    }

    private boolean isSignatureRequiredTransaction() {
        return this.transactionRequest.getTransactionType() == TransactionType.VOID ||
                this.transactionRequest.getTransactionType() == TransactionType.AUTH_COMPLETION;
    }

    private CardHolderVerification cvmForPICC() {
        switch (this.readCardResponse.getPiccCvm()) {
            case SIGNATURE:
                return CardHolderVerification.SIGNATURE_VERIFIED;
            case ONLINE_PIN:
                return CardHolderVerification.PIN_VERIFIED;
            case CD_CVM:
            case DEVICE_OWNER:
                return CardHolderVerification.DEVICE_OWNER_VERIFIED;
            default:
                return CardHolderVerification.NO_VERIFICATION;
        }
    }

    private String retrieveTransactionStatus(boolean isApproved) {
        if (isApproved)
            return "APPROVED"; //TODO
        else
            return "DECLINED"; //TODO
    }

    private boolean isApprovedByActionCode(int responseCode) {
        return responseCode == 0;
    }

    private EmvTags prepareEmvTags() {
        return EmvTags.Builder.defaultBuilder()
                .withAc(this.getFromTagCollection(IccData.APPLICATION_CRYPTOGRAM.getTag()))
                .withAid(this.getFromTagCollection(IccData.DF_NAME.getTag()))
                .withCardType(this.readCardResponse.getCardDetails().getCardType().getPosEntryMode())
                .withCid(this.getFromTagCollection(IccData.CID.getTag()))
                .withCvm(this.getFromTagCollection(IccData.CVM.getTag()))
                .withKernelId(this.getFromTagCollection(IccData.KERNEL_ID.getTag()))
                .withResponseCode(this.retrieveResponseCode())
                .withTvr(this.getFromTagCollection(IccData.TVR.getTag()))
                .withTsi(this.getFromTagCollection(IccData.TSI.getTag()))
                .withIin(this.retrieveIin())
                .build();
    }

    private String retrieveIin() {
        return this.applicationRepository.retrieveCardScheme(new CardSchemeRetrieveRequest(
                this.readCardResponse.getCardDetails().getCardScheme().getCardSchemeId(),
                FieldAttributes.CARD_SCHEME_S3_F10_IIN)).getData();
    }

    private String retrieveResponseCode() {
        return IsoMessageUtils.retrieveFromDataElementsAsString(this.isoMessageResponse, 39);
    }

    private String getFromTagCollection(int tag) {
        try {
            return this.readCardResponse.getCardDetails().getTagCollection().get(tag).getData();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
