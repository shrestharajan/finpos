package global.citytech.finpos.device.weipass.emv.aid

import com.wiseasy.emvprocess.KernelInit
import com.wiseasy.emvprocess.bean.AIDData
import com.wiseasy.emvprocess.bean.BaseAIDData
import global.citytech.finpos.device.weipass.utils.WeipassConstant.EMPTY_AID_LIST
import global.citytech.finpos.device.weipass.utils.WeipassConstant.FAILURE_AID_ERASE
import global.citytech.finpos.device.weipass.utils.WeipassConstant.FAILURE_AID_INJECTION
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SUCCESS_AID_ERASE
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SUCCESS_AID_INJECTION
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finpos.device.weipass.utils.defaultOrEmptyValue
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import sdk4.wangpos.libemvbinder.utils.MoneyUtil

/**
 * Created by Rishav Chudal on 5/3/20.
 */
class WeipassAidServiceImpl :
    global.citytech.finposframework.hardware.emv.aid.AidParametersService {

    private var finalSuccessCount = 0
    private lateinit var deviceResponse: DeviceResponse
    private lateinit var visaAidManager: WeipassVisaAidManager
    private lateinit var amexAidManager: WeipassAmexAidManager
    private lateinit var masterCardAidManager: WeipassMasterCardAidManager
    private lateinit var unionPayAidManager: WeipassUnionPayAidManager

    override fun injectAid(request: global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest): DeviceResponse {
        deviceResponse = DeviceResponse(Result.FAILURE, FAILURE_AID_INJECTION)
        if (request.aidParametersList.isEmpty()) {
            deviceResponse = DeviceResponse(Result.FAILURE, EMPTY_AID_LIST)
        } else {
            eraseAllAid()
            processEachAidForInjection(request)
            if (finalSuccessCount > 0) {
                deviceResponse = DeviceResponse(Result.SUCCESS, SUCCESS_AID_INJECTION)
            }
        }
        return deviceResponse
    }

    override fun eraseAllAid(): DeviceResponse {
        WeipassLogger.log("Erasing All AID")
        var coreResponse = DeviceResponse(Result.FAILURE, FAILURE_AID_ERASE)
        var result = -1
        result = KernelInit.deleteAID()
        if (result == 0) {
            coreResponse = DeviceResponse(Result.SUCCESS, SUCCESS_AID_ERASE)
        } else {
            WeipassLogger.log("Couldn't Clear AID Keys")
        }
        WeipassLogger.log("Erase All AID result ::: ".plus(result))
        return coreResponse
    }

    private fun processEachAidForInjection(injectionRequest: global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest) {
        for (aidParameters in injectionRequest.aidParametersList) {
            try {
                WeipassLogger.log(
                    "=============== AID ::: ".plus(aidParameters.aid).plus(" ===============")
                )
                checkForThePaymentCard(
                    aidParameters, injectionRequest.emvParametersRequest,
                    injectionRequest.supportedTransactionTypes
                )
            } catch (exception: Exception) {
                WeipassLogger.log("Exception ::: ".plus(exception.message))
            }
        }
        WeipassLogger.log(
            "Final AID Injection Success Count  ::: ".plus(finalSuccessCount)
                .plus(" out of ::: ".plus(injectionRequest.aidParametersList.size))
        )
    }

    private fun checkForThePaymentCard(
        aidParameters: global.citytech.finposframework.hardware.emv.aid.AidParameters,
        emvParametersRequest: EmvParametersRequest,
        supportedTransactionTypes: List<String>
    ) {
        if (aidParameters.aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid)) {
            processForVisaAid(aidParameters, emvParametersRequest)
        } else if (aidParameters.aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid)) {
            processForAmexAid(aidParameters, emvParametersRequest)
        } else if (aidParameters.aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid)) {
            processForMasterCardAid(aidParameters, emvParametersRequest, supportedTransactionTypes)
        } else if (aidParameters.aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid)) {
            processForUnionPayAid(aidParameters, emvParametersRequest)
        } else {
            processForOtherAid(aidParameters, emvParametersRequest)
        }
    }

    private fun processForVisaAid(
        aidParameters: global.citytech.finposframework.hardware.emv.aid.AidParameters,
        emvParametersRequest: EmvParametersRequest
    ) {
        WeipassLogger.debug("AID is VISA AID ...")
        val baseAIDData = prepareBaseAidData(aidParameters, emvParametersRequest)
        visaAidManager = WeipassVisaAidManager(baseAIDData, aidParameters, emvParametersRequest)
        finalSuccessCount += visaAidManager.injectVisaAidInCore()
    }

    private fun processForAmexAid(
        aidParameters: global.citytech.finposframework.hardware.emv.aid.AidParameters,
        emvParametersRequest: EmvParametersRequest
    ) {
        WeipassLogger.debug("AID is AMEX AID ...")
        val baseAIDData = prepareBaseAidData(aidParameters, emvParametersRequest)
        amexAidManager = WeipassAmexAidManager(baseAIDData, aidParameters, emvParametersRequest)
        finalSuccessCount += amexAidManager.injectAmexAidConfigurations()
    }

    private fun processForMasterCardAid(
        aidParameters: global.citytech.finposframework.hardware.emv.aid.AidParameters,
        emvParametersRequest: EmvParametersRequest,
        supportedTransactionTypes: List<String>
    ) {
        WeipassLogger.debug("AID is MASTERCARD AID ...")
        val baseAIDData = prepareBaseAidData(aidParameters, emvParametersRequest)
        masterCardAidManager = WeipassMasterCardAidManager(
            baseAIDData, aidParameters,
            emvParametersRequest, supportedTransactionTypes
        )
        finalSuccessCount += masterCardAidManager.injectMCAidConfigurations()
    }

    private fun processForUnionPayAid(
        aidParameters: global.citytech.finposframework.hardware.emv.aid.AidParameters,
        emvParametersRequest: EmvParametersRequest
    ) {
        WeipassLogger.debug("AID is UNION PAY AID ...")
        val baseAIDData = prepareBaseAidData(aidParameters, emvParametersRequest)
        unionPayAidManager = WeipassUnionPayAidManager(
            baseAIDData, aidParameters,
            emvParametersRequest
        )
        finalSuccessCount += unionPayAidManager.injectUnionPayAidInCore()
    }

    private fun processForOtherAid(
        aidParameters: global.citytech.finposframework.hardware.emv.aid.AidParameters,
        emvParametersRequest: EmvParametersRequest
    ) {
        WeipassLogger.debug("AID is Other AID ...")
        val aidData = AIDData()
        aidData.baseAIDData = prepareBaseAidData(aidParameters, emvParametersRequest)
        val result = KernelInit.addAID(aidData)
        WeipassLogger.log(
            "Aid ".plus(aidParameters.aid).plus(" Injection Result ::: ".plus(result))
        )
        if (result == 0) {
            finalSuccessCount += 1
        }
    }

    private fun prepareBaseAidData(
        aidParameters: global.citytech.finposframework.hardware.emv.aid.AidParameters,
        emvParametersRequest: EmvParametersRequest
    ): BaseAIDData {
        WeipassLogger.log("===== BaseAIDData ::: Contact =====")
        val baseAIDData = BaseAIDData()

        val aid = aidParameters.aid.defaultOrEmptyValue()
        baseAIDData.aiD_9F06 = aid
        WeipassLogger.debug("Aid ::: ".plus(aid))

        val label = aidParameters.label.defaultOrEmptyValue()
        baseAIDData.appName = label
        WeipassLogger.debug("Label ::: ".plus(label))

        val aidVersion = aidParameters.terminalAidVersion.defaultOrEmptyValue()
        baseAIDData.version_9F09 = aidVersion
        WeipassLogger.debug("Aid Version ::: ".plus(aidVersion))

        val defaultTdol = aidParameters.defaultTDOL.defaultOrEmptyValue()
        baseAIDData.defaultTDOL = defaultTdol
        WeipassLogger.debug("Default TDOL ::: ".plus(defaultTdol))

        val defaultDdol = aidParameters.defaultDDOL.defaultOrEmptyValue()
        baseAIDData.defaultDDOL = defaultDdol
        WeipassLogger.debug("Default DDOL ::: ".plus(defaultDdol))

        val tacDenial = aidParameters.denialActionCode.defaultOrEmptyValue()
        baseAIDData.tacDenial = tacDenial
        WeipassLogger.debug("Denial Action Code ::: ".plus(tacDenial))

        val tacOnline = aidParameters.onlineActionCode.defaultOrEmptyValue()
        baseAIDData.tacOnline = tacOnline
        WeipassLogger.debug("Online Action Code ::: ".plus(tacOnline))

        val tacDefault = aidParameters.defaultActionCode.defaultOrEmptyValue()
        baseAIDData.tacDefault = tacDefault
        WeipassLogger.debug("Default Action Code ::: ".plus(tacDefault))

        var terminalFloorLimit = aidParameters.terminalFloorLimit.defaultOrEmptyValue()
        terminalFloorLimit = MoneyUtil.toCent(terminalFloorLimit)
        baseAIDData.floorLimit_9F1B = terminalFloorLimit
        WeipassLogger.debug("Terminal Floor Limit ::: ".plus(terminalFloorLimit))

        val isFullMatch = "0"
        baseAIDData.isFullMatch = isFullMatch
        WeipassLogger.debug("Aid Selection Flag ::: ".plus(isFullMatch))

        val enableFloorLimitCheck = "1"
        baseAIDData.enableFloorLimitCheck = enableFloorLimitCheck
        WeipassLogger.debug("Enable floor limit check ::: ".plus(enableFloorLimitCheck))

        val enableTerminalInfo = "1"
        baseAIDData.enableTerminalInfo = enableTerminalInfo
        WeipassLogger.debug("Enable Terminal Info ::: ".plus(enableFloorLimitCheck))

        val merchantId = emvParametersRequest.merchantIdentifier.defaultOrEmptyValue()
        baseAIDData.merchantId_9F16 = merchantId
        WeipassLogger.debug("Merchant Id ::: ".plus(merchantId))

        val terminalId = emvParametersRequest.terminalId.defaultOrEmptyValue()
        baseAIDData.terminalId_9F1C = terminalId
        WeipassLogger.debug("Terminal Id ::: ".plus(terminalId))

        val merchantName = emvParametersRequest.merchantName.defaultOrEmptyValue()
        baseAIDData.merchantName = merchantName
        WeipassLogger.debug("Merchant Name ::: ".plus(merchantName))

        val mcc = emvParametersRequest.merchantCategoryCode.defaultOrEmptyValue()
        baseAIDData.mcC_9F15 = mcc
        WeipassLogger.debug("Merchant Category Code ::: ".plus(mcc))

        val transCurrCode = emvParametersRequest.transactionCurrencyCode.defaultOrEmptyValue()
        baseAIDData.transCurrencyCode_5F2A = transCurrCode
        WeipassLogger.debug("Transaction Currency Code ::: ".plus(transCurrCode))

        val transCurrExp = emvParametersRequest.transactionCurrencyExponent.defaultOrEmptyValue()
        baseAIDData.transCurrencyExp = transCurrExp
        WeipassLogger.debug("Transaction Currency Exponent ::: ".plus(transCurrExp))

        val referCurrCode = emvParametersRequest.transactionCurrencyCode.defaultOrEmptyValue()
        baseAIDData.referCurrencyCode_9F3C = referCurrCode
        WeipassLogger.debug("Refer Currency Code ::: ".plus(referCurrCode))

        val referCurrExp = emvParametersRequest.transactionCurrencyExponent.defaultOrEmptyValue()
        baseAIDData.referCurrencyExp_9F3D = referCurrExp
        WeipassLogger.debug("Refer Currency Exponent ::: ".plus(referCurrExp))

        val terminalType = emvParametersRequest.terminalType.defaultOrEmptyValue()
        baseAIDData.terminalType_9F35 = terminalType
        WeipassLogger.debug("Terminal Type ::: ".plus(terminalType))

        val terminalCap = emvParametersRequest.terminalCapabilities.defaultOrEmptyValue()
        baseAIDData.terminalCap_9F33 = terminalCap
        WeipassLogger.debug("Terminal Capabilities ::: ".plus(terminalCap))

        val exTerminalCap =
            emvParametersRequest.additionalTerminalCapabilities.defaultOrEmptyValue()
        baseAIDData.exTerminalCap_9F40 = exTerminalCap
        WeipassLogger.debug("Additional Terminal Capabilities ::: ".plus(exTerminalCap))

        val countryCode = emvParametersRequest.terminalCountryCode.defaultOrEmptyValue()
        baseAIDData.countryCode_9F1A = countryCode
        WeipassLogger.debug("Country Code ::: ".plus(countryCode))

        val displayOfflinePinRetryTime = "1"
        baseAIDData.displayOfflinePINRetryTime = displayOfflinePinRetryTime
        WeipassLogger.debug("Display Offline Pin Retry Limit ::: ".plus(displayOfflinePinRetryTime))

        val riskManagementData = aidParameters.terminalRiskManagementData.defaultOrEmptyValue()
        baseAIDData.riskManagementData_9F1D = riskManagementData
        WeipassLogger.debug("Risk Management Data ::: ".plus(riskManagementData))

        return baseAIDData
    }
}