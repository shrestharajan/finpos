package global.citytech.finposframework.hardware.network

/**
 * Created by Rishav Chudal on 6/7/20.
 */
enum class NetworkType {
    WIFI, GPRS
}