package global.citytech.finpos.merchant.framework.datasource.core.autoreversal

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.autoreversal.AutoReversalDataSource
import global.citytech.finpos.merchant.domain.model.autoreversal.AutoReversalRequestEntity
import global.citytech.finpos.merchant.domain.model.autoreversal.AutoReversalResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.presentation.data.mapToAutoReversalResponseUIModel
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finpos.processor.nibl.transaction.autoreversal.AutoReversalRequestModel
import global.citytech.finpos.processor.nibl.transaction.autoreversal.AutoReversalResponseModel
import global.citytech.finposframework.switches.ProcessorManager
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 6/22/2021.
 */
class AutoReversalDataSourceImpl : AutoReversalDataSource {
    override fun performAutoReversal(configurationItem: ConfigurationItem): Observable<AutoReversalResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val transactionRepository = TransactionRepositoryImpl(NiblMerchant.INSTANCE)
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
        val printerService = PrinterSourceImpl(NiblMerchant.INSTANCE)
        val autoReversalRequestEntity = AutoReversalRequestEntity(
            applicationRepository = applicationRepository,
            printerService = printerService,
            transactionRepository = transactionRepository)
        val autoReversalRequester = ProcessorManager.getInterface(
            terminalRepository,
            NotificationHandler
        ).autoReversalRequester
        return Observable.fromCallable {
            (autoReversalRequester.execute(autoReversalRequestEntity.mapToModel())).mapToAutoReversalResponseUIModel()
        }
    }
}