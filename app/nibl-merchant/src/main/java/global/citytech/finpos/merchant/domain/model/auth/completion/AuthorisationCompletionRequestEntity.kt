package global.citytech.finpos.merchant.domain.model.auth.completion

import global.citytech.finpos.merchant.presentation.model.preauth.completion.AuthorisationCompletionRequestItem
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService
import global.citytech.finposframework.hardware.io.led.LedService
import global.citytech.finposframework.hardware.io.printer.PrinterService
import global.citytech.finposframework.hardware.io.sound.SoundService
import global.citytech.finposframework.repositories.ApplicationRepository
import global.citytech.finposframework.repositories.TransactionRepository
import global.citytech.finposframework.usecases.controllers.DeviceController
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator


/**
 * Created by Saurav Ghimire on 2/15/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
data class AuthorisationCompletionRequestEntity(
    var transactionRepository: TransactionRepository? = null,
    var readCardService: ReadCardService? = null,
    var deviceController: DeviceController? = null,
    var transactionAuthenticator: TransactionAuthenticator? = null,
    var printerService: PrinterService? = null,
    var applicationRepository: ApplicationRepository? = null,
    var authorisationCompletionRequest: AuthorisationCompletionRequestItem? = null,
    var ledService: LedService? = null,
    var soundService: SoundService? = null
)