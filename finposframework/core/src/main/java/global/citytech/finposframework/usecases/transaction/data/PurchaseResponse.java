package global.citytech.finposframework.usecases.transaction.data;

public class PurchaseResponse {

    private PurchaseResult result;
    private String message;
    private String authorizationCode;
    private boolean returnToDashboard = true;
    private String retrievalReferenceNumber;
    private boolean reprint = false;

    public PurchaseResponse(PurchaseResult result, String message, String authorizationCode) {
        this.result = result;
        this.message = message;
        this.authorizationCode = authorizationCode;
    }

    public void setResult(PurchaseResult result) {
        this.result = result;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public PurchaseResult getResult() {
        return result;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public String getMessage() {
        return message;
    }

    public boolean isReturnToDashboard() {
        return returnToDashboard;
    }

    public void setReturnToDashboard(boolean returnToDashboard) {
        this.returnToDashboard = returnToDashboard;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    public boolean isReprint() {
        return reprint;
    }

    public void setReprint(boolean reprint) {
        this.reprint = reprint;
    }
}