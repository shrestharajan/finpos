package global.citytech.finpos.processor.nibl.transaction.refund;

import global.citytech.finpos.processor.nibl.transaction.TransactionRequestModel;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.io.sound.SoundService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;

/**
 * Created by Unique Shakya on 9/28/2020.
 */
public class RefundRequestModel extends TransactionRequestModel {

    public static final class Builder {
        TransactionRequest transactionRequest;
        TransactionRepository transactionRepository;
        ReadCardService readCardService;
        DeviceController deviceController;
        TransactionAuthenticator transactionAuthenticator;
        PrinterService printerService;
        ApplicationRepository applicationRepository;
        LedService ledService;
        SoundService soundService;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withTransactionRequest(TransactionRequest transactionRequest) {
            this.transactionRequest = transactionRequest;
            return this;
        }

        public Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public Builder withReadCardService(ReadCardService readCardService) {
            this.readCardService = readCardService;
            return this;
        }

        public Builder withDeviceController(DeviceController deviceController) {
            this.deviceController = deviceController;
            return this;
        }

        public Builder withTransactionAuthenticator(TransactionAuthenticator transactionAuthenticator) {
            this.transactionAuthenticator = transactionAuthenticator;
            return this;
        }

        public Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public Builder withLedService(LedService ledService) {
            this.ledService = ledService;
            return this;
        }

        public Builder withSoundService(SoundService soundService) {
            this.soundService = soundService;
            return this;
        }

        public RefundRequestModel build() {
            RefundRequestModel refundRequestModel = new RefundRequestModel();
            refundRequestModel.printerService = this.printerService;
            refundRequestModel.transactionRepository = this.transactionRepository;
            refundRequestModel.deviceController = this.deviceController;
            refundRequestModel.transactionRequest = this.transactionRequest;
            refundRequestModel.readCardService = this.readCardService;
            refundRequestModel.transactionAuthenticator = this.transactionAuthenticator;
            refundRequestModel.applicationRepository = this.applicationRepository;
            refundRequestModel.ledService = this.ledService;
            refundRequestModel.soundService = this.soundService;
            return refundRequestModel;
        }
    }
}
