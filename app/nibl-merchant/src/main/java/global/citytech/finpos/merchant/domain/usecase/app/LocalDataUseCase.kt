package global.citytech.finpos.merchant.domain.usecase.app

import global.citytech.finpos.merchant.domain.model.app.*
import global.citytech.finpos.merchant.domain.model.device.DeviceResponse
import global.citytech.finpos.merchant.domain.repository.app.LocalRepository
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.presentation.model.ConfigResponse
import global.citytech.finpos.merchant.presentation.model.banner.Banner
import global.citytech.finpos.merchant.presentation.model.setting.TerminalSettingResponse
import global.citytech.finpos.merchant.presentation.purchase.PaymentItems
import global.citytech.finposframework.switches.posmode.PosMode
import global.citytech.finposframework.usecases.TransactionType
import io.reactivex.Observable
import io.reactivex.functions.BiFunction


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
data class HostMerchant(val hostList: List<Host>, val merchantList: List<Merchant>)
data class Configuration(
    val hostList: List<Host>,
    val merchantList: List<Merchant>,
    val logos: List<Logo>
)

class LocalDataUseCase(private val localRepository: LocalRepository) {

    private fun getHosts(): Observable<List<Host>> = localRepository.getHost()
    private fun getMerchants(): Observable<List<Merchant>> = localRepository.getMerchant()
    private fun getLogos(): Observable<List<Logo>> = localRepository.getLogo()
    fun getBanners(): List<Banner> = localRepository.getBanners()
    fun setBanners(banners: List<Banner>) = localRepository.setBanners(banners)

    private fun getConfiguration(): Observable<HostMerchant> =
        Observable.zip(getHosts(), getMerchants(), BiFunction { t1, t2 -> map(t1, t2) })

    fun getWholeConfiguration(): Observable<Configuration> =
        Observable.zip(getConfiguration(), getLogos(), BiFunction { t1, t2 -> map(t1, t2) })

    private fun map(hostList: List<Host>, merchantList: List<Merchant>): HostMerchant =
        HostMerchant(
            hostList,
            merchantList
        )

    private fun map(hostMerchant: HostMerchant, logos: List<Logo>): Configuration =
        Configuration(
            hostMerchant.hostList,
            hostMerchant.merchantList,
            logos
        )

    fun getTransactionByInvoiceNumber(invoiceNumber: String, ignoreBatchNumber: Boolean) =
        localRepository.getTransactionLogByInvoice(invoiceNumber, ignoreBatchNumber)

    fun getTransactionByRRN(rrn: String) = localRepository.getTransactionLogByRRN(rrn)

    fun getTransactionByStan(stan: String) = localRepository.getTransactionLogByStan(stan)

    fun saveConfigResponseAndInjectKey(
        configResponse: ConfigResponse
    ): Observable<DeviceResponse> =
        localRepository.saveConfigResponseAndInjectKey(configResponse)

    fun saveSettlementActiveStatus(value: Boolean) {
        localRepository.saveSettlementActivePref(value);
    }

    fun isSettlementActive(defaultValue: Boolean): Boolean {
        return localRepository.getSettlementActivePref(defaultValue);
    }

    fun saveTerminalConfigurationStatus(value: Boolean) {
        localRepository.saveTerminalConfigPref(value);
    }

    fun isTerminalConfigured(defaultValue: Boolean): Boolean {
        return localRepository.getTerminalConfigPref(defaultValue);
    }

    fun saveDebugModeStatus(value: Boolean) {
        localRepository.saveDebugModePreference(value);
    }

    fun saveSoundModeStatus(value: Boolean) {
        localRepository.saveSoundModePreference(value);
    }

    fun isDebugMode(defaultValue: Boolean): Boolean {
        return localRepository.getDebugModePreference(defaultValue);
    }

    fun isSoundMode(defaultValue: Boolean): Boolean {
        return localRepository.getSoundModePreference(defaultValue);
    }

    fun clearAllPreferences() {
        localRepository.clearPreferences()
    }

    fun isTransactionPresent(invoiceNumber: String): Observable<Boolean> {
        return localRepository.isTransactionPresent(invoiceNumber)
    }

    fun isTransactionPresentByAuthCode(authCode: String): Observable<Boolean> {
        return localRepository.isTransactionPresentByAuthCode(authCode)
    }

    fun isTransactionPresentByRrn(rrn: String): Observable<Boolean> {
        return localRepository.isTransactionPresentByRrn(rrn)
    }

    fun setCardMonitoringPreference(cardMonitoring: Boolean) {
        localRepository.setCardMonitoringPreference(cardMonitoring)
    }

    fun getCardMonitoringPreference(defaultValue: Boolean): Boolean {
        return localRepository.getCardMonitoringPreference(defaultValue)
    }

    fun setForceSettlementPreference(forceSettlement: Boolean) {
        localRepository.setForceSettlementPreference(forceSettlement)
    }

    fun getForceSettlementPreference(defaultValue: Boolean): Boolean {
        return localRepository.getForceSettlementPreference(defaultValue)
    }

    fun savePosMode(posMode: PosMode) {
        localRepository.savePosMode(posMode)
    }

    fun getPosMode(defaultPosMode: PosMode): PosMode {
        return localRepository.getPosMode(defaultPosMode)
    }

    fun getApprovedTransactions(): Observable<List<TransactionLog>> {
        return localRepository.getTransactionLogs()
    }

    fun getApprovedTransactions(limit: Int, offset: Int): Observable<List<TransactionLog>> {
        return localRepository.getTransactionLogs(limit, offset)
    }

    fun searchApprovedTransactions(
        searchParam: String,
        limit: Int,
        offset: Int
    ): Observable<List<TransactionLog>> {
        return localRepository.searchTransactionLogs(searchParam, limit, offset)
    }

    fun countApprovedTransactions(): Int {
        return localRepository.countTransactionLogs()
    }

    fun countSearchApprovedTransactions(searchParam: String): Int {
        return localRepository.countSearchTransactionLogs(searchParam)
    }

    fun saveTipLimit(value: Int) {
        localRepository.saveTipLimit(value)
    }

    fun saveNumpadLayoutType(numpadLayoutType: String){ localRepository.saveNumpadLayoutType(numpadLayoutType) }

    fun getNumpadLayoutType(defaultValue: String): String =  localRepository.getNumpadLayoutType(defaultValue)

    fun retrieveTipLimit(): Int {
        return localRepository.retrieveTipLimit()
    }

    fun getAdminPassword(): Observable<String> {
        return localRepository.getAdminPassword()
    }

    fun getEnabledTransactionsForGivenPosMode(posMode: PosMode): Observable<List<TransactionType>> =
        localRepository.getEnabledTransactionsForThePosMode(posMode)

    fun getSettlementTime(defaultValue: String): String =
        localRepository.getSettlementTime(defaultValue)

    fun storeSettlementTime(settlementTime: String) =
        localRepository.storeSettlementTime(settlementTime)

    fun getLastSuccessSettlementTimeInMillis(defaultValue: Long): Long =
        localRepository.getLastSuccessSettlementTimeInMillis(defaultValue)

    fun updateLastSuccessSettlementTimeInMillis(settlementTime: Long) =
        localRepository.updateLastSuccessSettlementTimeInMillis(settlementTime)

    fun updateReconciliationBatchNumber(batchNumber: Int) =
        localRepository.updateReconciliationBatchNumber(batchNumber)


    fun getExistingSettlementBatchNumber(): Observable<String> {
        return localRepository.getExistingSettlementBatchNumber()
    }

    fun getTipAdjustedTransactionLogOfInvoiceNumber(invoiceNumber: String): Observable<TransactionLog> =
        localRepository.getTipAdjustedTransactionLogOfInvoiceNumber(invoiceNumber)

    fun getDeviceConfiguration(): DeviceConfiguration {
        return localRepository.getDeviceConfiguration()
    }

    fun saveCardConfirmationRequiredPref(value: Boolean) {
        localRepository.saveCardConfirmationRequiredPref(value)
    }

    fun getCardConfirmationRequiredPref(defaultValue: Boolean): Boolean =
        localRepository.getCardConfirmationRequiredPref(defaultValue)

    fun saveVatRefundSupportedPref(value: Boolean) {
        localRepository.saveVatRefundSupportedPref(value)
    }

    fun getVatRefundSupportedPref(defaultValue: Boolean): Boolean =
        localRepository.getVatRefundSupportedPref(defaultValue)

    fun saveShouldShufflePinPadPref(value: Boolean) {
        localRepository.saveShouldShufflePinPadPref(value)
    }

    fun getShouldShufflePinPadPref(defaultValue: Boolean): Boolean =
        localRepository.getShouldShufflePinPadPref(defaultValue)

    fun saveMerchantCredential(merchantCredential: String) {
        localRepository.saveMerchantCredential(merchantCredential)
    }

    fun getMerchantCredential(): String = localRepository.getMerchantCredential()

    fun getActivityLog(): Observable<List<ActivityLog>> = localRepository.getActivityLog()

    fun nukeActivityLog(): Observable<DatabaseReponse> = localRepository.nukeActivityLog()

    fun deleteTransactionByStan(stan: String): Observable<Boolean> =
        localRepository.deleteTransactionByStan(stan)

    fun saveTerminalSetting(terminalSettingResponse: TerminalSettingResponse) {
        localRepository.saveTerminalSettings(terminalSettingResponse)
    }

    fun savePaymentInitiatorItems(paymentItems: MutableList<PaymentItems>){
        localRepository.savePaymentInitiatorItems(paymentItems)
    }

    fun getPaymentInitiatorItems(): ArrayList<PaymentItems> = localRepository.getPaymentInitiatorItems()

    fun updateShowQrDisclaimerSetting(showDisclaimer: Boolean) {
        localRepository.updateShowQrDisclaimerSetting(showDisclaimer)
    }

    fun getShowQrDisclaimerSetting(): Boolean = localRepository.getShowQrDisclaimerSetting()

    fun getTerminalSetting(): TerminalSettingResponse {
        return localRepository.getTerminalSettings()
    }

    fun isDisclaimerShown(): Boolean {
        return localRepository.isDisclaimerShown()
    }

    fun saveDisclaimerShownStatusPref(value: Boolean) {
        localRepository.saveDisclaimerShownStatusPref(value)
    }

    fun getInvoiceNumberByRrn(rrn: String): Observable<String> =
        localRepository.getInvoiceNumberByRrn(rrn)

    fun insertToActivityLog(activityLog: ActivityLog) =
        localRepository.insertToActivityLog(activityLog)

    fun addToDisclaimers(disclaimer: Disclaimer): Observable<Boolean> =
        localRepository.addToDisclaimers(disclaimer)

    fun addListToDisclaimers(disclaimers: List<Disclaimer>): Observable<Boolean> =
        localRepository.addListToDisclaimers(disclaimers)

    fun retrieveAllDisclaimers(): Observable<List<Disclaimer>> =
        localRepository.retrieveAllDisclaimers()

    fun retrieveDisclaimerById(disclaimerId: String): Observable<Disclaimer> =
        localRepository.retrieveDisclaimerById(disclaimerId)

    fun updateRemarksById(disclaimerId: String, remarks: String): Observable<Boolean> =
        localRepository.updateRemarksById(disclaimerId, remarks)

    fun deleteById(disclaimerId: String): Observable<Boolean> =
        localRepository.deleteById(disclaimerId)

    fun saveAppDownloadExtra(value: String) =
        localRepository.saveAppDownloadExtra(value)

    fun getAppDownloadExtra(): String = localRepository.getAppDownloadExtra()
    fun getStoredAppVersion() = localRepository.getStoredAppVersion()

    fun saveAppVersion(version: Int) = localRepository.saveAppVersion(version)

    fun isSwitchConfigActive() = localRepository.isSwitchConfigActive()

    fun setSwitchConfigActive(active: Boolean) = localRepository.setSwitchConfigActive(active)

    fun isBroadcastAcknowledgeActive() = localRepository.isBroadcastAcknowledgeActive()

    fun setBroadcastAcknowledgeActive(active: Boolean) =
        localRepository.setBroadcastAcknowledgeActive(active)

    fun isAppResetActive(defaultValue: Boolean) = localRepository.isAppResetActive(defaultValue)

    fun setAppResetActive(active: Boolean) = localRepository.setAppResetActive(active)

    fun nukeAllDatabaseTable(): Observable<DatabaseReponse> = localRepository.nukeAllDatabaseTable()

    fun getAmountPerTransaction() = localRepository.getAmountPerTransaction()

    fun getAmountLengthLimit() = localRepository.getAmountLengthLimit()

    fun saveAmountLengthLimit(value: Int) = localRepository.saveAmountLengthLimit(value)

    fun getFonePayLoginState(defaultValue: Boolean): Boolean = localRepository.getFonePayLoginState(defaultValue)

    fun saveFonePayLoginState(value: Boolean) {
        localRepository.saveFonePayLoginState(value)
    }

    fun saveGreenPinStatus(value: Boolean) = localRepository.saveGreenPinStatus(value)

    fun getGreenPinStatus(): Boolean = localRepository.getGreenPinStatus()
}