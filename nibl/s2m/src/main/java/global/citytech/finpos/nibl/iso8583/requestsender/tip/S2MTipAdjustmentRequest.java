package global.citytech.finpos.nibl.iso8583.requestsender.tip;

import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.transaction.Request;

/**
 * Created by Unique Shakya on 4/26/2021.
 */
public class S2MTipAdjustmentRequest implements Request {

    private String pan;
    private double amount;
    private String localTime;
    private String localDate;
    private String expireDate;
    private PosEntryMode posEntryMode;
    private String nii;
    private String posConditionCode;
    private String track2Data;
    private String originalRrn;
    private String originalAuthCode;
    private String originalResponseCode;
    private String pinData;
    private double tipAmount;
    private String iccDataBlock;
    private double originalAmount;
    private String originalInvoiceNumber;
    private String additionalData;

    private S2MTipAdjustmentRequest() {
    }

    public String getPan() {
        return pan;
    }

    public double getAmount() {
        return amount;
    }

    public String getLocalTime() {
        return localTime;
    }

    public String getLocalDate() {
        return localDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public String getNii() {
        return nii;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public String getTrack2Data() {
        return track2Data;
    }

    public String getOriginalRrn() {
        return originalRrn;
    }

    public String getOriginalAuthCode() {
        return originalAuthCode;
    }

    public String getOriginalResponseCode() {
        return originalResponseCode;
    }

    public String getPinData() {
        return pinData;
    }

    public double getTipAmount() {
        return tipAmount;
    }

    public String getIccDataBlock() {
        return iccDataBlock;
    }

    public double getOriginalAmount() {
        return originalAmount;
    }

    public String getOriginalInvoiceNumber() {
        return originalInvoiceNumber;
    }

    public String getAdditionalData() {
        return additionalData;
    }


    public static final class Builder {
        private String pan;
        private double amount;
        private String localTime;
        private String localDate;
        private String expireDate;
        private PosEntryMode posEntryMode;
        private String nii;
        private String posConditionCode;
        private String track2Data;
        private String originalRrn;
        private String originalAuthCode;
        private String originalResponseCode;
        private String pinData;
        private double tipAmount;
        private String iccDataBlock;
        private double originalAmount;
        private String originalInvoiceNumber;
        private String additionalData;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public Builder amount(double amount) {
            this.amount = amount;
            return this;
        }

        public Builder localTime(String localTime) {
            this.localTime = localTime;
            return this;
        }

        public Builder localDate(String localDate) {
            this.localDate = localDate;
            return this;
        }

        public Builder expireDate(String expireDate) {
            this.expireDate = expireDate;
            return this;
        }

        public Builder posEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public Builder nii(String nii) {
            this.nii = nii;
            return this;
        }

        public Builder posConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public Builder originalRrn(String originalRrn) {
            this.originalRrn = originalRrn;
            return this;
        }

        public Builder originalAuthCode(String originalAuthCode) {
            this.originalAuthCode = originalAuthCode;
            return this;
        }

        public Builder originalResponseCode(String originalResponseCode) {
            this.originalResponseCode = originalResponseCode;
            return this;
        }

        public Builder pinData(String pinData) {
            this.pinData = pinData;
            return this;
        }

        public Builder tipAmount(double tipAmount) {
            this.tipAmount = tipAmount;
            return this;
        }

        public Builder iccDataBlock(String iccDataBlock) {
            this.iccDataBlock = iccDataBlock;
            return this;
        }

        public Builder originalAmount(double originalAmount) {
            this.originalAmount = originalAmount;
            return this;
        }

        public Builder originalInvoiceNumber(String originalInvoiceNumber) {
            this.originalInvoiceNumber = originalInvoiceNumber;
            return this;
        }

        public Builder additionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public S2MTipAdjustmentRequest build() {
            S2MTipAdjustmentRequest s2MTipAdjustmentRequest = new S2MTipAdjustmentRequest();
            s2MTipAdjustmentRequest.track2Data = this.track2Data;
            s2MTipAdjustmentRequest.amount = this.amount;
            s2MTipAdjustmentRequest.posEntryMode = this.posEntryMode;
            s2MTipAdjustmentRequest.originalRrn = this.originalRrn;
            s2MTipAdjustmentRequest.iccDataBlock = this.iccDataBlock;
            s2MTipAdjustmentRequest.localDate = this.localDate;
            s2MTipAdjustmentRequest.originalAuthCode = this.originalAuthCode;
            s2MTipAdjustmentRequest.posConditionCode = this.posConditionCode;
            s2MTipAdjustmentRequest.pan = this.pan;
            s2MTipAdjustmentRequest.originalAmount = this.originalAmount;
            s2MTipAdjustmentRequest.originalResponseCode = this.originalResponseCode;
            s2MTipAdjustmentRequest.localTime = this.localTime;
            s2MTipAdjustmentRequest.tipAmount = this.tipAmount;
            s2MTipAdjustmentRequest.additionalData = this.additionalData;
            s2MTipAdjustmentRequest.originalInvoiceNumber = this.originalInvoiceNumber;
            s2MTipAdjustmentRequest.pinData = this.pinData;
            s2MTipAdjustmentRequest.nii = this.nii;
            s2MTipAdjustmentRequest.expireDate = this.expireDate;
            return s2MTipAdjustmentRequest;
        }
    }
}
