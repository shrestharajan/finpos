package global.citytech.finpos.processor.nibl.transaction.template;


import java.util.List;

import global.citytech.finpos.nibl.iso8583.S2MIccDataList;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finpos.processor.nibl.NiblConstants;
import global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor;
import global.citytech.finpos.processor.nibl.transaction.card.MagneticCardProcessor;
import global.citytech.finpos.processor.nibl.transaction.card.PICCCardProcessor;
import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.led.LedAction;
import global.citytech.finposframework.hardware.io.led.LedLight;
import global.citytech.finposframework.hardware.io.led.LedRequest;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.CardProcessor;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;

/**
 * Created by Unique Shakya on 3/8/2021.
 */
public abstract class CardTransactionUseCase extends TransactionUseCaseTemplate {

    private final ReadCardRequest.MultipleAidSelectionListener multipleAidSelectionListener = aids -> {
        notifier.notify(Notifier.EventType.DETECTED_MULTIPLE_AID, "Processing..");
        return -1;
    };
    private CardProcessor cardProcessor;

    public CardTransactionUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    protected abstract String getProcessingCode();

    @Override
    protected String retrieveInvoiceNumber() {
        return this.terminalRepository.getInvoiceNumber();
    }

    @Override
    protected void retrieveCardDetails(List<CardType> allowedCardTypes, boolean isForceContactCardPrompt) {
        ReadCardRequest readCardRequest = this.prepareReadCardRequest(allowedCardTypes);
        if (isForceContactCardPrompt)
            this.notifier.notify(Notifier.EventType.FORCE_ICC_CARD, Notifier.EventType.FORCE_ICC_CARD.getDescription());
        else
            this.notifier.notify(Notifier.EventType.STARTING_READ_CARD);
        this.readCardResponse = this.detectCard(readCardRequest);
        this.retrieveCardProcessor();
        this.readCardResponse = this.cardProcessor.validateReadCardResponse(readCardRequest, this.readCardResponse);
        this.retrieveCardProcessor();
    }

    @Override
    protected void processCard() {
        PurchaseRequest purchaseRequest = this.preparePurchaseRequest();
        this.readCardResponse = this.cardProcessor.processCard(this.stan, purchaseRequest);
    }

    @Override
    protected PurchaseRequest preparePurchaseRequest() {
        PurchaseRequest purchaseRequest = super.preparePurchaseRequest();
        purchaseRequest.setFallbackIccToMag(this.readCardResponse.getFallbackIccToMag());
        return purchaseRequest;
    }

    @Override
    protected void processOnlineResultToCard(IsoMessageResponse isoMessageResponse) {
        this.notifier.notify(Notifier.EventType.PROCESSING_ISSUER_SCRIPT, "Processing");
        this.readCardResponse = this.cardProcessor.processOnlineResult(
                this.readCardResponse, isoMessageResponse
        );
    }

    @Override
    protected boolean checkTransactionDeclinedByCard(IsoMessageResponse isoMessageResponse) {
        return this.transactionValidator.isTransactionDeclineByCardEvenIfSuccessOnSwitch(isoMessageResponse,
                this.readCardResponse);
    }

    @Override
    protected void processUnableToGoOnlineResultToCard() {
        this.readCardResponse = this.cardProcessor.processUnableToGoOnlineResult(this.readCardResponse);
    }

    @Override
    protected void notifyWithLed(boolean isApproved) {
        if (this.readCardResponse.getCardDetails().getCardType() == CardType.PICC) {
            LedLight ledLight;
            if (isApproved)
                ledLight = LedLight.GREEN;
            else
                ledLight = LedLight.RED;
            this.request.getLedService().doTurnLedWith(new LedRequest(ledLight, LedAction.ON));
        }
    }

    @Override
    protected TransactionIsoRequest prepareTransactionIsoRequest() {
        return TransactionIsoRequest.Builder.newInstance()
                .pan(this.readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .transactionAmount(this.readCardResponse.getCardDetails().getAmount())
                .localDate(HelperUtils.getDefaultLocaleDateWithoutYear())
                .localTime(HelperUtils.getDefaultLocaleTimeHhMmSs())
                .posEntryMode(this.retrievePosEntryMode())
                .posConditionCode("00")
                .track2Data(this.readCardResponse.getCardDetails().getTrackTwoData())
                .additionalData("")
                .currencyCode(this.transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .pinBlock(this.shouldSkipPinBlockFromCard() ? "" : readCardResponse.getCardDetails().getPinBlock().getPin())
                .emvData(this.readCardResponse.getCardDetails().getIccDataBlock())
                .expireDate(this.readCardResponse.getCardDetails().getExpiryDate())
                .cardSequenceNumber(this.readCardResponse.getCardDetails().getPrimaryAccountNumberSerialNumber())
                .invoiceNumber(this.invoiceNumber)
                .retrievalReferenceNumber("")
                .isFallbackFromIccToMag(this.readCardResponse.getFallbackIccToMag())
                .originalRetrievalReferenceNumber(this.transactionRequest.getOriginalRetrievalReferenceNumber())
                .build();
    }

    @Override
    protected boolean isForceContactCardPromptActionCode(IsoMessageResponse isoMessageResponse) {
        if (readCardResponse.getCardDetails().getCardType() == CardType.PICC)
            return IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, 39) == 65;
        else
            return false;
    }

    private void retrieveCardProcessor() {
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case MAG:
                this.cardProcessor = new MagneticCardProcessor(
                        this.request.getTransactionRepository(),
                        this.request.getTransactionAuthenticator(),
                        this.request.getApplicationRepository(),
                        this.request.getReadCardService(),
                        this.notifier,
                        terminalRepository
                );
                break;
            case ICC:
                this.cardProcessor = new IccCardProcessor(
                        this.request.getReadCardService(),
                        this.request.getTransactionRepository(),
                        this.request.getApplicationRepository(),
                        this.request.getTransactionAuthenticator(),
                        this.notifier,
                        terminalRepository
                );
                break;
            case PICC:
                this.cardProcessor = new PICCCardProcessor(
                        this.request.getTransactionRepository(),
                        this.request.getApplicationRepository(),
                        this.request.getReadCardService(),
                        this.notifier,
                        this.request.getTransactionAuthenticator(),
                        this.request.getLedService(),
                        this.request.getSoundService(),
                        terminalRepository
                );
                break;
            default:
                break;
        }
    }

    private ReadCardResponse detectCard(ReadCardRequest readCardRequest) {
        this.request.getLedService().doTurnLedWith(new LedRequest(LedLight.BLUE, LedAction.ON));
        ReadCardResponse readCardResponse = this.request.getReadCardService().readCardDetails(readCardRequest);
        this.validateReadCardResponse(readCardResponse);
        return readCardResponse;
    }

    private void validateReadCardResponse(ReadCardResponse readCardResponse) {
        if (readCardResponse == null)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        if (readCardResponse.getResult() == Result.TIMEOUT)
            throw new PosException(PosError.DEVICE_ERROR_CARD_READ_TIMEOUT);
        if (readCardResponse.getResult() == Result.USER_CANCELLED)
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        if (readCardResponse.getResult() == Result.FAILURE)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
    }

    private ReadCardRequest prepareReadCardRequest(List<CardType> allowedCardTypes) {
        TransactionType transactionType = this.transactionRequest.getTransactionType();
        ReadCardRequest readCardRequest = new ReadCardRequest(
                allowedCardTypes,
                transactionType,
                TransactionUtils.retrieveTransactionType9C(transactionType),
                this.request.getApplicationRepository().retrieveEmvParameterRequest()
        );
        readCardRequest.setStan(this.stan);
        readCardRequest.setAmount(this.transactionRequest.getAmount());
        readCardRequest.setCashBackAmount(this.transactionRequest.getAdditionalAmount());
        readCardRequest.setTransactionDate(DateUtils.yyMMddDate());
        readCardRequest.setTransactionTime(DateUtils.HHmmssTime());
        readCardRequest.setMultipleAidSelectionListener(this.multipleAidSelectionListener);
        readCardRequest.setIccDataList(S2MIccDataList.get());
        readCardRequest.setCurrencyName(NiblConstants.PIN_PAD_CURRENCY);
        readCardRequest.setPinPadFixedLayout(!terminalRepository.shouldShufflePinPad());
        return readCardRequest;
    }

    private PosEntryMode retrievePosEntryMode() {
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case MAG:
                if (this.readCardResponse.getFallbackIccToMag())
                    return PosEntryMode.ICC_FALLBACK_TO_MAGNETIC;
                else
                    return PosEntryMode.MAGNETIC_STRIPE;
            case ICC:
                return PosEntryMode.ICC;
            case PICC:
                return PosEntryMode.PICC;
            default:
                return PosEntryMode.UNSPECIFIED;
        }
    }
}
