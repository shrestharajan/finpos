package global.citytech.finposframework.hardware.io.cards

/**
 * Created by Rishav Chudal on 6/8/20.
 */
data class PinBlock (
    val pin: String = "Offline",
    val offlinePin: Boolean = true
) {
    override fun toString(): String {
        return "PinBlock(pin='$pin', offlinePin=$offlinePin)"
    }
}