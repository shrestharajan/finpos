package global.citytech.finposframework.switches.reconciliation;

import global.citytech.finposframework.switches.RequestParameter;

/**
 * Created by Unique Shakya on 9/22/2020.
 */
public interface ReconciliationRequestParameter extends RequestParameter {
}
