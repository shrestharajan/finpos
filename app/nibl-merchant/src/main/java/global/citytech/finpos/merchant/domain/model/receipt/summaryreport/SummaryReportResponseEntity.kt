package global.citytech.finpos.merchant.domain.model.receipt.summaryreport

import global.citytech.finposframework.hardware.common.Result

/**
 * Created by Saurav Ghimire on 3/30/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


data class SummaryReportResponseEntity(
    var result: Result? = null,
    var message: String? = null
)
