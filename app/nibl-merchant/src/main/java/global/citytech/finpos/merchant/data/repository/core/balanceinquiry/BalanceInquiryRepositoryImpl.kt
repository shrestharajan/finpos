package global.citytech.finpos.merchant.data.repository.core.balanceinquiry

import global.citytech.finpos.merchant.data.datasource.core.balanceinquiry.BalanceInquiryDataSource
import global.citytech.finpos.merchant.domain.model.balanceinquiry.BalanceInquiryResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.balanceinquiry.BalanceInquiryRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.balanceinquiry.BalanceInquiryRequestItem
import io.reactivex.Observable

class BalanceInquiryRepositoryImpl(private val balanceInquiryDataSource: BalanceInquiryDataSource): BalanceInquiryRepository {
    override fun generateBalanceInquiryRequest(
        configurationItem: ConfigurationItem,
        balanceInquiryRequestItem: BalanceInquiryRequestItem
    ): Observable<BalanceInquiryResponseEntity> = balanceInquiryDataSource.generateBalanceInquiryRequest(configurationItem, balanceInquiryRequestItem)

}