package global.citytech.finpos.processor.neps.receipt.detailreport;

import java.util.List;

import global.citytech.finpos.processor.neps.receipt.summmaryreport.SummaryReportRequest;
import global.citytech.finpos.processor.neps.receipt.summmaryreport.SummaryReportUseCase;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.detailreport.DetailReportRequester;
import global.citytech.finposframework.usecases.transaction.TransactionLog;

/**
 * Created by Saurav Ghimire on 3/29/21.
 * Citytech
 */

public class DetailReportUseCase implements UseCase<DetailReportRequestModel,DetailReportResponseModel>, DetailReportRequester<DetailReportRequestModel,DetailReportResponseModel> {

    private final TerminalRepository terminalRepository;
    private final Notifier notifier;

    public DetailReportUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        this.terminalRepository = terminalRepository;
        this.notifier = notifier;
    }

    @Override
    public DetailReportResponseModel execute(DetailReportRequestModel request) {
        try{
            String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
            this.notifier.notify(Notifier.EventType.PREPARING_DETAIL_REPORT,Notifier.EventType.PREPARING_DETAIL_REPORT.getDescription());
            List<TransactionLog> transactionLogs = request.getTransactionRepository().getTransactionLogsByBatchNumber(batchNumber);
            if(transactionLogs.isEmpty())
                throw new PosException(PosError.DEVICE_ERROR_NO_RECENT_TRANSACTION);
            DetailReportReceiptMaker detailReportReceiptMaker = new DetailReportReceiptMaker(this.terminalRepository);
            DetailReportReceipt detailReportReceipt = detailReportReceiptMaker.prepare(batchNumber, transactionLogs);
            this.notifier.notify(Notifier.EventType.PRINTING_DETAIL_REPORT, Notifier.EventType.PRINTING_DETAIL_REPORT.getDescription());
            DetailReportReceiptHandler receiptHandler = new DetailReportReceiptHandler(request.getPrinterService());
            PrinterResponse printerResponse = receiptHandler.print(detailReportReceipt);
            if (printerResponse.getResult() != Result.SUCCESS) {
                return new DetailReportResponseModel(
                        printerResponse.getResult(),
                        printerResponse.getMessage()
                );
            }
            if (request.isPrintSummaryReport()) {
                SummaryReportUseCase summaryReportUseCase = new SummaryReportUseCase(this.terminalRepository);
                summaryReportUseCase.execute(SummaryReportRequest.Builder.newInstance()
                        .withReconciliationRepository(request.getReconciliationRepository())
                        .withPrinterService(request.getPrinterService())
                        .withApplicationRepository(request.getApplicationRepository())
                        .build());
            }
        } catch (PosException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
        return new DetailReportResponseModel(Result.SUCCESS, "Print Success");
    }
}
