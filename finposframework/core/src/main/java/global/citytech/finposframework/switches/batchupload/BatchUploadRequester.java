package global.citytech.finposframework.switches.batchupload;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 5/11/2021.
 */
public interface BatchUploadRequester<T extends BatchUploadRequestParameter, E extends BatchUploadResponseParameter>
        extends Request<T, E> {
}
