package global.citytech.common.extensions

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

fun Application.hasInternetConnection(): Boolean {
    val connectivity = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val info = connectivity.allNetworkInfo
    for (i in info.indices)
        if (info[i].state == NetworkInfo.State.CONNECTED) {
            return true
        }

    return false
}