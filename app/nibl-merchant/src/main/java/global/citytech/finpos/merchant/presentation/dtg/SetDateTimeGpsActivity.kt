package global.citytech.finpos.merchant.presentation.dtg

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivitySetDateTimeGpsBinding
import kotlinx.android.synthetic.main.activity_set_date_time_gps.*

class SetDateTimeGpsActivity : AppBaseActivity<ActivitySetDateTimeGpsBinding,SetDateTimeGpsViewModel>(),SetGpsFragment.SetGpsListener {

    private val REQUEST_CODE_DATE_TIME = 1022

    lateinit var dialogFragment: DialogFragment


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        initObservers()
    }

    private fun initObservers() {
        getViewModel().date.observe(this, Observer {
            tv_date.text = it
        })
        getViewModel().time.observe(this, Observer {
            tv_time.text = it
        })
    }

    private fun initViews() {
        this.dialogFragment = SetGpsFragment.newInstance()
        cl_set_gps.setOnClickListener {
            startSetGpsDialog()
        }
        cl_set_date.setOnClickListener {
            startActivityForResult(
                Intent(Settings.ACTION_DATE_SETTINGS),
                REQUEST_CODE_DATE_TIME
            )
        }
        cl_set_time.setOnClickListener {
            cl_set_date.performClick()
        }
        btn_save.setOnClickListener {
            getViewModel().
            saveGps(tv_latitude.text.toString(),tv_longitude.text.toString())
            finish()
        }
        iv_back.setOnClickListener {
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        getViewModel().retrieveDateTime()
        getViewModel().retrieveCurrentLocation()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK &&
            requestCode == REQUEST_CODE_DATE_TIME
        ) {
            this.getViewModel().retrieveDateTime()
        }
    }

    private fun startSetGpsDialog() {
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        val prev: Fragment? =
            supportFragmentManager.findFragmentByTag(getString(R.string.tag_gps_dialog))
        if (prev != null) {
            ft.remove(prev)
        }
        this.dialogFragment.setCancelable(false)
        this.dialogFragment.show(ft, getString(R.string.tag_gps_dialog))
    }

    override fun onDestroy() {
        if (dialogFragment.isVisible) {
            dialogFragment.dismiss()
        }
        super.onDestroy()
    }

    override fun getBindingVariable(): Int = BR.dtgViewModel

    override fun getLayout(): Int = R.layout.activity_set_date_time_gps

    override fun getViewModel(): SetDateTimeGpsViewModel = ViewModelProviders.of(this)[SetDateTimeGpsViewModel::class.java]
    override fun onSubmitButtonClicked(gpsLocation: GpsLocation?) {
        tv_latitude.text = gpsLocation!!.getLatitude()
        tv_longitude.text = gpsLocation.getLongitude()
        dialogFragment.dismiss()
    }

    override fun onCancelButtonClicked() {
        dialogFragment.dismiss()
    }
}
