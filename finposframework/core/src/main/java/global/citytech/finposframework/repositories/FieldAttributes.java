package global.citytech.finposframework.repositories;

public enum FieldAttributes {

    /*
    MIDDLEWARE_CONFIGURATION
    */
    MIDDLEWARE_URL("Middleware URL", "MiddlewareConfigurations/URL"),

    /*
    TMS Host Configurations Fields
     */
    PRIMARY_TMS_HOST_IP("Primary Host IP", "TerminalRegistration/TMSHostConfiguration"),
    PRIMARY_TMS_HOST_PORT("Primary Host Port", "TerminalRegistration/TMSHostConfiguration"),
    SECONDARY_TMS_HOST_IP("Secondary Host IP", "TerminalRegistration/TMSHostConfiguration"),
    SECONDARY_TMS_HOST_PORT("Secondary Host Port", "TerminalRegistration/TMSHostConfiguration"),
    GPS_LOCATION("Gps Location", "TerminalRegistration/TMSHostConfiguration"),
    TMS_HOST_CONNECTION_DETAILS("Connection Details", "TerminalRegistration/TMSHostConfiguration"),
    TMS_HOST_NII("NII","TerminalRegistration/TMSHostConfiguration"),

    /*
    Financial Host Configurations Fields
     */
    PRIMARY_FINANCIAL_HOST_IP("Primary Host IP", "TerminalRegistration/FinancialHostConfiguration"),
    PRIMARY_FINANCIAL_HOST_PORT("Primary Host Port", "TerminalRegistration/FinancialHostConfiguration"),
    PRIMARY_FINANCIAL_CONNECTION_TYPE("Primary Connection Type", "TerminalRegistration/FinancialHostConfiguration"),
    SECONDARY_FINANCIAL_HOST_IP("Secondary Host IP", "TerminalRegistration/FinancialHostConfiguration"),
    SECONDARY_FINANCIAL_HOST_PORT("Secondary Host Port", "TerminalRegistration/FinancialHostConfiguration"),
    SECONDARY_FINANCIAL_CONNECTION_TYPE("Secondary Connection Type", "TerminalRegistration/FinancialHostConfiguration"),
    PRIMARY_FINANCIAL_HOST_CONNECTION_DETAILS("Primary Connection Details", "TerminalRegistration/FinancialHostConfiguration"),
    SECONDARY_FINANCIAL_HOST_CONNECTION_DETAILS("Secondary Connection Details", "TerminalRegistration/FinancialHostConfiguration"),
    GPRS_CONNECTION_DETAILS("GPRS Connection Details","TerminalRegistration/FinancialHostConfiguration"),
    WIFI_CONNECTION_DETAILS("WIFI Connection Details","TerminalRegistration/FinancialHostConfiguration"),
    FINANCIAL_HOST_NII("NII","TerminalRegistration/FinancialHostConfiguration"),


    /*
    Last Transaction Info Fields
     */
    LAST_TRANSMISSION_DATE_TIME("Last Transmission Date Time", "TerminalRegistration/LastTransaction"),
    STAN("System Trace Audit Number", "TerminalRegistration/LastTransaction"),
    CONNECTION_START_TIME("Connection Start Time", "TerminalRegistration/LastTransaction"),
    CONNECTION_END_TIME("Connection End Time", "TerminalRegistration/LastTransaction"),
    REQUEST_SENT_TIME("Request Sent Time", "TerminalRegistration/LastTransaction"),
    RESPONSE_RECEIVED_TIME("Response Received Time", "TerminalRegistration/LastTransaction"),
    PERFORMANCE_TIMERS_REFERENCE("Performance Timers Reference", "TerminalRegistration/LastTransaction"),
    RETRIEVAL_REFERENCE_NUMBER("Retrieval Reference Number", "TerminalRegistration/LastTransaction"),
    /*
    Security Info Fields
     */
    KEY_SET_ID("Key Set Id", "TerminalRegistration/SecurityInfo"),
    VENDOR_PRIVATE_KEY_ONE("Vendor Private Key One", "TerminalRegistration/SecurityInfo"),
    VENDOR_KEY_INDEX_ONE("Vendor Key Index One", "TerminalRegistration/SecurityInfo"),
    VENDOR_PRIVATE_KEY_TWO("Vendor Private Key Two", "TerminalRegistration/SecurityInfo"),
    VENDOR_KEY_INDEX_TWO("Vendor Key Index Two", "TerminalRegistration/SecurityInfo"),
    HOST_PUBLIC_KEY_ONE("Host Public Key One", "TerminalRegistration/SecurityInfo"),
    HOST_KEY_INDEX_ONE("Host Key Index One", "TerminalRegistration/SecurityInfo"),
    HOST_PUBLIC_KEY_TWO("Host Public Key Two", "TerminalRegistration/SecurityInfo"),
    HOST_KEY_INDEX_TWO("Host Key Index Two", "TerminalRegistration/SecurityInfo"),

    /*
    Terminal Info Fields
     */
    VENDOR_ID("Vendor ID", "TerminalRegistration/TerminalInfo"),
    VENDOR_TERMINAL_TYPE("Vendor Terminal Type", "TerminalRegistration/TerminalInfo"),
    VENDOR_TERMINAL_SERIAL_NUMBER("Vendor Terminal Serial Number", "TerminalRegistration/TerminalInfo"),
    ACQUIRER_ID("Acquirer ID", "TerminalRegistration/TerminalInfo"),
    CARD_ACCEPTOR_TERMINAL_ID("Card Acceptor Terminal ID", "TerminalRegistration/TerminalInfo"),
    CARD_ACCEPTOR_ID_CODE("Card Acceptor ID Code", "TerminalRegistration/TerminalInfo"),
    TERMINAL_DIAL_INDICATOR("Terminal Dial Indicator", "TerminalRegistration/TerminalInfo"),
    DATA_RECORD("Data Record", "TerminalRegistration/TerminalInfo"),
    RETAILER_LOGO("Retailer Logo", "TerminalRegistration/RetailerLogo"),
    TLS_CERTIFICATE("TLS Certificate", "TerminalRegistration/TLSCertificate"),

    /*
    Retailer Data Segment 1 fields
     */
    RETAILER_DATA_S1_F2_DATA_TYPE("2", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F3_SEGMENT_ID("3", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F4_NEXT_LOAD("4", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F6_RECONCILIATION_TIME("6", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F8_RETAILER_NAME_ARABIC("8", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F10_RETAILER_NUM_ENGLISH("10", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F12_RETAILER_NAME_ENGLISH("12", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F14_RETAILER_LANGUAGE_INDICATOR("14", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F16_TERMINAL_CURRENCY_CODE("16", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F18_TERMINAL_COUNTRY_CODE("18", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F20_TRANSACTION_CURRENCY_EXPONENT("20", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F22_CURRENCY_SYMBOL_ARBIC("22", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F24_CURRENCY_SYMBOL_ENG("24", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F26_ARABIC_RECEIPT_1("26", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F28_ARABIC_RECEIPT_2("28", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F30_ENGLISH_RECEIPT_1("30", "RetailerData/retailer_segment_1_data"),
    RETAILER_DATA_S1_F32_ENGLISH_RECEIPT_2("32", "RetailerData/retailer_segment_1_data"),

    /*
    Retailer Data Segment 2 fields
     */
    RETAILER_DATA_S2_F2_DATA_TYPE("2", "RetailerData/retailer_segment_2_data"),
    RETAILER_DATA_S2_F3_SEGMENT_ID("3", "RetailerData/retailer_segment_2_data"),
    RETAILER_DATA_S2_F4_RETAILER_ADDRESS_1_ARABIC("4", "RetailerData/retailer_segment_2_data"),
    RETAILER_DATA_S2_F6_RETAILER_ADDRESS_1_ENGLISH("6", "RetailerData/retailer_segment_2_data"),

    /*
    Retailer Data Segment 3 fields
     */
    RETAILER_DATA_S3_F2_DATA_TYPE("2", "RetailerData/retailer_segment_3_data"),
    RETAILER_DATA_S3_F3_SEGMENT_ID("3", "RetailerData/retailer_segment_3_data"),
    RETAILER_DATA_S3_F4_RETAILER_ADDRESS_2_ARABIC("4", "RetailerData/retailer_segment_3_data"),
    RETAILER_DATA_S3_F6_RETAILER_ADDRESS_2_ENGLISH("6", "RetailerData/retailer_segment_3_data"),

    /*
    Retailer Data Segment 4 fields
     */
    RETAILER_DATA_S4_F2_DATA_TYPE("2", "RetailerData/retailer_segment_4_data"),
    RETAILER_DATA_S4_F3_SEGMENT_ID("3", "RetailerData/retailer_segment_4_data"),
    RETAILER_DATA_S4_F4_TERMINAL_CAPABILITY("4", "RetailerData/retailer_segment_4_data"),
    RETAILER_DATA_S4_F6_ADDITIONAL_TERMINAL_CAPABILITIES("6", "RetailerData/retailer_segment_4_data"),
    RETAILER_DATA_S4_F8_DOWNLOAD_PHONE_NUMBER("8", "RetailerData/retailer_segment_4_data"),
    RETAILER_DATA_S4_F10_EMV_TERMINAL_TYPE("10", "RetailerData/retailer_segment_4_data"),
    RETAILER_DATA_S4_F12_AUTOMATIC_LOAD("12", "RetailerData/retailer_segment_4_data"),
    RETAILER_DATA_S4_F14_SAF_RETRY_LIMIT("14", "RetailerData/retailer_segment_4_data"),
    RETAILER_DATA_S4_F16_SAF_DEFAULT_MESSAGE_TRANSMISSION_NUM("16", "RetailerData/retailer_segment_4_data"),

    /*
    Revoked Certificates Segment 1 fields
     */
    REVOKED_CERTIFICATES_S1_F2_DATA_TYPE("2", "RevokedCertificates/revoked_certificates_segment_1_data"),
    REVOKED_CERTIFICATES_S1_F3_SEGMENT_ID("3", "RevokedCertificates/revoked_certificates_segment_1_data"),
    REVOKED_CERTIFICATES_S1_F4_RID("4", "RevokedCertificates/revoked_certificates_segment_1_data"),
    REVOKED_CERTIFICATES_S1_F6_IDX("6", "RevokedCertificates/revoked_certificates_segment_1_data"),
    REVOKED_CERTIFICATES_S1_F8_CERT_SERIAL_NUM("8", "RevokedCertificates/revoked_certificates_segment_1_data"),

    /*
    Message Texts Segments 1 fields
     */
    MESSAGE_TEXT_S1_F2_DATA_TYPE("2", "MessageText/message_text_segment_1_data"),
    MESSAGE_TEXT_S1_F3_SEGMENT_ID("3", "MessageText/message_text_segment_1_data"),
    MESSAGE_TEXT_S1_F4_MESSAGE_CODE("4", "MessageText/message_text_segment_1_data"),
    MESSAGE_TEXT_S1_F6_DISPLAY_CODE("6", "MessageText/message_text_segment_1_data"),
    MESSAGE_TEXT_S1_F8_ARABIC_MSG_TXT("8", "MessageText/message_text_segment_1_data"),
    MESSAGE_TEXT_S1_F10_ENGLISH_MSG_TXT("10", "MessageText/message_text_segment_1_data"),

    /*
    EMV Public key Segment 1 Fields
     */
    EMV_PUBLIC_KEY_S1_F2_DATA_TYPE("2", "EMVPublicKey/emvPK_segment_1_data"),
    EMV_PUBLIC_KEY_S1_F3_SEGMENT_ID("3", "EMVPublicKey/emvPK_segment_1_data"),
    EMV_PUBLIC_KEY_S1_F4_RID("4", "EMVPublicKey/emvPK_segment_1_data"),
    EMV_PUBLIC_KEY_S1_F6_KEY_INDEX("6", "EMVPublicKey/emvPK_segment_1_data"),
    EMV_PUBLIC_KEY_S1_F8_HASH_ID("8", "EMVPublicKey/emvPK_segment_1_data"),
    EMV_PUBLIC_KEY_S1_F10_DIGITAL_SIG_ID("10", "EMVPublicKey/emvPK_segment_1_data"),
    EMV_PUBLIC_KEY_S1_F12_PUBLIC_KEY("12", "EMVPublicKey/emvPK_segment_1_data"),
    EMV_PUBLIC_KEY_S1_F14_EXPONENT("14", "EMVPublicKey/emvPK_segment_1_data"),
    EMV_PUBLIC_KEY_S1_F16_CHECK_SUM("16", "EMVPublicKey/emvPK_segment_1_data"),
    EMV_PUBLIC_KEY_S1_F18_CA_PUBLIC_KEY_LENGTH("18", "EMVPublicKey/emvPK_segment_1_data"),
    EMV_PUBLIC_KEY_S1_F20_CA_PUBLIC_KEY_EXPIRY_DATE("20", "EMVPublicKey/emvPK_segment_1_data"),

    /*
    Device Specific Segment 1 fields
     */
    DEVICE_SPECIFIC_S1_F2_DATA_TYPE("2", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F3_SEGMENT_ID("3", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_DATA("4", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_1_CARD_SCHEME("4.1", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_2_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT("4.2", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_3_TERMINAL_CVM_REQUIRED_LIMIT("4.3", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_4_TERMINAL_CONTACTLESS_FLOOR_LIMIT("4.4", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_5_CARD_SCHEME("4.5", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_6_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT("4.6", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_7_TERMINAL_CVM_REQUIRED_LIMIT("4.7", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_8_TERMINAL_CONTACTLESS_FLOOR_LIMIT("4.8", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_9_CARD_SCHEME("4.9", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_10_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT("4.10", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_11_TERMINAL_CVM_REQUIRED_LIMIT("4.11", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_12_TERMINAL_CONTACTLESS_FLOOR_LIMIT("4.12", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_13_MAX_SAF_DEPTH("4.13", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_14_MAX_SAF_CUMULATIVE_AMOUNT("4.14", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_15_IDLE_TIME("4.15", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_16_MAX_RECONCILLATION_AMOUNT("4.16", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_17_MAX_TRANSACTION_PROCESSED("4.17", "DeviceSpecific/device_specific_segment_1_data"),
    DEVICE_SPECIFIC_S1_F4_18_QR_CODE_PRINT_INDICATOR("4.18", "DeviceSpecific/device_specific_segment_1_data"),

    /*
    AID Data Segment 1 fields
     */
    AID_DATA_S1_F2_DATA_TYPE("2", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F3_SEGMENT_ID("3", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F4_AID("4", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F6_AID_LABEL("6", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F8_TERMINAL_AID_VERSION_NUMBERS("8", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F10_EXACT_ONLY_SELECTION("10", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F12_SKIP_EMV_PROCESSING("12", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F14_DEFAULT_TDOL("14", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F16_DEFAULT_DDOL("16", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F18_EMV_ADDTIONAL_TAGS("18", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F20_DENIAL_ACTION_CODE("20", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F22_ONLINE_ACTION_CODE("22", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F24_DEFAULT_ACTION_CODE("24", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F26_THRESHOLD_VALUE_FOR_BIASED_RANDOM_SELECTION("26", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F28_TARGET_PERCENTAGE("28", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F30_MAXM_TARGET_PERCENTAGE_FOR_BIASED_RANDOM_SELECTION("30", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F32_CD_CVM_LIMIT("32", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F34_NO_CD_CVM_LIMIT("34", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F36_CVM_CAP_CVM_REQUIRED("36", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F38_CVM_CAP_NO_CVM_REQUIRED("38", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F40_TERMINAL_RISK_MANAGEMENT_DATA("40", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F42_CL_TAC_DENIAL("42", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F44_CL_TAC_ONLINE("44", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F46_CL_TAC_DEFAULT("46", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F48_ACQUIRER_ID("48", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F50_KERNEL_CONFIG("50", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F52_CL_CARD_DATA_INPUT_CAP("52", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F54_CL_SECURITY_CAP("54", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F56_UDOL("56", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F58_MSD_APP_VERSION("58", "AIDData/aid_data_segment_1_data"),
    AID_DATA_S1_F60_TTQ("60", "AIDData/aid_data_segment_1_data"),

    /*
    AID List Segment 1 fields
     */
    AID_LIST_S1_F2_DATA_TYPE("2", "AIDList/aid_list_segment_1_data"),
    AID_LIST_S1_F3_SEGMENT_ID("3", "AIDList/aid_list_segment_1_data"),
    AID_LIST_S1_F4_AID_1("4", "AIDList/aid_list_segment_1_data"),
    AID_LIST_S1_F6_AID_2("6", "AIDList/aid_list_segment_1_data"),
    AID_LIST_S1_F8_AID_3("8", "AIDList/aid_list_segment_1_data"),
    AID_LIST_S1_F10_AID_4("10", "AIDList/aid_list_segment_1_data"),
    AID_LIST_S1_F12_AID_5("12", "AIDList/aid_list_segment_1_data"),
    AID_LIST_S1_F14_AID_6("14", "AIDList/aid_list_segment_1_data"),
    AID_LIST_S1_F16_AID_7("16", "AIDList/aid_list_segment_1_data"),
    AID_LIST_S1_F18_AID_8("18", "AIDList/aid_list_segment_1_data"),
    AID_LIST_S1_F20_AID_9("20", "AIDList/aid_list_segment_1_data"),
    AID_LIST_S1_F22_AID_10("22", "AIDList/aid_list_segment_1_data"),

    /*
    Card Scheme Segment 1 fields
     */
    CARD_SCHEME_S1_F2_DATA_TYPE("2", "CardScheme/card_scheme_segment_1_data"),
    CARD_SCHEME_S1_F3_SEGMENT_ID("3", "CardScheme/card_scheme_segment_1_data"),
    CARD_SCHEME_S1_F4_CARD_SCHEME_ID("4", "CardScheme/card_scheme_segment_1_data"),
    CARD_SCHEME_S1_F6_CARD_SCHEME_NAME_ARABIC("6", "CardScheme/card_scheme_segment_1_data"),
    CARD_SCHEME_S1_F8_CARD_SCHEME_NAME_ENGLISH("8", "CardScheme/card_scheme_segment_1_data"),
    CARD_SCHEME_S1_F10_CARD_SCHEME_ACQUIRER_ID("10", "CardScheme/card_scheme_segment_1_data"),
    CARD_SCHEME_S1_F12_MERCHANT_CATEGORY_CODE("12", "CardScheme/card_scheme_segment_1_data"),
    CARD_SCHEME_S1_F14_MERCHANT_ID("14", "CardScheme/card_scheme_segment_1_data"),
    CARD_SCHEME_S1_F16_TERMINAL_ID("16", "CardScheme/card_scheme_segment_1_data"),
    CARD_SCHEME_S1_F18_ENABLE_EMV("18", "CardScheme/card_scheme_segment_1_data"),
    CARD_SCHEME_S1_F20_CHECK_SERVICE_CODE("20", "CardScheme/card_scheme_segment_1_data"),
    CARD_SCHEME_S1_F22_OFFLINE_REFUND_INDICATOR("22", "CardScheme/card_scheme_segment_1_data"),

    /*
    Card Scheme Segment 2 fields
     */
    CARD_SCHEME_S2_F2_DATA_TYPE("2", "CardScheme/card_scheme_segment_2_data"),
    CARD_SCHEME_S2_F3_SEGMENT_ID("3", "CardScheme/card_scheme_segment_2_data"),
    CARD_SCHEME_S2_F4_CARD_SCHEME_ID("4", "CardScheme/card_scheme_segment_2_data"),
    CARD_SCHEME_S2_F6_TRANSACTIONS_ALLOWED("6", "CardScheme/card_scheme_segment_2_data"),
    CARD_SCHEME_S2_F8_CARDHOLDER_AUTHENTICATION("8", "CardScheme/card_scheme_segment_2_data"),
    CARD_SCHEME_S2_F10_SUPERVISOR_FUNCTIONS("10", "CardScheme/card_scheme_segment_2_data"),
    CARD_SCHEME_S2_F12_MANUAL_ENTRY_ALLOWED("12", "CardScheme/card_scheme_segment_2_data"),
    CARD_SCHEME_S2_F14_FLOOR_LIMIT_INDICATOR("14", "CardScheme/card_scheme_segment_2_data"),
    CARD_SCHEME_S2_F16_TERMINAL_FLOOR_LIMIT("16", "CardScheme/card_scheme_segment_2_data"),
    CARD_SCHEME_S2_F18_TERMINAL_FLOOR_LIMIT_FALLBACK("18", "CardScheme/card_scheme_segment_2_data"),
    CARD_SCHEME_S2_F20_MAXM_CASH_BACK("20", "CardScheme/card_scheme_segment_2_data"),
    CARD_SCHEME_S2_F22_MAXM_TRANSACTION_AMOUNT_INDICATOR("22", "CardScheme/card_scheme_segment_2_data"),
    CARD_SCHEME_S2_F24_MAXM_AMOUNT_ALLOWED("24", "CardScheme/card_scheme_segment_2_data"),
    CARD_SCHEME_S2_F26_LUHN_CHECK("26", "CardScheme/card_scheme_segment_2_data"),
    CARD_SCHEME_S2_F28_EXPIRY_DATE_POSITION("28", "CardScheme/card_scheme_segment_2_data"),
    CARD_SCHEME_S2_F30_DELAY_CALL_SET_UP("30", "CardScheme/card_scheme_segment_2_data"),

    /*
    Card Scheme Segment 3 fields
     */
    CARD_SCHEME_S3_F2_DATA_TYPE("2", "CardScheme/card_scheme_segment_3_data"),
    CARD_SCHEME_S3_F3_SEGMENT_ID("3", "CardScheme/card_scheme_segment_3_data"),
    CARD_SCHEME_S3_F4_CARD_SCHEME_ID("4", "CardScheme/card_scheme_segment_3_data"),
    CARD_SCHEME_S3_F6_CARD_RANGES("6", "CardScheme/card_scheme_segment_3_data"),
    CARD_SCHEME_S3_F8_CARD_PREFIX_SEQUENCE_INDICATOR("8", "CardScheme/card_scheme_segment_3_data"),
    CARD_SCHEME_S3_F10_IIN("10", "CardScheme/card_scheme_segment_3_data"),

    /*
    Dialup Connection Segment 1 fields
     */
    DIALUP_S1_F2_DATA_TYPE("2", "ConnectionParameters/dialupconnection_segment_1_data"),
    DIALUP_S1_F3_SEGMENT_ID("3", "ConnectionParameters/dialupconnection_segment_1_data"),
    DIALUP_S1_F4_PRIORITY("4", "ConnectionParameters/dialupconnection_segment_1_data"),
    DIALUP_S1_F6_COMMUNICATION_TYPE("6", "ConnectionParameters/dialupconnection_segment_1_data"),
    DIALUP_S1_F8_PHONE_NUMBER("8", "ConnectionParameters/dialupconnection_segment_1_data"),
    DIALUP_S1_F10_DIAL_ATTEMPTS_TO_PHONE("10", "ConnectionParameters/dialupconnection_segment_1_data"),
    DIALUP_S1_F12_CONNECT_TIME_FOR_PHONE("12", "ConnectionParameters/dialupconnection_segment_1_data"),
    DIALUP_S1_F14_BAUD_RATE("14", "ConnectionParameters/dialupconnection_segment_1_data"),
    DIALUP_S1_F16_PARITY_BIT("16", "ConnectionParameters/dialupconnection_segment_1_data"),
    DIALUP_S1_F18_DATA_BIT("18", "ConnectionParameters/dialupconnection_segment_1_data"),
    DIALUP_S1_F20_STOP_BIT("20", "ConnectionParameters/dialupconnection_segment_1_data"),
    DIALUP_S1_F22_RESPONSE_TIMEOUT("22", "ConnectionParameters/dialupconnection_segment_1_data"),

    /*
    GPRS Connection Segment 1 fields
     */
    GPRS_S1_F2_DATA_TYPE("2", "ConnectionParameters/gprs_segment_1_data"),
    GPRS_S1_F3_SEGMENT_ID("3", "ConnectionParameters/gprs_segment_1_data"),
    GPRS_S1_F4_PRIORITY("4", "ConnectionParameters/gprs_segment_1_data"),
    GPRS_S1_F6_COMMUNICATION_TYPE("6", "ConnectionParameters/gprs_segment_1_data"),
    GPRS_S1_F8_GPRS_DIAL_NUM("8", "ConnectionParameters/gprs_segment_1_data"),
    GPRS_S1_F10_GPRS_ACCESS_POINT_NAME("10", "ConnectionParameters/gprs_segment_1_data"),
    GPRS_S1_F12_CONNECT_TIME_FOR_GPRS_PHONE("12", "ConnectionParameters/gprs_segment_1_data"),
    GPRS_S1_F14_NETWORK_IP_ADDRESS("14", "ConnectionParameters/gprs_segment_1_data"),
    GPRS_S1_F16_NETWORK_TCP_PORT("16", "ConnectionParameters/gprs_segment_1_data"),
    GPRS_S1_F18_DIAL_ATTEMPTS_TO_NETWORK("18", "ConnectionParameters/gprs_segment_1_data"),
    GPRS_S1_F20_RESPONSE_TIME_OUT("20", "ConnectionParameters/gprs_segment_1_data"),
    GPRS_S1_F22_SSL_CERTIFICATE_FILE("22", "ConnectionParameters/gprs_segment_1_data"),

    /*
    GSM Connection Segment 1 file
     */
    GSM_S1_F2_DATA_TYPE("2", "ConnectionParameters/gsm_segment_1_data"),
    GSM_S1_F3_SEGMENT_ID("3", "ConnectionParameters/gsm_segment_1_data"),
    GSM_S1_F4_PRIORITY("4", "ConnectionParameters/gsm_segment_1_data"),
    GSM_S1_F6_COMMUNICATION_TYPE("6", "ConnectionParameters/gsm_segment_1_data"),
    GSM_S1_F8_PHONE_NUMBER("8", "ConnectionParameters/gsm_segment_1_data"),
    GSM_S1_F10_DIAL_ATTEMPTS_TO_PHONE("10", "ConnectionParameters/gsm_segment_1_data"),
    GSM_S1_F12_CONNECT_TIME_FOR_PHONE("12", "ConnectionParameters/gsm_segment_1_data"),
    GSM_S1_F14_BAUD_RATE("14", "ConnectionParameters/gsm_segment_1_data"),
    GSM_S1_F16_PARITY_BIT("16", "ConnectionParameters/gsm_segment_1_data"),
    GSM_S1_F18_DATA_BIT("18", "ConnectionParameters/gsm_segment_1_data"),
    GSM_S1_F20_STOP_BIT("20", "ConnectionParameters/gsm_segment_1_data"),
    GSM_S1_F22_RESPONSE_TIME_OUT("22", "ConnectionParameters/gsm_segment_1_data"),

    /*
    TCP IP Connection Segment 1 file
     */
    TCP_IP_S1_F2_DATA_TYPE("2", "ConnectionParameters/tcpip_segment_1_data"),
    TCP_IP_S1_F3_SEGMENT_ID("3", "ConnectionParameters/tcpip_segment_1_data"),
    TCP_IP_S1_F4_PRIORITY("4", "ConnectionParameters/tcpip_segment_1_data"),
    TCP_IP_S1_F6_COMMUNICATION_TYPE("6", "ConnectionParameters/tcpip_segment_1_data"),
    TCP_IP_S1_F8_NETWORK_IP_ADDRESS("8", "ConnectionParameters/tcpip_segment_1_data"),
    TCP_IP_S1_F10_NETWORK_TCP_SUPPORT("10", "ConnectionParameters/tcpip_segment_1_data"),
    TCP_IP_S1_F12_COUNT_ACCESS_RETRIES("12", "ConnectionParameters/tcpip_segment_1_data"),
    TCP_IP_S1_F14_RESPONSE_TIME_OUT("14", "ConnectionParameters/tcpip_segment_1_data"),
    TCP_IP_S1_F16_SSL_CERTIFICATE_FILE("16", "ConnectionParameters/tcpip_segment_1_data"),

    /*
    WIFI Connection Segment 1 file
     */
    WIFI_S1_F2_DATA_TYPE("2", "ConnectionParameters/wifi_segment_1_data"),
    WIFI_S1_F3_SEGMENT_ID("3", "ConnectionParameters/wifi_segment_1_data"),
    WIFI_S1_F4_PRIORITY("4", "ConnectionParameters/wifi_segment_1_data"),
    WIFI_S1_F6_COMMUNICATION_TYPE("6", "ConnectionParameters/wifi_segment_1_data"),
    WIFI_S1_F8_NETWORK_IP_ADDRESS("8", "ConnectionParameters/wifi_segment_1_data"),
    WIFI_S1_F10_NETWORK_TCP_PORT("10", "ConnectionParameters/wifi_segment_1_data"),
    WIFI_S1_F12_COUNT_ACCESS_RETRIES("12", "ConnectionParameters/wifi_segment_1_data"),
    WIFI_S1_F14_RESPONSE_TIME_OUT("14", "ConnectionParameters/wifi_segment_1_data"),
    WIFI_S1_F16_SSO_CERTIFICATE_FILE("16", "ConnectionParameters/wifi_segment_1_data");

    String fieldIdentifier;
    String filePath;

    FieldAttributes(String fieldIdentifier1, String filePath) {
        this.fieldIdentifier = fieldIdentifier1;
        this.filePath = filePath;
    }

    public String getFieldIdentifier() {
        return fieldIdentifier;
    }

    public String getFilePath() {
        return filePath;
    }
}
