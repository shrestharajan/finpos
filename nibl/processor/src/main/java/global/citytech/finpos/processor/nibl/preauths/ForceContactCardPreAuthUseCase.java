//package global.citytech.finpos.processor.nibl.preauths;
//
//import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
//import global.citytech.finpos.nibl.iso8583.ProcessingCode;
//import global.citytech.finpos.nibl.iso8583.requestsender.preauth.IccPreAuthRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.preauth.MagPreAuthRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.preauth.PICCPreAuthRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.preauth.PreAuthRequest;
//import global.citytech.finpos.nibl.iso8583.requestsender.preauth.PreAuthRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
//import global.citytech.finpos.processor.nibl.reversal.ReversalRequestModel;
//import global.citytech.finpos.processor.nibl.reversal.ReversalUseCase;
//import global.citytech.finpos.processor.nibl.transaction.NiblTransactionReceiptPrintHandler;
//import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
//import global.citytech.finpos.processor.nibl.transaction.ReversedTransactionReceiptHandler;
//import global.citytech.finpos.processor.nibl.transaction.TransactionUseCase;
//import global.citytech.finpos.processor.nibl.transaction.TransactionUtils;
//import global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor;
//import global.citytech.finpos.processor.nibl.transaction.card.MagneticCardProcessor;
//import global.citytech.finpos.processor.nibl.transaction.card.PICCCardProcessor;
//import global.citytech.finposframework.exceptions.FinPosException;
//import global.citytech.finposframework.exceptions.PosError;
//import global.citytech.finposframework.exceptions.PosException;
//import global.citytech.finposframework.hardware.io.cards.CardDetails;
//import global.citytech.finposframework.hardware.io.cards.CardType;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
//import global.citytech.finposframework.hardware.io.led.LedAction;
//import global.citytech.finposframework.hardware.io.led.LedLight;
//import global.citytech.finposframework.hardware.io.led.LedRequest;
//import global.citytech.finposframework.hardware.io.led.LedService;
//import global.citytech.finposframework.iso8583.DataElement;
//import global.citytech.finposframework.iso8583.IsoMessageResponse;
//import global.citytech.finposframework.iso8583.RequestContext;
//import global.citytech.finposframework.notifier.Notifier;
//import global.citytech.finposframework.repositories.ApplicationRepository;
//import global.citytech.finposframework.repositories.TerminalRepository;
//import global.citytech.finposframework.repositories.TransactionRepository;
//import global.citytech.finposframework.switches.PosEntryMode;
//import global.citytech.finposframework.usecases.MagneticSwipe;
//import global.citytech.finposframework.usecases.PICCWave;
//import global.citytech.finposframework.usecases.TransactionType;
//import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
//import global.citytech.finposframework.usecases.transaction.TransactionLog;
//import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
//import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
//import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
//import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
//import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
//import global.citytech.finposframework.utility.HelperUtils;
//import global.citytech.finposframework.utility.IsoMessageUtils;
//import global.citytech.finposframework.utility.JsonUtils;
//import global.citytech.finposframework.utility.PosResponse;
//import global.citytech.finposframework.utility.StringUtils;
//
///**
// * Created by Unique Shakya on 1/19/2021.
// */
//public class ForceContactCardPreAuthUseCase extends TransactionUseCase {
//
//    private PreAuthRequest preAuthRequest;
//
//    public ForceContactCardPreAuthUseCase(TerminalRepository terminalRepository, Notifier notifier) {
//        super(terminalRepository, notifier);
//    }
//
//    @Override
//    protected String getClassName() {
//        return ForceContactCardPreAuthUseCase.class.getSimpleName();
//    }
//
//    public PreAuthResponseModel execute(PreAuthRequestModel requestModel) {
//        logger.log("::: FORCED CONTACT PRE AUTH USE CASE :::");
//        if (!this.isDeviceReady(requestModel.getDeviceController(),
//                requestModel.getTransactionRequest().getTransactionType())) {
//            throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
//        }
//        return initiateTransaction(requestModel);
//    }
//
//    private PosResponse getDeviceStatus(PreAuthRequestModel preAuthRequestModel) {
//        return preAuthRequestModel
//                .getDeviceController()
//                .isReady(
//                        TransactionUtils.getAllowedCardEntryModes(
//                                preAuthRequestModel
//                                        .getTransactionRequest()
//                                        .getTransactionType()
//                        )
//                );
//    }
//
//    private PreAuthResponseModel initiateTransaction(PreAuthRequestModel requestModel) {
//        try {
//            String stan = this.terminalRepository.getSystemTraceAuditNumber();
//            String invoiceNumber = this.terminalRepository.getInvoiceNumber();
//            String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
//            TransactionRequest transactionRequest = requestModel.getTransactionRequest();
//            this.transactionRepository = requestModel.getTransactionRepository();
//            this.validateRequest(transactionRequest, requestModel.getApplicationRepository());
//            ReadCardRequest readCardRequest = prepareForceContactCardReadCardRequest(stan, requestModel.getApplicationRepository(),
//                    transactionRequest);
//            this.notifier.notify(Notifier.EventType.FORCE_ICC_CARD, "EXCEED COUNT\nINSERT CARD");
//            ReadCardResponse readCardResponse = this.detectCard(readCardRequest, requestModel.getReadCardService(),
//                    requestModel.getLedService());
//            this.notifier.notify(Notifier.EventType.READING_CARD, "Reading Card..");
//            CardType detectedCardType = readCardResponse.getCardDetails().getCardType();
//            this.logger.debug("::: PRE AUTH USE CASE ::: CARD TYPE === " + detectedCardType);
//            this.logger.debug("::: CARD SCHEME AFTER VALIDATE READ CARD RESPONSE ::: " + readCardResponse.getCardDetails().getCardScheme());
//            switch (detectedCardType) {
//                case MAG:
//                    this.cardProcessor = new MagneticCardProcessor(
//                            requestModel.getTransactionRepository(),
//                            requestModel.getTransactionAuthenticator(),
//                            requestModel.getApplicationRepository(),
//                            requestModel.getReadCardService(),
//                            this.notifier,
//                            this.terminalRepository
//                    );
//                    break;
//                case ICC:
//                    this.cardProcessor = new IccCardProcessor(
//                            requestModel.getReadCardService(),
//                            requestModel.getTransactionRepository(),
//                            requestModel.getApplicationRepository(),
//                            requestModel.getTransactionAuthenticator(),
//                            this.notifier,
//                            this.terminalRepository
//                    );
//                    break;
//                case PICC:
//                    this.cardProcessor = new PICCCardProcessor(
//                            requestModel.getTransactionRepository(),
//                            requestModel.getApplicationRepository(),
//                            requestModel.getReadCardService(),
//                            this.notifier,
//                            requestModel.getTransactionAuthenticator(),
//                            requestModel.getLedService(),
//                            requestModel.getSoundService(),
//                            this.terminalRepository
//                    );
//                    break;
//                default:
//                    break;
//            }
//            readCardResponse = this.cardProcessor.validateReadCardResponse(readCardRequest, readCardResponse);
//            this.checkForCardTypeChange(readCardResponse.getCardDetails().getCardType(), requestModel);
//            if (!isValidTransaction(
//                    requestModel.getApplicationRepository(),
//                    transactionRequest,
//                    readCardResponse
//            )) {
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
//            }
//            if (!isTransactionAuthorizationSuccess(
//                    requestModel.getTransactionAuthenticator(),
//                    requestModel.getApplicationRepository(),
//                    transactionRequest.getTransactionType(),
//                    readCardResponse
//            )) {
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
//            }
//            global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest
//                    = this.preparePurchaseCardRequest(
//                    transactionRequest,
//                    readCardResponse.getCardDetails(),
//                    readCardResponse.getFallbackIccToMag()
//            );
//            readCardResponse = this.cardProcessor.processCard(stan, purchaseRequest);
//            logger.debug("::: READ CARD RESPONSE ::: " + readCardResponse.toString());
//            this.notifier.notify(
//                    Notifier.EventType.TRANSMITTING_REQUEST,
//                    "Transmitting Request.."
//            );
//            try {
//                IsoMessageResponse isoMessageResponse = this.sendIsoMessage(
//                        stan,
//                        invoiceNumber,
//                        readCardResponse,
//                        requestModel
//                );
//                this.notifier.notify(
//                        Notifier.EventType.RESPONSE_RECEIVED,
//                        "Please wait.."
//                );
//                InvalidResponseHandlerResponse invalidResponseHandlerResponse =
//                        this.checkForInvalidResponseFromHost(isoMessageResponse);
//                if (invalidResponseHandlerResponse.isInvalid())
//                    return this.handleInvalidResponseFromHost(transactionRequest,
//                            invalidResponseHandlerResponse, batchNumber, stan, invoiceNumber,
//                            readCardResponse, requestModel);
//                boolean isPreAuthApproved = this.isTransactionApprovedByActionCode(isoMessageResponse);
//                this.notifier.notify(Notifier.EventType.PROCESSING_ISSUER_SCRIPT, "Processing..");
//                this.setOnlineResultToCard(
//                        readCardResponse,
//                        requestModel.getReadCardService(),
//                        isoMessageResponse
//                );
//                if (this.isTransactionDeclineByCardEvenIfSuccessOnSwitch(isoMessageResponse, readCardResponse))
//                    return this.handleApprovedTransactionDeclinedByCard(stan, batchNumber, invoiceNumber,
//                            readCardResponse, requestModel, transactionRequest);
//                this.notifyTransactionCompletion(
//                        stan,
//                        batchNumber,
//                        invoiceNumber,
//                        isPreAuthApproved,
//                        transactionRequest,
//                        requestModel.getTransactionRepository(),
//                        isoMessageResponse,
//                        readCardResponse,
//                        requestModel.getApplicationRepository(),
//                        requestModel.getLedService()
//                );
//                boolean shouldPrint = this.printReceipt(
//                        batchNumber,
//                        stan,
//                        invoiceNumber,
//                        requestModel.getPrinterService(),
//                        transactionRequest,
//                        requestModel.getTransactionRepository(),
//                        readCardResponse,
//                        isoMessageResponse,
//                        requestModel.getApplicationRepository()
//                );
//                return this.preparePreAuthResponse(isoMessageResponse, isPreAuthApproved,
//                        requestModel.getApplicationRepository(), shouldPrint);
//            } catch (FinPosException e) {
//                e.printStackTrace();
//                if (this.isResponseNotReceivedException(e)) {
//                    return this.handleTimeoutTransaction(transactionRequest, batchNumber, stan,
//                            invoiceNumber, readCardResponse, requestModel);
//                } else if (e.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT) {
//                    logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED UNABLE TO CONNECT");
//                    transactionRepository.removeAutoReversal(this.autoReversal);
//                    throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
//                } else {
//                    throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//                }
//            }
//
//        } catch (PosException ex) {
//            ex.printStackTrace();
//            throw ex;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        } finally {
//            requestModel.getReadCardService().cleanUp();
//            requestModel.getLedService().doTurnLedWith(new LedRequest(LedLight.ALL, LedAction.OFF));
//            PICCWave.getInstance().clear();
//            MagneticSwipe.getInstance().clear();
//        }
//    }
//
//    private PreAuthResponseModel handleTimeoutTransaction(TransactionRequest transactionRequest,
//                                                          String batchNumber, String stan,
//                                                          String invoiceNumber, ReadCardResponse readCardResponse,
//                                                          PreAuthRequestModel requestModel) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, "TIMEOUT");
//        this.setUnableToGoOnlineResultToCard(readCardResponse, requestModel.getReadCardService());
//        this.performReversal(stan, readCardResponse, requestModel);
//        boolean shouldPrint = !this.approvePrintOnly(requestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest,
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.TIMEOUT, "Timeout"));
//            new NiblTransactionReceiptPrintHandler(requestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            requestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PreAuthResponseModel.Builder.createDefaultBuilder()
//                .message("TIMEOUT")
//                .approved(false)
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private PreAuthResponseModel preparePreAuthResponse(
//            IsoMessageResponse response,
//            boolean isPurchaseApproved,
//            ApplicationRepository applicationRepository,
//            boolean shouldPrint
//    ) {
//        PreAuthResponseModel.Builder builder = PreAuthResponseModel.Builder.createDefaultBuilder();
//        builder.approved(isPurchaseApproved);
//        builder.debugRequestMessage(response.getDebugRequestString());
//        builder.debugResponseMessage(response.getDebugResponseString());
//        builder.message(retrieveMessage(response, isPurchaseApproved, applicationRepository));
//        builder.shouldPrintCustomerCopy(shouldPrint);
//        return builder.build();
//    }
//
//    private void notifyTransactionCompletion(
//            String stan, String batchNumber,
//            String invoiceNumber,
//            boolean authCompletionApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository,
//            LedService ledService
//    ) {
//        this.updateTransactionLog(
//                stan,
//                batchNumber,
//                invoiceNumber,
//                authCompletionApproved,
//                transactionRequest,
//                transactionRepository,
//                isoMessageResponse,
//                readCardResponse,
//                applicationRepository
//        );
//        this.updateTransactionIds(authCompletionApproved);
//        this.notifyTransactionStatus(
//                authCompletionApproved,
//                isoMessageResponse,
//                applicationRepository,
//                ledService,
//                readCardResponse.getCardDetails().getCardType() == CardType.PICC
//        );
//    }
//
//    private void updateTransactionLog(
//            String stan, String batchNumber,
//            String invoiceNumber,
//            boolean authCompletionApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository
//    ) {
//        this.logger.log("::: NIBL ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
//        this.logger.log("::: NIBL ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
//        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
//                .withInvoiceNumber(invoiceNumber)
//                .withStan(stan)
//                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
//                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
//                .withTransactionType(TransactionType.PRE_AUTH)
//                .withOriginalTransactionType(TransactionType.PRE_AUTH)
//                .withTransactionAmount(transactionRequest.getAmount())
//                .withOriginalTransactionAmount(transactionRequest.getAmount())
//                .withTransactionDate(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_DATE))
//                .withTransactionTime(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_TIME))
//                .withTransactionStatus(authCompletionApproved ? "APPROVED" : "DECLINED")
//                .withPosEntryMode(PosEntryMode.ICC)
//                .withOriginalPosEntryMode(PosEntryMode.ICC)
//                .withPosConditionCode(this.preAuthRequest.getPosConditionCode())
//                .withOriginalPosConditionCode("")
//                .withReconcileStatus("")
//                .withReconcileTime("")
//                .withReconcileDate("")
//                .withReconcileBatchNo(batchNumber)
//                .withReadCardResponse(readCardResponse)
//                .withAuthorizationCompleted(false)
//                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
//                .withTipAdjusted(false)
//                .withTransactionVoided(false)
//                .withTerminalId(this.terminalRepository.findTerminalInfo().getTerminalID())
//                .withMerchantId(this.terminalRepository.findTerminalInfo().getMerchantID())
//                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse,
//                        applicationRepository, batchNumber, stan, invoiceNumber))
//                .build();
//        transactionRepository.updateTransactionLog(transactionLog);
//    }
//
//    private PreAuthResponseModel handleApprovedTransactionDeclinedByCard(String stan, String batchNumber,
//                                                                         String invoiceNumber,
//                                                                         ReadCardResponse readCardResponse,
//                                                                         PreAuthRequestModel requestModel,
//                                                                         TransactionRequest transactionRequest) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, ReasonForReversal.Type.DECLINED_BY_CARD.getDescription());
//        this.performReversal(stan, readCardResponse, requestModel);
//        boolean shouldPrint = !this.approvePrintOnly(requestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest, readCardResponse,
//                    this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.DECLINED_BY_CARD,
//                            ReasonForReversal.Type.DECLINED_BY_CARD.getDescription()));
//            new NiblTransactionReceiptPrintHandler(requestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            requestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PreAuthResponseModel.Builder.createDefaultBuilder()
//                .approved(false)
//                .message(ReasonForReversal.Type.DECLINED_BY_CARD.getDescription())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private PreAuthResponseModel handleInvalidResponseFromHost(TransactionRequest transactionRequest,
//                                                               InvalidResponseHandlerResponse invalidResponseHandlerResponse,
//                                                               String batchNumber, String stan,
//                                                               String invoiceNumber,
//                                                               ReadCardResponse readCardResponse,
//                                                               PreAuthRequestModel requestModel) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, invalidResponseHandlerResponse.getMessage());
//        this.performReversal(stan, readCardResponse, requestModel);
//        boolean shouldPrint = !this.approvePrintOnly(requestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest,
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.INVALID_RESPONSE,
//                            invalidResponseHandlerResponse.getMessage()));
//            new NiblTransactionReceiptPrintHandler(requestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            requestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PreAuthResponseModel.Builder.createDefaultBuilder()
//                .approved(false)
//                .message(invalidResponseHandlerResponse.getMessage())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private void performReversal(
//            String stan,
//            ReadCardResponse readCardResponse,
//            PreAuthRequestModel preAuthRequestModel
//    ) {
//        ReversalUseCase reversalUseCase = new ReversalUseCase(
//                this.prepareReversalRequest(stan, readCardResponse, preAuthRequestModel),
//                this.terminalRepository,
//                this.notifier
//        );
//        try {
//            reversalUseCase.execute(
//                    prepareReversalRequestModel(
//                            preAuthRequestModel,
//                            readCardResponse
//                    )
//            );
//            this.transactionRepository.removeAutoReversal(this.autoReversal);
//        } catch (FinPosException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private NiblReversalRequest prepareReversalRequest(
//            String stan,
//            ReadCardResponse readCardResponse,
//            PreAuthRequestModel preAuthRequestModel
//    ) {
//        NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
//        niblReversalRequest.setProcessingCode(ProcessingCode.PRE_AUTHORIZATION.getCode());
//        niblReversalRequest.setAmount(
//                this.preAuthRequest.getTransactionAmount()
//        );
//        niblReversalRequest.setPan(this.preAuthRequest.getPan());
//        niblReversalRequest.setExpiryDate(this.preAuthRequest.getExpireDate());
//        niblReversalRequest.setPosEntryMode(this.preAuthRequest.getPosEntryMode());
//        niblReversalRequest.setPosConditionCode(this.preAuthRequest.getPosConditionCode());
//        niblReversalRequest.setTrack2Data(this.preAuthRequest.getTrack2Data());
//        niblReversalRequest.setPinBlock(this.preAuthRequest.getPinBlock());
//        niblReversalRequest.setInvoiceNumber(this.preAuthRequest.getInvoiceNumber());
//        niblReversalRequest.setStan(stan);
//        return niblReversalRequest;
//    }
//
//    private ReversalRequestModel prepareReversalRequestModel(
//            PreAuthRequestModel preAuthRequestModel,
//            ReadCardResponse readCardResponse
//    ) {
//        return ReversalRequestModel.Builder.newInstance()
//                .withTransactionRequest(
//                        preAuthRequestModel.getTransactionRequest()
//                )
//                .withTransactionRepository(
//                        preAuthRequestModel.getTransactionRepository()
//                )
//                .withPrinterService(
//                        preAuthRequestModel.getPrinterService()
//                )
//                .withApplicationRepository(
//                        preAuthRequestModel.getApplicationRepository()
//                )
//                .withReadCardResponse(readCardResponse)
//                .build();
//    }
//
//    private IsoMessageResponse sendIsoMessage(String stan, String invoiceNumber, ReadCardResponse readCardResponse, PreAuthRequestModel requestModel) {
//        RequestContext requestContext = this.prepareTemplateRequestContext(stan);
//        logger.debug(String.format(" Request Context ::%s", requestContext));
//        this.preAuthRequest = preparePreAuthRequest(invoiceNumber, readCardResponse);
//        requestContext.setRequest(this.preAuthRequest);
//        CardType detectedCardType = readCardResponse.getCardDetails().getCardType();
//        if (detectedCardType == null) {
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        }
//        this.autoReversal = new AutoReversal(
//                TransactionType.PRE_AUTH,
//                JsonUtils.toJsonObj(prepareReversalRequest(stan, readCardResponse, requestModel)),
//                0,
//                AutoReversal.Status.ACTIVE,
//                StringUtils.dateTimeStamp()
//        );
//        this.transactionRepository.saveAutoReversal(this.autoReversal);
//        PreAuthRequestSender sender;
//        switch (detectedCardType) {
//            case MAG:
//                sender = new MagPreAuthRequestSender(new NIBLSpecInfo(), requestContext);
//                break;
//
//            case PICC:
//                sender = new PICCPreAuthRequestSender(new NIBLSpecInfo(), requestContext);
//                break;
//
//            default:
//                sender = new IccPreAuthRequestSender(new NIBLSpecInfo(), requestContext);
//                break;
//        }
//        IsoMessageResponse isoMessageResponse = sender.send();
//        this.transactionRepository.removeAutoReversal(this.autoReversal);
//        return isoMessageResponse;
//    }
//
//    private PreAuthRequest preparePreAuthRequest(String invoiceNumber, ReadCardResponse readCardResponse) {
//        this.preAuthRequest = new PreAuthRequest();
//        this.preAuthRequest.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        this.preAuthRequest.setTransactionAmount(readCardResponse.getCardDetails().getAmount());
//        this.preAuthRequest.setPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()));
//        this.preAuthRequest.setPosConditionCode("00"); //TODO Change later
//        this.preAuthRequest.setTrack2Data(readCardResponse.getCardDetails().getTrackTwoData());
//        if (readCardResponse.getCardDetails().getPinBlock().getOfflinePin() ||
//                StringUtils.isEmpty(readCardResponse.getCardDetails().getPinBlock().getPin()) ||
//                readCardResponse.getCardDetails().getPinBlock().getPin().equalsIgnoreCase("offline"))
//            preAuthRequest.setPinBlock("");
//        else
//            preAuthRequest.setPinBlock(readCardResponse.getCardDetails().getPinBlock().getPin());
//        this.preAuthRequest.setEmvData(readCardResponse.getCardDetails().getIccDataBlock());
//        this.preAuthRequest.setExpireDate(readCardResponse.getCardDetails().getExpiryDate());
//        this.preAuthRequest.setCardSequenceNumber(readCardResponse.getCardDetails().getPrimaryAccountNumberSerialNumber());
//        this.preAuthRequest.setInvoiceNumber(invoiceNumber);
//        this.preAuthRequest.setLocalTime(HelperUtils.getDefaultLocaleTimeHhMmSs());
//        this.preAuthRequest.setLocalDate(HelperUtils.getDefaultLocaleDateWithoutYear());
//        this.preAuthRequest.setFallbackIccToMag(readCardResponse.getFallbackIccToMag());
//        return this.preAuthRequest;
//    }
//
//    private PurchaseRequest preparePurchaseCardRequest(TransactionRequest transactionRequest, CardDetails cardDetails, Boolean fallbackIccToMag) {
//        global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest
//                = new global.citytech.finposframework.usecases.transaction.data.PurchaseRequest(
//                transactionRequest.getTransactionType(),
//                transactionRequest.getAmount(),
//                transactionRequest.getAdditionalAmount()
//        );
//        cardDetails.setAmount(transactionRequest.getAmount());
//        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
//        purchaseRequest.setCardDetails(cardDetails);
//        purchaseRequest.setFallbackIccToMag(fallbackIccToMag);
//        return purchaseRequest;
//    }
//
//    private void checkForCardTypeChange(CardType cardType, PreAuthRequestModel requestModel) {
//        switch (cardType) {
//            case MAG:
//                this.cardProcessor = new global.citytech.finpos.processor.nibl.transaction.card.MagneticCardProcessor(
//                        requestModel.getTransactionRepository(),
//                        requestModel.getTransactionAuthenticator(),
//                        requestModel.getApplicationRepository(),
//                        requestModel.getReadCardService(),
//                        this.notifier,
//                        this.terminalRepository
//                );
//                break;
//            case ICC:
//                this.cardProcessor = new global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor(
//                        requestModel.getReadCardService(),
//                        requestModel.getTransactionRepository(),
//                        requestModel.getApplicationRepository(),
//                        requestModel.getTransactionAuthenticator(),
//                        this.notifier,
//                        this.terminalRepository
//                );
//                break;
//            case PICC:
//                this.cardProcessor = new PICCCardProcessor(
//                        requestModel.getTransactionRepository(),
//                        requestModel.getApplicationRepository(),
//                        requestModel.getReadCardService(),
//                        this.notifier,
//                        requestModel.getTransactionAuthenticator(),
//                        requestModel.getLedService(),
//                        requestModel.getSoundService(),
//                        this.terminalRepository
//                );
//                break;
//            default:
//                break;
//        }
//    }
//}