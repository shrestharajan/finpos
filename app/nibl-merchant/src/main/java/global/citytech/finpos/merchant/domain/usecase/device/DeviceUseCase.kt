package global.citytech.finpos.merchant.domain.usecase.device

import global.citytech.finpos.merchant.domain.model.device.DeviceResponse
import global.citytech.finpos.merchant.domain.model.device.LoadParameterRequest
import global.citytech.finpos.merchant.domain.repository.device.DeviceRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyInjectionRequest
import global.citytech.finposframework.utility.PosResponse
import io.reactivex.Observable

/**
 * Created by Rishav Chudal on 8/10/20.
 */
class DeviceUseCase(val deviceRepository: DeviceRepository) {
    fun injectKey(keyInjectionRequest: KeyInjectionRequest): Observable<DeviceResponse>
            = deviceRepository.inject(keyInjectionRequest)

    fun initSdk(): Observable<DeviceResponse> = deviceRepository.initSdk()

    fun loadTerminalParameters(loadParameterRequest: LoadParameterRequest):
            Observable<DeviceResponse> = deviceRepository
        .loadTerminalParameters(loadParameterRequest)

    fun closeUpIO() = deviceRepository.closeUpIO()

    fun checkIccCardPresentInReader(): Observable<Boolean>
            = deviceRepository.cardPresentInIccReader()

    fun enableHardwareButtons(): Observable<DeviceResponse>
            = deviceRepository.enableHardwareButtons()

    fun disableHardwareButtons(): Observable<DeviceResponse>
            = deviceRepository.disableHardwareButtons()

    fun readCardDetails(configurationItem: ConfigurationItem): Observable<ReadCardResponse>
            = deviceRepository.readCardDetails(configurationItem)

    fun posReady(cardTypeList: MutableList<CardType>): Observable<PosResponse>
            = deviceRepository.posReady(cardTypeList)
}