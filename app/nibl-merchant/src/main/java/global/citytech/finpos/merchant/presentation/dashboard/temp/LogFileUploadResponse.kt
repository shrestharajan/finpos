package global.citytech.finpos.merchant.presentation.dashboard.temp

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "PostResponse")
data class LogFileUploadResponse(
    @field:Element(name = "Bucket")
    var bucket: String? = null,

    @field:Element(name = "Key")
    var key: String? = null,

    @field:Element(name = "ETag")
    var eTag: String? = null,

    @field:Element(name = "Location")
    var location: String? = null
)