package global.citytech.finpos.processor.nibl.transactiontype;

import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.transactiontype.TransactionTypeRequestParameter;

/**
 * Created by Unique Shakya on 12/1/2020.
 */
public class TransactionTypeRequestModel implements UseCase.Request, TransactionTypeRequestParameter {
}
