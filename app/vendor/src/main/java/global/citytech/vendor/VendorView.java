package global.citytech.vendor;

import android.content.Context;

import global.citytech.easydroid.core.utils.MessageConfig;


/**
 * Created by Rishav Chudal on 6/9/21.
 */
interface VendorView {
    void showProgressDialogWithMessage(String message);

    void hideProgressDialog();

    Context getUiContext();

    String getStringWithGivenId(int stringId);

    void displayAlertMessage(MessageConfig.Builder builder);

}
