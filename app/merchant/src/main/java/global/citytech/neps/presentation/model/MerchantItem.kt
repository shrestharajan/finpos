package global.citytech.neps.presentation.model

import android.content.ContentValues
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import global.citytech.neps.domain.model.Host
import global.citytech.neps.domain.model.Merchant

data class MerchantItem(
    var id: String,
    var name: String? = null,
    var address: String? = null,
    var phoneNumber: String? = null,
    var switchId: String? = null,
    var terminalId: String? = null
)

fun Merchant.mapToPresentation(): MerchantItem = MerchantItem(id,name,address,phoneNumber,switchId,terminalId)
fun List<Merchant>.mapToPresentation(): List<MerchantItem> = map { it.mapToPresentation() }
