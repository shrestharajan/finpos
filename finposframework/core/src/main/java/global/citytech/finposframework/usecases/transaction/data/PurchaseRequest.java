package global.citytech.finposframework.usecases.transaction.data;

import java.math.BigDecimal;

import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.usecases.TransactionType;

/**
 * Created by Unique Shakya
 */

public class PurchaseRequest {

    private TransactionType transactionType;
    private BigDecimal amount = BigDecimal.ZERO;
    private BigDecimal additionalAmount = BigDecimal.ZERO;
    private CardDetails cardDetails;
    private String originalRrn;
    private String authCode;
    private String originalTransactionDate;
    private boolean fallbackIccToMag;

    public PurchaseRequest(TransactionType transactionType, BigDecimal amount, BigDecimal additionalAmount) {
        this.transactionType = transactionType;
        this.amount = amount;
        this.additionalAmount = additionalAmount;
    }

    public void setCardDetails(CardDetails cardDetails) {
        this.cardDetails = cardDetails;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getAdditionalAmount() {
        return additionalAmount;
    }

    public CardDetails getCardDetails() {
        return cardDetails;
    }

    public String getOriginalRrn() {
        return originalRrn;
    }

    public void setOriginalRrn(String originalRrn) {
        this.originalRrn = originalRrn;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getOriginalTransactionDate() {
        return originalTransactionDate;
    }

    public void setOriginalTransactionDate(String originalTransactionDate) {
        this.originalTransactionDate = originalTransactionDate;
    }

    public void setFallbackIccToMag(boolean fallbackIccToMag) {
        this.fallbackIccToMag = fallbackIccToMag;
    }

    public boolean isFallbackIccToMag() {
        return fallbackIccToMag;
    }

}