package global.citytech.finpos.processor.nibl.receipt.detailreport;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.hardware.io.printer.Printable;
import global.citytech.finposframework.hardware.io.printer.PrinterRequest;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finposframework.utility.ReceiptUtils.addBase64Image;
import static global.citytech.finposframework.utility.ReceiptUtils.addDoubleColumnString;
import static global.citytech.finposframework.utility.ReceiptUtils.addNullableValueDoubleColumnString;
import static global.citytech.finposframework.utility.ReceiptUtils.addSingleColumnString;

/**
 * Created by Unique Shakya on 1/4/2021.
 */
public class DetailReportReceiptHandler {

    private PrinterService printerService;

    public DetailReportReceiptHandler(PrinterService printerService) {
        this.printerService = printerService;
    }

    public PrinterResponse print(DetailReportReceipt detailReportReceipt) {
        PrinterRequest printerRequest = this.preparePrinterRequest(detailReportReceipt);
        printerRequest.setFeedPaperAfterFinish(false);
        return this.printerService.print(printerRequest);
    }

    private PrinterRequest preparePrinterRequest(DetailReportReceipt detailReportReceipt) {
        List<Printable> printableList = new ArrayList<>();
        if (detailReportReceipt.getRetailer().getRetailerLogo() != null)
            addBase64Image(printableList, detailReportReceipt.getRetailer().getRetailerLogo());
        addSingleColumnString(printableList, detailReportReceipt.getRetailer().getRetailerName(), DetailReportReceiptStyle.RETAILER_NAME.getStyle());
        addSingleColumnString(printableList, detailReportReceipt.getRetailer().getRetailerAddress(), DetailReportReceiptStyle.RETAILER_ADDRESS.getStyle());
        addSingleColumnString(printableList, DetailReportReceiptLabels.LINE_BREAK, DetailReportReceiptStyle.LINE_BREAK.getStyle());
        addDoubleColumnString(printableList,
                this.prepareDateString(detailReportReceipt.getPerformance().getStartDateTime()),
                this.prepareTimeString(detailReportReceipt.getPerformance().getStartDateTime()),
                DetailReportReceiptStyle.START_DATE_TIME.getStyle());
        addDoubleColumnString(printableList,
                this.merchantId(detailReportReceipt.getRetailer().getMerchantId()),
                this.terminalId(detailReportReceipt.getRetailer().getTerminalId()),
                DetailReportReceiptStyle.TID_MID.getStyle());
        addDoubleColumnString(printableList,
                DetailReportReceiptLabels.BATCH_NUMBER,
                detailReportReceipt.getReconciliationBatchNumber(),
                DetailReportReceiptStyle.BATCH_NUMBER.getStyle());
        addDoubleColumnString(printableList,
                DetailReportReceiptLabels.HOST,
                detailReportReceipt.getHost(),
                DetailReportReceiptStyle.HOST.getStyle());
        addSingleColumnString(printableList, DetailReportReceiptLabels.LINE_BREAK, DetailReportReceiptStyle.LINE_BREAK.getStyle());
        addSingleColumnString(printableList, DetailReportReceiptLabels.DETAIL_REPORT, DetailReportReceiptStyle.HEADER.getStyle());
        addSingleColumnString(printableList, DetailReportReceiptLabels.LINE_BREAK, DetailReportReceiptStyle.LINE_BREAK.getStyle());
        this.addReportLabels(printableList);
        this.addReportValues(printableList, detailReportReceipt.getDetailReports());
        addSingleColumnString(printableList, DetailReportReceiptLabels.FOOTER, DetailReportReceiptStyle.FOOTER.getStyle());
        addSingleColumnString(printableList, DetailReportReceiptLabels.LINE_BREAK, DetailReportReceiptStyle.LINE_BREAK.getStyle());
        return new PrinterRequest(printableList);
    }

    private void addReportValues(List<Printable> printableList, List<DetailReport> detailReports) {
        for (DetailReport detailReport : detailReports) {
            addNullableValueDoubleColumnString(printableList, detailReport.getCardNumber(), detailReport.getCardScheme(), DetailReportReceiptStyle.REPORT_VALUE.getStyle());
            addNullableValueDoubleColumnString(printableList, detailReport.getInvoiceNumber(), detailReport.getApprovalCode(), DetailReportReceiptStyle.REPORT_VALUE.getStyle());
            addNullableValueDoubleColumnString(printableList, detailReport.getTransactionType(), detailReport.getAmount(), DetailReportReceiptStyle.REPORT_VALUE.getStyle());
            addNullableValueDoubleColumnString(printableList, detailReport.getTransactionDate(), detailReport.getTransactionTime(), DetailReportReceiptStyle.REPORT_VALUE.getStyle());
            addSingleColumnString(printableList, DetailReportReceiptLabels.LINE_BREAK, DetailReportReceiptStyle.LINE_BREAK.getStyle());
        }
    }

    private void addReportLabels(List<Printable> printableList) {
        addDoubleColumnString(printableList, DetailReportReceiptLabels.CARD_NUMBER, DetailReportReceiptLabels.CARD_SCHEME, DetailReportReceiptStyle.REPORT_LABEL.getStyle());
        addDoubleColumnString(printableList, DetailReportReceiptLabels.INVOICE_NUMBER, DetailReportReceiptLabels.APPROVAL_CODE, DetailReportReceiptStyle.REPORT_LABEL.getStyle());
        addDoubleColumnString(printableList, DetailReportReceiptLabels.TRANSACTION_TYPE, DetailReportReceiptLabels.AMOUNT, DetailReportReceiptStyle.REPORT_LABEL.getStyle());
        addDoubleColumnString(printableList, DetailReportReceiptLabels.TRANSACTION_DATE, DetailReportReceiptLabels.TRANSACTION_TIME, DetailReportReceiptStyle.REPORT_LABEL.getStyle());
        addSingleColumnString(printableList, DetailReportReceiptLabels.DIVIDER, DetailReportReceiptStyle.DIVIDER.getStyle());
    }

    private String merchantId(String merchantId) {
        return this.formatLabelWithValue(DetailReportReceiptLabels.MERCHANT_ID, merchantId);
    }

    private String terminalId(String terminalId) {
        return this.formatLabelWithValue(DetailReportReceiptLabels.TERMINAL_ID, terminalId);
    }

    private String prepareTimeString(String dateTime) {
        if (StringUtils.isEmpty(dateTime) || dateTime.length() < 15)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(dateTime, 6, 8);
        stringBuilder.append(":");
        stringBuilder.append(dateTime, 8, 10);
        stringBuilder.append(":");
        stringBuilder.append(dateTime, 10, 12);
        return this.formatLabelWithValue(DetailReportReceiptLabels.TIME, stringBuilder.toString());
    }

    private String prepareDateString(String dateTime) {
        if (StringUtils.isEmpty(dateTime) || dateTime.length() < 15)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(dateTime, 4, 6);
        stringBuilder.append("/");
        stringBuilder.append(dateTime, 2, 4);
        stringBuilder.append("/");
        stringBuilder.append("20");
        stringBuilder.append(dateTime, 0, 2);
        return this.formatLabelWithValue(DetailReportReceiptLabels.DATE, stringBuilder.toString());
    }

    private String formatLabelWithValue(String label, String value) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(label);
        stringBuilder.append(":");
        stringBuilder.append(value);
        return stringBuilder.toString();
    }
}
