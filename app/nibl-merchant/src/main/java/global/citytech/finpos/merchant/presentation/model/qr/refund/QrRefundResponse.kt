package global.citytech.finpos.merchant.presentation.model.qr.refund

data class QrRefundResponse(
    val requestNumber: String
)