package global.citytech.finpos.merchant.presentation.payment;


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/12/07 - 11:54 AM
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import global.citytech.easydroid.core.utils.Jsons;
import global.citytech.finpos.merchant.NiblMerchant;
import global.citytech.finpos.merchant.framework.datasource.device.DeviceControllerImpl;
import global.citytech.finpos.merchant.utils.ActivityUtils;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.payment.sdk.api.PaymentResponse;
import global.citytech.payment.sdk.api.PaymentResult;
import global.citytech.payment.sdk.core.PaymentConstants;

public class PaymentSdkSingleton {

    private static PaymentSdkSingleton INSTANCE;
    private boolean isFromPaymentSdk = false;
    private PaymentResponse paymentResponse;

    public static PaymentSdkSingleton getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PaymentSdkSingleton();
        }
        return INSTANCE;
    }

    public synchronized boolean isFromPaymentSdk() {
        return isFromPaymentSdk;
    }

    public synchronized void setFromPaymentSdk(boolean fromPaymentSdk) {
        isFromPaymentSdk = fromPaymentSdk;
    }

    public synchronized PaymentResponse getPaymentResponse() {
        return paymentResponse;
    }

    public synchronized void updatePaymentResponse(PaymentResponse paymentResponse) {
        this.paymentResponse = paymentResponse;
    }

    public void returnActivityResultWithExtraData(Context context) {
        Intent intent = prepareIntentWithPaymentResponse();
        enableHardwareButtons(context);
        ActivityUtils.setResultAndFinish(context, Activity.RESULT_OK, intent);
    }

    private Intent prepareIntentWithPaymentResponse() {
        String paymentResponseJson = Jsons.toJsonObj(paymentResponse);
        Intent intent = new Intent();
        intent.putExtra(PaymentConstants.EXTRA_DATA, paymentResponseJson);
        return intent;
    }

    public void enableHardwareButtons(Context context) {
        DeviceController deviceController = new DeviceControllerImpl(NiblMerchant.INSTANCE);
        deviceController.enableHardwareButtons();
    }

    public void cleanup() {
        isFromPaymentSdk = false;
        paymentResponse = new PaymentResponse(PaymentResult.FAILED);
        INSTANCE = null;
    }


}
