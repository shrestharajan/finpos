package global.citytech.finpos.processor.nibl.batchupload;

/**
 * Created by Unique Shakya on 5/14/2021.
 */
public class BatchUploadReceiptLabels {

    public static final String BATCH_UPLOAD = "BATCH REPORT";
    public static final String BATCH_CLEAR = "CLEARED BATCH REPORT";
    public static final String BATCH_REPORT_END = "END OF REPORT";
    public static final String BATCH_NUMBER = "BATCH NUMBER";
    public static final String CARD_NUMBER = "CARD NUMBER";
    public static final String CARD_SCHEME = "CARD SCHEME";
    public static final String INVOICE_NUMBER = "INVOICE NUMBER";
    public static final String APPROVAL_CODE = "APPROVAL CODE";
    public static final String TRANSACTION_TYPE = "TRANSACTION";
    public static final String TRANSACTION_DATE = "TRANSACTION DATE";
    public static final String TRANSACTION_TIME = "TRANSACTION TIME";
    public static final String AMOUNT = "AMOUNT";
    public static final String RESPONSE_CODE = "RESPONSE CODE";
    public static final String DIVIDER = "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --";
    public static final String LINE_BREAK = "  ";
}
