package global.citytech.finposframework.utility;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

/**
 * Created by Unique Shakya on 12/16/2020.
 */
public class JsonUtils {
    private static Gson gson = (new GsonBuilder()).serializeNulls().create();

    private JsonUtils() {
    }

    public static <T> String toJsonObj(T obj) {
        return gson.toJson(obj);
    }

    public static <T> String toJsonList(List<T> objCol) {
        return gson.toJson(objCol);
    }

    public static <T> T fromJsonToObj(String jsonString, Class<T> obj) {
        if (!isValidJsonString(jsonString)) {
            throw new IllegalArgumentException(" Invalid Json data.");
        } else {
            return gson.fromJson(jsonString, obj);
        }
    }

    public static String toJsonTree(Object src) {
        return gson.toJsonTree(src).toString();
    }

    public static boolean isValidJsonString(String jsonString) {
        if (jsonString == null || jsonString.equals("")) {
            return false;
        } else {
            try {
                gson.fromJson(jsonString, Object.class);
                return true;
            } catch (Exception var2) {
                return false;
            }
        }
    }
}
