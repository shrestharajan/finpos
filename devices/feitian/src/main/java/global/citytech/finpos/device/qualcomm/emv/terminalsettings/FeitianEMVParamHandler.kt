package global.citytech.finpos.device.qualcomm.emv.terminalsettings

import android.content.Context
import com.ftpos.library.smartpos.bean.DefaultParamsBean
import com.ftpos.library.smartpos.device.Device
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.emv.ICSparameter
import com.ftpos.library.smartpos.errcode.ErrCode
import global.citytech.common.extensions.defaultOrEmptyValue
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finpos.device.qualcomm.utils.FeitianUtils
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_FAILURE_EMV_PARAM_INJECT
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_SUCCESS_EMV_PARAM_INJECT
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result.FAILURE
import global.citytech.finposframework.hardware.common.Result.SUCCESS
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersService

/**
 * Created by Rishav Chudal on 4/6/21.
 */
class FeitianEMVParamHandler constructor(context: Context) : EmvParametersService,
    FeitianLogger.Listener {

    private val emv = Emv.getInstance(context.applicationContext)
    private val device: Device = Device.getInstance(context.applicationContext)
    private val logger = FeitianLogger(this)
    private lateinit var deviceResponse: DeviceResponse

    override fun injectEmvParameters(emvParametersRequest: EmvParametersRequest): DeviceResponse {
        this.deviceResponse = DeviceResponse(FAILURE, MSG_FAILURE_EMV_PARAM_INJECT)
        this.proceedForSettingICSParam()
        this.proceedForSettingEMVParam(emvParametersRequest)
        return this.deviceResponse
    }

    private fun proceedForSettingICSParam() {
        this.logger.logWithClassName("***** Setting ICS Param *****")
        val icsParameter = prepareICSParameter()
        val resultICSParameterSet = this.emv.setICS(icsParameter)
        this.logger.logWithClassName("Setting ICS Param result ::: ".plus(resultICSParameterSet))
    }

    private fun prepareICSParameter(): ICSparameter {
        val icsParameters = ICSparameter()

        val supportPinByPass = false
        this.logger.logWithClassName("Support PIN ByPass ::: ".plus(supportPinByPass))
        icsParameters.isEMVSupportPINBypass = supportPinByPass

        return icsParameters
    }

    private fun proceedForSettingEMVParam(emvParametersRequest: EmvParametersRequest) {
        this.logger.logWithClassName("***** Setting EMV Param ******")
        val defaultParamsBeanByteArray = prepareDefaultParamsBeanByteArray(emvParametersRequest)
        val resultDefaultParamSet = this.emv.setDefaultAppParameters(defaultParamsBeanByteArray)
        this.logger.logWithClassName("Setting Default Param result ::: ".plus(resultDefaultParamSet))
        if (resultDefaultParamSet == ErrCode.ERR_SUCCESS) {
            this.deviceResponse = DeviceResponse(SUCCESS, MSG_SUCCESS_EMV_PARAM_INJECT)
        }
    }

    private fun prepareDefaultParamsBeanByteArray(
        emvParametersRequest: EmvParametersRequest
    ): ByteArray? {
        val defaultParamsBean = DefaultParamsBean()

        val terminalType = emvParametersRequest.terminalType.defaultOrEmptyValue()
        this.logger.debugWithClassName("Terminal Type ::: ".plus(terminalType))
        defaultParamsBean.setmTerminalType_9F35(terminalType)

        val terminalCapabilities = emvParametersRequest.terminalCapabilities.defaultOrEmptyValue()
        this.logger.debugWithClassName("Terminal Capabilities ::: ".plus(terminalCapabilities))
        defaultParamsBean.setmTerminalCapabilities_9F33(terminalCapabilities)

        val additionalTerminalCapabilities =
            emvParametersRequest.additionalTerminalCapabilities.defaultOrEmptyValue()
        this.logger.debugWithClassName(
            "Additional Terminal Capabilities ::: ".plus(
                additionalTerminalCapabilities
            )
        )
        defaultParamsBean.setmAdditionalTerminalCapabilities_9F40(additionalTerminalCapabilities)

        val merchantCategoryCode = emvParametersRequest.merchantCategoryCode.defaultOrEmptyValue()
        this.logger.debugWithClassName("Merchant Category Code ::: ".plus(merchantCategoryCode))
        defaultParamsBean.setmMerchantCategoryCode_9F15(merchantCategoryCode)

        val merchantId = emvParametersRequest.merchantIdentifier.defaultOrEmptyValue()
        this.logger.debugWithClassName("Merchant Id ::: ".plus(merchantId))
        defaultParamsBean.setmMerchantID_9F16(merchantId)

        val merchantNameLocation = emvParametersRequest.merchantName.defaultOrEmptyValue()
        this.logger.debugWithClassName("Merchant name and location ::: ".plus(merchantNameLocation))
        defaultParamsBean.setmMerchantNameLocation_9F4E(merchantNameLocation)

        val terminalId = emvParametersRequest.terminalId.defaultOrEmptyValue()
        this.logger.debugWithClassName("Terminal Id ::: ".plus(terminalId))
        defaultParamsBean.setmTerminalID_9F1C(terminalId)

        val terminalCountryCode = emvParametersRequest.terminalCountryCode.defaultOrEmptyValue()
        this.logger.debugWithClassName("Terminal Country Code ::: ".plus(terminalCountryCode))
        defaultParamsBean.setmTerminalCountryCode_9F1A(terminalCountryCode)

        val ifdSerialNumber = FeitianUtils.retrieveDeviceSerialNumber(device)
        logger.debugWithClassName("IFD Serial Number ::: ".plus(ifdSerialNumber))
        defaultParamsBean.setmIFDSerial_9F1E(ifdSerialNumber)

        return defaultParamsBean.toTlvByteArray()
    }

    override fun getClassName(): String = FeitianEMVParamHandler::class.java.simpleName
}