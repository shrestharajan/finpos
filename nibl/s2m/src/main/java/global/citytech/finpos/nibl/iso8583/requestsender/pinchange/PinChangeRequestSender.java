package global.citytech.finpos.nibl.iso8583.requestsender.pinchange;

import global.citytech.finpos.nibl.iso8583.NIBLMessagePayloadBuilder;
import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.PosEntryModeConfig;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.StringUtils;

public class PinChangeRequestSender extends NIBLMessageSenderTemplate {

    private final Logger logger = Logger.getLogger(PinChangeRequestSender.class.getName());

    public PinChangeRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Authorization,
                        MTI.MessageFunction.Request,
                        MTI.Originator.Acquirer);
        TransactionIsoRequest pinChangeRequest = (TransactionIsoRequest) this.context.getRequest();
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);

        msg.addField(
                new DataElement(
                        DataElement.PRIMARY_ACCOUNT_NUMBER,
                        pinChangeRequest.getPan()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.LOCAL_TIME,
                        pinChangeRequest.getLocalTime()
                )
        );
        logger.debug("LOCAL_DATE"+ DataElement.LOCAL_DATE);
        msg.addField(
                new DataElement(
                        DataElement.LOCAL_DATE,
                        pinChangeRequest.getLocalDate()
                )

        );
        msg.addField(
                new DataElement(
                        DataElement.PAN_EXPIRE_DATE,
                        pinChangeRequest.getExpireDate()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(pinChangeRequest.getPosEntryMode())
                )
        );// Based on transaction source like ICC,Manual

        msg.addField(
                new DataElement(
                        DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE,
                        pinChangeRequest.getPosConditionCode()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.PIN_BLOCK,
                        pinChangeRequest.getPinBlock()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.PRIVATE_USE_FIELD_63,
                        pinChangeRequest.getAdditionalData()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.ICC_SYSTEM_RELATED_DATA,
                        pinChangeRequest.getEmvData()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.APPLICATION_PAN_SEQUENCE_NUMBER,
                        StringUtils.ofRequiredLength(
                                pinChangeRequest.getCardSequenceNumber(),
                                4
                        )
                )
        );



        MessagePayload.Builder builder = new NIBLMessagePayloadBuilder(this.specInfo, msg);
        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());
        return builder.build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.PIN_CHANGE.getCode();
    }

    @Override
    protected String getFunctionCode() {
        return "0011";
    }
}
