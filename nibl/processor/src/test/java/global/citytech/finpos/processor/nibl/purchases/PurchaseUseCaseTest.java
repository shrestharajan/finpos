package global.citytech.finpos.processor.nibl.purchases;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import global.citytech.finpos.processor.nibl.transaction.initializer.NiblTransactionInitializer;
import global.citytech.finpos.processor.nibl.transaction.validator.NiblTransactionValidator;
import global.citytech.finpos.processor.nibl.transaction.purchase.PurchaseRequestModel;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.TransactionInitializer;
import global.citytech.finposframework.usecases.transaction.TransactionValidator;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.PosResponse;
import global.citytech.finposframework.utility.PosResult;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.powermock.api.mockito.PowerMockito.*;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by Rishav Chudal on 10/8/20.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({EmvParametersRequest.class, ReadCardResponse.class})
public class PurchaseUseCaseTest {

    private global.citytech.finpos.processor.nibl.transaction.purchase.PurchaseRequestModel mockPurchaseRequestModel;
    private TerminalRepository mockTerminalRepository;
    private Notifier mockNotifier;
    private TransactionRequest transactionRequest;
    private TransactionRepository transactionRepository;
    private ReadCardService mockReadCardService;
    private DeviceController mockeDeviceController;
    private TransactionAuthenticator transactionAuthenticator;
    private PrinterService printerService;
    private ApplicationRepository mockApplicationRepository;
    private ReconciliationRepository reconciliationRepository;
    private EmvParametersRequest emvParametersRequest;
    private ReadCardResponse mockReadCardResponse;
    private PurchaseUseCase SUT;
    private TransactionInitializer mockTransactionInitializer;
    private TransactionValidator mockTransactionValidator;

    @Before
    public void setUp() {
        this.mockPurchaseRequestModel = mock(PurchaseRequestModel.class);
        this.mockTerminalRepository = mock(TerminalRepository.class);
        this.mockNotifier = mock(Notifier.class);
        this.mockeDeviceController = mock(DeviceController.class);
        this.mockReadCardService = mock(ReadCardService.class);
        this.mockApplicationRepository = mock(ApplicationRepository.class);
        this.mockReadCardResponse = mock(ReadCardResponse.class);
        this.mockTransactionInitializer = mock(TransactionInitializer.class);
        this.mockTransactionValidator = mock(TransactionValidator.class);
        this.SUT = new PurchaseUseCase(this.mockTerminalRepository, this.mockNotifier);
    }

    @Test
    public void getClassName() {
    }

    /**
     * Case Device not ready
     */
    @Test
    public void executeOnDeviceNotReady() {
        //Setup
        when(this.mockTerminalRepository.getSystemTraceAuditNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getInvoiceNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getReconciliationBatchNumber())
                .thenReturn("001");
        when(this.mockPurchaseRequestModel.getTransactionRequest())
                .thenReturn(prepareMockTransactionRequest());
        when(this.mockPurchaseRequestModel.getDeviceController())
                .thenReturn(this.mockeDeviceController);
        when(this.mockeDeviceController.isReady(any()))
                .thenReturn(new PosResponse(PosResult.FAILURE));
        when(this.mockPurchaseRequestModel.getReadCardService())
                .thenReturn(this.mockReadCardService);
        doNothing().when(this.mockReadCardService).cleanUp();

        //Run
        Exception exception = assertThrows(
                PosException.class,
                () -> this.SUT.execute(this.mockPurchaseRequestModel)
        );

        //Verify
        String expectedMessage = PosError.DEVICE_ERROR_NOT_READY.getErrorMessage();
        assertEquals(exception.getMessage(), expectedMessage);
    }

    /**
     *  Case Invalid ReadCardResponse
     */
    @Test
    public void executeOnInvalidReadCardResponse() throws Exception {
        //Setup
        when(this.mockTerminalRepository.getSystemTraceAuditNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getInvoiceNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getReconciliationBatchNumber())
                .thenReturn("001");
        when(this.mockPurchaseRequestModel.getTransactionRequest())
                .thenReturn(prepareMockTransactionRequest());
        when(this.mockPurchaseRequestModel.getDeviceController())
                .thenReturn(this.mockeDeviceController);
        when(this.mockeDeviceController.isReady(any()))
                .thenReturn(new PosResponse(PosResult.SUCCESS));
        when(this.mockPurchaseRequestModel.getReadCardService())
                .thenReturn(this.mockReadCardService);
        doNothing().when(this.mockReadCardService).cleanUp();

        whenNew(NiblTransactionInitializer.class)
                .withAnyArguments()
                .thenReturn((NiblTransactionInitializer) this.mockTransactionInitializer);
        when(this.mockTransactionInitializer.initialize(any())).thenReturn(any());

        whenNew(NiblTransactionValidator.class)
                .withAnyArguments()
                .thenReturn((NiblTransactionValidator) this.mockTransactionValidator);
        doNothing().when(this.mockTransactionValidator).validateRequest(any());


    }

    /**
     * Case Invalid Amount
     */
    @Test
    public void executeOnInvalidAmount() {
        //Setup
        when(this.mockTerminalRepository.getSystemTraceAuditNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getInvoiceNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getReconciliationBatchNumber())
                .thenReturn("001");
        when(this.mockPurchaseRequestModel.getTransactionRequest())
                .thenReturn(prepareMockInvalidAmountTransactionRequest());
        when(this.mockPurchaseRequestModel.getDeviceController())
                .thenReturn(this.mockeDeviceController);
        when(this.mockeDeviceController.isReady(any()))
                .thenReturn(new PosResponse(PosResult.SUCCESS));
        when(this.mockPurchaseRequestModel.getReadCardService())
                .thenReturn(this.mockReadCardService);
        doNothing().when(this.mockReadCardService).cleanUp();
        when(this.mockPurchaseRequestModel.getApplicationRepository())
                .thenReturn(this.mockApplicationRepository);
        this.emvParametersRequest = mock(EmvParametersRequest.class);
        when(this.mockApplicationRepository.retrieveEmvParameterRequest())
                .thenReturn(this.emvParametersRequest);

        //Run
        Exception exception = assertThrows(
                PosException.class,
                () -> this.SUT.execute(this.mockPurchaseRequestModel)
        );

        //Verify
        String expectedMessage = PosError.DEVICE_ERROR_INVALID_AMOUNT.getErrorMessage();
        assertEquals(expectedMessage, exception.getMessage());
    }

    /**
     * Case ReadCardResponse null
     */
    @Test
    public void executeOnReadCardResponseNull() {
        //Setup
        when(this.mockTerminalRepository.getSystemTraceAuditNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getInvoiceNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getReconciliationBatchNumber())
                .thenReturn("001");
        when(this.mockPurchaseRequestModel.getTransactionRequest())
                .thenReturn(prepareMockTransactionRequest());
        when(this.mockPurchaseRequestModel.getDeviceController())
                .thenReturn(this.mockeDeviceController);
        when(this.mockeDeviceController.isReady(any()))
                .thenReturn(new PosResponse(PosResult.SUCCESS));
        when(this.mockPurchaseRequestModel.getReadCardService())
                .thenReturn(this.mockReadCardService);
        doNothing().when(this.mockReadCardService).cleanUp();
        when(this.mockPurchaseRequestModel.getApplicationRepository())
                .thenReturn(this.mockApplicationRepository);
        this.emvParametersRequest = mock(EmvParametersRequest.class);
        when(this.mockApplicationRepository.retrieveEmvParameterRequest())
                .thenReturn(this.emvParametersRequest);
        doNothing().when(this.mockNotifier).notify(any());
        doNothing().when(this.mockNotifier).notify(any(), any());
        when(this.mockReadCardService.readCardDetails(any())).thenReturn(null);

        //Run
        Exception exception = assertThrows(
                PosException.class,
                () -> this.SUT.execute(this.mockPurchaseRequestModel)
        );

        //Verify
        Mockito.verify(this.mockNotifier).notify(any());
        String expectedMessage = PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD.getErrorMessage();
        assertEquals(expectedMessage, exception.getMessage());
    }

    /**
     * Case ReadCardResponse Application Blocked
     */
    @Test
    public void executeOnReadCardResponseApplicationBlocked() {
        //Setup
        when(this.mockTerminalRepository.getSystemTraceAuditNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getInvoiceNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getReconciliationBatchNumber())
                .thenReturn("001");
        when(this.mockPurchaseRequestModel.getTransactionRequest())
                .thenReturn(prepareMockTransactionRequest());
        when(this.mockPurchaseRequestModel.getDeviceController())
                .thenReturn(this.mockeDeviceController);
        when(this.mockeDeviceController.isReady(any()))
                .thenReturn(new PosResponse(PosResult.SUCCESS));
        when(this.mockPurchaseRequestModel.getReadCardService())
                .thenReturn(this.mockReadCardService);
        doNothing().when(this.mockReadCardService).cleanUp();
        when(this.mockPurchaseRequestModel.getApplicationRepository())
                .thenReturn(this.mockApplicationRepository);
        this.emvParametersRequest = mock(EmvParametersRequest.class);
        when(this.mockApplicationRepository.retrieveEmvParameterRequest())
                .thenReturn(this.emvParametersRequest);
        doNothing().when(this.mockNotifier).notify(any());
        doNothing().when(this.mockNotifier).notify(any(), any());
        when(this.mockReadCardService.readCardDetails(any())).thenReturn(this.mockReadCardResponse);
        when(this.mockReadCardResponse.getResult()).thenReturn(Result.APPLICATION_BLOCKED);

        //Run
        Exception exception = assertThrows(
                PosException.class,
                () -> this.SUT.execute(this.mockPurchaseRequestModel)
        );

        //Verify
        Mockito.verify(this.mockNotifier).notify(any());
        String expectedMessage = PosError.DEVICE_ERROR_APPLICATION_BLOCKED.getErrorMessage();
        assertEquals(expectedMessage, exception.getMessage());
    }

    /**
     * Case ReadCardResponse User Cancelled
     */
    @Test
    public void executeOnReadCardResponseUserCancelled() {
        //Setup
        when(this.mockTerminalRepository.getSystemTraceAuditNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getInvoiceNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getReconciliationBatchNumber())
                .thenReturn("001");
        when(this.mockPurchaseRequestModel.getTransactionRequest())
                .thenReturn(prepareMockTransactionRequest());
        when(this.mockPurchaseRequestModel.getDeviceController())
                .thenReturn(this.mockeDeviceController);
        when(this.mockeDeviceController.isReady(any()))
                .thenReturn(new PosResponse(PosResult.SUCCESS));
        when(this.mockPurchaseRequestModel.getReadCardService())
                .thenReturn(this.mockReadCardService);
        doNothing().when(this.mockReadCardService).cleanUp();
        when(this.mockPurchaseRequestModel.getApplicationRepository())
                .thenReturn(this.mockApplicationRepository);
        this.emvParametersRequest = mock(EmvParametersRequest.class);
        when(this.mockApplicationRepository.retrieveEmvParameterRequest())
                .thenReturn(this.emvParametersRequest);
        doNothing().when(this.mockNotifier).notify(any());
        doNothing().when(this.mockNotifier).notify(any(), any());
        when(this.mockReadCardService.readCardDetails(any())).thenReturn(this.mockReadCardResponse);
        when(this.mockReadCardResponse.getResult()).thenReturn(Result.USER_CANCELLED);

        //Run
        Exception exception = assertThrows(
                PosException.class,
                () -> this.SUT.execute(this.mockPurchaseRequestModel)
        );

        //Verify
        Mockito.verify(this.mockNotifier).notify(any());
        String expectedMessage = PosError.DEVICE_ERROR_USER_CANCELLED.getErrorMessage();
        assertEquals(expectedMessage, exception.getMessage());
    }

    /**
     * Case ReadCardResponse Time Out
     */
    @Test
    public void executeOnReadCardResponseTimeOut() {
        //Setup
        when(this.mockTerminalRepository.getSystemTraceAuditNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getInvoiceNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getReconciliationBatchNumber())
                .thenReturn("001");
        when(this.mockPurchaseRequestModel.getTransactionRequest())
                .thenReturn(prepareMockTransactionRequest());
        when(this.mockPurchaseRequestModel.getDeviceController())
                .thenReturn(this.mockeDeviceController);
        when(this.mockeDeviceController.isReady(any()))
                .thenReturn(new PosResponse(PosResult.SUCCESS));
        when(this.mockPurchaseRequestModel.getReadCardService())
                .thenReturn(this.mockReadCardService);
        doNothing().when(this.mockReadCardService).cleanUp();
        when(this.mockPurchaseRequestModel.getApplicationRepository())
                .thenReturn(this.mockApplicationRepository);
        this.emvParametersRequest = mock(EmvParametersRequest.class);
        when(this.mockApplicationRepository.retrieveEmvParameterRequest())
                .thenReturn(this.emvParametersRequest);
        doNothing().when(this.mockNotifier).notify(any());
        doNothing().when(this.mockNotifier).notify(any(), any());
        when(this.mockReadCardService.readCardDetails(any())).thenReturn(this.mockReadCardResponse);
        when(this.mockReadCardResponse.getResult()).thenReturn(Result.TIMEOUT);

        //Run
        Exception exception = assertThrows(
                PosException.class,
                () -> this.SUT.execute(this.mockPurchaseRequestModel)
        );

        //Verify
        Mockito.verify(this.mockNotifier).notify(any());
        String expectedMessage = PosError.DEVICE_ERROR_PINPAD_TIMEOUT.getErrorMessage();
        assertEquals(expectedMessage, exception.getMessage());
    }

    /**
     * Case ReadCardResponse PinPad Error
     */
    @Test
    public void executeOnReadCardResponsePinPadError() {
        //Setup
        when(this.mockTerminalRepository.getSystemTraceAuditNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getInvoiceNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getReconciliationBatchNumber())
                .thenReturn("001");
        when(this.mockPurchaseRequestModel.getTransactionRequest())
                .thenReturn(prepareMockTransactionRequest());
        when(this.mockPurchaseRequestModel.getDeviceController())
                .thenReturn(this.mockeDeviceController);
        when(this.mockeDeviceController.isReady(any()))
                .thenReturn(new PosResponse(PosResult.SUCCESS));
        when(this.mockPurchaseRequestModel.getReadCardService())
                .thenReturn(this.mockReadCardService);
        doNothing().when(this.mockReadCardService).cleanUp();
        when(this.mockPurchaseRequestModel.getApplicationRepository())
                .thenReturn(this.mockApplicationRepository);
        this.emvParametersRequest = mock(EmvParametersRequest.class);
        when(this.mockApplicationRepository.retrieveEmvParameterRequest())
                .thenReturn(this.emvParametersRequest);
        doNothing().when(this.mockNotifier).notify(any());
        doNothing().when(this.mockNotifier).notify(any(), any());
        when(this.mockReadCardService.readCardDetails(any())).thenReturn(this.mockReadCardResponse);
        when(this.mockReadCardResponse.getResult()).thenReturn(Result.PINPAD_ERROR);

        //Run
        Exception exception = assertThrows(
                PosException.class,
                () -> this.SUT.execute(this.mockPurchaseRequestModel)
        );

        //Verify
        Mockito.verify(this.mockNotifier).notify(any());
        String expectedMessage = PosError.DEVICE_ERROR_PINPAD_ERROR.getErrorMessage();
        assertEquals(expectedMessage, exception.getMessage());
    }

    /**
     * Case ReadCardResponse Processing Error
     */
    @Test
    public void executeOnReadCardResponseProcessingError() {
        //Setup
        when(this.mockTerminalRepository.getSystemTraceAuditNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getInvoiceNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getReconciliationBatchNumber())
                .thenReturn("001");
        when(this.mockPurchaseRequestModel.getTransactionRequest())
                .thenReturn(prepareMockTransactionRequest());
        when(this.mockPurchaseRequestModel.getDeviceController())
                .thenReturn(this.mockeDeviceController);
        when(this.mockeDeviceController.isReady(any()))
                .thenReturn(new PosResponse(PosResult.SUCCESS));
        when(this.mockPurchaseRequestModel.getReadCardService())
                .thenReturn(this.mockReadCardService);
        doNothing().when(this.mockReadCardService).cleanUp();
        when(this.mockPurchaseRequestModel.getApplicationRepository())
                .thenReturn(this.mockApplicationRepository);
        this.emvParametersRequest = mock(EmvParametersRequest.class);
        when(this.mockApplicationRepository.retrieveEmvParameterRequest())
                .thenReturn(this.emvParametersRequest);
        doNothing().when(this.mockNotifier).notify(any());
        doNothing().when(this.mockNotifier).notify(any(), any());
        when(this.mockReadCardService.readCardDetails(any())).thenReturn(this.mockReadCardResponse);
        when(this.mockReadCardResponse.getResult()).thenReturn(Result.PROCESSING_ERROR);

        //Run
        Exception exception = assertThrows(
                PosException.class,
                () -> this.SUT.execute(this.mockPurchaseRequestModel)
        );

        //Verify
        Mockito.verify(this.mockNotifier).notify(any());
        String expectedMessage = PosError.DEVICE_ERROR_PROCESSING_ERROR.getErrorMessage();
        assertEquals(expectedMessage, exception.getMessage());
    }

    /**
     * Case ReadCardResponse Failure
     */
    @Test
    public void executeOnReadCardResponseFailure() {
        //Setup
        when(this.mockTerminalRepository.getSystemTraceAuditNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getInvoiceNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getReconciliationBatchNumber())
                .thenReturn("001");
        when(this.mockPurchaseRequestModel.getTransactionRequest())
                .thenReturn(prepareMockTransactionRequest());
        when(this.mockPurchaseRequestModel.getDeviceController())
                .thenReturn(this.mockeDeviceController);
        when(this.mockeDeviceController.isReady(any()))
                .thenReturn(new PosResponse(PosResult.SUCCESS));
        when(this.mockPurchaseRequestModel.getReadCardService())
                .thenReturn(this.mockReadCardService);
        doNothing().when(this.mockReadCardService).cleanUp();
        when(this.mockPurchaseRequestModel.getApplicationRepository())
                .thenReturn(this.mockApplicationRepository);
        this.emvParametersRequest = mock(EmvParametersRequest.class);
        when(this.mockApplicationRepository.retrieveEmvParameterRequest())
                .thenReturn(this.emvParametersRequest);
        doNothing().when(this.mockNotifier).notify(any());
        doNothing().when(this.mockNotifier).notify(any(), any());
        when(this.mockReadCardService.readCardDetails(any())).thenReturn(this.mockReadCardResponse);
        when(this.mockReadCardResponse.getResult()).thenReturn(Result.FAILURE);

        //Run
        Exception exception = assertThrows(
                PosException.class,
                () -> this.SUT.execute(this.mockPurchaseRequestModel)
        );

        //Verify
        Mockito.verify(this.mockNotifier).notify(any());
        String expectedMessage = PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD.getErrorMessage();
        assertEquals(expectedMessage, exception.getMessage());
    }

    /**
     * Case ReadCardResponse Card Details null
     */
    @Test
    public void executeOnReadCardResponseCardDetailsNull() {
        //Setup
        when(this.mockTerminalRepository.getSystemTraceAuditNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getInvoiceNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getReconciliationBatchNumber())
                .thenReturn("001");
        when(this.mockPurchaseRequestModel.getTransactionRequest())
                .thenReturn(prepareMockTransactionRequest());
        when(this.mockPurchaseRequestModel.getDeviceController())
                .thenReturn(this.mockeDeviceController);
        when(this.mockeDeviceController.isReady(any()))
                .thenReturn(new PosResponse(PosResult.SUCCESS));
        when(this.mockPurchaseRequestModel.getReadCardService())
                .thenReturn(this.mockReadCardService);
        doNothing().when(this.mockReadCardService).cleanUp();
        when(this.mockPurchaseRequestModel.getApplicationRepository())
                .thenReturn(this.mockApplicationRepository);
        this.emvParametersRequest = mock(EmvParametersRequest.class);
        when(this.mockApplicationRepository.retrieveEmvParameterRequest())
                .thenReturn(this.emvParametersRequest);
        doNothing().when(this.mockNotifier).notify(any());
        doNothing().when(this.mockNotifier).notify(any(), any());
        when(this.mockReadCardService.readCardDetails(any())).thenReturn(this.mockReadCardResponse);
        when(this.mockReadCardResponse.getResult()).thenReturn(Result.SUCCESS);
        when(this.mockReadCardResponse.getCardDetails()).thenReturn(null);

        //Run
        Exception exception = assertThrows(
                PosException.class,
                () -> this.SUT.execute(this.mockPurchaseRequestModel)
        );

        //Verify
        Mockito.verify(this.mockNotifier).notify(any());
        String expectedMessage = PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD.getErrorMessage();
        assertEquals(expectedMessage, exception.getMessage());
    }

    /**
     * Case Card Details with Track Two Data null
     */
    @Test
    public void executeOnCardDetailsWithNullTrackTwoData() {
        //Setup
        when(this.mockTerminalRepository.getSystemTraceAuditNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getInvoiceNumber()).thenReturn("000010");
        when(this.mockTerminalRepository.getReconciliationBatchNumber())
                .thenReturn("001");
        when(this.mockPurchaseRequestModel.getTransactionRequest())
                .thenReturn(prepareMockTransactionRequest());
        when(this.mockPurchaseRequestModel.getDeviceController())
                .thenReturn(this.mockeDeviceController);
        when(this.mockeDeviceController.isReady(any()))
                .thenReturn(new PosResponse(PosResult.SUCCESS));
        when(this.mockPurchaseRequestModel.getReadCardService())
                .thenReturn(this.mockReadCardService);
        doNothing().when(this.mockReadCardService).cleanUp();
        when(this.mockPurchaseRequestModel.getApplicationRepository())
                .thenReturn(this.mockApplicationRepository);
        this.emvParametersRequest = mock(EmvParametersRequest.class);
        when(this.mockApplicationRepository.retrieveEmvParameterRequest())
                .thenReturn(this.emvParametersRequest);
        doNothing().when(this.mockNotifier).notify(any());
        doNothing().when(this.mockNotifier).notify(any(), any());
        when(this.mockReadCardService.readCardDetails(any())).thenReturn(this.mockReadCardResponse);
        when(this.mockReadCardResponse.getResult()).thenReturn(Result.SUCCESS);
        when(this.mockReadCardResponse.getCardDetails()).thenReturn(prepareCardDetailsWithNullTrackTwoData());

        //Run
        Exception exception = assertThrows(
                PosException.class,
                () -> this.SUT.execute(this.mockPurchaseRequestModel)
        );

        //Verify
        Mockito.verify(this.mockNotifier).notify(any());
        String expectedMessage = PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD.getErrorMessage();
        assertEquals(expectedMessage, exception.getMessage());
    }

    private TransactionRequest prepareMockTransactionRequest() {
        TransactionRequest transactionRequest = new TransactionRequest(TransactionType.PURCHASE);
        transactionRequest.setAmount(105.00);
        return transactionRequest;
    }

    private TransactionRequest prepareMockInvalidAmountTransactionRequest() {
        TransactionRequest transactionRequest = new TransactionRequest(TransactionType.PURCHASE);
        transactionRequest.setAmount(-10.00);
        return transactionRequest;
    }

    private CardDetails prepareCardDetailsWithNullTrackTwoData() {
        CardDetails cardDetails = new CardDetails();
        cardDetails.setCardType(CardType.MAG);
        cardDetails.setPrimaryAccountNumber("4438300123287540");
        cardDetails.setTrackTwoData(null);
        return cardDetails;
    }

    private ReadCardResponse prepareReadCardResponseWithCardTypeIccOrPiccTrackTwoData() {
        CardDetails cardDetails = new CardDetails();
        cardDetails.setCardType(CardType.ICC);
        cardDetails.setPrimaryAccountNumber("4438300123287540");
        cardDetails.setTrackTwoData("4438300123287540D123456");
        return new ReadCardResponse(cardDetails, Result.SUCCESS, "Success");
    }


}