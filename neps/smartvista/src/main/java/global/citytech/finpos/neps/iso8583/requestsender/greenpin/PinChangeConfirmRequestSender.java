package global.citytech.finpos.neps.iso8583.requestsender.greenpin;

import global.citytech.finpos.neps.iso8583.FunctionCodeConfig;
import global.citytech.finpos.neps.iso8583.NepsMessagePayloadBuilder;
import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.PosEntryModeConfig;
import global.citytech.finpos.neps.iso8583.ProcessingCode;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.switches.FunctionCode;
import global.citytech.finposframework.utility.HelperUtils;

public class PinChangeConfirmRequestSender extends NepsMessageSenderTemplate {
    private final Logger logger = Logger.getLogger(PinChangeRequestSender.class.getName());
    protected PinChangeConfirmIsoRequest pinChangeConfirmIsoRequest;

    public PinChangeConfirmRequestSender(RequestContext context) {
        super(context);
        pinChangeConfirmIsoRequest = (PinChangeConfirmIsoRequest) context.getRequest();
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Authorization,
                        MTI.MessageFunction.Advice,
                        MTI.Originator.Acquirer);
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);
        MessagePayload.Builder builder = new NepsMessagePayloadBuilder(this.specInfo, msg);
        msg.addField(
                new DataElement(
                        DataElement.PRIMARY_ACCOUNT_NUMBER,
                        pinChangeConfirmIsoRequest.getPan())
        );

        msg.addField(
                new DataElement(
                        DataElement.AMOUNT,
                        HelperUtils.toISOAmount(pinChangeConfirmIsoRequest.getTransactionAmount(), 12))
        );
        msg.addField(
                new DataElement(
                        DataElement.LOCAL_TIME,
                        pinChangeConfirmIsoRequest.getLocalDateTime())
        );
        msg.addField(
                new DataElement(
                        DataElement.PAN_EXPIRE_DATE,
                        pinChangeConfirmIsoRequest.getExpiryDate())
        );


        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(pinChangeConfirmIsoRequest.getPosEntryMode()))
        );
        msg.addField(
                new DataElement(
                        DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE,
                        pinChangeConfirmIsoRequest.getPosConditionCode()
                )
        );

        msg.addField(
                new DataElement(
                        DataElement.TRACK_II_DATA,
                        pinChangeConfirmIsoRequest.getTrack2Data()
                )
        );


        if (pinChangeConfirmIsoRequest.getRrn() != null) {
            msg.addField(
                    new DataElement(DataElement.RETRIEVAL_REFERENCE_NUMBER,
                            pinChangeConfirmIsoRequest.getRrn()
                    )
            );
        }

        if (pinChangeConfirmIsoRequest.getEmvData() != null) {
            msg.addField(
                    new DataElement(DataElement.ICC_SYSTEM_RELATED_DATA,
                            pinChangeConfirmIsoRequest.getEmvData())
            );
        }

        //no primary mac data

        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());

        return builder.build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.GREEN_PIN.getCode();
    }

    @Override
    protected String getFunctionCode() {
        return FunctionCodeConfig.getValue(FunctionCode.GREEN_PIN);
    }
}
