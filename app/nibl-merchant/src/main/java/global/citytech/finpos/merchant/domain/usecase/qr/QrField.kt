package global.citytech.finpos.merchant.domain.usecase.qr

import global.citytech.finpos.merchant.domain.usecase.qr.NqrConstants.MAX_ADDITIONAL_DATA_LENGTH
import global.citytech.finpos.merchant.domain.usecase.qr.NqrConstants.MAX_MERCHANT_CITY_NAME_LENGTH
import global.citytech.finpos.merchant.domain.usecase.qr.NqrConstants.MAX_MERCHANT_NAME_LENGTH

enum class QrField(val id: String, val lengthConfig: LengthConfig, val description: String) {
    PAYLOAD_FORMAT_INDICATOR("00", LengthConfig(2, LengthType.FIXED), "Payload Format Indicator"),
    INITIATION_METHOD("01", LengthConfig(2, LengthType.FIXED), "Point of Initiation Method"),
    CYCLIC_REDUNDANCY_CHECK("63", LengthConfig(4, LengthType.FIXED), "Cyclic Redundancy Check"),
    GUID("00", LengthConfig(32, LengthType.LLVAR), "Globally Unique Identifier"),
    NCHL_MERCHANT_INFO("29", LengthConfig(99, LengthType.LLVAR), "NCHL Use"),
    MERCHANT_CATEGORY_CODE("52", LengthConfig(4, LengthType.FIXED), "Merchant Category Code"),
    COUNTRY_CODE("58", LengthConfig(2, LengthType.FIXED), "Country Code"),
    MERCHANT_NAME("59", LengthConfig(MAX_MERCHANT_NAME_LENGTH, LengthType.LLVAR), "Merchant Name"),
    MERCHANT_CITY("60", LengthConfig(MAX_MERCHANT_CITY_NAME_LENGTH, LengthType.LLVAR), "Merchant City"),
    ADDITIONAL_DATA("62", LengthConfig(MAX_ADDITIONAL_DATA_LENGTH, LengthType.LLVAR), "Additonal Data"),
    TRANSACTION_CURRENCY("53", LengthConfig(3, LengthType.FIXED), "Transaction Currency"),
    TRANSACTION_AMOUNT("54", LengthConfig(13, LengthType.LLVAR), "Transaction Amount"),
    ADDITIONAL_DATA_BILL_NUMBER("01", LengthConfig(25, LengthType.LLVAR), "Bill Number"),
    ADDITIONAL_DATA_TERMINAL_LABEL("07", LengthConfig(25, LengthType.LLVAR), "Terminal Label"),
    ADDITIONAL_DATA_MERCHANT_CODE("03", LengthConfig(20, LengthType.LLVAR), "Merchant Code"),
}

