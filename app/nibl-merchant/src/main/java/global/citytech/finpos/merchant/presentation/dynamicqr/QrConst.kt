package global.citytech.finpos.merchant.presentation.dynamicqr

object QrConst {
    const val FONEPAY_PROVIDER_CODE = "FONEPAY"
    const val FONEPAY_REQUEST_NUMBER = "FP"
    const val FONEPAY_REFERENCE_NUMBER = "REFP"
    const val FONEPAY_BILL_NUMBER = "BILL"
    const val QR_TYPE_NQR = "nQr"
    const val QR_TYPE_FONEPAY = "FonePay"
}