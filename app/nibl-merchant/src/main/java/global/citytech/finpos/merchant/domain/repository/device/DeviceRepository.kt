package global.citytech.finpos.merchant.domain.repository.device

import global.citytech.finpos.merchant.domain.model.device.DeviceResponse
import global.citytech.finpos.merchant.domain.model.device.LoadParameterRequest
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyInjectionRequest
import global.citytech.finposframework.utility.PosResponse
import io.reactivex.Observable

/**
 * Created by Rishav Chudal on 8/10/20.
 */
interface DeviceRepository {
    fun inject(keyInjectionRequest: KeyInjectionRequest): Observable<DeviceResponse>
    fun initSdk(): Observable<DeviceResponse>
    fun loadTerminalParameters(loadParameterRequest: LoadParameterRequest):
            Observable<DeviceResponse>
    fun closeUpIO()
    fun cardPresentInIccReader(): Observable<Boolean>
    fun enableHardwareButtons(): Observable<DeviceResponse>
    fun disableHardwareButtons(): Observable<DeviceResponse>
    fun readCardDetails(configurationItem: ConfigurationItem): Observable<ReadCardResponse>
    fun posReady(cardTypeList: MutableList<CardType>?): Observable<PosResponse>
}