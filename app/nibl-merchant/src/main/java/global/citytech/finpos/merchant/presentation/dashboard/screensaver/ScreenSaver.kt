package global.citytech.finpos.merchant.presentation.dashboard.screensaver

import android.content.Context
import android.content.DialogInterface
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.*
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.dashboard.temp.DashActivity
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finposframework.log.Logger
import kotlinx.android.synthetic.main.dialog_screen_saver.*

/**
 * Created by Rishav Chudal on 5/6/21.
 */
class ScreenSaver : DialogFragment() {
    val TAG = ScreenSaver::class.java.name
    private lateinit var listener: ScreenSaverDialogListener
    private val logger = Logger(ScreenSaver::class.java)
    private var bankLogo: String? = ""

    companion object {
        const val TAG = "ScreenSaver"
        const val BUNDLE_BANK_LOGO = "bank logo"

        fun newInstance(bankLogo: String): ScreenSaver {

            val screenSaverDialog = ScreenSaver()
            val bundle = Bundle()
            bundle.putString(BUNDLE_BANK_LOGO, bankLogo)
            screenSaverDialog.arguments = bundle
            return screenSaverDialog
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        hideDialogTitle()
        return inflater.inflate(R.layout.dialog_screen_saver, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundleData()

        val decodedString: ByteArray = global.citytech.common.Base64.decode(bankLogo)
        val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

        Glide.with(this)
            .load(decodedByte)
            .placeholder(R.drawable.ic_full_finpos_logo)
            .into(img_bank_logo)

        this.screen_saver_dialog.setOnClickListener {
            //dialog?.dismiss()
            listener.onTouch()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as ScreenSaverDialogListener
        } catch (e: ClassCastException) {
            printLog(TAG,"ClassCastException ::: Activity Should implement ScreenSaverDialogListener")
        }
    }

    override fun onResume() {
        super.onResume()
        setDialogLayoutParams()
    }

    private fun getBundleData() {
        val bundle = arguments
        checkIfBundleIsNullAndProceed(bundle)
    }

    private fun checkIfBundleIsNullAndProceed(bundle: Bundle?) {
        if (bundle != null) {
            getBankLogoFromBundleData(bundle)
        }
    }

    private fun getBankLogoFromBundleData(bundle: Bundle) {
        this.bankLogo = bundle.getString(
            BUNDLE_BANK_LOGO,
            ""
        )
    }

    private fun hideDialogTitle() {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
    }

    private fun setDialogLayoutParams() {
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog?.window?.attributes = layoutParams
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    override fun getTheme(): Int {
        return R.style.FullScreenDialog
    }

    interface ScreenSaverDialogListener {
        fun onTouch()
    }
}