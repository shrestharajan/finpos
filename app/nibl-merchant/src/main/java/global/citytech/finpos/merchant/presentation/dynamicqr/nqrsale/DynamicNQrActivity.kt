package global.citytech.finpos.merchant.presentation.dynamicqr.nqrsale

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.qr.NqrConstants
import global.citytech.finpos.merchant.domain.usecase.qr.nqr.NqrGenerateRequest
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.qr.QrHelper
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmationDialog
import global.citytech.finpos.merchant.presentation.dynamicqr.QrConst.QR_TYPE_NQR
import global.citytech.finpos.merchant.presentation.dynamicqr.QrType
import global.citytech.finpos.merchant.presentation.dynamicqr.base.DynamicQrBaseActivity
import global.citytech.finpos.merchant.utils.AppConstant
import global.citytech.finpos.merchant.utils.SoundUtils
import global.citytech.finpos.merchant.utils.VoiceMessageUtils
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion
import global.citytech.finposframework.utility.DateUtils
import kotlinx.android.synthetic.main.activity_dynamic_qr.*
import java.math.BigDecimal

class DynamicNQrActivity : DynamicQrBaseActivity(),
    TransactionConfirmationDialog.Listener  {

    private var transactionAmount: BigDecimal = BigDecimal.ZERO
    private var merchantName: String = ""
    private var merchantAddress: String = ""
    lateinit var qrHelper: QrHelper
    private var transactionConfirmationDialog: TransactionConfirmationDialog? = null

    companion object {
        const val REQUEST_CODE = 377
        const val EXTRA_TRANSACTION_AMOUNT = "extra.transaction.amount"
    }

    private var localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
        observeAppConfigurationLiveData()
        observeQrData()
        observeQrConfiguration()
        observeQrOperator()
        observeTransactionConfirmation()
        getViewModel().bindService()
    }

    private fun observeTransactionConfirmation() {
        getViewModel().transactionConfirmationLiveData.observe(this, Observer {
            showTransactionConfirmationDialog(it, getViewModel().updateTransactionConfirmation)
        })
    }

    private fun observeQrOperator() {
        getViewModel().qrOperator.observe(this, Observer { qrOperatorItem ->
            this.qrOperatorItem = qrOperatorItem
            val selectedQrConfiguration =
                getViewModel().qrConfigs.value?.filter { it -> it.qrOperator.equals(qrOperatorItem.id) }
                    ?.firstOrNull()
            this.qrConfiguration = selectedQrConfiguration
            updateViews(qrOperatorItem)
            generateQr()
        })
    }

    private fun observeQrConfiguration() {
        getViewModel().qrConfiguration.observe(this, Observer {
            qrOperatorItem = it.qrOperatorInfo
            qrConfiguration = it
            updateViews(qrOperatorItem)
            generateQr()
        })
    }

    private fun initData() {
        transactionAmount = intent.getSerializableExtra(EXTRA_TRANSACTION_AMOUNT) as BigDecimal
        qrHelper = QrHelper()
    }


    private fun observeAppConfigurationLiveData() {
        getViewModel().appConfiguration.observe(
            this,
            Observer {
                merchantName = it.merchants?.get(0)?.name!!
                merchantAddress = it.merchants?.get(0)?.address!!
                tv_merchant_name.setText(merchantName)
                tv_merchant_address.setText(merchantAddress)
                getViewModel().getQrOperators(QrType.NQR)
            }
        )
    }

    private fun showTransactionConfirmationDialog(it: TransactionConfirmation, update: Boolean) {
        if (transactionConfirmationDialog == null)
            transactionConfirmationDialog = TransactionConfirmationDialog(this, this, it)
        if (update)
            transactionConfirmationDialog!!.update(it, this)
        transactionConfirmationDialog!!.show()
    }

    private fun hideTransactionConfirmationDialog() {
        transactionConfirmationDialog?.hide()
    }

    private fun observeQrData() {
        getViewModel().qrData.observe(this, Observer {
            setQrCode(it)
        })
    }

    private fun generateQr() {
        getViewModel().generateNqr(prepareNqrGenerateRequest())
    }

    private fun setQrCode(qrData: String) {
        val qrBitmap = qrHelper.generateQrBitmap(qrData)
        if (qrBitmap != null) {
            iv_qr_content.setImageBitmap(qrBitmap)
            getViewModel().startTimer()
            getViewModel().checkQrPaymentStatus(getViewModel().nqrGenerateRequest!!, 2000, QR_TYPE_NQR)
        }
    }

    private fun prepareNqrGenerateRequest(): NqrGenerateRequest {
        val nqrGenerateRequest = NqrGenerateRequest(
            initiationMethod = NqrConstants.DYNAMIC_QR,
            acquirerCode = qrOperatorItem?.acquirerCode!!,
            merchantCode = qrConfiguration?.merchantId!!,
            merchantCategoryCode = qrConfiguration?.merchantCategoryCode!!,
            countryCode = AppConstant.COUNTRY_CODE_NP,
            merchantName = qrHelper.toNqrMerchantName(merchantName),
            merchantCity = qrHelper.toNqrMerchantCityName(merchantAddress),
            transactionCurrency = qrConfiguration?.currency!!,
            transactionAmount = transactionAmount.toString(),
            terminalId = qrConfiguration?.terminalId!!,
            billNumber = DateUtils.yyMMddDate().plus(getViewModel().getBillingInvoiceNumber())
        )
        Logger.getLogger("DynamicNQrActivity").log("GENERATED REQUEST >>> $nqrGenerateRequest")
        getViewModel().nqrGenerateRequest = nqrGenerateRequest
        return nqrGenerateRequest
    }

    override fun getViewModel(): DynamicNQrViewModel {
        return ViewModelProviders.of(this).get(DynamicNQrViewModel::class.java)
    }

    override fun onPositiveButtonClick() {
        getViewModel().printPaymentReceipt(
            ReceiptVersion.CUSTOMER_COPY
        )
    }

    override fun onNegativeButtonClick() {
        setResultAndFinish()
    }

    override fun setResultAndFinish() {
        val intent = Intent()
        intent.putExtra(EXTRA_QR_PAYMENT, getViewModel().qrPayment)
        setResult(RESULT_OK, intent)
        finish()
    }
}