package global.citytech.finpos.merchant.domain.model.reconciliation

import global.citytech.finposframework.hardware.io.printer.PrinterService
import global.citytech.finposframework.hardware.keymgmt.keys.KeyService
import global.citytech.finposframework.repositories.ApplicationRepository
import global.citytech.finposframework.repositories.ReconciliationRepository
import global.citytech.finposframework.repositories.TransactionRepository

/**
 * Created by Saurav Ghimire on 4/1/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


data class ReconciliationRequestEntity(
    var reconciliationRepository: ReconciliationRepository? = null,
    var printerService: PrinterService? = null,
    var applicationRepository: ApplicationRepository? = null,
    var transactionRepository: TransactionRepository? = null,
    var applicationPackageName: String? = null,
    val hardwareKeyService: KeyService? = null,
    val settlementActive: Boolean? = false
)
