package global.citytech.finpos.neps.iso8583.requestsender.cashadvance;

import global.citytech.finpos.neps.iso8583.requestsender.cardtypesenders.ManualISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.CardTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transactiontypesenders.CashAdvanceISOMessageUpdater;
import global.citytech.finposframework.iso8583.RequestContext;

/**
 * Created by Saurav Ghimire on 2/2/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class ManualCashAdvanceRequestSender extends CashAdvanceRequestSender {
    public ManualCashAdvanceRequestSender(RequestContext context) {
        super(context);
    }

    @Override
    protected String getClassName() {
        return ManualCashAdvanceRequestSender.class.getName();
    }

    @Override
    protected CardTypeISOMessageUpdater getCardTypeSender() {
        return new ManualISOMessageUpdater();
    }

}
