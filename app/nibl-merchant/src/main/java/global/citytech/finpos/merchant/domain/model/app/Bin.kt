package global.citytech.finpos.merchant.domain.model.app

/**
 * Created by Rishav Chudal on 12/20/21.
 */
data class Bin (
    val id: String = "",
    val bin: String = "",
    val title: String = "",
    val type: String = "",
    val bank: String = "",
    val active: Boolean = true
)

fun Bin.mapToFinPosFramework(): global.citytech.finposframework.usecases.transaction.data.Bin =
    global.citytech.finposframework.usecases.transaction.data.Bin.Builder()
        .setId(id)
        .setBin(bin)
        .setTitle(title)
        .setType(type)
        .setBank(bank)
        .setIsActive(active)
        .build()

fun List<Bin>.mapToFinPosFramework(): List<global.citytech.finposframework.usecases.transaction.data.Bin> =
    map { it.mapToFinPosFramework() }

