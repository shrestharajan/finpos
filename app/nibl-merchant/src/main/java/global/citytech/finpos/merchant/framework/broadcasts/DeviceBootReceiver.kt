package global.citytech.finpos.merchant.framework.broadcasts

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.presentation.alarms.SettlementAlarmSetter
import global.citytech.finposframework.log.Logger

/**
 * Created by Rishav Chudal on 5/17/21.
 */
class DeviceBootReceiver : BroadcastReceiver() {

    val logger: Logger = Logger.getLogger(DeviceBootReceiver::class.java.name)
    private lateinit var context: Context

    companion object {
        const val ACTION_BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED"
    }

    override fun onReceive(context: Context, intent: Intent) {
        this.logger.log(DeviceBootReceiver::class.java.name + " onReceive >>>>> Device Status  >>>> TURNED ON")
        this.logger.log("On Device Boot Broadcast received")
        this.context = context
        if (intent.action.equals(ACTION_BOOT_COMPLETED)) {
            this.logger.log("Setting alarm for force settlement after device boot...")
            setUpAlarmForForValidSettlementTime()
        }
    }

    private fun setUpAlarmForForValidSettlementTime() {
        val settlementTime = getStoredSettlementTime()
        this.logger.log("Settlement Time ::: ".plus(settlementTime))
        if (isValidSettlementTime(settlementTime)) {
            setRepeatingAlarm(settlementTime)
        }
    }

    private fun isValidSettlementTime(settlementTime: String): Boolean {
        return !settlementTime.isNullOrEmptyOrBlank()
    }

    private fun getStoredSettlementTime(): String =
        PreferenceManager.getSettlementTime(defaultValue = "")

    private fun setRepeatingAlarm(settlementTime: String) {
        val settlementAlarmSetter = SettlementAlarmSetter(this.context.applicationContext)
        settlementAlarmSetter.setRepeatingAlarm(settlementTime)
    }
}