package global.citytech.finpos.merchant.mqtt

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.dashboard.temp.DashActivity
import kotlinx.android.synthetic.main.activity_mqtt_notification_detail_page.*


class MqttNotificationDetailPage : AppCompatActivity() {

    companion object {
        const val NOTIFICATION_CONTENT = "notification_content"
        const val NOTIFICATION_TITLE = "notification_title"
        const val NOTIFICATION_TYPE = "notification_type"
    }

    private var notificationTitle: String? = null
    private var notificationContent: String? = null
    private var notificationType: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mqtt_notification_detail_page)

        getContentFromIntent()
        setContentToView()
    }

    private fun getContentFromIntent() {
        val bundle: Bundle? = intent.extras
        notificationTitle = bundle?.getString(NOTIFICATION_TITLE)
        notificationContent = bundle?.getString(NOTIFICATION_CONTENT)
        notificationType= bundle?.getString(NOTIFICATION_TYPE)
    }

    private fun setContentToView() {
        tv_notification_title.text = notificationTitle
        toolbarTitle.text = notificationType
        tv_notification_content.text = notificationContent

        iv_back.setOnClickListener(View.OnClickListener {
            finish()
        })

    }
}