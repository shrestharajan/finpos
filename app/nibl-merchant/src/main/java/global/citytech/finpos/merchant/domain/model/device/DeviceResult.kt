package global.citytech.finpos.merchant.domain.model.device

/**
 * Created by Rishav Chudal on 8/10/20.
 */
enum class DeviceResult(
    val code: Int,
    val message: String
) {
    NONE(0, "None"),
    FAILURE(-1, "Failure"),
    SUCCESS(1, "Success");
}