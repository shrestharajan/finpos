package global.citytech.finposframework.comm;

/** User: Surajchhetry Date: 2/20/20 Time: 2:13 PM */
public class ConnectionParam {

  private final HostInfo primaryHost;

  private final HostInfo secondaryHost;

  public ConnectionParam(HostInfo primaryHost, HostInfo secondaryHost) {
    this.primaryHost = primaryHost;
    this.secondaryHost = secondaryHost;
  }

  public HostInfo getPrimaryHost() {
    return primaryHost;
  }

  public HostInfo getSecondaryHost() {
    return secondaryHost;
  }

  public boolean hasSecondaryConnection() {
    return this.secondaryHost != null;
  }

  @Override
  public String toString() {
    return "ConnectionParam{" +
            "primaryHost=" + primaryHost +
            ", secondaryHost=" + secondaryHost +
            '}';
  }
}
