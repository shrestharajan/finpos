package global.citytech.finpos.merchant.presentation.qroperators

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.presentation.model.qroperator.QrOperatorItem
import global.citytech.finpos.merchant.presentation.model.setting.QrConfiguration
import global.citytech.finpos.merchant.utils.AppConstant

/**
 * @author sachin
 */
class QrOperatorsViewModel(val context: Application) : BaseAndroidViewModel(context) {
    val qrOperators by lazy { MutableLiveData<List<QrOperatorItem>>() }
    val qrConfigs by lazy { MutableLiveData<List<QrConfiguration>>() }

    var localDataUseCase: LocalDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    fun getQrOperators() {
        val terminalSettingsResponse = this.localDataUseCase.getTerminalSetting()
        val qrOperatorList = ArrayList<QrOperatorItem>()
        qrConfigs.value = terminalSettingsResponse.qrConfigs
        if (!terminalSettingsResponse.qrCode.isNullOrEmptyOrBlank()) {
            qrOperatorList.add(
                QrOperatorItem(
                    id = AppConstant.FONE_PAY_ID,
                    name = "FonePay QR",
                    logo = "",
                    acquirerCode = "FONE PAY CODE",
                    host = "",
                    active = true,
                    issuers = emptyList()
                )
            )
        }
        terminalSettingsResponse.qrConfigs
            ?.forEach { it ->
                run {
                    if (it.enableStaticQr && it.qrOperatorInfo.active) {
                        qrOperatorList.add(it.qrOperatorInfo)
                    }
                }
            }
        qrOperators.value = qrOperatorList
    }

}