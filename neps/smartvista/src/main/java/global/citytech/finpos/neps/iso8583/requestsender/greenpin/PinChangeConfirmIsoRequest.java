package global.citytech.finpos.neps.iso8583.requestsender.greenpin;

import java.math.BigDecimal;

import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.voidsale.VoidSaleIsoRequest;
import global.citytech.finposframework.switches.PosEntryMode;

public class PinChangeConfirmIsoRequest extends TransactionIsoRequest {
    private String stan;
    private PosEntryMode posEntryMode;
    private String posConditionCode;
    private String rrn;

    public String getStan() {
        return stan;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public String getRrn() {
        return rrn;
    }

    public static final class Builder {
        private String pan;
        private BigDecimal transactionAmount;
        private String stan;
        private String localDateTime;
        private String expirationDate;
        private PosEntryMode posEntryMode;
        private String posConditionCode;
        private String track2Data;
        private String rrn;
        private String emvData;

        private Builder() {

        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public Builder localDateTime(String localDatetime) {
            this.localDateTime = localDatetime;
            return this;
        }

        public Builder stan(String stan) {
            this.stan = stan;
            return this;
        }


        public Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public Builder expirationDate(String expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }


        public Builder posEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public Builder posConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public Builder rrn(String rrn) {
            this.rrn = rrn;
            return this;
        }

        public Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public PinChangeConfirmIsoRequest build() {
            PinChangeConfirmIsoRequest pinChangeConfirmIsoRequest = new PinChangeConfirmIsoRequest();
            pinChangeConfirmIsoRequest.pan = this.pan;
            pinChangeConfirmIsoRequest.transactionAmount = this.transactionAmount;
            pinChangeConfirmIsoRequest.stan = this.stan;
            pinChangeConfirmIsoRequest.localDateTime = this.localDateTime;
            pinChangeConfirmIsoRequest.expiryDate = this.expirationDate;
            pinChangeConfirmIsoRequest.posEntryMode = this.posEntryMode;
            pinChangeConfirmIsoRequest.posConditionCode = this.posConditionCode;
            pinChangeConfirmIsoRequest.track2Data = this.track2Data;
            pinChangeConfirmIsoRequest.rrn = this.rrn;
            pinChangeConfirmIsoRequest.emvData = this.emvData;
            return pinChangeConfirmIsoRequest;
        }

    }
}
