package global.citytech.finposframework.comm;

/**
 * Created by Unique Shakya on 10/4/2021.
 */
public class TlsCertificate {

    private String certificate;
    private String tlsVersion;
    private String certificateKey;
    private String certificateFactoryType;
    private String expiryDate;

    public TlsCertificate(Builder builder) {
        this.certificate = builder.certificate;
        this.tlsVersion = builder.tlsVersion;
        this.certificateKey = builder.certificateKey;
        this.certificateFactoryType = builder.certificateFactoryType;
        this.expiryDate = builder.expiryDate;
    }

    public String getCertificate() {
        return certificate;
    }

    public String getTlsVersion() {
        return tlsVersion;
    }

    public String getCertificateKey() {
        return certificateKey;
    }

    public String getCertificateFactoryType() {
        return certificateFactoryType;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    @Override
    public String toString() {
        return "TlsCertificate{" +
                "certificate='" + certificate + '\'' +
                ", tlsVersion='" + tlsVersion + '\'' +
                ", certificateKey='" + certificateKey + '\'' +
                ", certificateFactoryType='" + certificateFactoryType + '\'' +
                ", expiryDate='" + expiryDate + '\'' +
                '}';
    }

    public static final class Builder {
        private String certificate;
        private String tlsVersion;
        private String certificateKey;
        private String certificateFactoryType;
        private String expiryDate;

        public Builder(){
        }

        public static Builder createDefaultBuilder(){
            return new Builder();
        }

        public Builder withCertificate(String certificate) {
            this.certificate = certificate;
            return this;
        }

        public Builder withTlsVersion(String tlsVersion) {
            this.tlsVersion = tlsVersion;
            return this;
        }

        public Builder withCertificateKey(String certificateKey) {
            this.certificateKey = certificateKey;
            return this;
        }

        public Builder withCertificateFactoryType(String certificateFactoryType) {
            this.certificateFactoryType = certificateFactoryType;
            return this;
        }

        public Builder withExpiryDate(String expiryDate) {
            this.expiryDate = expiryDate;
            return this;
        }

        public TlsCertificate build(){
            return new TlsCertificate(this);
        }
    }
}
