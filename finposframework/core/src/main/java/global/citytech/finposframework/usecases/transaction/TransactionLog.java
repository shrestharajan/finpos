package global.citytech.finposframework.usecases.transaction;

import java.math.BigDecimal;

import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;

/**
 * Created by Rishav Chudal on 9/22/20.
 */
public class TransactionLog {
    private String stan;
    private String merchantId;
    private String terminalId;
    private String invoiceNumber;
    private String rrn;
    private String authCode;
    private TransactionType transactionType;
    private TransactionType originalTransactionType;
    private BigDecimal transactionAmount;
    private BigDecimal originalTransactionAmount;
    private String transactionDate;
    private String transactionTime;
    private String transactionStatus;
    private PosEntryMode posEntryMode;
    private PosEntryMode originalPosEntryMode;
    private String posConditionCode;
    private String originalPosConditionCode;
    private String reconcileStatus;
    private String reconcileTime;
    private String reconcileDate;
    private String reconcileBatchNo;
    private ReadCardResponse readCardResponse;
    private String processingCode;
    private String responseCode;
    private boolean authorizationCompleted;
    private String originalTransactionReferenceNumber;
    private ReceiptLog receiptLog;
    private boolean transactionVoided;
    private boolean tipAdjusted;
    private boolean isEmiTransaction;
    private String emiInfo;
    private String currencyCode;
    private String vatInfo;

    private TransactionLog() {

    }

    public String getProcessingCode() {
        return processingCode;
    }

    public ReceiptLog getReceiptLog() {
        return receiptLog;
    }

    public String getStan() {
        return stan;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public ReadCardResponse getReadCardResponse() {
        return readCardResponse;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public String getRrn() {
        return rrn;
    }

    public String getAuthCode() {
        return authCode;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public TransactionType getOriginalTransactionType() {
        return originalTransactionType;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public BigDecimal getOriginalTransactionAmount() {
        return originalTransactionAmount;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public PosEntryMode getOriginalPosEntryMode() {
        return originalPosEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public String getOriginalPosConditionCode() {
        return originalPosConditionCode;
    }

    public String getReconcileStatus() {
        return reconcileStatus;
    }

    public String getReconcileTime() {
        return reconcileTime;
    }

    public String getReconcileDate() {
        return reconcileDate;
    }

    public String getReconcileBatchNo() {
        return reconcileBatchNo;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public boolean isAuthorizationCompleted() {
        return authorizationCompleted;
    }

    public String getOriginalTransactionReferenceNumber() {
        return originalTransactionReferenceNumber;
    }

    public boolean isTransactionVoided() {
        return transactionVoided;
    }

    public boolean isTipAdjusted() {
        return tipAdjusted;
    }

    public boolean isEmiTransaction() {
        return isEmiTransaction;
    }

    public String getEmiInfo() {
        return emiInfo;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getVatInfo() {
        return vatInfo;
    }

    public static final class Builder {
        private String stan;
        private String merchantId;
        private String terminalId;
        private String invoiceNumber;
        private String rrn;
        private String authCode;
        private TransactionType transactionType;
        private TransactionType originalTransactionType;
        private BigDecimal transactionAmount;
        private BigDecimal originalTransactionAmount;
        private String transactionDate;
        private String transactionTime;
        private String transactionStatus;
        private PosEntryMode posEntryMode;
        private PosEntryMode originalPosEntryMode;
        private String posConditionCode;
        private String originalPosConditionCode;
        private String reconcileStatus;
        private String reconcileTime;
        private String reconcileDate;
        private String reconcileBatchNo;
        private ReadCardResponse readCardResponse;
        private String responseCode;
        private boolean authorizationCompleted;
        private String originalTransactionReferenceNumber;
        private ReceiptLog receiptLog;
        private boolean transactionVoided;
        private String processingCode;
        private boolean tipAdjusted;
        private boolean isEmiTransaction;
        private String emiInfo;
        private String currencyCode;
        private String vatInfo;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder withTerminalId(String terminalId) {
            this.terminalId = terminalId;
            return this;
        }

        public Builder withMerchantId(String merchantId) {
            this.merchantId = merchantId;
            return this;
        }

        public Builder withInvoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }

        public Builder withRrn(String rrn) {
            this.rrn = rrn;
            return this;
        }

        public Builder withAuthCode(String authCode) {
            this.authCode = authCode;
            return this;
        }

        public Builder withTransactionType(TransactionType transactionType) {
            this.transactionType = transactionType;
            return this;
        }

        public Builder withOriginalTransactionType(TransactionType originalTransactionType) {
            this.originalTransactionType = originalTransactionType;
            return this;
        }

        public Builder withTransactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public Builder withOriginalTransactionAmount(BigDecimal originalTransactionAmount) {
            this.originalTransactionAmount = originalTransactionAmount;
            return this;
        }

        public Builder withTransactionDate(String transactionDate) {
            this.transactionDate = transactionDate;
            return this;
        }

        public Builder withTransactionTime(String transactionTime) {
            this.transactionTime = transactionTime;
            return this;
        }

        public Builder withTransactionStatus(String transactionStatus) {
            this.transactionStatus = transactionStatus;
            return this;
        }

        public Builder withPosEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public Builder withOriginalPosEntryMode(PosEntryMode originalPosEntryMode) {
            this.originalPosEntryMode = originalPosEntryMode;
            return this;
        }

        public Builder withPosConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public Builder withOriginalPosConditionCode(String originalPosConditionCode) {
            this.originalPosConditionCode = originalPosConditionCode;
            return this;
        }

        public Builder withReconcileStatus(String reconcileStatus) {
            this.reconcileStatus = reconcileStatus;
            return this;
        }

        public Builder withReconcileTime(String reconcileTime) {
            this.reconcileTime = reconcileTime;
            return this;
        }

        public Builder withReconcileDate(String reconcileDate) {
            this.reconcileDate = reconcileDate;
            return this;
        }

        public Builder withReconcileBatchNo(String reconcileBatchNo) {
            this.reconcileBatchNo = reconcileBatchNo;
            return this;
        }

        public Builder withProcessingCode(String processingCode) {
            this.processingCode = processingCode;
            return this;
        }

        public Builder withReadCardResponse(ReadCardResponse readCardResponse) {
            this.readCardResponse = readCardResponse;
            return this;
        }

        public Builder withResponseCode(String responseCode) {
            this.responseCode = responseCode;
            return this;
        }

        public Builder withAuthorizationCompleted(boolean authorizationCompleted) {
            this.authorizationCompleted = authorizationCompleted;
            return this;
        }

        public Builder withOriginalTransactionReferenceNumber(String originalTransactionReferenceNumber) {
            this.originalTransactionReferenceNumber = originalTransactionReferenceNumber;
            return this;
        }

        public Builder withReceiptLog(ReceiptLog receiptLog) {
            this.receiptLog = receiptLog;
            return this;
        }

        public Builder withTransactionVoided(boolean transactionVoided) {
            this.transactionVoided = transactionVoided;
            return this;
        }

        public Builder withTipAdjusted(boolean tipAdjusted) {
            this.tipAdjusted = tipAdjusted;
            return this;
        }

        public Builder withIsEmiTransaction(boolean isEmiTransaction) {
            this.isEmiTransaction = isEmiTransaction;
            return this;
        }

        public Builder withEmiInfo(String emiInfo) {
            this.emiInfo = emiInfo;
            return this;
        }

        public Builder withCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder withVatInfo(String vatInfo) {
            this.vatInfo = vatInfo;
            return this;
        }

        public TransactionLog build() {
            TransactionLog transactionLog = new TransactionLog();
            transactionLog.stan = this.stan;
            transactionLog.terminalId = this.terminalId;
            transactionLog.merchantId = this.merchantId;
            transactionLog.rrn = this.rrn;
            transactionLog.invoiceNumber = this.invoiceNumber;
            transactionLog.originalPosEntryMode = this.originalPosEntryMode;
            transactionLog.reconcileBatchNo = this.reconcileBatchNo;
            transactionLog.reconcileDate = this.reconcileDate;
            transactionLog.originalTransactionType = this.originalTransactionType;
            transactionLog.transactionDate = this.transactionDate;
            transactionLog.reconcileStatus = this.reconcileStatus;
            transactionLog.transactionStatus = this.transactionStatus;
            transactionLog.originalTransactionAmount = this.originalTransactionAmount;
            transactionLog.originalPosConditionCode = this.originalPosConditionCode;
            transactionLog.transactionType = this.transactionType;
            transactionLog.transactionTime = this.transactionTime;
            transactionLog.posConditionCode = this.posConditionCode;
            transactionLog.reconcileTime = this.reconcileTime;
            transactionLog.transactionAmount = this.transactionAmount;
            transactionLog.authCode = this.authCode;
            transactionLog.posEntryMode = this.posEntryMode;
            transactionLog.readCardResponse = this.readCardResponse;
            transactionLog.processingCode = this.processingCode;
            transactionLog.responseCode = this.responseCode;
            transactionLog.authorizationCompleted = this.authorizationCompleted;
            transactionLog.originalTransactionReferenceNumber = this.originalTransactionReferenceNumber;
            transactionLog.receiptLog = this.receiptLog;
            transactionLog.transactionVoided = this.transactionVoided;
            transactionLog.tipAdjusted = this.tipAdjusted;
            transactionLog.isEmiTransaction = this.isEmiTransaction;
            transactionLog.emiInfo = this.emiInfo;
            transactionLog.currencyCode = this.currencyCode;
            transactionLog.vatInfo = this.vatInfo;
            return transactionLog;
        }
    }
}
