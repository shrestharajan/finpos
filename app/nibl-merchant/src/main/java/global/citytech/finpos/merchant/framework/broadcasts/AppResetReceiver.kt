package global.citytech.finpos.merchant.framework.broadcasts

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import global.citytech.common.Constants
import global.citytech.common.data.PosCallback
import global.citytech.common.data.PosMessage
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finposframework.log.Logger

class AppResetReceiver : BroadcastReceiver() {

    private val logger = Logger(SettlementReceiver::class.java)
    lateinit var localDataUseCase: LocalDataUseCase

    override fun onReceive(p0: Context?, p1: Intent?) {

        val isTerminalConfigured = PreferenceManager.getTerminalConfigPref(false)
        if (isTerminalConfigured) {
            PreferenceManager.setAppResetActive(true)
            if (NiblMerchant.INSTANCE.isDashActivityVisible()) {
                logger.log("::: DASH ACTIVITY VISIBLE ::: START RESET >>>")
                val intent = Intent(Constants.INTENT_ACTION_POS_CALLBACK)
                intent.putExtra(

                    Constants.KEY_BROADCAST_MESSAGE,
                    PosCallback(PosMessage.POS_MESSAGE_START_RESET)
                )
                LocalBroadcastManager.getInstance(NiblMerchant.INSTANCE).sendBroadcast(intent)
            }
        } else {
            AppUtility.sendAppResetBroadcastToTMSPlatform(p0!!)
        }
    }
}