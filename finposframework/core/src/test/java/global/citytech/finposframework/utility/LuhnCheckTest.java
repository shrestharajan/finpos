package global.citytech.finposframework.utility;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LuhnCheck.class})
public class LuhnCheckTest {
    private LuhnCheck luhnCheckUnderTest;

    @Before
    public void setUp() {
        luhnCheckUnderTest = spy(LuhnCheck.getInstance());
    }

    @Test
    public void isValidCardNumber_Empty_Null() {
        // Setup

        // Run the test
        final boolean result = luhnCheckUnderTest.isValidCardNumber("");

        // Verify the results
        assertFalse(result);
    }

    @Test
    public void isValidCardNumber_NotEmptyMatchingLastDigits_True() {
        // Setup
        doReturn(2).when(luhnCheckUnderTest).getCheckNumber(any());

        // Run the test
        final boolean result = luhnCheckUnderTest.isValidCardNumber("123456789012");

        // Verify the results
        assertTrue(result);
    }

    @Test
    public void isValidCardNumber_NotEmptyNotMatchingLastDigits_False() {
        // Setup
        doReturn(3).when(luhnCheckUnderTest).getCheckNumber(any());

        // Run the test
        final boolean result = luhnCheckUnderTest.isValidCardNumber("123456789012");

        // Verify the results
        assertFalse(result);
    }


    @Test
    public void getCheckNumberTest() {
        // Setup

        // Run the test
        final int result = luhnCheckUnderTest.getCheckNumber("428331010430281");

        // Verify the results
        assertEquals(8, result);
    }

    @Test
    public void calulateTest() {

        // Run the test
        final int result = luhnCheckUnderTest.calculate(765);

        // Verify the results
        assertEquals(18, result);
    }

    @Test
    public void getInstanceTest() {
        // Setup

        // Run the test
        final LuhnCheck result = LuhnCheck.getInstance();

        // Verify the results
        assertNotNull(result);
    }

}