package global.citytech.finpos.processor.nibl.transaction.refund;

import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finpos.nibl.iso8583.requestsender.refund.IccRefundRequestSender;
import global.citytech.finpos.nibl.iso8583.requestsender.refund.MagRefundRequestSender;
import global.citytech.finpos.nibl.iso8583.requestsender.refund.PICCRefundRequestSender;
import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finpos.processor.nibl.transaction.template.CardTransactionUseCase;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.utility.IsoMessageUtils;

/**
 * Created by Unique Shakya on 3/16/2021.
 */
public class CardRefundUseCase extends CardTransactionUseCase {

    public CardRefundUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    protected String getTransactionTypeClassName() {
        return CardRefundUseCase.class.getSimpleName();
    }



    @Override
    protected NIBLMessageSenderTemplate getRequestSender(RequestContext context) {
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case MAG:
                return new MagRefundRequestSender(new NIBLSpecInfo(), context);
            case ICC:
                return new IccRefundRequestSender(new NIBLSpecInfo(), context);
            case PICC:
                return new PICCRefundRequestSender(new NIBLSpecInfo(), context);
            default:
                throw new IllegalArgumentException("Unknown card type");
        }
    }

    @Override
    protected String getOriginalPosConditionCode() {
        return null;
    }

    @Override
    protected PosEntryMode getOriginalPosEntryMode() {
        return null;
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.REFUND.getCode();
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy, String miniStatementData) {
        return null;
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy) {
        return RefundResponseModel.Builder.createDefaultBuilder()
                .isApproved(isApproved)
                .message(this.retrieveMessage(isoMessageResponse, isApproved))
                .debugRequestMessage(isoMessageResponse.getDebugRequestString())
                .debugResponseMessage(isoMessageResponse.getDebugResponseString())
                .shouldPrintCustomerCopy(shouldPrintCustomerCopy)
                .stan(this.stan)
                .build();
    }

    @Override
    protected TransactionResponseModel prepareDeclinedTransactionResponse(String message,
                                                                          boolean shouldPrintReceipt) {
        return RefundResponseModel.Builder.createDefaultBuilder()
                .isApproved(false)
                .message(message)
                .stan(this.stan)
                .shouldPrintCustomerCopy(shouldPrintReceipt)
                .build();
    }

    @Override
    protected String getOriginalTransactionReferenceNumber() {
        return this.transactionIsoRequest.getOriginalRetrievalReferenceNumber();
    }

    @Override
    protected boolean isAuthorizationCompleted() {
        return true;
    }

    @Override
    protected void updateTransactionLogOfOriginalTransaction(boolean isApproved) {
        // do nothing
    }

    @Override
    protected boolean shouldIncrementInvoiceNumber(boolean isApproved) {
        return isApproved;
    }

    @Override
    protected TransactionLog retrieveTransactionLogToStore(boolean isApproved, IsoMessageResponse isoMessageResponse) {
        return TransactionLog.Builder.newInstance()
                .withStan(stan)
                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
                .withInvoiceNumber(invoiceNumber)
                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
                .withTransactionType(TransactionType.REFUND)
                .withOriginalTransactionType(TransactionType.REFUND)
                .withTransactionAmount(transactionRequest.getAmount())
                .withOriginalTransactionAmount(transactionRequest.getAmount())
                .withTransactionDate(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.LOCAL_DATE))
                .withTransactionTime(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.LOCAL_TIME))
                .withTransactionStatus(isApproved ? APPROVED : DECLINED)
                .withPosEntryMode(this.transactionIsoRequest.getPosEntryMode())
                .withOriginalPosEntryMode(this.getOriginalPosEntryMode())
                .withPosConditionCode(this.transactionIsoRequest.getPosConditionCode())
                .withOriginalPosConditionCode(this.getOriginalPosConditionCode())
                .withReconcileStatus("")
                .withReconcileTime("")
                .withReconcileDate("")
                .withReconcileBatchNo(batchNumber)
                .withReadCardResponse(readCardResponse)
                .withOriginalTransactionReferenceNumber(this.transactionIsoRequest.getOriginalRetrievalReferenceNumber())
                .withReceiptLog(this.receiptLog)
                .withAuthorizationCompleted(true)
                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                .withTransactionVoided(false)
                .withTipAdjusted(false)
                .withCurrencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .withVatInfo(transactionRequest.getVatInfo())
                .build();
    }
}
