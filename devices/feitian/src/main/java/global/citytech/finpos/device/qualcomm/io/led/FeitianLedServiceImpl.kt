package global.citytech.finpos.device.qualcomm.io.led

import android.content.Context
import com.ftpos.library.smartpos.errcode.ErrCode
import com.ftpos.library.smartpos.led.Led
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.LED_ACTION_OFF
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.LED_ACTION_ON
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.led.LedAction
import global.citytech.finposframework.hardware.io.led.LedLight
import global.citytech.finposframework.hardware.io.led.LedLight.*
import global.citytech.finposframework.hardware.io.led.LedRequest
import global.citytech.finposframework.hardware.io.led.LedService
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.LED_ACTION_FAILURE
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.LED_ACTION_SUCCESS

/**
 * Created by Rishav Chudal on 4/15/21.
 */
class FeitianLedServiceImpl(val context: Context): LedService, FeitianLogger.Listener {

    private lateinit var response: DeviceResponse
    private val led = Led.getInstance(context.applicationContext)
    private val logger = FeitianLogger(this)

    override fun doTurnLedWith(request: LedRequest): DeviceResponse {
        this.response = DeviceResponse(
            Result.FAILURE,
            LED_ACTION_FAILURE
        )
        val result = analyzeLEDAction(request)
        if (result == ErrCode.ERR_SUCCESS) {
            this.response = DeviceResponse(
                Result.SUCCESS,
                LED_ACTION_SUCCESS
            )
        }
        return this.response
    }

    private fun analyzeLEDAction(request: LedRequest): Int {
        return if (request.ledAction == LedAction.ON) {
            implementActionOnLED(request.ledLight, LED_ACTION_ON)
        } else {
            implementActionOnLED(request.ledLight, LED_ACTION_OFF)
        }
    }

    private fun implementActionOnLED(ledLight: LedLight, action: Boolean): Int {
        this.logger.logWithClassName("LED Light ::: ".plus(ledLight))
        this.logger.logWithClassName("Action (true is On, false is Off) ::: ".plus(action))
        return when (ledLight) {
            ALL -> this.led.ledStatus(
                action,
                action,
                action,
                action
            )

            RED -> this.led.ledStatus(
                action,
                LED_ACTION_OFF,
                LED_ACTION_OFF,
                LED_ACTION_OFF
            )

            YELLOW -> this.led.ledStatus(
                LED_ACTION_OFF,
                action,
                LED_ACTION_OFF,
                LED_ACTION_OFF
            )

            GREEN -> this.led.ledStatus(
                LED_ACTION_OFF,
                LED_ACTION_OFF,
                action,
                LED_ACTION_OFF
            )

            BLUE -> this.led.ledStatus(
                LED_ACTION_OFF,
                LED_ACTION_OFF,
                LED_ACTION_OFF,
                action
            )
        }
    }

    override fun getClassName() = FeitianLedServiceImpl::class.java.simpleName
}