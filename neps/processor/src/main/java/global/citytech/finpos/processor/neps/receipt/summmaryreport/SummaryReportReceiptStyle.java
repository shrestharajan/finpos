package global.citytech.finpos.processor.neps.receipt.summmaryreport;

import global.citytech.finposframework.hardware.io.printer.Style;

/**
 * Created by Saurav Ghimire on 3/30/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public enum  SummaryReportReceiptStyle {
    RETAILER_NAME(Style.Align.CENTER, Style.FontSize.XLARGE, 1, true, true, false, false),
    RETAILER_ADDRESS(Style.Align.CENTER, Style.FontSize.SMALL, 1, false, false, false, false),
    START_DATE_TIME(Style.Align.LEFT, Style.FontSize.SMALL, 2, true, false, false, false),
    TID_MID(Style.Align.LEFT, Style.FontSize.SMALL, 2, true, false, false, false),
    BATCH_NUMBER(Style.Align.LEFT, Style.FontSize.SMALL, 2, true, false, false, false),
    HOST(Style.Align.LEFT, Style.FontSize.SMALL, 2, true, false, false, false),
    HEADER(Style.Align.LEFT, Style.FontSize.LARGE, 1, true, true, false, false),
    CARD_SCHEME(Style.Align.LEFT, Style.FontSize.SMALL, 1, true, false, false, false),
    REPORT_LABEL(Style.Align.LEFT, Style.FontSize.SMALL, 4, true, false, false, false),
    REPORT_VALUE(Style.Align.LEFT, Style.FontSize.SMALL, 4, true, false, false, false),
    LINE_BREAK(Style.Align.CENTER, Style.FontSize.NORMAL, 1, true, true, false, false),
    DIVIDER(Style.Align.CENTER, Style.FontSize.XSMALL, 1, false, false, false, false),
    ;

    private Style.Align alignment;
    private Style.FontSize fontSize;
    private int columns;
    private boolean allCaps;
    private boolean bold;
    private boolean italic;
    private boolean underline;

    SummaryReportReceiptStyle(Style.Align alignment, Style.FontSize fontSize, int columns, boolean allCaps, boolean bold, boolean italic, boolean underline) {
        this.alignment = alignment;
        this.fontSize = fontSize;
        this.columns = columns;
        this.allCaps = allCaps;
        this.bold = bold;
        this.italic = italic;
        this.underline = underline;
    }

    public Style getStyle() {
        return new Style.Builder()
                .alignment(this.alignment)
                .allCaps(this.allCaps)
                .bold(this.bold)
                .fontSize(this.fontSize)
                .italic(this.italic)
                .multipleAlignment(this.isMultipleAlignment())
                .underline(this.underline)
                .build();
    }

    public int[] columnWidths() {
        int num = 100 / this.columns;
        int[] columnWidths = new int[this.columns];
        for (int i = 0; i < this.columns; i++) {
            columnWidths[i] = num;
        }
        return columnWidths;
    }

    private boolean isMultipleAlignment() {
        return this.columns > 1;
    }
}
