package global.citytech.finpos.merchant.presentation.alertdialogs

class TransactionConfirmation constructor(builder: Builder) {

    var imageId: Int = 0
    var title: String
    var amount: String? = null
    var message: String
    var positiveLabel: String
    var negativeLabel: String
    var qrImageString: String? = null
    var transactionStatus: String? = null
    var approve:Boolean = false


    init {
        this.title = builder.title
        this.amount = builder.amount
        this.message = builder.message
        this.positiveLabel = builder.positiveLabel
        this.negativeLabel = builder.negativeLabel
        this.imageId = builder.imageId
        this.qrImageString = builder.qrImageString!!
        this.transactionStatus = builder.transactionStatus!!
        this.approve = builder.approve
    }

    class Builder {

        lateinit var title: String
        var amount: String? = null
        lateinit var message: String
        lateinit var positiveLabel: String
        lateinit var negativeLabel: String
        var imageId: Int = 0
        var qrImageString: String? = null
        var transactionStatus: String? = null
        var approve:Boolean = false

        fun title(title: String) = apply { this.title = title }

        fun isApprove(approve:Boolean) = apply { this.approve = approve }
        fun amount(amount: String) = apply { this.amount = amount }
        fun message(message: String) = apply { this.message = message }
        fun positiveLabel(positiveLabel: String) = apply { this.positiveLabel = positiveLabel }
        fun negativeLabel(negativeLabel: String) = apply { this.negativeLabel = negativeLabel }
        fun imageId(imageId: Int) = apply { this.imageId = imageId }
        fun qrImageString(qrImageString: String) = apply { this.qrImageString = qrImageString }
        fun transactionStatus(transactionStatus: String) = apply { this.transactionStatus = transactionStatus }

        fun build(): TransactionConfirmation {
            return TransactionConfirmation(this)
        }
    }

    override fun toString(): String {
        return "TransactionConfirmation(imageId=$imageId, title='$title',amount='$amount', message='$message', positiveLabel='$positiveLabel', negativeLabel='$negativeLabel', approve= '$approve')"
    }
}