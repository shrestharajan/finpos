package global.citytech.finposframework.iso8583;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finposframework.utility.EncodingUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/** User: Surajchhetry Date: 2/24/20 Time: 3:10 PM */
public class MessageParserTest {
  private Logger logger = Logger.getLogger(MessageParserTest.class.getName());

  byte[] sampleResponseWithOutHeaderLength =
      EncodingUtils.hex2Bytes(
          "30383130222000000A80080039393030303030323235313230363232303030303031303030323838393330343936303030343132333430323731364543D34E66E11D4FAEADCED388EF080F");

 // @Test
  public void parseMTITest() {
    MessageParser parser = new DefaultMessageParser(new MockSpecInfo());
    Iso8583Msg iso8583Msg = parser.parse(sampleResponseWithOutHeaderLength);
    assertNotNull(iso8583Msg);
    assertEquals("0810", iso8583Msg.getMti().toString());
  }
/*
  @Test
  public void parseMessageTest() {
    MessageParser parser = new DefaultMessageParser(new MockSpecInfo());
    Iso8583Msg iso8583Msg = parser.parse(sampleResponseWithOutHeaderLength);
    assertNotNull(iso8583Msg);
    assertEquals("0810", iso8583Msg.getMti().toString());
    assertEquals(
        "4543D34E66E11D4FAEADCED388EF080F", iso8583Msg.getDataElementByIndex(53).getValue());
  }*/
}
