package global.citytech.finpos.merchant.presentation.dashboard.billing

enum class BillingBroadcastMessageType {
    BILLING_SYSTEM_CONNECTED,
    BILLING_SYSTEM_DISCONNECTED,
    PURCHASE,
    VOID,
    CANCEL_TRANSACTION
}