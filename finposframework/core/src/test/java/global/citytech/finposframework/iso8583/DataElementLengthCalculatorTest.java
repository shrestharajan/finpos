package global.citytech.finposframework.iso8583;

import org.junit.Test;

import java.io.IOException;
import global.citytech.finposframework.log.Logger;;

import global.citytech.finposframework.utility.HexDump;

/***
 * @author Surajchhetry
 * @since 1.0
 */
public class DataElementLengthCalculatorTest {

    private Logger logger = Logger.getLogger(DataElementLengthCalculatorTest.class.getName());

    @Test
    public void calculateLengthTest() throws IOException {
        MockSpecInfo specInfo = new MockSpecInfo();
        DataElementConfig deConfig = specInfo.getDEConfig().getByFieldNumber(35);
        DataElement track2 = new DataElement(35, "4249720010000129d24116261861919299999f");
        byte[] length = DataElementLengthCalculator.lengthAsByte(track2, deConfig);
        logger.log(HexDump.dumpHexString(length));
    }

    @Test
    public void calculateLengthForBCDTest(){
        MockSpecInfo specInfo = new MockSpecInfo();
        DataElementConfig deConfig = specInfo.getDEConfig().getByFieldNumber(35);
        DataElement track2 = new DataElement(55, "9F26084266DA1FF65DFD3F9F2701809F100706011203A000009F3704FA0077CA9F36020002950502800088009A032008119C01009F02060000001200005F2A020524820258009F1A0205249F03060000000000009F3303E0F0C89F34031E03009F3501229F1E0845303139353438398407A00000000310109F090201409F4104000000045F340101");
        byte[] length = DataElementLengthCalculator.lengthAsByte(track2, deConfig);
        logger.log(HexDump.dumpHexString(length));
    }


}
