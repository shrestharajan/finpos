package global.citytech.finposframework.usecases.transaction;

import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;

public interface TransactionInitializer {
    TransactionRequest initialize(TransactionRequest transactionRequest);
}
