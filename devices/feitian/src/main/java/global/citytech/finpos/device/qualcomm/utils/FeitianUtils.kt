package global.citytech.finpos.device.qualcomm.utils

import androidx.annotation.IntegerRes
import com.ftpos.library.smartpos.device.Device
import com.ftpos.library.smartpos.emv.Amount
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.emv.IPinBlockFormat
import com.ftpos.library.smartpos.keymanager.AlgName
import com.ftpos.library.smartpos.keymanager.KeyType
import com.ftpos.library.smartpos.util.TlvUtil
import global.citytech.common.extensions.defaultOrEmptyValue
import global.citytech.common.extensions.isNullOrEmptyOrBlank
import global.citytech.common.extensions.removeCharactersIfNotADigit
import global.citytech.finpos.device.qualcomm.io.cards.transaction.FeitianSingleton
import global.citytech.finposframework.hardware.EmvTagList
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.*
import global.citytech.finposframework.hardware.io.cards.CardType.ICC
import global.citytech.finposframework.hardware.io.cards.CardType.PICC
import global.citytech.finposframework.hardware.io.cards.cvm.PICCCvm
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.hardware.keymgmt.keys.KeyAlgorithm
import global.citytech.finposframework.hardware.keymgmt.keys.KeyAlgorithm.*
import global.citytech.finposframework.hardware.keymgmt.keys.KeyType.*
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat.*
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_ALG_NOT_SUPPORT
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_KEY_NOT_SUPPORT
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_PHASE_PROCESS_EMV_SUCCESS
import global.citytech.finposframework.hardware.utility.DeviceBytesUtil
import global.citytech.finposframework.hardware.utility.DeviceUtils
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.utility.AmountUtils
import global.citytech.finposframework.utility.HexString
import global.citytech.finposframework.utility.StringUtils
import java.math.BigDecimal
import java.util.*

/**
 * Created by Rishav Chudal on 4/5/21.
 */
class FeitianUtils {
    companion object {
        fun retrieveDeviceSerialNumber(device: Device?): String {
            var serialNumber = ""
            if (device != null) {
                serialNumber = device.serialNumber
            }
            return serialNumber
        }

        fun getDeviceKeyType(keyType: global.citytech.finposframework.hardware.keymgmt.keys.KeyType?): Int {
            if (keyType != null) {
                return when (keyType) {
                    KEY_TYPE_DEK -> KeyType.KEY_TYPE_DEK
                    KEY_TYPE_FIXED_KEY -> KeyType.KEY_TYPE_FIXEDKEY
                    KEY_TYPE_IPEK -> KeyType.KEY_TYPE_IPEK
                    KEY_TYPE_KBPK -> KeyType.KEY_TYPE_KBPK
                    KEY_TYPE_MEK -> KeyType.KEY_TYPE_MEK
                    KEY_TYPE_TMK -> KeyType.KEY_TYPE_MK
                    KEY_TYPE_PEK -> KeyType.KEY_TYPE_PEK
                    KEY_TYPE_RSA -> KeyType.KEY_TYPE_RSA
                    else -> throw Exception(MSG_KEY_NOT_SUPPORT)
                }
            } else {
                throw Exception(MSG_KEY_NOT_SUPPORT)
            }
        }

        fun getDeviceAlgorithmMode(keyAlgorithm: KeyAlgorithm): Int {
            return when (keyAlgorithm) {
                TYPE_3DES -> AlgName.SYM_ARITH_3DES
                TYPE_AES -> AlgName.SYM_ARITH_AES
                TYPE_DES -> AlgName.SYM_ARITH_DES
                TYPE_RSA -> AlgName.SYM_ARITH_RSA
                TYPE_SM4 -> AlgName.SYM_ARITH_SM4
                else -> throw Exception(MSG_ALG_NOT_SUPPORT)
            }
        }

        fun openCardReaders(slotsToOpen: List<CardType?>): Int {
            var mode = 0
            if (slotsToOpen.contains(ICC)) {
                mode = mode or FeitianConstants.CARD_DETECTION_MODE_ICC
            }
            if (slotsToOpen.contains(PICC)) {
                mode = mode or FeitianConstants.CARD_DETECTION_MODE_PICC
            }
            if (slotsToOpen.contains(CardType.MAG)) {
                mode = mode or FeitianConstants.CARD_DETECTION_MODE_MAG
            }
            return mode
        }

        private fun retrieveAID(emv: Emv): String {
            var aid = ""
            val tagInHex: String = EmvTagList.TAG_9F06_APPLICATION_IDENTIFIER.tagInHex()
            val emvTag: EmvTag? = retrieveEmvTag(
                emv,
                tagInHex
            )
            if (emvTag != null) {
                aid = emvTag.data
            }
            return aid
        }

        fun retrievePAN(emv: Emv): String {
            var pan = ""
            val tagInHex: String = EmvTagList.APPLICATION_PRIMARY_ACCOUNT_NUMBER.tagInHex()
            val emvTag: EmvTag? = retrieveEmvTag(
                emv,
                tagInHex
            )
            pan = formatPAN(emv, emvTag)
            return pan
        }

        private fun formatPAN(emv: Emv, emvTagPAN: EmvTag?): String {
            return emvTagPAN?.data?.removeCharactersIfNotADigit() ?: retrievePanFromTrack2Data(emv)
        }

        private fun retrievePanFromTrack2Data(emv: Emv): String {
            var pan = StringUtils.TAG_EMPTY_STRING
            try {
                pan = retrieveTrack2Data(emv).split("D").toTypedArray()[0]
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            return pan
        }

        private fun retrieveTrack2Data(emv: Emv): String {
            var track2Data = ""
            val tagInHex: String = EmvTagList.TRACK_2_EQUIVALENT_DATA.tagInHex()
            val emvTag: EmvTag? = retrieveEmvTag(
                emv,
                tagInHex
            )
            if (emvTag != null) {
                track2Data = emvTag.data
                val track2DataLen = track2Data.indexOf("F")
                if (track2DataLen > 0) {
                    track2Data = track2Data.substring(0, track2DataLen)
                }
                if (track2Data.length > 37) {
                    track2Data = track2Data.substring(0, 37)
                }
            }
            return track2Data
        }

        private fun retrieveExpiryDate(emv: Emv): String {
            var expiryDate = ""
            val tagInHex: String = EmvTagList.CARD_EXPIRATION_DATE.tagInHex()
            val emvTag: EmvTag? = retrieveEmvTag(
                emv,
                tagInHex
            )
            if (emvTag != null) {
                expiryDate = emvTag.data.substring(0, 4)
            }
            if (StringUtils.isEmpty(expiryDate))
                expiryDate = DeviceUtils.expiryDateFromTrackTwoData(retrieveTrack2Data(emv))
            return expiryDate
        }

        private fun retrievePANSerialNumber(emv: Emv): String {
            var cardSerialNumber = ""
            val tagInHex: String = EmvTagList.PAN_SEQUENCE_NUMBER.tagInHex()
            val emvTag: EmvTag? = retrieveEmvTag(
                emv,
                EmvTagList.PAN_SEQUENCE_NUMBER.tagInHex()
            )
            cardSerialNumber = emvTag?.data ?: "00"
            return cardSerialNumber
        }

        private fun retrieveCardHolderName(emv: Emv): String? {
            var cardHolderName: String? = ""
            val tagInHex: String = EmvTagList.CARD_HOLDER_NAME.tagInHex()
            val emvTag: EmvTag? = retrieveEmvTag(
                emv,
                tagInHex
            )
            if (emvTag != null) {
                cardHolderName = StringUtils.hextToString(emvTag.data)
            }
            return cardHolderName
        }

        private fun retrieveIssuerCountryCode(emv: Emv): String {
            var issuerCountryCode = ""
            val tagInHex: String = EmvTagList.TAG_5F28_ISSUER_COUNTRY_CODE.tagInHex()
            val emvTag: EmvTag? = retrieveEmvTag(
                emv,
                tagInHex
            )
            if (emvTag != null) {
                issuerCountryCode = emvTag.data
            }
            return issuerCountryCode
        }

        private fun retrieveApplicationCurrencyCode(emv: Emv): String {
            var currencyCodeApplication = ""
            val tagInHex: String = EmvTagList.TAG_9F42_CURRENCY_CODE_APPLICATION.tagInHex()
            val emvTag: EmvTag? = retrieveEmvTag(
                emv,
                tagInHex
            )
            if (emvTag != null) {
                currencyCodeApplication = emvTag.data
            }
            return currencyCodeApplication
        }


        private fun retrieveApplicationPreferredName(emv: Emv): String {
            var applicationPreferredName: String = ""
            val tagInHex: String = EmvTagList.TAG_9F12_APPLICATION_PREFERRED_NAME.tagInHex()
            val emvTag: EmvTag? = retrieveEmvTag(
                emv,
                tagInHex
            )
            if (emvTag != null) {
                applicationPreferredName = formatCardSchemeLabelHexToAscii(emvTag.data)
            }
            return applicationPreferredName
        }

        private fun formatCardSchemeLabelHexToAscii(hexData: String):  String{
            var cardSchemeLabel = ""
            if (!hexData.isNullOrEmptyOrBlank()) {
                cardSchemeLabel = HexString.hexToString(hexData)
            }
            return cardSchemeLabel
        }

        private fun retrieveEmvTag(emv: Emv, tagInHex: String): EmvTag? {
            var emvTag: EmvTag? = null
            try {
                val outData = ByteArray(100)
                val outDataLen = IntArray(1)
                emv.getTLV(tagInHex, outData, outDataLen)
                if (outDataLen[0] > 0) {
                    val formattedOutData: ByteArray? =
                        formatValueFromTLV(tagInHex, outData, outDataLen)
                    if (formattedOutData != null) {
                        emvTag = EmvTag(
                            tagInHex.toInt(16),
                            DeviceBytesUtil.bytes2HexString(formattedOutData),
                            outDataLen[0]
                        )
                    }
                }
            } catch (ex: java.lang.Exception) {
                FeitianLogger.logStatic("Exception in retrieving tag ::: ".plus(tagInHex))
                ex.printStackTrace()
            }
            return emvTag
        }

        private fun formatValueFromTLV(
            tagInHex: String,
            outData: ByteArray,
            outDataLen: IntArray
        ): ByteArray? {
            var formattedOutData: ByteArray? = null
            try {
                formattedOutData = Arrays.copyOf(outData, outDataLen[0])
                val tlvInHex = DeviceBytesUtil.bytes2HexString(formattedOutData)
                if (!StringUtils.isEmpty(tagInHex) || !StringUtils.isEmpty(tlvInHex)) {
                    val tagInHexCharactersLength = tagInHex.length
                    val tlvLengthMaxCharacters = 2
                    formattedOutData = StringUtils.hexString2bytes(
                        tlvInHex.substring(tagInHexCharactersLength + tlvLengthMaxCharacters)
                    )
                }
            } catch (ex: java.lang.Exception) {
                ex.printStackTrace()
            }
            return formattedOutData
        }

        fun addEmvDataOnCardDetails(
            emv: Emv?,
            cardDetails: CardDetails
        ) {
            cardDetails.aid = retrieveAID(emv!!)
            cardDetails.primaryAccountNumber = retrievePAN(emv)
            cardDetails.trackTwoData = retrieveTrack2Data(emv)
            cardDetails.expiryDate = retrieveExpiryDate(emv)
            cardDetails.primaryAccountNumberSerialNumber = retrievePANSerialNumber(emv)
            cardDetails.cardHolderName = retrieveCardHolderName(emv)
            cardDetails.tag5F28 = retrieveIssuerCountryCode(emv)
            cardDetails.tag9F42 = retrieveApplicationCurrencyCode(emv)
            cardDetails.cardSchemeLabel = retrieveApplicationPreferredName(emv)
        }

        fun getDevicePinBlockFormat(pinBlockFormat: PinBlockFormat): Int {
            return when (pinBlockFormat) {
                ISO9564_FORMAT_1 -> IPinBlockFormat.BLOCK_FORMAT_1
                ISO9564_FORMAT_2 -> IPinBlockFormat.BLOCK_FORMAT_2
                ISO9564_FORMAT_3 -> IPinBlockFormat.BLOCK_FORMAT_3
                ISO9564_FORMAT_4 -> IPinBlockFormat.BLOCK_FORMAT_4
                else -> IPinBlockFormat.BLOCK_FORMAT_0
            }
        }

        fun getRespectivePinEntryType(pinEntryType: Int): String {
            var type = ""
            when (pinEntryType.toByte()) {
                Emv.EMV_CVMFLAG_PLOFFLINE_PIN_SIGN -> type = "Plain Text Offline Pin"
                Emv.EMV_CVMFLAG_OLPIN_SIGN -> type = "Encrypted Online Pin"
                Emv.EMV_CVMFLAG_ENOFFLINE_PIN_SIGN -> type = "Encrypted Offline Pin"
                else -> {
                }
            }
            return type
        }

        fun prepareProcessEMVReadCardResponseForPICC(
            emv: Emv,
            readCardRequest: ReadCardRequest
        ): ReadCardResponse {
            val cardDetails =
                CardDetails()
            cardDetails.transactionState =
                readCardRequest.cardDetailsBeforeEmvNext!!.transactionState
            cardDetails.tagCollection =
                readCardRequest.cardDetailsBeforeEmvNext!!.tagCollection
            cardDetails.iccDataBlock = readCardRequest.cardDetailsBeforeEmvNext!!.iccDataBlock
            cardDetails.pinBlock = readCardRequest.cardDetailsBeforeEmvNext!!.pinBlock
            cardDetails.transactionAcceptedByCard = true
            cardDetails.aid = readCardRequest.cardDetailsBeforeEmvNext!!.aid
            cardDetails.cashBackAmount = readCardRequest.cashBackAmount
            cardDetails.amount = readCardRequest.amount
            cardDetails.cardType = readCardRequest.cardDetailsBeforeEmvNext!!.cardType
            cardDetails.primaryAccountNumber =
                readCardRequest.cardDetailsBeforeEmvNext!!.primaryAccountNumber
            cardDetails.primaryAccountNumberSerialNumber =
                readCardRequest.cardDetailsBeforeEmvNext!!.primaryAccountNumberSerialNumber
            cardDetails.expiryDate = readCardRequest.cardDetailsBeforeEmvNext!!.expiryDate
            cardDetails.trackTwoData = readCardRequest.cardDetailsBeforeEmvNext!!.trackTwoData
            cardDetails.cardScheme = readCardRequest.cardDetailsBeforeEmvNext!!.cardScheme
            cardDetails.cardHolderName = readCardRequest.cardDetailsBeforeEmvNext!!.cardHolderName
            cardDetails.cardSchemeLabel = readCardRequest.cardDetailsBeforeEmvNext!!.cardSchemeLabel
            cardDetails.transactionInitializeDateTime =
                readCardRequest.cardDetailsBeforeEmvNext!!.transactionInitializeDateTime
            val readCardResponse = ReadCardResponse(
                cardDetails,
                Result.SUCCESS,
                MSG_PHASE_PROCESS_EMV_SUCCESS
            )
            readCardResponse.piccCvm = checkForContactlessCVM(emv)
            return readCardResponse
        }

        private fun retrieveTransactionOutcome(emv: Emv): String {
            var transactionOutcome = ""
            val tagInHex: String = EmvTagList.TAG_DF8129_TRANSACTION_OUTCOME.tagInHex()
            val emvTag: EmvTag? = retrieveEmvTag(
                emv,
                tagInHex
            )
            if (emvTag != null) {
                transactionOutcome = emvTag.data
            }
            return transactionOutcome
        }

        private fun checkForContactlessCVM(emv: Emv): PICCCvm {
            FeitianLogger.logStatic("Checking for Contactless CVM...")
            var piccCVM: PICCCvm = PICCCvm.NO_CVM
            try {
                val transactionOutcome = retrieveTransactionOutcome(emv)
                if (!transactionOutcome.isNullOrEmptyOrBlank()) {
                    piccCVM =
                        DeviceUtils.retrieveCVMFromTransactionOutcome(transactionOutcome)
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            return piccCVM
        }

        fun addBasicEMVDataBasedOnCardDetails(
            emv: Emv,
            singleton: FeitianSingleton,
            cardDetails: CardDetails
        ) {
            when (singleton.currentCardDetected) {
                PICC -> addEMVDataForPICC(emv, singleton, cardDetails)
                ICC -> addEmvDataForICC(singleton, cardDetails)
                else -> return
            }
        }

        private fun addEMVDataForPICC(
            emv: Emv,
            feitianSingleton: FeitianSingleton,
            cardDetails: CardDetails
        ) {
            FeitianLogger.logStatic("Adding EMV Data for PICC...")
            cardDetails.cardType = PICC
            addEmvDataOnCardDetails(emv, cardDetails)
        }

        private fun addEmvDataForICC(
            singleton: FeitianSingleton,
            cardDetails: CardDetails
        ) {
            FeitianLogger.logStatic("Adding EMV Data for ICC...")
            val readCardRequest: ReadCardRequest = singleton.readCardRequest!!
            val earlierEmvProcessCardDetails = readCardRequest.cardDetailsBeforeEmvNext
            if (earlierEmvProcessCardDetails != null) {
                cardDetails.transactionAcceptedByCard = true
                cardDetails.aid = earlierEmvProcessCardDetails.aid
                cardDetails.cashBackAmount = readCardRequest.cashBackAmount
                cardDetails.amount = readCardRequest.amount
                cardDetails.cardType = earlierEmvProcessCardDetails.cardType
                cardDetails.primaryAccountNumber = earlierEmvProcessCardDetails.primaryAccountNumber
                cardDetails.primaryAccountNumberSerialNumber =
                    earlierEmvProcessCardDetails.primaryAccountNumberSerialNumber
                cardDetails.expiryDate = earlierEmvProcessCardDetails.expiryDate
                cardDetails.trackTwoData = earlierEmvProcessCardDetails.trackTwoData
                cardDetails.cardSchemeLabel = earlierEmvProcessCardDetails.cardSchemeLabel
                cardDetails.cardHolderName = earlierEmvProcessCardDetails.cardHolderName
                cardDetails.tag9F42 = earlierEmvProcessCardDetails.tag9F42
                cardDetails.tag5F28 = earlierEmvProcessCardDetails.tag5F28
                cardDetails.cardScheme = earlierEmvProcessCardDetails.cardScheme
            }
        }

        fun addICCDataBlockAndTagCollection(
            transData: String?,
            emv: Emv,
            cardDetails: CardDetails
        ) {
            val iccDataBlock = retrieveRequestedICCData(emv)
            cardDetails.iccDataBlock = iccDataBlock
            val emvTagCollection = retrieveEMVTagCollection(emv, iccDataBlock)
            cardDetails.tagCollection = emvTagCollection
//            addTag8AIfPresent(emv, cardDetails)
        }

        private fun retrieveRequestedICCData(emv: Emv): String {
            var dataBlock = ""
            try {
                dataBlock = appendRequiredICCDataTagsAndProceed(emv)
                println("Retrived data"+ dataBlock)
            } catch (ex: Exception) {
                FeitianLogger.logStatic("Exception in building ICC Data Block...")
                ex.printStackTrace()
            }
            return validateICCDataBlockAndReturn(emv, dataBlock)
        }

        private fun appendRequiredICCDataTagsAndProceed(emv: Emv): String {
            val requestICCDataItems = appendRequiredICCDataTags()
            return validateICCDataItemsAndProceed(emv, requestICCDataItems)
        }

        private fun appendRequiredICCDataTags(): String {
            val tagListBuilder = StringBuilder()
            for (iccData in FeitianSingleton.readCardRequest!!.iccDataList) {
                if(FeitianSingleton.readCardRequest!!.transactionType == TransactionType.GREEN_PIN  || FeitianSingleton.readCardRequest!!.transactionType == TransactionType.PIN_CHANGE){
                    if (iccData.tag == 0x8A || iccData.tag == 0x9F09 || iccData.tag  == 0x9F35 || iccData.tag == 0x4F
                    ) {
                        FeitianLogger.logStatic("Remove icc Data ::::::"+ iccData.tag)
                    }else{
                        tagListBuilder.append(iccData.tagInHex())
                    }
                }else{
                    tagListBuilder.append(iccData.tagInHex())
                }
            }
            val requiredICCDataItems = tagListBuilder.toString()
            FeitianLogger.debugStatic("ICC Data items ::: ".plus(requiredICCDataItems))
            return requiredICCDataItems
        }

        private fun validateICCDataItemsAndProceed(
            emv: Emv,
            requestedICCDataItems: String
        ): String {
            return if (requestedICCDataItems.isNullOrEmptyOrBlank()) {
                ""
            } else {
                retrieveTLVData(requestedICCDataItems, emv)
            }
        }

        private fun validateICCDataBlockAndReturn(
            emv: Emv,
            dataBlock: String
        ): String {
            FeitianLogger.debugStatic("ICC Data Block ::: ".plus(dataBlock))
            var iccDataBlock = dataBlock
            if (iccDataBlock.isNullOrEmptyOrBlank()) {
                iccDataBlock = ""
            }
            return iccDataBlock
        }

        private fun retrieveTLVData(tagList: String, emv: Emv): String {
            var outData = ByteArray(256)
            val outDataLen = IntArray(1)
            var data = ""
            try {
                emv.getTLV(tagList, outData, outDataLen)
                outData = outData.copyOf(outDataLen[0])
                data = DeviceBytesUtil.bytes2HexString(outData)
                if (StringUtils.isEmpty(data)) {
                    data = ""
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            return data
        }

        private fun retrieveEMVTagCollection(
            emv: Emv,
            iccData: String
        ): EMVTagCollection {
            var iccDataBlock = retrieveGenericICCData(emv)
            FeitianLogger.logStatic("********** Retrieving EMV Tag Collection **********")
            if (StringUtils.isEmpty(iccDataBlock)) {
                iccDataBlock = retrieveRequestedICCData(emv)
            }
            val emvTagCollection = EMVTagCollection()
            val tlvList = TlvUtil.getTLVs(DeviceBytesUtil.hexString2Bytes(iccDataBlock))
            if (tlvList.isNotEmpty()) {
                for (tlv in tlvList) {
                    try {
                        val emvTag = EmvTag(
                            DeviceBytesUtil.bytesToInt(tlv.tagBytes),
                            DeviceBytesUtil.bytes2HexString(tlv.valueBytes),
                            tlv.length
                        )
                        FeitianLogger.logStatic("***** TLV *****")
                        FeitianLogger.debugStatic(
                            "TAG ::: ".plus(tlv.tagBytesAsHex.substring(2))
                        )
                        FeitianLogger.debugStatic(
                            "LENGTH ::: ".plus(tlv.length)
                        )
                        FeitianLogger.debugStatic(
                            "VALUE ::: ".plus(DeviceBytesUtil.bytes2HexString(tlv.valueBytes))
                        )
                        emvTagCollection.add(emvTag)
                    } catch (ex: Exception) {
                        FeitianLogger.logStatic(
                            "Exception in getting TAG ::: ".plus(tlv.tagBytesAsHex)
                        )
                        ex.printStackTrace()
                    }
                }
            }
            DeviceUtils.appendTagIfNotPresent(
                emvTagCollection,
                EmvTagList.ADDITIONAL_AMOUNT,
                6
            )
            DeviceUtils.appendTagIfNotPresent(
                emvTagCollection,
                EmvTagList.TAG_9F34_CVMR,
                3
            )
            return emvTagCollection
        }

        private fun retrieveGenericICCData(emv: Emv): String {
            var dataBlock = ""
            val tagListBuilder = StringBuilder()
            try {
                for (iccData in IccData.values()) {
                    tagListBuilder.append(iccData.tagInHex())
                }
                dataBlock = retrieveTLVData(
                    tagListBuilder.toString(),
                    emv
                )
            } catch (ex: Exception) {
                FeitianLogger.logStatic("Exception in building Generic ICC Data Block...")
                ex.printStackTrace()
            }
            if (StringUtils.isEmpty(dataBlock)) {
                dataBlock = ""
            }
            FeitianLogger.debugStatic("Generic ICC Data Block ::: ".plus(dataBlock))
            return dataBlock
        }

        private fun addTag8AIfPresent(
            emv: Emv,
            cardDetails: CardDetails
        ) {
            val emvTag = retrieveEmvTag(
                emv,
                EmvTagList.TAG_8A_AUTHORISATION_RESPONSE_CODE.tagInHex()
            )
            if (emvTag != null) {
                cardDetails.tagCollection!!.add(emvTag)
            }
        }

        fun retrieveDateElement39FromIssuerResponse(issuerResponseData: String): String {
            var de39 = ""
            val tag8A: String = retrieveValueFromTLVList(
                EmvTagList.TAG_8A_AUTHORISATION_RESPONSE_CODE,
                issuerResponseData
            )
            if (!StringUtils.isEmpty(tag8A)) {
                val tag8ABytes = DeviceBytesUtil.hexString2Bytes(tag8A)
                de39 = String(tag8ABytes)
            }
            FeitianLogger.debugStatic("Retrieved DE 39 (Tag 8A) value ::: ".plus(de39))
            return de39
        }

        fun retrieveTag91FromIssuerResponseData(issuerResponseData: String?): String {
            val tag91 = retrieveValueFromTLVList(
                EmvTagList.TAG_91_ISSUER_AUTHENTICATION_DATA,
                issuerResponseData
            )
            FeitianLogger.debugStatic(
                "Retrieved Tag 91 (Issuer Authentication Data) value ::: ".plus(tag91)
            )
            return tag91
        }

        fun retrieveIssuerScript71FromIssuerResponseData(issuerResponseData: String?): String {
            val tag71 = retrieveValueFromTLVList(
                EmvTagList.TAG_71_ISSUER_SCRIPT_71,
                issuerResponseData
            )
            FeitianLogger.debugStatic("Retrieved Tag 71 (Issuer Script 71) value ::: ".plus(tag71))
            return tag71
        }

        fun retrieveIssuerScript72FromIssuerResponseData(issuerResponseData: String?): String {
            val tag72 = retrieveValueFromTLVList(
                EmvTagList.TAG_72_ISSUER_SCRIPT_72,
                issuerResponseData
            )
            FeitianLogger.debugStatic("Retrieved Tag 72 (Issuer Script 72) value ::: ".plus(tag72))
            return tag72
        }

        private fun retrieveValueFromTLVList(
            emvTagList: EmvTagList,
            tlvData: String?
        ): String {
            var value = ""
            try {
                if (!StringUtils.isEmpty(tlvData)) {
                    val tlvList = TlvUtil.getTLVs(DeviceBytesUtil.hexString2Bytes(tlvData))
                    if (tlvList != null && !tlvList.isEmpty()) {
                        for (tlv in tlvList) {
                            if (tlv.tagBytesAsHex.substring(2)
                                    .equals(emvTagList.tagInHex(), ignoreCase = true)
                            ) {
                                value = DeviceBytesUtil.bytes2HexString(tlv.valueBytes)
                            }
                        }
                    }
                }
            } catch (ex: Exception) {
                FeitianLogger.logStatic("Exception in retrieveValueOfTagFromTLVData")
                ex.printStackTrace()
            }
            return value
        }

        fun prepareTransactionAmount(
            amount: BigDecimal?,
            cashBackAmount: BigDecimal?,
            transactionType: TransactionType
        ): Amount? {
            var sdkAmount: Amount? = null
            if (transactionAmountIsValid(amount, transactionType)) {
                sdkAmount = prepareSDKTransactionAmount(amount!!, cashBackAmount!!)
            }
            return sdkAmount
        }

        private fun transactionAmountIsValid(
            amount: BigDecimal?,
            transactionType: TransactionType
        ): Boolean {
            var amountValid = true
            if ((amount == null || amount == BigDecimal.ZERO) && transactionType != TransactionType.VOID) {
                amountValid = false
            }
            return amountValid
        }

        private fun prepareSDKTransactionAmount(
            amount: BigDecimal,
            cashBackAmount: BigDecimal
        ): Amount {
            val transactionAmount = AmountUtils.toLowerCurrencyFromBigDecimalInLong(amount)
            val transactionCashBackAmount = AmountUtils.toLowerCurrencyFromBigDecimalInLong(cashBackAmount)
            return Amount(transactionAmount, transactionCashBackAmount)
        }

        fun prepareCardSummary(emv: Emv): CardSummary {
            return CardSummary(
                retrieveApplicationPreferredName(emv).defaultOrEmptyValue(),
                retrievePAN(emv).defaultOrEmptyValue(),
                retrieveCardHolderName(emv).defaultOrEmptyValue(),
                retrieveExpiryDate(emv).defaultOrEmptyValue()
            )
        }
    }
}