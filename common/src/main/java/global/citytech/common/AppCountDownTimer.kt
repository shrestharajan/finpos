package global.citytech.common

import android.os.CountDownTimer

/**
 * Created by Unique Shakya on 9/14/2021.
 */
class AppCountDownTimer(millisInFuture: Long,
                        countDownInterval: Long,
                        private val onCountDownTick :  (millisUntilFinished: Long)->Unit,
                        private val onCountDownFinish :  ()->Unit
)
    : CountDownTimer(millisInFuture, countDownInterval) {

    override fun onTick(millisUntilFinished: Long) {
        onCountDownTick(millisUntilFinished)
    }

    override fun onFinish() {
        onCountDownFinish()
    }
}