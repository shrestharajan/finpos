package global.citytech.finpos.nibl.iso8583.requestsender.cashadvance;

import global.citytech.finpos.nibl.iso8583.PosEntryModeConfig;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.switches.PosEntryMode;

/**
 * Created by Unique Shakya on 2/4/2021.
 */
public class ManualCashAdvanceRequestSender extends CashAdvanceRequestSender {

    public ManualCashAdvanceRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
    }

    @Override
    protected Iso8583Msg addConditionalFields(Iso8583Msg msg, TransactionIsoRequest cashAdvanceRequest) {
        msg.addField(
                new DataElement(
                        DataElement.PRIMARY_ACCOUNT_NUMBER,
                        cashAdvanceRequest.getPan()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.PAN_EXPIRE_DATE,
                        cashAdvanceRequest.getExpireDate()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(PosEntryMode.MANUAL)
                )
        );
        return msg;
    }
}
