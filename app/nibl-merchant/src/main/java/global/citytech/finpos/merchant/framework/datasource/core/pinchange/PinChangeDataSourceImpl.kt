package global.citytech.finpos.merchant.framework.datasource.core.pinchange

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.pinchange.PinChangeDataSource
import global.citytech.finpos.merchant.domain.model.pinchange.PinChangeRequestEntity
import global.citytech.finpos.merchant.domain.model.pinchange.PinChangeResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionAuthenticatorImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.*
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.data.mapToPinChangeResponseUiModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.pinchange.PinChangeRequestItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finposframework.switches.ProcessorManager
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest
import io.reactivex.Observable

class PinChangeDataSourceImpl : PinChangeDataSource {
    override fun pinChangeRequester(
        configurationItem: ConfigurationItem,
        pinChangeRequestItem: PinChangeRequestItem
    ): Observable<PinChangeResponseEntity> {

        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val transactionRepository =
            TransactionRepositoryImpl(
                NiblMerchant.INSTANCE
            )
        val transactionRequest = prepareTransactionRequest(pinChangeRequestItem)
        val deviceController = DeviceControllerImpl(NiblMerchant.INSTANCE)
        val cardReader = CardSourceImpl(NiblMerchant.getActivityContext())
        val transactionAuthenticator =
            TransactionAuthenticatorImpl()
        val printerServiceImpl = PrinterSourceImpl(NiblMerchant.INSTANCE)
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)

        val pinChangeRequestEntity = PinChangeRequestEntity(
            readCardService = cardReader,
            deviceController = deviceController,
            printerService = printerServiceImpl,
            transactionAuthenticator = transactionAuthenticator,
            transactionRepository = transactionRepository,
            transactionRequest = transactionRequest,
            applicationRepository = applicationRepository,
            ledService = LedSourceImpl(NiblMerchant.INSTANCE),
            soundService = SoundSourceImpl(NiblMerchant.INSTANCE)
        )

        val pinChangeRequester = ProcessorManager.getInterface(
            terminalRepository, NotificationHandler
        ).pinChangeRequester

        return Observable.fromCallable {
            pinChangeRequester.execute(
                pinChangeRequestEntity.mapToModel()
            ).mapToPinChangeResponseUiModel()
        }
    }


    private fun prepareTransactionRequest(pinChangeRequestItem: PinChangeRequestItem): TransactionRequest {
        val transactionRequest = TransactionRequest(
            pinChangeRequestItem.transactionType,
        )
        transactionRequest.amount = pinChangeRequestItem.amount
        return transactionRequest
    }
}