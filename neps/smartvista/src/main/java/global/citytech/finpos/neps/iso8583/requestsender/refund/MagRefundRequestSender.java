package global.citytech.finpos.neps.iso8583.requestsender.refund;

import global.citytech.finpos.neps.iso8583.requestsender.cardtypesenders.MagneticISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.purchases.PurchaseRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.CardTypeISOMessageUpdater;
import global.citytech.finposframework.iso8583.RequestContext;

/**
 * Created by Saurav Ghimire on 1/28/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class MagRefundRequestSender extends RefundRequestSender {

    public MagRefundRequestSender(RequestContext context) {
        super(context);
    }

    @Override
    protected String getClassName() {
        return MagRefundRequestSender.class.getName();
    }

    @Override
    protected CardTypeISOMessageUpdater getCardTypeSender() {
        return new MagneticISOMessageUpdater();
    }

}
