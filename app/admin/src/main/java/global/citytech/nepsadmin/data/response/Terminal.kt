package global.citytech.nepsadmin.data.response

data class Terminal(
    var switchId: String? = null
)