package global.citytech.finpos.merchant.domain.usecase.qr

/**
 * @author sachin
 */
interface CrcCalculator {
    fun calcualteCrc(data: ByteArray): String
}