package global.citytech.neps.data.db.dao

import android.database.Cursor
import androidx.room.*
import global.citytech.neps.domain.model.Host
import global.citytech.neps.domain.model.Merchant


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
@Dao
interface MerchantDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(merchants: List<Merchant>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(merchant: Merchant): Long

    @Query("SELECT * FROM merchant")
    fun get(): Cursor

    @Query("SELECT * FROM merchant")
    fun getMerchantList(): List<Merchant>

    @Query("SELECT * FROM merchant where id = :id")
    fun getById(id: Long): Cursor?

    @Update
    fun update(merchant: Merchant):Int

    @Query("DELETE FROM merchant WHERE id = :id")
    fun deleteById(id: Long): Int


}