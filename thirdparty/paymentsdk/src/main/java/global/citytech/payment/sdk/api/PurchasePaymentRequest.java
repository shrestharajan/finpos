package global.citytech.payment.sdk.api;

/**
 * Created by Rishav Chudal on 6/11/21.
 */
public class PurchasePaymentRequest {
    private final double billingAmount;
    private final String transactionCurrencyCode;
    private String additionalData;


    public PurchasePaymentRequest(double billingAmount, String transactionCurrencyCode) {
        this.billingAmount = billingAmount;
        this.transactionCurrencyCode = transactionCurrencyCode;
    }

    public double getBillingAmount() {
        return billingAmount;
    }

    public String getTransactionCurrencyCode() {
        return transactionCurrencyCode;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }
}
