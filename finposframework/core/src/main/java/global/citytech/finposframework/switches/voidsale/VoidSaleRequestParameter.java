package global.citytech.finposframework.switches.voidsale;

import global.citytech.finposframework.switches.RequestParameter;

/**
 * Created by Rishav Chudal on 9/17/20.
 */
public interface VoidSaleRequestParameter extends RequestParameter {
}
