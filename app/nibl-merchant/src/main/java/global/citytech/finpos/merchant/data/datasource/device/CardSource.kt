package global.citytech.finpos.merchant.data.datasource.device

import global.citytech.finposframework.hardware.io.cards.read.ReadCardService

/**
 * Created by Unique Shakya on 8/27/2020.
 */
interface CardSource :
    ReadCardService {
}