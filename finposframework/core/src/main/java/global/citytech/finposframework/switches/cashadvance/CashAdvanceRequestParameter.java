package global.citytech.finposframework.switches.cashadvance;

import global.citytech.finposframework.switches.RequestParameter;

/**
 * Created by Unique Shakya on 2/4/2021.
 */
public interface CashAdvanceRequestParameter extends RequestParameter {
}
