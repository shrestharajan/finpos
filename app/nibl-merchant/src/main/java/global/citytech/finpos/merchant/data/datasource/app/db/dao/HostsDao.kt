package global.citytech.finpos.merchant.data.datasource.app.db.dao

import androidx.room.*
import global.citytech.finpos.merchant.domain.model.app.Host


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
@Dao
interface HostsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(hosts: List<Host>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(host: Host): Long

    @Query("SELECT id, connection_time_out, ip, nii, `order`, port, retry_limit, tls_certificate FROM hosts")
    fun getHostList(): List<Host>

    @Update
    fun update(host: Host):Int

    @Query("DELETE FROM hosts WHERE id = :id")
    fun deleteById(id: Long): Int

    @Query("DELETE FROM hosts")
    fun nukeTableData()

}