package global.citytech.finpos.merchant.presentation.alertdialogs

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Unique Shakya on 9/9/2020.
 */
@Parcelize
data class PosEntrySelection(
    val posEntryMessage: String,
    val isPICCAllowed: Boolean,
    val isManualAllowed: Boolean,
    val entryMode:PosEntryMode
) :Parcelable{
}

enum class PosEntryMode(val loaderImage:String){
    ACCEPT_ALL("file:///android_asset/images/all.gif"),
    ACCEPT_ICC_PICC("file:///android_asset/images/icc_picc.gif"),
    ACCEPT_MAG_ONLY("file:///android_asset/images/mag_only.gif"),
    ACCEPT_PICC_ONLY("file:///android_asset/images/picc_only.gif"),
    ACCEPT_ICC_ONLY("file:///android_asset/images/icc_only.gif")
}