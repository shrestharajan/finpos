package global.citytech.finpos.merchant.presentation.refund

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.common.extensions.hideKeyboard
import global.citytech.easydroid.core.extension.showToast
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityBaseTransactionBinding
import global.citytech.finpos.merchant.presentation.model.refund.ManualRefundRequestItem
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionActivity
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finpos.merchant.utils.handleDebounce
import global.citytech.finposframework.usecases.TransactionType
import kotlinx.android.synthetic.main.activity_base_transaction.*
import kotlinx.android.synthetic.main.layout_refund.*

/**
 * Created by Unique Shakya on 9/28/2020.
 */
class RefundActivity : BaseTransactionActivity<ActivityBaseTransactionBinding, RefundViewModel>() {

    private lateinit var viewModel: RefundViewModel
    private var originalRrn: String? = null
    private var manualRefund: Boolean = false
    private var cardNumber: String = ""
    private var expiryDate: String = ""
    private var cvv: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadRefundLayout()
        viewModel = getViewModel()
        initBaseViews()
        initViews()
        initObservers()
    }

    private fun loadRefundLayout() {
        layout_refund.visibility = View.VISIBLE
    }

    private fun hideRefundLayout() {
        layout_refund.visibility = View.GONE
    }

    private fun initObservers() {
        this.viewModel.validRrn.observe(this, Observer {
            if (it) {
                this.hideKeyboard(et_refund_retrieval_reference)
                this.startAmountActivity()
            }
            else
                this.showToast("Please enter valid retrieval reference number", Toast.LENGTH_SHORT)
        })

        this.viewModel.configurationItem.observe(this, Observer {
            showProgressScreen("Initializing Transaction...")
            hideRefundLayout()
            if (manualRefund)
                this.viewModel.manualRefund(
                    configurationItem = it,
                    manualRefundRequestItem = prepareManualRefundRequestItem()
                )
            else
                this.viewModel.refund(it, amount, originalRrn!!, getTransactionType())
        })

        initBaseLiveDataObservers()
    }

    private fun prepareManualRefundRequestItem(): ManualRefundRequestItem {
        return ManualRefundRequestItem(
            amount = amount,
            transactionType = getTransactionType(),
            originalRetrievalReferenceNumber = originalRrn,
            cardNumber = cardNumber,
            expiryDate = expiryDate,
            cvv = cvv
        )
    }

    private fun initViews() {
        iv_refund_back.setOnClickListener {
            this.hideKeyboard(et_refund_retrieval_reference)
            checkForCardPresent()
        }

        btn_refund_cancel.setOnClickListener {
            this.hideKeyboard(et_refund_retrieval_reference)
            btn_refund_cancel.handleDebounce()
            checkForCardPresent()
        }

        btn_refund_confirm.setOnClickListener {
            btn_refund_confirm.handleDebounce()
            this.showProgressScreen("Validating Retrieval Reference Number")
            originalRrn = et_refund_retrieval_reference.text.toString()
            this.viewModel.validateRetrievalReferenceNumber(originalRrn!!)
        }
    }

    override fun getTransactionType(): TransactionType = TransactionType.REFUND

    override fun getTransactionViewModel(): BaseTransactionViewModel = this.viewModel

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayout(): Int = R.layout.activity_base_transaction

    override fun getViewModel(): RefundViewModel =
        ViewModelProviders.of(this)[RefundViewModel::class.java]

    override fun onPositiveButtonClick() {
        this.showProgressScreen("Printing Customer Copy..")
        this.viewModel.printCustomerCopy()
    }

    override fun onNegativeButtonClick() {
        checkForCardPresent()
    }

    override fun onManualButtonClicked() {
        this.viewModel.shouldDispose = true
        this.manualRefund = true
        this.viewModel.doCleanUp(BaseTransactionViewModel.Task.MANUAL_TRANSACTION)
    }

    override fun onCancelButtonClicked() {
        this.viewModel.shouldDispose = true
        this.viewModel.doCleanUp(BaseTransactionViewModel.Task.RETURN_TO_DASHBOARD)
    }

    override fun onAmountActivityResult(data: Intent?) {
        data?.let {
            val amountInString =
                data.getStringExtra(getString(R.string.intent_confirmed_amount))
            amount = amountInString?.toBigDecimal()!!
            viewModel.getConfigurationItem()
        }
    }

    override fun onManualActivityResult(data: Intent?) {
        data?.let {
            this.cardNumber = data.getStringExtra(getString(R.string.title_card_number))!!
            this.expiryDate = data.getStringExtra(getString(R.string.title_expiry_date))!!
            this.cvv = data.getStringExtra(getString(R.string.title_cvv))!!
            this.viewModel.getConfigurationItem()
        }
    }
}