package global.citytech.finpos.processor.neps.reconciliation.clear;

import java.util.List;

import global.citytech.finpos.processor.neps.batchupload.BatchUploadReceipt;
import global.citytech.finpos.processor.neps.batchupload.BatchUploadReceiptPrintHandler;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.SettlementStatus;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reconciliation.clear.BatchClearRequester;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.utility.StringUtils;

public class NepsBatchClearUseCase implements BatchClearRequester<BatchClearRequestModel, BatchClearResponseModel>,
        UseCase<BatchClearRequestModel, BatchClearResponseModel> {

    private final TerminalRepository terminalRepository;

    public NepsBatchClearUseCase(TerminalRepository terminalRepository) {
        this.terminalRepository = terminalRepository;
    }

    @Override
    public BatchClearResponseModel execute(BatchClearRequestModel batchClearRequestModel) {
        try {
            String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
            List<TransactionLog> transactionLogs = batchClearRequestModel.getTransactionRepository()
                    .getTransactionLogsByBatchNumber(batchNumber);
            int totalSize = transactionLogs.size();
            if (totalSize == 0)
                return new BatchClearResponseModel(true, "No transactions in batch");
            int count = 1;
            for (TransactionLog transactionLog : transactionLogs) {
                this.printBatchReceipt(transactionLog, batchClearRequestModel.getPrinterService(),
                        count == 1, count == totalSize, batchNumber);
                count++;
            }
            batchClearRequestModel.getReconciliationRepository().updateBatch(batchNumber, "", "");
            this.terminalRepository.incrementReconciliationBatchNumber();
        } catch (Exception e) {
            e.printStackTrace();
            return new BatchClearResponseModel(false, "Failed to clear batch");
        }
        batchClearRequestModel.getReconciliationRepository().saveSettlementStatus(SettlementStatus.OPEN);
        return new BatchClearResponseModel(true, "Batch cleared successfully");
    }

    private void printBatchReceipt(TransactionLog transactionLog, PrinterService printerService,
                                   boolean printBatchUploadHeader, boolean feedPaperAfterFinish,
                                   String batchNumber) {
        BatchUploadReceipt batchUploadReceipt = BatchUploadReceipt.Builder.newInstance()
                .batchNumber(batchNumber)
                .amount(transactionLog.getReceiptLog().getAmountWithCurrency())
                .approvalCode(transactionLog.getAuthCode())
                .cardNumber(StringUtils.encodeAccountNumber(transactionLog.getReadCardResponse().getCardDetails().getPrimaryAccountNumber()))
                .cardScheme(transactionLog.getReadCardResponse().getCardDetails().getCardScheme().getCardScheme())
                .invoiceNumber(transactionLog.getInvoiceNumber())
                .responseCode(transactionLog.getResponseCode())
                .transactionType(transactionLog.getTransactionType().getPrintName())
                .transactionDate(transactionLog.getTransactionDate())
                .transactionTime(transactionLog.getTransactionTime())
                .build();
        new BatchUploadReceiptPrintHandler(printerService).printBatchUploadReceipt(batchUploadReceipt,
                printBatchUploadHeader, feedPaperAfterFinish);
    }
}
