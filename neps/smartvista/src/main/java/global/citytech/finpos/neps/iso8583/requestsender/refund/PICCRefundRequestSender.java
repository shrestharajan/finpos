package global.citytech.finpos.neps.iso8583.requestsender.refund;

import global.citytech.finpos.neps.iso8583.requestsender.cardtypesenders.PICCISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.purchases.PurchaseRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.CardTypeISOMessageUpdater;
import global.citytech.finposframework.iso8583.RequestContext;

/**
 * Created by Saurav Ghimire on 2/1/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class PICCRefundRequestSender extends RefundRequestSender {
    public PICCRefundRequestSender(RequestContext context) {
        super(context);
    }

    @Override
    protected String getClassName() {
        return PICCRefundRequestSender.class.getName();
    }

    @Override
    protected CardTypeISOMessageUpdater getCardTypeSender() {
        return new PICCISOMessageUpdater();
    }

}
