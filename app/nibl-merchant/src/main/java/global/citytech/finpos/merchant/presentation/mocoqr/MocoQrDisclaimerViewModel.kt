package global.citytech.finpos.merchant.presentation.mocoqr

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.AppNotificationRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.Action
import global.citytech.finpos.merchant.domain.model.app.ActivityLog
import global.citytech.finpos.merchant.domain.usecase.app.AppNotificationUseCase
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.AppNotificationDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.service.ActivityLogPushService
import global.citytech.finposframework.usecases.activity.ActivityLogStatus
import global.citytech.finposframework.utility.JsonUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.sql.Timestamp
import java.util.*
import kotlin.collections.HashMap

class MocoQrDisclaimerViewModel(val context: Application) : BaseAndroidViewModel(context) {

    val serviceFeeItemsLiveData by lazy { MutableLiveData<List<ServiceFeeItem>>() }
    val closeActivity by lazy { MutableLiveData<Boolean>() }

    private val localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    private val appNotificationUseCase = AppNotificationUseCase(
        AppNotificationRepositoryImpl(
            AppNotificationDataSourceImpl(context)
        )
    )

    fun prepareServiceFeeItems() {
        val serviceFeeItems = mutableListOf<ServiceFeeItem>()
        serviceFeeItems.add(ServiceFeeItem("Payment Network", "Domestic", "International"))
        serviceFeeItems.add(ServiceFeeItem("AliPay", "-", "2.20%"))
        serviceFeeItems.add(ServiceFeeItem("VISA (Scan to Pay)", "1.20%", "2.20%"))
        serviceFeeItems.add(ServiceFeeItem("Union Pay International", "1.20%", "2.20%"))
        serviceFeeItems.add(ServiceFeeItem("NCHL", "Free", "-"))
        serviceFeeItemsLiveData.value = serviceFeeItems
    }

    fun acceptMocoQrPayments() {
        updateDisclaimerShownStatus(true)
        updateQRDisclaimerShowStatus(false)
        removeQrDisclaimerNotification()
        pushActivityLogToFinPos(true)
    }

    private fun updateDisclaimerShownStatus(isShown: Boolean) {
        localDataUseCase.saveDisclaimerShownStatusPref(isShown)
    }

    fun acceptLaterMocoQrPayments() {
        updateQRDisclaimerShowStatus(true)
        closeActivity.value = true
    }

    fun declineMocoQrPayments() {
        updateQRDisclaimerShowStatus(false)
        removeQrDisclaimerNotification()
        pushActivityLogToFinPos(false)
    }

    private fun updateQRDisclaimerShowStatus(showDisclaimer: Boolean) {
        localDataUseCase.updateShowQrDisclaimerSetting(showDisclaimer)
    }

    private fun pushActivityLogToFinPos(disclaimerAccepted: Boolean) {
        isLoading.value = true
        compositeDisposable.add(
            localDataUseCase.insertToActivityLog(
                ActivityLog(
                    id = System.currentTimeMillis().toString(),
                    stan = "0",
                    type = "QR_DISCLAIMER",
                    merchantId = DeviceConfiguration.get().merchantId!!,
                    terminalId = DeviceConfiguration.get().terminalId!!,
                    batchNumber = "0",
                    amount = 0,
                    responseCode = "",
                    isoRequest = "",
                    isoResponse = "",
                    status = if (disclaimerAccepted) ActivityLogStatus.SUCCESS.name else ActivityLogStatus.FAILED.name,
                    remarks = prepareRemarks(disclaimerAccepted),
                    posDateTime = Timestamp(Date().time).toString(),
                    additionalData = prepareAdditionalDataWithSerialNumber()
                )
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    ActivityLogPushService.pushActivityLog()
                    if (disclaimerAccepted)
                        message.value = context.getString(R.string.msg_qr_disclaimer_accepted)
                    else
                        closeActivity.value = true
                }, {
                    isLoading.value = false
                })
        )
    }

    private fun prepareRemarks(disclaimerShown: Boolean): String {
        return if (disclaimerShown)
            "MOCO QR DISCLAIMER ACCEPTED"
        else
            "MOCO QR DISCLAIMER DECLINED"
    }

    private fun prepareAdditionalDataWithSerialNumber(): String? {
        val map = HashMap<String, Any>()
        map["serialNumber"] = NiblMerchant.INSTANCE.iPlatformManager.serialNumber
        return JsonUtils.toJsonObj(map)
    }

    private fun removeQrDisclaimerNotification() {
        appNotificationUseCase.removeNotificationByAction(Action.QR_DISCLAIMER)
    }
}