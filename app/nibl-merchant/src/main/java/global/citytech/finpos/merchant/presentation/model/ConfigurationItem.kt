package global.citytech.finpos.merchant.presentation.model

import global.citytech.finpos.merchant.domain.usecase.app.Configuration

data class ConfigurationItem(
    var hosts: List<HostItem>? = null,
    var merchants: List<MerchantItem>? = null,
    var logos: List<LogoItem>? = null
)

fun Configuration.mapToPresentation(): ConfigurationItem =
    ConfigurationItem(
        hosts = hostList.mapToPresentation(),
        merchants = merchantList.mapToPresentation(),
        logos = logos.mapToPresentation()
    )

fun List<Configuration>.mapToPresentation(): List<ConfigurationItem> =
    map { it.mapToPresentation() }