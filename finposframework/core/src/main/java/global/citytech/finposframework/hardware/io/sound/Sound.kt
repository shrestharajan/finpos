package global.citytech.finposframework.hardware.io.sound

/**
 * Created by Rishav Chudal on 6/4/20.
 */
enum class Sound {
    SUCCESS, FAILURE
}