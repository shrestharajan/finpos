package global.citytech.nepsadmin

object AppConstant {
    const val MERCHANT_PACKAGE_NAME = "global.citytech.finpos.nepsmerchant"
}

enum class DeviceConfigurationStatus(val message:String){
    DownloadingConfiguration("Downloading Configuration"),
    ConfigurationDownloaded("Configuration Downloaded"),
    SavingParameters("Saving Parameters"),
    ParametersSaved("Parameters Saved"),
    InjectingKeys("Injecting Keys"),
    KeysInjectedSuccesfully("Keys Injected"),
    ConfigurationFailed("Configuration Failed"),
    ConfigurationCanceled("Configuration Canceled")
}