package global.citytech.finpos.processor.nibl.receipt.customercopy;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.switches.receipt.customercopy.CustomerCopyResponseParameter;
import global.citytech.finposframework.supports.UseCase;

/**
 * Created by Unique Shakya on 9/2/2020.
 */
public class CustomerCopyResponseModel implements UseCase.Response, CustomerCopyResponseParameter {

    private Result result;
    private String message;

    public CustomerCopyResponseModel(Result result, String message) {
        this.result = result;
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }
}
