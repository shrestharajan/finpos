package global.citytech.neps.presentation.alertdialogs

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import global.citytech.neps.R
import global.citytech.neps.presentation.purchase.PurchaseActivity
import global.citytech.neps.presentation.purchase.PurchaseViewModel
import kotlinx.android.synthetic.main.dialog_pos_entry_selection.view.*
import java.lang.ref.WeakReference

/**
 * Created by Rishav Chudal on 6/26/20.
 */
class PosEntrySelectionDialog constructor(
    val message: String,
    val isPICCAllowed: Boolean,
    val isManualAllowed: Boolean
) {
    private lateinit var tvMessage: TextView
    private lateinit var btnCancel: Button
    private lateinit var btnManual: Button
    private lateinit var ivPICC: ImageView
    private var alertDialog: AlertDialog? = null
    private lateinit var viewModel: PurchaseViewModel

    fun show(activityWeakReference: WeakReference<Activity>) {
        val dialogBuilder = AlertDialog.Builder(activityWeakReference.get())
        val inflater = activityWeakReference.get()!!.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_pos_entry_selection, null)
        dialogBuilder.setView(dialogView)
        initViewModel(activityWeakReference)
        initViews(dialogView)
        setMessage()
        setPICCImage()
        setManualButton()
        handleClickEvents()
        alertDialog = dialogBuilder.create()
        alertDialog!!.setCancelable(false)
        alertDialog!!.show()
    }

    fun isStillVisible(): Boolean {
        return (alertDialog != null && alertDialog!!.isShowing)
    }

    fun hideDialog() {
        if (isStillVisible()) {
            alertDialog!!.hide()
        }
    }

    private fun initViewModel(activityWeakReference: WeakReference<Activity>) {
        viewModel = ViewModelProviders
            .of(activityWeakReference.get() as PurchaseActivity)[PurchaseViewModel::class.java]
    }

    private fun initViews(dialog: View) {
        tvMessage = dialog.tv_progress_msg
        btnCancel = dialog.btn_cancel
        btnManual = dialog.btn_manual
        ivPICC = dialog.img_contactless
    }

    private fun setMessage() {
        tvMessage.text = message
    }

    private fun setPICCImage() {
        if (isPICCAllowed) {
            ivPICC.visibility = View.VISIBLE
        } else {
            ivPICC.visibility = View.GONE
        }
    }

    private fun setManualButton() {
        if (isManualAllowed) {
            btnManual.visibility = View.VISIBLE
        } else {
            btnManual.visibility = View.GONE
        }
    }

    private fun handleClickEvents() {
        btnManual.setOnClickListener{
            hide()
            viewModel.isManualButtonClickedInDialog.value = true
        }

        btnCancel.setOnClickListener {
            hide()
            viewModel.isCancelButtonClickedInDialog.value = true
        }
    }

    private fun hide() {
        if (alertDialog != null && alertDialog!!.isShowing) {
            alertDialog!!.dismiss()
        }
    }
}