package global.citytech.finposframework.switches.receipt.tmslogs

/**
 * Created by Rishav Chudal on 11/6/20.
 */
enum class TmsLogsParameter {
    SWITCH_PARAMETERS,
    AID,
    EMV_KEYS,
    CARD_SCHEME
}