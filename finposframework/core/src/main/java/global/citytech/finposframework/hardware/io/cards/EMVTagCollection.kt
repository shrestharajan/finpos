package global.citytech.finposframework.hardware.io.cards

import java.util.*
import kotlin.collections.HashMap

/**
 * Created by Rishav Chudal on 6/8/20.
 */
class EMVTagCollection {
    private var tags: MutableMap<Int, EmvTag> = mutableMapOf()

    fun getTags(): MutableMap<Int, EmvTag> = tags

    fun add(emvTag: EmvTag) {
        if (!tags.containsKey(emvTag.tag)) {
            tags.put(emvTag.tag, emvTag)
        }
    }

    fun remove(tagId: Int) {
        tags.remove(tagId)
    }

    fun get(tagId: Int): EmvTag {
        return tags.get(tagId)!!
    }

    fun toMap(): HashMap<String, String> {
        val map = HashMap<String, String>()
        for (emvtag in tags.values) {
            map.put(Integer.toHexString(emvtag.tag).toUpperCase(Locale.getDefault()), emvtag.data)
        }
        return map
    }
}