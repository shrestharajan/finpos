package global.citytech.finpos.neps.iso8583.requestsender.transactiontypesenders;

import global.citytech.finpos.neps.iso8583.ProcessingCode;
import global.citytech.finpos.neps.iso8583.requestsender.authcompletion.AuthCompletionIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.refund.RefundIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionTypeISOMessageUpdater;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;

public class RefundIsoMessageUpdater implements TransactionTypeISOMessageUpdater {
    @Override
    public Iso8583Msg update(TransactionIsoRequest transactionIsoRequest, Iso8583Msg iso8583Msg) {

        //Add any Purchase specific Data Elements
        iso8583Msg.addField(new DataElement(
                DataElement.RETRIEVAL_REFERENCE_NUMBER,
                ((RefundIsoRequest) transactionIsoRequest).getOriginalRrn()));
        return iso8583Msg;
    }

    @Override
    public String getProcessingCode() {
        return ProcessingCode.REFUND.getCode();
    }
}
