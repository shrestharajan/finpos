package global.citytech.finposframework.hardware.io.printer

/**
 * Created by Unique Shakya on 7/13/2021.
 */
class BarCodeReceipt(val data: String): Printable {
    private val isQrCode = false
    private val isAllow = true
    private val isImage = false
    private val isFeedPaper = false
    private val isBase64Image = false
    private val isBarCode = true
    
    override fun isAllow(): Boolean {
        return isAllow
    }

    override fun isImage(): Boolean {
        return isImage
    }

    override fun isQrCode(): Boolean {
        return isQrCode
    }

    override fun isFeedPaper(): Boolean {
        return isFeedPaper
    }

    override fun isBase64Image(): Boolean {
        return isBase64Image
    }

    override fun isBarCode(): Boolean {
        return isBarCode
    }

    override fun getData(): Any? {
        return data
    }

    companion object{
        fun getBarCodeInstance(data: String): BarCodeReceipt {
            return BarCodeReceipt(data)
        }
    }
}