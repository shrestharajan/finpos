package global.citytech.finpos.merchant.domain.model.reconciliation.clear

data class BatchClearResponseEntity (
    val isSuccess: Boolean,
    val message: String
)