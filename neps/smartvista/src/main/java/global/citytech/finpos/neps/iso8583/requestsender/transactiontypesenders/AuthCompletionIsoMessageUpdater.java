package global.citytech.finpos.neps.iso8583.requestsender.transactiontypesenders;

import global.citytech.finpos.neps.iso8583.PosEntryModeConfig;
import global.citytech.finpos.neps.iso8583.ProcessingCode;
import global.citytech.finpos.neps.iso8583.requestsender.authcompletion.AuthCompletionIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionTypeISOMessageUpdater;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.switches.PosEntryMode;

public class AuthCompletionIsoMessageUpdater implements TransactionTypeISOMessageUpdater {
    @Override
    public Iso8583Msg update(TransactionIsoRequest transactionIsoRequest, Iso8583Msg msg) {

        //Add any Auth Completion specific Data Elements

        msg.addField(new DataElement(
                DataElement.POS_ENTRY_MODE,
                PosEntryModeConfig.getValue(PosEntryMode.ICC)));
        msg.addField(new DataElement(
                DataElement.TRACK_II_DATA,
                transactionIsoRequest.getTrack2Data()));
        msg.addField(new DataElement(
                DataElement.RETRIEVAL_REFERENCE_NUMBER,
                ((AuthCompletionIsoRequest) transactionIsoRequest).getOriginalRRN()));
        msg.addField(new DataElement(
                DataElement.ICC_SYSTEM_RELATED_DATA,
                transactionIsoRequest.getEmvData()
        ));
        return msg;
    }

    @Override
    public String getProcessingCode() {
        return ProcessingCode.PRE_AUTHORIZATION_COMPLETION.getCode();
    }
}
