package global.citytech.finposframework.hardware.io.cards.cvm;

public enum CvmCondition {

    ALWAYS,
    IF_UNATTENDED_CASH,
    IF_NOT_UNATTENDED_NOT_MANUAL_NOT_CASHBACK,
    IF_TERMINAL_SUPPORTS_CVM,
    IF_MANUAL_CASH,
    IF_CASHBACK,
    IF_UNDER_X,
    IF_OVER_X,
    IF_UNDER_Y,
    IF_OVER_Y,
    RFU,
    RFU_PAYMENT_SYSTEMS,
    UNKNOWN
}