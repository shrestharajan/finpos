package global.citytech.finpos.merchant.presentation.model.purchase

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

/**
 * Created by Unique Shakya on 8/26/2020.
 */
data class PurchaseRequestItem(
    val amount: BigDecimal? = null,
    val transactionType: TransactionType? = null,
    val isEmiTransaction: Boolean = false,
    val emiInfo: String = "",
    val vatInfo: String = ""
)