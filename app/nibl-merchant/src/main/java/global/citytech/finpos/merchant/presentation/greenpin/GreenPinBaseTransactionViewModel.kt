package global.citytech.finpos.merchant.presentation.greenpin

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.common.Base64
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.receipt.CustomerCopyRepositoryImpl
import global.citytech.finpos.merchant.data.repository.device.DeviceRepositoryImpl
import global.citytech.finpos.merchant.domain.model.receipt.CustomerCopyResponseEntity
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.core.receipt.CoreCustomerCopyUseCase
import global.citytech.finpos.merchant.domain.usecase.device.DeviceUseCase
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.receipt.CustomerCopyDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.device.DeviceConfigurationSourceImpl
import global.citytech.finpos.merchant.framework.datasource.device.DeviceControllerImpl
import global.citytech.finpos.merchant.presentation.alertdialogs.PosEntryMode
import global.citytech.finpos.merchant.presentation.alertdialogs.PosEntrySelection
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finpos.merchant.service.NetworkConnectionReceiver
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.usecases.TransactionType
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

abstract class GreenPinBaseTransactionViewModel(val context: Application) :
    BaseAndroidViewModel(context) {

    private val logger = Logger.getLogger(GreenPinBaseTransactionViewModel::class.java.name)

    val posEntrySelection by lazy { MutableLiveData<PosEntrySelection>() }
    val configurationItem by lazy { MutableLiveData<ConfigurationItem>() }
    val customerCopyPrintMessage by lazy { MutableLiveData<String>() }
    val errorMessage by lazy { MutableLiveData<String>() }
    val successMessage by lazy { MutableLiveData<String>() }
    val failureMessage by lazy { MutableLiveData<String>() }
    private val paymentError by lazy { MutableLiveData<PosError>() }
    var transactionType: TransactionType? = null
    var shouldDispose: Boolean = false
    var task: Task = Task.RETURN_TO_DASHBOARD
    val doReturnToDashboard by lazy { MutableLiveData<Boolean>() }
    val cleanupCompletion by lazy { MutableLiveData<Boolean>() }
    val cardPresent by lazy { MutableLiveData<Boolean>() }
    val cardPresentForPinSet by lazy { MutableLiveData<Boolean>() }
    private var deviceController = DeviceControllerImpl(context)
    val terminalIsConfigured by lazy { MutableLiveData<Boolean>() }
    val appConfiguration by lazy { MutableLiveData<ConfigurationItem>() }
    val defaultBankDisplayImage by lazy { MutableLiveData<Boolean>() }
    val bankDisplayImage by lazy { MutableLiveData<ByteArray>() }
    lateinit var terminalRepository: TerminalRepositoryImpl
    var currencyCode=""


    private var localDataUseCase: LocalDataUseCase =
        LocalDataUseCase(LocalRepositoryImpl(LocalDatabaseSourceImpl(), PreferenceManager))


    var deviceConfigurationUseCase = DeviceUseCase(
        DeviceRepositoryImpl(DeviceConfigurationSourceImpl(context))
    )

    fun retrieveAllowedCardTypes(transactionType: TransactionType) {
        val mapOfCardTypes = DeviceConfiguration.get().purchaseCardTypeMap
        var cardTypes = mapOfCardTypes?.get(transactionType)
        if (cardTypes == null || cardTypes.isEmpty())
            cardTypes = defaultCardTypes()
        val stringBuilder = StringBuilder()
        if (cardTypes.contains(CardType.MAG)) {
            stringBuilder.append("SWIPE")
        }
        if (cardTypes.contains(CardType.ICC)) {
            if (stringBuilder.toString().isNotEmpty())
                stringBuilder.append(",  ")
            stringBuilder.append("INSERT")
        }
        if (cardTypes.contains(CardType.PICC)) {
            if (stringBuilder.toString().isNotEmpty())
                stringBuilder.append(",  ")
            stringBuilder.append("WAVE ")
        }
        stringBuilder.append("CARD")

        val posEntrySelection = PosEntrySelection(
            stringBuilder.toString(),
            cardTypes.contains(CardType.PICC),
            cardTypes.contains(CardType.MANUAL),
            PosEntryMode.ACCEPT_ALL
        )
        this.posEntrySelection.postValue(posEntrySelection)
    }

    fun getTerminalConfiguredStatusAndProceed() {
        this.terminalIsConfigured.value =
            localDataUseCase.isTerminalConfigured(defaultValue = false)
    }

    private fun defaultCardTypes(): List<CardType> {
        val cardTypes = mutableListOf<CardType>()
        cardTypes.add(CardType.MAG)
        cardTypes.add(CardType.ICC)
        cardTypes.add(CardType.PICC)
        cardTypes.add(CardType.MANUAL)
        return cardTypes
    }

    protected fun completePreviousTask() {
        if (task == Task.RETURN_TO_DASHBOARD)
            doReturnToDashboard.value = true
    }

    fun getConfigurationItem() {
        disableNetworkPing()
        compositeDisposable.add(localDataUseCase.getWholeConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.mapToPresentation() }
            .subscribe({
                configurationItem.value = it
            },
                {
                    paymentError.value = PosError.DEVICE_ERROR_NOT_READY
                    errorMessage.value = it.message
                })
        )
    }

    fun doCleanUp(task: Task) = Completable
        .fromAction {
            this.task = task
            deviceConfigurationUseCase.closeUpIO()
        }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            { this.onCleanUpComplete(task) },
            { this.onCleanUpError(it, task) }
        )

    private fun onCleanUpComplete(task: Task) {
        logger.log("corelogger Clean Up Complete")
        cleanupCompletion.value = true
        logger.log("Task :::: " + task.name)
        this.task = task
    }

    private fun onCleanUpError(throwable: Throwable, task: Task) {
        logger.log("corelogger Clean Up Error ::: ".plus(throwable.message))
        cleanupCompletion.value = false
        this.doCleanUp(task)
    }

    fun enableNetworkPing() {
        NiblMerchant.INSTANCE.disableNetworkPing = false
    }

    private fun disableNetworkPing() {
        NiblMerchant.INSTANCE.disableNetworkPing = true
    }

    fun checkForCardPresent() {
        isLoading.value = true
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Boolean> {
                isCardPresentTask(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    logger.log("corelogger Check for Card Complete")
                    isLoading.value = false
                    cardPresent.value = it
                }, {
                    logger.log("corelogger Check for Card Error")
                    isLoading.value = false
                    it.printStackTrace()
                    checkForCardPresent()
                })
        )
    }


    fun checkForCardPresentForPinSet() {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Boolean> {
                isCardPresentTask(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    logger.log("corelogger Check for Card Complete")
                    isLoading.value = false
                    cardPresentForPinSet.value = it

                }, {
                    logger.log("corelogger Check for Card Error")
                    isLoading.value = false
                    it.printStackTrace()
                })
        )
    }

    private fun isCardPresentTask(it: ObservableEmitter<Boolean>) {
        it.onNext(deviceController.isIccCardPresent)
        it.onComplete()
    }


    enum class Task {
        RETURN_TO_DASHBOARD,
        CANCEL_CARD_TRANSACTION,
        GREEN_PIN
    }


    fun getConfiguration(): Boolean {
        return compositeDisposable.add(
            localDataUseCase.getWholeConfiguration()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe({
                    appConfiguration.value = it
                    NetworkConnectionReceiver.networkConfiguration = it
                    retrieveBankDisplayImage(it)
                },
                    {
                        this.isLoading.value = false
                        message.value = it.message
                    })
        )
    }

    private fun retrieveBankDisplayImage(it: ConfigurationItem?) {
        it?.let { configItem ->
            configItem.logos?.let {
                if (it.isNotEmpty()) {
                    it[0].displayLogo?.let { s ->
                        retrieveBankDisplayImageByteArray(s)
                    } ?: run {
                        defaultBankDisplayImage.value = true
                    }
                } else {
                    defaultBankDisplayImage.value = true
                }
            } ?: run {
                defaultBankDisplayImage.value = true
            }
        } ?: run {
            defaultBankDisplayImage.value = true
        }
    }

    private fun retrieveBankDisplayImageByteArray(s: String) {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<ByteArray> {
                it.onNext(Base64.decode(s))
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    bankDisplayImage.value = it
                }, {
                    isLoading.value = false
                    defaultBankDisplayImage.value = true
                })
        )
    }
}