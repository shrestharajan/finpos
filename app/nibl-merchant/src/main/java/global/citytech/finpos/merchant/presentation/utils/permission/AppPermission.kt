package global.citytech.tms.utility.permission

import android.Manifest
import global.citytech.finpos.merchant.presentation.splash.SplashScreenActivity


/**
 * Created by Saurav Ghimire on 4/21/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


data class AppPermission(
    val permission:String
)

object PermissionConstant{

    val permissions = listOf(
        AppPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE),
        AppPermission(Manifest.permission.READ_EXTERNAL_STORAGE),
    )
}
