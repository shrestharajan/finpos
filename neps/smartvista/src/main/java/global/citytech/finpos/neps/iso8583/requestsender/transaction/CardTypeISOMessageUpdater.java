package global.citytech.finpos.neps.iso8583.requestsender.transaction;

import global.citytech.finposframework.iso8583.Iso8583Msg;

public interface CardTypeISOMessageUpdater {
    Iso8583Msg update(TransactionIsoRequest transactionIsoRequest, Iso8583Msg iso8583Msg);
}
