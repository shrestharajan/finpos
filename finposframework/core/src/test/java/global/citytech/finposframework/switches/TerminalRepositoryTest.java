package global.citytech.finposframework.switches;

import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;

/**
 * @author rajudhital on 4/3/20
 * @project pos-switch-intregator
 */
public class TerminalRepositoryTest implements TerminalRepository {
  @Override
  public String getSystemTraceAuditNumber() {
    return null;
  }

  @Override
  public void incrementSystemTraceAuditNumber() {

  }

  @Override
  public TerminalInfo findTerminalInfo() {
    return null;
  }

  @Override
  public HostInfo findPrimaryHost() {
    return null;
  }

  @Override
  public HostInfo findSecondaryHost() {
    return null;
  }

  @Override
  public String getReconciliationBatchNumber() {
    return null;
  }

  @Override
  public void incrementReconciliationBatchNumber() {

  }

  @Override
  public String getRetrievalReferenceNumber() {
    return null;
  }

  @Override
  public void incrementRetrievalReferenceNumber() {

  }

  @Override
  public String getInvoiceNumber() {
    return null;
  }

  @Override
  public void incrementInvoiceNumber() {

  }
}
