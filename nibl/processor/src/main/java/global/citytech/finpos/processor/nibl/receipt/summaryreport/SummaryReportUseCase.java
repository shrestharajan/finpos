package global.citytech.finpos.processor.nibl.receipt.summaryreport;

import java.util.List;

import global.citytech.finpos.processor.nibl.posmode.PosModeRequestModel;
import global.citytech.finpos.processor.nibl.posmode.PosModeResponseModel;
import global.citytech.finpos.processor.nibl.posmode.PosModeUseCase;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.summaryreport.SummaryReportRequester;
import global.citytech.finposframework.usecases.CardSchemeType;

/**
 * Created by Unique Shakya on 1/5/2021.
 * Modified by Rishav Chudal on 5/6/2021
 */
public class SummaryReportUseCase implements SummaryReportRequester<SummaryReportRequestModel, SummaryReportResponseModel>,
        UseCase<SummaryReportRequestModel, SummaryReportResponseModel> {

    private final TerminalRepository terminalRepository;
    private SummaryReportRequestModel summaryReportRequestModel;
    private SummaryReportResponseModel summaryReportResponseModel;

    public SummaryReportUseCase(TerminalRepository terminalRepository) {
        this.terminalRepository = terminalRepository;
    }

    @Override
    public SummaryReportResponseModel execute(SummaryReportRequestModel summaryReportRequestModel) {
        try {
            prepareSummaryReportAndPrint(summaryReportRequestModel);
        } catch (PosException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
        return this.summaryReportResponseModel;
    }

    private void prepareSummaryReportAndPrint(SummaryReportRequestModel summaryReportRequestModel) {
        this.summaryReportRequestModel = summaryReportRequestModel;
        SummaryReportReceipt summaryReportReceipt = prepareSummaryReportReceipt();
        checkSummaryReportDataAndPrint(summaryReportReceipt);
    }

    private SummaryReportReceipt prepareSummaryReportReceipt() {
        String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
        SummaryReportReceiptMaker summaryReportReceiptMaker = new SummaryReportReceiptMaker(
                this.terminalRepository,
                this.summaryReportRequestModel.getReconciliationRepository()
        );
        List<CardSchemeType> cardSchemeTypes = summaryReportRequestModel.getApplicationRepository()
                .getSupportedCardSchemes();
        String merchantCategoryCode = summaryReportRequestModel.getApplicationRepository().retrieveEmvParameterRequest().getMerchantCategoryCode();
        PosModeUseCase posModeUseCase = new PosModeUseCase();
        PosModeResponseModel posModeResponseModel = posModeUseCase.execute(new PosModeRequestModel(merchantCategoryCode));
        return summaryReportReceiptMaker.prepare(batchNumber, cardSchemeTypes, posModeResponseModel.getPosMode());
    }

    private void checkSummaryReportDataAndPrint(SummaryReportReceipt summaryReportReceipt) {
        if (summaryReportIsEmpty(summaryReportReceipt)) {
            assignSummaryResponseWithNoTransactionMessage();
        } else {
            printSummaryReport(summaryReportReceipt);
        }
    }

    private boolean summaryReportIsEmpty(SummaryReportReceipt summaryReportReceipt) {
        return summaryReportReceipt.getSummaryReport().getMap().isEmpty();
    }

    private void assignSummaryResponseWithNoTransactionMessage() {
        throw new PosException(PosError.DEVICE_ERROR_NO_RECENT_TRANSACTION);
    }

    private void printSummaryReport(SummaryReportReceipt summaryReportReceipt) {
        SummaryReportReceiptHandler summaryReportReceiptHandler
                = new SummaryReportReceiptHandler(summaryReportRequestModel.getPrinterService());
        PrinterResponse printerResponse = summaryReportReceiptHandler.print(summaryReportReceipt);
        assignSummaryResponseOnFinishPrinting(printerResponse);
    }

    private void assignSummaryResponseOnFinishPrinting(PrinterResponse printerResponse) {
        this.summaryReportResponseModel = new SummaryReportResponseModel(
                printerResponse.getResult(),
                printerResponse.getMessage()
        );
    }
}
