package global.citytech.finpos.device.qualcomm.emv.aid

import android.content.Context
import com.ftpos.library.smartpos.bean.CVisaBean
import com.ftpos.library.smartpos.device.Device
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.emv.EmvTags
import com.ftpos.library.smartpos.emv.IActionFlag
import global.citytech.common.extensions.defaultOrEmptyValue
import global.citytech.common.extensions.isNullOrEmptyOrBlank
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.CHECK_FLAG
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.DO_NOT_CHECK_FLAG
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finpos.device.qualcomm.utils.FeitianUtils
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.PAYWAVE_KERNEL_ID
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.TRANSACTION_TYPE_PURCHASE_SALE
import global.citytech.finposframework.hardware.emv.aid.AidParameters
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import global.citytech.finposframework.utility.AmountUtils

/**
 * Created by Rishav Chudal on 4/5/21.
 * PayWave Kernel - Visa Contactless Kernel - Kernel 03
 */
class FeitianPayWaveKernelConfigHandler constructor(
    private val context: Context,
    private val emv: Emv,
    private val aidParameters: AidParameters,
    private val emvParameters: EmvParametersRequest,
    private val supportedTransactionTypes: List<String>
) : FeitianLogger.Listener {

    private var finalResult = 0
    private val logger = FeitianLogger(this)
    private val device: Device = Device.getInstance(context.applicationContext)

    fun injectPayWaveKernelConfigurations(): Int {
        var successCount = 0
        supportedTransactionTypes.forEach {
            injectKernelConfigurationsForTransType(it)
        }
        if (finalResult == 0) {
            successCount++
        }
        return successCount
    }

    private fun injectKernelConfigurationsForTransType(transactionType: String) {
        val configurationsByteArray = preparePayWaveKernelConfigurationsInByteArray(
            transactionType
        )
        val resultInjectCLParams = emv.manageEmvclAppParameters(
            IActionFlag.ADD,
            configurationsByteArray
        )
        logger.logWithClassName(
            "PayWave kernel config injection result ::: ".plus(resultInjectCLParams)
        )
        finalResult += resultInjectCLParams
    }

    private fun preparePayWaveKernelConfigurationsInByteArray(
        transactionType: String
    ): ByteArray? {
        logger.logWithClassName("***** Contactless Params *****")
        val cVisaBean = CVisaBean()

        val aid9F06 = aidParameters.aid.defaultOrEmptyValue()
        logger.debugWithClassName("AID ::: ".plus(aid9F06))
        cVisaBean.setmAID_9F06(aid9F06)

        val kernelId = PAYWAVE_KERNEL_ID //hardcoded as the AID parameters don't have this parameter
        this.logger.debugWithClassName("Kernel ID ::: ".plus(kernelId))
        cVisaBean.setmKernelID_1F60(kernelId)

        val appSelectionIndicator =
            DO_NOT_CHECK_FLAG //hardcoded as the AID parameters don't have this parameter
        logger.debugWithClassName("App Selection Indicator ::: ".plus(appSelectionIndicator))
        cVisaBean.setmASI_1F14(appSelectionIndicator)

        var transType = transactionType
        if (transType.isNullOrEmptyOrBlank()) {
            transType = TRANSACTION_TYPE_PURCHASE_SALE
        }
        logger.debugWithClassName("Transaction Type 9C ::: ".plus(transType))
        cVisaBean.setmTransType_9C(transType)

        var statusCheck = ""
        logger.debugWithClassName("Status Check Flag ::: ".plus(statusCheck))
        if (statusCheck.isNullOrEmptyOrBlank()) {
            statusCheck = DO_NOT_CHECK_FLAG
        }
        logger.debugWithClassName("Formatted Status Check Flag ::: ".plus(statusCheck))
        cVisaBean.setmStatusCheck_1F32(statusCheck)

        val zeroAmountAllowed = CHECK_FLAG //Setting by default
        logger.debugWithClassName("Zero amount allowed ::: ".plus(zeroAmountAllowed))
        cVisaBean.setmZeroAmountAllowed_1F33(zeroAmountAllowed)

        val onlineOptionOnZeroAmount = CHECK_FLAG //Setting by default
        logger.debugWithClassName("Online option on zero amount ::: ".plus(onlineOptionOnZeroAmount))
        cVisaBean.setmOnlineOptionOnZeroAmount_1F34(onlineOptionOnZeroAmount)

        val ttq = aidParameters.ttq.defaultOrEmptyValue()
        logger.debugWithClassName("Terminal Transaction Qualifier (TTQ) ::: ".plus(ttq))
        cVisaBean.setmTerminalTransactionQualifier_9F66(ttq)

        val transCurrencyCode = emvParameters.transactionCurrencyCode.defaultOrEmptyValue()
        logger.debugWithClassName("Transaction Currency Code ::: ".plus(transCurrencyCode))
        cVisaBean.setmTransCurrencyCode_5F2A(transCurrencyCode)

        val transCurrencyExponent = emvParameters.transactionCurrencyExponent.defaultOrEmptyValue()
        logger.debugWithClassName("Transaction Currency Exponent ::: ".plus(transCurrencyExponent))
        cVisaBean.setmTransCurrencyExponent_5F36(transCurrencyExponent)

        val acquirerId = aidParameters.acquirerId.defaultOrEmptyValue()
        logger.debugWithClassName("Acquirer Id ::: ".plus(acquirerId))
        cVisaBean.setmAcquirerID_9F01(acquirerId)

        val ifdSerialNumber = FeitianUtils.retrieveDeviceSerialNumber(device)
        logger.debugWithClassName("IFD Serial Number ::: ".plus(ifdSerialNumber))
        cVisaBean.setmIFDSerial_9F1E(ifdSerialNumber)

        val terminalType = emvParameters.terminalType.defaultOrEmptyValue()
        logger.debugWithClassName("Terminal Type ::: ".plus(terminalType))
        cVisaBean.setmTerminalType_9F35(terminalType)

        var contactlessFloorLimit = aidParameters.contactlessFloorLimit.defaultOrEmptyValue()
        logger.debugWithClassName("Reader Contactless Floor Limit ::: ".plus(contactlessFloorLimit))
        contactlessFloorLimit = AmountUtils.toISOFormattedLowerCurrency(contactlessFloorLimit)
        logger.debugWithClassName(
            "Formatted Reader Contactless Floor Limit ::: ".plus(contactlessFloorLimit)
        )
        cVisaBean.setmReaderContactlessFloorLimit_DF8123(contactlessFloorLimit)

        var readerCVMLimit = aidParameters.cvmLimit.defaultOrEmptyValue()
        logger.debugWithClassName("Reader Contactless CVM Limit ::: ".plus(readerCVMLimit))
        readerCVMLimit = AmountUtils.toISOFormattedLowerCurrency(readerCVMLimit)
        logger.debugWithClassName("Formatted Reader Contactless CVM Limit ::: ".plus(readerCVMLimit))
        cVisaBean.setmCVMRequiredLimit_DF8126(readerCVMLimit)

        var contactlessTransactionLimit =
            aidParameters.contactlessTransactionLimit.defaultOrEmptyValue()
        logger.debugWithClassName(
            "Contactless Transaction Limit ::: ".plus(contactlessTransactionLimit)
        )
        contactlessTransactionLimit =
            AmountUtils.toISOFormattedLowerCurrency(contactlessTransactionLimit)
        logger.debugWithClassName(
            "Formatted Contactless Transaction Limit ::: ".plus(contactlessTransactionLimit)
        )
        cVisaBean.setmReaderContactlessTransactionLimit_DF8124(contactlessTransactionLimit)

        val extendedSupportFlag = CHECK_FLAG
        logger.debugWithClassName("Extended Selection Support Flag ::: ".plus(extendedSupportFlag))
        cVisaBean.setmExtendedSelectionSupportFlag_1F38(extendedSupportFlag)

        val terminalCountryCode = emvParameters.terminalCountryCode.defaultOrEmptyValue()
        logger.debugWithClassName("Terminal Country Code ::: ".plus(terminalCountryCode))
        cVisaBean.setmTerminalCountryCode_9F1A(terminalCountryCode)

        val merchantNameLocation = emvParameters.merchantName.defaultOrEmptyValue()
        logger.debugWithClassName("Merchant Name and Location ::: ".plus(merchantNameLocation))
        cVisaBean.setmMerchantNameLocation_9F4E(merchantNameLocation)

        val riskManagementData = aidParameters.terminalRiskManagementData.defaultOrEmptyValue()
        if (!riskManagementData.isNullOrEmptyOrBlank() && riskManagementData.length >= 16) {
            val riskManagementTLV = EmvTags.TAG_EMV_TERMINAL_RISK_MANAGEMENT_DATA
                .plus("08")
                .plus(riskManagementData.substring(0, 16))
            logger.debugWithClassName("Risk Management Data ::: ".plus(riskManagementData))
            cVisaBean.setmExtraTags(riskManagementTLV)
        }

        return cVisaBean.toTlvByteArray()
    }

    override fun getClassName(): String {
        return FeitianPayWaveKernelConfigHandler::class.java.simpleName
    }
}