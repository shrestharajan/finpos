package global.citytech.finpos.merchant.presentation.amount.oldlayout

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import android.os.BatteryManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityAmountBinding
import global.citytech.finpos.merchant.presentation.TextToSpeech.TextToSpeech
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.settlement.SettlementActivity
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finpos.merchant.utils.loadCurrency
import global.citytech.finposframework.log.Logger
import kotlinx.android.synthetic.main.activity_amount.btn_cancel
import kotlinx.android.synthetic.main.activity_amount.btn_confirm
import kotlinx.android.synthetic.main.activity_amount.btn_digit_double_zero
import kotlinx.android.synthetic.main.activity_amount.btn_digit_eight
import kotlinx.android.synthetic.main.activity_amount.btn_digit_five
import kotlinx.android.synthetic.main.activity_amount.btn_digit_four
import kotlinx.android.synthetic.main.activity_amount.btn_digit_nine
import kotlinx.android.synthetic.main.activity_amount.btn_digit_one
import kotlinx.android.synthetic.main.activity_amount.btn_digit_seven
import kotlinx.android.synthetic.main.activity_amount.btn_digit_six
import kotlinx.android.synthetic.main.activity_amount.btn_digit_three
import kotlinx.android.synthetic.main.activity_amount.btn_digit_two
import kotlinx.android.synthetic.main.activity_amount.btn_digit_zero
import kotlinx.android.synthetic.main.activity_amount.img_btn_clear
import kotlinx.android.synthetic.main.activity_amount.iv_back
import kotlinx.android.synthetic.main.activity_amount.progress_bar
import kotlinx.android.synthetic.main.activity_amount.tv_amount
import kotlinx.android.synthetic.main.activity_amount.tv_amount_decimal
import kotlinx.android.synthetic.main.activity_amount.tv_currency

/**
 * Created by Rishav Chudal on 8/24/20.
 */
class AmountActivity : AppBaseActivity<ActivityAmountBinding, AmountViewModel>(),
    View.OnClickListener,
    View.OnLongClickListener {

    companion object {
        const val REQUEST_CODE = 1000
        const val EXTRA_LIMIT = "extra_limit"
    }

    private val logger = Logger(AmountActivity::class.java.name)
    private lateinit var viewModel: AmountViewModel
    private var intentFilter: IntentFilter = IntentFilter()
    private lateinit var batteryBroadcast: BroadcastReceiver
    private var batteryLowDialogCount = 0
    private var batteryLevel: Int = 0
    private var isCharging: Boolean = false
    private var extraLimit: String = "0.00"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        getDataFromBundle()
        viewModel = getViewModel()
        initObservers()
        viewModel.retrieveAmountLengthLimit()
        viewModel.calculateAmount()
        viewModel.getConfiguration()
        batteryLevelCheck()
    }

    private fun getDataFromBundle() {
        val intent = getIntent()
        if(intent != null && intent.hasExtra(EXTRA_LIMIT)){
            extraLimit = intent.getStringExtra(
                EXTRA_LIMIT
            )
        }
    }

    private fun batteryLevelCheck() {
        batteryBroadcast = object : BroadcastReceiver() {
            override fun onReceive(contxt: Context?, intent: Intent?) {
                val batteryManager =
                    applicationContext.getSystemService(BATTERY_SERVICE) as BatteryManager
                batteryLevel =
                    batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)

                val status = intent?.getIntExtra(BatteryManager.EXTRA_STATUS, -1)

                isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING
                        || status == BatteryManager.BATTERY_STATUS_FULL


                if (Intent.ACTION_BATTERY_CHANGED == intent?.action) {
                    batteryLevel = intent.getIntExtra("level", 0)
                }

                if ((batteryLevel > 10 && batteryLowDialogCount != 0) || isCharging) {
                    batteryLowDialogCount = 0
                    hideConfirmationMessage()
                } else if (batteryLevel <= 10 && batteryLowDialogCount == 0) {
                    batteryLowDialogCount++
                    showFailureMessage(
                        MessageConfig.Builder()
                            .title(getString(R.string.title_battery_low))
                            .message(getString(R.string.msg_battery_low_desc))
                            .positiveLabel(getString(R.string.title_ok))
                            .onPositiveClick {
                                exitAmountPageOnCancel()
                            }
                    )
                }
            }
        }
    }

    private fun initViews() {
        btn_digit_one.setOnClickListener(this)
        btn_digit_two.setOnClickListener(this)
        btn_digit_three.setOnClickListener(this)
        btn_digit_four.setOnClickListener(this)
        btn_digit_five.setOnClickListener(this)
        btn_digit_six.setOnClickListener(this)
        btn_digit_seven.setOnClickListener(this)
        btn_digit_eight.setOnClickListener(this)
        btn_digit_nine.setOnClickListener(this)
        btn_digit_zero.setOnClickListener(this)
        btn_digit_double_zero.setOnClickListener(this)
        img_btn_clear.setOnClickListener(this)
        btn_confirm.setOnClickListener(this)
        btn_cancel.setOnClickListener(this)
        img_btn_clear.setOnLongClickListener(this)
        iv_back.setOnClickListener(this)

        tv_currency.loadCurrency()
    }

    private fun initObservers() {
        viewModel.integralAmount.observe(
            this,
            Observer {
                val changedIntegralAmount = it
                if (changedIntegralAmount.isNotEmpty()) {
                    tv_amount.text = changedIntegralAmount
                    if (changedIntegralAmount.length <= 6) {
                        tv_amount.textSize = 56f
                    } else if (changedIntegralAmount.length in 6..8) {
                        tv_amount.textSize = 44f
                    } else {
                        tv_amount.textSize = 34f
                    }
                }
            }
        )

        viewModel.fractionalAmount.observe(
            this,
            Observer {
                val changedFractionalAmount = it
                if (changedFractionalAmount.isNotEmpty()) {
                    tv_amount_decimal.text = changedFractionalAmount
                }
            }
        )

        viewModel.validAmount.observe(this, Observer {
            if (it) {
                onAmountConfirmed(
                    tv_amount.text.toString()
                        .plus(tv_amount_decimal.text.toString())
                )
            } else {
                onAmountLimitExceeded()
            }
        })

        viewModel.isLoading.observe(
            this,
            Observer {
                if (it) {
                    btn_confirm.isEnabled = false
                    btn_confirm.backgroundTintList =
                        ContextCompat.getColorStateList(this, R.color.finpos_purple_light)
                    progress_bar.visibility = View.VISIBLE
                } else {
                    btn_confirm.isEnabled = true
                    btn_confirm.backgroundTintList =
                        ContextCompat.getColorStateList(this, R.color.finpos_purple_dark)
                    progress_bar.visibility = View.INVISIBLE
                }
            }
        )
    }

    private fun onAmountLimitExceeded() {
        val builder = MessageConfig.Builder()
            .title(getString(R.string.title_confirm_amount))
            .message(
                viewModel.maxAmountMessage.value.plus("\n").plus(
                    getString(R.string.error_msg_max_amount_exceeded)
                )
            )
            .positiveLabel(getString(R.string.title_ok))
            .onPositiveClick { enableOrDisableButtons(true) }
        showFailureMessage(builder)
    }

    private fun confirmAmount() {
        val amount = tv_amount.text.toString()
            .plus(tv_amount_decimal.text.toString())
        val builder = MessageConfig.Builder()
            .title(getString(R.string.title_confirm_amount))
            .message(amount)
            .positiveLabel(getString(R.string.action_confirm))
            .negativeLabel(getString(R.string.action_cancel))
            .onPositiveClick {
                Logger.getLogger(AmountActivity::class.simpleName)
                    .debug("ON POSITIVE BUTTON CLICKED")
                onAmountConfirmed(amount)
            }
            .onNegativeClick { onCancelledClicked() }
        showAmountConfirmationMessage(builder)
    }

    override fun onClick(view: View?) {
        return when (view!!.id) {
            R.id.img_btn_clear -> manageForClearButtonClicked()

            R.id.btn_cancel, R.id.iv_back -> exitAmountPageOnCancel()

            R.id.btn_confirm -> {
                manageForConfirmButtonClicked(
                    tv_amount.text.toString()
                        .plus(tv_amount_decimal.text.toString())
                )
            }

            R.id.btn_digit_double_zero -> {
                getViewModel().onNumericButtonPressed(
                    (view as Button).text.toString(),
                    viewModel.amountLengthLimit - 1
                )
            }

            else -> {
                getViewModel().onNumericButtonPressed(
                    (view as Button).text.toString(),
                    viewModel.amountLengthLimit
                )
            }
        }
    }

    override fun onLongClick(view: View?): Boolean {
        when (view!!.id) {
            R.id.img_btn_clear -> manageForClearButtonLongClicked()
        }
        return true
    }

    private fun manageForClearButtonClicked() {
        viewModel.onClearButtonClicked()
    }

    private fun manageForCancelButtonClicked() {
        val builder = MessageConfig.Builder()
            .title(getString(R.string.title_exit))
            .message(getString(R.string.exit_msg))
            .positiveLabel(getString(R.string.title_ok))
            .negativeLabel(getString(R.string.action_cancel))
            .onPositiveClick { exitAmountPageOnCancel() }
            .onNegativeClick {}
        showConfirmationMessage(builder)
    }

    private fun manageForConfirmButtonClicked(amount: String) {
        enableOrDisableButtons(false)
        if (amount.isNotEmpty()) {
            if (amount.toDouble() > 0) {
                //val amountLimit = intent.getDoubleExtra(EXTRA_LIMIT, 0.0)
                val amountLimit = extraLimit.toDouble()
                getViewModel().checkMaximumAmountAllowed(amount.toDouble(), amountLimit)
            } else {
                enableButtonsAndShowToast(getString(R.string.error_msg_amount_zero))
            }
        } else {
            enableButtonsAndShowToast(getString(R.string.error_msg_amount_empty))
        }
    }

    private fun manageForClearButtonLongClicked() {
        viewModel.onClearButtonLongClicked()
    }

    private fun onAmountConfirmed(amount: String) {
        logger.log("Confirmed Amount ::: ".plus(amount))
        val intent = Intent()
        intent.putExtra(
            getString(R.string.intent_confirmed_amount),
            amount
        )
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun exitAmountPageOnCancel() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    override fun onBackPressed() {
    }

    private fun onCancelledClicked() {
        enableOrDisableButtons(true)
    }

    private fun enableOrDisableButtons(doEnable: Boolean) {
        btn_digit_one.isEnabled = doEnable
        btn_digit_two.isEnabled = doEnable
        btn_digit_three.isEnabled = doEnable
        btn_digit_four.isEnabled = doEnable
        btn_digit_five.isEnabled = doEnable
        btn_digit_six.isEnabled = doEnable
        btn_digit_seven.isEnabled = doEnable
        btn_digit_eight.isEnabled = doEnable
        btn_digit_nine.isEnabled = doEnable
        btn_digit_zero.isEnabled = doEnable
        btn_digit_double_zero.isEnabled = doEnable
        img_btn_clear.isEnabled = doEnable
    }

    private fun enableButtonsAndShowToast(message: String) {
        enableOrDisableButtons(true)
        showToast(message)
    }

    override fun onResume() {
        super.onResume()
        logger.debug(":::OnResume Called:::")
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED)
        try {
            registerReceiver(batteryBroadcast, intentFilter)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onPause() {
        super.onPause()
        logger.debug(":::OnPause Called:::")
        try {
            unregisterReceiver(batteryBroadcast)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun getBindingVariable() = BR.amountViewModel

    override fun getLayout() = R.layout.activity_amount

    override fun getViewModel() = ViewModelProviders.of(this)[AmountViewModel::class.java]
}