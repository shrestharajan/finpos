package global.citytech.finpos.device.qualcomm.io.cards.transaction

import com.ftpos.library.smartpos.emv.Amount
import global.citytech.finpos.device.qualcomm.io.cards.detect.FeitianDetectCardResponse
import global.citytech.finpos.device.qualcomm.utils.FeitianPinEntryResult
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse

/**
 * Created by Rishav Chudal on 4/7/21.
 */
interface TransactionCallbacksListener {
    fun onCardDetectCompleted(detectCardResponse: FeitianDetectCardResponse)

    fun onAmountRequired(): Amount

    fun onAppSelectionProcessCompleted(selectApplicationIndex: Int)

    fun collectBasicEMVDataAndReturnResponseForICC()

    fun onPinEntryProcessCompleted(pinEntryResult: FeitianPinEntryResult)

    fun onOnlineConnectionToHostRequired(transData: String?)

    fun onTransactionEndProcess(readCardResponse: ReadCardResponse)

    fun cleanUpRequired()
}