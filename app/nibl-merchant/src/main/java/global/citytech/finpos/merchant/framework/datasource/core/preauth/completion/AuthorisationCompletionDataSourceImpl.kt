package global.citytech.finpos.merchant.framework.datasource.core.preauth.completion

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.preauth.completion.AuthorisationCompletionDataSource
import global.citytech.finpos.merchant.domain.model.auth.completion.AuthorisationCompletionRequestEntity
import global.citytech.finpos.merchant.domain.model.auth.completion.AuthorisationCompletionResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionAuthenticatorImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.*
import global.citytech.finpos.merchant.presentation.data.mapToAuthorisationCompletionUiModel
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.preauth.completion.AuthorisationCompletionRequestItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finposframework.switches.ProcessorManager
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/29/2020.
 */
class AuthorisationCompletionDataSourceImpl : AuthorisationCompletionDataSource {
    override fun doAuthorisationCompletion(
        configurationItem: ConfigurationItem,
        authorisationCompletionRequestItem: AuthorisationCompletionRequestItem
    ): Observable<AuthorisationCompletionResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)

        val authorisationCompletionRequestEntity =
            AuthorisationCompletionRequestEntity(
                transactionRepository = TransactionRepositoryImpl(NiblMerchant.INSTANCE),
                authorisationCompletionRequest = authorisationCompletionRequestItem,
                deviceController = DeviceControllerImpl(NiblMerchant.INSTANCE),
                readCardService = CardSourceImpl(NiblMerchant.getActivityContext()),
                transactionAuthenticator = TransactionAuthenticatorImpl(),
                printerService = PrinterSourceImpl(NiblMerchant.INSTANCE),
                applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE),
                ledService = LedSourceImpl(NiblMerchant.INSTANCE),
                soundService = SoundSourceImpl(NiblMerchant.INSTANCE)
            )

        val authorisationCompletionRequester =
            ProcessorManager.getInterface(terminalRepository, NotificationHandler)
                .authorisationCompletionRequester
        return Observable.fromCallable {
            (authorisationCompletionRequester.execute(authorisationCompletionRequestEntity.mapToModel())).mapToAuthorisationCompletionUiModel()
        }
    }
}