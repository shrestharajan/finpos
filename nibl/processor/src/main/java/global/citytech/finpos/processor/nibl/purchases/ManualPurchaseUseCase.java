//package global.citytech.finpos.processor.nibl.purchases;
//
//import java.util.concurrent.CountDownLatch;
//
//import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
//import global.citytech.finpos.nibl.iso8583.ProcessingCode;
//import global.citytech.finpos.nibl.iso8583.requestsender.purchase.ManualPurchaseRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.purchase.PurchaseRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
//import global.citytech.finpos.processor.nibl.reversal.ReversalRequestModel;
//import global.citytech.finpos.processor.nibl.reversal.ReversalUseCase;
//import global.citytech.finpos.processor.nibl.transaction.ManualTransactionUseCase;
//import global.citytech.finpos.processor.nibl.transaction.NiblTransactionReceiptPrintHandler;
//import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
//import global.citytech.finpos.processor.nibl.transaction.ReversedTransactionReceiptHandler;
//import global.citytech.finposframework.exceptions.FinPosException;
//import global.citytech.finposframework.exceptions.PosError;
//import global.citytech.finposframework.exceptions.PosException;
//import global.citytech.finposframework.hardware.io.cards.CardDetails;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
//import global.citytech.finposframework.iso8583.DataElement;
//import global.citytech.finposframework.iso8583.IsoMessageResponse;
//import global.citytech.finposframework.iso8583.RequestContext;
//import global.citytech.finposframework.listeners.CardConfirmationListener;
//import global.citytech.finposframework.notifier.Notifier;
//import global.citytech.finposframework.repositories.ApplicationRepository;
//import global.citytech.finposframework.repositories.TerminalRepository;
//import global.citytech.finposframework.repositories.TransactionRepository;
//import global.citytech.finposframework.supports.UseCase;
//import global.citytech.finposframework.switches.PosEntryMode;
//import global.citytech.finposframework.switches.purchase.ManualPurchaseRequester;
//import global.citytech.finposframework.usecases.TransactionType;
//import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
//import global.citytech.finposframework.usecases.transaction.TransactionLog;
//import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
//import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
//import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
//import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
//import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
//import global.citytech.finposframework.utility.HelperUtils;
//import global.citytech.finposframework.utility.IsoMessageUtils;
//import global.citytech.finposframework.utility.JsonUtils;
//import global.citytech.finposframework.utility.StringUtils;
//
///**
// * Created by Rishav Chudal on 11/2/20.
// */
//public class ManualPurchaseUseCase extends ManualTransactionUseCase
//        implements UseCase<PurchaseRequestModel, PurchaseResponseModel>,
//        ManualPurchaseRequester<PurchaseRequestModel, PurchaseResponseModel> {
//
//    private global.citytech.finpos.nibl.iso8583.requestsender.purchase.PurchaseRequest isoPurchaseRequest;
//    static CountDownLatch countDownLatch;
//    PurchaseRequestModel purchaseRequestModel;
//    ReadCardResponse readCardResponse;
//    String stan;
//    String invoiceNum;
//    TransactionRequest transactionRequest;
//    String batchNumber;
//    private global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest;
//
//    public ManualPurchaseUseCase(
//            TerminalRepository terminalRepository,
//            Notifier notifier
//    ) {
//        super(terminalRepository, notifier);
//    }
//
//    @Override
//    public PurchaseResponseModel execute(PurchaseRequestModel purchaseRequestModel) {
//        try {
//            this.logger.log("::: REACHED MANUAL PURCHASE USE CASE :::");
//            this.purchaseRequestModel = purchaseRequestModel;
//            stan = this.terminalRepository.getSystemTraceAuditNumber();
//            invoiceNum = this.terminalRepository.getInvoiceNumber();
//            batchNumber = this.terminalRepository.getReconciliationBatchNumber();
//            this.transactionRepository = purchaseRequestModel.getTransactionRepository();
//            transactionRequest
//                    = purchaseRequestModel.getTransactionRequest();
//            if (!this.deviceIsReady(
//                    purchaseRequestModel.getDeviceController(),
//                    transactionRequest.getTransactionType()
//            )) {
//                throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
//            }
//            this.initTransaction(
//                    purchaseRequestModel.getApplicationRepository(),
//                    transactionRequest
//            );
//            ReadCardResponse readCardResponse
//                    = this.prepareManualReadCardResponse(transactionRequest);
//            readCardResponse.getCardDetails().setAmount(transactionRequest.getAmount());
//            readCardResponse.getCardDetails().setCashBackAmount(transactionRequest.getAdditionalAmount());
//            this.identifyAndSetCardScheme(
//                    readCardResponse,
//                    purchaseRequestModel.getTransactionRepository(),
//                    purchaseRequestModel.getApplicationRepository()
//            );
//            if (!this.validTransaction(
//                    purchaseRequestModel.getApplicationRepository(),
//                    transactionRequest,
//                    readCardResponse
//            )) {
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
//            }
//
//            if (!this.transactionAuthorizationSucceeds(
//                    purchaseRequestModel.getTransactionAuthenticator(),
//                    purchaseRequestModel.getApplicationRepository(),
//                    transactionRequest.getTransactionType(),
//                    readCardResponse
//            )) {
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
//            }
//            purchaseRequest = preparePurchaseRequest(
//                    transactionRequest,
//                    readCardResponse.getCardDetails()
//            );
//
//            final boolean[] isConfirm = {false};
//            countDownLatch = new CountDownLatch(1);
//            this.notifier.notify(Notifier.EventType.CARD_CONFIRMATION_EVENT,
//                    readCardResponse.getCardDetails(),
//                    (CardConfirmationListener) confirmed -> {
//                        isConfirm[0] = confirmed;
//                        countDownLatch.countDown();
//                    });
//            countDownLatch.await();
//            if (!isConfirm[0]) {
//                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
//            }
//            return processCardAndProceed();
//        } catch (PosException ex) {
//            ex.printStackTrace();
//            throw ex;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        }
//    }
//
//    private PurchaseResponseModel processCardAndProceed() {
//        readCardResponse = this.retrievePinBlockIfRequired(
//                purchaseRequest,
//                purchaseRequestModel.getApplicationRepository(),
//                purchaseRequestModel.getTransactionAuthenticator()
//        );
//        this.logger.debug("::: READ CARD RESPONSE ::: " + readCardResponse.toString());
//        this.notifier.notify(
//                Notifier.EventType.TRANSMITTING_REQUEST,
//                Notifier.EventType.TRANSMITTING_REQUEST.getDescription()
//        );
//        return this.initiateIsoMessageTransmission(
//                purchaseRequestModel,
//                stan,
//                invoiceNum,
//                batchNumber,
//                transactionRequest,
//                readCardResponse,
//                purchaseRequestModel.getApplicationRepository()
//        );
//    }
//
//    @Override
//    protected String getClassName() {
//        return ManualPurchaseUseCase.this.getClass().getSimpleName();
//    }
//
//    private PurchaseRequest preparePurchaseRequest(
//            TransactionRequest transactionRequest,
//            CardDetails cardDetails
//    ) {
//        PurchaseRequest purchaseRequest = new PurchaseRequest(
//                transactionRequest.getTransactionType(),
//                transactionRequest.getAmount(),
//                transactionRequest.getAdditionalAmount()
//        );
//        cardDetails.setAmount(transactionRequest.getAmount());
//        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
//        purchaseRequest.setCardDetails(cardDetails);
//        purchaseRequest.setFallbackIccToMag(transactionRequest.isFallbackIccToMag());
//        return purchaseRequest;
//    }
//
//    private PurchaseResponseModel initiateIsoMessageTransmission(
//            PurchaseRequestModel purchaseRequestModel,
//            String stan,
//            String invoiceNum,
//            String batchNumber,
//            TransactionRequest transactionRequest,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository
//    ) {
//        try {
//            IsoMessageResponse isoMessageResponse = this.sendIsoMessage(
//                    stan,
//                    invoiceNum,
//                    readCardResponse,
//                    purchaseRequestModel
//            );
//            InvalidResponseHandlerResponse invalidResponseHandlerResponse
//                    = this.checkForInvalidResponseFromHost(isoMessageResponse);
//            if (invalidResponseHandlerResponse.isInvalid()) {
//                return this.onInvalidResponseFromHost(
//                        transactionRequest,
//                        invalidResponseHandlerResponse,
//                        batchNumber,
//                        stan,
//                        invoiceNum,
//                        readCardResponse,
//                        purchaseRequestModel
//                );
//            }
//            boolean purchaseApproved = this.transactionApprovedByActionCode(isoMessageResponse);
//            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait...");
//            this.notifyTransactionCompletion(
//                    stan,
//                    batchNumber,
//                    invoiceNum,
//                    purchaseApproved,
//                    transactionRequest,
//                    purchaseRequestModel,
//                    isoMessageResponse,
//                    readCardResponse,
//                    applicationRepository
//            );
//            boolean shouldPrint = this.printReceipt(
//                    batchNumber,
//                    stan,
//                    invoiceNum,
//                    purchaseRequestModel.getPrinterService(),
//                    purchaseRequestModel.getApplicationRepository(),
//                    purchaseRequestModel.getTransactionRepository(),
//                    transactionRequest,
//                    readCardResponse,
//                    isoMessageResponse
//            );
//            return this.preparePurchaseResponse(stan,
//                    isoMessageResponse,
//                    purchaseApproved,
//                    purchaseRequestModel.getApplicationRepository(),
//                    shouldPrint
//            );
//        } catch (FinPosException ex) {
//            if (this.isResponseNotReceivedException(ex)) {
//                return onTransactionTimeOut(
//                        transactionRequest,
//                        batchNumber,
//                        stan,
//                        invoiceNum,
//                        readCardResponse,
//                        purchaseRequestModel
//                );
//            } else if (ex.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT) {
//                logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED UNABLE TO CONNECT");
//                transactionRepository.removeAutoReversal(this.autoReversal);
//                throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
//            } else {
//                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//            }
//        }
//    }
//
//    private IsoMessageResponse sendIsoMessage(
//            String stan,
//            String invoiceNumber,
//            ReadCardResponse readCardResponse,
//            PurchaseRequestModel requestModel) {
//        RequestContext requestContext = this.prepareManualTemplateRequestContext(stan);
//        this.logger.debug(String.format(" Request Context ::%s", requestContext));
//        this.isoPurchaseRequest = isoPurchaseRequest(
//                invoiceNumber,
//                readCardResponse
//        );
//        PurchaseRequestSender purchaseRequestSender = new ManualPurchaseRequestSender(
//                new NIBLSpecInfo(),
//                requestContext
//        );
//        requestContext.setRequest(isoPurchaseRequest);
//        this.autoReversal = new AutoReversal(
//                TransactionType.PURCHASE,
//                JsonUtils.toJsonObj(prepareReversalRequest(stan, readCardResponse, requestModel)),
//                0,
//                AutoReversal.Status.ACTIVE,
//                StringUtils.dateTimeStamp()
//        );
//        this.transactionRepository.saveAutoReversal(this.autoReversal);
//        IsoMessageResponse isoMessageResponse = purchaseRequestSender.send();
//        this.transactionRepository.removeAutoReversal(this.autoReversal);
//        return isoMessageResponse;
//    }
//
//    private global.citytech.finpos.nibl.iso8583.requestsender.purchase.PurchaseRequest isoPurchaseRequest(
//            String invoiceNumber,
//            ReadCardResponse readCardResponse
//    ) {
//        global.citytech.finpos.nibl.iso8583.requestsender.purchase.PurchaseRequest purchaseRequest
//                = new global.citytech.finpos.nibl.iso8583.requestsender.purchase.PurchaseRequest();
//        purchaseRequest.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        purchaseRequest.setExpireDate(readCardResponse.getCardDetails().getExpiryDate());
//        purchaseRequest.setPosEntryMode(PosEntryMode.MANUAL);
//        purchaseRequest.setPosConditionCode("05");
//        if (readCardResponse.getCardDetails().getPinBlock().getOfflinePin() ||
//                StringUtils.isEmpty(readCardResponse.getCardDetails().getPinBlock().getPin()) ||
//                readCardResponse.getCardDetails().getPinBlock().getPin().equalsIgnoreCase("offline"))
//            purchaseRequest.setPinBlock("");
//        else
//            purchaseRequest.setPinBlock(readCardResponse.getCardDetails().getPinBlock().getPin());
//        purchaseRequest.setLocalTime(HelperUtils.getDefaultLocaleTimeHhMmSs());
//        purchaseRequest.setLocalDate(HelperUtils.getDefaultLocaleDateWithoutYear());
//        purchaseRequest.setInvoiceNumber(invoiceNumber);
//        purchaseRequest.setTransactionAmount(readCardResponse.getCardDetails().getAmount());
//        purchaseRequest.setCvv(readCardResponse.getCardDetails().getCvv());
//        return purchaseRequest;
//    }
//
//    private PurchaseResponseModel onInvalidResponseFromHost(
//            TransactionRequest transactionRequest,
//            InvalidResponseHandlerResponse invalidResponseHandlerResponse,
//            String batchNumber,
//            String stan,
//            String invoiceNumber,
//            ReadCardResponse readCardResponse,
//            PurchaseRequestModel requestModel
//    ) {
//        this.notifier.notify(
//                Notifier.EventType.TRANSACTION_DECLINED,
//                invalidResponseHandlerResponse.getMessage()
//        );
//        this.performReversal(stan, readCardResponse, requestModel);
//        boolean shouldPrint = !this.approvePrintOnly(requestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(
//                    transactionRequest,
//                    readCardResponse,
//                    this.terminalRepository
//            ).prepare(
//                    batchNumber,
//                    stan,
//                    invoiceNumber,
//                    new ReasonForReversal(
//                            ReasonForReversal.Type.INVALID_RESPONSE,
//                            invalidResponseHandlerResponse.getMessage()
//                    )
//            );
//            new NiblTransactionReceiptPrintHandler(requestModel.getPrinterService())
//                    .printTransactionReceipt(
//                            receiptLog,
//                            ReceiptVersion.MERCHANT_COPY
//                    );
//            requestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PurchaseResponseModel.Builder.createDefaultBuilder()
//                .stan(stan)
//                .isApproved(false)
//                .message(invalidResponseHandlerResponse.getMessage())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private void performReversal(
//            String stan,
//            ReadCardResponse readCardResponse,
//            PurchaseRequestModel purchaseRequestModel
//    ) {
//        ReversalUseCase reversalUseCase = new ReversalUseCase(
//                this.prepareReversalRequest(stan, readCardResponse, purchaseRequestModel),
//                this.terminalRepository,
//                this.notifier
//        );
//        try {
//            reversalUseCase.execute(
//                    prepareReversalRequestModel(
//                            purchaseRequestModel,
//                            readCardResponse
//                    )
//            );
//            this.transactionRepository.removeAutoReversal(this.autoReversal);
//        } catch (FinPosException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private NiblReversalRequest prepareReversalRequest(
//            String stan,
//            ReadCardResponse readCardResponse,
//            PurchaseRequestModel purchaseRequestModel
//    ) {
//        NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
//        niblReversalRequest.setProcessingCode(ProcessingCode.PURCHASE.getCode());
//        niblReversalRequest.setAmount(
//                purchaseRequestModel.getTransactionRequest().getAmount()
//        );
//        niblReversalRequest.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        niblReversalRequest.setExpiryDate(readCardResponse.getCardDetails().getExpiryDate());
//        niblReversalRequest.setPosEntryMode(this.isoPurchaseRequest.getPosEntryMode());
//        niblReversalRequest.setPosConditionCode(this.isoPurchaseRequest.getPosConditionCode());
//        niblReversalRequest.setPinBlock(this.isoPurchaseRequest.getPinBlock());
//        niblReversalRequest.setInvoiceNumber(this.isoPurchaseRequest.getInvoiceNumber());
//        niblReversalRequest.setStan(stan);
//        return niblReversalRequest;
//    }
//
//    private ReversalRequestModel prepareReversalRequestModel(
//            PurchaseRequestModel purchaseRequestModel,
//            ReadCardResponse readCardResponse
//    ) {
//        return ReversalRequestModel.Builder.newInstance()
//                .withTransactionRequest(
//                        purchaseRequestModel.getTransactionRequest()
//                )
//                .withTransactionRepository(
//                        purchaseRequestModel.getTransactionRepository()
//                )
//                .withPrinterService(
//                        purchaseRequestModel.getPrinterService()
//                )
//                .withApplicationRepository(
//                        purchaseRequestModel.getApplicationRepository()
//                )
//                .withReadCardResponse(readCardResponse)
//                .build();
//    }
//
//    private void notifyTransactionCompletion(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean purchaseApproved,
//            TransactionRequest transactionRequest,
//            PurchaseRequestModel purchaseRequestModel,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository
//    ) {
//        this.updateTransactionLog(
//                stan,
//                batchNumber,
//                invoiceNumber,
//                purchaseApproved,
//                transactionRequest,
//                purchaseRequestModel.getTransactionRepository(),
//                isoMessageResponse,
//                readCardResponse,
//                applicationRepository
//        );
//        this.updateTransactionIds(purchaseApproved);
//        this.notifyTransactionStatus(
//                purchaseApproved,
//                isoMessageResponse,
//                purchaseRequestModel.getApplicationRepository()
//        );
//    }
//
//    private void updateTransactionLog(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean purchaseApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository
//    ) {
//        this.logger.log("::: NIBL ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
//        this.logger.log("::: NIBL ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
//        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
//                .withStan(stan)
//                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
//                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
//                .withInvoiceNumber(invoiceNumber)
//                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
//                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
//                .withTransactionType(TransactionType.PURCHASE)
//                .withOriginalTransactionType(TransactionType.PURCHASE)
//                .withTransactionAmount(transactionRequest.getAmount())
//                .withOriginalTransactionAmount(transactionRequest.getAmount())
//                .withTransactionDate(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_DATE))
//                .withTransactionTime(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_TIME))
//                .withTransactionStatus(purchaseApproved ? "APPROVED" : "DECLINED")
//                .withPosEntryMode(PosEntryMode.MANUAL)
//                .withOriginalPosEntryMode(PosEntryMode.MANUAL)
//                .withPosConditionCode(this.isoPurchaseRequest.getPosConditionCode())
//                .withOriginalPosConditionCode("")
//                .withReconcileStatus("")
//                .withReconcileTime("")
//                .withReconcileDate("")
//                .withReconcileBatchNo(batchNumber)
//                .withReadCardResponse(readCardResponse)
//                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse,
//                        applicationRepository, batchNumber, stan, invoiceNumber))
//                .withAuthorizationCompleted(true)
//                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
//                .withTransactionVoided(false)
//                .withTipAdjusted(false)
//                .build();
//        transactionRepository.updateTransactionLog(transactionLog);
//    }
//
//
//    private PurchaseResponseModel preparePurchaseResponse(
//            String stan,
//            IsoMessageResponse isoMessageResponse,
//            boolean purchaseApproved,
//            ApplicationRepository applicationRepository,
//            boolean shouldPrint
//    ) {
//        PurchaseResponseModel.Builder builder = PurchaseResponseModel.Builder.createDefaultBuilder();
//        builder.stan(stan);
//        builder.isApproved(purchaseApproved);
//        builder.debugRequestMessage(isoMessageResponse.getDebugRequestString());
//        builder.debugResponseMessage(isoMessageResponse.getDebugResponseString());
//        builder.message(
//                this.retrieveMessage(
//                        isoMessageResponse,
//                        purchaseApproved,
//                        applicationRepository
//                )
//        );
//        builder.shouldPrintCustomerCopy(shouldPrint);
//        return builder.build();
//    }
//
//    private PurchaseResponseModel onTransactionTimeOut(
//            TransactionRequest transactionRequest,
//            String batchNumber,
//            String stan,
//            String invoiceNumber,
//            ReadCardResponse readCardResponse,
//            PurchaseRequestModel purchaseRequestModel
//    ) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, "TIMEOUT");
//        this.performReversal(
//                stan,
//                readCardResponse,
//                purchaseRequestModel
//        );
//        boolean shouldPrint = !this.approvePrintOnly(purchaseRequestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(
//                    transactionRequest,
//                    readCardResponse,
//                    this.terminalRepository
//            ).prepare(
//                    batchNumber,
//                    stan,
//                    invoiceNumber,
//                    new ReasonForReversal(
//                            ReasonForReversal.Type.TIMEOUT,
//                            ReasonForReversal.Type.TIMEOUT.getDescription()
//                    )
//            );
//            new NiblTransactionReceiptPrintHandler(purchaseRequestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            purchaseRequestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PurchaseResponseModel.Builder.createDefaultBuilder()
//                .message("TIMEOUT")
//                .isApproved(false)
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//}
