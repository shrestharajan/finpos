package global.citytech.finpos.merchant.data.datasource.core.receipt

import global.citytech.finpos.merchant.domain.model.receipt.CustomerCopyResponseEntity
import global.citytech.finposframework.usecases.transaction.data.StatementList
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/2/2020.
 */
interface CustomerCopyDataSource {
    fun print(): Observable<CustomerCopyResponseEntity>
    fun printStatement(statementList: StatementList): Observable<CustomerCopyResponseEntity>
}