package global.citytech.finpos.merchant.presentation.ministatement

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import global.citytech.finpos.merchant.R
import global.citytech.finposframework.usecases.transaction.data.StatementItems

class MiniStatementAdapter (private val miniStatementList: List<StatementItems>):
RecyclerView.Adapter<MiniStatementAdapter.ViewHolder>(){
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val dateTextView: TextView = itemView.findViewById(R.id.tv_date_mini_statement)
        val descriptionTextView: TextView = itemView.findViewById(R.id.tv_transaction_name)
        val dbCrIndicatorTextView: TextView = itemView.findViewById(R.id.tv_transaction_type)
        val amountTextView: TextView = itemView.findViewById(R.id.tv_transaction_amount_mini_statement)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.mini_statement_item_of_recycler_view, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val miniStatementItem = miniStatementList[position]
        holder.dateTextView.text = miniStatementItem.date
        holder.descriptionTextView.text = miniStatementItem.description
        if(miniStatementItem.dbCrIndicator == "Dr"){
            holder.dbCrIndicatorTextView.setTextColor(Color.RED)
            holder.dbCrIndicatorTextView.text = miniStatementItem.dbCrIndicator
        }else{
            holder.dbCrIndicatorTextView.setTextColor(Color.GREEN)
            holder.dbCrIndicatorTextView.text = miniStatementItem.dbCrIndicator
        }
        holder.amountTextView.text = miniStatementItem.amount.trim()
    }

    override fun getItemCount(): Int {
        return miniStatementList.size
    }
}