package global.citytech.finpos.device.qualcomm.io.cards.transaction

import android.content.Context
import com.ftpos.library.smartpos.emv.Amount
import com.ftpos.library.smartpos.emv.CandidateAIDInfo
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.emv.OnEmvResponse
import global.citytech.common.extensions.removePaddedLettersFromTheCardNumber
import global.citytech.finpos.device.qualcomm.io.cards.detect.FeitianDetectCardRequest
import global.citytech.finpos.device.qualcomm.io.cards.detect.FeitianDetectCardServiceImpl
import global.citytech.finpos.device.qualcomm.io.cards.handler.FeitianApplicationSelectionHandler
import global.citytech.finpos.device.qualcomm.io.cards.handler.FeitianOnEndProcessHandler
import global.citytech.finpos.device.qualcomm.io.cards.handler.FeitianPinPadHandler
import global.citytech.finpos.device.qualcomm.utils.*
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.CardSummary
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest
import global.citytech.finposframework.hardware.utility.DeviceUtils
import global.citytech.finposframework.hardware.utility.TransactionProcessedState
import global.citytech.finposframework.notifier.Notifier
import global.citytech.finposframework.utility.StringUtils
import java.lang.ref.WeakReference

/**
 * Created by Rishav Chudal on 4/7/21.
 */
class FeitianTransactionCallbacks constructor(
    val context: Context,
    val listener: TransactionCallbacksListener,
    val notifier: Notifier
): OnEmvResponse, FeitianLogger.Listener {

    private val contextWeakReference = WeakReference(context)
    private val emv = Emv.getInstance(context.applicationContext)
    private val logger = FeitianLogger(this)

    override fun onAppSelect(
        reselectApplication: Boolean,
        candidateAIDInfos: List<CandidateAIDInfo>
    ) {
        this.logger.logWithClassName("TransactionCallbacks ::: onAppSelect")
        this.logger.logWithClassName("CandidateAIDInfo Size ::: ".plus(candidateAIDInfos.size))
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.APPLICATION_SELECTION
        val selectionHandler = FeitianApplicationSelectionHandler(
            context,
            reselectApplication,
            candidateAIDInfos
        )
        val selectedApplicationIndex = selectionHandler.getSelectedApplicationIndex()
        this.listener.onAppSelectionProcessCompleted(selectedApplicationIndex)
    }

    override fun onPinEntry(pinEntryType: Int) {
        this.logger.logWithClassName("onPinEntry ::: CVM Id ::: ".plus(pinEntryType))
        this.logger.logWithClassName(
            "onPinEntry ::: TYPE ::: ".plus(FeitianUtils.getRespectivePinEntryType(pinEntryType))
        )
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.PIN_ENTRY
        FeitianSingleton.pinEntryType = pinEntryType
        this.logger.logWithClassName("is Pin Required ::::" + FeitianSingleton.readCardRequest?.isPinpadRequired)
        if( FeitianSingleton.readCardRequest?.isPinpadRequired== true){
            val pinPadManager = FeitianPinPadHandler(
                contextWeakReference.get()!!,
                pinEntryType,
                preparePinRequest()
            )
            val pinPadResult = pinPadManager.showPinPadAndCalculatePinBlock()
            listener.onPinEntryProcessCompleted(pinPadResult)
        }else{
            this.logger.logWithClassName("AT BYPASS")
             listener.onPinEntryProcessCompleted(FeitianPinEntryResult(
                 Result.PIN_BYPASSED,
                 preparePinPadTlvData(FeitianConstants.PIN_PAD_TLV_VALUE_PIN_BYPASS)!!
             ))
        }
    }


    private fun preparePinPadTlvData(data: Any): String? {
        return java.lang.String.format(
            FeitianCustomEMVTags.CUSTOM_TAG_PINPAD_RESULT.tagInHex()
                .plus(FeitianCustomEMVTags.CUSTOM_TAG_PINPAD_RESULT.lengthInHex())
                .plus("%02x"),
            data
        )
    }

    override fun onOnlineProcess(transactionData: String?) {
        this.logger.logWithClassName(
            "onOnlineProcess ::: Transaction Data ::: ".plus(transactionData)
        )
        FeitianSingleton.onlineTransaction = true
        this.listener.onOnlineConnectionToHostRequired(transactionData)
    }

    override fun onEndProcess(returnCode: Int, transactionData: String?) {
        val onEndProcessHandler = FeitianOnEndProcessHandler(
            context,
            returnCode,
            transactionData,
            notifier
        )
        if (!onEndProcessHandler.isTransactionAnalysisRequired()) {
            return
        }
        val readCardResponse = onEndProcessHandler.analyzeTransactionResult()
        this.listener.onTransactionEndProcess(readCardResponse)
    }

    override fun onDisplayPanInfo(pan: String?) {
        this.logger.logWithClassName("Transaction Callback ::: onDisplayPanInfo...")
        this.logger.logWithClassName("PAN ::: ".plus(StringUtils.maskCardNumber(pan).removePaddedLettersFromTheCardNumber()))
    }

    override fun onSearchCard() {
        this.logger.logWithClassName("Transaction Callback ::: onSearchCard...")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.SEARCHING_FOR_CARD
        val detectCardService = FeitianDetectCardServiceImpl(context, notifier)
        val detectCardResponse = detectCardService.detectCard(prepareDetectCardRequest())
        this.listener.onCardDetectCompleted(detectCardResponse)
    }

    override fun onSearchCardAgain() {
        this.logger.logWithClassName("Transaction Callback ::: onSearchCardAgain...")
    }

    override fun onProcessInteractionPoint(interactionPoint: Int) {
        this.logger.logWithClassName("TransactionCallbacks ::: onProcessInteractionPoint...")
        this.logger.logWithClassName("Interaction Point ::: ".plus(interactionPoint))
        updateCardSummary()
        if (FeitianSingleton.currentCardDetected == CardType.ICC) {
            this.listener.collectBasicEMVDataAndReturnResponseForICC()
        } else if (FeitianSingleton.currentCardDetected == CardType.PICC){
            this.emv.respondEvent(null)
        }
    }

    override fun onObtainData(ins: Int, data: ByteArray, dataInformation: ByteArray) {
        this.logger.logWithClassName("onObtainData...")
        this.logger.logWithClassName("onObtainData ::: ins ::: ".plus(ins))
        this.logger.logWithClassName("onObtainData ::: data ::: ".plus(data))
        this.logger.logWithClassName("onObtainData ::: dataInformation ::: ".plus(dataInformation))
    }

    override fun onUpdateTransAmount(): Amount {
        this.logger.logWithClassName("Transaction Callback ::: onUpdateTransAmount")
        return this.listener.onAmountRequired()
    }

    private fun prepareDetectCardRequest() = FeitianDetectCardRequest(
        FeitianSingleton.readCardRequest!!.cardTypes,
        FeitianSingleton.transactionTimeOutInSecs + 5
    )

    private fun preparePinRequest(): PinRequest {
        val pan = FeitianUtils.retrievePAN(emv)
        val amountMessage = FeitianSingleton.readCardRequest!!.currencyName!!
            .plus(" ")
            .plus(StringUtils.formatAmountTwoDecimal(FeitianSingleton.readCardRequest!!.amount!!))
        val cardSummary = CardSummary(
            cardSchemeLabel = FeitianSingleton.cardSummary!!.cardSchemeLabel,
            primaryAccountNumber = pan,
            cardHolderName = FeitianSingleton.cardSummary!!.cardHolderName,
            expiryDate = FeitianSingleton.cardSummary!!.expiryDate
        )
        val pinRequest = PinRequest(
            primaryAccountNumber = pan,
            pinPadMessage = "",
            pinBlockFormat = FeitianSingleton.readCardRequest!!.pinBlockFormat,
            pinMaxLength = DeviceUtils.getMaxPinLengthOrDefault(
                FeitianSingleton.readCardRequest!!.maxmPinLength
            ),
            pinMinLength = DeviceUtils.getMinPinLengthOrDefault(
                FeitianSingleton.readCardRequest!!.minmPinLength
            ),
            pinPadTimeout = DeviceUtils.getTimeOutOrDefault(
                FeitianSingleton.readCardRequest!!.pinpadTimeout
            ),
            amountMessage = amountMessage,
            pinPadFixedLayout = FeitianSingleton.readCardRequest?.pinPadFixedLayout!!,
            dynamicTitle = "Enter Pin",
        )
        pinRequest.cardSummary = cardSummary
        return pinRequest
    }

    private fun updateCardSummary() {
        val cardSummary = FeitianUtils.prepareCardSummary(emv)
        FeitianSingleton.cardSummary = cardSummary
    }

    override fun getClassName(): String = FeitianTransactionCallbacks::class.java.simpleName
}