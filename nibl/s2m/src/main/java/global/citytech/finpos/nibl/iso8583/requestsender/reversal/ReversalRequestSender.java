package global.citytech.finpos.nibl.iso8583.requestsender.reversal;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.iso8583.NIBLMessagePayloadBuilder;
import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.PosEntryModeConfig;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Rishav Chudal on 9/28/20.
 */
public class ReversalRequestSender extends NIBLMessageSenderTemplate {
    private Logger logger = Logger.getLogger(ReversalRequestSender.class.getName());
    private NiblReversalRequest reversalRequest;

    public ReversalRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
        this.reversalRequest = (NiblReversalRequest) context.getRequest();
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti = new MTI(
                MTI.Version.Version_1987,
                MTI.MessageClass.Reversal,
                MTI.MessageFunction.Request,
                MTI.Originator.Acquirer
        );

        Iso8583Msg msg = this.getTemplateIsoMessage(mti);

        String pan = reversalRequest.getPan();
        if (!StringUtils.isEmpty(pan)) {
            msg.addField(
                    new DataElement(
                            DataElement.PRIMARY_ACCOUNT_NUMBER,
                            pan
                    )
            );
        }

        msg.addField(
                new DataElement(
                        DataElement.AMOUNT,
                        HelperUtils.toISOAmount(this.reversalRequest.getAmount(), 12)
                )
        );

        String panExpiryDate = reversalRequest.getExpiryDate();
        if (!StringUtils.isEmpty(pan) && !StringUtils.isEmpty(panExpiryDate)) {
            msg.addField(
                    new DataElement(
                            DataElement.PAN_EXPIRE_DATE,
                            panExpiryDate
                    )
            );
        }

        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(this.reversalRequest.getPosEntryMode())
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE,
                        this.reversalRequest.getPosConditionCode()
                )
        );
        if (!StringUtils.isEmpty(this.reversalRequest.getTrack2Data()))
            msg.addField(
                    new DataElement(
                            DataElement.TRACK_II_DATA,
                            this.reversalRequest.getTrack2Data()
                    )
            );
//        msg.addField(
//                new DataElement(
//                        DataElement.PIN_BLOCK,
//                        this.reversalRequest.getPinBlock()
//                )
//        );
        msg.addField(
                new DataElement(
                        DataElement.PRIVATE_USE_FIELD_62,
                        this.reversalRequest.getInvoiceNumber()
                )
        );

        msg.addField(
                new DataElement(
                        DataElement.LOCAL_TIME,
                        this.reversalRequest.getLocalTime()
                )
        );

        msg.addField(
                new DataElement(
                        DataElement.LOCAL_DATE,
                        this.reversalRequest.getLocalDate()
                )
        );

        msg.addField(
                new DataElement(
                        DataElement.ICC_SYSTEM_RELATED_DATA,
                        this.reversalRequest.getEmvData()
                )
        );
        MessagePayload.Builder builder = new NIBLMessagePayloadBuilder(this.specInfo, msg);
        logger.debug("===== Request =====");
        logger.debug(msg.getPrintableData());
        return builder.build();
    }

    @Override
    protected String getProcessingCode() {
        return this.reversalRequest.getProcessingCode();
    }

    @Override
    protected String getFunctionCode() {
        return "0011";
    }
}
