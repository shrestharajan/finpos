package global.citytech.neps.domain.usecase.core.configuration

import global.citytech.neps.domain.repository.core.configuration.ConfigurationRepository
import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.posswitchintregator.api.logon.LogOnResponseParameter
import global.citytech.posswitchintregator.neps.usecases.keyexchange.KeyExchangeResponseModel
import io.reactivex.Observable

class CoreConfigurationUseCase(val repository: ConfigurationRepository) {
    fun logOn( configurationItem: ConfigurationItem): Observable<LogOnResponseParameter> =
        repository.logOn(configurationItem)

    fun keyExchange(configurationItem: ConfigurationItem ): Observable<KeyExchangeResponseModel> =
        repository.exchangeKey(configurationItem)
}