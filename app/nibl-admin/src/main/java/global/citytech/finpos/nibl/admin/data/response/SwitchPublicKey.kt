package global.citytech.finpos.nibl.admin.data.response

data class SwitchPublicKey(
    var index: String? = null,
    var key: String? = null
)