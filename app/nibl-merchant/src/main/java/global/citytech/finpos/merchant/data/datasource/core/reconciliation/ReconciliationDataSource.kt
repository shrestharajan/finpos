package global.citytech.finpos.merchant.data.datasource.core.reconciliation

import global.citytech.finpos.merchant.domain.model.reconciliation.ReconciliationResponseEntity
import global.citytech.finpos.merchant.domain.model.reconciliation.clear.BatchClearResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/23/2020.
 */
interface ReconciliationDataSource {

    fun reconcile(
        configurationItem: ConfigurationItem,
        settlementActive: Boolean
    ): Observable<ReconciliationResponseEntity>

    fun checkReconciliationRequired(configurationItem: ConfigurationItem): Observable<Boolean>

    fun clearBatch(configurationItem: ConfigurationItem): Observable<BatchClearResponseEntity>
}