package global.citytech.finposframework.usecases.activity;


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/11/08 - 12:09 PM
 */

public enum ActivityLogType {
    LOGON,
    KEY_EXCHANGE,
    REVERSAL,
    BATCH_ADVICE,
    SETTLEMENT,
    SETTLEMENT_CLOSURE
}
