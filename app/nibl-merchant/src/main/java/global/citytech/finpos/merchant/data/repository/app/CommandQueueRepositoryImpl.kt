package global.citytech.finpos.merchant.data.repository.app

import global.citytech.finpos.merchant.data.datasource.app.commands.CommandQueueDataSource
import global.citytech.finpos.merchant.domain.model.app.Command
import global.citytech.finpos.merchant.domain.model.app.Status
import global.citytech.finpos.merchant.domain.repository.app.CommandQueueRepository
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/20/2021.
 */
class CommandQueueRepositoryImpl(private val commandQueueDataSource: CommandQueueDataSource) :
    CommandQueueRepository {
    override fun isQueueEmpty(): Boolean = commandQueueDataSource.isQueueEmpty()

    override fun hasActiveCommands(): Boolean = commandQueueDataSource.hasActiveCommands()

    override fun getCount(): Int = commandQueueDataSource.getCount()

    override fun getAllCommands(): MutableList<Command> = commandQueueDataSource.getAllCommands()

    override fun removeCommand(command: Command) {
        commandQueueDataSource.removeCommand(command)
    }

    override fun addCommand(command: Command) {
        commandQueueDataSource.addCommand(command)
    }

    override fun updateCommandStatus(command: Command, status: Status, remarks: String) {
        commandQueueDataSource.updateCommandStatus(command, status, remarks)
    }

    override fun clear() {
        commandQueueDataSource.clear()
    }
}