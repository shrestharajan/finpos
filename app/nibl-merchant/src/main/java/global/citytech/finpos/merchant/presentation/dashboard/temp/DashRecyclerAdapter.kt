package global.citytech.finpos.merchant.presentation.dashboard.temp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.button.MaterialButton
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import global.citytech.common.AppCountDownTimer
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuAdapter
import global.citytech.finpos.merchant.presentation.utils.AdvertisementsCustomViewPagerAdapter
import global.citytech.finpos.merchant.presentation.utils.SpaceItemDecoration

/**
 * Created by Unique Shakya on 6/15/2021.
 */
class DashRecyclerAdapter(
    val context: Context,
    val dashItems: DashItems
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var appCountDownTimer: AppCountDownTimer? = null

    companion object {
        const val ADVERTISEMENT_VIEW_TYPE = 111
        const val PRIMARY_TRANSACTION_VIEW_TYPE = 112
        const val MENU_ITEMS_VIEW_TYPE = 113

        const val NO_SUCH_VIEW_TYPE_EXCEPTION = "No such item view type"
        const val ADVERTISEMENT_INTERVAL = 5000L
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ADVERTISEMENT_VIEW_TYPE -> AdvertisementViewHolder(
                LayoutInflater.from(context)
                    .inflate(R.layout.item_advertisement, parent, false)
            )
            PRIMARY_TRANSACTION_VIEW_TYPE -> PrimaryTransactionViewHolder(
                LayoutInflater.from(context)
                    .inflate(R.layout.item_primary_transaction, parent, false)
            )
            MENU_ITEMS_VIEW_TYPE -> MenuItemsViewHolder(
                LayoutInflater.from(context)
                    .inflate(R.layout.item_menu_items, parent, false)
            )
            else -> throw IllegalArgumentException(NO_SUCH_VIEW_TYPE_EXCEPTION)
        }
    }

    override fun getItemCount(): Int {
        return 3
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            ADVERTISEMENT_VIEW_TYPE -> handleForAdvertisement(holder)
            PRIMARY_TRANSACTION_VIEW_TYPE -> handleForPrimaryTransaction(holder)
            MENU_ITEMS_VIEW_TYPE -> handleForMenuItems(holder)
            else -> throw IllegalArgumentException(NO_SUCH_VIEW_TYPE_EXCEPTION)
        }
    }

    private fun handleForCardSchemes(holder: RecyclerView.ViewHolder) {
        val ivCardSchemes = holder.itemView.findViewById<ImageView>(R.id.iv_card_scheme)
        ivCardSchemes.setImageResource(R.drawable.card_schemes)
    }

    private fun handleForMenuItems(holder: RecyclerView.ViewHolder) {
        val rvMenu = holder.itemView.findViewById<RecyclerView>(R.id.rv_menu)
        val layoutManager = GridLayoutManager(context, 4)
        rvMenu.layoutManager = layoutManager
        rvMenu.adapter = MenuAdapter(dashItems.menus!!, dashItems.menusAdapterListener!!)
        rvMenu.addItemDecoration(SpaceItemDecoration(4, 28, false))
    }

    private fun handleForPrimaryTransaction(holder: RecyclerView.ViewHolder) {
        val btnTransaction = holder.itemView.findViewById<MaterialButton>(R.id.btn_purchase)
        btnTransaction.text = context.getString(dashItems.primaryTransaction!!.menu!!.title)
        btnTransaction.setIconResource(dashItems.primaryTransaction!!.menu!!.icon)
        btnTransaction.setOnClickListener {
            dashItems.primaryMenuAdapterListener!!
                .onMenuAdapterItemClicked(dashItems.primaryTransaction!!)
        }
    }

    private fun handleForAdvertisement(holder: RecyclerView.ViewHolder) {
        val vpAdvertisement = holder.itemView.findViewById<ViewPager2>(R.id.vp_advertisement)
        val tlAdvertisement = holder.itemView.findViewById<TabLayout>(R.id.tab_layout_advertisement)
        var currentPage = 0
        val PAGE_SIZE = dashItems.advertisements?.size
        vpAdvertisement.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                currentPage = position;
            }
        })
        vpAdvertisement.adapter = AdvertisementsCustomViewPagerAdapter(
            context,
            dashItems.advertisements
        )

        appCountDownTimer = AppCountDownTimer(101000, ADVERTISEMENT_INTERVAL,
            {
                if (currentPage == PAGE_SIZE) {
                    currentPage = 0
                }
                vpAdvertisement.setCurrentItem(currentPage, true)
                currentPage+=1
            }, {
                appCountDownTimer?.cancel()
                appCountDownTimer?.start()
            })
        appCountDownTimer?.start()

        TabLayoutMediator(tlAdvertisement, vpAdvertisement) { tab, position -> }.attach()
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> ADVERTISEMENT_VIEW_TYPE
            1 -> PRIMARY_TRANSACTION_VIEW_TYPE
            2 -> MENU_ITEMS_VIEW_TYPE
            else -> throw IllegalArgumentException(NO_SUCH_VIEW_TYPE_EXCEPTION)
        }
    }

    class AdvertisementViewHolder(view: View) : RecyclerView.ViewHolder(view)

    class PrimaryTransactionViewHolder(view: View) : RecyclerView.ViewHolder(view)

    class MenuItemsViewHolder(view: View) : RecyclerView.ViewHolder(view)

    fun onDestroy(){
        appCountDownTimer?.cancel()
        appCountDownTimer = null
    }

    fun startSlideCountDown() {
        appCountDownTimer?.cancel()
        appCountDownTimer?.start()
    }

    fun stopSlideCountDown() {
        appCountDownTimer?.cancel()
    }
}