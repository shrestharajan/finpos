package global.citytech.finpos.processor.neps.transaction;

import global.citytech.finpos.neps.iso8583.SmartVistaActionCode;
import global.citytech.finpos.processor.neps.receipt.ReceiptLabels;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.cvm.CardHolderVerification;
import global.citytech.finposframework.hardware.io.cards.cvm.Cvm;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.EmvTags;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.TransactionInfo;
import global.citytech.finposframework.utility.CvmResultsParser;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Rishav Chudal on 7/6/20.
 */
public class TransactionReceiptHandler {
    private TransactionRequest transactionRequest;
    private ReadCardResponse readCardResponse;
    private IsoMessageResponse isoMessageResponse;
    private TerminalRepository terminalRepository;
    private ApplicationRepository applicationRepository;

    public TransactionReceiptHandler(
            TransactionRequest transactionRequest,
            ReadCardResponse readCardResponse,
            IsoMessageResponse isoMessageResponse,
            TerminalRepository terminalRepository,
            ApplicationRepository applicationRepository
    ) {
        this.transactionRequest = transactionRequest;
        this.readCardResponse = readCardResponse;
        this.isoMessageResponse = isoMessageResponse;
        this.terminalRepository = terminalRepository;
        this.applicationRepository = applicationRepository;
    }

    public ReceiptLog prepare(String batchNumber, String stan, String invoiceNumber) {
        return new ReceiptLog.Builder()
                .withRetailer(prepareRetailerInfo())
                .withPerformance(preparePerformanceInfo())
                .withTransaction(prepareTransactionInfo(batchNumber, stan, invoiceNumber))
                .withEmvTags(prepareEmvTags())
                .withBarCodeString(this.retrieveBarCodeString())
                .build();
    }

    private String retrieveBarCodeString() {
        if (!terminalRepository.isVatRefundSupported())
            return "";
        if (transactionRequest.getTransactionType() == TransactionType.PURCHASE)
            return this.retrieveRrn();
        else
            return "";
    }

    private Retailer prepareRetailerInfo() {
        return new Retailer.Builder()
                .withRetailerLogo(this.terminalRepository.findTerminalInfo().getMerchantPrintLogo())
                .withRetailerName(this.terminalRepository.findTerminalInfo().getMerchantName())
                .withRetailerAddress(this.terminalRepository.findTerminalInfo().getMerchantAddress())
                .withMerchantId(this.terminalRepository.findTerminalInfo().getMerchantID())
                .withTerminalId(this.terminalRepository.findTerminalInfo().getTerminalID())
                .build();
    }

    private Performance preparePerformanceInfo() {
        return new Performance.Builder()
                .withStartDateTime(
                        this.readCardResponse
                                .getCardDetails()
                                .getTransactionInitializeDateTime()
                )
                .withEndDateTime(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, 12))
                .build();
    }

    private TransactionInfo prepareTransactionInfo(String batchNumber, String stan, String invoiceNumber) {
        boolean isApproved = this.isApprovedByActionCode(Integer.parseInt(this.retrieveResponseCode()));
        return new TransactionInfo.Builder()
                .withApprovalCode(this.retrieveApprovalCode())
                .withTransactionCurrencyName(this.terminalRepository.getTerminalTransactionCurrencyName())
                .withBatchNumber(batchNumber)
                .withCardHolderName(!StringUtils.isEmpty(readCardResponse.getCardDetails().getCardHolderName()) ? readCardResponse.getCardDetails().getCardHolderName().trim() : "")
                .withCardNumber(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .withCardScheme(readCardResponse.getCardDetails().getCardSchemeLabel())
                .withExpireDate(readCardResponse.getCardDetails().getExpiryDate())
                .withPurchaseAmount(this.retrieveAmount())
                .withPurchaseType(this.transactionRequest.getTransactionType().getPrintName())
                .withRrn(this.retrieveRrn())
                .withStan(stan)
                .withInvoiceNumber(invoiceNumber)
                .withTransactionMessage(this.retrieveTransactionMessage(isApproved, Integer.parseInt(this.retrieveResponseCode())))
                .withSignatureRequired(this.retrieveSignatureRequired(isApproved))
                .withDebitAcknowledgementRequired(this.transactionRequest.getTransactionType() != TransactionType.VOID)
                .withTransactionStatus(this.retrieveTransactionStatus(isApproved))
                .build();
    }

    private String retrieveAmount() {
        return StringUtils.formatAmountTwoDecimal(readCardResponse.getCardDetails().getAmount());
    }

    private boolean retrieveSignatureRequired(boolean isApproved) {
        if (isApproved) {
            if (this.isSignatureRequiredTransaction())
                return true;
            switch (this.readCardResponse.getCardDetails().getCardType()) {
                case ICC:
                    return CvmResultsParser.retrieveCvmResults(readCardResponse.getCardDetails().getTagCollection()
                            .get(0x9F34).getData()).getCvm() == Cvm.SIGNATURE;
                case PICC:
                    return this.cvmForContactless() == CardHolderVerification.SIGNATURE_VERIFIED;
                case MAG:
                case MANUAL:
                    return this.readCardResponse.getCvm() == Cvm.SIGNATURE;
                default:
                    return false;
            }
        } else {
            return false;
        }
    }

    private boolean isSignatureRequiredTransaction() {
        return this.transactionRequest.getTransactionType() == TransactionType.VOID ||
                this.transactionRequest.getTransactionType() == TransactionType.AUTH_COMPLETION;
    }

    private String retrieveRrn() {
        return IsoMessageUtils.retrieveFromDataElementsAsString(this.isoMessageResponse, 37);
    }

    private String retrieveApprovalCode() {
        return IsoMessageUtils.retrieveFromDataElementsAsString(this.isoMessageResponse, 38);
    }

    private String retrieveTransactionMessage(boolean isApproved, int responseCode) {
        if (isApproved)
            return this.retrieveCvmMessage();
        else {
            String messageText = this.applicationRepository.getMessageTextByActionCode(responseCode);
            if (StringUtils.isEmpty(messageText))
                messageText = SmartVistaActionCode.getByActionCode(responseCode).getDescription();
            return messageText;
        }
    }

    private String retrieveCvmMessage() {
        if (this.isSignatureRequiredTransaction())
            return CardHolderVerification.SIGNATURE_VERIFIED.getCardHolderVerificationEn();
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case ICC:
                return CvmResultsParser.retrieveCvmResults(readCardResponse.getCardDetails().getTagCollection()
                        .get(0x9F34).getData()).getCvm().getCardHolderVerification().getCardHolderVerificationEn();
            case PICC:
                return this.cvmForContactless().getCardHolderVerificationEn();
            case MAG:
            case MANUAL:
                return this.readCardResponse.getCvm().getCardHolderVerification().getCardHolderVerificationEn();
            default:
                return "";
        }
    }

    private CardHolderVerification cvmForContactless() {
        switch (this.readCardResponse.getPiccCvm()) {
            case SIGNATURE:
                return CardHolderVerification.SIGNATURE_VERIFIED;
            case ONLINE_PIN:
                return CardHolderVerification.PIN_VERIFIED;
            case CD_CVM:
            case DEVICE_OWNER:
                return CardHolderVerification.DEVICE_OWNER_VERIFIED;
            default:
                return CardHolderVerification.NO_VERIFICATION;
        }
    }

    private String retrieveTransactionStatus(boolean isApproved) {
        if (isApproved)
            return "APPROVED"; //TODO
        else
            return "DECLINED"; //TODO
    }

    private boolean isApprovedByActionCode(int responseCode) {
        return responseCode == 0;
    }

    private EmvTags prepareEmvTags() {
        return EmvTags.Builder.defaultBuilder()
                .withAc(this.getFromTagCollection(IccData.APPLICATION_CRYPTOGRAM.getTag()))
                .withAid(this.getFromTagCollection(IccData.DF_NAME.getTag()))
                .withCardType(this.readCardResponse.getCardDetails().getCardType().getPosEntryMode())
                .withCid(this.getFromTagCollection(IccData.CID.getTag()))
                .withCvm(this.getFromTagCollection(IccData.CVM.getTag()))
                .withKernelId(this.getFromTagCollection(IccData.KERNEL_ID.getTag()))
                .withResponseCode(this.retrieveResponseCode())
                .withTvr(this.getFromTagCollection(IccData.TVR.getTag()))
                .withTsi(getFromTagCollection(IccData.TSI.getTag()))
                .build();
    }

    private String retrieveResponseCode() {
        return IsoMessageUtils.retrieveFromDataElementsAsString(this.isoMessageResponse, 39);
    }

    private String getFromTagCollection(int tag) {
        try {
            return this.readCardResponse.getCardDetails().getTagCollection().get(tag).getData();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
