package global.citytech.finpos.merchant.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import global.citytech.common.AppCountDownTimer
import global.citytech.common.Constants
import global.citytech.common.data.PosCallback
import global.citytech.common.data.PosMessage
import global.citytech.common.data.Response
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.repository.app.CommandQueueRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.Status
import global.citytech.finpos.merchant.domain.usecase.app.CommandQueueUseCase
import global.citytech.finpos.merchant.framework.datasource.app.CommandQueueDataSourceImpl
import global.citytech.finpos.merchant.presentation.model.command.execution.CommandExecutionAcknowledgmentRequest
import global.citytech.finpos.merchant.presentation.model.command.pull.PullCommandRequest
import global.citytech.finpos.merchant.presentation.model.command.pull.PullCommandResponse
import global.citytech.finpos.merchant.presentation.model.command.pull.mapToCommand
import global.citytech.finpos.merchant.presentation.model.command.receive.CommandReceiveAcknowledgmentRequest
import global.citytech.finpos.merchant.utils.ApiConstant
import global.citytech.finpos.merchant.utils.AppConstant
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/09/17 - 1:36 PM
*/

class FinposCommandService : Service() {

    val TAG = FinposCommandService::class.java.name

    private val commandQueueUseCase = CommandQueueUseCase(
        CommandQueueRepositoryImpl(
            CommandQueueDataSourceImpl(
                NiblMerchant.INSTANCE
            )
        )
    )

    companion object {
        fun start(context: Context) {
            stop(context)
            context.startService(
                Intent(
                    context.applicationContext,
                    FinposCommandService::class.java
                )
            )
        }

        private fun stop(context: Context) {
            context.stopService(
                Intent(
                    context.applicationContext,
                    FinposCommandService::class.java
                )
            )
        }
    }

    private var disposable: Disposable? = null
    private var countDownTimer = AppCountDownTimer(
        AppConstant.SERVER_PING_INTERVAL_MILLIS,
        AppConstant.SERVER_PING_INTERVAL_MILLIS,
        this::onCountDownTick,
        this::onCountDownFinish
    )

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        pingServer()
        countDownTimer.start()
        return super.onStartCommand(intent, flags, startId)
    }


    private fun onCountDownTick(millisUntilFinished: Long) {
        if (!NiblMerchant.INSTANCE.iPlatformManager.asBinder().pingBinder()) {
            printLog(TAG, ":: PLATFORM MANAGER DISCONNECTED ::", true)
        } else {
            printLog(TAG, ":: PLATFORM MANAGER CONNECTED ::", true)
            pingServer()
        }
    }

    private fun pingServer() {
        printLog(TAG, ":: FINPOS COMMAND CHECK :: STARTED", true)
        disposable = Observable.create(ObservableOnSubscribe<Void> { onSubscribe(it) })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({}, {}, {
                if (NiblMerchant.INSTANCE.isDashActivityVisible()) {
                    printLog(TAG, "::: DASH ACTIVITY VISIBLE ::: START COMMAND >>>")
                    val intent = Intent(Constants.INTENT_ACTION_POS_CALLBACK)
                    intent.putExtra(
                        Constants.KEY_BROADCAST_MESSAGE,
                        PosCallback(PosMessage.POS_MESSAGE_START_COMMAND)
                    )
                    LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
                }
            })
    }

    private fun onCountDownFinish() {
        countDownTimer.start()
    }

    override fun onBind(p0: Intent?): IBinder? = null

    private fun onSubscribe(it: ObservableEmitter<Void>) {
        printLog(TAG, "FINPOS COMMAND CHECK :: ", true)
        val serialNumber = NiblMerchant.INSTANCE.iPlatformManager.serialNumber
        serialNumber?.let {
            sendExecutionAcknowledgmentOfPreviousCommands(it)
            performPullCommand(it)
        }
        it.onComplete()
    }

    private fun performPullCommand(serialNumber: String) {
        printLog(TAG, "COMMAND SERIAL NUMBER IS NOT EMPTY :: ", true)
        val jsonRequest = Jsons.toJsonObj(PullCommandRequest(serialNumber = serialNumber))
        printLog(TAG, "FINPOS COMMAND CHECK :: Request :: $jsonRequest", true)
        val jsonResponse =
            NiblMerchant.INSTANCE.iPlatformManager.post(
                ApiConstant.COMMAND_CHECK_URL,
                jsonRequest,
                true
            )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            printLog(
                TAG,
                "FINPOS COMMAND CHECK :: Response :: ${response.code}. ${response.message}"
            )
            prepareResponse(
                Jsons.fromJsonToList(
                    response.data.toString(),
                    Array<PullCommandResponse>::class.java
                ),
                serialNumber
            )
        } else {
            printLog(
                TAG,
                "FINPOS COMMAND CHECK :: Error Code :: ${response.code}. ${response.message}"
            )
        }
    }

    private fun prepareResponse(response: List<PullCommandResponse>, serialNumber: String) {
        val listOfReceivedId = mutableListOf<String>()
        response.forEach {
            printLog(TAG, "::: COMMAND RECEIVED >>> ${it.command} ${it.id}")
            commandQueueUseCase.addCommand(it.mapToCommand())
            listOfReceivedId.add(it.id)
        }
        if (listOfReceivedId.isNotEmpty())
            sendReceiveAcknowledgmentOfCommand(listOfReceivedId, serialNumber)
    }

    private fun sendExecutionAcknowledgmentOfPreviousCommands(serialNumber: String) {
        val commandList = commandQueueUseCase.getAllCommands()
        if (commandList.isNotEmpty()) {
            commandList.forEach {
                if (it.status != Status.OPEN) {
                    val commandExecutionAcknowledgmentRequest =
                        CommandExecutionAcknowledgmentRequest(
                            serialNumber = serialNumber,
                            status = it.status.name,
                            terminalCommandId = it.id,
                            remarks = it.remarks
                        )
                    val jsonRequest = Jsons.toJsonObj(commandExecutionAcknowledgmentRequest)
                    printLog(TAG, "FINPOS COMMAND EXEC ACK :: REQUEST :: $jsonRequest", true)
                    val jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.post(
                        ApiConstant.COMMAND_EXECUTION_ACK_URL,
                        jsonRequest,
                        false
                    )
                    val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
                    printLog(
                        TAG,
                        "FINPOS COMMAND EXEC ACK :: Response :: ${response.code}. ${response.message}",
                        true
                    )
                    if (response.code == "0")
                        commandQueueUseCase.removeCommand(it)
                }
            }
        }
    }

    private fun sendReceiveAcknowledgmentOfCommand(it: MutableList<String>, serialNumber: String) {
        val commandReceiveAcknowledgmentRequest = CommandReceiveAcknowledgmentRequest(
            serialNumber = serialNumber,
            commands = it
        )
        val jsonRequest = Jsons.toJsonObj(commandReceiveAcknowledgmentRequest)
        printLog(TAG, "FINPOS COMMAND REC ACK :: Request :: $jsonRequest", true)
        val jsonResponse =
            NiblMerchant.INSTANCE.iPlatformManager.post(
                ApiConstant.COMMAND_RECEIVE_ACK_URL,
                jsonRequest,
                false
            )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            printLog(
                TAG,
                "FINPOS COMMAND REC ACK :: Response :: ${response.code}. ${response.message}"
            )
        } else {
            printLog(
                TAG,
                "FINPOS COMMAND REC ACK :: Error Code :: ${response.code}. ${response.message}"
            )
        }
    }
}