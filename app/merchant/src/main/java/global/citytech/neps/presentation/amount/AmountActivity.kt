package global.citytech.neps.presentation.amount

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.easydroid.core.utils.MessageConfig
import global.citytech.neps.BR
import global.citytech.neps.R
import global.citytech.neps.databinding.ActivityAmountBinding
import kotlinx.android.synthetic.main.activity_amount.*

/**
 * Created by Rishav Chudal on 6/17/20.
 */
class AmountActivity: AppBaseActivity<ActivityAmountBinding, AmountViewModel>(),
    View.OnClickListener,
    View.OnLongClickListener
{
    private lateinit var viewModel: AmountViewModel

    override fun getBindingVariable(): Int = BR.viewModel
    override fun getLayout(): Int = R.layout.activity_amount
    override fun getViewModel(): AmountViewModel =
        ViewModelProviders.of(this)[AmountViewModel::class.java]

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        viewModel = getViewModel()
        initObservers()
        viewModel.calculateAmount()
    }

    private fun initViews() {
        btn_digit_one.setOnClickListener(this)
        btn_digit_two.setOnClickListener(this)
        btn_digit_three.setOnClickListener(this)
        btn_digit_four.setOnClickListener(this)
        btn_digit_five.setOnClickListener(this)
        btn_digit_six.setOnClickListener(this)
        btn_digit_seven.setOnClickListener(this)
        btn_digit_eight.setOnClickListener(this)
        btn_digit_nine.setOnClickListener(this)
        btn_digit_zero.setOnClickListener(this)
        btn_digit_double_zero.setOnClickListener(this)
        img_btn_clear.setOnClickListener(this)
        btn_confirm.setOnClickListener(this)
        btn_cancel.setOnClickListener(this)
        img_btn_clear.setOnLongClickListener(this)
    }

    private fun initObservers() {
        viewModel.integralAmount.observe(
            this,
            Observer {
                val changedIntegralAmount = it
                if (changedIntegralAmount.isNotEmpty()) {
                    tv_amount.text = changedIntegralAmount
                }
            }
        )

        viewModel.fractionalAmount.observe(
            this,
            Observer {
                val changedFractionalAmount = it
                if (changedFractionalAmount.isNotEmpty()) {
                    tv_amount_decimal.text = changedFractionalAmount
                }
            }
        )
    }

    override fun onClick(view: View?) {
        return when (view!!.id) {
            R.id.img_btn_clear -> manageForClearButtonClicked()

            R.id.btn_cancel -> manageForCancelButtonClicked()

            R.id.btn_confirm -> manageForConfirmButtonClicked(
                tv_amount.text.toString()
                    .plus(tv_amount_decimal.text.toString()))

            R.id.btn_digit_double_zero -> {
                getViewModel().onNumericButtonPressed(
                    (view as Button).text.toString(),
                    7
                )
            }

            else -> getViewModel().onNumericButtonPressed(
                (view as Button).text.toString(),
                8
            )
        }
    }

    override fun onLongClick(view: View?): Boolean {
        when (view!!.id) {
            R.id.img_btn_clear -> manageForClearButtonLongClicked()
        }
        return true
    }

    private fun manageForClearButtonClicked() {
        viewModel.onClearButtonClicked()
    }

    private fun manageForCancelButtonClicked() {
        val builder = MessageConfig.Builder()
            .title(getString(R.string.title_exit))
            .message(getString(R.string.exit_msg))
            .positiveLabel(getString(R.string.title_ok))
            .negativeLabel(getString(R.string.action_cancel))
            .onPositiveClick {
                    dialogInterface, _ -> dialogInterface.dismiss()
                exitAmountPageOnCancel()
            }
            .onNegativeClick {
                    dialogInterface, _ -> dialogInterface.dismiss()
            }
        this.showConfirmationMessage(builder)
    }

    private fun manageForConfirmButtonClicked(amount: String) {
        enableOrDisableButtons(false)
        if (!amount.isEmpty()) {
            if (amount.toDouble() > 0) {
                val builder = MessageConfig.Builder()
                    .title(getString(R.string.title_confirm_amount))
                    .message(getString(R.string.title_rupees).plus(amount))
                    .positiveLabel(getString(R.string.action_confirm))
                    .negativeLabel(getString(R.string.action_cancel))
                    .onPositiveClick { _, _ -> onAmountConfirmed(amount) }
                    .onNegativeClick { _, _ -> onCancelledClicked() }
                this.showConfirmationMessage(builder)
            } else {
                enableButtonsAndShowToast(getString(R.string.error_msg_amount_zero))
            }
        } else {
            enableButtonsAndShowToast(getString(R.string.error_msg_amount_empty))
        }

    }

    private fun manageForClearButtonLongClicked() {
        viewModel.onClearButtonLongClicked()
    }

    private fun onAmountConfirmed(amount: String) {
        Log.i("AmountActivity", "Confirmed Amount ::: ".plus(amount))
        val intent = Intent()
        intent.putExtra(
            getString(R.string.intent_confirmed_amount),
            amount
        )
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun exitAmountPageOnCancel() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    private fun onCancelledClicked() {
        enableOrDisableButtons(true)
    }

    private fun enableOrDisableButtons(doEnable: Boolean) {
        btn_digit_one.isEnabled = doEnable
        btn_digit_two.isEnabled = doEnable
        btn_digit_three.isEnabled = doEnable
        btn_digit_four.isEnabled = doEnable
        btn_digit_five.isEnabled = doEnable
        btn_digit_six.isEnabled = doEnable
        btn_digit_seven.isEnabled = doEnable
        btn_digit_eight.isEnabled = doEnable
        btn_digit_nine.isEnabled = doEnable
        btn_digit_zero.isEnabled = doEnable
        btn_digit_double_zero.isEnabled = doEnable
        img_btn_clear.isEnabled = doEnable
    }

    private fun enableButtonsAndShowToast(message: String) {
        enableOrDisableButtons(true)
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}