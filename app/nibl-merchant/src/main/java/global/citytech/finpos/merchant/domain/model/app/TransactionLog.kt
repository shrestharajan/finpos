package global.citytech.finpos.merchant.domain.model.app

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import global.citytech.common.extensions.toBigDecimalCurrencyFormat
import global.citytech.common.extensions.toCurrencyFormat
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.switches.PosEntryMode
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog

/**
 * Created by Rishav Chudal on 9/22/20.
 */
@Entity(tableName = TransactionLog.TABLE_NAME)
data class TransactionLog(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_STAN)
    var stan: String,

    @ColumnInfo(name = COLUMN_TERMINAL_ID)
    var terminalId: String,

    @ColumnInfo(name = COLUMN_MERCHANT_ID)
    var merchantId: String,

    @ColumnInfo(name = COLUMN_INVOICE_NUM)
    var invoiceNumber: String,

    @ColumnInfo(name = COLUMN_RRN)
    var rrn: String? = "",

    @ColumnInfo(name = COLUMN_AUTH_CODE)
    var authCode: String? = "",

    @ColumnInfo(name = COLUMN_TRANSACTION_TYPE)
    var transactionType: String? = "",

    @ColumnInfo(name = COLUMN_ORIGINAL_TRANSACTION_TYPE)
    var origTransactionType: String? = "",

    @ColumnInfo(name = COLUMN_TRANSACTION_AMOUNT)
    var transactionAmount: Long? = 0,

    @ColumnInfo(name = COLUMN_ORIGINAL_TRANSACTION_AMOUNT)
    var origTransactionAmount: Long? = 0,

    @ColumnInfo(name = COLUMN_TRANSACTION_DATE)
    var transactionDate: String? = "",

    @ColumnInfo(name = COLUMN_TRANSACTION_TIME)
    var transactionTime: String? = "",

    @ColumnInfo(name = COLUMN_TRANSACTION_STATUS)
    var transactionStatus: String? = "",

    @ColumnInfo(name = COLUMN_POS_ENTRY_MODE)
    var posEntryMode: String? = "",

    @ColumnInfo(name = COLUMN_ORIGINAL_POS_ENTRY_MODE)
    var origPosEntryMode: String? = "",

    @ColumnInfo(name = COLUMN_POS_CONDITION_CODE)
    var posConditionCode: String? = "",

    @ColumnInfo(name = COLUMN_ORIGINAL_POS_CONDITION_CODE)
    var origPosConditionCode: String? = "",

    @ColumnInfo(name = COLUMN_RECONCILE_STATUS)
    var reconcileStatus: String? = "",

    @ColumnInfo(name = COLUMN_RECONCILE_TIME)
    var reconcileTime: String? = "",

    @ColumnInfo(name = COLUMN_RECONCILE_DATE)
    var reconcileDate: String? = "",

    @ColumnInfo(name = COLUMN_RECONCILE_BATCH_NO)
    var reconcileBatchNo: String? = "",

    @ColumnInfo(name = COLUMN_READ_CARD_RESPONSE)
    var readCardResponse: String? = "",

    @ColumnInfo(name = COLUMN_PROCESSING_CODE)
    var processingCode: String? = "",

    @ColumnInfo(name = COLUMN_RESPONSE_CODE)
    var responseCode: String? = "",

    @ColumnInfo(name = COLUMN_AUTHORIZATION_COMPLETED)
    var authorizationCompleted: Boolean? = false,

    @ColumnInfo(name = COLUMN_ORIGINAL_TRANSACTION_REFERENCE_NUMBER)
    var originalTransactionReferenceNumber: String? = "",

    @ColumnInfo(name = COLUMN_RECEIPT_LOG)
    var receiptLog: String? = "",

    @ColumnInfo(name = COLUMN_TRANSACTION_VOIDED)
    var transactionVoided: Boolean? = false,

    @ColumnInfo(name = COLUMN_TIP_ADJUSTED)
    var tipAdjusted: Boolean? = false,

    @ColumnInfo(name = COLUMN_IS_EMI_TRANSACTION)
    var isEmiTransaction: Boolean? = false,

    @ColumnInfo(name = COLUMN_EMI_INFO)
    var emiInfo: String? = "",

    @ColumnInfo(name = COLUMN_CURRENCY_CODE)
    var currencyCode: String? = "",

    @ColumnInfo(name = COLUMN_VAT_INFO)
    var vatInfo: String? = ""

) {
    companion object {
        const val TABLE_NAME = "transactions"
        const val COLUMN_STAN = "stan"
        const val COLUMN_TERMINAL_ID = "terminal_id"
        const val COLUMN_MERCHANT_ID = "merchant_id"
        const val COLUMN_INVOICE_NUM = "invoice_num"
        const val COLUMN_RRN = "retrieval_reference_number"
        const val COLUMN_AUTH_CODE = "auth_code"
        const val COLUMN_TRANSACTION_TYPE = "transaction_type"
        const val COLUMN_ORIGINAL_TRANSACTION_TYPE = "original_transaction_type"
        const val COLUMN_TRANSACTION_AMOUNT = "transaction_amount"
        const val COLUMN_ORIGINAL_TRANSACTION_AMOUNT = "original_transaction_amount"
        const val COLUMN_TRANSACTION_DATE = "transaction_date"
        const val COLUMN_TRANSACTION_TIME = "transaction_time"
        const val COLUMN_TRANSACTION_STATUS = "transaction_status"
        const val COLUMN_POS_ENTRY_MODE = "pos_entry_mode"
        const val COLUMN_ORIGINAL_POS_ENTRY_MODE = "original_pos_entry_mode"
        const val COLUMN_POS_CONDITION_CODE = "pos_condition_code"
        const val COLUMN_ORIGINAL_POS_CONDITION_CODE = "original_pos_condition_code"
        const val COLUMN_RECONCILE_STATUS = "reconcile_status"
        const val COLUMN_RECONCILE_TIME = "reconcile_time"
        const val COLUMN_RECONCILE_DATE = "reconcile_date"
        const val COLUMN_RECONCILE_BATCH_NO = "reconcile_batch_no"
        const val COLUMN_READ_CARD_RESPONSE = "read_card_response"
        const val COLUMN_PROCESSING_CODE = "processing_code"
        const val COLUMN_RESPONSE_CODE = "response_code"
        const val COLUMN_AUTHORIZATION_COMPLETED = "authorization_completed"
        const val COLUMN_ORIGINAL_TRANSACTION_REFERENCE_NUMBER = "original_txn_ref_number"
        const val COLUMN_RECEIPT_LOG = "receipt_log"
        const val COLUMN_TRANSACTION_VOIDED = "transaction_voided"
        const val COLUMN_TIP_ADJUSTED = "tip_adjusted"
        const val COLUMN_IS_EMI_TRANSACTION = "is_emi_trnasaction"
        const val COLUMN_EMI_INFO = "emi_info"
        const val COLUMN_CURRENCY_CODE = "currency_code"
        const val COLUMN_VAT_INFO = "vat_info"
    }

    override fun toString(): String {
        return "TransactionLog(stan='$stan', terminalId='$terminalId', merchantId='$merchantId', invoiceNumber='$invoiceNumber', rrn=$rrn, authCode=$authCode, transactionType=$transactionType, origTransactionType=$origTransactionType, transactionAmount=$transactionAmount, origTransactionAmount=$origTransactionAmount, transactionDate=$transactionDate, transactionTime=$transactionTime, transactionStatus=$transactionStatus, posEntryMode=$posEntryMode, origPosEntryMode=$origPosEntryMode, posConditionCode=$posConditionCode, origPosConditionCode=$origPosConditionCode, reconcileStatus=$reconcileStatus, reconcileTime=$reconcileTime, reconcileDate=$reconcileDate, reconcileBatchNo=$reconcileBatchNo, readCardResponse=$readCardResponse, responseCode=$responseCode, authorizationCompleted=$authorizationCompleted, originalTransactionReferenceNumber=$originalTransactionReferenceNumber, receiptLog=$receiptLog, transactionVoided=$transactionVoided, isEmiTransaction=$isEmiTransaction, emiInfo=$emiInfo, currencyCode=$currencyCode), vatInfo=$vatInfo)"
    }
}

fun TransactionLog.mapToUseCase(): global.citytech.finposframework.usecases.transaction.TransactionLog {
    var receiptLogObject: ReceiptLog? = null
    var readCardResponseObject: ReadCardResponse? = null
    var transactionTypeEnum: TransactionType? = null
    var originalTransactionTypeEnum: TransactionType? = null
    var posEntryModeEnum: PosEntryMode? = null
    var originalPosEntryModeEnum: PosEntryMode? = null
    var tipAdjustedVar: Boolean? = this.tipAdjusted
    try {
        if (Jsons.isValidJsonString(this.receiptLog))
            receiptLogObject = Jsons.fromJsonToObj(this.receiptLog, ReceiptLog::class.java)
        if (Jsons.isValidJsonString(this.readCardResponse))
            readCardResponseObject =
                Jsons.fromJsonToObj(this.readCardResponse, ReadCardResponse::class.java)
        if (Jsons.isValidJsonString(this.transactionType))
            transactionTypeEnum =
                Jsons.fromJsonToObj(this.transactionType, TransactionType::class.java)
        if (Jsons.isValidJsonString(this.origTransactionType))
            originalTransactionTypeEnum =
                Jsons.fromJsonToObj(this.origTransactionType, TransactionType::class.java)
        if (Jsons.isValidJsonString(this.posEntryMode))
            posEntryModeEnum = Jsons.fromJsonToObj(this.posEntryMode, PosEntryMode::class.java)
        if (Jsons.isValidJsonString(this.origPosEntryMode))
            originalPosEntryModeEnum =
                Jsons.fromJsonToObj(this.origPosEntryMode, PosEntryMode::class.java)
        if (this.tipAdjusted == null)
            tipAdjustedVar = false
    } catch (e: Exception) {
        e.printStackTrace()
    }

    var isEmiTransactionTemp = false
    this.isEmiTransaction?.let {
        isEmiTransactionTemp = it
    }
    return global.citytech.finposframework.usecases.transaction.TransactionLog.Builder.newInstance()
        .withStan(this.stan)
        .withInvoiceNumber(this.invoiceNumber)
        .withRrn(this.rrn)
        .withAuthCode(this.authCode)
        .withTransactionType(transactionTypeEnum)
        .withOriginalTransactionType(originalTransactionTypeEnum)
        .withTransactionAmount(this.transactionAmount!!.toBigDecimalCurrencyFormat())
        .withOriginalTransactionAmount(this.origTransactionAmount!!.toBigDecimalCurrencyFormat())
        .withTransactionDate(this.transactionDate)
        .withTransactionTime(this.transactionTime)
        .withTransactionStatus(this.transactionStatus)
        .withPosEntryMode(posEntryModeEnum)
        .withOriginalPosEntryMode(originalPosEntryModeEnum)
        .withPosConditionCode(this.posConditionCode)
        .withOriginalPosConditionCode(this.origPosConditionCode)
        .withReconcileStatus(this.reconcileStatus)
        .withReconcileTime(this.reconcileTime)
        .withReconcileDate(this.reconcileDate)
        .withReadCardResponse(readCardResponseObject)
        .withResponseCode(this.responseCode)
        .withAuthorizationCompleted(this.authorizationCompleted!!)
        .withOriginalTransactionReferenceNumber(this.originalTransactionReferenceNumber)
        .withReceiptLog(receiptLogObject)
        .withTransactionVoided(this.transactionVoided!!)
        .withProcessingCode(this.processingCode)
        .withTerminalId(this.terminalId)
        .withMerchantId(this.merchantId)
        .withIsEmiTransaction(isEmiTransactionTemp)
        .withEmiInfo(this.emiInfo)
        .withCurrencyCode(this.currencyCode)
        .withVatInfo(this.vatInfo)
        .withTipAdjusted(tipAdjustedVar!!)
        .build()
}