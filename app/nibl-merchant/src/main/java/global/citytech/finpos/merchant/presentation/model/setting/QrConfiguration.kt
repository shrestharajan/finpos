package global.citytech.finpos.merchant.presentation.model.setting

import global.citytech.finpos.merchant.presentation.model.qroperator.QrOperatorItem

data class QrConfiguration(
    val qrOperator: String,
    val merchantCategoryCode: String,
    val currency: String,
    val merchantId: String,
    val terminalId: String,
    val enableStaticQr: Boolean,
    val enableDynamicQr: Boolean,
    val qrOperatorInfo: QrOperatorItem
)