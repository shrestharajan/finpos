package global.citytech.finpos.merchant.domain.model.receipt;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.transaction.data.StatementList;

/**
 * Created by Saurav Ghimire on 1/26/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class CustomerCopyRequestEntity {
    private TransactionRepository transactionRepository;
    private PrinterService printerService;
    private StatementList statementList;


    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public StatementList getStatementList(){
        return statementList;
    }

    public static final class Builder {
        TransactionRepository transactionRepository;
        PrinterService printerService;
        StatementList statementList;

        private Builder() {
        }

        public static CustomerCopyRequestEntity.Builder newInstance() {
            return new CustomerCopyRequestEntity.Builder();
        }

        public CustomerCopyRequestEntity.Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public CustomerCopyRequestEntity.Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public CustomerCopyRequestEntity.Builder withMiniStatementData(StatementList statementList){
            this.statementList = statementList;
            return this;
        }

        public CustomerCopyRequestEntity build() {
            CustomerCopyRequestEntity customerCopyRequestModel = new CustomerCopyRequestEntity();
            customerCopyRequestModel.transactionRepository = this.transactionRepository;
            customerCopyRequestModel.printerService = this.printerService;
            customerCopyRequestModel.statementList = this.statementList;
            return customerCopyRequestModel;
        }
    }
}
