package global.citytech.finpos.device.weipass.emv.revokedkeys

import com.wiseasy.emvprocess.KernelInit
import com.wiseasy.emvprocess.bean.CAPKRevoke
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finposframework.hardware.common.Result
import io.mockk.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by Rishav Chudal on 4/30/20.
 */
class WeipassRevokedKeysServiceImplTest {

    private lateinit var SUT: WeipassRevokedKeysServiceImpl
    private val exceptionMessage = "This is a test exception"

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        this.SUT =
            WeipassRevokedKeysServiceImpl()
        mockKernelInit()
        mockWeipassLogger()
    }

    private fun mockKernelInit() {
        mockkStatic(KernelInit::class)
    }

    private fun mockWeipassLogger() {
        mockkObject(WeipassLogger)
        mockkObject(WeipassLogger.Companion)
        every { WeipassLogger.log(any()) } just Runs
    }

    private fun prepareMockRevokedKeysInjectionRequest(): global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKeysInjectionRequest {
        val revokedKeyList = listOf(prepareMockRevokedKey(), prepareMockRevokedKey())
        return global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKeysInjectionRequest(
            revokedKeyList
        )
    }

    private fun prepareMockRevokedKey(): global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKey {
        return global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKey(
            "testRevokedRid",
            "testRevokedKeyIndex",
            "testCertificateSerialNumber"
        )
    }

    /*
     * Revoked Keys Injection tests
     */
    @Test
    fun injectRevokedKeys_WhenRevokedKeysInjectionRequestIsProvided_ReturnsSuccessResponse() {
        //Setup
        val request = prepareMockRevokedKeysInjectionRequest()
        every { KernelInit.addRemovedCAPK(any()) } returns 0
        every { KernelInit.deleteCAPKRemoved() } returns 0

        //Run
        val response = this.SUT.injectRevokedKeys(request)

        //Verify
        verify { KernelInit.deleteCAPKRemoved() }
        verify (exactly = request.revokedKeysList.size){
            KernelInit.addRemovedCAPK(any())
        }
        assertFalse(request.revokedKeysList.isEmpty())
        assertFalse(request.revokedKeysList.get(0).rid.isNullOrEmptyOrBlank())
        assertFalse(request.revokedKeysList.get(0).index.isNullOrEmptyOrBlank())
        assertFalse(request.revokedKeysList.get(0).certificateSerialNumber.isNullOrEmptyOrBlank())
        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun injectRevokedKeys_WhenRevokedKeysListIsEmtpy_ReturnsFailureResponse() {
        //Setup
        val request =
            global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKeysInjectionRequest(
                emptyList()
            )

        //Run
        val response = SUT.injectRevokedKeys(request)

        //Verify
        assertEquals(Result.FAILURE, response.result)
    }

    @Test
    fun injectRevokedKeys_ForEachRevokedKeyInjectionResultInCoreIsZero_ReturnsSuccessResponse() {
        //Setup
        val request = prepareMockRevokedKeysInjectionRequest()
        val slot = slot<CAPKRevoke>()
        every { KernelInit.addRemovedCAPK(capture(slot)) } returns 0
        every { KernelInit.deleteCAPKRemoved() } returns 0

        //Run
        val response = SUT.injectRevokedKeys(request)

        //Verify
        verify { KernelInit.deleteCAPKRemoved() }
        verify (exactly = request.revokedKeysList.size){
            KernelInit.addRemovedCAPK(any())
        }

        assertTrue(slot.isCaptured)
        val coreRevokedKey = slot.captured

        assertFalse(coreRevokedKey.rid.isNullOrEmptyOrBlank())
        assertTrue(coreRevokedKey.rid.equals(request.revokedKeysList.get(0).rid))

        assertFalse(coreRevokedKey.index.isNullOrEmptyOrBlank())
        assertTrue(coreRevokedKey.index.equals(request.revokedKeysList.get(0).index))

        assertFalse(coreRevokedKey.certificateSN.isNullOrEmptyOrBlank())
        assertTrue(coreRevokedKey.certificateSN
            .equals(request.revokedKeysList.get(0).certificateSerialNumber))

        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun injectRevokedKeys_ExceptionInRevokedKeyInjectionInCore_ReturnsSuccessResponse() {
        //Setup
        val request = prepareMockRevokedKeysInjectionRequest()
        every { KernelInit.deleteCAPKRemoved() } returns 0
        every { KernelInit.addRemovedCAPK(any()) } throws Exception(exceptionMessage)

        //Run
        val response = SUT.injectRevokedKeys(request)

        //Verify
        verify { KernelInit.deleteCAPKRemoved() }
        verify (exactly = request.revokedKeysList.size){
            WeipassLogger.log("Exception in injecting revoked key ::: ".plus(exceptionMessage))
        }
        assertEquals(Result.SUCCESS, response.result)
    }

    /*
     * Erase all revoked keys tests
     */
    @Test
    fun eraseAllRevokedKeys_WhenEraseRevokedKeysResultInCoreIsZero_ReturnsSuccessResponse() {
        //Setup
        every { KernelInit.deleteCAPKRemoved() } returns 0

        //Run
        val response = SUT.eraseAllRevokedKeys()

        //Verify
        verify { KernelInit.deleteCAPKRemoved() }
        verify { WeipassLogger.log("Revoked Keys Erase Result ::: ".plus(0)) }
        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun eraseAllRevokedKeys_WhenEraseRevokedKeysResultInCoreIsNotZero_ReturnsFailureResponse() {
        //Setup
        every { KernelInit.deleteCAPKRemoved() } returns -1

        //Run
        val response = SUT.eraseAllRevokedKeys()

        //Verify
        verify { KernelInit.deleteCAPKRemoved() }
        verify { WeipassLogger.log("Revoked Keys Erase Result ::: ".plus(-1)) }
        verify { WeipassLogger.log("Couldn't clear revoked keys") }
        assertEquals(Result.FAILURE, response.result)
    }
}