package global.citytech.finpos.merchant.presentation.cashin

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.finpos.merchant.data.repository.core.cashin.CashInRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.receipt.CustomerCopyRepositoryImpl
import global.citytech.finpos.merchant.domain.model.receipt.CustomerCopyResponseEntity
import global.citytech.finpos.merchant.domain.usecase.core.cashin.CoreCashInUsecase
import global.citytech.finpos.merchant.domain.usecase.core.receipt.CoreCustomerCopyUseCase
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.cashin.CashInDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.receipt.CustomerCopyDataSourceImpl
import global.citytech.finpos.merchant.presentation.greenpin.GreenPinBaseTransactionViewModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.cashin.CashInRequestItem
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.repositories.TerminalRepository
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.data.StatementList
import global.citytech.finposframework.usecases.transaction.data.StatementItems
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CashInViewModel(val instance: Application) : GreenPinBaseTransactionViewModel(instance) {

    private var cashInUseCase: CoreCashInUsecase =
        CoreCashInUsecase(CashInRepositoryImpl(CashInDataSourceImpl()))
    val cashInResult by lazy { MutableLiveData<Boolean>() }
    private var customerCopyUseCase: CoreCustomerCopyUseCase =
        CoreCustomerCopyUseCase(CustomerCopyRepositoryImpl(CustomerCopyDataSourceImpl()))


    fun generateCashInRequest(
        configurationItem: ConfigurationItem,
        transactionType: TransactionType,
        displayAmount: String
    ) {
        terminalRepository= TerminalRepositoryImpl(configurationItem)
        compositeDisposable.add(
            cashInUseCase.generateCashInRequest(
                configurationItem,
                CashInRequestItem(
                    transactionType = transactionType,
                    amount = displayAmount.toBigDecimal()
                )
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    cashInResult.value = it.isApproved
                    if (!cashInResult.value!!) {
                        failureMessage.value = it.message
                    }
                }, {
//                    isLoading.value = false
                    message.value = it.message
                    cashInResult.value = false
                })
        )

    }
fun printCashInStatement(amount:String){
    isLoading.value = true
    val emptyListOfStatementItems: List<StatementItems> = emptyList()
    compositeDisposable.add(
        customerCopyUseCase.printStatement(statementList = StatementList(emptyListOfStatementItems,"","",amount, AppUtility.checkCurrencyCode(currencyCode)))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                onCustomerCopyResponseEntityReceived(it)
            }, {
                customerCopyPrintMessage.postValue("Customer Print Failure")
                isLoading.value = false
            })
    )
}
    private fun onCustomerCopyResponseEntityReceived(it: CustomerCopyResponseEntity) {
        isLoading.value = false
        if (it.result == Result.SUCCESS || it.result == Result.CANCEL) {
            checkForCardPresent()
            return
        }
        customerCopyPrintMessage.postValue(it.message)
    }
}