package global.citytech.finpos.processor.nibl.greenpin.pinset;


import java.math.BigDecimal;

import java.util.List;

import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finpos.nibl.iso8583.S2MActionCode;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.greenpin.PinSetRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.greenpin.PinSetRequestSender;
import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
import global.citytech.finpos.processor.nibl.transaction.TransactionRequestModel;
import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor;
import global.citytech.finpos.processor.nibl.transaction.card.MagneticCardProcessor;
import global.citytech.finpos.processor.nibl.transaction.card.PICCCardProcessor;
import global.citytech.finpos.processor.nibl.transaction.receipt.NiblIsoMessageReceiptHandler;
import global.citytech.finpos.processor.nibl.transaction.template.TransactionUseCaseTemplate;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardSummary;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.PinBlock;
import global.citytech.finposframework.hardware.io.cards.cvm.Cvm;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat;
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest;
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.TransactionStatusFromHost;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.CardProcessor;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.ServiceCodeUtils;
import global.citytech.finposframework.utility.StringUtils;

public class PinSetUseCase extends TransactionUseCaseTemplate {

    private String userPin = "";
    private String OTP = "";
    IsoMessageResponse isoMessageResponse;
    TransactionResponseModel transactionResponse;
    private CardProcessor cardProcessor;
    private String rightPadding="FFFFFFFFFFFF";

    public PinSetUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @java.lang.Override
    protected String getTransactionTypeClassName() {
        return null;
    }

    protected void retrieveCardDetails(List<CardType> allowedCardTypes, boolean isForceContactCardPrompt) {

    }

    public void setCardResponse(ReadCardResponse readCardResponse, TransactionRequest transactionRequest, TransactionRequestModel request, String stan, String otp, CardProcessor cardProcessor) {
        this.readCardResponse = readCardResponse;
        this.request = request;
        this.stan = stan;
        this.OTP = otp;
    }

    private void retrieveCardProcessor() {
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case MAG:
                cardProcessor = new MagneticCardProcessor(
                        this.request.getTransactionRepository(),
                        this.request.getTransactionAuthenticator(),
                        this.request.getApplicationRepository(),
                        this.request.getReadCardService(),
                        this.notifier,
                        terminalRepository
                );
                break;
            case ICC:
                cardProcessor = new IccCardProcessor(
                        this.request.getReadCardService(),
                        this.request.getTransactionRepository(),
                        this.request.getApplicationRepository(),
                        this.request.getTransactionAuthenticator(),
                        this.notifier,
                        terminalRepository
                );
                break;
            case PICC:
                cardProcessor = new PICCCardProcessor(
                        this.request.getTransactionRepository(),
                        this.request.getApplicationRepository(),
                        this.request.getReadCardService(),
                        this.notifier,
                        this.request.getTransactionAuthenticator(),
                        this.request.getLedService(),
                        this.request.getSoundService(),
                        terminalRepository
                );
                break;
            default:
                break;
        }
    }

    @Override
    public TransactionResponseModel execute(TransactionRequestModel request) {
        setUpRequiredTransactionFields(request);
         processCard();
         return transactionResponse;
    }

    protected void setUpRequiredTransactionFields(TransactionRequestModel request) {
        this.request = request;
        this.stan = this.terminalRepository.getSystemTraceAuditNumber();
        this.transactionRequest = this.request.getTransactionRequest();
        retrieveCardProcessor();
    }

    protected void processCard() {
        PurchaseRequest purchaseRequest = preparePurchaseRequest(
                transactionRequest,
                readCardResponse.getCardDetails()
        );
        this.notifier.notify(Notifier.EventType.PROCESSING, "Please wait...");
        PinBlock pinBlockResponse = this.retrieveInitialPinBlock(purchaseRequest);
        transactionResponse=  proceedConfirmPin(purchaseRequest, pinBlockResponse);
        }

    protected TransactionIsoRequest prepareTransactionIsoRequest() {
        return PinSetRequest.Builder
                .newInstance()
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .transactionAmount(BigDecimal.valueOf(0.00))
                .localDate(HelperUtils.getDefaultLocaleDateWithoutYear())
                .localTime(HelperUtils.getDefaultLocaleTimeHhMmSs())
                .expireDate(readCardResponse.getCardDetails().getExpiryDate())
                .posEntryMode(this.retrievePosEntryMode())
                .posConditionCode("00")
                .track2Data(readCardResponse.getCardDetails().getTrackTwoData())
                .additionalData(userPin)
                .rrn("")
                .pinBlock(userPin)
                .otp(OTP+rightPadding)
                .currencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .emvData(readCardResponse.getCardDetails().getIccDataBlock())
                .cardSequenceNumber(readCardResponse.getCardDetails().getPrimaryAccountNumberSerialNumber())
                .build();
    }

    protected NIBLMessageSenderTemplate getRequestSender(RequestContext context) {
        return new PinSetRequestSender(new NIBLSpecInfo(), context);
    }

    protected void processOnlineResultToCard(IsoMessageResponse isoMessageResponse) {

    }

    protected void processUnableToGoOnlineResultToCard() {

    }

    protected boolean checkTransactionDeclinedByCard(IsoMessageResponse isoMessageResponse) {
        return false;
    }

    protected void notifyWithLed(boolean isApproved) {

    }

    protected String getOriginalPosConditionCode() {
        return null;
    }

    protected PosEntryMode getOriginalPosEntryMode() {
        return null;
    }

    protected PurchaseRequest preparePurchaseRequest(TransactionRequest transactionRequest, CardDetails cardDetails) {
        PurchaseRequest purchaseRequest = new PurchaseRequest(
                transactionRequest.getTransactionType(),
                transactionRequest.getAmount(),
                transactionRequest.getAdditionalAmount()
        );
        cardDetails.setAmount(transactionRequest.getAmount());
        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
        purchaseRequest.setCardDetails(cardDetails);
        return purchaseRequest;

    }

    TransactionResponseModel proceedConfirmPin(PurchaseRequest purchaseRequest, PinBlock pinBlock) {
        PinBlock confirmPinBlockResponse = this.retrieveConfirmPinBlock(purchaseRequest);
        if (confirmPinBlockResponse.getPin().equals(pinBlock.getPin())) {
            userPin = confirmPinBlockResponse.getPin();
            logger.log("PIN MATCHED");
            return transactionResponse = performTransactionWithHost();
        } else {
            logger.log("PIN NOT MATCHED");
            this.notifier.notify(Notifier.EventType.PIN_UNMATCH, "Please wait...");
           return proceedConfirmPin(purchaseRequest, pinBlock);
        }
    }

    private void printDebugReceipt(IsoMessageResponse isoMessageResponse) {
        if (this.terminalRepository.findTerminalInfo().isDebugModeEnabled()) {
            ReceiptHandler.IsoMessageReceiptHandler receiptHandler = new NiblIsoMessageReceiptHandler(this.request.getPrinterService());
            receiptHandler.printIsoMessageReceipt(isoMessageResponse);
        }
    }

    protected TransactionResponseModel performTransactionWithHost() {
        this.notifier.notify(Notifier.EventType.PROCESSING, "Transmitting Request...");
        try {
            isoMessageResponse = this.sendIsoMessage();
            printDebugReceipt(isoMessageResponse);
            updateTransactionIds(false);
            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait...");
            InvalidResponseHandlerResponse invalidResponseHandlerResponse = this.checkForInvalidResponseFromHost(isoMessageResponse);
            if (invalidResponseHandlerResponse.isInvalid()) {
                return this.handleInvalidResponseFromHost(invalidResponseHandlerResponse);
            }
            boolean isApproved = this.isTransactionApprovedByActionCode(isoMessageResponse);
            if (isApproved) {
                return this.prepareTransactionResponse(isoMessageResponse, isApproved, false, "");
            } else {
                return this.prepareDeclinedTransactionResponse("", false);
            }
        } catch (FinPosException fpe) {
            if (this.unableToGoOnlineException(fpe))
                throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
            else
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
    }

    private IsoMessageResponse sendIsoMessage() {
        RequestContext context = this.prepareTemplateRequestContext();
        this.transactionIsoRequest = this.prepareTransactionIsoRequest();
        context.setRequest(transactionIsoRequest);
        NIBLMessageSenderTemplate sender = this.getRequestSender(context);
        return sender.send();
    }

    protected TransactionResponseModel handleUnableToGoOnlineTransaction() {
        return this.prepareDeclinedTransactionResponse("TIMEOUT", false);
    }

    protected RequestContext prepareTemplateRequestContext() {
        HostInfo primaryHostInfo = this.terminalRepository.findPrimaryHost();
        HostInfo secondaryHostInfo = this.terminalRepository.findSecondaryHost();
        TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
        ConnectionParam connectionParam = new ConnectionParam(primaryHostInfo, secondaryHostInfo);
        return new RequestContext(this.stan, terminalInfo, connectionParam);
    }

    protected TransactionResponseModel handleInvalidResponseFromHost(InvalidResponseHandlerResponse invalidResponseHandlerResponse) {
        return this.prepareDeclinedTransactionResponse(invalidResponseHandlerResponse.getMessage(),
                false);
    }


    protected boolean isTransactionApprovedByActionCode(IsoMessageResponse isoMessageResponse) {
        int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse,
                DataElement.RESPONSE_CODE);
        return S2MActionCode.getByActionCode(actionCode).getTransactionStatusFromHost() == TransactionStatusFromHost.ONLINE_APPROVED;
    }

    protected PinBlock retrieveInitialPinBlock(PurchaseRequest purchaseRequest) {
        PinBlock pinBlock = new PinBlock("", true);
        String pinPadAmountMessage = "";
        pinBlock = this.retrievePinBlock(
                purchaseRequest
                        .getCardDetails()
                        .getPrimaryAccountNumber(),
                pinPadAmountMessage,
                "Enter Pin"

        );

        Cvm cvm = this.retrieveCvm(
                purchaseRequest.getTransactionType(),
                purchaseRequest.getCardDetails()
        );

        ReadCardResponse readCardResponse = new ReadCardResponse(
                purchaseRequest.getCardDetails(),
                Result.SUCCESS,
                "Successful"
        );
        readCardResponse.setCvm(cvm);
        readCardResponse.getCardDetails().setPinBlock(pinBlock);
        readCardResponse
                .getCardDetails()
                .setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
        readCardResponse.setFallbackIccToMag(purchaseRequest.isFallbackIccToMag());

        return pinBlock;
    }

    private Cvm retrieveCvm(TransactionType transactionType, CardDetails cardDetails) {
        if (ServiceCodeUtils.isPinRequired(cardDetails.getTrackTwoData()))
            return Cvm.ENCIPHERED_PIN_ONLINE;
        return Cvm.SIGNATURE;
    }

    protected PinBlock retrieveConfirmPinBlock(PurchaseRequest purchaseRequest) {
        PinBlock pinBlock = new PinBlock("", true);
        String pinPadAmountMessage = "";
        pinBlock = this.retrievePinBlock(
                purchaseRequest
                        .getCardDetails()
                        .getPrimaryAccountNumber(),
                pinPadAmountMessage,
                "Confirm Pin"
        );
        return pinBlock;
    }

    private PinBlock retrievePinBlock(String primaryAccountNumber, String pinPadAmountMessage, String pinPadMessage) {
        if (!StringUtils.isEmpty(primaryAccountNumber)) {
            CardSummary cardSummary = new CardSummary(readCardResponse.getCardDetails().getCardSchemeLabel(),
                    primaryAccountNumber,
                    readCardResponse.getCardDetails().getCardHolderName(),
                    readCardResponse.getCardDetails().getExpiryDate());
            PinRequest pinRequest = new PinRequest(
                    primaryAccountNumber,
                    pinPadMessage,
                    PinBlockFormat.ISO9564_FORMAT_0,
                    4,
                    4,
                    30,
                    pinPadAmountMessage,
                    true,
                    false,
                    ""

            );
            pinRequest.setCardSummary(cardSummary);
            PinResponse response = request.getTransactionAuthenticator().authenticateUser(pinRequest);
            this.logger.debug("::: PIN RESPONSE ::: " + response.toString());
            if (response.getResult() == Result.SUCCESS)
                return response.getPinBlock();
            else if (response.getResult() == Result.USER_CANCELLED)
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            else if (response.getResult() == Result.TIMEOUT || response.getResult() == Result.FAILURE) {
                throw  new PosException(PosError.DEVICE_ERROR_CARD_READ_TIMEOUT);
            } else
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    protected String getClassName() {
        return null;
    }


    protected NIBLMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context) {
        return null;
    }

    protected void processOnlineResult(IsoMessageResponse isoMessageResponse) {

    }

    protected void updateTransactionLog(boolean transactionApproved, TransactionRequest transactionRequest, TransactionRepository transactionRepository, IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse) {

    }

    protected TransactionResponseModel prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository, boolean shouldPrint) {
        return PinSetResponse.Builder.newInstance()
                .withStan(stan)
                .withMessage("Pin Set Successful")
                .withDebugRequestMessage(isoMessageResponse.getDebugRequestString())
                .withDebugResponseMessage(isoMessageResponse.getDebugResponseString())
                .withMessage(
                        this.retrieveMessage(
                                isoMessageResponse,
                                transactionApproved
                        )
                ).build();
    }

    protected String getProcessingCode() {
        return null;
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy) {
        return PinSetResponse.Builder.newInstance()
                .withApproved(isApproved)
                .withMessage(this.retrieveMessage(isoMessageResponse, isApproved))
                .withDebugRequestMessage(isoMessageResponse.getDebugRequestString())
                .withDebugResponseMessage(isoMessageResponse.getDebugResponseString())
                .withStan(this.stan)
                .withShouldPrintCustomerCopy(shouldPrintCustomerCopy)
                .build();
    }

    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy, String miniStatementData) {
        return PinSetResponse.Builder.newInstance()
                .withApproved(isApproved)
                .withMessage(this.retrieveMessage(isoMessageResponse, isApproved))
                .withDebugRequestMessage(isoMessageResponse.getDebugRequestString())
                .withDebugResponseMessage(isoMessageResponse.getDebugResponseString())
                .withStan(this.stan)
                .withShouldPrintCustomerCopy(shouldPrintCustomerCopy)
                .build();
    }


    protected TransactionResponseModel prepareDeclinedTransactionResponse(String message, boolean shouldPrint) {
        return PinSetResponse.Builder.newInstance()
                .withShouldPrintCustomerCopy(shouldPrint)
                .withStan(stan)
                .withApproved(false)
                .withMessage(this.retrieveMessage(isoMessageResponse, false))
                .build();
    }

    protected boolean isForceContactCardPromptActionCode(IsoMessageResponse isoMessageResponse) {
        return false;
    }

    protected String getOriginalTransactionReferenceNumber() {
        return null;
    }

    protected boolean isAuthorizationCompleted() {
        return false;
    }

    protected void updateTransactionLogOfOriginalTransaction(boolean isApproved) {

    }

    protected boolean shouldIncrementInvoiceNumber(boolean isApproved) {
        return false;
    }

    protected String retrieveInvoiceNumber() {
        return null;
    }

    protected TransactionLog retrieveTransactionLogToStore(boolean isApproved, IsoMessageResponse isoMessageResponse) {
        return null;
    }

    private PosEntryMode retrievePosEntryMode() {
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case MAG:
                if (this.readCardResponse.getFallbackIccToMag())
                    return PosEntryMode.ICC_FALLBACK_TO_MAGNETIC;
                else
                    return PosEntryMode.MAGNETIC_STRIPE;
            case ICC:
                return PosEntryMode.ICC;
            case PICC:
                return PosEntryMode.PICC;
            default:
                return PosEntryMode.UNSPECIFIED;
        }
    }

}
