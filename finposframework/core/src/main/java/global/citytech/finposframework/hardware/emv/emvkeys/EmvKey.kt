package global.citytech.finposframework.hardware.emv.emvkeys

/**
 * Created by Rishav Chudal on 4/29/20.
 */
data class EmvKey (val rid: String,
                   val index: String,
                   val length: String,
                   val exponent: String,
                   val modulus: String,
                   val hashId: String,
                   val keySignatureId: String,
                   val checkSum: String,
                   val expiryDate: String
)
