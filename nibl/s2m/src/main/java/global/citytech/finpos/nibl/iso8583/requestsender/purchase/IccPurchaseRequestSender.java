package global.citytech.finpos.nibl.iso8583.requestsender.purchase;

import global.citytech.finpos.nibl.iso8583.PosEntryModeConfig;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Rishav Chudal on 10/14/20.
 */
public class IccPurchaseRequestSender extends PurchaseRequestSender {

    public IccPurchaseRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
    }

    @Override
    public Iso8583Msg addConditionalFields(Iso8583Msg msg, TransactionIsoRequest transactionIsoRequest) {
        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(PosEntryMode.ICC)
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.APPLICATION_PAN_SEQUENCE_NUMBER,
                        StringUtils.ofRequiredLength(
                                transactionIsoRequest.getCardSequenceNumber(),
                                4
                        )
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.TRACK_II_DATA,
                        transactionIsoRequest.getTrack2Data()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.ICC_SYSTEM_RELATED_DATA,
                        transactionIsoRequest.getEmvData()
                )
        );
        return msg;
    }
}
