package global.citytech.finpos.nibl.admin.presentation.configuration

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.nibl.admin.R
import global.citytech.finpos.nibl.admin.data.response.ConfigResponse
import kotlinx.android.synthetic.main.activity_configuration.*

class ConfigurationActivity : AppCompatActivity() {

    lateinit var configResponse: ConfigResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configuration)
        val bundle = intent.getBundleExtra("BUNDLE")
        val configString = bundle.getString("Config")
        configResponse = Jsons.fromJsonToObj(configString, ConfigResponse::class.java)

        initViews()
    }

    private fun initViews() {

        val hosts = configResponse.hosts
        hosts!!.sortedBy { it.order }
        val primaryHost = hosts[0]
        val secondaryHost = hosts[1]
        val merchant = configResponse.merchant
        val terminal = configResponse.terminal

        primary_ip.text = primaryHost.ip
        primary_port.text = "${primaryHost.port}"
        primary_nii.text = primaryHost.nii
        primary_connection_time_out.text = "${primaryHost.connectionTimeout}"
        primary_retry_limit.text = "${primaryHost.retryLimit}"

        secondary_ip.text = secondaryHost.ip
        secondary_port.text = "${secondaryHost.port}"
        secondary_nii.text = secondaryHost.nii
        secondary_connection_time_out.text = "${secondaryHost.connectionTimeout}"
        secondary_retry_limit.text = "${secondaryHost.retryLimit}"

        switch_id.text = merchant!!.switchId
        merchant_name.text = merchant.name
        merchant_address.text = merchant.address
        merchant_phone_number.text = merchant.phoneNumber

        terminal_id.text = terminal!!.switchId

        iv_back.setOnClickListener {
            finish()
        }
    }
}
