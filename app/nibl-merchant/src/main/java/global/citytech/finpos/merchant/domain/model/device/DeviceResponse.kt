package global.citytech.finpos.merchant.domain.model.device

/**
 * Created by Rishav Chudal on 8/10/20.
 */
open class DeviceResponse(
    val result: DeviceResult,
    val message: String
) {

}