package global.citytech.finpos.merchant.utils


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/09/17 - 1:51 PM
*/

object AppConstant {
    const val APP_NAME: String = "finPOS"
    const val SERVER_PING_INTERVAL_MILLIS: Long = 1000 * 60 * 5 //5 minutes
    const val SERVER_ACTIVITY_LOG_PUSH_INTERVAL_MILLIS: Long = 1000 * 60 * 3 //3 minutes
    const val MERCHANT_TRANSACTION_LOG_PUSH_INTERVAL_MILLIS: Long = 1000 * 60 * 3 //3 minutes
    const val QR_TRANSACTION_WAITING_INTERVAL_MILLIS: Long = 1000 * 60 * 1 //1 minutes
    const val MOBILE_PAYMENT_TIME_OUT: Long = 1000 * 60 * 1  //1 minutes
    const val SCREEN_SAVER_INTERVAL_MILLIS: Long = 1000 * 60 * 2 //2 minutes
    const val MOCO_ID = "moco"
    const val COUNTRY_CODE_NP = "NP"
    const val PAYMENT_TIME_OUT: Long = 1000 * 60 * 2 //2 minutes
    const val APP_VERSION_CODE_1_9_0 = 21010900
    const val FONE_PAY_ID = "fone_pay"
    const val MAX_TRANSACTIONS_COUNT_FOR_BATCH_PUSH = 20


    /**
     * Payment Service App
     */
    const val INTENT_ACTION_PAYMENT_SERVICE_APP_BILLING_BROADCAST =
        "global.citytech.finpos.payment.service.billing.broadcast"
    const val EXTRA_BILLING_BROADCAST_MESSAGE = "extra_billing_broadcast_message"
    const val BILLING_TRANSACTION_TARGETED_PACKAGE = "global.citytech.finpos.payment.service"
    const val EXTRA_TRANSACTION_MESSAGE = "extra_transaction_message"
    const val ACTION_CASHIER_BUSY_MESSAGE = "action_cashier_busy_message"
    const val EXTRA_MESSAGE_CASHIER_BUSY = "extra_message_cashier_busy"

    const val EXTRA_APP_DOWNLOAD = "extra.app.download"
    const val EXTRA_APP_DOWNLOAD_PRE_ACTION_RESULT = "extra.app.download.pre.action.result"
}
