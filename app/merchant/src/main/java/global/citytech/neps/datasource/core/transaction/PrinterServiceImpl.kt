package global.citytech.neps.datasource.core.transaction

import android.content.Context
import global.citytech.finpos.core.api.DeviceServiceType
import global.citytech.finpos.core.api.common.DeviceResult
import global.citytech.finpos.core.api.io.printer.PrintMessage
import global.citytech.finpos.core.api.io.printer.Printable
import global.citytech.neps.service.AppCoreFactory
import global.citytech.posswitchintregator.card.data.Result
import global.citytech.posswitchintregator.device.printer.*

class PrinterServiceImpl(private val instance: Context?) : PrinterService {

    private var service: global.citytech.finpos.core.api.io.printer.PrinterService =
        AppCoreFactory.getCore(
            instance,
            DeviceServiceType.PRINTER
        ) as global.citytech.finpos.core.api.io.printer.PrinterService

    override fun print(printerRequest: PrinterRequest?): PrinterResponse {
        val request = preparePrinterRequest(printerRequest)
        val response = service.print(request)
        return preparePrinterResponse(response)
    }

    private fun preparePrinterResponse(response: global.citytech.finpos.core.api.io.printer.PrinterResponse): PrinterResponse {
        return PrinterResponse(prepareResult(response.result), response.message)
    }

    private fun prepareResult(result: DeviceResult): Result? {
        return Result.getByCode(result.code)
    }

    private fun preparePrinterRequest(printerRequest: PrinterRequest?): global.citytech.finpos.core.api.io.printer.PrinterRequest {
        val printableList = ArrayList<Printable>()
        for (printable in printerRequest?.printableList!!) {
            printableList.add(preparePrintable(printable))
        }
        return global.citytech.finpos.core.api.io.printer.PrinterRequest(printableList)
    }

    private fun preparePrintable(printable: global.citytech.posswitchintregator.device.printer.Printable?): Printable {
        if (printable is global.citytech.posswitchintregator.device.printer.PrintMessage)
            return preparePrintMessage(printable)
        if (printable is BitmapReceipt)
            return prepareBitmapReceipt(printable)
        if (printable is FeedPaper)
            return prepareFeedPaper(printable)
        throw IllegalArgumentException("No such printable instance")
    }

    private fun prepareFeedPaper(printable: FeedPaper): global.citytech.finpos.core.api.io.printer.FeedPaper {
        return global.citytech.finpos.core.api.io.printer.FeedPaper(printable.data as Int)
    }

    private fun prepareBitmapReceipt(printable: BitmapReceipt): global.citytech.finpos.core.api.io.printer.BitmapReceipt {
        return global.citytech.finpos.core.api.io.printer.BitmapReceipt(printable.data)
    }

    private fun preparePrintMessage(printMessage: global.citytech.posswitchintregator.device.printer.PrintMessage): PrintMessage {
        return when (printMessage.instanceType) {
            global.citytech.posswitchintregator.device.printer.PrintMessage.InstanceType.SINGLE -> singleColumnInstance(
                printMessage
            )
            global.citytech.posswitchintregator.device.printer.PrintMessage.InstanceType.DOUBLE -> doubleColumnInstance(
                printMessage
            )
            global.citytech.posswitchintregator.device.printer.PrintMessage.InstanceType.MULTI -> multiColumnInstance(
                printMessage
            )
            global.citytech.posswitchintregator.device.printer.PrintMessage.InstanceType.QR -> qrCodeInstance(
                printMessage
            )
            else -> throw IllegalArgumentException("Print Message Instance Mismatch")
        }
    }

    private fun qrCodeInstance(printMessage: global.citytech.posswitchintregator.device.printer.PrintMessage): PrintMessage {
        return PrintMessage.getQRCodeInstance(
            printMessage.value,
            printMessage.style.qrWidth,
            prepareAlignment(printMessage.style.alignment)
        )
    }

    private fun multiColumnInstance(printMessage: global.citytech.posswitchintregator.device.printer.PrintMessage): PrintMessage {
        return PrintMessage.getMultiColumnInstance(
            printMessage.textList,
            printMessage.columnWidths,
            prepareStyle(printMessage.style)
        )
    }

    private fun doubleColumnInstance(printMessage: global.citytech.posswitchintregator.device.printer.PrintMessage): PrintMessage {
        return PrintMessage.getDoubleColumnInstance(
            printMessage.label,
            printMessage.value,
            prepareStyle(printMessage.style)
        )
    }

    private fun singleColumnInstance(printMessage: global.citytech.posswitchintregator.device.printer.PrintMessage): PrintMessage {
        return PrintMessage.getInstance(printMessage.value, prepareStyle(printMessage.style))
    }

    private fun prepareStyle(style: Style?): global.citytech.finpos.core.api.io.printer.Style {
        return global.citytech.finpos.core.api.io.printer.Style.Builder()
            .bold(style?.isBold!!)
            .alignment(prepareAlignment(style.alignment))
            .allCaps(style.isAllCaps)
            .italic(style.isItalic)
            .multipleAlignment(style.isMultipleAlignment)
            .fontSize(prepareFontSize(style.fontSize))
            .underline(style.isUnderline)
            .build()
    }

    private fun prepareFontSize(fontSize: Style.FontSize?): global.citytech.finpos.core.api.io.printer.Style.FontSize {
        when (fontSize) {
            Style.FontSize.NORMAL -> return global.citytech.finpos.core.api.io.printer.Style.FontSize.NORMAL;
            Style.FontSize.XSMALL -> return global.citytech.finpos.core.api.io.printer.Style.FontSize.XSMALL;
            Style.FontSize.SMALL -> return global.citytech.finpos.core.api.io.printer.Style.FontSize.SMALL;
            Style.FontSize.LARGE -> return global.citytech.finpos.core.api.io.printer.Style.FontSize.LARGE;
            Style.FontSize.XLARGE -> return global.citytech.finpos.core.api.io.printer.Style.FontSize.XLARGE;
            else -> throw IllegalArgumentException("No such Font Size")
        }
    }

    private fun prepareAlignment(alignment: Style.Align?): global.citytech.finpos.core.api.io.printer.Style.Align {
        when (alignment) {
            Style.Align.CENTER -> return global.citytech.finpos.core.api.io.printer.Style.Align.CENTER
            Style.Align.LEFT -> return global.citytech.finpos.core.api.io.printer.Style.Align.LEFT
            Style.Align.RIGHT -> return global.citytech.finpos.core.api.io.printer.Style.Align.RIGHT
            else -> throw java.lang.IllegalArgumentException("No such Alignment")
        }
    }

    override fun feedPaper(numberOfEmptyLines: Int) {
        service.feedPaper(numberOfEmptyLines)
    }
}