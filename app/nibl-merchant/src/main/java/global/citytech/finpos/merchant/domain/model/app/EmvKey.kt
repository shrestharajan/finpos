package global.citytech.finpos.merchant.domain.model.app

import android.content.ContentValues
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = EmvKey.TABLE_NAME)
data class EmvKey(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_ID)
    var id: String,
    @ColumnInfo(name = COLUMN_RID)
    var rid: String? = null,
    @ColumnInfo(name = COLUMN_INDEX)
    var index: String? = null,
    @ColumnInfo(name = COLUMN_LENGTH)
    var length: String? = null,
    @ColumnInfo(name = COLUMN_EXPONENT)
    var exponent: String? = null,
    @ColumnInfo(name = COLUMN_MODULES)
    var modules: String? = null,
    @ColumnInfo(name = COLUMN_HASH_ID)
    var hashId: String? = null,
    @ColumnInfo(name = COLUMN_KEY_SIGNATURE_ID)
    var keySignatureId: String? = null,
    @ColumnInfo(name = COLUMN_CHECKSUM)
    var checkSum: String? = null,
    @ColumnInfo(name = COLUMN_EXPIRY_DATE)
    var expiryDate: String? = null
) {
    companion object {
        const val TABLE_NAME = "emv_keys"
        const val COLUMN_ID = "id"
        const val COLUMN_RID = "rid"
        const val COLUMN_INDEX = "index"
        const val COLUMN_LENGTH = "length"
        const val COLUMN_EXPONENT = "exponent"
        const val COLUMN_MODULES = "modules"
        const val COLUMN_HASH_ID = "hash_id"
        const val COLUMN_KEY_SIGNATURE_ID = "key_signature_id"
        const val COLUMN_CHECKSUM = "checksum"
        const val COLUMN_EXPIRY_DATE = "expiry_date"


        fun fromContentValues(values: ContentValues): EmvKey {
            values.let {
                val emvKey =
                    EmvKey(
                        id = values.getAsString(COLUMN_ID),
                        rid = values.getAsString(
                            COLUMN_RID
                        )
                    )
                if (it.containsKey(COLUMN_INDEX))
                    emvKey.index = it.getAsString(COLUMN_INDEX)
                if (it.containsKey(COLUMN_LENGTH))
                    emvKey.length = it.getAsString(COLUMN_LENGTH)
                if (it.containsKey(COLUMN_EXPONENT))
                    emvKey.exponent = it.getAsString(COLUMN_EXPONENT)
                if (it.containsKey(COLUMN_MODULES))
                    emvKey.modules = it.getAsString(COLUMN_MODULES)
                if (it.containsKey(COLUMN_HASH_ID))
                    emvKey.hashId = it.getAsString(COLUMN_HASH_ID)
                if (it.containsKey(COLUMN_KEY_SIGNATURE_ID))
                    emvKey.keySignatureId = it.getAsString(COLUMN_KEY_SIGNATURE_ID)
                if (it.containsKey(COLUMN_CHECKSUM))
                    emvKey.checkSum = it.getAsString(COLUMN_CHECKSUM)
                if (it.containsKey(COLUMN_EXPIRY_DATE))
                    emvKey.expiryDate = it.getAsString(COLUMN_EXPIRY_DATE)
                return emvKey
            }
        }
    }
}