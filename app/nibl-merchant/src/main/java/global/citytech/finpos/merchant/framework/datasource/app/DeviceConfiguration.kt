package global.citytech.finpos.merchant.framework.datasource.app

import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.nibl.merchant.presentation.model.response.Host
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.usecases.TransactionType

/**
 * Created by Unique Shakya on 9/9/2020.
 */
class DeviceConfiguration {
    companion object {
        private var instance: DeviceConfiguration? = null
        fun get(): DeviceConfiguration {
            if (instance == null || 
                instance!!.terminalId.isNullOrEmpty() ||
                instance!!.merchantId.isNullOrEmpty() ||
                instance!!.purchaseCardTypeMap.isNullOrEmpty()) {
                val json = PreferenceManager.getDeviceConfiguration()
                return Jsons.fromJsonToObj(json,DeviceConfiguration::class.java)
            }
            return instance!!
        }
        fun set(deviceConfiguration: DeviceConfiguration){
            instance = deviceConfiguration
            PreferenceManager.saveDeviceConfiguration(Jsons.toJsonObj(deviceConfiguration))
        }
    }
    var purchaseCardTypeMap: Map<TransactionType, List<CardType>>? = null
    var terminalId: String? = null
    var merchantId: String? = null
    var merchantName: String? = null
    var merchantLogo: String? = null
    var retailerName: String? = null
    var retailerAddress: String? = null
    var retailerPhoneNumber: String? = null
    var dashboardDisplayLogo: String? = null
    var listOfHosts: List<Host>? = null // TODO may need to retrieve tls certificate if used
    var debugMode: Boolean = false
    var currencyCode:String? = null
}