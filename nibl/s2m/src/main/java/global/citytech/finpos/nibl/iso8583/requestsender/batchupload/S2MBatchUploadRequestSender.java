package global.citytech.finpos.nibl.iso8583.requestsender.batchupload;

import global.citytech.finpos.nibl.iso8583.NIBLMessagePayloadBuilder;
import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.PosEntryModeConfig;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.utility.HelperUtils;

/**
 * Created by Unique Shakya on 5/11/2021.
 */
public class S2MBatchUploadRequestSender extends NIBLMessageSenderTemplate {

    private final S2MBatchUploadRequest request;
    private final Logger logger = Logger.getLogger(S2MBatchUploadRequestSender.class.getName());

    public S2MBatchUploadRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
        request = (S2MBatchUploadRequest) context.getRequest();
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti = new MTI(MTI.Version.Version_1987, MTI.MessageClass.File_Actions,
                MTI.MessageFunction.Advice, MTI.Originator.Acquirer);
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);
        msg.addField(new DataElement(DataElement.PRIMARY_ACCOUNT_NUMBER, this.request.getPan()));
        msg.addField(new DataElement(DataElement.AMOUNT, HelperUtils.toISOAmount(this.request.getTransactionAmount(),12)));
        msg.addField(new DataElement(DataElement.LOCAL_TIME, this.request.getLocalTime()));
        msg.addField(new DataElement(DataElement.LOCAL_DATE, this.request.getLocalDate()));
        msg.addField(new DataElement(DataElement.PAN_EXPIRE_DATE, this.request.getExpirationDate()));
        msg.addField(new DataElement(DataElement.POS_ENTRY_MODE, PosEntryModeConfig.getValue(this.request.getPosEntryMode())));
        msg.addField(new DataElement(DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE, this.request.getPosConditionCode()));
        msg.addField(new DataElement(DataElement.RETRIEVAL_REFERENCE_NUMBER, this.request.getOriginalRrn()));
        msg.addField(new DataElement(DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE, this.request.getOriginalApprovalCode()));
        msg.addField(new DataElement(DataElement.RESPONSE_CODE, this.request.getOriginalResponseCode()));
        msg.addField(new DataElement(DataElement.PRIVATE_USE_FIELD_60, this.request.getOriginalData()));
        msg.addField(new DataElement(DataElement.PRIVATE_USE_FIELD_62, this.request.getInvoiceNumber()));
        MessagePayload.Builder builder = new NIBLMessagePayloadBuilder(this.specInfo, msg);
        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());
        return builder.build();
    }

    @Override
    protected String getProcessingCode() {
        return this.request.getOriginalProcessingCode();
    }

    @Override
    protected String getFunctionCode() {
        return this.request.getFunctionCode();
    }
}
