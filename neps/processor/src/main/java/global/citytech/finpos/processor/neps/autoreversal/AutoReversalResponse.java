package global.citytech.finpos.processor.neps.autoreversal;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.autoreversal.AutoReversalResponseParameter;

/**
 * Created by Saurav Ghimire on 7/12/21.
 * sauravnghimire@gmail.com
 */


public class AutoReversalResponse implements UseCase.Response, AutoReversalResponseParameter {

    private Result result;
    private String message;

    public AutoReversalResponse(Result result, String message) {
        this.result = result;
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }

}
