// IPosServiceInterface.aidl
package global.citytech.common;

// Declare any non-default types here with import statements

interface IPosServiceInterface {
    String getDecryptedData(String publicKey, String encryptedData);

    String getEncryptedData(String privateKey, String plainData);
}
