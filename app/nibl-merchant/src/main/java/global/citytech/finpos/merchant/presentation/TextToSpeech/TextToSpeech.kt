package global.citytech.finpos.merchant.presentation.TextToSpeech

import androidx.appcompat.app.AppCompatActivity
import android.speech.tts.TextToSpeech
import android.speech.tts.TextToSpeech.OnInitListener
import android.util.Log
import java.util.Locale

class TextToSpeech(val textMessage:String, val context: android.content.Context) : AppCompatActivity(), OnInitListener {

    private var textToSpeech: TextToSpeech = TextToSpeech(context,this)

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {

//            val result = textToSpeech.setLanguage(Locale("ne", "NP"))
            val result = textToSpeech.setLanguage(Locale.getDefault())

            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {

            } else {

                val nepaliText = textMessage
                textToSpeech.speak(nepaliText, TextToSpeech.QUEUE_FLUSH, null, "utteranceId")
            }
        }
    }
}