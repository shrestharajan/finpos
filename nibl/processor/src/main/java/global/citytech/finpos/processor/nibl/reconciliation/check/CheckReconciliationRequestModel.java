package global.citytech.finpos.processor.nibl.reconciliation.check;

import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reconciliation.check.CheckReconciliationRequestParameter;

/**
 * Created by Unique Shakya on 8/11/2021.
 */
public class CheckReconciliationRequestModel implements CheckReconciliationRequestParameter, UseCase.Request {

    private ReconciliationRepository reconciliationRepository;
    private ApplicationRepository applicationRepository;

    public CheckReconciliationRequestModel(ReconciliationRepository reconciliationRepository, ApplicationRepository applicationRepository) {
        this.reconciliationRepository = reconciliationRepository;
        this.applicationRepository = applicationRepository;
    }

    public ReconciliationRepository getReconciliationRepository() {
        return reconciliationRepository;
    }

    public ApplicationRepository getApplicationRepository() {
        return applicationRepository;
    }
}
