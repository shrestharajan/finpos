package global.citytech.finpos.merchant.presentation.mocoqr

import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseViewModel
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.presentation.model.setting.TerminalSettingResponse

class MocoQrViewModel : BaseViewModel() {

    val terminalSettings by lazy { MutableLiveData<TerminalSettingResponse>() }
    val isDisclaimerShown by lazy { MutableLiveData<Boolean>(false) }

    private var localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    fun getTerminalSettings() {
        terminalSettings.value = localDataUseCase.getTerminalSetting()
    }

    fun isDisclaimerShown() {
        isDisclaimerShown.value = localDataUseCase.isDisclaimerShown()
    }
}