package global.citytech.finpos.merchant.data.repository.core.receipt

import global.citytech.finpos.merchant.data.datasource.core.receipt.SummaryReportDataSource
import global.citytech.finpos.merchant.domain.model.receipt.summaryreport.SummaryReportResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.receipt.SummaryReportRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.processor.nibl.receipt.summaryreport.SummaryReportResponseModel
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 1/5/2021.
 */
class SummaryReportRepositoryImpl(private val summaryReportDataSource: SummaryReportDataSource) :
    SummaryReportRepository {
    override fun print(configurationItem: ConfigurationItem): Observable<SummaryReportResponseEntity> {
        return summaryReportDataSource.print(configurationItem)
    }
}