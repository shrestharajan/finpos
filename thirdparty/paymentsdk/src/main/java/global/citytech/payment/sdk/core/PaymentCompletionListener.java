package global.citytech.payment.sdk.core;


import android.os.Parcelable;

import global.citytech.payment.sdk.api.PaymentResponse;

/**
 * Created by BikashShrestha on 6/5/19.
 */
public interface PaymentCompletionListener extends Parcelable {
    void onPaymentResponseReceived(PaymentResponse paymentResponse);
}
