package global.citytech.finpos.merchant.presentation.amount.oldlayout

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseViewModel
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.presentation.utils.StringUtils
import global.citytech.finpos.merchant.service.NetworkConnectionReceiver
import global.citytech.finpos.merchant.utils.AppUtility.printLog

import global.citytech.finposframework.utility.AmountUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Rishav Chudal on 8/24/20.
 */
class AmountViewModel : BaseViewModel() {

    private var TAG = AmountViewModel::class.java.name
    private var enteredAmount: Long = 0
    var integralAmount = MutableLiveData<String>()
    var fractionalAmount = MutableLiveData<String>()
    val validAmount by lazy { MutableLiveData<Boolean>() }
    val maxAmountMessage by lazy { MutableLiveData<String>() }

    var amountLengthLimit: Int = 0

    var localDataUseCase: LocalDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    fun onNumericButtonPressed(buttonText: String, limit: Int) {
        if (enteredAmount.toString().length < limit) {
            enteredAmount = (enteredAmount.toString().plus(buttonText)).toLong()
            calculateAmount()
        }
    }

    fun calculateAmount() {
        val enteredMoneyInString = StringUtils.decimalFormatterFor(enteredAmount)
        val amountBeforeDecimal = enteredMoneyInString.substring(0, enteredMoneyInString.length - 2)
        val amountAfterDecimal = enteredMoneyInString.substring(enteredMoneyInString.length - 2)
        integralAmount.value = amountBeforeDecimal
        fractionalAmount.value = amountAfterDecimal
    }

    fun onClearButtonClicked() {
        if (enteredAmount > 0) {
            enteredAmount /= 10
            calculateAmount()
        }
    }

    fun onClearButtonLongClicked() {
        enteredAmount = 0
        calculateAmount()
    }

    fun checkMaximumAmountAllowed(amount: Double, amountLimit: Double) {
        maxAmountMessage.value =
            "Max Allowed Amount: ".plus(AmountUtils.formatAmountTwoDecimal(amountLimit))
        if (amountLimit > 0) {
            validAmount.value = amount <= amountLimit
        } else {
            checkMaximumAmountAllowedByTms(amount)
        }
    }

    private fun checkMaximumAmountAllowedByTms(amount: Double) {
        compositeDisposable.add(
            localDataUseCase.getAmountPerTransaction()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    maxAmountMessage.value =
                        "Max Allowed Amount: ".plus(AmountUtils.toHigherCurrency(it.toDouble()))
                    if (it == 0.toLong())
                        validAmount.value = true
                    else
                        validAmount.value = AmountUtils.toLongLowerCurrency(amount) <= it
                }, {
                    printLog(TAG, "Error in Maximum amount allowed by TMS :: " + it.message)
                    validAmount.value = true
                })
        )
    }

    fun getConfiguration(): Boolean {
        this.isLoading.value = true
        return compositeDisposable.add(
            localDataUseCase.getWholeConfiguration()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe({
                    NetworkConnectionReceiver.networkConfiguration = it

                    this.isLoading.value = false

                },
                    {
                        printLog(TAG, "Get Configuration error :: " + it.message)
                        this.isLoading.value = false
                        message.value = it.message
                    })
        )
    }

    fun retrieveAmountLengthLimit() {
        amountLengthLimit = localDataUseCase.getAmountLengthLimit()
    }
}