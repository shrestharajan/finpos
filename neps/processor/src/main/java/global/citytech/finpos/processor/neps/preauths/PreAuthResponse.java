package global.citytech.finpos.processor.neps.preauths;

import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finposframework.hardware.common.Result;

/**
 * Created by Saurav Ghimire on 2/3/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class PreAuthResponse extends TransactionResponse {


    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private boolean approved;
        private boolean shouldPrintCustomerCopy;
        private String message;
        private String stan;
        private Result result;
        private String approvalCode;

        private Builder() {
        }

        public static PreAuthResponse.Builder createDefaultBuilder() {
            return new PreAuthResponse.Builder();
        }

        public PreAuthResponse.Builder debugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public PreAuthResponse.Builder debugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public PreAuthResponse.Builder approved(boolean approved) {
            this.approved = approved;
            return this;
        }
        public PreAuthResponse.Builder shouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public PreAuthResponse.Builder message(String message) {
            this.message = message;
            return this;
        }

        public PreAuthResponse.Builder stan(String stan) {
            this.stan = stan;
            return this;
        }

        public PreAuthResponse.Builder result(Result result) {
            this.result = result;
            return this;
        }

        public PreAuthResponse.Builder approvalCode(String approvalCode) {
            this.approvalCode = approvalCode;
            return this;
        }

        public PreAuthResponse build() {
            PreAuthResponse preAuthResponse = new PreAuthResponse();
            preAuthResponse.debugRequestMessage = this.debugRequestMessage;
            preAuthResponse.debugResponseMessage = this.debugResponseMessage;
            preAuthResponse.approved = this.approved;
            preAuthResponse.message = this.message;
            preAuthResponse.result = this.result;
            preAuthResponse.approvalCode = this.approvalCode;
            preAuthResponse.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            preAuthResponse.stan = this.stan;
            return preAuthResponse;
        }
    }
}
