package global.citytech.finpos.device.weipass.emv.revokedkeys

import com.wiseasy.emvprocess.KernelInit
import com.wiseasy.emvprocess.bean.CAPKRevoke
import global.citytech.finpos.device.weipass.utils.WeipassConstant.EMPTY_REVOKED_KEYS_LIST
import global.citytech.finpos.device.weipass.utils.WeipassConstant.FAILURE_REVOKED_KEYS_ERASE
import global.citytech.finpos.device.weipass.utils.WeipassConstant.FAILURE_REVOKED_KEYS_INJECTION
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SUCCESS_REVOKED_KEYS_ERASE
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SUCCESS_REVOKED_KEYS_INJECTION
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finpos.device.weipass.utils.defaultOrEmptyValue
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result

/**
 * Created by Rishav Chudal on 4/30/20.
 */
class WeipassRevokedKeysServiceImpl:
    global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKeysService {

    private lateinit var deviceResponse: DeviceResponse

    /*
     * Revoked Keys Injection
     */
    override fun injectRevokedKeys(injectionRequest: global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKeysInjectionRequest): DeviceResponse {
        deviceResponse = DeviceResponse(Result.FAILURE, FAILURE_REVOKED_KEYS_INJECTION)
        when {
            injectionRequest.revokedKeysList.isEmpty() -> {
                deviceResponse = DeviceResponse(Result.FAILURE, EMPTY_REVOKED_KEYS_LIST)
            }
            else -> {
                eraseAllRevokedKeys()
                injectRevokedKeysInCore(injectionRequest.revokedKeysList)
                deviceResponse = DeviceResponse(Result.SUCCESS, SUCCESS_REVOKED_KEYS_INJECTION)
                WeipassLogger.log("Revoked Keys Injection Completed ...")
            }
        }
        return deviceResponse
    }

    private fun injectRevokedKeysInCore(revokedKeysList: List<global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKey>): Int {
        WeipassLogger.log("=============== Injecting Revoked Keys ===============")
        var finalResult = 0
        for (revokedKey in revokedKeysList) {
            try {
                WeipassLogger.log("========== Revoked Key ::: ".plus(revokedKey.rid)
                    .plus(" =========="))
                val coreRevokedKey = prepareCoreCapkRevokeData(revokedKey)
                val result = KernelInit.addRemovedCAPK(coreRevokedKey)
                WeipassLogger.log("Revoked Key ::: ".plus(revokedKey.rid)
                    .plus(", injection result ::: ".plus(result)))
                finalResult = finalResult + result
            } catch (exception: Exception) {
                WeipassLogger.log("Exception in injecting revoked key ::: ".plus(exception.message))
            }
        }
        WeipassLogger.log("Revoked Keys injection final result ::: ".plus(finalResult))
        return finalResult
    }

    private fun prepareCoreCapkRevokeData(revokedKey: global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKey): CAPKRevoke {
        val capkRevoke = CAPKRevoke()

        val ridData = revokedKey.rid.defaultOrEmptyValue()
        capkRevoke.rid = ridData
        WeipassLogger.debug("Revoked Key Rid ::: ".plus(ridData))

        val indexData = revokedKey.index.defaultOrEmptyValue()
        capkRevoke.index = indexData
        WeipassLogger.debug("Reovked Key Index ::: ".plus(indexData))

        val csnData = revokedKey.certificateSerialNumber.defaultOrEmptyValue()
        capkRevoke.certificateSN = csnData
        WeipassLogger.debug("Certificate Serial Number ::: ".plus(csnData))

        return capkRevoke
    }

    /*
     * Erase All Revoked Keys
     */
    override fun eraseAllRevokedKeys(): DeviceResponse {
        WeipassLogger.log("Erasing revoked keys in core ...")
        deviceResponse = DeviceResponse(Result.FAILURE, FAILURE_REVOKED_KEYS_ERASE)
        val result = KernelInit.deleteCAPKRemoved()
        WeipassLogger.log("Revoked Keys Erase Result ::: ".plus(result))
        when (result) {
            0 -> {
                deviceResponse = DeviceResponse(Result.SUCCESS, SUCCESS_REVOKED_KEYS_ERASE)
            }
            else -> {
                WeipassLogger.log("Couldn't clear revoked keys")
            }
        }
        return deviceResponse
    }
}