package global.citytech.finpos.merchant.data.datasource.app.alert

import global.citytech.finpos.merchant.domain.model.app.Action
import global.citytech.finpos.merchant.domain.model.app.AppNotification

/**
 * Created by Unique Shakya on 5/28/2021.
 */
interface AppNotificationDataSource {

    fun noNotifications(): Boolean

    fun getNotificationCount(): Long

    fun getAllNotifications(): List<AppNotification>

    fun removeNotification(appNotification: AppNotification)

    fun removeNotificationByAction(action: Action)

    fun addNotification(appNotification: AppNotification)

    fun removeNotificationByReferenceId(referenceId: String)

    fun clearSharedPreference()
}