package global.citytech.neps.presentation.idle

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.wajahatkarim3.roomexplorer.RoomExplorer
import global.citytech.common.extensions.loadBase64String
import global.citytech.common.extensions.openActivity
import global.citytech.easydroid.core.extension.showSuccessMessage
import global.citytech.easydroid.core.extension.showToast
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.easydroid.core.utils.MessageConfig
import global.citytech.neps.BR
import global.citytech.neps.R
import global.citytech.neps.data.db.AppDatabase
import global.citytech.neps.databinding.ActivityIdleBinding
import global.citytech.neps.datasource.pref.PreferenceManager
import global.citytech.neps.presentation.dashboard.DashboardActivity
import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.neps.presentation.model.LogoItem
import global.citytech.neps.presentation.model.MerchantItem
import kotlinx.android.synthetic.main.activity_idle.*
import kotlin.system.exitProcess

class IdleActivity : AppBaseActivity<ActivityIdleBinding, IdleViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObserver()
        initViews()
        getViewModel().getConfiguration()
    }

    private fun initViews() {
        iv_menu.setOnClickListener {
            openActivity(DashboardActivity::class.java)
            finish()
        }
        tv_ready.setOnClickListener {
            RoomExplorer.show(this, AppDatabase::class.java, "neps_db.db")
        }
    }

    private fun initObserver() {
        getViewModel().configuration.observe(this, Observer { configurationItem ->
            handleConfig(configurationItem)
        })
    }

    private fun handleConfig(configurationItem: ConfigurationItem?) {
        if (configurationItem == null || configurationItem.merchants.isNullOrEmpty()) {
            PreferenceManager.setIsConfigured(false)
            showNotConfiguredDialog()
        } else {
            PreferenceManager.setIsConfigured(true)
            val merchant: MerchantItem = configurationItem.merchants!![0]
            val logo : LogoItem = configurationItem.logos!![0]
//            iv_wallpaper.loadBase64String(logo.appWallpaper)
            tv_merchant_id.text = "TERMINAL ID : ${merchant.terminalId}"
            tv_ready.visibility = View.VISIBLE
            iv_menu.visibility = View.VISIBLE
        }
    }

    private fun showNotConfiguredDialog() {
        showSuccessMessage(MessageConfig.Builder()
            .message(getString(R.string.msg_not_configured))
            .positiveLabel("OK")
            .onPositiveClick { dialog, _ ->
                finish()
                dialog.dismiss()
            })
    }

    override fun onBackPressed() {
        showSuccessMessageAndExit()
    }

    private fun showSuccessMessageAndExit(){
        val messageConfig = MessageConfig.Builder()
            .message(getString(R.string.msg_confirm_exit))
            .positiveLabel(getString(R.string.action_yes))
            .negativeLabel(getString(R.string.action_no))
            .onPositiveClick {
                    dialogInterface, _ -> dialogInterface.dismiss()
                closeApplication()
            }
            .onNegativeClick {
                    dialogInterface, _ -> dialogInterface.dismiss()
            }
        showSuccessMessage(messageConfig)
    }

    private fun closeApplication() {
        finishAndRemoveTask()
        exitProcess(0)
    }

    override fun getBindingVariable(): Int = BR.viewModel
    override fun getLayout() = R.layout.activity_idle
    override fun getViewModel(): IdleViewModel =
        ViewModelProviders.of(this)[IdleViewModel::class.java]
}
