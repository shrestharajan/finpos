package global.citytech.finpos.merchant.presentation.model.printparameter

/**
 * Created by Rishav Chudal on 11/6/20.
 */
data class PrintParameterResponse(
    val success: Boolean,
    val message: String
)