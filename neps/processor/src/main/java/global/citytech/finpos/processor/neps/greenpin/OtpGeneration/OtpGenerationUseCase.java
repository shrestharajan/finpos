package global.citytech.finpos.processor.neps.greenpin.OtpGeneration;

import java.math.BigDecimal;
import java.util.List;

import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.PosConditionCode;
import global.citytech.finpos.neps.iso8583.SmartVistaIccDataList;
import global.citytech.finpos.neps.iso8583.requestsender.greenpin.OtpGenerationIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.greenpin.OtpGenerationRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.processor.neps.NepsConstant;
import global.citytech.finpos.processor.neps.cardprocessor.ICCCardProcessor;
import global.citytech.finpos.processor.neps.cardprocessor.PICCCardProcessor;
import global.citytech.finpos.processor.neps.transaction.NepsInvalidResponseHandler;
import global.citytech.finpos.processor.neps.transaction.NepsIsoMessageReceiptHandler;
import global.citytech.finpos.processor.neps.transaction.NepsTransactionReceiptPrintHandler;
import global.citytech.finpos.processor.neps.transaction.ReasonForReversal;
import global.citytech.finpos.processor.neps.transaction.ReversedTransactionReceiptHandler;
import global.citytech.finpos.processor.neps.transaction.TransactionExecutorTemplate;
import global.citytech.finpos.processor.neps.transaction.TransactionReceiptHandler;
import global.citytech.finpos.processor.neps.transaction.TransactionRequest;
import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finpos.processor.neps.transaction.TransactionUtils;
import global.citytech.finpos.processor.neps.voidsale.VoidSaleResponse;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedAction;
import global.citytech.finposframework.hardware.io.led.LedLight;
import global.citytech.finposframework.hardware.io.led.LedRequest;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.TransactionStatusFromHost;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandler;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;

public class OtpGenerationUseCase extends TransactionExecutorTemplate {

    protected final ReadCardRequest.MultipleAidSelectionListener multipleAidSelectionListener = aids -> {
        notifier.notify(Notifier.EventType.DETECTED_MULTIPLE_AID, "Processing..");
        return -1;
    };


    public OtpGenerationUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    public TransactionResponse execute(TransactionRequest request) {
        setUpRequiredTransactionFields(request);
        checkIfDeviceReady();
        this.initTransaction(this.request.getApplicationRepository(), transactionRequest);
        this.retrieveCardResponse();
        this.notifier.notify(
                Notifier.EventType.PROCESSING,
                "Transmitting Request ..."
        );
        try {
            IsoMessageResponse isoMessageResponse = sendIsoMessage();
            printDebugReceiptIfDebugMode(request.getPrinterService(), isoMessageResponse);
            return checkForInvalidResponseAndProceed(isoMessageResponse);
        }
        catch (FinPosException e) {
            e.printStackTrace();
            if (e.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT){
                throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
            } else if(
                    e.getType() == FinPosException.ExceptionType.READ_TIME_OUT) {
                throw new PosException(PosError.DEVICE_ERROR_CARD_READ_TIMEOUT);
            }else if(
                    e.getType() == FinPosException.ExceptionType.CONNECTION_ERROR){
                throw new PosException(PosError.DEVICE_ERROR_SERVER_CONNECTION_ERROR);
            }

            else throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
    }


    protected TransactionResponse checkForInvalidResponseAndProceed(IsoMessageResponse isoMessageResponse) {
        InvalidResponseHandlerResponse invalidResponseHandlerResponse = this.checkForInvalidResponseFromHost(isoMessageResponse);
        if (invalidResponseHandlerResponse.isInvalid()) {
            return this.handleInvalidResponseFromHost(invalidResponseHandlerResponse);
        } else {
            return proceedValidResponseFromHost(isoMessageResponse);
        }
    }

    protected InvalidResponseHandlerResponse checkForInvalidResponseFromHost(IsoMessageResponse isoMessageResponse) {
        InvalidResponseHandler invalidResponseHandler = new NepsInvalidResponseHandler();
        return invalidResponseHandler.isInvalidResponse(isoMessageResponse);
    }


    protected TransactionResponse handleInvalidResponseFromHost(InvalidResponseHandlerResponse invalidResponseHandlerResponse) {
        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, invalidResponseHandlerResponse.getMessage());
        return prepareErrorResponse(invalidResponseHandlerResponse.getMessage(), false);
    }

    private TransactionResponse proceedValidResponseFromHost(IsoMessageResponse isoMessageResponse) {
        boolean transactionApproved = this.transactionApprovedByActionCode(isoMessageResponse);
        updateTransactionIds(false);
        return this.prepareResponse(isoMessageResponse, transactionApproved,
                request.getApplicationRepository(), false);
    }

    @Override
    protected void setUpRequiredTransactionFields(global.citytech.finpos.processor.neps.transaction.TransactionRequest request) {
        this.request = request;
        stan = this.terminalRepository.getSystemTraceAuditNumber();
        transactionRequest = request.getTransactionRequest();
        applicationRepository = request.getApplicationRepository();
    }

    @Override
    protected void retrieveCardResponse() {
        List<CardType> allowedCardTypes = TransactionUtils
                .getAllowedCardEntryModes(transactionRequest.getTransactionType());
        ReadCardRequest readCardRequest = this.prepareReadCardRequest(
                allowedCardTypes,
                request.getApplicationRepository(),
                transactionRequest
        );
        this.notifier.notify(Notifier.EventType.STARTING_READ_CARD);
        performTransactionWithCard(readCardRequest);

    }

    protected ReadCardRequest prepareReadCardRequest(
            List<CardType> allowedCardTypes,
            ApplicationRepository applicationRepository,
            global.citytech.finposframework.usecases.transaction.data.TransactionRequest
                    transactionRequest
    ) {
        ReadCardRequest readCardRequest = new ReadCardRequest(
                allowedCardTypes,
                transactionRequest.getTransactionType(),
                TransactionUtils.retrieveTransactionType9C(transactionRequest.getTransactionType()),
                applicationRepository.retrieveEmvParameterRequest()
        );
        readCardRequest.setStan(stan);
        readCardRequest.setPinpadRequired(false);
        readCardRequest.setAmount(transactionRequest.getAmount());
        readCardRequest.setCashBackAmount(transactionRequest.getAdditionalAmount());
        readCardRequest.setTransactionDate(DateUtils.yyMMddDate());
        readCardRequest.setTransactionTime(DateUtils.HHmmssTime());
        readCardRequest.setMultipleAidSelectionListener(this.multipleAidSelectionListener);
        readCardRequest.setIccDataList(getSmartVistaIccDataList());
        readCardRequest.setCurrencyName(NepsConstant.PIN_PAD_CURRENCY);
        readCardRequest.setPinPadFixedLayout(!this.terminalRepository.shouldShufflePinPad());
        return readCardRequest;
    }


    private void performTransactionWithCard(ReadCardRequest readCardRequest) {
        readCardResponse = this.detectCard(readCardRequest, request.getReadCardService(),
                request.getLedService());
        this.notifier.notify(Notifier.EventType.READING_CARD, "Reading Card..");

    }

    private List<IccData> getSmartVistaIccDataList(
    ) {
        List<IccData> iccData = SmartVistaIccDataList.get();
        if (iccData.contains(IccData.RC)) iccData.remove(IccData.RC);
        if (iccData.contains(IccData.TERMINAL_TYPE)) iccData.remove(IccData.TERMINAL_TYPE);
        if (iccData.contains(IccData.ADF)) iccData.remove(IccData.ADF);
        if (iccData.contains(IccData.APP_VERSION_NUMBER)) iccData.remove(IccData.APP_VERSION_NUMBER);
        return iccData;
    }

    private ReadCardResponse detectCard(ReadCardRequest readCardRequest, ReadCardService readCardService, LedService ledService) {
        ledService.doTurnLedWith(new LedRequest(LedLight.BLUE, LedAction.ON));
        ReadCardResponse readCardResponse = readCardService.readCardDetails(readCardRequest);
        this.readCardResponse = readCardResponse;
        if (readCardResponse.getResult().getCode() == 310) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        this.validateReadCardResponse(readCardResponse);
//        this.readCardResponse= readCardResponse;
        this.retrieveCardProcessor();
        this.readCardResponse= this.cardProcessor.validateReadCardResponse(readCardRequest, this.readCardResponse);
        PurchaseRequest purchaseRequest = preparePurchaseRequest(
                transactionRequest,
                readCardResponse.getCardDetails()
        );

        readCardResponse = this.cardProcessor.processCard(this.stan, purchaseRequest);
        request.getReadCardService().cleanUp();
        return readCardResponse;
    }

    private void validateReadCardResponse(ReadCardResponse readCardResponse) {
        if (readCardResponse == null)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        if (readCardResponse.getResult() == Result.TIMEOUT)
            throw new PosException(PosError.DEVICE_ERROR_CARD_READ_TIMEOUT);
        if (readCardResponse.getResult() == Result.USER_CANCELLED ||
                readCardResponse.getResult() == Result.CANCEL)
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        if (readCardResponse.getResult() == Result.PROCESSING_ERROR)
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        if (readCardResponse.getResult() == Result.FAILURE)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        if (readCardResponse.getResult() == Result.CARD_NOT_SUPPORTED)
            throw new PosException(PosError.DEVICE_ERROR_CARD_NOT_SUPPORTED);
    }

    private void retrieveCardProcessor() {
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case ICC:
                this.cardProcessor = new ICCCardProcessor(
                        this.request.getTransactionRepository(),
                        this.request.getTransactionAuthenticator(),
                        this.request.getApplicationRepository(),
                        this.request.getReadCardService(),
                        this.notifier,
                        terminalRepository
                );
                break;
            case PICC:
                this.cardProcessor = new PICCCardProcessor(
                        this.request.getTransactionRepository(),
                        this.request.getTransactionAuthenticator(),
                        this.request.getApplicationRepository(),
                        this.request.getReadCardService(),
                        this.notifier,
                        this.request.getLedService(),
                        this.request.getSoundService(),
                        terminalRepository
                );
                break;
            default:
                break;
        }
    }

    @Override
    protected void processCard() {
    }

    @Override
    protected String getClassName() {
        return null;
    }

    @Override
    protected TransactionIsoRequest prepareIsoRequest(ReadCardResponse readCardResponse, global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest) {
       return OtpGenerationIsoRequest.Builder
                .newInstance()
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .transactionAmount(BigDecimal.valueOf(0.00))
                .stan(stan)
                .localDateTime(DateUtils.yyMMddHHmmssDate())
                .expirationDate(readCardResponse.getCardDetails().getExpiryDate())
                .posEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
                .posConditionCode(PosConditionCode.ATTENDANT_TERMINAL.getValue())
                .track2Data(readCardResponse.getCardDetails().getTrackTwoData())
                .currencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .emvData(readCardResponse.getCardDetails().getIccDataBlock())
                .build();
    }

    @Override
    protected NepsMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context) {
        return new OtpGenerationRequestSender(context);
    }

    @Override
    protected void processOnlineResult(IsoMessageResponse isoMessageResponse) {

    }

    @Override
    protected void updateTransactionLog(boolean transactionApproved, global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest, TransactionRepository transactionRepository, IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse) {

    }

    @Override
    protected void updateTransactionIdsAndNotifyTransactionStatus(boolean transactionApproved, IsoMessageResponse isoMessageResponse, TransactionRequest request, ReadCardResponse readCardResponse) {

    }

    @Override
    protected TransactionResponse prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository, boolean shouldPrint) {
        return OtpGenerationResponse.Builder.newInstance()
                .withDebugRequestMessage(isoMessageResponse.getDebugRequestString())
                .withDebugResponseMessage(isoMessageResponse.getDebugResponseString())
                .withApproved(transactionApproved)
                .withMessage(
                        this.retrieveMessage(
                                isoMessageResponse,
                                transactionApproved,
                                request.getApplicationRepository()
                        )
                ).withReadCardResponse(readCardResponse).build();
    }

    @Override
    protected String getProcessingCode() {
        return null;
    }

    @Override
    protected TransactionResponse prepareErrorResponse(String responseMessage, boolean shouldPrint) {
        return OtpGenerationResponse.Builder.newInstance()
                .withStan(this.stan)
                .withMessage(responseMessage)
                .withApproved(false)
                .build();
    }

    @Override
    protected TransactionResponse promptToPerformIccTransaction() {
        return null;
    }
}
