package global.citytech.finposframework.usecases.transaction.receipt.transaction;

public class EmvTags {

    private String cardType;
    private String responseCode;
    private String aid;
    private String tvr;
    private String cvm;
    private String cid;
    private String ac;
    private String kernelId;
    private String tsi;
    private String iin;

    public String getCardType() {
        return cardType;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public String getAid() {
        return aid;
    }

    public String getTvr() {
        return tvr;
    }

    public String getCvm() {
        return cvm;
    }

    public String getCid() {
        return cid;
    }

    public String getAc() {
        return ac;
    }

    public String getKernelId() {
        return kernelId;
    }

    public String getTsi() {
        return tsi;
    }

    public String getIin() {
        return iin;
    }

    public static final class Builder {
        private String cardType;
        private String responseCode;
        private String aid;
        private String tvr;
        private String cvm;
        private String cid;
        private String ac;
        private String kernelId;
        private String tsi;
        private String iin;

        private Builder() {
        }

        public static Builder defaultBuilder() {
            return new Builder();
        }

        public Builder withCardType(String cardType) {
            this.cardType = cardType;
            return this;
        }

        public Builder withResponseCode(String responseCode) {
            this.responseCode = responseCode;
            return this;
        }

        public Builder withAid(String aid) {
            this.aid = aid;
            return this;
        }

        public Builder withTvr(String tvr) {
            this.tvr = tvr;
            return this;
        }

        public Builder withCvm(String cvm) {
            this.cvm = cvm;
            return this;
        }

        public Builder withCid(String cid) {
            this.cid = cid;
            return this;
        }

        public Builder withAc(String ac) {
            this.ac = ac;
            return this;
        }

        public Builder withKernelId(String kernelId) {
            this.kernelId = kernelId;
            return this;
        }

        public Builder withTsi(String tsi) {
            this.tsi = tsi;
            return this;
        }

        public Builder withIin(String iin) {
            this.iin = iin;
            return this;
        }

        public EmvTags build() {
            EmvTags emvTags = new EmvTags();
            emvTags.ac = this.ac;
            emvTags.cardType = this.cardType;
            emvTags.cvm = this.cvm;
            emvTags.cid = this.cid;
            emvTags.tvr = this.tvr;
            emvTags.kernelId = this.kernelId;
            emvTags.responseCode = this.responseCode;
            emvTags.aid = this.aid;
            emvTags.tsi = this.tsi;
            emvTags.iin = this.iin;
            return emvTags;
        }
    }
}
