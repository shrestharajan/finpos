package global.citytech.finpos.neps.iso8583;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.DefaultMessageParser;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.MessageParser;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.comm.Request;
import global.citytech.finposframework.comm.Response;
import global.citytech.finposframework.comm.SocketClient;
import global.citytech.finposframework.comm.TcpClient;
import global.citytech.finposframework.utility.HexDump;
import global.citytech.finposframework.utility.NumberUtils;
import global.citytech.finposframework.supports.MessagePayload;

import static org.junit.Assert.assertEquals;

/** User: Surajchhetry Date: 2/24/20 Time: 5:33 PM */
public class NEPSSwitchTestIT {

  private Logger logger = Logger.getLogger(NEPSSwitchTestIT.class.getName());

 // @Test
  public void sendMerchantLogOnRequestTest() {
    logger.log("=== sendMerchantLogOnRequestTest ===");
    NepsSpecInfo specInfo = new NepsSpecInfo();
    HostInfo primaryHost = NEPSHostConfig.getPrimaryConfig();
    HostInfo secondaryHost = NEPSHostConfig.getSecondaryConfig();
    ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);
    SocketClient socketClient = new TcpClient(connectionParam);
    HostInfo result = socketClient.dial();

    socketClient.write(this.getNetworkKeyExchangeRequest());
    Response lenResponse = socketClient.read(specInfo.getMessageHeaderLength());
    logger.log(" Response Received ");
    int len = NumberUtils.fromHexASCIIBytesToInt(lenResponse.getData());
    logger.log("Header Length : " + len);
    Response data = socketClient.read(len);
    logger.log(" ==== RESPONSE ====");
    logger.log(HexDump.dumpHexString(data.getData()));
    MessageParser messageParser = new DefaultMessageParser(specInfo);
    Iso8583Msg msg = messageParser.parse(data.getData());
    assertEquals("0810", msg.getMti().toString());
    logger.log(msg.getPrintableData());
  }

  public Request getLogOnRequest() {
    MTI mti =
        new MTI(
            MTI.Version.Version_1987,
            MTI.MessageClass.Network_Management,
            MTI.MessageFunction.Request,
            MTI.Originator.Acquirer);
    Iso8583Msg msg = new Iso8583Msg(mti);
    msg.addField(new DataElement(3, "900000"));
    msg.addField(new DataElement(7, "1111111111"));
    msg.addField(new DataElement(11, "000001"));
    msg.addField(new DataElement(24, "801"));
    msg.addField(new DataElement(41, "41234027"));
    msg.addField(new DataElement(42, "100512345678910"));

    NepsMessagePayloadBuilder builder = new NepsMessagePayloadBuilder(new NepsSpecInfo(), msg);
    MessagePayload payload = builder.build();
    return new Request(payload.getAsBytes());
  }

  public Request getNetworkRequest() {
    MTI mti =
        new MTI(
            MTI.Version.Version_1987,
            MTI.MessageClass.Network_Management,
            MTI.MessageFunction.Request,
            MTI.Originator.Acquirer);
    Iso8583Msg msg = new Iso8583Msg(mti);
    msg.addField(new DataElement(3, "990000"));
    msg.addField(new DataElement(7, "1111111111"));
    msg.addField(new DataElement(11, "000001"));
    msg.addField(new DataElement(24, "831"));
    msg.addField(new DataElement(41, "41234027"));
    msg.addField(new DataElement(42, "100512345678910"));

    NepsMessagePayloadBuilder builder = new NepsMessagePayloadBuilder(new NepsSpecInfo(), msg);
    MessagePayload payload = builder.build();
    logger.log(HexDump.dumpHexString(payload.getAsBytes()));
    return new Request(payload.getAsBytes());
  }

  public Request getNetworkKeyExchangeRequest() {
    MTI mti =
        new MTI(
            MTI.Version.Version_1987,
            MTI.MessageClass.Network_Management,
            MTI.MessageFunction.Request,
            MTI.Originator.Acquirer);
    Iso8583Msg msg = new Iso8583Msg(mti);
    msg.addField(new DataElement(3, "990000"));
    msg.addField(new DataElement(7, "1111111111"));
    msg.addField(new DataElement(11, "000001"));
    msg.addField(new DataElement(24, "811"));
    msg.addField(new DataElement(41, "41234027"));
    msg.addField(new DataElement(42, "100512345678910"));

    NepsMessagePayloadBuilder builder = new NepsMessagePayloadBuilder(new NepsSpecInfo(), msg);
    MessagePayload payload = builder.build();
    return new Request(payload.getAsBytes());
  }
}
