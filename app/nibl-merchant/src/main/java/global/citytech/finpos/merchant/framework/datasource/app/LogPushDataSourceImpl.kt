package global.citytech.finpos.merchant.framework.datasource.app

import global.citytech.common.data.Response
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.app.db.LogPushDatabaseSource
import global.citytech.finpos.merchant.presentation.dashboard.temp.LogResponse
import global.citytech.finpos.merchant.presentation.dashboard.temp.LogsSaveRequest
import global.citytech.finpos.merchant.presentation.dashboard.temp.LogFileUploadResponse
import java.text.SimpleDateFormat
import java.util.*

class LogPushDataSourceImpl: LogPushDatabaseSource {
    override fun getLogsList(serialNumber: Any, filePaths:String): LogResponse {
        val datePart = filePaths.substringBeforeLast(".txt").substringAfterLast("/")
        val modifiedUrl = "$serialNumber"+"_Cashier"+"_${datePart}"
        val apiUrl = "/device-tms-platform/v1/devices/logs/public/upload-url/$modifiedUrl"
        val jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.get(apiUrl)
        return Jsons.fromJsonToObj(jsonResponse, LogResponse::class.java)
    }

    override fun pushLogFileToServer(logFileUploadResponse: LogFileUploadResponse, filePath: String, serialNumber: Any): Response {
        val inputString = logFileUploadResponse.eTag
        val resultString = inputString?.removeSurrounding("\"")
        val currentDateTime = Date()
        val pattern = "yyyy-MM-dd"
        val simpleDateFormat = SimpleDateFormat(pattern, Locale.US)
        val formattedDate = simpleDateFormat.format(currentDateTime)
        val fileName = filePath.substringAfterLast("/").substringBeforeLast(".txt")
        val datePart = fileName.removePrefix("logcat_")
        val modifiedUrl = "$serialNumber"+"_Cashier"+"_${fileName}"

        val requestBody = resultString?.let {
            LogsSaveRequest(
                serialNumber = NiblMerchant.INSTANCE.iPlatformManager.serialNumber,
                etag = resultString,
                objectId = logFileUploadResponse.key.toString(),
                storagePath = filePath,
                uploadDate = formattedDate.toString(),
                expires = false,
                fileName = modifiedUrl,
                logDate = datePart
            )
        }

        val jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.post("/device-tms-platform/v1/devices/logs/public/save", Jsons.toJsonObj(requestBody),true)
        return Jsons.fromJsonToObj(jsonResponse, Response::class.java)
    }

}