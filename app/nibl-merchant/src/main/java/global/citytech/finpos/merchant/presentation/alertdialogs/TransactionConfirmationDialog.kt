package global.citytech.finpos.merchant.presentation.alertdialogs

import android.app.Activity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.gson.Gson
import global.citytech.common.Base64
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.framework.qr.QrHelper
import global.citytech.finpos.merchant.utils.ApiConstant
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.loadCurrency
import global.citytech.payment.sdk.utils.StringUtils
import kotlinx.android.synthetic.main.activity_dashboard.*

class TransactionConfirmationDialog constructor(
    activity: Activity, listener: Listener, transactionConfirmation: TransactionConfirmation
) {
    private lateinit var tvTitle: TextView
    private lateinit var tvMessage: TextView
    private lateinit var tvAmount: TextView
    private lateinit var imgResult: ImageView
    private lateinit var btnPositive: Button
    private lateinit var btnNegative: Button
    private lateinit var tvCurrency: AppCompatTextView
    private lateinit var imgTransactionResult: ImageView
    private lateinit var qrTitle: TextView

    private var alertDialog: AlertDialog

    private var listener: Listener
    private var transactionConfirmation: TransactionConfirmation

    init {
        this.listener = listener
        this.transactionConfirmation = transactionConfirmation
        val alertDialogBuilder = AlertDialog.Builder(activity, R.style.FullScreenDialog)
        val inflater = activity.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_transaction_confirmation, null)
        alertDialogBuilder.setView(dialogView)
        alertDialogBuilder.setCancelable(false)
        initViews(dialogView)
        setViewAttributes()
        handleClickEvents()
        initBankLogo(activity, dialogView)
        alertDialog = alertDialogBuilder.create()
        alertDialog.setCancelable(false)
    }

    private fun initBankLogo(activity: Activity, dialogView: View) {
        val imageView = dialogView.findViewById<ImageView>(R.id.iv_bank_logo);
        if (DeviceConfiguration.get().dashboardDisplayLogo != null) {
            try {
                val logo = Base64.decode(DeviceConfiguration.get().dashboardDisplayLogo)
                Glide.with(activity).load(logo).into(imageView)
            } catch (e: Exception) {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        activity,
                        R.drawable.ic_full_finpos_logo
                    )
                )
            }
        } else {
            imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    activity,
                    R.drawable.ic_full_finpos_logo
                )
            )
        }
    }

    fun show() {
        alertDialog.show()
    }

    fun hide() {
        alertDialog.dismiss()
    }

    fun update(title: String, authorizationCode: String, listener: Listener) {
        showButtons()
        tvTitle.text = title
        tvMessage.text = authorizationCode
        this.listener = listener
    }

    fun update(transactionConfirmation: TransactionConfirmation, listener: Listener) {
        showButtons()
        tvTitle.text = transactionConfirmation.title
        if (this.transactionConfirmation.amount != null) {
            tvAmount.visibility = View.VISIBLE
            tvAmount.text = this.transactionConfirmation.amount
            tvCurrency.loadCurrency()
        } else {
            tvAmount.visibility = View.GONE
        }
        tvMessage.text = transactionConfirmation.message
        if (transactionConfirmation.positiveLabel.isNullOrEmptyOrBlank())
            btnPositive.visibility = View.GONE
        btnPositive.text = transactionConfirmation.positiveLabel
        if (transactionConfirmation.negativeLabel.isNullOrEmptyOrBlank())
            btnNegative.visibility = View.GONE
        btnNegative.text = transactionConfirmation.negativeLabel

        if (!StringUtils.isEmpty(NiblMerchant.INSTANCE.iPlatformManager.hostDomain) && AppUtility.isTransactionApprovedAndDeclined(
                transactionConfirmation.transactionStatus.toString()
            )
        ) {
            Log.d("TransactionConfirmTag",Gson().toJson(transactionConfirmation))
            qrTitle.visibility = View.VISIBLE
            imgTransactionResult.setImageBitmap(
                QrHelper().generateQrBitmap(
                    AppUtility.prepareQrUrlString(
                        transactionConfirmation.qrImageString.toString()
                    )
                )
            )
        }
        this.listener = listener
    }

    private fun showButtons() {
        btnNegative.visibility = View.VISIBLE
        btnPositive.visibility = View.VISIBLE
    }

    private fun handleClickEvents() {
        this.btnPositive.setOnClickListener {
            this.hide()
            this.listener.onPositiveButtonClick()
        }

        this.btnNegative.setOnClickListener {
            this.hide()
            this.listener.onNegativeButtonClick()
        }
    }

    private fun setViewAttributes() {
        tvTitle.text = this.transactionConfirmation.title
        if (this.transactionConfirmation.amount != null) {
            tvAmount.visibility = View.VISIBLE
            tvAmount.text = this.transactionConfirmation.amount
            tvCurrency.loadCurrency()
        } else {
            tvAmount.visibility = View.GONE
        }
        showButtons()
        if (!this.transactionConfirmation.negativeLabel.isNullOrEmptyOrBlank()) {
            btnNegative.text = this.transactionConfirmation.negativeLabel
            btnNegative.visibility = View.VISIBLE
        } else {
            btnNegative.visibility = View.GONE
        }
        if (!this.transactionConfirmation.positiveLabel.isNullOrEmptyOrBlank()) {
            btnPositive.text = this.transactionConfirmation.positiveLabel
            btnPositive.visibility = View.VISIBLE
        } else {
            btnPositive.visibility = View.GONE
        }
        tvMessage.text = this.transactionConfirmation.message
        imgResult.setImageResource(this.transactionConfirmation.imageId)
    }

    private fun initViews(dialogView: View) {
        tvTitle = dialogView.findViewById(R.id.tv_title)
        tvAmount = dialogView.findViewById(R.id.tv_amount)
        tvMessage = dialogView.findViewById(R.id.tv_message)
        imgResult = dialogView.findViewById(R.id.img_transaction_result)
        btnPositive = dialogView.findViewById(R.id.btn_positive)
        btnNegative = dialogView.findViewById(R.id.btn_negative)
        tvCurrency = dialogView.findViewById(R.id.tv_currency)
        imgTransactionResult = dialogView.findViewById(R.id.img_transaction_result)
        qrTitle = dialogView.findViewById(R.id.qr_title)
        qrTitle.visibility = View.GONE
    }

    interface Listener {
        fun onPositiveButtonClick()
        fun onNegativeButtonClick()
    }

}