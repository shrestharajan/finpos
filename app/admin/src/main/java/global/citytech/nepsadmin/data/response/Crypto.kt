package global.citytech.nepsadmin.data.response

data class Crypto(
    var type: String? = null,
    var values: Values? = null
)