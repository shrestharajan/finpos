package global.citytech.finpos.device.weipass.led

import com.wiseasy.emvprocess.SDKInstance
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_LED_ACTION_OFF
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_LED_ACTION_ON
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.led.LedAction
import global.citytech.finposframework.hardware.io.led.LedLight
import global.citytech.finposframework.hardware.io.led.LedRequest
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import wangpos.sdk4.libbasebinder.Core

/**
 * Created by Rishav Chudal on 6/4/20.
 */
class WeipassLedServiceImplTest {

    @MockK
    private lateinit var mockedCore: Core

    private lateinit var SUT: WeipassLedServiceImpl

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        SUT = WeipassLedServiceImpl()
        mockSDKInstance()
        mockWeipassLogger()
    }

    @Test
    fun doTurnLedWith_OnLedRequestWithALLLightAndLedActionIsOnAndLedApiReturnsZero_ReturnsDeviceResponseSuccess() {
        //Setup
        val request = LedRequest(
            LedLight.ALL,
            LedAction.ON
        )
        val led1Arg = slot<Int>()
        val led2Arg = slot<Int>()
        val led3Arg = slot<Int>()
        val led4Arg = slot<Int>()
        val actionArg = slot<Int>()
        every { SDKInstance.mCore.led(capture(led1Arg),
            capture(led2Arg),
            capture(led3Arg),
            capture(led4Arg),
            capture(actionArg))
        } returns 0

        //Run
        val response = SUT.doTurnLedWith(request)

        //Verify
        assertEquals(LedLight.ALL, request.ledLight)
        assertEquals(LedAction.ON, request.ledAction)

        verifyOrder {

            WeipassLogger.log("Led Light ::: "
                .plus(LedLight.ALL)
                .plus(" Action (0 -> Off, 1 -> On) ::: "
                    .plus(SDK_LED_ACTION_ON)))

            SDKInstance.mCore.led(
                SDK_LED_ACTION_ON,
                SDK_LED_ACTION_ON,
                SDK_LED_ACTION_ON,
                SDK_LED_ACTION_ON,
                SDK_LED_ACTION_ON
            )
        }

        assertTrue(led1Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_ON, led1Arg.captured)

        assertTrue(led2Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_ON, led2Arg.captured)

        assertTrue(led3Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_ON, led3Arg.captured)

        assertTrue(led4Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_ON, led4Arg.captured)

        assertTrue(actionArg.isCaptured)
        assertEquals(SDK_LED_ACTION_ON, actionArg.captured)

        assertEquals(Result.SUCCESS, response.result)
        clearAllMocks()
    }

    @Test
    fun doTurnLedWith_OnLedRequestWithBlueLightOnlyAndLedActionIsOnAndLedApiReturnsZero_ReturnsDeviceResponseSuccess() {
        //Setup
        val request = LedRequest(
            LedLight.BLUE,
            LedAction.ON
        )
        val led1Arg = slot<Int>()
        val led2Arg = slot<Int>()
        val led3Arg = slot<Int>()
        val led4Arg = slot<Int>()
        val actionArg = slot<Int>()
        every { SDKInstance.mCore.led(capture(led1Arg),
            capture(led2Arg),
            capture(led3Arg),
            capture(led4Arg),
            capture(actionArg))
        } returns 0

        //Run
        val response = SUT.doTurnLedWith(request)

        //Verify
        assertEquals(LedLight.BLUE, request.ledLight)
        assertEquals(LedAction.ON, request.ledAction)

        verifyOrder {

            WeipassLogger.log("Led Light ::: "
                .plus(LedLight.BLUE)
                .plus(" Action (0 -> Off, 1 -> On) ::: "
                    .plus(SDK_LED_ACTION_ON)))

            SDKInstance.mCore.led(
                SDK_LED_ACTION_ON,
                SDK_LED_ACTION_OFF,
                SDK_LED_ACTION_OFF,
                SDK_LED_ACTION_OFF,
                SDK_LED_ACTION_ON
            )
        }

        assertTrue(led1Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_ON, led1Arg.captured)

        assertTrue(led2Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led2Arg.captured)

        assertTrue(led3Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led3Arg.captured)

        assertTrue(led4Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led4Arg.captured)

        assertTrue(actionArg.isCaptured)
        assertEquals(SDK_LED_ACTION_ON, actionArg.captured)

        assertEquals(Result.SUCCESS, response.result)
        clearAllMocks()
    }

    @Test
    fun doTurnLedWith_OnLedRequestWithYellowLightOnlyAndLedActionIsOnAndLedApiReturnsZero_ReturnsDeviceResponseSuccess() {
        //Setup
        val request = LedRequest(
            LedLight.YELLOW,
            LedAction.ON
        )
        val led1Arg = slot<Int>()
        val led2Arg = slot<Int>()
        val led3Arg = slot<Int>()
        val led4Arg = slot<Int>()
        val actionArg = slot<Int>()
        every { SDKInstance.mCore.led(capture(led1Arg),
            capture(led2Arg),
            capture(led3Arg),
            capture(led4Arg),
            capture(actionArg))
        } returns 0

        //Run
        val response = SUT.doTurnLedWith(request)

        //Verify
        assertEquals(LedLight.YELLOW, request.ledLight)
        assertEquals(LedAction.ON, request.ledAction)

        verifyOrder {

            WeipassLogger.log("Led Light ::: "
                .plus(LedLight.YELLOW)
                .plus(" Action (0 -> Off, 1 -> On) ::: "
                    .plus(SDK_LED_ACTION_ON)))

            SDKInstance.mCore.led(
                SDK_LED_ACTION_OFF,
                SDK_LED_ACTION_ON,
                SDK_LED_ACTION_OFF,
                SDK_LED_ACTION_OFF,
                SDK_LED_ACTION_ON
            )
        }

        assertTrue(led1Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led1Arg.captured)

        assertTrue(led2Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_ON, led2Arg.captured)

        assertTrue(led3Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led3Arg.captured)

        assertTrue(led4Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led4Arg.captured)

        assertTrue(actionArg.isCaptured)
        assertEquals(SDK_LED_ACTION_ON, actionArg.captured)

        assertEquals(Result.SUCCESS, response.result)

        clearAllMocks()
    }

    @Test
    fun doTurnLedWith_OnLedRequestWithGreenLightOnlyAndLedActionIsOnAndLedApiReturnsZero_ReturnsDeviceResponseSuccess() {
        //Setup
        val request = LedRequest(
            LedLight.GREEN,
            LedAction.ON
        )
        val led1Arg = slot<Int>()
        val led2Arg = slot<Int>()
        val led3Arg = slot<Int>()
        val led4Arg = slot<Int>()
        val actionArg = slot<Int>()
        every { SDKInstance.mCore.led(capture(led1Arg),
            capture(led2Arg),
            capture(led3Arg),
            capture(led4Arg),
            capture(actionArg))
        } returns 0

        //Run
        val response = SUT.doTurnLedWith(request)

        //Verify
        assertEquals(LedLight.GREEN, request.ledLight)
        assertEquals(LedAction.ON, request.ledAction)

        verifyOrder {

            WeipassLogger.log("Led Light ::: "
                .plus(LedLight.GREEN)
                .plus(" Action (0 -> Off, 1 -> On) ::: "
                    .plus(SDK_LED_ACTION_ON)))

            SDKInstance.mCore.led(
                SDK_LED_ACTION_OFF,
                SDK_LED_ACTION_OFF,
                SDK_LED_ACTION_ON,
                SDK_LED_ACTION_OFF,
                SDK_LED_ACTION_ON
            )
        }

        assertTrue(led1Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led1Arg.captured)

        assertTrue(led2Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led2Arg.captured)

        assertTrue(led3Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_ON, led3Arg.captured)

        assertTrue(led4Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led4Arg.captured)

        assertTrue(actionArg.isCaptured)
        assertEquals(SDK_LED_ACTION_ON, actionArg.captured)

        assertEquals(Result.SUCCESS, response.result)

        clearAllMocks()
    }

    @Test
    fun doTurnLedWith_OnLedRequestWithRedLightOnlyAndLedActionIsOnAndLedApiReturnsZero_ReturnsDeviceResponseSuccess() {
        //Setup
        val request = LedRequest(
            LedLight.RED,
            LedAction.ON
        )
        val led1Arg = slot<Int>()
        val led2Arg = slot<Int>()
        val led3Arg = slot<Int>()
        val led4Arg = slot<Int>()
        val actionArg = slot<Int>()
        every { SDKInstance.mCore.led(capture(led1Arg),
            capture(led2Arg),
            capture(led3Arg),
            capture(led4Arg),
            capture(actionArg))
        } returns 0

        //Run
        val response = SUT.doTurnLedWith(request)

        //Verify
        assertEquals(LedLight.RED, request.ledLight)
        assertEquals(LedAction.ON, request.ledAction)

        verifyOrder {

            WeipassLogger.log("Led Light ::: "
                .plus(LedLight.RED)
                .plus(" Action (0 -> Off, 1 -> On) ::: "
                    .plus(SDK_LED_ACTION_ON)))

            SDKInstance.mCore.led(
                SDK_LED_ACTION_OFF,
                SDK_LED_ACTION_OFF,
                SDK_LED_ACTION_OFF,
                SDK_LED_ACTION_ON,
                SDK_LED_ACTION_ON
            )
        }
        assertTrue(led1Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led1Arg.captured)

        assertTrue(led2Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led2Arg.captured)

        assertTrue(led3Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led3Arg.captured)

        assertTrue(led4Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_ON, led4Arg.captured)

        assertTrue(actionArg.isCaptured)
        assertEquals(SDK_LED_ACTION_ON, actionArg.captured)

        assertEquals(Result.SUCCESS, response.result)

        clearAllMocks()
    }

    @Test
    fun doTurnLedWith_OnLedRequestWithAnyLightAndLedActionIsOffAndLedApiReturnsZero_ReturnsDeviceResponseSuccess() {
        //Setup
        val request = LedRequest(
            LedLight.RED,
            LedAction.OFF
        )
        val led1Arg = slot<Int>()
        val led2Arg = slot<Int>()
        val led3Arg = slot<Int>()
        val led4Arg = slot<Int>()
        val actionArg = slot<Int>()
        every { SDKInstance.mCore.led(capture(led1Arg),
            capture(led2Arg),
            capture(led3Arg),
            capture(led4Arg),
            capture(actionArg))
        } returns 0

        //Run
        val response = SUT.doTurnLedWith(request)

        //Verify
        assertEquals(LedLight.RED, request.ledLight)
        assertEquals(LedAction.OFF, request.ledAction)

        verify { SDKInstance.mCore.led(any(), any(), any(), any(), any()) }

        assertTrue(led1Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led1Arg.captured)

        assertTrue(led2Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led2Arg.captured)

        assertTrue(led3Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led3Arg.captured)

        assertTrue(led4Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led4Arg.captured)

        assertTrue(actionArg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, actionArg.captured)

        assertEquals(Result.SUCCESS, response.result)

        clearAllMocks()
    }

    @Test
    fun doTurnLedWith_OnLedRequestWithAnyLightAndLedActionIsAnyAndLedApiReturnsNonZero_ReturnsDeviceResponseFailure() {
        //Setup
        val request = LedRequest(
            LedLight.RED,
            LedAction.OFF
        )
        val led1Arg = slot<Int>()
        val led2Arg = slot<Int>()
        val led3Arg = slot<Int>()
        val led4Arg = slot<Int>()
        val actionArg = slot<Int>()
        every { SDKInstance.mCore.led(capture(led1Arg),
            capture(led2Arg),
            capture(led3Arg),
            capture(led4Arg),
            capture(actionArg))
        } returns -1

        //Run
        val response = SUT.doTurnLedWith(request)

        //Verify
        assertEquals(LedLight.RED, request.ledLight)
        assertEquals(LedAction.OFF, request.ledAction)

        verify { SDKInstance.mCore.led(any(), any(), any(), any(), any()) }

        assertTrue(led1Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led1Arg.captured)

        assertTrue(led2Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led2Arg.captured)

        assertTrue(led3Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led3Arg.captured)

        assertTrue(led4Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led4Arg.captured)

        assertTrue(actionArg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, actionArg.captured)

        assertEquals(Result.FAILURE, response.result)

        clearAllMocks()
    }

    @Test
    fun doTurnLedWith_OnLedRequestWithAnyLightAndLedActionIsAnyAndLedApiThrowsException_ReturnsDeviceResponseFailure() {
        //Setup
        val request = LedRequest(
            LedLight.RED,
            LedAction.OFF
        )
        val led1Arg = slot<Int>()
        val led2Arg = slot<Int>()
        val led3Arg = slot<Int>()
        val led4Arg = slot<Int>()
        val actionArg = slot<Int>()
        every { SDKInstance.mCore.led(capture(led1Arg),
            capture(led2Arg),
            capture(led3Arg),
            capture(led4Arg),
            capture(actionArg))
        } throws Exception("Led Exception")

        //Run
        val response = SUT.doTurnLedWith(request)

        //Verify
        assertEquals(LedLight.RED, request.ledLight)
        assertEquals(LedAction.OFF, request.ledAction)

        verifyOrder {
            SDKInstance.mCore.led(any(), any(), any(), any(), any())
            WeipassLogger.log("Exception ::: Led Exception")
        }

        assertTrue(led1Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led1Arg.captured)

        assertTrue(led2Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led2Arg.captured)

        assertTrue(led3Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led3Arg.captured)

        assertTrue(led4Arg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, led4Arg.captured)

        assertTrue(actionArg.isCaptured)
        assertEquals(SDK_LED_ACTION_OFF, actionArg.captured)

        assertEquals(Result.FAILURE, response.result)

        clearAllMocks()
    }

    private fun mockSDKInstance() {
        mockkStatic(SDKInstance::class)
        SDKInstance.mCore = mockedCore
    }

    private fun mockWeipassLogger() {
        mockkObject(WeipassLogger)
        mockkObject(WeipassLogger.Companion)
        every { WeipassLogger.log(any()) } just Runs
    }
}