package global.citytech.finpos.merchant.domain.usecase.qr.base
/**
 * @author sachin
 */
interface QrGenerator<I : QrGenerateRequest, O : QrGenerateResponse> {
    fun generate(qrGenerateRequest: I): O
}