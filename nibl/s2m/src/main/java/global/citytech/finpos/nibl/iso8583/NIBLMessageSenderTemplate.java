package global.citytech.finpos.nibl.iso8583;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import global.citytech.finposframework.comm.NonCloseableTcpClient;
import global.citytech.finposframework.log.Logger;;

import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.DefaultMessageParser;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.MessageParser;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.comm.Request;
import global.citytech.finposframework.comm.Response;
import global.citytech.finposframework.comm.TPDU;
import global.citytech.finposframework.comm.TcpClient;
import global.citytech.finposframework.utility.ByteUtils;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.HexDump;
import global.citytech.finposframework.utility.NumberUtils;
import global.citytech.finposframework.utility.StackTraceUtil;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.supports.MessagePayload;

public abstract class NIBLMessageSenderTemplate {
    protected final SpecInfo specInfo;
    protected final RequestContext context;
    NonCloseableTcpClient tcpClient = null;
    private Logger logger = Logger.getLogger(NIBLMessageSenderTemplate.class.getName());

    public NIBLMessageSenderTemplate(SpecInfo specInfo, RequestContext context) {
        this.context = context;
        this.specInfo = specInfo;
    }

    protected abstract MessagePayload prepareRequestPayload();

    protected abstract String getProcessingCode();

    protected abstract String getFunctionCode();

    protected Iso8583Msg getTemplateIsoMessage(MTI mti) {
        Iso8583Msg msg = new Iso8583Msg(mti);
        msg.addField(new DataElement(DataElement.PROCESSING_CODE, this.getProcessingCode()));
        msg.addField(new DataElement(DataElement.SYSTEM_TRACE_AUDIT_NUMBER, this.context.getStan()));
        msg.addField(new DataElement(DataElement.FUNCTION_CODE_OR_NETWORK_INTERNATIONAL_IDENTIFIER, this.getFunctionCode()));
        msg.addField(new DataElement(DataElement.CARD_ACCEPTOR_TERMINAL_ID, this.context.getTerminalInfo().getTerminalID()));
        msg.addField(new DataElement(DataElement.CARD_ACCEPTOR_ACQUIRER_ID, this.context.getTerminalInfo().getMerchantID()));
        logger.log("Terminal ID " + this.context.getTerminalInfo().getTerminalID());
        logger.log("Merchant ID " + this.context.getTerminalInfo().getMerchantID());
        return msg;
    }

    //TODO : Need to send this on utility ...
    protected String formatStan(String stan) {
        //HelperUtils.generateRetrievalReferenceNumber()
        StringBuilder builder = new StringBuilder();
        if (stan.length() <= 6) {
            int diff = 6 - stan.length();
            builder.append(HelperUtils.repeat("0", diff));
        }
        builder.append(stan);
        //TODO : remove
        String nanoTime = String.valueOf(System.nanoTime());
        // return builder.toString();
        return nanoTime.substring(0, 6);
    }

    public IsoMessageResponse send() {
        return send(true);
    }

    public IsoMessageResponse send(boolean closeSocketAfterExecution) {
        String isoRequest = "";
        String isoResponse = "";
        try {
            tcpClient = NonCloseableTcpClient.getInstance(context.getParam());
            HostInfo hostInfo = tcpClient.dial();
            logger.log("Connected with ::" + hostInfo);
            // Prepare ISO request payload
            MessagePayload payload = this.prepareRequestPayload();
            // Decorate request message with required other data tpdu and length
            Request requestPacket = this.prepareRequestPacket(payload, hostInfo);
            //Adding request packet to Message Payload
            payload.setRequestPacket(requestPacket);
            isoRequest = HexDump.dumpHexString(requestPacket.getData());
            logger.debug("Request");
            logger.debug(isoRequest);
            tcpClient.write(requestPacket);
            Response lenResponse = tcpClient.read(specInfo.getMessageHeaderLength());
            logger.log(" Response Received ");
            logger.debug(" ==== LENGTH DATA RESPONSE ====");
            logger.debug(HexDump.dumpHexString(lenResponse.getData()));
            int len = NumberUtils.fromHexASCIIBytesToInt(lenResponse.getData());
            logger.debug("Header Length : " + len);
            Response data = tcpClient.read(len);
            logger.debug(" ==== RESPONSE ====");
            isoResponse = HexDump.dumpHexString(data.getData());
            logger.debug(isoResponse);
            MessageParser messageParser = new DefaultMessageParser(this.specInfo);
            // Parse data after removing TPDU
            byte[] onlyMessage = this.removeTPDU(data.getData());
            logger.debug(HexDump.dumpHexString(onlyMessage));
            Iso8583Msg msg = messageParser.parse(onlyMessage);
            logger.debug(msg.getPrintableData());
            IsoMessageResponse.Builder responseBuilder =
                    IsoMessageResponse.Builder.createDefaultBuilder(msg, hostInfo, payload);
            responseBuilder.responseLength(len);
            responseBuilder.debugRequestString(HexDump.dumpHexString(requestPacket.getData()));
            responseBuilder.debugResponseString(HexDump.dumpHexString(data.getData()));
            return responseBuilder.build();
        } catch (Exception e) {
            closeTcpClient();
            e.printStackTrace();
            if (e instanceof FinPosException) {
                throw new FinPosException(((FinPosException) e).getType(), ((FinPosException) e).getDetailMessage(), isoRequest, isoResponse);
            }
            throw new FinPosException(
                    FinPosException.ExceptionType.SYSTEM_ERROR, StackTraceUtil.getStackTraceAsString(e), isoRequest, isoResponse);
        } finally {
            if (closeSocketAfterExecution) {
                closeTcpClient();
            }
        }
    }

    public void closeTcpClient() {
        try {
            if (tcpClient != null)
                tcpClient.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected byte[] removeTPDU(byte[] data) {
        return ByteUtils.remove(data, TPDU.SIZE_IN_BYTES);
    }

    protected Request prepareRequestPacket(MessagePayload request, HostInfo hostInfo) {
        try (ByteArrayOutputStream messagePacket = new ByteArrayOutputStream()) {
            int totalMessageLength = hostInfo.getTpdu().getAsBytes().length + request.getAsBytes().length;
            // Add Message Length
            messagePacket.write(NumberUtils.toBytes(totalMessageLength, specInfo.getMessageHeaderLength()));
            // Add TPDU
            messagePacket.write(hostInfo.getTpdu().getAsBytes());
            // Add ISO message
            messagePacket.write(request.getAsBytes());
            // Prepare Request
            return new Request(messagePacket.toByteArray());

        } catch (IOException e) {
            throw new FinPosException(
                    FinPosException.ExceptionType.SYSTEM_ERROR, StackTraceUtil.getStackTraceAsString(e));
        }
    }
}