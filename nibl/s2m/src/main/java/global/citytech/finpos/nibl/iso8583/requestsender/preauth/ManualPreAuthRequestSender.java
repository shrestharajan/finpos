package global.citytech.finpos.nibl.iso8583.requestsender.preauth;

import global.citytech.finpos.nibl.iso8583.PosEntryModeConfig;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.switches.PosEntryMode;

/**
 * Created by Rishav Chudal on 10/15/20.
 */
public class ManualPreAuthRequestSender extends PreAuthRequestSender {

    public ManualPreAuthRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
    }

    @Override
    public Iso8583Msg addConditionalFields(Iso8583Msg msg, TransactionIsoRequest preAuthRequest) {
        msg.addField(
                new DataElement(
                        DataElement.PRIMARY_ACCOUNT_NUMBER,
                        preAuthRequest.getPan()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.PAN_EXPIRE_DATE,
                        preAuthRequest.getExpireDate()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(PosEntryMode.MANUAL)
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.ADDITIONAL_DATA_PRIVATE,
                        preAuthRequest.getCvv()
                )
        );
        return msg;
    }
}
