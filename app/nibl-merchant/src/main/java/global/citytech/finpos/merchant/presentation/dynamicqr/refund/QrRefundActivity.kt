package global.citytech.finpos.merchant.presentation.dynamicqr.refund

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.common.extensions.hideKeyboard
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityRefundBinding
import global.citytech.finpos.merchant.domain.model.app.QrPayment
import global.citytech.finpos.merchant.presentation.admin.login.LoginDialog
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmationDialog
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finpos.merchant.utils.disableMe
import global.citytech.finpos.merchant.utils.enableMe
import global.citytech.finpos.merchant.utils.retrieveCurrencyName
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion
import global.citytech.finposframework.utility.StringUtils
import kotlinx.android.synthetic.main.activity_refund.*

class QrRefundActivity : AppBaseActivity<ActivityRefundBinding, QrRefundViewModel>(),
    TransactionConfirmationDialog.Listener, LoginDialog.LoginDialogListener {

    private var transactionConfirmationDialog: TransactionConfirmationDialog? = null
    private lateinit var loginDialog: LoginDialog

    companion object {

        const val REQUEST_CODE = 9090
        private const val EXTRA_INVOICE_NUMBER = "extra.invoice.number"

        fun getLaunchIntent(context: Context, invoiceNumber: String): Intent {
            val intent = Intent(context, QrRefundActivity::class.java)
            intent.putExtra(EXTRA_INVOICE_NUMBER, invoiceNumber)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        initObservers()
        hideRefundLayout()
        getViewModel().getConfigurationItem()
        intent.getStringExtra(EXTRA_INVOICE_NUMBER)?.let {
            et_refund_invoice.setText(StringUtils.ofRequiredLength(it, 6))
            btn_refund_confirm.performClick()
        }
    }

    private fun initObservers() {
        getViewModel().isLoading.observe(this, Observer {
            if (it)
                showProgress()
            else
                hideProgress()
        })

        getViewModel().finishActivity.observe(this, Observer {
            if (it)
                finish()
        })

        getViewModel().message.observe(this, Observer {
            showNormalMessage(
                MessageConfig.Builder().message(it).positiveLabel(getString(R.string.title_ok))
                    .onPositiveClick { }
            )
        })

        getViewModel().originalQrPayment.observe(this, Observer {
            it?.let {
                loadRefundLayout(it)
            }
        })

        NiblMerchant.INSTANCE.networkConnectionAvailable.observe(
            this,
            Observer {
                if (it) {
                    ll_refund_no_connection.visibility = View.GONE
                    btn_refund_confirm.enableMe()
                } else {
                    ll_refund_no_connection.visibility = View.VISIBLE
                    btn_refund_confirm.disableMe()
                }
            }
        )

        getViewModel().transactionConfirmationLiveData.observe(this, Observer {
            showTransactionConfirmationDialog(it, getViewModel().updateTransactionConfirmation)
        })
    }

    private fun showTransactionConfirmationDialog(it: TransactionConfirmation, update: Boolean) {
        if (transactionConfirmationDialog == null)
            transactionConfirmationDialog = TransactionConfirmationDialog(this, this, it)
        if (update)
            transactionConfirmationDialog!!.update(it, this)
        transactionConfirmationDialog!!.show()
    }

    private fun initViews() {
        iv_refund_back.setOnClickListener {
            finish()
        }

        btn_refund_cancel.setOnClickListener {
            finish()
        }

        btn_refund_confirm.setOnClickListener {
            if (btn_refund_confirm.text.toString().toLowerCase() == "next") {
                getViewModel().onInvoiceNumberRetrieved(
                    StringUtils.ofRequiredLength(
                        et_refund_invoice.text.toString(),
                        6
                    )
                )
                btn_refund_confirm.text = "Confirm"
            } else {
                showMerchantLoginDialog(
                    getString(R.string.msg_merchant_password),
                    getViewModel().getMerchantCredential()
                )
            }
        }

        et_refund_invoice.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                // do nothing
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                hideRefundLayout()
            }

            override fun afterTextChanged(p0: Editable?) {
                // do nothing
            }

        })
    }

    private fun showMerchantLoginDialog(pageTitle: String, validCredential: String) {
        this.loginDialog = LoginDialog.newInstance(
            pageTitle = pageTitle,
            validCredential = validCredential
        )
        this.loginDialog.isCancelable = false
        this.loginDialog.show(
            supportFragmentManager,
            LoginDialog.TAG
        )
    }

    override fun onLoginDialogCorrectCredentialEntered() {
        hideKeyboard(et_refund_invoice)
        hideRefundLayout()
        getViewModel().performRefundTransaction()
    }

    override fun onLoginDialogCancelButtonClicked() {
        hideKeyboard(et_refund_invoice)
    }

    private fun loadRefundLayout(qrPayment: QrPayment) {
        ll_refund_txn_detail.visibility = View.VISIBLE
        tv_refund_txn_type.text = qrPayment.transactionType
        tv_refund_amount.text = retrieveCurrencyName(qrPayment.transactionCurrency).plus(" ")
            .plus(qrPayment.transactionAmount)
        tv_payment_initiatior.text = qrPayment.paymentInitiator
        tv_initiator_id.text = qrPayment.initiatorId
    }

    private fun hideRefundLayout() {
        ll_refund_txn_detail.visibility = View.GONE
        btn_refund_confirm.text = "Next"
    }

    override fun getBindingVariable(): Int = BR.qrRefundViewModel

    override fun getLayout(): Int = R.layout.activity_refund

    override fun getViewModel(): QrRefundViewModel =
        ViewModelProviders.of(this)[QrRefundViewModel::class.java]

    override fun onPositiveButtonClick() {
        getViewModel().printQrRefundReceipt(
            getViewModel().refundQrPayment.value,
            ReceiptVersion.CUSTOMER_COPY
        )
    }

    override fun onNegativeButtonClick() {
        finish()
    }
}