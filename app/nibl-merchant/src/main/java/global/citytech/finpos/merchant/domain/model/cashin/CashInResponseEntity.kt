package global.citytech.finpos.merchant.domain.model.cashin

data class CashInResponseEntity (
    val stan:String? = null,
    val message: String = "",
    val isApproved: Boolean
)