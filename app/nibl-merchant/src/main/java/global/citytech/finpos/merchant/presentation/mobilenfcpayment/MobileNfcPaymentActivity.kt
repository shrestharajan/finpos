package global.citytech.finpos.merchant.presentation.mobilenfcpayment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bumptech.glide.Glide
import global.citytech.common.Constants
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.databinding.ActivityMobileNfcPaymentBinding
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmationDialog
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.dynamicqr.nqrsale.DynamicNQrActivity
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finpos.merchant.utils.SoundUtils
import global.citytech.finpos.merchant.utils.VoiceMessageUtils
import global.citytech.finpos.merchant.utils.loadCurrency
import global.citytech.finposframework.hardware.io.led.LedLight
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.notifier.Notifier
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion
import global.citytech.finposframework.utility.StringUtils
import kotlinx.android.synthetic.main.activity_mobile_nfc_payment.*
import java.math.BigDecimal

/**
 * @author SIDDHARTHA GHIMIRE
 */
class MobileNfcPaymentActivity :
    AppBaseActivity<ActivityMobileNfcPaymentBinding, MobileNfcPaymentViewModel>(),
    TransactionConfirmationDialog.Listener {

    private var transactionAmount: BigDecimal = BigDecimal.ZERO
    private var transactionConfirmationDialog: TransactionConfirmationDialog? = null
    val handler = Handler()

    private var localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    companion object {
        const val REQUEST_CODE = 2100
        const val EXTRA_TRANSACTION_AMOUNT = "extra.transaction.amount"
        const val EXTRA_MOBILE_PAYMENT = "extra.mobile.payment"
    }

    private lateinit var viewModel: MobileNfcPaymentViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.viewModel = getViewModel()
        initViews()
        initData()
        if (!AppUtility.isAppVariantNIBL()) {
            observeAid()
            getViewModel().getAidListForNFCPayment()
        } else {
            getViewModel().getConfiguration()
        }
        getViewModel().bindService()
        observeMobilePayStatusMessage()
        observeBaseLiveData()
        observeAppConfigurationLiveData()
        observeTransactionConfirmation()
        observeWaveAgain()
        observeNoInternetConnection()
    }

    private fun initViews() {
        getViewModel().showPiccLoading.value = true
        iv_back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun observeNoInternetConnection() {
        getViewModel().internetConnection.observe(
            this
        ) {
            if (it) {
                getViewModel().nfcStatusMessage.value = MobileNfcPaymentConst.ERROR_TIMEOUT
            } else {
                tv_progress_msg.text = getString(R.string.please_wait)
                showTransactionDeclinedDialog(getString(R.string.no_connection))
            }
        }
    }

    private fun showHeaderView(amount: BigDecimal) {
        ll_header.visibility = View.VISIBLE
        tv_transaction_type.text = TransactionType.PURCHASE.displayName
        tv_total_amount_label.text = getString(R.string.title_total_amount)
        tv_transaction_amount.text = StringUtils.formatAmountTwoDecimal(amount)
        tv_currency.loadCurrency()
    }

    private fun initData() {
        transactionAmount = intent.getSerializableExtra(EXTRA_TRANSACTION_AMOUNT) as BigDecimal
    }

    private fun observeMobilePayStatusMessage() {
        getViewModel().nfcStatusMessage.observe(this) {
            getViewModel().cancelNfcTimer()
            handler.removeCallbacksAndMessages(null)
            showNormalMessage(
                MessageConfig.Builder()
                    .message(it)
                    .positiveLabel(getString(R.string.title_ok))
                    .onPositiveClick {
                        cleanTask()
                    })
        }
    }

    private fun observeBaseLiveData() {
        getViewModel().isLoading.observe(this) {
            if (it) {
                showProgress()
            } else {
                hideProgress()
            }
        }

        getViewModel().message.observe(this) {
            if (it.contains("Success")) {
                setResultAndFinish()
            } else {
                showNormalMessage(
                    MessageConfig.Builder()
                        .message(it)
                        .positiveLabel(getString(R.string.title_ok))
                        .onPositiveClick {
                            setResultAndFinish()
                        })
            }

        }
        getViewModel().showNfcSuccess.observe(this) {
            if (it.contains("Success")) {
                val messageConfig = MessageConfig.Builder()
                    .message(it)
                    .positiveLabel(getString(R.string.title_ok))
                    .onPositiveClick {
//                    checkForCardPresent()
                        finish()
                    }
                showSuccessMessage(messageConfig)

            }
        }

        getViewModel().showAlertDialog.observe(this) {
            if (it) {
                getViewModel().cancelNfcTimer()
                showConfirmationMessage(MessageConfig.Builder()
                    .title(getString(R.string.time_out))
                    .message(getString(R.string.msg_confirmation_continue_nfc_payments))
                    .positiveLabel(getString(R.string.action_yes))
                    .onPositiveClick {
                        getViewModel().prepareNfcData()
                        hideConfirmationMessage()
                    }
                    .negativeLabel(getString(R.string.action_no))
                    .onNegativeClick {
                        cleanTask()
                    })
            } else {
                hideConfirmationMessage()
            }
        }

        getViewModel().showProgressMessage.observe(this) {
            tv_progress_msg.text = it
        }

        getViewModel().showPiccLoading.observe(this) {
            if (!it) {
                Glide.with(this).load("file:///android_asset/images/loading.gif").into(iv_loading)
                iv_loading_picc.visibility = View.GONE
                iv_loading.visibility = View.VISIBLE
            } else {
                Glide.with(this).load("file:///android_asset/images/picc_only.gif")
                    .into(iv_loading_picc)
                iv_loading_picc.visibility = View.VISIBLE
                iv_loading.visibility = View.GONE
                tv_progress_msg.text = getString(R.string.tap_and_pay)
            }
        }
    }

    private fun observeAppConfigurationLiveData() {
        getViewModel().appConfiguration.observe(
            this
        ) {
            if (it.merchants!!.isNotEmpty()) {
                showHeaderView(transactionAmount)
                getViewModel().amount = transactionAmount.toString()
                NotificationHandler.init(applicationContext)
                getViewModel().prepareNfcData();
            }
        }
    }

    private fun observeTransactionConfirmation() {
        getViewModel().transactionConfirmationLiveData.observe(this) {
            showTransactionConfirmationDialog(it, getViewModel().updateTransactionConfirmation)
        }
    }

    private fun observeWaveAgain() {
        getViewModel().performWaveAgain.observe(
            this
        ) {
            getViewModel().cancelNfcTimer()
            handler.removeCallbacksAndMessages(null)
            if (it) {
                Glide.with(this).load("file:///android_asset/images/picc_only.gif")
                    .into(iv_loading_picc)
                iv_loading_picc.visibility = View.VISIBLE
                iv_loading.visibility = View.GONE
                tv_progress_msg.text = getString(R.string.title_wave_again)
                handler.postDelayed({
                    getViewModel().prepareNfcData()
                }, 2000)
            }
        }
    }

    private fun observeAid() {
        getViewModel().aidReceivedSuccessMesage.observe(
            this
        ) {
            getViewModel().getConfiguration()
        }
    }

    private fun showTransactionConfirmationDialog(it: TransactionConfirmation, update: Boolean) {
        if (transactionConfirmationDialog == null)
            transactionConfirmationDialog = TransactionConfirmationDialog(this, this, it)
        if (update)
            transactionConfirmationDialog!!.update(it, this)
        transactionConfirmationDialog!!.show()
    }

    private fun showTransactionDeclinedDialog(message: String) {
        val transactionConfirmation = TransactionConfirmation.Builder()
            .title(TransactionType.PURCHASE.displayName + "\nTransaction Declined")
            .message(message)
            .imageId(R.drawable.declined)
            .positiveLabel("")
            .negativeLabel(getString(R.string.title_ok))
            .qrImageString("")
            .transactionStatus("")
            .build()
        this.showTransactionConfirmationDialog(transactionConfirmation, false)
    }

    override fun getBindingVariable(): Int = BR.mobileViewModel

    override fun getLayout(): Int = R.layout.activity_mobile_nfc_payment

    override fun getViewModel(): MobileNfcPaymentViewModel =
        ViewModelProviders.of(this)[MobileNfcPaymentViewModel::class.java]

    override fun onPositiveButtonClick() {
        getViewModel().printPaymentReceipt(ReceiptVersion.CUSTOMER_COPY)
    }

    override fun onNegativeButtonClick() {
        setResultAndFinish()
    }

    private fun setResultAndFinish() {
        try {
            getViewModel().cleanNfcTimerAndLatches()
            getViewModel().turnOffLedIfExist()
            getViewModel().dispose()
            getViewModel().doCleanUp()
            val intent = Intent()
            intent.putExtra(EXTRA_MOBILE_PAYMENT, getViewModel().mobileNfcPayment)
            setResult(RESULT_OK, intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        finish()
    }

    override fun onResume() {
        super.onResume()
        this.registerBroadCastReceivers()

    }

    private fun registerBroadCastReceivers() {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, IntentFilter(Constants.INTENT_ACTION_NOTIFICATION))
        registerReprintBroadcastReceivers()
    }

    private var broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null && intent.action == Constants.INTENT_ACTION_NOTIFICATION) {
                handleNotificationBroadcast(intent)
            }
        }
    }

    private fun handleNotificationBroadcast(intent: Intent) {
        val eventType =
            intent.getSerializableExtra(Constants.EXTRA_EVENT_TYPE) as Notifier.EventType
        val message = intent.getStringExtra(Constants.EXTRA_NOTIFICATION_MESSAGE)
        when (eventType) {
            Notifier.EventType.STARTING_READ_MOBILE_NFC -> {
                handler.removeCallbacksAndMessages(null)
                getViewModel().showPiccLoading.value = false
                getViewModel().showProgressMessage.value = message
            }

            Notifier.EventType.NFC_MOBILE_TIMEOUT -> {
                handler.removeCallbacksAndMessages(null)
                getViewModel().enableAlertDialogFlag()
            }

            Notifier.EventType.NFC_SOURCE_INVALID -> {
                getViewModel().nfcStatusMessage.value = message
            }
            else -> {}
        }
    }

    override fun onBackPressed() {
        getViewModel().cancelNfcTimer()
        handler.removeCallbacksAndMessages(null)
        showConfirmationMessage(MessageConfig.Builder()
            .title(getString(R.string.cancel))
            .message(getString(R.string.msg_confirmation_cancel_nfc_payments))
            .positiveLabel(getString(R.string.action_yes))
            .onPositiveClick {
                cleanTask()
            }
            .negativeLabel(getString(R.string.action_no))
            .onNegativeClick {
                getViewModel().prepareNfcData()
                hideConfirmationMessage()
            })
    }

    override fun onPause() {
        super.onPause()
        try {
            handler.removeCallbacksAndMessages(null)
            LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(broadcastReceiver)
            unregisterReprintBroadCastReceivers()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun cleanTask() {
        try {
            getViewModel().cleanNfcTimerAndLatches()
            getViewModel().turnOffLedIfExist()
            getViewModel().dispose()
            getViewModel().doCleanUp()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        setResult(RESULT_OK)
        finish()
    }
}