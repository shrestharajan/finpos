package global.citytech.finpos.neps.receipt.logon;

import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.usecases.transaction.receipt.logon.MerchantLogonReceipt;

public class NepsMerchantLogonReceiptHandler {

    private IsoMessageResponse isoMessageResponse;

    public NepsMerchantLogonReceiptHandler(IsoMessageResponse response) {
        this.isoMessageResponse = response;
    }

    public MerchantLogonReceipt prepare() {
        return MerchantLogonReceipt.Builder.aMerchantLogonReceipt()
                .withMessage(this.prepareLogonResultMessage())
                .build();
    }

    private String prepareLogonResultMessage() {
        if (isoMessageResponse.getMsg().getDataElementByIndex(39).get().getValueAsInt() == 0)
            return NepsMerchantLogonReceiptLabels.SUCCESS_MESSAGE;
        else
            return NepsMerchantLogonReceiptLabels.FAILURE_MESSAGE;
    }
}
