package global.citytech.finpos.merchant.data.repository.core.ministatement

import global.citytech.finpos.merchant.data.datasource.core.ministatement.MiniStatementDataSource
import global.citytech.finpos.merchant.domain.model.ministatement.MiniStatementResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.ministatement.MiniStatementRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.ministatement.MiniStatementRequestItem
import io.reactivex.Observable


class MiniStatementRepositoryImpl(private val miniStatementDataSource: MiniStatementDataSource) :
    MiniStatementRepository {
    override fun generateMiniStatementRequest(
        configurationItem: ConfigurationItem,
        miniStatementRequestItem: MiniStatementRequestItem
    ): Observable<MiniStatementResponseEntity> =
        miniStatementDataSource.generateMiniStatementRequest(
            configurationItem,
            miniStatementRequestItem
        )
}