package global.citytech.finposframework.switches.cashadvance;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 2/4/2021.
 */
public interface CashAdvanceRequester<T extends CashAdvanceRequestParameter, E extends CashAdvanceResponseParameter>
        extends Request<T, E> {
}
