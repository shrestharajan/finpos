package global.citytech.finpos.merchant.data.datasource.app.db

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import global.citytech.finpos.merchant.data.datasource.app.db.dao.*
import global.citytech.finpos.merchant.domain.model.app.*


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 *
 * Updated by Bikash Shrestha on 11/01/21
 */
@Database(
    entities = [Host::class, Merchant::class, Logo::class, AidParam::class, CardScheme::class,
        EmvKey::class, EmvParam::class, MessageCode::class, TransactionLog::class, TransactionConfig::class,
        MerchantTransactionLog::class, ActivityLog::class, Disclaimer::class, Config::class],
    version = 13,
    exportSchema = true
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getHostDao(): HostsDao
    abstract fun getMerchantDao(): MerchantsDao
    abstract fun getLogoDao(): LogosDao
    abstract fun getAidParamDao(): AidParamsDao
    abstract fun getCardSchemeDao(): CardSchemesDao
    abstract fun getEmvKeyDao(): EmvKeysDao
    abstract fun getEmvParamDao(): EmvParamsDao
    abstract fun getMessageCodeDao(): MessageCodesDao
    abstract fun getTransactionLogDao(): TransactionsDao
    abstract fun getTransactionConfigDao(): TransactionConfigsDao
    abstract fun getMerchantTransactionLogDao(): MerchantTransactionLogDao
    abstract fun getActivityLogDao(): ActivityLogDao
    abstract fun getDisclaimerDao(): DisclaimerDao
    abstract fun getConfigDao(): ConfigsDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    "nibl_db.db"
                )
                    .addMigrations(
                        MIGRATION_V1_V2,
                        MIGRATION_V2_V3,
                        MIGRATION_V3_V4,
                        MIGRATION_V4_V5,
                        MIGRATION_V5_V6,
                        MIGRATION_V6_V7,
                        MIGRATION_V7_V8,
                        MIGRATION_V8_V9,
                        MIGRATION_V9_V10,
                        MIGRATION_V10_V11,
                        MIGRATION_V11_V12,
                        MIGRATION_V12_V13
                    )
                    .build()
            }
            if (!INSTANCE!!.isOpen) {
                INSTANCE!!.close()
                INSTANCE!!.openHelper.writableDatabase
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        val MIGRATION_V1_V2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE ${MerchantTransactionLog.TABLE_NAME} ADD COLUMN ${MerchantTransactionLog.COLUMN_APPROVAL_CODE} TEXT DEFAULT ''")
                database.execSQL("ALTER TABLE ${MerchantTransactionLog.TABLE_NAME} ADD COLUMN ${MerchantTransactionLog.COLUMN_ORIGINAL_TXN_REFERENCE} TEXT DEFAULT ''")
                database.execSQL("ALTER TABLE ${MerchantTransactionLog.TABLE_NAME} ADD COLUMN ${MerchantTransactionLog.COLUMN_POS_ENTRY_MODE} TEXT DEFAULT ''")
                database.execSQL("ALTER TABLE ${MerchantTransactionLog.TABLE_NAME} ADD COLUMN ${MerchantTransactionLog.COLUMN_REMARK} TEXT DEFAULT ''")
                database.execSQL("ALTER TABLE ${TransactionLog.TABLE_NAME} ADD COLUMN ${TransactionLog.COLUMN_ORIGINAL_TRANSACTION_REFERENCE_NUMBER} TEXT DEFAULT ''")
                database.execSQL("ALTER TABLE ${TransactionLog.TABLE_NAME} ADD COLUMN ${TransactionLog.COLUMN_RESPONSE_CODE} TEXT DEFAULT ''")
                database.execSQL("ALTER TABLE ${TransactionLog.TABLE_NAME} ADD COLUMN ${TransactionLog.COLUMN_AUTHORIZATION_COMPLETED} INTEGER")
                database.execSQL("ALTER TABLE ${TransactionLog.TABLE_NAME} ADD COLUMN ${TransactionLog.COLUMN_RECEIPT_LOG} TEXT DEFAULT ''")
            }
        }

        @VisibleForTesting
        val MIGRATION_V2_V3 = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE ${TransactionLog.TABLE_NAME} ADD COLUMN ${TransactionLog.COLUMN_TRANSACTION_VOIDED} INTEGER")
            }
        }

        @VisibleForTesting
        val MIGRATION_V3_V4 = object : Migration(3, 4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE ${TransactionLog.TABLE_NAME} ADD COLUMN ${TransactionLog.COLUMN_TIP_ADJUSTED} INTEGER")
            }
        }

        @VisibleForTesting
        val MIGRATION_V4_V5 = object : Migration(4, 5) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE ${Host.TABLE_NAME} ADD COLUMN ${Host.COLUMN_TLS_CERTIFICATE} TEXT DEFAULT ''")
            }
        }

        @VisibleForTesting
        val MIGRATION_V5_V6 = object : Migration(5, 6) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE IF NOT EXISTS ${ActivityLog.TABLE_NAME} (${ActivityLog.COLUMN_ID} TEXT NOT NULL,${ActivityLog.COLUMN_STAN} TEXT NOT NULL,${ActivityLog.COLUMN_TYPE} TEXT NOT NULL,${ActivityLog.COLUMN_MERCHANT_ID} TEXT NOT NULL,${ActivityLog.COLUMN_TERMINAL_ID} TEXT NOT NULL,${ActivityLog.COLUMN_BATCH_NUMBER} TEXT,${ActivityLog.COLUMN_AMOUNT} INTEGER,${ActivityLog.COLUMN_RESPONSE_CODE} TEXT NOT NULL,${ActivityLog.COLUMN_ISO_REQUEST} TEXT,${ActivityLog.COLUMN_ISO_RESPONSE} TEXT,${ActivityLog.COLUMN_STATUS} TEXT NOT NULL,${ActivityLog.COLUMN_REMARKS} TEXT,${ActivityLog.COLUMN_POS_DATE_TIME} TEXT NOT NULL, PRIMARY KEY(${ActivityLog.COLUMN_ID}))");
                database.execSQL("ALTER TABLE ${MerchantTransactionLog.TABLE_NAME} ADD COLUMN ${MerchantTransactionLog.COLUMN_BIN} TEXT DEFAULT ''")
            }
        }

        @VisibleForTesting
        val MIGRATION_V6_V7 = object : Migration(6, 7) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE ${ActivityLog.TABLE_NAME} ADD COLUMN ${ActivityLog.COLUMN_ADDITIONAL_DATA} TEXT DEFAULT ''")
            }
        }

        @VisibleForTesting
        val MIGRATION_V7_V8 = object : Migration(7, 8) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE ${MerchantTransactionLog.TABLE_NAME} ADD COLUMN ${MerchantTransactionLog.COLUMN_EMI_INFO} TEXT DEFAULT ''")
                database.execSQL("ALTER TABLE ${TransactionLog.TABLE_NAME} ADD COLUMN ${TransactionLog.COLUMN_IS_EMI_TRANSACTION} INTEGER")
                database.execSQL("ALTER TABLE ${TransactionLog.TABLE_NAME} ADD COLUMN ${TransactionLog.COLUMN_EMI_INFO} TEXT DEFAULT ''")
            }
        }

        @VisibleForTesting
        val MIGRATION_V8_V9 = object : Migration(8, 9) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL(
                    "CREATE TABLE IF NOT EXISTS ${Config.TABLE_NAME} " +
                            "(" +
                            "${Config.COLUMN_ID} TEXT NOT NULL," +
                            "${Config.COLUMN_AMOUNT_PER_TRANSACTION} LONG," +
                            "${Config.COLUMN_RECONCILE_TIME} TEXT NOT NULL," +
                            "${Config.COLUMN_KEYS} TEXT NOT NULL," +
                            "${Config.COLUMN_ADDITIONAL_DATA} TEXT NOT NULL," +
                            "PRIMARY KEY(${Config.COLUMN_ID})" +
                            ")"
                )
                database.execSQL("ALTER TABLE ${TransactionLog.TABLE_NAME} ADD COLUMN ${TransactionLog.COLUMN_PROCESSING_CODE} TEXT DEFAULT ''")
            }
        }

        @VisibleForTesting
        val MIGRATION_V9_V10 = object : Migration(9, 10) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE IF NOT EXISTS ${Disclaimer.TABLE_NAME} (${Disclaimer.COLUMN_ID} TEXT NOT NULL PRIMARY KEY, ${Disclaimer.COLUMN_SERVICE_ID} TEXT NOT NULL, ${Disclaimer.COLUMN_SERVICE_NAME} TEXT NOT NULL, ${Disclaimer.COLUMN_NOTIFICATION_TITLE} TEXT NOT NULL, ${Disclaimer.COLUMN_NOTIFICATION_CONTENT} TEXT, ${Disclaimer.COLUMN_DISCLAIMER_INFO} TEXT, ${Disclaimer.COLUMN_DISCLAIMER_CONTENT} TEXT NOT NULL, ${Disclaimer.COLUMN_REMARKS} TEXT)")
            }
        }

        @VisibleForTesting
        val MIGRATION_V10_V11 = object : Migration(10, 11) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE ${MerchantTransactionLog.TABLE_NAME} ADD COLUMN ${MerchantTransactionLog.COLUMN_CURRENCY_CODE} TEXT DEFAULT ''")
                database.execSQL("ALTER TABLE ${TransactionLog.TABLE_NAME} ADD COLUMN ${TransactionLog.COLUMN_CURRENCY_CODE} TEXT DEFAULT ''")
            }
        }

        @VisibleForTesting
        val MIGRATION_V11_V12 = object : Migration(11, 12) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE ${MerchantTransactionLog.TABLE_NAME} ADD COLUMN  ${MerchantTransactionLog.COLUMN_VAT_AMOUNT} TEXT DEFAULT ''")
                database.execSQL("ALTER TABLE ${MerchantTransactionLog.TABLE_NAME} ADD COLUMN  ${MerchantTransactionLog.COLUMN_VAT_REFUND_AMOUNT} TEXT DEFAULT ''")
                database.execSQL("ALTER TABLE ${MerchantTransactionLog.TABLE_NAME} ADD COLUMN  ${MerchantTransactionLog.COLUMN_CVM_MESSAGE} TEXT DEFAULT ''")
                database.execSQL("ALTER TABLE ${MerchantTransactionLog.TABLE_NAME} ADD COLUMN  ${MerchantTransactionLog.COLUMN_TRANSACTION_EMV_INFO} TEXT DEFAULT ''")
                database.execSQL("ALTER TABLE ${TransactionLog.TABLE_NAME} ADD COLUMN ${TransactionLog.COLUMN_VAT_INFO} TEXT DEFAULT ''")
            }
        }

        @VisibleForTesting
        val MIGRATION_V12_V13 = object : Migration(12,13) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("DROP TABLE IF EXISTS ${Config.TABLE_NAME}")
                database.execSQL(
                    "CREATE TABLE IF NOT EXISTS ${Config.TABLE_NAME} " +
                            "(" +
                            "${Config.COLUMN_ID} TEXT NOT NULL," +
                            "${Config.COLUMN_AMOUNT_PER_TRANSACTION} INTEGER," +
                            "${Config.COLUMN_RECONCILE_TIME} TEXT," +
                            "${Config.COLUMN_KEYS} TEXT," +
                            "${Config.COLUMN_ADDITIONAL_DATA} TEXT," +
                            "PRIMARY KEY(${Config.COLUMN_ID})" +
                            ")"
                )
            }
        }
    }
}