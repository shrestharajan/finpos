plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("android.extensions")
}

android {
    compileSdkVersion(Versions.compileSdkVersion)
    buildToolsVersion(Versions.buildToolVersion)

    defaultConfig {
        minSdkVersion(Versions.minSdkVersion)
        targetSdkVersion(Versions.targetSdkVersion)
        versionCode = CoreWeipass.versionCode
        versionName = CoreWeipass.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

}

dependencies {
    implementation(fileTree("dir" to "libs", "include" to listOf("*.jar")))
    implementation(project(":finposframework:core"))
    implementation(project(":common"))
    implementation(files("libs/emvprocess_P2.07.220A21.jar"))
    implementation(Libraries.kotlinStdLib)
    implementation(Libraries.appCompat)
    implementation(Libraries.kotlinKtx)
    implementation(Libraries.localBroadCastManager)
    implementation("com.google.android.material:material:1.4.0-rc01")

    testImplementation("junit:junit:4.12")
    testImplementation("org.assertj:assertj-core:${Versions.assertjCore}")
    testImplementation("androidx.arch.core:core-testing:${Versions.lifecycleTesting}")
    testImplementation("io.mockk:mockk:${Versions.mockk}")

    androidTestImplementation(AndroidTest.extJunit)
    androidTestImplementation(AndroidTest.espresso)
}
