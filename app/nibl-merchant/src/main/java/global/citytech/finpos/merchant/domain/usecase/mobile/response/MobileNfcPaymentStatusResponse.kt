package global.citytech.finpos.merchant.domain.usecase.mobile.response

import com.google.gson.annotations.SerializedName

/**
 * @author SIDDHARTHA GHIMIRE
 */
data class MobileNfcPaymentStatusResponse(
    @SerializedName("payment_status")
    val paymentStatus: String,
    val message: String?,
    val billingInformation: MobileNfcPaymentBillingInfo?
)
