package global.citytech.finpos.merchant.presentation.staticqr

import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.CountDownTimer
import androidx.lifecycle.MutableLiveData
import global.citytech.common.data.Response
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.QrPaymentRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.QrPayment
import global.citytech.finpos.merchant.domain.model.app.QrStatus
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.app.QrPaymentUseCase
import global.citytech.finpos.merchant.domain.usecase.qr.nqr.NqrGenerateRequest
import global.citytech.finpos.merchant.domain.usecase.qr.nqr.NqrGenerator
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.app.QrPaymentDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.dynamicqr.nqrsale.DynamicNQrActivity
import global.citytech.finpos.merchant.presentation.dynamicqr.receipt.DynamicQrReceiptGenerator
import global.citytech.finpos.merchant.presentation.dynamicqr.receipt.DynamicQrTransactionReceiptPrintHandler
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.presentation.model.qr.QrPaymentStatusRequest
import global.citytech.finpos.merchant.presentation.model.qr.QrPaymentStatusResponse
import global.citytech.finpos.merchant.service.NetworkConnectionReceiver
import global.citytech.finpos.merchant.utils.AppConstant
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.SoundUtils
import global.citytech.finpos.merchant.utils.VoiceMessageUtils
import global.citytech.finposframework.hardware.io.printer.PrinterResponse
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.repositories.TerminalRepository
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion
import global.citytech.finposframework.utility.StringUtils.formatAmountTwoDecimal
import global.citytech.payment.sdk.utils.StringUtils
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal

/**
 * @author sachin
 */
class StaticQrViewModel(val context: Application) : BaseAndroidViewModel(context) {

    val appConfiguration by lazy { MutableLiveData<ConfigurationItem>() }
    val qrData by lazy { MutableLiveData<String>() }
    lateinit var terminalRepository: TerminalRepository
    val printerService = PrinterSourceImpl(context)
    var qrPayment: QrPayment? = null
    val transactionConfirmationLiveData by lazy { MutableLiveData<TransactionConfirmation>() }
    var updateTransactionConfirmation = false
    var nqrGenerateRequest: NqrGenerateRequest? = null
    val noInternetConnection by lazy { MutableLiveData<Boolean>() }
    val qrStatusCode by lazy { MutableLiveData<String>() }
    val qrStatusMessage by lazy { MutableLiveData<String>() }
    val showAlertDialog by lazy { MutableLiveData<Boolean>() }

    private val qrTransactionWaitingTimer =
        object : CountDownTimer(AppConstant.QR_TRANSACTION_WAITING_INTERVAL_MILLIS, 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                enableAlertDialogFlag()
            }
        }

    var localDataUseCase: LocalDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    val qrPaymentUseCase = QrPaymentUseCase(
        QrPaymentRepositoryImpl(
            QrPaymentDataSourceImpl(context)
        )
    )

    var nqrGenerator = NqrGenerator()

    fun getBillingInvoiceNumber(): String {
        val invoiceNumber = qrPaymentUseCase.getBillingInvoiceNumber()
        qrPaymentUseCase.incrementBillingInvoiceNumber()
        return invoiceNumber
    }

    fun getConfiguration(): Boolean {
        this.isLoading.value = true
        return compositeDisposable.add(
            localDataUseCase.getWholeConfiguration()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe({
                    isLoading.value = false
                    appConfiguration.value = it
                    terminalRepository = TerminalRepositoryImpl(it)
                    NetworkConnectionReceiver.networkConfiguration = it
                },
                    {
                        this.isLoading.value = false
                        message.value = it.message
                    })
        )
    }

    fun generateNqr(nqrGenerateRequest: NqrGenerateRequest) {
        this.nqrGenerateRequest = nqrGenerateRequest
        qrData.value = nqrGenerator.generate(nqrGenerateRequest).data
    }

    fun bindService() {
        compositeDisposable.add(
            Single.create(SingleOnSubscribe<Response> {
                val bindIntent = Intent()
                val packageName = "global.citytech.tms"
                val className = "global.citytech.tms.service.PlatformManagerService"
                bindIntent.setClassName(packageName, className)
                context.bindService(
                    bindIntent,
                    NiblMerchant.INSTANCE.getPlatformServiceConnection(),
                    Context.BIND_AUTO_CREATE
                )
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                }, {
                    it.printStackTrace()
                    isLoading.value = false
                    message.value = it.message
                })
        )
    }

    fun checkQrPaymentStatus(nqrGenerateRequest: NqrGenerateRequest, sleepTime: Long) {
        compositeDisposable.add(
            Single.create(SingleOnSubscribe<QrPaymentStatusResponse> {
                qrPaymentStatusSubscribe(it, sleepTime, nqrGenerateRequest)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    qrStatusMessage.value = QrStatus.QR000.message
                    qrStatusCode.value = QrStatus.QR000.code
                    showAlertDialog.value = false
                    noInternetConnection.value = false

                    val isApproved = !StringUtils.isEmpty(it.authorizationCode)
                    updateTransactionConfirmation = false
                    transactionConfirmationLiveData.value = TransactionConfirmation.Builder()
                        .amount(formatAmountTwoDecimal(it.amount))
                        .imageId(if (isApproved) R.drawable.approved else R.drawable.declined)
                        .message(if (isApproved) "Approval Code: ${it.authorizationCode}" else "DECLINED")
                        .title(if (isApproved) "APPROVED" else "DECLINED")
                        .positiveLabel("")
                        .negativeLabel("")
                        .qrImageString("")
                        .transactionStatus("")
                        .build()
                    storeTransactionLog(nqrGenerateRequest, isApproved, it)
                    playSound(isApproved, it.amount)
                }, {
                    it.printStackTrace()
                    when {
                        it.localizedMessage.contains(QrStatus.QR001.code) -> {
                            qrStatusMessage.value = QrStatus.QR001.message
                            qrStatusCode.value = QrStatus.QR001.code
                        }
                        it.localizedMessage.contains(QrStatus.QR002.code) -> {
                            qrStatusMessage.value = QrStatus.QR002.message
                            qrStatusCode.value = QrStatus.QR002.code
                        }
                        it.localizedMessage.contains(QrStatus.QR003.code) -> {
                            qrStatusMessage.value = QrStatus.QR003.message
                            qrStatusCode.value = QrStatus.QR003.code
                        }
                    }
                    noInternetConnection.value = it.localizedMessage?.contains("404")!!
                    restartQrPaymentStatusCheck(nqrGenerateRequest)
                })
        )
    }

    private fun restartQrPaymentStatusCheck(nqrGenerateRequest: NqrGenerateRequest) {
        if (!compositeDisposable.isDisposed)
            checkQrPaymentStatus(nqrGenerateRequest, 2000)
    }

    private fun storeTransactionLog(
        nqrGenerateRequest: NqrGenerateRequest,
        isApproved: Boolean,
        qrPaymentStatusResponse: QrPaymentStatusResponse
    ) {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<QrPayment> {
                val responseDateTimeStamp = qrPaymentStatusResponse.localDateTime
                val transactionStatus = if (isApproved) "APPROVED" else "DECLINED"
                val qrPayment = QrPayment(
                    merchantId = nqrGenerateRequest.merchantCode,
                    terminalId = nqrGenerateRequest.terminalId,
                    referenceNumber = qrPaymentStatusResponse.rrn,
                    invoiceNumber = "",
                    transactionType = "SALE",
                    transactionDate = responseDateTimeStamp.substring(0, 10),
                    transactionTime = responseDateTimeStamp.substring(10),
                    transactionCurrency = nqrGenerateRequest.transactionCurrency,
                    transactionAmount = BigDecimal(qrPaymentStatusResponse.amount),
                    transactionStatus = transactionStatus,
                    approvalCode = qrPaymentStatusResponse.authorizationCode,
                    paymentInitiator = qrPaymentStatusResponse.network,
                    initiatorId = qrPaymentStatusResponse.payerPAN
                )
                qrPaymentUseCase.addQrPayment(qrPayment)
                it.onNext(qrPayment)
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    qrPayment = it
                    printPaymentReceipt(ReceiptVersion.MERCHANT_COPY)
                }, {
                    it.printStackTrace()
                })
        )
    }

    fun printPaymentReceipt(receiptVersion: ReceiptVersion) {
        if (receiptVersion == ReceiptVersion.CUSTOMER_COPY)
            isLoading.value = true
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<PrinterResponse> {
                val dynamicReceipt =
                    DynamicQrReceiptGenerator(terminalRepository).generate(
                        qrPayment!!
                    )
                val printerResponse =
                    DynamicQrTransactionReceiptPrintHandler(printerService).printReceipt(
                        dynamicReceipt,
                        receiptVersion
                    )
                it.onNext(printerResponse)
                it.onComplete()
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    updateTransactionConfirmation = true
                    if (receiptVersion == ReceiptVersion.MERCHANT_COPY) {
                        val transactionConfirmation = transactionConfirmationLiveData.value
                        transactionConfirmation!!.positiveLabel = "PRINT CUSTOMER COPY"
                        transactionConfirmation.negativeLabel = "RETURN TO DASHBOARD"
                        transactionConfirmationLiveData.value = transactionConfirmation
                    } else {
                        message.value = it.message
                    }
                }, {
                    isLoading.value = false
                    it.printStackTrace()
                })
        )
    }

    fun dispose() {
        if (compositeDisposable != null) {
            compositeDisposable.clear()
            compositeDisposable.dispose()
        }
    }

    private fun clearCompose() {
        if (compositeDisposable != null) {
            compositeDisposable.clear()
        }
    }

    private fun qrPaymentStatusSubscribe(
        it: SingleEmitter<QrPaymentStatusResponse>,
        sleepTime: Long,
        nqrGenerateRequest: NqrGenerateRequest
    ) {
        Thread.sleep(sleepTime)
        val qrPaymentStatusRequest = QrPaymentStatusRequest(
            nqrGenerateRequest.merchantCode,
            nqrGenerateRequest.terminalId,
            nqrGenerateRequest.billNumber
        )
        val jsonRequest = Jsons.toJsonObj(qrPaymentStatusRequest)
        val jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.post(
            "/device-payment-processor-endpoints/reports/status/",
            jsonRequest,
            true
        )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        when {
            response.data != null -> {
                val responseDataJson = Jsons.toJsonObj(response.data)
                it.onSuccess(
                    Jsons.fromJsonToObj(
                        responseDataJson,
                        QrPaymentStatusResponse::class.java
                    )
                )
            }
            response.message == "Please check your internet connection and try again." -> {
                it.onError(Exception("404 ${response.message}"))
            }
            response.code == QrStatus.QR001.code -> {
                it.onError(Exception("${response.code}. ${response.message}"))
            }
            response.code == QrStatus.QR002.code -> {
                it.onError(Exception("${response.code}. ${response.message}"))
            }
            response.code == QrStatus.QR003.code -> {
                it.onError(Exception("${response.code}. ${response.message}"))
            }
            else -> {
                it.onError(Exception("Error Code :: ${response.code}. ${response.message}"))
            }
        }
    }

    fun startTimer() {
        qrTransactionWaitingTimer.start()
    }

    fun stopTimer() {
        qrTransactionWaitingTimer.cancel()
    }

    private fun enableAlertDialogFlag() {
        stopTimer()
        showAlertDialog.value = true
    }

    fun disableAlertDialogFlag() {
        showAlertDialog.value = false
        /*clearCompose()
        checkQrPaymentStatus(
            nqrGenerateRequest!!,
            2000
        )*/
    }

    private fun playSound(isApproved: Boolean, amount: Double) {
        if (isApproved){
            /** APPROVED */
            if (localDataUseCase.isSoundMode(false))
                SoundUtils.play(getApplication<Application?>().applicationContext, amount,"blank", AppUtility.getCurrencyCode()
                )
            Logger.getLogger(StaticQrActivity::class.simpleName)
                .debug("transaction approved")
        }else{
            /** DECLINED */
            if (localDataUseCase.isSoundMode(false))
                VoiceMessageUtils.playFail(getApplication<Application?>().applicationContext)
            Logger.getLogger(StaticQrActivity::class.simpleName)
                .debug("transaction declined")
        }
    }
}