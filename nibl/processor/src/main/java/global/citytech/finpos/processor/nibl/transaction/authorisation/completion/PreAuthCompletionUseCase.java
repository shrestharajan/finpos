package global.citytech.finpos.processor.nibl.transaction.authorisation.completion;

import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.authcompletion.AuthorisationCompletionRequestSender;
import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finpos.processor.nibl.transaction.template.CardLessTransactionUseCase;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;

/**
 * Created by Unique Shakya on 3/10/2021.
 */
public class PreAuthCompletionUseCase extends CardLessTransactionUseCase {

    public PreAuthCompletionUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }


    @Override
    protected String retrieveInvoiceNumber() {
        return this.terminalRepository.getInvoiceNumber();
    }

    @Override
    protected TransactionLog retrieveTransactionLogToStore(boolean isApproved, IsoMessageResponse isoMessageResponse) {
        return TransactionLog.Builder.newInstance()
                .withStan(stan)
                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
                .withInvoiceNumber(invoiceNumber)
                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
                .withAuthCode(IsoMessageUtils.retrieveFromRequestDataElementAsString(
                        isoMessageResponse,
                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
                .withTransactionType(TransactionType.AUTH_COMPLETION)
                .withOriginalTransactionType(TransactionType.PRE_AUTH)
                .withTransactionAmount(transactionRequest.getAmount())
                .withOriginalTransactionAmount(transactionRequest.getAmount())
                .withTransactionDate(retrieveTransactionDate(isoMessageResponse))
                .withTransactionTime(retrieveTransactionTime(isoMessageResponse))
                .withTransactionStatus(isApproved ? APPROVED : DECLINED)
                .withPosEntryMode(this.originalTransactionLog.getPosEntryMode())
                .withOriginalPosEntryMode(this.originalTransactionLog.getPosEntryMode())
                .withPosConditionCode(this.transactionIsoRequest.getPosConditionCode())
                .withOriginalPosConditionCode(this.originalTransactionLog.getPosConditionCode())
                .withReconcileStatus("")
                .withReconcileTime("")
                .withReconcileDate("")
                .withReconcileBatchNo(batchNumber)
                .withReadCardResponse(readCardResponse)
                .withAuthorizationCompleted(true)
                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                .withOriginalTransactionReferenceNumber(originalTransactionLog.getAuthCode())
                .withReceiptLog(this.receiptLog)
                .withTransactionVoided(false)
                .withTipAdjusted(false)
                .withCurrencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .withVatInfo(originalTransactionLog.getVatInfo())
                .build();
    }

    @Override
    protected TransactionLog retrieveOriginalTransactionLog() {
        String invoiceNumber = this.request.getTransactionRequest().getOriginalInvoiceNumber();
        return this.request.getTransactionRepository().getTransactionLogWithInvoiceNumberWithoutBatchNumber(invoiceNumber);
    }

    @Override
    protected void validateTransactionLog() {
        if (this.originalTransactionLog.getTransactionType() != TransactionType.PRE_AUTH)
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
        if (!this.originalTransactionLog.getTransactionStatus().equalsIgnoreCase(APPROVED))
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_FOUND);
    }

    @Override
    protected void setTotalTransactionAmount() {
        this.totalTransactionAmount = this.request.getTransactionRequest().getAmount();
    }

    @Override
    protected String getTransactionTypeClassName() {
        return PreAuthCompletionUseCase.class.getSimpleName();
    }

    @Override
    protected TransactionIsoRequest prepareTransactionIsoRequest() {
        return TransactionIsoRequest.Builder
                .newInstance()
                .transactionAmount(this.transactionRequest.getAmount())
                .posEntryMode(PosEntryMode.MANUAL)
                .posConditionCode("05")
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .originalRetrievalReferenceNumber(this.originalTransactionLog.getRrn())
                .originalResponseCode("00")
                .originalInvoiceNumber(this.originalTransactionLog.getInvoiceNumber())
                .originalApprovalCode(this.originalTransactionLog.getAuthCode())
                .localTime(HelperUtils.getDefaultLocaleTimeHhMmSs())
                .localDate(HelperUtils.getDefaultLocaleDateWithoutYear())
                .expireDate(readCardResponse.getCardDetails().getExpiryDate())
                .build();
    }

    @Override
    protected NIBLMessageSenderTemplate getRequestSender(RequestContext context) {
        return new AuthorisationCompletionRequestSender(new NIBLSpecInfo(), context);
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.PRE_AUTHORIZATION_COMPLETION.getCode();
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse,
                                                                  boolean isApproved,
                                                                  boolean shouldPrintCustomerCopy, String miniStatementData) {
        return null;
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse,
                                                                  boolean isApproved,
                                                                  boolean shouldPrintCustomerCopy) {
        return AuthorisationCompletionResponseModel.Builder.newInstance()
                .withIsApproved(isApproved)
                .withDebugResponseMessage(isoMessageResponse.getDebugResponseString())
                .withDebugRequestMessage(isoMessageResponse.getDebugRequestString())
                .withMessage(this.retrieveMessage(isoMessageResponse, isApproved))
                .withShouldPrintCustomerCopy(shouldPrintCustomerCopy)
                .withStan(this.stan)
                .build();
    }

    @Override
    protected TransactionResponseModel prepareDeclinedTransactionResponse(String message, boolean shouldPrintReceipt) {
        return AuthorisationCompletionResponseModel.Builder.newInstance()
                .withMessage(message)
                .withIsApproved(false)
                .withShouldPrintCustomerCopy(shouldPrintReceipt)
                .withStan(this.stan)
                .build();
    }

    @Override
    protected String getOriginalTransactionReferenceNumber() {
        return this.transactionIsoRequest.getOriginalRetrievalReferenceNumber();
    }

    @Override
    protected boolean isAuthorizationCompleted() {
        return true;
    }

    @Override
    protected void updateTransactionLogOfOriginalTransaction(boolean isApproved) {
        this.request.getTransactionRepository().updateAuthorizationCompletedStatusByAuthCode(isApproved, TransactionType.PRE_AUTH,
                transactionIsoRequest.getOriginalApprovalCode());
    }

    @Override
    protected boolean shouldIncrementInvoiceNumber(boolean isApproved) {
        return isApproved;
    }
}
