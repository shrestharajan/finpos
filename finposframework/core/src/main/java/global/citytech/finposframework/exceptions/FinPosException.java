package global.citytech.finposframework.exceptions;

import global.citytech.finposframework.supports.Result;

/** User: Surajchhetry Date: 2/26/20 Time: 5:49 PM */
public class FinPosException extends RuntimeException {
  private final ExceptionType exceptionType;
  private final String detailMessage;
  private final String isoRequest;
  private final String isoResponse;

  public FinPosException(ExceptionType exception) {
    super(exception.getMessage());
    this.exceptionType = exception;
    this.detailMessage = "";
    this.isoRequest = "";
    this.isoResponse = "";

  }

  public FinPosException(ExceptionType exception, String detailMessage) {
    super(exception.getMessage());
    this.exceptionType = exception;
    this.detailMessage = detailMessage;
    this.isoRequest = "";
    this.isoResponse = "";
  }

  public FinPosException(ExceptionType exception, String detailMessage, String isoRequest, String isoResponse) {
    super(exception.getMessage());
    this.exceptionType = exception;
    this.detailMessage = detailMessage;
    this.isoRequest = isoRequest;
    this.isoResponse = isoResponse;
  }

  public ExceptionType getType() {
    return exceptionType;
  }

  public String getDetailMessage() {
    return detailMessage;
  }

  public String getIsoRequest() {
    return isoRequest;
  }

  public String getIsoResponse() {
    return isoResponse;
  }

  public enum ExceptionType implements Result.ErrorInfo {
    // General Error
    INVALID_PARAMETER_SUPPLIED("GE-000001", "Invalid parameter supplied."),
    SYSTEM_ERROR("GE-000002", "System error"),
    // IO related error
    UNABLE_TO_CONNECT("IO-000001", "Unable to connect with host."),
    READ_TIME_OUT("IO-000002", "Read time out."),
    WRITE_EXCEPTION("IO-000004", "Write Exception"),
    CONNECTION_ERROR("IO-000003", "Unable to establish socket connection."),
    RESPONSE_IS_BLANK("MS-000001", "Response is blank.");

    private String code;
    private String message;

    ExceptionType(String code, String message) {
      this.code = code;
      this.message = message;
    }

    public String getCode() {
      return code;
    }

    public String getMessage() {
      return message;
    }
  }
}
