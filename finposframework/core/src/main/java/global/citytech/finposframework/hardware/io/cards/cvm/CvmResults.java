package global.citytech.finposframework.hardware.io.cards.cvm;

public class CvmResults {

    private Cvm cvm;
    private CvmCondition cvmCondition;
    private CvmResult cvmResult;

    public CvmResults(Cvm cvm, CvmCondition cvmCondition, CvmResult cvmResult) {
        this.cvm = cvm;
        this.cvmCondition = cvmCondition;
        this.cvmResult = cvmResult;
    }

    public Cvm getCvm() {
        return cvm;
    }

    public CvmCondition getCvmCondition() {
        return cvmCondition;
    }

    public CvmResult getCvmResult() {
        return cvmResult;
    }
}
