package global.citytech.finpos.merchant.data.repository.core.receipt

import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.data.datasource.core.receipt.DuplicateReceiptDataSource
import global.citytech.finpos.merchant.domain.model.receipt.duplicate.DuplicateReceiptResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.receipt.DuplicateReceiptRepository
import global.citytech.finpos.processor.nibl.receipt.duplicatereceipt.DuplicateReceiptResponseModel
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 12/15/2020.
 */
class DuplicateReceiptRepositoryImpl(private val duplicateReceiptDataSource: DuplicateReceiptDataSource) :
    DuplicateReceiptRepository {

    override fun print(stan: String?): Observable<DuplicateReceiptResponseEntity> {
        if (stan.isNullOrEmptyOrBlank())
            return duplicateReceiptDataSource.print()
        return duplicateReceiptDataSource.print(stan)
    }
}