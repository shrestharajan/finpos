package global.citytech.service.app;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import global.citytech.common.IPosServiceInterface;
import global.citytech.crypto.PosCryptoApi;
import global.citytech.crypto.PosCryptoService;

/**
 * Created by Rishav Chudal on 6/16/21.
 */
public class PosServiceBinder extends Service {

  private PosCryptoService posCryptoService;

  @Override public void onCreate() {
    super.onCreate();
    posCryptoService = new PosCryptoApi();
  }

  @Nullable
  @Override public IBinder onBind(Intent intent) {
    return posServiceBinder;
  }

  private final IPosServiceInterface.Stub posServiceBinder = new IPosServiceInterface.Stub() {

    @Override public String getDecryptedData(String publicKey, String encryptedData) {
      return posCryptoService.decrypt(publicKey, encryptedData);
    }

    @Override public String getEncryptedData(String privateKey, String plainData) {
      return posCryptoService.encrypt(privateKey, plainData);
    }
  };
}
