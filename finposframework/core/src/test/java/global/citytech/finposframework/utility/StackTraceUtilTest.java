package global.citytech.finposframework.utility;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class StackTraceUtilTest {

  @Test
  public void getStackTrace() {
    String hello = StackTraceUtil.getStackTraceAsString(new IllegalArgumentException("Hello"));
    assertTrue(hello.contains("Hello"));
  }
}
