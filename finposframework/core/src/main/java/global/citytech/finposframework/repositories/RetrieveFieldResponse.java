package global.citytech.finposframework.repositories;

public class RetrieveFieldResponse {

    private String data;

    public RetrieveFieldResponse(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }
}
