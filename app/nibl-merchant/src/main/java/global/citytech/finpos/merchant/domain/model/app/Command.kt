package global.citytech.finpos.merchant.domain.model.app

import java.lang.IllegalArgumentException

/**
 * Created by Unique Shakya on 9/20/2021.
 */

data class Command(
    var id: String,
    var commandType: CommandType,
    var status: Status,
    var remarks: String,
    var timeStamp: String
) {
    override fun equals(other: Any?): Boolean {
        return this.id == (other as Command).id
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}

enum class Status {
    OPEN, ORDER_RECEIVED, SUCCESS, FAILED
}

enum class CommandType {
    SWITCH_CONFIG_UPDATE,
    KEY_EXCHANGE,
    BANNER_UPDATE,
    SYNC_SETTINGS,
    TRANSACTION_LOG_PUSH,
    SETTLEMENT,
    UNKNOWN
}

fun String.getCommandType(): CommandType {
    return when(this) {
        CommandType.SWITCH_CONFIG_UPDATE.name -> CommandType.SWITCH_CONFIG_UPDATE
        CommandType.KEY_EXCHANGE.name -> CommandType.KEY_EXCHANGE
        CommandType.BANNER_UPDATE.name -> CommandType.BANNER_UPDATE
        CommandType.SYNC_SETTINGS.name -> CommandType.SYNC_SETTINGS
        CommandType.TRANSACTION_LOG_PUSH.name -> CommandType.TRANSACTION_LOG_PUSH
        CommandType.SETTLEMENT.name -> CommandType.SETTLEMENT
        else -> CommandType.UNKNOWN
    }
}