package global.citytech.finposframework.listeners;

import java.io.Serializable;

public interface CardConfirmationListenerForGreenPin extends Serializable {
    void onResult(boolean confirmed);
}
