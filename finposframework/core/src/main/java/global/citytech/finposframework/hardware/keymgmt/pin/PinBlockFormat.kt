package global.citytech.finposframework.hardware.keymgmt.pin

/**
 * Created by Rishav Chudal on 8/20/20.
 */
enum class PinBlockFormat {
    ISO9564_FORMAT_0,
    ISO9564_FORMAT_1,
    ISO9564_FORMAT_2,
    ISO9564_FORMAT_3,
    ISO9564_FORMAT_4
}