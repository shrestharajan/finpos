package global.citytech.finposframework.usecases.transaction;

import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;

public interface TransactionValidator {
    void validateRequest(TransactionRequest transactionRequest);

    boolean isValidTransaction(TransactionRequest transactionRequest,
                               ReadCardResponse readCardResponse,
                               String cardScheme);

    boolean isTransactionDeclineByCardEvenIfSuccessOnSwitch(IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse);
}
