package global.citytech.finposframework.switches.receipt.ministatement;

import global.citytech.finposframework.switches.Request;
import global.citytech.finposframework.switches.receipt.customercopy.CustomerCopyRequestParameter;
import global.citytech.finposframework.switches.receipt.customercopy.CustomerCopyResponseParameter;

public interface StatementRequester<T extends CustomerCopyRequestParameter,
        E extends CustomerCopyResponseParameter>
        extends Request<T, E> {
}