package global.citytech.finpos.merchant.presentation.pinchange

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.finpos.merchant.data.repository.core.pinchange.PinChangeRepositoryImpl
import global.citytech.finpos.merchant.domain.usecase.core.CorePinChangeUseCase
import global.citytech.finpos.merchant.framework.datasource.core.pinchange.PinChangeDataSourceImpl
import global.citytech.finpos.merchant.presentation.greenpin.GreenPinBaseTransactionViewModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.pinchange.PinChangeRequestItem
import global.citytech.finposframework.usecases.TransactionType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PinChangeViewModel(val instance: Application) : GreenPinBaseTransactionViewModel(instance) {

    val pinChangeResult by lazy { MutableLiveData<Boolean>() }

    private var pinChangeUseCase: CorePinChangeUseCase =
        CorePinChangeUseCase(PinChangeRepositoryImpl(PinChangeDataSourceImpl()))

    fun pinChangeRequest(configurationItem: ConfigurationItem, transactionType: TransactionType) {
        compositeDisposable.add(
            pinChangeUseCase.pinChangeRequest(
                configurationItem,
                PinChangeRequestItem(
                    transactionType = transactionType,
                    amount = "0.00".toBigDecimal()
                )
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    if (!shouldDispose) {
                        pinChangeResult.value = it.isApproved
                        if (pinChangeResult.value!!) {
                            successMessage.value = it.message
                        } else {
                            failureMessage.value = it.message
                        }

                    } else {
                        completePreviousTask()
                    }

                }, {
                    isLoading.value= false
                    if (!shouldDispose) {
                        message.value = it.message
                    } else {
                        completePreviousTask()
                    }

                })

        )
    }


}