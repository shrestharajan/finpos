package global.citytech.finpos.merchant.presentation.cashin

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import global.citytech.common.Base64
import global.citytech.common.Constants
import global.citytech.easydroid.core.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityCashInBinding
import global.citytech.finpos.merchant.presentation.alertdialogs.CashInSuccessDialog
import global.citytech.finpos.merchant.presentation.alertdialogs.PosEntryMode
import global.citytech.finpos.merchant.presentation.amount.newlayout.NewAmountActivity
import global.citytech.finpos.merchant.presentation.amount.oldlayout.AmountActivity
import global.citytech.finpos.merchant.presentation.card.check.CardCheckActivity
import global.citytech.finpos.merchant.presentation.greenpin.GreenPinBaseTransactionActivity
import global.citytech.finpos.merchant.presentation.greenpin.GreenPinBaseTransactionViewModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finposframework.usecases.TransactionType
import kotlinx.android.synthetic.main.activity_green_pin.*
import kotlinx.android.synthetic.main.ministatement_list.*

class CashInActivity : GreenPinBaseTransactionActivity<ActivityCashInBinding, CashInViewModel>(),
View.OnClickListener, CashInSuccessDialog.CashInListener{

    private lateinit var viewModel: CashInViewModel
    private lateinit var configurationItem: ConfigurationItem
    private var bankLogo: String = ""
    private lateinit var cashInSuccessDialog: CashInSuccessDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel()

        val intent = Intent(this, AmountActivity::class.java)
        startActivityForResult(intent, AmountActivity.REQUEST_CODE)

        initBaseViews()
        initObservers()
        showPosEntrySelectionDialog()

        viewModel.cashInResult.observe(this,{
            if(it){
                showCashInSuccessAlertDialog(displayAmount.toString())
            }

        })
        observerBankDisplayImage()
    }


    private fun showPosEntrySelectionDialog() {
        Glide.with(this).load(PosEntryMode.ACCEPT_ALL.loaderImage).into(image)
    }

    private fun initObservers() {
        this.viewModel.configurationItem.observe(this, Observer {
            this.configurationItem = it
            this.viewModel.generateCashInRequest(configurationItem, getTransactionType(),displayAmount.toString())
        })
    }

    override fun getBindingVariable(): Int = BR.cashInViewModel

    override fun getLayout(): Int = R.layout.activity_cash_in

    override fun getViewModel(): CashInViewModel =
        ViewModelProviders.of(this)[CashInViewModel::class.java]

    override fun onManualButtonClicked() {
        TODO("Not yet implemented")
    }

    override fun onCancelButtonClicked() {
        this.viewModel.shouldDispose = true
        this.viewModel.doCleanUp(GreenPinBaseTransactionViewModel.Task.RETURN_TO_DASHBOARD)
    }

    override fun getGreenPinBaseTransactionViewModel(): GreenPinBaseTransactionViewModel =
        this.viewModel

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                AmountActivity.REQUEST_CODE, NewAmountActivity.REQUEST_CODE -> {
                    data?.let {
                        val amountInString =
                            data.getStringExtra(getString(R.string.intent_confirmed_amount))
                        displayAmount = amountInString!!.toBigDecimal()
                        viewModel.getConfigurationItem()
                    }

                }
            }
        }else if(resultCode== RESULT_CANCELED){
            finish()
        }

        super.onActivityResult(requestCode, resultCode, data)
    }



    override fun onManualActivityResult(data: Intent?) {
        TODO("Not yet implemented")
    }

    override fun getTransactionType(): TransactionType = TransactionType.CASH_IN

    override fun onClick(p0: View?) {
        TODO("Not yet implemented")
    }

    override fun onAmountActivityResult(data: Intent?) {
        TODO("Not yet implemented")
    }

    private fun observerBankDisplayImage(){
        getViewModel().bankDisplayImage.observe(this, {
            this.bankLogo = Base64.encodeToString(it, false)
            Glide.with(this).load(it).into(iv_bank_logo_mini_statement)
        })


        getViewModel().defaultBankDisplayImage.observe(this, Observer {
            if (it)
                iv_bank_logo_mini_statement.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_full_finpos_logo
                    )
                )
        })
    }

    fun showCashInSuccessAlertDialog(amount: String) {
        cashInSuccessDialog = CashInSuccessDialog(amount, this, this)
        cashInSuccessDialog.isCancelable = false
        cashInSuccessDialog.show(
            supportFragmentManager,
            CashInSuccessDialog.TAG
        )
    }

    override fun printCashIn() {
        this.viewModel.printCashInStatement(displayAmount.toString())
    }

}