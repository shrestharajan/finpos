package global.citytech.sendlog.SendLogInterface

import global.citytech.finpos.merchant.presentation.dashboard.temp.LogFileUploadResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface LogPushInterface {
    @Multipart
    @POST
    fun uploadLogFileToServer(
    @Url endPoint: String,
    @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>,
    @Part file: MultipartBody.Part
): Call<LogFileUploadResponse>

}