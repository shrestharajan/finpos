package global.citytech.finpos.processor.neps.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import global.citytech.finpos.processor.neps.NepsConstant;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StringUtils;

public class TransactionUtils {

    public static List<CardType> getAllowedCardEntryModes(TransactionType transactionType) {
        return getAllowedCardEntryModesForPurchase(transactionType);
    }

    private static List<CardType> getAllowedCardEntryModesForPurchase(TransactionType transactionType) {
        List<CardType> cardTypes = new ArrayList<>();
        switch (transactionType) {
            case GREEN_PIN:
            case PIN_CHANGE:
            case CASH_ADVANCE:
                cardTypes.add(CardType.ICC);
                break;
            default:
                cardTypes.add(CardType.MAG);
                cardTypes.add(CardType.ICC);
                cardTypes.add(CardType.PICC);
                break;
        }
        return cardTypes;
    }


    static List<CardType> getCardTypes() {
        List<CardType> cardTypes = new ArrayList<>();
        cardTypes.add(CardType.MAG);
        cardTypes.add(CardType.ICC);
        cardTypes.add(CardType.PICC);
        return cardTypes;
    }


    public static String retrieveTransactionType9C(TransactionType transactionType) {
        switch (transactionType) {
            case PRE_AUTH:
                return global.citytech.finpos.processor.neps.transaction.TransactionType
                        .PRE_AUTH
                        .getProcessingCode()
                        .substring(0, 2);

            case REFUND:
                return global.citytech.finpos.processor.neps.transaction.TransactionType
                        .REFUND
                        .getProcessingCode()
                        .substring(0, 2);

            case VOID:
                return global.citytech.finpos.processor.neps.transaction.TransactionType
                        .VOID
                        .getProcessingCode()
                        .substring(0, 2);
            case AUTH_COMPLETION:
                return global.citytech.finpos.processor.neps.transaction.TransactionType
                        .PRE_AUTH_COMPLETION
                        .getProcessingCode()
                        .substring(0, 2);

            case GREEN_PIN:
                return global.citytech.finpos.processor.neps.transaction.TransactionType
                        .GREEN_PIN
                        .getProcessingCode()
                        .substring(0, 2);

            case PIN_CHANGE:
                return global.citytech.finpos.processor.neps.transaction.TransactionType
                        .PIN_CHANGE
                        .getProcessingCode()
                        .substring(0, 2);

            case PURCHASE:
            default:
                return global.citytech.finpos.processor.neps.transaction.TransactionType
                        .PURCHASE
                        .getProcessingCode()
                        .substring(0, 2);
        }
    }

    public static global.citytech.finpos.processor.neps.transaction.TransactionType mapPurchaseTypeWithTransactionType(TransactionType transactionType) {
        if (transactionType == null)
            return null;

        switch (transactionType) {
            case PURCHASE:
                return global.citytech.finpos.processor.neps.transaction.TransactionType.PURCHASE;
            case PRE_AUTH:
                return global.citytech.finpos.processor.neps.transaction.TransactionType.PRE_AUTH;
            case VOID:
                return global.citytech.finpos.processor.neps.transaction.TransactionType.VOID;
            case REFUND:
                return global.citytech.finpos.processor.neps.transaction.TransactionType.REFUND;
            case AUTH_COMPLETION:
                return global.citytech.finpos.processor.neps.transaction.TransactionType.PRE_AUTH_COMPLETION;
            case CASH_ADVANCE:
                return global.citytech.finpos.processor.neps.transaction.TransactionType.CASH_ADVANCE;
            case CASH_VOID:
                return global.citytech.finpos.processor.neps.transaction.TransactionType.VOID;
            case GREEN_PIN:
                return global.citytech.finpos.processor.neps.transaction.TransactionType.GREEN_PIN;
            case PIN_CHANGE:
                return global.citytech.finpos.processor.neps.transaction.TransactionType.PIN_CHANGE;
            default:
                return null;
        }
    }

    public static String retrievePinPadMessageWithAmount(BigDecimal amount) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(NepsConstant.PIN_PAD_CURRENCY);
        stringBuilder.append("\t");
        stringBuilder.append(StringUtils.formatAmountTwoDecimal(amount));
        return stringBuilder.toString();
    }

    public static String getDateOrDefaultFromTimeStamp(String timeStamp) {
        if (timeStamp.isEmpty() || timeStamp.length() < 12) {
            timeStamp = DateUtils.yyMMddHHmmssDate();
        }
        return timeStamp.substring(0, 6);
    }

    public static String getTimeOrDefaultFromTimeStamp(String timeStamp) {
        if (timeStamp.isEmpty() || timeStamp.length() < 12) {
            timeStamp = DateUtils.yyMMddHHmmssDate();
        }
        return timeStamp.substring(6, 12);
    }

    public static String getTimeStampForReconciliationWithIsoResponseDate(IsoMessageResponse isoMessageResponse) {
        String defaultTimeStamp = DateUtils.yyyyMMddHHmmssDate();
        String time = DateUtils.HHmmssTime();
        if (isoMessageResponse == null) {
            return defaultTimeStamp;
        }
        String isoResponseDate = IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.SETTLEMENT_DATE);
        if (isoResponseDate.isEmpty()) {
            return defaultTimeStamp;
        }
        return isoResponseDate.concat(time);
    }
}
