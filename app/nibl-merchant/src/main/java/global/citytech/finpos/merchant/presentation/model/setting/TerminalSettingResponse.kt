package global.citytech.finpos.merchant.presentation.model.setting

import global.citytech.finpos.merchant.domain.model.app.Bin
import global.citytech.finpos.merchant.domain.model.app.Disclaimer


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/11/19 - 11:04 AM
*/
data class TerminalSettingResponse(
    val enableActivityLog: Boolean,
    val enableDebugLog: Boolean,
    val enableEmiScheme: Boolean,
    var baseUrl: String? = null,
    val bins: List<Bin>? = null,
    var qrCode: String? = null,
    var showQrDisclaimer: Boolean = true,
    var qrConfigs: List<QrConfiguration>?,
    var disclaimers: List<Disclaimer>?,
    var receiptConfiguration: ReceiptConfiguration?,
    var settlementTime: String? = null,
    var pushNotificationConfiguration: PushNotificationConfiguration? = null,
    var batchNumber: String? = null,
    var qrBillNumber: String? = null,
    var nfcBillNumber: String? = null,
    val enableAutoSettlement: Boolean,
    val enableNfcTapAndPay: Boolean,
    val enableGreenPin : Boolean,
    val isSoundEnabled: Boolean,
)