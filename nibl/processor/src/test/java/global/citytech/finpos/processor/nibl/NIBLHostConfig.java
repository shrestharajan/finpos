package global.citytech.finpos.processor.nibl;

import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;

/**
 * User: Surajchhetry Date: 2/25/20 Time: 9:19 AM
 */
public class NIBLHostConfig {

    /*
     Primary Connection
     ==================
    ip- 182.93.95.151
    port-9999

     Secondary Connection
     ===================
    ip- 202.166.206.197
    port-7777

     */
    public static HostInfo getPrimaryConfig() {
        return HostInfo.Builder.createDefaultBuilder("202.63.245.73", 7780)
                .timeOut(3)
                .retryLimit(1)
                .tpduString(getTPDUString())
                .addEventListener(
                        (event, context) -> {
                            Logger.getLogger(NIBLHostConfig.class.getName()).log(context.get("MSG").toString());
                        })
                .build();
    }

    public static HostInfo getSecondaryConfig() {
        return HostInfo.Builder.createDefaultBuilder("116.90.236.134", 7780)
                .timeOut(2)
                .retryLimit(2)
                .tpduString(getTPDUString())
                .addEventListener(
                        (event, context) -> {
                            Logger.getLogger(NIBLHostConfig.class.getName()).log(context.get("MSG").toString());
                        })
                .build();
    }

    public static String getTPDUString() {
        return "00110000";
    }

    public static TerminalInfo getTerminalInfo(){
        String terminalID = "99991002";
        String axquirerID = "405635000000002";
        TerminalInfo terminalInfo = new TerminalInfo(terminalID, axquirerID,"Citytech","Kamaladi");
        return terminalInfo;
    }


}
