package global.citytech.finpos.merchant.data.datasource.app.db.dao

import androidx.room.*
import global.citytech.finpos.merchant.domain.model.app.CardScheme

@Dao
interface CardSchemesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(cardSchemes: List<CardScheme>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(cardScheme: CardScheme): Long

    @Query("SELECT id, card_scheme_id, attribute, display_label, print_label, value FROM card_schemes")
    fun getCardScheme(): List<CardScheme>

    @Query("SELECT value FROM card_schemes where card_scheme_id = :cardSchemeType AND attribute = :attribute")
    fun getByCardSchemeTypeAndAttribute(
        cardSchemeType: String,
        attribute: String
    ): String?

    @Update
    fun update(cardScheme: CardScheme):Int

    @Query("DELETE FROM card_schemes WHERE id = :id")
    fun deleteById(id: Long): Int

    @Query("DELETE FROM card_schemes")
    fun nukeTableData()
}