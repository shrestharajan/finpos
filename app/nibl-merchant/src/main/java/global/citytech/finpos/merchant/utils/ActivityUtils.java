package global.citytech.finpos.merchant.utils;


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/12/07 - 11:55 AM
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class ActivityUtils {

    public static void showActivity(Context context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
    }

    public static void showActivity(Context context, Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(context, cls);
        intent.putExtra(AppConstant.APP_NAME, bundle);
        context.startActivity(intent);
    }

    public static void showActivity(Context context, Class<?> cls, String action) {
        Intent intent = new Intent(context, cls);
        intent.setAction(action);
        context.startActivity(intent);
    }

    public static void showNewTaskActivity(Context context, Class<?> cls, String action) {
        Intent intent = new Intent(context, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(action);
        context.startActivity(intent);
    }

    public static void setResultAndFinish(Context context, int resultCode, Intent intent) {
        ((Activity) context).setResult(resultCode, intent);
        ((Activity) context).finish();
    }

    public static void startAnotherActivityForResult(Context context, Class<?> cls, int requestCode){
        Activity activity = (Activity) context;
        Intent intent = new Intent(context, cls);
        activity.startActivityForResult(intent, requestCode);
    }
}
