package global.citytech.finpos.processor.neps.receipt.summmaryreport;

import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;

/**
 * Created by Saurav Ghimire on 3/30/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public class SummaryReportReceipt {
    private Retailer retailer;
    private Performance performance;
    private String reconciliationBatchNumber;
    private String host;
    private SummaryReport summaryReport;

    public Retailer getRetailer() {
        return retailer;
    }

    public Performance getPerformance() {
        return performance;
    }

    public String getReconciliationBatchNumber() {
        return reconciliationBatchNumber;
    }

    public String getHost() {
        return host;
    }

    public SummaryReport getSummaryReport() {
        return summaryReport;
    }

    public static final class Builder {
        private Retailer retailer;
        private Performance performance;
        private String reconciliationBatchNumber;
        private String host;
        private SummaryReport summaryReport;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withRetailer(Retailer retailer) {
            this.retailer = retailer;
            return this;
        }

        public Builder withPerformance(Performance performance) {
            this.performance = performance;
            return this;
        }

        public Builder withReconciliationBatchNumber(String reconciliationBatchNumber) {
            this.reconciliationBatchNumber = reconciliationBatchNumber;
            return this;
        }

        public Builder withHost(String host) {
            this.host = host;
            return this;
        }

        public Builder withSummaryReport(SummaryReport summaryReport) {
            this.summaryReport = summaryReport;
            return this;
        }

        public SummaryReportReceipt build() {
            SummaryReportReceipt summaryReportReceipt = new SummaryReportReceipt();
            summaryReportReceipt.retailer = this.retailer;
            summaryReportReceipt.performance = this.performance;
            summaryReportReceipt.reconciliationBatchNumber = this.reconciliationBatchNumber;
            summaryReportReceipt.host = this.host;
            summaryReportReceipt.summaryReport = this.summaryReport;
            return summaryReportReceipt;
        }
    }
}
