package global.citytech.finpos.processor.nibl.receipt.summaryreport;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.List;

import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.utility.JsonUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

/**
 * Created by Unique Shakya on 1/5/2021.
 */
@RunWith(PowerMockRunner.class)
public class SummaryReportReceiptMakerTest {

    private Logger logger = Logger.getLogger(SummaryReportReceiptMaker.class.getSimpleName());
    private TerminalRepository terminalRepository;
    private ReconciliationRepository reconciliationRepository;
    private TerminalInfo terminalInfo;

    @Before
    public void setup() {
        this.terminalRepository = PowerMockito.mock(TerminalRepository.class);
        this.reconciliationRepository = PowerMockito.mock(ReconciliationRepository.class);
        this.terminalInfo = PowerMockito.mock(TerminalInfo.class);
    }

    @Test
    public void prepareTest() {
        PowerMockito.when(this.terminalInfo.getMerchantName()).thenReturn("CityTech");
        PowerMockito.when(this.terminalInfo.getMerchantAddress()).thenReturn("Kamaladi");
        PowerMockito.when(this.terminalInfo.getMerchantID()).thenReturn("123456123456");
        PowerMockito.when(this.terminalInfo.getTerminalID()).thenReturn("1122334455");

        PowerMockito.when(this.terminalRepository.findTerminalInfo()).thenReturn(this.terminalInfo);

        PowerMockito.when(this.reconciliationRepository.getCountByOriginalTransactionTypeWithCardScheme(
                any(), any(), any(), any())).thenReturn((long) 1);
        PowerMockito.when(this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(
                any(), any(), any(), any())).thenReturn((long) 1200);
        PowerMockito.when(this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(
                any(), any(), any())).thenReturn((long) 2500);
        PowerMockito.when(this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(
                any(), any(), any())).thenReturn((long) 20);

        String batchNumber = "0001";
        List<CardSchemeType> cardSchemeTypes = Arrays.asList(CardSchemeType.VISA_CREDIT,
                CardSchemeType.VISA_DEBIT, CardSchemeType.MASTERCARD);

        SummaryReportReceiptMaker reportReceiptMaker = new SummaryReportReceiptMaker(this.terminalRepository,
                this.reconciliationRepository);
        SummaryReportReceipt summaryReportReceipt = reportReceiptMaker.prepare(batchNumber, cardSchemeTypes);

        assertNotNull(summaryReportReceipt);
        this.logger.debug("::: SUMMARY REPORT ::: " + JsonUtils.toJsonObj(summaryReportReceipt));
        assertEquals(batchNumber, summaryReportReceipt.getReconciliationBatchNumber());
        assertEquals(3, summaryReportReceipt.getSummaryReport().getMap().size());
    }
}
