package global.citytech.finpos.device.qualcomm.emv.revokedkeys

import android.content.Context
import com.ftpos.library.smartpos.bean.CCertRevokeListBean
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.emv.IActionFlag
import com.ftpos.library.smartpos.errcode.ErrCode
import global.citytech.common.extensions.defaultOrEmptyValue
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result.FAILURE
import global.citytech.finposframework.hardware.common.Result.SUCCESS
import global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKey
import global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKeysInjectionRequest
import global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKeysService
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_FAILURE_REVOKED_KEYS_ERASE
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_FAILURE_REVOKED_KEYS_INJECTION
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_REVOKED_CERT_INJECT_SUCCESS
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_REVOKED_CERT_LIST_EMPTY
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_SUCCESS_REVOKED_KEYS_ERASE
import global.citytech.finposframework.utility.StringUtils

/**
 * Created by Rishav Chudal on 4/15/21.
 */
class FeitianRevokedKeysServiceImpl(val context: Context): RevokedKeysService, FeitianLogger.Listener {

    private val emv = Emv.getInstance(context.applicationContext)
    private val logger = FeitianLogger(this)
    private lateinit var deviceResponse: DeviceResponse
    private var finalSuccessCount = 0

    override fun injectRevokedKeys(injectionRequest: RevokedKeysInjectionRequest): DeviceResponse {
        this.logger.logWithClassName("***** Injecting Revoked Certificates *****")
        this.deviceResponse = DeviceResponse(
            FAILURE,
            MSG_FAILURE_REVOKED_KEYS_INJECTION
        )
        if (injectionRequest.revokedKeysList.isEmpty()) {
            this.onRevokedCertificatesListEmpty()
        } else {
            this.onRevokedCertificatesListNotEmpty(injectionRequest)
        }
        return this.deviceResponse
    }

    private fun onRevokedCertificatesListEmpty() {
        this.logger.logWithClassName("Revoked Certificates List is Empty...")
        this.deviceResponse = DeviceResponse(
            FAILURE,
            MSG_REVOKED_CERT_LIST_EMPTY
        )
    }

    private fun onRevokedCertificatesListNotEmpty(injectionRequest: RevokedKeysInjectionRequest) {
        this.logger.logWithClassName("Revoked Certificates List is not empty...")
        this.eraseAllRevokedKeys()
        this.proceedForRevokedCertificatesInjection(injectionRequest)
        if (this.finalSuccessCount > 0) {
            this.deviceResponse = DeviceResponse(
                SUCCESS,
                MSG_REVOKED_CERT_INJECT_SUCCESS
            )
        }
    }

    private fun proceedForRevokedCertificatesInjection(
        injectionRequest: RevokedKeysInjectionRequest
    ) {
        for (revokedKey in injectionRequest.revokedKeysList) {
            try {
                this.prepareAndInjectRevokedCertificate(revokedKey)
            } catch (ex: Exception) {
                this.logger.logWithClassName(
                    "Exception in Revoke Certificate ::: "
                        .plus(revokedKey.rid)
                )
                this.logger.logWithClassName("Exception ::: ".plus(ex.message))
            }
            this.logger.logWithClassName(
                "Revoked Certificate Injected ::: "
                    .plus(this.finalSuccessCount)
                    .plus("/")
                    .plus(injectionRequest.revokedKeysList.size)
            )
        }
    }

    private fun prepareAndInjectRevokedCertificate(revokedKey: RevokedKey) {
        this.logger.logWithClassName("*** Revoked Key ***")
        val certRevokeListBean = CCertRevokeListBean()

        val rid: String = revokedKey.rid.defaultOrEmptyValue()
        this.logger.logWithClassName("RID ::: ".plus(rid))
        certRevokeListBean.riD_1F42 = rid

        val keyIndex = revokedKey.index.defaultOrEmptyValue()
        this.logger.logWithClassName("KEY Index ::: ".plus(keyIndex))
        certRevokeListBean.capkIndex_9F22 = keyIndex

        val serialNumber = revokedKey.certificateSerialNumber.defaultOrEmptyValue()
        this.logger.logWithClassName("Certificate Serial Number ::: ".plus(serialNumber))
        certRevokeListBean.certsN_1F43 = serialNumber

        val result = emv.setCRL(
            IActionFlag.ADD,
            certRevokeListBean.toTlvByteArray()
        )
        this.logger.logWithClassName("CRL Injection Result ::: ".plus(result))
        if (result == ErrCode.ERR_SUCCESS) {
            this.finalSuccessCount += 1
        }
    }

    override fun eraseAllRevokedKeys(): DeviceResponse {
        this.logger.logWithClassName("Clearing existing Revoked Certificate List...")
        this.deviceResponse = DeviceResponse(
            SUCCESS,
            MSG_FAILURE_REVOKED_KEYS_ERASE
        )
        val resultCRLErase = this.emv.setCRL(
            IActionFlag.CLEAR,
            null
        )
        this.logger.logWithClassName("Erase Revoked Keys result ::: ".plus(resultCRLErase))
        if (resultCRLErase == ErrCode.ERR_SUCCESS) {
            this.deviceResponse = DeviceResponse(
                SUCCESS,
                MSG_SUCCESS_REVOKED_KEYS_ERASE
            )
        }
        return this.deviceResponse
    }

    override fun getClassName() = FeitianRevokedKeysServiceImpl::class.java.simpleName
}