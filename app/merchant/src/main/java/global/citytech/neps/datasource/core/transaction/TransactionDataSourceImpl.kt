package global.citytech.neps.datasource.core.transaction

import global.citytech.neps.App
import global.citytech.neps.data.datasource.core.transaction.TransactionDataSource
import global.citytech.neps.datasource.core.configuration.TerminalRepositoryImpl
import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.neps.presentation.model.transaction.PurchaseRequestItem
import global.citytech.posswitchintregator.api.SwitchManager
import global.citytech.posswitchintregator.neps.usecases.purchase.PurchaseRequestModel
import global.citytech.posswitchintregator.neps.usecases.purchase.PurchaseResponseModel
import global.citytech.posswitchintregator.transaction.data.TransactionRequest
import io.reactivex.Observable

class TransactionDataSourceImpl : TransactionDataSource {
    override fun purchase(
        configurationItem: ConfigurationItem,
        purchaseRequestItem: PurchaseRequestItem
    ): Observable<PurchaseResponseModel> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepo =
            TerminalRepositoryImpl(configurationItem)
        val transactionRepositoryImpl = TransactionRepositoryImpl(App.INSTANCE)
        val transactionRequest = prepareTransactionRequest(purchaseRequestItem)
        val deviceController = DeviceControllerImpl(App.INSTANCE)
        val cardReader = CardReaderImpl(App.activityContext)
        val transactionAuthenticatorImpl = TransactionAuthenticatorImpl()
        val printerServiceImpl = PrinterServiceImpl(App.activityContext)
        val purchaseRequestModel = PurchaseRequestModel(
            transactionRepositoryImpl,
            transactionRequest,
            cardReader,
            deviceController,
            transactionAuthenticatorImpl,
            printerServiceImpl
        )
        val purchaseRequester =
            SwitchManager.getInterface(terminalRepo)!!.purchaseExchangeRequester
        return Observable.fromCallable {
            (purchaseRequester.execute(purchaseRequestModel)) as PurchaseResponseModel
        }

    }

    override fun cleanUp() {
        val cardReaderImpl = CardReaderImpl(App.activityContext)
        cardReaderImpl.cleanUp()
    }

    private fun prepareTransactionRequest(purchaseRequestItem: PurchaseRequestItem): TransactionRequest {
        val transactionRequest = TransactionRequest(
            purchaseRequestItem.transactionType
        )
        transactionRequest.amount = purchaseRequestItem.amount!!
        return transactionRequest

    }
}