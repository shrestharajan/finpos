package global.citytech.finpos.processor.neps.reconciliation.check;

import global.citytech.finpos.processor.neps.reconciliation.NepsReconciliationHandler;
import global.citytech.finpos.processor.neps.reconciliation.NepsReconciliationTotals;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reconciliation.check.CheckReconciliationRequester;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationHandler;
import global.citytech.finposframework.utility.StringUtils;

public class NepsCheckReconciliationUseCase implements UseCase<CheckReconciliationRequestModel, CheckReconciliationResponseModel>,
        CheckReconciliationRequester<CheckReconciliationRequestModel, CheckReconciliationResponseModel> {

    private static final long TWENTY_FOUR_HOURS = 86400000;

    private final TerminalRepository terminalRepository;

    public NepsCheckReconciliationUseCase(TerminalRepository terminalRepository) {
        this.terminalRepository = terminalRepository;
    }

    @Override
    public CheckReconciliationResponseModel execute(CheckReconciliationRequestModel checkReconciliationRequestModel) {
        long lastSuccessfulSettlementTime = checkReconciliationRequestModel.getReconciliationRepository().getLastSuccessfulSettlementTime();
        long currentTime = System.currentTimeMillis();
        long timeElapsed = currentTime - lastSuccessfulSettlementTime;
        if (timeElapsed > TWENTY_FOUR_HOURS) {
            String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
            if (checkReconciliationRequestModel.getReconciliationRepository().getTransactionCountByBatchNumber(batchNumber) == 0) {
                checkReconciliationRequestModel.getReconciliationRepository().updateLastSuccessfulSettlementTime(currentTime);
                return new CheckReconciliationResponseModel(false);
            }
            ReconciliationHandler reconciliationHandler = new NepsReconciliationHandler(this.terminalRepository,
                    checkReconciliationRequestModel.getReconciliationRepository(), batchNumber,
                    checkReconciliationRequestModel.getApplicationRepository());
            NepsReconciliationTotals reconciliationTotals = (NepsReconciliationTotals) reconciliationHandler.prepareReconciliationTotals();
            if (isZeroAmountSettlement(reconciliationTotals)) {
                checkReconciliationRequestModel.getReconciliationRepository().updateLastSuccessfulSettlementTime(currentTime);
                return new CheckReconciliationResponseModel(false);
            }
            return new CheckReconciliationResponseModel(true);
        }
        return new CheckReconciliationResponseModel(false);
    }

    private boolean isZeroAmountSettlement(NepsReconciliationTotals reconciliationTotals) {
        String totalsBlock = reconciliationTotals.toTotalsBlock();
        String totalReconcileAmount = totalsBlock.substring(1);
        return totalReconcileAmount.isEmpty() || StringUtils.isAllZero(totalReconcileAmount);
    }
}
