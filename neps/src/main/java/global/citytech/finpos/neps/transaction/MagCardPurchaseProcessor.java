package global.citytech.finpos.neps.transaction;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardSummary;
import global.citytech.finposframework.hardware.io.cards.PinBlock;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat;
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest;
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.utility.ServiceCodeUtils;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.CardSchemeRetrieveRequest;
import global.citytech.finposframework.repositories.CardSchemeRetrieveResponse;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.repositories.FieldAttributes;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.utility.CardUtils;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.utility.StringUtils;

public class MagCardPurchaseProcessor implements PurchaseProcessor {

    TransactionRepository transactionRepository;
    TransactionAuthenticator transactionAuthenticator;
    ApplicationRepository applicationRepository;
    TerminalRepository terminalRepository;

    public MagCardPurchaseProcessor(
            TransactionRepository transactionRepository,
            TransactionAuthenticator transactionAuthenticator,
            ApplicationRepository applicationRepository,
            TerminalRepository terminalRepository
    ) {
        this.transactionRepository = transactionRepository;
        this.transactionAuthenticator = transactionAuthenticator;
        this.applicationRepository = applicationRepository;
        this.terminalRepository = terminalRepository;
    }

    @Override
    public ReadCardResponse purchase(String stan, PurchaseRequest purchaseRequest) {
        PinBlock pinBlock = new PinBlock("", true);
        if (this.pinRequired(purchaseRequest.getTransactionType(), purchaseRequest.getCardDetails())) {
            pinBlock = this.retrievePinBlock(purchaseRequest.getCardDetails());
        }
        ReadCardResponse readCardResponse = this.prepareFromPurchaseRequest(pinBlock, purchaseRequest);
        readCardResponse.getCardDetails().setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
        readCardResponse.setFallbackIccToMag(purchaseRequest.isFallbackIccToMag());
        return readCardResponse;
    }

    private ReadCardResponse prepareFromPurchaseRequest(PinBlock pinBlock, PurchaseRequest purchaseRequest) {
        return new ReadCardResponse(purchaseRequest.getCardDetails(), Result.SUCCESS, "Successful");
    }

    private boolean pinRequired(TransactionType transactionType, CardDetails cardDetails) {
//        String shortCardScheme = cardDetails.getCardScheme().getCardSchemeId();
//        if (this.retrieveCardSchemeParameters(CardUtils.retrieveCardSchemeType(shortCardScheme),
//                FieldAttributes.CARD_SCHEME_S1_F20_CHECK_SERVICE_CODE).equals("1")) {
//            return ServiceCodeUtils.isPinRequired(cardDetails.getTrackTwoData());
//        }
//        TransactionSetTableVerifier verifier = new TransactionSetTableVerifier(
//                this.applicationRepository,
//                shortCardScheme,
//                TransactionUtils.mapPurchaseTypeWithTransactionType(transactionType));
//        return verifier.isPinVerificationNecessary();

        return ServiceCodeUtils.isPinRequired(cardDetails.getTrackTwoData());
    }

    private String retrieveCardSchemeParameters(CardSchemeType cardSchemeType, FieldAttributes fieldAttributes) {
        CardSchemeRetrieveRequest cardSchemeRetrieveRequest = new CardSchemeRetrieveRequest(cardSchemeType.getCardSchemeId(), fieldAttributes);
        CardSchemeRetrieveResponse cardSchemeRetrieveResponse
                = this.applicationRepository.retrieveCardScheme(cardSchemeRetrieveRequest);
        return cardSchemeRetrieveResponse.getData();
    }

    private PinBlock retrievePinBlock(CardDetails cardDetails) {
        String pan = cardDetails.getPrimaryAccountNumber();
        if (!StringUtils.isEmpty(pan)) {
            CardSummary cardSummary = new CardSummary(cardDetails.getCardSchemeLabel(),
                    pan,cardDetails.getCardHolderName(),cardDetails.getExpiryDate());
            PinRequest pinRequest = new PinRequest(
                    pan,
                    "Customer Pin Entry",
                    PinBlockFormat.ISO9564_FORMAT_1, //Todo use actual value
                    4, //Todo use actual value
                    12, //Todo use actual value
                    30, //Todo use actual value
                    "",
                    !this.terminalRepository.shouldShufflePinPad()
            );
            pinRequest.setCardSummary(cardSummary);
            PinResponse response = transactionAuthenticator.authenticateUser(pinRequest);
            if (response.getResult() == Result.SUCCESS)
                return response.getPinBlock();
            else if (response.getResult() == Result.USER_CANCELLED)
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            else if (response.getResult() == Result.TIMEOUT)
                throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
            else
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }
}
