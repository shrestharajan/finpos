package global.citytech.finpos.processor.neps.transaction;

import java.math.BigDecimal;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.neps.iso8583.SmartVistaActionCode;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.usecases.TransactionStatusFromHost;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionValidator;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.CardUtils;
import global.citytech.finposframework.utility.StringUtils;

public class NepsTransactionValidator implements TransactionValidator {

    ApplicationRepository applicationRepository;

    public NepsTransactionValidator(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    @Override
    public void validateRequest(TransactionRequest transactionRequest) {
        checkAmount(transactionRequest.getAmount());
    }

    private void checkAmount(BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new PosException(PosError.DEVICE_ERROR_INVALID_AMOUNT);
        }
    }

    @Override
    public boolean isValidTransaction(TransactionRequest transactionRequest,
                                      ReadCardResponse readCardResponse, String cardScheme) {
        return this.isTransactionAllowed(transactionRequest, readCardResponse, cardScheme);
    }

    @Override
    public boolean isTransactionDeclineByCardEvenIfSuccessOnSwitch(IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse) {

        if (readCardResponse.getCardDetails().getCardType() == CardType.MAG ||
                readCardResponse.getCardDetails().getCardType() == CardType.MANUAL)
            return false;
        boolean isApprovedByActionCode = this.isApprovedByActionCode(isoMessageResponse.getMsg().getDataElementByIndex(39).get().getValueAsInt());
        return isApprovedByActionCode && this.declinedByCard(readCardResponse);


    }

    private boolean declinedByCard(ReadCardResponse readCardResponse) {
        if (readCardResponse.getCardDetails().getCardType() == CardType.PICC)
            return false;
        try {
            return readCardResponse.getCardDetails().getTagCollection().get(IccData.CID.getTag()).getData().equals("00");
        } catch (Exception e) {
            Logger.getLogger(e.getLocalizedMessage());
            return false;
        }
    }

    private boolean isApprovedByActionCode(int actionCode) {
        return SmartVistaActionCode.getByActionCode(actionCode).getTransactionStatusFromHost() == TransactionStatusFromHost.ONLINE_APPROVED;
    }

    private boolean isTransactionAllowed(TransactionRequest transactionRequest, ReadCardResponse readCardResponse,
                                         String cardScheme) {
        TransactionType transactionType = transactionRequest.getTransactionType();
        return this.isTransactionAllowedForIccFallbackToMag() &&
                this.isTransactionAllowedByTms(transactionType, cardScheme);
    }

    private boolean isTransactionAllowedByTms(TransactionType transactionType, String cardScheme) {
        if (!StringUtils.isEmpty(cardScheme)) {
            TransactionSetTableVerifier verifier = new TransactionSetTableVerifier(
                    applicationRepository,
                    CardUtils.getShortAidLabel(cardScheme),
                    TransactionUtils.mapPurchaseTypeWithTransactionType(transactionType)
            );
            return verifier.isTransactionAllowed();
        }
        return false;
    }

    private boolean isTransactionAllowedForIccFallbackToMag() {
        return true;
    }

}
