package global.citytech.finpos.neps.iso8583;

import global.citytech.finposframework.switches.PosEntryMode;

public class PosEntryModeConfig {

    public static String getValue(PosEntryMode entryMode) { // TODO confirm added 1
        switch (entryMode) {
            case ICC:
                return "051";
            case MANUAL:
                return "011";
            case MANUAL_WITHOUT_PIN:
                return "010";
            case MAGNETIC_STRIPE:
                return "021";
            case ICC_FALLBACK_TO_MAGNETIC:
                return "721";
            case PICC:
                return "071";
            default:
                return "0000";
        }
    }
}
