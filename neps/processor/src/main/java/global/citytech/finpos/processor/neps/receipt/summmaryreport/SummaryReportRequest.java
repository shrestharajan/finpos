package global.citytech.finpos.processor.neps.receipt.summmaryreport;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.summaryreport.SummaryReportRequestParameter;

/**
 * Created by Saurav Ghimire on 3/30/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public class SummaryReportRequest implements UseCase.Request, SummaryReportRequestParameter {
    private ApplicationRepository applicationRepository;
    private ReconciliationRepository reconciliationRepository;
    private PrinterService printerService;

    public SummaryReportRequest() {
    }

    public SummaryReportRequest(ApplicationRepository applicationRepository, ReconciliationRepository reconciliationRepository, PrinterService printerService) {
        this.applicationRepository = applicationRepository;
        this.reconciliationRepository = reconciliationRepository;
        this.printerService = printerService;
    }

    public ApplicationRepository getApplicationRepository() {
        return applicationRepository;
    }

    public ReconciliationRepository getReconciliationRepository() {
        return reconciliationRepository;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public static final class Builder {
        private ApplicationRepository applicationRepository;
        private ReconciliationRepository reconciliationRepository;
        private PrinterService printerService;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public Builder withReconciliationRepository(ReconciliationRepository reconciliationRepository) {
            this.reconciliationRepository = reconciliationRepository;
            return this;
        }

        public Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public SummaryReportRequest build() {
            SummaryReportRequest summaryReportRequest = new SummaryReportRequest();
            summaryReportRequest.printerService = this.printerService;
            summaryReportRequest.applicationRepository = this.applicationRepository;
            summaryReportRequest.reconciliationRepository = this.reconciliationRepository;
            return summaryReportRequest;
        }
    }
}
