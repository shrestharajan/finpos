package global.citytech.finposframework.utility;

import java.nio.charset.StandardCharsets;

/** User: Surajchhetry Date: 2/14/20 Time: 7:41 AM */
public class HexDump {
  private static final char[] HEX_DIGITS = {
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
  };

  private HexDump() {}

  public static String dumpHexString(byte[] array) {
    if (array == null) return "";
    return dumpHexString(array, 0, array.length);
  }

  public static String dumpHexString(byte[] array, int offset, int length) {
    if (array == null) return "";
    StringBuilder result = new StringBuilder();

    byte[] line = new byte[16];
    int lineIndex = 0;

    result.append("\n0x");
    result.append(toHexString(offset));

    for (int i = offset; i < offset + length; i++) {
      if (lineIndex == 16) {
        result.append(" ");

        for (int j = 0; j < 16; j++) {
          if (line[j] > ' ' && line[j] < '~') {
            result.append(new String(line, j, 1, StandardCharsets.UTF_8));
          } else {
            result.append(".");
          }
        }

        result.append("\n0x");
        result.append(toHexString(i));
        lineIndex = 0;
      }

      byte b = array[i];
      result.append(" ");
      result.append(HEX_DIGITS[(b >>> 4) & 0x0F]);
      result.append(HEX_DIGITS[b & 0x0F]);

      line[lineIndex++] = b;
    }

    if (lineIndex != 16) {
      int count = (16 - lineIndex) * 3;
      count++;
      for (int i = 0; i < count; i++) {
        result.append(" ");
      }

      for (int i = 0; i < lineIndex; i++) {
        if (line[i] > ' ' && line[i] < '~') {
          result.append(new String(line, i, 1, StandardCharsets.UTF_8));
        } else {
          result.append(".");
        }
      }
    }

    return result.toString();
  }

  public static String toHexString(byte b) {
    return toHexString(toByteArray(b));
  }

  public static String toHexString(byte[] array) {
    return toHexString(array, 0, array.length);
  }

  public static String toHexString(int i) {
    return toHexString(toByteArray(i));
  }

  public static String toHexString(byte[] array, int offset, int length) {
    char[] buf = new char[length * 2];
    int bufIndex = 0;
    for (int i = offset; i < offset + length; i++) {
      byte b = array[i];
      buf[bufIndex++] = HEX_DIGITS[(b >>> 4) & 0x0F];
      buf[bufIndex++] = HEX_DIGITS[b & 0x0F];
    }
    return new String(buf);
  }

  public static byte[] toByteArray(byte b) {
    byte[] array = new byte[1];
    array[0] = b;
    return array;
  }

  public static byte[] toByteArray(int i) {
    byte[] array = new byte[4];

    array[3] = (byte) (i & 0xFF);
    array[2] = (byte) ((i >> 8) & 0xFF);
    array[1] = (byte) ((i >> 16) & 0xFF);
    array[0] = (byte) ((i >> 24) & 0xFF);

    return array;
  }
}
