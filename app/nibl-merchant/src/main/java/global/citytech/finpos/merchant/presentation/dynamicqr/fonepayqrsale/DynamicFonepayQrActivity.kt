package global.citytech.finpos.merchant.presentation.dynamicqr.fonepayqrsale

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.qr.fonepayqr.FonepayQrGenerateRequest
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.qr.QrHelper
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmationDialog
import global.citytech.finpos.merchant.presentation.dynamicqr.QrConst.FONEPAY_BILL_NUMBER
import global.citytech.finpos.merchant.presentation.dynamicqr.QrConst.FONEPAY_PROVIDER_CODE
import global.citytech.finpos.merchant.presentation.dynamicqr.QrConst.FONEPAY_REFERENCE_NUMBER
import global.citytech.finpos.merchant.presentation.dynamicqr.QrConst.FONEPAY_REQUEST_NUMBER
import global.citytech.finpos.merchant.presentation.dynamicqr.QrConst.QR_TYPE_FONEPAY
import global.citytech.finpos.merchant.presentation.dynamicqr.QrType
import global.citytech.finpos.merchant.presentation.dynamicqr.base.DynamicQrBaseActivity
import global.citytech.finpos.merchant.utils.SoundUtils
import global.citytech.finpos.merchant.utils.VoiceMessageUtils
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion
import kotlinx.android.synthetic.main.activity_dynamic_qr.*
import java.math.BigDecimal

class DynamicFonepayQrActivity : DynamicQrBaseActivity(),
    TransactionConfirmationDialog.Listener {
    private var transactionAmount: BigDecimal = BigDecimal.ZERO
    private var transactionConfirmationDialog: TransactionConfirmationDialog? = null
    lateinit var qrHelper: QrHelper

    companion object {
        const val REQUEST_CODE = 378
        const val EXTRA_TRANSACTION_AMOUNT = "extra.transaction.amount"
        const val EXTRA_QR_PAYMENT = "extra.qr.payment"
    }

    private var localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDatas();
        observeQrData()
        observeQrConfiguration()
        observeAppConfigurationLiveData()
        observeTransactionConfirmation()
        getViewModel().bindService()
    }

    private fun initDatas(){
        transactionAmount = intent.getSerializableExtra(EXTRA_TRANSACTION_AMOUNT) as BigDecimal
        qrHelper = QrHelper()
    }

    private fun observeAppConfigurationLiveData() {
        getViewModel().appConfiguration.observe(
            this,
            Observer {
                if (it.merchants != null){
                    tv_merchant_name.setText(it.merchants?.get(0)?.name)
                    tv_merchant_address.setText(it.merchants?.get(0)?.address)
                    getViewModel().getQrOperators(QrType.FONEPAY)
                }
            }
        )
    }

    private fun observeQrData() {
        getViewModel().qrData.observe(this, Observer {
            setQrCode(it)
        })
    }

    private fun observeQrConfiguration() {
        getViewModel().qrConfiguration.observe(this, Observer {
            qrOperatorItem = it.qrOperatorInfo
            qrConfiguration = it
            getQrData()
        })
    }

    private fun getQrData() {
        getViewModel().getFonepayQr(prepareFonepayQrGenerateRequest())
    }

    private fun setQrCode(qrData: String) {
        val qrBitmap = qrHelper.generateQrBitmap(qrData)
        updateViews(qrOperatorItem)
        if (qrBitmap != null) {
            iv_qr_content.setImageBitmap(qrBitmap)
            getViewModel().startTimer()
            getViewModel().fonepayQrGenerateRequest?.let { getViewModel().checkQrPaymentStatus(it, 2000, QR_TYPE_FONEPAY) }
        }
    }

    private fun observeTransactionConfirmation() {
        getViewModel().transactionConfirmationLiveData.observe(this, Observer {
            showTransactionConfirmationDialog(it, getViewModel().updateTransactionConfirmation)
        })
    }

    private fun showTransactionConfirmationDialog(it: TransactionConfirmation, update: Boolean) {
        if (transactionConfirmationDialog == null)
            transactionConfirmationDialog = TransactionConfirmationDialog(this, this, it)

        if (update)
            transactionConfirmationDialog!!.update(it, this)
        transactionConfirmationDialog!!.show()
    }

    private fun prepareFonepayQrGenerateRequest(): FonepayQrGenerateRequest {
        val fonepayQrGenerateRequest = FonepayQrGenerateRequest(
            requestNumber = getViewModel().getFonepayQrIdentifier(FONEPAY_REQUEST_NUMBER),
            merchantId = qrConfiguration?.merchantId.toString(),
            terminalId = qrConfiguration?.terminalId.toString(),
            merchantBillNumber = getViewModel().getFonepayQrIdentifier(FONEPAY_BILL_NUMBER),
            merchantTransactionReference = getViewModel().getFonepayQrIdentifier(FONEPAY_REFERENCE_NUMBER),
            providerCode = FONEPAY_PROVIDER_CODE,
            amount = transactionAmount
        )
        Log.d("FONEPAY", "QR CONTENT REQUEST: "+Jsons.toJsonObj(fonepayQrGenerateRequest))
        getViewModel().fonepayQrGenerateRequest = fonepayQrGenerateRequest
        return fonepayQrGenerateRequest
    }

    override fun getViewModel(): DynamicFonepayQrViewModel {
        return ViewModelProviders.of(this).get(DynamicFonepayQrViewModel::class.java)
    }

    override fun onPositiveButtonClick() {
        getViewModel().printPaymentReceipt(
            ReceiptVersion.CUSTOMER_COPY
        )
    }

    override fun onNegativeButtonClick() {
        setResultAndFinish()
    }

    override fun setResultAndFinish() {
        val intent = Intent()
        intent.putExtra(EXTRA_QR_PAYMENT, getViewModel().qrPayment)
        setResult(RESULT_OK, intent)
        finish()
    }
}