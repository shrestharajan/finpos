package global.citytech.finposframework.switches.cashadvance;

import global.citytech.finposframework.switches.ResponseParameter;

/**
 * Created by Unique Shakya on 2/4/2021.
 */
public interface CashAdvanceResponseParameter extends ResponseParameter {
}
