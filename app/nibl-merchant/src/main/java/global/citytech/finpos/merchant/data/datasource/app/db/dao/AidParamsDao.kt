package global.citytech.finpos.merchant.data.datasource.app.db.dao

import androidx.room.*
import global.citytech.finpos.merchant.domain.model.app.AidParam

@Dao
interface AidParamsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(aidParams: List<AidParam>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(aidParam: AidParam): Long

    @Query("SELECT id, aid, label, terminal_aid_version, default_tdol, default_ddol, denial_action_code, online_action_code, default_action_code, terminal_floor_limit, contactless_floor_limit, contactless_transaction_limit, cvm_limit, additional_data FROM aid_params")
    fun getAidParamList(): List<AidParam>

    @Update
    fun update(aidParam: AidParam): Int

    @Query("DELETE FROM aid_params WHERE id = :id")
    fun deleteById(id: Long): Int

    @Query("DELETE FROM aid_params")
    fun nukeTableData()
}