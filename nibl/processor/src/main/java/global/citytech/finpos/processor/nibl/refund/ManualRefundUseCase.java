//package global.citytech.finpos.processor.nibl.refund;
//
//import java.util.concurrent.CountDownLatch;
//
//import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
//import global.citytech.finpos.nibl.iso8583.ProcessingCode;
//import global.citytech.finpos.nibl.iso8583.requestsender.refund.ManualRefundRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.refund.RefundRequest;
//import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
//import global.citytech.finpos.processor.nibl.reversal.ReversalRequestModel;
//import global.citytech.finpos.processor.nibl.reversal.ReversalUseCase;
//import global.citytech.finpos.processor.nibl.transaction.ManualTransactionUseCase;
//import global.citytech.finpos.processor.nibl.transaction.NiblTransactionReceiptPrintHandler;
//import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
//import global.citytech.finpos.processor.nibl.transaction.ReversedTransactionReceiptHandler;
//import global.citytech.finposframework.exceptions.FinPosException;
//import global.citytech.finposframework.exceptions.PosError;
//import global.citytech.finposframework.exceptions.PosException;
//import global.citytech.finposframework.hardware.io.cards.CardDetails;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
//import global.citytech.finposframework.iso8583.DataElement;
//import global.citytech.finposframework.iso8583.IsoMessageResponse;
//import global.citytech.finposframework.iso8583.RequestContext;
//import global.citytech.finposframework.listeners.CardConfirmationListener;
//import global.citytech.finposframework.notifier.Notifier;
//import global.citytech.finposframework.repositories.ApplicationRepository;
//import global.citytech.finposframework.repositories.TerminalRepository;
//import global.citytech.finposframework.repositories.TransactionRepository;
//import global.citytech.finposframework.supports.UseCase;
//import global.citytech.finposframework.switches.PosEntryMode;
//import global.citytech.finposframework.switches.refund.ManualRefundRequester;
//import global.citytech.finposframework.usecases.TransactionType;
//import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
//import global.citytech.finposframework.usecases.transaction.TransactionLog;
//import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
//import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
//import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
//import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
//import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
//import global.citytech.finposframework.utility.HelperUtils;
//import global.citytech.finposframework.utility.IsoMessageUtils;
//import global.citytech.finposframework.utility.JsonUtils;
//import global.citytech.finposframework.utility.StringUtils;
//
///**
// * Created by Unique Shakya on 11/3/2020.
// */
//public class ManualRefundUseCase extends ManualTransactionUseCase implements
//        ManualRefundRequester<RefundRequestModel, RefundResponseModel>,
//        UseCase<RefundRequestModel, RefundResponseModel> {
//
//    static CountDownLatch countDownLatch;
//    RefundRequestModel refundRequestModel;
//    ReadCardResponse readCardResponse;
//    String stan;
//    String invoiceNum;
//    TransactionRequest transactionRequest;
//    String batchNumber;
//    private global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest;
//    private RefundRequest isoRefundRequest;
//
//    public ManualRefundUseCase(TerminalRepository terminalRepository, Notifier notifier) {
//        super(terminalRepository, notifier);
//    }
//
//    @Override
//    protected String getClassName() {
//        return ManualRefundUseCase.this.getClass().getSimpleName();
//    }
//
//    @Override
//    public RefundResponseModel execute(RefundRequestModel refundRequestModel) {
//        this.refundRequestModel = refundRequestModel;
//        try {
//            this.logger.log("::: REACHED MANUAL REFUND USE CASE :::");
//            stan = this.terminalRepository.getSystemTraceAuditNumber();
//            invoiceNum = this.terminalRepository.getInvoiceNumber();
//            batchNumber = this.terminalRepository.getReconciliationBatchNumber();
//            transactionRequest
//                    = refundRequestModel.transactionRequest;
//            this.transactionRepository = refundRequestModel.transactionRepository;
//            if (!this.deviceIsReady(
//                    refundRequestModel.deviceController,
//                    transactionRequest.getTransactionType()
//            )) {
//                throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
//            }
//            this.initTransaction(
//                    refundRequestModel.applicationRepository,
//                    transactionRequest
//            );
//            ReadCardResponse readCardResponse
//                    = this.prepareManualReadCardResponse(transactionRequest);
//            readCardResponse.getCardDetails().setAmount(transactionRequest.getAmount());
//            readCardResponse.getCardDetails().setCashBackAmount(transactionRequest.getAdditionalAmount());
//            this.identifyAndSetCardScheme(
//                    readCardResponse,
//                    refundRequestModel.transactionRepository,
//                    refundRequestModel.applicationRepository
//            );
//            if (!this.validTransaction(
//                    refundRequestModel.applicationRepository,
//                    transactionRequest,
//                    readCardResponse
//            )) {
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
//            }
//
//            if (!this.transactionAuthorizationSucceeds(
//                    refundRequestModel.transactionAuthenticator,
//                    refundRequestModel.applicationRepository,
//                    transactionRequest.getTransactionType(),
//                    readCardResponse
//            )) {
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
//            }
//            PurchaseRequest purchaseRequest = preparePurchaseRequest(
//                    transactionRequest,
//                    readCardResponse.getCardDetails()
//            );
//
//            final boolean[] isConfirm = {false};
//            countDownLatch = new CountDownLatch(1);
//            this.notifier.notify(Notifier.EventType.CARD_CONFIRMATION_EVENT,
//                    readCardResponse.getCardDetails(),
//                    (CardConfirmationListener) confirmed -> {
//                        isConfirm[0] = confirmed;
//                        countDownLatch.countDown();
//                    });
//            countDownLatch.await();
//            if (!isConfirm[0]) {
//                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
//            }
//            return processCardAndProceed();
//        } catch (PosException ex) {
//            ex.printStackTrace();
//            throw ex;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        }
//    }
//
//    private RefundResponseModel processCardAndProceed() {
//
//        readCardResponse = this.retrievePinBlockIfRequired(
//                purchaseRequest,
//                refundRequestModel.applicationRepository,
//                refundRequestModel.transactionAuthenticator
//        );
//        this.logger.debug("::: READ CARD RESPONSE ::: " + readCardResponse.toString());
//        this.notifier.notify(
//                Notifier.EventType.TRANSMITTING_REQUEST,
//                Notifier.EventType.TRANSMITTING_REQUEST.getDescription()
//        );
//        return this.initiateIsoMessageTransmission(
//                refundRequestModel,
//                stan,
//                invoiceNum,
//                batchNumber,
//                transactionRequest,
//                readCardResponse
//        );
//    }
//
//    private RefundResponseModel initiateIsoMessageTransmission(RefundRequestModel refundRequestModel,
//                                                               String stan, String invoiceNum,
//                                                               String batchNumber,
//                                                               TransactionRequest transactionRequest,
//                                                               ReadCardResponse readCardResponse) {
//        try {
//            IsoMessageResponse isoMessageResponse = this.sendIsoMessage(
//                    stan,
//                    invoiceNum,
//                    transactionRequest.getOriginalRetrievalReferenceNumber(),
//                    readCardResponse,
//                    refundRequestModel
//            );
//            InvalidResponseHandlerResponse invalidResponseHandlerResponse
//                    = this.checkForInvalidResponseFromHost(isoMessageResponse);
//            if (invalidResponseHandlerResponse.isInvalid()) {
//                return this.onInvalidResponseFromHost(
//                        transactionRequest,
//                        invalidResponseHandlerResponse,
//                        batchNumber,
//                        stan,
//                        invoiceNum,
//                        readCardResponse,
//                        refundRequestModel
//                );
//            }
//            boolean purchaseApproved = this.transactionApprovedByActionCode(isoMessageResponse);
//            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait...");
//            this.notifyTransactionCompletion(
//                    stan,
//                    batchNumber,
//                    invoiceNum,
//                    purchaseApproved,
//                    transactionRequest,
//                    refundRequestModel,
//                    isoMessageResponse,
//                    readCardResponse,
//                    refundRequestModel.applicationRepository
//            );
//            boolean shouldPrint = this.printReceipt(
//                    batchNumber,
//                    stan,
//                    invoiceNum,
//                    refundRequestModel.printerService,
//                    refundRequestModel.applicationRepository,
//                    refundRequestModel.transactionRepository,
//                    transactionRequest,
//                    readCardResponse,
//                    isoMessageResponse
//            );
//            return this.prepareRefundResponse(
//                    stan,
//                    isoMessageResponse,
//                    purchaseApproved,
//                    refundRequestModel.applicationRepository,
//                    shouldPrint
//            );
//        } catch (FinPosException ex) {
//            if (this.isResponseNotReceivedException(ex)) {
//                return onTransactionTimeOut(
//                        transactionRequest,
//                        batchNumber,
//                        stan,
//                        invoiceNum,
//                        readCardResponse,
//                        refundRequestModel
//                );
//            } else if (ex.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT) {
//                logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED UNABLE TO CONNECT");
//                transactionRepository.removeAutoReversal(this.autoReversal);
//                throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
//            } else {
//                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//            }
//        }
//    }
//
//    private RefundResponseModel onTransactionTimeOut(TransactionRequest transactionRequest, String batchNumber,
//                                                     String stan, String invoiceNumber,
//                                                     ReadCardResponse readCardResponse,
//                                                     RefundRequestModel refundRequestModel) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, "TIMEOUT");
//        this.performReversal(
//                stan,
//                readCardResponse,
//                refundRequestModel
//        );
//        boolean shouldPrint = !this.approvePrintOnly(refundRequestModel.applicationRepository);
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(
//                    transactionRequest,
//                    readCardResponse,
//                    this.terminalRepository
//            ).prepare(
//                    batchNumber,
//                    stan,
//                    invoiceNumber,
//                    new ReasonForReversal(
//                            ReasonForReversal.Type.TIMEOUT,
//                            "Timeout"
//                    )
//            );
//            new NiblTransactionReceiptPrintHandler(refundRequestModel.printerService)
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            refundRequestModel.transactionRepository.updateReceiptLog(receiptLog);
//        }
//        return RefundResponseModel.Builder.createDefaultBuilder()
//                .message("TIMEOUT")
//                .isApproved(false)
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private RefundResponseModel prepareRefundResponse(String stan, IsoMessageResponse isoMessageResponse, boolean purchaseApproved,
//                                                      ApplicationRepository applicationRepository,
//                                                      boolean shouldPrint) {
//        RefundResponseModel.Builder builder = RefundResponseModel.Builder.createDefaultBuilder();
//        builder.isApproved(purchaseApproved);
//        builder.debugRequestMessage(isoMessageResponse.getDebugRequestString());
//        builder.debugResponseMessage(isoMessageResponse.getDebugResponseString());
//        builder.stan(stan);
//        builder.message(
//                this.retrieveMessage(
//                        isoMessageResponse,
//                        purchaseApproved,
//                        applicationRepository
//                )
//        );
//        builder.shouldPrintCustomerCopy(shouldPrint);
//        return builder.build();
//    }
//
//    private void notifyTransactionCompletion(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean purchaseApproved,
//            TransactionRequest transactionRequest,
//            RefundRequestModel refundRequestModel,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository
//    ) {
//        this.updateTransactionLog(
//                stan,
//                batchNumber,
//                invoiceNumber,
//                purchaseApproved,
//                transactionRequest,
//                refundRequestModel.transactionRepository,
//                isoMessageResponse,
//                readCardResponse,
//                applicationRepository
//        );
//        this.updateTransactionIds(purchaseApproved);
//        this.notifyTransactionStatus(
//                purchaseApproved,
//                isoMessageResponse,
//                refundRequestModel.applicationRepository
//        );
//    }
//
//    private void updateTransactionLog(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean purchaseApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository
//    ) {
//        this.logger.log("::: NIBL ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
//        this.logger.log("::: NIBL ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
//        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
//                .withStan(stan)
//                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
//                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
//                .withInvoiceNumber(invoiceNumber)
//                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
//                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
//                .withTransactionType(TransactionType.REFUND)
//                .withOriginalTransactionType(TransactionType.REFUND)
//                .withTransactionAmount(transactionRequest.getAmount())
//                .withOriginalTransactionAmount(transactionRequest.getAmount())
//                .withTransactionDate(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_DATE))
//                .withTransactionTime(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_TIME))
//                .withTransactionStatus(purchaseApproved ? "APPROVED" : "DECLINED")
//                .withPosEntryMode(PosEntryMode.MANUAL)
//                .withOriginalPosEntryMode(PosEntryMode.MANUAL)
//                .withPosConditionCode(this.isoRefundRequest.getPosConditionCode())
//                .withOriginalPosConditionCode("")
//                .withReconcileStatus("")
//                .withReconcileTime("")
//                .withReconcileDate("")
//                .withReconcileBatchNo(batchNumber)
//                .withReadCardResponse(readCardResponse)
//                .withOriginalTransactionReferenceNumber(isoRefundRequest.getOriginalRetrievalReferenceNumber())
//                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse,
//                        applicationRepository, batchNumber, stan, invoiceNumber))
//                .withAuthorizationCompleted(true)
//                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
//                .withTransactionVoided(false)
//                .withTipAdjusted(false)
//                .build();
//        transactionRepository.updateTransactionLog(transactionLog);
//    }
//
//    private RefundResponseModel onInvalidResponseFromHost(
//            TransactionRequest transactionRequest,
//            InvalidResponseHandlerResponse invalidResponseHandlerResponse,
//            String batchNumber,
//            String stan,
//            String invoiceNumber,
//            ReadCardResponse readCardResponse,
//            RefundRequestModel requestModel
//    ) {
//        this.notifier.notify(
//                Notifier.EventType.TRANSACTION_DECLINED,
//                invalidResponseHandlerResponse.getMessage()
//        );
//        this.performReversal(stan, readCardResponse, requestModel);
//        boolean shouldPrint = !this.approvePrintOnly(requestModel.applicationRepository);
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(
//                    transactionRequest,
//                    readCardResponse,
//                    this.terminalRepository
//            ).prepare(
//                    batchNumber,
//                    stan,
//                    invoiceNumber,
//                    new ReasonForReversal(
//                            ReasonForReversal.Type.INVALID_RESPONSE,
//                            invalidResponseHandlerResponse.getMessage()
//                    )
//            );
//            new NiblTransactionReceiptPrintHandler(requestModel.printerService)
//                    .printTransactionReceipt(
//                            receiptLog,
//                            ReceiptVersion.MERCHANT_COPY
//                    );
//            requestModel.transactionRepository.updateReceiptLog(receiptLog);
//        }
//        return RefundResponseModel.Builder.createDefaultBuilder()
//                .isApproved(false)
//                .message(invalidResponseHandlerResponse.getMessage())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private void performReversal(
//            String stan,
//            ReadCardResponse readCardResponse,
//            RefundRequestModel refundRequestModel
//    ) {
//        ReversalUseCase reversalUseCase = new ReversalUseCase(
//                this.prepareReversalRequest(stan, readCardResponse, refundRequestModel),
//                this.terminalRepository,
//                this.notifier
//        );
//        try {
//            reversalUseCase.execute(
//                    prepareReversalRequestModel(
//                            refundRequestModel,
//                            readCardResponse
//                    )
//            );
//            this.transactionRepository.removeAutoReversal(this.autoReversal);
//        } catch (FinPosException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private NiblReversalRequest prepareReversalRequest(
//            String stan,
//            ReadCardResponse readCardResponse,
//            RefundRequestModel refundRequestModel
//    ) {
//        NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
//        niblReversalRequest.setProcessingCode(ProcessingCode.PURCHASE.getCode());
//        niblReversalRequest.setAmount(
//                refundRequestModel.transactionRequest.getAmount()
//        );
//        niblReversalRequest.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        niblReversalRequest.setExpiryDate(readCardResponse.getCardDetails().getExpiryDate());
//        niblReversalRequest.setPosEntryMode(this.isoRefundRequest.getEntryMode());
//        niblReversalRequest.setPosConditionCode(this.isoRefundRequest.getPosConditionCode());
//        niblReversalRequest.setPinBlock(this.isoRefundRequest.getPinBlock());
//        niblReversalRequest.setInvoiceNumber(this.isoRefundRequest.getInvoiceNumber());
//        niblReversalRequest.setStan(stan);
//        return niblReversalRequest;
//    }
//
//    private ReversalRequestModel prepareReversalRequestModel(
//            RefundRequestModel refundRequestModel,
//            ReadCardResponse readCardResponse
//    ) {
//        return ReversalRequestModel.Builder.newInstance()
//                .withTransactionRequest(
//                        refundRequestModel.transactionRequest
//                )
//                .withTransactionRepository(
//                        refundRequestModel.transactionRepository
//                )
//                .withPrinterService(
//                        refundRequestModel.printerService
//                )
//                .withApplicationRepository(
//                        refundRequestModel.applicationRepository
//                )
//                .withReadCardResponse(readCardResponse)
//                .build();
//    }
//
//    private IsoMessageResponse sendIsoMessage(
//            String stan,
//            String invoiceNumber,
//            String originalRrn,
//            ReadCardResponse readCardResponse, RefundRequestModel requestModel) {
//        RequestContext requestContext = this.prepareManualTemplateRequestContext(stan);
//        this.logger.debug(String.format(" Request Context ::%s", requestContext));
//        this.isoRefundRequest = isoRefundRequest(
//                invoiceNumber,
//                originalRrn,
//                readCardResponse
//        );
//        ManualRefundRequestSender refundRequestSender = new ManualRefundRequestSender(
//                new NIBLSpecInfo(),
//                requestContext
//        );
//        requestContext.setRequest(isoRefundRequest);
//        this.autoReversal = new AutoReversal(
//                TransactionType.REFUND,
//                JsonUtils.toJsonObj(prepareReversalRequest(stan, readCardResponse, requestModel)),
//                0,
//                AutoReversal.Status.ACTIVE,
//                StringUtils.dateTimeStamp()
//        );
//        this.transactionRepository.saveAutoReversal(this.autoReversal);
//        IsoMessageResponse isoMessageResponse = refundRequestSender.send();
//        this.transactionRepository.removeAutoReversal(this.autoReversal);
//        return isoMessageResponse;
//    }
//
//    private RefundRequest isoRefundRequest(String invoiceNumber, String originalRrn, ReadCardResponse readCardResponse) {
//        RefundRequest refundRequest = new RefundRequest();
//        refundRequest.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        refundRequest.setExpireDate(readCardResponse.getCardDetails().getExpiryDate());
//        refundRequest.setEntryMode(PosEntryMode.MANUAL);
//        refundRequest.setPosConditionCode("05");
//        if (readCardResponse.getCardDetails().getPinBlock().getOfflinePin() ||
//                StringUtils.isEmpty(readCardResponse.getCardDetails().getPinBlock().getPin()) ||
//                readCardResponse.getCardDetails().getPinBlock().getPin().equalsIgnoreCase("offline"))
//            refundRequest.setPinBlock("");
//        else
//            refundRequest.setPinBlock(readCardResponse.getCardDetails().getPinBlock().getPin());
//        refundRequest.setLocalTime(HelperUtils.getDefaultLocaleTimeHhMmSs());
//        refundRequest.setLocalDate(HelperUtils.getDefaultLocaleDateWithoutYear());
//        refundRequest.setInvoiceNumber(invoiceNumber);
//        refundRequest.setTransactionAmount(readCardResponse.getCardDetails().getAmount());
//        refundRequest.setOriginalRetrievalReferenceNumber(originalRrn);
//        refundRequest.setCvv(readCardResponse.getCardDetails().getCvv());
//        return refundRequest;
//    }
//
//    private PurchaseRequest preparePurchaseRequest(
//            TransactionRequest transactionRequest,
//            CardDetails cardDetails
//    ) {
//        PurchaseRequest purchaseRequest = new PurchaseRequest(
//                transactionRequest.getTransactionType(),
//                transactionRequest.getAmount(),
//                transactionRequest.getAdditionalAmount()
//        );
//        cardDetails.setAmount(transactionRequest.getAmount());
//        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
//        purchaseRequest.setCardDetails(cardDetails);
//        purchaseRequest.setFallbackIccToMag(transactionRequest.isFallbackIccToMag());
//        return purchaseRequest;
//    }
//}
