package global.citytech.finpos.merchant.data.datasource.app.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import global.citytech.finpos.merchant.domain.model.app.TransactionConfig

/**
 * Created by Unique Shakya on 9/24/2020.
 */
@Dao
interface TransactionConfigsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(transactionConfig: TransactionConfig)

    @Query("SELECT stan FROM transaction_configs")
    fun getStan(): Long

    @Query("UPDATE transaction_configs set stan = stan + 1")
    fun incrementStan(): Int

    @Query("SELECT rrn FROM transaction_configs")
    fun getRrn(): Long

    @Query("UPDATE transaction_configs set rrn = rrn + 1")
    fun incrementRrn(): Int

    @Query("SELECT invoice_number FROM transaction_configs")
    fun getInvoiceNumber(): Long

    @Query("UPDATE transaction_configs set invoice_number = invoice_number + 1")
    fun incrementInvoiceNumber(): Int

    @Query("SELECT reconciliation_batch_number FROM transaction_configs")
    fun getReconciliationBatchNumber(): Long

    @Query("UPDATE transaction_configs set reconciliation_batch_number = reconciliation_batch_number + 1")
    fun incrementReconciliationBatchNumber(): Int

    @Query("UPDATE transaction_configs set reconciliation_batch_number = :batchNumber")
    fun updateReconciliationBatchNumber(batchNumber: Int)

    @Query("DELETE FROM transaction_configs")
    fun nukeTableData()
}