package global.citytech.finpos.processor.nibl.transaction.initializer;

import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.usecases.transaction.TransactionInitializer;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;

/**
 * Created by Unique Shakya on 8/27/2020.
 */
public class NiblTransactionInitializer implements TransactionInitializer {

    private ApplicationRepository applicationRepository;

    public NiblTransactionInitializer(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    @Override
    public TransactionRequest initialize(TransactionRequest transactionRequest) {
        transactionRequest.setEmvParameterRequest(
                applicationRepository.retrieveEmvParameterRequest()
        );
        return transactionRequest;
    }
}
