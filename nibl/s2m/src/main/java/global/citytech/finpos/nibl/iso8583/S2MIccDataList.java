package global.citytech.finpos.nibl.iso8583;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.hardware.io.cards.IccData;

/**
 * Created by Unique Shakya on 9/29/2020.
 */
public class S2MIccDataList {

    private S2MIccDataList() {
    }

    public static List<IccData> get(){
        List<IccData> iccDataList = new ArrayList<>();
        iccDataList.add(IccData.TRANSACTION_CURRENCY_CODE);
        iccDataList.add(IccData.PSN);
        iccDataList.add(IccData.AIP);
        iccDataList.add(IccData.DF_NAME);
        iccDataList.add(IccData.TVR);
        iccDataList.add(IccData.TRANSACTION_DATE);
        iccDataList.add(IccData.TRANSACTION_TYPE);
        iccDataList.add(IccData.AMOUNT_AUTHORISED);
        iccDataList.add(IccData.AMOUNT_OTHER);
        iccDataList.add(IccData.APP_VERSION_NUMBER);
        iccDataList.add(IccData.IAD);
        iccDataList.add(IccData.TERMINAL_COUNTRY_CODE);
        iccDataList.add(IccData.APPLICATION_CRYPTOGRAM);
        iccDataList.add(IccData.CID);
        iccDataList.add(IccData.TERMINAL_CAPABILITIES);
        iccDataList.add(IccData.CVM);
        iccDataList.add(IccData.TERMINAL_TYPE);
        iccDataList.add(IccData.ATC);
        iccDataList.add(IccData.UNPREDICTABLE_NUMBER);
        iccDataList.add(IccData.TRANSACTION_SEQUENCE_COUNTER);
        iccDataList.add(IccData.FORM_FACTOR_INDICATOR);
        return iccDataList;
    }
}
