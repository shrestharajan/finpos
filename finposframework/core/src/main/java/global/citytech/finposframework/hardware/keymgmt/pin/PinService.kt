package global.citytech.finposframework.hardware.keymgmt.pin

import global.citytech.finposframework.hardware.common.DeviceService

/**
 * Created by Rishav Chudal on 6/19/20.
 */
interface PinService: DeviceService {
    fun showPinPadAndCalculatePinBlock(pinRequest: PinRequest): PinResponse
}