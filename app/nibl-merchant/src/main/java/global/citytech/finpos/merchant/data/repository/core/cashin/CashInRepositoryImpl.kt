package global.citytech.finpos.merchant.data.repository.core.cashin

import global.citytech.finpos.merchant.data.datasource.core.cashin.CashInDataSource
import global.citytech.finpos.merchant.domain.model.cashin.CashInResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.cashin.CashInRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.cashin.CashInRequestItem
import io.reactivex.Observable

class CashInRepositoryImpl(private val cashInDataSource: CashInDataSource):
CashInRepository
{
    override fun generateCashInRequest(
        configurationItem: ConfigurationItem,
        cashInRequestItem: CashInRequestItem
    ): Observable<CashInResponseEntity> = cashInDataSource.generateCashInRequest(configurationItem, cashInRequestItem)

}