package global.citytech.finpos.merchant.presentation.model.greenpin

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

data class GreenPinRequestItem(
    val transactionType: TransactionType? = null,
    val amount:BigDecimal? = null
)