package global.citytech.finpos.merchant.domain.repository.core.balanceinquiry

import global.citytech.finpos.merchant.domain.model.balanceinquiry.BalanceInquiryResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.balanceinquiry.BalanceInquiryRequestItem
import io.reactivex.Observable

interface BalanceInquiryRepository {
    fun generateBalanceInquiryRequest(
        configurationItem: ConfigurationItem,
        balanceInquiryRequestItem: BalanceInquiryRequestItem
    ): Observable<BalanceInquiryResponseEntity>
}