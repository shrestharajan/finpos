package global.citytech.finposframework.switches.receipt.tmslogs;

import global.citytech.finposframework.switches.RequestParameter;

/**
 * Created by Rishav Chudal on 11/5/20.
 */
public interface TmsLogRequestParameter extends RequestParameter {
}
