package global.citytech.finpos.processor.nibl.transaction.tipadjustment;

import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;

/**
 * Created by Unique Shakya on 5/4/2021.
 */
public class TipAdjustmentResponseModel extends TransactionResponseModel {

    private boolean approved;
    private String message;
    private boolean shouldPrintCustomerCopy;
    private String stan;
    private String debugRequestMessage;
    private String debugResponseMessage;

    public boolean isApproved() {
        return approved;
    }

    public String getMessage() {
        return message;
    }

    public boolean isShouldPrintCustomerCopy() {
        return shouldPrintCustomerCopy;
    }

    public String getStan() {
        return stan;
    }

    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }

    public static final class Builder {
        private boolean approved;
        private String message;
        private boolean shouldPrintCustomerCopy;
        private String stan;
        private String debugRequestMessage;
        private String debugResponseMessage;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder approved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder shouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public Builder stan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder debugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public Builder debugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public TipAdjustmentResponseModel build() {
            TipAdjustmentResponseModel tipAdjustmentResponseModel = new TipAdjustmentResponseModel();
            tipAdjustmentResponseModel.debugResponseMessage = this.debugResponseMessage;
            tipAdjustmentResponseModel.debugRequestMessage = this.debugRequestMessage;
            tipAdjustmentResponseModel.approved = this.approved;
            tipAdjustmentResponseModel.stan = this.stan;
            tipAdjustmentResponseModel.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            tipAdjustmentResponseModel.message = this.message;
            return tipAdjustmentResponseModel;
        }
    }
}
