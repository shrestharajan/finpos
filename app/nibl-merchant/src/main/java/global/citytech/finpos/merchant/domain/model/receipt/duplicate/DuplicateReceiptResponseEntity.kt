package global.citytech.finpos.merchant.domain.model.receipt.duplicate

import global.citytech.finposframework.hardware.common.Result

data class DuplicateReceiptResponseEntity(
    var result: Result? = null,
    val message: String? = null
)
