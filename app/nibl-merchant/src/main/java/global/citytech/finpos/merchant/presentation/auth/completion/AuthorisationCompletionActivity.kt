package global.citytech.finpos.merchant.presentation.auth.completion

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.common.extensions.getBundle
import global.citytech.common.extensions.hideKeyboard
import global.citytech.easydroid.core.extension.showToast
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityBaseTransactionBinding
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionActivity
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finpos.merchant.presentation.tipadjustment.TipsAdjustmentActivity
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.utility.StringUtils
import kotlinx.android.synthetic.main.activity_base_transaction.*
import kotlinx.android.synthetic.main.layout_auth_completion.*
import kotlinx.android.synthetic.main.layout_tip_adjustment.*
import kotlinx.android.synthetic.main.layout_void.*
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Created by Unique Shakya on 9/29/2020.
 */
class AuthorisationCompletionActivity :
    BaseTransactionActivity<ActivityBaseTransactionBinding, AuthorisationCompletionViewModel>() {

    private lateinit var viewModel: AuthorisationCompletionViewModel
    private lateinit var originalData: String

    companion object{
        const val INVOICE = "invoice_number"
        const val FROM_TRANSACTION_DETAIL = "from_transaction_detail"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadAuthCompletionLayout()
        this.viewModel = getViewModel()
        this.initViews()
        this.initBaseViews()
        this.initObservers()
        getBundle()?.let {
            fromTransactionDetail = getBundle()!!.getBoolean(FROM_TRANSACTION_DETAIL, false)
            val invoiceNumber :String? = getBundle()?.getString(INVOICE)
            invoiceNumber?.let {
                et_auth_completion_auth_code.setText(invoiceNumber)
            }
        }
    }

    private fun loadAuthCompletionLayout() {
        layout_auth_completion.visibility = View.VISIBLE
    }

    private fun hideAuthCompletionLayout() {
        layout_auth_completion.visibility = View.GONE
    }

    private fun initObservers() {
        this.viewModel.validData.observe(this, Observer {
            if (it)
                this.viewModel.checkForTransactionPresent(originalData)
            else
                this.showToast(getString(R.string.error_msg_invalid_auth_completion_input), Toast.LENGTH_SHORT)
        })

        this.viewModel.configurationItem.observe(this, Observer {
            hideAuthCompletionLayout()
            this.viewModel.performAuthCompletion(it, amount, originalData, getTransactionType())
        })

        this.viewModel.transactionPresent.observe(this, Observer {
            if (it) {
                hideKeyboard(et_auth_completion_auth_code)
                //this.startAmountActivity()
                val amount  = getViewModel().preAuthAmount.toBigDecimal()
                    .divide(BigDecimal.valueOf(100.00), 2, RoundingMode.CEILING)
                this.startAmountActivityWithAuthLimit(amount)
            }
            else
                this.showToast("Transaction not found", Toast.LENGTH_SHORT)
        })

        initBaseLiveDataObservers()
    }

    private fun initViews() {
        iv_auth_completion_back.setOnClickListener {
            hideKeyboard(et_auth_completion_auth_code)
            returnToDashboard()
        }

        btn_auth_cancel.setOnClickListener {
            hideKeyboard(et_auth_completion_auth_code)
            returnToDashboard()
        }

        btn_auth_confirm.setOnClickListener {
            this.showProgressScreen(getString(R.string.auth_completion_validating_message))
            originalData = StringUtils.ofRequiredLength(et_auth_completion_auth_code.text.toString(),6)
            this.viewModel.validateData(originalData)
        }
    }

    override fun getTransactionType(): TransactionType = TransactionType.AUTH_COMPLETION

    override fun getTransactionViewModel(): BaseTransactionViewModel = this.viewModel

    override fun onAmountActivityResult(data: Intent?) {
        data?.let {
            val amountInString =
                data.getStringExtra(getString(R.string.intent_confirmed_amount))
            amount = amountInString?.toBigDecimal()!!
            viewModel.getConfigurationItem()
        }
    }

    override fun getBindingVariable(): Int = BR.authCompletionViewModel

    override fun getLayout(): Int = R.layout.activity_base_transaction

    override fun getViewModel(): AuthorisationCompletionViewModel =
        ViewModelProviders.of(this)[AuthorisationCompletionViewModel::class.java]

    override fun onPositiveButtonClick() {
        this.showProgressScreen("Printing Customer Copy...")
        this.viewModel.printCustomerCopy()
    }

    override fun onNegativeButtonClick() = checkForCardPresent()

    override fun onManualButtonClicked() {
        showToast("Manual Button Clicked", Toast.LENGTH_SHORT)
        this.viewModel.doCleanUp(BaseTransactionViewModel.Task.MANUAL_TRANSACTION)
    }

    override fun onCancelButtonClicked() {
        showToast("Cancel Button Clicked", Toast.LENGTH_SHORT)
        this.viewModel.doCleanUp(BaseTransactionViewModel.Task.RETURN_TO_DASHBOARD)
    }

    override fun onManualActivityResult(data: Intent?) {
        //TODO
    }
}