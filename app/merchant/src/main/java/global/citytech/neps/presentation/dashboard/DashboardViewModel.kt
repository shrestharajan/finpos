package global.citytech.neps.presentation.dashboard

import android.util.Log
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseViewModel
import global.citytech.neps.data.repository.core.transaction.TransactionRepositoryImpl
import global.citytech.neps.data.repository.local.LocalRepositoryImpl
import global.citytech.neps.datasource.core.transaction.TransactionDataSourceImpl
import global.citytech.neps.datasource.local.LocalConfigurationDataSourceImpl
import global.citytech.neps.domain.usecase.core.transaction.CoreTransactionUseCase
import global.citytech.neps.domain.usecase.local.LocalDataUseCase
import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.neps.presentation.model.mapToPresentation
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers

class DashboardViewModel : BaseViewModel() {

    val configuration by lazy { MutableLiveData<ConfigurationItem>() }
    val bankDisplayImage by lazy { MutableLiveData<ByteArray>() }
    var configurationUseCase: LocalDataUseCase =
        LocalDataUseCase(
            LocalRepositoryImpl(LocalConfigurationDataSourceImpl())
        )
    val transactionUseCase: CoreTransactionUseCase =
        CoreTransactionUseCase(
            TransactionRepositoryImpl(
                TransactionDataSourceImpl()
            )
        )

    fun getConfiguration() = compositeDisposable.add(configurationUseCase.getWholeConfiguration()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .map { it.mapToPresentation() }
        .subscribe({
            configuration.value = it
            doCleanUpAtStart()
        },
        {
            message.value = it.message
        })
    )

    private fun doCleanUpAtStart() = Completable
        .fromAction { transactionUseCase.cleanUp() }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            this::onCleanUpComplete,
            this::onCleanUpError
        )

    private fun onCleanUpComplete() {
        Log.i("coreLogger", "Clean Up Complete")
    }

    private fun onCleanUpError(throwable: Throwable) {
        Log.i("coreLogger", "Clean Up Error ::: ".plus(throwable.message))
    }
}