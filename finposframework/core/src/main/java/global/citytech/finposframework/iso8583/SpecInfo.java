package global.citytech.finposframework.iso8583;

/** User: Surajchhetry Date: 2/19/20 Time: 5:19 PM */
public interface SpecInfo {

  DataElementConfigs getDEConfig();

  int getMessageHeaderLength();

  int getMTILength();
}
