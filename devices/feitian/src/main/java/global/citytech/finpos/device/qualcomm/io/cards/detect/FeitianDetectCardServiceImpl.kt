package global.citytech.finpos.device.qualcomm.io.cards.detect

import android.content.Context
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.emv.OnSearchCardCallback
import com.ftpos.library.smartpos.emv.TrackData
import com.ftpos.library.smartpos.errcode.ErrCode
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.ICC_CARD_DETECTED
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.MAG_CARD_DETECTED
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.PICC_CARD_DETECTED
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CARD_DETECT_FAILED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_CARD_DETECT_TIMEOUT
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_CONTACTLESS_CARD_DETECTED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_CONTACT_CARD_DETECTED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_MAG_CARD_DETECTED
import global.citytech.finposframework.hardware.common.Result.*
import global.citytech.finposframework.hardware.io.cards.CardType.*
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_ICC_READER_CARD_DETECT_ERROR
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_PLEASE_WAIT
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_READING_CARD
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_SWIPE_CARD_AGAIN
import global.citytech.finposframework.notifier.Notifier
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
/**
 * Created by Rishav Chudal on 4/7/21.
 */
class FeitianDetectCardServiceImpl constructor(val context: Context, val notifier: Notifier) :
    FeitianDetectCardService,
    OnSearchCardCallback,
    FeitianLogger.Listener {

    private val emv = Emv.getInstance(context.applicationContext)
    private lateinit var detectCardResponse: FeitianDetectCardResponse
    private lateinit var countDownLatch: CountDownLatch
    private val logger = FeitianLogger(this)

    override fun detectCard(request: FeitianDetectCardRequest): FeitianDetectCardResponse {
        this.detectCardResponse = FeitianDetectCardResponse(
            FAILURE,
            CARD_DETECT_FAILED,
            UNKNOWN
        )
        val slotsToOpen = request.slotsToOpen
        val detectTimeOut = request.timeOutInSecs
        if (slotsToOpen.isNotEmpty()) {
            this.emv.searchCard(
                detectTimeOut!!,
                this
            )
            this.awaitCountdownLatch(modifiedTimeOut(detectTimeOut))
        }
        return this.detectCardResponse
    }

    override fun onSuccess(returnCode: Int, trackData: TrackData?) {
        this.logger.logWithClassName(
            "OnSearchCardCallback ::: onSuccess (returnCode) ::: ".plus(returnCode)
        )
        when (returnCode) {
            MAG_CARD_DETECTED -> onCardTypeMagDetected(trackData)
            ICC_CARD_DETECTED -> onCardTypeICCDetected()
            PICC_CARD_DETECTED -> onCardTypePICCDetected()
            ErrCode.ERR_OP_TIMEOUT -> onCardDetectTimeOut()
        }
        this.countDownLatch.countDown()
    }

    override fun onError(returnCode: Int) {
        this.logger.logWithClassName(
            "OnSearchCardCallback ::: onError (returnCode) ".plus(returnCode)
        )
        notifyCardReadStatusToUI(
            Notifier.EventType.DETECT_CARD_ERROR,
            MSG_PLEASE_WAIT
        )
        when (returnCode) {
            ErrCode.ERR_MAG_READER -> onSwipeAgainRequired()
            ErrCode.ERR_IC_NO_ATR -> onIccReaderDetectError()
        }
        this.countDownLatch.countDown()
    }

    private fun awaitCountdownLatch(timeOut: Int) {
        this.logger.logWithClassName("CountDown Latch Await TimeOut ::: $timeOut")
        try {
            countDownLatch = CountDownLatch(1)
            this.logger.logWithClassName("Awaiting CountDownLatch ::: ".plus(countDownLatch))
            countDownLatch.await(timeOut.toLong(), TimeUnit.SECONDS)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun modifiedTimeOut(timeInSecs: Int): Int {
        var timeOut = timeInSecs
        timeOut += 5
        return timeOut
    }

    private fun onCardTypeMagDetected(trackData: TrackData?) {
        this.logger.logWithClassName("Magnetic card detected...")
        notifyCardReadStatusToUI(
            Notifier.EventType.READING_CARD,
            MSG_READING_CARD
        )
        this.detectCardResponse = FeitianDetectCardResponse(
            SUCCESS,
            MSG_MAG_CARD_DETECTED,
            MAG
        )
        this.detectCardResponse.trackData = trackData
    }

    private fun onCardTypeICCDetected() {
        this.logger.logWithClassName("Contact card detected...")
        notifyCardReadStatusToUI(
            Notifier.EventType.READING_CARD,
            MSG_READING_CARD
        )
        this.detectCardResponse = FeitianDetectCardResponse(
            SUCCESS,
            MSG_CONTACT_CARD_DETECTED,
            ICC
        )
    }

    private fun notifyCardReadStatusToUI(
        eventType: Notifier.EventType,
        message: String
    ) {
        notifier.notify(
            eventType,
            message
        )
    }

    private fun onCardTypePICCDetected() {
        this.logger.logWithClassName("PICC card detected...")
        notifyCardReadStatusToUI(
            Notifier.EventType.READING_CARD,
            MSG_READING_CARD
        )
        this.detectCardResponse = FeitianDetectCardResponse(
            SUCCESS,
            MSG_CONTACTLESS_CARD_DETECTED,
            PICC
        )
    }

    private fun onCardDetectTimeOut() {
        this.logger.logWithClassName("Card Detect TimeOut...")
        notifyCardReadStatusToUI(
            Notifier.EventType.DETECT_CARD_ERROR,
            MSG_PLEASE_WAIT
        )
        this.detectCardResponse = FeitianDetectCardResponse(
            TIMEOUT,
            MSG_CARD_DETECT_TIMEOUT,
            UNKNOWN
        )
    }

    private fun onSwipeAgainRequired() {
        this.logger.logWithClassName("Swipe Again Required...")
        this.detectCardResponse = FeitianDetectCardResponse(
            SWIPE_AGAIN,
            MSG_SWIPE_CARD_AGAIN,
            MAG
        )
    }

    private fun onIccReaderDetectError() {
        this.logger.logWithClassName("ICC Reader Detect Error...")
        detectCardResponse = FeitianDetectCardResponse(
            ICC_CARD_DETECT_ERROR,
            MSG_ICC_READER_CARD_DETECT_ERROR,
            ICC
        )
    }

    override fun getClassName(): String = FeitianDetectCardServiceImpl::class.java.simpleName
}