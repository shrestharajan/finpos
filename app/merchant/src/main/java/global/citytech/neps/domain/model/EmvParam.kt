package global.citytech.neps.domain.model

import android.content.ContentValues
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "emv_param")
data class EmvParam(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_ID)
    var id: String,
    @ColumnInfo(name = COLUMN_TTQ)
    var ttq: String? = null,
    @ColumnInfo(name = COLUMN_TRANSACTION_CURRENCY_EXPONENT)
    var transactionCurrencyExponent: String? = null,
    @ColumnInfo(name = COLUMN_TRANSACTION_CURRENCY_CODE)
    var transactionCurrencyCode: String? = null,
    @ColumnInfo(name = COLUMN_TERMINAL_TYPE)
    var terminalType: String? = null,
    @ColumnInfo(name = COLUMN_TERMINAL_ID)
    var terminalId: String? = null,
    @ColumnInfo(name = COLUMN_TERMINAL_COUNTRY_CODE)
    var terminalCountryCode: String? = null,
    @ColumnInfo(name = COLUMN_TERMINAL_CAPABILITIES)
    var terminalCapabilities: String? = null,
    @ColumnInfo(name = COLUMN_MERCHANT_NAME)
    var merchantName: String? = null,
    @ColumnInfo(name = COLUMN_MERCHANT_IDENTIFIER)
    var merchantIdentifier: String? = null,
    @ColumnInfo(name = COLUMN_MERCHANT_CATEGORY_CODE)
    var merchantCategoryCode: String? = null,
    @ColumnInfo(name = COLUMN_FORCE_ONLINE_FLAG)
    var forceOnlineFlag: String? = null,
    @ColumnInfo(name = COLUMN_ADDITIONAL_TERMINAL_CAPABILITIES)
    var additionalTerminalCapabilities: String? = null
) {
    companion object {

        const val COLUMN_ID = "id"
        const val COLUMN_TTQ = "ttq"
        const val COLUMN_TRANSACTION_CURRENCY_EXPONENT = "transaction_currency_exponent"
        const val COLUMN_TRANSACTION_CURRENCY_CODE = "transaction_currency_code"
        const val COLUMN_TERMINAL_TYPE = "terminal_type"
        const val COLUMN_TERMINAL_ID = "terminal_id"
        const val COLUMN_TERMINAL_COUNTRY_CODE = "terminal_country_code"
        const val COLUMN_TERMINAL_CAPABILITIES = "terminal_capabilities"
        const val COLUMN_MERCHANT_NAME = "merchant_name"
        const val COLUMN_MERCHANT_IDENTIFIER = "merchant_identifier"
        const val COLUMN_MERCHANT_CATEGORY_CODE = "merchant_category_code"
        const val COLUMN_FORCE_ONLINE_FLAG = "force_online_flag"
        const val COLUMN_ADDITIONAL_TERMINAL_CAPABILITIES = "additional_terminal_capabilities"


        fun fromContentValues(values: ContentValues): EmvParam {
            values.let {
                val emvParam = EmvParam(values.getAsString(COLUMN_ID))
                if (values.containsKey(COLUMN_TTQ))
                    emvParam.ttq = values.getAsString(COLUMN_TTQ)
                if (values.containsKey(COLUMN_TRANSACTION_CURRENCY_EXPONENT))
                    emvParam.transactionCurrencyExponent = values.getAsString(
                        COLUMN_TRANSACTION_CURRENCY_EXPONENT
                    )
                if (values.containsKey(COLUMN_TRANSACTION_CURRENCY_CODE))
                    emvParam.transactionCurrencyCode = values.getAsString(
                        COLUMN_TRANSACTION_CURRENCY_CODE
                    )
                if (values.containsKey(COLUMN_TERMINAL_TYPE))
                    emvParam.terminalType = values.getAsString(
                        COLUMN_TERMINAL_TYPE
                    )
                if (values.containsKey(COLUMN_TERMINAL_ID))
                    emvParam.terminalId= values.getAsString(
                        COLUMN_TERMINAL_ID
                    )
                if (values.containsKey(COLUMN_TERMINAL_COUNTRY_CODE))
                    emvParam.terminalCountryCode = values.getAsString(
                        COLUMN_TERMINAL_COUNTRY_CODE
                    )
                if (values.containsKey(COLUMN_TERMINAL_CAPABILITIES))
                    emvParam.terminalCapabilities = values.getAsString(
                        COLUMN_TERMINAL_CAPABILITIES
                    )
                if (values.containsKey(COLUMN_MERCHANT_NAME))
                    emvParam.merchantName = values.getAsString(
                        COLUMN_MERCHANT_NAME
                    )
                if (values.containsKey(COLUMN_MERCHANT_IDENTIFIER))
                    emvParam.merchantIdentifier = values.getAsString(
                        COLUMN_MERCHANT_IDENTIFIER
                    )
                if (values.containsKey(COLUMN_MERCHANT_CATEGORY_CODE))
                    emvParam.merchantCategoryCode = values.getAsString(
                        COLUMN_MERCHANT_CATEGORY_CODE
                    )
                if (values.containsKey(COLUMN_FORCE_ONLINE_FLAG))
                    emvParam.forceOnlineFlag = values.getAsString(
                        COLUMN_FORCE_ONLINE_FLAG
                    )
                if (values.containsKey(COLUMN_ADDITIONAL_TERMINAL_CAPABILITIES))
                    emvParam.additionalTerminalCapabilities = values.getAsString(
                        COLUMN_ADDITIONAL_TERMINAL_CAPABILITIES
                    )
                return emvParam
            }
        }
    }
}