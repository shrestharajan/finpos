package global.citytech.finposframework.switches.reconciliation.clear;

import global.citytech.finposframework.switches.ResponseParameter;

/**
 * Created by Unique Shakya on 9/28/2021.
 */
public interface BatchClearResponseParameter extends ResponseParameter {
}
