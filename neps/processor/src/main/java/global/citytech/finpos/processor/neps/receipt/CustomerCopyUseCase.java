package global.citytech.finpos.processor.neps.receipt;

import global.citytech.finpos.processor.neps.receipt.customercopy.CustomerCopyRequestModel;
import global.citytech.finpos.processor.neps.receipt.customercopy.CustomerCopyResponseModel;
import global.citytech.finpos.processor.neps.transaction.NepsTransactionReceiptPrintHandler;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.customercopy.CustomerCopyRequester;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;

public class CustomerCopyUseCase implements UseCase<CustomerCopyRequestModel, CustomerCopyResponseModel>,
        CustomerCopyRequester<CustomerCopyRequestModel, CustomerCopyResponseModel> {

    @Override
    public CustomerCopyResponseModel execute(CustomerCopyRequestModel request) {
        ReceiptLog receiptLog = request.getTransactionRepository().getReceiptLog();
        NepsTransactionReceiptPrintHandler transactionReceiptHandler = new
                NepsTransactionReceiptPrintHandler(request.getPrinterService());
        PrinterResponse printerResponse = transactionReceiptHandler.printTransactionReceipt(
                receiptLog,
                ReceiptVersion.CUSTOMER_COPY
        );
        return new CustomerCopyResponseModel(
                printerResponse.getResult(),
                printerResponse.getMessage()
        );
    }
}