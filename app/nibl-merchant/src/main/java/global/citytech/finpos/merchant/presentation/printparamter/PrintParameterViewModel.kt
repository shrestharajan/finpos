package global.citytech.finpos.merchant.presentation.printparamter

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel

import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.core.printparameter.PrintParameterRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.printparameter.PrintParameterDataSourceImpl
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.core.printparameters.PrintParametersUseCase
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.presentation.model.printparameter.PrintParameterResponse
import global.citytech.finposframework.switches.receipt.tmslogs.TmsLogsParameter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class PrintParameterViewModel(private val context: Application) : BaseAndroidViewModel(context) {

    val printSuccess by lazy { MutableLiveData<Boolean>() }
    val terminalConfigured by lazy { MutableLiveData<Boolean>() }
    val configurationItem by lazy { MutableLiveData<ConfigurationItem>() }

    private var localDataUseCase: LocalDataUseCase =
        LocalDataUseCase(LocalRepositoryImpl(LocalDatabaseSourceImpl(), PreferenceManager))

    private var printParametersUseCase: PrintParametersUseCase
            = PrintParametersUseCase(
        PrintParameterRepositoryImpl(PrintParameterDataSourceImpl(context))
    )

    fun validateParametersToPrint(parameters: ArrayList<TmsLogsParameter>) {
        if (parameters.size != 0) {
            getConfigurationItem()
        } else {
            message.value = context.getString(R.string.error_msg_empty_print_parameter)
            isLoading.value = false
        }
    }

    fun checkDeviceConfigured() {
        terminalConfigured.value = localDataUseCase.isTerminalConfigured(false)
    }

    fun getConfigurationItem() {
        compositeDisposable.add(
            localDataUseCase.getWholeConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.mapToPresentation() }
            .subscribe(
                {
                    configurationItem.value = it
                }, {
                message.value = it.message
                    isLoading.value = false
                }
            )
        )
    }

    fun printParameters(
        configurationItem: ConfigurationItem,
        parameters: ArrayList<TmsLogsParameter>
    ) {
        isLoading.value = true
        compositeDisposable.add(
            this.printParametersUseCase.prepareAndPrintParameters(
                configurationItem, parameters
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {onPrintParametersNext(it)},
                    {onPrintParametersError(it)}
                )
        )
    }

    private fun onPrintParametersNext(response: PrintParameterResponse) {
        isLoading.value = false
        message.value = response.message
        printSuccess.value = response.success
    }

    private fun onPrintParametersError(throwable: Throwable) {
        isLoading.value = false
        message.value = throwable.message
        printSuccess.value = false
    }
}