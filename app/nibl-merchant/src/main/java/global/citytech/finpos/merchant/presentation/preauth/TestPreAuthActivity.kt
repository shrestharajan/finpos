//package global.citytech.finpos.merchant.presentation.preauth
//
//import android.app.ProgressDialog
//import android.os.Bundle
//import android.widget.Toast
//import androidx.appcompat.app.AppCompatActivity
//import citytech.global.easydroid.concurrent.LongRunningTask
//import global.citytech.easydroid.core.extension.showToast
//import global.citytech.finpos.merchant.NiblMerchant
//import global.citytech.finpos.merchant.R
//import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
//import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
//import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
//import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
//import global.citytech.finpos.merchant.framework.datasource.core.TransactionAuthenticatorImpl
//import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
//import global.citytech.finpos.merchant.framework.datasource.device.*
//import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
//import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
//import global.citytech.finpos.merchant.presentation.model.mapToPresentation
//import global.citytech.finpos.merchant.service.AppDeviceFactory
//import global.citytech.finpos.processor.nibl.NiblConstants
//import global.citytech.finpos.processor.nibl.preauths.PreAuthRequestModel
//import global.citytech.finpos.processor.nibl.preauths.PreAuthResponseModel
//import global.citytech.finpos.processor.nibl.preauths.PreAuthUseCase
//import global.citytech.finposframework.hardware.common.Result
//import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
//import global.citytech.finposframework.hardware.DeviceServiceType
//import global.citytech.finposframework.hardware.io.cards.CardDetails
//import global.citytech.finposframework.hardware.io.cards.CardType
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardService
//import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat
//import global.citytech.finposframework.log.Logger
//import global.citytech.finposframework.usecases.TransactionType
//import global.citytech.finposframework.usecases.transaction.data.TransactionRequest
//import io.reactivex.android.schedulers.AndroidSchedulers
//import io.reactivex.disposables.CompositeDisposable
//import io.reactivex.schedulers.Schedulers
//import kotlinx.android.synthetic.main.activity_test_purchase.*
//
//class TestPreAuthActivity : AppCompatActivity() {
//
//    private val logger = Logger(TestPreAuthActivity::class.java.name)
//    lateinit var progressDialog: ProgressDialog
//
//
//    var localDataUseCase: LocalDataUseCase =
//        LocalDataUseCase(
//            LocalRepositoryImpl(
//                LocalDatabaseSourceImpl(),
//                PreferenceManager
//            )
//        )
//
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_pre_auth)
//        progressDialog = ProgressDialog(this)
//        btn_start.setOnClickListener { this.readCard() }
//    }
//
//    private fun readCard() {
//        this.showProgressDialog("Reading Card")
//        val longRunningTask = LongRunningTask.Builder()
//            .withTask {
//                val service =
//                    AppDeviceFactory.getCore(this, DeviceServiceType.CARD) as ReadCardService
//                val readCardRequest = prepareReadCardRequest()
//                logger.log("::: READ CARD REQUEST ::: " + readCardRequest.toString())
//                val readCardResponse = service.readCardDetails(readCardRequest)
//                logger.log(readCardResponse.result.message)
//                if (readCardResponse.result != Result.SUCCESS)
//                    throw IllegalArgumentException("FAILURE TO READ CARD")
//                return@withTask service.processEMV(
//                    prepareReadCardRequestWithCardDetails(
//                        readCardResponse.cardDetails
//                    )
//                )
//            }
//            .onCompleted {
//                this.hideProgressDialog()
//                logger.log("::: READ CARD RESPONSE ::: " + (it as ReadCardResponse).toString())
//                this.getConfigurationItem(it, prepareReadCardRequest())
//            }
//            .onError {
//                this.hideProgressDialog()
//                it.printStackTrace()
//            }
//            .observeInUIThread(this)
//            .build()
//        longRunningTask.execute()
//    }
//
//    private fun startPreAuth(
//        it: ReadCardResponse,
//        readCardRequest: ReadCardRequest,
//        configurationItem: ConfigurationItem
//    ) {
//        val terminalRepository = TerminalRepositoryImpl(configurationItem)
//        this.showProgressDialog("Sending Request")
//        val longRunningTask = LongRunningTask.Builder()
//            .withTask {
//                val preAuthRequestModel = preparePreAuthRequestModel(it, readCardRequest)
//                val preAuthUseCase =
//                    PreAuthUseCase(
//                        terminalRepository
//                    ,null)
//                return@withTask preAuthUseCase.execute(preAuthRequestModel)
//            }
//            .onCompleted {
//                this.showToast(
//                    (it as PreAuthResponseModel).isApproved.toString(),
//                    Toast.LENGTH_SHORT
//                )
//                this.hideProgressDialog()
//            }
//            .onError {
//                this.hideProgressDialog()
//                it.printStackTrace()
//            }
//            .observeInUIThread(this)
//            .build()
//        longRunningTask.execute()
//    }
//
//    private fun preparePreAuthRequestModel(
//        it: ReadCardResponse,
//        readCardRequest: ReadCardRequest
//    ): PreAuthRequestModel {
//        val transactionRequest = TransactionRequest(TransactionType.PRE_AUTH)
//        transactionRequest.amount = et_amount.text.toString().toDouble()
//
//        val preAuthRequestModel = PreAuthRequestModel.Builder.newInstance()
//            .withTransactionRepository(
//                TransactionRepositoryImpl(
//                    NiblMerchant.INSTANCE
//                )
//            )
//            .withTransactionRequest(transactionRequest)
//            .withDeviceController(DeviceControllerImpl(NiblMerchant.INSTANCE))
//            .withReadCardService(CardSourceImpl(NiblMerchant.getActivityContext()))
//            .withTransactionAuthenticator(TransactionAuthenticatorImpl())
//            .withPrinterService(PrinterSourceImpl(NiblMerchant.INSTANCE))
//            .build()
//        return preAuthRequestModel
//    }
//
//    private fun getConfigurationItem(
//        readCardResponse: ReadCardResponse,
//        readCardRequest: ReadCardRequest
//    ) {
//        val compositeDisposable = CompositeDisposable()
//        compositeDisposable.add(localDataUseCase.getWholeConfiguration()
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .map { it.mapToPresentation() }
//            .subscribe(
//                { this.startPreAuth(readCardResponse, readCardRequest, it) },
//                { it.printStackTrace() })
//        )
//    }
//
//    private fun prepareReadCardRequestWithCardDetails(cardDetails: CardDetails?): ReadCardRequest {
//        val readCardRequest =
//            ReadCardRequest(
//                prepareSlotsToOpen(),
//                TransactionType.PURCHASE,
//                "01",
//                prepareEmvParameters()
//            )
//        val stringAmount = et_amount.text.toString()
//        readCardRequest.packageName = NiblMerchant.INSTANCE.packageName
//        readCardRequest.amount = stringAmount.toDouble()
//        readCardRequest.additionalAmount = 0.00
//        readCardRequest.isPinpadRequired = true
//        readCardRequest.pinpadTimeout = 60
//        readCardRequest.cardReadTimeOut = 60
//        readCardRequest.stan = "000011"
//        readCardRequest.cardDetailsBeforeEmvNext = cardDetails
//        readCardRequest.forceOnlineForContact = false
//        readCardRequest.pinBlockFormat =
//            this.retrievePinBlockFormat(sp_pin_block_type.selectedItemId)
//        readCardRequest.currencyName = NiblConstants.PIN_PAD_CURRENCY
//        return readCardRequest
//    }
//
//    private fun retrievePinBlockFormat(selectedItem: Long): PinBlockFormat {
//        return when (selectedItem) {
//            0.toLong() -> PinBlockFormat.ISO9564_FORMAT_0
//            1.toLong() -> PinBlockFormat.ISO9564_FORMAT_1
//            2.toLong() -> PinBlockFormat.ISO9564_FORMAT_2
//            else -> PinBlockFormat.ISO9564_FORMAT_1
//        }
//    }
//
//    private fun prepareReadCardRequest(): ReadCardRequest {
//        val readCardRequest =
//            ReadCardRequest(
//                prepareSlotsToOpen(),
//                TransactionType.PURCHASE,
//                "01",
//                prepareEmvParameters()
//            )
//        val stringAmount = et_amount.text.toString()
//        readCardRequest.packageName = NiblMerchant.INSTANCE.packageName
//        readCardRequest.amount = stringAmount.toDouble()
//        readCardRequest.additionalAmount = 0.00
//        readCardRequest.isPinpadRequired = true
//        readCardRequest.pinpadTimeout = 60
//        readCardRequest.cardReadTimeOut = 60
//        readCardRequest.stan = "000011"
//        readCardRequest.forceOnlineForContact = false
//        readCardRequest.pinBlockFormat =
//            this.retrievePinBlockFormat(sp_pin_block_type.selectedItemId)
//        readCardRequest.currencyName = NiblConstants.PIN_PAD_CURRENCY
//        return readCardRequest
//    }
//
//    private fun prepareEmvParameters(): EmvParametersRequest {
//        return EmvParametersRequest(
//            "524", "524", "02",
//            "E0F0C8", "22", "5411", "41234027",
//            "100512345678910", "Citytech", "35E04000", "D000F0A000",
//            "0"
//        )
//    }
//
//    private fun prepareSlotsToOpen(): List<CardType> {
//        val slotTypes = ArrayList<CardType>()
//        slotTypes.add(CardType.ICC)
//        return slotTypes
//    }
//
//    fun showProgressDialog(message: String?) {
//        this.progressDialog.setCancelable(false)
//        this.progressDialog.setMessage(message)
//        this.progressDialog.show()
//    }
//
//    fun hideProgressDialog() {
//        if (this.progressDialog.isShowing) {
//            this.progressDialog.dismiss()
//        }
//    }
//
//
//}
