package global.citytech.nepsadmin.constants

object Parameter{
    const val SWITCH_PARAM = "switch_param"
    const val AID_PARAM = "aid_param"
    const val EMV_KEYS = "emv_keys"
    const val CARD_SCHEME = "card_scheme"
}