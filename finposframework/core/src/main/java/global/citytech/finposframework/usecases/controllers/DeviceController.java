package global.citytech.finposframework.usecases.controllers;

import java.util.List;

import global.citytech.finposframework.hardware.common.DeviceResponse;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.utility.PosResponse;

public interface DeviceController {
    PosResponse isReady(List<CardType> cardTypeList);

    boolean isIccCardPresent();

    DeviceResponse enableHardwareButtons();

    DeviceResponse disableHardwareButtons();

    DeviceResponse enableHomeButton();

    DeviceResponse disableHomeButton();
}
