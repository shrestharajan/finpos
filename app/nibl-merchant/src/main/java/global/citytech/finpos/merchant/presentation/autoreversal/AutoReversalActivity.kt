package global.citytech.finpos.merchant.presentation.autoreversal

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import global.citytech.common.Constants
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityAutoReversalBinding
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.notifier.Notifier

/**
 * Created by Unique Shakya on 6/22/2021.
 */
class AutoReversalActivity : AppBaseActivity<ActivityAutoReversalBinding, AutoReversalViewModel>() {

    companion object {
        const val REQUEST_CODE = 7383
        const val EXTRA_ERROR_MESSAGE = "extra_error_message"
    }

    private val TAG = AutoReversalActivity::class.java.name

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, intent: Intent?) {
            printLog(TAG, "Broadcast ::: AutoReversalActivity :::".plus(intent!!.action))
            if (intent.action == Constants.INTENT_ACTION_NOTIFICATION) {
                handleNotificationBroadcast(intent)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObservers()
        getViewModel().getConfigurationItem()
    }

    private fun initObservers() {
        getViewModel().configurationItem.observe(this, Observer {
            getViewModel().performAutoReversal(it)
        })

        getViewModel().autoReversalSuccess.observe(this, Observer {
            printLog(TAG, "::: AUTO REVERSAL RESULT ::: $it")
            if (it)
                setResult(Activity.RESULT_OK)
            else {
                val intent = Intent()
                intent.putExtra(EXTRA_ERROR_MESSAGE, this.getViewModel().reversalMessage)
                setResult(Activity.RESULT_CANCELED,intent)
            }
            hideProgressScreen()
            finish()
            overridePendingTransition(0, 0)
        })

        getViewModel().isLoading.observe(this, Observer {
            if (it)
                showProgressScreen(getString(R.string.msg_performing_auto_reversal))
            else
                hideProgressScreen()
        })
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, IntentFilter(Constants.INTENT_ACTION_NOTIFICATION))
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        super.onPause()
    }

    private fun handleNotificationBroadcast(intent: Intent) {
        val eventType =
            intent.getSerializableExtra(Constants.EXTRA_EVENT_TYPE) as Notifier.EventType
        printLog(TAG, "BROADCAST :::: EVENT TYPE IN BROADCAST ::: $eventType.name")
        val message = intent.getStringExtra(Constants.EXTRA_NOTIFICATION_MESSAGE)
        printLog(TAG, "BROADCAST :::: MESSAGE IN BROADCAST ::: $message")
        when (eventType) {
            Notifier.EventType.CONNECTING_TO_SWITCH,
            Notifier.EventType.TRANSMITTING_REQUEST,
            Notifier.EventType.PERFORMING_REVERSAL,
            Notifier.EventType.RESPONSE_RECEIVED,
            Notifier.EventType.PREPARING_RECONCILIATION_TOTALS,
            Notifier.EventType.PREPARING_DETAIL_REPORT,
            Notifier.EventType.BATCH_UPLOADING,
            Notifier.EventType.BATCH_UPLOAD_PROGRESS,
            Notifier.EventType.PRINTING_DETAIL_REPORT,
            Notifier.EventType.PRINTING_SETTLEMENT_REPORT -> {
                this.showProgressScreen(message!!)
            }
            Notifier.EventType.PUSH_MERCHANT_LOG -> {
                this.getViewModel().pushMerchantTransactionLog(intent)
            }
            else -> this.hideProgressScreen()
        }
    }

    private fun showProgressScreen(message: String) {
        showProgress(message)
    }

    private fun hideProgressScreen() {
        hideProgress()
    }

    override fun getBindingVariable(): Int = BR.autoReversalViewModel

    override fun getLayout(): Int = R.layout.activity_auto_reversal

    override fun getViewModel(): AutoReversalViewModel =
        ViewModelProviders.of(this)[AutoReversalViewModel::class.java]
}