package global.citytech.finposframework.usecases.transaction.receipt;

public class Retailer {

    private String retailerLogo;
    private String retailerName;
    private String retailerAddress;
    private String merchantId;
    private String terminalId;

    public String getRetailerLogo() {
        return retailerLogo;
    }

    public String getRetailerName() {
        return retailerName;
    }

    public String getRetailerAddress() {
        return retailerAddress;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public static final class Builder {
        private String retailerLogo;
        private String retailerName;
        private String retailerAddress;
        private String merchantId;
        private String terminalId;

        public Builder() {
        }

        public static Builder defaultBuilder() {
            return new Builder();
        }

        public Builder withRetailerLogo(String retailerLogo) {
            this.retailerLogo = retailerLogo;
            return this;
        }

        public Builder withRetailerName(String retailerName) {
            this.retailerName = retailerName;
            return this;
        }

        public Builder withRetailerAddress(String retailerAddress) {
            this.retailerAddress = retailerAddress;
            return this;
        }

        public Builder withMerchantId(String merchantId) {
            this.merchantId = merchantId;
            return this;
        }

        public Builder withTerminalId(String terminalId) {
            this.terminalId = terminalId;
            return this;
        }

        public Retailer build() {
            Retailer retailer = new Retailer();
            retailer.retailerAddress = this.retailerAddress;
            retailer.retailerName = this.retailerName;
            retailer.retailerLogo = this.retailerLogo;
            retailer.merchantId = this.merchantId;
            retailer.terminalId = this.terminalId;
            return retailer;
        }
    }
}
