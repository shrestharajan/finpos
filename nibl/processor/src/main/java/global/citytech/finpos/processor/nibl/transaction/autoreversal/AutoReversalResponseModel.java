package global.citytech.finpos.processor.nibl.transaction.autoreversal;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.autoreversal.AutoReversalResponseParameter;

/**
 * Created by Unique Shakya on 6/22/2021.
 */
public class AutoReversalResponseModel implements UseCase.Response, AutoReversalResponseParameter {

    private Result result;
    private String message;

    public AutoReversalResponseModel(Result result, String message) {
        this.result = result;
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }
}
