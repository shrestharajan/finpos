package global.citytech.finpos.merchant.framework.datasource.device

import android.app.Application
import global.citytech.finpos.merchant.service.AppDeviceFactory
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.hardware.utility.DeviceApiConstants
import global.citytech.finposframework.hardware.DeviceServiceType
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService
import global.citytech.finposframework.hardware.io.status.IOStatusRequest
import global.citytech.finposframework.hardware.io.status.IOStatusResponse
import global.citytech.finposframework.hardware.io.status.IOStatusService
import global.citytech.finposframework.hardware.system.SystemService
import global.citytech.finposframework.usecases.controllers.DeviceController
import global.citytech.finposframework.utility.PosResponse
import global.citytech.finposframework.utility.PosResult

class DeviceControllerImpl(val instance: Application) : DeviceController {
    override fun isReady(p0: MutableList<CardType>?): PosResponse {
        val request = prepareHardwareStatusRequest(p0)
        val service = AppDeviceFactory.getCore(
            instance.applicationContext,
            DeviceServiceType.HARDWARE_STATUS
        ) as IOStatusService
        val hardwareStatusResponse = service.retrieveHardwareStatus(request)
        return preparePosResponse(p0, hardwareStatusResponse)
    }

    override fun isIccCardPresent(): Boolean {
        val service = AppDeviceFactory.getCore(instance.applicationContext, DeviceServiceType.CARD)
                as ReadCardService
        val cardPresent = service.getIccCardStatus().result == Result.ICC_CARD_PRESENT
        service.cleanUp()
        return cardPresent
    }

    override fun enableHardwareButtons(): DeviceResponse {
        val systemService = AppDeviceFactory.getCore(
            instance.applicationContext,
            DeviceServiceType.SYSTEM
        ) as SystemService
        systemService.enableHomeButton()
        return systemService.enablePowerButton()
    }

    override fun disableHomeButton(): DeviceResponse {
        val systemService = AppDeviceFactory.getCore(
            instance.applicationContext,
            DeviceServiceType.SYSTEM
        ) as SystemService
        return systemService.disableHomeButton()
    }

    override fun disableHardwareButtons(): DeviceResponse {
        val systemService = AppDeviceFactory.getCore(
            instance.applicationContext,
            DeviceServiceType.SYSTEM
        ) as SystemService
        systemService.disableHomeButton()
        return systemService.disablePowerButton()
    }

    override fun enableHomeButton(): DeviceResponse {
        val systemService = AppDeviceFactory.getCore(
            instance.applicationContext,
            DeviceServiceType.SYSTEM
        ) as SystemService
        return systemService.enableHomeButton()
    }

    private fun prepareHardwareStatusRequest(
        cardTypeList: MutableList<CardType>?
    ): IOStatusRequest {
        var hardwareStatusRequest =
            IOStatusRequest(
                true
            )
        if (cardTypeList != null
            && doCardTypeListContainAnyOfTheHardwareCardReader(cardTypeList)
        ) {
            hardwareStatusRequest =
                IOStatusRequest(
                    false
                )
        }
        return hardwareStatusRequest
    }

    private fun preparePosResponse(
        cardTypeList: MutableList<CardType>?,
        IOStatusResponse: IOStatusResponse
    ): PosResponse {
        var posResponse = getFailurePosResponse(PosError.DEVICE_ERROR_NOT_READY)

        if (cardTypeList == null
            || !doCardTypeListContainAnyOfTheHardwareCardReader(cardTypeList)
        ) {
            if (isPrinterOkay(IOStatusResponse)) {
                posResponse = getSuccessPosResponse()
            } else if (isPrinterOutOfPaper(IOStatusResponse)) {
                posResponse = getFailurePosResponse(PosError.PRINTER_OUT_OF_PAPER)
            }
        } else {
            posResponse = manageIfCardTypeListIsNotNull(cardTypeList, IOStatusResponse)
        }
        return posResponse
    }

    private fun isPrinterOutOfPaper(ioStatusResponse: IOStatusResponse): Boolean {
        return ioStatusResponse.printerStatus == DeviceApiConstants.CORE_PRINTER_OUT_OF_PAPER
    }

    private fun manageIfCardTypeListIsNotNull(
        cardTypeList: MutableList<CardType>,
        IOStatusResponse: IOStatusResponse
    ): PosResponse {
        var posResponse = getFailurePosResponse(PosError.PRINTER_ERROR_NOT_READY)
        if (isPrinterOkay(IOStatusResponse)) {
            posResponse = iterateForCardReaderStatus(cardTypeList, IOStatusResponse)
        } else if (isPrinterOutOfPaper(IOStatusResponse)) {
            posResponse = getFailurePosResponse(PosError.PRINTER_OUT_OF_PAPER)
        }
        return posResponse
    }

    private fun iterateForCardReaderStatus(
        cardTypeList: MutableList<CardType>,
        IOStatusResponse: IOStatusResponse
    ): PosResponse {
        var posResponse = getFailurePosResponse(PosError.DEVICE_ERROR_NOT_READY)
        iteration@ for (cardType in cardTypeList) {
            when (cardType) {
                CardType.MAG -> {
                    posResponse = manageOnCardTypeMag(IOStatusResponse)
                    if (posResponse.posResult == PosResult.FAILURE) {
                        break@iteration
                    }
                }
                CardType.ICC -> {
                    posResponse = manageOnCardTypeIcc(IOStatusResponse)
                    if (posResponse.posResult == PosResult.FAILURE) {
                        break@iteration
                    }
                }
                CardType.PICC -> {
                    posResponse = manageOnCardTypePicc(IOStatusResponse)
                    if (posResponse.posResult == PosResult.FAILURE) {
                        break@iteration
                    }
                }
                CardType.MANUAL ->{
                    continue@iteration
                }

                else -> posResponse = getFailurePosResponse(PosError.DEVICE_ERROR_NOT_READY)
            }
        }
        return posResponse
    }

    private fun manageOnCardTypeMag(IOStatusResponse: IOStatusResponse): PosResponse {
        return if (isMagneticReaderOkay(IOStatusResponse)) {
            getSuccessPosResponse()
        } else {
            getFailurePosResponse(PosError.MAG_READER_ERROR_NOT_READY)
        }
    }

    private fun manageOnCardTypeIcc(IOStatusResponse: IOStatusResponse): PosResponse {
        return if (isICCReaderOkay(IOStatusResponse)) {
            getSuccessPosResponse()
        } else {
            getFailurePosResponse(PosError.ICC_READER_ERROR_NOT_READY)
        }
    }

    private fun manageOnCardTypePicc(IOStatusResponse: IOStatusResponse): PosResponse {
        return if (isPICCReaderOkay(IOStatusResponse)) {
            getSuccessPosResponse()
        } else {
            getFailurePosResponse(PosError.PICC_READER_ERROR_NOT_READY)
        }
    }

    private fun doCardTypeListContainAnyOfTheHardwareCardReader(
        cardTypeList: MutableList<CardType>
    ): Boolean {
        var result = false
        if (cardTypeList.contains(CardType.MAG)
            || cardTypeList.contains(CardType.ICC)
            || cardTypeList.contains(CardType.PICC)
        ) {
            result = true
        }
        return result
    }

    private fun isPrinterOkay(IOStatusResponse: IOStatusResponse): Boolean {
        var result = false
        if (IOStatusResponse.printerStatus == DeviceApiConstants.CORE_PRINTER_PLAIN_RECEIPT_PAPER) {
            result = true
        }
        return result
    }

    private fun isMagneticReaderOkay(
        IOStatusResponse: IOStatusResponse
    ): Boolean {
        var result = false
        if (IOStatusResponse.magReaderStatus.equals(
                DeviceApiConstants.CORE_MAG_READER_OK
            )
        ) {
            result = true
        }
        return result
    }

    private fun isICCReaderOkay(
        ioStatusResponse: IOStatusResponse
    ): Boolean {
        var result = false
        if (ioStatusResponse.iccReaderStatus.equals(
                DeviceApiConstants.CORE_ICC_READER_OK
            )
        ) {
            result = true
        }
        return result
    }

    private fun isPICCReaderOkay(
        IOStatusResponse: IOStatusResponse
    ): Boolean {
        var result = false
        if (IOStatusResponse.piccReaderStatus.equals(
                DeviceApiConstants.CORE_PICC_READER_OK
            )
        ) {
            result = true
        }
        return result
    }

    private fun getSuccessPosResponse() =
        PosResponse(
            PosResult.SUCCESS,
            0,
            "Pos is Ready"
        )

    private fun getFailurePosResponse(posError: PosError) =
        PosResponse(
            posError
        )

}