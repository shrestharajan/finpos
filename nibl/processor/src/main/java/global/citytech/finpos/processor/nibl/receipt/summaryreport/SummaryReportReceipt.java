package global.citytech.finpos.processor.nibl.receipt.summaryreport;

import global.citytech.finposframework.switches.posmode.PosMode;
import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;

/**
 * Created by Unique Shakya on 1/5/2021.
 */
public class SummaryReportReceipt {

    private Retailer retailer;
    private Performance performance;
    private String reconciliationBatchNumber;
    private String host;
    private SummaryReport summaryReport;
    private PosMode posMode;

    public Retailer getRetailer() {
        return retailer;
    }

    public Performance getPerformance() {
        return performance;
    }

    public String getReconciliationBatchNumber() {
        return reconciliationBatchNumber;
    }

    public String getHost() {
        return host;
    }

    public SummaryReport getSummaryReport() {
        return summaryReport;
    }

    public PosMode getPosMode() {
        return posMode;
    }

    public static final class Builder {
        private Retailer retailer;
        private Performance performance;
        private String reconciliationBatchNumber;
        private String host;
        private SummaryReport summaryReport;
        private PosMode posMode;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withRetailer(Retailer retailer) {
            this.retailer = retailer;
            return this;
        }

        public Builder withPerformance(Performance performance) {
            this.performance = performance;
            return this;
        }

        public Builder withReconciliationBatchNumber(String reconciliationBatchNumber) {
            this.reconciliationBatchNumber = reconciliationBatchNumber;
            return this;
        }

        public Builder withHost(String host) {
            this.host = host;
            return this;
        }

        public Builder withSummaryReport(SummaryReport summaryReport) {
            this.summaryReport = summaryReport;
            return this;
        }

        public Builder withPosMode(PosMode posMode) {
            this.posMode = posMode;
            return this;
        }

        public SummaryReportReceipt build() {
            SummaryReportReceipt summaryReportReceipt = new SummaryReportReceipt();
            summaryReportReceipt.retailer = this.retailer;
            summaryReportReceipt.performance = this.performance;
            summaryReportReceipt.reconciliationBatchNumber = this.reconciliationBatchNumber;
            summaryReportReceipt.host = this.host;
            summaryReportReceipt.summaryReport = this.summaryReport;
            summaryReportReceipt.posMode = this.posMode;
            return summaryReportReceipt;
        }
    }
}
