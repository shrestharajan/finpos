package global.citytech.finposframework.usecases.transaction;

import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;

public interface CardProcessorInterface {

    ReadCardResponse validateReadCardResponse(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse);

    ReadCardResponse processCard(String stan, PurchaseRequest purchaseRequest);

}
