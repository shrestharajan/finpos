package global.citytech.finpos.processor.neps.cardprocessor;

import global.citytech.finpos.processor.neps.transaction.CardIdentifier;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.IdentifyCardResponse;
import global.citytech.finposframework.usecases.transaction.CardProcessorInterface;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Saurav Ghimire on 1/27/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public abstract class CardProcessor implements CardProcessorInterface {

    TransactionRepository transactionRepository;
    TransactionAuthenticator transactionAuthenticator;
    ApplicationRepository applicationRepository;
    ReadCardService readCardService;
    Notifier notifier;
    TerminalRepository terminalRepository;

    public CardProcessor(
            TransactionRepository transactionRepository,
            TransactionAuthenticator transactionAuthenticator,
            ApplicationRepository applicationRepository,
            ReadCardService readCardService,
            Notifier notifier,
            TerminalRepository terminalRepository
    ) {
        this.transactionRepository = transactionRepository;
        this.transactionAuthenticator = transactionAuthenticator;
        this.applicationRepository = applicationRepository;
        this.readCardService = readCardService;
        this.notifier = notifier;
        this.terminalRepository = terminalRepository;
    }

    protected abstract void validateByRespectiveProcessor(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse);

    @Override
    public ReadCardResponse validateReadCardResponse(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse) {
        if (readCardResponse == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }

        if(checkForRetry(readCardResponse)){
            return retryByProcessor(readCardRequest,readCardResponse);
        }

        if (checkFallback(readCardResponse)) {
            return readCardFromFallbackProcessor(readCardRequest);
        }

        if (readCardResponse.getResult() == Result.TIMEOUT) {
            throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
        }

        if (readCardResponse.getResult() == Result.CARD_NOT_SUPPORTED) {
            throw new PosException(PosError.DEVICE_ERROR_CARD_NOT_SUPPORTED);
        }

        if (readCardResponse.getResult() == Result.OFFLINE_DECLINED) {
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_DECLINED);
        }

        validateByRespectiveProcessor(readCardRequest, readCardResponse);

        CardIdentifier cardIdentifier = new CardIdentifier(transactionRepository, applicationRepository);
        IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(readCardResponse);
        readCardResponse.getCardDetails().setCardScheme(identifyCardResponse.getCardDetails().getCardScheme());

        return readCardResponse;
    }

    protected abstract ReadCardResponse retryByProcessor(ReadCardRequest readCardRequest,ReadCardResponse readCardResponse);

    protected abstract boolean checkForRetry(ReadCardResponse readCardResponse);

    protected abstract ReadCardResponse readCardFromFallbackProcessor(ReadCardRequest readCardRequest);

    protected abstract boolean checkFallback(ReadCardResponse readCardResponse);

}
