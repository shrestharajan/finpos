package global.citytech.finpos.merchant.presentation.alertdialogs

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import kotlin.concurrent.thread

class CashInConfirmationAlertDialog constructor(
    var activity: Activity,
    amount: String,
    val bankLogo: ByteArray,
    val terminalRepository: TerminalRepositoryImpl
) {
    private lateinit var amount: TextView
    private var bankLogoDisplay: ByteArray = byteArrayOf()
    var bankLogoBitMap: Bitmap
    private lateinit var loader: ImageView
    private var alertDialog: AlertDialog
    var cashInAmount: String = ""

    init {
        val alertDialogBuilder = AlertDialog.Builder(activity, R.style.FullScreenDialog)
        val inflater = activity.layoutInflater
        val dialogView = inflater.inflate(R.layout.processing_card_deposited, null)
        alertDialogBuilder.setView(dialogView)
        alertDialogBuilder.setCancelable(false)
        cashInAmount = amount
        bankLogoDisplay = bankLogo
        bankLogoBitMap = BitmapFactory.decodeByteArray(bankLogoDisplay, 0, bankLogoDisplay.size)
        alertDialog = alertDialogBuilder.create()
        alertDialog.setCancelable(false)
        initViews(dialogView)
        setViewAttributes()

    }

    private fun initViews(dialogView: View) {
        val bankLogoDisplay = dialogView.findViewById<ImageView>(R.id.iv_bank_logo)
        bankLogoDisplay.setImageBitmap(bankLogoBitMap)
        loader = dialogView.findViewById<ImageView>(R.id.iv_loading_cash_deposit)
        amount = dialogView.findViewById<TextView>(R.id.npr_amount_processing)
        thread {
            dialogView.findViewById<TextView>(R.id.npr_processing).text =
                terminalRepository.terminalTransactionCurrencyName
        }

    }

    private fun setViewAttributes() {
        amount.text = cashInAmount
        Glide.with(activity).load("file:///android_asset/images/loading.gif")
            .into(loader)
    }

    fun show() {
        alertDialog.show()
    }

    fun hide() {
        alertDialog.dismiss()
    }


}