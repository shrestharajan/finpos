package global.citytech.finposframework.iso8583;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Surajchhetry Date: 2/19/20 Time: 1:49 PM
 */
public class MockDEConfig extends DataElementConfigs {

    @Override
    protected List<DataElementConfig> config() {
        List<DataElementConfig> deConfig  = new ArrayList<>();

        deConfig.add(
                new DataElementConfig(
                        1,
                        new DataConfig(DataConfig.DataType.ALPHA),
                        new LengthConfig(64, LengthConfig.LengthType.FIXED)));

        deConfig.add(
                new DataElementConfig(
                        2,
                        new DataConfig(DataConfig.DataType.ALPHA),
                        new LengthConfig(19, LengthConfig.LengthType.LLVAR)));

        deConfig.add(
                new DataElementConfig(
                        3,
                        new DataConfig(DataConfig.DataType.ALPHA),
                        new LengthConfig(6, LengthConfig.LengthType.FIXED)));

        deConfig.add(
                new DataElementConfig(
                        4,
                        new DataConfig(DataConfig.DataType.ALPHA),
                        new LengthConfig(16, LengthConfig.LengthType.FIXED)));

        deConfig.add(
                new DataElementConfig(
                        5,
                        new DataConfig(DataConfig.DataType.ALPHA),
                        new LengthConfig(12, LengthConfig.LengthType.FIXED)));

        deConfig.add(
                new DataElementConfig(
                        6,
                        new DataConfig(DataConfig.DataType.ALPHA),
                        new LengthConfig(12, LengthConfig.LengthType.FIXED)));

        deConfig.add(
                new DataElementConfig(
                        7,
                        new DataConfig(DataConfig.DataType.ALPHA),
                        new LengthConfig(10, LengthConfig.LengthType.FIXED)));

        deConfig.add(
                new DataElementConfig(
                        8,
                        new DataConfig(DataConfig.DataType.ALPHA),
                        new LengthConfig(8, LengthConfig.LengthType.FIXED)));

        deConfig.add(new DataElementConfig(
                35,
                new DataConfig(DataConfig.DataType.ALPHA, EncodingType.BCD),
                new LengthConfig(LengthConfig.LengthType.LLVAR,EncodingType.BCD),
                PaddingType.POSTFIX_WITH_CHAR_F
        ));
        deConfig.add(new DataElementConfig(
                55,
                new DataConfig(DataConfig.DataType.ALPHA,EncodingType.BCD),
                new LengthConfig(LengthConfig.LengthType.LLLVAR,EncodingType.BCD)
        ));

        return deConfig;
    }

    private void setupDataElement() {



    /*
    this.dataElements.put(3,new DataElement(43, 40, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(44, 25, DataElement.DataType.LLVAR));
    this.dataElements.put(3,new DataElement(45, 76, DataElement.DataType.LLVAR));
    this.dataElements.put(3,new DataElement(46, 300, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(47, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(48, 300, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(49, 3, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(50, 3, DataElement.DataType.ALPHA));

    this.dataElements.put(1, new DataElement(51, 3, DataElement.DataType.ALPHA));
    this.dataElements.put(2,new DataElement(52, 16, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(53, 18, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(54, 120, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(55, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(56, 43, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(57, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(58, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(59, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(60, 7, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(61, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(62, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(63, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(64, 16, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(65, 64, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(66, 1, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(67, 2, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(68, 3, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(69, 3, DataElement.DataType.ALPHA));
    this.dataElements.put(70,new DataElement(70, 3, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(71, 4, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(72, 999, DataElement.DataType.LLLVAR));

    this.dataElements.put(3,new DataElement(73, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(74, 10, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(75, 10, DataElement.DataType.ALPHA));

    this.dataElements.put(1, new DataElement(76, 10, DataElement.DataType.ALPHA));
    this.dataElements.put(2,new DataElement(77, 10, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(78, 10, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(80, 10, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(81, 10, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(82, 12, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(83, 12, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(84, 12, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(85, 12, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(86, 15, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(87, 15, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(88, 15, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(89, 15, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(90, 42, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(91, 1, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(92, 2, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));

    this.dataElements.put(1, new DataElement(1, 64, DataElement.DataType.ALPHA));
    this.dataElements.put(2,new DataElement(2, 19, DataElement.DataType.LLVAR));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));

    this.dataElements.put(1, new DataElement(1, 64, DataElement.DataType.ALPHA));
    this.dataElements.put(2,new DataElement(2, 19, DataElement.DataType.LLVAR));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    */

    }
}
