package global.citytech.finpos.merchant.presentation.model.qr.voidqr

data class QrVoidRequest(
    val merchant_pan: String,
    val terminal_id: String,
    val txn_number: String,
    val amount: String
)