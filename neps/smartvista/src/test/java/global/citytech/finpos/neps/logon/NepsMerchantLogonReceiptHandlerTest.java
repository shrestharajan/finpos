package global.citytech.finpos.neps.logon;

import org.junit.Before;
import org.powermock.api.mockito.PowerMockito;

import global.citytech.finpos.neps.receipt.logon.NepsMerchantLogonReceiptHandler;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.usecases.transaction.receipt.logon.MerchantLogonReceipt;
import global.citytech.finposframework.supports.Optional;

import static junit.framework.TestCase.assertEquals;

public class NepsMerchantLogonReceiptHandlerTest {

    IsoMessageResponse isoMessageResponse;
    Iso8583Msg iso8583Msg;
    DataElement dataElement;

    @Before
    public void setup() {
        isoMessageResponse = PowerMockito.mock(IsoMessageResponse.class);
        iso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        dataElement = PowerMockito.mock(DataElement.class);
    }

  //  @Test
    public void prepareWhenSuccess() {
        PowerMockito.when(dataElement.getData()).thenReturn("000");
        PowerMockito.when(iso8583Msg.getDataElementByIndex(39)).thenReturn(Optional.of(dataElement));
        PowerMockito.when(isoMessageResponse.getMsg()).thenReturn(iso8583Msg);

        NepsMerchantLogonReceiptHandler receiptHandler = new NepsMerchantLogonReceiptHandler(isoMessageResponse);
        MerchantLogonReceipt receipt = receiptHandler.prepare();

        assertEquals("SHOULD BE LOGON SUCCESSFUL", "LOGON SUCCESSFUL", receipt.getMessage());
    }

  //  @Test
    public void prepareWhenFailure() {
        PowerMockito.when(dataElement.getData()).thenReturn("800");
        PowerMockito.when(iso8583Msg.getDataElementByIndex(39)).thenReturn(Optional.of(dataElement));
        PowerMockito.when(isoMessageResponse.getMsg()).thenReturn(iso8583Msg);

        NepsMerchantLogonReceiptHandler receiptHandler = new NepsMerchantLogonReceiptHandler(isoMessageResponse);
        MerchantLogonReceipt receipt = receiptHandler.prepare();

        assertEquals("SHOULD BE LOGON FAILURE", "LOGON FAILURE", receipt.getMessage());
    }
}
