package global.citytech.finpos.processor.neps.receipt.duplicatereconciliation;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.duplicatereceipt.DuplicateReceiptRequestParameter;
import global.citytech.finposframework.switches.receipt.duplicatereconciliation.DuplicateReconciliationReceiptRequestParameter;

/**
 * Created by Saurav Ghimire on 6/16/21.
 * sauravnghimire@gmail.com
 */


public class DuplicateReconciliationReceiptRequest implements UseCase.Request, DuplicateReconciliationReceiptRequestParameter {

    private ReconciliationRepository reconciliationRepository;
    private PrinterService printerService;

    public DuplicateReconciliationReceiptRequest(ReconciliationRepository reconciliationRepository, PrinterService printerService) {
        this.reconciliationRepository = reconciliationRepository;
        this.printerService = printerService;
    }

    public ReconciliationRepository getReconciliationRepository() {
        return reconciliationRepository;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }
}
