package global.citytech.finposframework.switches.receipt.duplicatereceipt;

import global.citytech.finposframework.switches.RequestParameter;

/**
 * Created by Unique Shakya on 12/15/2020.
 */
public interface DuplicateReceiptRequestParameter extends RequestParameter {
}
