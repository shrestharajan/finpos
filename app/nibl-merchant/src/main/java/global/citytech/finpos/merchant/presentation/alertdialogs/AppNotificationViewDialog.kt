package global.citytech.finpos.merchant.presentation.alertdialogs

import android.app.Activity
import android.app.AlertDialog
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.domain.model.app.AppNotification
import global.citytech.finpos.merchant.domain.model.app.Type

/**
 * Created by Unique Shakya on 5/31/2021.
 */
class AppNotificationViewDialog constructor(
    private val activity: Activity, private val appNotification: AppNotification,
    private val listener: Listener
) {

    private lateinit var tvTitle: TextView
    private lateinit var tvMessage: TextView
    private lateinit var ivAppNotification: ImageView
    private lateinit var btnPositive: Button
    private lateinit var btnNegative: Button

    private var alertDialog: AlertDialog

    init {
        val alertDialogBuilder = AlertDialog.Builder(activity)
        val inflater = activity.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_app_notification_view, null)
        alertDialogBuilder.setView(dialogView)
        initViews(dialogView)
        setViewAttributes()
        handleClickEvents()
        alertDialog = alertDialogBuilder.create()
        alertDialog.setCancelable(false)
    }

    fun show() {
        alertDialog.show()
    }

    fun hide() {
        alertDialog.dismiss()
    }

    private fun handleClickEvents() {
        btnPositive.setOnClickListener {
            listener.onPositiveButtonClick()
            hide()
        }

        btnNegative.setOnClickListener {
            listener.onNegativeButtonClick()
            hide()
        }
    }

    private fun setViewAttributes() {
        tvTitle.text = appNotification.title
        tvMessage.text = appNotification.body
        ivAppNotification.setImageResource(appNotification.type.displayImageId)
        if (appNotification.type == Type.SUCCESS) {
            btnPositive.text = activity.getString(R.string.title_ok)
            btnPositive.visibility = View.VISIBLE
            btnNegative.visibility = View.GONE
        } else {
            btnPositive.text = appNotification.action.positiveAction
            btnNegative.text = appNotification.action.negativeAction
            if (appNotification.action.positiveAction.isNullOrEmptyOrBlank())
                btnPositive.visibility = View.GONE
            if (appNotification.action.negativeAction.isNullOrEmptyOrBlank())
                btnNegative.visibility = View.GONE
        }
    }

    private fun initViews(dialogView: View) {
        tvTitle = dialogView.findViewById(R.id.tv_title)
        tvMessage = dialogView.findViewById(R.id.tv_message)
        ivAppNotification = dialogView.findViewById(R.id.iv_app_notification)
        btnPositive = dialogView.findViewById(R.id.btn_positive)
        btnNegative = dialogView.findViewById(R.id.btn_negative)
    }

    interface Listener {
        fun onPositiveButtonClick()
        fun onNegativeButtonClick()
    }
}