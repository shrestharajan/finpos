package global.citytech.finpos.merchant.extensions

import android.content.ContentValues
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.View
import android.view.inputmethod.InputMethodManager
import global.citytech.easydroid.core.utils.Jsons

/**
 * Created by Saurav Ghimire on 4/15/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


fun ContentValues.putValue(key: String, value: String?) {
    if(value != null){
        this.put(key,value)
    }
}

fun ContentValues.putValue(key: String, value: Int?) {
    if (value != null) {
        this.put(key, value)
    }
}

fun ContentValues.putAsJson(key: String, value: Any?) {
    if (value != null) {
        this.put(key, Jsons.toJsonObj(value))
    }

}