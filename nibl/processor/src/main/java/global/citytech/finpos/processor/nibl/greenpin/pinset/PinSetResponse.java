package global.citytech.finpos.processor.nibl.greenpin.pinset;

import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;

public class PinSetResponse extends TransactionResponseModel {

    private String debugRequestMessage;
    private String debugResponseMessage;
    private String message;
    private String stan;
    private boolean approved;
    private boolean shouldPrintCustomerCopy;
    private ReadCardResponse readCardResponse;

    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }

    public boolean isApproved() {
        return approved;
    }

    public String getMessage() {
        return message;
    }


    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private String message;
        private String stan;
        private boolean approved;
        private boolean shouldPrintCustomerCopy;
        private ReadCardResponse readCardResponse;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withDebugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public Builder withDebugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder withShouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public Builder withApproved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public Builder withReadCardResponse(ReadCardResponse readCardResponse) {
            this.readCardResponse = readCardResponse;
            return this;
        }

        public PinSetResponse build() {
            PinSetResponse PinSetResponseModel = new PinSetResponse();
            PinSetResponseModel.debugRequestMessage = this.debugRequestMessage;
            PinSetResponseModel.approved = this.approved;
            PinSetResponseModel.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            PinSetResponseModel.stan = this.stan;
            PinSetResponseModel.debugResponseMessage = this.debugResponseMessage;
            PinSetResponseModel.message = this.message;
            PinSetResponseModel.readCardResponse = this.readCardResponse;
            return PinSetResponseModel;
        }
    }
}
