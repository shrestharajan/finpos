package global.citytech.finpos.processor.neps.batchupload;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.batchupload.BatchUploadRequestParameter;

/**
 * Created by Saurav Ghimire on 6/14/21.
 * sauravnghimire@gmail.com
 */


public class BatchUploadRequestModel implements UseCase.Request, BatchUploadRequestParameter {
    private final ApplicationRepository applicationRepository;
    private final TransactionRepository transactionRepository;
    private final PrinterService printerService;

    public BatchUploadRequestModel(
            ApplicationRepository applicationRepository,
            TransactionRepository transactionRepository,
            PrinterService printerService
    ) {
        this.applicationRepository = applicationRepository;
        this.transactionRepository = transactionRepository;
        this.printerService = printerService;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }

    public ApplicationRepository getApplicationRepository() {
        return applicationRepository;
    }
}
