package global.citytech.finpos.processor.nibl.batchupload;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.batchupload.BatchUploadResponseParameter;

/**
 * Created by Unique Shakya on 5/11/2021.
 */
public class BatchUploadResponseModel implements UseCase.Response, BatchUploadResponseParameter {

    private Result result;
    private String message;

    public BatchUploadResponseModel(Result result, String message) {
        this.result = result;
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }
}
