package global.citytech.finposframework.switches.tipadjustment;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 5/4/2021.
 */
public interface TipAdjustmentRequester<T extends TipAdjustmentRequestParameter, E extends TipAdjustmentResponseParameter>
        extends Request<T, E> {
}
