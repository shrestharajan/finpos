package global.citytech.finpos.merchant.presentation.model.balanceinquiry

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

class BalanceInquiryRequestItem(
    val transactionType: TransactionType? = null,
    val amount: BigDecimal? = null,
)