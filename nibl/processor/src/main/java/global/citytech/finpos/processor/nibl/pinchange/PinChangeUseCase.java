package global.citytech.finpos.processor.nibl.pinchange;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finpos.nibl.iso8583.S2MIccDataList;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.pinchange.PinChangeRequestSender;
import global.citytech.finpos.nibl.iso8583.requestsender.pinchange.PinChangeRequest;
import global.citytech.finpos.processor.nibl.NiblConstants;
import global.citytech.finpos.processor.nibl.transaction.TransactionRequestModel;
import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor;
import global.citytech.finpos.processor.nibl.transaction.card.MagneticCardProcessor;
import global.citytech.finpos.processor.nibl.transaction.card.PICCCardProcessor;
import global.citytech.finpos.processor.nibl.transaction.receipt.NiblIsoMessageReceiptHandler;
import global.citytech.finpos.processor.nibl.transaction.template.CardTransactionUseCase;
import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils;
import global.citytech.finpos.processor.nibl.transaction.validator.NiblTransactionValidator;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardSummary;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.PinBlock;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.led.LedAction;
import global.citytech.finposframework.hardware.io.led.LedLight;
import global.citytech.finposframework.hardware.io.led.LedRequest;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat;
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest;
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.listeners.CardConfirmationListener;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.CardProcessor;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.PosResponse;
import global.citytech.finposframework.utility.PosResult;
import global.citytech.finposframework.utility.StringUtils;


public class PinChangeUseCase extends CardTransactionUseCase {
    IsoMessageResponse isoMessageResponse;
    private String userPin = "";
    TransactionResponseModel transactionResponse;
    private String oldPin = "";
    private CardProcessor cardProcessor;

    protected final ReadCardRequest.MultipleAidSelectionListener multipleAidSelectionListener = aids -> {
        notifier.notify(Notifier.EventType.DETECTED_MULTIPLE_AID, "Processing..");
        return -1;
    };

    public PinChangeUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    protected String getTransactionTypeClassName() {
        return null;
    }

    @Override
    public TransactionResponseModel execute(TransactionRequestModel request) {
        this.request = request;
        this.transactionRequest = this.request.getTransactionRequest();
        if (this.deviceNotReady())
            throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
        this.transactionValidator = new NiblTransactionValidator(this.request.getApplicationRepository());
        this.stan = this.terminalRepository.getSystemTraceAuditNumber();
        try {
            this.initTransaction();
            return this.initializeTransactionWithCard(TransactionUtils.getAllowedCardEntryModes(this.transactionRequest.getTransactionType()), false);
        } catch (PosException pe) {
            pe.printStackTrace();
            throw pe;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } finally {
            this.cleanup();
        }
    }

    protected boolean deviceNotReady() {
        PosResponse deviceReadyResponse = this.request.getDeviceController().isReady(TransactionUtils
                .getAllowedCardEntryModes(this.transactionRequest.getTransactionType()));
        if (deviceReadyResponse.getPosResult() == PosResult.SUCCESS) {
            return false;
        }
        this.notifier.notify(Notifier.EventType.DETECT_CARD_ERROR);
        throw new PosException(deviceReadyResponse.getPosError());
    }

    private TransactionResponseModel initializeTransactionWithCard(List<CardType> allowedCardEntryModes, boolean isForceContactCardPrompt) throws InterruptedException {
        this.retrieveCardDetails(allowedCardEntryModes, isForceContactCardPrompt);
        if (this.invalidTransaction())
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
        if (this.isTransactionAuthorizationFailure()) {
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
        }
        if (isCardConfirmationRequired) {
            final boolean[] isConfirm = {false};
            CountDownLatch countDownLatch = new CountDownLatch(1);
            this.notifier.notify(Notifier.EventType.CARD_CONFIRMATION_EVENT,
                    readCardResponse.getCardDetails(),
                    (CardConfirmationListener) confirmed -> {
                        isConfirm[0] = confirmed;
                        countDownLatch.countDown();
                    });
            countDownLatch.await();
            if (!isConfirm[0]) {
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            }
        }
        try {
            processCard();
            return transactionResponse;
        } catch (PosException e) {
            throw new PosException(e.getPosError());
        }
    }

    protected PurchaseRequest preparePurchaseRequest(TransactionRequest transactionRequest, CardDetails cardDetails) {
        PurchaseRequest purchaseRequest = new PurchaseRequest(
                transactionRequest.getTransactionType(),
                transactionRequest.getAmount(),
                transactionRequest.getAdditionalAmount()
        );
        cardDetails.setAmount(transactionRequest.getAmount());
        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
        purchaseRequest.setCardDetails(cardDetails);
        return purchaseRequest;
    }

    @Override
    protected void processCard() {
        PurchaseRequest purchaseRequest = preparePurchaseRequest(
                transactionRequest,
                readCardResponse.getCardDetails()
        );
        this.notifier.notify(Notifier.EventType.PROCESSING, "Please wait...");
        PinBlock oldPinBlockResponse = this.retrieveOldPinBlock(purchaseRequest);
        PinBlock newPinBlockResponse = this.retrieveInitialPinBlock(purchaseRequest);
        transactionResponse = proceedConfirmPin(purchaseRequest, newPinBlockResponse);
    }


    protected void retrieveCardDetails(List<CardType> allowedCardTypes, boolean isForceContactCardPrompt) {
        ReadCardRequest readCardRequest = this.prepareReadCardRequest(allowedCardTypes);
        this.notifier.notify(Notifier.EventType.STARTING_READ_CARD);
        this.readCardResponse = this.detectCard(readCardRequest);
        this.retrieveCardProcessor();
        this.readCardResponse = this.cardProcessor.validateReadCardResponseForICC(readCardRequest, this.readCardResponse);
    }

    private void retrieveCardProcessor() {
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case MAG:
                this.cardProcessor = new MagneticCardProcessor(
                        this.request.getTransactionRepository(),
                        this.request.getTransactionAuthenticator(),
                        this.request.getApplicationRepository(),
                        this.request.getReadCardService(),
                        this.notifier,
                        terminalRepository
                );
                break;
            case ICC:
                this.cardProcessor = new IccCardProcessor(
                        this.request.getReadCardService(),
                        this.request.getTransactionRepository(),
                        this.request.getApplicationRepository(),
                        this.request.getTransactionAuthenticator(),
                        this.notifier,
                        terminalRepository
                );
                break;
            case PICC:
                this.cardProcessor = new PICCCardProcessor(
                        this.request.getTransactionRepository(),
                        this.request.getApplicationRepository(),
                        this.request.getReadCardService(),
                        this.notifier,
                        this.request.getTransactionAuthenticator(),
                        this.request.getLedService(),
                        this.request.getSoundService(),
                        terminalRepository
                );
                break;
            default:
                break;
        }
    }

    private ReadCardRequest prepareReadCardRequest(List<CardType> allowedCardTypes) {
        TransactionType transactionType = this.transactionRequest.getTransactionType();
        ReadCardRequest readCardRequest = new ReadCardRequest(
                allowedCardTypes,
                transactionType,
                TransactionUtils.retrieveTransactionType9C(transactionType),
                this.request.getApplicationRepository().retrieveEmvParameterRequest()
        );
        readCardRequest.setStan(this.stan);
        readCardRequest.setAmount(this.transactionRequest.getAmount());
        readCardRequest.setCashBackAmount(this.transactionRequest.getAdditionalAmount());
        readCardRequest.setTransactionDate(DateUtils.yyMMddDate());
        readCardRequest.setTransactionTime(DateUtils.HHmmssTime());
        readCardRequest.setMultipleAidSelectionListener(this.multipleAidSelectionListener);
        readCardRequest.setIccDataList(S2MIccDataList.get());
        readCardRequest.setCurrencyName(NiblConstants.PIN_PAD_CURRENCY);
        readCardRequest.setPinPadFixedLayout(!terminalRepository.shouldShufflePinPad());
        return readCardRequest;
    }

    private ReadCardResponse detectCard(ReadCardRequest readCardRequest) {
        this.request.getLedService().doTurnLedWith(new LedRequest(LedLight.BLUE, LedAction.ON));
        ReadCardResponse readCardResponse = this.request.getReadCardService().readCardDetails(readCardRequest);
        this.validateReadCardResponse(readCardResponse);
        return readCardResponse;
    }

    private void validateReadCardResponse(ReadCardResponse readCardResponse) {
        if (readCardResponse == null)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        if (readCardResponse.getResult() == Result.TIMEOUT)
            throw new PosException(PosError.DEVICE_ERROR_CARD_READ_TIMEOUT);
        if (readCardResponse.getResult() == Result.USER_CANCELLED)
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        if (readCardResponse.getResult() == Result.FAILURE)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
    }

    protected PinBlock retrieveOldPinBlock(PurchaseRequest purchaseRequest) {
        PinBlock oldPinBlock = new PinBlock("", true);
        String pinPadAmountMessage = "";
        oldPinBlock = this.retrievePinBlock(
                purchaseRequest
                        .getCardDetails()
                        .getPrimaryAccountNumber(),
                pinPadAmountMessage,
                "Enter Old Pin",
                "Enter Old PIN"
        );
        oldPin = oldPinBlock.getPin();
        return oldPinBlock;

    }

    protected PinBlock retrieveInitialPinBlock(PurchaseRequest purchaseRequest) {
        PinBlock newPinBlock = new PinBlock("", true);
        String pinPadAmountMessage = "";
        newPinBlock = this.retrievePinBlock(
                purchaseRequest
                        .getCardDetails()
                        .getPrimaryAccountNumber(),
                pinPadAmountMessage,
                "Enter New Pin",
                "Enter PIN"
        );
        return newPinBlock;
    }

    protected PinBlock retrieveConfirmPinBlock(PurchaseRequest purchaseRequest) {
        PinBlock pinBlock = new PinBlock("", true);
        String pinPadAmountMessage = "";
        pinBlock = this.retrievePinBlock(
                purchaseRequest
                        .getCardDetails()
                        .getPrimaryAccountNumber(),
                pinPadAmountMessage,
                "Confirm Pin",
                "Confirm PIN"
        );
        return pinBlock;
    }

    private PinBlock retrievePinBlock(String primaryAccountNumber, String pinPadAmountMessage, String pinPadMessage, String title) {
        if (!StringUtils.isEmpty(primaryAccountNumber)) {
            CardSummary cardSummary = new CardSummary(readCardResponse.getCardDetails().getCardSchemeLabel(),
                    primaryAccountNumber,
                    readCardResponse.getCardDetails().getCardHolderName(),
                    readCardResponse.getCardDetails().getExpiryDate());
            PinRequest pinRequest = new PinRequest(
                    primaryAccountNumber,
                    pinPadMessage,
                    PinBlockFormat.ISO9564_FORMAT_0,
                    4,
                    4,
                    30,
                    pinPadAmountMessage,
                    !this.terminalRepository.shouldShufflePinPad(),
                    true,
                    title
            );
            pinRequest.setCardSummary(cardSummary);
            PinResponse response = request.getTransactionAuthenticator().authenticateUser(pinRequest);
            this.logger.debug("::: PIN RESPONSE ::: " + response.toString());
            if (response.getResult() == Result.SUCCESS) {
                return response.getPinBlock();
            } else if (response.getResult() == Result.USER_CANCELLED)
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            else if (response.getResult() == Result.TIMEOUT || response.getResult() == Result.FAILURE) {
                throw new PosException(PosError.DEVICE_ERROR_CARD_READ_TIMEOUT);
            } else
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    TransactionResponseModel proceedConfirmPin(PurchaseRequest purchaseRequest, PinBlock pinBlock) {
        PinBlock confirmPinBlockResponse = this.retrieveConfirmPinBlock(purchaseRequest);
        logger.log("Pin 1 " + pinBlock.getPin());
        logger.log("Pin 2 " + confirmPinBlockResponse.getPin());
        if (confirmPinBlockResponse.getPin().equals(pinBlock.getPin())) {
            userPin = confirmPinBlockResponse.getPin();
            logger.log("PIN MATCHED");
            this.notifier.notify(Notifier.EventType.PROCESSING, "Transmitting Request...");
            try {
                isoMessageResponse = sendIsoMessage();
                printDebugReceiptIfDebugMode(request.getPrinterService(), isoMessageResponse);
                InvalidResponseHandlerResponse invalidResponseHandlerResponse = this.checkForInvalidResponseFromHost(isoMessageResponse);
                if (invalidResponseHandlerResponse.isInvalid()) {
                    handleInvalidResponseFromHost(invalidResponseHandlerResponse);
                }
                boolean isApproved = this.isTransactionApprovedByActionCode(isoMessageResponse);

                System.out.println("PIN CHANGE ISO RESPONSE" + isApproved);

                this.handleTransactionCompletion(false, isoMessageResponse, Notifier.TransactionType.CARD.getDescription());
                if (isApproved) {
                    return this.prepareTransactionResponse(isoMessageResponse, isApproved, false, "");
                } else {
                    return this.prepareDeclinedTransactionResponse("Pin Change not successful.",
                            false);
                }
            } catch (FinPosException fpe) {
                if (this.unableToGoOnlineException(fpe))
                    throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
                else
                    throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
            }


        } else {
            logger.log("PIN NOT MATCHED");
            this.notifier.notify(Notifier.EventType.PIN_UNMATCH, "Please wait...");
            return proceedConfirmPin(purchaseRequest, pinBlock);
        }
    }

    private IsoMessageResponse sendIsoMessage() {
        RequestContext context = this.prepareTemplateRequestContext();
        this.transactionIsoRequest = this.prepareTransactionIsoRequest();
        context.setRequest(transactionIsoRequest);
        NIBLMessageSenderTemplate sender = this.getRequestSender(context);
        return sender.send();
    }

    protected void printDebugReceiptIfDebugMode(PrinterService printerService, IsoMessageResponse isoMessageResponse) {
        boolean isDebugVersion = this.terminalRepository.findTerminalInfo().isDebugModeEnabled();
        if (isDebugVersion) {
            ReceiptHandler.IsoMessageReceiptHandler isoMessageReceiptHandler
                    = new NiblIsoMessageReceiptHandler(printerService);
            isoMessageReceiptHandler.printIsoMessageReceipt(isoMessageResponse);
        }
    }

    protected TransactionResponseModel handleInvalidResponseFromHost(InvalidResponseHandlerResponse invalidResponseHandlerResponse) {
        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, invalidResponseHandlerResponse.getMessage());
        boolean shouldPrint = false;
        return this.prepareDeclinedTransactionResponse(invalidResponseHandlerResponse.getMessage(),
                shouldPrint);
    }

    protected RequestContext prepareTemplateRequestContext() {
        HostInfo primaryHostInfo = this.terminalRepository.findPrimaryHost();
        HostInfo secondaryHostInfo = this.terminalRepository.findSecondaryHost();
        TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
        ConnectionParam connectionParam = new ConnectionParam(primaryHostInfo, secondaryHostInfo);
        return new RequestContext(this.stan, terminalInfo, connectionParam);
    }

    protected TransactionIsoRequest prepareTransactionIsoRequest() {
        return PinChangeRequest.Builder
                .newInstance()
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .transactionAmount(BigDecimal.valueOf(0.00))
                .localDate(HelperUtils.getDefaultLocaleDateWithoutYear())
                .localTime(HelperUtils.getDefaultLocaleTimeHhMmSs())
                .expireDate(readCardResponse.getCardDetails().getExpiryDate())
                .posEntryMode(this.retrievePosEntryMode())
                .posConditionCode("00")
                .track2Data(readCardResponse.getCardDetails().getTrackTwoData())
                .additionalData(userPin)
                .rrn("")
                .pinBlock(oldPin)
                .currencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .emvData(readCardResponse.getCardDetails().getIccDataBlock())
                .cardSequenceNumber(readCardResponse.getCardDetails().getPrimaryAccountNumberSerialNumber())
                .build();
    }

    private PosEntryMode retrievePosEntryMode() {
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case MAG:
                if (this.readCardResponse.getFallbackIccToMag())
                    return PosEntryMode.ICC_FALLBACK_TO_MAGNETIC;
                else
                    return PosEntryMode.MAGNETIC_STRIPE;
            case ICC:
                return PosEntryMode.ICC;
            case PICC:
                return PosEntryMode.PICC;
            default:
                return PosEntryMode.UNSPECIFIED;
        }
    }

    @Override
    protected NIBLMessageSenderTemplate getRequestSender(RequestContext context) {
        return new PinChangeRequestSender(new NIBLSpecInfo(), context);
    }

    @Override
    protected String getOriginalPosConditionCode() {
        return null;
    }

    @Override
    protected PosEntryMode getOriginalPosEntryMode() {
        return null;
    }

    @Override
    protected String getProcessingCode() {
        return null;
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy, String miniStatementData) {
        return PinChangeResponse.Builder.newInstance()
                .withStan(stan)
                .withApproved(isApproved)
                .withMessage(isApproved ? "Your Pin has been updated." : "Cannot perform Task").build();
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy) {
        return null;
    }


    @Override
    protected TransactionResponseModel prepareDeclinedTransactionResponse(String message, boolean shouldPrint) {
        return PinChangeResponse.Builder.newInstance()
                .withMessage(this.retrieveMessage(isoMessageResponse, false))
                .build();
    }

    @Override
    protected String getOriginalTransactionReferenceNumber() {
        return null;
    }

    @Override
    protected boolean isAuthorizationCompleted() {
        return false;
    }

    @Override
    protected void updateTransactionLogOfOriginalTransaction(boolean isApproved) {

    }

    @Override
    protected boolean shouldIncrementInvoiceNumber(boolean isApproved) {
        return false;
    }

    @Override
    protected TransactionLog retrieveTransactionLogToStore(boolean isApproved, IsoMessageResponse isoMessageResponse) {
        return null;
    }
}
