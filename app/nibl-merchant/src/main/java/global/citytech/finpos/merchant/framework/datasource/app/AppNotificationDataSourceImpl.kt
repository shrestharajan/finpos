package global.citytech.finpos.merchant.framework.datasource.app

import android.content.Context
import global.citytech.easydroid.core.preference.SecurePreference
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.data.datasource.app.alert.AppNotificationDataSource
import global.citytech.finpos.merchant.domain.model.app.Action
import global.citytech.finpos.merchant.domain.model.app.AppNotification

/**
 * Created by Unique Shakya on 5/28/2021.
 */
class AppNotificationDataSourceImpl(context: Context) : AppNotificationDataSource {

    private var securePreference: SecurePreference

    init {
        securePreference = SecurePreference(context.applicationContext, PREFERENCE_NAME)
    }

    override fun noNotifications(): Boolean {
        return getNotificationCount().toInt() == 0
    }

    override fun getNotificationCount(): Long {
        return getAllNotifications().size.toLong()
    }

    override fun getAllNotifications(): MutableList<AppNotification> {
        val data = getAsString()
        data?.let {
            return Jsons.fromJsonToList<AppNotification>(data, Array<AppNotification>::class.java)
                .toMutableList()
        }
        return emptyList<AppNotification>().toMutableList()
    }

    override fun removeNotification(appNotification: AppNotification) {
        val list = getAllNotifications()
        if (list.isNotEmpty()) {
            val copyList = copy(list)
            copyList.remove(appNotification)
            update(copyList)
        }
    }

    override fun removeNotificationByAction(action: Action) {
        val list = getAllNotifications()
        if (list.isNotEmpty()) {
            val copyList = copy(list)
            list.forEach {
                if (it.action == action)
                    copyList.remove(it)
            }
            update(copyList)
        }
    }

    override fun addNotification(appNotification: AppNotification) {
        var list = getAllNotifications()
        if (list.isEmpty())
            list = mutableListOf()
        val copyList = copy(list)
        list.forEach {
            if (it.action == Action.DISCLAIMER) {
                if (it.referenceId == appNotification.referenceId)
                    copyList.remove(it)
            } else {
                if (it.action == appNotification.action)
                    copyList.remove(it)
            }
        }
        copyList.add(appNotification)
        copyList.sortWith(AlertListComparator())
        update(copyList)
    }

    override fun removeNotificationByReferenceId(referenceId: String) {
        val list = getAllNotifications()
        if (list.isNotEmpty()) {
            val copyList = copy(list)
            list.forEach {
                if (it.referenceId == referenceId)
                    copyList.remove(it)
            }
            update(copyList)
        }
    }

    override fun clearSharedPreference() {
        securePreference.clear()
    }

    private fun copy(list: MutableList<AppNotification>): MutableList<AppNotification> {
        val copyList = mutableListOf<AppNotification>()
        list.forEach {
            copyList.add(it)
        }
        return copyList.toMutableList()
    }

    private fun getAsString(): String? {
        return securePreference.retrieveData(PREFERENCE_KEY, null)
    }

    private fun update(list: MutableList<AppNotification>) {
        val data = Jsons.toJsonList(list)
        securePreference.saveData(PREFERENCE_KEY, data)
    }

    companion object {
        const val PREFERENCE_NAME = "ALERT-STORE"
        const val PREFERENCE_KEY = "ALERT-KEY"
    }

    internal class AlertListComparator : Comparator<AppNotification> {
        override fun compare(p0: AppNotification?, p1: AppNotification?): Int {
            val timeStamp1 = p0!!.timeStamp.toLong()
            val timeStamp2 = p1!!.timeStamp.toLong()
            return (timeStamp2 - timeStamp1).toInt()
        }
    }
}