package global.citytech.finpos.nibl.iso8583.requestsender.refund;

import org.junit.Test;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.NIBLHostConfig;
import global.citytech.finpos.nibl.iso8583.MockValues;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Rishav Chudal on 10/16/20.
 */
public class MagRefundRequestSenderTest {
    private Logger logger = Logger.getLogger(MagRefundRequestSenderTest.class.getName());

    @Test
    public void send() {
        try {
            String terminalID = "99991002";
            String acquirerID = "405635000000002";
            TerminalInfo terminalInfo = new TerminalInfo(
                    terminalID,
                    acquirerID,"",
                    "Citytech",
                    "Kamaladi"
            );
            HostInfo primaryHost = NIBLHostConfig.getPrimaryConfig();
            HostInfo secondaryHost = NIBLHostConfig.getSecondaryConfig();
            ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);
            RequestContext context = new RequestContext(
                    "000405",
                    terminalInfo,
                    connectionParam
            );

            RefundRequest request = new RefundRequest();
            request.setTransactionAmount(105.0);
            request.setPosConditionCode("00");
            request.setOriginalRetrievalReferenceNumber("000000000001");
            request.setPinBlock("c247220c967c1d");
            request.setInvoiceNumber("000090");
            request.setTrack2Data(MockValues.TRACK_2_DATA);
            request.setEntryMode(PosEntryMode.MAGNETIC_STRIPE);

            context.setRequest(request);

            MagRefundRequestSender sender = new MagRefundRequestSender(
                    new NIBLSpecInfo(),
                    context
            );

            IsoMessageResponse response = sender.send();
            assertNotNull(response);

        } catch (FinPosException e) {
            this.logger.log(e.getDetailMessage());
            this.logger.log(e.getMessage());
        }
    }
}