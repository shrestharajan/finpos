package global.citytech.finpos.merchant.framework.datasource.core.refund

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.refund.RefundDataSource
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionAuthenticatorImpl
import global.citytech.finpos.merchant.framework.datasource.device.*
import global.citytech.finpos.merchant.domain.model.purchase.RefundRequestEntity
import global.citytech.finpos.merchant.domain.model.purchase.RefundResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.data.mapToRefundResponseUiModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.refund.ManualRefundRequestItem
import global.citytech.finpos.merchant.presentation.model.refund.RefundRequestItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finposframework.switches.ProcessorManager
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/28/2020.
 */
class RefundDataSourceImpl : RefundDataSource {

    override fun refund(
        configurationItem: ConfigurationItem,
        refundRequestItem: RefundRequestItem
    ): Observable<RefundResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val transactionRepository = TransactionRepositoryImpl(NiblMerchant.INSTANCE)
        val transactionRequest = prepareTransactionRequest(refundRequestItem)
        val deviceController = DeviceControllerImpl(NiblMerchant.INSTANCE)
        val cardReader = CardSourceImpl(NiblMerchant.getActivityContext())
        val transactionAuthenticator =
            TransactionAuthenticatorImpl()
        val printerService = PrinterSourceImpl(NiblMerchant.INSTANCE)
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
        val refundRequestEntity = RefundRequestEntity(
            transactionRequest = transactionRequest,
            applicationRepository = applicationRepository,
            deviceController = deviceController,
            printerService = printerService,
            readCardService = cardReader,
            transactionAuthenticator = transactionAuthenticator,
            transactionRepository = transactionRepository,
            ledService = LedSourceImpl(NiblMerchant.INSTANCE),
            soundService = SoundSourceImpl(NiblMerchant.INSTANCE)
        )
        val refundRequester = ProcessorManager.getInterface(terminalRepository, NotificationHandler)
            .refundRequester

        return Observable.fromCallable {
            (refundRequester.execute(refundRequestEntity.mapToModel())).mapToRefundResponseUiModel()
        }
    }

    override fun manualRefund(
        configurationItem: ConfigurationItem,
        manualRefundRequestItem: ManualRefundRequestItem
    ): Observable<RefundResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val transactionRepository = TransactionRepositoryImpl(NiblMerchant.INSTANCE)
        val transactionRequest = prepareManualTransactionRequest(manualRefundRequestItem)
        val deviceController = DeviceControllerImpl(NiblMerchant.INSTANCE)
        val cardReader = CardSourceImpl(NiblMerchant.getActivityContext())
        val transactionAuthenticator =
            TransactionAuthenticatorImpl()
        val printerService = PrinterSourceImpl(NiblMerchant.INSTANCE)
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
        val refundRequestEntity = RefundRequestEntity(
            transactionRequest = transactionRequest,
            applicationRepository = applicationRepository,
            deviceController = deviceController,
            printerService = printerService,
            readCardService = cardReader,
            transactionAuthenticator = transactionAuthenticator,
            transactionRepository = transactionRepository
        )
        val refundRequester = ProcessorManager.getInterface(terminalRepository, NotificationHandler)
            .manualRefundRequester
        return Observable.fromCallable {
            (refundRequester.execute(refundRequestEntity.mapToModel())).mapToRefundResponseUiModel()
        }
    }

    private fun prepareManualTransactionRequest(
        manualRefundRequestItem: ManualRefundRequestItem
    ): TransactionRequest {
        val transactionRequest = TransactionRequest(
            manualRefundRequestItem.transactionType
        )
        transactionRequest.amount = manualRefundRequestItem.amount!!
        transactionRequest.cardNumber = manualRefundRequestItem.cardNumber
        transactionRequest.expiryDate = manualRefundRequestItem.expiryDate
        transactionRequest.originalRetrievalReferenceNumber =
            manualRefundRequestItem.originalRetrievalReferenceNumber
        transactionRequest.cvv = manualRefundRequestItem.cvv
        return transactionRequest
    }

    private fun prepareTransactionRequest(refundRequestItem: RefundRequestItem): TransactionRequest {
        val transactionRequest = TransactionRequest(
            refundRequestItem.transactionType
        )
        transactionRequest.amount = refundRequestItem.amount!!
        transactionRequest.originalRetrievalReferenceNumber =
            refundRequestItem.originalRetrievalReferenceNumber
        return transactionRequest
    }
}