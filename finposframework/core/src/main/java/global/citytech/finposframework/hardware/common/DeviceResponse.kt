package global.citytech.finposframework.hardware.common

/**
 * Created by Rishav Chudal on 4/8/20.
 */
open class DeviceResponse(val result: Result, val message: String)