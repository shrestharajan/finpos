package global.citytech.neps.presentation.splash

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.finpos.core.api.common.DeviceResponse
import global.citytech.finpos.core.api.common.DeviceResult
import global.citytech.neps.data.device.configuration.ConfigurationImpl
import global.citytech.neps.domain.model.LoadParameterRequest
import global.citytech.neps.domain.usecase.device.configuration.DeviceConfigurationUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Rishav Chudal on 6/24/20.
 */
class SplashViewModel(val context: Application):
    BaseAndroidViewModel(context) {

    val isTerminalConfigured by lazy { MutableLiveData<Boolean>() }
    val errorMessage by lazy { MutableLiveData<String>() }

    var deviceConfigurationUseCase: DeviceConfigurationUseCase =
        DeviceConfigurationUseCase(ConfigurationImpl(context))

    fun initializeSdk() {
        isLoading.value = true
        compositeDisposable.add(
            deviceConfigurationUseCase.initSdk()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSdkInitializedResponse, this::onErrorInSdkInitialize)
        )
    }

    private fun onSdkInitializedResponse(deviceResponse: DeviceResponse) {
        if (deviceResponse.result == DeviceResult.SUCCESS) {
            configureTerminal()
        } else {
            isLoading.value = false
        }
    }

    private fun onErrorInSdkInitialize(throwable: Throwable) {
        isLoading.value = false
        errorMessage.value = throwable.message
    }

    private fun configureTerminal() {
        isLoading.value = true
        compositeDisposable.add(
            deviceConfigurationUseCase.loadParameters(LoadParameterRequest())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::onConfigureTerminalResponse,
                    this::onCongifureTerminalError
                )
        )
    }

    private fun onConfigureTerminalResponse(deviceResponse: DeviceResponse) {
        isLoading.value = false
        isTerminalConfigured.value = deviceResponse.result == DeviceResult.SUCCESS
    }

    private fun onCongifureTerminalError(throwable: Throwable){
        isLoading.value = false
        errorMessage.value = throwable.message
    }

}