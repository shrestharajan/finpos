package global.citytech.finpos.merchant.data.datasource.app.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import global.citytech.finpos.merchant.domain.model.app.Disclaimer

@Dao
interface DisclaimerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(disclaimer: Disclaimer)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(disclaimers: List<Disclaimer>)

    @Query("SELECT ${Disclaimer.COLUMN_ID}, ${Disclaimer.COLUMN_SERVICE_ID},${Disclaimer.COLUMN_SERVICE_NAME},${Disclaimer.COLUMN_NOTIFICATION_TITLE},${Disclaimer.COLUMN_NOTIFICATION_CONTENT},${Disclaimer.COLUMN_DISCLAIMER_INFO},${Disclaimer.COLUMN_DISCLAIMER_CONTENT},${Disclaimer.COLUMN_REMARKS} FROM ${Disclaimer.TABLE_NAME}")
    fun retrieveAll(): List<Disclaimer>

    @Query("SELECT ${Disclaimer.COLUMN_ID}, ${Disclaimer.COLUMN_SERVICE_ID},${Disclaimer.COLUMN_SERVICE_NAME},${Disclaimer.COLUMN_NOTIFICATION_TITLE},${Disclaimer.COLUMN_NOTIFICATION_CONTENT},${Disclaimer.COLUMN_DISCLAIMER_INFO},${Disclaimer.COLUMN_DISCLAIMER_CONTENT},${Disclaimer.COLUMN_REMARKS} FROM ${Disclaimer.TABLE_NAME} WHERE ${Disclaimer.COLUMN_ID} = :disclaimerId")
    fun retrieveById(disclaimerId: String): Disclaimer

    @Query("UPDATE ${Disclaimer.TABLE_NAME} SET ${Disclaimer.COLUMN_REMARKS} = :remarks WHERE ${Disclaimer.COLUMN_ID} = :disclaimerId")
    fun updateRemarksById(disclaimerId: String, remarks: String): Int

    @Query("DELETE FROM ${Disclaimer.TABLE_NAME} WHERE ${Disclaimer.COLUMN_ID} = :disclaimerId")
    fun deleteById(disclaimerId: String): Int

    @Query("DELETE FROM ${Disclaimer.TABLE_NAME} WHERE ${Disclaimer.COLUMN_ID} in (:ids)")
    fun deleteByIds(ids: List<String>): Int

    @Query("DELETE FROM ${Disclaimer.TABLE_NAME}")
    fun deleteAll(): Int
}