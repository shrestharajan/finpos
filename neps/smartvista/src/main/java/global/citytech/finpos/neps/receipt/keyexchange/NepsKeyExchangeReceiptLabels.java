package global.citytech.finpos.neps.receipt.keyexchange;

/**
 * Created by Unique Shakya on 1/19/2021.
 */
public class NepsKeyExchangeReceiptLabels {

    private NepsKeyExchangeReceiptLabels() {
    }

    public static final String DIVIDER = "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --";
    public static final String SUCCESS_MESSAGE = "KEY EXCHANGE SUCCESSFUL";
    public static final String FAILURE_MESSAGE = "KEY EXCHANGE FAILURE";
}
