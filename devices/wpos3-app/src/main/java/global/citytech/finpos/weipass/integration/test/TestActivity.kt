package global.citytech.finpos.weipass.integration.test

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import kotlinx.android.synthetic.main.activity_test.*

class TestActivity : AppCompatActivity(), View.OnClickListener, View.OnLongClickListener {
    private lateinit var coreResponse: DeviceResponse
    private lateinit var initSDKService: global.citytech.finposframework.hardware.init.InitializeSDKService
    private var isSdkInit: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        btn_init.setOnClickListener(this)
        btn_inject.setOnClickListener(this)
        btn_check_key.setOnClickListener(this)
        btn_erase_key.setOnClickListener(this)
        btn_emv_param.setOnClickListener(this)
        btn_emv_keys.setOnClickListener(this)
        btn_emv_keys.setOnLongClickListener(this)
        btn_revoked_keys.setOnClickListener(this)
        btn_revoked_keys.setOnLongClickListener(this)
        btn_aid_parameters.setOnClickListener(this)
        btn_aid_parameters.setOnLongClickListener(this)
        img_btn_clear.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.btn_init -> doInitSdk()
            R.id.btn_inject -> prepareForKeyInjection()
            R.id.btn_check_key -> prepareForKeyExistCheck()
            R.id.btn_erase_key -> prepareForKeyErase()
            R.id.btn_emv_param -> prepareForEmvParamInjection()
            R.id.btn_emv_keys -> prepareForEmvKeysInjection()
            R.id.btn_revoked_keys -> prepareForRevokedKeyInjection()
            R.id.btn_aid_parameters -> prepareForAidParametersInjection()
            R.id.img_btn_clear -> clearTextsInTextView()
        }
    }

    override fun onLongClick(view: View?): Boolean {
        when (view!!.id) {
            R.id.btn_emv_keys -> prepareForErasingEmvKeys()
            R.id.btn_revoked_keys -> prepareForErasingRevokedKeys()
            R.id.btn_aid_parameters -> prepareForErasingAidParameters()
        }
        return true
    }

    /*
     * SDK Initialization
     */
    private fun doInitSdk() {
        Thread(Runnable {
            initSDKService = AppDeviceFactory.getCore(this, global.citytech.finposframework.hardware.DeviceServiceType.INIT) as global.citytech.finposframework.hardware.init.InitializeSDKService
            coreResponse = initSDKService.initializeSDK()
            if (coreResponse.result == Result.SUCCESS) {
                isSdkInit = true
            }
            placeResultOnTextview(coreResponse)
        }).start()
    }

    /*
     * Key Injection
     */
    private fun prepareForKeyInjection() {
        if (!isSdkInit) {
            placeMsgOnTextview("Please Init the SDK first")
        } else {
            doInjectKeyInCore()
        }
    }

    private fun doInjectKeyInCore() {
        Thread(Runnable {
            val injectKeyService: global.citytech.finposframework.hardware.keymgmt.keys.KeyService =
                AppDeviceFactory.getCore(this, global.citytech.finposframework.hardware.DeviceServiceType.KEYS) as global.citytech.finposframework.hardware.keymgmt.keys.KeyService
            coreResponse = injectKeyService.injectKey(prepareKeyInjectionRequest())
            placeResultOnTextview(coreResponse)
        }) .start()
    }

    private fun prepareKeyInjectionRequest(): global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyInjectionRequest {
        return global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyInjectionRequest(
            et_package_name.text.toString(),
            global.citytech.finposframework.hardware.keymgmt.keys.KeyType.KEY_TYPE_TMK,
            global.citytech.finposframework.hardware.keymgmt.keys.KeyAlgorithm.TYPE_3DES,
            "0123456789abcdef0123456789abcdef".toUpperCase(),
            true
        )
    }

    /*
     * Key Exist Check
     */
    private fun prepareForKeyExistCheck() {
        if (!isSdkInit) {
            placeMsgOnTextview("Please Init the SDK first ...")
        } else {
            doKeyExistCheckInCore()
        }
    }

    private fun doKeyExistCheckInCore() {
        Thread(Runnable {
            val keyService: global.citytech.finposframework.hardware.keymgmt.keys.KeyService =
                AppDeviceFactory.getCore(this, global.citytech.finposframework.hardware.DeviceServiceType.KEYS) as global.citytech.finposframework.hardware.keymgmt.keys.KeyService
            coreResponse = keyService.checkKeyExist(prepareKeyCheckRequest())
            placeResultOnTextview(coreResponse)
        }).start()
    }

    private fun prepareKeyCheckRequest(): global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyCheckRequest {
        return global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyCheckRequest(
            et_package_name.text.toString(),
            global.citytech.finposframework.hardware.keymgmt.keys.KeyType.KEY_TYPE_TMK
        )
    }

    /*
     * Key Erase
     */
    private fun prepareForKeyErase() {
        if (!isSdkInit) {
            placeMsgOnTextview("Please Init the SDK first ...")
        } else {
            doKeyEraseInCore()
        }
    }

    private fun doKeyEraseInCore() {
        Thread(Runnable {
            val keyService: global.citytech.finposframework.hardware.keymgmt.keys.KeyService = AppDeviceFactory.getCore(this, global.citytech.finposframework.hardware.DeviceServiceType.KEYS) as global.citytech.finposframework.hardware.keymgmt.keys.KeyService
            coreResponse = keyService.eraseAllKeys()
            placeResultOnTextview(coreResponse)
        }).start()
    }

    /*
     * Emv Param Injection
     */
    private fun prepareForEmvParamInjection() {
        if (!isSdkInit) {
            placeMsgOnTextview("Please Init the SDK first ...")
        } else {
            doEmvParamInjectionInCore()
        }
    }

    private fun doEmvParamInjectionInCore() {
        Thread(Runnable {
            val emvParametersService = AppDeviceFactory.getCore(this, global.citytech.finposframework.hardware.DeviceServiceType.EMV_PARAM) as global.citytech.finposframework.hardware.emv.emvparam.EmvParametersService
            coreResponse = emvParametersService.injectEmvParameters(prepareEmvParameterRequest())
            placeResultOnTextview(coreResponse)
        }).start()
    }

    private fun prepareEmvParameterRequest(): EmvParametersRequest {
        return EmvParametersRequest(
            "0524",
            "0524",
            "01",
            "E0F0C8",
            "22",
            "7399",
            "06804344",
            "800150400566",
            "Bhat Bhateni Super Market",
            "35E04000",
            "D000F0A000",
            "01"
        )
    }

    /*
     * Emv Keys Injection
     */
    private fun prepareForEmvKeysInjection() {
        if (!isSdkInit) {
            placeMsgOnTextview("Please Init the SDK first ...")
        } else {
            doEmvKeyInjectionInCore()
        }
    }

    private fun doEmvKeyInjectionInCore() {
        Thread(Runnable {
            val emvKeysService = AppDeviceFactory.getCore(this, global.citytech.finposframework.hardware.DeviceServiceType.EMV_KEYS) as global.citytech.finposframework.hardware.emv.emvkeys.EmvKeysService
            coreResponse = emvKeysService.injectEmvKeys(prepareEmvKeysInjectionRequest())
            placeResultOnTextview(coreResponse)
        }).start()
    }

    private fun prepareEmvKeysInjectionRequest(): global.citytech.finposframework.hardware.emv.emvkeys.EmvKeysInjectionRequest {
        val emvKeyList = listOf(prepareEmvKeyData(), prepareEmvKeyData(), prepareEmvKeyData())
        return global.citytech.finposframework.hardware.emv.emvkeys.EmvKeysInjectionRequest(
            emvKeyList
        )
    }

    private fun prepareEmvKeyData(): global.citytech.finposframework.hardware.emv.emvkeys.EmvKey {
        return global.citytech.finposframework.hardware.emv.emvkeys.EmvKey(
            "A000000228",
            "28",
            "00",
            "03",
            "C05B993401063615EF211036DD8154066BE4BBEC7E93A82E83C65E2CFD76E498A6DBF6135C816F606B9564A30D259A9FD5463AA78261223FBB4718EAD4A17347E11BE475BF0DDD9BE9315DCF585D58863A7BCA0E67440586DA098E33047C0DF6F6A1D1BD081BF283321DDF248FDFFA9FB749D0FDA47ADE2E7C0AAA76B146A00A5EBDA270C52832E8132FBC631EAC1120F02215829EB1D852B1969F1C1504A659AB6057C92AF92D981C8171B68E3300E3",
            "01",
            "01",
            "F1B99C16FF415746FF423241E66F17AF0235B2C2",
            "171231"
        )
    }

    /*
     * Erasing Emv Keys
     */
    private fun prepareForErasingEmvKeys() {
        if (!isSdkInit) {
            placeMsgOnTextview("Please Init the SDK first ...")
        } else {
            doEraseEmvKeysInCore()
        }
    }

    private fun doEraseEmvKeysInCore() {
        Thread(Runnable {
            val emvKeyService = AppDeviceFactory.getCore(this, global.citytech.finposframework.hardware.DeviceServiceType.EMV_KEYS) as global.citytech.finposframework.hardware.emv.emvkeys.EmvKeysService
            coreResponse = emvKeyService.eraseAllEmvKeys()
            placeResultOnTextview(coreResponse)
        }).start()
    }

    /*
     * Revoked Keys Injection
     */
    private fun prepareForRevokedKeyInjection() {
        if (!isSdkInit) {
            placeMsgOnTextview("Please Init the SDK first ...")
        } else {
            doRevokedKeyInjectionInCore()
        }
    }

    private fun doRevokedKeyInjectionInCore() {
        Thread(Runnable {
            val revokedKeyService = AppDeviceFactory.getCore(this, global.citytech.finposframework.hardware.DeviceServiceType.REVOKED_KEYS) as global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKeysService
            coreResponse = revokedKeyService.injectRevokedKeys(prepareRevokedKeyInjectionRequest())
            placeResultOnTextview(coreResponse)
        }).start()
    }

    private fun prepareRevokedKeyInjectionRequest(): global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKeysInjectionRequest {
        val revokedKeyList = listOf(prepareRevokedKeyData(), prepareRevokedKeyData())
        return global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKeysInjectionRequest(
            revokedKeyList
        )
    }

    private fun prepareRevokedKeyData(): global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKey {
        return global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKey(
            "A000000025",
            "0F",
            "01"
        )
    }

    /*
     * Erasing Revoked Keys
     */
    private fun prepareForErasingRevokedKeys() {
        if (!isSdkInit) {
            placeMsgOnTextview("Please Init the SDK first ...")
        } else {
            doEraseRevokedKeysInCore()
        }
    }

    private fun doEraseRevokedKeysInCore() {
        Thread(Runnable {
            val revokedKeysService = AppDeviceFactory.getCore(this, global.citytech.finposframework.hardware.DeviceServiceType.REVOKED_KEYS) as global.citytech.finposframework.hardware.emv.revokedkeys.RevokedKeysService
            coreResponse = revokedKeysService.eraseAllRevokedKeys()
            placeResultOnTextview(coreResponse)
        }).start()
    }

    /*
     * Aid Parameters Injection
     */
    private fun prepareForAidParametersInjection() {
        if (!isSdkInit) {
            placeMsgOnTextview("Please Init the SDK first ...")
        } else {
            doAidParametersInjectionInCore()
        }
    }

    private fun doAidParametersInjectionInCore() {
        Thread(Runnable {
            val aidParametersService = AppDeviceFactory.getCore(this, global.citytech.finposframework.hardware.DeviceServiceType.AID_PARAM) as global.citytech.finposframework.hardware.emv.aid.AidParametersService
            coreResponse = aidParametersService.injectAid(prepareAidInjectionRequest())
            placeResultOnTextview(coreResponse)
        }).start()
    }

    private fun prepareAidInjectionRequest(): global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest {
        val aidList = listOf(mockAid(), mockVisaAid(), mockAmexAid(), mockMasterCardAid(), mockUnionPayAid())
        return global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest(
            aidList,
            prepareEmvParameterRequest(),
            prepareListOfSupportedTransactionTypes()
        )
    }

    private fun prepareListOfSupportedTransactionTypes(): List<String> {
        return listOf("00","09")
    }

    private fun mockAid(): global.citytech.finposframework.hardware.emv.aid.AidParameters {
        return global.citytech.finposframework.hardware.emv.aid.AidParameters(
            "A0000002281010",
            "SPAN MCHIP",
            "0002",
            "",
            "9F3704",
            "0000000000",
            "FC408CF800",
            "FC408CA800",
            "000000000000",
            "000000000000",
            "000001000000",
            "000000010000"
        )
    }

    private fun mockVisaAid(): global.citytech.finposframework.hardware.emv.aid.AidParameters {
        return global.citytech.finposframework.hardware.emv.aid.AidParameters(
            "A0000000031010",
            "VISA CREDIT",
            "00960096008D",
            "9700",
            "9F3704",
            "0010000000",
            "D84004F800",
            "D84004A800",
            "000000009000",
            "000000000000",
            "000001000000",
            "000000010000"
        )
    }

    private fun mockAmexAid(): global.citytech.finposframework.hardware.emv.aid.AidParameters {
        return global.citytech.finposframework.hardware.emv.aid.AidParameters(
            "A0000000251010",
            "American Express",
            "00960096008D",
            "9700",
            "9F3704",
            "0010000000",
            "D84004F800",
            "D84004A800",
            "000000000000",
            "000000000000",
            "000001000000",
            "000000010000"
        )
    }

    private fun mockMasterCardAid() =
        global.citytech.finposframework.hardware.emv.aid.AidParameters(
            "A0000000041010",
            "MASTERCARD",
            "0002",
            "970F9F02065F2A029A039C0195059F3704",
            "9F3704",
            "0000000000",
            "FE50BCF800",
            "FE50BCA000",
            "000000000000",
            "000000000000",
            "000001000000",
            "000000010000"
        )

    private fun mockUnionPayAid() =
        global.citytech.finposframework.hardware.emv.aid.AidParameters(
            "A000000333010101",
            "UNION PAY",
            "0002",
            "970F9F02065F2A029A039C0195059F3704",
            "9F3704",
            "0000000000",
            "FE50BCF800",
            "FE50BCA000",
            "000000000000",
            "000000000000",
            "000001000000",
            "000000010000"
        )

    /*
     * Erasing All Aid Parameters
     */
    private fun prepareForErasingAidParameters() {
        if (!isSdkInit) {
            placeMsgOnTextview("Please Init the SDK first ...")
        } else {
            doEraseAidParametersInCore()
        }
    }

    private fun doEraseAidParametersInCore() {
        Thread(Runnable {
            val aidParametersService = AppDeviceFactory.getCore(this, global.citytech.finposframework.hardware.DeviceServiceType.AID_PARAM) as global.citytech.finposframework.hardware.emv.aid.AidParametersService
            coreResponse = aidParametersService.eraseAllAid()
            placeResultOnTextview(coreResponse)
        }).start()
    }

    private fun clearTextsInTextView() {
        tv_status.setText("")
    }

    private fun placeResultOnTextview(coreResponse: DeviceResponse) {
        runOnUiThread {
            tv_status.setText("Result ::: ".plus(coreResponse.result).plus("\nMessage ::: ").plus(coreResponse.message))
        }
    }

    private fun placeMsgOnTextview(message: String) {
        tv_status.setText(message)
    }

    private fun checkForEmptyOrBlankMessage(message: String): Boolean {
        return (message.isEmpty() || message.isBlank())
    }

    override fun onBackPressed() {
        finishAndRemoveTask()
        System.exit(0)
    }
}