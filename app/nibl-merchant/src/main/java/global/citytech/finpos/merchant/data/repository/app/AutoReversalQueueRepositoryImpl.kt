package global.citytech.finpos.merchant.data.repository.app

import global.citytech.finpos.merchant.data.datasource.app.autoreversal.AutoReversalQueueDataSource
import global.citytech.finpos.merchant.domain.repository.app.AutoReversalQueueRepository
import global.citytech.finposframework.usecases.transaction.data.AutoReversal

/**
 * Created by Unique Shakya on 6/22/2021.
 */
class AutoReversalQueueRepositoryImpl(private val autoReversalQueueDataSource: AutoReversalQueueDataSource) :
    AutoReversalQueueRepository {
    override fun isQueueEmpty(): Boolean {
        return autoReversalQueueDataSource.isQueueEmpty()
    }

    override fun isActive(): Boolean {
        return autoReversalQueueDataSource.isActive()
    }

    override fun getSize(): Long {
        return autoReversalQueueDataSource.getSize()
    }

    override fun getAllQueueRequests(): MutableList<AutoReversal> {
        return autoReversalQueueDataSource.getAllQueueRequests()
    }

    override fun getRecentRequest(): AutoReversal? {
        return autoReversalQueueDataSource.getRecentRequest()
    }

    override fun removeRequest(autoReversal: AutoReversal) {
        autoReversalQueueDataSource.removeRequest(autoReversal)
    }

    override fun addRequest(autoReversal: AutoReversal) {
        autoReversalQueueDataSource.addRequest(autoReversal)
    }

    override fun incrementRetryCount(autoReversal: AutoReversal) {
        autoReversalQueueDataSource.incrementRetryCount(autoReversal)
    }

    override fun changeStatus(autoReversal: AutoReversal, status: AutoReversal.Status) {
        autoReversalQueueDataSource.changeStatus(autoReversal, status)
    }

    override fun clear() {
        autoReversalQueueDataSource.clear()
    }
}