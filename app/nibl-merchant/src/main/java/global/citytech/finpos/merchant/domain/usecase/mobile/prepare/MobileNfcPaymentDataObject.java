package global.citytech.finpos.merchant.domain.usecase.mobile.prepare;

import java.util.Map;

/**
 * @author SUSHIL THAPA
 * updated By RAJAN SHRESTHA
 */
public class MobileNfcPaymentDataObject {
    private String name;
    private String id;
    private MobileNfcPaymentDataFormat format;
    private int length;
    private PresenceType presence;
    private String value;
    private Boolean childTagPresence;
    //private Map<String, MobileNfcPaymentDataObject> subDataObject;

    public MobileNfcPaymentDataObject(DataObjectBuilder dataObjectBuilder) {
        this.name = dataObjectBuilder.name;
        this.id = dataObjectBuilder.id;
        this.format = dataObjectBuilder.format;
        this.length = dataObjectBuilder.length;
        this.presence = dataObjectBuilder.presence;
        this.value = dataObjectBuilder.value;
        this.childTagPresence = dataObjectBuilder.childTagPresence;
        //this.subDataObject = dataObjectBuilder.subDataObject;
    }

    public enum PresenceType {
        M("Mandatory"), O("Optional"), C("Conditional");

        private String detail;

        PresenceType(String detail) {
            this.detail = detail;
        }

    }

    public static class DataObjectBuilder {
        private String name;
        private String id;
        private MobileNfcPaymentDataFormat format;
        private int length;
        private PresenceType presence;
        private String value;
        private Boolean childTagPresence;
        //private Map<String, MobileNfcPaymentDataObject> subDataObject;

        public DataObjectBuilder name(String name) {
            this.name = name;
            return this;
        }

        public DataObjectBuilder id(String id) {
            this.id = id;
            return this;
        }

        public DataObjectBuilder format(MobileNfcPaymentDataFormat format) {
            this.format = format;
            return this;
        }


        public DataObjectBuilder length(int length) {
            this.length = length;
            return this;
        }

        public DataObjectBuilder presence(PresenceType presence) {
            this.presence = presence;
            return this;
        }

        public DataObjectBuilder value(String value) {
            this.value = value;
            return this;
        }

        public DataObjectBuilder childTagPresence(Boolean childTagPresence) {
            this.childTagPresence = childTagPresence;
            return this;
        }

        public MobileNfcPaymentDataObject build() {
            return new MobileNfcPaymentDataObject(this);
        }


    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public MobileNfcPaymentDataFormat getFormat() {
        return format;
    }

    public int getLength() {
        return length;
    }

    public PresenceType getPresence() {
        return presence;
    }

    public String getValue() {
        return value;
    }

    public Boolean getChildTagPresence() {
        return childTagPresence;
    }

   /* public Map<String, MobileNfcPaymentDataObject> getSubDataObject() {
        return subDataObject;
    }*/

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFormat(MobileNfcPaymentDataFormat format) {
        this.format = format;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setPresence(PresenceType presence) {
        this.presence = presence;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setChildTagPresence(Boolean childTagPresence) {
        this.childTagPresence = childTagPresence;
    }
}
