package global.citytech.finposframework.switches.posmode;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 1/7/2021.
 */
public interface PosModeRequester<T extends PosModeRequestParameter, E extends PosModeResponseParameter>
        extends Request<T, E> {
}
