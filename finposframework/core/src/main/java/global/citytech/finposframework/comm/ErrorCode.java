package global.citytech.finposframework.comm;

import global.citytech.finposframework.supports.Result;

/** User: Surajchhetry Date: 2/20/20 Time: 2:12 PM */
public enum ErrorCode implements Result.ErrorInfo {
  UNABLE_TO_CONNECT("504", "Unable to connect with host"),
  READ_TIME_OUT("405", "Read time out");
  private String code;
  private String message;

  ErrorCode(String code, String message) {
    this.code = code;
    this.message = message;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }
}
