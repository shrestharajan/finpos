package global.citytech.finpos.merchant.domain.usecase.core.ministatement

import global.citytech.finpos.merchant.domain.model.ministatement.MiniStatementResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.ministatement.MiniStatementRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.ministatement.MiniStatementRequestItem
import io.reactivex.Observable


class CoreMiniStatementUseCase(private val generateMiniStatementRepository: MiniStatementRepository) {
    fun generateMiniStatementRequest(
        configurationItem: ConfigurationItem,
        miniStatementRequestItem: MiniStatementRequestItem
    ): Observable<MiniStatementResponseEntity> =
        generateMiniStatementRepository.generateMiniStatementRequest(
            configurationItem,
            miniStatementRequestItem
        )

}