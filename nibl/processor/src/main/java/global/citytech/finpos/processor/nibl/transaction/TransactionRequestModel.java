package global.citytech.finpos.processor.nibl.transaction;

import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.io.sound.SoundService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.transaction.TransactionRequestParameter;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;

/**
 * Created by Saurav Ghimire on 2/3/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class TransactionRequestModel implements UseCase.Request, TransactionRequestParameter {
    protected TransactionRequest transactionRequest;
    protected TransactionRepository transactionRepository;
    protected ReadCardService readCardService;
    protected DeviceController deviceController;
    protected TransactionAuthenticator transactionAuthenticator;
    protected PrinterService printerService;
    protected ApplicationRepository applicationRepository;
    protected ReconciliationRepository reconciliationRepository;
    protected LedService ledService;
    protected SoundService soundService;

    public ReconciliationRepository getReconciliationRepository() {
        return reconciliationRepository;
    }

    public LedService getLedService() {
        return ledService;
    }

    public SoundService getSoundService() {
        return soundService;
    }

    public TransactionRequest getTransactionRequest() {
        return transactionRequest;
    }

    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }

    public ReadCardService getReadCardService() {
        return readCardService;
    }

    public DeviceController getDeviceController() {
        return deviceController;
    }

    public TransactionAuthenticator getTransactionAuthenticator() {
        return transactionAuthenticator;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public ApplicationRepository getApplicationRepository() {
        return applicationRepository;
    }
}
