package global.citytech.finpos.merchant.presentation.purchase

import android.app.Activity
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import global.citytech.common.Constants
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.MobileNfcPayment
import global.citytech.finpos.merchant.domain.model.app.QrPayment
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.presentation.alertdialogs.PosEntryMode
import global.citytech.finpos.merchant.presentation.alertdialogs.PosEntrySelection
import global.citytech.finpos.merchant.presentation.alertdialogs.PosEntrySelectionDialog
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmationDialog
import global.citytech.finpos.merchant.presentation.amount.newlayout.NewAmountActivity
import global.citytech.finpos.merchant.presentation.amount.oldlayout.AmountActivity
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.card.check.CardCheckActivity
import global.citytech.finpos.merchant.presentation.dynamicqr.base.DynamicQrBaseActivity
import global.citytech.finpos.merchant.presentation.dynamicqr.fonepayqrsale.DynamicFonepayQrActivity
import global.citytech.finpos.merchant.presentation.dynamicqr.nqrsale.DynamicNQrActivity
import global.citytech.finpos.merchant.presentation.manual.ManualActivity
import global.citytech.finpos.merchant.presentation.mobilenfcpayment.MobileNfcPaymentActivity
import global.citytech.finpos.merchant.presentation.payment.PaymentSdkSingleton
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finpos.merchant.utils.SoundUtils
import global.citytech.finpos.merchant.utils.VoiceMessageUtils
import global.citytech.finpos.merchant.utils.disableMe
import global.citytech.finpos.merchant.utils.enableMe
import global.citytech.finpos.merchant.utils.formatCardHolderName
import global.citytech.finpos.merchant.utils.formatCardNumber
import global.citytech.finpos.merchant.utils.formatExpiryDate
import global.citytech.finpos.merchant.utils.handleDebounce
import global.citytech.finpos.merchant.utils.loadCurrency
import global.citytech.finposframework.listeners.CardConfirmationListener
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.notifier.Notifier
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.utility.StringUtils
import kotlinx.android.synthetic.main.activity_base_transaction.btn_cancel
import kotlinx.android.synthetic.main.activity_base_transaction.btn_card_cancel
import kotlinx.android.synthetic.main.activity_base_transaction.btn_card_confirm
import kotlinx.android.synthetic.main.activity_base_transaction.btn_manual
import kotlinx.android.synthetic.main.activity_base_transaction.img_contactless
import kotlinx.android.synthetic.main.activity_base_transaction.iv_back
import kotlinx.android.synthetic.main.activity_base_transaction.iv_progress
import kotlinx.android.synthetic.main.activity_base_transaction.ll_card
import kotlinx.android.synthetic.main.activity_base_transaction.ll_header
import kotlinx.android.synthetic.main.activity_base_transaction.ll_payment_method_selection
import kotlinx.android.synthetic.main.activity_base_transaction.ll_progress
import kotlinx.android.synthetic.main.activity_base_transaction.rl_pos_entry
import kotlinx.android.synthetic.main.activity_base_transaction.toolbar
import kotlinx.android.synthetic.main.activity_base_transaction.tv_card_holder_name
import kotlinx.android.synthetic.main.activity_base_transaction.tv_card_number
import kotlinx.android.synthetic.main.activity_base_transaction.tv_currency
import kotlinx.android.synthetic.main.activity_base_transaction.tv_expiry_date
import kotlinx.android.synthetic.main.activity_base_transaction.tv_message
import kotlinx.android.synthetic.main.activity_base_transaction.tv_payment_title
import kotlinx.android.synthetic.main.activity_base_transaction.tv_progress_msg
import kotlinx.android.synthetic.main.activity_base_transaction.tv_scheme
import kotlinx.android.synthetic.main.activity_base_transaction.tv_total_amount_label
import kotlinx.android.synthetic.main.activity_base_transaction.tv_transaction_amount
import kotlinx.android.synthetic.main.activity_base_transaction.tv_transaction_type
import kotlinx.android.synthetic.main.layout_auth_completion.btn_auth_confirm
import kotlinx.android.synthetic.main.layout_auth_completion.ll_auth_no_connection
import kotlinx.android.synthetic.main.layout_refund.btn_refund_confirm
import kotlinx.android.synthetic.main.layout_refund.ll_refund_no_connection
import kotlinx.android.synthetic.main.layout_tip_adjustment.btn_tips_confirm
import kotlinx.android.synthetic.main.layout_tip_adjustment.ll_tip_no_connection
import kotlinx.android.synthetic.main.layout_void.btn_void_confirm
import kotlinx.android.synthetic.main.layout_void.ll_void_no_connection
import java.math.BigDecimal

/**
 * Created by Unique Shakya on 9/4/2020.
 */
abstract class BaseTransactionActivity<T : ViewDataBinding, E : BaseTransactionViewModel> :
    AppBaseActivity<T, E>(),
    TransactionConfirmationDialog.Listener,
    PosEntrySelectionDialog.Listener {

    private lateinit var cardConfirmationListener: CardConfirmationListener
    private val logger = Logger(BaseTransactionActivity::class.java.name)
    private var progressDialog: Dialog? = null
    private var progressView: View? = null
    private var transactionConfirmationDialog: TransactionConfirmationDialog? = null
    protected var amount: BigDecimal = BigDecimal.ZERO
    protected var displayAmount: BigDecimal = BigDecimal.ZERO
    protected var isEmiTransaction: Boolean = false
    protected var emiInfo: String = ""
    protected var vatInfo: String = ""
    protected var paymentIntentData: Intent = Intent()
    protected var paymentFailed: Boolean = false
    protected var fromTransactionDetail = false
    protected var isQrTransaction = false
    protected var isMobileTransaction = false
    protected var isManualCardEntry: Boolean = false
    protected var isFromPurchasePage: Boolean = false
    protected var isFromPreAuthPage: Boolean = false

    private var localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )


    protected open fun initBaseViews(isPreAuthPage: Boolean = false) {
        this.isFromPreAuthPage = isPreAuthPage
        Glide.with(this).load("file:///android_asset/images/loading.gif")
            .into(iv_progress)
        btn_manual.setOnClickListener {
            btn_cancel.handleDebounce()
            btn_manual.handleDebounce()
            onManualButtonClicked()
        }

        btn_cancel.setOnClickListener {
            btn_manual.handleDebounce()
            btn_cancel.handleDebounce()
            onCancelButtonClicked()
        }

        iv_back.setOnClickListener {
            if (ll_payment_method_selection.visibility == View.VISIBLE)
                finish()
            else
                onCancelButtonClicked()
        }

        NiblMerchant.INSTANCE.networkConnectionAvailable.observe(
            this,
            Observer {
                if (it) {
                    hideConnectionLostView()
                    enableButtons()
                } else {
                    showConnectionLostView()
                    disableButtons()
                }
            }
        )
    }

    private fun enableButtons() {
        btn_tips_confirm.enableMe()
        btn_void_confirm.enableMe()
        btn_refund_confirm.enableMe()
        btn_auth_confirm.enableMe()
    }

    private fun disableButtons() {
        btn_tips_confirm.disableMe()
        btn_void_confirm.disableMe()
        btn_refund_confirm.disableMe()
        btn_auth_confirm.disableMe()
    }

    private fun showConnectionLostView() {
        ll_refund_no_connection.visibility = View.VISIBLE
        ll_void_no_connection.visibility = View.VISIBLE
        ll_auth_no_connection.visibility = View.VISIBLE
        ll_tip_no_connection.visibility = View.VISIBLE
    }

    private fun hideConnectionLostView() {
        ll_refund_no_connection.visibility = View.GONE
        ll_void_no_connection.visibility = View.GONE
        ll_auth_no_connection.visibility = View.GONE
        ll_tip_no_connection.visibility = View.GONE
    }

    private var broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            logger.log("BROADCAST :::: BROADCAST RECEIVED IN TRANSACTION ACTIVITY ::: $intent.name")
            if (intent!!.action == Constants.INTENT_ACTION_NOTIFICATION) {
                handleNotificationBroadcast(intent)
            }
        }
    }

    private fun handleNotificationBroadcast(intent: Intent) {
        val eventType =
            intent.getSerializableExtra(Constants.EXTRA_EVENT_TYPE) as Notifier.EventType
        logger.log("BROADCAST :::: EVENT TYPE IN BROADCAST ::: ${eventType.name}")
        val message = intent.getStringExtra(Constants.EXTRA_NOTIFICATION_MESSAGE)
        var transactionType = Notifier.TransactionType.OTHER.name
        val intentTransactionType = intent.getStringExtra(Constants.EXTRA_TRANSACTION_TYPE)
        if (intentTransactionType != null)
            transactionType = intentTransactionType

        logger.log("BROADCAST :::: MESSAGE IN BROADCAST ::: $message")
        when (eventType) {
            Notifier.EventType.STARTING_READ_CARD -> {
                this.hideProgressScreen()
                this.getTransactionViewModel().retrieveAllowedCardTypes(this.getTransactionType())
            }

            Notifier.EventType.READING_CARD -> {
                btn_cancel.handleDebounce()
                btn_manual.handleDebounce()
                this.hidePosEntrySelectionDialog()
                this.showProgressScreen(message!!)
            }

            Notifier.EventType.DETECT_CARD_ERROR -> {
                this.hidePosEntrySelectionDialog()
            }

            Notifier.EventType.CONNECTING_TO_SWITCH,
            Notifier.EventType.TRANSMITTING_REQUEST,
            Notifier.EventType.RESPONSE_RECEIVED,
            Notifier.EventType.PROCESSING_ISSUER_SCRIPT,
            Notifier.EventType.DETECTED_MULTIPLE_AID -> {
                this.hidePosEntrySelectionDialog()
                this.showProgressScreen(message!!)
            }

            Notifier.EventType.TRANSACTION_APPROVED -> {
                this.hidePosEntrySelectionDialog()
                this.hideProgressScreen()
                this.showTransactionApprovedDialog(message!!, transactionType)
            }

            Notifier.EventType.TRANSACTION_DECLINED -> {
                this.hidePosEntrySelectionDialog()
                this.hideProgressScreen()
                this.showTransactionDeclinedDialog(message!!, transactionType)
            }

            Notifier.EventType.PERFORMING_REVERSAL -> {
                this.showProgressScreen(message!!)
            }

            Notifier.EventType.REVERSAL_COMPLETED -> {
                this.hideProgressScreen()
            }

            Notifier.EventType.WAVE_AGAIN -> {
                this.hideProgressScreen()
                this.getTransactionViewModel().posEntrySelection.postValue(
                    PosEntrySelection(
                        message!!,
                        isPICCAllowed = true,
                        isManualAllowed = false,
                        entryMode = PosEntryMode.ACCEPT_PICC_ONLY
                    )
                )
            }

            Notifier.EventType.SWIPE_AGAIN,
            Notifier.EventType.SWITCH_ICC_TO_MAGNETIC -> {
                this.getTransactionViewModel().posEntrySelection.postValue(
                    PosEntrySelection(
                        message!!,
                        isPICCAllowed = false,
                        isManualAllowed = false,
                        entryMode = PosEntryMode.ACCEPT_MAG_ONLY
                    )
                )

            }

            Notifier.EventType.SWITCH_PICC_TO_ICC,
            Notifier.EventType.SWITCH_MAGNETIC_TO_ICC,
            Notifier.EventType.FORCE_ICC_CARD -> {
                this.getTransactionViewModel().posEntrySelection.postValue(
                    PosEntrySelection(
                        message!!,
                        isPICCAllowed = false,
                        isManualAllowed = false,
                        entryMode = PosEntryMode.ACCEPT_ICC_ONLY
                    )
                )
            }

            Notifier.EventType.CARD_CONFIRMATION_EVENT -> {
                handleCardConfirmationEvent(intent)
            }

            Notifier.EventType.PUSH_MERCHANT_LOG -> {
                this.getTransactionViewModel().pushMerchantTransactionLog(intent)
            }
        }
    }

    private fun handleCardConfirmationEvent(intent: Intent) {
        cardConfirmationListener =
            intent.getSerializableExtra(Constants.EXTRA_CARD_CONFIRMATION_LISTENER) as CardConfirmationListener
        if (getViewModel().getCardConfirmationRequiredStatus())
            displayCardConfirmation(intent)
        else
            cardConfirmationListener.onResult(true)
    }

    private fun displayCardConfirmation(intent: Intent) {
        val pan = intent.getStringExtra(Constants.EXTRA_CARD_NUMBER)
        val scheme = intent.getStringExtra(Constants.EXTRA_CARD_SCHEME)
        val holderName = intent.getStringExtra(Constants.EXTRA_CARD_HOLDER_NAME)
        val expiryDate = intent.getStringExtra(Constants.EXTRA_CARD_EXPIRY)

        rl_pos_entry.visibility = View.GONE
        ll_progress.visibility = View.GONE
        scheme?.let {
            tv_scheme.text = it
        }
        holderName?.let {
            tv_card_holder_name.text = it.formatCardHolderName()
        }
        pan?.let {
            tv_card_number.text = it.formatCardNumber()
        }
        expiryDate?.let {
            tv_expiry_date.text = it.formatExpiryDate()
        }
        btn_card_cancel.setOnClickListener {
            onCardConfirmationCancel()
        }
        btn_card_confirm.setOnClickListener {
            ll_card.visibility = View.GONE
            cardConfirmationListener.onResult(true)
        }
        ll_card.visibility = View.VISIBLE
    }

    private fun onCardConfirmationCancel() {
        ll_card.visibility = View.GONE
        cardConfirmationListener.onResult(false)
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
    }


    override fun onDestroy() {
        getViewModel().enableNetworkPing()
        super.onDestroy()
    }

    private fun showTransactionApprovedDialog(message: String, transactionType: String) {
        if (transactionType == Notifier.TransactionType.VOID.name) {
            if (localDataUseCase.isSoundMode(false))
                SoundUtils.play(
                    applicationContext,
                    displayAmount.toDouble(),
                    "radha",
                    AppUtility.getCurrencyCode()
                )
            Logger.getLogger(BaseTransactionActivity::class.simpleName)
                .debug("transaction approved")
        }

        if (transactionType == Notifier.TransactionType.CARD.name) {
            if (localDataUseCase.isSoundMode(false))
                SoundUtils.play(
                    applicationContext,
                    displayAmount.toDouble(),
                    "blank",
                    AppUtility.getCurrencyCode()
                )
            Logger.getLogger(BaseTransactionActivity::class.simpleName)
                .debug("transaction approved")

        }

        val transactionConfirmation = TransactionConfirmation.Builder()
            .title(getTransactionType().displayName + "\nTransaction Successful")
            .amount(StringUtils.formatAmountTwoDecimal(displayAmount))
            .message(message)
            .imageId(R.drawable.approved)
            .positiveLabel("")
            .negativeLabel("")
            .qrImageString("")
            .transactionStatus("")
            .build()
        this.showTransactionConfirmationDialog(transactionConfirmation, false)

    }

    abstract fun getTransactionViewModel(): BaseTransactionViewModel

    private fun showTransactionDeclinedDialog(message: String, transactionType: String) {

        if (transactionType == Notifier.TransactionType.CARD.name) {
            if (localDataUseCase.isSoundMode(false))
                VoiceMessageUtils.playFail(applicationContext)
        }

        if (transactionType == Notifier.TransactionType.VOID.name) {
            if (localDataUseCase.isSoundMode(false))
                VoiceMessageUtils.playFail(applicationContext)
        }

        val transactionConfirmation = TransactionConfirmation.Builder()
            .title(getTransactionType().displayName + "\nTransaction Declined")
            .message(message)
            .imageId(R.drawable.declined)
            .positiveLabel("")
            .negativeLabel("")
            .qrImageString("")
            .transactionStatus("")
            .build()
        this.showTransactionConfirmationDialog(transactionConfirmation, false)

    }


    override fun onResume() {
        super.onResume()
        getViewModel().transactionType = getTransactionType()
        this.registerBroadCastReceivers()
    }

    protected fun initBaseLiveDataObservers() {
        this.getTransactionViewModel().transactionConfirmationData.observe(this, Observer {
            if (isFromPurchasePage) {
                getViewModel().transactionComplete.value = true
            }
            this.showTransactionConfirmationDialog(it, true)
        })

        this.getTransactionViewModel().customerCopyPrintMessage.observe(this, Observer {
            this.showCustomerCopyResponseDialog(it)
        })

        this.getTransactionViewModel().posEntrySelection.observe(this, Observer {
            showPosEntrySelectionDialog(it)
        })

        this.getTransactionViewModel().transactionComplete.observe(this, Observer {
            if (!it) {
                hidePosEntrySelectionDialog()
                hideProgressScreen()
                hideTransactionConfirmationDialog()
            }
        })

        this.getTransactionViewModel().doLoadManualPage.observe(this, Observer {
            if (it) {
                startManualActivity()
            }
        })

        this.getTransactionViewModel().doReturnToIdlePage.observe(this, Observer {
            if (it) {
                checkForCardPresent()
            }
        })

        this.getTransactionViewModel().isLoading.observe(this, Observer {
            if (it) {
                showProgressScreen(getString(R.string.please_wait))
            } else {
                hideProgressScreen()
            }
        })

        this.getTransactionViewModel().message.observe(this, Observer {
            showNormalMessage(it)
        })

        this.getTransactionViewModel().errorMessage.observe(this, Observer {
            showFailureMessage(it)
        })

        this.getTransactionViewModel().cardPresent.observe(this, Observer {
            if (it)
                this.startCardCheckActivity()
            else
                checkTransactionCompletion()
        })

        getTransactionViewModel().mapOfTransactionLog.observe(this, Observer {
            Logger.getLogger(BaseTransactionActivity::class.simpleName)
                .debug(Gson().toJson(it))
            logger.log("::: GETTING MAP OF TRANSACTION LOG OBSERVING :::")
            paymentIntentData.putExtra(
                Constants.EXTRA_AUTH_CODE,
                it[Constants.EXTRA_AUTH_CODE] as String
            )
            paymentIntentData.putExtra(
                Constants.EXTRA_CARD_NUMBER,
                it[Constants.EXTRA_CARD_NUMBER] as String
            )
            paymentIntentData.putExtra(
                Constants.EXTRA_MESSAGE,
                it[Constants.EXTRA_MESSAGE] as String
            )
            paymentIntentData.putExtra(
                Constants.EXTRA_APPROVED,
                it[Constants.EXTRA_APPROVED] as Boolean
            )
            paymentIntentData.putExtra(Constants.EXTRA_RRN, it[Constants.EXTRA_RRN] as String)
            paymentIntentData.putExtra(Constants.EXTRA_STAN, it[Constants.EXTRA_STAN] as String)
            paymentIntentData.putExtra(
                Constants.EXTRA_TRANSACTION_TIME,
                it[Constants.EXTRA_TRANSACTION_TIME] as String
            )
            paymentIntentData.putExtra(
                Constants.EXTRA_PAYMENT_MODE,
                it[Constants.EXTRA_PAYMENT_MODE] as String
            )
            paymentIntentData.putExtra(
                Constants.EXTRA_PAYMENT_NETWORK,
                it[Constants.EXTRA_PAYMENT_NETWORK] as String
            )
            if (isQrTransaction or isMobileTransaction)
                returnToDashboard()

        })

        getTransactionViewModel().paymentError.observe(this, Observer {
            paymentFailed = true
            paymentIntentData.putExtra(Constants.EXTRA_CODE, it.errorCode)
            paymentIntentData.putExtra(Constants.EXTRA_MESSAGE, it.errorMessage)
        })
    }


    private fun registerBroadCastReceivers() {
        registerReprintBroadcastReceivers()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, IntentFilter(Constants.INTENT_ACTION_NOTIFICATION))
    }

    override fun onPause() {
        this.unregisterBroadCastReceivers()
        super.onPause()
    }

    private fun unregisterBroadCastReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        unregisterReprintBroadCastReceivers()
    }

    private fun showPosEntrySelectionDialog(posEntrySelection: PosEntrySelection) {
        rl_pos_entry.visibility = View.VISIBLE
        tv_progress_msg.text = posEntrySelection.posEntryMessage
        if (getViewModel().transactionType == TransactionType.CASH_ADVANCE) {
            Glide.with(this).load(PosEntryMode.ACCEPT_ICC_ONLY.loaderImage)
                .into(img_contactless)
        } else {
            Glide.with(this).load(posEntrySelection.entryMode.loaderImage)
                .into(img_contactless)
        }
        setManualButton(posEntrySelection)
    }

    private fun setManualButton(posEntrySelection: PosEntrySelection) {
        if (posEntrySelection.isManualAllowed) {
            if (isFromPreAuthPage)
                btn_manual.visibility = View.GONE
            else
                btn_manual.visibility = View.VISIBLE
        } else {
            btn_manual.visibility = View.GONE
        }
    }

    protected fun showProgressScreen(message: String) {
        ll_progress.visibility = View.VISIBLE
        tv_message.text = message
    }

    protected fun hideProgressScreen() {
        ll_progress.visibility = View.GONE
    }

    protected fun hidePosEntrySelectionDialog() {
        rl_pos_entry.visibility = View.GONE
    }

    private fun showCustomerCopyResponseDialog(it: String?) {
        val messageConfig = MessageConfig.Builder()
            .message("CUSTOMER COPY")
            .message(it).positiveLabel(getString(R.string.title_ok))
            .onPositiveClick {
                checkForCardPresent()
            }
        showNormalMessage(messageConfig)
    }

    private fun showTransactionConfirmationDialog(it: TransactionConfirmation, update: Boolean) {
        hideProgress()
        if (transactionConfirmationDialog == null)
            transactionConfirmationDialog = TransactionConfirmationDialog(this, this, it)
        if (update)
            transactionConfirmationDialog!!.update(it, this)
        transactionConfirmationDialog!!.show()

    }

    private fun hideTransactionConfirmationDialog() {
        transactionConfirmationDialog?.hide()
    }

    protected fun showSuccessMessage(it: String?) {
        if (!it.isNullOrEmpty()) {
            val messageConfig = MessageConfig.Builder()
                .message(it)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    checkForCardPresent()
                }
            showSuccessMessage(messageConfig)
        }
    }

    protected fun showNormalMessage(it: String?) {
        if (!it.isNullOrEmpty()) {
            val messageConfig = MessageConfig.Builder()
                .message(it)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    checkForCardPresent()
                }
            showNormalMessage(messageConfig)
        }
    }

    protected fun showFailureMessage(it: String?) {
        if (!it.isNullOrEmpty()) {
            val messageConfig = MessageConfig.Builder()
                .message(it)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    checkForCardPresent()
                }
            showFailureMessage(messageConfig)
        }
    }

    protected fun checkForCardPresent() {
        showProgressScreen(" ")
        this.getTransactionViewModel().checkForCardPresent()
    }

    protected fun startCardCheckActivity() {
        val intent = Intent(this, CardCheckActivity::class.java)
        startActivityForResult(intent, CardCheckActivity.REQUEST_CODE)
    }

    protected fun returnToDashboard() {
        hidePosEntrySelectionDialog()
        hidePaymentItemsAvailableDialog()
        hideProgressScreen()
        hideTransactionConfirmationDialog()
        if (PaymentSdkSingleton.getInstance().isFromPaymentSdk) {
            setResultForTransactionActivity()
        }
        finish()
    }

    private fun setResultForTransactionActivity() {
        val result = if (paymentFailed)
            Activity.RESULT_CANCELED
        else
            Activity.RESULT_OK
        setResult(result, paymentIntentData)
    }

    protected fun startAmountActivity() {
        if (isEmiTransaction) {
            val intent = Intent(Constants.EMI_ACTIVITY_PACKAGE_NAME)
            intent.putExtra(Constants.EMI_EXTRA_LIMIT, 1000000.0)
            intent.putExtra(
                Constants.EMI_EXTRA_CURRENCY_CODE,
                DeviceConfiguration.get().currencyCode
            )
            this.startActivityForResult(intent, Constants.EMI_REQUEST_CODE)
        } else {
            if (getViewModel().getNumpadType(getString(R.string.title_old_layout)) == getString(R.string.title_old_layout)) {
                val intent = Intent(this, AmountActivity::class.java)
                startActivityForResult(intent, AmountActivity.REQUEST_CODE)
            } else {
                val intent = Intent(this, NewAmountActivity::class.java)
                startActivityForResult(intent, NewAmountActivity.REQUEST_CODE)
            }
        }
    }

    protected fun startAmountActivityWithLimit(amountLimit: BigDecimal) {
        if (getViewModel().getNumpadType(getString(R.string.title_old_layout)) == getString(R.string.title_old_layout)) {
            val intent = Intent(this, AmountActivity::class.java)
            intent.putExtra(AmountActivity.EXTRA_LIMIT, amountLimit.toString())
            startActivityForResult(intent, AmountActivity.REQUEST_CODE)
        } else {
            val intent = Intent(this, NewAmountActivity::class.java)
            intent.putExtra(NewAmountActivity.EXTRA_LIMIT, amountLimit.toString())
            startActivityForResult(intent, NewAmountActivity.REQUEST_CODE)
        }
    }

    protected fun startAmountActivityWithAuthLimit(amountLimit: BigDecimal) {
        if (getViewModel().getNumpadType(getString(R.string.title_old_layout)) == getString(R.string.title_old_layout)) {
            val intent = Intent(this, AmountActivity::class.java)
            intent.putExtra(AmountActivity.EXTRA_LIMIT, amountLimit.toString())
            startActivityForResult(intent, AmountActivity.REQUEST_CODE)
        } else {
            val intent = Intent(this, NewAmountActivity::class.java)
            intent.putExtra(NewAmountActivity.EXTRA_LIMIT, amountLimit.toString())
            startActivityForResult(intent, NewAmountActivity.REQUEST_CODE)
        }
    }

    protected fun startManualActivity() {
        val intent = Intent(this, ManualActivity::class.java)
        startActivityForResult(intent, ManualActivity.REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                AmountActivity.REQUEST_CODE, NewAmountActivity.REQUEST_CODE -> {
                    this.onAmountActivityResult(data)
                    data?.let {
                        val amountInString =
                            data.getStringExtra(getString(R.string.intent_confirmed_amount))
                        displayAmount = amountInString!!.toBigDecimal()
                        showHeaderView(displayAmount)
                    }
                }

                Constants.EMI_REQUEST_CODE -> {
                    this.onAmountActivityResult(data)
                    data?.let {
                        val amountInString =
                            it.getStringExtra(Constants.EMI_EXTRA_CONFIRM_AMOUNT)
                        displayAmount = amountInString!!.toBigDecimal()
                        showHeaderView(displayAmount)
                        emiInfo = it.getStringExtra(Constants.EMI_EXTRA_EMI_INFO)
                    }
                }

                ManualActivity.REQUEST_CODE -> {
                    this.onManualActivityResult(data)
                }

                MobileNfcPaymentActivity.REQUEST_CODE -> {
                    this.handleMobileNfcPayment(data)
                }

                CardCheckActivity.REQUEST_CODE -> {
                    checkTransactionCompletion()
                }

                DynamicNQrActivity.REQUEST_CODE, DynamicFonepayQrActivity.REQUEST_CODE -> {
                    handleQrPayment(data)
                }
            }
        } else {
            if (isFromPurchasePage) {
                if (requestCode == ManualActivity.REQUEST_CODE) {
                    getViewModel().task = BaseTransactionViewModel.Task.CANCEL_CARD_TRANSACTION
                }
            }
            updatePaymentCancelled()
            if (requestCode == AmountActivity.REQUEST_CODE || requestCode == Constants.EMI_REQUEST_CODE || requestCode == NewAmountActivity.REQUEST_CODE)
                returnToDashboard()
            else
                checkForCardPresent()
        }
    }

    private fun handleMobileNfcPayment(data: Intent?) {
        getViewModel().shouldDispose = false
        try {
            val mobileNfcPayment =
                data?.getParcelableExtra<MobileNfcPayment>(MobileNfcPaymentActivity.EXTRA_MOBILE_PAYMENT)
            val message = mobileNfcPayment?.transactionStatus
            val isApproved = !mobileNfcPayment?.approvalCode.isNullOrEmptyOrBlank()
            if (mobileNfcPayment != null && message != null) {
                isMobileTransaction = true
                getTransactionViewModel().updatePaymentTransactionByMobile(
                    mobileNfcPayment,
                    message,
                    isApproved
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun handleQrPayment(data: Intent?) {
        getViewModel().shouldDispose = false
        data?.let {
            val qrPayment = it.getParcelableExtra<QrPayment>(DynamicQrBaseActivity.EXTRA_QR_PAYMENT)
            Logger.getLogger(BaseTransactionActivity::class.simpleName)
                .debug(Gson().toJson(qrPayment))

            if (qrPayment != null) {
                isQrTransaction = true
                val message = qrPayment.transactionStatus
                val isApproved = qrPayment.approvalCode.isNullOrEmptyOrBlank()
                getTransactionViewModel().updatePaymentTransactionByQr(
                    qrPayment,
                    message,
                    isApproved
                )
                paymentFailed = false
            }
        }
    }

    protected fun updatePaymentCancelled() {
        paymentFailed = true
        paymentIntentData.putExtra(Constants.EXTRA_CODE, -102)
        paymentIntentData.putExtra(Constants.EXTRA_MESSAGE, "Cancelled")
    }

    protected fun updatePaymentFailedIntent(code: Int, message: String) {
        paymentFailed = true
        paymentIntentData.putExtra(Constants.EXTRA_CODE, code)
        paymentIntentData.putExtra(Constants.EXTRA_MESSAGE, message)
    }

    fun showHeaderView(amount: BigDecimal) {
        ll_header.visibility = View.VISIBLE
        tv_transaction_type.text = getTransactionType().displayName
        tv_total_amount_label.text = getString(R.string.title_total_amount)
        tv_transaction_amount.text = StringUtils.formatAmountTwoDecimal(amount)
        tv_currency.loadCurrency()
    }

    override fun onBackPressed() {
        if (ll_card.visibility == View.VISIBLE) {
            onCardConfirmationCancel()
            return
        }
        if (rl_pos_entry.visibility == View.VISIBLE) {
//            onCancelButtonClicked()
            return
        }
        if (ll_progress.visibility != View.VISIBLE) {
            returnToDashboard()
        }
    }

    protected fun showPaymentItemsAvailableDialog() {
        isManualCardEntry = false
        getViewModel().shouldDispose = false
        toolbar.visibility = View.VISIBLE
        tv_transaction_type.visibility = View.GONE
        tv_payment_title.text = getString(R.string.title_payment_method)
        rl_pos_entry.visibility = View.GONE
        ll_payment_method_selection.visibility = View.VISIBLE
    }

    protected fun hidePaymentItemsAvailableDialog() {
        ll_payment_method_selection.visibility = View.GONE
    }

    private fun checkTransactionCompletion() {
        if (isFromPurchasePage) {
            if (getViewModel().transactionComplete.value == true) {
                this.returnToDashboard()
            } else {
                showPaymentItemsAvailableDialog()
            }
        } else {
            this.returnToDashboard()
        }
    }

    private fun getProgressMessage() = tv_message.text.toString()
    abstract fun onAmountActivityResult(data: Intent?)
    abstract fun onManualActivityResult(data: Intent?)
    abstract fun getTransactionType(): TransactionType
}