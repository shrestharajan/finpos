package global.citytech.payment.sdk.core;

/**
 * Created by Unique Shakya on 8/18/2021.
 */
public enum PurchaseType {
    PURCHASE,
    VOID
}
