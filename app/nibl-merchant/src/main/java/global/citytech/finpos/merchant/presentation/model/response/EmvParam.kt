package global.citytech.finpos.merchant.presentation.model.response

import global.citytech.common.data.LabelValue

data class EmvParam(
    var additionalTerminalCapabilities: LabelValue? = null,
    var forceOnlineFlag: LabelValue? = null,
    var merchantCategoryCode: LabelValue? = null,
    var merchantIdentifier: LabelValue? = null,
    var merchantName: LabelValue? = null,
    var terminalCapabilities: LabelValue? = null,
    var terminalCountryCode: LabelValue? = null,
    var terminalId: LabelValue? = null,
    var terminalType: LabelValue? = null,
    var transactionCurrencyCode: LabelValue? = null,
    var transactionCurrencyExponent: LabelValue? = null,
    var ttq: LabelValue? = null,
    var additionalData: Map<String, LabelValue>? = null
) {
    companion object {

        val TABLE_NAME = "emv_param"

        const val COLUMN_ID = "id"
        const val COLUMN_TTQ = "ttq"
        const val COLUMN_TRANSACTION_CURRENCY_EXPONENT = "transaction_currency_exponent"
        const val COLUMN_TRANSACTION_CURRENCY_CODE = "transaction_currency_code"
        const val COLUMN_TERMINAL_TYPE = "terminal_type"
        const val COLUMN_TERMINAL_ID = "terminal_id"
        const val COLUMN_TERMINAL_COUNTRY_CODE = "terminal_country_code"
        const val COLUMN_TERMINAL_CAPABILITIES = "terminal_capabilities"
        const val COLUMN_MERCHANT_NAME = "merchant_name"
        const val COLUMN_MERCHANT_IDENTIFIER = "merchant_identifier"
        const val COLUMN_MERCHANT_CATEGORY_CODE = "merchant_category_code"
        const val COLUMN_FORCE_ONLINE_FLAG = "force_online_flag"
        const val COLUMN_ADDITIONAL_TERMINAL_CAPABILITIES = "additional_terminal_capabilities"
        const val COLUMN_ADDITIONAL_DATA = "additional_data"

    }
}