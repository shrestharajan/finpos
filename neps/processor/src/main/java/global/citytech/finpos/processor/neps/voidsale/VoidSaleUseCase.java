package global.citytech.finpos.processor.neps.voidsale;

import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.PosConditionCode;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.voidsale.VoidSaleIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.voidsale.VoidSaleRequestSender;
import global.citytech.finpos.processor.neps.reversal.ReversalUseCase;
import global.citytech.finpos.processor.neps.transaction.NepsTransactionReceiptPrintHandler;
import global.citytech.finpos.processor.neps.transaction.ReasonForReversal;
import global.citytech.finpos.processor.neps.transaction.ReversedTransactionReceiptHandler;
import global.citytech.finpos.processor.neps.transaction.TransactionExecutorTemplate;
import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finpos.processor.neps.transaction.TransactionUtils;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.led.LedAction;
import global.citytech.finposframework.hardware.io.led.LedLight;
import global.citytech.finposframework.hardware.io.led.LedRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.JsonUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Saurav Ghimire on 4/19/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public class VoidSaleUseCase extends TransactionExecutorTemplate {

    private TransactionLog transactionLogByRRN;


    public VoidSaleUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    public TransactionResponse execute(global.citytech.finpos.processor.neps.transaction.TransactionRequest request) {
        setUpRequiredTransactionFields(request);
        checkIfDeviceReady();
        this.initTransaction(this.request.getApplicationRepository(), transactionRequest);
        this.retrieveCardResponse();
        this.notifier.notify(
                Notifier.EventType.TRANSMITTING_REQUEST,
                "Transmitting Request ..."
        );
        try {
            VoidSaleIsoRequest reversalIsoRequest = prepareReversalIsoRequest();
            IsoMessageResponse isoMessageResponse = sendIsoMessage();
            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait...");
            return checkForInvalidResponseAndProceed(isoMessageResponse);
        } catch (FinPosException e) {
            if (this.unableToGoOnlineException(e))
                return this.handleFinposExceptionForTransaction(e.getType());
            else
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }

    }

    protected TransactionResponse checkForInvalidResponseAndProceed(IsoMessageResponse isoMessageResponse) {
        InvalidResponseHandlerResponse invalidResponseHandlerResponse = this.checkForInvalidResponseFromHost(isoMessageResponse);
        if (invalidResponseHandlerResponse.isInvalid()) {
            return this.handleInvalidResponseFromHost(invalidResponseHandlerResponse);
        } else {
            return proceedValidResponseFromHost(isoMessageResponse);
        }
    }

    private TransactionResponse proceedValidResponseFromHost(IsoMessageResponse isoMessageResponse) {
        if (isIccTransactionRequired(isoMessageResponse)) {
            return promptToPerformIccTransaction();
        }
        boolean transactionApproved = this.transactionApprovedByActionCode(isoMessageResponse);
        if (isCardTypeNonChipOrNonEmv(readCardResponse)) {
            return finalizeCompletedTransaction(isoMessageResponse, transactionApproved);
        } else {
            return processIsoResponseToChipOrEmvCard(isoMessageResponse, transactionApproved);
        }
    }

    protected TransactionResponse processIsoResponseToChipOrEmvCard(
            IsoMessageResponse isoMessageResponse,
            boolean transactionApproved
    ) {
        processOnlineResult(isoMessageResponse);
        return checkIfApprovedTransactionDeclinedByCardAndProceed(
                isoMessageResponse,
                transactionApproved
        );
    }

    private TransactionResponse checkIfApprovedTransactionDeclinedByCardAndProceed(
            IsoMessageResponse isoMessageResponse,
            boolean transactionApproved
    ) {
        if (this.isTransactionDeclineByCardEvenIfSuccessOnSwitch(isoMessageResponse))
            return this.handleApprovedTransactionDeclinedByCard();
        else {
            return finalizeCompletedTransaction(isoMessageResponse, transactionApproved);
        }
    }


    protected TransactionResponse handleInvalidResponseFromHost(InvalidResponseHandlerResponse invalidResponseHandlerResponse) {
        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, invalidResponseHandlerResponse.getMessage(), Notifier.TransactionType.VOID.name());
        this.receiptLog = new ReversedTransactionReceiptHandler(transactionRequest,
                readCardResponse, this.terminalRepository)
                .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.INVALID_RESPONSE,
                        invalidResponseHandlerResponse.getMessage()));
        this.performAutoReversal();
        boolean shouldPrint = !this.approvePrintOnly(request.getApplicationRepository());
        if (shouldPrint) {
            new NepsTransactionReceiptPrintHandler(request.getPrinterService())
                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
            request.getTransactionRepository().updateReceiptLog(receiptLog);
        }
        return prepareErrorResponse(invalidResponseHandlerResponse.getMessage(), shouldPrint);
    }

    private void performAutoReversal() {
        ReversalUseCase reversalUseCase = new ReversalUseCase(
                this.terminalRepository,
                prepareReversalIsoRequest(),
                this.notifier
        );
        try {
            reversalUseCase.execute(
                    prepareReversalRequest()

            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void setUpRequiredTransactionFields(global.citytech.finpos.processor.neps.transaction.TransactionRequest request) {
        this.request = request;
        this.transactionLogByRRN = getTransactionLogByRRN();
        stan = this.terminalRepository.getSystemTraceAuditNumber();
        invoiceNumber = transactionLogByRRN.getInvoiceNumber();
        batchNumber = this.terminalRepository.getReconciliationBatchNumber();
        transactionRequest = request.getTransactionRequest();
        applicationRepository = request.getApplicationRepository();
    }

    private TransactionLog getTransactionLogByRRN() {
        if (this.request != null) {
            TransactionLog transactionLog = this.request
                    .getTransactionRepository()
                    .getTransactionLogWithRRN(getRRN());
            if (transactionLog != null) {
                return transactionLog;
            }
        }
        throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
    }

    private String getRRN() {
        if (this.request != null) {
            System.out.println("::: ORIGINAL RRN ::: " + this.request.getTransactionRequest().getOriginalRetrievalReferenceNumber());
            return this.request.getTransactionRequest().getOriginalRetrievalReferenceNumber();
        }
        throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
    }

    @Override
    protected void retrieveCardResponse() {
        this.readCardResponse = transactionLogByRRN.getReadCardResponse();
        this.readCardResponse.getCardDetails()
                .setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
        if (this.transactionLogByRRN.getTransactionType() == TransactionType.REVERSAL)
            throw new PosException(PosError.DEVICE_ERROR_RECENT_TRANSACTION_CANNOT_VOID);
    }

    @Override
    protected void processCard() {
        /*No need in case of Void Sale*/
    }

    @Override
    protected String getClassName() {
        return VoidSaleUseCase.class.getName();
    }

    @Override
    protected TransactionIsoRequest prepareIsoRequest(ReadCardResponse readCardResponse, TransactionRequest transactionRequest) {
        return VoidSaleIsoRequest.Builder
                .newInstance()
                .processingCode(transactionLogByRRN.getProcessingCode())
                .pan(transactionLogByRRN.getReadCardResponse().getCardDetails().getPrimaryAccountNumber())
                .transactionAmount(transactionLogByRRN.getTransactionAmount())
                .localDateTime(DateUtils.yyMMddHHmmssDate())
                .posEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
                .posConditionCode(PosConditionCode.ATTENDANT_TERMINAL.getValue())
                .track2Data(readCardResponse.getCardDetails().getTrackTwoData())
                .rrn(transactionLogByRRN.getRrn())
                .currencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .emvData(transactionLogByRRN.getReadCardResponse().getCardDetails().getIccDataBlock())
                .build();
    }

    @Override
    protected NepsMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context) {
        return new VoidSaleRequestSender(context);
    }

    @Override
    protected void processOnlineResult(IsoMessageResponse isoMessageResponse) {
        /*No need in case of Void Sale*/
    }

    @Override
    protected void updateTransactionLog(boolean transactionApproved, TransactionRequest transactionRequest, TransactionRepository transactionRepository, IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse) {
        System.out.println("::: NEPS ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
        System.out.println("::: NEPS ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
        String timeStamp = IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.LOCAL_TIME);
        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
                .withStan(stan)
                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
                .withInvoiceNumber(invoiceNumber)
                .withRrn(
                        IsoMessageUtils.retrieveFromDataElementsAsString(
                                isoMessageResponse,
                                DataElement.RETRIEVAL_REFERENCE_NUMBER
                        )
                )
                .withAuthCode(
                        IsoMessageUtils.retrieveFromDataElementsAsString(
                                isoMessageResponse,
                                DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE
                        )
                )
                .withTransactionType(TransactionType.VOID)
                .withOriginalTransactionType(
                        this.transactionLogByRRN.getOriginalTransactionType()
                )
                .withTransactionAmount(request.getTransactionRequest().getAmount())
                .withOriginalTransactionAmount(
                        this.transactionLogByRRN.getOriginalTransactionAmount()
                )
                .withTransactionDate(TransactionUtils.getDateOrDefaultFromTimeStamp(timeStamp))
                .withTransactionTime(TransactionUtils.getTimeOrDefaultFromTimeStamp(timeStamp))
                .withTransactionStatus(transactionApproved ? "APPROVED" : "DECLINED")
                .withPosEntryMode(this.transactionLogByRRN.getPosEntryMode())
                .withOriginalPosEntryMode(this.transactionLogByRRN.getPosEntryMode())
                .withPosConditionCode(this.transactionLogByRRN.getPosConditionCode())
                .withOriginalPosConditionCode(this.transactionLogByRRN.getOriginalPosConditionCode())
                .withReconcileStatus("") //TODO
                .withReconcileTime("") //TODO
                .withReconcileDate("") //TODO
                .withReconcileBatchNo(batchNumber)
                .withReadCardResponse(this.transactionLogByRRN.getReadCardResponse())
                .withOriginalTransactionReferenceNumber(request.getTransactionRequest().getOriginalInvoiceNumber())
                .withReceiptLog(getReceiptLog(transactionRequest, this.transactionLogByRRN.getReadCardResponse(), isoMessageResponse,
                        applicationRepository, batchNumber, stan, invoiceNumber))
                .withAuthorizationCompleted(true)
                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                .withProcessingCode(getProcessingCode())
                .withOriginalTransactionReferenceNumber(transactionRequest.getOriginalRetrievalReferenceNumber())
                .withCurrencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .withVatInfo(transactionLogByRRN.getVatInfo())
                .build();
        transactionRepository.updateTransactionLog(transactionLog);
        transactionRepository.updateVoidStatusForGivenTransactionDetails(
                transactionApproved,
                this.transactionLogByRRN.getTransactionType(),
                this.transactionLogByRRN.getInvoiceNumber());
    }

    @Override
    protected void updateTransactionIdsAndNotifyTransactionStatus(
            boolean transactionApproved,
            IsoMessageResponse isoMessageResponse,
            global.citytech.finpos.processor.neps.transaction.TransactionRequest request,
            ReadCardResponse readCardResponse
    ) {
        terminalRepository.incrementSystemTraceAuditNumber();
        terminalRepository.incrementRetrievalReferenceNumber();
        if (transactionApproved) {
            request.getLedService().doTurnLedWith(new LedRequest(LedLight.GREEN, LedAction.ON));
            notifier.notify(
                    Notifier.EventType.TRANSACTION_APPROVED,
                    this.approvedMessage(isoMessageResponse),
                    Notifier.TransactionType.VOID.name()
            );
        } else {
            request.getLedService().doTurnLedWith(new LedRequest(LedLight.RED, LedAction.ON));
            notifier.notify(
                    Notifier.EventType.TRANSACTION_DECLINED,
                    this.declinedMessage(
                            isoMessageResponse,
                            request.getApplicationRepository()
                    ),
                    Notifier.TransactionType.VOID.name()
            );
        }
    }

    @Override
    protected TransactionResponse prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository, boolean shouldPrint) {
        return VoidSaleResponse.Builder.newInstance()
                .withShouldPrintCustomerCopy(shouldPrint)
                .withStan(stan)
                .withApproved(transactionApproved)
                .withShouldPrintCustomerCopy(shouldPrint)
                .withDebugRequestMessage(isoMessageResponse.getDebugRequestString())
                .withDebugResponseMessage(isoMessageResponse.getDebugResponseString())
                .withMessage(retrieveMessage(isoMessageResponse, transactionApproved, this.request.getApplicationRepository()))
                .build();
    }

    @Override
    protected String getProcessingCode() {
        return transactionLogByRRN.getProcessingCode();
    }

    @Override
    protected TransactionResponse prepareErrorResponse(String responseMessage, boolean shouldPrint) {
        return VoidSaleResponse.Builder.newInstance()
                .withStan(this.stan)
                .withMessage(responseMessage)
                .withApproved(false)
                .withShouldPrintCustomerCopy(shouldPrint)
                .build();
    }

    @Override
    protected TransactionResponse promptToPerformIccTransaction() {
        return null;
    }
}