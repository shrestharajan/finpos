package global.citytech.finpos.processor.nibl.transaction.authorisation.preauth;

import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finpos.nibl.iso8583.requestsender.preauth.IccPreAuthRequestSender;
import global.citytech.finpos.nibl.iso8583.requestsender.preauth.MagPreAuthRequestSender;
import global.citytech.finpos.nibl.iso8583.requestsender.preauth.PICCPreAuthRequestSender;
import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finpos.processor.nibl.transaction.template.CardTransactionUseCase;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.utility.IsoMessageUtils;

/**
 * Created by Unique Shakya on 3/16/2021.
 */
public class CardPreAuthUseCase extends CardTransactionUseCase {

    public CardPreAuthUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    protected String getTransactionTypeClassName() {
        return CardPreAuthUseCase.class.getSimpleName();
    }

    @Override
    protected NIBLMessageSenderTemplate getRequestSender(RequestContext context) {
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case MAG:
                return new MagPreAuthRequestSender(new NIBLSpecInfo(), context);
            case ICC:
                return new IccPreAuthRequestSender(new NIBLSpecInfo(), context);
            case PICC:
                return new PICCPreAuthRequestSender(new NIBLSpecInfo(), context);
            default:
                throw new IllegalArgumentException("Unknown card type");
        }
    }

    @Override
    protected String getOriginalPosConditionCode() {
        return null;
    }

    @Override
    protected PosEntryMode getOriginalPosEntryMode() {
        return null;
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.PRE_AUTHORIZATION.getCode();
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy, String miniStatementData) {
        return null;
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy) {
        return PreAuthResponseModel.Builder.createDefaultBuilder()
                .approved(isApproved)
                .message(this.retrieveMessage(isoMessageResponse, isApproved))
                .debugRequestMessage(isoMessageResponse.getDebugRequestString())
                .debugResponseMessage(isoMessageResponse.getDebugResponseString())
                .shouldPrintCustomerCopy(shouldPrintCustomerCopy)
                .stan(this.stan)
                .build();
    }

    @Override
    protected TransactionResponseModel prepareDeclinedTransactionResponse(String message,
                                                                          boolean shouldPrintReceipt) {
        return PreAuthResponseModel.Builder.createDefaultBuilder()
                .message(message)
                .approved(false)
                .stan(this.stan)
                .shouldPrintCustomerCopy(shouldPrintReceipt)
                .build();
    }

    @Override
    protected String getOriginalTransactionReferenceNumber() {
        return "";
    }

    @Override
    protected boolean isAuthorizationCompleted() {
        return false;
    }

    @Override
    protected void updateTransactionLogOfOriginalTransaction(boolean isApproved) {
        // do nothing
    }

    @Override
    protected boolean shouldIncrementInvoiceNumber(boolean isApproved) {
        return isApproved;
    }

    @Override
    protected TransactionLog retrieveTransactionLogToStore(boolean isApproved, IsoMessageResponse isoMessageResponse) {
        return TransactionLog.Builder.newInstance()
                .withStan(stan)
                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
                .withInvoiceNumber(invoiceNumber)
                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
                .withTransactionType(TransactionType.PRE_AUTH)
                .withOriginalTransactionType(TransactionType.PRE_AUTH)
                .withTransactionAmount(transactionRequest.getAmount())
                .withOriginalTransactionAmount(transactionRequest.getAmount())
                .withTransactionDate(retrieveTransactionDate(isoMessageResponse))
                .withTransactionTime(retrieveTransactionTime(isoMessageResponse))
                .withTransactionStatus(isApproved ? APPROVED : DECLINED)
                .withPosEntryMode(this.transactionIsoRequest.getPosEntryMode())
                .withOriginalPosEntryMode(this.getOriginalPosEntryMode())
                .withPosConditionCode(this.transactionIsoRequest.getPosConditionCode())
                .withOriginalPosConditionCode(this.getOriginalPosConditionCode())
                .withReconcileStatus("")
                .withReconcileTime("")
                .withReconcileDate("")
                .withReconcileBatchNo(batchNumber)
                .withReadCardResponse(readCardResponse)
                .withReceiptLog(this.receiptLog)
                .withAuthorizationCompleted(false)
                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                .withTransactionVoided(false)
                .withTipAdjusted(false)
                .withCurrencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .withVatInfo(transactionRequest.getVatInfo())
                .build();
    }
}
