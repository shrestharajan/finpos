package global.citytech.finpos.processor.neps.reconciliation;

import global.citytech.finpos.neps.iso8583.requestsender.reconciliation.ReconciliationISORequest;
import global.citytech.finpos.processor.neps.transaction.TransactionUtils;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;
import global.citytech.finposframework.utility.IsoMessageUtils;

public class NepsReconciliationReceiptMaker {

    private final RequestContext requestContext;
    private final IsoMessageResponse isoMessageResponse;

    public NepsReconciliationReceiptMaker(RequestContext requestContext, IsoMessageResponse isoMessageResponse) {
        this.requestContext = requestContext;
        this.isoMessageResponse = isoMessageResponse;
    }


    public NepsReconciliationReceipt prepare() {
        ReconciliationISORequest settlementRequest = (ReconciliationISORequest) this.requestContext.getRequest();
        return NepsReconciliationReceipt.Builder.newInstance()
                .withRetailer(this.prepareRetailerInfo(requestContext))
                .withPerformance(this.preparePerformanceInfo(isoMessageResponse))
                .withReconciliationBatchNumber(settlementRequest.getBatchNumber())
                .withReconciliationTotals((NepsReconciliationTotals) settlementRequest.getReconciliationTotals())
                .withReconciliationResult(this.prepareReconciliationResult(isoMessageResponse))
                .withHost("000048")
                .withApplicationVersion(settlementRequest.getApplicationVersion())
                .build();
    }

    private Retailer prepareRetailerInfo(RequestContext requestContext) {
        TerminalInfo terminalInfo = requestContext.getTerminalInfo();
        return Retailer.Builder.defaultBuilder()
                .withRetailerLogo(terminalInfo.getMerchantPrintLogo())
                .withRetailerName(terminalInfo.getMerchantName())
                .withRetailerAddress(terminalInfo.getMerchantAddress())
                .withMerchantId(terminalInfo.getMerchantID())
                .withTerminalId(terminalInfo.getTerminalID())
                .build();
    }

    private Performance preparePerformanceInfo(IsoMessageResponse isoMessageResponse) {
        String timeStamp = TransactionUtils.getTimeStampForReconciliationWithIsoResponseDate(isoMessageResponse);
        return Performance.Builder.defaultBuilder()
                .withStartDateTime(timeStamp)
                .withEndDateTime(timeStamp)
                .build();
    }

    private String prepareReconciliationResult(IsoMessageResponse isoMessageResponse) {
        int responseCode = IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, 39);
        return responseCode == 0 ? ReconciliationReceiptLabels.SETTLEMENT_SUCCESS : ReconciliationReceiptLabels.SETTLEMENT_FAILURE;
    }
}
