package global.citytech.finposframework.hardware.keymgmt.keys.requests

import global.citytech.finposframework.hardware.keymgmt.keys.KeyType

/**
 * Created by Rishav Chudal on 4/9/20.
 */
class KeyCheckRequest(
    val packageName: String,
    val keyType: KeyType
) {
    var keyIndex: Int? = null
}