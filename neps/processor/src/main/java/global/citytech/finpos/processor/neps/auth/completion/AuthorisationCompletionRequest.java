package global.citytech.finpos.processor.neps.auth.completion;

import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;

public class AuthorisationCompletionRequest extends TransactionRequest {

    private String originalRRN;

    public AuthorisationCompletionRequest(TransactionType transactionType) {
        super(transactionType);
    }

    public String getOriginalRRN() {
        return originalRRN;
    }

    public void setOriginalRRN(String originalRRN) {
        this.originalRRN = originalRRN;
    }
}