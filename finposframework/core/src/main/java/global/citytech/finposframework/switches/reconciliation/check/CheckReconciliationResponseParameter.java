package global.citytech.finposframework.switches.reconciliation.check;

import global.citytech.finposframework.switches.ResponseParameter;

/**
 * Created by Unique Shakya on 8/11/2021.
 */
public interface CheckReconciliationResponseParameter extends ResponseParameter {
}
