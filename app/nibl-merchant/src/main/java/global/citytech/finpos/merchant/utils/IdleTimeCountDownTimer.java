package global.citytech.finpos.merchant.utils;


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/12/07 - 12:06 PM
 */

import android.os.CountDownTimer;

public class IdleTimeCountDownTimer extends CountDownTimer {

    private IdleTimeCountDownTimerListener listener;

    public IdleTimeCountDownTimer(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    public void addListener(IdleTimeCountDownTimerListener listener) {
        this.listener = listener;
    }

    @Override
    public void onTick(long millisUntilFinished) {
    }

    @Override
    public void onFinish() {
        listener.onIdleTimeCountDownReached();
    }

    public interface IdleTimeCountDownTimerListener {
        void onIdleTimeCountDownReached();
    }

}
