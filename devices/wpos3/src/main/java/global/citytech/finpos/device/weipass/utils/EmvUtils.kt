package global.citytech.finpos.device.weipass.utils

import android.os.RemoteException
import com.wiseasy.emvprocess.SDKInstance
import com.wiseasy.emvprocess.TransResult
import com.wiseasy.emvprocess.utils.ByteUtil
import global.citytech.finpos.device.weipass.card.handler.WeipassStateHandler
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.*
import global.citytech.finposframework.hardware.io.cards.cvm.PICCCvm
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.utility.HexString
import sdk4.wangpos.libemvbinder.utils.TLV
import wangpos.sdk4.libbasebinder.HEX
import java.util.*

/**
 * Created by Rishav Chudal on 6/14/20.
 */
class EmvUtils {
    companion object {
        fun retrievePAN(): String {
            var pan = WeipassConstant.EMPTY_STRING
            try {
                val outData = ByteArray(100)
                val outDataLen = IntArray(1)
                val result = SDKInstance.mEmvCore.getTLV(
                    EmvTagList.TAG_5A_APPLICATION_PRIMARY_ACCOUNT_NUMBER.tag,
                    outData,
                    outDataLen
                )
                if (result != 0) {
                    WeipassLogger.log("Couldn't retrieve PAN from TLV. Result ::: ".plus(result))
                } else {
                    pan = formatPAN(outData, outDataLen)
                }
            } catch (ex: Exception) {
                WeipassLogger.log("Unable to retrieve PAN. Exception ::: ".plus(ex.message))
            }
            return pan
        }

        private fun formatPAN(
            outData: ByteArray,
            outDataLen: IntArray
        ): String {
            var pan = ByteUtil.bytes2HexString(Arrays.copyOf(outData, outDataLen[0]))
            if (!pan.isNullOrEmptyOrBlank()) {
                if (pan.endsWith("F")) {
                    pan = pan.replace("F", WeipassConstant.EMPTY_STRING)
                }
            } else {
                pan = retrieveTrack2Data().split("D").toTypedArray()[0]
                if (pan.isNullOrEmptyOrBlank()) {
                    pan = WeipassConstant.EMPTY_STRING
                }
            }
            return pan
        }

        private fun retrieveTrack2Data(): String {
            var track2Data: String = WeipassConstant.EMPTY_STRING
            try {
                val outData = ByteArray(100)
                val outDataLen = IntArray(1)
                val result = SDKInstance.mEmvCore
                    .getTLV(EmvTagList.TAG_57_TRACK_2_EQUIVALENT_DATA.tag, outData, outDataLen)
                if (result != 0) {
                    WeipassLogger.log(
                        "Couldn't retrieve Track 2 Data from TLV. Result ::: "
                                + result
                    )
                    return track2Data
                }
                track2Data = formatTrack2Data(
                    outData,
                    outDataLen
                )
            } catch (ex: Exception) {
                WeipassLogger.log(
                    "Unable to retrieve Track 2 Data. Exception ::: ".plus(ex.message)
                )
            }
            return track2Data
        }

        private fun formatTrack2Data(
            outData: ByteArray,
            outDataLen: IntArray
        ): String {
            var track2Data: String =
                ByteUtil.bytes2HexString(Arrays.copyOf(outData, outDataLen[0]))
            if (!track2Data.isNullOrEmptyOrBlank()) {
                val track2DataLen = track2Data.indexOf("F")
                if (track2DataLen >= 0) {
                    track2Data = track2Data.substring(0, track2DataLen)
                }
                if (track2Data.length > 37) {
                    track2Data = track2Data.substring(0, 37)
                }
            } else {
                track2Data = WeipassConstant.EMPTY_STRING
            }
            return track2Data
        }

        fun prepareCardDetailsForTransaction(cardDetails: CardDetails) {
            prepareEmvDataForCardDetails(WeipassStateHandler.readCardRequest!!, cardDetails)
            cardDetails.pinBlock = WeipassStateHandler.pinBlock
            val emvTagCollection = collectEMVTags()
            cardDetails.tagCollection = emvTagCollection
            WeipassLogger.debug(":: EMV UTILS :: TAG COLLECTION :: " + cardDetails.tagCollection)
            WeipassLogger.debug(":: EMV UTILS :: CARD TYPE :: " + cardDetails.cardType)
            val field55Data = retrieveField55Data(emvTagCollection, cardDetails.cardType!!)
            cardDetails.iccDataBlock = field55Data

        }

        private fun prepareEmvDataForCardDetails(
            readCardRequest: ReadCardRequest,
            cardDetails: CardDetails
        ) {
            when (WeipassStateHandler.CURRENT_CARD_TYPE) {
                CardType.PICC -> prepareContactlessCardDetails(readCardRequest, cardDetails)
                CardType.ICC -> prepareContactCardDetails(readCardRequest, cardDetails)
                else -> return
            }
        }

        private fun prepareContactlessCardDetails(
            readCardRequest: ReadCardRequest,
            cardDetails: CardDetails
        ) {
            WeipassLogger.log("Prepare Contactless Card Details...")
            cardDetails.cardType = CardType.PICC
            cardDetails.amount = readCardRequest.amount
            cardDetails.cashBackAmount = readCardRequest.cashBackAmount
            prepareBasicEmvDataForCardDetails(cardDetails)
        }

        private fun prepareContactCardDetails(
            readCardRequest: ReadCardRequest,
            cardDetails: CardDetails
        ) {
            WeipassLogger.log("Prepare Contact Card Details...")
            prepareCardDetailsFromEarlierEmvResponse(readCardRequest, cardDetails)
        }

        fun prepareBasicEmvDataForCardDetails(cardDetails: CardDetails) {
            cardDetails.aid = retrieveAid()
            cardDetails.primaryAccountNumber = retrievePAN()
            cardDetails.trackTwoData = retrieveTrack2Data()
            cardDetails.expiryDate = retrieveExpiryDate()
            cardDetails.primaryAccountNumberSerialNumber = retrieveCardSerialNumber()
            cardDetails.cardHolderName = retrieveCardHolderName()
            cardDetails.tag5F28 = retrieveTag5F28()
            cardDetails.tag9F42 = retrieveTag9F42()
            cardDetails.cardSchemeLabel = retrieveCardSchemeLabel()

            val emvTagCollection = collectEMVTags()
            cardDetails.tagCollection = emvTagCollection
            WeipassLogger.debug(":: EMV UTILS :: TAG COLLECTION :: " + cardDetails.tagCollection)
            WeipassLogger.debug(":: EMV UTILS :: CARD TYPE :: " + cardDetails.cardType)
            val field55Data = retrieveField55Data(emvTagCollection, cardDetails.cardType!!)
            cardDetails.iccDataBlock = field55Data
        }

        private fun retrieveCardSchemeLabel(): String? {
            var cardSchemeLabel: String = WeipassConstant.EMPTY_STRING
            try {
                val outData = ByteArray(32)
                val outDataLen = IntArray(1)
                val result = SDKInstance.mEmvCore.getTLV(
                    EmvTagList.TAG_9F12_APPLICATION_PREFERRED_NAME.tag,
                    outData, outDataLen
                )
                if (result != 0) {
                    WeipassLogger.log("Couldn't retrieve cardSchemeLabel from TLV. Result ::: $result")
                    return cardSchemeLabel
                }
                cardSchemeLabel = formatCardSchemeLabel(outData, outDataLen)
            } catch (ex: RemoteException) {
                WeipassLogger.log("Unable to retrieve cardSchemeLabel. Exception ::: " + ex.message)
            }
            return cardSchemeLabel
        }

        private fun formatCardSchemeLabel(outData: ByteArray, outDataLen: IntArray): String {
            var cardSchemeLabel: String = ByteUtil.bytes2HexString(Arrays.copyOf(outData, outDataLen[0]))
            if (cardSchemeLabel.isNullOrEmptyOrBlank()) {
                return cardSchemeLabel
            }
            var cardSchemeLabelAscii = HexString.hexToString(cardSchemeLabel)
            WeipassLogger.debug("cardSchemeLabel of Card ::: ".plus(cardSchemeLabel))
            WeipassLogger.debug("cardSchemeLabel of Card ::: ".plus(cardSchemeLabelAscii))
            return cardSchemeLabelAscii
        }

        private fun retrieveAid(): String? {
            var aid: String = WeipassConstant.EMPTY_STRING
            try {
                val outData = ByteArray(17)
                val outDataLen = IntArray(1)
                val result = SDKInstance.mEmvCore.getTLV(
                    EmvTagList.TAG_4F_AID.tag,
                    outData, outDataLen
                )
                if (result != 0) {
                    WeipassLogger.log("Couldn't retrieve Aid from TLV. Result ::: $result")
                    return aid
                }
                aid = formatAid(outData, outDataLen)
            } catch (ex: RemoteException) {
                WeipassLogger.log("Unable to retrieve AID. Exception ::: " + ex.message)
            }
            return aid
        }

        private fun formatAid(outData: ByteArray, outDataLen: IntArray): String {
            var aid: String = ByteUtil.bytes2HexString(Arrays.copyOf(outData, outDataLen[0]))
            if (aid.isNullOrEmptyOrBlank()) {
                aid = WeipassConstant.EMPTY_STRING
            }
            WeipassLogger.debug("Aid of Card ::: ".plus(aid))
            return aid
        }

        private fun retrieveExpiryDate(): String {
            var expiryDate: String = WeipassConstant.EMPTY_STRING
            try {
                val outData = ByteArray(100)
                val outDataLen = IntArray(1)
                val result = SDKInstance.mEmvCore
                    .getTLV(EmvTagList.TAG_5F24_CARD_EXPIRATION_DATE.tag, outData, outDataLen)
                if (result != 0) {
                    WeipassLogger.log(
                        "Couldn't retrieve Expiry Data from TLV. Result ::: " +
                                result
                    )
                    expiryDate = formatExpiryDateFromTrack2Data()
                } else {
                    expiryDate = formatExpiryDateWithOutputData(outData, outDataLen)
                }
            } catch (ex: Exception) {
                WeipassLogger.log("Unable to retrieve Expiry Date...")
                expiryDate = WeipassConstant.EMPTY_STRING

            }
            return expiryDate
        }

        private fun formatExpiryDateWithOutputData(
            outData: ByteArray,
            outDataLen: IntArray
        ): String {
            var expiryDate: String
            expiryDate = ByteUtil
                .bytes2HexString(outData.copyOf(outDataLen[0]))
                .substring(0, 4)
            if (expiryDate.isNullOrEmptyOrBlank()) {
                expiryDate = formatExpiryDateFromTrack2Data()
            }
            WeipassLogger.debug("Formatted Expiry Date From Output Data ::: ".plus(expiryDate))
            return expiryDate
        }

        private fun formatExpiryDateFromTrack2Data(): String {
            val expiryDate: String
            val data = retrieveTrack2Data().split("D").toTypedArray()[1]
            expiryDate = if (data.isNullOrEmptyOrBlank()) {
                WeipassConstant.EMPTY_STRING
            } else {
                data.substring(0, 4)
            }
            WeipassLogger.debug("Formatted Expiry Date From Track 2 Data ::: ".plus(expiryDate))
            return expiryDate
        }

        private fun retrieveCardSerialNumber(): String {
            var cardSerialNumber: String = WeipassConstant.EMPTY_STRING
            try {
                var outData = ByteArray(100)
                val outDataLen = IntArray(1)
                val result = SDKInstance.mEmvCore
                    .getTLV(EmvTagList.TAG_5F34_PAN_SEQUENCE_NUMBER.tag, outData, outDataLen)
                if (result != 0) {
                    WeipassLogger.log(
                        "Couldn't retrieve Card Serial Number from TLV. Result ::: "
                                + result
                    )
                    return WeipassConstant.EMPTY_STRING
                }
                cardSerialNumber = formatCardSerialNumber(
                    outData,
                    outDataLen
                )
            } catch (ex: RemoteException) {
                WeipassLogger.log(
                    "Unable to retrieve Card Serial Number. Exception ::: "
                            + ex.message
                )
            }
            return cardSerialNumber
        }

        private fun formatCardSerialNumber(
            outData: ByteArray,
            outDataLen: IntArray
        ): String {
            var cardSerialNumber: String
            if (outDataLen[0] > 0) {
                cardSerialNumber = HEX.bytesToHex(outData.copyOf(outDataLen[0]))
            } else {
                cardSerialNumber = "00"
            }
            if (cardSerialNumber.isNullOrEmptyOrBlank()) {
                cardSerialNumber = WeipassConstant.EMPTY_STRING
            }
            WeipassLogger.debug("Card Serial Number ::: $cardSerialNumber")
            return cardSerialNumber
        }

        private fun retrieveCardHolderName(): String {
            var cardHolderName: String = WeipassConstant.EMPTY_STRING
            try {
                val outData = ByteArray(100)
                val outDataLen = IntArray(1)
                val result = SDKInstance.mEmvCore
                    .getTLV(EmvTagList.TAG_5F20_CARD_HOLDER_NAME.tag, outData, outDataLen)
                if (result != 0) {
                    WeipassLogger.log(
                        "Couldn't retrieve Card Holder Name from TLV. Result ::: " +
                                result
                    )
                    return WeipassConstant.EMPTY_STRING
                }
                cardHolderName = formatCardHolderName(
                    outData,
                    outDataLen
                )
            } catch (ex: RemoteException) {
                WeipassLogger.log(
                    "Unable to retrieve Card holder name. Exception ::: "
                        .plus(ex.message)
                )
            }
            return cardHolderName
        }

        private fun formatCardHolderName(outData: ByteArray, outDataLen: IntArray): String {
            var cardHolderName: String
            cardHolderName = String(Arrays.copyOf(outData, outDataLen[0]))
            if (cardHolderName.isNullOrEmptyOrBlank()) {
                cardHolderName = WeipassConstant.EMPTY_STRING
            }
            WeipassLogger.debug("Card Holder Name ::: $cardHolderName")
            return cardHolderName
        }

        private fun retrieveTag5F28(): String {
            var tag5F28: String = WeipassConstant.EMPTY_STRING
            try {
                val emvTag: EmvTag? = retrieveFromTLV(WeipassConstant.TAG_5F28)
                if (emvTag != null) {
                    tag5F28 = emvTag.data
                }
            } catch (ex: java.lang.Exception) {
                WeipassLogger.log("Unable to retrieve tag 5F28. Exception ::: ".plus(ex.message))
            }
            WeipassLogger.debug("Tag 5F28 ::: $tag5F28")
            return tag5F28
        }

        fun retrieveFromTLV(tag: Int): EmvTag? {
            val outData = ByteArray(100)
            try {
                val outDataLen = IntArray(1)
                val result = SDKInstance.mEmvCore.getTLV(tag, outData, outDataLen)
                if (result != 0) {
                    val tagInHex = Integer.toHexString(tag).toUpperCase(Locale.getDefault())
                    WeipassLogger.log(
                        "Couldn't retrieve tag : "
                                + tagInHex
                                + " from TLV. Result ::: "
                                + result
                    )
                }
                val data: String = ByteUtil.bytes2HexString(Arrays.copyOf(outData, outDataLen[0]))
                return EmvTag(
                    tag,
                    data,
                    outDataLen[0]
                )
            } catch (ex: RemoteException) {
                WeipassLogger.log("Retrieve TLV, Exception : " + ex.message)
            }
            return null
        }

        private fun retrieveTag9F42(): String {
            var tag9F42: String = WeipassConstant.EMPTY_STRING
            try {
                val emvTag: EmvTag? = retrieveFromTLV(WeipassConstant.TAG_9F42)
                if (emvTag != null) {
                    tag9F42 = emvTag.data
                }
            } catch (ex: java.lang.Exception) {
                WeipassLogger.log("Unable to retrieve tag 9F42. Exception ::: " + ex.message)
            }
            WeipassLogger.debug("Tag 9F42 ::: $tag9F42")
            return tag9F42
        }

        private fun prepareCardDetailsFromEarlierEmvResponse(
            readCardRequest: ReadCardRequest,
            cardDetails: CardDetails
        ) {
            val cardDetailsBeforeEmvNext = readCardRequest.cardDetailsBeforeEmvNext
            if (cardDetailsBeforeEmvNext != null) {
                cardDetails.transactionAcceptedByCard = true
                cardDetails.aid = cardDetailsBeforeEmvNext.aid
                cardDetails.cashBackAmount = readCardRequest.cashBackAmount
                cardDetails.amount = readCardRequest.amount
                cardDetails.cardType = cardDetailsBeforeEmvNext.cardType
                cardDetails.primaryAccountNumber = cardDetailsBeforeEmvNext.primaryAccountNumber
                cardDetails.primaryAccountNumberSerialNumber = cardDetailsBeforeEmvNext.primaryAccountNumberSerialNumber
                cardDetails.expiryDate = cardDetailsBeforeEmvNext.expiryDate
                cardDetails.trackTwoData = cardDetailsBeforeEmvNext.trackTwoData
                cardDetails.cardScheme = cardDetailsBeforeEmvNext.cardScheme
                cardDetails.cardHolderName = cardDetailsBeforeEmvNext.cardHolderName
                cardDetails.tag9F42 = cardDetailsBeforeEmvNext.tag9F42
                cardDetails.tag5F28 = cardDetailsBeforeEmvNext.tag5F28
                cardDetails.cardSchemeLabel = cardDetailsBeforeEmvNext.cardSchemeLabel
            }
        }

        fun checkForPICCCvm(cardType: CardType): PICCCvm {
            WeipassLogger.log("checkForContactlessCVM")
            var PICCCvm: PICCCvm = PICCCvm.NO_CVM
            if (cardType == CardType.PICC) {
                WeipassLogger.debug("checkForContactlessCvm SlotType ::: $cardType")
                try {
                    val outComeData = TransResult.getOutCome()
                    if (outComeData != null) {
                        WeipassLogger.debug("Contactless TransResult Outcome ::: $outComeData")
                        val df8129Value = outComeData.dF8129_Value
                        if (!df8129Value.isNullOrEmptyOrBlank() && df8129Value.length == 16) {
                            WeipassLogger.debug("checkForContactlessCvm DF8129 ::: $df8129Value")
                            PICCCvm = WeipassOutcomeCVMParser
                                .retrieveCvmByTransactionOutcome(df8129Value)
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            return PICCCvm
        }

        fun getOutComeDF8129Msg(): String {
            var message: String? = WeipassConstant.EMPTY_STRING
            try {
                val outComeData = TransResult.getOutCome()

                if (outComeData != null) {
                    WeipassLogger.debug("Contactless TransResult Outcome ::: $outComeData")
                    val df8129_msg = outComeData.dF8129_Msg
                    if (!df8129_msg.isNullOrEmptyOrBlank()) {
                        message = df8129_msg
                    }
                }
            } catch (e: Exception){
                e.printStackTrace()
            }
            return message!!
        }

        fun getIssuerScriptResults(): String {
            var issuerScriptResults: String = WeipassConstant.EMPTY_STRING
            var outData: ByteArray? = ByteArray(256)
            val outDataLen = IntArray(1)
            var result: Int
            try {
                result = SDKInstance.mEmvCore.getScriptResult(outData, outDataLen)
                WeipassLogger.log("getScriptProcessingResult ::: $result")
                if (result == 0) {
                    outData = (outData!!).copyOf(outDataLen[0])
                    val sdkResult =
                        HEX.bytesToHex(outData)
                    WeipassLogger.debug("SDK Issuer Script Result ::: $sdkResult")
                    issuerScriptResults = sdkResult.substring(2)
                    WeipassLogger.debug("Formatted Issuer Script Result ::: $issuerScriptResults")
                    return issuerScriptResults
                }
            } catch (ex: Exception) {
                WeipassLogger.log("saveScriptResult exception ::: ".plus(ex.message))
            }
            return issuerScriptResults
        }

        private fun collectEMVTags(): EMVTagCollection? {
            WeipassLogger.log("================== Collecting EMV Tags =========================")
            val collection = EMVTagCollection()
            val iccDataList = IccData.values().toList()
            /*if (iccDataList.isEmpty()) {
                iccDataList = IccData.values().toList()
            }*/
            for (iccData in iccDataList) {
                val emvTag: EmvTag? = retrieveFromTLV(iccData.tag)
                if (emvTag != null) {
                    appendEmptyTag("9F03", 6, emvTag)
                    appendEmptyTag("9F34", 3, emvTag)
                    var value: String = emvTag.data
                    if (emvTag.tag == IccData.APPLICATION_PAN.tag) {
                        value = WeipassDeviceUtils.maskCardNumber(value)
                    }
                    if (emvTag.tag == IccData.TRACK_2.tag) {
                        value = WeipassDeviceUtils.maskTrack2(value)
                    }
                    WeipassLogger.debug(
                        "EMV Tag ::: " +
                                Integer.toHexString(emvTag.tag).toUpperCase(Locale.getDefault()) +
                                ", Value ::: " + value
                    )
                    if (emvTag.size == 0) {
                        continue
                    }
                    collection.add(emvTag)
                }
            }
            return collection
        }


        private fun appendEmptyTag(
            tagInHex: String,
            tagLength: Int,
            emvTag: EmvTag
        ) {
            val emvTagInHex =
                Integer.toHexString(emvTag.tag).toUpperCase(Locale.getDefault())
            if (emvTagInHex.equals(tagInHex, ignoreCase = true) && emvTag.size == 0) {
                emvTag.size = tagLength
                emvTag.data = ofRequiredLength(0, tagLength * 2)
            }
        }

        private fun ofRequiredLength(value: Int, length: Int): String {
            val stringValue = value.toString()
            if (stringValue.length < length) {
                val requiredZeros = length - stringValue.length
                val beforeZeros = StringBuilder()
                for (i in 0 until requiredZeros) {
                    beforeZeros.append("0")
                }
                beforeZeros.append(stringValue)
                return beforeZeros.toString()
            }
            return stringValue.substring(0, length)
        }

        private fun retrieveField55Data(
            _EMVTagCollection: EMVTagCollection?,
            cardType: CardType
        ): String {
            WeipassLogger.debug("Retrieving Field 55 data, slot type ::: $cardType")
            var emvTagCollection = _EMVTagCollection
            var field55Data: String = WeipassConstant.EMPTY_STRING
            try {
                if (emvTagCollection == null) {
                    emvTagCollection = collectEMVTags()
                }
                val tags = emvTagCollection!!.toMap()
                var field55 = ""
                var iccDataList = WeipassStateHandler.readCardRequest!!.iccDataList
                if (iccDataList.isEmpty()) {
                    iccDataList = IccData.values().toList()
                }
                for (iccdata in iccDataList) {
                    if ((cardType === CardType.ICC && iccdata === IccData.CTQ) ||
                        (cardType === CardType.ICC && iccdata === IccData.TTQ) ||
                        iccdata === IccData.TSI ||
                        iccdata === IccData.PIN_BLOCK ||
                        iccdata === IccData.KERNEL_ID ||
                        iccdata == IccData.APPLICATION_PAN || // TODO REMOVE FROM HERE
                        iccdata == IccData.TRACK_2 ||
                        iccdata == IccData.CTQ ||
                        iccdata == IccData.TTQ
                    ) {
                        continue
                    }
                    field55 = field55
                        .plus(Integer.toHexString(iccdata.tag).toUpperCase(Locale.getDefault()))
                        .plus(",")
                }
                field55 = field55.substring(0, field55.length - 1)
                WeipassLogger.debug("Field 55 Tags ::: $field55")
                field55Data = TLV.pack(tags, field55)
            } catch (ex: Exception) {
                WeipassLogger.log("Exception :::  ".plus(ex.message))
            }
            if (field55Data.isNullOrEmptyOrBlank()) {
                WeipassLogger.log("Unable to prepare ICC Data")
                throw Exception(WeipassConstant.MSG_READ_CARD_FAIL)
            }
            return field55Data
        }

        fun prepareReadCardResponseForContactless(
            readCardRequest: ReadCardRequest
        ): ReadCardResponse {
            val cardDetails =
                CardDetails()
            cardDetails.transactionState =
                readCardRequest.cardDetailsBeforeEmvNext!!.transactionState
            cardDetails.tagCollection =
                readCardRequest.cardDetailsBeforeEmvNext!!.tagCollection
            cardDetails.iccDataBlock = readCardRequest.cardDetailsBeforeEmvNext!!.iccDataBlock
            cardDetails.pinBlock = readCardRequest.cardDetailsBeforeEmvNext!!.pinBlock
            cardDetails.transactionAcceptedByCard = true
            cardDetails.aid = readCardRequest.cardDetailsBeforeEmvNext!!.aid
            cardDetails.cashBackAmount = readCardRequest.cashBackAmount
            cardDetails.amount = readCardRequest.amount
            cardDetails.cardType = readCardRequest.cardDetailsBeforeEmvNext!!.cardType
            cardDetails.primaryAccountNumber = readCardRequest.cardDetailsBeforeEmvNext!!.primaryAccountNumber
            cardDetails.primaryAccountNumberSerialNumber = readCardRequest.cardDetailsBeforeEmvNext!!.primaryAccountNumberSerialNumber
            cardDetails.expiryDate = readCardRequest.cardDetailsBeforeEmvNext!!.expiryDate
            cardDetails.trackTwoData = readCardRequest.cardDetailsBeforeEmvNext!!.trackTwoData
            cardDetails.cardScheme = readCardRequest.cardDetailsBeforeEmvNext!!.cardScheme
            cardDetails.cardHolderName = readCardRequest.cardDetailsBeforeEmvNext!!.cardHolderName
            cardDetails.cardSchemeLabel = readCardRequest.cardDetailsBeforeEmvNext!!.cardSchemeLabel
            cardDetails.transactionInitializeDateTime =
                readCardRequest.cardDetailsBeforeEmvNext!!.transactionInitializeDateTime
            val readCardResponse =
                ReadCardResponse(
                    cardDetails, Result.SUCCESS,
                    WeipassConstant.MSG_CL_SECOND_PHASE_SUCCESS
                )
            readCardResponse.piccCvm = checkForPICCCvm(CardType.PICC)
            return readCardResponse
        }


        fun clearEmvLogHistory(): Int {
            var result = -1
            try {
                result = SDKInstance.mEmvCore.clearTransLog()
            } catch (ex: java.lang.Exception) {
                WeipassLogger.log("clearEmvLogHistory exception ::: " + ex.message)
            }
            return result
        }

        fun getValueFromTLV(emvTagList: EmvTagList, data: String): String {
            var value: String = WeipassConstant.EMPTY_STRING
            val tlvList: TLVList = TLVList.fromBinary(data)
            val test: String = ByteUtil.intToHexString(emvTagList.tag)
                .replace("^0+(?!$)".toRegex(), "")
            if (tlvList.getTLV(test) != null) {
                value = tlvList.getTLV(test).getValue()
            }
            WeipassLogger.debug("getValueFromTLV TAG ::: " + Integer.toHexString(emvTagList.tag))
            WeipassLogger.debug("getValueFromTLV Value ::: $value")
            return value
        }

        fun prepareCardSummary(): CardSummary {
            return CardSummary(
                cardSchemeLabel = retrieveCardSchemeLabel().defaultOrEmptyValue(),
                primaryAccountNumber = retrievePAN().defaultOrEmptyValue(),
                cardHolderName = retrieveCardHolderName().defaultOrEmptyValue(),
                expiryDate = retrieveExpiryDate().defaultOrEmptyValue()
            )
        }
    }
}