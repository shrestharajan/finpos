package global.citytech.finpos.merchant.presentation.appnotification

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityAppNotificationBinding
import global.citytech.finpos.merchant.domain.model.app.Action
import global.citytech.finpos.merchant.domain.model.app.AppNotification
import global.citytech.finpos.merchant.domain.model.app.Type
import global.citytech.finpos.merchant.presentation.alertdialogs.AppNotificationViewDialog
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.disclaimer.DisclaimerActivity
import global.citytech.finpos.merchant.presentation.mocoqr.MocoQrDisclaimerActivity
import global.citytech.finpos.merchant.presentation.settlement.SettlementActivity
import kotlinx.android.synthetic.main.activity_app_notification.*

/**
 * Created by Unique Shakya on 5/31/2021.
 */
class AppNotificationActivity :
    AppBaseActivity<ActivityAppNotificationBinding, AppNotificationViewModel>() {

    private var appNotificationListener = object : AppNotificationAdapter.Listener {
        override fun onAppNotificationClicked(appNotification: AppNotification) {
            if (appNotification.action == Action.DISCLAIMER) {
                startDisclaimerActivity(appNotification.referenceId!!)
            } else if (appNotification.action == Action.QR_DISCLAIMER) {
                startMocoDisclaimerActivity()
            } else {
                showAlertDialog(appNotification)
            }
        }
    }

    private fun startMocoDisclaimerActivity() {
        val intent = Intent(this, MocoQrDisclaimerActivity::class.java)
        startActivityForResult(intent, MocoQrDisclaimerActivity.REQUEST_CODE)
    }

    private fun startDisclaimerActivity(referenceId: String) {
        val intent = DisclaimerActivity.getLaunchIntent(this, referenceId)
        startActivityForResult(intent, DisclaimerActivity.REQUEST_CODE)
    }

    private fun showAlertDialog(appNotification: AppNotification) {
        val appNotificationViewDialog = AppNotificationViewDialog(this, appNotification,
            object : AppNotificationViewDialog.Listener {
                override fun onPositiveButtonClick() {
                    if (appNotification.type != Type.SUCCESS)
                        getViewModel().handleAppNotificationPositiveAction(appNotification)
                }

                override fun onNegativeButtonClick() {
                    // only dismiss dialog
                }
            })
        appNotificationViewDialog.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel().retrieveAppNotifications()
        initObservers()
        initViews()
    }

    private fun initViews() {
        iv_back.setOnClickListener {
            finish()
        }
    }

    private fun initObservers() {
        getViewModel().appNotificationsLiveData.observe(this, Observer {
            updateAppNotifications(it)
        })

        getViewModel().startSettlementLiveData.observe(this, Observer {
            startSettlementActivity()
        })

        getViewModel().allNotificationsCleared.observe(this, Observer {
            if (it)
                finish()
            else
                getViewModel().retrieveAppNotifications()
        })
    }

    private fun startSettlementActivity() {
        SettlementActivity.getLaunchIntent(
            this,
            isManualSettlement = false,
            isSettlementSuccessConfirmationRequired = true
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SettlementActivity.REQUEST_CODE) {
            getViewModel().retrieveAppNotifications()
        } else if (requestCode == DisclaimerActivity.REQUEST_CODE || requestCode == MocoQrDisclaimerActivity.REQUEST_CODE) {
            getViewModel().checkForRemainingNotifications()
        }
    }

    private fun updateAppNotifications(it: List<AppNotification>?) {
        it?.let {
            if (it.isEmpty()) {
                handleNoNotifications()
            } else {
                cl_no_notifications.visibility = View.GONE
                rv_notifications.visibility = View.VISIBLE
                rv_notifications.layoutManager = LinearLayoutManager(this)
                rv_notifications.adapter = AppNotificationAdapter(it, appNotificationListener)
            }
        }
    }

    private fun handleNoNotifications() {
        rv_notifications.visibility = View.GONE
        cl_no_notifications.visibility = View.VISIBLE
    }

    override fun getBindingVariable(): Int = BR.appNotificationViewModel

    override fun getLayout(): Int = R.layout.activity_app_notification

    override fun getViewModel(): AppNotificationViewModel =
        ViewModelProviders.of(this)[AppNotificationViewModel::class.java]
}