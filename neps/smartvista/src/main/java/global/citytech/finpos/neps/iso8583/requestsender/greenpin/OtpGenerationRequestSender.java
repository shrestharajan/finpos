package global.citytech.finpos.neps.iso8583.requestsender.greenpin;

import global.citytech.finpos.neps.iso8583.FunctionCodeConfig;
import global.citytech.finpos.neps.iso8583.NepsMessagePayloadBuilder;
import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.PosEntryModeConfig;
import global.citytech.finpos.neps.iso8583.ProcessingCode;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.switches.FunctionCode;
import global.citytech.finposframework.utility.HelperUtils;

public class OtpGenerationRequestSender extends NepsMessageSenderTemplate {

    private final Logger logger = Logger.getLogger(OtpGenerationRequestSender.class.getName());
    protected OtpGenerationIsoRequest otpGenerationIsoRequest;

    public OtpGenerationRequestSender(RequestContext context) {
        super(context);
        otpGenerationIsoRequest = (OtpGenerationIsoRequest) context.getRequest();
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Authorization,
                        MTI.MessageFunction.Request,
                        MTI.Originator.Acquirer);
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);
        MessagePayload.Builder builder = new NepsMessagePayloadBuilder(this.specInfo, msg);
        msg.addField(
                new DataElement(
                        DataElement.PRIMARY_ACCOUNT_NUMBER,
                        otpGenerationIsoRequest.getPan())
        );
        msg.addField(
                new DataElement(
                        DataElement.AMOUNT,
                        HelperUtils.toISOAmount(otpGenerationIsoRequest.getTransactionAmount(), 12))
        );
        msg.addField(
                new DataElement(
                        DataElement.LOCAL_TIME,
                otpGenerationIsoRequest.getLocalDateTime())

        );

        msg.addField(
                new DataElement(
                        DataElement.PAN_EXPIRE_DATE,
                        otpGenerationIsoRequest.getExpiryDate())
        );

        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(otpGenerationIsoRequest.getPosEntryMode()))
        );

        msg.addField(
                new DataElement(
                        DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE,
                        otpGenerationIsoRequest.getPosConditionCode()
                )
        );

        msg.addField(
                new DataElement(
                        DataElement.TRACK_II_DATA,
                        otpGenerationIsoRequest.getTrack2Data()
                )
        );

        msg.addField(
                new DataElement(
                        DataElement.CURRENCY_CODE,
                        otpGenerationIsoRequest.getCurrencyCode()
                )
        );

        if (otpGenerationIsoRequest.getEmvData() != null) {
            msg.addField(
                    new DataElement(DataElement.ICC_SYSTEM_RELATED_DATA,
                            otpGenerationIsoRequest.getEmvData())
            );
        }

        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());

        return builder.build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.OTP_GENERATION.getCode();
    }

    @Override
    protected String getFunctionCode() {
        return FunctionCodeConfig.getValue(FunctionCode.GREEN_PIN);
    }
}
