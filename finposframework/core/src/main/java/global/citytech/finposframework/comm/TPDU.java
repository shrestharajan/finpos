package global.citytech.finposframework.comm;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

import global.citytech.finposframework.utility.ByteUtils;
import global.citytech.finposframework.utility.EncodingUtils;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.exceptions.FinPosException;

/** User: Surajchhetry Date: 2/19/20 Time: 5:17 PM */
public class TPDU {
  public static final int SIZE_IN_BYTES = 5;
  private final byte id;
  private final byte[] destinationAddress;
  private final byte[] originatorAddress;

  public TPDU(byte id, byte[] destinationAddress, byte[] originatorAddress) {
    this.id = id;
    this.destinationAddress = Arrays.copyOfRange(destinationAddress, 0, destinationAddress.length);
    this.originatorAddress = Arrays.copyOfRange(originatorAddress, 0, originatorAddress.length);
  }

  public byte[] getAsBytes() {
    try (ByteArrayOutputStream bout = new ByteArrayOutputStream()) {
      bout.write(id);
      bout.write(destinationAddress);
      bout.write(originatorAddress);
      return bout.toByteArray();

    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

  public static TPDU fromHexString(String hexData) {
    if (HelperUtils.isBlank(hexData))
      throw new FinPosException(
          FinPosException.ExceptionType.INVALID_PARAMETER_SUPPLIED, "TPDU/NII can't be empty");
    byte[] data = EncodingUtils.hex2Bytes(hexData);
    if (data.length != 4)
      throw new FinPosException(
          FinPosException.ExceptionType.INVALID_PARAMETER_SUPPLIED,
          "TPDU/NII length is invalid.Should be 4 bytes");
    return new TPDU((byte) 0x60, ByteUtils.subByte(data, 0, 2), ByteUtils.subByte(data, 2, 2));
  }
}
