package global.citytech.neps.data.db.dao

import android.database.Cursor
import androidx.room.*
import global.citytech.neps.domain.model.Host


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
@Dao
interface HostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(hosts: List<Host>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(host: Host): Long

    @Query("SELECT * FROM host")
    fun get(): Cursor

    @Query("SELECT * FROM host")
    fun getHostList(): List<Host>

    @Query("SELECT * FROM host where id = :id")
    fun getById(id: Long): Cursor?

    @Update
    fun update(host: Host):Int

    @Query("DELETE FROM host WHERE id = :id")
    fun deleteById(id: Long): Int


}