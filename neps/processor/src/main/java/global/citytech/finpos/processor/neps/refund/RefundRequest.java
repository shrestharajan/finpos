package global.citytech.finpos.processor.neps.refund;

import global.citytech.finpos.processor.neps.transaction.TransactionRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.io.sound.SoundService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;

public class RefundRequest extends TransactionRequest {
    public static  class Builder {
        private TransactionRepository transactionRepository;
        private ReadCardService readCardService;
        private DeviceController deviceController;
        private TransactionAuthenticator transactionAuthenticator;
        private PrinterService printerService;
        private ApplicationRepository applicationRepository;
        private global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest;
        private LedService ledService;
        private SoundService soundService;


        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public Builder withReadCardService(ReadCardService readCardService) {
            this.readCardService = readCardService;
            return this;
        }

        public Builder withDeviceController(DeviceController deviceController) {
            this.deviceController = deviceController;
            return this;
        }

        public Builder withTransactionAuthenticator(TransactionAuthenticator transactionAuthenticator) {
            this.transactionAuthenticator = transactionAuthenticator;
            return this;
        }

        public Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public Builder withTransactionRequest(global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest) {
            this.transactionRequest = transactionRequest;
            return this;
        }

        public Builder withLedService(LedService ledService) {
            this.ledService = ledService;
            return this;
        }

        public Builder withSoundService(SoundService soundService) {
            this.soundService = soundService;
            return this;
        }

        public RefundRequest build() {
            RefundRequest refundRequest = new RefundRequest();
            refundRequest.applicationRepository = this.applicationRepository;
            refundRequest.transactionRequest = this.transactionRequest;
            refundRequest.transactionRepository = this.transactionRepository;
            refundRequest.readCardService = this.readCardService;
            refundRequest.transactionAuthenticator = this.transactionAuthenticator;
            refundRequest.deviceController = this.deviceController;
            refundRequest.printerService = this.printerService;
            refundRequest.ledService = this.ledService;
            refundRequest.soundService = this.soundService;
            return refundRequest;
        }
    }

}

