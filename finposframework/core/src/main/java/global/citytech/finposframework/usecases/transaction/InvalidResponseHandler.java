package global.citytech.finposframework.usecases.transaction;

import global.citytech.finposframework.iso8583.IsoMessageResponse;

/**
 * Created by Unique Shakya on 10/2/2020.
 */
public interface InvalidResponseHandler {
    InvalidResponseHandlerResponse isInvalidResponse(IsoMessageResponse isoMessageResponse);
}
