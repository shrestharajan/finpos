package global.citytech.finpos.merchant.presentation.model

import global.citytech.finpos.merchant.domain.model.app.Logo

data class LogoItem(
    var id: String,
    var appWallpaper: String? = null,
    var displayLogo: String? = null,
    var printLogo: String? = null
)

fun Logo.mapToPresentation(): LogoItem = LogoItem(id, appWallpaper, displayLogo, printLogo)
fun List<Logo>.mapToPresentation(): List<LogoItem> = map { it.mapToPresentation() }