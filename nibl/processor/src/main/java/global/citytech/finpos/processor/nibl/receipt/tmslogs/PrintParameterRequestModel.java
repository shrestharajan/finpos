package global.citytech.finpos.processor.nibl.receipt.tmslogs;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.tmslogs.TmsLogRequestParameter;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.transaction.receipt.tmslogs.TmsLogsRequest;


/**
 * Created by Rishav Chudal on 11/5/20.
 */
public class PrintParameterRequestModel implements UseCase.Request, TmsLogRequestParameter {
    private TmsLogsRequest tmsLogsRequest;
    private ApplicationRepository applicationRepository;
    private DeviceController deviceController;
    private PrinterService printerService;

    public TmsLogsRequest getTmsLogsRequest() {
        return tmsLogsRequest;
    }

    public ApplicationRepository getApplicationRepository() {
        return applicationRepository;
    }

    public DeviceController getDeviceController() {
        return deviceController;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public static final class Builder {
        private TmsLogsRequest tmsLogsRequest;
        private ApplicationRepository applicationRepository;
        private DeviceController deviceController;
        private PrinterService printerService;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withTmsLogsRequest(TmsLogsRequest tmsLogsRequest) {
            this.tmsLogsRequest = tmsLogsRequest;
            return this;
        }

        public Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public Builder withDeviceController(DeviceController deviceController) {
            this.deviceController = deviceController;
            return this;
        }

        public Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public PrintParameterRequestModel build() {
            PrintParameterRequestModel printParameterRequestModel = new PrintParameterRequestModel();
            printParameterRequestModel.tmsLogsRequest = this.tmsLogsRequest;
            printParameterRequestModel.deviceController = this.deviceController;
            printParameterRequestModel.printerService = this.printerService;
            printParameterRequestModel.applicationRepository = this.applicationRepository;
            return printParameterRequestModel;
        }
    }
}
