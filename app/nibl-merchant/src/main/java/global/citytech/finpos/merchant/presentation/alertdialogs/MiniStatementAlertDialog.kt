package global.citytech.finpos.merchant.presentation.alertdialogs

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.dashboard.temp.DashActivity
import global.citytech.finpos.merchant.presentation.ministatement.MiniStatementAdapter
import global.citytech.finposframework.usecases.transaction.data.StatementItems

class MiniStatementAlertDialog(
    val miniStatementData: List<StatementItems>,
    val bankLogo: ByteArray,
    val dialogListener: MiniStatementDialogListener
) : DialogFragment() {
    val TAG = MiniStatementAlertDialog::class.java
    var miniStatementDataList: List<StatementItems> = arrayListOf()
    var bankLogoDisplay: ByteArray = byteArrayOf()
    var bankLogoBitMap: Bitmap
    var isButtonClicked = false

    companion object {
        const val TAG = "MiniStatementDialog"
    }
    init {
        miniStatementDataList = miniStatementData
        bankLogoDisplay = bankLogo
        bankLogoBitMap = BitmapFactory.decodeByteArray(bankLogoDisplay, 0, bankLogoDisplay.size)


        println("BANK DISPLAY LOGO DATA::::"+bankLogoDisplay.toString())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        hideDialogTitle()
        return inflater.inflate(R.layout.ministatement_list, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView = view.findViewById<RecyclerView>(R.id.rv_mini_statement_list)
        val bankLogoImageView: ImageView = view.findViewById(R.id.iv_bank_logo_mini_statement)
        val noTransactionTextView: AppCompatTextView = view.findViewById(R.id.tv_no_transaction)
        val button = view.findViewById<Button>(R.id.btn_print_mini_statement)
        val cbutton = view.findViewById<Button>(R.id.btn_print_cancel)
        bankLogoImageView.setImageBitmap(bankLogoBitMap)
        val adapter = MiniStatementAdapter(miniStatementDataList)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        view.findViewById<AppCompatImageView>(R.id.iv_back).setOnClickListener {
            context?.startActivity(Intent(view.context, DashActivity::class.java))

        }

        if(miniStatementDataList.isEmpty()){
            recyclerView.visibility= View.GONE
            noTransactionTextView.visibility= View.VISIBLE
            button.visibility= View.GONE
            cbutton.visibility= View.VISIBLE
        }else{
            recyclerView.visibility= View.VISIBLE
            noTransactionTextView.visibility= View.GONE
            button.visibility= View.VISIBLE
            cbutton.visibility= View.GONE
        }

        button.setOnClickListener {
            println("Clicked first")
            dialogListener.onPrintMiniStatementClicked()
            isButtonClicked= true
            dismiss()
        }

        cbutton.setOnClickListener{
            dialogListener.onCancelMiniStatementClicked()
            dismiss()
        }
    }

    override fun onResume() {
        super.onResume()
        setDialogLayoutParams()
    }

    private fun setDialogLayoutParams() {
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog?.window?.attributes = layoutParams
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private fun hideDialogTitle() {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
    }

    override fun getTheme(): Int {
        return R.style.FullScreenDialog
    }
}

interface MiniStatementDialogListener {
    fun onPrintMiniStatementClicked()
    fun onCancelMiniStatementClicked()
}
