package global.citytech.finpos.processor.neps.transaction;

import java.util.ArrayList;
import java.util.List;


import global.citytech.finposframework.hardware.io.printer.Printable;
import global.citytech.finposframework.hardware.io.printer.PrinterRequest;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finposframework.utility.ReceiptUtils.addMultiColumnString;
import static global.citytech.finposframework.utility.ReceiptUtils.addSingleColumnString;
import static global.citytech.finposframework.utility.ReceiptUtils.createStringList;
import static global.citytech.finposframework.utility.ReceiptUtils.getValueOrEmpty;
import static global.citytech.finposframework.utility.ReceiptUtils.putSpaceAfterEveryTwoCharacters;

/**
 * Created by Rishav Chudal on 9/8/20.
 */
public class NepsIsoMessageReceiptHandler implements
        ReceiptHandler.IsoMessageReceiptHandler {

    private List<Printable> printableList;
    private IsoMessageResponse isoMessageResponse;
    private PrinterService printerService;

    public NepsIsoMessageReceiptHandler(PrinterService printerService) {
        this.printerService = printerService;
        this.printableList = new ArrayList<>();
    }

    public List<Printable> prepareDebugLogReceipt() {
        prepareDebugRequestLogReceipt();
        prepareDebugResponseLogReceipt();
        return this.printableList;
    }

    @Override
    public void printIsoMessageReceipt(IsoMessageResponse isoMessageResponse) {
        this.isoMessageResponse = isoMessageResponse;
        prepareDebugRequestLogReceipt();
        prepareDebugResponseLogReceipt();
        PrinterRequest printerRequest = new PrinterRequest(this.printableList);
        printerRequest.setFeedPaperAfterFinish(false);
        this.printerService.print(printerRequest);
    }

    private void prepareDebugRequestLogReceipt() {
        List<DataElement> dataElements = this.isoMessageResponse
                .getRequestPayload()
                .getRequestMsg()
                .getDataElements();
        addSingleColumnString(
                printableList,
                prepareTimeString(getRequestLocalTime())
                        .concat("   ")
                        .concat("Request DEBUG"),
                TransactionReceiptStyle.DEBUG_LOG.getStyle()
        );
        addSingleColumnString(
                printableList,
                "Length: ".concat(getRequestMessageLength()),
                TransactionReceiptStyle.DEBUG_LOG.getStyle()
        );
        addSingleColumnString(
                printableList,
                "TPDU: ".concat(getRequestTPDU()),
                TransactionReceiptStyle.DEBUG_LOG.getStyle()
        );
        addSingleColumnString(
                this.printableList,
                "MTI: ".concat(getRequestMTI()),
                TransactionReceiptStyle.DEBUG_LOG.getStyle()

        );
        addMultiColumnString(
                this.printableList,
                createStringList(
                        getValueOrEmpty(String.valueOf(1)),
                        " : ",
                        putSpaceAfterEveryTwoCharacters(this.isoMessageResponse
                                .getRequestPayload()
                                .getRequestMsg()
                                .getBitMap()
                                .asHexValue())
                ),
                new int[]{10, 5, 85},
                TransactionReceiptStyle.DATA_ELEMENT.getStyle()
        );
        addDataElementsToPrintList(dataElements);
        addSingleColumnString(
                printableList,
                "  ",
                TransactionReceiptStyle.DIVIDER.getStyle()
        );
//        addSingleColumnString(
//                printableList,
//                isoMessageResponse.getDebugRequestString(),
//                TransactionReceiptStyle.DEBUG_LOG.getStyle()
//        );
        addSingleColumnString(
                printableList,
                "  ",
                TransactionReceiptStyle.DIVIDER.getStyle()
        );
    }

    private void prepareDebugResponseLogReceipt() {
        List<DataElement> dataElements = this.isoMessageResponse
                .getMsg()
                .getDataElements();
        addSingleColumnString(
                printableList,
                prepareTimeString(getResponseLocalTime())
                        .concat("   ")
                        .concat("Response DEBUG"),
                TransactionReceiptStyle.DEBUG_LOG.getStyle()
        );
        addSingleColumnString(
                printableList,
                "Length: ".concat(getResponseMessageLength()),
                TransactionReceiptStyle.DEBUG_LOG.getStyle()
        );
        addSingleColumnString(
                printableList,
                "TPDU: ".concat(getResponseTPDU()),
                TransactionReceiptStyle.DEBUG_LOG.getStyle()
        );
        addSingleColumnString(
                this.printableList,
                "MTI: ".concat(getResponseMTI()),
                TransactionReceiptStyle.DEBUG_LOG.getStyle()

        );
        addDataElementsToPrintList(dataElements);
        addSingleColumnString(
                printableList,
                "  ",
                TransactionReceiptStyle.DIVIDER.getStyle()
        );
//        addSingleColumnString(
//                printableList,
//                isoMessageResponse.getDebugResponseString(),
//                TransactionReceiptStyle.DEBUG_LOG.getStyle()
//        );
    }

    private String getRequestLocalTime() {
        String data = HelperUtils.getDefaultLocaleTimeHhMmSs();
        return getValueOrEmpty(data);
    }

    private String getRequestMessageLength() {
        String length = String.valueOf(isoMessageResponse
                .getRequestPayload()
                .getRequestPacket()
                .getData()
                .length
        );
        return getValueOrEmpty(length);
    }

    private String getRequestTPDU() {
        String tpdu = IsoMessageUtils.retrieveFromRequestDataElementAsString(isoMessageResponse,
                DataElement.FUNCTION_CODE_OR_NETWORK_INTERNATIONAL_IDENTIFIER);
        return getValueOrEmpty(tpdu);
    }

    private String getRequestMTI() {
        String mti = String.valueOf(this.isoMessageResponse
                .getRequestPayload()
                .getRequestMsg()
                .getMti()
        );
        return getValueOrEmpty(mti);
    }

    private String getResponseLocalTime() {
        String time = IsoMessageUtils.retrieveFromDataElementsAsString(this.isoMessageResponse, DataElement.LOCAL_TIME);
        return getValueOrEmpty(time);
    }

    private String getResponseMessageLength() {
        String length = String.valueOf(this.isoMessageResponse.getResponseLength());
        return getValueOrEmpty(length);
    }

    private String getResponseTPDU() {
        String tpdu = IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse,
                DataElement.FUNCTION_CODE_OR_NETWORK_INTERNATIONAL_IDENTIFIER);
        return getValueOrEmpty(tpdu);
    }

    private String getResponseMTI() {
        String mtiInHex = String.valueOf(isoMessageResponse.getMsg().getMti());
        String mti = StringUtils.hextToString(mtiInHex);
        return getValueOrEmpty(mti);
    }

    private String prepareTimeString(String time) {
        if (StringUtils.isEmpty(time) || time.length() < 6)
            return "";
        return time.substring(0, 2)
                .concat(":")
                .concat(time.substring(2, 4))
                .concat(":")
                .concat(time.substring(4, 6));
    }

    private void addDataElementsToPrintList(List<DataElement> dataElements) {
        for (DataElement dataElement : dataElements) {
            addMultiColumnString(
                    this.printableList,
                    createStringList(
                            getValueOrEmpty(String.valueOf(dataElement.getIndex())),
                            " : ",
                            putSpaceAfterEveryTwoCharacters(String.valueOf(dataElement.getData()))
                    ),
                    new int[]{10, 5, 85},
                    TransactionReceiptStyle.DATA_ELEMENT.getStyle()
            );
        }
    }

}
