package global.citytech.finpos.neps.transaction;

import org.powermock.api.mockito.PowerMockito;

import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;

import static org.junit.Assert.assertNull;

public class MagCardPurchaseProcessorTest {

    MagCardPurchaseProcessor SUT;
    TransactionRepository transactionRepository;
    TransactionAuthenticator transactionAuthenticator;
    ApplicationRepository applicationRepository;
    TerminalRepository terminalRepository;


  //  @Before
    public void setUp() throws Exception {
        transactionRepository = PowerMockito.mock(TransactionRepository.class);
        transactionAuthenticator = PowerMockito.mock(TransactionAuthenticator.class);
        terminalRepository = PowerMockito.mock(TerminalRepository.class);
        SUT = new MagCardPurchaseProcessor(
                transactionRepository,
                transactionAuthenticator,
                applicationRepository,
                terminalRepository
        );
    }

  //  @Test
    public void purchase(){
        String stan ="03030000";
        PurchaseRequest purchaseRequest = MockPurchase.getRequest();
        TransactionSetTableVerifier transactionSetTableVerifier = PowerMockito.mock(TransactionSetTableVerifier.class);
        PowerMockito.when(transactionSetTableVerifier.isPinVerificationNecessary()).thenReturn(false);


        ReadCardResponse readCardResponse = SUT.purchase(stan,purchaseRequest);

        assertNull(readCardResponse);
    }

}