package global.citytech.finposframework.switches.receipt.detailreport;

import global.citytech.finposframework.switches.RequestParameter;

/**
 * Created by Unique Shakya on 1/4/2021.
 */
public interface DetailReportRequestParameter extends RequestParameter {
}
