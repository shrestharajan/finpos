package global.citytech.finpos.neps.iso8583.requestsender.cardtypesenders;

import global.citytech.finpos.neps.iso8583.PosEntryModeConfig;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.CardTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.utility.StringUtils;

public class ManualISOMessageUpdater implements CardTypeISOMessageUpdater {
    @Override
    public Iso8583Msg update(TransactionIsoRequest transactionIsoRequest, Iso8583Msg msg) {
        msg.addField(
                new DataElement(
                        DataElement.PAN_EXPIRE_DATE,
                        transactionIsoRequest.getExpiryDate()
                )
        );

        PosEntryMode posEntryMode = PosEntryMode.MANUAL;
        if (StringUtils.isEmpty(transactionIsoRequest.getPinBlock()))
            posEntryMode = PosEntryMode.MANUAL_WITHOUT_PIN;
        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(posEntryMode)
                )
        );
        return msg;
    }
}
