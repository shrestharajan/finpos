package global.citytech.finpos.merchant.data.datasource.core.receipt

import global.citytech.finpos.merchant.domain.model.receipt.duplicate.DuplicateReceiptResponseEntity
import global.citytech.finpos.processor.nibl.receipt.duplicatereceipt.DuplicateReceiptResponseModel
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 12/15/2020.
 */
interface DuplicateReceiptDataSource {
    fun print(stan:String? = ""): Observable<DuplicateReceiptResponseEntity>
}