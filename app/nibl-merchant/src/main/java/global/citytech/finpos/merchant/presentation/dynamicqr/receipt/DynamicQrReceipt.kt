package global.citytech.finpos.merchant.presentation.dynamicqr.receipt

import global.citytech.finposframework.usecases.transaction.receipt.Performance
import global.citytech.finposframework.usecases.transaction.receipt.Retailer

data class DynamicQrReceipt (
    val retailer: Retailer,
    val performance: Performance,
    val qrTransactionDetail: QrTransactionDetail,
    val qrCodeString: String,
    val thankYouMessage: String
)