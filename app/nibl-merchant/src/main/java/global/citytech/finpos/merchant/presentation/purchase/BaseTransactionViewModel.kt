package global.citytech.finpos.merchant.presentation.purchase

import android.app.Application
import android.content.Intent
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import global.citytech.common.data.Response
import global.citytech.common.extensions.hasInternetConnection
import global.citytech.common.extensions.isNullOrEmptyOrBlank
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.receipt.CustomerCopyRepositoryImpl
import global.citytech.finpos.merchant.data.repository.device.DeviceRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.*
import global.citytech.finpos.merchant.domain.model.receipt.CustomerCopyResponseEntity
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.core.receipt.CoreCustomerCopyUseCase
import global.citytech.finpos.merchant.domain.usecase.device.DeviceUseCase
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.receipt.CustomerCopyDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.device.DeviceConfigurationSourceImpl
import global.citytech.finpos.merchant.framework.datasource.device.DeviceControllerImpl
import global.citytech.finpos.merchant.presentation.alertdialogs.PosEntrySelection
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.presentation.model.merchanttransactionlog.MerchantTransactionLogResponse
import global.citytech.finpos.merchant.presentation.model.purchase.QrData
import global.citytech.finpos.merchant.presentation.payment.PaymentSdkSingleton
import global.citytech.finpos.merchant.presentation.utils.Constants
import global.citytech.finpos.merchant.service.TransactionPushService
import global.citytech.finpos.merchant.utils.*
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.exceptions.PosException
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.switches.PosEntryMode
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog
import global.citytech.finposframework.usecases.transaction.receipt.transaction.TransactionEmvInfo
import global.citytech.finposframework.usecases.transaction.receipt.transaction.TransactionReadCardResponse
import global.citytech.finposframework.usecases.transaction.receipt.transaction.VatInfo
import global.citytech.finposframework.utility.DateUtils
import global.citytech.finposframework.utility.HelperUtils
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Unique Shakya on 9/4/2020.
 */
abstract class BaseTransactionViewModel(val context: Application) :
    BaseAndroidViewModel(context) {

    private val logger = Logger.getLogger(BaseTransactionViewModel::class.java.name)
    val configurationItem by lazy { MutableLiveData<ConfigurationItem>() }
    val transactionConfirmationData by lazy { MutableLiveData<TransactionConfirmation>() }
    val customerCopyPrintMessage by lazy { MutableLiveData<String>() }
    val posEntrySelection by lazy { MutableLiveData<PosEntrySelection>() }
    val transactionComplete by lazy { MutableLiveData<Boolean>() }
    val doLoadManualPage by lazy { MutableLiveData<Boolean>() }
    val doLoadNfcMobilePage by lazy { MutableLiveData<Boolean>() }
    val doReturnToIdlePage by lazy { MutableLiveData<Boolean>() }
    val doReturnToPaymentItemSelectionPage by lazy { MutableLiveData<Boolean>() }
    val cleanupCompletion by lazy { MutableLiveData<Boolean>() }
    val cardPresent by lazy { MutableLiveData<Boolean>() }
    val errorMessage by lazy { MutableLiveData<String>() }
    val mapOfTransactionLog by lazy { MutableLiveData<Map<String, Any>>() }
    val paymentError by lazy { MutableLiveData<PosError>() }
    var transactionType: TransactionType? = null
    var task: Task = Task.RETURN_TO_DASHBOARD
    val doQrTransaction by lazy { MutableLiveData<Boolean>() }
    private val qrDigitalReceiptImage by lazy { MutableLiveData<String>() }
    private val transactionStatus by lazy { MutableLiveData<String>() }

    var checkoutAdditionalData: String? = ""
    var shouldDispose: Boolean = false

    private var customerCopyUseCase: CoreCustomerCopyUseCase =
        CoreCustomerCopyUseCase(CustomerCopyRepositoryImpl(CustomerCopyDataSourceImpl()))

    private var localDataUseCase: LocalDataUseCase =
        LocalDataUseCase(LocalRepositoryImpl(LocalDatabaseSourceImpl(), PreferenceManager))

    private var localRepository = LocalRepositoryImpl(
        LocalDatabaseSourceImpl(),
        PreferenceManager
    )

    var deviceConfigurationUseCase = DeviceUseCase(
        DeviceRepositoryImpl(DeviceConfigurationSourceImpl(context))
    )

    private var deviceController = DeviceControllerImpl(context)

    fun retrieveAllowedCardTypes(transactionType: TransactionType) {
        val mapOfCardTypes = DeviceConfiguration.get().purchaseCardTypeMap
        var cardTypes = mapOfCardTypes?.get(transactionType)
        if (cardTypes == null || cardTypes.isEmpty())
            cardTypes = defaultCardTypes()
        val stringBuilder = StringBuilder()
        if (cardTypes.contains(CardType.MAG)) {
            stringBuilder.append("SWIPE")
        }
        if (cardTypes.contains(CardType.ICC)) {
            if (stringBuilder.toString().isNotEmpty())
                stringBuilder.append(",  ")
            stringBuilder.append("INSERT")
        }
        if (cardTypes.contains(CardType.PICC)) {
            if (stringBuilder.toString().isNotEmpty())
                stringBuilder.append(",  ")
            stringBuilder.append("WAVE ")
        }
        stringBuilder.append("CARD")

        val posEntrySelection = PosEntrySelection(
            stringBuilder.toString(),
            cardTypes.contains(CardType.PICC),
            cardTypes.contains(CardType.MANUAL),
            global.citytech.finpos.merchant.presentation.alertdialogs.PosEntryMode.ACCEPT_ALL
        )
        this.posEntrySelection.postValue(posEntrySelection)
    }

    private fun defaultCardTypes(): List<CardType> {
        val cardTypes = mutableListOf<CardType>()
        cardTypes.add(CardType.MAG)
        cardTypes.add(CardType.ICC)
        cardTypes.add(CardType.PICC)
        cardTypes.add(CardType.MANUAL)
        return cardTypes
    }

    fun printCustomerCopy() {
        isLoading.value = true
        compositeDisposable.add(
            customerCopyUseCase.print()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onCustomerCopyResponseEntityReceived(it)
                }, {
                    customerCopyPrintMessage.postValue("Customer Print Failure")
                    isLoading.value = false
                })
        )
    }

    private fun onCustomerCopyResponseEntityReceived(it: CustomerCopyResponseEntity) {
        isLoading.value = false
        if (it.result == Result.SUCCESS || it.result == Result.CANCEL) {
            checkForCardPresent()
            return
        }
        customerCopyPrintMessage.postValue(it.message)
    }

    fun getConfigurationItem() {
        disableNetworkPing()
        compositeDisposable.add(localDataUseCase.getWholeConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.mapToPresentation() }
            .subscribe({
                configurationItem.value = it
            },
                {
                    paymentError.value = PosError.DEVICE_ERROR_NOT_READY
                    errorMessage.value = it.message
                })
        )
    }

    fun doCleanUp(task: Task) = Completable
        .fromAction {
            this.task = task
            deviceConfigurationUseCase.closeUpIO()
        }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            { this.onCleanUpComplete(task) },
            { this.onCleanUpError(it, task) }
        )

    fun checkForCardPresent() {
        isLoading.value = true
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Boolean> {
                isCardPresentTask(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    logger.log("corelogger Check for Card Complete")
                    isLoading.value = false
                    cardPresent.value = it
                }, {
                    logger.log("corelogger Check for Card Error")
                    isLoading.value = false
                    it.printStackTrace()
                    checkForCardPresent()
                })
        )
    }

    fun getCardConfirmationRequiredStatus() =
        localDataUseCase.getCardConfirmationRequiredPref(false)

    private fun isCardPresentTask(it: ObservableEmitter<Boolean>) {
        it.onNext(deviceController.isIccCardPresent)
        it.onComplete()
    }

    private fun onCleanUpComplete(task: Task) {
        logger.log("corelogger Clean Up Complete")
        cleanupCompletion.value = true
        this.task = task
    }

    private fun onCleanUpError(throwable: Throwable, task: Task) {
        logger.log("corelogger Clean Up Error ::: ".plus(throwable.message))
        cleanupCompletion.value = false
        this.doCleanUp(task)
    }

    private fun retrieveImageId(approved: Boolean): Int {
        return if (approved)
            R.drawable.approved
        else
            R.drawable.declined
    }

    private fun retrieveTitle(approved: Boolean): String {
        return if (approved)
            getApplication<NiblMerchant>().getString(R.string.title_payment_successful)
        else
            getApplication<NiblMerchant>().getString(R.string.title_payment_declined)
    }

    private fun retrieveTransactionConfirmationMessage(
        shouldPrint: Boolean,
        message: String?
    ): String {
        return if (shouldPrint)
            message.plus("\n\nPRINT CUSTOMER COPY?")
        else
            message!!
    }

    private fun retrievePositiveLabel(shouldPrint: Boolean): String {
        return if (shouldPrint)
            "Print Customer Copy"
        else
            ""
    }

    private fun retrieveNegativeLabel(shouldPrint: Boolean): String {
        return if (shouldPrint)
            "Finish"
        else
            "OK"
    }

    fun retrieveTransactionConfirmation(
        isApproved: Boolean,
        message: String?,
        shouldPrint: Boolean
    ): TransactionConfirmation? {
        return TransactionConfirmation.Builder()
            .imageId(retrieveImageId(isApproved))
            .title(transactionType!!.displayName + "\n" + retrieveTitle(isApproved))
            .message(message!!)
            .positiveLabel(this.retrievePositiveLabel(shouldPrint))
            .negativeLabel(this.retrieveNegativeLabel(shouldPrint))
            .qrImageString(this.qrDigitalReceiptImage.value!!)
            .transactionStatus(this.transactionStatus.value!!)
            .build()
    }

    protected fun updateMerchantTransactionLog(
        stan: String?,
        message: String?,
        approved: Boolean
    ) {
        stan?.let {
            compositeDisposable.add(
                localRepository.getTransactionLogByStan(stan)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        saveMerchantTransactionLog(it, message!!, approved)
                    }, {
                        it.printStackTrace()
                        it?.let {
                            logger.log(it.message)
                        }
                    })
            )
        }

    }


    private fun saveMerchantTransactionLog(
        transactionLog: TransactionLog,
        message: String,
        approved: Boolean
    ) {
        logger.log("::: SAVING MERCHANT LOG :::")
        logger.log("::: Transaction Status  ::: ${transactionLog.transactionStatus}")

        updatePaymentTransaction(transactionLog, message, approved)
        val readCardResponse =
            Jsons.fromJsonToObj(transactionLog.readCardResponse, ReadCardResponse::class.java)
        var cardScheme = ""
        var cardSchemeLabel = ""
        if (readCardResponse.cardDetails!!.cardScheme != null) {
            cardScheme = readCardResponse.cardDetails!!.cardScheme!!.cardSchemeId

            var transactionReadCardResponse: TransactionReadCardResponse = Jsons.fromJsonToObj(
                transactionLog.readCardResponse,
                TransactionReadCardResponse::class.java
            )

            cardSchemeLabel = transactionReadCardResponse.cardDetails.cardSchemeLabel
        }

        val binNumber = readCardResponse.cardDetails!!.primaryAccountNumber!!.substring(0, 6)
        var transactionType =
            Jsons.fromJsonToObj(transactionLog.transactionType, TransactionType::class.java)
        transactionLog.isEmiTransaction?.let {
            if (it)
                transactionType = TransactionType.EMI_PURCHASE
        }
        val amount =
            if (transactionType == TransactionType.VOID) transactionLog.origTransactionAmount
            else transactionLog.transactionAmount
        val posEntryMode =
            Jsons.fromJsonToObj(transactionLog.posEntryMode, PosEntryMode::class.java)
        val transactionDate = prepareTransactionDateBasedOnFormats(transactionLog)
        logger.log("Formatted Date ::: ".plus(transactionDate))
        val transactionTime = prepareTransactionTimeBasedOnFormats(transactionLog)
        logger.log("Formatted Time ::: ".plus(transactionTime))

        val transactionEmvInfo: TransactionEmvInfo =
            prepareTransactionEmvInfo(
                transactionLog,
                posEntryMode,
                cardSchemeLabel,
                readCardResponse
            )

        val vatInfo: VatInfo = prepareVatInfo(transactionLog)

        val merchantTransactionLog = MerchantTransactionLog(
            stan = transactionLog.stan,
            terminalId = transactionLog.terminalId,
            merchantId = transactionLog.merchantId,
            invoiceNumber = transactionLog.invoiceNumber,
            rrn = transactionLog.rrn,
            txnDate = transactionDate,
            txnTime = transactionTime,
            txnType = transactionType.name,
            amount = amount,
            scheme = cardScheme,
            txnStatus = transactionLog.transactionStatus,
            approvalCode = transactionLog.authCode,
            posEntryMode = posEntryMode.entryMode,
            originalTxnReference = transactionLog.originalTransactionReferenceNumber,
            remarks = prepareRemarks(transactionLog),
            bin = binNumber,
            emiInfo = transactionLog.emiInfo,
            currencyCode = transactionLog.currencyCode,
            vatAmount = vatInfo.vatAmount,
            vatRefundAmount = vatInfo.vatRefundAmount,
            cvmMessage = prepareCvmMessage(transactionLog),
            transactionEmvInfo = Jsons.toJsonObj(transactionEmvInfo)
        )
        compositeDisposable.add(
            localRepository.saveMerchantTransactionLog(merchantTransactionLog)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.result == DataBaseResult.SUCCESS) {
                        getMerchantTransactionLogsForPush()
                    }
                }, {
                    logger.log(it.message)
                })
        )
    }

    private fun prepareTransactionDateBasedOnFormats(transactionLog: TransactionLog): String {
        var transactionDate: String? = transactionLog.transactionDate ?: ""
        if (transactionDate.isNullOrEmptyOrBlank()) {
            return ""
        }
        if (transactionDate!!.isTransactionDateOrTimeOfGivenFormat(DateUtils.DATE_FORMAT_ONE)) {
            transactionDate = transactionDate.parseDateInFormatOneForMerchantLog()
        } else if (transactionDate.isTransactionDateOrTimeOfGivenFormat(DateUtils.DATE_FORMAT_TWO)) {
            transactionDate = transactionDate.parseDateInFormatTwoForMerchantLog()
        }
        return transactionDate
    }

    private fun prepareTransactionTimeBasedOnFormats(transactionLog: TransactionLog): String {
        var transactionTime: String? =
            if (!transactionLog.receiptLog.isNullOrEmptyOrBlank() && Jsons.isValidJsonString(
                    transactionLog.receiptLog
                )
            ) {
                val receiptLog =
                    Jsons.fromJsonToObj(transactionLog.receiptLog, ReceiptLog::class.java)
                HelperUtils.formatDateTime(
                    receiptLog.performance.startDateTime,
                    "yyMMddHHmmssSSS", DateUtils.TIME_FORMAT_FINPOS_TXN_LOG
                )
            } else {
                transactionLog.transactionTime ?: ""
            }
        if (transactionTime.isNullOrEmptyOrBlank()) {
            return ""
        }
        transactionTime!!.isTransactionDateOrTimeOfGivenFormat(DateUtils.TIME_FORMAT_FINPOS_TXN_LOG)
            .apply {
                if (!this) {
                    transactionTime = ""
                }
            }
        return transactionTime!!
    }

    private fun updatePaymentTransaction(
        transactionLog: TransactionLog,
        message: String,
        approved: Boolean
    ) {
        mapOfTransactionLog.value = prepareMapOfTransactionLog(transactionLog, message, approved)
    }

    private fun prepareMapOfTransactionLog(
        transactionLog: TransactionLog,
        message: String,
        approved: Boolean
    ): Map<String, Any>? {
        val map = mutableMapOf<String, Any>()
        val readCardResponse = Jsons
            .fromJsonToObj(transactionLog.readCardResponse, ReadCardResponse::class.java)
        map[global.citytech.common.Constants.EXTRA_AUTH_CODE] = transactionLog.authCode!!
        map[global.citytech.common.Constants.EXTRA_CARD_NUMBER] = readCardResponse
            .cardDetails!!.primaryAccountNumber!!
        map[global.citytech.common.Constants.EXTRA_MESSAGE] = message
        map[global.citytech.common.Constants.EXTRA_APPROVED] = approved
        map[global.citytech.common.Constants.EXTRA_RRN] = transactionLog.rrn!!
        map[global.citytech.common.Constants.EXTRA_STAN] = transactionLog.stan
        map[global.citytech.common.Constants.EXTRA_TRANSACTION_TIME] =
            transactionLog.transactionTime!!
        map[global.citytech.common.Constants.EXTRA_PAYMENT_MODE] = "CARD"
        map[global.citytech.common.Constants.EXTRA_PAYMENT_NETWORK] =
            readCardResponse.cardDetails!!.cardScheme!!.cardScheme
        return map
    }

    fun updatePaymentTransactionByQr(qrPayment: QrPayment, message: String, isApproved: Boolean) {
        val map = mutableMapOf<String, Any>()
        map[global.citytech.common.Constants.EXTRA_AUTH_CODE] = qrPayment.approvalCode!!
        map[global.citytech.common.Constants.EXTRA_CARD_NUMBER] = qrPayment.initiatorId!!
        map[global.citytech.common.Constants.EXTRA_MESSAGE] = message
        map[global.citytech.common.Constants.EXTRA_APPROVED] = isApproved
        map[global.citytech.common.Constants.EXTRA_RRN] = qrPayment.referenceNumber
        map[global.citytech.common.Constants.EXTRA_STAN] = qrPayment.invoiceNumber
        map[global.citytech.common.Constants.EXTRA_TRANSACTION_TIME] = qrPayment.transactionTime
        map[global.citytech.common.Constants.EXTRA_PAYMENT_MODE] = "QR"
        map[global.citytech.common.Constants.EXTRA_PAYMENT_NETWORK] = qrPayment.paymentInitiator!!
        mapOfTransactionLog.value = map
    }

    fun updatePaymentTransactionByMobile(
        mobileNfcPayment: MobileNfcPayment,
        message: String,
        isApproved: Boolean
    ) {
        val map = mutableMapOf<String, Any>()
        map[global.citytech.common.Constants.EXTRA_AUTH_CODE] = mobileNfcPayment.approvalCode!!
        map[global.citytech.common.Constants.EXTRA_CARD_NUMBER] = mobileNfcPayment.initiatorId!!
        map[global.citytech.common.Constants.EXTRA_MESSAGE] = message
        map[global.citytech.common.Constants.EXTRA_APPROVED] = isApproved
        map[global.citytech.common.Constants.EXTRA_RRN] = mobileNfcPayment.referenceNumber
        map[global.citytech.common.Constants.EXTRA_STAN] = mobileNfcPayment.invoiceNumber
        map[global.citytech.common.Constants.EXTRA_TRANSACTION_TIME] =
            mobileNfcPayment.transactionTime
        map[global.citytech.common.Constants.EXTRA_PAYMENT_MODE] = "NFC"
        map[global.citytech.common.Constants.EXTRA_PAYMENT_NETWORK] =
            mobileNfcPayment.paymentInitiator!!
        mapOfTransactionLog.value = map
    }

    private fun prepareRemarks(transactionLog: TransactionLog): String? {
        val posEntryMode =
            Jsons.fromJsonToObj(transactionLog.posEntryMode, PosEntryMode::class.java)
        val mapOfRemarks = HashMap<String, String?>()
        mapOfRemarks[Constants.REMARKS_RESPONSE_CODE] = transactionLog.responseCode
        mapOfRemarks[Constants.REMARKS_BATCH_NO] = transactionLog.reconcileBatchNo
        mapOfRemarks[Constants.REMARKS_POS_ENTRY_MODE] = posEntryMode.entryMode
        if (PaymentSdkSingleton.getInstance().isFromPaymentSdk)
            mapOfRemarks[Constants.REMARKS_FROM_BILLING_SYSTEM] = checkoutAdditionalData
        val remarksString = Jsons.toJsonObj(mapOfRemarks)
        logger.log("::: REMARKS IN MERCHANT LOG ::: $remarksString")

        return remarksString
    }

    private fun prepareVatInfo(transactionLog: TransactionLog): VatInfo {
        return if (transactionLog.vatInfo.isNullOrEmpty()) {
            VatInfo()
        } else {
            Jsons.fromJsonToObj(transactionLog.vatInfo, VatInfo::class.java)
        }
    }

    private fun prepareTransactionEmvInfo(
        transactionLog: TransactionLog,
        posEntryMode: PosEntryMode,
        cardSchemeLabel: String,
        cardResponse: ReadCardResponse
    ): TransactionEmvInfo {
        return if (transactionLog.receiptLog.isNullOrEmpty() || transactionLog.receiptLog.equals("null")) {
            TransactionEmvInfo()
        } else {
            val receiptLog = Jsons.fromJsonToObj(transactionLog.receiptLog, ReceiptLog::class.java)
            Log.d("RECEIPT LOG :: ", transactionLog.receiptLog)
            TransactionEmvInfo(
                posEntryMode.getCardType(),
                receiptLog.emvTags.responseCode,
                receiptLog.emvTags.aid,
                receiptLog.emvTags.tvr,
                receiptLog.emvTags.tsi,
                receiptLog.emvTags.cvm,
                receiptLog.emvTags.cid,
                receiptLog.emvTags.ac,
                cardSchemeLabel,
                if (checkCustomerDetailFlag())
                    if (cardResponse.cardDetails?.cardHolderName.isNullOrEmptyOrBlank())
                        ""
                    else
                        cardResponse.cardDetails?.cardHolderName!!.trim()
                else
                    "",
                if (checkCardDetailFlag())
                    if (cardResponse.cardDetails?.primaryAccountNumber.isNullOrEmptyOrBlank())
                        ""
                    else
                        cardResponse.cardDetails?.primaryAccountNumber!!
                else "",
                if (checkCardDetailFlag()) cardResponse.cardDetails?.expiryDate!! else "",
                receiptLog.emvTags.kernelId,
                (receiptLog.emvTags.iin) ?: ""
            )
        }
    }

    private fun checkCustomerDetailFlag(): Boolean {
        localDataUseCase.getTerminalSetting().receiptConfiguration?.enableCustomerDetails?.let {
            return it
        }
        return false
    }

    private fun checkCardDetailFlag(): Boolean {
        localDataUseCase.getTerminalSetting().receiptConfiguration?.enableCardDetails?.let {
            return it
        }
        return false
    }

    private fun prepareCvmMessage(transactionLog: TransactionLog): String {
        return if (transactionLog.receiptLog.isNullOrEmpty() || transactionLog.receiptLog.equals("null")) {
            ""
        } else {
            val receiptLog = Jsons.fromJsonToObj(transactionLog.receiptLog, ReceiptLog::class.java)
            receiptLog.transactionInfo.transactionMessage.toString()
        }
    }

    fun prepareBase64UrlToDisplayQr(
        isApproved: Boolean,
        message: String,
        shouldPrintCustomerCopy: Boolean,
        stan: String
    ) {
        compositeDisposable.add(
            localRepository.getTransactionLogByStan(stan)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    val qrData = Jsons.toJsonObj(
                        QrData(
                            it.merchantId,
                            it.terminalId,
                            it.rrn!!,
                            it.stan,
                            prepareTransactionDateBasedOnFormats(it),
                            prepareTransactionTimeBasedOnFormats(it),
                            it.transactionType!!
                        )
                    )
                    qrDigitalReceiptImage.value = AppUtility.prepareQrBase64String(qrData)
                    this.transactionStatus.value = it.transactionStatus
                    //"${it.merchantId}-${it.terminalId}-${it.rrn}-${it.stan}-${it.transactionDate}-${it.transactionTime}"

                    val transactionConfirmation = this.retrieveTransactionConfirmation(
                        isApproved,
                        message,
                        shouldPrintCustomerCopy
                    )
                    transactionConfirmationData.value = transactionConfirmation
                }, {
                    logger.log(it.message)
                    buildConfirmationDialogForError(isApproved, message)
                })
        )
    }

    private fun buildConfirmationDialogForError(isApproved: Boolean, message: String) {

        val transactionConfirmation = TransactionConfirmation.Builder()
            .imageId(retrieveImageId(isApproved))
            .title(transactionType!!.displayName + "\n" + retrieveTitle(isApproved))
            .message(message)
            .positiveLabel("")
            .negativeLabel("OK")
            .qrImageString("")
            .transactionStatus("")
            .build()
        transactionConfirmationData.value = transactionConfirmation
    }

    private fun getMerchantTransactionLogsForPush() {
        compositeDisposable.add(
            localRepository.getMerchantTransactionLogsOfGivenCount(
                AppConstant.MAX_TRANSACTIONS_COUNT_FOR_BATCH_PUSH
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    updateMerchantTransactionLogsToFinpos(it)
                }, {
                    logger.log(it.message)
                })
        )
    }

    private fun updateMerchantTransactionLogsToFinpos(merchantTransactionLogs: List<MerchantTransactionLog>?) {
        if (context.hasInternetConnection()) {
            TransactionPushService.isLogPushInProgress = true
            compositeDisposable.add(
                Observable.create(ObservableOnSubscribe<MerchantTransactionLogResponse> {
                    onSubscribe(merchantTransactionLogs, it)
                })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onNext, this::onError)
            )
        }
    }

    private fun onNext(response: MerchantTransactionLogResponse) {
        TransactionPushService.isLogPushInProgress = false
        if (response.data != null) {
            compositeDisposable.add(
                localRepository.deleteMerchantTransactionLogsOfGivenCountFromInitial(
                    AppConstant.MAX_TRANSACTIONS_COUNT_FOR_BATCH_PUSH
                )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({

                        logger.log("${AppConstant.MAX_TRANSACTIONS_COUNT_FOR_BATCH_PUSH} Merchant Transaction Logs Cleared From Database...")
                    }, {
                        logger.log(it.message)
                    })
            )
        } else {
            logger.log("Some Error occurred during pushing merchant transaction log.")
        }

    }

    private fun onError(throwable: Throwable) {
        TransactionPushService.isLogPushInProgress = false
        logger.log(throwable.message)
    }


    private fun onSubscribe(
        merchantTransactionLogs: List<MerchantTransactionLog>?,
        emitter: ObservableEmitter<MerchantTransactionLogResponse>
    ) {
        val jsonRequest = Jsons.toJsonObj(merchantTransactionLogs)
        logger.debug("Merchant Transaction Log Request::: $jsonRequest")
        val jsonResponse =
            NiblMerchant.INSTANCE.iPlatformManager.post(
                ApiConstant.UPDATE_MERCHANT_TRANSACTION_LOG_URL,
                jsonRequest,
                true
            )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            prepareResponse(emitter, response)
        } else {
            if (response.code == "401")
                emitter.onError(PosException(PosError.DEVICE_ERROR_OPERATOR_LOGIN_REQUIRED))
            else
                emitter.onError(Exception("Error Code :: ${response.code}. ${response.message}"))
        }
        emitter.onComplete()
    }

    private fun prepareResponse(
        emitter: ObservableEmitter<MerchantTransactionLogResponse>,
        data: Any
    ) {
        val responseJson = Jsons.toJsonObj(data)
        val response = Jsons.fromJsonToObj(responseJson, MerchantTransactionLogResponse::class.java)
        emitter.onNext(response)
    }

    fun updatePaymentError(it: Throwable) {
        paymentError.value = if (it is PosException) {
            it.posError
        } else {
            PosError.DEVICE_ERROR_PROCESSING_ERROR
        }

    }

    fun enableNetworkPing() {
        NiblMerchant.INSTANCE.disableNetworkPing = false
    }

    private fun disableNetworkPing() {
        NiblMerchant.INSTANCE.disableNetworkPing = true
    }

    protected fun completePreviousTask() {
        if (task == Task.MANUAL_TRANSACTION)
            doLoadManualPage.value = true
        else if (task == Task.QR_TRANSACTION)
            doQrTransaction.value = true
        else if (task == Task.MOBILE_TRANSACTION)
            doLoadNfcMobilePage.value = true
        else if (task == Task.CANCEL_CARD_TRANSACTION)
            doReturnToPaymentItemSelectionPage.value = true
        else
            doReturnToIdlePage.value = true
    }

    enum class Task {
        MANUAL_TRANSACTION,
        RETURN_TO_DASHBOARD,
        QR_TRANSACTION,
        CANCEL_CARD_TRANSACTION,
        MOBILE_TRANSACTION
    }

    fun pushMerchantTransactionLog(intent: Intent) {
        val stan = intent.getStringExtra(global.citytech.common.Constants.KEY_STAN)
        val message = intent.getStringExtra(global.citytech.common.Constants.KEY_MESSAGE)
        val isApproved =
            intent.getBooleanExtra(global.citytech.common.Constants.KEY_IS_APPROVED, false)

        updateMerchantTransactionLog(stan, message, isApproved)
    }

    fun getNumpadType(defaultValue: String): String = localDataUseCase.getNumpadLayoutType(defaultValue)
}