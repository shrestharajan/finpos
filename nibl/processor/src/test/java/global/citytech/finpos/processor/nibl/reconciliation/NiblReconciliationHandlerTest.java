package global.citytech.finpos.processor.nibl.reconciliation;

import org.junit.Before;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationHandler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Unique Shakya on 10/8/2020.
 */
public class NiblReconciliationHandlerTest {

    ReconciliationRepository reconciliationRepository;
    String batchNumber = "001";

    @Before
    public void setup(){
        this.reconciliationRepository = PowerMockito.mock(ReconciliationRepository.class);
    }

    @Test
    public void prepareReconciliationTotalsTest(){
        long salesCount = 10;
        long salesAmount = 1000;
        long salesVoidCount = 2;
        long salesVoidAmount = 200;
        long salesRefundCount = 2;
        long salesRefundAmount = 200;
        long salesRefundVoidCount = 1;
        long salesRefundVoidAmount = 50;
        long authCount = 1;
        long authAmount = 200;

        PowerMockito.when(this.reconciliationRepository.getTotalAmountByTransactionType(this.batchNumber,
                TransactionType.PRE_AUTH)).thenReturn(authAmount);
        PowerMockito.when(this.reconciliationRepository.getCountByTransactionType(this.batchNumber,
                TransactionType.PRE_AUTH)).thenReturn(authCount);

        PowerMockito.when(this.reconciliationRepository.getTotalAmountByTransactionType(this.batchNumber,
                TransactionType.REFUND)).thenReturn(salesRefundAmount);
        PowerMockito.when(this.reconciliationRepository.getTotalAmountByOriginalTransactionType(this.batchNumber,
                TransactionType.VOID, TransactionType.REFUND)).thenReturn(salesRefundVoidAmount);
        PowerMockito.when(this.reconciliationRepository.getTotalAmountByTransactionType(this.batchNumber,
                TransactionType.PURCHASE)).thenReturn(salesAmount);
        PowerMockito.when(this.reconciliationRepository.getTotalAmountByOriginalTransactionType(this.batchNumber,
                TransactionType.VOID, TransactionType.PURCHASE)).thenReturn(salesVoidAmount);

        PowerMockito.when(this.reconciliationRepository.getCountByTransactionType(this.batchNumber,
                TransactionType.REFUND)).thenReturn(salesRefundCount);
        PowerMockito.when(this.reconciliationRepository.getCountByOriginalTransactionType(this.batchNumber,
                TransactionType.VOID, TransactionType.REFUND)).thenReturn(salesRefundVoidCount);
        PowerMockito.when(this.reconciliationRepository.getCountByTransactionType(this.batchNumber,
                TransactionType.PURCHASE)).thenReturn(salesCount);
        PowerMockito.when(this.reconciliationRepository.getCountByOriginalTransactionType(this.batchNumber,
                TransactionType.VOID, TransactionType.PURCHASE)).thenReturn(salesVoidCount);

        ReconciliationHandler reconciliationHandler = new NiblReconciliationHandler(this.batchNumber,
                this.reconciliationRepository);
        NiblReconciliationTotals reconciliationTotals = (NiblReconciliationTotals) reconciliationHandler.prepareReconciliationTotals();

        assertNotNull(reconciliationTotals);
        assertEquals(8, reconciliationTotals.getSalesCount());
        assertEquals(800, reconciliationTotals.getSalesAmount());
        assertEquals(1, reconciliationTotals.getSalesRefundCount());
        assertEquals(150, reconciliationTotals.getSalesRefundAmount());
        assertEquals(0, reconciliationTotals.getDebitCount());
        assertEquals(0, reconciliationTotals.getDebitAmount());
        assertEquals(0, reconciliationTotals.getDebitRefundCount());
        assertEquals(0, reconciliationTotals.getDebitRefundAmount());
        assertEquals(1, reconciliationTotals.getAuthCount());
        assertEquals(200, reconciliationTotals.getAuthAmount());
        assertEquals(0, reconciliationTotals.getAuthRefundCount());
        assertEquals(0, reconciliationTotals.getAuthRefundAmount());
    }
}
