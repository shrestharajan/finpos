package global.citytech.finpos.merchant.framework.datasource.core.printparameter

/**
 * Created by Rishav Chudal on 11/6/20.
 */
public class TmsParametersLabels {
    companion object {
        /**
         * Tms Parameters Labels
         */
        const val TERMINAL_ID: String = "TID"
        const val SWITCH_PARAMETERS: String = "SWITCH PARAMETERS"
        const val AID_DATA: String = "AID DATA"
        const val EMV_KEYS: String = "EMV KEYS"
        const val CARD_SCHEME: String = "CARD SCHEME"
        const val SEMICOLON: String = " : "
        const val APP_VERSION: String = "App Version"
        const val MERCHANT_ID: String = "MID"

        /**
         * Switch Parameters Labels
         */
        const val IP = "IP ADDRESS"
        const val PORT = "PORT"
        const val NII = "NII"
        const val ORDER = "PRIORITY ORDER"
        const val TIMEOUT = "CONNECTION TIMEOUT"
        const val RETRY_LIMIT = "RETRY LIMIT"

        /**
         * CardScheme Parameters Labels
         */
        const val CARD_SCHEME_ID = "CardScheme Id"
    }
}