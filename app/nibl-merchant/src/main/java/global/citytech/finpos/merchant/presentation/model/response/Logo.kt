package global.citytech.finpos.nibl.merchant.presentation.model.response

import global.citytech.finpos.merchant.presentation.model.banner.Banner

data class Logo(
    var appWallpaper: String? = null,
    var displayLogo: String? = null,
    var printLogo: String? = null,
    var banners: List<Banner>? = null
) {
    companion object {
        const val TABLE_NAME = "logo"
        const val COLUMN_ID = "id"
        const val COLUMN_APP_WALLPAPER = "app_wallpaper"
        const val COLUMN_DISPLAY_LOGO = "display_logo"
        const val COLUMN_PRINT_LOGO = "print_logo"
    }
}