package global.citytech.finposframework.switches.receipt.duplicatereconciliation;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 12/16/2020.
 */
public interface DuplicateReconciliationReceiptRequester<T extends DuplicateReconciliationReceiptRequestParameter,
        E extends DuplicateReconciliationReceiptResponseParameter> extends Request<T, E> {
}
