package global.citytech.finposframework.hardware.io.cards.read

import global.citytech.finposframework.hardware.io.cards.IccData

import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import global.citytech.finposframework.hardware.io.cards.CardDetails
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat
import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

/**
 * Created by Rishav Chudal on 6/8/20.
 */
class ReadCardRequest(
    val cardTypes: List<CardType>,
    val transactionType: TransactionType,
    val transactionType9C: String,
    val emvParametersRequest: EmvParametersRequest

) {
    var packageName: String?= null
    var amount: BigDecimal? = BigDecimal.ZERO
    var cashBackAmount: BigDecimal? = BigDecimal.ZERO
    var stan: String? = null
    var transactionDate: String? = null
    var transactionTime: String? = null
    var isPinpadRequired: Boolean? = true
    var primaryAccountNumber: String? = null
    var cardDetailsBeforeEmvNext: CardDetails? = null
    var forceOnlineForICC: Boolean = false
    var forceOnlineForPICC: Boolean = false
    var fallbackFromIccToMag: Boolean = false
    var multipleAidSelectionListener: MultipleAidSelectionListener? = null
    var cardReadTimeOut: Int = 0
    var minmPinLength: Int = 0
    var maxmPinLength: Int = 0
    var pinpadTimeout: Int = 0
    var pinBlockFormat: PinBlockFormat = PinBlockFormat.ISO9564_FORMAT_0
    var iccDataList: List<IccData> = ArrayList()
    var currencyName: String? = null
    var pinPadFixedLayout: Boolean = true

    interface MultipleAidSelectionListener {
        fun onMultipleAid(aids: Array<String>): Int
    }

    override fun toString(): String {
        return "ReadCardRequest(slotsToOpen=$cardTypes, transactionType='$transactionType', emvParametersRequest=$emvParametersRequest, packageName='$packageName', amount=$amount, additionalAmount=$cashBackAmount, stan=$stan, transactionDate=$transactionDate, transactionTime=$transactionTime, isPinpadRequired=$isPinpadRequired, primaryAccountNumber=$primaryAccountNumber, cardDetailsBeforeEmvNext=$cardDetailsBeforeEmvNext, forceOnlineForContact=$forceOnlineForICC, forceOnlineForContactless=$forceOnlineForPICC, fallbackFromIccToMag=$fallbackFromIccToMag, multipleAidSelectionListener=$multipleAidSelectionListener, cardReadTimeOut=$cardReadTimeOut, minmPinLength=$minmPinLength, maxmPinLength=$maxmPinLength, pinpadTimeout=$pinpadTimeout, pinBlockFormat=$pinBlockFormat)"
    }
}