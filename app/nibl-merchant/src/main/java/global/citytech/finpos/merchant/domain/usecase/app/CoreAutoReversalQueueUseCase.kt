package global.citytech.finpos.merchant.domain.usecase.app

import global.citytech.finpos.merchant.domain.repository.app.AutoReversalQueueRepository
import global.citytech.finposframework.usecases.transaction.data.AutoReversal

/**
 * Created by Unique Shakya on 6/22/2021.
 */
class CoreAutoReversalQueueUseCase(private val autoReversalQueueRepository: AutoReversalQueueRepository) {
    fun isQueueEmpty(): Boolean {
        return autoReversalQueueRepository.isQueueEmpty()
    }

    fun isActive(): Boolean {
        return autoReversalQueueRepository.isActive()
    }

    fun getSize(): Long {
        return autoReversalQueueRepository.getSize()
    }

    fun getAllQueueRequests(): MutableList<AutoReversal> {
        return autoReversalQueueRepository.getAllQueueRequests()
    }

    fun getRecentRequest(): AutoReversal? {
        return autoReversalQueueRepository.getRecentRequest()
    }

    fun removeRequest(autoReversal: AutoReversal) {
        autoReversalQueueRepository.removeRequest(autoReversal)
    }

    fun addRequest(autoReversal: AutoReversal) {
        autoReversalQueueRepository.addRequest(autoReversal)
    }

    fun incrementRetryCount(autoReversal: AutoReversal) {
        autoReversalQueueRepository.incrementRetryCount(autoReversal)
    }

    fun changeStatus(autoReversal: AutoReversal, status: AutoReversal.Status) {
        autoReversalQueueRepository.changeStatus(autoReversal, status)
    }

    fun clear() {
        autoReversalQueueRepository.clear()
    }
}