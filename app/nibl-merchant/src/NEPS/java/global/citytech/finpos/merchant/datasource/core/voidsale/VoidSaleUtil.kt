package global.citytech.finpos.merchant.datasource.core.voidsale

import global.citytech.finpos.merchant.presentation.model.TransactionItem
import global.citytech.finpos.merchant.presentation.model.voidsale.VoidSaleRequestItem
import global.citytech.finpos.merchant.presentation.voidsale.VoidViewModel
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest
import global.citytech.finposframework.utility.HelperUtils
import java.math.BigDecimal

/**
 * Created by Saurav Ghimire on 4/19/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


object VoidSaleUtil {

    fun prepareVoidSaleTransactionRequest(
        voidSaleRequestItem: VoidSaleRequestItem
    ): TransactionRequest {
        val transactionRequest = TransactionRequest(voidSaleRequestItem.transactionType)
        transactionRequest.amount = voidSaleRequestItem.amount!!
        transactionRequest.originalRetrievalReferenceNumber = voidSaleRequestItem.originalRRN
        return transactionRequest
    }

    fun prepareVoidSaleRequestItem(
        transactionType: TransactionType,
        amount: BigDecimal,
        data: String
    ): VoidSaleRequestItem {
        return VoidSaleRequestItem(
            transactionType = transactionType,
            amount = amount,
            originalRRN = data
        )
    }

    fun getTotalAmount(
        transactionItem: TransactionItem,
        voidSaleViewModel: VoidViewModel? = null
    ): BigDecimal {
        val transactionAmount = HelperUtils.fromIsoAmountToBigDecimal(transactionItem.amount)
        /*Add Tip here if necessary */
        return transactionAmount;
    }
}