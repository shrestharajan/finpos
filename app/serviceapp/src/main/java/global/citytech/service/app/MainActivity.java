package global.citytech.service.app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import global.citytech.crypto.PosCryptoApi;

public class MainActivity extends AppCompatActivity {

    private TextView tvEncrypt;
    private Button buttonEncrypt;
    private TextView tvDecrypt;
    private Button buttonDecrypt;
    private final String TEST_DATA = "How about this test data?";
    private final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCN6IMFtnyzqJHGT774HibNzwEyyJGbJx7kydft7Ak5Ot19y+dVFyTRlAbtZ4fgwDhv5y40vV3gD1+EAjfLIyGSpnNsbAt0nkMTQ+mi+BjuQWm8WRGnB63Sst4QGzMuA8+/r7HdpGzW0H9/Q6EmNKvA6RG6o0c9mhEGYfmxwdMLLwIDAQAB";
    private final String PRIVATE_KEY = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAI3ogwW2fLOokcZPvvgeJs3PATLIkZsnHuTJ1+3sCTk63X3L51UXJNGUBu1nh+DAOG/nLjS9XeAPX4QCN8sjIZKmc2xsC3SeQxND6aL4GO5BabxZEacHrdKy3hAbMy4Dz7+vsd2kbNbQf39DoSY0q8DpEbqjRz2aEQZh+bHB0wsvAgMBAAECgYBrEpnxsIyM3k8Hi9oyykVLGrrzv2Ql5jGoPSfgzCz1cpX2uehVSr4sJ/3GGUzr5v1uacdyYcO4MMFIEIhajKpiJdlMHoOl/Uqw5AAQjh80ihZolj3W1NuPVpasokGKiP9zJG9DZxUwvBlDC81NjRHXchsuN5mnXGpZTfC6Ckj4GQJBAMQQzLU8zdDkdEK1nimVXrxpJ8VkH5xoab1A3QtuIjUJwep7XoPQe+VVj2C72VvSKotNS7jUad0B3m8o6xYfqR0CQQC5SZp12hsUQBIFCNGO36gtIztovgsb94/JWvqIoy3Oy1AEzkCY4DI4ZndlMfHRKkjZvn0DmW1p6tHc7UKimB+7AkAQAisktSU85BpWBAw46vhEO/XCWS13kNLpX+1sbH6gg/5wTchmzNQA4p6FUHEr3RoaK6J+IeHO6MMnolingshFAkB/QvuRGJXiycLWrGDqpdln7zv9zBHrSpT6sr9DG/j5gRFby6H4nnw/2rkEy1IQ3N8el0RcNYQ97GfqOxe6f4vDAkB8Sx161JpjzJIB2vOEAflowI2Eo/6X8OQb/DMyaOwGft7oNEQgLIyEdqGp+QJulb+M1en9UpEBY32P600HkNe9";
    private String encryptedData = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvEncrypt = findViewById(R.id.tv_encrypted_data);
        tvDecrypt = findViewById(R.id.tv_decrypted_data);
        buttonEncrypt = findViewById(R.id.btn_encrypt);
        buttonEncrypt.setOnClickListener(v -> {
            PosCryptoApi posCryptoApi = new PosCryptoApi();
            encryptedData = posCryptoApi.encrypt(PRIVATE_KEY, TEST_DATA);
            System.out.println("Encrypted Data ::: " + encryptedData);
            tvEncrypt.setText(encryptedData);
        });

        buttonDecrypt = findViewById(R.id.btn_decrypt);
        buttonDecrypt.setOnClickListener(v -> {
            PosCryptoApi posCryptoApi = new PosCryptoApi();
            String decryptedData = posCryptoApi.decrypt(PUBLIC_KEY, encryptedData);
            System.out.println("Decrypted Data ::: " + decryptedData);
            tvDecrypt.setText(decryptedData);
        });
    }
}
