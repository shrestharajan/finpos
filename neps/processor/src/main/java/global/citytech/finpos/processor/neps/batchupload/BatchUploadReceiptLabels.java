package global.citytech.finpos.processor.neps.batchupload;

/**
 * Created by Saurav Ghimire on 6/14/21.
 * sauravnghimire@gmail.com
 */


public class BatchUploadReceiptLabels {
    public static final String BATCH_UPLOAD = "BATCH REPORT";
    public static final String BATCH_NUMBER = "BATCH NUMBER";
    public static final String BATCH_REPORT_END = "END OF REPORT";
    public static final String CARD_NUMBER = "CARD NUMBER";
    public static final String CARD_SCHEME = "CARD SCHEME";
    public static final String INVOICE_NUMBER = "INVOICE NUMBER";
    public static final String APPROVAL_CODE = "APPROVAL CODE";
    public static final String TRANSACTION_TYPE = "TRANSACTION";
    public static final String TRANSACTION_DATE = "TRANSACTION DATE";
    public static final String TRANSACTION_TIME = "TRANSACTION TIME";
    public static final String AMOUNT = "AMOUNT";
    public static final String RESPONSE_CODE = "RESPONSE CODE";
    public static final String DIVIDER = "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --";
    public static final String LINE_BREAK = "  ";
}
