package global.citytech.finposframework.hardware.io.cards

import global.citytech.finposframework.switches.PosEntryMode

/**
 * Created by Rishav Chudal on 6/8/20.
 */
enum class CardType(
    val cardType: String,
    val posEntryMode: String
) {
    ICC("Contact Chip Card", "CHIP"),
    MAG("Magnetic Stripe Card", "SWIPED"),
    PICC("Contactless Chip Card", "TAPPED"),
    MANUAL("Manual PAN entry", "MANUAL"),
    UNKNOWN("Card Type Unknown", "UNKNOWN")
    ;

}

fun CardType.getPosEntry():PosEntryMode{
     return when(this){
        CardType.ICC -> PosEntryMode.ICC
        CardType.MAG -> PosEntryMode.MAGNETIC_STRIPE
        CardType.MANUAL -> PosEntryMode.MANUAL
        CardType.PICC -> PosEntryMode.PICC
        else -> PosEntryMode.UNSPECIFIED
    }
}