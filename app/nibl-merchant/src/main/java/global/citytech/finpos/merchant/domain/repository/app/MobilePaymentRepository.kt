package global.citytech.finpos.merchant.domain.repository.app

import global.citytech.common.data.Response
import global.citytech.finpos.merchant.domain.usecase.mobile.request.MobileNfcPaymentGenerateRequest
import global.citytech.finpos.merchant.domain.usecase.mobile.request.MobileNfcPaymentStatusRequest
import global.citytech.finpos.merchant.domain.usecase.mobile.response.MobileNfcPaymentStatusResponse
import global.citytech.finpos.merchant.presentation.mobilenfcpayment.NfcResponse
import global.citytech.finposframework.hardware.io.led.LedLight

/**
 * @author SIDDHARTHA GHIMIRE
 */
interface MobilePaymentRepository {
    fun getNfcData(): NfcResponse?
    fun requestMobileNfcPaymentTransaction(mobileNfcPaymentStatusRequest: MobileNfcPaymentStatusRequest): Response
    fun checkMobileNfcPaymentStatus(response: Any): MobileNfcPaymentStatusResponse
    fun parseNfcData(nfcData: ByteArray, aid:String): MobileNfcPaymentGenerateRequest
    fun prepareMobileNfcPaymentRequest(mobileNfcPaymentGenerateRequest: MobileNfcPaymentGenerateRequest): MobileNfcPaymentStatusRequest
    fun incrementNfcPaymentIdentifiers()
    fun cancelTimer()
    fun displaySoundAndLedWithRespectiveDevice(ledLight: LedLight)
    fun hideAlertLed()
    fun cleanMobileNfcTask()
    fun disableNfcReaderMode()
    fun saveNfcPaymentIdentifiers(nfcPaymentIdentifiers: String)
}