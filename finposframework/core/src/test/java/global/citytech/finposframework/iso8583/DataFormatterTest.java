package global.citytech.finposframework.iso8583;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import global.citytech.finposframework.log.Logger;;

import global.citytech.finposframework.utility.HexDump;

import static org.junit.Assert.assertEquals;

/***
 @author Surajchhetry
 ***/
public class DataFormatterTest {

    private Logger logger = Logger.getLogger(DataFormatterTest.class.getName());

    @Test
    public void formatAsBytesTest() {
        try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
            DataFormatter formatter = new DataFormatter();
            DataElementConfig de3Config = new DataElementConfig(
                    3,
                    new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.BCD),
                    new LengthConfig(6, LengthConfig.LengthType.FIXED)
            );
            stream.write(formatter.formatAsBytes(new DataElement(3, "920000"), de3Config));
            DataElementConfig de11Config = new DataElementConfig(
                    11,
                    new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.BCD),
                    new LengthConfig(6, LengthConfig.LengthType.FIXED)
            );
            stream.write(formatter.formatAsBytes(new DataElement(11, "000311"), de11Config));
            DataElementConfig de24Config = new DataElementConfig(
                    24,
                    new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.BCD),
                    new LengthConfig(2, LengthConfig.LengthType.FIXED)
            );
            stream.write(formatter.formatAsBytes(new DataElement(24, "011"), de24Config));

            DataElementConfig de41Config = new DataElementConfig(
                    41,
                    new DataConfig(DataConfig.DataType.ALPHA),
                    new LengthConfig(8, LengthConfig.LengthType.FIXED)
            );
            stream.write(formatter.formatAsBytes(new DataElement(41, "99991003"), de41Config));
            DataElementConfig de42Config = new DataElementConfig(
                    42,
                    new DataConfig(DataConfig.DataType.ALPHA),
                    new LengthConfig(15, LengthConfig.LengthType.FIXED)
            );
            stream.write(formatter.formatAsBytes(new DataElement(42, "405635000000003"), de42Config));
            String hexDump = "\n" +
                    "0x00000000 92 00 00 00 03 11 00 11 39 39 39 39 31 30 30 33 ........99991003\n" +
                    "0x00000010 34 30 35 36 33 35 30 30 30 30 30 30 30 30 33    405635000000003";
            logger.log(HexDump.dumpHexString(stream.toByteArray()));
            assertEquals(hexDump, HexDump.dumpHexString(stream.toByteArray()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void formatAsBytesForVariableLengthTest() {
        try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
            DataFormatter formatter = new DataFormatter();
            DataElementConfig de3Config = new DataElementConfig(
                    2,
                    new DataConfig(DataConfig.DataType.NUMERIC),
                    new LengthConfig(19, LengthConfig.LengthType.LLVAR)
            );
            stream.write(formatter.formatAsBytes(new DataElement(2, "4485622073862713"), de3Config));
            logger.log(HexDump.dumpHexString(stream.toByteArray()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void formatIfPaddingEnableTest() {
        try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
            DataFormatter formatter = new DataFormatter();
            DataElementConfig de3Config = new DataElementConfig(
                    DataElement.TRACK_II_DATA,
                    new DataConfig(DataConfig.DataType.ALPHA, EncodingType.BCD),
                    new LengthConfig(LengthConfig.LengthType.LLVAR, EncodingType.BCD),
                    PaddingType.POSTFIX_WITH_CHAR_F
            );
            stream.write(formatter.formatAsBytes(new DataElement(DataElement.TRACK_II_DATA, "4249720010000129D24116261861919299999"), de3Config));
            logger.log(HexDump.dumpHexString(stream.toByteArray()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
