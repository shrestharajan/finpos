package global.citytech.finposframework.iso8583;

import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.Request;

/**
 * User: Surajchhetry Date: 2/28/20 Time: 3:16 PM
 */
public class RequestContext {
    private final String stan;
    private final TerminalInfo terminalInfo;
    private final ConnectionParam param;
    private Request request;

    public RequestContext(String stan, TerminalInfo terminalInfo, ConnectionParam param) {
        this.stan = stan;
        this.terminalInfo = terminalInfo;
        this.param = param;
    }

    public String getStan() {
        return stan;
    }

    public TerminalInfo getTerminalInfo() {
        return terminalInfo;
    }

    public ConnectionParam getParam() {
        return param;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    @Override
    public String toString() {
        return "RequestContext{" +
                "stan='" + stan + '\'' +
                ", terminalInfo=" + terminalInfo +
                ", param=" + param +
                ", request=" + request +
                '}';
    }
}
