package global.citytech.finpos.device.qualcomm.io.cards.handler

import android.content.Context
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.errcode.ErrCode
import global.citytech.finpos.device.qualcomm.io.cards.transaction.FeitianSingleton
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finpos.device.qualcomm.utils.FeitianUtils
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.CardDetails
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.TransactionState
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.hardware.utility.DeviceApiConstants
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.APPLICATION_SELECTION_CANCELLED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_CARD_DETECT_TIMEOUT
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_CARD_NOT_SUPPORTED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_CARD_READ_FAILURE
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_CL_FALLBACK_TO_CONTACT
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_CONTACT_FALLBACK_TO_MAGNETIC
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_ICC_READER_CARD_DETECT_ERROR
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_ONLINE_APPROVED_CARD_DECLINED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_PINPAD_CANCELLED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_PINPAD_FAILURE
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_PIN_INPUT_TIMEOUT
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_SEE_PHONE
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_SWIPE_CARD_AGAIN
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_TRANSACTION_COMPLETE_APPROVED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_TRANSACTION_COMPLETE_DECLINED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_TRANSACTION_PROCESSING_FAILED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_TRANSACTION_TIMEOUT
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_WAVE_AGAIN
import global.citytech.finposframework.hardware.utility.TransactionProcessedState
import global.citytech.finposframework.notifier.Notifier

/**
 * Created by Rishav Chudal on 4/12/21.
 */
class FeitianOnEndProcessHandler constructor(
    private val context: Context,
    private val returnCode: Int,
    private val transactionData: String?,
    private val notifier: Notifier
) : FeitianLogger.Listener {

    private val logger = FeitianLogger(this)
    private val emv = Emv.getInstance(this.context.applicationContext)
    private var readCardResponse: ReadCardResponse? = null


    fun isTransactionAnalysisRequired(): Boolean {
        return when (FeitianSingleton.transactionProcessedState) {
            TransactionProcessedState.CARD_DETECTED_MAG -> false
            else -> true
        }
    }

    fun analyzeTransactionResult(): ReadCardResponse {
        this.logger.logWithClassName(
            "Current Transaction Processed Status ::: "
                    + FeitianSingleton.transactionProcessedState
        )
        if(FeitianSingleton.transactionProcessedState == TransactionProcessedState.NEED_TO_GO_ONLINE || FeitianSingleton.transactionProcessedState == TransactionProcessedState.PIN_ENTRY_BYPASS){
            this.logger.logWithClassName("PROCESSED STATE MATCHED")
            val cardDetails= CardDetails()
            cardDetails.transactionState = TransactionState.OFFLINE_DECLINED
            val result = Result.PIN_BYPASSED
            this.prepareCardDetailsAndReadCardResponse(
                cardDetails,
                result,
                Result.PIN_BYPASSED.message
            )
            return this.readCardResponse!!
        }
        when (FeitianSingleton.transactionProcessedState) {

            TransactionProcessedState.SEARCHING_FOR_CARD,
            TransactionProcessedState.SEARCH_CARD_FAILED,
            TransactionProcessedState.CARD_DETECTED_UNKNOWN,
            TransactionProcessedState.FAILURE -> onTransactionFailureCase()

            TransactionProcessedState.SEARCH_CARD_TIMEOUT -> onSearchCardTimeOut()
            TransactionProcessedState.MAG_NEEDS_SWIPE_AGAIN -> onSwipeAgainRequired()
            TransactionProcessedState.ICC_READER_CARD_DETECT_ERROR -> onIccReaderDetectError()
            TransactionProcessedState.APPLICATION_SELECTION_CANCELLED -> onAppSelectionProcessCancelled()
            TransactionProcessedState.APPLICATION_SELECTION_FAILED -> onAppSelectionProcessFailed()
            TransactionProcessedState.PIN_ENTRY_CANCEL -> onPinEntryCancelledByUser()
            TransactionProcessedState.PINPAD_ERROR -> onPinPadErrorOccurred()
            TransactionProcessedState.PIN_ENTRY_TIMEOUT -> onPinEntryTimeOut()
            TransactionProcessedState.TRANSACTION_LATCHES_TIMEOUT -> onTransactionTimeOut()
            else -> analyzeSdkReturnCode()
        }
        return this.readCardResponse!!
    }

    private fun onTransactionFailureCase() {
        notifyCardReadStatusToUI(
            Notifier.EventType.DETECT_CARD_ERROR,
            DeviceApiConstants.MSG_MSG_NO_CARD_DETECTED
        )
        prepareReadCardResponse(
            cardDetails = null,
            result = Result.FAILURE,
            message = MSG_CARD_READ_FAILURE
        )
    }

    private fun onSearchCardTimeOut() {
        notifyCardReadStatusToUI(
            Notifier.EventType.DETECT_CARD_ERROR,
            MSG_CARD_DETECT_TIMEOUT
        )
        prepareReadCardResponse(
            cardDetails = null,
            result = Result.TIMEOUT,
            message = MSG_CARD_DETECT_TIMEOUT
        )
    }

    private fun onSwipeAgainRequired() {
        val cardDetails = CardDetails()
        cardDetails.cardType = CardType.MAG
        prepareReadCardResponse(
            cardDetails,
            Result.SWIPE_AGAIN,
            MSG_SWIPE_CARD_AGAIN
        )
    }

    private fun onIccReaderDetectError() {
        notifyCardReadStatusToUI(
            Notifier.EventType.DETECT_CARD_ERROR,
            MSG_ICC_READER_CARD_DETECT_ERROR
        )
        val cardDetails = CardDetails()
        cardDetails.cardType = CardType.ICC
        prepareReadCardResponse(
            cardDetails,
            Result.ICC_CARD_DETECT_ERROR,
            MSG_ICC_READER_CARD_DETECT_ERROR
        )
    }

    private fun onAppSelectionProcessCancelled() {
        val cardDetails = CardDetails()
        addTransactionDataToCardDetails(cardDetails)
        prepareReadCardResponse(
            cardDetails = cardDetails,
            result = Result.USER_CANCELLED,
            message = APPLICATION_SELECTION_CANCELLED
        )
    }

    private fun onAppSelectionProcessFailed() {
        if (FeitianSingleton.currentCardDetected == CardType.PICC) {
            onPICCWaveAgain()
        } else {
            onApplicationIsBlocked()
        }
    }

    private fun onApplicationIsBlocked() {
        val cardDetails = CardDetails()
        addTransactionDataToCardDetails(cardDetails)
        prepareReadCardResponse(
            cardDetails = cardDetails,
            result = Result.APPLICATION_BLOCKED,
            message = APPLICATION_SELECTION_CANCELLED
        )
    }

    private fun onPinEntryCancelledByUser() {
        val cardDetails = CardDetails()
        addTransactionDataToCardDetails(cardDetails)
        prepareReadCardResponse(
            cardDetails = cardDetails,
            result = Result.USER_CANCELLED,
            message = MSG_PINPAD_CANCELLED
        )
    }

    private fun onPICCWaveAgain() {
        val cardDetails = CardDetails()
        addTransactionDataToCardDetails(cardDetails)
        prepareReadCardResponse(
            cardDetails = cardDetails,
            result = Result.WAVE_AGAIN,
            message = MSG_WAVE_AGAIN
        )
    }

    private fun onPinPadErrorOccurred() {
        val cardDetails = CardDetails()
        addTransactionDataToCardDetails(cardDetails)
        prepareReadCardResponse(
            cardDetails = cardDetails,
            result = Result.PINPAD_ERROR,
            message = MSG_PINPAD_FAILURE
        )
    }

    private fun onPinEntryTimeOut() {
        val cardDetails = CardDetails()
        addTransactionDataToCardDetails(cardDetails)
        prepareReadCardResponse(
            cardDetails = cardDetails,
            result = Result.TIMEOUT,
            message = MSG_PIN_INPUT_TIMEOUT
        )
    }

    private fun onTransactionTimeOut() {
        prepareReadCardResponse(
            cardDetails = null,
            result = Result.TIMEOUT,
            message = MSG_TRANSACTION_TIMEOUT
        )
    }

    private fun analyzeSdkReturnCode() {
        this.logger.logWithClassName("onEndProcess ::: Status ::: ".plus(returnCode))
        this.logger.logWithClassName(
            "onEndProcess ::: Status Data ::: ".plus(ErrCode.toString(this.returnCode))
        )
        this.logger.logWithClassName("onEndProcess ::: transData ::: ".plus(this.transactionData))
        this.logger.logWithClassName("Analyzing transaction result...")
        when (this.returnCode) {
            ErrCode.ERR_ONLINE_APPROVED -> onTransactionOnlineApproved()
            ErrCode.ERR_ONLINE_DECLINED -> onTransactionOnlineDeclined()
            ErrCode.ERR_OFFLINE_APPROVED -> onTransactionOfflineApproved()
            ErrCode.ERR_OFFLINE_DECLINED -> onTransactionOfflineDeclined()

            ErrCode.ERR_REFUND_DECLINED,
            ErrCode.ERR_AUC_RESTRICTION,
            ErrCode.ERR_AAC_NOT_CASH_NOT_CASHBACK -> onTransactionOfflineDeclined()

            ErrCode.ERR_USE_ANOTHER_INTERFACE -> onTransactionFallBack()
            ErrCode.ERR_USE_IC_INTERFACE -> onTransactionFallBackToContactInterface()
            ErrCode.ERR_USE_MAG_INTERFACE -> onTransactionFallBackToMagInterface()

            ErrCode.ERR_RESTART_B -> onPICCWaveAgain()

            FeitianConstants.ERROR_CODE_AID_BLOCKED,
            ErrCode.ERR_CARD_BLOCK -> onCardIsBlocked()

            ErrCode.ERR_SEE_PHONE -> onSdkReturnSeePhone()
            ErrCode.ERR_ONLINE_END_CARD_DECLINED -> onOnlineApprovedCardDeclined()

            ErrCode.ERR_NOT_FOUND_SUPPORTED_APP,
            ErrCode.ERR_NOT_FOUND_MATCHED_APP,
            ErrCode.ERR_NOT_SUPPORTED -> onCardNotSupported()

            else -> onOtherErrorCodes()
        }
    }

    private fun onTransactionOnlineApproved() {
        val cardDetails = CardDetails()
        this.logger.logWithClassName("Transaction ::: Online Approved...")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.ONLINE_APPROVED
        cardDetails.transactionState = TransactionState.ONLINE_APPROVED
        val result = Result.ONLINE_APPROVED
        this.prepareCardDetailsAndReadCardResponse(
            cardDetails,
            result,
            MSG_TRANSACTION_COMPLETE_APPROVED
        )
    }

    private fun onTransactionOfflineApproved() {
        val cardDetails = CardDetails()
        this.logger.logWithClassName("Transaction ::: Offline Approved...")
        checkOfflineApprovalAndUpdateTransactionProcessedState()
        cardDetails.transactionState = TransactionState.OFFLINE_APPROVED
        val result = Result.OFFLINE_APPROVED
        this.prepareCardDetailsAndReadCardResponse(
            cardDetails,
            result,
            MSG_TRANSACTION_COMPLETE_APPROVED
        )
    }

    private fun checkOfflineApprovalAndUpdateTransactionProcessedState() {
        val transactionNeedOnline = FeitianSingleton.onlineTransaction
        val hostIsNotConnected = !FeitianSingleton.hostConnected
        if (transactionNeedOnline && hostIsNotConnected) {
            logger.logWithClassName("Transaction ::: Online Fail Offline Approved")
            FeitianSingleton.transactionProcessedState =
                TransactionProcessedState.ONLINE_FAIL_OFFLINE_APPROVED
        } else {
            logger.logWithClassName("Transaction ::: Offline Approved")
            FeitianSingleton.transactionProcessedState = TransactionProcessedState.OFFLINE_APPROVED

        }
    }

    private fun onTransactionOnlineDeclined() {
        val cardDetails = CardDetails()
        this.logger.logWithClassName("Transaction ::: Online Declined...")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.ONLINE_DECLINED
        cardDetails.transactionState = TransactionState.ONLINE_DECLINED
        val result = Result.ONLINE_DECLINED
        this.prepareCardDetailsAndReadCardResponse(
            cardDetails,
            result,
            MSG_TRANSACTION_COMPLETE_DECLINED
        )
    }

    private fun onTransactionOfflineDeclined() {
        val cardDetails = CardDetails()
        this.logger.logWithClassName("Transaction ::: Offline Declined...")
        checkOfflineDeclinedAndUpdateTransactionProcessedState()
        cardDetails.transactionState = TransactionState.OFFLINE_DECLINED
        val result = Result.OFFLINE_DECLINED
        this.prepareCardDetailsAndReadCardResponse(
            cardDetails,
            result,
            MSG_TRANSACTION_COMPLETE_DECLINED
        )
    }

    private fun checkOfflineDeclinedAndUpdateTransactionProcessedState() {
        val transactionNeedOnline = FeitianSingleton.onlineTransaction
        val hostIsNotConnected = !FeitianSingleton.hostConnected
        if (transactionNeedOnline && hostIsNotConnected) {
            logger.logWithClassName("Transaction ::: Online Fail Offline Declined")
            FeitianSingleton.transactionProcessedState =
                TransactionProcessedState.ONLINE_FAIL_OFFLINE_DECLINED
        } else {
            logger.logWithClassName("Transaction ::: Offline Declined")
            FeitianSingleton.transactionProcessedState = TransactionProcessedState.OFFLINE_DECLINED

        }
    }

    private fun prepareCardDetailsAndReadCardResponse(
        cardDetails: CardDetails,
        result: Result,
        message: String
    ) {
        this.prepareCardDetailsOfTransaction(cardDetails, "")
        this.prepareReadCardResponse(
            cardDetails,
            result,
            message
        )
    }

    private fun onTransactionFallBack() {
        this.logger.logWithClassName("onTransactionFallBack...")
        if (FeitianSingleton.currentCardDetected === CardType.PICC) {
            FeitianSingleton.transactionProcessedState = TransactionProcessedState.PICC_FALLBACK
            this.onTransactionFallBackToContactInterface()
        } else if (FeitianSingleton.currentCardDetected === CardType.ICC) {
            FeitianSingleton.transactionProcessedState = TransactionProcessedState.ICC_FALLBACK
            this.onTransactionFallBackToMagInterface()
        } else {
            this.onOtherErrorCodes()
        }
    }

    private fun onTransactionFallBackToContactInterface() {
        this.logger.logWithClassName("onTransactionFallBackToContactInterface...")
        val cardDetails = CardDetails()
        cardDetails.cardType = CardType.PICC
        prepareReadCardResponse(
            cardDetails,
            Result.PICC_FALLBACK_TO_ICC,
            MSG_CL_FALLBACK_TO_CONTACT
        )
    }

    private fun onTransactionFallBackToMagInterface() {
        this.logger.logWithClassName("onTransactionFallBackToMagInterface...")
        val cardDetails = CardDetails()
        cardDetails.cardType = CardType.ICC
        prepareReadCardResponse(
            cardDetails,
            Result.ICC_FALLBACK_TO_MAGNETIC,
            MSG_CONTACT_FALLBACK_TO_MAGNETIC
        )
    }

    private fun onCardIsBlocked() {
        this.logger.logWithClassName("onCardIsBlocked...")
        val cardDetails = CardDetails()
        addTransactionDataToCardDetails(cardDetails)
        prepareReadCardResponse(
            cardDetails,
            Result.CARD_BLOCKED,
            MSG_CONTACT_FALLBACK_TO_MAGNETIC
        )
    }

    private fun onSdkReturnSeePhone() {
        logger.logWithClassName("See Phone...")
        val cardDetails = CardDetails()
        prepareCardDetailsAndReadCardResponse(
            cardDetails,
            Result.SEE_PHONE,
            MSG_SEE_PHONE
        )
    }

    private fun onOnlineApprovedCardDeclined() {
        logger.logWithClassName("Transaction Approved Online, declined by Card")
        val cardDetails = CardDetails()
        prepareCardDetailsAndReadCardResponse(
            cardDetails,
            Result.OFFLINE_DECLINED,
            MSG_ONLINE_APPROVED_CARD_DECLINED
        )
    }

    private fun onCardNotSupported() {
        logger.logWithClassName("Card Function Not Supported...")
        val cardDetails = CardDetails()
        prepareCardDetailsAndReadCardResponse(
            cardDetails,
            Result.CARD_NOT_SUPPORTED,
            MSG_CARD_NOT_SUPPORTED
        )
    }



    private fun onOtherErrorCodes() {
        this.logger.logWithClassName("onOtherErrorCodes...")
        val cardDetails = CardDetails()
        prepareCardDetailsOfTransaction(cardDetails, "")
        prepareReadCardResponse(
            cardDetails,
            Result.PROCESSING_ERROR,
            MSG_TRANSACTION_PROCESSING_FAILED
        )

    }

    private fun prepareCardDetailsOfTransaction(cardDetails: CardDetails, transData: String) {
        addTransactionDataToCardDetails(cardDetails)
        addEmvDataToCardDetails(cardDetails, transData)
    }

    private fun addTransactionDataToCardDetails(cardDetails: CardDetails) {
        cardDetails.cardType = FeitianSingleton.currentCardDetected
        cardDetails.amount = FeitianSingleton.readCardRequest!!.amount
        cardDetails.cashBackAmount = FeitianSingleton.readCardRequest!!.cashBackAmount
    }

    private fun addEmvDataToCardDetails(cardDetails: CardDetails, transData: String) {
        FeitianUtils.addBasicEMVDataBasedOnCardDetails(
            emv,
            FeitianSingleton,
            cardDetails
        )
        FeitianUtils.addICCDataBlockAndTagCollection(transData, emv, cardDetails)
    }

    private fun notifyCardReadStatusToUI(
        eventType: Notifier.EventType,
        message: String
    ) {
        notifier.notify(
            eventType,
            message
        )
    }

    private fun prepareReadCardResponse(
        cardDetails: CardDetails?,
        result: Result,
        message: String
    ) {
        this.logger.logWithClassName("Result ::: ".plus(result))
        this.logger.logWithClassName("Message ::: ".plus(message))
        this.readCardResponse = ReadCardResponse(
            cardDetails,
            result,
            message
        )
        FeitianSingleton.readCardResponse = this.readCardResponse!!
    }

    override fun getClassName() = FeitianOnEndProcessHandler::class.java.simpleName

}