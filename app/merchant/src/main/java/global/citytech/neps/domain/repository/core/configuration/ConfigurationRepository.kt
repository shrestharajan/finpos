package global.citytech.neps.domain.repository.core.configuration

import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.posswitchintregator.api.logon.LogOnResponseParameter
import global.citytech.posswitchintregator.neps.usecases.keyexchange.KeyExchangeResponseModel
import io.reactivex.Observable
import java.util.*

interface ConfigurationRepository {
   fun logOn(configurationItem: ConfigurationItem ):Observable<LogOnResponseParameter>
   fun exchangeKey(configurationItem: ConfigurationItem):Observable<KeyExchangeResponseModel>

}