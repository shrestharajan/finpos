package global.citytech.finpos.merchant.presentation.pinchange

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityPinChangeBinding
import global.citytech.finpos.merchant.presentation.greenpin.GreenPinBaseTransactionActivity
import global.citytech.finpos.merchant.presentation.greenpin.GreenPinBaseTransactionViewModel
import global.citytech.finpos.merchant.presentation.alertdialogs.PosEntryMode
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finposframework.usecases.TransactionType
import kotlinx.android.synthetic.main.activity_pin_change.*

class PinChangeActivity :
    GreenPinBaseTransactionActivity<ActivityPinChangeBinding, PinChangeViewModel>(),
    View.OnClickListener {

    private lateinit var viewModel: PinChangeViewModel
    private lateinit var configurationItem: ConfigurationItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel()
        initBaseViews()
        initObservers()
        viewModel.getConfigurationItem()
        showPosEntrySelectionDialog()
    }

    private fun showPosEntrySelectionDialog() {
        Glide.with(this).load(PosEntryMode.ACCEPT_ALL.loaderImage).into(image)
    }

    override fun getBindingVariable(): Int = BR.pinChangeViewModel

    override fun getLayout(): Int = R.layout.activity_pin_change

    override fun getViewModel(): PinChangeViewModel =
        ViewModelProviders.of(this)[PinChangeViewModel::class.java]

    private fun initObservers(){
        this.viewModel.configurationItem.observe(this, Observer{
            this.configurationItem= it
            this.viewModel.pinChangeRequest(configurationItem, getTransactionType())
        })

        viewModel.doReturnToDashboard.observe(this, Observer{
            if(it){
                returnToDashboard()
            }

        })
    }
    
    override fun onManualButtonClicked() {
        TODO("Not yet implemented")
    }

    override fun onCancelButtonClicked() {
        this.viewModel.shouldDispose = true
        this.viewModel.doCleanUp(GreenPinBaseTransactionViewModel.Task.RETURN_TO_DASHBOARD)
    }

    override fun getGreenPinBaseTransactionViewModel(): GreenPinBaseTransactionViewModel =
        this.viewModel


    override fun onAmountActivityResult(data: Intent?) {
        TODO("Not yet implemented")
    }

    override fun onManualActivityResult(data: Intent?) {
        TODO("Not yet implemented")
    }

    override fun getTransactionType(): TransactionType = TransactionType.PIN_CHANGE

    override fun onClick(p0: View?) {

    }

}