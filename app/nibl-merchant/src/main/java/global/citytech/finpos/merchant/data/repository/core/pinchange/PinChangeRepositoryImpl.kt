package global.citytech.finpos.merchant.data.repository.core.pinchange

import global.citytech.finpos.merchant.data.datasource.core.pinchange.PinChangeDataSource
import global.citytech.finpos.merchant.domain.model.pinchange.PinChangeResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.pinchange.PinChangeRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.pinchange.PinChangeRequestItem
import io.reactivex.Observable

class PinChangeRepositoryImpl(private val pinChangeDataSource: PinChangeDataSource): PinChangeRepository {
    override fun pinChangeRequest(
        configurationItem: ConfigurationItem,
        pinChangeRequestItem: PinChangeRequestItem
    ): Observable<PinChangeResponseEntity> =
            pinChangeDataSource.pinChangeRequester(configurationItem, pinChangeRequestItem)

}