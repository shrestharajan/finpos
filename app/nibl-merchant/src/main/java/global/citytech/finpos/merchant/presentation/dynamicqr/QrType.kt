package global.citytech.finpos.merchant.presentation.dynamicqr

enum class QrType(
    val codes: ArrayList<String>
) {
    FONEPAY(arrayListOf("FONEPAY", "FONE PAY")),
    NQR(arrayListOf("NQR", "NEPALPAY","NEPAL PAY QR"));

    companion object {
        fun getByCodes(code: String): QrType? {
            values().forEach { qrType ->
                qrType.codes.forEach {
                    if (it.equals(code, true))
                        return qrType
                }
            }
            return null
        }
    }

}