package global.citytech.finposframework.repositories;

public class RetrieveStanResponse {

    private String stan;

    public RetrieveStanResponse(String stan) {
        this.stan = stan;
    }

    public String getStan() {
        return stan;
    }
}