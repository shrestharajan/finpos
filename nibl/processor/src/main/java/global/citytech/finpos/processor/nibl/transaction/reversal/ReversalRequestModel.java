package global.citytech.finpos.processor.nibl.transaction.reversal;

import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reversal.ReversalRequestParameter;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;

/**
 * Created by Rishav Chudal on 9/29/20.
 */
public class ReversalRequestModel implements UseCase.Request, ReversalRequestParameter {
    private TransactionRequest transactionRequest;
    private TransactionRepository transactionRepository;
    private PrinterService printerService;
    private ApplicationRepository applicationRepository;
    private ReadCardResponse readCardResponse;
    private ReceiptLog receiptLog;
    private String currencyCode;

    private ReversalRequestModel() {

    }

    public TransactionRequest getTransactionRequest() {
        return transactionRequest;
    }

    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public ApplicationRepository getApplicationRepository() {
        return applicationRepository;
    }

    public ReadCardResponse getReadCardResponse() {
        return readCardResponse;
    }

    public ReceiptLog getReceiptLog() {
        return receiptLog;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public static final class Builder {
        private TransactionRequest transactionRequest;
        private TransactionRepository transactionRepository;
        private PrinterService printerService;
        private ApplicationRepository applicationRepository;
        private ReadCardResponse readCardResponse;
        private ReceiptLog receiptLog;
        private String currencyCode;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withTransactionRequest(TransactionRequest transactionRequest) {
            this.transactionRequest = transactionRequest;
            return this;
        }

        public Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public Builder withReadCardResponse(ReadCardResponse readCardResponse) {
            this.readCardResponse = readCardResponse;
            return this;
        }

        public Builder withReceiptLog(ReceiptLog receiptLog) {
            this.receiptLog = receiptLog;
            return this;
        }

        public Builder withCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public ReversalRequestModel build() {
            ReversalRequestModel reversalRequestModel = new ReversalRequestModel();
            reversalRequestModel.transactionRequest = this.transactionRequest;
            reversalRequestModel.transactionRepository = this.transactionRepository;
            reversalRequestModel.readCardResponse = this.readCardResponse;
            reversalRequestModel.applicationRepository = this.applicationRepository;
            reversalRequestModel.printerService = this.printerService;
            reversalRequestModel.receiptLog = this.receiptLog;
            reversalRequestModel.currencyCode = this.currencyCode;
            return reversalRequestModel;
        }
    }
}
