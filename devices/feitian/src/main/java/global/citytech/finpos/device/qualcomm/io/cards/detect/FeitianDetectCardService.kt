package global.citytech.finpos.device.qualcomm.io.cards.detect

import global.citytech.finposframework.hardware.io.cards.detect.DetectCardService

/**
 * Created by Rishav Chudal on 4/7/21.
 */
interface FeitianDetectCardService: DetectCardService {
    fun detectCard(request: FeitianDetectCardRequest): FeitianDetectCardResponse
}