package global.citytech.finpos.processor.neps.reconciliation;

import global.citytech.finposframework.usecases.reconciliation.ReconciliationTotals;
import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;

public class NepsReconciliationReceipt {
    private Retailer retailer;
    private Performance performance;
    private String reconciliationBatchNumber;
    private NepsReconciliationTotals reconciliationTotals;
    private String reconciliationResult;
    private String host;
    private String applicationVersion;

    public NepsReconciliationReceipt() {
    }

    public Retailer getRetailer() {
        return retailer;
    }

    public Performance getPerformance() {
        return performance;
    }

    public String getReconciliationBatchNumber() {
        return reconciliationBatchNumber;
    }

    public ReconciliationTotals getReconciliationTotals() {
        return reconciliationTotals;
    }

    public String getReconciliationResult() {
        return reconciliationResult;
    }

    public String getHost() {
        return host;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public static final class Builder {
        private Retailer retailer;
        private Performance performance;
        private String reconciliationBatchNumber;
        private NepsReconciliationTotals reconciliationTotals;
        private String reconciliationResult;
        private String host;
        private String applicationVersion;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withRetailer(Retailer retailer) {
            this.retailer = retailer;
            return this;
        }

        public Builder withPerformance(Performance performance) {
            this.performance = performance;
            return this;
        }

        public Builder withReconciliationBatchNumber(String reconciliationBatchNumber) {
            this.reconciliationBatchNumber = reconciliationBatchNumber;
            return this;
        }

        public Builder withReconciliationTotals(NepsReconciliationTotals reconciliationTotals) {
            this.reconciliationTotals = reconciliationTotals;
            return this;
        }

        public Builder withReconciliationResult(String reconciliationResult) {
            this.reconciliationResult = reconciliationResult;
            return this;
        }

        public Builder withHost(String host) {
            this.host = host;
            return this;
        }

        public Builder withApplicationVersion(String applicationVersion) {
            this.applicationVersion = applicationVersion;
            return this;
        }

        public NepsReconciliationReceipt build() {
            NepsReconciliationReceipt nepsReconciliationReceipt = new NepsReconciliationReceipt();
            nepsReconciliationReceipt.retailer = this.retailer;
            nepsReconciliationReceipt.reconciliationResult = this.reconciliationResult;
            nepsReconciliationReceipt.performance = this.performance;
            nepsReconciliationReceipt.reconciliationTotals = this.reconciliationTotals;
            nepsReconciliationReceipt.reconciliationBatchNumber = this.reconciliationBatchNumber;
            nepsReconciliationReceipt.host = this.host;
            nepsReconciliationReceipt.applicationVersion = this.applicationVersion;
            return nepsReconciliationReceipt;
        }
    }
}
