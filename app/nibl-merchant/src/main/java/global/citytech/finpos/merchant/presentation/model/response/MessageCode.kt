package global.citytech.finpos.merchant.presentation.model.response

import global.citytech.common.data.LabelValue

/**
 * Created by Rishav Chudal on 9/14/20.
 */
data class MessageCode(
    var messageCode: LabelValue? = null,
    var messageText: LabelValue? = null
)