package global.citytech.finpos.processor.neps.receipt.summmaryreport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;
import global.citytech.finposframework.utility.DateUtils;

/**
 * Created by Saurav Ghimire on 3/30/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public class SummaryReportReceiptMaker {

    private final TerminalRepository terminalRepository;
    private final ReconciliationRepository reconciliationRepository;

    public SummaryReportReceiptMaker(TerminalRepository terminalRepository, ReconciliationRepository reconciliationRepository) {
        this.terminalRepository = terminalRepository;
        this.reconciliationRepository = reconciliationRepository;
    }

    public SummaryReportReceipt prepare(String batchNumber, List<CardSchemeType> cardSchemeTypes) {
        return SummaryReportReceipt.Builder.newInstance()
                .withRetailer(this.prepareRetailerInfo(this.terminalRepository.findTerminalInfo()))
                .withHost("000048")
                .withPerformance(this.preparePerformanceInfo())
                .withReconciliationBatchNumber(batchNumber)
                .withSummaryReport(this.prepareSummaryReport(batchNumber, cardSchemeTypes))
                .build();
    }

    private Retailer prepareRetailerInfo(TerminalInfo terminalInfo) {
        return Retailer.Builder.defaultBuilder()
                .withRetailerLogo(terminalInfo.getMerchantPrintLogo())
                .withRetailerName(terminalInfo.getMerchantName())
                .withRetailerAddress(terminalInfo.getMerchantAddress())
                .withMerchantId(terminalInfo.getMerchantID())
                .withTerminalId(terminalInfo.getTerminalID())
                .build();
    }

    private Performance preparePerformanceInfo() {
        return Performance.Builder.defaultBuilder()
                .withEndDateTime(DateUtils.yyMMddHHmmssSSSDate())
                .withStartDateTime(DateUtils.yyMMddHHmmssSSSDate())
                .build();
    }

    private SummaryReport prepareSummaryReport(String batchNumber, List<CardSchemeType> cardSchemeTypes) {
        Map<CardSchemeType, CardSchemeSummary> cardSchemeSummaryMap = new HashMap<>();
        for (CardSchemeType cardSchemeType : cardSchemeTypes) {
            addCardSchemeOnlyIfTransactionWasPerformed(
                    cardSchemeSummaryMap,
                    batchNumber,
                    cardSchemeType
            );
        }
        return new SummaryReport(this.terminalRepository.getTerminalTransactionCurrencyName(),cardSchemeSummaryMap);
    }

    private void addCardSchemeOnlyIfTransactionWasPerformed(
            Map<CardSchemeType, CardSchemeSummary> cardSchemeSummaryMap,
            String batchNumber,
            CardSchemeType cardSchemeType
    ) {
        CardSchemeSummary cardSchemeSummary = prepareSummaryWithRespectToCardScheme(
                batchNumber, cardSchemeType
        );
        checkTotalCountForTheCardSchemeAndProceed(cardSchemeType, cardSchemeSummary, cardSchemeSummaryMap);
    }

    private void checkTotalCountForTheCardSchemeAndProceed(
            CardSchemeType cardSchemeType,
            CardSchemeSummary cardSchemeSummary,
            Map<CardSchemeType, CardSchemeSummary> cardSchemeSummaryMap
    ) {
        if (cardSchemeSummary.getTotalCount() > 0) {
            cardSchemeSummaryMap.put(cardSchemeType, cardSchemeSummary);
        }
    }

    private CardSchemeSummary prepareSummaryWithRespectToCardScheme(String batchNumber, CardSchemeType cardSchemeType) {
        return CardSchemeSummary.Builder.newInstance()
                .withRefundAmount(this.prepareRefundAmount(batchNumber, cardSchemeType))
                .withRefundCount(this.prepareRefundCount(batchNumber, cardSchemeType))
                .withSalesAmount(this.prepareSalesAmount(batchNumber, cardSchemeType))
                .withSalesCount(this.prepareSalesCount(batchNumber, cardSchemeType))
                .withVoidAmount(this.prepareVoidAmount(batchNumber, cardSchemeType))
                .withVoidCount(this.prepareVoidCount(batchNumber, cardSchemeType))
                .build();
    }

    private double prepareRefundAmount(String batchNumber, CardSchemeType cardSchemeType) {
        return (double) (this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardSchemeType,
                batchNumber, TransactionType.REFUND) -
                this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardSchemeType,
                        batchNumber, TransactionType.VOID, TransactionType.REFUND));
    }

    private long prepareRefundCount(String batchNumber, CardSchemeType cardSchemeType) {
        return this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardSchemeType, batchNumber, TransactionType.REFUND) -
                this.reconciliationRepository.getCountByOriginalTransactionTypeWithCardScheme(cardSchemeType,
                        batchNumber, TransactionType.VOID, TransactionType.REFUND);
    }

    private double prepareSalesAmount(String batchNumber, CardSchemeType cardSchemeType) {
        return this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardSchemeType, batchNumber, TransactionType.PURCHASE) -
                this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardSchemeType, batchNumber, TransactionType.VOID, TransactionType.PURCHASE);
    }

    private long prepareSalesCount(String batchNumber, CardSchemeType cardSchemeType) {
        return this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardSchemeType, batchNumber, TransactionType.PURCHASE) -
                this.reconciliationRepository.getCountByOriginalTransactionTypeWithCardScheme(cardSchemeType, batchNumber, TransactionType.VOID, TransactionType.PURCHASE);
    }

    private double prepareVoidAmount(String batchNumber, CardSchemeType cardSchemeType) {
        return this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardSchemeType, batchNumber, TransactionType.VOID);
    }

    private long prepareVoidCount(String batchNumber, CardSchemeType cardSchemeType) {
        return this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardSchemeType, batchNumber, TransactionType.VOID);
    }
}
