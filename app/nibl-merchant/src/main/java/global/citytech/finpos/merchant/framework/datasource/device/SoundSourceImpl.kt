package global.citytech.finpos.merchant.framework.datasource.device

import android.content.Context
import global.citytech.finpos.merchant.service.AppDeviceFactory
import global.citytech.finposframework.hardware.DeviceServiceType
import global.citytech.finposframework.hardware.io.sound.Sound
import global.citytech.finposframework.hardware.io.sound.SoundService

/**
 * Created by Unique Shakya on 11/26/2020.
 */
class SoundSourceImpl(val context: Context?) : SoundService {

    private var service = AppDeviceFactory.getCore(context, DeviceServiceType.SOUND)
            as SoundService

    override fun playSound(sound: Sound?) {
        service.playSound(sound)
    }
}