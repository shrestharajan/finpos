package global.citytech.finpos.merchant.domain.usecase.mobile.prepare;

public enum MobileNfcPaymentDataFormat {
    N("Numeric")
    , ANS("Alphanumeric Special")
    , S("String");

    private String detail;

    MobileNfcPaymentDataFormat(String detail) {
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
