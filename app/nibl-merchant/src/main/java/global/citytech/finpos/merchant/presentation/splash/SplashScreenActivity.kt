package global.citytech.finpos.merchant.presentation.splash

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import global.citytech.common.extensions.openActivity
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivitySplashScreenBinding
import global.citytech.finpos.merchant.presentation.admin.AdminActivity
import global.citytech.finpos.merchant.presentation.autoreversal.AutoReversalActivity
import global.citytech.finpos.merchant.presentation.dashboard.temp.DashActivity
import global.citytech.finpos.merchant.utils.CashierLogManager
import global.citytech.finpos.merchant.utils.MessageConfig

import global.citytech.finposframework.log.Logger
import global.citytech.tms.utility.permission.AppPermission
import global.citytech.tms.utility.permission.PermissionConstant
import kotlinx.android.synthetic.main.activity_splash_screen.*
import kotlin.system.exitProcess


class SplashScreenActivity : AppBaseActivity<ActivitySplashScreenBinding, SplashViewModel>() {

    companion object {
        const val SPLASH_TIME_OUT: Long = 2800
    }

    private lateinit var viewModel: SplashViewModel


    var shouldAskForPermission: Boolean = false
    val _REQUEST_PERMISSION_SETTING = 10001
    val _REQUEST_PERMISSION = 10000


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        this.viewModel = getViewModel()
        initObservers()
        initSdk()
    }

    override fun onResume() {
        if (shouldAskForPermission) {
            asKRequiredPermission(PermissionConstant.permissions)
        }
        super.onResume()
    }

    private fun asKRequiredPermission(permissions: List<AppPermission>) {
        val permissionToBeAsked = mutableListOf<AppPermission>()
        permissions.forEach {
            if (ContextCompat.checkSelfPermission(
                    this,
                    it.permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                permissionToBeAsked.add(it)
            }
        }
        if (permissionToBeAsked.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                permissionToBeAsked.map { it.permission }.toTypedArray(),
                _REQUEST_PERMISSION
            )
        } else {
            handleOtherTask()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (permissions.isEmpty()) {
            return
        }
        val permissionsStillRequired = mutableListOf<AppPermission>()
        val permissionsDeniedPermanently = mutableListOf<AppPermission>()

        for (i in grantResults.indices) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i])) {
                    permissionsStillRequired.add(AppPermission(permissions[i]))
                } else {
                    permissionsDeniedPermanently.add(AppPermission(permissions[i]))
                }

            }
        }
        when {
            permissionsStillRequired.isNotEmpty() -> {
                asKRequiredPermission(permissionsStillRequired)
                return
            }
            permissionsDeniedPermanently.isNotEmpty() -> {
                shouldAskForPermission = false
                AlertDialog.Builder(this)
                    .setTitle("Permission Required")
                    .setMessage("There are some permission missing. Please go to App Setting and grant the required permissions.")
                    .setPositiveButton(
                        "Go to Settings"
                    ) { _, _ ->
                        val intent = Intent()
                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        val uri = Uri.fromParts("package", packageName, null)
                        intent.data = uri
                        startActivityForResult(intent, _REQUEST_PERMISSION_SETTING);
                    }
                    .setCancelable(false)
                    .show()
            }
            else -> {
                handleOtherTask()
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun handleOtherTask() {
        Thread {
            CashierLogManager.getInstance().initLog()
        }.start()
        openDashActivity()
    }


    private fun initViews() {
        Glide.with(this).load("file:///android_asset/images/finpos_logo_animation.gif")
            .into(iv_logo)
        Glide.with(this).load("file:///android_asset/images/loading.gif")
            .into(pb_configure_terminal)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AutoReversalActivity.REQUEST_CODE) {
            Logger.getLogger(SplashScreenActivity::class.java.name)
                .log("::: AUTO REVERSAL ACTIVITY RESULT RECEIVED :::")
            getViewModel().checkForAppUpdate()
        } else if (requestCode == _REQUEST_PERMISSION_SETTING) {
            shouldAskForPermission = true
        }
    }

    private fun initObservers() {
        this.viewModel.isLoading.observe(
            this,
            Observer {
                if (!it) {
                    showTerminalConfigurationVisible(View.INVISIBLE)
                } else {
                    showTerminalConfigurationVisible(View.VISIBLE)
                }
            }
        )

        this.viewModel.errorMessage.observe(
            this,
            Observer {
                showErrorMessageAndExit(it)
            }
        )

        this.viewModel.isTerminalConfigured.observe(
            this,
            Observer {
                if (it) {
                    getViewModel().checkForAutoReversal()
                } else {
                    showErrorMessageAndExit(getString(R.string.msg_not_configured))
                }
            }
        )

        getViewModel().autoReversalPresent.observe(this, Observer {
            if (it)
                openAutoReversalActivity()
            else
                getViewModel().checkForAppUpdate()
        })

        getViewModel().askAppPermission.observe(this) {
            if (it) {
                asKRequiredPermission(PermissionConstant.permissions)
            }
        }
    }

    private fun openAutoReversalActivity() {
        val intent = Intent(this, AutoReversalActivity::class.java)
        startActivityForResult(intent, AutoReversalActivity.REQUEST_CODE)
    }

    private fun openDashActivity() {
        Handler().postDelayed({
            openActivity(DashActivity::class.java)
            finish()
        }, SPLASH_TIME_OUT)
    }

    private fun initSdk() {
        showTerminalConfigurationVisible(View.VISIBLE)
        this.viewModel.initializeSdk()
    }

    private fun showTerminalConfigurationVisible(visibility: Int) {
        tv_progress.visibility = visibility
        tv_progress.text = getString(R.string.msg_configure_terminal)
        pb_configure_terminal.visibility = visibility
    }

    private fun showErrorMessageAndExit(it: String?) {
        if (!it.isNullOrEmpty()) {
            val messageConfig = MessageConfig.Builder()
                .message(getString(R.string.msg_not_configured))
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    startAdminActivity()
                }
            showFailureMessage(messageConfig)
        }
    }

    private fun startAdminActivity() {
        val intent = AdminActivity.getLaunchIntent(this, false)
        startActivity(intent)
        finish()
    }

    private fun closeApplication() {
        finishAndRemoveTask()
        exitProcess(0)
    }

    override fun getBindingVariable(): Int = BR.splashViewModel

    override fun getLayout(): Int = R.layout.activity_splash_screen

    override fun getViewModel(): SplashViewModel = ViewModelProviders
        .of(this)[SplashViewModel::class.java]

    override fun onDestroy() {
        super.onDestroy()
    }
}
