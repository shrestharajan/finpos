package global.citytech.finposframework.hardware.io.cards.tvr;

/**
 * Created by Unique Shakya on 11/24/2020.
 */
public enum TvrResults {
    BYTE_1_BIT_8("Offline data authentication not performed"),
    BYTE_1_BIT_7("Offline static data authentication failed"),
    BYTE_1_BIT_6("Missing IC card data"),
    BYTE_1_BIT_5("The card appears in the terminal exception file"),
    BYTE_1_BIT_4("Offline dynamic data authentication failed"),
    BYTE_1_BIT_3("Composite dynamic data authentication / application password generation failed"),
    BYTE_1_BIT_2("RFU"),
    BYTE_1_BIT_1("RFU"),
    BYTE_2_BIT_8("IC card and terminal application version are inconsistent"),
    BYTE_2_BIT_7("App has expired"),
    BYTE_2_BIT_6("App has not yet taken effect"),
    BYTE_2_BIT_5("Card products do not allow the requested service"),
    BYTE_2_BIT_4("New card"),
    BYTE_2_BIT_3("RFU"),
    BYTE_2_BIT_2("RFU"),
    BYTE_2_BIT_1("RFU"),
    BYTE_3_BIT_8("Cardholder verification was unsuccessful"),
    BYTE_3_BIT_7("Unknown CVM"),
    BYTE_3_BIT_6("PIN retries exceeded"),
    BYTE_3_BIT_5("Requires PIN, but no PIN pad or PIN pad failure"),
    BYTE_3_BIT_4("Ask for PIN, PIN pad, but no PIN"),
    BYTE_3_BIT_3("Enter online PIN"),
    BYTE_3_BIT_2("RFU"),
    BYTE_3_BIT_1("RFU"),
    BYTE_4_BIT_8("Transaction exceeds minimum"),
    BYTE_4_BIT_7("Exceeding the limit of continuous offline transactions"),
    BYTE_4_BIT_6("Exceeded the continuous offline transaction limit"),
    BYTE_4_BIT_5("Transactions are randomly selected for online processing"),
    BYTE_4_BIT_4("Merchants request online transactions"),
    BYTE_4_BIT_3("RFU"),
    BYTE_4_BIT_2("RFU"),
    BYTE_4_BIT_1("RFU"),
    BYTE_5_BIT_8("Use default TDOL"),
    BYTE_5_BIT_7("Issuer authentication failed"),
    BYTE_5_BIT_6("Script processing failed before the last time the GENERATE AC command was generated"),
    BYTE_5_BIT_5("Script processing failed after the last application password (AC) command was generated"),
    BYTE_5_BIT_4("RFU"),
    BYTE_5_BIT_3("RFU"),
    BYTE_5_BIT_2("RFU"),
    BYTE_5_BIT_1("RFU"),
    ;

    private String description;

    TvrResults(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
