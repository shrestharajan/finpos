package global.citytech.finpos.neps.iso8583.requestsender.reconciliation;

import global.citytech.finposframework.usecases.reconciliation.ReconciliationTotals;
import global.citytech.finposframework.usecases.transaction.Request;

public class ReconciliationISORequest implements Request {
    private String batchNumber;
    private ReconciliationTotals reconciliationTotals;
    private String date;
    private String currencyCode;
    private String applicationVersion;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getDate() {
        return date;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public ReconciliationTotals getReconciliationTotals() {
        return reconciliationTotals;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public static final class Builder {
        private String batchNumber;
        private ReconciliationTotals reconciliationTotals;
        private String date;
        private String currencyCode;
        private String applicationVersion;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withBatchNumber(String batchNumber) {
            this.batchNumber = batchNumber;
            return this;
        }

        public Builder withDate(String date) {
            this.date = date;
            return this;
        }

        public Builder withCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder withReconciliationTotals(ReconciliationTotals reconciliationTotals) {
            this.reconciliationTotals = reconciliationTotals;
            return this;
        }

        public Builder withApplicationVersion(String applicationVersion) {
            this.applicationVersion = applicationVersion;
            return this;
        }

        public ReconciliationISORequest build() {
            ReconciliationISORequest reconciliationISORequest = new ReconciliationISORequest();
            reconciliationISORequest.batchNumber = this.batchNumber;
            reconciliationISORequest.reconciliationTotals = this.reconciliationTotals;
            reconciliationISORequest.date = this.date;
            reconciliationISORequest.currencyCode = this.currencyCode;
            reconciliationISORequest.applicationVersion = this.applicationVersion;
            return reconciliationISORequest;
        }
    }
}
