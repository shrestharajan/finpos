package global.citytech.nepsadmin.data.response

data class Keys(
    var devicePrivateKey: List<DevicePrivateKey>? = null,
    var switchPublicKey: List<SwitchPublicKey>? = null
)