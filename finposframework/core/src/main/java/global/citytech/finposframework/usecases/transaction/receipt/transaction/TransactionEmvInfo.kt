package global.citytech.finposframework.usecases.transaction.receipt.transaction


class TransactionEmvInfo(
    val posEntryMode: String = "",
    val responseCode: String = "",
    val aid: String = "",
    val tvr: String = "",
    val tsi: String = "",
    val cvm: String = "",
    val cid: String = "",
    val ac: String = "",
    val cardSchemeLabel: String = "",
    val cardHolderName: String = "",
    val cardNumber: String = "",
    val expiryDate: String = "",
    val kernelId: String = "",
    val iin: String? = ""
) {

    override fun toString(): String {
        return "Pos Entry Mode=${posEntryMode}, Response Code=${responseCode}, AID=$aid, TVR=$tvr, TSI=$tsi, CVM=$cvm, CID=$cid, Application Cryptogram=$ac"
    }
}