package global.citytech.finpos.processor.nibl.transaction.reversal;

import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reversal.ReversalResponseParameter;

/**
 * Created by Rishav Chudal on 9/29/20.
 */
public class ReversalResponseModel implements UseCase.Response, ReversalResponseParameter {
    private String debugRequestMessage;
    private String debugResponseMessage;
    private String message;
    private String stan;
    private boolean approved;

    private ReversalResponseModel() {

    }

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private String message;
        private boolean approved;
        private String stan;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withDebugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public Builder withDebugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder withApproved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public ReversalResponseModel build() {
            ReversalResponseModel reversalResponseModel = new ReversalResponseModel();
            reversalResponseModel.debugRequestMessage = this.debugRequestMessage;
            reversalResponseModel.approved = this.approved;
            reversalResponseModel.debugResponseMessage = this.debugResponseMessage;
            reversalResponseModel.message = this.message;
            reversalResponseModel.stan = this.stan;
            return reversalResponseModel;
        }
    }
}
