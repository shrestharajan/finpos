package global.citytech.finpos.nibl.admin.data.response

data class Crypto(
    var type: String? = null,
    var values: Values? = null
)