package global.citytech.finpos.nibl.iso8583.requestsender.refund;

import global.citytech.finpos.nibl.iso8583.PosEntryModeConfig;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.switches.PosEntryMode;

/**
 * Created by Rishav Chudal on 10/16/20.
 */
public class ManualRefundRequestSender extends RefundRequestSender {

    public ManualRefundRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
    }

    @Override
    public void addConditionalFields(Iso8583Msg msg, TransactionIsoRequest refundRequest) {
        msg.addField(
                new DataElement(
                        DataElement.PRIMARY_ACCOUNT_NUMBER,
                        refundRequest.getPan()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.PAN_EXPIRE_DATE,
                        refundRequest.getExpireDate()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(PosEntryMode.MANUAL)
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.ADDITIONAL_DATA_PRIVATE,
                        refundRequest.getCvv()
                )
        );
    }
}
