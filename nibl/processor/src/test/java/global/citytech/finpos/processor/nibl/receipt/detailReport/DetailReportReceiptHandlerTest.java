package global.citytech.finpos.processor.nibl.receipt.detailReport;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportReceipt;
import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportReceiptHandler;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.utility.JsonUtils;

import static org.mockito.ArgumentMatchers.any;

/**
 * Created by Unique Shakya on 1/5/2021.
 */
@RunWith(PowerMockRunner.class)
public class DetailReportReceiptHandlerTest {

    private PrinterService printerService;

    @Before
    public void setUp() throws Exception {
        this.printerService = PowerMockito.mock(PrinterService.class);
    }

    @Test
    public void printTest() {
        String detailReportReceiptString = "{\"retailer\":{\"retailerName\":\"CityTech\",\"retailerAddress\":\"Kamaladi\",\"merchantId\":\"1122334455\",\"terminalId\":\"1234567890\"},\"performance\":{\"startDateTime\":\"210105151328172\",\"endDateTime\":\"210105151328172\"},\"reconciliationBatchNumber\":\"0001\",\"host\":\"000048\",\"detailReports\":[{\"cardNumber\":\"424942******2330\",\"cardScheme\":\"Visa Credit\",\"invoiceNumber\":\"111111\",\"approvalCode\":\"123342\",\"transactionType\":\"PURCHASE\",\"amount\":\"NPR 1500.00\"}]}";
        DetailReportReceipt detailReportReceipt = JsonUtils.fromJsonToObj(detailReportReceiptString, DetailReportReceipt.class);

        DetailReportReceiptHandler detailReportReceiptHandler = new DetailReportReceiptHandler(this.printerService);
        detailReportReceiptHandler.print(detailReportReceipt);

        PowerMockito.when(this.printerService.print(any())).thenReturn(new PrinterResponse(Result.SUCCESS, "SUCCESS"));
    }
}
