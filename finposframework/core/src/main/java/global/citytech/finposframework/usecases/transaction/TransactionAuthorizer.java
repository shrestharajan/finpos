package global.citytech.finposframework.usecases.transaction;

import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.data.TransactionAuthorizationResponse;

public interface TransactionAuthorizer {
    TransactionAuthorizationResponse authorizeUser(TransactionType transactionType, String cardScheme);
}
