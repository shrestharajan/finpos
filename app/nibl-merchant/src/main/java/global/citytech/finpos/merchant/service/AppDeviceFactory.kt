package global.citytech.finpos.merchant.service

import android.content.Context
import android.os.Build
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finposframework.hardware.DeviceFactory
import global.citytech.finposframework.hardware.DeviceServiceType
import global.citytech.finposframework.hardware.common.DeviceService
import global.citytech.finposframework.log.Logger
import java.util.*

/**
 * Created by Rishav Chudal on 4/20/20.
 */

class AppDeviceFactory {
    companion object {
        private val logger = Logger(AppDeviceFactory::class.java.name)
        private val notifier = NotificationHandler

        fun getCore(context: Context?, coreServiceType: DeviceServiceType): DeviceService {
            val manufacturer = getDeviceManufacturer()
            val className: String = "global.citytech.finpos.device."
                .plus(manufacturer.toLowerCase(Locale.ROOT))
                .plus(".Device")
                .plus(manufacturer)
                .plus("Factory")

            try {
                val clazz: Class<*> = Class.forName(className)
                val deviceFactory: DeviceFactory = clazz.newInstance() as DeviceFactory
                return deviceFactory.getDevice(context, coreServiceType, notifier)!!
            } catch (ex: Exception) {
                logger.log("corelogger ${ex.message!!}")
            }
            throw NullPointerException("Core is null")
        }

        private fun getDeviceManufacturer(): String {
            val manufacturer: String = Build.MANUFACTURER.toLowerCase(Locale.ROOT);
            logger.log("corelogger Manufacturer ::: ".plus(manufacturer))
            manufacturer.replace(" ", "")
            return manufacturer.substring(0, 1).toUpperCase(Locale.ROOT)
                .plus(manufacturer.substring(1))
        }


    }
}