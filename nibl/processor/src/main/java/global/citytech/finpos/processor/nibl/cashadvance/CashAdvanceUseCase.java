//package global.citytech.finpos.processor.nibl.cashadvance;
//
//import java.util.concurrent.CountDownLatch;
//
//import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
//import global.citytech.finpos.nibl.iso8583.ProcessingCode;
//import global.citytech.finpos.nibl.iso8583.requestsender.cashadvance.CashAdvanceRequest;
//import global.citytech.finpos.nibl.iso8583.requestsender.cashadvance.CashAdvanceRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.cashadvance.IccCashAdvanceRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.cashadvance.MagneticCashAdvanceRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.cashadvance.NfcCashAdvanceRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
//import global.citytech.finpos.processor.nibl.reversal.ReversalRequestModel;
//import global.citytech.finpos.processor.nibl.reversal.ReversalUseCase;
//import global.citytech.finpos.processor.nibl.transaction.NiblTransactionReceiptPrintHandler;
//import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
//import global.citytech.finpos.processor.nibl.transaction.ReversedTransactionReceiptHandler;
//import global.citytech.finpos.processor.nibl.transaction.TransactionUseCase;
//import global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor;
//import global.citytech.finpos.processor.nibl.transaction.card.MagneticCardProcessor;
//import global.citytech.finpos.processor.nibl.transaction.card.PICCCardProcessor;
//import global.citytech.finposframework.exceptions.FinPosException;
//import global.citytech.finposframework.exceptions.PosError;
//import global.citytech.finposframework.exceptions.PosException;
//import global.citytech.finposframework.hardware.io.cards.CardType;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
//import global.citytech.finposframework.hardware.io.led.LedAction;
//import global.citytech.finposframework.hardware.io.led.LedLight;
//import global.citytech.finposframework.hardware.io.led.LedRequest;
//import global.citytech.finposframework.iso8583.DataElement;
//import global.citytech.finposframework.iso8583.IsoMessageResponse;
//import global.citytech.finposframework.iso8583.RequestContext;
//import global.citytech.finposframework.listeners.CardConfirmationListener;
//import global.citytech.finposframework.notifier.Notifier;
//import global.citytech.finposframework.repositories.ApplicationRepository;
//import global.citytech.finposframework.repositories.TerminalRepository;
//import global.citytech.finposframework.supports.UseCase;
//import global.citytech.finposframework.switches.cashadvance.CashAdvanceRequester;
//import global.citytech.finposframework.usecases.MagneticSwipe;
//import global.citytech.finposframework.usecases.PICCWave;
//import global.citytech.finposframework.usecases.TransactionType;
//import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
//import global.citytech.finposframework.usecases.transaction.TransactionLog;
//import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
//import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
//import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
//import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
//import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
//import global.citytech.finposframework.utility.HelperUtils;
//import global.citytech.finposframework.utility.IsoMessageUtils;
//import global.citytech.finposframework.utility.JsonUtils;
//import global.citytech.finposframework.utility.StringUtils;
//
///**
// * Created by Unique Shakya on 2/4/2021.
// */
//public class CashAdvanceUseCase extends TransactionUseCase implements CashAdvanceRequester<CashAdvanceRequestModel, CashAdvanceResponseModel>,
//        UseCase<CashAdvanceRequestModel, CashAdvanceResponseModel> {
//    static CountDownLatch countDownLatch;
//    private CashAdvanceRequestModel cashAdvanceRequestModel;
//    ReadCardResponse readCardResponse;
//    String stan;
//    String invoiceNumber;
//    TransactionRequest transactionRequest;
//    String batchNumber;
//    boolean isCardConfirmationRequired;
//    private global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest;
//    private CashAdvanceRequest isoCashAdvanceRequest;
//    private ApplicationRepository applicationRepository;
//
//    public CashAdvanceUseCase(TerminalRepository terminalRepository, Notifier notifier) {
//        super(terminalRepository, notifier);
//    }
//
//    @Override
//    public CashAdvanceResponseModel execute(CashAdvanceRequestModel cashAdvanceRequestModel) {
//        logger.log("::: REACHED CASH ADVANCE USE CASE :::");
//        this.cashAdvanceRequestModel = cashAdvanceRequestModel;
//        try {
//            if (!this.isDeviceReady(this.cashAdvanceRequestModel.getDeviceController(),
//                    this.cashAdvanceRequestModel.getTransactionRequest().getTransactionType()))
//                throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
//            stan = this.terminalRepository.getSystemTraceAuditNumber();
//            invoiceNumber = this.terminalRepository.getInvoiceNumber();
//            batchNumber = this.terminalRepository.getReconciliationBatchNumber();
//            transactionRequest = cashAdvanceRequestModel.getTransactionRequest();
//            applicationRepository = cashAdvanceRequestModel.getApplicationRepository();
//            this.transactionRepository = cashAdvanceRequestModel.getTransactionRepository();
//            this.initTransaction(this.cashAdvanceRequestModel.getApplicationRepository(),
//                    this.cashAdvanceRequestModel.getTransactionRequest());
//            ReadCardRequest readCardRequest = prepareReadCardRequest(stan, this.cashAdvanceRequestModel.getApplicationRepository(),
//                    this.cashAdvanceRequestModel.getTransactionRequest());
//            return this.initializeTransactionWithCard(stan, batchNumber, invoiceNumber, readCardRequest);
//        } catch (PosException pe) {
//            pe.printStackTrace();
//            throw pe;
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        } finally {
//            this.cashAdvanceRequestModel.getReadCardService().cleanUp();
//            this.cashAdvanceRequestModel.getLedService().doTurnLedWith(new LedRequest(LedLight.ALL, LedAction.OFF));
//            PICCWave.getInstance().clear();
//            MagneticSwipe.getInstance().clear();
//            this.cashAdvanceRequestModel = null;
//        }
//    }
//
//    private CashAdvanceResponseModel initializeTransactionWithCard(String stan, String batchNumber,
//                                                                   String invoiceNumber, ReadCardRequest readCardRequest) throws InterruptedException {
//        this.notifier.notify(Notifier.EventType.STARTING_READ_CARD);
//        ReadCardResponse readCardResponse = this.detectCard(readCardRequest, this.cashAdvanceRequestModel.getReadCardService(),
//                this.cashAdvanceRequestModel.getLedService());
//        this.notifier.notify(Notifier.EventType.READING_CARD, "Reading Card..");
//        this.checkForCardType(readCardResponse);
//        readCardResponse = this.cardProcessor.validateReadCardResponse(readCardRequest, readCardResponse);
//        this.checkForCardType(readCardResponse);
//        if (!this.isValidTransaction(this.cashAdvanceRequestModel.getApplicationRepository(),
//                this.cashAdvanceRequestModel.getTransactionRequest(),
//                readCardResponse))
//            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
//        if (!this.isTransactionAuthorizationSuccess(this.cashAdvanceRequestModel.getTransactionAuthenticator(),
//                this.cashAdvanceRequestModel.getApplicationRepository(),
//                this.cashAdvanceRequestModel.getTransactionRequest().getTransactionType(),
//                readCardResponse))
//            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
//        purchaseRequest = this.preparePurchaseRequest(readCardResponse);
//        if (isCardConfirmationRequired) {
//            final boolean[] isConfirm = {false};
//            countDownLatch = new CountDownLatch(1);
//            this.notifier.notify(Notifier.EventType.CARD_CONFIRMATION_EVENT,
//                    readCardResponse.getCardDetails(),
//                    (CardConfirmationListener) confirmed -> {
//                        isConfirm[0] = confirmed;
//                        countDownLatch.countDown();
//                    });
//            countDownLatch.await();
//            if (!isConfirm[0]) {
//                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
//            }
//        }
//        return processCardAndProceed();
//
//    }
//
//    private CashAdvanceResponseModel processCardAndProceed() throws InterruptedException {
//        readCardResponse = this.cardProcessor.processCard(stan, purchaseRequest);
//        return this.performTransactionWithHost(stan, invoiceNumber, batchNumber, readCardResponse);
//    }
//
//    private CashAdvanceResponseModel performTransactionWithHost(String stan, String invoiceNumber,
//                                                                String batchNumber, ReadCardResponse readCardResponse) throws InterruptedException {
//        this.notifier.notify(Notifier.EventType.TRANSMITTING_REQUEST, "Transmitting Request..");
//        try {
//            IsoMessageResponse isoMessageResponse = this.sendIsoMessage(stan, invoiceNumber, readCardResponse);
//            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait..");
//            InvalidResponseHandlerResponse invalidResponseHandlerResponse = this.checkForInvalidResponseFromHost(isoMessageResponse);
//            if (invalidResponseHandlerResponse.isInvalid())
//                return this.handleInvalidResponseFromHost(invalidResponseHandlerResponse,
//                        batchNumber, stan, invoiceNumber, readCardResponse);
//            boolean isCashAdvanceApproved = this.isTransactionApprovedByActionCode(isoMessageResponse);
//            this.notifier.notify(Notifier.EventType.PROCESSING_ISSUER_SCRIPT, "Processing..");
//            readCardResponse = this.cardProcessor.processOnlineResult(readCardResponse, isoMessageResponse);
//            if (this.isTransactionDeclineByCardEvenIfSuccessOnSwitch(isoMessageResponse, readCardResponse))
//                return this.handleApprovedTransactionDeclinedByCard(stan, batchNumber, invoiceNumber, readCardResponse);
//            if (this.isForceContactCardPromptActionCode(readCardResponse, isoMessageResponse))
//                return this.handleForceContactPromptFromHost();
//            this.notifyTransactionCompletion(batchNumber, invoiceNumber, isCashAdvanceApproved,
//                    isoMessageResponse, readCardResponse, stan);
//            boolean shouldPrint = this.printReceipt(batchNumber, stan, invoiceNumber, this.cashAdvanceRequestModel.getPrinterService(),
//                    this.cashAdvanceRequestModel.getTransactionRequest(), this.cashAdvanceRequestModel.getTransactionRepository(),
//                    readCardResponse, isoMessageResponse, this.cashAdvanceRequestModel.getApplicationRepository());
//            return this.prepareCashAdvanceResponse(stan, isoMessageResponse, isCashAdvanceApproved, shouldPrint);
//        } catch (FinPosException fe) {
//            fe.printStackTrace();
//            if (this.isResponseNotReceivedException(fe)) {
//                return this.handleResponseNotReceivedTransaction(batchNumber, stan, invoiceNumber,
//                        readCardResponse);
//            } else if (fe.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT) {
//                logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED UNABLE TO CONNECT");
//                transactionRepository.removeAutoReversal(this.autoReversal);
//                throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
//            } else {
//                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//            }
//        }
//    }
//
//    private CashAdvanceResponseModel handleResponseNotReceivedTransaction(String batchNumber, String stan,
//                                                                          String invoiceNumber,
//                                                                          ReadCardResponse readCardResponse) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, "TIMEOUT");
//        this.setUnableToGoOnlineResultToCard(readCardResponse, this.cashAdvanceRequestModel.getReadCardService());
//        this.performReversal(stan, readCardResponse);
//        boolean shouldPrint = !this.approvePrintOnly(this.cashAdvanceRequestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(this.cashAdvanceRequestModel.getTransactionRequest(),
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.TIMEOUT, "Timeout"));
//            new NiblTransactionReceiptPrintHandler(this.cashAdvanceRequestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            this.cashAdvanceRequestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return CashAdvanceResponseModel.Builder.newInstance()
//                .message("TIMEOUT")
//                .isApproved(false)
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private CashAdvanceResponseModel prepareCashAdvanceResponse(String stan, IsoMessageResponse isoMessageResponse,
//                                                                boolean isCashAdvanceApproved,
//                                                                boolean shouldPrint) {
//        return CashAdvanceResponseModel.Builder.newInstance()
//                .isApproved(isCashAdvanceApproved)
//                .stan(stan)
//                .message(this.retrieveMessage(isoMessageResponse, isCashAdvanceApproved, this.cashAdvanceRequestModel.getApplicationRepository()))
//                .debugRequestMessage(isoMessageResponse.getDebugRequestString())
//                .debugResponseMessage(isoMessageResponse.getDebugResponseString())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private void notifyTransactionCompletion(String batchNumber, String invoiceNumber,
//                                             boolean isCashAdvanceApproved,
//                                             IsoMessageResponse isoMessageResponse,
//                                             ReadCardResponse readCardResponse, String stan) {
//        this.updateTransactionLog(stan, batchNumber, invoiceNumber, isCashAdvanceApproved, isoMessageResponse, readCardResponse);
//        this.updateTransactionIds(isCashAdvanceApproved);
//        this.notifyTransactionStatus(isCashAdvanceApproved, isoMessageResponse,
//                this.cashAdvanceRequestModel.getApplicationRepository(),
//                this.cashAdvanceRequestModel.getLedService(),
//                readCardResponse.getCardDetails().getCardType() == CardType.PICC);
//    }
//
//    private void updateTransactionLog(String stan, String batchNumber, String invoiceNumber,
//                                      boolean isCashAdvanceApproved, IsoMessageResponse isoMessageResponse,
//                                      ReadCardResponse readCardResponse) {
//        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
//                .withStan(stan)
//                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
//                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
//                .withInvoiceNumber(invoiceNumber)
//                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
//                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
//                .withTransactionType(TransactionType.CASH_ADVANCE)
//                .withOriginalTransactionType(TransactionType.CASH_ADVANCE)
//                .withTransactionAmount(this.cashAdvanceRequestModel.getTransactionRequest().getAmount())
//                .withOriginalTransactionAmount(this.cashAdvanceRequestModel.getTransactionRequest().getAmount())
//                .withTransactionDate(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_DATE))
//                .withTransactionTime(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_TIME))
//                .withTransactionStatus(isCashAdvanceApproved ? "APPROVED" : "DECLINED")
//                .withPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
//                .withOriginalPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
//                .withPosConditionCode(this.isoCashAdvanceRequest.getPosConditionCode())
//                .withOriginalPosConditionCode("")
//                .withReconcileStatus("")
//                .withReconcileTime("")
//                .withReconcileDate("")
//                .withReconcileBatchNo(batchNumber)
//                .withReadCardResponse(readCardResponse)
//                .withAuthorizationCompleted(true)
//                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
//                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse,
//                        applicationRepository, batchNumber, stan, invoiceNumber))
//                .withTransactionVoided(false)
//                .withTipAdjusted(false)
//                .build();
//        this.cashAdvanceRequestModel.getTransactionRepository().updateTransactionLog(transactionLog);
//    }
//
//    private CashAdvanceResponseModel handleForceContactPromptFromHost() throws InterruptedException {
//        this.updateTransactionIds(false);
//        this.cashAdvanceRequestModel.getReadCardService().cleanUp();
//        String stan = this.terminalRepository.getSystemTraceAuditNumber();
//        String invoiceNumber = this.terminalRepository.getInvoiceNumber();
//        String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
//        ReadCardRequest forceContactCardReadCardRequest = this.prepareForceContactCardReadCardRequest(stan,
//                this.cashAdvanceRequestModel.getApplicationRepository(),
//                this.cashAdvanceRequestModel.getTransactionRequest());
//        return this.initializeTransactionWithCard(stan, batchNumber, invoiceNumber, forceContactCardReadCardRequest);
//    }
//
//    private CashAdvanceResponseModel handleApprovedTransactionDeclinedByCard(String stan, String batchNumber,
//                                                                             String invoiceNumber,
//                                                                             ReadCardResponse readCardResponse) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, ReasonForReversal.Type.DECLINED_BY_CARD.getDescription());
//        this.performReversal(stan, readCardResponse);
//        boolean shouldPrint = !this.approvePrintOnly(this.cashAdvanceRequestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(this.cashAdvanceRequestModel.getTransactionRequest(),
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.DECLINED_BY_CARD,
//                            ReasonForReversal.Type.DECLINED_BY_CARD.getDescription()));
//            new NiblTransactionReceiptPrintHandler(this.cashAdvanceRequestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            this.cashAdvanceRequestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return CashAdvanceResponseModel.Builder.newInstance()
//                .isApproved(false)
//                .message(ReasonForReversal.Type.DECLINED_BY_CARD.getDescription())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private CashAdvanceResponseModel handleInvalidResponseFromHost(InvalidResponseHandlerResponse invalidResponseHandlerResponse,
//                                                                   String batchNumber, String stan,
//                                                                   String invoiceNumber,
//                                                                   ReadCardResponse readCardResponse) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, invalidResponseHandlerResponse.getMessage());
//        this.performReversal(stan, readCardResponse);
//        boolean shouldPrint = !this.approvePrintOnly(this.cashAdvanceRequestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(this.cashAdvanceRequestModel.getTransactionRequest(),
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.INVALID_RESPONSE,
//                            invalidResponseHandlerResponse.getMessage()));
//            new NiblTransactionReceiptPrintHandler(this.cashAdvanceRequestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            this.cashAdvanceRequestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return CashAdvanceResponseModel.Builder.newInstance()
//                .isApproved(false)
//                .message(invalidResponseHandlerResponse.getMessage())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private void performReversal(String stan, ReadCardResponse readCardResponse) {
//        ReversalUseCase reversalUseCase = new ReversalUseCase(this.prepareReversalRequest(stan, readCardResponse),
//                this.terminalRepository, this.notifier);
//        try {
//            reversalUseCase.execute(prepareReversalRequestModel(readCardResponse));
//            this.transactionRepository.removeAutoReversal(this.autoReversal);
//        } catch (FinPosException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private ReversalRequestModel prepareReversalRequestModel(ReadCardResponse readCardResponse) {
//        return ReversalRequestModel.Builder.newInstance()
//                .withTransactionRequest(this.cashAdvanceRequestModel.getTransactionRequest())
//                .withTransactionRepository(this.cashAdvanceRequestModel.getTransactionRepository())
//                .withPrinterService(this.cashAdvanceRequestModel.getPrinterService())
//                .withApplicationRepository(this.cashAdvanceRequestModel.getApplicationRepository())
//                .withReadCardResponse(readCardResponse)
//                .build();
//    }
//
//    private NiblReversalRequest prepareReversalRequest(String stan, ReadCardResponse readCardResponse) {
//        NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
//        niblReversalRequest.setProcessingCode(ProcessingCode.PURCHASE.getCode());
//        niblReversalRequest.setAmount(this.cashAdvanceRequestModel.getTransactionRequest().getAmount());
//        niblReversalRequest.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        niblReversalRequest.setExpiryDate(readCardResponse.getCardDetails().getExpiryDate());
//        niblReversalRequest.setPosEntryMode(this.isoCashAdvanceRequest.getEntryMode());
//        niblReversalRequest.setPosConditionCode(this.isoCashAdvanceRequest.getPosConditionCode());
//        niblReversalRequest.setTrack2Data(this.isoCashAdvanceRequest.getTrack2Data());
//        niblReversalRequest.setPinBlock(this.isoCashAdvanceRequest.getPinBlock());
//        niblReversalRequest.setInvoiceNumber(this.isoCashAdvanceRequest.getInvoiceNumber());
//        niblReversalRequest.setStan(stan);
//        return niblReversalRequest;
//    }
//
//    private IsoMessageResponse sendIsoMessage(String stan, String invoiceNumber, ReadCardResponse readCardResponse) {
//        RequestContext context = this.prepareTemplateRequestContext(stan);
//        this.isoCashAdvanceRequest = prepareIsoCashAdvanceRequest(invoiceNumber, readCardResponse);
//        context.setRequest(isoCashAdvanceRequest);
//        this.autoReversal = new AutoReversal(
//                TransactionType.CASH_ADVANCE,
//                JsonUtils.toJsonObj(prepareReversalRequest(stan, readCardResponse)),
//                0,
//                AutoReversal.Status.ACTIVE,
//                StringUtils.dateTimeStamp()
//        );
//        this.transactionRepository.saveAutoReversal(this.autoReversal);
//        CashAdvanceRequestSender sender;
//        switch (readCardResponse.getCardDetails().getCardType()) {
//            case MAG:
//                sender = new MagneticCashAdvanceRequestSender(new NIBLSpecInfo(), context);
//                break;
//            case ICC:
//            default:
//                sender = new IccCashAdvanceRequestSender(new NIBLSpecInfo(), context);
//                break;
//            case PICC:
//                sender = new NfcCashAdvanceRequestSender(new NIBLSpecInfo(), context);
//                break;
//        }
//        IsoMessageResponse isoMessageResponse = sender.send();
//        this.transactionRepository.removeAutoReversal(this.autoReversal);
//        return isoMessageResponse;
//    }
//
//    private CashAdvanceRequest prepareIsoCashAdvanceRequest(String invoiceNumber, ReadCardResponse readCardResponse) {
//        return CashAdvanceRequest.Builder.newInstance()
//                .withPan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
//                .withTransactionAmount(readCardResponse.getCardDetails().getAmount())
//                .withTrack2Data(readCardResponse.getCardDetails().getTrackTwoData())
//                .withEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
//                .withPosConditionCode("00")
//                .withPinBlock(this.retrievePinBlock(readCardResponse))
//                .withEmvData(readCardResponse.getCardDetails().getIccDataBlock())
//                .withExpireDate(readCardResponse.getCardDetails().getExpiryDate())
//                .withCardSequenceNumber(readCardResponse.getCardDetails().getPrimaryAccountNumberSerialNumber())
//                .withLocalDate(HelperUtils.getDefaultLocaleDateWithoutYear())
//                .withLocalTime(HelperUtils.getDefaultLocaleTimeHhMmSs())
//                .withInvoiceNumber(invoiceNumber)
//                .withFallbackFromIccToMag(readCardResponse.getFallbackIccToMag())
//                .build();
//    }
//
//    private String retrievePinBlock(ReadCardResponse readCardResponse) {
//        String pin = readCardResponse.getCardDetails().getPinBlock().getPin();
//        if (readCardResponse.getCardDetails().getPinBlock().getOfflinePin() ||
//                StringUtils.isEmpty(pin) || pin.equalsIgnoreCase("offline"))
//            return "";
//        else
//            return pin;
//    }
//
//    private PurchaseRequest preparePurchaseRequest(ReadCardResponse readCardResponse) {
//        PurchaseRequest purchaseRequest = new PurchaseRequest(
//                this.cashAdvanceRequestModel.getTransactionRequest().getTransactionType(),
//                this.cashAdvanceRequestModel.getTransactionRequest().getAmount(),
//                this.cashAdvanceRequestModel.getTransactionRequest().getAdditionalAmount());
//        readCardResponse.getCardDetails().setAmount(this.cashAdvanceRequestModel.getTransactionRequest().getAmount());
//        readCardResponse.getCardDetails().setCashBackAmount(this.cashAdvanceRequestModel.getTransactionRequest().getAdditionalAmount());
//        purchaseRequest.setCardDetails(readCardResponse.getCardDetails());
//        purchaseRequest.setFallbackIccToMag(readCardResponse.getFallbackIccToMag());
//        return purchaseRequest;
//    }
//
//    private void checkForCardType(ReadCardResponse readCardResponse) {
//        switch (readCardResponse.getCardDetails().getCardType()) {
//            case MAG:
//                isCardConfirmationRequired = true;
//                this.cardProcessor = new MagneticCardProcessor(
//                        this.cashAdvanceRequestModel.getTransactionRepository(),
//                        this.cashAdvanceRequestModel.getTransactionAuthenticator(),
//                        this.cashAdvanceRequestModel.getApplicationRepository(),
//                        this.cashAdvanceRequestModel.getReadCardService(),
//                        this.notifier,
//                        this.terminalRepository);
//                break;
//            case ICC:
//                isCardConfirmationRequired = true;
//                this.cardProcessor = new IccCardProcessor(this.cashAdvanceRequestModel.getReadCardService(),
//                        this.cashAdvanceRequestModel.getTransactionRepository(),
//                        this.cashAdvanceRequestModel.getApplicationRepository(),
//                        this.cashAdvanceRequestModel.getTransactionAuthenticator(),
//                        this.notifier,
//                        this.terminalRepository);
//                break;
//            case PICC:
//                isCardConfirmationRequired = false;
//                this.cardProcessor = new PICCCardProcessor(this.cashAdvanceRequestModel.getTransactionRepository(),
//                        this.cashAdvanceRequestModel.getApplicationRepository(),
//                        this.cashAdvanceRequestModel.getReadCardService(),
//                        this.notifier,
//                        this.cashAdvanceRequestModel.getTransactionAuthenticator(),
//                        this.cashAdvanceRequestModel.getLedService(),
//                        this.cashAdvanceRequestModel.getSoundService(),
//                        this.terminalRepository);
//                break;
//            default:
//                isCardConfirmationRequired = false;
//                break;
//        }
//    }
//
//    @Override
//    protected String getClassName() {
//        return CashAdvanceUseCase.class.getSimpleName();
//    }
//}
