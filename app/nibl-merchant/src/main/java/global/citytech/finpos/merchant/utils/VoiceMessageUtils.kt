package global.citytech.finpos.merchant.utils


import android.content.Context;
import android.media.MediaPlayer;
import global.citytech.finpos.merchant.R

class VoiceMessageUtils {

    companion object{
        fun  playSuccess(context:Context) {
            play(context, R.raw.np_success)
        }

        fun playFail(context:Context) {
            play(context, R.raw.fail_message)
        }

        fun play(context:Context, resId:Int)
        {
            val mp3 = MediaPlayer.create(context.applicationContext, resId)
            MediaPlayerRegistry.mList.add(mp3)
            mp3.start();
        }
    }
}
