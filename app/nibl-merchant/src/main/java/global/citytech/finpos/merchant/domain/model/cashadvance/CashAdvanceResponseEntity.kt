package global.citytech.finpos.merchant.domain.model.cashadvance


/**
 * Created by Saurav Ghimire on 1/20/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
data class CashAdvanceResponseEntity(
    val stan:String? = null,
    val message: String? = null,
    val isApproved: Boolean? = false,
    val shouldPrintCustomerCopy: Boolean? = false

)