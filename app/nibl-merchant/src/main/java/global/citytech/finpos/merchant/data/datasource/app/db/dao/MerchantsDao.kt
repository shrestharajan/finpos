package global.citytech.finpos.merchant.data.datasource.app.db.dao

import androidx.room.*
import global.citytech.finpos.merchant.domain.model.app.Merchant


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
@Dao
interface MerchantsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(merchants: List<Merchant>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(merchant: Merchant): Long

    @Query("SELECT id, merchant_name, merchant_address, merchant_phone, merchant_switch_id, merchant_terminal_id FROM merchants")
    fun getMerchantList(): List<Merchant>

    @Update
    fun update(merchant: Merchant):Int

    @Query("DELETE FROM merchants WHERE id = :id")
    fun deleteById(id: Long): Int

    @Query("DELETE FROM merchants")
    fun nukeTableData()

}