package global.citytech.neps.datasource.model

import android.content.ContentValues
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import global.citytech.neps.domain.model.Host
import global.citytech.neps.domain.model.Merchant

data class MerchantEntity(
    var id: String,
    var name: String? = null,
    var address: String? = null,
    var phoneNumber: String? = null,
    var switchId: String? = null
)

fun MerchantEntity.mapToDomain(): Merchant = Merchant(id,name,address,phoneNumber,switchId)
fun List<MerchantEntity>.mapToDomain(): List<Merchant> = map { it.mapToDomain() }
