package global.citytech.neps.presentation.utils.customviews

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import global.citytech.neps.R

class CustomMenuButton : LinearLayout {
    var ivIcon: ImageView? = null
    var tvMenuTitle: TextView? = null
    var llMenu: LinearLayout? = null

    constructor(context: Context?) : super(context) {
        initView(null)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
        context,
        attrs
    ) {
        initView(attrs)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        initView(attrs)
    }

    private fun initView(atts: AttributeSet?) {
        val view =
            View.inflate(context, R.layout.custom_menu_button, this)
        ivIcon = view.findViewById(R.id.iv_icon)
        tvMenuTitle = view.findViewById(R.id.tv_menu_title)
        llMenu = view.findViewById(R.id.ll_menu)
        llMenu!!.setLayoutParams(
            LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        )
        if (atts != null) {
            val typedArray =
                context.obtainStyledAttributes(atts, R.styleable.CustomMenuButton)
            val menuIcon = typedArray.getResourceId(R.styleable.CustomMenuButton_image, 0)
            val menuTitle =
                typedArray.getString(R.styleable.CustomMenuButton_title)
            val contentPadding = typedArray.getDimension(
                R.styleable.CustomMenuButton_contentPadding,
                0f
            ).toInt()
            ivIcon!!.setImageResource(menuIcon)
            tvMenuTitle!!.setText(menuTitle)
            llMenu!!.setPadding(0, contentPadding, 0, contentPadding)
        }
    }

    override fun onDetachedFromWindow() {
        Log.d("memory", "onDetachedFromWindow: ")
        ivIcon = null
        tvMenuTitle = null
        llMenu = null
        super.onDetachedFromWindow()
    }
}