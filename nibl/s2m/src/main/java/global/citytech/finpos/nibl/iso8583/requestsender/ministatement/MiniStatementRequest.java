package global.citytech.finpos.nibl.iso8583.requestsender.ministatement;

import java.math.BigDecimal;

import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.greenpin.PinSetRequest;
import global.citytech.finposframework.switches.PosEntryMode;

public class MiniStatementRequest extends TransactionIsoRequest {
    private String pan;
    private BigDecimal transactionAmount = BigDecimal.ZERO;
    private String localTime;
    private String localDate;
    private String expireDate;
    private PosEntryMode posEntryMode;
    private String posConditionCode;
    private String track2Data;
    private String additionalData;
    private String currencyCode;
    private String pinBlock;
    private String emvData;
    private String retrievalReferenceNumber;
    private String cardSequenceNumber;

    public String getPan() {
        return pan;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }


    public String getLocalTime() {
        return localTime;
    }

    public String getLocalDate() {
        return localDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public String getTrack2Data() {
        return track2Data;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getPinBlock() {
        return pinBlock;
    }

    public String getEmvData() {
        return emvData;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }
    public String getCardSequenceNumber() {
        return cardSequenceNumber;
    }

    public static final class Builder {
        private String pan;
        private BigDecimal transactionAmount = BigDecimal.ZERO;
        private String localTime;
        private String localDate;
        private String expireDate;
        private PosEntryMode posEntryMode;
        private String posConditionCode;
        private String track2Data;
        private String additionalData;
        private String currencyCode;
        private String pinBlock;
        private String otp;
        private String emvData;
        private String retrievalReferenceNumber;
        private String cardSequenceNumber;

        private Builder() {
        }

        public static MiniStatementRequest.Builder newInstance() {
            return new MiniStatementRequest.Builder();
        }


        public MiniStatementRequest.Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public MiniStatementRequest.Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public MiniStatementRequest.Builder localTime(String localTime) {
            this.localTime = localTime;
            return this;
        }

        public MiniStatementRequest.Builder localDate(String localDate) {
            this.localDate = localDate;
            return this;
        }

        public MiniStatementRequest.Builder expireDate(String expiryDate) {
            this.expireDate = expiryDate;
            return this;
        }


        public MiniStatementRequest.Builder posEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public MiniStatementRequest.Builder posConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public MiniStatementRequest.Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public MiniStatementRequest.Builder additionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public MiniStatementRequest.Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public MiniStatementRequest.Builder pinBlock(String pinBlock) {
            this.pinBlock = pinBlock;
            return this;
        }

        public MiniStatementRequest.Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public MiniStatementRequest.Builder rrn(String rrn) {
            this.retrievalReferenceNumber = rrn;
            return this;
        }

        public MiniStatementRequest.Builder cardSequenceNumber(String cardSequenceNumber) {
            this.cardSequenceNumber = cardSequenceNumber;
            return this;
        }



        public MiniStatementRequest build() {
            MiniStatementRequest miniStatementRequest = new MiniStatementRequest();
            miniStatementRequest.pan = this.pan;
            miniStatementRequest.transactionAmount = this.transactionAmount;
            miniStatementRequest.localDate = this.localDate;
            miniStatementRequest.localTime = this.localTime;
            miniStatementRequest.expireDate = this.expireDate;
            miniStatementRequest.posEntryMode = this.posEntryMode;
            miniStatementRequest.posConditionCode = this.posConditionCode;
            miniStatementRequest.track2Data = this.track2Data;
            miniStatementRequest.additionalData = this.additionalData;
            miniStatementRequest.currencyCode = this.currencyCode;
            miniStatementRequest.pinBlock = this.pinBlock;
            miniStatementRequest.emvData = this.emvData;
            miniStatementRequest.retrievalReferenceNumber = this.retrievalReferenceNumber;
            miniStatementRequest.cardSequenceNumber = this.cardSequenceNumber;

            return miniStatementRequest;
        }
    }
}
