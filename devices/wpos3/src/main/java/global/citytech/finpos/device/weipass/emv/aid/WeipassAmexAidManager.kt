package global.citytech.finpos.device.weipass.emv.aid

import com.wiseasy.emvprocess.KernelInit
import com.wiseasy.emvprocess.bean.AIDData
import com.wiseasy.emvprocess.bean.BaseAIDData
import com.wiseasy.emvprocess.bean.CLAMEXAIDData
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finpos.device.weipass.utils.defaultOrEmptyValue
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import sdk4.wangpos.libemvbinder.utils.MoneyUtil

/**
 * Created by Rishav Chudal on 5/4/20.
 */
class WeipassAmexAidManager constructor(val baseAIDData: BaseAIDData,
                                        val aidParameters: global.citytech.finposframework.hardware.emv.aid.AidParameters,
                                        val emvParametersRequest: EmvParametersRequest
) {
    private var finalResult = 0

    fun injectAmexAidConfigurations(): Int {
        var successCount = 0
        injectAmexAidConfigurationForTransTypePurchase()
        if (finalResult == 0) {
            successCount ++
        }
        return successCount
    }

    private fun injectAmexAidConfigurationForTransTypePurchase(){
        val sdkAIDData = AIDData()
        sdkAIDData.baseAIDData = baseAIDData
        sdkAIDData.clAIDData = prepareCLAMEXAIDDataForPurchase()
        injectAidInCore(sdkAIDData)
    }

    private fun prepareCLAMEXAIDDataForPurchase(): CLAMEXAIDData {
        WeipassLogger.debug("===== CLAMEXAIDData ::: Contactless =====")
        val clAmexaidData = CLAMEXAIDData()

        val merchantName = emvParametersRequest.merchantName.defaultOrEmptyValue()
        clAmexaidData.mercNameORLocation_9F4E = merchantName
        WeipassLogger.debug("Merchant Name ::: ".plus(merchantName))

        var clFloorLimit = aidParameters.contactlessFloorLimit.defaultOrEmptyValue()
        clFloorLimit = MoneyUtil.toCent(clFloorLimit)
        clAmexaidData.clFloorLimit = clFloorLimit
        WeipassLogger.debug("CL Floor Limit ::: ".plus(clFloorLimit))

        var clTransLimit = aidParameters.contactlessTransactionLimit.defaultOrEmptyValue()
        clTransLimit = MoneyUtil.toCent(clTransLimit)
        clAmexaidData.clTransLimit = clTransLimit
        WeipassLogger.debug("CL Transaction Limit ::: ".plus(clTransLimit))

        var cvmLimit = aidParameters.cvmLimit.defaultOrEmptyValue()
        cvmLimit = MoneyUtil.toCent(cvmLimit)
        clAmexaidData.clcvmLimit = cvmLimit
        WeipassLogger.debug("CL Cvm Limit ::: ".plus(cvmLimit))

        val ttq = aidParameters.ttq.defaultOrEmptyValue()
        clAmexaidData.ttQ_9F66 = ttq
        WeipassLogger.debug("TTQ ::: ".plus(ttq))

        val transType9C = "00"
        clAmexaidData.transType_9C = transType9C
        WeipassLogger.debug("Trans Type ::: ".plus(transType9C))

        return clAmexaidData
    }

    private fun injectAidInCore(aidData: AIDData) {
        WeipassLogger.log("Injecting the Amex AID ...")
        val result = KernelInit.addAID(aidData)
        WeipassLogger.log("AID ".plus(aidParameters.aid).plus(" Injection Result ::: ".plus(result)))
        this.finalResult += result
    }
}