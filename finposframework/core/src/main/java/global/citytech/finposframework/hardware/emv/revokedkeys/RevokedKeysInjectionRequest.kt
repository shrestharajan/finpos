package global.citytech.finposframework.hardware.emv.revokedkeys

/**
 * Created by Rishav Chudal on 4/30/20.
 */
class RevokedKeysInjectionRequest(val revokedKeysList: List<RevokedKey>)