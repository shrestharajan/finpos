package global.citytech.finposframework.comm;

/** User: Surajchhetry Date: 2/20/20 Time: 7:15 PM */
public class Response {
  private final byte[] data;

  public Response(byte[] data) {
    this.data = data;
  }

  public byte[] getData() {
    return data;
  }

  public int getLength() {
    return this.data == null ? -1 : this.data.length;
  }
}
