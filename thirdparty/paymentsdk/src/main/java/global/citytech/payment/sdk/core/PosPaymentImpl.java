package global.citytech.payment.sdk.core;

import static global.citytech.payment.sdk.api.PaymentResult.CANCELLATION_INVALID_PAYMENT_REQUEST_NUMBER;
import static global.citytech.payment.sdk.api.PaymentResult.CANCELLATION_INVALID_REQUESTER;
import static global.citytech.payment.sdk.api.PaymentResult.INVALID_APPLICATION_IDENTIFIER;
import static global.citytech.payment.sdk.api.PaymentResult.INVALID_DATA;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;

import java.util.concurrent.CountDownLatch;
import java.util.logging.Logger;

import global.citytech.payment.sdk.api.PaymentCancellationRequest;
import global.citytech.payment.sdk.api.PaymentResponse;
import global.citytech.payment.sdk.api.PaymentResult;
import global.citytech.payment.sdk.api.PosPaymentConfiguration;
import global.citytech.payment.sdk.api.PurchasePaymentRequest;
import global.citytech.payment.sdk.api.VoidPaymentRequest;
import global.citytech.payment.sdk.utils.JsonUtils;

/**
 * Created by BikashShrestha on 6/5/19.
 * Modified by RishavChudal on 06/12/21.
 */
public final class PosPaymentImpl {

    private static CountDownLatch countDownLatch;
    private static PaymentResponse paymentResponse;
    private final Context context;
    private final PosPaymentConfiguration posPaymentConfiguration;
    private final Logger logger = Logger.getLogger(PosPaymentImpl.class.getSimpleName());
    private PaymentRequest paymentRequest;
    public static final String ACTION_CANCEL_PAYMENT_REQUEST = "global.citytech.payment.sdk.cancel.payment.request";
    public static final String EXTRAS_PAYMENT_CANCELLATION_REQUEST = "EXTRAS_PAYMENT_CANCELLATION_REQUEST";

    public PosPaymentImpl(Context context, PosPaymentConfiguration posPaymentConfiguration) {
        this.context = context;
        this.posPaymentConfiguration = posPaymentConfiguration;
    }

    private static void releaseCountdownLatch() {
        countDownLatch.countDown();
    }

    public void initialize() {
        initializePaymentServiceComponents();
    }

    private void initializePaymentServiceComponents() {
        paymentRequest = null;
        paymentResponse = new PaymentResponse(PaymentResult.FAILED);
    }

    public boolean validApplicationIdentifier() {
        return context.getPackageName().equalsIgnoreCase(
                posPaymentConfiguration.getApplicationIdentifier());
    }

    public PaymentResponse doPurchaseOnValidPaymentConfiguration(PurchasePaymentRequest request) {
        if (!validatePurchasePaymentRequest(request))
            return paymentResponse;
        if (validApplicationIdentifier()) {
            doPurchase(request);
        } else {
            paymentResponse = new PaymentResponse(INVALID_APPLICATION_IDENTIFIER);
        }
        return paymentResponse;
    }

    private boolean validatePurchasePaymentRequest(PurchasePaymentRequest purchasePaymentRequest) {
        if (purchasePaymentRequest == null) {
            paymentResponse = new PaymentResponse(INVALID_DATA);
            return false;
        }

        if (purchasePaymentRequest.getBillingAmount() <= 0.0) {
            paymentResponse = new PaymentResponse(PaymentResult.INVALID_AMOUNT);
            return false;
        }

        if (purchasePaymentRequest.getTransactionCurrencyCode().isEmpty()) {
            paymentResponse = new PaymentResponse(PaymentResult.INVALID_CURRENCY);
            return false;
        }
        return true;

    }

    private void doPurchase(PurchasePaymentRequest request) {
        try {
            proceedForPaymentWithPurchasePaymentRequest(request);
        } catch (Exception exception) {
            onExceptionInPaymentService(exception);
        }
    }

    private void proceedForPaymentWithPurchasePaymentRequest(
            PurchasePaymentRequest request
    ) throws InterruptedException {
        paymentRequest = paymentRequestFromPurchasePaymentRequest(request);
        proceedForPaymentActivity();
    }

    private PaymentRequest paymentRequestFromPurchasePaymentRequest(
            PurchasePaymentRequest request) {
        return PaymentRequest.Builder.getInstance(
                request.getBillingAmount(),
                PurchaseType.PURCHASE
        )
                .withCurrencyCode(request.getTransactionCurrencyCode())
                .withAdditionalData(request.getAdditionalData())
                .build();
    }

    private void proceedForPaymentActivity()
            throws InterruptedException {
        countDownLatch = new CountDownLatch(1);
        prepareIntentAndStartActivity();
        countDownLatch.await();
    }

    private void onExceptionInPaymentService(Exception exception) {
        checkIfPaymentExceptionAndProceed(exception);
        releaseCountdownLatch();
    }

    private void checkIfPaymentExceptionAndProceed(Exception exception) {
        if (isPaymentException(exception)) {
            assignPaymentResponseWithPaymentExceptionResult(exception);
        } else {
            assignPaymentResponseWithFailedPaymentResultAndExceptionMessage(exception.getMessage());
        }
    }

    private boolean isPaymentException(Exception exception) {
        return exception instanceof PaymentException;
    }

    private void assignPaymentResponseWithPaymentExceptionResult(Exception exception) {
        PaymentException paymentException = (PaymentException) exception;
        PaymentResult paymentResult = paymentException.getPaymentResult();
        paymentResponse = new PaymentResponse(paymentResult);
    }

    private void assignPaymentResponseWithFailedPaymentResultAndExceptionMessage(String message) {
        paymentResponse = new PaymentResponse(PaymentResult.FAILED);
        paymentResponse.setMessage(message);
    }

    private void prepareIntentAndStartActivity() {
        Intent paymentActivityIntent = SdkPaymentActivity
                .prepareIntentForPaymentActivity(
                        context,
                        posPaymentConfiguration,
                        paymentRequest,
                        new PaymentCompletionListenerImpl()
                );
        context.startActivity(paymentActivityIntent);
    }

    public PaymentResponse performVoidOnValidPaymentConfiguration(VoidPaymentRequest voidPaymentRequest) {
        if (!validateVoidPaymentRequest(voidPaymentRequest))
            return paymentResponse;
        if (validApplicationIdentifier())
            performVoid(voidPaymentRequest);
        else
            paymentResponse = new PaymentResponse(INVALID_APPLICATION_IDENTIFIER);
        return paymentResponse;
    }

    private boolean validateVoidPaymentRequest(VoidPaymentRequest voidPaymentRequest) {
        if (voidPaymentRequest == null) {
            paymentResponse = new PaymentResponse(INVALID_APPLICATION_IDENTIFIER);
            return false;
        }

        if (voidPaymentRequest.getBilledAmount() <= 0.0) {
            paymentResponse = new PaymentResponse(PaymentResult.INVALID_AMOUNT);
            return false;
        }

        if (voidPaymentRequest.getInvoiceNumber().isEmpty()) {
            paymentResponse = new PaymentResponse(PaymentResult.INVALID_INVOICE_NUMBER);
            return false;
        }
        return true;
    }

    private void performVoid(VoidPaymentRequest voidPaymentRequest) {
        try {
            proceedForVoidWithVoidPaymentRequest(voidPaymentRequest);
        } catch (Exception e) {
            onExceptionInPaymentService(e);
        }
    }

    private void proceedForVoidWithVoidPaymentRequest(VoidPaymentRequest voidPaymentRequest)
            throws InterruptedException {
        paymentRequest = paymentRequestFromVoidPaymentRequest(voidPaymentRequest);
        proceedForPaymentActivity();
    }

    private PaymentRequest paymentRequestFromVoidPaymentRequest(VoidPaymentRequest voidPaymentRequest) {
        return PaymentRequest.Builder.getInstance(voidPaymentRequest.getBilledAmount(),
                PurchaseType.VOID)
                .withRetrievalReferenceNumber(voidPaymentRequest.getInvoiceNumber())
                .withAdditionalData(voidPaymentRequest.getAdditionalData())
                .build();
    }

    public void cancelPaymentOnValidPaymentConfiguration(
            PaymentCancellationRequest paymentCancellationRequest
    ) {
        if (!validApplicationIdentifier())
            return;

        if (isValidPaymentPaymentCancellationRequest(paymentCancellationRequest)) {
            onValidPaymentCancellationRequest(paymentCancellationRequest);
        }
    }

    private boolean isValidPaymentPaymentCancellationRequest(PaymentCancellationRequest request) {
        if (request == null) {
            paymentResponse = new PaymentResponse(INVALID_DATA);
            return false;
        }
        return true;
    }

    private void onValidPaymentCancellationRequest(
            PaymentCancellationRequest paymentCancellationRequest
    ) {
        doPaymentProcessCancellation(paymentCancellationRequest);

    }

    private void doPaymentProcessCancellation(PaymentCancellationRequest request) {
        try {
            Intent intent = new Intent(ACTION_CANCEL_PAYMENT_REQUEST);
            intent.putExtra(EXTRAS_PAYMENT_CANCELLATION_REQUEST, JsonUtils.toJsonObj(request));
            context.sendBroadcast(intent);
        } catch (Exception e) {
            onExceptionInPaymentService(e);
        }
    }

    private static class PaymentCompletionListenerImpl implements PaymentCompletionListener {

        public static Creator<PaymentCompletionListenerImpl> CREATOR =
                new Creator<PaymentCompletionListenerImpl>() {

                    @Override
                    public PaymentCompletionListenerImpl createFromParcel(Parcel parcel) {
                        return new PaymentCompletionListenerImpl();
                    }

                    @Override
                    public PaymentCompletionListenerImpl[] newArray(int size) {
                        return new PaymentCompletionListenerImpl[size];
                    }
                };

        private PaymentCompletionListenerImpl() {
        }

        @Override
        public void onPaymentResponseReceived(PaymentResponse paymentResponse) {
            PosPaymentImpl.paymentResponse = paymentResponse;
            releaseCountdownLatch();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {

        }
    }
}
