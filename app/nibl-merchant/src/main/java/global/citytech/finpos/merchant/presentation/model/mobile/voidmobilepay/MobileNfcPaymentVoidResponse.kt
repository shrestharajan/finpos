package global.citytech.finpos.merchant.presentation.model.mobile.voidmobilepay

data class MobileNfcPaymentVoidResponse(
    val requestNumber: String,
    val message: String,
    val localDateTime: String
)