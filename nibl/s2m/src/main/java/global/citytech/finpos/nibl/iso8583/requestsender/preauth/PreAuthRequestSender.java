package global.citytech.finpos.nibl.iso8583.requestsender.preauth;

import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.iso8583.NIBLMessagePayloadBuilder;
import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.StringUtils;


/**
 * Modified by Rishav Chudal on 10/15/20.
 */
public abstract class PreAuthRequestSender extends NIBLMessageSenderTemplate {
    private Logger logger = Logger.getLogger(PreAuthRequestSender.class.getName());

    public PreAuthRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Authorization,
                        MTI.MessageFunction.Request,
                        MTI.Originator.Acquirer);
        TransactionIsoRequest preAuthRequest = (TransactionIsoRequest) this.context.getRequest();
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);
        msg.addField(
                new DataElement(
                        DataElement.AMOUNT,
                        HelperUtils.toISOAmount(preAuthRequest.getTransactionAmount(), 12)
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE,
                        preAuthRequest.getPosConditionCode()
                )
        );
        if (!StringUtils.isEmpty(preAuthRequest.getPinBlock()))
            msg.addField(
                    new DataElement(
                            DataElement.PIN_BLOCK,
                            preAuthRequest.getPinBlock()
                    )
            );
        msg.addField(
                new DataElement(
                        DataElement.PRIVATE_USE_FIELD_62,
                        preAuthRequest.getInvoiceNumber()
                )
        );

        addConditionalFields(msg, preAuthRequest);
        MessagePayload.Builder builder = new NIBLMessagePayloadBuilder(this.specInfo, msg);
        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());
        return builder.build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.PRE_AUTHORIZATION.getCode();
    }

    @Override
    protected String getFunctionCode() {
        return "0011";
    }

    public abstract Iso8583Msg addConditionalFields(
            Iso8583Msg msg,
            TransactionIsoRequest preAuthRequest
    );
}
