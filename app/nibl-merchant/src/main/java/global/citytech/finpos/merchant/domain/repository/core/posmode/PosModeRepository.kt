package global.citytech.finpos.merchant.domain.repository.core.posmode

import global.citytech.finposframework.switches.posmode.PosMode

/**
 * Created by Unique Shakya on 1/7/2021.
 */
interface PosModeRepository {
    fun getPosMode(merchantCategoryCode: String): PosMode
}