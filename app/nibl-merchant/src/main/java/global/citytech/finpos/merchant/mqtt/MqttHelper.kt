package global.citytech.finpos.merchant.mqtt

import android.content.Context
import android.os.Build
import android.provider.Settings
import android.telephony.TelephonyManager
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.presentation.model.setting.PushNotificationConfiguration
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.AppUtility.getPushNotificationConfiguration
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finposframework.log.Logger
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*


class MqttHelper(private val context: Context) : MqttCallbackExtended {

    private val TAG = MqttHelper::class.java.name

    companion object {
        val TAG = MqttHelper.javaClass.simpleName
        val pushNotificationConfiguration: PushNotificationConfiguration =
            getPushNotificationConfiguration()
        const val MAX_MQTT_TIMEOUT_IN_SECS = 60
    }

    private var mqttAndroidClient: MqttAndroidClient? = null
    private var serverUri: String? = ""

    private var clientId: String = ""


    override fun connectionLost(cause: Throwable?) {
        printLog(TAG, "Connection Lost to Mqtt...", true)
    }

    override fun messageArrived(topic: String?, message: MqttMessage?) {
        printLog(TAG, "${TAG} ${message}")
        if (message != null) {
            NiblMerchant.INSTANCE.executeMqttAfterCommandReceived(message)
        }
    }

    override fun deliveryComplete(token: IMqttDeliveryToken?) {
        printLog(TAG, "Delivery Complete ::: Token ::: ".plus(token), true)
    }

    override fun connectComplete(reconnect: Boolean, serverURI: String?) {
        printLog(TAG, "Connection Complete ::: serverURI ::: ".plus(serverURI), true)
        if (NiblMerchant.INSTANCE.mqttClient.isConnected()) {
            subscribeToFinPosCashierAppTopic(serverUri)
        }
    }

    fun subscribeToFinPosCashierAppTopic(serverURI: String?) {
        printLog(TAG, "MQTT ${isConnected()}...", true)
        printLog(TAG, "".plus(TAG).plus(serverURI), true)
        NiblMerchant.INSTANCE.mqttClient.subscribe(
            "$SUBSCRIBER_TOPIC${NiblMerchant.INSTANCE.iPlatformManager.serialNumber}",
            SUBSCRIBER_QOS
        )
        NiblMerchant.INSTANCE.mqttClient.subscribe(
            "$SUBSCRIBER_TOPIC_FOR_ALL",
            SUBSCRIBER_QOS
        )

    }

    fun connectToFinPosMqttServer() {
        clientId = "CASHIER-" + getAndroidId()
        serverUri = pushNotificationConfiguration.ip?.trim().plus(":")
            .plus(pushNotificationConfiguration.port?.trim())
        printLog(
            TAG,
            "PUSH URL ".plus(
                pushNotificationConfiguration.ip?.trim().plus(":")
                    .plus(pushNotificationConfiguration.port?.trim())
            )
        )
        mqttAndroidClient = MqttAndroidClient(context, serverUri, clientId)
        mqttAndroidClient?.setCallback(this)

        val mqttConnectionOptions = MqttConnectOptions()
        mqttConnectionOptions.isAutomaticReconnect = SUBSCRIBER_CONNECTION_RECONNECT
        mqttConnectionOptions.isCleanSession = SUBSCRIBER_CONNECTION_CLEAN_SESSION
        if (pushNotificationConfiguration.requiresAuth) {
            mqttConnectionOptions.userName = pushNotificationConfiguration.userId
            mqttConnectionOptions.password = pushNotificationConfiguration.password?.toCharArray()
        }
        mqttConnectionOptions.connectionTimeout =
            getDefaultOrMaxConnectionTimeOutOrKeepAliveInterval(pushNotificationConfiguration.connectionTimeOut)
        mqttConnectionOptions.keepAliveInterval =
            getDefaultOrMaxConnectionTimeOutOrKeepAliveInterval(pushNotificationConfiguration.connectionKeepAliveInterval)

        try {
            mqttAndroidClient?.connect(mqttConnectionOptions, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    printLog(TAG, " CONNECTED TO : ".plus(serverUri))
                    val disconnectedBufferOptions = DisconnectedBufferOptions()
                    disconnectedBufferOptions.isBufferEnabled = true
                    disconnectedBufferOptions.bufferSize = 100
                    disconnectedBufferOptions.isPersistBuffer = true
                    disconnectedBufferOptions.isDeleteOldestMessages = false
                    mqttAndroidClient?.setBufferOpts(disconnectedBufferOptions)
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    printLog(
                        TAG,
                        "".plus("FAILED TO CONNECT TO ::: ")
                            .plus(serverUri)
                            .plus(" ::: EXCEPTION ::: ")
                            .plus(exception)
                    )
                }
            })

        } catch (e: MqttException) {
            e.printStackTrace()
        }

    }


    fun subscribe(topic: String, qos: Int = 0) {

        try {
            mqttAndroidClient?.let {
                mqttAndroidClient?.subscribe(
                    topic,
                    qos,
                    NiblMerchant.INSTANCE.applicationContext,
                    object : IMqttActionListener {
                        override fun onSuccess(asyncActionToken: IMqttToken?) {
                            printLog(
                                TAG,
                                "".plus("SUBSCRIBED TO TOPIC ::: ")
                                    .plus(topic)
                                    .plus(" ::: SUCCEEDED")
                            )
                        }

                        override fun onFailure(
                            asyncActionToken: IMqttToken?,
                            exception: Throwable?
                        ) {
                            printLog(
                                TAG,
                                "".plus("SUBSCRIBED TO TOPIC ::: ")
                                    .plus(topic)
                                    .plus(" ::: FAILED")
                            )
                        }

                    })
            }
        } catch (e: MqttException) {
            e.printStackTrace()
        } catch (np: NullPointerException) {
            np.printStackTrace()
        }
    }

    fun unSubscribe(topic: String) {
        try {
            mqttAndroidClient?.unsubscribe(topic, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    printLog(
                        TAG, "${TAG} UNSUBSCRIBED TO TOPIC $topic"
                    )
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    printLog(TAG, "${TAG} UNSUBSCRIBED TO TOPIC $topic FAILED!")
                }

            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    fun isConnected(): Boolean {
        return if (mqttAndroidClient != null) {
            try {
                val result: Boolean = mqttAndroidClient!!.isConnected
                result
            } catch (ex: Exception) {
                ex.printStackTrace()
                false
            }
        } else {
            false
        }
    }

    fun destroy() {
        mqttAndroidClient?.unregisterResources()
        mqttAndroidClient?.disconnect()
    }

    private fun getAndroidId(): String {

       /* val deviceId: String = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            Settings.Secure.getString(
                context.contentResolver,
                Settings.Secure.ANDROID_ID
            )
        } else {
            val mTelephony = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            if (mTelephony.deviceId != null) {
                mTelephony.deviceId
            } else {
                Settings.Secure.getString(
                    context.contentResolver,
                    Settings.Secure.ANDROID_ID
                )
            }
        }*/
        val deviceId: String = Settings.Secure.getString(
            context.contentResolver,
            Settings.Secure.ANDROID_ID
        )
        printLog(TAG, "DEVICE ID  $deviceId", true)
        return deviceId
    }

    private fun getDefaultOrMaxConnectionTimeOutOrKeepAliveInterval(timeout: Int?): Int {
        if (timeout == null || timeout <= 0) {
            return MAX_MQTT_TIMEOUT_IN_SECS
        }
        return timeout;
    }

}