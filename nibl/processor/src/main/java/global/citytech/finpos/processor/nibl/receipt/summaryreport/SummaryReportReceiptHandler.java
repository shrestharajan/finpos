package global.citytech.finpos.processor.nibl.receipt.summaryreport;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportReceiptLabels;
import global.citytech.finposframework.hardware.io.printer.Printable;
import global.citytech.finposframework.hardware.io.printer.PrinterRequest;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.switches.posmode.PosMode;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finposframework.utility.ReceiptUtils.addBase64Image;
import static global.citytech.finposframework.utility.ReceiptUtils.addDoubleColumnString;
import static global.citytech.finposframework.utility.ReceiptUtils.addMultiColumnString;
import static global.citytech.finposframework.utility.ReceiptUtils.addSingleColumnString;
import static global.citytech.finposframework.utility.ReceiptUtils.createStringList;

/**
 * Created by Unique Shakya on 1/5/2021.
 */
public class SummaryReportReceiptHandler {

    private PrinterService printerService;

    public SummaryReportReceiptHandler(PrinterService printerService) {
        this.printerService = printerService;
    }

    public PrinterResponse print(SummaryReportReceipt summaryReportReceipt) {
        PrinterRequest printerRequest = this.preparePrinterRequest(summaryReportReceipt);
        return this.printerService.print(printerRequest);
    }

    private PrinterRequest preparePrinterRequest(SummaryReportReceipt summaryReportReceipt) {
        List<Printable> printableList = new ArrayList<>();
        if (summaryReportReceipt.getRetailer().getRetailerLogo() != null)
            addBase64Image(printableList, summaryReportReceipt.getRetailer().getRetailerLogo());
        addSingleColumnString(printableList, summaryReportReceipt.getRetailer().getRetailerName(), SummaryReportReceiptStyle.RETAILER_NAME.getStyle());
        addSingleColumnString(printableList, summaryReportReceipt.getRetailer().getRetailerAddress(), SummaryReportReceiptStyle.RETAILER_ADDRESS.getStyle());
        addSingleColumnString(printableList, SummaryReportReceiptLabels.LINE_BREAK, SummaryReportReceiptStyle.LINE_BREAK.getStyle());
        addDoubleColumnString(printableList,
                this.prepareDateString(summaryReportReceipt.getPerformance().getStartDateTime()),
                this.prepareTimeString(summaryReportReceipt.getPerformance().getStartDateTime()),
                SummaryReportReceiptStyle.START_DATE_TIME.getStyle());
        addDoubleColumnString(printableList,
                this.merchantId(summaryReportReceipt.getRetailer().getMerchantId()),
                this.terminalId(summaryReportReceipt.getRetailer().getTerminalId()),
                SummaryReportReceiptStyle.TID_MID.getStyle());
        addDoubleColumnString(printableList,
                SummaryReportReceiptLabels.BATCH_NUMBER,
                summaryReportReceipt.getReconciliationBatchNumber(),
                SummaryReportReceiptStyle.BATCH_NUMBER.getStyle());
        addDoubleColumnString(printableList,
                SummaryReportReceiptLabels.HOST,
                summaryReportReceipt.getHost(),
                SummaryReportReceiptStyle.HOST.getStyle());
        addSingleColumnString(printableList, SummaryReportReceiptLabels.LINE_BREAK, SummaryReportReceiptStyle.LINE_BREAK.getStyle());
        addSingleColumnString(printableList, SummaryReportReceiptLabels.HEADER, SummaryReportReceiptStyle.HEADER.getStyle());
        addSingleColumnString(printableList, SummaryReportReceiptLabels.LINE_BREAK, SummaryReportReceiptStyle.LINE_BREAK.getStyle());
        this.addReportLabels(printableList);
        this.addReportValues(printableList, summaryReportReceipt);
        this.addCumulativeTotalsTable(printableList, summaryReportReceipt);
        return new PrinterRequest(printableList);
    }

    private void addCumulativeTotalsTable(List<Printable> printableList, SummaryReportReceipt summaryReportReceipt) {
        addSingleColumnString(printableList, SummaryReportReceiptLabels.LINE_BREAK, SummaryReportReceiptStyle.LINE_BREAK.getStyle());
        addSingleColumnString(printableList, SummaryReportReceiptLabels.CUMULATIVE, SummaryReportReceiptStyle.CARD_SCHEME.getStyle());
        addSingleColumnString(printableList, SummaryReportReceiptLabels.DIVIDER, SummaryReportReceiptStyle.DIVIDER.getStyle());
        addMultiColumnString(printableList, this.prepareCumulativeSalesRow(summaryReportReceipt.getPosMode(), summaryReportReceipt.getSummaryReport().getTransactionCurrencyName(),
                summaryReportReceipt.getSummaryReport()), this.getReportTableColumnWidths(), SummaryReportReceiptStyle.REPORT_VALUE.getStyle());
        addMultiColumnString(printableList, this.prepareCumulativeVoidRow(summaryReportReceipt.getPosMode(), summaryReportReceipt.getSummaryReport().getTransactionCurrencyName(),
                summaryReportReceipt.getSummaryReport()), this.getReportTableColumnWidths(), SummaryReportReceiptStyle.REPORT_VALUE.getStyle());
        addMultiColumnString(printableList, this.prepareCumulativeSalesRefundRow(summaryReportReceipt.getSummaryReport().getTransactionCurrencyName(),
                summaryReportReceipt.getSummaryReport()), this.getReportTableColumnWidths(), SummaryReportReceiptStyle.REPORT_VALUE.getStyle());
        addSingleColumnString(printableList, SummaryReportReceiptLabels.DIVIDER, SummaryReportReceiptStyle.DIVIDER.getStyle());
        addMultiColumnString(printableList, this.prepareCumulativeTotalsRow(summaryReportReceipt.getSummaryReport().getTransactionCurrencyName(),
                summaryReportReceipt.getSummaryReport()), this.getReportTableColumnWidths(), SummaryReportReceiptStyle.REPORT_VALUE.getStyle());
        addSingleColumnString(printableList, SummaryReportReceiptLabels.DIVIDER, SummaryReportReceiptStyle.DIVIDER.getStyle());
    }

    private List<String> prepareCumulativeTotalsRow(String transactionCurrencyName, SummaryReport summaryReport) {
        List<String> strings = new ArrayList<>();
        strings.add(SummaryReportReceiptLabels.TOTAL);
        strings.add(summaryReport.getTotalCount());
        strings.add(transactionCurrencyName);
        strings.add(summaryReport.getTotalAmount());
        return strings;
    }

    private List<String> prepareCumulativeVoidRow(PosMode posMode, String transactionCurrencyName, SummaryReport summaryReport) {
        List<String> strings = new ArrayList<>();
        if (posMode == PosMode.RETAILER)
            strings.add(SummaryReportReceiptLabels.VOID);
        else
            strings.add(SummaryReportReceiptLabels.CASH_VOID);
        strings.add(summaryReport.getVoidCount());
        strings.add(transactionCurrencyName);
        strings.add(summaryReport.getVoidAmount());
        return strings;
    }

    private List<String> prepareCumulativeSalesRefundRow(String transactionCurrencyName, SummaryReport summaryReport) {
        List<String> strings = new ArrayList<>();
        strings.add(SummaryReportReceiptLabels.REFUND);
        strings.add(summaryReport.getRefundCount());
        strings.add(transactionCurrencyName);
        strings.add(summaryReport.getRefundAmount());
        return strings;
    }

    private List<String> prepareCumulativeSalesRow(PosMode posMode, String transactionCurrencyName, SummaryReport summaryReport) {
        List<String> strings = new ArrayList<>();
        if (posMode == PosMode.RETAILER)
            strings.add(SummaryReportReceiptLabels.SALE);
        else
            strings.add(SummaryReportReceiptLabels.CASH);
        strings.add(summaryReport.getSalesCount());
        strings.add(transactionCurrencyName);
        strings.add(summaryReport.getSalesAmount());
        return strings;
    }

    private void addReportValues(List<Printable> printableList, SummaryReportReceipt summaryReportReceipt) {
        SummaryReport summaryReport = summaryReportReceipt.getSummaryReport();
        String saleLabel;
        String voidLabel;
        if (summaryReportReceipt.getPosMode() == PosMode.CASH) {
            saleLabel = SummaryReportReceiptLabels.CASH;
            voidLabel = SummaryReportReceiptLabels.CASH_VOID;
        }else {
            saleLabel = SummaryReportReceiptLabels.SALE;
            voidLabel = SummaryReportReceiptLabels.VOID;
        }
        for (Map.Entry<CardSchemeType, CardSchemeSummary> entry : summaryReport.getMap().entrySet()) {
            CardSchemeType cardSchemeType = entry.getKey();
            CardSchemeSummary cardSchemeSummary = entry.getValue();
            addSingleColumnString(printableList, SummaryReportReceiptLabels.DIVIDER, SummaryReportReceiptStyle.DIVIDER.getStyle());
            addSingleColumnString(printableList, cardSchemeType.getCardScheme(), SummaryReportReceiptStyle.CARD_SCHEME.getStyle());
            addSingleColumnString(printableList, SummaryReportReceiptLabels.DIVIDER, SummaryReportReceiptStyle.DIVIDER.getStyle());

            addMultiColumnString(printableList,
                    createStringList(saleLabel,
                            cardSchemeSummary.getSalesCountInString(),
                            summaryReport.getTransactionCurrencyName(),
                            cardSchemeSummary.getSalesAmountInString()),
                    this.getReportTableColumnWidths(),
                    SummaryReportReceiptStyle.REPORT_VALUE.getStyle());
            addMultiColumnString(printableList,
                    createStringList(voidLabel,
                            cardSchemeSummary.getVoidCountInString(),
                            summaryReport.getTransactionCurrencyName(),
                            cardSchemeSummary.getVoidAmountInString()),
                    this.getReportTableColumnWidths(),
                    SummaryReportReceiptStyle.REPORT_VALUE.getStyle());
            addMultiColumnString(printableList,
                    createStringList(SummaryReportReceiptLabels.REFUND,
                            cardSchemeSummary.getRefundCountInString(),
                            summaryReport.getTransactionCurrencyName(),
                            cardSchemeSummary.getRefundAmountInString()),
                    this.getReportTableColumnWidths(),
                    SummaryReportReceiptStyle.REPORT_VALUE.getStyle());
            addSingleColumnString(printableList, SummaryReportReceiptLabels.DIVIDER, SummaryReportReceiptStyle.DIVIDER.getStyle());
            addMultiColumnString(printableList,
                    createStringList(SummaryReportReceiptLabels.TOTAL,
                            cardSchemeSummary.getTotalCountForReceipt(),
                            summaryReport.getTransactionCurrencyName(),
                            cardSchemeSummary.getTotalAmountForReceipt()),
                    this.getReportTableColumnWidths(),
                    SummaryReportReceiptStyle.REPORT_VALUE.getStyle());
            addSingleColumnString(printableList, SummaryReportReceiptLabels.DIVIDER, SummaryReportReceiptStyle.DIVIDER.getStyle());
            addSingleColumnString(printableList, SummaryReportReceiptLabels.LINE_BREAK, SummaryReportReceiptStyle.LINE_BREAK.getStyle());
        }
    }

    private void addReportLabels(List<Printable> printableList) {
        addMultiColumnString(printableList,
                createStringList(SummaryReportReceiptLabels.TRANSACTION,
                        SummaryReportReceiptLabels.COUNT,
                        "  ",
                        SummaryReportReceiptLabels.AMOUNT),
                this.getReportTableColumnWidths(),
                SummaryReportReceiptStyle.REPORT_LABEL.getStyle());
    }

    private int[] getReportTableColumnWidths() {
        return new int[]{35, 20, 20, 25};
    }

    private String merchantId(String merchantId) {
        return this.formatLabelWithValue(DetailReportReceiptLabels.MERCHANT_ID, merchantId);
    }

    private String terminalId(String terminalId) {
        return this.formatLabelWithValue(DetailReportReceiptLabels.TERMINAL_ID, terminalId);
    }

    private String prepareTimeString(String dateTime) {
        if (StringUtils.isEmpty(dateTime) || dateTime.length() < 15)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(dateTime, 6, 8);
        stringBuilder.append(":");
        stringBuilder.append(dateTime, 8, 10);
        stringBuilder.append(":");
        stringBuilder.append(dateTime, 10, 12);
        return this.formatLabelWithValue(DetailReportReceiptLabels.TIME, stringBuilder.toString());
    }

    private String prepareDateString(String dateTime) {
        if (StringUtils.isEmpty(dateTime) || dateTime.length() < 15)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(dateTime, 4, 6);
        stringBuilder.append("/");
        stringBuilder.append(dateTime, 2, 4);
        stringBuilder.append("/");
        stringBuilder.append("20");
        stringBuilder.append(dateTime, 0, 2);
        return this.formatLabelWithValue(DetailReportReceiptLabels.DATE, stringBuilder.toString());
    }

    private String formatLabelWithValue(String label, String value) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(label);
        stringBuilder.append(":");
        stringBuilder.append(value);
        return stringBuilder.toString();
    }
}
