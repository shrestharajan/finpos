package global.citytech.finpos.processor.neps;

import global.citytech.finpos.processor.neps.auth.completion.AuthorisationCompletionUseCase;
import global.citytech.finpos.processor.neps.autoreversal.AutoReversalUseCase;
import global.citytech.finpos.processor.neps.cashadvance.CardCashAdvanceUseCase;
import global.citytech.finpos.processor.neps.cashadvance.ManualCashAdvanceUseCase;
import global.citytech.finpos.processor.neps.greenpin.GreenPinUseCase;
import global.citytech.finpos.processor.neps.idlepage.IdlePageTransactionUseCase;
import global.citytech.finpos.processor.neps.keyexchange.MerchantKeyExchangeUseCase;
import global.citytech.finpos.processor.neps.logon.MerchantLogOnUseCase;
import global.citytech.finpos.processor.neps.pinchange.PinChangeUseCase;
import global.citytech.finpos.processor.neps.posmode.PosModeUseCase;
import global.citytech.finpos.processor.neps.preauths.CardPreAuthUseCase;
import global.citytech.finpos.processor.neps.preauths.ManualPreAuthUseCase;
import global.citytech.finpos.processor.neps.purchases.CardPurchaseUseCase;
import global.citytech.finpos.processor.neps.purchases.ManualPurchaseUseCase;
import global.citytech.finpos.processor.neps.receipt.CustomerCopyUseCase;
import global.citytech.finpos.processor.neps.receipt.detailreport.DetailReportUseCase;
import global.citytech.finpos.processor.neps.receipt.duplicate.DuplicateReceiptUseCase;
import global.citytech.finpos.processor.neps.receipt.duplicatereconciliation.DuplicateReconciliationReceiptUseCase;
import global.citytech.finpos.processor.neps.receipt.summmaryreport.SummaryReportUseCase;
import global.citytech.finpos.processor.neps.reconciliation.ReconciliationUseCase;
import global.citytech.finpos.processor.neps.reconciliation.check.NepsCheckReconciliationUseCase;
import global.citytech.finpos.processor.neps.reconciliation.clear.NepsBatchClearUseCase;
import global.citytech.finpos.processor.neps.refund.CardRefundUseCase;
import global.citytech.finpos.processor.neps.refund.ManualRefundUseCase;
import global.citytech.finpos.processor.neps.transactiontype.TransactionTypeUseCase;
import global.citytech.finpos.processor.neps.voidsale.VoidSaleUseCase;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.switches.Processor;
import global.citytech.finposframework.switches.ProcessorManager;
import global.citytech.finposframework.switches.autoreversal.AutoReversalRequester;
import global.citytech.finposframework.switches.keyexchange.KeyExchangeRequester;
import global.citytech.finposframework.switches.logon.LogOnRequester;
import global.citytech.finposframework.switches.posmode.PosModeRequester;
import global.citytech.finposframework.switches.receipt.customercopy.CustomerCopyRequester;
import global.citytech.finposframework.switches.receipt.detailreport.DetailReportRequester;
import global.citytech.finposframework.switches.receipt.duplicatereceipt.DuplicateReceiptRequester;
import global.citytech.finposframework.switches.receipt.duplicatereconciliation.DuplicateReconciliationReceiptRequester;
import global.citytech.finposframework.switches.receipt.ministatement.StatementRequester;
import global.citytech.finposframework.switches.receipt.summaryreport.SummaryReportRequester;
import global.citytech.finposframework.switches.receipt.tmslogs.TmsLogsRequester;
import global.citytech.finposframework.switches.reconciliation.ReconciliationRequester;
import global.citytech.finposframework.switches.reconciliation.check.CheckReconciliationRequester;
import global.citytech.finposframework.switches.reconciliation.clear.BatchClearRequester;
import global.citytech.finposframework.switches.transaction.TransactionRequester;
import global.citytech.finposframework.switches.transactiontype.TransactionTypeRequester;


/**
 * @author rajudhital on 4/3/20
 */
public class NepsProcessor implements Processor {

    static {
        ProcessorManager.register(new NepsProcessor());
    }

    private TerminalRepository terminalRepository;
    private Notifier notifier;

    @Override
    public void init(TerminalRepository terminalRepository, Notifier notifier) {
        if (terminalRepository != null) {
            this.terminalRepository = terminalRepository;
        }
        if (notifier != null)
            this.notifier = notifier;
    }

    @Override
    public LogOnRequester getLogOnRequest() {
        return new MerchantLogOnUseCase(this.terminalRepository);
    }

    @Override
    public KeyExchangeRequester getKeyExchangeRequester() {
        return new MerchantKeyExchangeUseCase(this.terminalRepository);
    }

    @Override
    public TransactionRequester getPurchaseRequester() {
        return new CardPurchaseUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getManualPurchaseRequester() {
        return new ManualPurchaseUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getIdlePurchaseRequester() {
        return new IdlePageTransactionUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getPreAuthRequester() {
        return new CardPreAuthUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getManualPreAuthRequester() {
        return new ManualPreAuthUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public CustomerCopyRequester getCustomerCopyRequester() {
        return new CustomerCopyUseCase();
    }

    @Override
    public StatementRequester getStatementPrintRequester() {
        return null;
    }

    @Override
    public ReconciliationRequester getReconciliationRequester() {
        return new ReconciliationUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getVoidSaleRequester() {
        return new VoidSaleUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getRefundRequester() {
        return new CardRefundUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getManualRefundRequester() {
        return new ManualRefundUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getAuthorisationCompletionRequester() {
        return new AuthorisationCompletionUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TmsLogsRequester getTmsLogsRequester() {
        return null;
    }

    @Override
    public TransactionTypeRequester getTransactionTypeRequester() {
        return new TransactionTypeUseCase();
    }

    @Override
    public DuplicateReceiptRequester getDuplicateReceiptRequester() {
        return new DuplicateReceiptUseCase();
    }

    @Override
    public DuplicateReconciliationReceiptRequester getDuplicateReconciliationReceiptRequester() {
        return new DuplicateReconciliationReceiptUseCase();
    }

    @Override
    public DetailReportRequester getDetailReportRequester() {
        return new DetailReportUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public SummaryReportRequester getSummaryReportRequester() {
        return new SummaryReportUseCase(this.terminalRepository);
    }

    @Override
    public PosModeRequester getPosModeRequester() {
        return new PosModeUseCase();
    }

    @Override
    public TransactionRequester getCashAdvanceRequester() {
        return new CardCashAdvanceUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getManualCashAdvanceRequester() {
        return new ManualCashAdvanceUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getTipAdjustmentRequester() {
        throw new PosException(PosError.DEVICE_ERROR_TIP_ADJUSTMENT_NOT_SUPPORTED);
    }

    @Override
    public AutoReversalRequester getAutoReversalRequester() {
        return new AutoReversalUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public CheckReconciliationRequester getCheckReconciliationRequester() {
        return new NepsCheckReconciliationUseCase(this.terminalRepository);
    }

    @Override
    public BatchClearRequester getBatchClearRequester() {
        return new NepsBatchClearUseCase(this.terminalRepository);
    }

    @Override
    public TransactionRequester getGreenPinRequester() {
        return new GreenPinUseCase(this.terminalRepository,this.notifier);
    }

    @Override
    public TransactionRequester getPinChangeRequester() {
        return new PinChangeUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getBalanceInquiryRequester() {
        return null;
    }

    @Override
    public TransactionRequester getCashInRequester() {
        return null;
    }
    public TransactionRequester getMiniStatementRequester() {
        return null;
    }
}
