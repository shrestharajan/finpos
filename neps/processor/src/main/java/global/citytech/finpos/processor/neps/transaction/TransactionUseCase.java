package global.citytech.finpos.processor.neps.transaction;

import org.jetbrains.annotations.NotNull;


import global.citytech.finpos.neps.iso8583.SmartVistaActionCode;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.transaction.CardProcessorInterface;
import global.citytech.finposframework.usecases.transaction.TransactionValidator;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Saurav Ghimire on 1/20/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public abstract class TransactionUseCase {
    protected final TerminalRepository terminalRepository;
    protected final Notifier notifier;
    protected final Logger logger;

    protected TransactionValidator transactionValidator;
    protected CardProcessorInterface cardProcessor;
    protected final ReadCardRequest.MultipleAidSelectionListener multipleAidSelectionListener = new ReadCardRequest.MultipleAidSelectionListener() {
        @Override
        public int onMultipleAid(@NotNull String[] aids) {
            notifier.notify(Notifier.EventType.DETECTED_MULTIPLE_AID, "Processing..");
            return -1;
        }
    };

    public TransactionUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        this.terminalRepository = terminalRepository;
        this.notifier = notifier;
        this.logger = Logger.getLogger(getClassName());
    }

    protected abstract String getClassName();


    protected String approvedMessage(IsoMessageResponse purchaseIsoMessageResponse) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("APPROVED");
        stringBuilder.append("\n");
        stringBuilder.append(
                IsoMessageUtils.retrieveFromDataElementsAsString(
                        purchaseIsoMessageResponse, DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE)
        );
        return stringBuilder.toString();
    }

    protected String declinedMessage(IsoMessageResponse purchaseIsoMessageResponse, ApplicationRepository applicationRepository) {
        int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(purchaseIsoMessageResponse, 39);
        if (actionCode < 0)
            throw new IllegalArgumentException("Invalid Action Code");
        String messageText = applicationRepository.getMessageTextByActionCode(actionCode);
        if (StringUtils.isEmpty(messageText))
            messageText = SmartVistaActionCode.getByActionCode(actionCode).getDescription();
        return messageText;
    }

    protected String retrieveMessage(IsoMessageResponse purchaseIsoMessageResponse, boolean isPurchaseApproved, ApplicationRepository applicationRepository) {
        if (isPurchaseApproved)
            return this.approvedMessage(purchaseIsoMessageResponse);
        else
            return this.declinedMessage(purchaseIsoMessageResponse, applicationRepository);
    }

}
