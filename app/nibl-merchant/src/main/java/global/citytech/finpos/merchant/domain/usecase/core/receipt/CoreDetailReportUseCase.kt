package global.citytech.finpos.merchant.domain.usecase.core.receipt

import global.citytech.finpos.merchant.domain.model.receipt.detailreport.DetailReportResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.receipt.DetailReportRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportResponseModel
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 1/4/2021.
 */
class CoreDetailReportUseCase(private val detailReportRepository: DetailReportRepository) {
    fun printDetailReport(configurationItem: ConfigurationItem): Observable<DetailReportResponseEntity> {
        return detailReportRepository.printDetailReport(configurationItem)
    }
}