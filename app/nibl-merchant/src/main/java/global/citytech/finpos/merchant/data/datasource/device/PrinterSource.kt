package global.citytech.finpos.merchant.data.datasource.device

import global.citytech.finposframework.hardware.io.printer.PrinterService

/**
 * Created by Rishav Chudal on 8/27/20.
 */
interface PrinterSource:
    PrinterService