package global.citytech.finpos.processor.neps.purchases;

import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finposframework.hardware.common.Result;

public final class PurchaseResponse extends TransactionResponse {

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private boolean approved;
        private String message;
        private String stan;
        private Result result;
        private String approvalCode;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static Builder createDefaultBuilder() {
            return new Builder();
        }

        public Builder debugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public Builder debugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public Builder shouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }
        public Builder approved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder stan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder result(Result result) {
            this.result = result;
            return this;
        }

        public Builder approvalCode(String approvalCode) {
            this.approvalCode = approvalCode;
            return this;
        }

        public PurchaseResponse build() {
            PurchaseResponse purchaseResponse = new PurchaseResponse();
            purchaseResponse.debugRequestMessage = this.debugRequestMessage;
            purchaseResponse.debugResponseMessage = this.debugResponseMessage;
            purchaseResponse.approved = this.approved;
            purchaseResponse.message = this.message;
            purchaseResponse.result = this.result;
            purchaseResponse.approvalCode = this.approvalCode;
            purchaseResponse.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            purchaseResponse.stan = this.stan;
            return purchaseResponse;
        }
    }


}
