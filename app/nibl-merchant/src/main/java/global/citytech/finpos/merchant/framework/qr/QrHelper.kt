package global.citytech.finpos.merchant.framework.qr

import android.graphics.Bitmap
import android.graphics.Color
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import global.citytech.finpos.merchant.domain.usecase.qr.NqrConstants.MAX_MERCHANT_CITY_NAME_LENGTH
import global.citytech.finpos.merchant.domain.usecase.qr.NqrConstants.MAX_MERCHANT_NAME_LENGTH

/**
 * @author sachin
 */
class QrHelper {

    private final val writer: QRCodeWriter = QRCodeWriter()

    public fun generateQrBitmap(qrData: String, width: Int = 512, height: Int = 512): Bitmap? {
        try {
            val bitMatrix: BitMatrix = writer.encode(qrData, BarcodeFormat.QR_CODE, width, height)
            val width = bitMatrix.width
            val height = bitMatrix.height
            val bmp: Bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
            for (x in 0 until width) {
                for (y in 0 until height) {
                    bmp.setPixel(x, y, if (bitMatrix[x, y]) Color.BLACK else Color.WHITE)
                }
            }
            return bmp;
        } catch (e: WriterException) {
            e.printStackTrace()
            return null
        }
    }

    fun toNqrMerchantName(merchantName: String): String {
        return merchantName.substring(0, Math.min(merchantName.length, MAX_MERCHANT_NAME_LENGTH))
    }

    fun toNqrMerchantCityName(merchantCity: String): String {
        val alphaNumericRegex = Regex("[^A-Za-z0-9 ]")
        var merchantCityData = merchantCity
        merchantCityData = alphaNumericRegex.replace(merchantCityData,"")
        return merchantCityData.substring(
            0,
            Math.min(merchantCityData.length, MAX_MERCHANT_CITY_NAME_LENGTH)
        )
    }
}