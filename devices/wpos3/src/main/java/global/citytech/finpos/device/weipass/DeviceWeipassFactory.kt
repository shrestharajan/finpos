package global.citytech.finpos.device.weipass

import android.content.Context
import global.citytech.finposframework.hardware.common.DeviceService
import global.citytech.finposframework.notifier.Notifier

/**
 * Created by Rishav Chudal on 4/9/20.
 */
class DeviceWeipassFactory :
    global.citytech.finposframework.hardware.DeviceFactory {

    override fun getDevice(
        context: Any?,
        deviceServiceType: global.citytech.finposframework.hardware.DeviceServiceType,
        notifier: Notifier
    ): DeviceService? {
        return WeipassFactory.getService(context as Context, deviceServiceType, notifier)
    }
}