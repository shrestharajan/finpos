package global.citytech.finpos.processor.neps.transactiontype;

import java.util.ArrayList;
import java.util.List;


import global.citytech.finpos.neps.iso8583.ProcessingCode;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.transactiontype.TransactionTypeRequester;

/**
 * Created by Unique Shakya on 1/20/2021.
 */
public class TransactionTypeUseCase implements TransactionTypeRequester<TransactionTypeRequestModel, TransactionTypeResponseModel>,
        UseCase<TransactionTypeRequestModel, TransactionTypeResponseModel> {
    @Override
    public TransactionTypeResponseModel execute(TransactionTypeRequestModel transactionTypeRequestModel) {
        List<String> supportedTransactions = new ArrayList<>();
        supportedTransactions.add(ProcessingCode.PURCHASE.getCode().substring(0, 2));
        return new TransactionTypeResponseModel(supportedTransactions);
    }
}
