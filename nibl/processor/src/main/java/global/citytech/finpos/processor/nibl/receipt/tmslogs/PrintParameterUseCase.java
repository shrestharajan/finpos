package global.citytech.finpos.processor.nibl.receipt.tmslogs;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.tmslogs.TmsLogsRequester;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.utility.PosResponse;
import global.citytech.finposframework.utility.PosResult;

/**
 * Created by Rishav Chudal on 11/6/20.
 */
public class PrintParameterUseCase implements
        UseCase<PrintParameterRequestModel, PrintParameterResponseModel>,
        TmsLogsRequester<PrintParameterRequestModel, PrintParameterResponseModel> {

    private TerminalRepository terminalRepository;
    private Notifier notifier;
    private Logger logger;

    public PrintParameterUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        this.terminalRepository = terminalRepository;
        this.notifier = notifier;
        this.logger = Logger.getLogger(PrintParameterUseCase.class.getSimpleName());
    }

    @Override
    public PrintParameterResponseModel execute(PrintParameterRequestModel parameterRequestModel) {
        try {
            this.logger.log("::: PRINT PARAMETER USE CASE :::");
            if (!deviceReady(parameterRequestModel.getDeviceController())) {
                throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
            }
        } catch (PosException ex) {
            ex.printStackTrace();
            throw ex;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_PRINT);
        }
        return null;
    }

    private boolean deviceReady(DeviceController deviceController) {
        PosResponse deviceReadyResponse = deviceController.isReady(null);
        return deviceReadyResponse.getPosResult() == PosResult.SUCCESS;
    }
}
