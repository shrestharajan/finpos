package global.citytech.finpos.merchant.presentation.model.qr

data class QrPaymentStatusResponse(
    val payerName: String,
    val payerPAN: String,
    val traceAudit: String,
    val rrn: String,
    val authorizationCode: String,
    val batchNumber: Double,
    val localDateTime: String,
    val network: String,
    val amount: Double,
    val merchantId: String,
    val terminalId: String,
    val transactionCurrency: String
)