package global.citytech.finpos.merchant.presentation.utils

import android.net.Uri

/**
 * Created by Saurav Ghimire on 6/17/21.
 * sauravnghimire@gmail.com
 */


object Constants {
    const val AMOUNT  = "amount"
    const val REMARKS_RESPONSE_CODE  = "responseCode"
    const val REMARKS_BATCH_NO  = "batchNumber"
    const val REMARKS_POS_ENTRY_MODE  = "posEntryMode"
    const val REMARKS_FROM_BILLING_SYSTEM  = "fromBillingSystem"
    val PIN_PAD_CURRENCY: String = "NPR"
    val PIN_PAD_MESSAGE: String = "CUSTOMER PIN ENTRY"
    const val CONNECT_FONEPAY_PACKAGE = "global.citytech.fonepay"
    private const val CONNECT_FONEPAY_PROVIDER_AUTHORITY = "global.citytech.fonepay.provider"
     const val CONNECT_FONEPAY_PROVIDER_FILE_PATH = "/app_flutter/message.txt"
    val CONNECT_FONEPAY_PROVIDER_CONTENT_URI: Uri = Uri.parse("content://$CONNECT_FONEPAY_PROVIDER_AUTHORITY$CONNECT_FONEPAY_PROVIDER_FILE_PATH")
    val listOfAid: List<String> = listOf(
        "F2600609640539",
        "F2222223452124",
        "F2222232122124",
        "F222222123",
        "F222222124",
        "F222221162124",
        "F2223462222124"
    )
}