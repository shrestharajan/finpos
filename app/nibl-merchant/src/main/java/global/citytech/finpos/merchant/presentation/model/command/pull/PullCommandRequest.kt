package global.citytech.finpos.merchant.presentation.model.command.pull

/**
 * Created by Unique Shakya on 9/24/2021.
 */
data class PullCommandRequest (var serialNumber: String)