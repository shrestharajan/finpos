package global.citytech.finpos.device.weipass.system

import global.citytech.finpos.device.weipass.utils.WeipassConstant
import global.citytech.finpos.device.weipass.utils.WeipassConstant.ADB_MODE_DISABLING_FAILED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.ADB_MODE_ENABLING_FAILED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.ADB_MODE_SETTINGS_APPLIED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.FACTORY_RESET_FAILED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.HOME_BUTTON_DISABLING_FAILED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.HOME_BUTTON_ENABLING_FAILED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.HOME_BUTTON_SETTINGS_APPLIED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MICROSD_MODE_DISABLING_FAILED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MICROSD_MODE_ENABLING_FAILED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MICROSD_MODE_SETTINGS_APPLIED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.OTG_MODE_DISABLING_FAILED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.OTG_MODE_ENABLING_FAILED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.OTG_MODE_SETTINGS_APPLIED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.POWER_BUTTON_DISABLING_FAILED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.POWER_BUTTON_ENABLING_FAILED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.POWER_BUTTON_SETTINGS_APPLIED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SYSTEM_UPDATE_FAILED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SYSTEM_UPDATE_SUCCESS
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.system.SystemService
import global.citytech.finposframework.hardware.utility.DeviceApiConstants

/**
 * Created by Rishav Chudal on 6/7/20.
 */
class WeipassSystemServiceImpl:
    SystemService {
    private lateinit var response: DeviceResponse

    /**
     * ADB Settings
     */
    override fun enableADB(): DeviceResponse {
        response = DeviceResponse(Result.FAILURE, ADB_MODE_ENABLING_FAILED)
        WeipassLogger.log("ADB Mode Enabling...")
        setADBMode(WeipassConstant.SDK_ENABLE_ADB_MODE)
        return response
    }

    override fun disableADB(): DeviceResponse {
        response = DeviceResponse(Result.FAILURE, ADB_MODE_DISABLING_FAILED)
        WeipassLogger.log("ADB Mode Disabling...")
        setADBMode(WeipassConstant.SDK_DISABLE_ADB_MODE)
        return response
    }

    private fun setADBMode(mode: Int) {
        WeipassLogger.log("setADBMode ::: mode ::: ".plus(mode))
        try {
            val method =
                WangPosManager.getMethod("setAdbMode", Int::class.javaPrimitiveType)
            method!!.invoke(WangPosManager.getInstance(), mode)
            response = DeviceResponse(Result.SUCCESS, ADB_MODE_SETTINGS_APPLIED)
        } catch (ex: Exception) {
            WeipassLogger.log("Exception in setADBMode ::: " + ex.message)
        }
    }

    /**
     * Factory Reset Settings
     */
    override fun doFactoryReset(): DeviceResponse {
        response = DeviceResponse(Result.FAILURE, FACTORY_RESET_FAILED)
        WeipassLogger.log("Performing FactoryReset ...")
        try {
            val method = WangPosManager.getMethod("factoryReset", Boolean::class.java) //TODO review again
            method!!.invoke(WangPosManager.getInstance())
            response = DeviceResponse(Result.SUCCESS, "Factory Reset Success")
        } catch (ex: Exception) {
            WeipassLogger.log("Exception in factoryReset ::: " + ex.message)
        }
        return response
    }

    /**
     *  System Update Settings
     */
    override fun doSystemUpdate(firmwareFilePath: String): DeviceResponse {
        response = DeviceResponse(Result.FAILURE, SYSTEM_UPDATE_FAILED)
        WeipassLogger.log("systemUpdate ::: firmwareFilePath ::: ".plus(firmwareFilePath))
        try {
            val method =
                WangPosManager.getMethod("systemUpdate", String::class.java)
            method!!.invoke(WangPosManager.getInstance(), firmwareFilePath)
            response = DeviceResponse(Result.SUCCESS, SYSTEM_UPDATE_SUCCESS)
        } catch (ex: Exception) {
            WeipassLogger.log("Exception in systemUpdate ::: " + ex.message)
        }
        return response
    }

    /**
     * OTG Settings
     */
    override fun enableOTG(): DeviceResponse {
        response = DeviceResponse(Result.FAILURE, OTG_MODE_ENABLING_FAILED)
        WeipassLogger.log("OTG Mode enabling...")
        setOTGMode(true)
        return response
    }

    override fun disableOTG(): DeviceResponse {
        response = DeviceResponse(Result.FAILURE, OTG_MODE_DISABLING_FAILED)
        WeipassLogger.log("OTG Mode Disabling...")
        setOTGMode(false)
        return response
    }

    private fun setOTGMode(enable: Boolean) {
        WeipassLogger.log("setOTGMode ::: enable ::: ".plus(enable))
        try {
            val method =
                WangPosManager.getMethod("setOtgEnable",
                    Boolean::class.javaPrimitiveType)
            method!!.invoke(WangPosManager.getInstance(), enable)
            response = DeviceResponse(Result.SUCCESS, OTG_MODE_SETTINGS_APPLIED)
        } catch (ex: Exception) {
            WeipassLogger.log("Exception in setOTGMode ::: ".plus(ex.message))
        }
    }

    /**
     * Micro SD Settings
     */
    override fun enableMicroSD(): DeviceResponse {
        response = DeviceResponse(Result.FAILURE, MICROSD_MODE_ENABLING_FAILED)
        WeipassLogger.log("MicroSD Mode enabling...")
        setOTGMode(true)
        return response
    }

    override fun disableMicroSD(): DeviceResponse {
        response = DeviceResponse(Result.FAILURE, MICROSD_MODE_DISABLING_FAILED)
        WeipassLogger.log("MicroSD Mode Disabling...")
        setMicroSDMode(false)
        return response
    }

    private fun setMicroSDMode(enable: Boolean) {
        WeipassLogger.log("setMicroSDMode ::: enable ::: ".plus(enable))
        return try {
            val method =
                WangPosManager.getMethod("setSdCard", Boolean::class.javaPrimitiveType)
            method!!.invoke(WangPosManager.getInstance(), enable)
            response = DeviceResponse(Result.SUCCESS, MICROSD_MODE_SETTINGS_APPLIED)
        } catch (ex: Exception) {
            WeipassLogger.log("Exception in setMicroSDMode ::: ".plus(ex.message))
        }
    }

    /**
     * Home Button Settings
     * @param disabled
     * If true --->  Home Button is disabled
     * If false ---> Home Button is enabled
     */
    override fun enableHomeButton(): DeviceResponse {
        response = DeviceResponse(Result.FAILURE, HOME_BUTTON_ENABLING_FAILED)
        WeipassLogger.log("Home Button Enabling...")
        setHomeButtonMode(false)
        return response
    }

    override fun disableHomeButton(): DeviceResponse {
        response = DeviceResponse(Result.FAILURE, HOME_BUTTON_DISABLING_FAILED)
        WeipassLogger.log("Home Button Disabling...")
        setHomeButtonMode(true)
        return response
    }

    private fun setHomeButtonMode(disabled: Boolean) {
        WeipassLogger.log("setHomeButtonMode ::: disabled ::: ".plus(disabled))
        try {
            val method =
                WangPosManager.getMethod("setPropForControlHomeKey",
                    Boolean::class.javaPrimitiveType)
            method!!.invoke(WangPosManager.getInstance(), disabled)
            response = DeviceResponse(Result.SUCCESS, HOME_BUTTON_SETTINGS_APPLIED)
        } catch (ex: Exception) {
            WeipassLogger.log("Exception in setHomeButtonMode ::: ".plus(ex.message))
        }
    }

    /**
     * Power Button Settings
     * @param disabled
     * If true --->  Power Button is disabled
     * If false ---> Power Button is enabled
     */
    override fun enablePowerButton(): DeviceResponse {
        response = DeviceResponse(Result.FAILURE, POWER_BUTTON_ENABLING_FAILED)
        WeipassLogger.log("Power Button Enabling...")
        setPowerButtonMode(false)
        return response
    }

    override fun disablePowerButton(): DeviceResponse {
        response = DeviceResponse(Result.FAILURE, POWER_BUTTON_DISABLING_FAILED)
        WeipassLogger.log("Power Button Disabling...")
        setPowerButtonMode(true)
        return response
    }

    override fun enableRecentButton(): DeviceResponse {
        return DeviceResponse(
            Result.FAILURE,
            DeviceApiConstants.MSG_FEATURE_NOT_AVAILABLE
        )
    }

    override fun disableRecentButton(): DeviceResponse {
        return DeviceResponse(
            Result.FAILURE,
            DeviceApiConstants.MSG_FEATURE_NOT_AVAILABLE
        )
    }

    override fun enableBluetooth(): DeviceResponse {
        return DeviceResponse(
            Result.FAILURE,
            DeviceApiConstants.MSG_FEATURE_NOT_AVAILABLE
        )
    }

    override fun disableBluetooth(): DeviceResponse {
        return DeviceResponse(
            Result.FAILURE,
            DeviceApiConstants.MSG_FEATURE_NOT_AVAILABLE
        )
    }

    override fun enableWifi(): DeviceResponse {
        return DeviceResponse(
            Result.FAILURE,
            DeviceApiConstants.MSG_FEATURE_NOT_AVAILABLE
        )
    }

    override fun disableWifi(): DeviceResponse {
        return DeviceResponse(
            Result.FAILURE,
            DeviceApiConstants.MSG_FEATURE_NOT_AVAILABLE
        )
    }

    private fun setPowerButtonMode(disabled: Boolean) {
        WeipassLogger.log("setPowerButtonMode ::: disabled ::: ".plus(disabled))
        try {
            val method =
                WangPosManager.getMethod("setPropForControlPowerKey",
                    Boolean::class.javaPrimitiveType)
            method!!.invoke(WangPosManager.getInstance(), disabled)
            response = DeviceResponse(Result.SUCCESS, POWER_BUTTON_SETTINGS_APPLIED)
        } catch (ex: Exception) {
            WeipassLogger.log("Exception in setPowerButtonMode ::: ".plus(ex.message))
        }
    }

}