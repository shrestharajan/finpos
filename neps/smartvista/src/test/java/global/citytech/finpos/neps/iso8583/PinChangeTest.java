package global.citytech.finpos.neps.iso8583;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import global.citytech.finposframework.utility.EncodingUtils;
import global.citytech.finposframework.utility.HexDump;

public class PinChangeTest {


    public static void main(String[] arg){
        //System.out.println(generateAdditionalData());
        trimTest();
    }

    public static void trimTest(){
        String response = "05 ";
        System.out.println(response);
        System.out.println(Integer.valueOf(response.trim()));
    }

    private static String generateAdditionalData(){
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(("014").getBytes(StandardCharsets.UTF_8));
            outputStream.write(EncodingUtils.hex2Bytes(stringToHex("002")));
            outputStream.write(("008").getBytes(StandardCharsets.UTF_8));
            //07 16 AB 90 36 75 9C DF
          //  outputStream.write( hexStringToByteArray ("0716AB9036759CDF"));
//            outputStream.write((byte) Integer.parseInt("07", 16));
//            outputStream.write((byte) Integer.parseInt("16", 16));
//            outputStream.write((byte) Integer.parseInt("AB", 16));
//            outputStream.write((byte) Integer.parseInt("90", 16));
//            outputStream.write((byte) Integer.parseInt("36", 16));
//            outputStream.write((byte) Integer.parseInt("75", 16));
//            outputStream.write((byte) Integer.parseInt("9C", 16));
//            outputStream.write((byte) Integer.parseInt("DF", 16));
            System.out.println("OUTPUT STREAM ::"+ outputStream);
            System.out.println(HexDump.dumpHexString(outputStream.toByteArray()));
            return outputStream.toString();
        }catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static String stringToHex(String input) {
        StringBuilder hexString = new StringBuilder();

        for (char ch : input.toCharArray()) {
            String hex = Integer.toHexString(ch);
            hexString.append(hex);
        }

        return hexString.toString();
    }

}
