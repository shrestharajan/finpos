package global.citytech.payment.sdk.core;

/**
 * Created by BikashShrestha on 5/31/19.
 * Modified by RishavChudal on 06/12/21.
 */
public class PaymentRequest {
    private final double transactionAmount;
    private final PurchaseType purchaseType;
    private final double transactionCashbackAmount;
    private final String transactionCurrencyCode;
    private final String retrievalReferenceNumber;
    private final String additionalData;


    private PaymentRequest(Builder builder) {
        this.transactionAmount = builder.transactionAmount;
        this.purchaseType = builder.purchaseType;
        this.transactionCashbackAmount = builder.transactionCashbackAmount;
        this.transactionCurrencyCode = builder.transactionCurrencyCode;
        this.retrievalReferenceNumber = builder.retrievalReferenceNumber;
        this.additionalData = builder.additionalData;
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public PurchaseType getPurchaseType() {
        return purchaseType;
    }

    public double getTransactionCashbackAmount() {
        return transactionCashbackAmount;
    }

    public String getTransactionCurrencyCode() {
        return transactionCurrencyCode;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public static class Builder {
        private final double transactionAmount;
        private final PurchaseType purchaseType;
        private double transactionCashbackAmount;
        private String transactionCurrencyCode;
        private String retrievalReferenceNumber;
        private String additionalData;

        private Builder(
                double transactionAmount,
                PurchaseType purchaseType,
                double vatAmount,
                double vatRefundAmount
        ) {
            this.transactionAmount = transactionAmount;
            this.purchaseType = purchaseType;
        }

        private Builder(
                double transactionAmount,
                PurchaseType purchaseType
        ) {
            this.transactionAmount = transactionAmount;
            this.purchaseType = purchaseType;
        }

        public static Builder getInstance(
                double transactionAmount,
                PurchaseType purchaseType,
                double vatAmount,
                double vatRefundAmount
        ) {
            return new Builder(transactionAmount, purchaseType, vatAmount, vatRefundAmount);
        }

        public static Builder getInstance(
                double transactionAmount,
                PurchaseType purchaseType
        ) {
            return new Builder(transactionAmount, purchaseType);
        }

        public Builder withCashbackAmount(double transactionCashbackAmount) {
            this.transactionCashbackAmount = transactionCashbackAmount;
            return this;
        }

        public Builder withCurrencyCode(String transactionCurrencyCode) {
            this.transactionCurrencyCode = transactionCurrencyCode;
            return this;
        }

        public Builder withRetrievalReferenceNumber(String retrievalReferenceNumber) {
            this.retrievalReferenceNumber = retrievalReferenceNumber;
            return this;
        }

        public Builder withAdditionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public PaymentRequest build() {
            return new PaymentRequest(this);
        }
    }
}
