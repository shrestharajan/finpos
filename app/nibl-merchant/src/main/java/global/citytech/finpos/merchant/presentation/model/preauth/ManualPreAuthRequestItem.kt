package global.citytech.finpos.merchant.presentation.model.preauth

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

/**
 * Created by Rishav Chudal on 11/3/20.
 */
data class ManualPreAuthRequestItem(
    val amount: BigDecimal? = null,
    val transactionType: TransactionType? = null,
    val cardNumber: String? = "",
    val expiryDate: String? = "",
    val cvv: String? = ""
)