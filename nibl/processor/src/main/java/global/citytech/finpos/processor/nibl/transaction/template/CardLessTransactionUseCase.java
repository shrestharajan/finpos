package global.citytech.finpos.processor.nibl.transaction.template;

import java.math.BigDecimal;
import java.util.List;

import global.citytech.finpos.processor.nibl.transaction.TransactionRequestModel;
import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.utility.DateUtils;

/**
 * Created by Unique Shakya on 3/10/2021.
 */
public abstract class CardLessTransactionUseCase extends TransactionUseCaseTemplate {

    protected TransactionLog originalTransactionLog;
    protected BigDecimal totalTransactionAmount = BigDecimal.ZERO;

    protected CardLessTransactionUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    protected abstract TransactionLog retrieveOriginalTransactionLog();

    protected abstract void validateTransactionLog();

    protected abstract void setTotalTransactionAmount();

    @Override
    public TransactionResponseModel execute(TransactionRequestModel request) {
        this.request = request;
        this.originalTransactionLog = this.retrieveOriginalTransactionLog();
        if (this.originalTransactionLog == null)
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_FOUND);
        this.validateTransactionLog();
        this.setTotalTransactionAmount();
        return super.execute(request);
    }

    @Override
    protected void retrieveCardDetails(List<CardType> allowedCardTypes, boolean isForceContactCardPrompt) {
        this.readCardResponse = this.originalTransactionLog.getReadCardResponse();
        this.readCardResponse.getCardDetails().setAmount(this.totalTransactionAmount);
        this.readCardResponse.getCardDetails().setCashBackAmount(this.transactionRequest.getAdditionalAmount());
        this.readCardResponse.getCardDetails().setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
    }

    @Override
    protected void processCard() {
        // not needed for card less transaction
    }

    @Override
    protected void processOnlineResultToCard(IsoMessageResponse isoMessageResponse) {
        // not needed
    }

    @Override
    protected void processUnableToGoOnlineResultToCard() {
        // not needed
    }

    @Override
    protected boolean checkTransactionDeclinedByCard(IsoMessageResponse isoMessageResponse) {
        return false;
    }

    @Override
    protected void notifyWithLed(boolean isApproved) {
        // not needed
    }

    @Override
    protected boolean isForceContactCardPromptActionCode(IsoMessageResponse isoMessageResponse) {
        return false;
    }

    @Override
    protected String getOriginalPosConditionCode() {
        return this.originalTransactionLog.getPosConditionCode();
    }

    @Override
    protected PosEntryMode getOriginalPosEntryMode() {
        return this.originalTransactionLog.getPosEntryMode();
    }
}
