package global.citytech.finpos.merchant.domain.repository.core.ministatement

import global.citytech.finpos.merchant.domain.model.ministatement.MiniStatementResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.ministatement.MiniStatementRequestItem
import io.reactivex.Observable

interface MiniStatementRepository {
    fun generateMiniStatementRequest(
        configurationItem: ConfigurationItem,
        miniStatementRequestItem: MiniStatementRequestItem
    ): Observable<MiniStatementResponseEntity>
}