package global.citytech.finpos.neps.iso8583.requestsender.refund;

import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transactiontypesenders.RefundIsoMessageUpdater;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;

public abstract class RefundRequestSender extends TransactionRequestSender {
    public RefundRequestSender(RequestContext context) {
        super(context);
    }

    @Override
    protected TransactionTypeISOMessageUpdater getTransactionTypeSender() {
        return new RefundIsoMessageUpdater();
    }

    @Override
    protected MTI.MessageFunction getMessageFunctionRequest() {
        return MTI.MessageFunction.Request;
    }

    @Override
    protected MTI.MessageClass getMessageClass() {
        return MTI.MessageClass.Financial;
    }


}
