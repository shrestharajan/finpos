package global.citytech.finposframework.iso8583;

import java.nio.charset.Charset;

/**
 * * This class represent MTI of ISO8583
 *
 * <p>This is a 4 digit numeric field, which classifies the high level function of the message. A
 * Message Type Indicator includes the ISO 8583 version, the Message Class, Message Sub-class, and
 * Transaction Originator
 *
 * <p>User: Surajchhetry Date: 8/1/18 Time: 1:56 PM
 */
public class MTI {

  private int version;
  private int messageClass;
  private int subClass;
  private int transactionOriginator;
  private String mtiMsg = "";

  public int getVersion() {
    return version;
  }

  public int getMessageClass() {
    return messageClass;
  }

  public int getSubClass() {
    return subClass;
  }

  public int getTransactionOriginator() {
    return transactionOriginator;
  }

  /**
   * *
   *
   * @param version
   *     <p>Version of ISO 8583
   * @param messageClass
   *     <p>Indicate Which message we are going to send.
   * @param function
   *     <p>function of the Message it could be Request or Response
   * @param transactionOriginator
   */


  public MTI(
      Version version,
      MessageClass messageClass,
      MessageFunction function,
      Originator transactionOriginator) {
    this.version = version.ordinal();
    this.messageClass = messageClass.value;
    this.subClass = function.value;
    this.transactionOriginator = transactionOriginator.ordinal();
    this.mtiMsg = this.version + "" + this.messageClass + "" + this.subClass + "" + this.transactionOriginator;
  }

  public MTI(String mtiMsg) {
    this.mtiMsg = mtiMsg;
    this.version = Integer.parseInt(mtiMsg.substring(0,1));
    this.messageClass = Integer.parseInt(mtiMsg.substring(1,2));
    this.subClass = Integer.parseInt(mtiMsg.substring(2,3));
    this.transactionOriginator = Integer.parseInt(mtiMsg.substring(3,4));
  }

  @Override
  public String toString() {
    if (mtiMsg.length() > 0) {
      return mtiMsg;
    }
    return this.version + "" + messageClass + "" + this.subClass + "" + this.transactionOriginator;
  }

  public byte[] getAsBytes() {
    return toString().getBytes(Charset.forName("UTF-8"));
  }

  public enum Version {
    Version_1987,
    Version_1993
  }

  /**
   * < p> class of the Message
   *
   * @author suraj
   */
  public enum MessageClass {
    Authorization(1),
    Financial(2),
    File_Actions(3),
    Reversal(4),
    Reconciliation(5),
    Administrative(6),
    Fee_Collection(7),
    Network_Management(8);

    private int value;

    MessageClass(int value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return String.valueOf(this.value);
    }
  }

  /**
   * < p> function of the Message
   *
   * @author suraj
   */
  public enum MessageFunction {
    Request(0),
    Request_Response(1),
    Advice(2),
    Advice_Response(3),
    Notification(4),
    Response_Acknowledgment(8),
    Negative_acknowledgment(9);

    private int value;

    MessageFunction(int value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return String.valueOf(this.value);
    }
  }

  /**
   * < p> who began the communication
   *
   * @author suraj
   */
  public enum Originator {
    Acquirer,
    Acquirer_Repeat,
    Issuer,
    Issuer_Repeat,
    Other,
    Other_Repeat
  }
}
