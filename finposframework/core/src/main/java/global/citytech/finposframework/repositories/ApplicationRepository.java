package global.citytech.finposframework.repositories;


import java.util.List;

import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.TransactionType;

/**
 * Created by Rishav Chudal on 9/15/20.
 */
public interface ApplicationRepository {
    CardSchemeRetrieveResponse retrieveCardScheme(
            CardSchemeRetrieveRequest cardSchemeRetrieveRequest
    );

    AidRetrieveResponse retrieveAid(AidRetrieveRequest aidRetrieveRequest);

    EmvKeyRetrieveResponse retrieveEmvKey(EmvKeyRetrieveRequest emvKeyRetrieveRequest);

    EmvParametersRequest retrieveEmvParameterRequest();

    String getMessageTextByActionCode(int actionCode);

    List<CardSchemeType> getSupportedCardSchemes();

    String retrieveFromAdditionalDataEmvParameters(String attribute);

    boolean isTransactionTypeEnabled(TransactionType transactionType);
}
