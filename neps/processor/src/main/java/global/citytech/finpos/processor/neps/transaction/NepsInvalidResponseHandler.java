package global.citytech.finpos.processor.neps.transaction;

import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandler;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;

/**
 * Created by Saurav Ghimire on 1/22/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class NepsInvalidResponseHandler implements InvalidResponseHandler {

    @Override
    public InvalidResponseHandlerResponse isInvalidResponse(IsoMessageResponse isoMessageResponse) {
        Iso8583Msg requestMessage = isoMessageResponse.getRequestPayload().getRequestMsg();
        Iso8583Msg responseMessage = isoMessageResponse.getMsg();
        boolean isInvalid = false;
        String message = "Valid Message";
        if (this.isMismatchDe02(requestMessage, responseMessage)) {
            isInvalid = true;
            message = "PAN Differs";
        }
        if (this.isMismatchDe03(requestMessage, responseMessage)) {
            isInvalid = true;
            message = "Proc. Code Differs";
        }
        if (this.isMismatchDe04(requestMessage, responseMessage)) {
            isInvalid = true;
            message = "Amount Mismatch";
        }
        if (this.isMismatchDe11(requestMessage, responseMessage)) {
            isInvalid = true;
            message = "STAN Mismatch";
        }
        if (this.isMismatchDe41(requestMessage, responseMessage)) {
            isInvalid = true;
            message = "Term ID Differs";
        }
        return new InvalidResponseHandlerResponse(isInvalid, message);
    }

    private boolean isMismatchMti(Iso8583Msg requestMessage, Iso8583Msg responseMessage) {
        MTI requestMti = requestMessage.getMti();
        MTI responseMti = responseMessage.getMti();
        return this.invalidMti(requestMti, responseMti);
    }

    private boolean invalidMti(MTI requestMti, MTI responseMti) {
        if (requestMti.getVersion() != responseMti.getVersion())
            return true;
        if (requestMti.getMessageClass() != responseMti.getMessageClass())
            return true;
        return this.mtiMessageFunctionMismatch(requestMti.getSubClass(), responseMti.getSubClass());
    }

    private boolean mtiMessageFunctionMismatch(int requestSubClass, int responseSubClass) {
        if (requestSubClass == 0)
            return responseSubClass != 1;
        if (requestSubClass == 2)
            return responseSubClass != 3;
        if (requestSubClass == 4)
            return responseSubClass != 4;
        return false;
    }

    private boolean isMismatchDe02(Iso8583Msg requestMessage, Iso8583Msg responseMessage) {
        if (responseMessage.getDataElementByIndex(2).isPresent())
            return this.isMismatchDataElement(requestMessage, responseMessage, 2);
        return false;
    }

    private boolean isMismatchDe03(Iso8583Msg requestMessage, Iso8583Msg responseMessage) {
        return this.isMismatchDataElement(requestMessage, responseMessage, 3);
    }

    private boolean isMismatchDe04(Iso8583Msg requestMessage, Iso8583Msg responseMessage) {
        if (responseMessage.getDataElementByIndex(4).isPresent())
            return this.isMismatchDataElement(requestMessage, responseMessage, 4);
        return false;
    }

    private boolean isMismatchDe11(Iso8583Msg requestMessage, Iso8583Msg responseMessage) {
        return this.isMismatchDataElement(requestMessage, responseMessage, 11);
    }

    private boolean isMismatchDe41(Iso8583Msg requestMessage, Iso8583Msg responseMessage) {
        return this.isMismatchDataElement(requestMessage, responseMessage, 41);
    }

    private boolean isMismatchDataElement(Iso8583Msg requestMessage, Iso8583Msg responseMessage, int index) {
        return !requestMessage.getDataElementByIndex(index).get().getAsString()
                .equals(responseMessage.getDataElementByIndex(index).get().getAsString());
    }
}
