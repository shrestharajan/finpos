package global.citytech.finpos.processor.nibl.transaction;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier;
import global.citytech.finpos.processor.nibl.transaction.card.CardSchemeRetriever;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.repositories.AidRetrieveResponse;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.CardSchemeRetrieveResponse;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.PICCWave;
import global.citytech.finposframework.usecases.IdentifyCardResponse;
import global.citytech.finposframework.usecases.MagneticSwipe;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

/**
 * Created by Unique Shakya on 10/9/2020.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({TransactionRepository.class, ApplicationRepository.class, ReadCardResponse.class,
        TransactionRequest.class, PICCWave.class, MagneticSwipe.class, CardDetails.class,
        global.citytech.finpos.processor.nibl.transaction.card.CardSchemeRetriever.class, global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier.class})
public class CardIdentifierTest {

    TransactionRepository transactionRepository;
    ApplicationRepository applicationRepository;
    ReadCardResponse readCardResponse;
    TransactionRequest transactionRequest;
    PICCWave PICCWave;
    MagneticSwipe magneticSwipe;
    CardDetails cardDetails;
    global.citytech.finpos.processor.nibl.transaction.card.CardSchemeRetriever cardSchemeRetriever;

    private Logger logger = Logger.getLogger(CardIdentifierTest.class.getName());

    @Before
    public void setup() {
        transactionRepository = PowerMockito.mock(TransactionRepository.class);
        applicationRepository = PowerMockito.mock(ApplicationRepository.class);
        readCardResponse = PowerMockito.mock(ReadCardResponse.class);
        transactionRequest = PowerMockito.mock(TransactionRequest.class);
        PowerMockito.mockStatic(PICCWave.class);
        PICCWave = PowerMockito.mock(PICCWave.class);
        PowerMockito.mockStatic(MagneticSwipe.class);
        magneticSwipe = PowerMockito.mock(MagneticSwipe.class);
        cardDetails = PowerMockito.mock(CardDetails.class);
        cardSchemeRetriever = PowerMockito.mock(global.citytech.finpos.processor.nibl.transaction.card.CardSchemeRetriever.class);
    }

    @Test
    public void identifyTestWhenReadCardResponseIsNull() {
        PosException e = assertThrows(PosException.class, () -> {
            global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(transactionRepository, applicationRepository);
            IdentifyCardResponse identifyCardResponse = cardIdentifier
                    .identify(null);
        });

        assertEquals(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD, e.getPosError());
    }

    @Test
    public void identifyTestWhenContactlessWaveAgain() {
        PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.WAVE_AGAIN);
        PowerMockito.when(this.PICCWave.isPICCWaveAgainAllowed())
                .thenReturn(true);
        PowerMockito.when(PICCWave.getInstance())
                .thenReturn(this.PICCWave);
        PowerMockito.doNothing().when(this.PICCWave).onPICCWaveFailure();

        global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
        IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(this.readCardResponse);

        assertTrue(identifyCardResponse.isWaveAgain());
    }

    @Test
    public void identifyTestWhenContactlessWaveAgainLimitReached() {
        PosException posException = assertThrows(PosException.class, () -> {
            PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.WAVE_AGAIN);
            PowerMockito.when(this.PICCWave.isPICCWaveAgainAllowed())
                    .thenReturn(false);
            PowerMockito.when(PICCWave.getInstance())
                    .thenReturn(this.PICCWave);
            PowerMockito.doNothing().when(this.PICCWave).onPICCWaveFailure();

            global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
            IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                    this.readCardResponse);
        });
        assertEquals(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD, posException.getPosError());
    }

    @Test
    public void identifyTestWhenApplicationBlocked() {
        PosException exception = assertThrows(PosException.class, () -> {
            PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.APPLICATION_BLOCKED);
            global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
            IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                    this.readCardResponse);
        });

        assertEquals(PosError.DEVICE_ERROR_APPLICATION_BLOCKED, exception.getPosError());
    }

    @Test
    public void identifyTestWhenUserCancelled() {
        PosException exception = assertThrows(PosException.class, () -> {
            PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.USER_CANCELLED);
            global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
            IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                    this.readCardResponse);
        });

        assertEquals(PosError.DEVICE_ERROR_USER_CANCELLED, exception.getPosError());
    }

    @Test
    public void identifyTestWhenPinPadTimeout() {
        PosException exception = assertThrows(PosException.class, () -> {
            PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.TIMEOUT);
            global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
            IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                    this.readCardResponse);
        });

        assertEquals(PosError.DEVICE_ERROR_PINPAD_TIMEOUT, exception.getPosError());
    }

    @Test
    public void identifyTestWhenPinPadError() {
        PosException exception = assertThrows(PosException.class, () -> {
            PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.PINPAD_ERROR);
            global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
            IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                    this.readCardResponse);
        });

        assertEquals(PosError.DEVICE_ERROR_PINPAD_ERROR, exception.getPosError());
    }

    @Test
    public void identifyTestWhenProcessingError() {
        PosException exception = assertThrows(PosException.class, () -> {
            PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.PROCESSING_ERROR);
            global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
            IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                    this.readCardResponse);
        });

        assertEquals(PosError.DEVICE_ERROR_PROCESSING_ERROR, exception.getPosError());
    }

    @Test
    public void identifyTestWhenMagneticSwipeAgain() {
        PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.SWIPE_AGAIN);
        PowerMockito.when(this.magneticSwipe.isMagneticSwipeAgainAllowed())
                .thenReturn(true);
        PowerMockito.when(MagneticSwipe.getInstance())
                .thenReturn(this.magneticSwipe);
        PowerMockito.doNothing().when(this.magneticSwipe).onMagneticSwipeFailure();

        global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
        IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                this.readCardResponse);

        assertTrue(identifyCardResponse.isSwipeAgain());
    }

    @Test
    public void identifyTestWhenMagneticSwipeAgainLimitReached() {
        PosException exception = assertThrows(PosException.class, () -> {
            PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.SWIPE_AGAIN);
            PowerMockito.when(this.magneticSwipe.isMagneticSwipeAgainAllowed())
                    .thenReturn(false);
            PowerMockito.when(MagneticSwipe.getInstance())
                    .thenReturn(this.magneticSwipe);
            PowerMockito.doNothing().when(this.magneticSwipe).onMagneticSwipeFailure();

            global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
            IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                    this.readCardResponse);
        });

        assertEquals(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD, exception.getPosError());
    }

    @Test
    public void identifyTestWhenCardReadFailure() {
        PosException exception = assertThrows(PosException.class, () -> {
            PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.FAILURE);
            PowerMockito.when(MagneticSwipe.getInstance())
                    .thenReturn(this.magneticSwipe);
            PowerMockito.doNothing().when(this.magneticSwipe).onMagneticSwipeSuccess();

            global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
            IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                    this.readCardResponse);
        });

        assertEquals(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD, exception.getPosError());
    }

    @Test
    public void identifyTestWhenCardDetailsIsNull() {
        PosException exception = assertThrows(PosException.class, () -> {
            PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.SUCCESS);
            PowerMockito.when(this.readCardResponse.getCardDetails()).thenReturn(null);
            PowerMockito.when(MagneticSwipe.getInstance())
                    .thenReturn(this.magneticSwipe);
            PowerMockito.doNothing().when(this.magneticSwipe).onMagneticSwipeSuccess();

            global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
            IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                    this.readCardResponse);
        });

        assertEquals(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD, exception.getPosError());
    }

    @Test
    public void identifyTestWhenCanNotReadTrack2Data() {
        PosException exception = assertThrows(PosException.class, () -> {
            PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.SUCCESS);
            PowerMockito.when(this.cardDetails.getCardType()).thenReturn(CardType.MAG);
            PowerMockito.when(this.cardDetails.getTrackTwoData()).thenReturn("");
            PowerMockito.when(this.readCardResponse.getCardDetails()).thenReturn(this.cardDetails);
            PowerMockito.when(MagneticSwipe.getInstance())
                    .thenReturn(this.magneticSwipe);
            PowerMockito.doNothing().when(this.magneticSwipe).onMagneticSwipeSuccess();

            global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
            IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                    this.readCardResponse);
        });

        assertEquals(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD, exception.getPosError());
    }

    @Test
    public void identifyTestWhenInvalidCardByLuhnCheck() {
        PosException exception = assertThrows(PosException.class, () -> {
            PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.SUCCESS);
            PowerMockito.when(this.cardDetails.getCardType()).thenReturn(CardType.ICC);
            PowerMockito.when(this.cardDetails.getAid()).thenReturn("A0000002282010");
            PowerMockito.when(this.cardDetails.getTrackTwoData()).thenReturn("");
            PowerMockito.when(this.cardDetails.getPrimaryAccountNumber()).thenReturn("4249720020000235");
            PowerMockito.when(this.readCardResponse.getCardDetails()).thenReturn(this.cardDetails);
            PowerMockito.when(MagneticSwipe.getInstance())
                    .thenReturn(this.magneticSwipe);
            PowerMockito.doNothing().when(this.magneticSwipe).onMagneticSwipeSuccess();
            PowerMockito.when(this.applicationRepository.retrieveAid(any())).thenReturn(new AidRetrieveResponse("VISA CREDIT"));
            PowerMockito.when(this.applicationRepository.retrieveCardScheme(any())).thenReturn(new CardSchemeRetrieveResponse("1"));

            global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
            IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                    this.readCardResponse);
        });

        assertEquals(PosError.DEVICE_ERROR_INVALID_CARD, exception.getPosError());
    }

    @Test
    public void identifyTestWhenInvalidCardButLuhnCheckNotRequired() {
        PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.SUCCESS);
        PowerMockito.when(this.cardDetails.getCardType()).thenReturn(CardType.ICC);
        PowerMockito.when(this.cardDetails.getAid()).thenReturn("A0000002282010");
        PowerMockito.when(this.cardDetails.getTrackTwoData()).thenReturn("");
        PowerMockito.when(this.cardDetails.getPrimaryAccountNumber()).thenReturn("4249720020000223");
        PowerMockito.when(this.readCardResponse.getCardDetails()).thenReturn(this.cardDetails);
        PowerMockito.when(MagneticSwipe.getInstance())
                .thenReturn(this.magneticSwipe);
        PowerMockito.doNothing().when(this.magneticSwipe).onMagneticSwipeSuccess();
        PowerMockito.when(this.applicationRepository.retrieveAid(any())).thenReturn(new AidRetrieveResponse("VISA CREDIT"));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(any())).thenReturn(new CardSchemeRetrieveResponse("0"));
        PowerMockito.doCallRealMethod().when(this.cardDetails).setCardScheme(CardSchemeType.VISA_CREDIT);

        global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
        IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                this.readCardResponse);
        this.logger.log("::: CARD SCHEME IN IDENTIFY CARD RESPONSE ::: " + identifyCardResponse.getCardDetails().getCardScheme());
        assertNotNull(identifyCardResponse);
    }

    @Test
    public void identifyTestWhenInvalidCardButLuhnCheckRequiredIsEmpty() {
        PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.SUCCESS);
        PowerMockito.when(this.cardDetails.getCardType()).thenReturn(CardType.ICC);
        PowerMockito.when(this.cardDetails.getAid()).thenReturn("A0000002282010");
        PowerMockito.when(this.cardDetails.getTrackTwoData()).thenReturn("");
        PowerMockito.when(this.cardDetails.getPrimaryAccountNumber()).thenReturn("4249720020000223");
        PowerMockito.when(this.readCardResponse.getCardDetails()).thenReturn(this.cardDetails);
        PowerMockito.when(MagneticSwipe.getInstance())
                .thenReturn(this.magneticSwipe);
        PowerMockito.doNothing().when(this.magneticSwipe).onMagneticSwipeSuccess();
        PowerMockito.when(this.applicationRepository.retrieveAid(any())).thenReturn(new AidRetrieveResponse("VISA CREDIT"));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(any())).thenReturn(new CardSchemeRetrieveResponse(""));
        PowerMockito.doCallRealMethod().when(this.cardDetails).setCardScheme(CardSchemeType.VISA_CREDIT);

        global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
        IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                this.readCardResponse);
        this.logger.log("::: CARD SCHEME IN IDENTIFY CARD RESPONSE ::: " + identifyCardResponse.getCardDetails().getCardScheme());
        assertNotNull(identifyCardResponse);
    }

    @Test
    public void identifyTestWhenValidIccCard() {
        PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.SUCCESS);
        PowerMockito.when(this.cardDetails.getCardType()).thenReturn(CardType.ICC);
        PowerMockito.when(this.cardDetails.getAid()).thenReturn("A0000002282010");
        PowerMockito.when(this.cardDetails.getTrackTwoData()).thenReturn("");
        PowerMockito.when(this.cardDetails.getPrimaryAccountNumber()).thenReturn("4249720020000234");
        PowerMockito.when(this.readCardResponse.getCardDetails()).thenReturn(this.cardDetails);
        PowerMockito.when(MagneticSwipe.getInstance())
                .thenReturn(this.magneticSwipe);
        PowerMockito.doNothing().when(this.magneticSwipe).onMagneticSwipeSuccess();
        PowerMockito.when(this.applicationRepository.retrieveAid(any())).thenReturn(new AidRetrieveResponse("VISA CREDIT"));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(any())).thenReturn(new CardSchemeRetrieveResponse("1"));
        PowerMockito.doCallRealMethod().when(this.cardDetails).setCardScheme(CardSchemeType.VISA_CREDIT);

        global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier(this.transactionRepository, this.applicationRepository);
        IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                this.readCardResponse);
        this.logger.log("::: CARD SCHEME IN IDENTIFY CARD RESPONSE ::: " + identifyCardResponse.getCardDetails().getCardScheme());
        assertNotNull(identifyCardResponse);
    }

    @Test
    public void identifyTestWhenValidMagCard() throws Exception {
        PowerMockito.when(this.readCardResponse.getResult()).thenReturn(Result.SUCCESS);
        PowerMockito.when(this.cardDetails.getCardType()).thenReturn(CardType.MAG);
        PowerMockito.when(this.cardDetails.getAid()).thenReturn("A0000002282010");
        PowerMockito.when(this.cardDetails.getTrackTwoData()).thenReturn("4249720020000234D2212");
        PowerMockito.when(this.cardDetails.getPrimaryAccountNumber()).thenReturn("4249720020000234");
        PowerMockito.when(this.readCardResponse.getCardDetails()).thenReturn(this.cardDetails);
        PowerMockito.when(MagneticSwipe.getInstance())
                .thenReturn(this.magneticSwipe);
        PowerMockito.doNothing().when(this.magneticSwipe).onMagneticSwipeSuccess();
        PowerMockito.when(this.applicationRepository.retrieveAid(any())).thenReturn(new AidRetrieveResponse("VISA CREDIT"));
        PowerMockito.when(this.applicationRepository.retrieveCardScheme(any())).thenReturn(new CardSchemeRetrieveResponse("1"));
        PowerMockito.doCallRealMethod().when(this.cardDetails).setCardScheme(CardSchemeType.VISA_CREDIT);
        PowerMockito.when(this.cardSchemeRetriever.retrieveCardScheme(anyString())).thenReturn(CardSchemeType.VISA_CREDIT);
        PowerMockito.whenNew(CardSchemeRetriever.class).withAnyArguments().thenReturn(this.cardSchemeRetriever);

        global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new CardIdentifier(this.transactionRepository, this.applicationRepository);
        IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(
                this.readCardResponse);
        this.logger.log("::: CARD SCHEME IN IDENTIFY CARD RESPONSE ::: " + identifyCardResponse.getCardDetails().getCardScheme());
        assertNotNull(identifyCardResponse);
    }

}
