package global.citytech.finpos.merchant.domain.usecase.qr
/**
 * @author sachin
 */
interface QrDataObject {
    fun getFieldId():String
    fun getFieldContentLength():String
    fun getFieldContent():Any
}