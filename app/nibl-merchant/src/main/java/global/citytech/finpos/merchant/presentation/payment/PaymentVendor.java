package global.citytech.finpos.merchant.presentation.payment;

import java.util.List;

/**
 * Created by Rishav Chudal on 6/20/21.
 */
public class PaymentVendor {
    public String applicationIdentifier;
    public String vendorName;
    public String vendorPublicKey;
    public String finposPrivateKey;
    public String finposPublicKey;
    public List<PaymentFeature> features;
}
