package global.citytech.neps.datasource.pref

import android.content.Context
import android.content.SharedPreferences
import global.citytech.easydroid.core.preference.SecurePreference

object PreferenceManager {

    private lateinit var pref: SecurePreference

    private val PREF_NAME = "neps.merchant"
    private val IS_CONFIGURED = "is_configured"

    fun init(context: Context) {
        pref = SecurePreference(context, PREF_NAME)
    }

    fun setIsConfigured(isConfigured: Boolean) {
        pref.saveBoolean(IS_CONFIGURED,isConfigured)
    }

    fun isConfigured() = pref.retrieveBoolean(IS_CONFIGURED,false)

    fun clear(){
        pref.clear()
    }
}