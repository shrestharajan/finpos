package global.citytech.finposframework.usecases.transaction.data;

public class InquiryRequest {
    private String date;
    private String description;
    private String dbCrIndicator;
    private String amount;
    private String cashInAmount;
    private String availableBalance;
    private String currentBalance;
}
