package global.citytech.finpos.processor.nibl.batchupload;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.hardware.io.printer.Printable;
import global.citytech.finposframework.hardware.io.printer.PrinterRequest;
import global.citytech.finposframework.hardware.io.printer.PrinterService;

import static global.citytech.finposframework.utility.ReceiptUtils.addDoubleColumnString;
import static global.citytech.finposframework.utility.ReceiptUtils.addNullableValueDoubleColumnString;
import static global.citytech.finposframework.utility.ReceiptUtils.addSingleColumnString;

/**
 * Created by Unique Shakya on 5/14/2021.
 */
public class BatchUploadReceiptPrintHandler {

    private PrinterService printerService;

    public BatchUploadReceiptPrintHandler(PrinterService printerService) {
        this.printerService = printerService;
    }

    public void printBatchUploadReceipt(BatchUploadReceipt batchUploadReceipt, boolean printHeader,
                                        boolean feedPaperAfterFinish, boolean isBatchClear) {
        PrinterRequest printerRequest = this.prepareBatchUploadReceipt(batchUploadReceipt, printHeader,
                feedPaperAfterFinish, isBatchClear);
        printerRequest.setFeedPaperAfterFinish(feedPaperAfterFinish);
        this.printerService.print(printerRequest);
    }

    private PrinterRequest prepareBatchUploadReceipt(BatchUploadReceipt batchUploadReceipt, boolean printHeader,
                                                     boolean feedPaperAfterFinish, boolean isBatchClear) {
        List<Printable> printableList = new ArrayList<>();
        if (printHeader) {
            addSingleColumnString(printableList, BatchUploadReceiptLabels.DIVIDER, BatchUploadReceiptStyle.DIVIDER.getStyle());
            if (isBatchClear) {
                addSingleColumnString(printableList, BatchUploadReceiptLabels.BATCH_CLEAR, BatchUploadReceiptStyle.BATCH_UPLOAD_MESSAGE.getStyle());
            } else {
                addSingleColumnString(printableList, BatchUploadReceiptLabels.BATCH_UPLOAD, BatchUploadReceiptStyle.BATCH_UPLOAD_MESSAGE.getStyle());
            }
            addSingleColumnString(printableList, BatchUploadReceiptLabels.DIVIDER, BatchUploadReceiptStyle.DIVIDER.getStyle());
            if (isBatchClear)
                addDoubleColumnString(printableList, BatchUploadReceiptLabels.BATCH_NUMBER, batchUploadReceipt.getBatchNumber(), BatchUploadReceiptStyle.REPORT_LABEL.getStyle());
            addDoubleColumnString(printableList, BatchUploadReceiptLabels.CARD_NUMBER, BatchUploadReceiptLabels.CARD_SCHEME, BatchUploadReceiptStyle.REPORT_LABEL.getStyle());
            addDoubleColumnString(printableList, BatchUploadReceiptLabels.INVOICE_NUMBER, BatchUploadReceiptLabels.APPROVAL_CODE, BatchUploadReceiptStyle.REPORT_LABEL.getStyle());
            addDoubleColumnString(printableList, BatchUploadReceiptLabels.TRANSACTION_TYPE, BatchUploadReceiptLabels.AMOUNT, BatchUploadReceiptStyle.REPORT_LABEL.getStyle());
            addSingleColumnString(printableList, BatchUploadReceiptLabels.RESPONSE_CODE, BatchUploadReceiptStyle.REPORT_LABEL.getStyle());
            addDoubleColumnString(printableList, BatchUploadReceiptLabels.TRANSACTION_DATE, BatchUploadReceiptLabels.TRANSACTION_TIME, BatchUploadReceiptStyle.REPORT_LABEL.getStyle());
        }
        addSingleColumnString(printableList, BatchUploadReceiptLabels.DIVIDER, BatchUploadReceiptStyle.DIVIDER.getStyle());
        addNullableValueDoubleColumnString(printableList, batchUploadReceipt.getCardNumber(), batchUploadReceipt.getCardScheme(), BatchUploadReceiptStyle.REPORT_VALUE.getStyle());
        addNullableValueDoubleColumnString(printableList, batchUploadReceipt.getInvoiceNumber(), batchUploadReceipt.getApprovalCode(), BatchUploadReceiptStyle.REPORT_VALUE.getStyle());
        addNullableValueDoubleColumnString(printableList, batchUploadReceipt.getTransactionType(), batchUploadReceipt.getAmount(), BatchUploadReceiptStyle.REPORT_VALUE.getStyle());
        addSingleColumnString(printableList, batchUploadReceipt.getResponseCode(), BatchUploadReceiptStyle.REPORT_VALUE.getStyle());
        addNullableValueDoubleColumnString(printableList, batchUploadReceipt.getTransactionDate(), batchUploadReceipt.getTransactionTime(), BatchUploadReceiptStyle.REPORT_VALUE.getStyle());
        if (feedPaperAfterFinish)
            addSingleColumnString(printableList, BatchUploadReceiptLabels.BATCH_REPORT_END, BatchUploadReceiptStyle.BATCH_UPLOAD_MESSAGE.getStyle());
        return new PrinterRequest(printableList);
    }
}
