package global.citytech.finpos.merchant.presentation.model.cashin

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

data class CashInRequestItem (
    val transactionType: TransactionType? = null,
    val amount: BigDecimal? = null
)