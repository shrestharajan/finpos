package global.citytech.finpos.processor.nibl.cashin;

import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;

public class CashInResponse extends TransactionResponseModel {
    private String debugRequestMessage;
    private String debugResponseMessage;
    private String message;
    private String stan;
    private boolean approved;
    private boolean shouldPrintCustomerCopy;


    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }


    public String getMessage() {
        return message;
    }

    public String getStan() {
        return stan;
    }


    public boolean isApproved() {
        return approved;
    }

    public boolean isShouldPrintCustomerCopy() {
        return shouldPrintCustomerCopy;
    }

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private String message;
        private String stan;
        private boolean approved;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static CashInResponse.Builder newInstance() {
            return new CashInResponse.Builder();
        }

        public CashInResponse.Builder withDebugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public CashInResponse.Builder withDebugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public CashInResponse.Builder withShouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }
        public CashInResponse.Builder withApproved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public CashInResponse.Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public CashInResponse.Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }


        public CashInResponse build() {
            CashInResponse CashInResponse = new CashInResponse();
            CashInResponse.debugRequestMessage = this.debugRequestMessage;
            CashInResponse.debugResponseMessage = this.debugResponseMessage;
            CashInResponse.approved = this.approved;
            CashInResponse.message = this.message;
            CashInResponse.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            CashInResponse.stan = this.stan;
            return CashInResponse;
        }
    }
}
