package global.citytech.finpos.merchant.presentation.payment;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import citytech.global.easydroid.concurrent.LongRunningTask;
import global.citytech.common.Constants;
import global.citytech.common.IPosServiceInterface;
import global.citytech.easydroid.core.utils.Jsons;
import global.citytech.finpos.merchant.R;
import global.citytech.finposframework.utility.JsonUtils;
import global.citytech.finposframework.utility.StringUtils;
import global.citytech.payment.sdk.api.PaymentResponse;
import global.citytech.payment.sdk.api.PaymentResult;
import global.citytech.payment.sdk.core.PaymentConstants;
import global.citytech.payment.sdk.core.PaymentRequest;

public class PosPaymentServiceActivity extends AppCompatActivity {

    private final static int POS_PAYMENT_SERVICE_ACTIVITY_REQUEST_CODE = 100;
    private IPosServiceInterface posServiceInterface;
    private String originatorApplicationIdentifier;
    private PaymentVendor paymentVendor;

    private final ServiceConnection posServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            posServiceInterface = IPosServiceInterface.Stub.asInterface(service);
            Log.i("MART", "Pos Service APP interface connected...");
            startDecryptionInUiThread();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.e("MART", "Pos Service APP interface disconnected...");
            setActivityResultFailed(PaymentResult.POS_PAYMENT_SERVICE_NOT_AVAILABLE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pos_payment_service);
        assignOriginatorApplicationIdentifier();
        assignPaymentVendor();
    }

    private void assignOriginatorApplicationIdentifier() {
        originatorApplicationIdentifier = getIntent()
                .getStringExtra(PaymentConstants.EXTRA_APP_ID_DATA);
        Log.i("MART", "Originator Application Identifier ::: " + originatorApplicationIdentifier);
    }

    private void assignPaymentVendor() {
        try {
            List<PaymentVendor> paymentVendors = getPaymentVendors();
            matchVendorsWithOriginatorApplicationIdentifier(paymentVendors);
        } catch (Exception exception) {
            exception.printStackTrace();
            paymentVendor = null;
        }
        if (paymentVendor == null) {
            setActivityResultFailed(PaymentResult.POS_PAYMENT_INVALID_APPLICATION_IDENTIFIER);
        }
    }

    private List<PaymentVendor> getPaymentVendors() {
        List<PaymentVendor> vendors = new ArrayList<>();
        vendors.add(prepareBettyMartVendor());
        vendors.add(preparePaymentServiceVendor());
        return vendors;
    }

    private PaymentVendor preparePaymentServiceVendor() {
        PaymentVendor paymentVendor = new PaymentVendor();
        paymentVendor.applicationIdentifier = "global.citytech.finpos.payment.service";
        paymentVendor.finposPrivateKey = Constants.FINPOS_PRIVATE_KEY;
        paymentVendor.finposPublicKey = Constants.FINPOS_PUBLIC_KEY;
        paymentVendor.vendorName = "Billing System";
        paymentVendor.vendorPublicKey = Constants.VENDOR_PUBLIC_KEY;
        paymentVendor.features = prepareFeatures();
        return paymentVendor;
    }

    private PaymentVendor prepareBettyMartVendor() {
        PaymentVendor paymentVendor = new PaymentVendor();
        paymentVendor.applicationIdentifier = "global.citytech.vendor";
        paymentVendor.finposPrivateKey = Constants.FINPOS_PRIVATE_KEY;
        paymentVendor.finposPublicKey = Constants.FINPOS_PUBLIC_KEY;
        paymentVendor.vendorName = "Betty Mart Pvt. Ltd.";
        paymentVendor.vendorPublicKey = Constants.VENDOR_PUBLIC_KEY;
        paymentVendor.features = prepareFeatures();
        return paymentVendor;
    }

    private List<PaymentFeature> prepareFeatures() {
        List<PaymentFeature> featureList = new ArrayList<>();
        featureList.add(new PaymentFeature("PURCHASE", "Purchase"));
        featureList.add(new PaymentFeature("REVERSAL", "Reversal"));
        featureList.add(new PaymentFeature("VOID", "Void"));
        return featureList;
    }

    private void matchVendorsWithOriginatorApplicationIdentifier(List<PaymentVendor> paymentVendors) {
        for (PaymentVendor data : paymentVendors) {
            Log.i("MART", "Payment Vendor Application Identifier ::: " + data.applicationIdentifier);
            Log.i("MART", "\nPayment Vendor Vendor Name ::: " + data.vendorName);
            Log.i("MART", "\nPayment Vendor Public Key ::: " + data.vendorPublicKey);
            Log.i("MART", "\nPayment Finpos Public Key ::: " + data.finposPublicKey);
            Log.i("MART", "\nPayment Finpos Private Key ::: " + data.finposPrivateKey);
            if (data.applicationIdentifier.equalsIgnoreCase(originatorApplicationIdentifier)) {
                paymentVendor = data;
                bindPosServiceInterface();
                break;
            }
        }
    }

    private void checkPaymentVendorAndBindService() {
        if (paymentVendor != null) {
            bindPosServiceInterface();
        } else {
            setActivityResultFailed(PaymentResult.POS_PAYMENT_SERVICE_NOT_AVAILABLE);
        }
    }

    private void bindPosServiceInterface() {
        LongRunningTask longRunningTask = new LongRunningTask.Builder()
                .withTask(this::taskBindPosServiceInterface)
                .onCompleted((d) -> { })
                .onError(this::onBindError)
                .build();
        longRunningTask.execute();
    }

    private boolean taskBindPosServiceInterface() {
        Log.i("MART", "Binding Pos Service App Interface...");
        Intent intent = new Intent();
        String packageName = "global.citytech.service.app";
        String className = "global.citytech.service.app.PosServiceBinder";
        intent.setClassName(packageName, className);
        getApplicationContext().bindService(intent, posServiceConnection, Context.BIND_AUTO_CREATE);
        Log.i("MART", "Binding Pos Service App Interface....");
        return true;
    }

    private void onBindError(Throwable throwable) {
        throwable.printStackTrace();
        setActivityResultFailed(PaymentResult.POS_PAYMENT_SERVICE_NOT_AVAILABLE);
    }

    private void startDecryptionInUiThread() {
        PosPaymentServiceActivity.this.runOnUiThread(
                this::decryptExtraDataAndProceedIfValidDecryptedData);
    }

    private void decryptExtraDataAndProceedIfValidDecryptedData() {
        String decryptedData = decryptedExtraData();
        if (JsonUtils.isValidJsonString(decryptedData)) {
            checkFeatureAllowedAndProceed(decryptedData);
        } else {
            setActivityResultFailed(PaymentResult.INVALID_DATA);
        }
    }

    private String decryptedExtraData() {
        String extraData = getExtraDataFromReceivedIntentData();
        String decryptedData = "";
        try {
            decryptedData = posServiceInterface.getDecryptedData(
                    paymentVendor.vendorPublicKey,
                    extraData
            );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return decryptedData;
    }

    private String getExtraDataFromReceivedIntentData() {
        return getIntent().getStringExtra(PaymentConstants.EXTRA_DATA);
    }

    private void checkFeatureAllowedAndProceed(String decryptedData) {
        PaymentRequest paymentRequest = Jsons.fromJsonToObj(decryptedData, PaymentRequest.class);
        if (isPaymentFeatureAllowedForPaymentRequest(paymentRequest)) {
            startPosPaymentActivityWithDecryptedData(decryptedData);
        } else {
            setActivityResultFailed(PaymentResult.POS_PAYMENT_FEATURE_NOT_ALLOWED);
        }
    }

    private boolean isPaymentFeatureAllowedForPaymentRequest(PaymentRequest paymentRequest) {
        boolean featureAllowed = false;
        for (PaymentFeature paymentFeature : paymentVendor.features) {
            if (paymentFeature.getCode().equalsIgnoreCase(paymentRequest.getPurchaseType().toString())) {
                featureAllowed = true;
                break;
            }
        }
        return featureAllowed;
    }

    private void startPosPaymentActivityWithDecryptedData(String decryptedData) {
        Log.i("MART", "Decrypted Data ::: " + decryptedData);
        Intent intent = new Intent(this, PosPaymentActivity.class);
        intent.putExtra(PaymentConstants.EXTRA_DATA, decryptedData);
        intent.putExtra(PaymentConstants.EXTRA_VENDOR_NAME_DATA, paymentVendor.vendorName);
        startActivityForResult(intent, POS_PAYMENT_SERVICE_ACTIVITY_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == POS_PAYMENT_SERVICE_ACTIVITY_REQUEST_CODE) {
            checkOnActivityResultCodeAndProceedWithIntentData(resultCode, data);
        } else {
            setActivityResultFailed(PaymentResult.POS_PAYMENT_SERVICE_NOT_AVAILABLE);
        }
    }

    private void checkOnActivityResultCodeAndProceedWithIntentData(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_CANCELED) {
            setActivityResultFailed(PaymentResult.CANCEL);
        } else {
            encryptNonNullIntentDataOnly(data);
        }
    }

    private void encryptNonNullIntentDataOnly(Intent data) {
        if (data != null) {
            encryptExtraDataAndReturnActivityResultToPaymentSdk(data);
        } else {
            setActivityResultFailed(PaymentResult.INVALID_DATA);
        }
    }

    private void encryptExtraDataAndReturnActivityResultToPaymentSdk(Intent data) {
        String encryptedData = encryptData(data.getStringExtra(PaymentConstants.EXTRA_DATA));
        if (StringUtils.isEmpty(encryptedData)) {
            setActivityResultFailed(PaymentResult.INVALID_DATA);
        } else {
            prepareIntentDataAndReturnActivityResult(encryptedData);
        }
    }

    private String encryptData(String plainExtraData) {
        String encryptedData = "";
        try {
            encryptedData = posServiceInterface.getEncryptedData(
                    paymentVendor.finposPrivateKey,
                    plainExtraData
            );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return encryptedData;
    }

    private void prepareIntentDataAndReturnActivityResult(String encryptedData) {
        Intent intent = new Intent();
        intent.putExtra(PaymentConstants.EXTRA_DATA, encryptedData);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void setActivityResultFailed(PaymentResult paymentResult) {
        PaymentResponse paymentResponse = new PaymentResponse(paymentResult);
        String encryptedData = JsonUtils.toJsonObj(paymentResponse);
        if (paymentVendor != null && !paymentVendor.finposPrivateKey.isEmpty()) {
            encryptedData = encryptData(encryptedData);
        }
        prepareIntentDataAndReturnActivityResult(encryptedData);
    }

}