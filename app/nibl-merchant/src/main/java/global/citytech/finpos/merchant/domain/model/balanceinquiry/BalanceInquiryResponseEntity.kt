package global.citytech.finpos.merchant.domain.model.balanceinquiry

data class BalanceInquiryResponseEntity (
    val stan :String?= null,
    val message: String= "",
    val additionalAmount:String,
    val approve:Boolean
        )