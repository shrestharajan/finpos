package global.citytech.finpos.merchant.presentation.manual

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.easydroid.core.extension.showToast
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityManualBinding
import global.citytech.finpos.merchant.utils.MessageConfig


import kotlinx.android.synthetic.main.activity_manual.*

class ManualActivity : AppBaseActivity<ActivityManualBinding, ManualViewModel>() {
    companion object {
        const val REQUEST_CODE = 2000
    }

    private lateinit var viewModel: ManualViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.viewModel = getViewModel()
        initObservers()
        initViews()
    }

    fun initObservers() {
        this.viewModel.invalidCardNumber.observe(
            this,
            Observer {
                observeCardNumber(it)
            }
        )

        this.viewModel.invalidExpiryDate.observe(
            this,
            Observer {
                observeExpiryDate(it)
            }
        )

        this.viewModel.invalidCVV.observe(
            this,
            Observer {
                observeCvv(it)
            }
        )

        this.viewModel.validData.observe(
            this,
            Observer {
                observeValidData(it)
            }
        )
    }

    private fun observeCvv(it: Boolean) {
        if (it) {
            showToast(getString(R.string.error_msg_invalid_cvv))
        }
    }

    private fun observeCardNumber(invalidCardNum: Boolean) {
        if (invalidCardNum) {
            showToast(getString(R.string.error_msg_invalid_card_num))
        }
    }

    private fun observeExpiryDate(invalidExpiryDate: Boolean) {
        if (invalidExpiryDate) {
            showToast(getString(R.string.error_msg_invalid_expiry_date))
        }
    }

    private fun observeValidData(validData: Boolean) {
        if (validData) {
            val intent = Intent()
            intent.putExtra(
                getString(R.string.title_card_number),
                text_input_et_pan.text.toString().trim()
            )
            intent.putExtra(
                getString(R.string.title_expiry_date),
                prepareExpiryDataForIntent(text_input_et_expiry_date.text.toString().trim())
            )
            intent.putExtra(
                getString(R.string.title_cvv),
                text_input_et_cvv.text.toString().trim()
            )
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    private fun prepareExpiryDataForIntent(expiryDate: String): String {
        val year = expiryDate.substring(2, 4)
        val month = expiryDate.substring(0, 2)
        return year + month
    }

    private fun initViews() {
        iv_back.setOnClickListener {
            onCancelButtonClicked()
        }

        btn_cancel.setOnClickListener {
            onCancelButtonClicked()
        }

        btn_confirm.setOnClickListener {
            onConfirmButtonClicked()
        }
    }

    private fun onCancelButtonClicked() {
        val builder = MessageConfig.Builder()
            .title(getString(R.string.title_exit))
            .message(getString(R.string.exit_msg))
            .positiveLabel(getString(R.string.title_ok))
            .negativeLabel(getString(R.string.action_cancel))
            .onPositiveClick {
                exitManualPageOnCancel()
            }
            .onNegativeClick {}
        showConfirmationMessage(builder)
    }

    private fun exitManualPageOnCancel() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    private fun onConfirmButtonClicked() {
        this.viewModel.validateEnteredData(
            text_input_et_pan.text.toString().trim(),
            text_input_et_expiry_date.text.toString().trim(),
            text_input_et_cvv.text.toString().trim()
        )
    }

    override fun getBindingVariable(): Int = BR.manualViewModel

    override fun getLayout(): Int = R.layout.activity_manual

    override fun getViewModel(): ManualViewModel =
        ViewModelProviders.of(this)[ManualViewModel::class.java]
}
