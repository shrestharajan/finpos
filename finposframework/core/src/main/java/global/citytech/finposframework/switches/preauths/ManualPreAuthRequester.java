package global.citytech.finposframework.switches.preauths;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Rishav Chudal on 11/3/20.
 */
public interface ManualPreAuthRequester<
        T extends PreauthRequestParameter,
        E extends PreauthResponseParameter>
        extends Request<T, E> {
}
