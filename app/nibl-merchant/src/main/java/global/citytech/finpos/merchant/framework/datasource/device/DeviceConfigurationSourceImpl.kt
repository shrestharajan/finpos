package global.citytech.finpos.merchant.framework.datasource.device

import android.app.Application
import com.google.gson.JsonParser
import global.citytech.common.Constants
import global.citytech.common.data.LabelValue
import global.citytech.common.extensions.toJsonString
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.app.db.AppDatabase
import global.citytech.finpos.merchant.data.datasource.device.DeviceConfigurationSource
import global.citytech.finpos.merchant.domain.model.app.AidParam
import global.citytech.finpos.merchant.domain.model.app.EmvParam
import global.citytech.finpos.merchant.domain.model.app.Host
import global.citytech.finpos.merchant.domain.model.app.TransactionConfig
import global.citytech.finpos.merchant.domain.model.device.DeviceResponse
import global.citytech.finpos.merchant.domain.model.device.DeviceResult
import global.citytech.finpos.merchant.domain.model.device.LoadParameterRequest
import global.citytech.finpos.merchant.domain.utils.mapToDomain
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.data.mapToUiModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finpos.merchant.service.AppDeviceFactory
import global.citytech.finpos.processor.neps.transaction.TransactionUtils
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.exceptions.PosException
import global.citytech.finposframework.hardware.DeviceServiceType
import global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest
import global.citytech.finposframework.hardware.emv.aid.AidParameters
import global.citytech.finposframework.hardware.emv.aid.AidParametersService
import global.citytech.finposframework.hardware.emv.emvkeys.EmvKey
import global.citytech.finposframework.hardware.emv.emvkeys.EmvKeysInjectionRequest
import global.citytech.finposframework.hardware.emv.emvkeys.EmvKeysService
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersService
import global.citytech.finposframework.hardware.init.InitializeSDKService
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest.MultipleAidSelectionListener
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService
import global.citytech.finposframework.hardware.keymgmt.keys.KeyService
import global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyInjectionRequest
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.notifier.Notifier
import global.citytech.finposframework.repositories.TerminalRepository
import global.citytech.finposframework.switches.ProcessorManager
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.utility.DateUtils
import global.citytech.finposframework.utility.PosResponse
import global.citytech.finposframework.utility.TMSConfigurationConstants.MANUAL_TRANSACTION
import io.reactivex.Observable
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set

/**
 * Created by Rishav Chudal on 8/10/20.
 */
class DeviceConfigurationSourceImpl(val context: Application) : DeviceConfigurationSource {

    private val notifier: Notifier = NotificationHandler
    private val applicationRepository = ApplicationRepositoryImpl(context)

    private val multipleAidSelectionListener: MultipleAidSelectionListener =
        object : MultipleAidSelectionListener {
            override fun onMultipleAid(aids: Array<String>): Int {
                notifier.notify(
                    Notifier.EventType.DETECTED_MULTIPLE_AID,
                    "Processing.."
                )
                return -1
            }
        }

    val keyService = AppDeviceFactory.getCore(
        context,
        DeviceServiceType.KEYS
    ) as KeyService

    override fun inject(keyInjectionRequest: KeyInjectionRequest): Observable<DeviceResponse> {
        return Observable.fromCallable {
            val response = this.keyService.injectKey(keyInjectionRequest)
            response.mapToDomain()
        }
    }

    override fun initSdk(): Observable<DeviceResponse> {
        val initializeSdkService = AppDeviceFactory
            .getCore(
                context,
                DeviceServiceType.INIT
            ) as InitializeSDKService
        return Observable.fromCallable {
            val response = initializeSdkService.initializeSDK()
            response.mapToDomain()
        }
    }

    override fun loadTerminalParameters(loadParameterRequest: LoadParameterRequest):
            Observable<DeviceResponse> {
        return Observable.fromCallable {
            prepareTerminalForOperating(loadParameterRequest)
            onSuccessLoadingTerminalParameters()
        }
    }

    private fun prepareTerminalForOperating(loadParameterRequest: LoadParameterRequest) {
        initializeTransactionIds()
        loadEMVConfigurationInHardware(loadParameterRequest)
        retrieveAndSetDeviceConfiguration()
    }

    private fun loadEMVConfigurationInHardware(loadParameterRequest: LoadParameterRequest) {
        loadEMVParamsConfigurationInHardware()
        loadAIDAndEMVKeysConfigurationInHardwareIfRequired(loadParameterRequest)
    }

    private fun loadEMVParamsConfigurationInHardware() {
        val emvParametersRequest = retrieveAndPrepareEMVParametersRequestForHardware()
        injectEmvParam(emvParametersRequest)
    }

    private fun loadAIDAndEMVKeysConfigurationInHardwareIfRequired(
        loadParameterRequest: LoadParameterRequest
    ) {
        if (loadParameterRequest.loadAllEMVConfigurationInHardware) {
            loadAIDAndEMVKeysConfigurations()
        }
    }

    private fun loadAIDAndEMVKeysConfigurations() {
        loadEMVKeysIfPresentInDatabase()
        loadAIDConfigurations()
    }

    private fun loadAIDConfigurations() {
        val emvParametersRequest = retrieveAndPrepareEMVParametersRequestForHardware()
        val aidParamsList = retrieveAndPrepareAIDParametersListForHardware(emvParametersRequest)
        val supportedTransactionTypes = this.retrieveSupportedTransactionTypes()
        injectAidParam(aidParamsList, emvParametersRequest, supportedTransactionTypes)
    }

    private fun onSuccessLoadingTerminalParameters() = DeviceResponse(
        DeviceResult.SUCCESS,
        "Terminal Parameters Loaded"
    )

    override fun closeUpIO() {
        val cardReader = CardSourceImpl(NiblMerchant.getActivityContext())
        cardReader.cleanUp()
    }

    override fun cardPresentInIccReader(): Observable<Boolean> {
        val service = AppDeviceFactory.getCore(
            context.applicationContext,
            DeviceServiceType.CARD
        ) as ReadCardService
        return Observable.fromCallable {
            service.getIccCardStatus().result == global.citytech.finposframework.hardware.common.Result.ICC_CARD_PRESENT
        }
    }

    override fun enableHardwareButtons(): Observable<DeviceResponse> {
        return Observable.fromCallable {
            DeviceControllerImpl(context).enableHardwareButtons().mapToDomain()
        }
    }

    override fun disableHardwareButtons(): Observable<DeviceResponse> {
        return Observable.fromCallable {
            DeviceControllerImpl(context).disableHardwareButtons().mapToDomain()
        }
    }

    override fun readCardDetails(
        configurationItem: ConfigurationItem
    ): Observable<ReadCardResponse> {
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val readCardService = AppDeviceFactory.getCore(
            NiblMerchant.getActivityContext(),
            DeviceServiceType.CARD
        ) as ReadCardService
        return Observable.fromCallable {
            val readCardRequest = prepareReadCardRequest(terminalRepository)
            readCardService.readCardDetails(readCardRequest = readCardRequest)
        }
    }

    override fun posReady(cardTypeList: MutableList<CardType>?): Observable<PosResponse> {
        return Observable.fromCallable {
            DeviceControllerImpl(context).isReady(cardTypeList)
        }
    }

    private fun initializeTransactionIds() {
        val stan = AppDatabase.getInstance(context).getTransactionConfigDao().getStan()
        val rrn = AppDatabase.getInstance(context).getTransactionConfigDao().getStan()
        val invoiceNumber = AppDatabase.getInstance(context).getTransactionConfigDao().getStan()
        val batchNumber = AppDatabase.getInstance(context).getTransactionConfigDao().getStan()

        if (stan == 0.toLong() && rrn == 0.toLong() &&
            invoiceNumber == 0.toLong() && batchNumber == 0.toLong()
        )
            AppDatabase.getInstance(context).getTransactionConfigDao()
                .insert(TransactionConfig(1, 1, 1, 1))
    }

    private fun retrieveAndSetDeviceConfiguration() {
        val merchantFromDb = AppDatabase.getInstance(context).getMerchantDao().getMerchantList()
        val logoFromDb = AppDatabase.getInstance(context).getLogoDao().getLogoList()
        val hostFromDb = AppDatabase.getInstance(context).getHostDao().getHostList()
        val emvParamFromDB = AppDatabase.getInstance(context).getEmvParamDao().getEmvParams()
        val currency: LabelValue =
            Jsons.fromJsonToObj(emvParamFromDB[0].transactionCurrencyCode, LabelValue::class.java)
        val deviceConfiguration = DeviceConfiguration.get()
        deviceConfiguration.currencyCode = currency.value as String
        deviceConfiguration.purchaseCardTypeMap = preparePurchaseCardTypeMap()
        deviceConfiguration.terminalId = merchantFromDb[0].terminalId
        deviceConfiguration.merchantId = merchantFromDb[0].switchId
        deviceConfiguration.merchantLogo = logoFromDb[0].printLogo
        deviceConfiguration.merchantName = merchantFromDb[0].name
        deviceConfiguration.retailerName = merchantFromDb[0].name
        deviceConfiguration.retailerAddress = merchantFromDb[0].address
        deviceConfiguration.dashboardDisplayLogo = logoFromDb[0].displayLogo
        deviceConfiguration.retailerPhoneNumber = merchantFromDb[0].phoneNumber
        deviceConfiguration.debugMode = PreferenceManager.getDebugModePref(false)
        deviceConfiguration.listOfHosts = retrieveHostList(hostFromDb)
        DeviceConfiguration.set(deviceConfiguration)
    }

    private fun preparePurchaseCardTypeMap(): Map<TransactionType, List<CardType>>? {
        val purchaseCardTypeMap: HashMap<TransactionType, List<CardType>> = HashMap()
        enumValues<TransactionType>().forEach {
            purchaseCardTypeMap[it] = retrieveCardTypeForPurchaseType(it)
        }
        return purchaseCardTypeMap
    }

    private fun retrieveCardTypeForPurchaseType(transactionType: TransactionType): List<CardType> {
        val listOfCardType: java.util.ArrayList<CardType> = java.util.ArrayList()
        listOfCardType.add(CardType.ICC)
        listOfCardType.add(CardType.MAG)
        listOfCardType.add(CardType.PICC)
        addManualTransactionIfAllowedOnly(listOfCardType)
        return listOfCardType
    }

    private fun retrieveHostList(hostFromDb: List<Host>): List<global.citytech.finpos.nibl.merchant.presentation.model.response.Host>? {
        val listOfHosts =
            ArrayList<global.citytech.finpos.nibl.merchant.presentation.model.response.Host>()
        hostFromDb.forEach {
            listOfHosts.add(
                global.citytech.finpos.nibl.merchant.presentation.model.response.Host(
                    it.connectionTimeout,
                    it.ip,
                    it.nii,
                    it.order,
                    it.port,
                    it.retryLimit
                )
            )
        }
        return listOfHosts
    }

    private fun loadEMVKeysIfPresentInDatabase() {
        val emvKeys = AppDatabase
            .getInstance(context)
            .getEmvKeyDao()
            .getEmvKeys()
        if (emvKeys.isNotEmpty()) {
            mapEMVKeysToHardwareTypeAndInject(emvKeys)
        } else {
            injectEmvKeys(emptyList())
        }
    }

    private fun mapEMVKeysToHardwareTypeAndInject(emvKeys: List<global.citytech.finpos.merchant.domain.model.app.EmvKey>) {
        val emvKeysForHardware = mapEmvKeysFromDatabaseToHardware(emvKeys)
        injectEmvKeys(emvKeysForHardware)
    }

    private fun mapEmvKeysFromDatabaseToHardware(
        emvKeys: List<global.citytech.finpos.merchant.domain.model.app.EmvKey>
    ): List<EmvKey> {
        return emvKeys.map {
            EmvKey(
                getValueFromLabelValue(it.rid),
                getValueFromLabelValue(it.index),
                getValueFromLabelValue(it.length),
                getValueFromLabelValue(it.exponent),
                getValueFromLabelValue(it.modules),
                getValueFromLabelValue(it.hashId),
                getValueFromLabelValue(it.keySignatureId),
                getValueFromLabelValue(it.checkSum),
                getValueFromLabelValue(it.expiryDate)
            )
        }
    }

    private fun injectEmvKeys(emvKeysList: List<EmvKey>) {
        val emvKeyService = AppDeviceFactory.getCore(
            context,
            DeviceServiceType.EMV_KEYS
        ) as EmvKeysService
        val deviceResponse =
            if (emvKeysList.isEmpty())
                emvKeyService.eraseAllEmvKeys()
            else
                emvKeyService.injectEmvKeys(
                    EmvKeysInjectionRequest(
                        emvKeysList
                    )
                )
        if (deviceResponse.result != global.citytech.finposframework.hardware.common.Result.SUCCESS) {
            throw PosException(
                PosError.DEVICE_ERROR_DOWNLOAD_TMS_AGAIN
            )
        }
    }

    private fun retrieveAndPrepareEMVParametersRequestForHardware(): EmvParametersRequest {
        val emvParams = AppDatabase.getInstance(context).getEmvParamDao().getEmvParams()
        return mapEMVParamsConfigurationFromDatabaseToHardware(emvParams)
    }

    private fun mapEMVParamsConfigurationFromDatabaseToHardware(
        emvParams: List<EmvParam>
    ): EmvParametersRequest = EmvParametersRequest(
        getValueFromLabelValue(emvParams[0].terminalCountryCode),
        getValueFromLabelValue(emvParams[0].transactionCurrencyCode),
        getValueFromLabelValue(emvParams[0].transactionCurrencyExponent),
        getValueFromLabelValue(emvParams[0].terminalCapabilities),
        getValueFromLabelValue(emvParams[0].terminalType),
        getValueFromLabelValue(emvParams[0].merchantCategoryCode),
        getValueFromLabelValue(emvParams[0].terminalId),
        getValueFromLabelValue(emvParams[0].merchantIdentifier),
        getValueFromLabelValue(emvParams[0].merchantName),
        getValueFromLabelValue(emvParams[0].ttq),
        getValueFromLabelValue(emvParams[0].additionalTerminalCapabilities),
        getValueFromLabelValue(emvParams[0].forceOnlineFlag)
    )

    private fun injectEmvParam(
        emvParametersRequest: EmvParametersRequest
    ) {
        val emvParamService = AppDeviceFactory
            .getCore(
                context,
                DeviceServiceType.EMV_PARAM
            ) as EmvParametersService
        val deviceResponse = emvParamService.injectEmvParameters(emvParametersRequest)
        if (deviceResponse.result != global.citytech.finposframework.hardware.common.Result.SUCCESS) {
            throw PosException(
                PosError.DEVICE_ERROR_DOWNLOAD_TMS_AGAIN
            )
        }
    }

    private fun retrieveAndPrepareAIDParametersListForHardware(
        emvParametersRequest: EmvParametersRequest
    ): List<AidParameters> {
        val aidParams = AppDatabase
            .getInstance(context)
            .getAidParamDao()
            .getAidParamList()
        return mapAIDConfigurationsFromDatabaseToHardware(emvParametersRequest, aidParams)
    }

    private fun mapAIDConfigurationsFromDatabaseToHardware(
        emvParametersRequest: EmvParametersRequest,
        aidParams: List<AidParam>
    ): List<AidParameters> {
        val aidParamsList: MutableList<AidParameters> = ArrayList()
        aidParams.forEach {
            val aidParameters = AidParameters(
                getValueFromLabelValue(it.aid),
                getValueFromLabelValue(it.label),
                getValueFromLabelValue(it.terminalAidVersion),
                getValueFromLabelValue(it.defaultTDOL),
                getValueFromLabelValue(it.defaultDDOL),
                getValueFromLabelValue(it.denialActionCode),
                getValueFromLabelValue(it.onlineActionCode),
                getValueFromLabelValue(it.defaultActionCode),
                getValueFromLabelValue(it.terminalFloorLimit),
                getValueFromLabelValue(it.contactlessFloorLimit),
                getValueFromLabelValue(it.contactlessTransactionLimit),
                getValueFromLabelValue(it.cvmLimit)
            )
            aidParameters.cdCvmLimit =
                getValueFromAdditionalData(it.additionalData, Constants.KEY_CD_CVM_LIMIT)
            aidParameters.noCdCvmLimit =
                getValueFromAdditionalData(it.additionalData, Constants.KEY_NO_CD_CVM_LIMIT)
            aidParameters.cvmCapCvmRequired =
                getValueFromAdditionalData(it.additionalData, Constants.KEY_CVM_CAP_CVM_REQUIRED)
            aidParameters.cvmCapNoCvmRequired =
                getValueFromAdditionalData(it.additionalData, Constants.KEY_CVM_CAP_NO_CVM_REQUIRED)
            aidParameters.terminalRiskManagementData =
                getValueFromAdditionalData(it.additionalData, Constants.KEY_TERMINAL_RISK_MGMT_DATA)
            aidParameters.clTacDenial =
                getValueFromAdditionalData(it.additionalData, Constants.KEY_CL_TAC_DENIAL)
            aidParameters.clTacOnline =
                getValueFromAdditionalData(it.additionalData, Constants.KEY_CL_TAC_ONLINE)
            aidParameters.clTacDefault =
                getValueFromAdditionalData(it.additionalData, Constants.KEY_CL_TAC_DEFAULT)
            aidParameters.acquirerId =
                getValueFromAdditionalData(it.additionalData, Constants.KEY_ACQUIRER_ID)
            aidParameters.kernelConfig =
                getValueFromAdditionalData(it.additionalData, Constants.KEY_KERNEL_CONFIG)
            aidParameters.clCardDataInputCapability =
                getValueFromAdditionalData(it.additionalData, Constants.KEY_CL_CARD_DATA_INPUT_CAP)
            aidParameters.clSecurityCapability =
                getValueFromAdditionalData(it.additionalData, Constants.KEY_CL_SECURITY_CAP)
            aidParameters.udol = getValueFromAdditionalData(it.additionalData, Constants.KEY_UDOL)
            aidParameters.msdAppVersion =
                getValueFromAdditionalData(it.additionalData, Constants.KEY_MSD_APP_VERSION)
            aidParameters.ttq = getValueFromAdditionalData(it.additionalData, Constants.KEY_TTQ)
            aidParamsList.add(aidParameters)
        }
        return aidParamsList
    }

    private fun retrieveSupportedTransactionTypes(): List<String> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val transactionTypeRequester =
            ProcessorManager.getInterface(null, null)
                .transactionTypeRequester
        return transactionTypeRequester.execute(mapToModel()).mapToUiModel()
    }

    private fun injectAidParam(
        aidParamsList: List<AidParameters>,
        emvParametersRequest: EmvParametersRequest,
        supportedTransactionTypes: List<String>
    ) {
        val aidService = AppDeviceFactory
            .getCore(
                context,
                DeviceServiceType.AID_PARAM
            ) as AidParametersService
        val deviceResponse = if (aidParamsList.isEmpty())
            aidService.eraseAllAid()
        else
            aidService.injectAid(
                AidInjectionRequest(
                    aidParamsList,
                    emvParametersRequest,
                    supportedTransactionTypes
                )
            )
        if (deviceResponse.result != global.citytech.finposframework.hardware.common.Result.SUCCESS) {
            throw PosException(
                PosError.DEVICE_ERROR_DOWNLOAD_TMS_AGAIN
            )
        }
    }

    private fun getValueFromLabelValue(
        item: String?
    ): String {
        var value = ""
        if (item != null) {
            val labelValue = Jsons.fromJsonToObj(item, LabelValue::class.java)
            if (labelValue?.value != null) {
                value = labelValue.value.toString()
            }
        }
        return value
    }

    private fun getValueFromAdditionalData(
        additionalData: String?,
        parameterToRetrieve: String?
    ): String {
        Logger.getLogger("DeviceConfigurationSourceImpl")
            .log("::: ADDITIONAL DATA ::: $additionalData")
        Logger.getLogger("DeviceConfigurationSourceImpl")
            .log("::: PARAMETER TO RETRIEVE ::: $parameterToRetrieve")
        if (parameterToRetrieve.isNullOrEmptyOrBlank() || additionalData.isNullOrEmptyOrBlank())
            return ""
        val jsonParser = JsonParser()
        try {
            val additionalDataObject = jsonParser.parse(additionalData).asJsonObject
            if (!additionalDataObject.has(parameterToRetrieve))
                return ""
            val labelValueDataObject = additionalDataObject.getAsJsonObject(parameterToRetrieve)
            return getValueFromLabelValue(labelValueDataObject.toJsonString())
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }

    private fun prepareReadCardRequest(
        terminalRepository: TerminalRepository
    ): ReadCardRequest {
        val cardTypeList = ArrayList<CardType>()
        cardTypeList.add(CardType.ICC)
        val readCardRequest = ReadCardRequest(
            cardTypeList,
            TransactionType.PURCHASE,
            TransactionUtils.retrieveTransactionType9C(TransactionType.PURCHASE),
            applicationRepository.retrieveEmvParameterRequest()
        )
        readCardRequest.packageName = NiblMerchant.INSTANCE.packageName
        readCardRequest.stan = terminalRepository.systemTraceAuditNumber
        readCardRequest.transactionDate = DateUtils.yyMMddDate()
        readCardRequest.transactionTime = DateUtils.HHmmssTime()
        readCardRequest.multipleAidSelectionListener = multipleAidSelectionListener
        readCardRequest.currencyName = terminalRepository.terminalTransactionCurrencyName
        readCardRequest.pinPadFixedLayout = !terminalRepository.shouldShufflePinPad()
        return readCardRequest
    }

    private fun addManualTransactionIfAllowedOnly(listOfCardType: java.util.ArrayList<CardType>) {
        if (manualTransactionIsAllowed()) {
            listOfCardType.add(CardType.MANUAL)
        }
    }

    fun manualTransactionIsAllowed(): Boolean {
        val manualTransactionConfiguration = getManualTransactionConfiguration()
        return checkConfigurationValueAndProceed(manualTransactionConfiguration)
    }

    private fun getManualTransactionConfiguration(): String {
        val appRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
        return appRepository.retrieveFromAdditionalDataEmvParameters(MANUAL_TRANSACTION)
    }

    private fun checkConfigurationValueAndProceed(manualTransactionConfiguration: String): Boolean {
        var manualAllowed = false
        if (manualTransactionConfiguration.equals(BuildConfig.MANUAL_TRANSACTION_ALLOWED, true)) {
            manualAllowed = true
        }
        return manualAllowed
    }

}