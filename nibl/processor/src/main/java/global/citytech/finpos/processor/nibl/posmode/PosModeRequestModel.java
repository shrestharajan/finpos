package global.citytech.finpos.processor.nibl.posmode;

import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.posmode.PosModeRequestParameter;

/**
 * Created by Unique Shakya on 1/7/2021.
 */
public class PosModeRequestModel implements PosModeRequestParameter, UseCase.Request {

    private String merchantCategoryCode;

    public PosModeRequestModel(String merchantCategoryCode) {
        this.merchantCategoryCode = merchantCategoryCode;
    }

    public String getMerchantCategoryCode() {
        return merchantCategoryCode;
    }
}
