package global.citytech.finpos.processor.neps.refund;

import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.ProcessingCode;
import global.citytech.finpos.neps.iso8583.requestsender.refund.IccRefundRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.refund.MagRefundRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.refund.PICCRefundRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.refund.RefundIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionRequestSender;
import global.citytech.finpos.processor.neps.transaction.CardTransactionUseCase;
import global.citytech.finpos.processor.neps.transaction.TransactionRequest;
import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.utility.DateUtils;

/**
 * Created by Saurav Ghimire on 2/2/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class CardRefundUseCase extends CardTransactionUseCase {

    TransactionLog transactionLog;

    @Override
    protected TransactionType getTransactionType() {
        return TransactionType.REFUND;
    }

    @Override
    protected boolean getAuthCompletionStatus() {
        return true;
    }

    public CardRefundUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    public TransactionResponse execute(TransactionRequest request) {
        transactionLog = request.getTransactionRepository()
                .getTransactionLogWithRRN(request.getTransactionRequest().getOriginalRetrievalReferenceNumber());
        if (transactionLog == null)
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_FOUND);
        return super.execute(request);
    }

    @Override
    protected NepsMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context) {
        TransactionRequestSender sender;
        switch (readCardResponse.getCardDetails().getCardType()) {
            case MAG:
                sender = new MagRefundRequestSender(context);
                break;
            case PICC:
                sender = new PICCRefundRequestSender(context);
                break;
            case ICC:
            default:
                sender = new IccRefundRequestSender(context);
                break;
        }
        return sender;
    }

    @Override
    protected TransactionResponse prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository, boolean shouldPrint) {
        return RefundResponse.Builder.createDefaultBuilder()
                .approved(transactionApproved)
                .stan(stan)
                .shouldPrintCustomerCopy(shouldPrint)
                .debugRequestMessage(isoMessageResponse.getDebugRequestString())
                .debugResponseMessage(isoMessageResponse.getDebugResponseString())
                .message(this.retrieveMessage(isoMessageResponse, transactionApproved, applicationRepository))
                .build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.REFUND.getCode();
    }

    @Override
    protected TransactionResponse prepareErrorResponse(String responseMessage, boolean shouldPrint) {
        return RefundResponse.Builder.createDefaultBuilder()
                .stan(this.stan)
                .message(responseMessage)
                .approved(false)
                .shouldPrintCustomerCopy(shouldPrint)
                .build();
    }

    @Override
    protected TransactionIsoRequest prepareIsoRequest(ReadCardResponse readCardResponse, global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest) {
        String pin = shouldSkipPinBlockFromCard(readCardResponse) ? "" : readCardResponse.getCardDetails().getPinBlock().getPin();
        logger.debug("PIN BLOCK :::: " + pin);
        return RefundIsoRequest.Builder.newInstance()
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .transactionAmount(readCardResponse.getCardDetails().getAmount())
                .localDateTime(DateUtils.yyMMddHHmmssDate())
                .track2Data(readCardResponse.getCardDetails().getTrackTwoData())
                .currencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .pinBlock(shouldSkipPinBlockFromCard(readCardResponse) ? "" : readCardResponse.getCardDetails().getPinBlock().getPin())
                .emvData(readCardResponse.getCardDetails().getIccDataBlock())
                .fallBackFromIccToMag(readCardResponse.getFallbackIccToMag())
                .originalRrn(transactionRequest.getOriginalRetrievalReferenceNumber())
                .build();
    }

    @Override
    protected String getClassName() {
        return CardRefundUseCase.class.getName();
    }


}
