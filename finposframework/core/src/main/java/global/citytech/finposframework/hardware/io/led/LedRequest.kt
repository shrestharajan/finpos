package global.citytech.finposframework.hardware.io.led

/**
 * Created by Rishav Chudal on 6/4/20.
 */
data class LedRequest(val ledLight: LedLight,
                      val ledAction: LedAction
)