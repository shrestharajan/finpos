package global.citytech.finpos.neps.iso8583;

import org.junit.Test;

import global.citytech.finposframework.log.Logger;

import global.citytech.finpos.neps.iso8583.requestsender.purchases.IccPurchaseRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.purchases.PurchaseIsoRequest;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.exceptions.FinPosException;

import static org.junit.Assert.assertNotNull;

public class PurchaseRequestSenderIT {

    private Logger logger = Logger.getLogger(PurchaseRequestSenderIT.class.getName());


    @Test
    public void sendTest(){
        try {
            HostInfo primaryHost = NEPSHostConfig.getPrimaryConfig();
            HostInfo secondaryHost = NEPSHostConfig.getSecondaryConfig();
            ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);
            RequestContext requestContext = new RequestContext("000030"
                    , new TerminalInfo("41234027", "100512345678910",
                    "","Citytech","Kamaladi"), connectionParam);
            PurchaseIsoRequest purchaseIsoRequest = MockPurchase.getPurchaseIsoRequest();
            requestContext.setRequest(purchaseIsoRequest);
            IccPurchaseRequestSender purchaseRequestSender = new IccPurchaseRequestSender(requestContext);
            IsoMessageResponse messageResponse = purchaseRequestSender.send();
            assertNotNull(messageResponse);
            logger.debug(messageResponse.getUsedHost().toString());


        }catch (FinPosException exception){
            logger.debug(exception.getMessage());
            logger.debug(exception.getDetailMessage());
        }
    }

}