package global.citytech.finposframework.switches.purchase;

import global.citytech.finposframework.switches.Request;

public interface PurchaseRequester<
        T extends PurchaseRequestParameter, E extends PurchaseResponseParameter>
        extends Request<T, E> {}