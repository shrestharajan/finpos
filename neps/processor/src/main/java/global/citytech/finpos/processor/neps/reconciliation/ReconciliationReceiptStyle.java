package global.citytech.finpos.processor.neps.reconciliation;

import global.citytech.finposframework.hardware.io.printer.Style;

public enum ReconciliationReceiptStyle {

    RETAILER_NAME(Style.Align.CENTER, Style.FontSize.XLARGE, 1, true, true, false, false),
    RETAILER_ADDRESS(Style.Align.CENTER, Style.FontSize.SMALL, 1, false, false, false, false),
    START_DATE_TIME(Style.Align.LEFT, Style.FontSize.SMALL, 2, true, false, false, false),
    RECEIPT_VERSION(Style.Align.CENTER, Style.FontSize.NORMAL, 1, true, false, false, false),
    RECONCILIATION_REPORT(Style.Align.CENTER, Style.FontSize.NORMAL, 1, true, true, false, false),
    TERMINAL_ID(Style.Align.LEFT, Style.FontSize.SMALL, 2, true, false, false, false),
    MERCHANT_ID(Style.Align.LEFT, Style.FontSize.SMALL, 2, true, false, false, false),
    BATCH_NUMBER(Style.Align.LEFT, Style.FontSize.SMALL, 2, true, false, false, false),
    HOST(Style.Align.LEFT, Style.FontSize.SMALL, 2, true, false, false, false),
    APPLICATION_VERSION(Style.Align.LEFT, Style.FontSize.SMALL, 2, true, false, false, false),
    CARD_SCHEME(Style.Align.CENTER, Style.FontSize.SMALL, 2, true, true, false, false),
    CUMULATIVE(Style.Align.CENTER, Style.FontSize.SMALL, 2, true, true, false, false),
    TOTALS(Style.Align.LEFT, Style.FontSize.SMALL, 4, true, false, false, false),
    RECONCILIATION_RESULT(Style.Align.CENTER, Style.FontSize.NORMAL, 1, true, true, false, false),
    DIVIDER(Style.Align.CENTER, Style.FontSize.SMALL, 1, false, false, false, false),
    LINE_BREAK(Style.Align.CENTER, Style.FontSize.SMALL, 1, false, false, false, false);

    private Style.Align alignment;
    private Style.FontSize fontSize;
    private int columns;
    private boolean allCaps;
    private boolean bold;
    private boolean italic;
    private boolean underline;

    ReconciliationReceiptStyle(Style.Align alignment, Style.FontSize fontSize, int columns, boolean allCaps, boolean bold, boolean italic, boolean underline) {
        this.alignment = alignment;
        this.fontSize = fontSize;
        this.columns = columns;
        this.allCaps = allCaps;
        this.bold = bold;
        this.italic = italic;
        this.underline = underline;
    }

    public Style getStyle() {
        return new Style.Builder()
                .alignment(this.alignment)
                .allCaps(this.allCaps)
                .bold(this.bold)
                .fontSize(this.fontSize)
                .italic(this.italic)
                .multipleAlignment(this.isMultipleAlignment())
                .underline(this.underline)
                .build();
    }

    public int[] columnWidths() {
        int num = 100 / this.columns;
        int[] columnWidths = new int[this.columns];
        for (int i = 0; i < this.columns; i++) {
            columnWidths[i] = num;
        }
        return columnWidths;
    }

    private boolean isMultipleAlignment() {
        return this.columns > 1;
    }
}
