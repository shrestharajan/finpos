package global.citytech.finpos.merchant.presentation.alarms

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import global.citytech.finpos.merchant.framework.broadcasts.SettlementAlarmReceiver
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finposframework.log.Logger
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Rishav Chudal on 5/26/21.
 */
class SettlementAlarmScheduler(val context: Context) {

    val logger: Logger = Logger.getLogger(SettlementAlarmScheduler::class.java.name)
    private val pendingIntentBroadcast: PendingIntent = assignPendingIntentBroadCast()
    private val alarmManager = this.context.getSystemService(Context.ALARM_SERVICE) as AlarmManager?

    companion object {
        const val POST_PONE_TIME_IN_MINUTES = 20
    }

    private fun assignPendingIntentBroadCast(): PendingIntent {
        val intent = Intent(this.context.applicationContext, SettlementAlarmReceiver::class.java)
        intent.action = SettlementAlarmReceiver.ACTION_ALARM_SETTLEMENT_POSTPONE

        return PendingIntent.getBroadcast(
            this.context.applicationContext,
            SettlementAlarmReceiver.REQUEST_CODE_ALARM_SETTLEMENT_POSTPONE,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    fun postPoneAlarm() {
        this.alarmManager.let {
            this.postponeAlarmForDefaultTime(it!!)
        }
    }

    fun cancelAlarm() {
        this.alarmManager.let {
            it?.cancel(this.pendingIntentBroadcast)
        }
        val settlementAlarmSetter = SettlementAlarmSetter(context.applicationContext)
        settlementAlarmSetter.setRepeatingAlarm(PreferenceManager.getSettlementTime(defaultValue = ""))
    }

    fun cancelAlarmForOnGoingSettlement() {
        val intent = Intent(this.context.applicationContext, SettlementAlarmReceiver::class.java)
        intent.action = SettlementAlarmReceiver.ACTION_ALARM_SETTLEMENT
        this.alarmManager.let {
            it?.cancel(
                PendingIntent.getBroadcast(
                    this.context.applicationContext,
                    SettlementAlarmReceiver.REQUEST_CODE_ALARM_SETTLEMENT,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT
                )
            )
        }
    }

    private fun postponeAlarmForDefaultTime(alarmManager: AlarmManager) {
        alarmManager.cancel(this.pendingIntentBroadcast)
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis() + POST_PONE_TIME_IN_MINUTES * 60 * 1000
        val triggerTimeInMillis = calendar.timeInMillis
        alarmManager.set(
            AlarmManager.RTC_WAKEUP,
            triggerTimeInMillis,
            this.pendingIntentBroadcast
        )
        logger.log("::: ALARM SET FOR TIME IN MILLIS ::: ".plus(triggerTimeInMillis))
        logger.log("::: ALARM SET FOR DATE :::".plus(this.getDate(triggerTimeInMillis)))
    }

    private fun getDate(timeInMillis: Long): String {
        val formatter = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault())
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = timeInMillis
        return formatter.format(calendar.time)
    }

}