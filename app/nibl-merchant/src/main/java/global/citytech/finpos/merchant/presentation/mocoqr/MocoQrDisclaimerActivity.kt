package global.citytech.finpos.merchant.presentation.mocoqr

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityMocoDisclaimerBinding
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import kotlinx.android.synthetic.main.activity_moco_disclaimer.*

class MocoQrDisclaimerActivity :
    AppBaseActivity<ActivityMocoDisclaimerBinding, MocoQrDisclaimerViewModel>() {

    companion object {
        const val REQUEST_CODE = 3472
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObservers()
        wv_disclaimer.setOnLongClickListener { true }
        wv_disclaimer.settings.javaScriptEnabled = false
        wv_disclaimer.isLongClickable = false
        wv_disclaimer.loadData(
            resources.getString(R.string.disclaimer_content),
            "text/html",
            "utf-8"
        )

        btn_confirm.setOnClickListener {
            if (wv_disclaimer.visibility == View.VISIBLE) {
                getViewModel().acceptMocoQrPayments()
            } else {
                cl_service_fee_information.visibility = View.GONE
                wv_disclaimer.visibility = View.VISIBLE
                tv_title.text = getString(R.string.disclaimer)
            }
        }

        btn_later.setOnClickListener {
            getViewModel().acceptLaterMocoQrPayments()
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        btn_cancel.setOnClickListener {
            showConfirmationMessage(global.citytech.finpos.merchant.utils.MessageConfig.Builder()
                .message(getString(R.string.msg_confirmation_cancel_qr_payments))
                .positiveLabel(getString(R.string.action_yes))
                .onPositiveClick {
                    getViewModel().declineMocoQrPayments()
                }
                .negativeLabel(getString(R.string.action_no))
                .onNegativeClick {
                })

        }
    }

    private fun initObservers() {
        getViewModel().serviceFeeItemsLiveData.observe(this, Observer {
            it?.let {
                val adapter = ServiceFeeRecyclerAdapter(this, it)
                rv_service_fee_information.layoutManager = LinearLayoutManager(this)
                rv_service_fee_information.adapter = adapter
            }
        })

        getViewModel().message.observe(this, Observer {
            it?.let {
                wv_disclaimer.visibility = View.INVISIBLE
                ll_actions.visibility = View.INVISIBLE
                showSuccessMessage(
                    global.citytech.finpos.merchant.utils.MessageConfig.Builder()
                        .message(it)
                        .positiveLabel(getString(R.string.title_ok))
                        .onPositiveClick {
                            finish()
                        })
            }
        })

        getViewModel().isLoading.observe(this, Observer {
            if (it)
                showProgress()
            else
                hideProgress()
        })

        getViewModel().closeActivity.observe(this, Observer {
            finish()
        })
    }

    override fun onStart() {
        super.onStart()
        cl_service_fee_information.visibility = View.VISIBLE
        wv_disclaimer.visibility = View.GONE
        getViewModel().prepareServiceFeeItems()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(Activity.RESULT_CANCELED)
    }

    override fun getBindingVariable(): Int = BR.disclaimerViewModel

    override fun getLayout(): Int = R.layout.activity_moco_disclaimer

    override fun getViewModel(): MocoQrDisclaimerViewModel =
        ViewModelProviders.of(this)[MocoQrDisclaimerViewModel::class.java]
}