package global.citytech.finpos.neps.transaction;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.PinBlock;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.cards.cvm.Cvm;
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat;
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest;
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.AidRetrieveRequest;
import global.citytech.finposframework.repositories.AidRetrieveResponse;
import global.citytech.finposframework.repositories.FieldAttributes;

import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;

import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.utility.CvmResultsParser;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.HEX;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.utility.StringUtils;

public class ContactlessCardPurchaseProcessor implements PurchaseProcessor {

    ReadCardService readCardService;
    TransactionRepository transactionRepository;
    TransactionAuthenticator transactionAuthenticator;
    ApplicationRepository applicationRepository;
    TerminalRepository terminalRepository;

    public ContactlessCardPurchaseProcessor(
            ReadCardService readCardService,
            TransactionRepository transactionRepository,
            TransactionAuthenticator transactionAuthenticator,
            ApplicationRepository applicationRepository,
            TerminalRepository terminalRepository
    ) {
        this.readCardService = readCardService;
        this.transactionRepository = transactionRepository;
        this.transactionAuthenticator = transactionAuthenticator;
        this.applicationRepository = applicationRepository;
        this.terminalRepository = terminalRepository;
    }

    @Override
    public ReadCardResponse purchase(String stan, PurchaseRequest purchaseRequest) {
        ReadCardRequest readCardRequest = this.prepareReadCardRequestForContactless(stan, purchaseRequest);
        ReadCardResponse readCardResponse = readCardService.processEMV(readCardRequest);
        this.highValuePaymentCvmProcessing(readCardResponse); // TODO not needed for NEPS
        if (readCardResponse.getResult() != Result.FAILURE) {
            if (readCardResponse.getResult() == Result.PROCESSING_ERROR) {
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
            } else if (readCardResponse.getResult() == Result.PINPAD_ERROR) {

                throw new PosException(PosError.DEVICE_ERROR_PINPAD_ERROR);
            }
            readCardResponse.getCardDetails().setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
            return readCardResponse;
        } else {

            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    private void highValuePaymentCvmProcessing(ReadCardResponse readCardResponse) {
        if (this.retrieveCvm(readCardResponse) == Cvm.NO_CVM) {
            if (this.isAmountGreaterThanCvmLimit(readCardResponse.getCardDetails().getAid(),readCardResponse.getCardDetails().getAmount())) {
                if (this.isCdCvmEnabled(readCardResponse)) {
                    readCardResponse.setContactlessCvm(ContactlessCvm.DEVICE_OWNER);
                } else {
                    PinBlock pinBlock = this.retrievePinBlock(readCardResponse
                            .getCardDetails().getPrimaryAccountNumber());
                    readCardResponse.getCardDetails().setPinBlock(pinBlock);
                    readCardResponse.setContactlessCvm(ContactlessCvm.ONLINE_PIN);
                }
            }
        }

    }

    private Cvm retrieveCvm(ReadCardResponse readCardResponse) {
        try {
            return CvmResultsParser.retrieveCvmResults(readCardResponse.getCardDetails()
                    .getTagCollection().get(IccData.CVM.getTag()).getData()).getCvm();
        } catch (Exception e) {
            e.printStackTrace();
            return Cvm.UNKNOWN;
        }
    }

    private boolean isAmountGreaterThanCvmLimit(String aid, double amount) {
        double cvmLimitAmount = this.retrieveCvmLimitAmount(aid);
        return amount > cvmLimitAmount;
    }

    private double retrieveCvmLimitAmount(String aid) {
        AidRetrieveRequest aidRetrieveRequest = new AidRetrieveRequest(aid, FieldAttributes.DEVICE_SPECIFIC_S1_F4_3_TERMINAL_CVM_REQUIRED_LIMIT);
        AidRetrieveResponse response = this.applicationRepository.retrieveAid(aidRetrieveRequest);
        try {
            return Double.parseDouble(response.getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private boolean isCdCvmEnabled(ReadCardResponse readCardResponse) {
        try {
            String aip = readCardResponse.getCardDetails().getTagCollection().get(IccData.AIP.getTag()).getData();
            byte[] aipBytes = HEX.hexToBytes(aip);
            return ((aipBytes[0] & 0x02)) == 0x02;
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    private PinBlock retrievePinBlock(String primaryAccountNumber) {
        if (!StringUtils.isEmpty(primaryAccountNumber)) {
            PinRequest pinRequest = new PinRequest(
                    primaryAccountNumber,
                    "Customer Pin Entry",
                    PinBlockFormat.ISO9564_FORMAT_1, //Todo use actual value
                    4, //Todo use actual value
                    12, //Todo use actual value
                    30, //Todo use actual value
                    "",
                    !this.terminalRepository.shouldShufflePinPad()

            );
            PinResponse response = transactionAuthenticator.authenticateUser(pinRequest);
            if (response.getResult() == Result.SUCCESS)
                return response.getPinBlock();
            else if (response.getResult() == Result.USER_CANCELLED)
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            else if (response.getResult() == Result.TIMEOUT)
                throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
            else
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }


    private ReadCardRequest prepareReadCardRequestForContactless(String stan,PurchaseRequest purchaseRequest) {
        ReadCardRequest readCardRequest =  new ReadCardRequest(
                TransactionUtils.getCardTypesFromTransaction(purchaseRequest.getTransactionType()),
                purchaseRequest.getTransactionType(),
                this.retrieveTransactionCode(purchaseRequest.getTransactionType()),
                this.applicationRepository.retrieveEmvParameterRequest()
        );
        readCardRequest.setStan(stan);
        readCardRequest.setAmount(purchaseRequest.getAmount());
        readCardRequest.setCashBackAmount(purchaseRequest.getAdditionalAmount());
        readCardRequest.setPinpadRequired(true);
        readCardRequest.setPrimaryAccountNumber(purchaseRequest.getCardDetails().getPrimaryAccountNumber());
        readCardRequest.setCardDetailsBeforeEmvNext(purchaseRequest.getCardDetails());
        readCardRequest.setTransactionDate(this.retrieveTransactionDate());
        readCardRequest.setTransactionTime(this.retrieveTransactionTime());
        readCardRequest.setPinPadFixedLayout(!this.terminalRepository.shouldShufflePinPad());
        return readCardRequest;
    }


    private String retrieveTransactionCode(TransactionType transactionType) {
        switch (transactionType) {
            case PURCHASE:
                return "00";
            default:
                return "00";
        }
    }



    private String retrieveTransactionTime() {
        return DateUtils.hhmmssTime();
    }

    private String retrieveTransactionDate() {
        return DateUtils.yyMMddDate();
    }

}
