package global.citytech.finpos.processor.nibl.receipt.duplicatereconciliation;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.duplicatereconciliation.DuplicateReconciliationReceiptResponseParameter;

/**
 * Created by Unique Shakya on 12/16/2020.
 */
public class DuplicateReconciliationReceiptResponseModel implements UseCase.Response, DuplicateReconciliationReceiptResponseParameter {

    private Result result;
    private String message;

    public DuplicateReconciliationReceiptResponseModel(Result result, String message) {
        this.result = result;
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }
}
