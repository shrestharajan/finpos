package global.citytech.finpos.merchant.presentation.base

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import global.citytech.easydroid.core.mvp.BaseFragment
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.utils.CustomProgressDialog
import java.lang.ref.WeakReference

abstract class AppBaseFragment: Fragment() {

    private var customProgressDialog: CustomProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        customProgressDialog = CustomProgressDialog(WeakReference(requireActivity()))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getLayoutId(), container, false)
    }

    abstract fun getLayoutId(): Int

    fun showProgress(message: String? = this.getString(R.string.title_please_wait)) {
        customProgressDialog?.show(message!!)
    }

    fun hideProgress() {
        customProgressDialog?.hide()
    }

    fun hideSoftKeyboard(activity:Activity, view: View){
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}