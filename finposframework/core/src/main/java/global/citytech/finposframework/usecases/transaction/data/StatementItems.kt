package global.citytech.finposframework.usecases.transaction.data

data class StatementItems(
    val date: String,
    val description: String,
    val dbCrIndicator: String,
    val amount: String
)

data class StatementList(
    var statementItems: List<StatementItems>? = null,
    var availableBalance:String,
    var currentBalance: String,
    var cashInAmount:String,
    var currenyName:String
)
