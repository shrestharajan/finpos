package global.citytech.finpos.merchant.domain.model.app

enum class QrStatus(
    val code: String,
    val message: String
) {
    QR000("0", "Success"),
    QR001("QR001", "Validating QR Code"),
    QR002("QR002", "Processing transaction"),
    QR003("QR003", "Data not found")
}