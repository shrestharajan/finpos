package global.citytech.finpos.merchant.framework.datasource.core.tipadjustment

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.tipadjustment.TipAdjustmentDataSource
import global.citytech.finpos.merchant.domain.model.tip.TipAdjustmentRequestEntity
import global.citytech.finpos.merchant.domain.model.tip.TipAdjustmentResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.DeviceControllerImpl
import global.citytech.finpos.merchant.framework.datasource.device.LedSourceImpl
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.data.mapToTipAdjustmentResponseUiModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.tipadjustment.TipAdjustmentRequestItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finpos.processor.nibl.transaction.tipadjustment.TipAdjustmentRequestModel
import global.citytech.finpos.processor.nibl.transaction.tipadjustment.TipAdjustmentResponseModel
import global.citytech.finposframework.switches.ProcessorManager
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 5/4/2021.
 */
class TipAdjustmentDataSourceImpl : TipAdjustmentDataSource {
    override fun performTipAdjustment(
        configurationItem: ConfigurationItem,
        tipAdjustmentRequestItem: TipAdjustmentRequestItem
    ): Observable<TipAdjustmentResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val transactionRepository =
            TransactionRepositoryImpl(
                NiblMerchant.INSTANCE
            )
        val transactionRequest = prepareTransactionRequest(tipAdjustmentRequestItem)
        val deviceController = DeviceControllerImpl(NiblMerchant.INSTANCE)
        val printerService = PrinterSourceImpl(NiblMerchant.getActivityContext())
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
        val tipAdjustmentRequester =
            ProcessorManager.getInterface(terminalRepository, NotificationHandler)
                .tipAdjustmentRequester
        val tipAdjustmentRequestEntity = TipAdjustmentRequestEntity(
            transactionRequest!!,
            transactionRepository,
            deviceController,
            printerService,
            applicationRepository,
            LedSourceImpl(NiblMerchant.INSTANCE)
        )
        return Observable.fromCallable {
            (tipAdjustmentRequester.execute(tipAdjustmentRequestEntity.mapToModel()) as TipAdjustmentResponseModel).mapToTipAdjustmentResponseUiModel()
        }
    }

    private fun prepareTransactionRequest(tipAdjustmentRequestItem: TipAdjustmentRequestItem): TransactionRequest? {
        val transactionRequest = TransactionRequest(TransactionType.TIP_ADJUSTMENT)
        transactionRequest.amount = tipAdjustmentRequestItem.originalAmount
        transactionRequest.additionalAmount = tipAdjustmentRequestItem.tipAmount
        transactionRequest.originalInvoiceNumber = tipAdjustmentRequestItem.originalInvoiceNumber
        return transactionRequest
    }
}