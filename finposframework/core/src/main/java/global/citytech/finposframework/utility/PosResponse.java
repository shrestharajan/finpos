package global.citytech.finposframework.utility;

import global.citytech.finposframework.exceptions.PosError;

public class PosResponse {
    PosResult posResult;
    int errorCode;
    String message;
    PosError posError;

    public PosResponse(PosResult posResult) {
        this.posResult = posResult;
    }

    public PosResponse(PosResult posResult, int errorCode, String message) {
        this.posResult = posResult;
        this.errorCode = errorCode;
        this.message = message;
    }

    public PosResponse(PosError posError) {
        this.posResult = PosResult.FAILURE;
        this.posError = posError;
    }

    public PosError getPosError() {
        return posError;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public PosResult getPosResult() {
        return posResult;
    }

    public String getMessage() {
        return message;
    }
}
