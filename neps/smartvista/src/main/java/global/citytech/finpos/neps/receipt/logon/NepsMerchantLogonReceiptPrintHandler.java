package global.citytech.finpos.neps.receipt.logon;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.hardware.io.printer.PrintMessage;
import global.citytech.finposframework.hardware.io.printer.Printable;
import global.citytech.finposframework.hardware.io.printer.PrinterRequest;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.receipt.logon.MerchantLogonReceipt;

public class NepsMerchantLogonReceiptPrintHandler implements ReceiptHandler.MerchantLogonReceiptHandler {

    private PrinterService printerService;

    public NepsMerchantLogonReceiptPrintHandler(PrinterService printerService) {
        this.printerService = printerService;
    }

    @Override
    public void printLogonReceipt(MerchantLogonReceipt merchantLogonReceipt) {
        PrinterRequest printerRequest = this.preparePrinterRequest(merchantLogonReceipt);
        printerService.print(printerRequest);
    }

    private PrinterRequest preparePrinterRequest(MerchantLogonReceipt merchantLogonReceipt) {
        List<Printable> printableList = new ArrayList<>();
        printableList.add(PrintMessage.Companion.getInstance(NepsMerchantLogonReceiptLabels.DIVIDER, NepsMerchantLogonReceiptStyle.DIVIDER.getStyle()));
        printableList.add(PrintMessage.Companion.getInstance(merchantLogonReceipt.getMessage(), NepsMerchantLogonReceiptStyle.LOGON_MESSAGE.getStyle()));
        PrinterRequest printerRequest = new PrinterRequest(printableList);
        printerRequest.setFeedPaperAfterFinish(false);
        return printerRequest;
    }
}
