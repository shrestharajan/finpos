package global.citytech.finpos.processor.neps.cashadvance;

import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.ProcessingCode;
import global.citytech.finpos.neps.iso8583.requestsender.cashadvance.CashAdvanceIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.cashadvance.ICCCashAdvanceRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.cashadvance.MagCashAdvanceRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.cashadvance.PICCCashAdvanceRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionRequestSender;
import global.citytech.finpos.processor.neps.transaction.CardTransactionUseCase;
import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.DateUtils;

public class CardCashAdvanceUseCase extends CardTransactionUseCase {
    @Override
    protected TransactionType getTransactionType() {
        return TransactionType.CASH_ADVANCE;
    }

    @Override
    protected boolean getAuthCompletionStatus() {
        return true;
    }

    public CardCashAdvanceUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    protected String getClassName() {
        return CardCashAdvanceUseCase.class.getName();
    }

    @Override
    protected TransactionIsoRequest prepareIsoRequest(ReadCardResponse readCardResponse, TransactionRequest transactionRequest) {
        String pin = shouldSkipPinBlockFromCard(readCardResponse) ? "" : readCardResponse.getCardDetails().getPinBlock().getPin();
        logger.debug("PIN BLOCK :::: " + pin);
        return CashAdvanceIsoRequest.Builder.newInstance()
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .transactionAmount(readCardResponse.getCardDetails().getAmount())
                .localDateTime(DateUtils.yyMMddHHmmssDate())
                .track2Data(readCardResponse.getCardDetails().getTrackTwoData())
                .currencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .pinBlock(shouldSkipPinBlockFromCard(readCardResponse) ? "" : readCardResponse.getCardDetails().getPinBlock().getPin())
                .emvData(readCardResponse.getCardDetails().getIccDataBlock())
                .fallBackFromIccToMag(readCardResponse.getFallbackIccToMag())
                .build();
    }

    @Override
    protected NepsMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context) {
        TransactionRequestSender sender;
        switch (readCardResponse.getCardDetails().getCardType()) {
            case MAG:
                sender = new MagCashAdvanceRequestSender(context);
                break;
            case PICC:
                sender = new PICCCashAdvanceRequestSender(context);
                break;
            case ICC:
            default:
                sender = new ICCCashAdvanceRequestSender(context);
                break;
        }
        return sender;
    }

    @Override
    protected TransactionResponse prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository,boolean shouldPrint) {
        return CashAdvanceResponse.Builder
                .createDefaultBuilder()
                .stan(stan)
                .approved(transactionApproved)
                .shouldPrintCustomerCopy(shouldPrint)
                .message(this.retrieveMessage(isoMessageResponse, transactionApproved, applicationRepository))
                .debugRequestMessage(isoMessageResponse.getDebugRequestString())
                .debugResponseMessage(isoMessageResponse.getDebugResponseString())
                .build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.CASH_ADVANCE.getCode();
    }

    @Override
    protected TransactionResponse prepareErrorResponse(String responseMessage, boolean shouldPrint) {
        return CashAdvanceResponse.Builder.createDefaultBuilder()
                .stan(this.stan)
                .message(responseMessage)
                .approved(false)
                .shouldPrintCustomerCopy(shouldPrint)
                .build();
    }
}
