package global.citytech.finpos.processor.neps.cardprocessor;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedAction;
import global.citytech.finposframework.hardware.io.led.LedLight;
import global.citytech.finposframework.hardware.io.led.LedRequest;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.sound.Sound;
import global.citytech.finposframework.hardware.io.sound.SoundService;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.PICCWave;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.StringUtils;

public class PICCCardProcessor extends ChipCardProcessor {

    private LedService ledService;
    private SoundService soundService;

    public PICCCardProcessor(
            TransactionRepository transactionRepository,
            TransactionAuthenticator transactionAuthenticator,
            ApplicationRepository applicationRepository,
            ReadCardService readCardService,
            Notifier notifier,
            LedService ledService,
            SoundService soundService,
            TerminalRepository terminalRepository
    ) {
        super(
                transactionRepository,
                transactionAuthenticator,
                applicationRepository,
                readCardService,
                notifier,
                terminalRepository
        );
        this.ledService = ledService;
        this.soundService = soundService;
    }

    @Override
    protected void validateByRespectiveProcessor(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse) {
        super.validateByRespectiveProcessor(readCardRequest,readCardResponse);
        if (readCardResponse.getResult() == Result.PINPAD_ERROR) {
            throw new PosException(PosError.DEVICE_ERROR_PINPAD_ERROR);
        }

        if (readCardResponse.getResult() == Result.SEE_PHONE) {
            throw new PosException(PosError.DEVICE_ERROR_SEE_PHONE);
        }

        if (readCardResponse.getResult() == Result.FAILURE ||
                readCardResponse.getCardDetails() == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (StringUtils.isEmpty(readCardResponse.getCardDetails().getTrackTwoData())) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        PICCWave.getInstance().onPICCWaveSuccess();
    }

    @Override
    protected ReadCardResponse retryByProcessor(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse) {
        return this.handleWaveAgain(readCardRequest);
    }

    private ReadCardResponse handleWaveAgain(ReadCardRequest readCardRequest) {
        if (PICCWave.getInstance().isPICCWaveAgainAllowed()) {
            PICCWave.getInstance().onPICCWaveFailure();
            this.readCardService.cleanUp();
            this.ledService.doTurnLedWith(new LedRequest(LedLight.YELLOW, LedAction.ON));
            this.soundService.playSound(Sound.FAILURE);
            this.notifier.notify(
                    Notifier.EventType.WAVE_AGAIN,
                    Notifier.EventType.WAVE_AGAIN.getDescription()
            );
            ReadCardResponse readCardResponse = this.readCardService.readCardDetails(
                    this.prepareWaveAgainReadCardRequest(readCardRequest));
            return validateReadCardResponse(readCardRequest, readCardResponse);

        } else {
            PICCWave.getInstance().onPICCWaveFailureLimitReached();
            this.ledService.doTurnLedWith(new LedRequest(LedLight.RED, LedAction.ON));
            this.soundService.playSound(Sound.FAILURE);
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    private ReadCardRequest prepareWaveAgainReadCardRequest(ReadCardRequest readCardRequest) {
        List<CardType> cardTypeList = new ArrayList<>();
        cardTypeList.add(CardType.PICC);
        ReadCardRequest waveAgainReadCardRequest = new ReadCardRequest(cardTypeList, readCardRequest.getTransactionType(),
                readCardRequest.getTransactionType9C(), readCardRequest.getEmvParametersRequest());
        waveAgainReadCardRequest.setCashBackAmount(readCardRequest.getCashBackAmount());
        waveAgainReadCardRequest.setAmount(readCardRequest.getAmount());
        waveAgainReadCardRequest.setCardDetailsBeforeEmvNext(readCardRequest.getCardDetailsBeforeEmvNext());
        waveAgainReadCardRequest.setCardReadTimeOut(readCardRequest.getCardReadTimeOut());
        waveAgainReadCardRequest.setFallbackFromIccToMag(readCardRequest.getFallbackFromIccToMag());
        waveAgainReadCardRequest.setForceOnlineForICC(readCardRequest.getForceOnlineForICC());
        waveAgainReadCardRequest.setForceOnlineForPICC(readCardRequest.getForceOnlineForPICC());
        waveAgainReadCardRequest.setIccDataList(readCardRequest.getIccDataList());
        waveAgainReadCardRequest.setMaxmPinLength(readCardRequest.getMaxmPinLength());
        waveAgainReadCardRequest.setMinmPinLength(readCardRequest.getMinmPinLength());
        waveAgainReadCardRequest.setMultipleAidSelectionListener(readCardRequest.getMultipleAidSelectionListener());
        waveAgainReadCardRequest.setPackageName(readCardRequest.getPackageName());
        waveAgainReadCardRequest.setPinBlockFormat(readCardRequest.getPinBlockFormat());
        waveAgainReadCardRequest.setPinpadRequired(readCardRequest.isPinpadRequired());
        waveAgainReadCardRequest.setPinpadTimeout(readCardRequest.getPinpadTimeout());
        waveAgainReadCardRequest.setStan(readCardRequest.getStan());
        waveAgainReadCardRequest.setPrimaryAccountNumber(readCardRequest.getPrimaryAccountNumber());
        waveAgainReadCardRequest.setTransactionDate(DateUtils.yyMMddDate());
        waveAgainReadCardRequest.setTransactionTime(DateUtils.HHmmssTime());
        waveAgainReadCardRequest.setCurrencyName(readCardRequest.getCurrencyName());
        waveAgainReadCardRequest.setPinPadFixedLayout(!terminalRepository.shouldShufflePinPad());
        return waveAgainReadCardRequest;
    }

    @Override
    protected boolean checkForRetry(ReadCardResponse readCardResponse) {
        return readCardResponse.getResult() == Result.WAVE_AGAIN;
    }

    @Override
    protected ReadCardResponse readCardFromFallbackProcessor(ReadCardRequest readCardRequest) {
        return this.handleContactlessFallback(readCardRequest);
    }

    private ReadCardResponse handleContactlessFallback(ReadCardRequest readCardRequest) {
        this.readCardService.cleanUp();
        this.notifier.notify(Notifier.EventType.SWITCH_PICC_TO_ICC,
                Notifier.EventType.SWITCH_PICC_TO_ICC.getDescription());
        ReadCardRequest contactlessFallbackReadCardRequest = this.prepareContactlessFallbackReadCardRequest(readCardRequest);
        ReadCardResponse readCardResponse = this.readCardService.readCardDetails(contactlessFallbackReadCardRequest);
        return new ICCCardProcessor(
                this.transactionRepository,
                this.transactionAuthenticator,
                this.applicationRepository,
                this.readCardService,
                this.notifier,
                terminalRepository
        ).validateReadCardResponse(readCardRequest, readCardResponse);
    }

    private ReadCardRequest prepareContactlessFallbackReadCardRequest(ReadCardRequest readCardRequest) {
        List<CardType> cardTypeList = new ArrayList<>();
        cardTypeList.add(CardType.ICC);
        ReadCardRequest fallbackRequest = new ReadCardRequest(cardTypeList, readCardRequest.getTransactionType(),
                readCardRequest.getTransactionType9C(), readCardRequest.getEmvParametersRequest());
        fallbackRequest.setCashBackAmount(readCardRequest.getCashBackAmount());
        fallbackRequest.setAmount(readCardRequest.getAmount());
        fallbackRequest.setCardDetailsBeforeEmvNext(readCardRequest.getCardDetailsBeforeEmvNext());
        fallbackRequest.setCardReadTimeOut(readCardRequest.getCardReadTimeOut());
        fallbackRequest.setFallbackFromIccToMag(readCardRequest.getFallbackFromIccToMag());
        fallbackRequest.setForceOnlineForICC(readCardRequest.getForceOnlineForICC());
        fallbackRequest.setForceOnlineForPICC(readCardRequest.getForceOnlineForPICC());
        fallbackRequest.setIccDataList(readCardRequest.getIccDataList());
        fallbackRequest.setMaxmPinLength(readCardRequest.getMaxmPinLength());
        fallbackRequest.setMinmPinLength(readCardRequest.getMinmPinLength());
        fallbackRequest.setMultipleAidSelectionListener(readCardRequest.getMultipleAidSelectionListener());
        fallbackRequest.setPackageName(readCardRequest.getPackageName());
        fallbackRequest.setPinBlockFormat(readCardRequest.getPinBlockFormat());
        fallbackRequest.setPinpadRequired(readCardRequest.isPinpadRequired());
        fallbackRequest.setPinpadTimeout(readCardRequest.getPinpadTimeout());
        fallbackRequest.setStan(readCardRequest.getStan());
        fallbackRequest.setPrimaryAccountNumber(readCardRequest.getPrimaryAccountNumber());
        fallbackRequest.setTransactionDate(DateUtils.yyMMddDate());
        fallbackRequest.setTransactionTime(DateUtils.HHmmssTime());
        fallbackRequest.setCurrencyName(readCardRequest.getCurrencyName());
        fallbackRequest.setPinPadFixedLayout(!terminalRepository.shouldShufflePinPad());
        return fallbackRequest;
    }

    @Override
    protected boolean checkFallback(ReadCardResponse readCardResponse) {
        return readCardResponse.getResult() == Result.PICC_FALLBACK_TO_ICC;
    }

    @Override
    protected void handleTransactionDeclinedCases(ReadCardResponse readCardResponse) {
        //Add NFC Specific Txn Declined cases here
    }

    @Override
    protected void handleErrorsByProcessor(ReadCardResponse readCardResponse) {
        //Add NFC Specific Error cases here
    }

    @Override
    protected Boolean checkIfPinRequired(TransactionType transactionType, String cardSchemeId) {
        //Always true for NFC
        return true;
    }

}
