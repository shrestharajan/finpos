package global.citytech.finposframework.hardware.io.cards

/**
 * Created by Saurav Ghimire on 5/6/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


data class CardSummary(
    val cardSchemeLabel:String? =null,
    val primaryAccountNumber:String? =null,
    val cardHolderName:String? =null,
    val expiryDate:String? =null
)
