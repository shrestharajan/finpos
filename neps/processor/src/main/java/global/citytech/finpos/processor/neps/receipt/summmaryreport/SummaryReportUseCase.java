package global.citytech.finpos.processor.neps.receipt.summmaryreport;

import java.util.List;

import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.summaryreport.SummaryReportRequester;
import global.citytech.finposframework.usecases.CardSchemeType;

/**
 * Created by Saurav Ghimire on 3/30/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public class SummaryReportUseCase implements UseCase<SummaryReportRequest, SummaryReportResponse>,
        SummaryReportRequester<SummaryReportRequest, SummaryReportResponse> {

    private TerminalRepository terminalRepository;
    private SummaryReportRequest summaryReportRequest;
    private SummaryReportResponse summaryReportResponse;

    public SummaryReportUseCase(TerminalRepository terminalRepository) {
        this.terminalRepository = terminalRepository;
    }

    @Override
    public SummaryReportResponse execute(SummaryReportRequest request) {
        try {
            prepareSummaryReportAndPrint(request);
        } catch (PosException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
        return this.summaryReportResponse;
    }

    private void prepareSummaryReportAndPrint(SummaryReportRequest request) {
        this.summaryReportRequest = request;
        SummaryReportReceipt summaryReportReceipt = prepareSummaryReportReceipt();
        checkSummaryReportDataAndPrint(summaryReportReceipt);
    }

    private void checkSummaryReportDataAndPrint(SummaryReportReceipt summaryReportReceipt) {
        if (summaryReportIsEmpty(summaryReportReceipt)) {
            assignSummaryResponseWithNoTransactionMessage();
        } else {
            printSummaryReport(summaryReportReceipt);
        }
    }

    private void printSummaryReport(SummaryReportReceipt summaryReportReceipt) {
        SummaryReportReceiptHandler summaryReportReceiptHandler
                = new SummaryReportReceiptHandler(summaryReportRequest.getPrinterService());
        PrinterResponse printerResponse = summaryReportReceiptHandler.print(summaryReportReceipt);
        if (printerResponse.getResult() != Result.SUCCESS) {
            summaryReportResponse = new SummaryReportResponse(
                    printerResponse.getResult(),
                    printerResponse.getMessage()
            );
            return;
        }
        assignSummaryResponseOnFinishPrinting();
    }

    private void assignSummaryResponseOnFinishPrinting() {
        this.summaryReportResponse = new SummaryReportResponse(
                Result.SUCCESS,
                "Summary Report Printed"
        );
    }

    private void assignSummaryResponseWithNoTransactionMessage() {
        throw new PosException(PosError.DEVICE_ERROR_NO_RECENT_TRANSACTION);
    }

    private boolean summaryReportIsEmpty(SummaryReportReceipt summaryReportReceipt) {
        return summaryReportReceipt.getSummaryReport().getMap().isEmpty();
    }

    private SummaryReportReceipt prepareSummaryReportReceipt() {
        String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
        SummaryReportReceiptMaker summaryReportReceiptMaker = new SummaryReportReceiptMaker(
                this.terminalRepository,
                this.summaryReportRequest.getReconciliationRepository()
        );
        List<CardSchemeType> cardSchemeTypes = summaryReportRequest.getApplicationRepository()
                .getSupportedCardSchemes();
        return summaryReportReceiptMaker.prepare(batchNumber, cardSchemeTypes);
    }


}
