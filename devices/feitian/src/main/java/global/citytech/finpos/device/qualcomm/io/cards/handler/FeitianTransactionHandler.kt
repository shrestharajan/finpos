package global.citytech.finpos.device.qualcomm.io.cards.handler

import android.content.Context
import com.ftpos.library.smartpos.datautils.IntTypeValue
import com.ftpos.library.smartpos.emv.Amount
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.emv.TrackData
import com.ftpos.library.smartpos.emv.TransRequest
import com.ftpos.library.smartpos.icreader.IcReader
import com.ftpos.library.smartpos.magreader.MagReader
import com.ftpos.library.smartpos.nfcreader.NfcReader
import global.citytech.common.extensions.isNullOrEmptyOrBlank
import global.citytech.finpos.device.qualcomm.io.cards.detect.FeitianDetectCardResponse
import global.citytech.finpos.device.qualcomm.io.cards.transaction.FeitianSingleton
import global.citytech.finpos.device.qualcomm.io.cards.transaction.FeitianTransactionCallbacks
import global.citytech.finpos.device.qualcomm.io.cards.transaction.TransactionCallbacksListener
import global.citytech.finpos.device.qualcomm.io.led.FeitianLedServiceImpl
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.CARD_PRESENT_IN_ICC_READER_POWER_OFF
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.CARD_PRESENT_IN_ICC_READER_POWER_ON
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.ONLINE_RESULT_FAILURE
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.ONLINE_RESULT_SUCCESS
import global.citytech.finpos.device.qualcomm.utils.FeitianCustomEMVTags.CUSTOM_TAG_APPLICATION_SELECTED
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finpos.device.qualcomm.utils.FeitianPinEntryResult
import global.citytech.finpos.device.qualcomm.utils.FeitianUtils
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.common.Result.*
import global.citytech.finposframework.hardware.io.cards.CardDetails
import global.citytech.finposframework.hardware.io.cards.CardType.*
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService
import global.citytech.finposframework.hardware.io.led.LedAction
import global.citytech.finposframework.hardware.io.led.LedLight
import global.citytech.finposframework.hardware.io.led.LedRequest
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_CALLBACK_AMOUNT
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_MAG_CARD_READ_SUCCESS
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_PHASE_PROCESS_EMV_SUCCESS
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_PHASE_READ_CARD_SUCCESS
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MULTI_AID_LIST_EMPTY
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MULTI_AID_SELECTION_CANCELLED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.TRACK_2_DATA_MAX_LENGTH
import global.citytech.finposframework.hardware.utility.DeviceUtils
import global.citytech.finposframework.hardware.utility.TransactionProcessedState
import global.citytech.finposframework.notifier.Notifier
import global.citytech.finposframework.utility.StringUtils
import java.lang.ref.WeakReference
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * Created by Rishav Chudal on 4/7/21.
 */
abstract class FeitianTransactionHandler(val context: Context, val notifier: Notifier) :
    TransactionCallbacksListener,
    FeitianLogger.Listener {

    private val contextWeakReference = WeakReference(context)
    private val emv = Emv.getInstance(context.applicationContext)
    private val logger = FeitianLogger(this)
    private lateinit var readCardRequest: ReadCardRequest
    private lateinit var readCardResponse: ReadCardResponse

    /**
     * ReadCardDetails
     */
    fun executeReadCardDetails(readCardRequest: ReadCardRequest): ReadCardResponse {
        this.logger.logWithClassName("********** ReadCardDetails **********")
        FeitianSingleton.clearPropertyInstances()
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.READ_CARD
        FeitianSingleton.transactionTimeOutInSecs = DeviceUtils.getTimeOutOrDefault(
            readCardRequest.cardReadTimeOut
        )
        this.readCardRequest = readCardRequest
        FeitianSingleton.readCardRequest = this.readCardRequest
        FeitianSingleton.contextWeakReference = this.contextWeakReference
        this.prepareForTransaction()
        this.awaitDeviceCountDownLatch()
        this.readCardResponse = FeitianSingleton.readCardResponse!!
        return this.readCardResponse
    }

    private fun prepareForTransaction() {
        val transactionAmount = this.readCardRequest.amount
        val cashBackAmount = this.readCardRequest.cashBackAmount
        val transactionType = this.readCardRequest.transactionType
        val amount = FeitianUtils.prepareTransactionAmount(
            transactionAmount,
            cashBackAmount,
            transactionType
        )
        val transRequest = TransRequest(
            Integer.parseInt(this.readCardRequest.transactionType9C),
            this.readCardRequest.emvParametersRequest.transactionCurrencyCode
        )
        transRequest.cardType = FeitianUtils.openCardReaders(this.readCardRequest.cardTypes)
        transRequest.isReadRecordCallback = true
        this.emv.startEMV(
            amount,
            transRequest,
            FeitianTransactionCallbacks(contextWeakReference.get()!!, this, notifier)
        )
    }

    override fun onCardDetectCompleted(detectCardResponse: FeitianDetectCardResponse) {
        this.logger.logWithClassName("onCardDetectCompleted...")
        this.logger.logWithClassName(
            "DetectCardResponse ::: Result ::: ".plus(detectCardResponse.result)
        )

        this.logger.logWithClassName(
            "DetectCardResponse ::: Message ::: ".plus(detectCardResponse.message)
        )
        when (detectCardResponse.result) {
            SUCCESS -> onCardDetectedSuccess(detectCardResponse)
            FAILURE -> onCardDetectFailure()
            TIMEOUT -> onCardDetectTimeOut()
            SWIPE_AGAIN -> onMagSwipeAgainRequired()
            ICC_CARD_DETECT_ERROR -> onICCReaderCardDetectError()
            else -> onOtherDetectResponse()
        }
    }

    override fun onAmountRequired(): Amount {
        prepareReadCardResponse(
            null,
            Result.CALLBACK_AMOUNT,
            MSG_CALLBACK_AMOUNT
        )
        this.releaseDeviceCountDownLatch()
        this.awaitKernelCountDownLatch()
        val amount = FeitianUtils.prepareTransactionAmount(
            FeitianSingleton.readCardRequest!!.amount,
            FeitianSingleton.readCardRequest!!.cashBackAmount,
            FeitianSingleton.readCardRequest!!.transactionType
        )
        return amount!!
    }

    override fun onAppSelectionProcessCompleted(selectApplicationIndex: Int) {
        when (selectApplicationIndex) {
            MULTI_AID_LIST_EMPTY -> onApplicationSelectionFailed()
            MULTI_AID_SELECTION_CANCELLED -> onApplicationSelectionCancelled()
            else -> proceedOnSelectedApplication(selectApplicationIndex)
        }
    }

    override fun collectBasicEMVDataAndReturnResponseForICC() {
        val cardDetails = CardDetails()
        cardDetails.cardType = ICC
        cardDetails.amount = FeitianSingleton.readCardRequest!!.amount
        cardDetails.cashBackAmount = FeitianSingleton.readCardRequest!!.cashBackAmount
        FeitianUtils.addEmvDataOnCardDetails(this.emv, cardDetails)
        this.prepareReadCardResponse(
            cardDetails,
            SUCCESS,
            MSG_PHASE_READ_CARD_SUCCESS
        )
        this.releaseDeviceCountDownLatch()
    }

    override fun onPinEntryProcessCompleted(pinEntryResult: FeitianPinEntryResult) {
        this.logger.logWithClassName("OnPinEntryProcessCompleted...")
        this.logger.logWithClassName(
            "FeitianPinPadResult ::: Result ::: ".plus(pinEntryResult.result)
        )
        this.logger.logWithClassName(
            "FeitianPinPadResult ::: TLV Data ::: ".plus(pinEntryResult.tlvData)
        )
        when (pinEntryResult.result) {
            SUCCESS -> onPinEntrySuccess(pinEntryResult)
            PIN_BYPASSED -> onPinEntryByPassed(pinEntryResult)
            TIMEOUT -> this.onPinEntryTimeOut()
            USER_CANCELLED -> this.onPinEntryCancelled()
            else -> this.onPinEntryFailure()
        }
    }

    override fun onOnlineConnectionToHostRequired(transData: String?) {
        this.logger.logWithClassName("onOnlineConnectionToHostRequired...")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.NEED_TO_GO_ONLINE
        val cardDetails = CardDetails()
        this.prepareCardDetails(cardDetails, transData)
        prepareReadCardResponse(
            cardDetails,
            SUCCESS,
            MSG_PHASE_PROCESS_EMV_SUCCESS
        )
        releaseDeviceCountDownLatch()
    }

    override fun onTransactionEndProcess(readCardResponse: ReadCardResponse) {
        this.readCardResponse = readCardResponse
        this.releaseDeviceCountDownLatch()
    }

    override fun cleanUpRequired() {
        this.executeCleanUp()
    }

    private fun onCardDetectFailure() {
        this.logger.logWithClassName("Detect Card Response Failure...")
        FeitianSingleton.currentCardDetected = UNKNOWN
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.SEARCH_CARD_FAILED
        abortEmvProcess()
    }

    private fun onCardDetectTimeOut() {
        this.logger.logWithClassName("Detect Card Response TimeOut...")
        FeitianSingleton.currentCardDetected = UNKNOWN
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.SEARCH_CARD_TIMEOUT
        abortEmvProcess()
    }

    private fun onMagSwipeAgainRequired() {
        this.logger.logWithClassName("Mag Swipe Again Required...")
        FeitianSingleton.currentCardDetected = MAG
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.MAG_NEEDS_SWIPE_AGAIN
        abortEmvProcess()
    }

    private fun onICCReaderCardDetectError() {
        this.logger.logWithClassName("ICC Reader Card Detect Error...")
        FeitianSingleton.currentCardDetected = ICC
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.ICC_READER_CARD_DETECT_ERROR
        abortEmvProcess()
    }

    private fun onOtherDetectResponse() {
        this.logger.logWithClassName("Detect Card Response Other...")
        FeitianSingleton.currentCardDetected = UNKNOWN
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.CARD_DETECTED_UNKNOWN
        abortEmvProcess()
    }

    private fun onCardDetectedSuccess(detectCardResponse: FeitianDetectCardResponse) {
        when (detectCardResponse.cardDetected) {
            MAG -> onCardTypeMagDetected(detectCardResponse)
            ICC -> onCardTypeICCDetected()
            PICC -> onCardTypePICCDetected()
            else -> onNoCardDetected()
        }
    }

    private fun onCardTypeMagDetected(detectCardResponse: FeitianDetectCardResponse) {
        this.logger.logWithClassName("Proceed for Mag Card...")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.CARD_DETECTED_MAG
        val trackData = detectCardResponse.trackData
        logger.logWithClassName("Track 1 Data ::: ".plus(trackData?.track1Data))
        logger.logWithClassName("Track 2 Data ::: ".plus(trackData?.track2Data))
        logger.logWithClassName("Track 3 Data ::: ".plus(trackData?.track3Data))
        if (trackData != null && !trackData.track2Data.isNullOrEmptyOrBlank()) {
            this.prepareMagneticCardResponse(trackData)
        } else {
            logger.logWithClassName("Track 2 Error Code ::: ".plus(trackData?.track2errode))
            this.prepareForSwipeAgainResponse()
        }
    }

    private fun prepareMagneticCardResponse(trackData: TrackData) {
        val cardDetails = CardDetails()
        cardDetails.cardType = MAG

        val pan = DeviceUtils.panFromTrackTwoData(trackData.track2Data)
        this.logger.debugWithClassName("PAN ::: ".plus(StringUtils.maskCardNumber(pan)))
        cardDetails.primaryAccountNumber = pan

        val cardHolderName = DeviceUtils.cardHolderNameFromTrackOneData(trackData.track1Data)
        this.logger.debugWithClassName("Card Holder Name ::: ".plus(cardHolderName))
        cardDetails.cardHolderName = cardHolderName

        val expiryDate = DeviceUtils.expiryDateFromTrackTwoData(trackData.track2Data)
        this.logger.debugWithClassName("Expiry Date ::: ".plus(expiryDate))
        cardDetails.expiryDate = expiryDate

        var trackTwoData = trackData.track2Data
        if (trackTwoData.length > TRACK_2_DATA_MAX_LENGTH) {
            trackTwoData = trackTwoData.substring(0, TRACK_2_DATA_MAX_LENGTH)
        }
        this.logger.debugWithClassName("Track 2 Data ::: ".plus(StringUtils.maskTrack2(trackTwoData)))
        cardDetails.trackTwoData = trackTwoData

        cardDetails.amount = readCardRequest.amount
        cardDetails.cashBackAmount = readCardRequest.cashBackAmount
        prepareReadCardResponse(
            cardDetails,
            SUCCESS,
            MSG_MAG_CARD_READ_SUCCESS
        )
        releaseDeviceCountDownLatch()
        abortEmvProcess()
    }

    private fun prepareForSwipeAgainResponse() {
        this.logger.logWithClassName("Prepare for Swipe Again Response...")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.MAG_NEEDS_SWIPE_AGAIN
        abortEmvProcess()
    }

    private fun onCardTypeICCDetected() {
        this.logger.logWithClassName("Proceed For ICC Card...")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.CARD_DETECTED_ICC
        FeitianSingleton.currentCardDetected = ICC
        respondEMVEvent(null)
    }

    private fun onCardTypePICCDetected() {
        this.logger.logWithClassName("Proceed For PICC Card...")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.CARD_DETECTED_PICC
        FeitianSingleton.currentCardDetected = PICC
        respondEMVEvent(null)
    }

    private fun onNoCardDetected() {
        this.logger.logWithClassName("onNoCardDetected...")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.CARD_DETECTED_UNKNOWN
        abortEmvProcess()
    }

    /**
     * Read EMV
     */
    protected fun executeReadEMV(readCardRequest: ReadCardRequest): ReadCardResponse {
        this.logger.logWithClassName("********** Read EMV **********")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.READ_EMV
        this.readCardRequest = readCardRequest
        FeitianSingleton.readCardRequest = this.readCardRequest
        FeitianSingleton.contextWeakReference = this.contextWeakReference
        FeitianSingleton.currentCardDetected = this.readCardRequest.cardTypes[0]
        this.releaseKernelCountDownLatch()
        this.awaitDeviceCountDownLatch()
        this.readCardResponse = FeitianSingleton.readCardResponse!!
        return this.readCardResponse
    }

    /**
     * Process EMV
     */
    protected fun executeProcessEMV(readCardRequest: ReadCardRequest): ReadCardResponse {
        this.logger.logWithClassName("********** Phase Process EMV **********")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.PROCESS_EMV
        this.readCardRequest = readCardRequest
        FeitianSingleton.readCardRequest = this.readCardRequest
        FeitianSingleton.contextWeakReference = this.contextWeakReference
        this.processEMVBasedOnCardDetected()
        readCardResponse = FeitianSingleton.readCardResponse!!
        return readCardResponse
    }

    private fun processEMVBasedOnCardDetected() {
        when (FeitianSingleton.currentCardDetected) {
            ICC -> processEMVOnICCTransaction()
            PICC -> processEMVOnPICCTransaction()
            else -> onProcessEMVError()
        }
    }

    private fun processEMVOnICCTransaction() {
        this.logger.logWithClassName("processEMVOnICCTransaction...")
        respondEMVEvent(null)
        awaitDeviceCountDownLatch()
    }

    private fun processEMVOnPICCTransaction() {
        this.logger.logWithClassName("processEMVOnPICCTransaction...")
        this.readCardResponse = FeitianUtils.prepareProcessEMVReadCardResponseForPICC(
            this.emv,
            this.readCardRequest
        )
        FeitianSingleton.readCardResponse = this.readCardResponse
        releaseTransactionLatches()
    }

    private fun onProcessEMVError() {
        this.logger.logWithClassName("onProcessEMVError...")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.PROCESSING_ERROR
        abortEmvProcess()
    }

    private fun prepareCardDetails(cardDetails: CardDetails, transData: String?) {
        cardDetails.cardType = FeitianSingleton.currentCardDetected
        cardDetails.amount = FeitianSingleton.readCardRequest!!.amount
        cardDetails.cashBackAmount = FeitianSingleton.readCardRequest!!.cashBackAmount
        cardDetails.pinBlock = FeitianSingleton.pinBlock
        FeitianUtils.addBasicEMVDataBasedOnCardDetails(
            emv,
            FeitianSingleton,
            cardDetails
        )
        FeitianUtils.addICCDataBlockAndTagCollection(transData, emv, cardDetails)
    }

    private fun onApplicationSelectionFailed() {
        this.logger.logWithClassName("onApplicationSelectionFailed...")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.APPLICATION_SELECTION_FAILED;
        abortEmvProcess()
    }

    private fun onApplicationSelectionCancelled() {
        this.logger.logWithClassName("onApplicationSelectionCancelled...")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.APPLICATION_SELECTION_CANCELLED
        abortEmvProcess()
    }

    private fun proceedOnSelectedApplication(selectedApplicationIndex: Int) {
        this.logger.logWithClassName("proceedOnSelectedApplication...")
        this.logger.logWithClassName(
            "Selected Application Index ::: ".plus(selectedApplicationIndex)
        )
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.APPLICATION_SELECTED
        val selectAppTlvData = java.lang.String.format(
            CUSTOM_TAG_APPLICATION_SELECTED.tagInHex()
                .plus(CUSTOM_TAG_APPLICATION_SELECTED.lengthInHex())
                .plus("%02x"),
            selectedApplicationIndex
        )
        this.logger.logWithClassName("Select APP TLV Data ::: ".plus(selectAppTlvData))
        respondEMVEvent(selectAppTlvData)
    }

    private fun onPinEntrySuccess(pinEntryResult: FeitianPinEntryResult) {
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.PIN_ENTRY_SUCCESS
        respondEMVEvent(pinEntryResult.tlvData)
    }

    private fun onPinEntryByPassed(pinEntryResult: FeitianPinEntryResult) {
        logger.logWithClassName("***** PIN ENTRY BYPASSED *****")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.PIN_ENTRY_BYPASS
        respondEMVEvent(pinEntryResult.tlvData)
    }

    private fun onPinEntryFailure() {
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.PINPAD_ERROR
        abortEmvProcess()
    }

    private fun onPinEntryTimeOut() {
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.PIN_ENTRY_TIMEOUT
        abortEmvProcess()
    }

    private fun onPinEntryCancelled() {
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.PIN_ENTRY_CANCEL
        abortEmvProcess()
    }

    /**
     * Setting EMV Online Result
     */
    protected fun executeEMVOnlineResultSet(
        responseCode: Int,
        onlineResult: ReadCardService.OnlineResult,
        responseField55Data: String
    ): ReadCardResponse {
        this.logger.logWithClassName("********** Phase Set Online Result **********")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.SET_ONLINE_RESULT
        FeitianSingleton.contextWeakReference = this.contextWeakReference
        this.readCardRequest = FeitianSingleton.readCardRequest!!
        this.proceedOnSettingOnlineResult(responseCode, onlineResult, responseField55Data)
        this.readCardResponse = FeitianSingleton.readCardResponse!!
        return this.readCardResponse
    }

    private fun proceedOnSettingOnlineResult(
        responseCode: Int,
        onlineResult: ReadCardService.OnlineResult,
        issuerResponseData: String
    ) {
        this.logger.debugWithClassName("Response Code ::: ".plus(responseCode))
        this.logger.logWithClassName("Online Result ::: ".plus(onlineResult.name))
        this.logger.debugWithClassName("Issuer Response Data ::: ".plus(issuerResponseData))
        when (onlineResult) {
            ReadCardService.OnlineResult.ONLINE_APPROVED -> onApprovedResponseFromHost(
                issuerResponseData
            )
            ReadCardService.OnlineResult.UNABLE_TO_GO_ONLINE -> onConnectionFailureToHost(
                issuerResponseData
            )
            else -> onDeclinedResponseFromHost(issuerResponseData)
        }
    }

    private fun onApprovedResponseFromHost(issuerResponseData: String) {
        this.logger.logWithClassName("onApprovedResponseFromHost...")
        FeitianSingleton.hostConnected = true
        this.setOnlineResultInSDK(
            ONLINE_RESULT_SUCCESS,
            issuerResponseData
        )
    }

    private fun onConnectionFailureToHost(issuerResponseData: String) {
        this.logger.logWithClassName("onConnectionFailureToHost...")
        FeitianSingleton.hostConnected = false
        setOnlineResultInSDK(
            ONLINE_RESULT_FAILURE,
            issuerResponseData
        )
    }

    private fun onDeclinedResponseFromHost(issuerResponseData: String) {
        this.logger.logWithClassName("onDeclinedResponseFromHost...")
        FeitianSingleton.hostConnected = true
        setOnlineResultInSDK(
            ONLINE_RESULT_SUCCESS,
            issuerResponseData
        )
    }

    private fun setOnlineResultInSDK(
        onlineResult: Int,
        issuerResponseData: String
    ) {
        val authenticationCodeDE38: String? = null
        val authenticationResponseCodeDE39: String =
            FeitianUtils.retrieveDateElement39FromIssuerResponse(issuerResponseData)
        val issuerAuthenticationDataTag91: String =
            FeitianUtils.retrieveTag91FromIssuerResponseData(issuerResponseData)
        val issuerScriptTag71: String =
            FeitianUtils.retrieveIssuerScript71FromIssuerResponseData(issuerResponseData)
        val issuerScriptTag72: String =
            FeitianUtils.retrieveIssuerScript72FromIssuerResponseData(issuerResponseData)
        emv.setIssuerOnlineResponseData(
            onlineResult,
            authenticationCodeDE38,
            authenticationResponseCodeDE39,
            issuerAuthenticationDataTag91,
            issuerScriptTag71,
            issuerScriptTag72
        )
        respondEMVEvent(null)
    }


    /**
     * Phase : Clean Up
     */
    protected fun executeCleanUp() {
        this.logger.logWithClassName("********** Clean Up **********")
        abortEmvProcess()
        releaseTransactionLatches()
        dismissAlertDialogIfShowing()
        dismissDialogIfShowing()
        cancelCardReadersCardSearch()
        closeAllLEDs()
    }

    private fun abortEmvProcess() {
        this.emv.stopEMV()
    }

    private fun releaseTransactionLatches() {
        releaseDeviceCountDownLatch()
        releaseKernelCountDownLatch()
    }

    private fun dismissAlertDialogIfShowing() {
        val alertDialogWeakReference = FeitianSingleton.alertDialogWeakReference
        if (alertDialogWeakReference?.get() != null &&
            alertDialogWeakReference.get()!!.isShowing
        ) {
            alertDialogWeakReference.get()!!.dismiss()
        }
    }

    private fun dismissDialogIfShowing() {
        val dialogWeakReference = FeitianSingleton.dialogWeakReference
        if (dialogWeakReference?.get() != null &&
            dialogWeakReference.get()!!.isShowing
        ) {
            dialogWeakReference.get()!!.dismiss()
        }
    }

    private fun cancelCardReadersCardSearch() {
        this.logger.logWithClassName("Cancelling and closing card readers...")
        cancelSearchForMagReader()
        cancelSearchForPICCReader()
        cancelSearchForICCReader()
    }

    private fun cancelSearchForPICCReader() {
        this.logger.logWithClassName("Cancelling and closing PICC reader...")
        val piccReader = NfcReader.getInstance(this.context.applicationContext)
        piccReader.cancel()
        val resultPICCReaderClose = piccReader.close()
        this.logger.logWithClassName("Close PICC reader ::: ".plus(resultPICCReaderClose))
    }

    private fun cancelSearchForICCReader() {
        this.logger.logWithClassName("Cancelling and closing ICC reader...")
        val iccReader = IcReader.getInstance(this.context.applicationContext)
        iccReader.cancel()
        val resultICCReaderClose = iccReader.close()
        this.logger.logWithClassName("Close ICC reader ::: ".plus(resultICCReaderClose))
    }

    private fun cancelSearchForMagReader() {
        this.logger.logWithClassName("Cancelling and closing Mag reader...")
        val magReader = MagReader.getInstance(this.context.applicationContext)
        magReader.cancel()
        this.logger.logWithClassName("Mag Reader Closed ::: ")
    }

    private fun closeAllLEDs() {
        val ledService = FeitianLedServiceImpl(this.context)
        ledService.doTurnLedWith(LedRequest(LedLight.ALL, LedAction.OFF))
    }

    private fun prepareReadCardResponse(
        cardDetails: CardDetails?,
        result: Result,
        message: String
    ) {
        this.logger.logWithClassName("Result ::: ".plus(result))
        this.logger.logWithClassName("Message ::: ".plus(message))
        readCardResponse = ReadCardResponse(
            cardDetails,
            result,
            message
        )
        FeitianSingleton.readCardResponse = readCardResponse
    }


    /**
     * Transaction CountDownLatches
     * Initialize, Await and Release
     */
    private fun initDeviceCountDownLatch() {
        this.logger.logWithClassName("Initializing Device CountDown Latch...")
        val deviceCountDownLatch = CountDownLatch(1)
        FeitianSingleton.deviceCountDownLatch = deviceCountDownLatch
    }

    private fun releaseDeviceCountDownLatch() {
        if (FeitianSingleton.deviceCountDownLatch != null) {
            this.logger.logWithClassName(
                "releasing DeviceCountDownLatch ::: " + FeitianSingleton.deviceCountDownLatch
            )
            FeitianSingleton.deviceCountDownLatch!!.countDown()
            this.logger.logWithClassName(
                "DeviceCountDownLatch after release ::: " + FeitianSingleton.deviceCountDownLatch
            )
        }
    }

    private fun awaitDeviceCountDownLatch() {
        initDeviceCountDownLatch()
        try {
            this.logger.logWithClassName(
                "Awaiting Device CountDownLatch ::: ".plus(FeitianSingleton.deviceCountDownLatch)
            )
            val latchIsAwaiting: Boolean = FeitianSingleton.deviceCountDownLatch!!.await(
                FeitianSingleton.transactionTimeOutInSecs.toLong(),
                TimeUnit.SECONDS
            )
            if (!latchIsAwaiting) {
                onTransactionLatchesTimeOut()
            }
        } catch (ex: Exception) {
            this.logger.logWithClassName("Exception ::: " + ex.message)
        }
    }

    private fun onTransactionLatchesTimeOut() {
        this.logger.logWithClassName("onDeviceLatchTimeOut...")
        FeitianSingleton.transactionProcessedState = TransactionProcessedState.TRANSACTION_LATCHES_TIMEOUT
        this.executeCleanUp()
    }

    private fun initKernelCountDownLatch() {
        this.logger.logWithClassName("Initializing Kernel CountDown Latch...")
        val kernelCountDownLatch = CountDownLatch(1)
        FeitianSingleton.kernelCountDownLatch = kernelCountDownLatch
    }

    private fun releaseKernelCountDownLatch() {
        if (FeitianSingleton.kernelCountDownLatch != null) {
            this.logger.logWithClassName(
                "releasing KernelCountDownLatch ::: " + FeitianSingleton.kernelCountDownLatch
            )
            FeitianSingleton.kernelCountDownLatch!!.countDown()
        }
    }

    private fun awaitKernelCountDownLatch() {
        if (FeitianSingleton.kernelCountDownLatch == null) {
            initKernelCountDownLatch()
        }
        try {
            this.logger.logWithClassName(
                "Awaiting Kernel CountDownLatch ::: ".plus(FeitianSingleton.kernelCountDownLatch)
            )
            val latchIsAwaiting: Boolean = FeitianSingleton.kernelCountDownLatch!!.await(
                FeitianSingleton.transactionTimeOutInSecs.toLong(),
                TimeUnit.SECONDS
            )
            if (!latchIsAwaiting) {
                onTransactionLatchesTimeOut()
            }
        } catch (ex: Exception) {
            this.logger.logWithClassName("Exception ::: " + ex.message)
        }
    }

    private fun respondEMVEvent(tlvData: String?) {
        this.logger.logWithClassName("Responding EMV Event...")
        emv.respondEvent(tlvData)
    }

    /**
     * Check ICC Present Status
     */
    protected fun executeCheckICCPresentStatus(): ReadCardResponse {
        this.logger.logWithClassName("Checking for ICC presence in the reader...")
        return this.initializeIcReaderAndProceed()
    }

    private fun initializeIcReaderAndProceed(): ReadCardResponse {
        val iccReader = IcReader.getInstance(this.context.applicationContext)
        val intTypeValue = IntTypeValue()
        return retrieveCardStatus(
            iccReader,
            intTypeValue
        )
    }

    private fun retrieveCardStatus(
        iccReader: IcReader,
        intTypeValue: IntTypeValue
    ): ReadCardResponse {
        val resultICCReaderCardStatus = iccReader.getCardStatus(intTypeValue)
        this.logger.logWithClassName(
            "Get Card Status invocation result ::: ".plus(resultICCReaderCardStatus)
        )
        this.logger.logWithClassName("IntTypeValue Data ::: ".plus(intTypeValue.data))
        return analyzeCardStatusAndReturnResponse(intTypeValue)
    }

    private fun analyzeCardStatusAndReturnResponse(intTypeValue: IntTypeValue): ReadCardResponse {
        var checkICCPresentStatusResponse = ReadCardResponse(
            null,
            ICC_CARD_NOT_PRESENT,
            ""
        )
        if (intTypeValue.data == CARD_PRESENT_IN_ICC_READER_POWER_OFF ||
            intTypeValue.data == CARD_PRESENT_IN_ICC_READER_POWER_ON) {
            checkICCPresentStatusResponse =
                ReadCardResponse(
                    null,
                    ICC_CARD_PRESENT,
                    ""
                )
        }
        return checkICCPresentStatusResponse
    }

    override fun getClassName(): String = FeitianTransactionHandler::class.java.simpleName
}