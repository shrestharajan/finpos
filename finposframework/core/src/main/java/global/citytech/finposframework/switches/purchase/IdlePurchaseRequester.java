package global.citytech.finposframework.switches.purchase;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Rishav Chudal on 11/23/20.
 */
public interface IdlePurchaseRequester<
        T extends PurchaseRequestParameter, E extends PurchaseResponseParameter>
        extends Request<T, E> {}
