package global.citytech.finposframework.switches.logon;

import global.citytech.finposframework.switches.RequestParameter;

/** @author rajudhital on 3/29/20 */
public interface LogOnRequestParameter extends RequestParameter {}
