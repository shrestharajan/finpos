package global.citytech.finpos.processor.neps.transactiontype;

import java.util.List;

import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.transactiontype.TransactionTypeResponseParameter;

/**
 * Created by Unique Shakya on 1/20/2021.
 */
public class TransactionTypeResponseModel implements TransactionTypeResponseParameter, UseCase.Response {

    private List<String> supportedTransactionTypes;

    public TransactionTypeResponseModel(List<String> supportedTransactionTypes) {
        this.supportedTransactionTypes = supportedTransactionTypes;
    }

    public List<String> getSupportedTransactionTypes() {
        return supportedTransactionTypes;
    }
}
