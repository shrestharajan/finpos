package global.citytech.finpos.merchant.domain.repository.core.pinchange

import global.citytech.finpos.merchant.domain.model.pinchange.PinChangeResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.pinchange.PinChangeRequestItem
import io.reactivex.Observable

interface PinChangeRepository {
    fun pinChangeRequest(
        configurationItem: ConfigurationItem,
        pinChangeRequestItem: PinChangeRequestItem
    ): Observable<PinChangeResponseEntity>
}