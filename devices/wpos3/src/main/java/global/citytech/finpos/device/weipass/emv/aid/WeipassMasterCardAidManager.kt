package global.citytech.finpos.device.weipass.emv.aid

import com.wiseasy.emvprocess.KernelInit
import com.wiseasy.emvprocess.bean.AIDData
import com.wiseasy.emvprocess.bean.BaseAIDData
import com.wiseasy.emvprocess.bean.CLPayPassAIDData
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finpos.device.weipass.utils.defaultOrEmptyValue
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import sdk4.wangpos.libemvbinder.utils.MoneyUtil

/**
 * Created by Rishav Chudal on 5/4/20.
 */
class WeipassMasterCardAidManager constructor(
    val baseAIDData: BaseAIDData,
    val aidParameters: global.citytech.finposframework.hardware.emv.aid.AidParameters,
    val emvParametersRequest: EmvParametersRequest,
    val supportedTransactionTypes: List<String>
) {
    private var finalResult = 0

    fun injectMCAidConfigurations(): Int {
        var successCount = 0
        supportedTransactionTypes.forEach {
            injectMCAidConfigurationForTransTypePurchase(it)
        }
        if (finalResult == 0) {
            successCount++
        }
        return successCount
    }

    private fun injectMCAidConfigurationForTransTypePurchase(transactionType: String) {
        val sdkAIDData = AIDData()
        sdkAIDData.baseAIDData = baseAIDData
        sdkAIDData.clAIDData = prepareCLPayPassAIDDataForPurchase(transactionType)
        injectAidInCore(sdkAIDData)
    }

    private fun prepareCLPayPassAIDDataForPurchase(transactionType: String): CLPayPassAIDData {
        WeipassLogger.debug("===== CLPayPassAIDData ::: Contactless =====")
        val clPayPassAIDData = CLPayPassAIDData()

        val msdAppVersion = aidParameters.msdAppVersion.defaultOrEmptyValue()
        clPayPassAIDData.msdAppVersion_9F6D = msdAppVersion
        WeipassLogger.debug("MSD App Version ::: ".plus(msdAppVersion))

        val udol = aidParameters.udol.defaultOrEmptyValue()
        clPayPassAIDData.udoL_DF811A = udol
        WeipassLogger.debug("UDOL ::: ".plus(udol))

        var cdCvmTransLimit = aidParameters.cdCvmLimit.defaultOrEmptyValue()
        cdCvmTransLimit = MoneyUtil.toCent(cdCvmTransLimit)
        clPayPassAIDData.clTransLimitCDCVM_DF8125 = cdCvmTransLimit
        WeipassLogger.debug("CL Trans Limit CD CVM ::: ".plus(cdCvmTransLimit))

        var noCdCvmTransLimit = aidParameters.noCdCvmLimit.defaultOrEmptyValue()
        noCdCvmTransLimit = MoneyUtil.toCent(noCdCvmTransLimit)
        clPayPassAIDData.clTransLimitNoCDCVM_DF8124 = noCdCvmTransLimit
        WeipassLogger.debug("CL Trans Limit CD CVM ::: ".plus(noCdCvmTransLimit))

        val tacDenial = aidParameters.clTacDenial.defaultOrEmptyValue()
        clPayPassAIDData.cltacDenial_DF8121 = tacDenial
        WeipassLogger.debug("CL TAC Denial ::: ".plus(tacDenial))

        val tacOnline = aidParameters.clTacOnline.defaultOrEmptyValue()
        clPayPassAIDData.cltacOnline_DF8122 = tacOnline
        WeipassLogger.debug("CL TAC Online ::: ".plus(tacOnline))

        val tacDefault = aidParameters.clTacDefault.defaultOrEmptyValue()
        clPayPassAIDData.clTACDefault_DF8120 = tacDefault
        WeipassLogger.debug("CL TAC Default ::: ".plus(tacDefault))

        clPayPassAIDData.transType_9C = transactionType
        WeipassLogger.debug("Trans Type 9C ::: ".plus(transactionType))

        val enableFloorLimit = "1"
        clPayPassAIDData.enableFloorLimit = enableFloorLimit
        WeipassLogger.debug("Enable Floor Limit ::: ".plus(enableFloorLimit))

        val enableClFloorLimit = "1"
        clPayPassAIDData.enableCLFloorLimit = enableClFloorLimit
        WeipassLogger.debug("Enable CL Floor Limit ::: ".plus(enableFloorLimit))

        val enableTransLimit = "1"
        clPayPassAIDData.enableTransLimit = enableTransLimit
        WeipassLogger.debug("Enable Trans Limit ::: ".plus(enableTransLimit))

        val enableCvmLimit = "1"
        clPayPassAIDData.enableCVMLimit = enableCvmLimit
        WeipassLogger.debug("Enable CVM Limit ::: ".plus(enableCvmLimit))

        val enableTransLimitOnDevice = "1"
        clPayPassAIDData.enableTransLimitONdevice = enableTransLimitOnDevice
        WeipassLogger.debug("Enable Trans Limit On Device ::: ".plus(enableTransLimitOnDevice))

        val enabletTransLimitNoOnDevice = "1"
        clPayPassAIDData.enableTransLimitNoONdevice = enabletTransLimitNoOnDevice
        WeipassLogger.debug("Enable Trans Limit No On Device ::: ".plus(enabletTransLimitNoOnDevice))

        var floorLimit = aidParameters.contactlessFloorLimit.defaultOrEmptyValue()
        floorLimit = MoneyUtil.toCent(floorLimit)
        clPayPassAIDData.clFloorLimit = floorLimit
        WeipassLogger.debug("CL Floor Limit ::: ".plus(floorLimit))

        var cvmLimit = aidParameters.cvmLimit.defaultOrEmptyValue()
        cvmLimit = MoneyUtil.toCent(cvmLimit)
        clPayPassAIDData.clcvmLimit = cvmLimit
        WeipassLogger.debug("CL CVM Limit ::: ".plus(cvmLimit))

        val chipCvmCapData = aidParameters.cvmCapCvmRequired.defaultOrEmptyValue()
        clPayPassAIDData.chipCVMCap_DF8118 = chipCvmCapData.defaultOrEmptyValue()
        WeipassLogger.debug("ChipCVMCap ::: ".plus(chipCvmCapData.defaultOrEmptyValue()))

        val chipCvmCapNoCvmData = aidParameters.cvmCapNoCvmRequired.defaultOrEmptyValue()
        clPayPassAIDData.chipCVMCapNoCVM_DF8119 = chipCvmCapNoCvmData.defaultOrEmptyValue()
        WeipassLogger.debug("ChipCVMCapNoCVM ::: " + chipCvmCapNoCvmData.defaultOrEmptyValue())

        val cardDataInputCap = aidParameters.clCardDataInputCapability.defaultOrEmptyValue()
        clPayPassAIDData.cardDataInputCap_DF8117 = cardDataInputCap.defaultOrEmptyValue()
        WeipassLogger.debug("CardDataInputCapability ::: " + cardDataInputCap.defaultOrEmptyValue())

        val securityCap = aidParameters.clSecurityCapability.defaultOrEmptyValue()
        clPayPassAIDData.securityCap_DF811F = securityCap.defaultOrEmptyValue()
        WeipassLogger.debug("SecurityCapability ::: " + securityCap.defaultOrEmptyValue())

        val kernelConfig = aidParameters.kernelConfig.defaultOrEmptyValue()
        clPayPassAIDData.kernelConfig_DF811B = kernelConfig.defaultOrEmptyValue()
        WeipassLogger.debug("KernelConfig ::: " + kernelConfig.defaultOrEmptyValue())

        return clPayPassAIDData
    }

    private fun injectAidInCore(aidData: AIDData) {
        WeipassLogger.log("Injecting the MasterCard AID ...")
        val result = KernelInit.addAID(aidData)
        WeipassLogger.log(
            "AID ".plus(aidParameters.aid).plus(" Injection Result ::: ".plus(result))
        )
        this.finalResult += result
    }
}