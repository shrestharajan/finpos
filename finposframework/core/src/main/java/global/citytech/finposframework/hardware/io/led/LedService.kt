package global.citytech.finposframework.hardware.io.led

import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.DeviceService

/**
 * Created by Rishav Chudal on 6/4/20.
 */
interface LedService: DeviceService {
    fun doTurnLedWith(request: LedRequest): DeviceResponse
}