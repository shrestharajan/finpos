package global.citytech.finposframework.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import global.citytech.finposframework.hardware.io.printer.BarCodeReceipt;
import global.citytech.finposframework.hardware.io.printer.PrintMessage;
import global.citytech.finposframework.hardware.io.printer.Printable;
import global.citytech.finposframework.hardware.io.printer.Style;

/**
 * Created by Rishav Chudal on 9/8/20.
 */
public class ReceiptUtils {

    public static void addBase64Image(List<Printable> printableList, Object base64Image) {
        printableList.add(PrintMessage.Companion.getBase64ImageInstance(base64Image));
    }

    public static void addBarCodeImage(List<Printable> printableList, String data) {
        printableList.add(BarCodeReceipt.Companion.getBarCodeInstance(data));
    }

    public static void addQRCodeImage(List<Printable> printableList, String data, int width, Style style) {
        printableList.add(PrintMessage.Companion.getQRCodeInstance(data, width, style.getAlignment()));
    }

    public static void addMultiColumnString(
            List<Printable> printableList,
            List<String> stringList,
            int[] columnWidths,
            Style style
    ) {
        if (!stringList.isEmpty())
            printableList.add(PrintMessage
                    .Companion
                    .getMultiColumnInstance(
                            stringList,
                            columnWidths,
                            style
                    )
            );
    }

    public static List<String> createStringList(String... args) {
        return new ArrayList<>(Arrays.asList(args));
    }

    public static void addDoubleColumnString(
            List<Printable> printableList,
            String label,
            String value,
            Style style
    ) {
        if (!StringUtils.isEmpty(label) && !StringUtils.isEmpty(value))
            printableList.add(PrintMessage
                    .Companion
                    .getDoubleColumnInstance(
                            label,
                            value,
                            style
                    )
            );
    }

    public static void addNullableValueDoubleColumnString(
            List<Printable> printableList,
            String label,
            String value,
            Style style
    ) {
        if (label != null && value != null)
            printableList.add(PrintMessage
                    .Companion
                    .getDoubleColumnInstance(
                            label,
                            value,
                            style
                    )
            );
    }

    public static void addSingleColumnString(
            List<Printable> printableList,
            String value,
            Style style
    ) {
        if (!StringUtils.isEmpty(value))
            printableList.add(PrintMessage
                    .Companion
                    .getInstance(
                            value,
                            style
                    )
            );
    }

    public static String putSpaceAfterEveryTwoCharacters(String data) {
        if (StringUtils.isEmpty(data)) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        char[] chars = data.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if ((i > 0) && (i % 2 == 0)) {
                stringBuilder.append(" ");
            }
            stringBuilder.append(chars[i]);
        }
        return stringBuilder.toString();
    }

    public static String getValueOrEmpty(String value) {
        if (!StringUtils.isEmpty(value)) {
            return value;
        }
        return "";
    }
}
