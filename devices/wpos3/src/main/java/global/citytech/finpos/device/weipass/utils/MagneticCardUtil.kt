package global.citytech.finpos.device.weipass.utils

/**
 * Created by Rishav Chudal on 6/16/20.
 */
class MagneticCardUtil {
    companion object {
        fun getMagPan(track2Data: String): String? {
            var pan: String = WeipassConstant.EMPTY_STRING
            if (!track2Data.isNullOrEmptyOrBlank()) {
                pan = track2Data.split(retrieveFieldSeparator(track2Data)).toTypedArray()[0]
            }
            WeipassLogger.debug("Mag PAN ::: " + WeipassDeviceUtils.maskCardNumber(pan))
            return pan
        }

        fun getMagExpiryDate(track2Data: String): String? {
            var expiryDate: String = WeipassConstant.EMPTY_STRING
            if (!track2Data.isNullOrEmptyOrBlank()) {
                expiryDate =
                    track2Data.split(retrieveFieldSeparator(track2Data)).toTypedArray()[1]
                        .substring(0, 4)
            }
            WeipassLogger.debug("Mag Expiry Date ::: $expiryDate")
            return expiryDate
        }

        fun getCardHolderName(track1Data: String?): String? {
            var cardHolderName: String = WeipassConstant.EMPTY_STRING
            if (!track1Data.isNullOrEmptyOrBlank()) {
                val fieldSeparator = retrieveFieldSeparator(track1Data!!)
                cardHolderName = track1Data.substring(
                    track1Data.indexOf(fieldSeparator) + 1,
                    track1Data.lastIndexOf(fieldSeparator)
                )
            }
            WeipassLogger.debug("Card Holder Name ::: $cardHolderName")
            return cardHolderName
        }

        fun getTrackData(trackData: String): String? {
            return if (trackData.isNullOrEmptyOrBlank()) WeipassConstant.EMPTY_STRING else trackData
        }

        fun retrieveFieldSeparator(trackData: String): String {
            return if (trackData.contains("^")) {
                "^"
            } else if (trackData.contains("=")) {
                "="
            } else {
                "D"
            }
        }
    }
}