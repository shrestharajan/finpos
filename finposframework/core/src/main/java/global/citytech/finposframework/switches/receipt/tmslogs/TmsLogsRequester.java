package global.citytech.finposframework.switches.receipt.tmslogs;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Rishav Chudal on 11/5/20.
 */
public interface TmsLogsRequester <T extends TmsLogRequestParameter,
        E extends TmsLogsResponseParameter>
        extends Request<T, E> {
}
