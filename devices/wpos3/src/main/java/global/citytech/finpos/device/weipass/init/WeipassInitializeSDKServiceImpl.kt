package global.citytech.finpos.device.weipass.init

import android.content.Context
import com.wiseasy.emvprocess.LibInit
import com.wiseasy.emvprocess.SDKInstance
import global.citytech.finpos.device.weipass.utils.WeipassConstant.FAILURE_SDK_INIT
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SUCCESS_SDK_INIT
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result

/**
 * Created by Rishav Chudal on 4/9/20.
 */
class WeipassInitializeSDKServiceImpl constructor(val context: Context) :
    global.citytech.finposframework.hardware.init.InitializeSDKService {

    private lateinit var response: DeviceResponse

    override fun initializeSDK(): DeviceResponse {
        response = DeviceResponse(Result.FAILURE, FAILURE_SDK_INIT)
        initializeWeipassSdk()
        initializeWeipassEmvLibrary()
        response = SDKInstance.mBankCard.let {
            DeviceResponse(Result.SUCCESS, SUCCESS_SDK_INIT)
        }
        return response
    }

    private fun initializeWeipassSdk() {
        WeipassLogger.log("Initializing Weipass SDK")
        SDKInstance.initSDK(context.applicationContext)
    }

    private fun initializeWeipassEmvLibrary() {
        WeipassLogger.log("Initializing Weipass Emv Library")
        val result = LibInit.init(true)
        WeipassLogger.log("Emv Library init result ::: ".plus(result))
    }
}