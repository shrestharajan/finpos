package global.citytech.finposframework.usecases.transaction.data;


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/12/24 - 11:23 AM
 */

public class EmiInfo {
    private double emiAmount;
    private double amount;
    private double rate;
    private double timePeriod;

    public double getEmiAmount() {
        return emiAmount;
    }

    public void setEmiAmount(double emiAmount) {
        this.emiAmount = emiAmount;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(double timePeriod) {
        this.timePeriod = timePeriod;
    }
}
