package global.citytech.finpos.nibl.admin.presentation.splash

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.common.extensions.doesAppExist
import global.citytech.common.extensions.openActivity
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.nibl.admin.AppConstant.MERCHANT_PACKAGE_NAME
import global.citytech.finpos.nibl.admin.BR
import global.citytech.finpos.nibl.admin.R
import global.citytech.finpos.nibl.admin.databinding.ActivitySplashScreenBinding
import global.citytech.tms.operator.splash.MainActivity

class SplashScreenActivity : AppBaseActivity<ActivitySplashScreenBinding, SplashViewModel>() {

    private val SPLASH_TIME_OUT: Long = 3000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (doesAppExist(MERCHANT_PACKAGE_NAME)) {
            initObservers()
            getViewModel().initSdk()
        } else {
            val alertDialog = AlertDialog.Builder(this)
                .setMessage("Admin app cannot be run without merchant app. Please download and install finPos merchant app.")
                .setPositiveButton("OK") { dialog, _ ->
                    dialog.dismiss()
                    finish()
                }
            alertDialog.show()
        }
    }

    private fun initObservers() {
        getViewModel().sdkInitialized.observe(this, Observer {
            if (it) {
                Handler().postDelayed({
                    openActivity(MainActivity::class.java)
                    finish()
                }, SPLASH_TIME_OUT)
            } else {
                val alertDialog = AlertDialog.Builder(this)
                    .setMessage("SDK cannot be initialized")
                    .setNegativeButton("RETRY") { _, _ ->
                        getViewModel().initSdk()
                    }
                    .setPositiveButton("EXIT") { _, _ ->
                        finish()
                    }
                alertDialog.show()
            }
        })

        getViewModel().errorMessage.observe(this, Observer {
            val alertDialog = AlertDialog.Builder(this)
                .setMessage(it)
                .setNegativeButton("RETRY") { _, _ ->
                    getViewModel().initSdk()
                }
                .setPositiveButton("EXIT") { _, _ ->
                    finish()
                }
            alertDialog.show()
        })
    }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayout(): Int = R.layout.activity_splash_screen

    override fun getViewModel(): SplashViewModel =
        ViewModelProviders.of(this)[SplashViewModel::class.java]
}
