package global.citytech.finposframework.hardware

/**
 * Created by Rishav Chudal on 4/8/20.
 */
enum class DeviceServiceType {
    INIT,
    KEYS,
    EMV_PARAM,
    EMV_KEYS,
    REVOKED_KEYS,
    AID_PARAM,
    HARDWARE_STATUS,
    LED,
    SOUND,
    CARD,
    PINPAD,
    PRINTER,
    SYSTEM
}