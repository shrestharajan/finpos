package global.citytech.finpos.merchant.domain.usecase.core

import global.citytech.finpos.merchant.domain.model.pinchange.PinChangeResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.pinchange.PinChangeRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.pinchange.PinChangeRequestItem
import io.reactivex.Observable

class CorePinChangeUseCase(private val pinChangeRepository: PinChangeRepository) {
    fun pinChangeRequest(
        configurationItem: ConfigurationItem,
        pinChangeRequestItem: PinChangeRequestItem
    ): Observable<PinChangeResponseEntity> =
        pinChangeRepository.pinChangeRequest(configurationItem, pinChangeRequestItem)
}