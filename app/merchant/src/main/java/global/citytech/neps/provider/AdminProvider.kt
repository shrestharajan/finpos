package global.citytech.neps.provider

import android.content.ContentProvider
import android.content.ContentUris
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import global.citytech.neps.data.db.AppDatabase
import global.citytech.neps.data.db.dao.*
import global.citytech.neps.domain.model.*


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
class AdminProvider : ContentProvider() {

    lateinit var hostDao: HostDao
    lateinit var merchantDao: MerchantDao
    lateinit var logoDao: LogoDao
    lateinit var aidParamsDao: AidParamsDao
    lateinit var cardSchemeDao: CardSchemeDao
    lateinit var emvKeyDao: EmvKeyDao
    lateinit var emvParamsDao: EmvParamDao
    private val uriMatcher = UriMatcher(UriMatcher.NO_MATCH)
    private val HOST = 1
    private val MERCHANT = 2
    private val LOGO = 3
    private val AID_PARAM = 4
    private val CARD_SCHEME = 5
    private val EMV_KEY = 6
    private val EMV_PARAM = 7
    private val HOST_ID = 8
    private val MERCHANT_ID = 9
    private val LOGO_ID = 10
    private val AID_PARAM_ID = 11
    private val CARD_SCHEME_ID = 12
    private val EMV_KEY_ID = 13
    private val EMV_PARAM_ID = 14


    companion object {
        val AUTHORITY = "global.citytech.finpos.nepsmerchant.provider.AdminProvider"
        val HOST_TABLE_NAME = "host"
        val MERCHANT_TABLE_NAME = "merchant"
        val LOGO_TABLE_NAME = "logo"
        val AID_PARAM_TABLE_NAME = "aid_param"
        val CARD_SCHEME_TABLE_NAME = "card_scheme"
        val EMV_KEY_TABLE_NAME = "emv_key"
        val EMV_PARAM_TABLE_NAME = "emv_param"
    }

    init {
        uriMatcher.addURI(AUTHORITY, HOST_TABLE_NAME, HOST)
        uriMatcher.addURI(AUTHORITY, MERCHANT_TABLE_NAME, MERCHANT)
        uriMatcher.addURI(AUTHORITY, LOGO_TABLE_NAME, LOGO)
        uriMatcher.addURI(AUTHORITY, AID_PARAM_TABLE_NAME, AID_PARAM)
        uriMatcher.addURI(AUTHORITY, CARD_SCHEME_TABLE_NAME, CARD_SCHEME)
        uriMatcher.addURI(AUTHORITY, EMV_KEY_TABLE_NAME, EMV_KEY)
        uriMatcher.addURI(AUTHORITY, EMV_PARAM_TABLE_NAME, EMV_PARAM)
        uriMatcher.addURI(AUTHORITY, "$HOST_TABLE_NAME/#", HOST_ID)
        uriMatcher.addURI(AUTHORITY, "$MERCHANT_TABLE_NAME/#", MERCHANT_ID)
        uriMatcher.addURI(AUTHORITY, "$LOGO_TABLE_NAME/#", LOGO_ID)
        uriMatcher.addURI(AUTHORITY, "$AID_PARAM_TABLE_NAME/#", AID_PARAM_ID)
        uriMatcher.addURI(AUTHORITY, "$CARD_SCHEME_TABLE_NAME/#", CARD_SCHEME_ID)
        uriMatcher.addURI(AUTHORITY, "$EMV_KEY_TABLE_NAME/#", EMV_KEY_ID)
        uriMatcher.addURI(AUTHORITY, "$EMV_PARAM_TABLE_NAME/#", EMV_PARAM_ID)
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        return when (uriMatcher.match(uri)) {
            HOST -> {
                val context = context ?: return null
                val id: Long = hostDao.insert(Host.fromContentValues(values!!))
                context.contentResolver.notifyChange(uri, null)
                ContentUris.withAppendedId(uri, id)
            }
            MERCHANT -> {
                val context = context ?: return null
                val id: Long = merchantDao.insert(Merchant.fromContentValues(values!!))
                context.contentResolver.notifyChange(uri, null)
                ContentUris.withAppendedId(uri, id)
            }
            LOGO -> {
                val context = context ?: return null
                val id: Long = logoDao.insert(Logo.fromContentValues(values!!))
                context.contentResolver.notifyChange(uri, null)
                ContentUris.withAppendedId(uri, id)
            }
            EMV_PARAM -> {
                val context = context ?: return null
                val id: Long = emvParamsDao.insert(EmvParam.fromContentValues(values!!))
                context.contentResolver.notifyChange(uri, null)
                ContentUris.withAppendedId(uri, id)
            }
            AID_PARAM -> {
                val context = context ?: return null
                val id: Long = aidParamsDao.insert(AidParam.fromContentValues(values!!))
                context.contentResolver.notifyChange(uri, null)
                ContentUris.withAppendedId(uri, id)
            }
            CARD_SCHEME -> {
                val context = context ?: return null
                val id: Long = cardSchemeDao.insert(CardScheme.fromContentValues(values!!))
                context.contentResolver.notifyChange(uri, null)
                ContentUris.withAppendedId(uri, id)
            }
            EMV_KEY -> {
                val context = context ?: return null
                val id: Long = emvKeyDao.insert(EmvKey.fromContentValues(values!!))
                context.contentResolver.notifyChange(uri, null)
                ContentUris.withAppendedId(uri, id)
            }
            else -> throw IllegalArgumentException("Invalid URI : $uri")
        }
    }

    override fun bulkInsert(uri: Uri, values: Array<out ContentValues>): Int {
        when (uriMatcher.match(uri)) {
            HOST -> {
                val context = context ?: return 0
                for (value in values) {
                    hostDao.insert(Host.fromContentValues(value))
                }
                context.contentResolver.notifyChange(uri, null)
            }
            MERCHANT -> {
                val context = context ?: return 0
                for (value in values) {
                    merchantDao.insert(Merchant.fromContentValues(value))
                }
                context.contentResolver.notifyChange(uri, null)
            }
            LOGO -> {
                val context = context ?: return 0
                for (value in values) {
                    logoDao.insert(Logo.fromContentValues(value))
                }
                context.contentResolver.notifyChange(uri, null)
            }
            AID_PARAM -> {
                val context = context ?: return 0
                for (value in values) {
                    aidParamsDao.insert(AidParam.fromContentValues(value))
                }
                context.contentResolver.notifyChange(uri, null)
            }
            CARD_SCHEME -> {
                val context = context ?: return 0
                for (value in values) {
                    cardSchemeDao.insert(CardScheme.fromContentValues(value))
                }
                context.contentResolver.notifyChange(uri, null)
            }
            EMV_KEY -> {
                val context = context ?: return 0
                for (value in values) {
                    emvKeyDao.insert(EmvKey.fromContentValues(value))
                }
                context.contentResolver.notifyChange(uri, null)
            }
            EMV_PARAM -> {
                val context = context ?: return 0
                for (value in values) {
                    emvParamsDao.insert(EmvParam.fromContentValues(value))
                }
                context.contentResolver.notifyChange(uri, null)
            }
            else -> throw IllegalArgumentException("Invalid URI : $uri")
        }
        return values.size
    }

    override fun query(
        uri: Uri,
        projection: Array<out String>?,
        selection: String?,
        selectionArgs: Array<out String>?,
        sortOrder: String?
    ): Cursor? {
        val code = uriMatcher.match(uri)
        return if (code == HOST || code == HOST_ID || code == MERCHANT || code == MERCHANT_ID
            || code == LOGO || code == LOGO_ID
        ) {
            val context = context ?: return null
            val cursor: Cursor? =
                when (code) {
                    HOST -> hostDao.get()
                    HOST_ID -> hostDao.getById(ContentUris.parseId(uri))!!
                    MERCHANT -> merchantDao.get()
                    MERCHANT_ID -> merchantDao.getById(ContentUris.parseId(uri))!!
                    LOGO -> logoDao.get()
                    LOGO_ID -> logoDao.getById(ContentUris.parseId(uri))!!
                    AID_PARAM -> aidParamsDao.get()
                    AID_PARAM_ID -> aidParamsDao.getById(ContentUris.parseId(uri))!!
                    CARD_SCHEME -> cardSchemeDao.get()
                    CARD_SCHEME_ID -> cardSchemeDao.getById(ContentUris.parseId(uri))!!
                    EMV_KEY -> emvKeyDao.get()
                    EMV_KEY_ID -> emvKeyDao.getById(ContentUris.parseId(uri))!!
                    EMV_PARAM -> emvParamsDao.get()
                    EMV_PARAM_ID -> emvParamsDao.getById(ContentUris.parseId(uri))!!
                    else -> null
                }
            cursor!!.setNotificationUri(context.contentResolver, uri)
            cursor
        } else {
            throw java.lang.IllegalArgumentException("Invalid URI ::$uri")
        }
    }

    override fun onCreate(): Boolean {
        hostDao = AppDatabase.getInstance(context!!).getHostDao()
        merchantDao = AppDatabase.getInstance(context!!).getMerchantDao()
        logoDao = AppDatabase.getInstance(context!!).getLogoDao()
        aidParamsDao = AppDatabase.getInstance(context!!).getAidParamDao()
        cardSchemeDao = AppDatabase.getInstance(context!!).getCardSchemeDao()
        emvKeyDao = AppDatabase.getInstance(context!!).getEmvKeyDao()
        emvParamsDao = AppDatabase.getInstance(context!!).getEmvParamDao()
        return true
    }

    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<out String>?
    ): Int {
        return when (uriMatcher.match(uri)) {
            HOST_ID -> {
                val context = context ?: return 0
                val host = Host.fromContentValues(values!!)
                host.id = ContentUris.parseId(uri).toString()
                val count = hostDao.update(host)
                context.contentResolver.notifyChange(uri, null)
                count
            }
            MERCHANT_ID -> {
                val context = context ?: return 0
                val merchant = Merchant.fromContentValues(values!!)
                merchant.id = ContentUris.parseId(uri).toString()
                val count = merchantDao.update(merchant)
                context.contentResolver.notifyChange(uri, null)
                count
            }
            LOGO_ID -> {
                val context = context ?: return 0
                val logo = Logo.fromContentValues(values!!)
                logo.id = ContentUris.parseId(uri).toString()
                val count = logoDao.update(logo)
                context.contentResolver.notifyChange(uri, null)
                count
            }
            AID_PARAM_ID -> {
                val context = context ?: return 0
                val aidParam = AidParam.fromContentValues(values!!)
                aidParam.id = ContentUris.parseId(uri).toString()
                val count = aidParamsDao.update(aidParam)
                context.contentResolver.notifyChange(uri, null)
                count
            }
            CARD_SCHEME_ID -> {
                val context = context ?: return 0
                val cardScheme = CardScheme.fromContentValues(values!!)
                cardScheme.id = ContentUris.parseId(uri).toString()
                val count = cardSchemeDao.update(cardScheme)
                context.contentResolver.notifyChange(uri, null)
                count
            }
            EMV_KEY_ID -> {
                val context = context ?: return 0
                val emvKey = EmvKey.fromContentValues(values!!)
                emvKey.id = ContentUris.parseId(uri).toString()
                val count = emvKeyDao.update(emvKey)
                context.contentResolver.notifyChange(uri, null)
                count
            }
            EMV_PARAM_ID -> {
                val context = context ?: return 0
                val emvParam = EmvParam.fromContentValues(values!!)
                emvParam.id = ContentUris.parseId(uri).toString()
                val count = emvParamsDao.update(emvParam)
                context.contentResolver.notifyChange(uri, null)
                count
            }
            else -> throw java.lang.IllegalArgumentException("Invalid URI : $uri")
        }
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        return when (uriMatcher.match(uri)) {
            HOST -> throw java.lang.IllegalArgumentException("Invalid URI, cannot update without ID$uri")
            HOST_ID -> {
                val context = context ?: return 0
                val count: Int = hostDao.deleteById(ContentUris.parseId(uri))
                context.contentResolver.notifyChange(uri, null)
                count
            }
            MERCHANT -> throw java.lang.IllegalArgumentException("Invalid URI, cannot update without ID$uri")
            MERCHANT_ID -> {
                val context = context ?: return 0
                val count: Int = merchantDao.deleteById(ContentUris.parseId(uri))
                context.contentResolver.notifyChange(uri, null)
                count
            }
            LOGO -> throw java.lang.IllegalArgumentException("Invalid URI, cannot update without ID$uri")
            LOGO_ID -> {
                val context = context ?: return 0
                val count: Int = logoDao.deleteById(ContentUris.parseId(uri))
                context.contentResolver.notifyChange(uri, null)
                count
            }
            AID_PARAM -> throw java.lang.IllegalArgumentException("Invalid URI, cannot update without ID$uri")
            AID_PARAM_ID -> {
                val context = context ?: return 0
                val count: Int = aidParamsDao.deleteById(ContentUris.parseId(uri))
                context.contentResolver.notifyChange(uri, null)
                count
            }
            CARD_SCHEME -> throw java.lang.IllegalArgumentException("Invalid URI, cannot update without ID$uri")
            CARD_SCHEME_ID -> {
                val context = context ?: return 0
                val count: Int = cardSchemeDao.deleteById(ContentUris.parseId(uri))
                context.contentResolver.notifyChange(uri, null)
                count
            }
            EMV_PARAM -> throw java.lang.IllegalArgumentException("Invalid URI, cannot update without ID$uri")
            EMV_PARAM_ID -> {
                val context = context ?: return 0
                val count: Int = emvParamsDao.deleteById(ContentUris.parseId(uri))
                context.contentResolver.notifyChange(uri, null)
                count
            }
            EMV_KEY -> throw java.lang.IllegalArgumentException("Invalid URI, cannot update without ID $uri")
            EMV_KEY_ID -> {
                val context = context ?: return 0
                val count: Int = emvKeyDao.deleteById(ContentUris.parseId(uri))
                context.contentResolver.notifyChange(uri, null)
                count
            }
            else -> throw java.lang.IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun getType(uri: Uri): String? {
        return when (uriMatcher.match(uri)) {
            HOST -> "vnd.android.cursor.dir/" + AUTHORITY + "." + HOST_TABLE_NAME
            MERCHANT -> "vnd.android.cursor.dir/" + AUTHORITY + "." + MERCHANT_TABLE_NAME
            LOGO -> "vnd.android.cursor.dir/" + AUTHORITY + "." + LOGO_TABLE_NAME
            AID_PARAM -> "vnd.android.cursor.dir/" + AUTHORITY + "." + AID_PARAM_TABLE_NAME
            CARD_SCHEME -> "vnd.android.cursor.dir/" + AUTHORITY + "." + CARD_SCHEME_TABLE_NAME
            EMV_KEY -> "vnd.android.cursor.dir/" + AUTHORITY + "." + EMV_KEY_TABLE_NAME
            EMV_PARAM -> "vnd.android.cursor.dir/" + AUTHORITY + "." + EMV_PARAM_TABLE_NAME
            HOST_ID -> "vnd.android.cursor.item/" + AUTHORITY + "." + HOST_TABLE_NAME
            MERCHANT_ID -> "vnd.android.cursor.item/" + AUTHORITY + "." + MERCHANT_TABLE_NAME
            LOGO_ID -> "vnd.android.cursor.item/" + AUTHORITY + "." + LOGO_TABLE_NAME
            AID_PARAM_ID -> "vnd.android.cursor.item/" + AUTHORITY + "." + AID_PARAM_TABLE_NAME
            CARD_SCHEME_ID -> "vnd.android.cursor.item/" + AUTHORITY + "." + CARD_SCHEME_TABLE_NAME
            EMV_KEY_ID -> "vnd.android.cursor.item/" + AUTHORITY + "." + EMV_KEY_TABLE_NAME
            EMV_PARAM_ID -> "vnd.android.cursor.item/" + AUTHORITY + "." + EMV_PARAM_TABLE_NAME
            else -> throw java.lang.IllegalArgumentException("Invalid Uri ::$uri")
        }
    }
}