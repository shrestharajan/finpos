package global.citytech.finpos.processor.neps.reconciliation;

import java.util.Map;

import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationTotals;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.StringUtils;

public class NepsReconciliationTotals implements ReconciliationTotals {

    private String transactionCurrencyName;
    private Map<CardSchemeType, NepsCardSchemeTotals> totalsMap;
    private String totalsBlockString;
    private long salesCount;
    private long salesAmount;
    private long salesRefundCount;
    private long salesRefundAmount;
    private long authCount;
    private long authAmount;
    private long authRefundCount;
    private long authRefundAmount;
    private long voidCount;
    private long voidAmount;

    public NepsReconciliationTotals(String transactionCurrencyName,Map<CardSchemeType, NepsCardSchemeTotals> totalsMap) {
        this.totalsMap = totalsMap;
        this.transactionCurrencyName = transactionCurrencyName;
    }

    public String getTransactionCurrencyName() {
        return transactionCurrencyName;
    }

    public Map<CardSchemeType, NepsCardSchemeTotals> getTotalsMap() {
        return totalsMap;
    }

    public long getSalesCount() {
        return salesCount;
    }

    public long getSalesAmount() {
        return salesAmount;
    }

    public long getSalesRefundCount() {
        return salesRefundCount;
    }

    public long getSalesRefundAmount() {
        return salesRefundAmount;
    }

    public long getAuthCount() {
        return authCount;
    }

    public long getAuthAmount() {
        return authAmount;
    }

    public long getAuthRefundCount() {
        return authRefundCount;
    }

    public long getAuthRefundAmount() {
        return authRefundAmount;
    }

    public long getVoidCount() {
        return voidCount;
    }

    public long getVoidAmount() {
        return voidAmount;
    }

    public String getTotalsBlockString() {
        return totalsBlockString;
    }

    public long getTotalCountReceipt() {
        return salesCount;
    }

    public long getTotalAmountReceipt() {
        return salesAmount;
    }

    @Override
    public String toTotalsBlock() {

        if(StringUtils.isEmpty(totalsBlockString)) {
            for (Map.Entry<CardSchemeType, NepsCardSchemeTotals> cardSchemeTotals : this.totalsMap.entrySet()) {
                salesCount = salesCount + cardSchemeTotals.getValue().getSalesCount();
                salesAmount = salesAmount + cardSchemeTotals.getValue().getSalesAmount();
                salesRefundCount = salesRefundCount + cardSchemeTotals.getValue().getSalesRefundCount();
                salesRefundAmount = salesRefundAmount + cardSchemeTotals.getValue().getSalesRefundAmount();
                authCount = authCount + cardSchemeTotals.getValue().getAuthCount();
                authAmount = authAmount + cardSchemeTotals.getValue().getAuthAmount();
                authRefundCount = authRefundCount + cardSchemeTotals.getValue().getAuthRefundCount();
                authRefundAmount = authRefundAmount + cardSchemeTotals.getValue().getAuthRefundAmount();
                voidCount = voidCount + cardSchemeTotals.getValue().getVoidCount();
                voidAmount = voidAmount + cardSchemeTotals.getValue().getVoidAmount();
            }
            StringBuilder stringBuilder = new StringBuilder();
            String signedValue = "";
            signedValue = prepareTotalAmount() >= 0 ? "C" : "D";
            stringBuilder.append(signedValue);
            stringBuilder.append(
                    StringUtils.ofRequiredLength(
                            String.valueOf((int) Math.abs(prepareTotalAmount())),
                            12)
            );
            totalsBlockString = stringBuilder.toString();
        }
        return totalsBlockString;
    }

    private double prepareTotalAmount() {
        return salesAmount + salesRefundAmount + authAmount + authRefundAmount;
    }

    @Override
    public String toString() {
        return "NepsReconciliationTotals{" +
                "transactionCurrencyName='" + transactionCurrencyName + '\'' +
                ", totalsMap=" + totalsMap +
                ", totalsBlockString='" + totalsBlockString + '\'' +
                ", salesCount=" + salesCount +
                ", salesAmount=" + salesAmount +
                ", salesRefundCount=" + salesRefundCount +
                ", salesRefundAmount=" + salesRefundAmount +
                ", authCount=" + authCount +
                ", authAmount=" + authAmount +
                ", authRefundCount=" + authRefundCount +
                ", authRefundAmount=" + authRefundAmount +
                ", voidCount=" + voidCount +
                ", voidAmount=" + voidAmount +
                '}';
    }

}
