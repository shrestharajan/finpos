package global.citytech.finpos.device.weipass.card

import android.content.Context
import com.wiseasy.emvprocess.bean.TerminalSetting
import com.wiseasy.emvprocess.bean.TransInfoBean
import global.citytech.finpos.device.weipass.emvparam.WeipassEmvParametersServiceImpl
import global.citytech.finpos.device.weipass.utils.WeipassDeviceUtils
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest

/**
 * Created by Rishav Chudal on 6/8/20.
 */
class WeipassTransInfoBeanReadCard constructor(
    val context: Context,
    val transactionType: Int,
    val readCardRequest: ReadCardRequest,
    val cardType: CardType
){
    fun buildSdkCommonTransInfo(): TransInfoBean {
        val transInfoBean = TransInfoBean()
        transInfoBean.transAmount = WeipassDeviceUtils.prepareTransAmount(
            readCardRequest.amount,
            readCardRequest.cashBackAmount,
            readCardRequest.transactionType
        )
        transInfoBean.transType = transactionType
        transInfoBean.transType9C = readCardRequest.transactionType9C
        transInfoBean.transNumber = readCardRequest.stan!!.toInt()
        transInfoBean.terminalSetting = prepareTerminalSetting(context, readCardRequest, cardType)
        WeipassLogger.log("========== TransInfoBean ==========")
        WeipassLogger.log("TransType ::: ".plus(transInfoBean.transType))
        WeipassLogger.log("TransType 9C ::: ".plus(transInfoBean.transType9C))
        WeipassLogger.log("Trans Number ::: ".plus(transInfoBean.transNumber))
        return transInfoBean
    }

    private fun prepareTerminalSetting(
        context: Context,
        readCardRequest: ReadCardRequest,
        cardType: CardType
    ) : TerminalSetting {
        val weipassEmvParamServiceImpl = WeipassEmvParametersServiceImpl(context)
        return weipassEmvParamServiceImpl.prepareTerminalSettingWith(readCardRequest, cardType)
    }
}