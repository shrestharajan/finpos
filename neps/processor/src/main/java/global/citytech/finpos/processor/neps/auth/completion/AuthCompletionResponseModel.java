package global.citytech.finpos.processor.neps.auth.completion;

import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finposframework.hardware.common.Result;

/**
 * Created by Saurav Ghimire on 2/8/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class AuthCompletionResponseModel extends TransactionResponse {
    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private boolean approved;
        private String message;
        private String stan;
        private Result result;
        private String approvalCode;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static Builder createDefaultBuilder() {
            return new Builder();
        }

        public Builder debugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public Builder debugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            System.gc();
            return this;
        }

        public Builder approved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public Builder shouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }
        public Builder stan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder result(Result result) {
            this.result = result;
            return this;
        }

        public Builder approvalCode(String approvalCode) {
            this.approvalCode = approvalCode;
            return this;
        }

        public AuthCompletionResponseModel build() {
            AuthCompletionResponseModel authCompletionResponseModel = new AuthCompletionResponseModel();
            authCompletionResponseModel.debugRequestMessage = this.debugRequestMessage;
            authCompletionResponseModel.debugResponseMessage = this.debugResponseMessage;
            authCompletionResponseModel.approved = this.approved;
            authCompletionResponseModel.message = this.message;
            authCompletionResponseModel.stan = this.stan;
            authCompletionResponseModel.result = this.result;
            authCompletionResponseModel.approvalCode = this.approvalCode;
            authCompletionResponseModel.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            return authCompletionResponseModel;
        }
    }
}
