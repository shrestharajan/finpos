package global.citytech.finpos.device.weipass.emv.aid

import com.wiseasy.emvprocess.KernelInit
import com.wiseasy.emvprocess.bean.AIDData
import com.wiseasy.emvprocess.bean.BaseAIDData
import com.wiseasy.emvprocess.bean.CLqPBOCAIDData
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finpos.device.weipass.utils.defaultOrEmptyValue
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import sdk4.wangpos.libemvbinder.utils.MoneyUtil

/**
 * Created by Rishav Chudal on 5/4/20.
 */
class WeipassUnionPayAidManager constructor(val baseAIDData: BaseAIDData,
                                            val aidParameters: global.citytech.finposframework.hardware.emv.aid.AidParameters,
                                            val emvParametersRequest: EmvParametersRequest
) {
    fun injectUnionPayAidInCore(): Int {
        var successCount = 0
        val aidData = prepareSdkAidData()
        WeipassLogger.log("Injecting the Union Pay AID ...")
        val result = KernelInit.addAID(aidData)
        WeipassLogger.log("AID ".plus(aidParameters.aid).plus(" Injection Result ::: ".plus(result)))
        if (result == 0) {
            successCount ++
        }
        return successCount
    }

    private fun prepareSdkAidData(): AIDData {
        val sdkAidData = AIDData()
        sdkAidData.baseAIDData = baseAIDData
        sdkAidData.clAIDData = prepareCLqPBOCAIDData()
        return sdkAidData
    }

    private fun prepareCLqPBOCAIDData(): CLqPBOCAIDData {
        WeipassLogger.log("===== CLqPBOCAIDData ::: Contactless =====")
        val cLqPBOCAIDData = CLqPBOCAIDData()

        var clFloorLimit = aidParameters.contactlessFloorLimit
        clFloorLimit = MoneyUtil.toCent(clFloorLimit)
        cLqPBOCAIDData.clFloorLimit = clFloorLimit
        WeipassLogger.log("CL Floor Limit ::: ".plus(clFloorLimit))

        var clTransLimit = aidParameters.contactlessTransactionLimit
        clTransLimit = MoneyUtil.toCent(clTransLimit)
        cLqPBOCAIDData.clTransLimit = clTransLimit
        WeipassLogger.log("CL Trans Limit ::: ".plus(clTransLimit))

        var cvmLimit = aidParameters.cvmLimit
        cvmLimit = MoneyUtil.toCent(cvmLimit)
        cLqPBOCAIDData.clcvmLimit = cvmLimit
        WeipassLogger.log("CL Cvm Limit ::: ".plus(cvmLimit))

        val enableAmountZeroOption = "1"
        cLqPBOCAIDData.enableAmountZeroOption = enableAmountZeroOption
        WeipassLogger.log("Enable Amount Zero Option ::: ".plus(enableAmountZeroOption))

        val amountZeroOption = "0"
        cLqPBOCAIDData.amountZeroOption = amountZeroOption
        WeipassLogger.log("Amount Zero Option ::: ".plus(amountZeroOption))

        val enableFloorLimit = "1"
        cLqPBOCAIDData.enableFloorLimit = enableFloorLimit
        WeipassLogger.log("Enable Floor Limit ::: ".plus(enableFloorLimit))

        val enableTransLimit = "1"
        cLqPBOCAIDData.enableTransLimit = enableTransLimit
        WeipassLogger.log("Enable Trans Limit ::: ".plus(enableTransLimit))

        val enableCvmLimit = "1"
        cLqPBOCAIDData.enableCVMLimit = enableCvmLimit
        WeipassLogger.log("Enable Cvm Limit ::: ".plus(enableCvmLimit))

        val ttq = aidParameters.ttq.defaultOrEmptyValue()
        cLqPBOCAIDData.ttQ_9F66 = ttq
        WeipassLogger.log("TTQ ::: ".plus(ttq))

        return cLqPBOCAIDData
    }
}