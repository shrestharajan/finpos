package global.citytech.payment.sdk.core;

/**
 * Created by BikashShrestha on 5/31/19.
 */
public final class PaymentConstants {

    public static final String EXTRA_DATA = "extra_checkout_data";
    public static final String EXTRA_CONFIGURATION_DATA = "extra_configuration_data";
    public static final String EXTRA_APP_ID_DATA = "extra_app_id_data";
    public static final String EXTRA_VENDOR_NAME_DATA = "extra_vendor_name_data";

    private PaymentConstants() {
    }
}
