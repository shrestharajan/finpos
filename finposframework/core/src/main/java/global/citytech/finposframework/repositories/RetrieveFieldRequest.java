package global.citytech.finposframework.repositories;

public class RetrieveFieldRequest {

    private String aid;
    private FieldAttributes fieldAttributes;

    public RetrieveFieldRequest( FieldAttributes fieldAttributes) {
        this.fieldAttributes = fieldAttributes;
    }

    public FieldAttributes getFieldAttributes() {
        return fieldAttributes;
    }
}
