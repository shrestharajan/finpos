package global.citytech.finpos.merchant.presentation.purchase

import android.content.Context
import android.graphics.Color
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.card.MaterialCardView
import global.citytech.common.Constants.CARD
import global.citytech.common.Constants.MOBILEPAY
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.utils.handleDebounce
import global.citytech.finposframework.log.Logger
import java.util.*

class PaymentMethodAdapter(
    val context: Context,
    var paymentItems: MutableList<PaymentItems>,
    private val paymentItemClickListener: PaymentItemClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    ItemMoveCallbackListener.Listener {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PaymentMethodViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.layout_payment_method_selection, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val paymentItem = paymentItems[position]
        holder.itemView.findViewById<TextView>(R.id.tv_name).text =
            if (paymentItem.paymentName == MOBILEPAY) {
                SpannableStringBuilder().apply {
                    append(MOBILEPAY)
                    val tapAndPayText = " (Tap & pay)"
                    val greyColorSpan = ForegroundColorSpan(Color.GRAY)
                    append(tapAndPayText)
                    setSpan(
                        greyColorSpan,
                        length - tapAndPayText.length,
                        length,
                        SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                }
            } else {
                paymentItem.paymentName
            }

        loadImage(
            paymentItem.paymentLogo,
            paymentItem.placeHolder,
            holder.itemView.findViewById<ImageView>(R.id.iv_logo),
            paymentItem.paymentName
        )

        holder.itemView.setOnLongClickListener {
            this.paymentItemClickListener.onStartDrag(holder)
            return@setOnLongClickListener true
        }

        holder.itemView.setOnClickListener {
            holder.itemView.handleDebounce()
            paymentItemClickListener.onItemClicked(paymentItem)
        }
    }

    private fun loadImage(url: String, drawableImage: Int, view: ImageView, paymentName: String) {
        if (url.isEmpty()) {
            if(paymentName == CARD)
                view.setImageResource(R.drawable.card)
            else if (paymentName == MOBILEPAY)
                view.setImageResource(R.drawable.ic_contactless)
            else
                view.setImageResource(drawableImage)
        } else {
            Glide.with(context)
                .load(url)
                .placeholder(drawableImage)
                .error(drawableImage)
                .into(view)
        }
    }

    override fun getItemCount(): Int {
        return paymentItems.size
    }

    fun update(paymentItems: MutableList<PaymentItems>) {
        this.paymentItems = paymentItems
        notifyDataSetChanged()
    }

    class PaymentMethodViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onRowMoved(fromPosition: Int, toPosition: Int) {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(paymentItems, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(paymentItems, i, i - 1)
            }
        }
        notifyItemMoved(fromPosition, toPosition)
    }

    override fun onRowSelected(itemViewHolder: PaymentMethodViewHolder) {
        val paymentItemCardView =
            itemViewHolder.itemView.findViewById<MaterialCardView>(R.id.mcv_payment_item_view)
        paymentItemCardView.strokeWidth = 2
    }

    override fun onRowClear(itemViewHolder: PaymentMethodViewHolder) {
        val paymentItemCardView =
            itemViewHolder.itemView.findViewById<MaterialCardView>(R.id.mcv_payment_item_view)
        paymentItemCardView.strokeWidth = 0
        paymentItemClickListener.onReleaseItem(paymentItems)
    }

    interface PaymentItemClickListener {
        fun onItemClicked(paymentItems: PaymentItems)
        fun onStartDrag(viewHolder: RecyclerView.ViewHolder)
        fun onReleaseItem(paymentItems: MutableList<PaymentItems>)
    }
}