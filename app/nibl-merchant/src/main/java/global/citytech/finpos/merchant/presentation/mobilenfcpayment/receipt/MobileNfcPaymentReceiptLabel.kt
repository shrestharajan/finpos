package global.citytech.finpos.merchant.presentation.mobilenfcpayment.receipt

object MobileNfcPaymentReceiptLabel {
    const val DATE = "DATE: "
    const val TIME = "TIME: "
    const val MERCHANT_ID = "MID: "
    const val TERMINAL_ID = "TID: "
    const val REFERENCE_NUMBER = "REFERENCE #"
    const val PAYMENT_INITIATOR = "Payment Initiator"
    const val INITIATOR_ID = "Initiator ID"
    const val APPROVAL_CODE = "APPROVAL CODE"
    const val PROCESSING_NUMBER = "PROCESSING NO: "
    const val THANK_YOU_MESSAGE = "THANK YOU FOR USING NFC\nPLEASE RETAIN YOUR RECEIPT"
}