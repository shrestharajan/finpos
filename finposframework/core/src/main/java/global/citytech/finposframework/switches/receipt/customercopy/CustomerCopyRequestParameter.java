package global.citytech.finposframework.switches.receipt.customercopy;

import global.citytech.finposframework.switches.RequestParameter;

/**
 * Created by Unique Shakya on 9/2/2020.
 */
public interface CustomerCopyRequestParameter extends RequestParameter {
}
