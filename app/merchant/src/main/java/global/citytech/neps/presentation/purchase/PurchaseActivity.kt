package global.citytech.neps.presentation.purchase

import android.app.Activity
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import global.citytech.common.Constants
import global.citytech.common.data.PosCallback
import global.citytech.common.data.PosMessage
import global.citytech.common.extensions.openActivity
import global.citytech.easydroid.core.extension.showSuccessMessage
import global.citytech.easydroid.core.extension.showToast
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.easydroid.core.utils.MessageConfig
import global.citytech.neps.BR
import global.citytech.neps.R
import global.citytech.neps.databinding.ActivityPurchaseBinding
import global.citytech.neps.presentation.alertdialogs.PosEntrySelectionDialog
import global.citytech.neps.presentation.alertdialogs.TransactionConfirmation
import global.citytech.neps.presentation.alertdialogs.TransactionConfirmationDialog
import global.citytech.neps.presentation.amount.AmountActivity
import global.citytech.neps.presentation.dashboard.DashboardActivity
import global.citytech.neps.presentation.idle.IdleActivity
import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.neps.presentation.utils.PosCallbackUtils
import global.citytech.posswitchintregator.card.data.PurchaseType
import java.lang.ref.WeakReference

class PurchaseActivity : AppBaseActivity<ActivityPurchaseBinding,PurchaseViewModel>(), TransactionConfirmationDialog.Listener {

    private val REQUEST_AMOUNT: Int = 1000

    private lateinit var purchaseType: PurchaseType

    private var amount: Double = 0.0

    private var progressDialog: ProgressDialog? = null

    private var posEntrySelectionDialog:PosEntrySelectionDialog? = null

    private lateinit var activityWeakReference: WeakReference<Activity>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_purchase)
        activityWeakReference = WeakReference(this)
        initObservers()
        startActivityForResult(Intent(this,AmountActivity::class.java),REQUEST_AMOUNT)
    }

    override fun onResume() {
        super.onResume()
        registerBroadcastReceivers()
    }

    override fun onPause() {
        super.onPause()
        unregisterBroadcastReceivers()
    }

    private fun initObservers() {
        getViewModel().configurationItem.observe(this, Observer {
            purchase(it)
        })

        getViewModel().isManualButtonClickedInDialog.observe(this, Observer {
            if (it) {
                Toast.makeText(this,"Manual Clicked", Toast.LENGTH_SHORT).show()
                returnToIdlePage()
            }
        })

        getViewModel().isCancelButtonClickedInDialog.observe(this, Observer {
            if (it) {
                showToast("Cancel Clicked", Toast.LENGTH_SHORT)
                returnToIdlePage()
            }
        })

        getViewModel().isLoading.observe(this, Observer {
            if(!it) {
                hideProgressScreen()
            }
        })

        getViewModel().message.observe(this, Observer {
            showSuccessMessage(it)
        })

        getViewModel().transactionConfirmationDialog.observe(this, Observer {
            this.showTransactionConfirmationDialog(it)
        })
    }

    private fun showTransactionConfirmationDialog(it: TransactionConfirmation) {
        val transactionConfirmationDialog = TransactionConfirmationDialog(this,this,it)
        transactionConfirmationDialog.show()
    }

    private fun purchase(it: ConfigurationItem) {
        getViewModel().purchase(it,amount, purchaseType)
        showPosEntrySelectionDialog()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_AMOUNT && resultCode == Activity.RESULT_OK){
            data?.let {
                val amountInString = data.getStringExtra(getString(R.string.intent_confirmed_amount))
                amount = amountInString.toDouble()
                purchaseType = PurchaseType.PURCHASE
                getViewModel().getConfigurationItem()
            }

        } else {
            returnToIdlePage()
        }
    }

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null && intent.action.equals(Constants.INTENT_ACTION_NAME)) {
                prepareMessageForProgress(intent)
            }
        }
    }

    private fun prepareMessageForProgress(intent: Intent) {
        val posCallback
                = (intent.getSerializableExtra(Constants.KEY_BROADCAST_MESSAGE)) as PosCallback
        val stringBuilder = StringBuilder()
        stringBuilder.append(PosCallbackUtils
            .getMessage(
                this.applicationContext,
                posCallback
            ))
        if (posCallback.posMessage == PosMessage.POS_MESSAGE_CONNECTION_FAILED) {
            val extraMessage =
                intent.getStringExtra(Constants.KEY_BROADCAST_EXTRA_MESSAGE)
            if (!extraMessage.isNullOrEmpty()) {
                stringBuilder.append(" ")
                stringBuilder.append(extraMessage)
            }
        }
        hidePosEntrySelectionDialog()
        showProgressScreen(stringBuilder.toString())
    }

    private fun showProgressScreen(message: String) {
        if (progressDialog == null) {
            progressDialog = ProgressDialog(activityWeakReference.get())
        }
        progressDialog!!.setCancelable(true)
        progressDialog!!.setMessage(message)
        progressDialog!!.show()
    }

    private fun hideProgressScreen() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    private fun showPosEntrySelectionDialog() {
        posEntrySelectionDialog = PosEntrySelectionDialog(
            getString(R.string.title_swipe_insert_or_wave_card),
                true,
                true)
        posEntrySelectionDialog!!.show(activityWeakReference)
    }

    private fun hidePosEntrySelectionDialog() {
        if (posEntrySelectionDialog!= null && posEntrySelectionDialog!!.isStillVisible()) {
            posEntrySelectionDialog!!.hideDialog()
        }
    }

    private fun registerBroadcastReceivers() {
        LocalBroadcastManager
            .getInstance(this.applicationContext)
            .registerReceiver(
                broadcastReceiver,
                IntentFilter(Constants.INTENT_ACTION_NAME)
            )
    }

    private fun unregisterBroadcastReceivers() {
        LocalBroadcastManager
            .getInstance(this.applicationContext)
            .unregisterReceiver(broadcastReceiver)
    }

    private fun showSuccessMessage(it: String?) {
        if (!it.isNullOrEmpty()) {
            val messageConfig = MessageConfig.Builder()
                .message(it)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                        dialogInterface, _ ->
                        dialogInterface.dismiss()
                        returnToIdlePage()
                }
            showSuccessMessage(messageConfig)
        }
    }

    private fun returnToIdlePage() {
        openActivity(IdleActivity::class.java)
        finish()
    }

    override fun getBindingVariable(): Int = BR.viewModel
    override fun getLayout(): Int = R.layout.activity_purchase
    override fun getViewModel(): PurchaseViewModel
            = ViewModelProviders.of(this)[PurchaseViewModel::class.java]

    override fun onPositiveButtonClick() {
        returnToIdlePage()
    }

    override fun onNegativeButtonClick() {
        returnToIdlePage()
    }
}
