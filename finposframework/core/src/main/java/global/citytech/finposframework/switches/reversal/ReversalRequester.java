package global.citytech.finposframework.switches.reversal;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Rishav Chudal on 9/29/20.
 */
public interface ReversalRequester<
        T extends ReversalRequestParameter, E extends ReversalResponseParameter>
        extends Request<T, E> {
}
