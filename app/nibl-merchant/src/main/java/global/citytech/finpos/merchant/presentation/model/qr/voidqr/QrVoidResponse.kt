package global.citytech.finpos.merchant.presentation.model.qr.voidqr

data class QrVoidResponse(
    val requestNumber: String,
    val message: String,
    val localDateTime: String
)