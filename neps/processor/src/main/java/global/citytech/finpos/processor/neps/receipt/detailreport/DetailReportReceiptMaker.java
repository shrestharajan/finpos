package global.citytech.finpos.processor.neps.receipt.detailreport;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Saurav Ghimire on 3/29/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public class DetailReportReceiptMaker {

    private TerminalRepository terminalRepository;

    public DetailReportReceiptMaker(TerminalRepository terminalRepository) {
        this.terminalRepository = terminalRepository;
    }

    public DetailReportReceipt prepare(String batchNumber, List<TransactionLog> transactionLogs) {
        return DetailReportReceipt.Builder.newInstance()
                .withRetailer(this.prepareRetailerInfo(this.terminalRepository.findTerminalInfo()))
                .withReconciliationBatchNumber(batchNumber)
                .withPerformance(this.preparePerformanceInfo())
                .withHost("000048")
                .withDetailReports(this.prepareDetailReports(transactionLogs))
                .build();
    }

    private Retailer prepareRetailerInfo(TerminalInfo terminalInfo) {
        return Retailer.Builder.defaultBuilder()
                .withRetailerLogo(terminalInfo.getMerchantPrintLogo())
                .withRetailerName(terminalInfo.getMerchantName())
                .withRetailerAddress(terminalInfo.getMerchantAddress())
                .withMerchantId(terminalInfo.getMerchantID())
                .withTerminalId(terminalInfo.getTerminalID())
                .build();
    }

    private Performance preparePerformanceInfo() {
        return Performance.Builder.defaultBuilder()
                .withEndDateTime(DateUtils.yyMMddHHmmssSSSDate())
                .withStartDateTime(DateUtils.yyMMddHHmmssSSSDate())
                .build();
    }

    private List<DetailReport> prepareDetailReports(List<TransactionLog> transactionLogs) {
        List<DetailReport> detailReports = new ArrayList<>();
        for (TransactionLog transactionLog : transactionLogs) {
            if (!transactionLog.isTransactionVoided())
                detailReports.add(this.prepareDetailReport(transactionLog));
        }
        return detailReports;
    }

    private DetailReport prepareDetailReport(TransactionLog transactionLog) {
        return DetailReport.Builder.newInstance()
                .withAmount(this.prepareAmount(transactionLog))
                .withApprovalCode(transactionLog.getAuthCode())
                .withCardNumber(StringUtils.encodeAccountNumber(transactionLog.getReadCardResponse().getCardDetails().getPrimaryAccountNumber()))
                .withCardScheme(transactionLog.getReadCardResponse().getCardDetails().getCardScheme().getCardScheme())
                .withInvoiceNumber(transactionLog.getInvoiceNumber())
                .withTransactionType(transactionLog.getTransactionType().getPrintName())
                .withTransactionDate(transactionLog.getTransactionDate())
                .withTransactionTime(transactionLog.getTransactionTime())
                .build();
    }

    private String prepareAmount(TransactionLog transactionLog) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.terminalRepository.getTerminalTransactionCurrencyName());
        stringBuilder.append(" ");
        if (transactionLog.getTransactionType() == TransactionType.VOID)
            stringBuilder.append(StringUtils.formatAmountTwoDecimal(transactionLog.getOriginalTransactionAmount()));
        else
            stringBuilder.append(StringUtils.formatAmountTwoDecimal(transactionLog.getTransactionAmount()));
        return stringBuilder.toString();
    }
}
