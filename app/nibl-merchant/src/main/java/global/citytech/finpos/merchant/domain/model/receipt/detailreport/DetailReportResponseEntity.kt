package global.citytech.finpos.merchant.domain.model.receipt.detailreport

import global.citytech.finposframework.hardware.common.Result

/**
 * Created by Saurav Ghimire on 3/29/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


data class DetailReportResponseEntity(
    val result: Result? = null,
    val message: String? = null
)
