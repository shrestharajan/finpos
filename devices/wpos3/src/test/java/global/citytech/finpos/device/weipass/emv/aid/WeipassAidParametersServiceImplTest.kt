package global.citytech.finpos.device.weipass.emv.aid

import android.text.TextUtils
import android.util.Log
import com.wiseasy.emvprocess.KernelInit
import com.wiseasy.emvprocess.bean.*
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import io.mockk.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import sdk4.wangpos.libemvbinder.utils.MoneyUtil
import java.util.*

/**
 * Created by Rishav Chudal on 5/3/20.
 */
class WeipassAidParametersServiceImplTest {

    private lateinit var SUT: WeipassAidServiceImpl

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        SUT = WeipassAidServiceImpl()
        mockKernelInit()
        mockWeipassLogger()
        mockTextUtils()
        mockAndroidLog()
    }

    /*
     * Inject Aid Test Cases
     */
    @Test
    fun injectAid_AidInjectionRequestIsProvidedAndAllAidsInjectedResultInCoreAreZeros_ReturnsSuccessResponse() {
        //Setup
        val request = mockAidInjectionRequest()
        every { KernelInit.addAID(any()) } returns 0
        every { KernelInit.deleteAID() } returns 0

        //Run
        val response = SUT.injectAid(request)

        //Verify
        assertTrue(response is DeviceResponse)
        assertTrue(request.aidParametersList is List<global.citytech.finposframework.hardware.emv.aid.AidParameters>)
        for (aid in request.aidParametersList) {
            assertTrue(aid.aid is String)
            assertTrue(aid.label is String)
            assertTrue(aid.terminalAidVersion is String)
            assertTrue(aid.defaultTDOL is String)
            assertTrue(aid.defaultDDOL is String)
            assertTrue(aid.denialActionCode is String)
            assertTrue(aid.onlineActionCode is String)
            assertTrue(aid.defaultActionCode is String)
            assertTrue(aid.terminalFloorLimit is String)
            assertTrue(aid.contactlessFloorLimit is String)
            assertTrue(aid.contactlessTransactionLimit is String)
            assertTrue(aid.cvmLimit is String)
        }
        verify { KernelInit.deleteAID() }
        verify { WeipassLogger.log("Final AID Injection Success Count  ::: ".plus(3).plus(" out of ::: ".plus(request.aidParametersList.size))) }
        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun injectAid_AidInjectionRequestIsProvidedAndAllAidsInjectedResultInCoreAreNotZeros_ReturnsFailureResponse() {
        //Setup
        val request = mockAidInjectionRequest()
        every { KernelInit.addAID(any()) } returns -1
        every { KernelInit.deleteAID() } returns 0

        //Run
        val response = SUT.injectAid(request)

        //Verify
        assertTrue(response is DeviceResponse)
        assertTrue(request.aidParametersList is List<global.citytech.finposframework.hardware.emv.aid.AidParameters>)
        for (aid in request.aidParametersList) {
            assertTrue(aid.aid is String)
            assertTrue(aid.label is String)
            assertTrue(aid.terminalAidVersion is String)
            assertTrue(aid.defaultTDOL is String)
            assertTrue(aid.defaultDDOL is String)
            assertTrue(aid.denialActionCode is String)
            assertTrue(aid.onlineActionCode is String)
            assertTrue(aid.defaultActionCode is String)
            assertTrue(aid.terminalFloorLimit is String)
            assertTrue(aid.contactlessFloorLimit is String)
            assertTrue(aid.contactlessTransactionLimit is String)
            assertTrue(aid.cvmLimit is String)
        }
        verify { KernelInit.deleteAID() }
        verify { WeipassLogger.log("Final AID Injection Success Count  ::: ".plus(0).plus(" out of ::: ".plus(request.aidParametersList.size))) }
        assertEquals(Result.FAILURE, response.result)
    }

    @Test
    fun injectAid_AidInjectionRequestIsProvidedAndAtLeastOneAidInjectedResultInCoreIsZero_ReturnsSuccessResponse() {
        //Setup
        val request = mockAidInjectionRequest()
        every { KernelInit.addAID(any()) } returnsMany listOf(-1, -1, 0)
        every { KernelInit.deleteAID() } returns 0

        //Run
        val response = SUT.injectAid(request)

        //Verify
        assertTrue(response is DeviceResponse)
        assertTrue(request.aidParametersList is List<global.citytech.finposframework.hardware.emv.aid.AidParameters>)
        for (aid in request.aidParametersList) {
            assertTrue(aid.aid is String)
            assertTrue(aid.label is String)
            assertTrue(aid.terminalAidVersion is String)
            assertTrue(aid.defaultTDOL is String)
            assertTrue(aid.defaultDDOL is String)
            assertTrue(aid.denialActionCode is String)
            assertTrue(aid.onlineActionCode is String)
            assertTrue(aid.defaultActionCode is String)
            assertTrue(aid.terminalFloorLimit is String)
            assertTrue(aid.contactlessFloorLimit is String)
            assertTrue(aid.contactlessTransactionLimit is String)
            assertTrue(aid.cvmLimit is String)
        }
        verify { KernelInit.deleteAID() }
        verify (exactly = 3) { KernelInit.addAID(any()) }
        verify { WeipassLogger.log("Final AID Injection Success Count  ::: ".plus(1).plus(" out of ::: ".plus(request.aidParametersList.size))) }
        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun injectAid_WhenAidListInAidInjectionRequestIsEmpty_ReturnsFailureResponse() {
        //Setup
        val request =
            global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest(
                emptyList(),
                mockEmvParametersRequest(),
                listOf("00","09")
            )

        //Run
        val response = SUT.injectAid(request)

        //Verify
        assertEquals(Result.FAILURE, response.result)
    }

    /*
     * Visa
     */
    @Test
    fun injectAid_AidListWithVisaAidAndInjectionResultOfVisaAidsOnlyAreZeros_ReturnsSuccessResponse() {
        //Setup
        val aidList = listOf(mockVisaAid(), mockAid(), mockAmexAid(), mockMasterCardAid(),
            mockUnionPayAid(), mockVisaAid())
        val aidInjectionRequest =
            global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest(
                aidList,
                mockEmvParametersRequest(),
                listOf("00")
            )
        val slot = slot<AIDData>()
        every { KernelInit.addAID(capture(slot)) } returnsMany listOf(0, -1, -1, -1, -1, 0)
        every { KernelInit.deleteAID() } returns 0

        //Run
        val response = SUT.injectAid(aidInjectionRequest)

        //Verify
        assertTrue(aidInjectionRequest.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        assertFalse(aidInjectionRequest.aidParametersList.get(1).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        assertFalse(aidInjectionRequest.aidParametersList.get(2).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        assertFalse(aidInjectionRequest.aidParametersList.get(3).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        assertFalse(aidInjectionRequest.aidParametersList.get(4).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        assertTrue(aidInjectionRequest.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        verify { KernelInit.deleteAID() }
        verify (exactly = 6){ KernelInit.addAID(any()) }
        assertTrue(slot.isCaptured)
        assertTrue(slot.captured is AIDData)
        val sdkAidData = slot.captured
        verifyBaseAidParameters(sdkAidData, aidInjectionRequest)
        verifyClPayWaveAidParameters(sdkAidData, aidInjectionRequest)
        verify { WeipassLogger.log("Final AID Injection Success Count  ::: ".plus(2).plus(" out of ::: ".plus(aidInjectionRequest.aidParametersList.size))) }
        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun injectAid_AidListWithVisaAidAndInjectionResultOfOtherAidsOnlyAreZeros_ReturnsSuccessResponse() {
        //Setup
        val aidList = listOf(mockVisaAid(), mockAid(), mockAmexAid(), mockMasterCardAid(),
            mockUnionPayAid(), mockVisaAid())
        val aidInjectionRequest =
            global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest(
                aidList,
                mockEmvParametersRequest(),
                listOf("00")
            )
        val slot = slot<AIDData>()
        every { KernelInit.addAID(capture(slot)) } returnsMany listOf(-1, 0, 0, 0, 0, -1)
        every { KernelInit.deleteAID() } returns 0

        //Run
        val response = SUT.injectAid(aidInjectionRequest)

        //Verify
        assertTrue(aidInjectionRequest.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        assertFalse(aidInjectionRequest.aidParametersList.get(1).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        assertFalse(aidInjectionRequest.aidParametersList.get(2).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        assertFalse(aidInjectionRequest.aidParametersList.get(3).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        assertFalse(aidInjectionRequest.aidParametersList.get(4).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        assertTrue(aidInjectionRequest.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        verify { KernelInit.deleteAID() }
        verify { KernelInit.addAID(any()) }
        assertTrue(slot.isCaptured)
        assertTrue(slot.captured is AIDData)
        val sdkAidData = slot.captured
        verifyBaseAidParameters(sdkAidData, aidInjectionRequest)
        verifyClPayWaveAidParameters(sdkAidData, aidInjectionRequest)
        verify { WeipassLogger.log("Final AID Injection Success Count  ::: ".plus(4).plus(" out of ::: ".plus(aidInjectionRequest.aidParametersList.size))) }
        assertEquals(Result.SUCCESS, response.result)
    }

    /*
     * Amex
     */
    @Test
    fun injectAid_AidListHasAmexAidAndInjectionResultOfAmexAidsOnlyInCoreAreZeros_ReturnsSuccessResponse() {
        //Setup
        val aidList = listOf(mockAmexAid(), mockAid(), mockVisaAid(), mockMasterCardAid(),
            mockUnionPayAid(), mockAmexAid())
        val request =
            global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest(
                aidList,
                mockEmvParametersRequest(),
                listOf("00")
            )
        val slot = slot<AIDData>()
        every { KernelInit.addAID(capture(slot)) } returnsMany listOf(0, -1, -1, -1, -1, 0)
        every { KernelInit.deleteAID() } returns 0

        //Run
        val response = SUT.injectAid(request)

        //Verify
        assertTrue(request.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        assertFalse(request.aidParametersList.get(1).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        assertFalse(request.aidParametersList.get(2).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        assertFalse(request.aidParametersList.get(3).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        assertFalse(request.aidParametersList.get(4).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        assertTrue(request.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        verify { KernelInit.addAID(any()) }
        verify { KernelInit.deleteAID() }
        assertTrue(slot.isCaptured)
        assertTrue(slot.captured is AIDData)
        val capturedAIDData = slot.captured
        verifyBaseAidParameters(capturedAIDData, request)
        verifyClAmexAidParameters(capturedAIDData, request)
        verify { WeipassLogger.log("Final AID Injection Success Count  ::: ".plus(2).plus(" out of ::: ".plus(request.aidParametersList.size))) }
        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun injectAid_AidListHasAmexAidAndInjectionResultOfOtherAidsOnlyInCoreAreZeros_ReturnsSuccessResponse() {
        //Setup
        val aidList = listOf(mockAmexAid(), mockAid(), mockVisaAid(), mockMasterCardAid(),
            mockUnionPayAid(), mockAmexAid())
        val request =
            global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest(
                aidList,
                mockEmvParametersRequest(),
                listOf("00")
            )
        val slot = slot<AIDData>()
        every { KernelInit.addAID(capture(slot)) } returnsMany listOf(-1, 0, 0, 0, 0, -1)
        every { KernelInit.deleteAID() } returns 0

        //Run
        val response = SUT.injectAid(request)

        //Verify
        assertTrue(request.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        assertFalse(request.aidParametersList.get(1).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        assertFalse(request.aidParametersList.get(2).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        assertFalse(request.aidParametersList.get(3).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        assertFalse(request.aidParametersList.get(4).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        assertTrue(request.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        verify { KernelInit.deleteAID() }
        verify { KernelInit.addAID(any()) }
        assertTrue(slot.isCaptured)
        assertTrue(slot.captured is AIDData)
        val capturedAIDData = slot.captured
        verifyBaseAidParameters(capturedAIDData, request)
        verifyClAmexAidParameters(capturedAIDData, request)
        verify { WeipassLogger.log("Final AID Injection Success Count  ::: ".plus(4).plus(" out of ::: ".plus(request.aidParametersList.size))) }
        assertEquals(Result.SUCCESS, response.result)
    }

    /*
     * MasterCard
     */
    @Test
    fun injectAid_AidListHasMasterCardAidAndInjectionResultOfMasterCardAidsOnlyInCoreAreZeros_ReturnsSuccessResponse() {
        //Setup
        val aidList = listOf(mockMasterCardAid(), mockAid(), mockVisaAid(), mockAmexAid(), mockUnionPayAid(), mockMasterCardAid())
        val request =
            global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest(
                aidList,
                mockEmvParametersRequest(),
                listOf("00")
            )
        val slot = slot<AIDData>()
        every { KernelInit.addAID(capture(slot)) } returnsMany listOf(0, -1, -1, -1, -1, 0)
        every { KernelInit.deleteAID() } returns 0

        //Run
        val response = SUT.injectAid(request)

        //Verify
        assertTrue(request.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        assertFalse(request.aidParametersList.get(1).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        assertFalse(request.aidParametersList.get(2).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        assertFalse(request.aidParametersList.get(3).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        assertFalse(request.aidParametersList.get(4).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        assertTrue(request.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        verify { KernelInit.deleteAID() }
        verify (exactly = 6){ KernelInit.addAID(any()) }
        assertTrue(slot.isCaptured)
        assertTrue(slot.captured is AIDData)
        val capturedAIDData = slot.captured
        verifyBaseAidParameters(capturedAIDData, request)
        verifyCLPayPassAIDData(capturedAIDData, request)
        verify { WeipassLogger.log("Final AID Injection Success Count  ::: ".plus(2).plus(" out of ::: ".plus(request.aidParametersList.size))) }
        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun injectAid_AidListHasMasterCardAidAndInjectionResultOfOtherAidsOnlyInCoreAreZeros_ReturnsSuccessResponse() {
        //Setup
        val aidList = listOf(mockMasterCardAid(), mockAid(), mockVisaAid(), mockAmexAid(), mockUnionPayAid(), mockMasterCardAid())
        val request =
            global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest(
                aidList,
                mockEmvParametersRequest(),
                listOf("00")
            )
        val slot = slot<AIDData>()
        every { KernelInit.addAID(capture(slot)) } returnsMany listOf(-1, 0, 0, 0, 0, -1)
        every { KernelInit.deleteAID() } returns 0

        //Run
        val response = SUT.injectAid(request)

        //Verify
        assertTrue(request.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        assertFalse(request.aidParametersList.get(1).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        assertFalse(request.aidParametersList.get(2).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        assertFalse(request.aidParametersList.get(3).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        assertFalse(request.aidParametersList.get(4).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        assertTrue(request.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        verify { KernelInit.addAID(any()) }
        verify (exactly = 6){ KernelInit.addAID(any()) }
        assertTrue(slot.isCaptured)
        assertTrue(slot.captured is AIDData)
        val capturedAIDData = slot.captured
        verifyBaseAidParameters(capturedAIDData, request)
        verifyCLPayPassAIDData(capturedAIDData, request)
        verify { WeipassLogger.log("Final AID Injection Success Count  ::: ".plus(4).plus(" out of ::: ".plus(request.aidParametersList.size))) }
        assertEquals(Result.SUCCESS, response.result)
    }

    /*
     * Union Pay
     */
    @Test
    fun injectAid_AidListHasUnionPayAidAndInjectionResultOfUnionPayAidsOnlyInCoreAreZeros_ReturnsSuccessResponse() {
        //Setup
        val aidList = listOf(mockUnionPayAid(), mockAid(), mockVisaAid(), mockAmexAid(),
            mockMasterCardAid(), mockUnionPayAid())
        val request =
            global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest(
                aidList,
                mockEmvParametersRequest(),
                listOf("00")
            )
        val slot = slot<AIDData>()
        every { KernelInit.addAID(capture(slot)) } returnsMany listOf(0, -1, -1, -1, -1, 0)
        every { KernelInit.deleteAID() } returns 0

        //Run
        val response = SUT.injectAid(request)

        //Verify
        assertTrue(request.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        assertFalse(request.aidParametersList.get(1).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        assertFalse(request.aidParametersList.get(2).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        assertFalse(request.aidParametersList.get(3).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        assertFalse(request.aidParametersList.get(4).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        assertTrue(request.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        verify { KernelInit.deleteAID() }
        verify (exactly = 6){ KernelInit.addAID(any()) }
        assertTrue(slot.isCaptured)
        assertTrue(slot.captured is AIDData)
        val capturedAIDData = slot.captured
        verifyBaseAidParameters(capturedAIDData, request)
        verifyCLqPBOCAIDData(capturedAIDData, request)
        verify { WeipassLogger.log("Final AID Injection Success Count  ::: ".plus(2).plus(" out of ::: ".plus(request.aidParametersList.size))) }
        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun injectAid_WhenAidListHasUnionPayAidAndInjectionResultOfOtherAidsOnlyInCoreAreZeros_ReturnsSuccessResponse() {
        //Setup
        val aidList = listOf(mockUnionPayAid(), mockAid(), mockVisaAid(), mockAmexAid(),
            mockMasterCardAid(), mockUnionPayAid())
        val request =
            global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest(
                aidList,
                mockEmvParametersRequest(),
                listOf("00")
            )
        val slot = slot<AIDData>()
        every { KernelInit.addAID(capture(slot)) } returnsMany listOf(-1, 0, 0, 0, 0, -1)
        every { KernelInit.deleteAID() } returns 0

        //Run
        val response = SUT.injectAid(request)

        //Verify
        assertTrue(request.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        assertFalse(request.aidParametersList.get(1).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        assertFalse(request.aidParametersList.get(2).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        assertFalse(request.aidParametersList.get(3).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        assertFalse(request.aidParametersList.get(4).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        assertTrue(request.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        verify { KernelInit.deleteAID() }
        verify (exactly = 6){ KernelInit.addAID(any()) }
        assertTrue(slot.isCaptured)
        assertTrue(slot.captured is AIDData)
        val capturedAIDData = slot.captured
        verifyBaseAidParameters(capturedAIDData, request)
        verifyCLqPBOCAIDData(capturedAIDData, request)
        verify { WeipassLogger.log("Final AID Injection Success Count  ::: ".plus(4).plus(" out of ::: ".plus(request.aidParametersList.size))) }
        assertEquals(Result.SUCCESS, response.result)
    }

    /*
     * Other AIDs
     */
    @Test
    fun injectAid_AidInListIsNotVisaOrMasterCardOrAmexOrUnionPayAndInjectionResultInCoreForOtherAidsOnlyAreZeros_ReturnsSuccessResponse() {
        //Setup
        val aidList = listOf(mockAid(), mockVisaAid(), mockAmexAid(), mockMasterCardAid(),
            mockUnionPayAid(), mockAid())
        val request =
            global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest(
                aidList,
                mockEmvParametersRequest(),
                listOf("00")
            )
        val slot = slot<AIDData>()
        every { KernelInit.addAID(capture(slot)) } returnsMany listOf(0, -1, -1, -1, -1, 0)
        every { KernelInit.deleteAID() } returns 0

        //Run
        val response = SUT.injectAid(request)

        //Verify
        assertFalse(request.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        assertFalse(request.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        assertFalse(request.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        assertFalse(request.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        assertFalse(request.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        assertFalse(request.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        assertFalse(request.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        assertFalse(request.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        verify { KernelInit.deleteAID() }
        verify (exactly = 6){ KernelInit.addAID(any()) }
        assertTrue(slot.isCaptured)
        val capturedAIDData = slot.captured
        verifyBaseAidParameters(capturedAIDData, request)
        verify { WeipassLogger.log("Final AID Injection Success Count  ::: ".plus(2).plus(" out of ::: ".plus(request.aidParametersList.size))) }
        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun injectAid_AidInListIsNotVisaOrMasterCardOrAmexOrUnionPayAndInjectionResultForOtherAidsOnlyInCoreAreNotZeros_ReturnsSuccessResponse() {
        //Setup
        val aidList = listOf(mockAid(), mockVisaAid(), mockAmexAid(), mockMasterCardAid(),
            mockUnionPayAid(), mockAid())
        val request =
            global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest(
                aidList,
                mockEmvParametersRequest(),
                listOf("00")
            )
        val slot = slot<AIDData>()
        every { KernelInit.addAID(capture(slot)) } returnsMany listOf(-1, 0, 0, 0, 0, -1)
        every { KernelInit.deleteAID() } returns 0

        //Run
        val response = SUT.injectAid(request)

        //Verify
        assertFalse(request.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        assertFalse(request.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.VISA.rid))
        assertFalse(request.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        assertFalse(request.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.AMEX.rid))
        assertFalse(request.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        assertFalse(request.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.MASTERCARD.rid))
        assertFalse(request.aidParametersList.get(0).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        assertFalse(request.aidParametersList.get(5).aid.contains(global.citytech.finposframework.hardware.emv.aid.CardBrand.UNION_PAY.rid))
        verify { KernelInit.deleteAID() }
        verify (exactly = 6){ KernelInit.addAID(any()) }
        assertTrue(slot.isCaptured)
        val capturedAIDData = slot.captured
        verifyBaseAidParameters(capturedAIDData, request)
        verify { WeipassLogger.log("Final AID Injection Success Count  ::: ".plus(4).plus(" out of ::: ".plus(request.aidParametersList.size))) }
        assertEquals(Result.SUCCESS, response.result)
    }

    /*
     * Erase all Aid Test Cases
     */
    @Test
    fun eraseAllAid_WhenEraseAidResultInCoreIsZero_ReturnsSuccessResponse() {
        //Setup
        every { KernelInit.deleteAID() } returns 0

        //Run
        val response = SUT.eraseAllAid()

        //Verify
        verify { KernelInit.deleteAID() }
        verify { WeipassLogger.log("Erase All AID result ::: ".plus(0))}
        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun eraseAllAid_WhenEraseAidResultInCoreIsNotZero_ReturnsFailureResponse() {
        //Setup
        every { KernelInit.deleteAID() } returns -1

        //Run
        val response = SUT.eraseAllAid()

        //Verify
        verify { KernelInit.deleteAID() }
        verify { WeipassLogger.log("Erase All AID result ::: ".plus(-1))}
        verify { WeipassLogger.log("Couldn't Clear AID Keys") }
        assertEquals(Result.FAILURE, response.result)
    }

    private fun mockKernelInit() {
        mockkStatic(KernelInit::class)
    }

    private fun mockWeipassLogger() {
        mockkObject(WeipassLogger)
        mockkObject(WeipassLogger.Companion)
        every { WeipassLogger.log(any()) } just Runs
    }

    private fun mockTextUtils() {
        mockkStatic(TextUtils::class)
        every { TextUtils.isEmpty(any()) } returns false
    }

    private fun mockAndroidLog() {
        mockkStatic(Log::class)
        every { Log.i(any(), any()) } returns 0
        every { Log.d(any(), any()) } returns 0
        every { Log.v(any(), any()) } returns 0
        every { Log.e(any(), any()) } returns 0
    }

    private fun mockAidInjectionRequest(): global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest {
        val aidList = listOf(mockAid(), mockAid(), mockAid())
        return global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest(
            aidList,
            mockEmvParametersRequest(),
            listOf("00")
        )
    }

    private fun mockAid(): global.citytech.finposframework.hardware.emv.aid.AidParameters {
        return global.citytech.finposframework.hardware.emv.aid.AidParameters(
            "A0000002281010",
            "SPAN MCHIP",
            "0002",
            "",
            "9F3704",
            "0000000000",
            "FC408CF800",
            "FC408CA800",
            "000000000000",
            "000000000000",
            "000001000000",
            "000000010000"
        )
    }

    private fun mockVisaAid(): global.citytech.finposframework.hardware.emv.aid.AidParameters {
        return global.citytech.finposframework.hardware.emv.aid.AidParameters(
            "A0000000031010",
            "VISA CREDIT",
            "00960096008D",
            "9700",
            "9F3704",
            "0010000000",
            "D84004F800",
            "D84004A800",
            "000000000000",
            "000000000000",
            "000001000000",
            "000000010000"
        )
    }

    private fun mockAmexAid(): global.citytech.finposframework.hardware.emv.aid.AidParameters {
        return global.citytech.finposframework.hardware.emv.aid.AidParameters(
            "A0000000251010",
            "American Express",
            "00960096008D",
            "9700",
            "9F3704",
            "0010000000",
            "D84004F800",
            "D84004A800",
            "000000000000",
            "000000000000",
            "000001000000",
            "000000010000"
        )
    }

    private fun mockMasterCardAid() =
        global.citytech.finposframework.hardware.emv.aid.AidParameters(
            "A0000000041010",
            "MASTERCARD",
            "0002",
            "970F9F02065F2A029A039C0195059F3704",
            "9F3704",
            "0000000000",
            "FE50BCF800",
            "FE50BCA000",
            "000000000000",
            "000000000000",
            "000001000000",
            "000000010000"
        )

    private fun mockUnionPayAid() =
        global.citytech.finposframework.hardware.emv.aid.AidParameters(
            "A000000333010101",
            "UNION PAY",
            "0002",
            "970F9F02065F2A029A039C0195059F3704",
            "9F3704",
            "0000000000",
            "FE50BCF800",
            "FE50BCA000",
            "000000000000",
            "000000000000",
            "000001000000",
            "000000010000"
        )

    private fun mockEmvParametersRequest() =
        EmvParametersRequest(
            "0682",
            "0682",
            "01",
            "E0F0C8",
            "22",
            "7399",
            "06804344",
            "800150400566",
            "Bhat Bhateni Super Market",
            "35",
            "D000F0A000",
            "01"
        )


    /*
     * Common Verifying items for BaseAIDData Parameters
     */
    private fun verifyBaseAidParameters(sdkAidData: AIDData,
                                        aidInjectionRequest: global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest
    ) {
        assertNotNull(sdkAidData.baseAIDData)

        assertNotNull(sdkAidData.baseAIDData.aiD_9F06)
        assertEquals(sdkAidData.baseAIDData.aiD_9F06,
            aidInjectionRequest.aidParametersList.get(0).aid)
        assertEquals(sdkAidData.baseAIDData.aiD_9F06,
            aidInjectionRequest.aidParametersList.get(5).aid)

        assertNotNull(sdkAidData.baseAIDData.appName)
        assertEquals(sdkAidData.baseAIDData.appName,
            aidInjectionRequest.aidParametersList.get(0).label)
        assertEquals(sdkAidData.baseAIDData.appName,
            aidInjectionRequest.aidParametersList.get(5).label)

        assertNotNull(sdkAidData.baseAIDData.version_9F09)
        assertEquals(sdkAidData.baseAIDData.version_9F09,
            aidInjectionRequest.aidParametersList.get(0).terminalAidVersion)
        assertEquals(sdkAidData.baseAIDData.version_9F09,
            aidInjectionRequest.aidParametersList.get(5).terminalAidVersion)

        assertNotNull(sdkAidData.baseAIDData.defaultTDOL)
        assertEquals(sdkAidData.baseAIDData.defaultTDOL,
            aidInjectionRequest.aidParametersList.get(0).defaultTDOL)
        assertEquals(sdkAidData.baseAIDData.defaultTDOL,
            aidInjectionRequest.aidParametersList.get(5).defaultTDOL)

        assertNotNull(sdkAidData.baseAIDData.defaultDDOL)
        assertEquals(sdkAidData.baseAIDData.defaultDDOL,
            aidInjectionRequest.aidParametersList.get(0).defaultDDOL)
        assertEquals(sdkAidData.baseAIDData.defaultDDOL,
            aidInjectionRequest.aidParametersList.get(5).defaultDDOL)

        assertNotNull(sdkAidData.baseAIDData.tacDenial)
        assertEquals(sdkAidData.baseAIDData.tacDenial,
            aidInjectionRequest.aidParametersList.get(0).denialActionCode)
        assertEquals(sdkAidData.baseAIDData.tacDenial,
            aidInjectionRequest.aidParametersList.get(5).denialActionCode)

        assertNotNull(sdkAidData.baseAIDData.tacOnline)
        assertEquals(sdkAidData.baseAIDData.tacOnline,
            aidInjectionRequest.aidParametersList.get(0).onlineActionCode)
        assertEquals(sdkAidData.baseAIDData.tacOnline,
            aidInjectionRequest.aidParametersList.get(5).onlineActionCode)

        assertNotNull(sdkAidData.baseAIDData.tacDefault)
        assertEquals(sdkAidData.baseAIDData.tacDefault,
            aidInjectionRequest.aidParametersList.get(0).defaultActionCode)
        assertEquals(sdkAidData.baseAIDData.tacDefault,
            aidInjectionRequest.aidParametersList.get(5).defaultActionCode)

        assertNotNull(sdkAidData.baseAIDData.floorLimit_9F1B)
        assertEquals(sdkAidData.baseAIDData.floorLimit_9F1B, MoneyUtil
            .toCent(aidInjectionRequest.aidParametersList.get(0).terminalFloorLimit))
        assertEquals(sdkAidData.baseAIDData.floorLimit_9F1B, MoneyUtil
            .toCent(aidInjectionRequest.aidParametersList.get(5).terminalFloorLimit))

        assertNotNull(sdkAidData.baseAIDData.merchantId_9F16)
        assertEquals(sdkAidData.baseAIDData.merchantId_9F16,
            aidInjectionRequest.emvParametersRequest.merchantIdentifier)
        assertEquals(sdkAidData.baseAIDData.merchantId_9F16,
            aidInjectionRequest.emvParametersRequest.merchantIdentifier)

        assertNotNull(sdkAidData.baseAIDData.terminalId_9F1C)
        assertEquals(sdkAidData.baseAIDData.terminalId_9F1C,
            aidInjectionRequest.emvParametersRequest.terminalId)
        assertEquals(sdkAidData.baseAIDData.terminalId_9F1C,
            aidInjectionRequest.emvParametersRequest.terminalId)

        assertNotNull(sdkAidData.baseAIDData.merchantName)
        assertEquals(sdkAidData.baseAIDData.merchantName,
            aidInjectionRequest.emvParametersRequest.merchantName)
        assertEquals(sdkAidData.baseAIDData.merchantName,
            aidInjectionRequest.emvParametersRequest.merchantName)

        assertNotNull(sdkAidData.baseAIDData.mcC_9F15)
        assertEquals(sdkAidData.baseAIDData.mcC_9F15,
            aidInjectionRequest.emvParametersRequest.merchantCategoryCode)
        assertEquals(sdkAidData.baseAIDData.mcC_9F15,
            aidInjectionRequest.emvParametersRequest.merchantCategoryCode)

        assertNotNull(sdkAidData.baseAIDData.transCurrencyCode_5F2A)
        assertEquals(sdkAidData.baseAIDData.transCurrencyCode_5F2A,
            aidInjectionRequest.emvParametersRequest.transactionCurrencyCode)
        assertEquals(sdkAidData.baseAIDData.transCurrencyCode_5F2A,
            aidInjectionRequest.emvParametersRequest.transactionCurrencyCode)

        assertNotNull(sdkAidData.baseAIDData.transCurrencyExp)
        assertEquals(sdkAidData.baseAIDData.transCurrencyExp,
            aidInjectionRequest.emvParametersRequest.transactionCurrencyExponent)
        assertEquals(sdkAidData.baseAIDData.transCurrencyExp,
            aidInjectionRequest.emvParametersRequest.transactionCurrencyExponent)

        assertNotNull(sdkAidData.baseAIDData.referCurrencyCode_9F3C)
        assertEquals(sdkAidData.baseAIDData.referCurrencyCode_9F3C,
            aidInjectionRequest.emvParametersRequest.transactionCurrencyCode)
        assertEquals(sdkAidData.baseAIDData.referCurrencyCode_9F3C,
            aidInjectionRequest.emvParametersRequest.transactionCurrencyCode)

        assertNotNull(sdkAidData.baseAIDData.referCurrencyExp_9F3D)
        assertEquals(sdkAidData.baseAIDData.referCurrencyExp_9F3D,
            aidInjectionRequest.emvParametersRequest.transactionCurrencyExponent)
        assertEquals(sdkAidData.baseAIDData.referCurrencyExp_9F3D,
            aidInjectionRequest.emvParametersRequest.transactionCurrencyExponent)

        assertNotNull(sdkAidData.baseAIDData.terminalType_9F35)
        assertEquals(sdkAidData.baseAIDData.terminalType_9F35,
            aidInjectionRequest.emvParametersRequest.terminalType)
        assertEquals(sdkAidData.baseAIDData.terminalType_9F35,
            aidInjectionRequest.emvParametersRequest.terminalType)

        assertNotNull(sdkAidData.baseAIDData.terminalCap_9F33)
        assertEquals(sdkAidData.baseAIDData.terminalCap_9F33,
            aidInjectionRequest.emvParametersRequest.terminalCapabilities)
        assertEquals(sdkAidData.baseAIDData.terminalCap_9F33,
            aidInjectionRequest.emvParametersRequest.terminalCapabilities)

        assertNotNull(sdkAidData.baseAIDData.exTerminalCap_9F40)
        assertEquals(sdkAidData.baseAIDData.exTerminalCap_9F40,
            aidInjectionRequest.emvParametersRequest.additionalTerminalCapabilities)
        assertEquals(sdkAidData.baseAIDData.exTerminalCap_9F40,
            aidInjectionRequest.emvParametersRequest.additionalTerminalCapabilities)

        assertNotNull(sdkAidData.baseAIDData.countryCode_9F1A)
        assertEquals(sdkAidData.baseAIDData.countryCode_9F1A,
            aidInjectionRequest.emvParametersRequest.terminalCountryCode)
        assertEquals(sdkAidData.baseAIDData.countryCode_9F1A,
            aidInjectionRequest.emvParametersRequest.terminalCountryCode)
    }

    /*
     * Common Verifying items for CLPayWaveAIDData Parameters
     */
    private fun verifyClPayWaveAidParameters(sdkAidData: AIDData,
                                             aidInjectionRequest: global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest
    ) {
        assertNotNull(sdkAidData.clAIDData)
        assertTrue(sdkAidData.clAIDData is CLPayWaveAIDData)
        val paywaveData = sdkAidData.clAIDData as CLPayWaveAIDData

        assertNotNull(paywaveData.clFloorLimit)
        assertEquals(paywaveData.clFloorLimit, MoneyUtil
            .toCent(aidInjectionRequest.aidParametersList.get(0).contactlessFloorLimit))
        assertEquals(paywaveData.clFloorLimit, MoneyUtil
            .toCent(aidInjectionRequest.aidParametersList.get(5).contactlessFloorLimit))

        assertNotNull(paywaveData.clTransLimit)
        assertEquals(paywaveData.clTransLimit, MoneyUtil
            .toCent(aidInjectionRequest.aidParametersList.get(0).contactlessTransactionLimit))
        assertEquals(paywaveData.clTransLimit, MoneyUtil
            .toCent(aidInjectionRequest.aidParametersList.get(5).contactlessTransactionLimit))

        assertNotNull(paywaveData.clcvmLimit)
        assertEquals(paywaveData.clcvmLimit, MoneyUtil
            .toCent(aidInjectionRequest.aidParametersList.get(0).cvmLimit))
        assertEquals(paywaveData.clcvmLimit, MoneyUtil
            .toCent(aidInjectionRequest.aidParametersList.get(5).cvmLimit))

        assertNotNull(paywaveData.enableAmountZeroOption)
        assertEquals(paywaveData.enableAmountZeroOption, "1")

        assertNotNull(paywaveData.amountZeroOption)
        assertEquals(paywaveData.amountZeroOption, "0")

        assertNotNull(paywaveData.enableFloorLimit)
        assertEquals(paywaveData.enableFloorLimit, "1")

        assertNotNull(paywaveData.enableTransLimit)
        assertEquals(paywaveData.enableTransLimit, "1")

        assertNotNull(paywaveData.enableCVMLimit)
        assertEquals(paywaveData.enableCVMLimit, "1")

        assertNotNull(paywaveData.ttQ_9F66)
        assertEquals(paywaveData.ttQ_9F66, aidInjectionRequest.emvParametersRequest.ttq)
    }

    /*
     * Common Verifying items for CLAMEXAIDData Parameters
     */
    private fun verifyClAmexAidParameters(capturedAIDData: AIDData,
                                          request: global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest
    ) {
        assertTrue(capturedAIDData.clAIDData is CLAMEXAIDData)
        val clamexaidData = capturedAIDData.clAIDData as CLAMEXAIDData

        assertNotNull(clamexaidData.mercNameORLocation_9F4E)
        assertEquals(clamexaidData.mercNameORLocation_9F4E,
            request.emvParametersRequest.merchantName)

        assertNotNull(clamexaidData.clFloorLimit)
        assertEquals(clamexaidData.clFloorLimit,
            MoneyUtil.toCent(request.aidParametersList.get(0).contactlessFloorLimit))
        assertEquals(clamexaidData.clFloorLimit,
            MoneyUtil.toCent(request.aidParametersList.get(5).contactlessFloorLimit))

        assertNotNull(clamexaidData.clTransLimit)
        assertEquals(clamexaidData.clTransLimit,
            MoneyUtil.toCent(request.aidParametersList.get(0).contactlessTransactionLimit))
        assertEquals(clamexaidData.clTransLimit,
            MoneyUtil.toCent(request.aidParametersList.get(5).contactlessTransactionLimit))

        assertNotNull(clamexaidData.clcvmLimit)
        assertEquals(clamexaidData.clcvmLimit,
            MoneyUtil.toCent(request.aidParametersList.get(0).cvmLimit))
        assertEquals(clamexaidData.clcvmLimit,
            MoneyUtil.toCent(request.aidParametersList.get(5).cvmLimit))

        assertNotNull(clamexaidData.ttQ_9F66)
        assertEquals(clamexaidData.ttQ_9F66, request.emvParametersRequest.ttq)
    }

    /*
     * Common Verifying items for CLPaypassAIDData Parameters
     */
    private fun verifyCLPayPassAIDData(capturedAIDData: AIDData,
                                       request: global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest
    ) {
        assertNotNull(capturedAIDData.clAIDData)
        assertTrue(capturedAIDData.clAIDData is CLPayPassAIDData)
        val clPayPassAIDData = capturedAIDData.clAIDData as CLPayPassAIDData

        assertNotNull(clPayPassAIDData.msdAppVersion_9F6D)
        assertEquals(clPayPassAIDData.msdAppVersion_9F6D, "001")

        assertNotNull(clPayPassAIDData.udoL_DF811A)
        assertEquals(clPayPassAIDData.udoL_DF811A, "9F6A04")

        assertNotNull(clPayPassAIDData.clTransLimitCDCVM_DF8125)
        assertEquals(clPayPassAIDData.clTransLimitCDCVM_DF8125,
            MoneyUtil.toCent(request.aidParametersList.get(0).contactlessTransactionLimit))
        assertEquals(clPayPassAIDData.clTransLimitCDCVM_DF8125,
            MoneyUtil.toCent(request.aidParametersList.get(5).contactlessTransactionLimit))

        assertNotNull(clPayPassAIDData.clTransLimitNoCDCVM_DF8124)
        assertEquals(clPayPassAIDData.clTransLimitNoCDCVM_DF8124,
            MoneyUtil.toCent(request.aidParametersList.get(0).contactlessTransactionLimit))
        assertEquals(clPayPassAIDData.clTransLimitNoCDCVM_DF8124,
            MoneyUtil.toCent(request.aidParametersList.get(5).contactlessTransactionLimit))

        assertNotNull(clPayPassAIDData.cltacDenial_DF8121)
        assertEquals(clPayPassAIDData.cltacDenial_DF8121,
            request.aidParametersList.get(0).denialActionCode)
        assertEquals(clPayPassAIDData.cltacDenial_DF8121,
            request.aidParametersList.get(5).denialActionCode)

        assertNotNull(clPayPassAIDData.cltacOnline_DF8122)
        assertEquals(clPayPassAIDData.cltacOnline_DF8122,
            request.aidParametersList.get(0).onlineActionCode)
        assertEquals(clPayPassAIDData.cltacOnline_DF8122,
            request.aidParametersList.get(5).onlineActionCode)

        assertNotNull(clPayPassAIDData.clTACDefault_DF8120)
        assertEquals(clPayPassAIDData.clTACDefault_DF8120,
            request.aidParametersList.get(0).defaultActionCode)
        assertEquals(clPayPassAIDData.clTACDefault_DF8120,
            request.aidParametersList.get(5).defaultActionCode)

        assertNotNull(clPayPassAIDData.transType_9C)
        assertEquals(clPayPassAIDData.transType_9C, "00")

        assertNotNull(clPayPassAIDData.enableFloorLimit)
        assertEquals(clPayPassAIDData.enableFloorLimit, "1")

        assertNotNull(clPayPassAIDData.enableTransLimit)
        assertEquals(clPayPassAIDData.enableTransLimit, "1")

        assertNotNull(clPayPassAIDData.enableCVMLimit)
        assertEquals(clPayPassAIDData.enableCVMLimit, "1")

        assertNotNull(clPayPassAIDData.enableTransLimitONdevice)
        assertEquals(clPayPassAIDData.enableTransLimitONdevice, "1")

        assertNotNull(clPayPassAIDData.enableTransLimitNoONdevice)
        assertEquals(clPayPassAIDData.enableTransLimitNoONdevice, "1")

        assertNotNull(clPayPassAIDData.clFloorLimit)
        assertEquals(clPayPassAIDData.clFloorLimit,
            MoneyUtil.toCent(request.aidParametersList.get(0).contactlessFloorLimit))
        assertEquals(clPayPassAIDData.clFloorLimit,
            MoneyUtil.toCent(request.aidParametersList.get(5).contactlessFloorLimit))

        assertNotNull(clPayPassAIDData.clcvmLimit)
        assertEquals(clPayPassAIDData.clcvmLimit,
            MoneyUtil.toCent(request.aidParametersList.get(0).cvmLimit))
        assertEquals(clPayPassAIDData.clcvmLimit,
            MoneyUtil.toCent(request.aidParametersList.get(5).cvmLimit))
    }

    /*
     * Common Verifying items for CLqPBOCAIDData Parameters
     */
    private fun verifyCLqPBOCAIDData(capturedAIDData: AIDData,
                                     request: global.citytech.finposframework.hardware.emv.aid.AidInjectionRequest
    ) {
        assertNotNull(capturedAIDData.clAIDData)
        assertTrue(capturedAIDData.clAIDData is CLqPBOCAIDData)

        val cLqPBOCAIDData = capturedAIDData.clAIDData as CLqPBOCAIDData

        assertNotNull(cLqPBOCAIDData.clFloorLimit)
        assertEquals(cLqPBOCAIDData.clFloorLimit,
            MoneyUtil.toCent(request.aidParametersList.get(0).contactlessFloorLimit))
        assertEquals(cLqPBOCAIDData.clFloorLimit,
            MoneyUtil.toCent(request.aidParametersList.get(5).contactlessFloorLimit))

        assertNotNull(cLqPBOCAIDData.clTransLimit)
        assertEquals(cLqPBOCAIDData.clTransLimit,
            MoneyUtil.toCent(request.aidParametersList.get(0).contactlessTransactionLimit))
        assertEquals(cLqPBOCAIDData.clTransLimit,
            MoneyUtil.toCent(request.aidParametersList.get(5).contactlessTransactionLimit))

        assertNotNull(cLqPBOCAIDData.clFloorLimit)
        assertEquals(cLqPBOCAIDData.clFloorLimit,
            MoneyUtil.toCent(request.aidParametersList.get(0).contactlessFloorLimit))
        assertEquals(cLqPBOCAIDData.clFloorLimit,
            MoneyUtil.toCent(request.aidParametersList.get(5).contactlessFloorLimit))

        assertNotNull(cLqPBOCAIDData.clcvmLimit)
        assertEquals(cLqPBOCAIDData.clcvmLimit,
            MoneyUtil.toCent(request.aidParametersList.get(0).cvmLimit))
        assertEquals(cLqPBOCAIDData.clcvmLimit,
            MoneyUtil.toCent(request.aidParametersList.get(5).cvmLimit))

        assertNotNull(cLqPBOCAIDData.enableAmountZeroOption)
        assertEquals(cLqPBOCAIDData.enableAmountZeroOption, "1")

        assertNotNull(cLqPBOCAIDData.amountZeroOption)
        assertEquals(cLqPBOCAIDData.amountZeroOption, "0")

        assertNotNull(cLqPBOCAIDData.enableFloorLimit)
        assertEquals(cLqPBOCAIDData.enableFloorLimit, "1")

        assertNotNull(cLqPBOCAIDData.enableTransLimit)
        assertEquals(cLqPBOCAIDData.enableTransLimit, "1")

        assertNotNull(cLqPBOCAIDData.enableCVMLimit)
        assertEquals(cLqPBOCAIDData.enableCVMLimit, "1")

        assertNotNull(cLqPBOCAIDData.ttQ_9F66)
        assertEquals(cLqPBOCAIDData.ttQ_9F66, request.emvParametersRequest.ttq)
    }

}