package global.citytech.finpos.merchant.service

import android.content.Context
import android.content.Intent
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.Observer
import global.citytech.common.AppCountDownTimer
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.utils.AppConstant
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finpos.merchant.utils.getMinutesAndSecondsDataFromMillis
import io.reactivex.disposables.CompositeDisposable

class ScreenSaverService : LifecycleService() {
    val TAG = ScreenSaverService::class.java.name
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var countDownTimer = AppCountDownTimer(
        AppConstant.SCREEN_SAVER_INTERVAL_MILLIS,
        1000,
        this::onCountDownTick,
        this::onCountDownFinish
    )

    companion object {

        fun start(context: Context) {
            stop(context)
            printLog(ScreenSaverService::class.java.name, "START SCREEN SAVER  SERVICE...")
            context.startService(
                Intent(
                    context.applicationContext,
                    ScreenSaverService::class.java
                )
            )
        }

        fun stop(context: Context) {
            printLog(
                ScreenSaverService::class.java.name,
                "STOP PUSH MERCHANT TRANSACTION LOG SERVICE..."
            )
            context.stopService(
                Intent(
                    context.applicationContext,
                    ScreenSaverService::class.java
                )
            )
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        printLog(TAG, "ScreenSaverService ::: OnStartCommand...")
        observeStartAndStopTimer()
        //countDownTimer.start()
        return super.onStartCommand(intent, flags, startId)
    }

    private fun observeStartAndStopTimer() {
        NiblMerchant.startScreenSaverTimer.observe(this, Observer {
            if (it) {
                printLog(TAG, "Screen Saver time is Started...", true)
                startTimer()
            } else {
                printLog(TAG, "Screen Saver time is Stopped...", true)
                stopTimer()
            }
        })

    }

    fun startTimer() {
        countDownTimer.cancel()
        countDownTimer.start()
    }

    fun stopTimer() {
        countDownTimer.cancel()
    }


    private fun onCountDownTick(millisUntilFinished: Long) {
        printLog(
            TAG,
            "Time left to show Screen Saver ::: "
                .plus(millisUntilFinished.getMinutesAndSecondsDataFromMillis()), true
        )
        printLog(
            TAG,
            "CountDown  ::: ".plus(countDownTimer), true
        )
        NiblMerchant.showScreenSaver.value = false
    }

    private fun onCountDownFinish() {
        NiblMerchant.showScreenSaver.value = true
        NiblMerchant.startScreenSaverTimer.value = false
    }

}