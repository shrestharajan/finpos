package global.citytech.finpos.processor.nibl.transaction.template;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.S2MActionCode;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
import global.citytech.finpos.processor.nibl.transaction.TransactionRequestModel;
import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finpos.processor.nibl.transaction.authorizer.NiblTransactionAuthorizer;
import global.citytech.finpos.processor.nibl.transaction.initializer.NiblTransactionInitializer;
import global.citytech.finpos.processor.nibl.transaction.receipt.NiblIsoMessageReceiptHandler;
import global.citytech.finpos.processor.nibl.transaction.receipt.NiblTransactionReceiptPrintHandler;
import global.citytech.finpos.processor.nibl.transaction.receipt.ReversedTransactionReceiptHandler;
import global.citytech.finpos.processor.nibl.transaction.receipt.TransactionReceiptHandler;
import global.citytech.finpos.processor.nibl.transaction.responsehandler.NiblInvalidResponseHandler;
import global.citytech.finpos.processor.nibl.transaction.reversal.ReversalRequestModel;
import global.citytech.finpos.processor.nibl.transaction.reversal.ReversalUseCase;
import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils;
import global.citytech.finpos.processor.nibl.transaction.validator.NiblTransactionValidator;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.led.LedAction;
import global.citytech.finposframework.hardware.io.led.LedLight;
import global.citytech.finposframework.hardware.io.led.LedRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.listeners.CardConfirmationListener;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.switches.transaction.TransactionRequester;
import global.citytech.finposframework.switches.transaction.TransactionResponseParameter;
import global.citytech.finposframework.usecases.MagneticSwipe;
import global.citytech.finposframework.usecases.PICCWave;
import global.citytech.finposframework.usecases.TransactionStatusFromHost;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandler;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.TransactionAuthorizer;
import global.citytech.finposframework.usecases.transaction.TransactionInitializer;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.usecases.transaction.TransactionValidator;
import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
import global.citytech.finposframework.usecases.transaction.data.Bin;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.usecases.transaction.data.TransactionAuthorizationResponse;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.JsonUtils;
import global.citytech.finposframework.utility.PosResponse;
import global.citytech.finposframework.utility.PosResult;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finposframework.utility.TMSConfigurationConstants.APPROVED_PRINT_ONLY;

/**
 * Created by Unique Shakya on 3/4/2021.
 */
public abstract class TransactionUseCaseTemplate implements UseCase<TransactionRequestModel, TransactionResponseModel>,
        TransactionRequester<TransactionRequestModel, TransactionResponseModel> {

    protected static final String APPROVED = "APPROVED";
    protected static final String DECLINED = "DECLINED";
    private static final String TIMEOUT = "TIMEOUT";

    protected final TerminalRepository terminalRepository;
    protected final Notifier notifier;
    protected final Logger logger;

    protected TransactionRequestModel request;
    protected TransactionIsoRequest transactionIsoRequest;
    protected String stan;
    protected String invoiceNumber;
    protected String batchNumber;
    protected TransactionRequest transactionRequest;
    protected TransactionValidator transactionValidator;
    protected ReadCardResponse readCardResponse;
    protected ReceiptLog receiptLog;
    protected String currencyCode;
    protected boolean shouldPrintReceipt;

    protected AutoReversal autoReversal;
    protected boolean isCardConfirmationRequired;
    protected TransactionResponseParameter transactionResponseModel;

    protected TransactionUseCaseTemplate(TerminalRepository terminalRepository, Notifier notifier) {
        this.terminalRepository = terminalRepository;
        this.notifier = notifier;
        this.logger = Logger.getLogger(getTransactionTypeClassName());
    }

    protected abstract String getTransactionTypeClassName();

    protected abstract void retrieveCardDetails(List<CardType> allowedCardTypes, boolean isForceContactCardPrompt);

    protected abstract void processCard();

    protected abstract TransactionIsoRequest prepareTransactionIsoRequest();

    protected abstract NIBLMessageSenderTemplate getRequestSender(RequestContext context);

    protected abstract void processOnlineResultToCard(IsoMessageResponse isoMessageResponse);

    protected abstract void processUnableToGoOnlineResultToCard();

    protected abstract boolean checkTransactionDeclinedByCard(IsoMessageResponse isoMessageResponse);

    protected abstract void notifyWithLed(boolean isApproved);

    protected abstract String getOriginalPosConditionCode();

    protected abstract PosEntryMode getOriginalPosEntryMode();

    protected abstract String getProcessingCode();

    protected abstract TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse,
                                                                           boolean isApproved,
                                                                           boolean shouldPrintCustomerCopy, String miniStatementData);

    protected abstract TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse,
                                                                           boolean isApproved,
                                                                           boolean shouldPrintCustomerCopy);


    protected abstract TransactionResponseModel prepareDeclinedTransactionResponse(String message,
                                                                                   boolean shouldPrint);

    protected abstract boolean isForceContactCardPromptActionCode(IsoMessageResponse isoMessageResponse);

    protected abstract String getOriginalTransactionReferenceNumber();

    protected abstract boolean isAuthorizationCompleted();

    protected abstract void updateTransactionLogOfOriginalTransaction(boolean isApproved);

    protected abstract boolean shouldIncrementInvoiceNumber(boolean isApproved);

    protected abstract String retrieveInvoiceNumber();

    protected abstract TransactionLog retrieveTransactionLogToStore(boolean isApproved, IsoMessageResponse isoMessageResponse);

    @Override
    public TransactionResponseModel execute(TransactionRequestModel request) {
        this.request = request;
        this.transactionRequest = this.request.getTransactionRequest();
        if (this.deviceNotReady())
            throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
        this.transactionValidator = new NiblTransactionValidator(this.request.getApplicationRepository());
        this.stan = this.terminalRepository.getSystemTraceAuditNumber();
        this.invoiceNumber = this.retrieveInvoiceNumber();
        this.batchNumber = this.terminalRepository.getReconciliationBatchNumber();
        try {
            this.initTransaction();
            return this.initializeTransactionWithCard(TransactionUtils.getAllowedCardEntryModes(this.transactionRequest.getTransactionType()), false);
        } catch (PosException pe) {
            pe.printStackTrace();
            throw pe;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } finally {
            this.cleanup();
        }
    }

    private TransactionResponseModel initializeTransactionWithCard(List<CardType> allowedCardEntryModes, boolean isForceContactCardPrompt) throws InterruptedException {
        this.retrieveCardDetails(allowedCardEntryModes, isForceContactCardPrompt);
        if (this.invalidTransaction())
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
        this.validateIfEmiTransaction(readCardResponse);
        if (this.isTransactionAuthorizationFailure()) {
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
        }
        if (isCardConfirmationRequired) {
            final boolean[] isConfirm = {false};
            CountDownLatch countDownLatch = new CountDownLatch(1);
            this.notifier.notify(Notifier.EventType.CARD_CONFIRMATION_EVENT,
                    readCardResponse.getCardDetails(),
                    (CardConfirmationListener) confirmed -> {
                        isConfirm[0] = confirmed;
                        countDownLatch.countDown();
                    });
            countDownLatch.await();
            if (!isConfirm[0]) {
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            }
        }
        this.processCard();
        this.notifier.notify(Notifier.EventType.TRANSMITTING_REQUEST, Notifier.EventType.TRANSMITTING_REQUEST.getDescription());
        return this.performTransactionWithHost();
    }

    protected void validateIfEmiTransaction(ReadCardResponse readCardResponse) {
        boolean isEmiTransaction = request.getTransactionRequest().isEmiTransaction();
        if (isEmiTransaction) {
            CardDetails cardDetails = readCardResponse.getCardDetails();
            String primaryAccountNumber = cardDetails.getPrimaryAccountNumber();
            List<Bin> transactionBins = request.getTransactionRepository().getTransactionBins();
            validateForAllowedBin(primaryAccountNumber, transactionBins);
        }
    }

    private void validateForAllowedBin(String primaryAccountNumber, List<Bin> transactionBins) {
        try {
            String binFromCard = primaryAccountNumber.substring(0, 6);
            for (Bin bin : transactionBins) {
                if (bin.getBin().equalsIgnoreCase(binFromCard) && bin.isActive()) {
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new PosException(PosError.DEVICE_ERROR_EMI_NOT_SUPPORTED_BY_CARD);
    }

    protected PurchaseRequest preparePurchaseRequest() {
        PurchaseRequest purchaseRequest = new PurchaseRequest(
                this.transactionRequest.getTransactionType(),
                this.transactionRequest.getAmount(),
                this.transactionRequest.getAdditionalAmount()
        );
        this.readCardResponse.getCardDetails().setAmount(this.transactionRequest.getAmount());
        this.readCardResponse.getCardDetails().setCashBackAmount(this.transactionRequest.getAdditionalAmount());
        purchaseRequest.setCardDetails(this.readCardResponse.getCardDetails());
        return purchaseRequest;
    }

    protected boolean shouldSkipPinBlockFromCard() {
        return this.readCardResponse.getCardDetails().getPinBlock().getOfflinePin() ||
                StringUtils.isEmpty(this.readCardResponse.getCardDetails().getPinBlock().getPin()) ||
                this.readCardResponse.getCardDetails().getPinBlock().getPin().equalsIgnoreCase("offline");
    }

    protected TransactionResponseModel performTransactionWithHost() {
        try {
            IsoMessageResponse isoMessageResponse = this.sendIsoMessage();
            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait..");
            InvalidResponseHandlerResponse invalidResponseHandlerResponse = this.checkForInvalidResponseFromHost(isoMessageResponse);
            if (invalidResponseHandlerResponse.isInvalid()) {
                this.request.getTransactionRepository().removeAutoReversal(this.autoReversal);
                return this.handleInvalidResponseFromHost(invalidResponseHandlerResponse);
            }

            boolean isApproved = this.isTransactionApprovedByActionCode(isoMessageResponse);
            this.processOnlineResultToCard(isoMessageResponse);
            boolean declinedByCard = this.checkTransactionDeclinedByCard(isoMessageResponse);
            if (declinedByCard) {
                this.request.getTransactionRepository().removeAutoReversal(this.autoReversal);
                return this.handleApprovedTransactionDeclinedByCard();
            }
            if (this.isForceContactCardPromptActionCode(isoMessageResponse)) {
                this.request.getTransactionRepository().removeAutoReversal(this.autoReversal);
                return this.handleForceContactCardPromptFromHost();
            }
            this.handleTransactionCompletion(isApproved, isoMessageResponse,Notifier.TransactionType.CARD.getDescription());
            this.request.getTransactionRepository().removeAutoReversal(this.autoReversal);
            boolean shouldPrintCustomerCopy = prepareReceiptLog(isoMessageResponse);
            this.updateTransactionLog(isApproved, isoMessageResponse);
            this.updateTransactionLogOfOriginalTransaction(isApproved);

            this.transactionResponseModel = this.prepareTransactionResponse(isoMessageResponse, isApproved, shouldPrintCustomerCopy);
            logger.log("transactionResponseModel >>> " + transactionResponseModel);
            String message = this.retrieveMessage(isoMessageResponse, isApproved);
            this.notifier.notify(Notifier.EventType.PUSH_MERCHANT_LOG, this.stan, message, isApproved);

            this.printReceipt(shouldPrintCustomerCopy);

            return this.prepareTransactionResponse(isoMessageResponse, isApproved, shouldPrintCustomerCopy);
        } catch (FinPosException fpe) {
            if (this.unableToGoOnlineException(fpe))
                return this.handleUnableToGoOnlineTransaction();
            else
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
    }

    protected TransactionResponseModel handleForceContactCardPromptFromHost() throws InterruptedException {
        this.updateTransactionIds(false);
        this.cleanup();
        this.stan = this.terminalRepository.getSystemTraceAuditNumber();
        this.batchNumber = this.terminalRepository.getReconciliationBatchNumber();
        this.invoiceNumber = this.terminalRepository.getInvoiceNumber();
        return this.initializeTransactionWithCard(Collections.singletonList(CardType.ICC), true);
    }

    protected void cleanup() {
        if (this.request.getReadCardService() != null)
            this.request.getReadCardService().cleanUp();
        if (this.request.getLedService() != null)
            this.request.getLedService().doTurnLedWith(new LedRequest(LedLight.ALL, LedAction.OFF));
        PICCWave.getInstance().clear();
        MagneticSwipe.getInstance().clear();
    }


    protected TransactionResponseModel handleUnableToGoOnlineTransaction() {
        this.notifier.notify(
                Notifier.EventType.TRANSACTION_DECLINED,
                TIMEOUT,
                Notifier.TransactionType.CARD.name());

        this.processUnableToGoOnlineResultToCard();
        ReasonForReversal reasonForReversal = new ReasonForReversal(ReasonForReversal.Type.TIMEOUT, ReasonForReversal.Type.TIMEOUT.getDescription());
        this.currencyCode = this.transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode();
        this.receiptLog = new ReversedTransactionReceiptHandler(this.transactionRequest,
                this.readCardResponse, this.terminalRepository)
                .prepare(this.batchNumber, this.stan, this.invoiceNumber, reasonForReversal);
        this.performReversal();
        boolean shouldPrintReceipt = this.printReversedTransactionReceipt(new ReasonForReversal(ReasonForReversal.Type.TIMEOUT, TIMEOUT));
        return this.prepareDeclinedTransactionResponse(TIMEOUT, shouldPrintReceipt);
    }

    protected boolean unableToGoOnlineException(FinPosException e) {
        return e.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT ||
                e.getType() == FinPosException.ExceptionType.READ_TIME_OUT ||
                e.getType() == FinPosException.ExceptionType.CONNECTION_ERROR;
    }

    protected String retrieveMessage(IsoMessageResponse isoMessageResponse, boolean isApproved) {
        if (isApproved)
            return this.approvedMessage(isoMessageResponse);
        else
            return this.declinedMessage(isoMessageResponse);
    }

    private String declinedMessage(IsoMessageResponse isoMessageResponse) {
        int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, DataElement.RESPONSE_CODE);
        if (actionCode < 0)
            throw new IllegalArgumentException("Invalid Action Code");
        String messageText = this.request.getApplicationRepository().getMessageTextByActionCode(actionCode);
        if (StringUtils.isEmpty(messageText))
            messageText = S2MActionCode.getByActionCode(actionCode).getDescription();
        return messageText;
    }

    private String approvedMessage(IsoMessageResponse isoMessageResponse) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(APPROVED);
        stringBuilder.append("\n");
        stringBuilder.append(
                IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse, DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE)
        );
        return stringBuilder.toString();
    }

    protected boolean prepareReceiptLog(IsoMessageResponse isoMessageResponse) {
        this.printDebugReceipt(isoMessageResponse);
        boolean approved = this.isTransactionApprovedByActionCode(isoMessageResponse);
        boolean approvePrintOnly = this.approvePrintOnly(this.request.getApplicationRepository());
        boolean shouldPrintReceipt = approved || !approvePrintOnly;
        this.currencyCode = this.transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode();
        this.receiptLog = new TransactionReceiptHandler(this.transactionRequest,
                this.readCardResponse, isoMessageResponse, this.terminalRepository,
                this.request.getApplicationRepository())
                .prepare(this.batchNumber, this.stan, this.invoiceNumber);
        return shouldPrintReceipt;
    }

    protected boolean printReceipt(boolean shouldPrintReceipt) {
        logger.log("::: TRANSACTION USE CASE ::: SHOULD PRINT RECEIPT == " + shouldPrintReceipt);
        if (shouldPrintReceipt) {
            ReceiptHandler.TransactionReceiptHandler transactionReceiptHandler =
                    new NiblTransactionReceiptPrintHandler(this.request.getPrinterService());
            this.request.getTransactionRepository().updateReceiptLog(this.receiptLog);
            transactionReceiptHandler.printTransactionReceipt(this.receiptLog, ReceiptVersion.MERCHANT_COPY);
        }
        return shouldPrintReceipt;
    }

    private boolean approvePrintOnly(ApplicationRepository applicationRepository) {
        String approvedPrintOnly = applicationRepository
                .retrieveFromAdditionalDataEmvParameters(APPROVED_PRINT_ONLY);
        logger.log("::: TRANSACTION USE CASE ::: APPROVED PRINT ONLY FROM EMV PARAMETERS === " + approvedPrintOnly);
        if (StringUtils.isEmpty(approvedPrintOnly))
            return false;
        return Boolean.parseBoolean(approvedPrintOnly);
    }

    private void printDebugReceipt(IsoMessageResponse isoMessageResponse) {
        if (this.terminalRepository.findTerminalInfo().isDebugModeEnabled()) {
            ReceiptHandler.IsoMessageReceiptHandler receiptHandler = new NiblIsoMessageReceiptHandler(this.request.getPrinterService());
            receiptHandler.printIsoMessageReceipt(isoMessageResponse);
        }
    }

    protected void handleTransactionCompletion(boolean isApproved, IsoMessageResponse isoMessageResponse, String transaction_type) {

        this.updateTransactionIds(shouldIncrementInvoiceNumber(isApproved));
        this.notifyToUi(isApproved, isoMessageResponse,transaction_type);
        this.notifyWithLed(isApproved);
    }

    private void notifyToUi(boolean isApproved, IsoMessageResponse isoMessageResponse,String transaction_type) {
        if(transaction_type == Notifier.TransactionType.CARD.getDescription()){
            if (isApproved)
                this.notifier.notify(
                        Notifier.EventType.TRANSACTION_APPROVED,
                        this.approvedMessage(isoMessageResponse),
                        Notifier.TransactionType.CARD.name());

            else
                this.notifier.notify(
                        Notifier.EventType.TRANSACTION_DECLINED,
                        this.declinedMessage(isoMessageResponse),
                        Notifier.TransactionType.CARD.name()
                );
        }else{
            if (isApproved)
                this.notifier.notify(
                        Notifier.EventType.TRANSACTION_APPROVED,
                        this.approvedMessage(isoMessageResponse),
                        Notifier.TransactionType.VOID.name());

            else
                this.notifier.notify(
                        Notifier.EventType.TRANSACTION_DECLINED,
                        this.declinedMessage(isoMessageResponse),
                        Notifier.TransactionType.VOID.name()
                );
        }
    }

    protected void updateTransactionIds(boolean incrementInvoiceNumber) {
        this.terminalRepository.incrementSystemTraceAuditNumber();
        this.terminalRepository.incrementRetrievalReferenceNumber();
        if (incrementInvoiceNumber)
            this.terminalRepository.incrementInvoiceNumber();
    }

    protected void updateTransactionLog(boolean isApproved, IsoMessageResponse isoMessageResponse) {
        TransactionLog transactionLog = this.retrieveTransactionLogToStore(isApproved, isoMessageResponse);
        this.request.getTransactionRepository().updateTransactionLog(transactionLog);
    }

    protected String retrieveTransactionTime(IsoMessageResponse isoMessageResponse) {
        String transactionTime = IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.LOCAL_TIME);
        if (StringUtils.isEmpty(transactionTime))
            transactionTime = IsoMessageUtils.retrieveFromRequestDataElementAsString(isoMessageResponse, DataElement.LOCAL_TIME);
        return transactionTime;
    }

    protected String retrieveTransactionDate(IsoMessageResponse isoMessageResponse) {
        String transactionDate = IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.LOCAL_DATE);
        if (StringUtils.isEmpty(transactionDate))
            transactionDate = IsoMessageUtils.retrieveFromRequestDataElementAsString(isoMessageResponse, DataElement.LOCAL_DATE);
        return transactionDate;
    }


    protected TransactionResponseModel handleApprovedTransactionDeclinedByCard() {
        this.notifier.notify(
                Notifier.EventType.TRANSACTION_DECLINED,
                ReasonForReversal.Type.DECLINED_BY_CARD.getDescription(),
                Notifier.TransactionType.CARD.name());


        ReasonForReversal reasonForReversal = new ReasonForReversal(ReasonForReversal.Type.DECLINED_BY_CARD, ReasonForReversal.Type.DECLINED_BY_CARD.getDescription());
        this.currencyCode = this.transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode();
        this.receiptLog = new ReversedTransactionReceiptHandler(this.transactionRequest,
                this.readCardResponse, this.terminalRepository)
                .prepare(this.batchNumber, this.stan, this.invoiceNumber, reasonForReversal);

        this.performReversal();
        boolean shouldPrintReceipt = this.printReversedTransactionReceipt(new ReasonForReversal(ReasonForReversal.Type.DECLINED_BY_CARD,
                ReasonForReversal.Type.DECLINED_BY_CARD.getDescription()));
        return this.prepareDeclinedTransactionResponse(ReasonForReversal.Type.DECLINED_BY_CARD.getDescription(), shouldPrintReceipt);
    }

    protected boolean isTransactionApprovedByActionCode(IsoMessageResponse isoMessageResponse) {
        int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse,
                DataElement.RESPONSE_CODE);
        return S2MActionCode.getByActionCode(actionCode).getTransactionStatusFromHost() == TransactionStatusFromHost.ONLINE_APPROVED;
    }

    protected TransactionResponseModel handleInvalidResponseFromHost(InvalidResponseHandlerResponse invalidResponseHandlerResponse) {
        this.notifier.notify(
                Notifier.EventType.TRANSACTION_DECLINED,
                invalidResponseHandlerResponse.getMessage(),
                Notifier.TransactionType.CARD.name()
        );

        ReasonForReversal reasonForReversal = new ReasonForReversal(ReasonForReversal.Type.INVALID_RESPONSE,
                invalidResponseHandlerResponse.getMessage());
        this.receiptLog = new ReversedTransactionReceiptHandler(this.transactionRequest,
                this.readCardResponse, this.terminalRepository)
                .prepare(this.batchNumber, this.stan, this.invoiceNumber, reasonForReversal);
        this.performReversal();
        boolean shouldPrint = this.printReversedTransactionReceipt(reasonForReversal);
        return this.prepareDeclinedTransactionResponse(invalidResponseHandlerResponse.getMessage(),
                shouldPrint);
    }

    private boolean printReversedTransactionReceipt(ReasonForReversal reasonForReversal) {
        boolean shouldPrint = !this.approvePrintOnly(this.request.getApplicationRepository());
        if (shouldPrint) {
//            this.receiptLog = new ReversedTransactionReceiptHandler(this.transactionRequest,
//                    this.readCardResponse, this.terminalRepository)
//                    .prepare(this.batchNumber, this.stan, this.invoiceNumber, reasonForReversal);
            new NiblTransactionReceiptPrintHandler(this.request.getPrinterService())
                    .printTransactionReceipt(this.receiptLog, ReceiptVersion.MERCHANT_COPY);
            this.request.getTransactionRepository().updateReceiptLog(this.receiptLog);
        }
        return shouldPrint;
    }

    private void performReversal() {
        ReversalUseCase reversalUseCase = new ReversalUseCase(this.prepareReversalRequest(),
                this.terminalRepository, this.notifier);
        try {
            reversalUseCase.execute(this.prepareReversalRequestModel());
        } catch (FinPosException fpe) {
            fpe.printStackTrace();
        }
    }

    private ReversalRequestModel prepareReversalRequestModel() {
        return ReversalRequestModel.Builder.newInstance()
                .withApplicationRepository(this.request.getApplicationRepository())
                .withPrinterService(this.request.getPrinterService())
                .withReadCardResponse(this.readCardResponse)
                .withTransactionRepository(this.request.getTransactionRepository())
                .withTransactionRequest(this.transactionRequest)
                .withReceiptLog(this.receiptLog)
                .withCurrencyCode(this.currencyCode)
                .build();
    }

    protected InvalidResponseHandlerResponse checkForInvalidResponseFromHost(IsoMessageResponse isoMessageResponse) {
        InvalidResponseHandler invalidResponseHandler = new NiblInvalidResponseHandler();
        return invalidResponseHandler.isInvalidResponse(isoMessageResponse);
    }

    private IsoMessageResponse sendIsoMessage() {
        RequestContext context = this.prepareTemplateRequestContext();
        this.transactionIsoRequest = this.prepareTransactionIsoRequest();
        context.setRequest(transactionIsoRequest);
        NIBLMessageSenderTemplate sender = this.getRequestSender(context);
        this.autoReversal = new AutoReversal(this.transactionRequest.getTransactionType(), JsonUtils.toJsonObj(prepareReversalRequest()),
                0, AutoReversal.Status.ACTIVE, StringUtils.dateTimeStamp(), readCardResponse);
        this.request.getTransactionRepository().saveAutoReversal(this.autoReversal);
        return sender.send();
    }

    protected RequestContext prepareTemplateRequestContext() {
        HostInfo primaryHostInfo = this.terminalRepository.findPrimaryHost();
        HostInfo secondaryHostInfo = this.terminalRepository.findSecondaryHost();
        TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
        ConnectionParam connectionParam = new ConnectionParam(primaryHostInfo, secondaryHostInfo);
        return new RequestContext(this.stan, terminalInfo, connectionParam);
    }

    protected boolean isTransactionAuthorizationFailure() {
        TransactionAuthorizer transactionAuthorizer = new NiblTransactionAuthorizer(this.request.getTransactionAuthenticator(),
                this.request.getApplicationRepository());
        TransactionAuthorizationResponse transactionAuthorizationResponse = transactionAuthorizer
                .authorizeUser(this.transactionRequest.getTransactionType(),
                        this.readCardResponse.getCardDetails().getCardScheme().getCardSchemeId());
        return !transactionAuthorizationResponse.isSuccess();
    }

    protected boolean invalidTransaction() {
        return !this.transactionValidator.isValidTransaction(this.transactionRequest, this.readCardResponse,
                this.readCardResponse.getCardDetails().getCardScheme().getCardSchemeId());
    }

    protected void initTransaction() {
        TransactionInitializer transactionInitializer = new NiblTransactionInitializer(this.request.getApplicationRepository());
        TransactionRequest afterInitTransactionRequest = transactionInitializer.initialize(this.transactionRequest);
        this.transactionValidator.validateRequest(afterInitTransactionRequest);
    }

    protected boolean deviceNotReady() {
        PosResponse deviceReadyResponse = this.request.getDeviceController().isReady(TransactionUtils
                .getAllowedCardEntryModes(this.transactionRequest.getTransactionType()));
        if (deviceReadyResponse.getPosResult() == PosResult.SUCCESS) {
            return false;
        }
        throw new PosException(deviceReadyResponse.getPosError());
    }

    private NiblReversalRequest prepareReversalRequest() {
        NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
        niblReversalRequest.setAmount(this.transactionRequest.getAmount());
        niblReversalRequest.setExpiryDate(this.readCardResponse.getCardDetails().getExpiryDate());
        niblReversalRequest.setInvoiceNumber(this.invoiceNumber);
        niblReversalRequest.setNii("0011");
        niblReversalRequest.setPan(this.readCardResponse.getCardDetails().getPrimaryAccountNumber());
        niblReversalRequest.setPinBlock(this.readCardResponse.getCardDetails().getPinBlock().getPin());
        niblReversalRequest.setPosConditionCode(this.transactionIsoRequest.getPosConditionCode());
        niblReversalRequest.setPosEntryMode(this.transactionIsoRequest.getPosEntryMode());
        niblReversalRequest.setProcessingCode(this.getProcessingCode());
        niblReversalRequest.setStan(this.stan);
        niblReversalRequest.setTrack2Data(this.readCardResponse.getCardDetails().getTrackTwoData());
        niblReversalRequest.setLocalDate(DateUtils.yymmDate());
        niblReversalRequest.setLocalTime(DateUtils.HHmmssTime());
        niblReversalRequest.setEmvData(this.readCardResponse.getCardDetails().getIccDataBlock() == null ? "" : this.readCardResponse.getCardDetails().getIccDataBlock());
        return niblReversalRequest;
    }
}
