package global.citytech.finpos.device.weipass.emvparam

import android.content.Context
import com.wiseasy.emvprocess.SDKInstance
import com.wiseasy.emvprocess.TransProcess
import com.wiseasy.emvprocess.bean.TerminalSetting
import global.citytech.finpos.device.weipass.utils.WeipassDeviceUtils
import global.citytech.finpos.device.weipass.utils.WeipassConstant
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finpos.device.weipass.utils.defaultOrEmptyValue
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest
import sdk4.wangpos.libemvbinder.EmvParam

/**
 * Created by Rishav Chudal on 4/22/20.
 */
class WeipassEmvParametersServiceImpl constructor(private val context: Context) :
    global.citytech.finposframework.hardware.emv.emvparam.EmvParametersService {
    private lateinit var emvParametersRequest: EmvParametersRequest
    private lateinit var deviceResponse: DeviceResponse

    override fun injectEmvParameters(emvParametersRequest: EmvParametersRequest): DeviceResponse {
        deviceResponse = DeviceResponse(Result.FAILURE, WeipassConstant.FAILURE_EMV_PARAM_INJECT)
        this.emvParametersRequest = emvParametersRequest
        if (emvParamInjectResult() == 0) {
            deviceResponse = DeviceResponse(Result.SUCCESS, WeipassConstant.SUCCESS_EMV_PARAM_INJECT)
        }
        return deviceResponse
    }

    private fun emvParamInjectResult(): Int {
        var result = -1
        val emvParam: EmvParam? = TransProcess.getEMVParamFromTerminalSetting(
            prepareTerminalSetting(),
            WeipassConstant.TRANS_TYPE_SALE, context
        )
        emvParam?.let { result = injectEmvParamInCore(emvParam) }
            ?: kotlin.run { WeipassLogger.log("Emv Param is Null") }

        return result
    }

    private fun prepareTerminalSetting(): TerminalSetting {
        val terminalSetting = TerminalSetting(context)

        val mcc = emvParametersRequest.merchantCategoryCode.defaultOrEmptyValue()
        terminalSetting.mcC_9F15 = mcc
        WeipassLogger.log("Merchant Category Code ::: ".plus(mcc))

        val merchantId = emvParametersRequest.merchantIdentifier.defaultOrEmptyValue()
        terminalSetting.merchantId_9F16 = merchantId
        WeipassLogger.log("Merchant Id ::: ".plus(merchantId))

        val terminalId = emvParametersRequest.terminalId.defaultOrEmptyValue()
        terminalSetting.terminalId_9F1C = terminalId
        WeipassLogger.log("Terminal Id ::: ".plus(terminalId))

        val merchantName = emvParametersRequest.merchantName.defaultOrEmptyValue()
        terminalSetting.merchantName = merchantName
        WeipassLogger.log("Merchant Name ::: ".plus(merchantName))

        val terminalType = emvParametersRequest.terminalType.defaultOrEmptyValue()
        terminalSetting.terminalType_9F35 = terminalType
        WeipassLogger.log("Terminal Type ::: ".plus(terminalType))

        val terminalCapabilities = emvParametersRequest.terminalCapabilities
            .defaultOrEmptyValue()
        terminalSetting.terminalCap_9F33 = terminalCapabilities
        WeipassLogger.log("Terminal Capabilities ::: ".plus(terminalCapabilities))

        val addTerminalCapabilites = emvParametersRequest.additionalTerminalCapabilities
            .defaultOrEmptyValue()
        terminalSetting.exTerminalCap_9F40 = addTerminalCapabilites
        WeipassLogger.log("Additional Terminal Capabilities ::: ".plus(addTerminalCapabilites))

        val countryCode = emvParametersRequest.terminalCountryCode.defaultOrEmptyValue()
        terminalSetting.countryCode_9F1A = countryCode
        WeipassLogger.log("Terminal Country Code ::: ".plus(countryCode))

        val transCurrCode = emvParametersRequest.transactionCurrencyCode
            .defaultOrEmptyValue()
        terminalSetting.transCurrencyCode_5F2A = transCurrCode
        WeipassLogger.log("Transaction Currency Code ::: ".plus(transCurrCode))

        val transCurrExp = emvParametersRequest.transactionCurrencyExponent
            .defaultOrEmptyValue()
        terminalSetting.transCurrencyExp = transCurrExp
        WeipassLogger.log("Transaction Currency Exponent ::: ".plus(transCurrExp))

        val referTransCurrCode = emvParametersRequest.transactionCurrencyCode
            .defaultOrEmptyValue()
        terminalSetting.referCurrencyCode_9F3C = referTransCurrCode
        WeipassLogger.log("Refer Transaction Currency Code ::: ".plus(referTransCurrCode))

        val referTransCurrExp = emvParametersRequest.transactionCurrencyExponent
            .defaultOrEmptyValue()
        terminalSetting.referCurrencyExp_9F3D = referTransCurrExp
        WeipassLogger.log("Refer Transaction Currency Exponent ::: ".plus(referTransCurrExp))

        val displayOfflinePINRetryTime = "1"
        terminalSetting.displayOfflinePINRetryTime = displayOfflinePINRetryTime
        WeipassLogger.log("Display Offline PIN Retry Time ::: ".plus(displayOfflinePINRetryTime))

        val forceOnlineFlag = emvParametersRequest.forceOnlineFlag.defaultOrEmptyValue()
        terminalSetting.forceOnline = forceOnlineFlag
        WeipassLogger.log("Force Online Flag ::: ".plus(forceOnlineFlag))

        val ttq = emvParametersRequest.ttq.defaultOrEmptyValue()
        terminalSetting.ttQ_9F66 = ttq
        WeipassLogger.log("Terminal Transaction Qualifier (TTQ) ::: ".plus(ttq))
        return terminalSetting
    }

    private fun injectEmvParamInCore(emvParam: EmvParam): Int {
        var result = -1
        try {
            SDKInstance.mEmvCore.setParam(emvParam.toByteArray())
            result = 0
        } catch (ex: Exception) {
            WeipassLogger.log("Exception in injectEmvParamInCore ::: ".plus(ex.message))
        }
        WeipassLogger.log("Emv Param Injection Result ::: ".plus(result))
        return result
    }
    
    fun prepareTerminalSettingWith(
        readCardRequest: ReadCardRequest,
        cardType: CardType
        ) : TerminalSetting {
        val terminalSetting = TerminalSetting(context)

        val mcc = readCardRequest.emvParametersRequest.merchantCategoryCode.defaultOrEmptyValue()
        terminalSetting.mcC_9F15 = mcc
        WeipassLogger.log("Merchant Category Code ::: ".plus(mcc))

        val merchantId = readCardRequest.emvParametersRequest.merchantIdentifier.defaultOrEmptyValue()
        terminalSetting.merchantId_9F16 = merchantId
        WeipassLogger.log("Merchant Id ::: ".plus(merchantId))

        val terminalId = readCardRequest.emvParametersRequest.terminalId.defaultOrEmptyValue()
        terminalSetting.terminalId_9F1C = terminalId
        WeipassLogger.log("Terminal Id ::: ".plus(terminalId))

        val merchantName = readCardRequest.emvParametersRequest.merchantName.defaultOrEmptyValue()
        terminalSetting.merchantName = merchantName
        WeipassLogger.log("Merchant Name ::: ".plus(merchantName))

        val terminalType = readCardRequest.emvParametersRequest.terminalType.defaultOrEmptyValue()
        terminalSetting.terminalType_9F35 = terminalType
        WeipassLogger.log("Terminal Type ::: ".plus(terminalType))

        val terminalCapabilities = readCardRequest.emvParametersRequest.terminalCapabilities
            .defaultOrEmptyValue()
        terminalSetting.terminalCap_9F33 = terminalCapabilities
        WeipassLogger.log("Terminal Capabilities ::: ".plus(terminalCapabilities))

        val addTerminalCapabilites = readCardRequest.emvParametersRequest.additionalTerminalCapabilities
            .defaultOrEmptyValue()
        terminalSetting.exTerminalCap_9F40 = addTerminalCapabilites
        WeipassLogger.log("Additional Terminal Capabilities ::: ".plus(addTerminalCapabilites))

        val countryCode = readCardRequest.emvParametersRequest.terminalCountryCode.defaultOrEmptyValue()
        terminalSetting.countryCode_9F1A = countryCode
        WeipassLogger.log("Terminal Country Code ::: ".plus(countryCode))

        val transCurrCode = readCardRequest.emvParametersRequest.transactionCurrencyCode
            .defaultOrEmptyValue()
        terminalSetting.transCurrencyCode_5F2A = transCurrCode
        WeipassLogger.log("Transaction Currency Code ::: ".plus(transCurrCode))

        val transCurrExp = readCardRequest.emvParametersRequest.transactionCurrencyExponent
            .defaultOrEmptyValue()
        terminalSetting.transCurrencyExp = transCurrExp
        WeipassLogger.log("Transaction Currency Exponent ::: ".plus(transCurrExp))

        val referTransCurrCode = readCardRequest.emvParametersRequest.transactionCurrencyCode
            .defaultOrEmptyValue()
        terminalSetting.referCurrencyCode_9F3C = referTransCurrCode
        WeipassLogger.log("Refer Transaction Currency Code ::: ".plus(referTransCurrCode))

        val referTransCurrExp = readCardRequest.emvParametersRequest.transactionCurrencyExponent
            .defaultOrEmptyValue()
        terminalSetting.referCurrencyExp_9F3D = referTransCurrExp
        WeipassLogger.log("Refer Transaction Currency Exponent ::: ".plus(referTransCurrExp))

        val displayOfflinePINRetryTime = "1"
        terminalSetting.displayOfflinePINRetryTime = displayOfflinePINRetryTime
        WeipassLogger.log("Display Offline PIN Retry Time ::: ".plus(displayOfflinePINRetryTime))

        val ttq = readCardRequest.emvParametersRequest.ttq.defaultOrEmptyValue()
        terminalSetting.ttQ_9F66 = ttq
        WeipassLogger.log("Terminal Transaction Qualifier (TTQ) ::: ".plus(ttq))

        var forceOnlineFlag: Boolean
        if (cardType == CardType.ICC) {
            forceOnlineFlag = readCardRequest.forceOnlineForICC!!
        } else {
            forceOnlineFlag = readCardRequest.forceOnlineForPICC!!
        }
        terminalSetting.forceOnline = WeipassDeviceUtils.getBooleanToBinaryAndViceVersa(forceOnlineFlag)
        WeipassLogger.log("Force Online Flag ::: ".plus(forceOnlineFlag))

        return terminalSetting
    }
}