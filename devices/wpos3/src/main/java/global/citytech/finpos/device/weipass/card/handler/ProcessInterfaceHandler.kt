package global.citytech.finpos.device.weipass.card.handler

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import com.wiseasy.emvprocess.bean.OnlineResult
import com.wiseasy.emvprocess.bean.PINSetting
import com.wiseasy.emvprocess.bean.PinPadConfig
import com.wiseasy.emvprocess.bean.TransAmount
import com.wiseasy.emvprocess.interfaces.ProcessInterface
import global.citytech.common.Constants
import global.citytech.common.data.PosCallback
import global.citytech.common.data.PosMessage
import global.citytech.finpos.device.weipass.card.WeipassPinPad
import global.citytech.finpos.device.weipass.led.WeipassLedServiceImpl
import global.citytech.finpos.device.weipass.sound.WeipassSoundServiceImpl
import global.citytech.finpos.device.weipass.utils.*
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.CardDetails
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.hardware.io.led.LedAction
import global.citytech.finposframework.hardware.io.led.LedLight
import global.citytech.finposframework.hardware.io.led.LedRequest
import global.citytech.finposframework.hardware.io.sound.Sound
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.atomic.AtomicInteger

/**
 * Created by Rishav Chudal on 6/4/20.
 */
class ProcessInterfaceHandler: ProcessInterface {
    private var weipassLedServiceImpl: WeipassLedServiceImpl = WeipassLedServiceImpl()
    private lateinit var weipassSoundServiceImpl: WeipassSoundServiceImpl
    private var contextWeakReference: WeakReference<Context>? = null
    private var weipassPinPad: WeipassPinPad = WeipassPinPad()

    /**
     * fun ConfigTransTPK
     */
    override fun ConfigTransTPK(): PINSetting {
        WeipassLogger.log("ProcessInterfaceHandler ::: ConfigTransTPK")
        val pinSetting = PINSetting()
        pinSetting.packageName = WeipassStateHandler.readCardRequest!!.packageName
        pinSetting.isBypass = false
        pinSetting.minPINLength = WeipassDeviceUtils.getMinmPinLengthOrDefault(
            WeipassStateHandler.readCardRequest!!.minmPinLength
        )
        pinSetting.maxPINLength = WeipassDeviceUtils.getMaxmPinLengthOrDefault(
            WeipassStateHandler.readCardRequest!!.maxmPinLength
        )
        pinSetting.timeOut = WeipassDeviceUtils.getPinpadTimeoutOrDefault(
            WeipassStateHandler.readCardRequest!!.pinpadTimeout
        )
        pinSetting.pinBlockFormat = WeipassDeviceUtils.getPinBlockFormatOrDefault(
            WeipassStateHandler.readCardRequest!!.pinBlockFormat
        )
        pinSetting.isDUKPT = false
        pinSetting.isClearButtonNotCannel = false
        pinSetting.isAutoComfirm = false
        pinSetting.isFixLayout = WeipassStateHandler.readCardRequest!!.pinPadFixedLayout
        pinSetting.pan = EmvUtils.retrievePAN()
        return pinSetting
    }

    /**
     * func setTransAmount
     */
    override fun setTransAmount(): TransAmount? {
        WeipassLogger.log("ProcessInterfaceHandler ::: setTransAmount")
        WeipassStateHandler.initKernelCountDownLatch()
        prepareReadCardResponseForICCAmountCallback()
        WeipassStateHandler.releaseServiceCountdownLatch()
        WeipassStateHandler.awaitKernelCountDownLatch()
        WeipassLogger.log("ProcessInterfaceHandler ::: setTransAmount ::: After latch release")
        WeipassLogger.log("ProcessInterfaceHandler ::: setTransAmount ::: Amount ::: "
            .plus(WeipassStateHandler.readCardRequest!!.amount))
        return WeipassDeviceUtils.prepareTransAmount(
            WeipassStateHandler.readCardRequest!!.amount,
            WeipassStateHandler.readCardRequest!!.cashBackAmount!!,
            WeipassStateHandler.readCardRequest!!.transactionType
        )
    }

    private fun prepareReadCardResponseForICCAmountCallback() {
        val cardDetails =
            CardDetails()
        cardDetails.cardType = WeipassStateHandler.CURRENT_CARD_TYPE
        val readCardResponse =
            ReadCardResponse(
                cardDetails,
                Result.CALLBACK_AMOUNT,
                WeipassConstant.MSG_CALLBACK_AMOUNT
            )
        WeipassStateHandler.readCardResponse = readCardResponse
    }

    /**
     * func processOnline
     */
    override fun processOnline(): OnlineResult{
        WeipassLogger.log("ProcessInterfaceHandler ::: processOnline")
        weipassLedServiceImpl.doTurnLedWith(
            LedRequest(
                LedLight.ALL,
                LedAction.OFF
            )
        )
        prepareForBroadCastToUI()
        WeipassStateHandler.initKernelCountDownLatch()
        WeipassStateHandler.isOnlineProcess = true
        prepareReadCardResponseForOnlineProcess()
        WeipassStateHandler.releaseServiceCountdownLatch()
        WeipassStateHandler.awaitKernelCountDownLatch()
        return WeipassStateHandler.onlineResult!!
    }

    private fun prepareForBroadCastToUI() {
        val intentActionName = Constants.INTENT_ACTION_POS_CALLBACK
        val intentPutExtraName = Constants.KEY_BROADCAST_MESSAGE
        val posCallback = PosCallback(PosMessage.POS_MESSAGE_PROCESSING)
    }

    private fun prepareReadCardResponseForOnlineProcess() {
        val cardDetails =
            CardDetails()
        EmvUtils.prepareCardDetailsForTransaction(cardDetails)
        cardDetails.transactionInitializeDateTime = SimpleDateFormat("yyMMddHHmmssSSS",
            Locale.getDefault())
            .format(Date())
        val readCardResponse =
            ReadCardResponse(
                cardDetails,
                Result.SUCCESS,
                WeipassConstant.MSG_READ_CARD_SUCCESS
            )
        readCardResponse.piccCvm = EmvUtils
            .checkForPICCCvm(WeipassStateHandler.CURRENT_CARD_TYPE!!)
        WeipassStateHandler.readCardResponse = readCardResponse
    }

    /**
     * func  processAdvice
     */
    override fun processAdvice(bytes: ByteArray) {
        WeipassLogger.log("ProcessInterfaceHandler ::: processAdvice")
    }

    /**
     * func displayOfflinePinVerifyResult
     */
    override fun displayOfflinePinVerifyResult() {
        WeipassLogger.log("ProcessInterfaceHandler ::: displayOfflinePinVerifyResult")
        WeipassLogger.log("ProcessInterfaceHandler ::: Pin Offline Verified")
    }

    /**
     * func processMutilAID
     */
    override fun processMutilAID(aidList: Array<String>): Int {
        WeipassLogger.log("ProcessInterfaceHandler ::: processMultiAID")
        for (aid in aidList) {
            WeipassLogger.debug("Aid ::: $aid")
        }
        val cardType: CardType = WeipassStateHandler.CURRENT_CARD_TYPE!!
        var selectedAidIndex = 0
        if (cardType === CardType.PICC) {
            selectedAidIndex = this.getAidIndexFromMultipleSelection(aidList)
        } else if (cardType === CardType.ICC) {
            selectedAidIndex = this.getAidIndexFromMultipleSelection(aidList)
            WeipassLogger.debug("Selected Aid Index::: $selectedAidIndex")
            WeipassLogger.debug("Selected Aid ::: " + aidList[selectedAidIndex])
        }
        return selectedAidIndex
    }

    private fun getAidIndexFromMultipleSelection(aidList: Array<String>): Int {
        WeipassLogger.log("getAidIndexFromMultipleSelection")
        contextWeakReference = WeipassStateHandler.contextWeakReference
        val index: Int = WeipassStateHandler
            .readCardRequest!!
            .multipleAidSelectionListener!!
            .onMultipleAid(aidList)
        WeipassLogger.log("getAidIndexFromMultipleSelection ::: $index")
        if (index >= 0) {
            return index
        } else {
            return proceedToShowAidListInUi(aidList)
        }
    }

    private fun proceedToShowAidListInUi(aidList: Array<String>): Int {
        if (contextWeakReference != null) {
            val countDownLatch = CountDownLatch(1)
            val selectItem = AtomicInteger(0)
            (contextWeakReference!!.get()!! as Activity)
                .runOnUiThread {
                    displayAidListInAlertDialog(aidList, countDownLatch, selectItem)
                }
            try {
                countDownLatch.await()
            } catch (ex: Exception) {
                WeipassLogger.log("Exception ::: ".plus(ex.message))
            }
            return selectItem.get()

        } else {
            throw Exception(WeipassConstant.MSG_READ_CARD_FAIL)
        }
    }

    private fun displayAidListInAlertDialog(
        aidList: Array<String>,
        countDownLatch: CountDownLatch,
        selectItem: AtomicInteger
    ) {
        val alertDialog = AlertDialog.Builder(contextWeakReference!!.get())
            .setSingleChoiceItems(
                aidList, 0
            ) { dialog: DialogInterface, which: Int ->
                WeipassLogger.log("::: SELECTED INDEX OF AID ::: $which")
                WeipassLogger.log("::: SELECTED AID ::: " + aidList[which])
                selectItem.set(which)
                dialog.dismiss()
                countDownLatch.countDown()
            }
            .setNegativeButton("Cancel") { dialog: DialogInterface, which: Int ->
                selectItem.set(-1)
                onCancellingSelApplication(dialog, countDownLatch)
            }
            .setCancelable(false)
            .create()

        WeipassStateHandler.alertDialogWeakReference = WeakReference(alertDialog)
        if (WeipassStateHandler.alertDialogWeakReference != null) {
            WeipassStateHandler.alertDialogWeakReference!!.get()!!.show()
        } else {
            WeipassStateHandler.transState = WeipassTransState.PROCESSING_ERROR
            WeipassDeviceUtils.callForSdkBreakOffCommand(contextWeakReference!!.get()!!.applicationContext)
        }
    }

    private fun onCancellingSelApplication(
        dialog: DialogInterface,
        countDownLatch: CountDownLatch
    ) {
        dialog.dismiss()
        countDownLatch.countDown()
        WeipassStateHandler.transState = WeipassTransState.SELECT_APPLICATION_CANCELLED
        WeipassDeviceUtils.callForSdkBreakOffCommand(
            contextWeakReference!!.get()!!.applicationContext
        )
    }

    /**
     * func displayPan
     */
    override fun displayPan(pan: String){
        WeipassLogger.log("ProcessInterfaceHandler ::: displayPan")
        contextWeakReference = WeipassStateHandler.contextWeakReference
        if (!pan.isNullOrEmptyOrBlank()) {
            updateCardSummary()
            this.validateCardInICCTransaction(pan)
            this.turnLedAndPlaySound(LedLight.ALL, LedAction.ON, Sound.SUCCESS)
            this.turnLedAndPlaySound(LedLight.ALL, LedAction.OFF, null)
        } else {
            WeipassLogger.debug("OutCome Msg ::: " + EmvUtils.getOutComeDF8129Msg())
            this.prepareForCardReadFailure()
        }
    }

    private fun validateCardInICCTransaction(currentPan: String) {
        WeipassLogger.log("Validating Card in ICC Transaction")
        if (WeipassStateHandler.CURRENT_CARD_TYPE == CardType.ICC) {
            val earlierPan = WeipassStateHandler.readCardRequest!!.cardDetailsBeforeEmvNext!!.primaryAccountNumber
            if (!earlierPan.equals(currentPan, ignoreCase = true)) {
                this.prepareForCardReadFailure()
            }
        }
    }

    private fun turnLedAndPlaySound(ledLight: LedLight, ledAction: LedAction, sound: Sound?) {
        weipassSoundServiceImpl = WeipassSoundServiceImpl(
            contextWeakReference!!.get()!!.applicationContext
        )
        if (WeipassStateHandler.CURRENT_CARD_TYPE === CardType.PICC) {
            weipassLedServiceImpl.doTurnLedWith(
                LedRequest(
                    ledLight,
                    ledAction
                )
            )
            this.weipassSoundServiceImpl.playSound(sound)
        }
    }

    private fun updateCardSummary() {
        WeipassStateHandler.cardSummary = EmvUtils.prepareCardSummary()
    }

    private fun prepareForCardReadFailure() {
        contextWeakReference = WeipassStateHandler.contextWeakReference
        WeipassStateHandler.transState = WeipassTransState.FAILURE
        WeipassDeviceUtils.callForSdkBreakOffCommand(
            contextWeakReference!!.get()!!.applicationContext
        )
        throw Exception(WeipassConstant.INVALID_CARD)
    }

    /**
     * func onAmexOutCome
     */
    override fun onAmexOutCome(bytes: ByteArray) {
        WeipassLogger.log("ProcessInterfaceHandler ::: onAmexOutCome")
        contextWeakReference = WeipassStateHandler.contextWeakReference
    }

    /**
     * func doAfterCardDataReading
     */
    override fun doAfterCardDataReading(): Int {
        WeipassLogger.log("ProcessInterfaceHandler ::: doAfterCardDataReading")
        contextWeakReference = WeipassStateHandler.contextWeakReference
        WeipassStateHandler.initKernelCountDownLatch()
        this.prepareReadCardResponseAfterCardDataReadCallback()
        WeipassStateHandler.releaseServiceCountdownLatch()
        WeipassStateHandler.awaitKernelCountDownLatch()
        WeipassLogger.log("ProcessInterfaceHandler ::: doAfterCardDataReading ::: After latch release")
        WeipassLogger.log("ProcessInterfaceHandler ::: doAfterCardDataReading ::: halt emv process ::: " + WeipassStateHandler.haltEmvProcess)
        return if (WeipassStateHandler.haltEmvProcess) 1 else 0
    }

    private fun prepareReadCardResponseAfterCardDataReadCallback() {
        val cardDetails =
            CardDetails()
        cardDetails.cardType = WeipassStateHandler.CURRENT_CARD_TYPE
        cardDetails.amount = WeipassStateHandler.readCardRequest!!.amount
        cardDetails.cashBackAmount = WeipassStateHandler.readCardRequest!!.cashBackAmount
        EmvUtils.prepareBasicEmvDataForCardDetails(cardDetails)
        val readCardResponse =
            ReadCardResponse(
                cardDetails, Result.SUCCESS,
                WeipassConstant.MSG_READ_CARD_SUCCESS
            )
        WeipassStateHandler.readCardResponse = readCardResponse
    }


    override fun getPINPADButton(): PinPadConfig {
        return weipassPinPad.getPinPadButton()
    }

    override fun dismissPINPAD() {
        weipassPinPad.dismissPINPAD()
    }

    override fun displayPINPAD() {
        weipassPinPad.displayPinpad()
    }

    override fun updatePINPAD(count: Int) {
        weipassPinPad.updatePINPAD(count)
    }

    override fun pinPadResult(
        pinInputResult: Int,
        isPinUpload: Int,
        pinLen: Int,
        pin: ByteArray?
    ) {
        weipassPinPad.evaluatePinPadResult(pinInputResult, isPinUpload, pinLen, pin)
    }

    override fun refreshPINPADLayout(
        pinType: Int,
        totalTryForOfflinePin: Int,
        remainingTryForOfflinePin: Int,
        numberOrder: String
    ) {
        var test = null
        weipassPinPad.refreshPINPADLayout(
            pinType,
            totalTryForOfflinePin,
            remainingTryForOfflinePin,
            numberOrder)
    }
}