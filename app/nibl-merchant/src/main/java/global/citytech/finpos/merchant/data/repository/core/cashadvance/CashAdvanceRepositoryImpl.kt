package global.citytech.finpos.merchant.data.repository.core.cashadvance

import global.citytech.finpos.merchant.data.datasource.core.cashadvance.CashAdvanceDataSource
import global.citytech.finpos.merchant.domain.model.cashadvance.CashAdvanceResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.cashadvance.CashAdvanceRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.cashadvance.CashAdvanceRequestItem
import global.citytech.finpos.merchant.presentation.model.cashadvance.ManualCashAdvanceRequestItem
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 2/4/2021.
 */
class CashAdvanceRepositoryImpl(private val cashAdvanceDataSource: CashAdvanceDataSource) :
    CashAdvanceRepository {
    override fun performCashAdvance(
        configurationItem: ConfigurationItem,
        cashAdvanceRequestItem: CashAdvanceRequestItem
    ): Observable<CashAdvanceResponseEntity> {
        return cashAdvanceDataSource.performCashAdvance(configurationItem, cashAdvanceRequestItem)
    }

    override fun performManualCashAdvance(
        configurationItem: ConfigurationItem,
        manualCashAdvanceRequestItem: ManualCashAdvanceRequestItem
    ): Observable<CashAdvanceResponseEntity> {
        return cashAdvanceDataSource.performManualCashAdvance(
            configurationItem,
            manualCashAdvanceRequestItem
        )
    }
}