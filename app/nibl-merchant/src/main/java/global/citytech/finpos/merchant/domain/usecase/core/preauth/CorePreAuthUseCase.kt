package global.citytech.finpos.merchant.domain.usecase.core.preauth

import global.citytech.finpos.merchant.domain.model.preauth.PreAuthResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.preauth.PreAuthRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.preauth.ManualPreAuthRequestItem
import global.citytech.finpos.merchant.presentation.model.preauth.PreAuthRequestItem
import io.reactivex.Observable

/**
 * Created by Rishav Chudal on 8/31/20.
 */
class CorePreAuthUseCase(private val preAuthRepository: PreAuthRepository) {
    fun doPreAuth(
        configurationItem: ConfigurationItem,
        preAuthRequestItem: PreAuthRequestItem
    ): Observable<PreAuthResponseEntity> {
        return preAuthRepository.doPreAuth(
            configurationItem,
            preAuthRequestItem
        )
    }

    fun doManualPreAuth(
        configurationItem: ConfigurationItem,
        manualPreAuthRequestItem: ManualPreAuthRequestItem
    ): Observable<PreAuthResponseEntity> = preAuthRepository.doManualPreAuth(
        configurationItem, manualPreAuthRequestItem
    )
}