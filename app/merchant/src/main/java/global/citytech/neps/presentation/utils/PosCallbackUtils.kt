package global.citytech.neps.presentation.utils

import android.content.Context
import global.citytech.common.data.PosCallback
import global.citytech.common.data.PosMessage
import global.citytech.neps.R

/**
 * Created by Rishav Chudal on 6/26/20.
 */
object PosCallbackUtils {
    var callbackMessageMap = HashMap<PosMessage, Int>()
    init {
        callbackMessageMap[PosMessage.POS_MESSAGE_PLEASE_WAIT] = R.string.title_please_wait
        callbackMessageMap[PosMessage.POS_MESSAGE_CONNECTION_MADE] = R.string.title_connection_made
        callbackMessageMap[PosMessage.POS_MESSAGE_TRANSMITTING_REQUEST] =
            R.string.title_transmitting_request
        callbackMessageMap[PosMessage.POS_MESSAGE_CONNECTING] = R.string.msg_connecting
        callbackMessageMap[PosMessage.POS_MESSAGE_CARD_INSERTED] = R.string.title_please_wait
        callbackMessageMap[PosMessage.POS_MESSAGE_PROCESSING] = R.string.title_processing
        callbackMessageMap[PosMessage.POS_MESSAGE_RECONCILIATION] = R.string.msg_reconciliation
        callbackMessageMap[PosMessage.POS_MESSAGE_CONNECTION_FAILED] = R.string.msg_connecting
    }

    fun getMessage(
        context: Context,
        posCallback: PosCallback
    ): String {
        return try {
            val posMessage: PosMessage = posCallback.posMessage
            val id = callbackMessageMap[posMessage]!!
            context.getString(id)
        } catch (ex: Exception) {
            ex.printStackTrace()
            context.getString( R.string.title_please_wait)
        }
    }
}