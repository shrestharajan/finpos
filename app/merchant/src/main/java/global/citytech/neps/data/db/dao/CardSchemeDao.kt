package global.citytech.neps.data.db.dao

import android.database.Cursor
import androidx.room.*
import global.citytech.neps.domain.model.AidParam
import global.citytech.neps.domain.model.CardScheme
import global.citytech.neps.domain.model.Host

@Dao
interface CardSchemeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(cardSchemes: List<CardScheme>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(cardScheme: CardScheme): Long

    @Query("SELECT * FROM card_scheme")
    fun get(): Cursor

    @Query("SELECT * FROM card_scheme")
    fun getCardScheme(): List<CardScheme>

    @Query("SELECT * FROM card_scheme where id = :id")
    fun getById(id: Long): Cursor?

    @Query("SELECT value FROM card_scheme where card_scheme_id = :cardSchemeType AND attribute = :attribute")
    fun getByCardSchemeTypeAndAttribute(
        cardSchemeType: String,
        attribute: String
    ): String?

    @Update
    fun update(cardScheme: CardScheme):Int

    @Query("DELETE FROM card_scheme WHERE id = :id")
    fun deleteById(id: Long): Int

}