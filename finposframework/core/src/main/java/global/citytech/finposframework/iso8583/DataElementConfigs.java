package global.citytech.finposframework.iso8583;

import java.util.List;

/** User: Surajchhetry Date: 2/14/20 Time: 7:22 AM */
public abstract class DataElementConfigs {

  private final List<DataElementConfig> dataElements;

  public DataElementConfigs() {
    this.dataElements = this.config();
  }

  public DataElementConfig getByFieldNumber(int index) {
    if (index < 0 || index > 128) {
      throw new IllegalArgumentException("Invalid index");
    }

    for (DataElementConfig config : this.dataElements) {
      if (config.getIndex() == index)
        return config;
    }
    throw new IllegalArgumentException("Not found with given index");
  }

  protected abstract List<DataElementConfig>  config();


}
