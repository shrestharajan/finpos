package global.citytech.finpos.merchant.domain.model.pinchange

data class
PinChangeResponseEntity(
    val stan: String? = null,
    val message: String = "",
    val isApproved: Boolean
)