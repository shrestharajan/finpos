package global.citytech.finposframework.hardware.io.printer

class PrinterRequest {
    var printableList: List<Printable>? = null
    var feedPaperAfterFinish: Boolean = true
    constructor()

    constructor(printList: List<Printable>) {
        this.printableList = printList
    }

}