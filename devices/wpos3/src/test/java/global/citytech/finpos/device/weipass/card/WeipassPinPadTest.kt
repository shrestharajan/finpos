package global.citytech.finpos.device.weipass.card

import android.app.Activity
import android.content.Context
import com.wiseasy.emvprocess.bean.PinPadConfig
import global.citytech.finpos.device.weipass.card.handler.WeipassStateHandler
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import io.mockk.*
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations
import java.lang.ref.WeakReference
import java.util.concurrent.CountDownLatch

/**
 * Created by Rishav Chudal on 5/29/20.
 */
class WeipassPinPadTest {

    private lateinit var SUT: WeipassPinPad

    private var mockkedContextWeakReference: WeakReference<Context>? = null

    private lateinit var mockkedCountDownLatch: CountDownLatch

    private lateinit var mockkedContext: Context

    private lateinit var mockkedActivity: Activity

    private lateinit var mockkedPinPadConfig: PinPadConfig

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mockkContext()
        SUT = WeipassPinPad()
        mockkWeipassStateHandler()
        mockkCountdownLatch()
        mockkActivity()
        mockkWeipassLogger()
        mockkPinPadConfig()
    }

    @Test //TODO
    fun displayPinpad_WhenMethodIsCalled_ShowsPinpad() {
    }

    @Test //TODO
    fun getPinPadButton_OnMethodCall_ReturnsPinpadConfig() {

    }

    private fun mockkWeipassStateHandler() {
        mockkObject(WeipassStateHandler)
    }

    private fun mockkContext() {
        mockkedContext = spyk()
    }

    private fun mockkCountdownLatch() {
        mockkConstructor(CountDownLatch::class)
        mockkedCountDownLatch = mockk()
    }

    private fun mockkActivity() {
        mockkConstructor(Activity::class)
        mockkedActivity = mockk()
    }

    private fun mockkWeipassLogger() {
        mockkObject(WeipassLogger)
        mockkObject(WeipassLogger.Companion)
        every { WeipassLogger.log(any()) } just Runs
    }

    private fun mockkPinPadConfig() {
        mockkedPinPadConfig = mockk()
    }
}