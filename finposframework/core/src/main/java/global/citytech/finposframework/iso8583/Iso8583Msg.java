package global.citytech.finposframework.iso8583;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import global.citytech.finposframework.supports.Optional;

/**
 * User: Surajchhetry Date: 2/19/20 Time: 5:23 PM
 */
public class Iso8583Msg {
    private MTI mti;
    private List<DataElement> dataElements;

    public Iso8583Msg(MTI mti) {
        this.mti = mti;
        this.dataElements = new ArrayList<>();
    }

    public MTI getMti() {
        return mti;
    }

    public List<DataElement> getDataElements() {
        return dataElements;
    }

    public void addField(DataElement field) {
        if (field == null || isFieldAlreadyAdded(field.getIndex())) {
            return;
        }
        this.dataElements.add(field);
        Collections.sort(this.dataElements, new DataElementComparator());
    }

    public void removeField(int fieldIndex) {
        if (fieldIndex <= 0) {
            return;
        }
        for (DataElement field : dataElements) {
            if (field.getIndex() == fieldIndex) {
                dataElements.remove(field);
            }
        }
    }

    public boolean isFieldAlreadyAdded(int fieldIndex) {
        for (DataElement field : dataElements) {
            return field.getIndex() == fieldIndex;
        }
        return false;
    }

    public void addFields(List<DataElement> fields) {
        for (DataElement dataElement : fields) this.addField(dataElement);
    }

    public Optional<DataElement> getDataElementByIndex(int index) {
        for (DataElement dataElement : this.dataElements) {
            if (dataElement.getIndex() == index) return Optional.of(dataElement);
        }
        return Optional.empty();
    }

    public BitMap getBitMap() {
        BitMap bitMap = new BitMap();
        for (DataElement dataElement : this.dataElements) {
            bitMap.set(dataElement.getIndex());
        }
        return bitMap;
    }

    // get the length of current data
    private int getDataLength(DataElement dataElement) {
        if (dataElement.getData() instanceof byte[])
            return ((byte[]) dataElement.getData()).length;
        return dataElement.getData().toString().length();
    }

    private void writeIfVariableDataLength(DataElement de, DataElementConfig dataElement, ByteArrayOutputStream bout) {
        if (dataElement.getLengthConfig().getLengthType() == LengthConfig.LengthType.LLLVAR
                || dataElement.getLengthConfig().getLengthType() == LengthConfig.LengthType.LLVAR) {
            /*
            int dataLength = this.getDataLength(de);
            int shifter = 48;
            // write the length in ASCII
            if (dataElement.getLengthConfig().getLengthType() == LengthConfig.LengthType.LLLVAR) {
                bout.write((dataLength / 100) + shifter);
            }
            if (dataLength >= 10) {
                bout.write(((dataLength % 100) / 10) + shifter);
            } else {
                bout.write(48); // ASCII value
            }
            bout.write((dataLength % 10) + shifter);

             */
            byte[] lengthAsByte = DataElementLengthCalculator.lengthAsByte(de,dataElement);
            if(lengthAsByte.length > 0) {
                try {
                    bout.write(lengthAsByte);
                } catch (IOException e) {
                   throw new IllegalArgumentException(e);
                }
            }
        }

    }
    public byte[] getDataElementsAsByteArray(SpecInfo spec) {
        DataFormatter formatter = new DataFormatter();
        if (dataElements == null || dataElements.size() <= 0) {
            throw new IllegalArgumentException("Invalid argument");
        }
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream()) {
            for (DataElement de : dataElements) {
                DataElementConfig dataElement = spec.getDEConfig().getByFieldNumber(de.getIndex());
                this.writeIfVariableDataLength(de, dataElement, bout);
                bout.write(formatter.formatAsBytes(de,dataElement));
            }
            return bout.toByteArray();
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }


    public String getPrintableData() {
        StringBuilder builder = new StringBuilder();
        builder.append("\n");
        builder.append("==============================");
        builder.append("\n");
        builder.append("MTI     : ").append(this.getMti().toString()).append("\n");
        builder.append("BIT MAP : ").append(this.getBitMap().printField()).append("\n");
        for (DataElement dataElement : this.dataElements) {
            builder.append(
                    String.format("[ %d ]   |       %s", dataElement.getIndex(), dataElement.getData()));
            builder.append("\n");
        }
        builder.append("==============================");
        builder.append("\n");
        return builder.toString();
    }



/*
  public byte[] getDataElementsAsByteArray(SpecInfo spec) {
    if (dataElements == null || dataElements.size() <= 0) {
      throw new IllegalArgumentException("Invalid argument");
    }
    try (ByteArrayOutputStream bout = new ByteArrayOutputStream()) {
      for (DataElement de : dataElements) {
        DataElement dataElement = spec.getDEConfig().getByFieldNumber(de.getIndex());
        int dataLength;
        if (de.getValue() instanceof byte[])
          dataLength = ((byte[]) de.getValue()).length;
        else
          dataLength = de.getValue().toString().length();
        int shifter = 48;
        System.out.println("Field Index::"+de.getIndex());
        if (dataElement.getLengthType() == DataElement.LengthType.LLLVAR
            || dataElement.getLengthType() == DataElement.LengthType.LLVAR) {
          // write the length in ASCII
          if (dataElement.getLengthType() == DataElement.LengthType.LLLVAR) {
            bout.write((dataLength / 100) + shifter);
          }
          if (dataLength >= 10) {
            bout.write(((dataLength % 100) / 10) + shifter);
          } else {
            bout.write(48); // ASCII value
          }
          bout.write((dataLength % 10) + shifter);
        }
        bout.write(de.formatAccordingToType(dataElement));
      }
      return bout.toByteArray();
    } catch (IOException e) {
      throw new IllegalArgumentException(e.getMessage());
    }
  }

  public String getPrintableData() {
    StringBuilder builder = new StringBuilder();
    builder.append("\n");
    builder.append("==============================");
    builder.append("\n");
    builder.append("MTI     : ").append(this.getMti().toString()).append("\n");
    builder.append("BIT MAP : ").append(this.getBitMap().printField()).append("\n");
    for (DataElement dataElement : this.dataElements) {
      builder.append(
          String.format("[ %d ]   =       %s", dataElement.getIndex(), dataElement.getValue()));
      builder.append("\n");
    }
    builder.append("==============================");
    builder.append("\n");
    return builder.toString();
  }

 */
}
