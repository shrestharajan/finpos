package global.citytech.finpos.device.weipass.utils

/**
 * Created by Rishav Chudal on 4/10/20.
 */
 fun String?.isNullOrEmptyOrBlank(): Boolean {
    var boolean = true
    if ((this != null) && !(this.isEmpty() || this.isBlank())) {
        boolean = false
    }
    return boolean;
}

fun String?.defaultOrEmptyValue(): String {
    var defaultValue = WeipassConstant.EMPTY_STRING
    try {
        if (!this.isNullOrEmptyOrBlank()) {
            defaultValue = this!!
        }
    } catch (ex: Exception) {
        WeipassLogger.log("Exception ::: ".plus(ex.message))
    }
    return defaultValue
}