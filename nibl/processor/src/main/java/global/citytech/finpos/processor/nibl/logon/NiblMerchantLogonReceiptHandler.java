package global.citytech.finpos.processor.nibl.logon;

import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.usecases.transaction.receipt.logon.MerchantLogonReceipt;
import global.citytech.finposframework.utility.IsoMessageUtils;

public class NiblMerchantLogonReceiptHandler {

    private IsoMessageResponse isoMessageResponse;

    public NiblMerchantLogonReceiptHandler(IsoMessageResponse response) {
        this.isoMessageResponse = response;
    }

    public MerchantLogonReceipt prepare() {
        return MerchantLogonReceipt.Builder.aMerchantLogonReceipt()
                .withMessage(this.prepareLogonResultMessage())
                .build();
    }

    private String prepareLogonResultMessage() {
        if (IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, 39) == 0)
            return NiblMerchantLogonReceiptLabels.SUCCESS_MESSAGE;
        else
            return NiblMerchantLogonReceiptLabels.FAILURE_MESSAGE;
    }
}
