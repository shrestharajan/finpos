package global.citytech.finpos.device.weipass.printer

import android.content.Context
import android.graphics.Bitmap
import android.os.RemoteException
import android.util.Log
import global.citytech.common.printer.BarCodeUtils
import global.citytech.common.printer.BitmapGenerator
import global.citytech.common.printer.FontManager
import global.citytech.finpos.device.weipass.R
import global.citytech.finpos.device.weipass.utils.WeipassDeviceUtils
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MAX_BITMAP_HEIGHT_FOR_PRINTING
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getDeviceStatus_PRINTER_STATUS_OK
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getPrinterStatus_PRINTER_NOT_AVAILABLE
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getPrinterStatus_PRINTER_OUT_OF_PAPER
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getPrinterStatus_PRINTER_OVER_HEAT
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getPrinterStatus_PRINTER_STATUS_OK
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.exceptions.PosException
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.printer.*
import wangpos.sdk4.libbasebinder.Printer
import java.lang.ref.WeakReference
import java.util.*

/**
 * Created by Rishav Chudal on 6/19/20.
 */
class WeipassPrinterServiceImpl(context: Context) :
    PrinterService{
    var printer: Printer
    private val contextWeakReference: WeakReference<Context?> = WeakReference(context)

    companion object {
        private const val TAG = "PRINT"
    }


    init {
        printer = Printer(this.contextWeakReference.get())
    }

    override fun print(printerRequest: PrinterRequest?): PrinterResponse {
        return openPrinter(printerRequest)
    }

    private fun execute(printerRequest: PrinterRequest?): PrinterResponse {
        if (contextWeakReference.get() != null) {
            FontManager.setArialFontAsCustomTypeFaceFromAssets(
                contextWeakReference.get()!!
                    .applicationContext.assets
            )
            return printList(printerRequest!!)
        }
        return PrinterResponse(
            Result.FAILURE,
            contextWeakReference.get()!!.getString(R.string.msg_failure_print)
        )
    }

    private fun printList(printerRequest: PrinterRequest): PrinterResponse {
        val printList: List<Printable>? = printerRequest.printableList
        log("::: PRINT LIST SIZE ::: " + printList!!.size)
        var totalPendingBitmapHeight = 0
        try {
            val bitmaps: MutableList<Bitmap> =
                ArrayList()
            for (printable in printList) {
                var bitmap: Bitmap?
                if (!printable.isAllow()) {
                    throw PosException(
                        PosError.DEVICE_ERROR_INVALID_OBJECT_IN_LIST
                    )
                }
                if (printable.isImage()) {
                    bitmap = printable.getData() as Bitmap?
                    totalPendingBitmapHeight =
                        addBitmapToPendingList(bitmaps, bitmap, totalPendingBitmapHeight)
                } else if (printable.isBase64Image()) {
                    bitmap = BitmapGenerator.base64ToImage(printable.getData() as String)
                    totalPendingBitmapHeight =
                        addBitmapToPendingList(bitmaps, bitmap, totalPendingBitmapHeight)
                } else if (printable.isQrCode()) {
                    totalPendingBitmapHeight = loadBitmapsAndQRCode(
                        bitmaps,
                        printable.getData() as PrintMessage?,
                        totalPendingBitmapHeight
                    )
                } else if (printable.isBarCode()) {
                    bitmap = BitmapGenerator.resize(BarCodeUtils.generateBarCode(printable.getData() as String))
                    totalPendingBitmapHeight =
                        addBitmapToPendingList(bitmaps, bitmap, totalPendingBitmapHeight)
                } else if (printable.isFeedPaper()) {
                    printBitmaps(bitmaps)
                    feedPaper((printable.getData() as Int?)!!)
                } else {
                    bitmap = generateBitmap(printable.getData() as PrintMessage?)
                    totalPendingBitmapHeight =
                        addBitmapToPendingList(bitmaps, bitmap, totalPendingBitmapHeight)
                }
            }
            printBitmaps(bitmaps)
            if (printerRequest.feedPaperAfterFinish)
                feedPaper(5)
            return preparePrinterResponse(
                Result.SUCCESS, contextWeakReference.get()!!
                    .getString(R.string.msg_success_print)
            )
        } catch (e: Exception) {
            WeipassLogger.log("Exception in printList ::: " + e.message)
            if (e is PosException) {
                return handlePosException(e)
            }
        } finally {
            closePrinter()
        }
        return preparePrinterResponse(
            Result.FAILURE, contextWeakReference.get()!!.getString(R.string.msg_failure_print)
        )
    }


    private fun handlePosException(e: PosException): PrinterResponse {
        return when (e.posError) {
            PosError.DEVICE_ERROR_PRINTER_OVERHEAT -> preparePrinterResponse(
                Result.FAILURE,
                contextWeakReference.get()!!.getString(R.string.msg_failure_print_overheat)
            )
            PosError.DEVICE_ERROR_PRINTER_OUT_OF_PAPER -> preparePrinterResponse(
                Result.FAILURE, contextWeakReference.get()!!
                    .getString(R.string.msg_failure_print_out_of_paper)
            )
            else -> preparePrinterResponse(
                Result.FAILURE, contextWeakReference.get()!!
                    .getString(R.string.msg_failure_print)
            )
        }
    }

    private fun loadBitmapsAndQRCode(
        bitmaps: MutableList<Bitmap>,
        qrCodeData: PrintMessage?,
        totalPendingBitmapHeight: Int
    ): Int {
        var totalPendingBitmapHeight = totalPendingBitmapHeight
        log("::: LOAD QR CODE :::")
        try { //            printer.printInit();
            if (totalPendingBitmapHeight + qrCodeData!!
                    .getStyle()
                    .qrWidth
                > MAX_BITMAP_HEIGHT_FOR_PRINTING
            ) {
                printBitmaps(bitmaps)
                totalPendingBitmapHeight = 0
            } else {
                if (!bitmaps.isEmpty()) {
                    val data =
                        BitmapGenerator.mergeBitmapsVertically(bitmaps)
                    cleanupBitmap(bitmaps)
                    val res = printer.printImageBase(
                        data, data.width, data.height,
                        Printer.Align.CENTER, 0
                    )
                    log("::: BITMAP PRINT RESULT ::: $res")
                }
            }
            printQRCode(qrCodeData)
            totalPendingBitmapHeight += qrCodeData.getStyle().qrWidth
        } catch (e: Exception) {
            WeipassLogger.log("Exception ::: " + e.message)
            throw PosException(
                PosError.DEVICE_ERROR_UNABLE_TO_PRINT
            )
        }
        return totalPendingBitmapHeight
    }

    @Throws(RemoteException::class)
    private fun printBitmaps(bitmaps: MutableList<Bitmap>) {
        val mergedBitmap = BitmapGenerator.mergeBitmapsVertically(bitmaps)
        log("::: BITMAP PRINTING BITMAP HEIGHT::: " + mergedBitmap.height)
        printBitmapImage(mergedBitmap)
        cleanupBitmap(bitmaps)
        cleanupBitmap(mergedBitmap)
    }

    @Throws(RemoteException::class)
    private fun addBitmapToPendingList(
        bitmaps: MutableList<Bitmap>,
        bitmap: Bitmap?,
        totalPendingBitmapHeight: Int
    ): Int {
        var totalPendingBitmapHeight = totalPendingBitmapHeight
        if (bitmap == null) return totalPendingBitmapHeight
        if (totalPendingBitmapHeight + bitmap.height > MAX_BITMAP_HEIGHT_FOR_PRINTING) {
            printBitmaps(bitmaps)
            totalPendingBitmapHeight = 0
        }
        bitmaps.add(bitmap)
        totalPendingBitmapHeight += bitmap.height
        return totalPendingBitmapHeight
    }

    private fun cleanupBitmap(bitmaps: MutableList<Bitmap>) {
        for (bitmap in bitmaps) {
            cleanupBitmap(bitmap)
        }
        bitmaps.clear()
    }

    private fun cleanupBitmap(bitmap: Bitmap) {
        var bitmap: Bitmap? = bitmap
        if (bitmap != null) {
            bitmap.recycle()
            bitmap = null
        }
    }

    private fun printQRCode(printMessage: PrintMessage?) {
        log(":::: PRINTING QR CODE :::")
        try {
            printer.printQRCode(
                printMessage!!.value, printMessage.getStyle().qrWidth,
                WeipassDeviceUtils.getAlignmentForQRCode(printMessage.getStyle().getAlignment())
            )
        } catch (ex: Exception) {
            throw PosException(
                PosError.DEVICE_ERROR_UNABLE_TO_PRINT
            )
        }
    }

    private fun log(message: String) {
        Log.d(TAG, message)
    }

    private fun generateBitmap(printMessage: PrintMessage?): Bitmap? {
        return if (printMessage!!.hasMultiString()) {
            if (printMessage.textList == null && printMessage.textList!!.isEmpty()) null
            else BitmapGenerator.fromMultiColumn(
                printMessage.textList,
                printMessage.getStyle(), printMessage.columnWidths
            )
        } else {
            if (printMessage.label!!.isEmpty()) BitmapGenerator.fromText(
                printMessage.value, printMessage.getStyle(),
                WeipassDeviceUtils.getAlignmentForBitmap(printMessage.getStyle().getAlignment()),
                BitmapGenerator.PAPER_WIDTH
            ) else BitmapGenerator.fromDoubleColumn(
                printMessage.label,
                printMessage.value, printMessage.getStyle(),
                WeipassDeviceUtils.getAlignmentForBitmap(printMessage.getStyle().getAlignment())
            )
        }
    }

    @Throws(RemoteException::class)
    private fun printListInLoop(printList: List<Printable>) {
        for (printable in printList) {
            if (!printable.isAllow()) throw PosException(
                PosError.DEVICE_ERROR_INVALID_OBJECT_IN_LIST
            )
            if (printable.isImage()) printBitmapImage(printable.getData() as Bitmap?) else printText(
                printable.getData() as PrintMessage?
            )
        }
    }

    @Throws(RemoteException::class)
    private fun printText(printMessage: PrintMessage?) {
        if (printMessage!!.hasMultiString()) {
            printMultipleColumn(printMessage)
        } else {
            if (printMessage.label!!.isEmpty()) printForeignLanguageText(printMessage)
            else printDoubleColumnText(
                printMessage
            )
        }
    }

    @Throws(RemoteException::class)
    private fun printDoubleColumnText(printMessage: PrintMessage?) {
        val bitmap = BitmapGenerator.fromDoubleColumn(
            printMessage!!.label,
            printMessage.value, printMessage.getStyle(),
            WeipassDeviceUtils.getAlignmentForBitmap(printMessage.getStyle().getAlignment())
        )
        printBitmapImage(bitmap)
        cleanupBitmap(bitmap)
    }

    @Throws(RemoteException::class)
    private fun printMultipleColumn(printMessage: PrintMessage?) {
        if (printMessage!!.textList == null && printMessage.textList!!.isEmpty()) return
        val bitmap = BitmapGenerator.fromMultiColumn(
            printMessage.textList,
            printMessage.getStyle(), printMessage.columnWidths
        )
        printBitmapImage(bitmap)
        cleanupBitmap(bitmap)
    }

    @Throws(RemoteException::class)
    private fun printForeignLanguageText(printMessage: PrintMessage?) {
        val bitmap = BitmapGenerator.fromText(
            printMessage!!.value, printMessage.getStyle(),
            WeipassDeviceUtils.getAlignmentForBitmap(printMessage.getStyle().getAlignment()),
            BitmapGenerator.PAPER_WIDTH
        )
        printBitmapImage(bitmap)
        cleanupBitmap(bitmap)
    }

    @Throws(RemoteException::class)
    private fun printBitmapImage(data: Bitmap?) {
        var res = printer.printImageBase(
            data, data!!.width, data.height,
            Printer.Align.CENTER, 0
        )
        log("::: BITMAP PRINT RESULT ::: $res")
        res = printer.printFinish()
        log("::: BITMAP PRINT FINISH ::: $res")
        handlePrintResultResponse(res)
    }

    private fun handlePrintResultResponse(res: Int) {
        when (res) {
            SDK_API_getDeviceStatus_PRINTER_STATUS_OK -> {
                clearPrintDataCache()
                try {
                    Thread.sleep(100)
                    printer.printInit()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            SDK_API_getPrinterStatus_PRINTER_OVER_HEAT -> throw PosException(
                PosError.DEVICE_ERROR_PRINTER_OVERHEAT
            )
            SDK_API_getPrinterStatus_PRINTER_OUT_OF_PAPER -> throw PosException(
                PosError.DEVICE_ERROR_PRINTER_OUT_OF_PAPER
            )
            else -> throw PosException(
                PosError.DEVICE_ERROR_PRINTER_ERROR
            )
        }
    }

    private fun clearPrintDataCache() {
        try {
            printer.clearPrintDataCache()
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    private fun openPrinter(printerRequest: PrinterRequest?): PrinterResponse {
        try {
            printer.printInit()
            clearPrintDataCache()
            printer.setGrayLevel(1)
            return checkStatus(printerRequest)
        } catch (e: Exception) {
            WeipassLogger.log("Printer Exception in OpenPrinter, exception ::: " + e.message)
        } finally {
            closePrinter()
        }
        return preparePrinterResponse(
            Result.FAILURE, contextWeakReference.get()!!.getString(R.string.msg_failure_print)
        )
    }

    private fun checkStatus(printerRequest: PrinterRequest?): PrinterResponse {
        try {
            val status = IntArray(1)
            val result = printer.getPrinterStatus(status)
            return handlePrinterResponseCode(printerRequest, result)
        } catch (e: Exception) {
            WeipassLogger.log("Printer Exception in checkStatus, exception ::: " + e.message)
        }
        return PrinterResponse(
            Result.FAILURE, contextWeakReference.get()!!.getString(R.string.msg_failure_print)
        )
    }

    override fun feedPaper(numberOfEmptyLines: Int) {
        try {
            val result = printer.printPaper(numberOfEmptyLines * 20)
            WeipassLogger.log("printPaper result ::: $result")
            if (result == 0) {
                WeipassLogger.log("Print Success Finally")
            } else {
                throw PosException(
                    PosError.DEVICE_ERROR_UNABLE_TO_PRINT
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
            throw PosException(
                PosError.DEVICE_ERROR_UNABLE_TO_PRINT
            )
        }
    }

    private fun closePrinter() {
        try {
            printer.printFinish()
        } catch (e: Exception) {
            WeipassLogger.log("Printer Exception" + e.message)
            e.printStackTrace()
        }
    }


    private fun handlePrinterResponseCode(
        printerRequest: PrinterRequest?,
        printerResponseCode: Int
    ): PrinterResponse {
        return when (printerResponseCode) {
            SDK_API_getPrinterStatus_PRINTER_STATUS_OK -> execute(printerRequest)
            SDK_API_getPrinterStatus_PRINTER_NOT_AVAILABLE -> {
                log(
                    "Printer error ::: Not Available,  code ::: "
                            + printerResponseCode
                )
                preparePrinterResponse(
                    Result.FAILURE,
                    contextWeakReference.get()!!.getString(R.string.msg_failure_print)
                )
            }
            SDK_API_getPrinterStatus_PRINTER_OUT_OF_PAPER -> {
                log(
                    "Printer error ::: Out of Paper,  code ::: "
                            + printerResponseCode
                )
                preparePrinterResponse(
                    Result.FAILURE, contextWeakReference.get()!!
                        .getString(R.string.msg_failure_print_out_of_paper)
                )
            }
            SDK_API_getPrinterStatus_PRINTER_OVER_HEAT -> {
                log(
                    "Printer error ::: Over heat,  code ::: "
                            + printerResponseCode
                )
                preparePrinterResponse(
                    Result.FAILURE, contextWeakReference.get()!!
                        .getString(R.string.msg_failure_print_overheat)
                )
            }
            else -> {
                log("Printer error,  code ::: $printerResponseCode")
                preparePrinterResponse(
                    Result.FAILURE, contextWeakReference.get()!!
                        .getString(R.string.msg_failure_print)
                )
            }
        }
    }

    private fun preparePrinterResponse(
        result: Result,
        message: String
    ): PrinterResponse {
        return PrinterResponse(
            result,
            message
        )
    }
}