package global.citytech.finpos.processor.neps.keyexchange;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.keymgmt.keys.KeyService;
import global.citytech.finposframework.switches.keyexchange.KeyExchangeRequestParameter;
import global.citytech.finposframework.supports.UseCase;

/** User: Surajchhetry Date: 2/28/20 Time: 6:03 PM */
public class KeyExchangeRequestModel implements UseCase.Request, KeyExchangeRequestParameter {

    private PrinterService printerService;
    private final KeyService hardwareKeyService;
    private final String applicationPackageName;

    public KeyExchangeRequestModel(PrinterService printerService, KeyService hardwareKeyService, String applicationPackageName) {
        this.printerService = printerService;
        this.hardwareKeyService = hardwareKeyService;
        this.applicationPackageName = applicationPackageName;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public KeyService getHardwareKeyService() {
        return hardwareKeyService;
    }

    public String getApplicationPackageName() {
        return applicationPackageName;
    }
}
