package global.citytech.finpos.merchant.data.datasource.app.db

import global.citytech.common.data.Response
import global.citytech.finpos.merchant.presentation.dashboard.temp.LogResponse
import global.citytech.finpos.merchant.presentation.dashboard.temp.LogFileUploadResponse

interface LogPushDatabaseSource {
    fun getLogsList(serialNumber: Any,filePaths: String): LogResponse
    fun pushLogFileToServer(logFileUploadResponse: LogFileUploadResponse, filePath: String, serialNumber: Any): Response

}