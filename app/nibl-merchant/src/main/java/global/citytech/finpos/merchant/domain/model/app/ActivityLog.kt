package global.citytech.finpos.merchant.domain.model.app

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author BikashShrestha
 * @project finpos
 * @created 2021/11/03 - 11:18 AM
 */

@Entity(tableName = ActivityLog.TABLE_NAME)
data class ActivityLog(

    @PrimaryKey
    @ColumnInfo(name = COLUMN_ID)
    var id: String,

    @ColumnInfo(name = COLUMN_STAN)
    var stan: String,

    @ColumnInfo(name = COLUMN_TYPE)
    var type: String,

    @ColumnInfo(name = COLUMN_MERCHANT_ID)
    var merchantId: String,

    @ColumnInfo(name = COLUMN_TERMINAL_ID)
    var terminalId: String,

    @ColumnInfo(name = COLUMN_BATCH_NUMBER)
    var batchNumber: String? = "",

    @ColumnInfo(name = COLUMN_AMOUNT)
    var amount: Long? = 0,

    @ColumnInfo(name = COLUMN_RESPONSE_CODE)
    var responseCode: String,

    @ColumnInfo(name = COLUMN_ISO_REQUEST)
    var isoRequest: String? = "",

    @ColumnInfo(name = COLUMN_ISO_RESPONSE)
    var isoResponse: String? = "",

    @ColumnInfo(name = COLUMN_STATUS)
    var status: String,

    @ColumnInfo(name = COLUMN_REMARKS)
    var remarks: String? = "",

    @ColumnInfo(name = COLUMN_POS_DATE_TIME)
    var posDateTime: String,

    @ColumnInfo(name = COLUMN_ADDITIONAL_DATA)
    var additionalData: String? = ""
) {

    companion object {
        const val TABLE_NAME = "activity_logs"
        const val COLUMN_ID = "id"
        const val COLUMN_STAN = "stan"
        const val COLUMN_TYPE = "type"
        const val COLUMN_MERCHANT_ID = "merchant_id"
        const val COLUMN_TERMINAL_ID = "terminal_id"
        const val COLUMN_BATCH_NUMBER = "batch_number"
        const val COLUMN_AMOUNT = "amount"
        const val COLUMN_RESPONSE_CODE = "response_code"
        const val COLUMN_ISO_REQUEST = "iso_request"
        const val COLUMN_ISO_RESPONSE = "iso_response"
        const val COLUMN_STATUS = "status"
        const val COLUMN_REMARKS = "remarks"
        const val COLUMN_POS_DATE_TIME = "pos_date_time"
        const val COLUMN_ADDITIONAL_DATA = "additional_data"
    }

    override fun toString(): String {
        return "ActivityLog(id='$id', stan='$stan', type='$type', merchantId='$merchantId', terminalId='$terminalId', batchNumber=$batchNumber, amount=$amount, responseCode='$responseCode', isoRequest=$isoRequest, isoResponse=$isoResponse, status='$status', remarks=$remarks, posDateTime='$posDateTime', additionalData=$additionalData)"
    }
}