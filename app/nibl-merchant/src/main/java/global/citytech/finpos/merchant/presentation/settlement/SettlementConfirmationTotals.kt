package global.citytech.finpos.merchant.presentation.settlement

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Unique Shakya on 8/4/2021.
 */
@Parcelize
data class SettlementConfirmationTotals(
    val currencyName: String,
    val salesCount: String,
    val salesAmount: String,
    val refundCount: String,
    val refundAmount: String,
    val voidCount: String,
    val voidAmount: String,
    val authCount: String,
    val authAmount: String,
    val totalCount: String,
    val totalAmount: String
) : Parcelable