package global.citytech.finpos.merchant.presentation.voidsale

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.data.repository.core.voidsale.VoidSaleRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.datasource.core.voidsale.VoidSaleUtil
import global.citytech.finpos.merchant.domain.model.app.TransactionLog
import global.citytech.finpos.merchant.domain.model.voidsale.VoidSaleResponseEntity
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.voidsale.VoidSaleDataSourceImpl
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.core.voidsale.VoidSaleUseCase
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.TransactionItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finposframework.usecases.TransactionType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal

/**
 * Created by Rishav Chudal on 9/18/20.
 */
class VoidViewModel(val instance: Application) : BaseTransactionViewModel(instance) {

    val invalidInvoiceNumber by lazy { MutableLiveData<Boolean>() }
    val transactionFound by lazy { MutableLiveData<Boolean>() }
    val transactionItem by lazy { MutableLiveData<TransactionItem>() }
    val transactionAlreadyVoided by lazy { MutableLiveData<Boolean>() }
    val tipAdjustedAmount by lazy { MutableLiveData<Double>() }
    val invoiceNumberFromRrn by lazy { MutableLiveData<String>() }
    val invalidRrnFromCheckout by lazy { MutableLiveData<Boolean>() }
    lateinit var transactionLog: TransactionLog
    lateinit var mode: String

    private var voidSaleUseCase = VoidSaleUseCase(
        VoidSaleRepositoryImpl(
            VoidSaleDataSourceImpl(instance)
        )
    )

    private var localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    fun getMerchantCredential(): String = localDataUseCase.getMerchantCredential()

    fun validateData(data: String, mode: String) {
        this.mode = mode
        if (VoidSaleUtils.validateData(data)) {
            checkForTransactionPresence(data)
        } else {
            isLoading.value = false
            invalidInvoiceNumber.value = true
        }
    }

    private fun checkForTransactionPresence(data: String) {
        compositeDisposable.add(
            VoidSaleUtils.getTransactionPresenceValidatorObservable(localDataUseCase, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        onCheckTransactionPresentNext(data, it)
                    },
                    {
                        this.transactionFound.value = false
                    }
                )
        )
    }

    private fun onCheckTransactionPresentNext(
        data: String,
        isTransactionPresent: Boolean
    ) {
        if (isTransactionPresent) {
            updateTransactionItemIfTransactionCanBeVoid(data)
        } else {
            isLoading.value = false
            this.transactionFound.value = false
        }
    }

    private fun updateTransactionItemIfTransactionCanBeVoid(data: String) {
        compositeDisposable.add(
            VoidSaleUtils.getTransactionValidatorObservable(localDataUseCase, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter {
                    validTransactionForVoid(it)
                }
                .subscribe({
                    onTransactionLogRetrievedForInvoiceNumber(it)
                }, {
                    message.value = it.message
                })
        )
    }

    private fun onTransactionLogRetrievedForInvoiceNumber(
        transactionLog: TransactionLog
    ) {
        transactionItem.value = transactionLog.mapToPresentation()
        /*Check for tips if necessary*/
        this.transactionLog = transactionLog
        getConfigurationItem()
    }


    private fun validTransactionForVoid(
        transactionLog: TransactionLog
    ): Boolean {
        return if (isTransactionOfRespectiveMode(transactionLog)) {
            !isTransactionAlreadyVoided(transactionLog)
        } else {
            updateLiveDataIfNotRespectiveModeTransaction()
            false
        }
    }

    private fun updateLiveDataIfNotRespectiveModeTransaction() {
        this.transactionFound.value = false
    }

    private fun isTransactionOfRespectiveMode(
        transactionLog: TransactionLog
    ): Boolean {
        return if (mode == VoidActivity.CASH) {
            isCashAdvanceTransaction(transactionLog)
        } else {
            isPurchaseTransaction(transactionLog)
        }

    }

    private fun isCashAdvanceTransaction(transactionLog: TransactionLog): Boolean =
        transactionLog.transactionType.equals(
            Jsons.toJsonObj(TransactionType.CASH_ADVANCE),
            true
        )

    private fun isPurchaseTransaction(transactionLog: TransactionLog): Boolean =
        transactionLog.transactionType.equals(
            Jsons.toJsonObj(TransactionType.PURCHASE),
            true
        )

    private fun isTransactionAlreadyVoided(
        transactionLog: TransactionLog
    ): Boolean {
        var alreadyVoided: Boolean
        transactionLog.transactionVoided!!.let {
            this.transactionAlreadyVoided.value = it
            alreadyVoided = it
        }
        return alreadyVoided
    }

    fun doVoidSale(
        configurationItem: ConfigurationItem,
        amount: BigDecimal,
        transactionType: TransactionType
    ) {
        isLoading.value = true
        compositeDisposable.add(
            voidSaleUseCase.doVoidSale(
                configurationItem,
                VoidSaleUtil.prepareVoidSaleRequestItem(
                    transactionType,
                    amount,
                    transactionLog.rrn!!
                )
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        onVoidSaleNext(it)
                    },
                    {
                        onVoidSaleError(it)
                    }
                )
        )
    }

    private fun onVoidSaleNext(voidSaleResponseEntity: VoidSaleResponseEntity) {
        isLoading.value = false
        showTransactionConfirmationDialog(voidSaleResponseEntity)
    }

    private fun showTransactionConfirmationDialog(it: VoidSaleResponseEntity) {
        prepareBase64UrlToDisplayQr(
            it.isApproved,
            it.message!!,
            it.shouldPrintCustomerCopy,
            it.stan!!
        )
    }

    private fun onVoidSaleError(throwable: Throwable) {
        throwable.printStackTrace()
        isLoading.value = false
        transactionComplete.value = false
        message.value = throwable.message
    }

    fun retrieveInvoiceNumberFromRrn(rrn: String?) {
        isLoading.value = true
        rrn?.let {
            compositeDisposable.add(
                localDataUseCase.getInvoiceNumberByRrn(it)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        isLoading.value = false
                        invoiceNumberFromRrn.value = it
                    }, {
                        isLoading.value = false
                        invalidRrnFromCheckout.value = true
                    })
            )
        }
    }
}