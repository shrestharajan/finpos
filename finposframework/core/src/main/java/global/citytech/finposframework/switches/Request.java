package global.citytech.finposframework.switches;

/** @author rajudhital on 3/29/20 */
@FunctionalInterface
public interface Request<Req extends RequestParameter, Res extends ResponseParameter> {
  Res execute(Req req);
}
