package global.citytech.finpos.merchant.mqtt

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import global.citytech.finpos.merchant.mqtt.MqttNotificationDetailPage.Companion.NOTIFICATION_CONTENT
import global.citytech.finpos.merchant.mqtt.MqttNotificationDetailPage.Companion.NOTIFICATION_TITLE
import global.citytech.finpos.merchant.mqtt.MqttNotificationDetailPage.Companion.NOTIFICATION_TYPE

class MqttDetailBroadCastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val launchIntent = Intent(context, MqttNotificationDetailPage::class.java)
        launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val bundle = Bundle()
        bundle.putString(NOTIFICATION_CONTENT, intent?.getStringExtra(NOTIFICATION_CONTENT))
        bundle.putString(NOTIFICATION_TITLE, intent?.getStringExtra(NOTIFICATION_TITLE))
        bundle.putString(NOTIFICATION_TYPE, intent?.getStringExtra(NOTIFICATION_TYPE))

        launchIntent.putExtras(bundle)
        context?.startActivity(launchIntent)
    }
}