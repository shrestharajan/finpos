package global.citytech.finposframework.supports;

/** User: Surajchhetry Date: 2/18/20 Time: 10:47 AM */
public class Result<T> {
  private boolean error;
  private T result;
  private String message;
  private String code;

  private Result() {
    this.error = true;
  }

  public String getCode() {
    return code;
  }

  public boolean hasError() {
    return error;
  }

  public boolean isSuccess() {
    return !error;
  }

  public T getResult() {
    return this.result;
  }

  public String getMessage() {
    return message;
  }

  public interface ErrorInfo {
    String getCode();

    String getMessage();
  }

  public static class Builder<T> {
    private boolean error;
    private T result;
    private String message;
    private String code;

    private Builder() {}

    public static Builder createDefaultResultBuilder() {
      return new Builder();
    }

    public Builder error(ErrorInfo errorInfo) {
      this.error = true;
      this.code = errorInfo.getCode();
      this.message = errorInfo.getMessage();
      return this;
    }

    public Builder error(String message) {
      this.error = true;
      this.code = "SYS-500";
      this.message = message;
      return this;
    }

    public Builder errorWithResult(String code, String message, T result) {
      this.error = true;
      this.code = code;
      this.message = message;
      this.result = result;
      return this;
    }

    public Builder success(String code, String message) {
      this.error = false;
      this.code = code;
      this.message = message;
      return this;
    }

    public Builder success(T result) {
      this.error = false;
      this.code = "0";
      this.message = "success";
      this.result = result;
      return this;
    }

    public Builder successWithResult(String code, String message, T result) {
      this.error = false;
      this.code = code;
      this.message = message;
      this.result = result;
      return this;
    }

    public Result build() {
      Result result = new Result<T>();
      result.message = this.message;
      result.error = this.error;
      result.code = this.code;
      result.result = this.result;
      return result;
    }
  }
}
