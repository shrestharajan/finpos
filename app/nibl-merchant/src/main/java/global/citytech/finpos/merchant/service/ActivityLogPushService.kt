package global.citytech.finpos.merchant.service

import android.content.Context
import android.content.Intent
import androidx.lifecycle.*
import global.citytech.common.AppCountDownTimer
import global.citytech.common.data.Response
import global.citytech.common.extensions.hasInternetConnection
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.ActivityLog
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.presentation.model.activitylog.ActivityLogResponse
import global.citytech.finpos.merchant.utils.ApiConstant
import global.citytech.finpos.merchant.utils.AppConstant
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finpos.merchant.utils.getMinutesAndSecondsDataFromMillis
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.exceptions.PosException
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/11/09 - 11:36 PM
*/

class ActivityLogPushService : LifecycleService() {

    private val TAG = ActivityLogPushService::class.java.name


    private val localDataUseCase: LocalDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    companion object {
        private val startPushingActivityLog by lazy { MutableLiveData<Boolean>() }
        fun start(context: Context) {
            stop(context)
            printLog(ActivityLogPushService::class.java.name, "START PUSH ACTIVITY LOG SERVICE")
            context.startService(
                Intent(
                    context.applicationContext,
                    ActivityLogPushService::class.java
                )
            )
        }

        private fun stop(context: Context) {
            printLog(ActivityLogPushService::class.java.name, "STOP PUSH ACTIVITY LOG SERVICE")
            context.stopService(
                Intent(
                    context.applicationContext,
                    ActivityLogPushService::class.java
                )
            )
        }

        fun pushActivityLog() {
            startPushingActivityLog.value = true
        }
    }

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var countDownTimer = AppCountDownTimer(
        AppConstant.SERVER_ACTIVITY_LOG_PUSH_INTERVAL_MILLIS,
        1000,
        this::onCountDownTick,
        this::onCountDownFinish
    )
    private var activityLogPushInProgress: Boolean = false

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        printLog(TAG, ":: OnStartCommand ::", true)
        observerStartActivityLog()
        pushActivityLogIfFeasible()
        countDownTimer.start()
        return super.onStartCommand(intent, flags, startId)
    }

    private fun observerStartActivityLog() {
        printLog(TAG, "Change detected in StartPushingActivityLog MutableLiveData...")
        startPushingActivityLog.observe(this, Observer {
            if (it) {
                printLog(TAG, "StartPushingActivityLog MutableLiveData is true...")
                pushActivityLogIfFeasible()
            }
        })
    }


    private fun onCountDownTick(millisUntilFinished: Long) {
        printLog(
            TAG,
            "Time left to push next batch ::: "
                .plus(millisUntilFinished.getMinutesAndSecondsDataFromMillis()), true
        )
        if (!NiblMerchant.INSTANCE.iPlatformManager.asBinder().pingBinder()) {
            printLog(TAG, ":: PLATFORM MANAGER DISCONNECTED ::", true)
            NiblMerchant.INSTANCE.bindService()
        }
    }

    private fun checkActivityLogPushEnabled(): Boolean {
        val terminalSettings = localDataUseCase.getTerminalSetting()
        printLog(TAG, "Terminal Settings ::: ".plus(terminalSettings.toString()), true)
        return terminalSettings.enableActivityLog;
    }

    private fun pushActivityLogIfFeasible() {
        val activityLogPushIsEnabled = checkActivityLogPushEnabled()
        printLog(
            TAG,
            ":: Is Activity Log Push in Progress? ::: ".plus(activityLogPushInProgress),
            true
        )
        printLog(TAG, ":: Is Activity Log Push Enabled? ::: ".plus(activityLogPushIsEnabled), true)
        if (activityLogPushInProgress || !activityLogPushIsEnabled) {
            return
        }
        printLog(TAG, ":: Activity log push in progress now...", true)
        activityLogPushInProgress = true
        fetchActivityLogs()
    }

    private fun fetchActivityLogs() {
        printLog(TAG, ":: Fetching activity logs first...", true)
        compositeDisposable.add(
            localDataUseCase.getActivityLog().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    validateActivityLogsToPush(it)
                }, {
                    it.printStackTrace()
                    printLog(TAG, "ERROR IN RETRIEVING ACTIVITY LOGS")
                    printLog(TAG, it.message.toString())
                    activityLogPushInProgress = false
                })
        )
    }

    private fun validateActivityLogsToPush(activityLogs: List<ActivityLog>?) {
        if (activityLogs == null || activityLogs.isEmpty()) {
            printLog(TAG, ":: No Activity Logs to Push...")
            activityLogPushInProgress = false
            return
        }
        updateLogsIfActiveConnection(activityLogs)
    }

    private fun updateLogsIfActiveConnection(activityLogs: List<ActivityLog>?) {
        if (!NiblMerchant.INSTANCE.hasInternetConnection()) {
            activityLogPushInProgress = false
            return
        }
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<ActivityLogResponse> {
                pushActivityLogTask(activityLogs, it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onNext, this::onError)
        )
    }

    private fun pushActivityLogTask(
        activityLogs: List<ActivityLog>?,
        emitter: ObservableEmitter<ActivityLogResponse>
    ) {
        printLog(TAG, ":: Pushing Activity Log...")
        val jsonRequest = Jsons.toJsonObj(activityLogs)
        printLog(TAG, "Activity Log Request::: $jsonRequest", true)
        if (!NiblMerchant.INSTANCE.iPlatformManager.asBinder().pingBinder()) {
            NiblMerchant.INSTANCE.bindService()
            emitter.onError(Exception("Error Code :: iPlatformManager bind error"))
        }
        val jsonResponse =
            NiblMerchant.INSTANCE.iPlatformManager.post(
                ApiConstant.UPDATE_ACTIVITY_LOG_URL,
                jsonRequest,
                true
            )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            prepareResponse(emitter, response)
        } else {
            if (response.code == "401")
                emitter.onError(PosException(PosError.DEVICE_ERROR_OPERATOR_LOGIN_REQUIRED))
            else
                emitter.onError(Exception("Error Code :: ${response.code}. ${response.message}"))
        }
        emitter.onComplete()
    }

    private fun prepareResponse(
        emitter: ObservableEmitter<ActivityLogResponse>,
        data: Any
    ) {
        val responseJson = Jsons.toJsonObj(data)
        val response = Jsons.fromJsonToObj(responseJson, ActivityLogResponse::class.java)
        emitter.onNext(response)
    }

    private fun onNext(response: ActivityLogResponse) {
        if (response.data != null) {
            compositeDisposable.add(
                localDataUseCase.nukeActivityLog()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        activityLogPushInProgress = false
                        printLog(TAG,"Activity Log Database Cleared")
                    }, {
                        activityLogPushInProgress = false
                        printLog(TAG,it.message.toString())
                    })
            )
        } else {
            activityLogPushInProgress = false
            printLog(TAG,"Some Error occurred during pushing activity log.")
        }

    }

    private fun onError(throwable: Throwable) {
        activityLogPushInProgress = false
        printLog(TAG,throwable.message.toString())
    }


    private fun onCountDownFinish() {
        pushActivityLogIfFeasible()
        countDownTimer.start()
    }

}