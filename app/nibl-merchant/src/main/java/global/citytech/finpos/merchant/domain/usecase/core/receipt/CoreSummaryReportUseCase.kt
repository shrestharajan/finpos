package global.citytech.finpos.merchant.domain.usecase.core.receipt

import global.citytech.finpos.merchant.domain.model.receipt.summaryreport.SummaryReportResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.receipt.SummaryReportRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.processor.nibl.receipt.summaryreport.SummaryReportResponseModel
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 1/5/2021.
 */
class CoreSummaryReportUseCase(private val summaryReportRepository: SummaryReportRepository) {
    fun print(configurationItem: ConfigurationItem): Observable<SummaryReportResponseEntity> {
        return summaryReportRepository.print(configurationItem)
    }
}