package global.citytech.finpos.merchant.presentation.dashboard.temp

import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuAdapter
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuItem
import global.citytech.finpos.merchant.presentation.model.banner.Banner

/**
 * Created by Unique Shakya on 6/16/2021.
 */
data class DashItems(
    var advertisements: List<Banner>? = null,
    var primaryTransaction: MenuItem? = null,
    var primaryMenuAdapterListener: MenuAdapter.MenuAdapterListener? = null,
    var menus: List<MenuItem>? = null,
    var menusAdapterListener: MenuAdapter.MenuAdapterListener? = null
)