package global.citytech.finposframework.hardware.emv.emvparam

import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.DeviceService

/**
 * Created by Rishav Chudal on 4/22/20.
 */
interface EmvParametersService: DeviceService {
    fun injectEmvParameters(emvParametersRequest: EmvParametersRequest): DeviceResponse
}