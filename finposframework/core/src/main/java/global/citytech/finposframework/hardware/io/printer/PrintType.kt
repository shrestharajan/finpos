package global.citytech.finposframework.hardware.io.printer

/**
 * Created by Rishav Chudal on 6/19/20.
 */
enum class PrintType {
    STRING, BITMAP
}