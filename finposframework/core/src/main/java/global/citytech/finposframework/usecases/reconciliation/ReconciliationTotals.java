package global.citytech.finposframework.usecases.reconciliation;

/**
 * Created by Unique Shakya on 9/22/2020.
 */
public interface ReconciliationTotals {
    String toTotalsBlock();

}
