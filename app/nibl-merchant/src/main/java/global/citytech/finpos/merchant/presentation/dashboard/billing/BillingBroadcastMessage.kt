package global.citytech.finpos.merchant.presentation.dashboard.billing

/**
 * Created by Rishav Chudal on 08/06/2022.
 */
data class BillingBroadcastMessage(
    val messageType: String,
    val transactionPage: String,
    val transactionMessage: String
)
