package global.citytech.finpos.merchant.presentation.refund

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.common.extensions.isAlphaNumeric
import global.citytech.finpos.merchant.data.repository.core.refund.RefundRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.refund.RefundDataSourceImpl
import global.citytech.finpos.merchant.domain.model.purchase.RefundResponseEntity
import global.citytech.finpos.merchant.domain.usecase.core.refund.CoreRefundUseCase
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.refund.ManualRefundRequestItem
import global.citytech.finpos.merchant.presentation.model.refund.RefundRequestItem
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finposframework.usecases.TransactionType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal

/**
 * Created by Unique Shakya on 9/28/2020.
 */
class RefundViewModel(application: Application) : BaseTransactionViewModel(application) {

    val validRrn by lazy { MutableLiveData<Boolean>() }

    private var refundUseCase = CoreRefundUseCase(RefundRepositoryImpl(RefundDataSourceImpl()))

    fun validateRetrievalReferenceNumber(retrievalReferenceNumber: String) {
        if (retrievalReferenceNumber.isAlphaNumeric() && retrievalReferenceNumber.length == 12) {
            isLoading.value = false
            validRrn.value = true
        } else {
            isLoading.value = false
            validRrn.value = false
        }
    }

    fun refund(
        configurationItem: ConfigurationItem, amount: BigDecimal, retrievalReferenceNumber: String,
        transactionType: TransactionType
    ) {
        isLoading.value = true
        compositeDisposable.add(
            refundUseCase.refund(
                configurationItem,
                RefundRequestItem(amount, transactionType, retrievalReferenceNumber)
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onRefundResponse(it)
                }, {
                    onRefundError(it)
                })
        )
    }

    private fun onRefundError(it: Throwable?) {
        it!!.printStackTrace()
        isLoading.value = false
        if (!shouldDispose) {
            transactionComplete.value = false
            message.value = it.message
        } else {
            completePreviousTask()
        }
    }

    private fun onRefundResponse(it: RefundResponseEntity) {
        isLoading.value = false
        if (!shouldDispose) {
            prepareBase64UrlToDisplayQr(
                it.isApproved!!,
                it.message!!,
                it.shouldPrintCustomerCopy!!,
                it.stan!!
            )
        } else {
            completePreviousTask()
        }
    }

    private fun showTransactionConfirmationDialog(it: RefundResponseEntity?) {
        val transactionConfirmation = this.retrieveTransactionConfirmation(
            it!!.isApproved!!, it.message, it.shouldPrintCustomerCopy!!
        )
        transactionConfirmationData.postValue(transactionConfirmation)
    }

    fun manualRefund(
        configurationItem: ConfigurationItem?,
        manualRefundRequestItem: ManualRefundRequestItem
    ) {
        isLoading.value = true
        compositeDisposable.add(
            refundUseCase.manualRefund(
                configurationItem!!,
                manualRefundRequestItem
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onManualRefundResponse(it)
                }, {
                    onManualRefundError(it)
                })
        )
    }

    private fun onManualRefundError(it: Throwable?) {
        it!!.printStackTrace()
        isLoading.value = false
        transactionComplete.value = false
        message.value = it.message

    }

    private fun onManualRefundResponse(it: RefundResponseEntity) {
        prepareBase64UrlToDisplayQr(
            it.isApproved!!,
            it.message!!,
            it.shouldPrintCustomerCopy!!,
            it.stan!!
        )
    }
}