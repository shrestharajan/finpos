package global.citytech.finpos.merchant.data.repository.core.refund

import global.citytech.finpos.merchant.data.datasource.core.refund.RefundDataSource
import global.citytech.finpos.merchant.domain.model.purchase.RefundResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.refund.RefundRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.refund.ManualRefundRequestItem
import global.citytech.finpos.merchant.presentation.model.refund.RefundRequestItem
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/28/2020.
 */
class RefundRepositoryImpl(private val refundDataSource: RefundDataSource) : RefundRepository {
    override fun refund(
        configurationItem: ConfigurationItem,
        refundRequestItem: RefundRequestItem
    ): Observable<RefundResponseEntity> {
        return refundDataSource.refund(configurationItem, refundRequestItem)
    }

    override fun manualRefund(
        configurationItem: ConfigurationItem,
        manualRefundRequestItem: ManualRefundRequestItem
    ): Observable<RefundResponseEntity> {
        return refundDataSource.manualRefund(configurationItem, manualRefundRequestItem)
    }
}