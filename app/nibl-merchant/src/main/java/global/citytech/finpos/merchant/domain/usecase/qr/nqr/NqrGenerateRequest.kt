package global.citytech.finpos.merchant.domain.usecase.qr.nqr

import global.citytech.finpos.merchant.domain.usecase.qr.base.QrGenerateRequest
/**
 * @author sachin
 */
data class NqrGenerateRequest(
    val initiationMethod: String,
    val acquirerCode: String,
    val merchantCode: String,
    val merchantCategoryCode: String,
    val countryCode: String,
    val merchantName: String,
    val merchantCity: String,
    val transactionCurrency: String,
    val transactionAmount: String,
    val terminalId: String,
    val billNumber: String
) : QrGenerateRequest {

    override fun toString(): String {
        return "NqrGenerateRequest(initiationMethod='$initiationMethod', acquirerCode='$acquirerCode', merchantCode='$merchantCode', merchantCategoryCode='$merchantCategoryCode', countryCode='$countryCode', merchantName='$merchantName', merchantCity='$merchantCity', transactionCurrency='$transactionCurrency', transactionAmount='$transactionAmount', terminalId='$terminalId', billNumber='$billNumber')"
    }
}