package global.citytech.finpos.neps.receipt.keyexchange;

import global.citytech.finposframework.hardware.io.printer.Style;

/**
 * Created by Unique Shakya on 1/19/2021.
 */
public enum NepsKeyExchangeReceiptStyle {

    KEY_EXCHANGE_MESSAGE(Style.Align.CENTER, Style.FontSize.NORMAL, 1, true, true, false, false),
    DIVIDER(Style.Align.CENTER, Style.FontSize.NORMAL, 1, false, false, false, false);

    private Style.Align align;
    private Style.FontSize fontSize;
    private int columns;
    private boolean allCaps;
    private boolean bold;
    private boolean italic;
    private boolean underline;

    NepsKeyExchangeReceiptStyle(Style.Align align, Style.FontSize fontSize, int columns, boolean allCaps, boolean bold, boolean italic, boolean underline) {
        this.align = align;
        this.fontSize = fontSize;
        this.columns = columns;
        this.allCaps = allCaps;
        this.bold = bold;
        this.italic = italic;
        this.underline = underline;
    }

    public Style.Align getAlign() {
        return align;
    }

    public Style.FontSize getFontSize() {
        return fontSize;
    }

    public int getColumns() {
        return columns;
    }

    public boolean isAllCaps() {
        return allCaps;
    }

    public boolean isBold() {
        return bold;
    }

    public boolean isItalic() {
        return italic;
    }

    public boolean isUnderline() {
        return underline;
    }

    public Style getStyle() {
        return new Style.Builder()
                .alignment(this.align)
                .allCaps(this.allCaps)
                .bold(this.bold)
                .fontSize(this.fontSize)
                .italic(this.italic)
                .multipleAlignment(this.isMultipleAlignment())
                .underline(this.underline)
                .build();
    }

    private boolean isMultipleAlignment() {
        return this.columns > 1;
    }
}
