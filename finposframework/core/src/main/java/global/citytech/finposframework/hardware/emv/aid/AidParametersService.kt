package global.citytech.finposframework.hardware.emv.aid

import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.DeviceService

/**
 * Created by Rishav Chudal on 5/5/20.
 */
interface AidParametersService: DeviceService {
    fun injectAid(request: AidInjectionRequest): DeviceResponse

    fun eraseAllAid(): DeviceResponse
}