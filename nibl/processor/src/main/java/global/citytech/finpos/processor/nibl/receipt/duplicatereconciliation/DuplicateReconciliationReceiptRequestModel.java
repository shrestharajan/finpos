package global.citytech.finpos.processor.nibl.receipt.duplicatereconciliation;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.duplicatereconciliation.DuplicateReconciliationReceiptRequestParameter;

/**
 * Created by Unique Shakya on 12/16/2020.
 */
public class DuplicateReconciliationReceiptRequestModel implements UseCase.Request, DuplicateReconciliationReceiptRequestParameter {

    private ReconciliationRepository reconciliationRepository;
    private PrinterService printerService;

    public DuplicateReconciliationReceiptRequestModel(ReconciliationRepository reconciliationRepository, PrinterService printerService) {
        this.reconciliationRepository = reconciliationRepository;
        this.printerService = printerService;
    }

    public ReconciliationRepository getReconciliationRepository() {
        return reconciliationRepository;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }
}
