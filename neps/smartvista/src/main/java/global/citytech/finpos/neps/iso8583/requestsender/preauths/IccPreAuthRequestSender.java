package global.citytech.finpos.neps.iso8583.requestsender.preauths;

import global.citytech.finpos.neps.iso8583.requestsender.cardtypesenders.ICCISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.CardTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transactiontypesenders.PreAuthIsoMessageUpdater;
import global.citytech.finposframework.iso8583.RequestContext;

/**
 * Created by Saurav Ghimire on 1/21/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class IccPreAuthRequestSender extends PreAuthRequestSender {

    public IccPreAuthRequestSender( RequestContext context) {
        super( context);
    }

    @Override
    protected String getClassName() {
        return IccPreAuthRequestSender.class.getName();
    }

    @Override
    protected CardTypeISOMessageUpdater getCardTypeSender() {
        return new ICCISOMessageUpdater();
    }



}
