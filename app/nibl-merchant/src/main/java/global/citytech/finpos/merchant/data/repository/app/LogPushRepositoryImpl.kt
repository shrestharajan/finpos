package global.citytech.finpos.merchant.data.repository.app

import global.citytech.common.data.Response
import global.citytech.finpos.merchant.data.datasource.app.db.LogPushDatabaseSource
import global.citytech.finpos.merchant.domain.repository.app.LogPushRepository
import global.citytech.finpos.merchant.presentation.dashboard.temp.LogResponse
import global.citytech.finpos.merchant.presentation.dashboard.temp.LogFileUploadResponse

class LogPushRepositoryImpl(
    private val logPushDatabaseSource: LogPushDatabaseSource
    ): LogPushRepository {
    override fun getLogsList(serialNumber: Any,filePaths: String): LogResponse {
        return logPushDatabaseSource.getLogsList(serialNumber,filePaths)
    }

    override fun pushLogFileToServer(logFileUploadResponse: LogFileUploadResponse, filePath: String, serialNumber: Any): Response {
        return logPushDatabaseSource.pushLogFileToServer(logFileUploadResponse, filePath, serialNumber)
    }


}