package global.citytech.finpos.merchant.domain.model.app

import android.content.ContentValues
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = Merchant.TABLE_NAME)
data class Merchant(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_ID)
    var id: String,
    @ColumnInfo(name = COLUMN_NAME)
    var name: String? = null,
    @ColumnInfo(name = COLUMN_ADDRESS)
    var address: String? = null,
    @ColumnInfo(name = COLUMN_PHONE_NUMBER)
    var phoneNumber: String? = null,
    @ColumnInfo(name = COLUMN_SWITCH_ID)
    var switchId: String? = null,
    @ColumnInfo(name = COLUMN_TERMINAL_ID)
    var terminalId: String? = null
) {

    companion object {
        const val TABLE_NAME = "merchants"
        const val COLUMN_ID = "id"
        const val COLUMN_NAME = "merchant_name"
        const val COLUMN_ADDRESS = "merchant_address"
        const val COLUMN_PHONE_NUMBER = "merchant_phone"
        const val COLUMN_SWITCH_ID = "merchant_switch_id"
        const val COLUMN_TERMINAL_ID = "merchant_terminal_id"

        fun fromContentValues(values: ContentValues): Merchant {
            values.let {
                val merchant =
                    Merchant(
                        id = values.getAsString(COLUMN_ID)
                    )
                if (it.containsKey(COLUMN_ID)) {
                    merchant.id = it.getAsString(COLUMN_ID)
                }
                if (it.containsKey(COLUMN_NAME))
                    merchant.name = it.getAsString(COLUMN_NAME)
                if (it.containsKey(COLUMN_ADDRESS))
                    merchant.address = it.getAsString(COLUMN_ADDRESS)
                if (it.containsKey(COLUMN_PHONE_NUMBER))
                    merchant.phoneNumber = it.getAsString(COLUMN_PHONE_NUMBER)
                if (it.containsKey(COLUMN_SWITCH_ID))
                    merchant.switchId = it.getAsString(COLUMN_SWITCH_ID)
                if (it.containsKey(COLUMN_TERMINAL_ID))
                    merchant.terminalId = it.getAsString(COLUMN_TERMINAL_ID)
                return merchant
            }
        }

    }
}