package global.citytech.finpos.processor.neps.greenpin;

import java.util.concurrent.CountDownLatch;

import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.processor.neps.greenpin.OtpGeneration.OtpGenerationUseCase;
import global.citytech.finpos.processor.neps.greenpin.PinSet.PinSetUseCase;
import global.citytech.finpos.processor.neps.transaction.TransactionExecutorTemplate;
import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.listeners.CardConfirmationListener;
import global.citytech.finposframework.listeners.CardConfirmationListenerForGreenPin;
import global.citytech.finposframework.listeners.OtpConfirmationListener;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;

public class GreenPinUseCase extends TransactionExecutorTemplate {

    private String OTP = "";
    boolean[] isConfirmForPinSet = {false};

    public GreenPinUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    public TransactionResponse execute(global.citytech.finpos.processor.neps.transaction.TransactionRequest request) {
        setUpRequiredTransactionFields(request);
        OtpGenerationUseCase otpGenerationUseCase = new OtpGenerationUseCase(this.terminalRepository, this.notifier);
        TransactionResponse transactionResponse = otpGenerationUseCase.execute(request);
        if (transactionResponse.isApproved()) {
            readCardResponse = transactionResponse.getReadCardResponse();
            boolean shouldExecutePinSetUseCase = true;
            try {
                proceedOtpConfirmation();
            } catch (PosException | InterruptedException e) {
                e.printStackTrace();
                this.notifier.notify(Notifier.EventType.RETURN_TO_DASHBOARD);
                shouldExecutePinSetUseCase = false;
            }
            if (shouldExecutePinSetUseCase) {
                try{
                    proceedPinSetIsoIfCardPresent();
                }catch (PosException | InterruptedException e) {
                    e.printStackTrace();
                }
                if(isConfirmForPinSet[0]){
                    PinSetUseCase pinSetUseCase = new PinSetUseCase(this.terminalRepository, this.notifier);
                    pinSetUseCase.setCardResponse(this.readCardResponse, this.transactionRequest, this.request, this.stan, OTP);
                    TransactionResponse pinSetTransactionResponse = pinSetUseCase.execute(request);
                    if (pinSetTransactionResponse.isApproved()) {
                        IsoMessageResponse isoMessageResponse = null;
                        return prepareResponse(isoMessageResponse, true, applicationRepository, false);
                    } else {
                        return prepareErrorResponse(pinSetTransactionResponse.getMessage(), false);
                    }
                } else {
                    this.notifier.notify(Notifier.EventType.HIDE_PROGRESS_SCREEN);
                    throw new PosException(PosError.CARD_REMOVED);
                }
            } else {
                return prepareErrorResponse("Transaction Declined", false);
            }

        } else {
            return prepareErrorResponse(transactionResponse.getMessage(), false);
        }
    }

    void proceedOtpConfirmation() throws InterruptedException {
        final boolean[] isConfirm = {false};
        CountDownLatch countDownLatch = new CountDownLatch(1);
        try {
            this.notifier.notify(Notifier.EventType.OTP_CONFIRMATION_EVENT,
                    readCardResponse.getCardDetails(),
                    (OtpConfirmationListener) (confirmed, otp) -> {
                        isConfirm[0] = confirmed;
                        this.OTP = otp;
                        countDownLatch.countDown();
                    });
        } catch (Exception e) {

        }
        countDownLatch.await();
        if (!isConfirm[0]) {
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        }
    }

    void proceedPinSetIsoIfCardPresent() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        try {
            this.notifier.notify(Notifier.EventType.CHECK_IF_CARD_PRESENT,
                    (CardConfirmationListenerForGreenPin) confirmed -> {
                        isConfirmForPinSet[0] = confirmed;
                        countDownLatch.countDown();
                    });
        } catch (Exception e) {
        }
        countDownLatch.await();
    }

    @Override
    protected void setUpRequiredTransactionFields(global.citytech.finpos.processor.neps.transaction.TransactionRequest request) {
        this.request = request;
        transactionRequest = request.getTransactionRequest();
        stan = this.terminalRepository.getSystemTraceAuditNumber();
        applicationRepository = request.getApplicationRepository();
    }


    @Override
    protected void retrieveCardResponse() {
    }

    @Override
    protected void processCard() {
    }

    @Override
    protected String getClassName() {
        return null;
    }


    @Override
    protected TransactionIsoRequest prepareIsoRequest(ReadCardResponse readCardResponse, TransactionRequest transactionRequest) {
        return null;
    }

    @Override
    protected NepsMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context) {
        return null;
    }

    @Override
    protected void processOnlineResult(IsoMessageResponse isoMessageResponse) {

    }

    @Override
    protected void updateTransactionLog(boolean transactionApproved, TransactionRequest transactionRequest, TransactionRepository transactionRepository, IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse) {

    }

    @Override
    protected void updateTransactionIdsAndNotifyTransactionStatus(boolean transactionApproved, IsoMessageResponse isoMessageResponse, global.citytech.finpos.processor.neps.transaction.TransactionRequest request, ReadCardResponse readCardResponse) {

    }

    @Override
    protected TransactionResponse prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository, boolean shouldPrint) {
        return GreenPinResponse.Builder.newInstance()
                .withStan(stan)
                .withApproved(transactionApproved)
                .withMessage(transactionApproved ? "Your Pin has been updated." : "Cannot perform Task").build();
    }

    @Override
    protected String getProcessingCode() {
        return null;
    }

    @Override
    protected TransactionResponse prepareErrorResponse(String responseMessage, boolean shouldPrint) {
        return GreenPinResponse.Builder.newInstance()
                .withStan(this.stan)
                .withMessage(responseMessage)
                .withApproved(false)
                .build();

    }

    @Override
    protected TransactionResponse promptToPerformIccTransaction() {
        return null;
    }
}

