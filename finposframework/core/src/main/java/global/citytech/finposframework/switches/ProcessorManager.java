package global.citytech.finposframework.switches;

import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;

/**
 * @author rajudhital on 4/3/20
 */
public class ProcessorManager {
    private static Processor processor;

    private ProcessorManager() {
    }

    public static synchronized void register(Processor processor) {
        if (ProcessorManager.processor == null) ProcessorManager.processor = processor;
    }

    public static Processor getInterface(TerminalRepository terminalRepository, Notifier notifier) {
        if (ProcessorManager.processor != null) {
            ProcessorManager.processor.init(terminalRepository, notifier);
        }
        return ProcessorManager.processor;
    }

    public static synchronized void unRegister() {
        ProcessorManager.processor = null;
    }
}
