package global.citytech.finpos.merchant.presentation.model.refund

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

/**
 * Created by Unique Shakya on 11/3/2020.
 */
data class ManualRefundRequestItem(
    val amount: BigDecimal? = null,
    val transactionType: TransactionType? = null,
    val originalRetrievalReferenceNumber: String? = null,
    val cardNumber: String = "",
    val expiryDate: String = "",
    val cvv: String = ""
) {
}