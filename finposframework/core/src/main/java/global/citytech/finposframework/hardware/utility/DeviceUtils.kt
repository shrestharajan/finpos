package global.citytech.finposframework.hardware.utility

import global.citytech.finposframework.hardware.EmvTagList
import global.citytech.finposframework.hardware.io.cards.EmvTag
import global.citytech.finposframework.hardware.io.cards.EMVTagCollection
import global.citytech.finposframework.hardware.io.cards.cvm.PICCCvm
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.DEFAULT_CARD_TIMEOUT_IN_SECS
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.DEFAULT_PIN_MAX_LENGTH
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.DEFAULT_PIN_MIN_LENGTH
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.TRACK_2_SEPARATOR_TYPE_D
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.TRACK_2_SEPARATOR_TYPE_EQUALS_TO
import global.citytech.finposframework.utility.HEX
import global.citytech.finposframework.utility.StringUtils

/**
 * Created by Rishav Chudal on 4/7/21.
 */
class DeviceUtils {
    companion object {
        fun getTimeOutOrDefault(time: Int): Int {
            var timeOut = time
            if (timeOut == 0 || timeOut > 60) {
                timeOut = DEFAULT_CARD_TIMEOUT_IN_SECS
            }
            return timeOut
        }

        fun panFromTrackTwoData(trackTwoData: String): String {
            println("Track 2 Data ::: ".plus(trackTwoData))
            var pan = ""
            if (!StringUtils.isEmpty(trackTwoData)) {
                if (trackTwoData.contains(TRACK_2_SEPARATOR_TYPE_EQUALS_TO)) {
                    pan = trackTwoData.split(TRACK_2_SEPARATOR_TYPE_EQUALS_TO)[0]
                } else if (trackTwoData.contains(TRACK_2_SEPARATOR_TYPE_D)) {
                    pan = trackTwoData.split(TRACK_2_SEPARATOR_TYPE_D)[0]
                }
            }
            return pan
        }

        fun cardHolderNameFromTrackOneData(trackOneData: String?): String {
            println("Track 1 Data ::: ".plus(trackOneData))
            var cardHolderName = ""
            if (!StringUtils.isEmpty(trackOneData)) {
                val trackOneDataArray = trackOneData!!.split("\\^".toRegex())
                if (trackOneDataArray.size > 1) {
                    cardHolderName = trackOneDataArray[1]
                }
            }
            return cardHolderName
        }

        fun expiryDateFromTrackTwoData(trackTwoData: String): String {
            var expiryDate = ""
            if (!StringUtils.isEmpty(trackTwoData)) {
                var trackTwoDataArray: Array<String>? = null
                if (trackTwoData.contains(TRACK_2_SEPARATOR_TYPE_EQUALS_TO)) {
                    trackTwoDataArray =
                        trackTwoData.split(TRACK_2_SEPARATOR_TYPE_EQUALS_TO).toTypedArray()
                } else if (trackTwoData.contains(TRACK_2_SEPARATOR_TYPE_D)) {
                    trackTwoDataArray = trackTwoData.split(TRACK_2_SEPARATOR_TYPE_D).toTypedArray()
                }
                if (trackTwoDataArray != null && trackTwoDataArray.size > 1) {
                    expiryDate = trackTwoDataArray[1].substring(0, 4)
                }
            }
            return expiryDate
        }

        fun appendTagIfNotPresent(
            EMVTagCollection: EMVTagCollection,
            emvTagList: EmvTagList,
            tagLength: Int
        ) {
            var emvTag = EMVTagCollection.get(emvTagList.tag)
            if (emvTag.size == 0) {
                println("Appending Absent Tag ::: " + emvTagList.name)
                emvTag = EmvTag(
                    emvTagList.tag,
                    StringUtils.ofRequiredLength(0, tagLength * 2),
                    tagLength
                )
                println(
                    "TAG ::: " + emvTag.tagInHex() +
                            ", Length ::: " + emvTag.size
                            + ", Value ::: " + maskDataIfRequired(emvTag)
                )
                EMVTagCollection.add(emvTag)
            }
        }

        fun maskDataIfRequired(emvTag: EmvTag): String {
            var data = emvTag.data
            if (!StringUtils.isEmpty(data)) {
                if (emvTag.tag == EmvTagList.APPLICATION_PRIMARY_ACCOUNT_NUMBER.tag) {
                    data = StringUtils.maskCardNumber(data)
                } else if (emvTag.tag == EmvTagList.TRACK_2_EQUIVALENT_DATA.tag) {
                    data = StringUtils.maskTrack2(data)
                } else if (emvTag.tag == EmvTagList.CARD_HOLDER_NAME.tag) {
                    data = String(StringUtils.hexString2bytes(data))
                }
            }
            return data
        }

        fun getMinPinLengthOrDefault(length: Int?): Int {
            var minLength = length
            if (minLength == null || minLength == 0) {
                minLength = DEFAULT_PIN_MIN_LENGTH
            }
            return minLength
        }

        fun getMaxPinLengthOrDefault(length: Int?): Int {
            var maxLength = length
            if (maxLength == null || maxLength < 4) {
                maxLength = DEFAULT_PIN_MAX_LENGTH
            }
            return maxLength
        }

        fun retrieveCVMFromTransactionOutcome(tagDF8129: String): PICCCvm {
            var piccCVM = PICCCvm.NO_CVM
            val tagDf8129InBytes = HEX.hexToBytes(tagDF8129)
            println("tagDF8129bytes[3] ::: ".plus(tagDf8129InBytes[3]))
            val cvmBitValue = (tagDf8129InBytes[3].toInt() ushr 4)
            println("CVM Bit Value of DF8129 ::: ".plus(Integer.toHexString(cvmBitValue)))
            when(cvmBitValue) {
                0x01 -> piccCVM = PICCCvm.SIGNATURE
                0x02 -> piccCVM = PICCCvm.ONLINE_PIN
                0x03 -> piccCVM = PICCCvm.CD_CVM
            }
            return piccCVM
        }
    }
}