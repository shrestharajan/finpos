package global.citytech.finpos.processor.neps.transaction;

import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;

/**
 * Created by Saurav Ghimire on 2/8/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public abstract class CardlessTransactionUseCase extends TransactionExecutorTemplate{



    public CardlessTransactionUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    protected void processOnlineResult(IsoMessageResponse isoMessageResponse) {

    }
}
