package global.citytech.finposframework.hardware.io.cards.cvm;


public enum Cvm {

    FAILED_CVM_PROCESSING("000000", CardHolderVerification.NO_VERIFICATION),
    PLAIN_TEXT_PIN_ICC("0000001", CardHolderVerification.PIN_VERIFIED),
    ENCIPHERED_PIN_ONLINE("000010", CardHolderVerification.PIN_VERIFIED),
    PLAIN_TEXT_PIN_ICC_SIGNATURE("000011", CardHolderVerification.PIN_VERIFIED),
    ENCIPHERED_PIN_ICC("000100", CardHolderVerification.PIN_VERIFIED),
    ENCIPHERED_PIN_ICC_SIGNATURE("000101", CardHolderVerification.PIN_VERIFIED),
    SIGNATURE("011110", CardHolderVerification.SIGNATURE_VERIFIED),
    NO_CVM("011111", CardHolderVerification.NO_VERIFICATION),
    RFU_EMV("", CardHolderVerification.NO_VERIFICATION),
    RFU_PAYMENT_SYSTEMS("", CardHolderVerification.NO_VERIFICATION),
    RFU_ISSUER("", CardHolderVerification.NO_VERIFICATION),
    NOT_USE("", CardHolderVerification.NO_VERIFICATION),
    UNKNOWN("", CardHolderVerification.NO_VERIFICATION),
    DEVICE_OWNER_VERIFIED("", CardHolderVerification.DEVICE_OWNER_VERIFIED);

    private final String byteCodes;
    private final CardHolderVerification cardHolderVerification;

    Cvm(String byteCodes, CardHolderVerification cardHolderVerification) {
        this.byteCodes = byteCodes;
        this.cardHolderVerification = cardHolderVerification;
    }

    public String getByteCodes() {
        return byteCodes;
    }

    public CardHolderVerification getCardHolderVerification() {
        return cardHolderVerification;
    }
}
