package global.citytech.finpos.processor.nibl.reconciliation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import global.citytech.finpos.nibl.iso8583.requestsender.settlement.SettlementRequestSender;
import global.citytech.finpos.processor.nibl.transaction.receipt.NiblIsoMessageReceiptHandler;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.utility.IsoMessageUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

/**
 * Created by Unique Shakya on 10/8/2020.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({TerminalRepository.class, Notifier.class, ReconciliationRequestModel.class,
        ReconciliationRepository.class, PrinterService.class, HostInfo.class, TerminalInfo.class,
        SettlementRequestSender.class, NiblReconciliationReceipt.class, ConnectionParam.class,
        NiblReconciliationReceiptPrintHandler.class, RequestContext.class, NiblReconciliationHandler.class,
        NiblReconciliationTotals.class, IsoMessageResponse.class, IsoMessageUtils.class, ApplicationRepository.class,
        ReceiptHandler.IsoMessageReceiptHandler.class, ReconciliationUseCase.class, NiblReconciliationReceiptMaker.class})
public class ReconciliationUseCaseTest {

    TerminalRepository terminalRepository;
    Notifier notifier;
    ReconciliationRequestModel reconciliationRequestModel;
    ReconciliationRepository reconciliationRepository;
    PrinterService printerService;
    HostInfo hostInfo;
    TerminalInfo terminalInfo;
    SettlementRequestSender settlementRequestSender;
    NiblReconciliationReceipt niblReconciliationReceipt;
    NiblReconciliationReceiptPrintHandler niblReconciliationReceiptPrintHandler;
    ReceiptHandler.IsoMessageReceiptHandler niblIsoMessageReceiptHandler;
    ConnectionParam connectionParam;
    RequestContext requestContext;
    NiblReconciliationHandler reconciliationHandler;
    NiblReconciliationTotals niblReconciliationTotals;
    IsoMessageResponse isoMessageResponse;
    ApplicationRepository applicationRepository;
    NiblReconciliationReceiptMaker niblReconciliationReceiptMaker;

    @Before
    public void setup() {
        terminalRepository = PowerMockito.mock(TerminalRepository.class);
        notifier = PowerMockito.mock(Notifier.class);
        isoMessageResponse = PowerMockito.mock(IsoMessageResponse.class);
        reconciliationRequestModel = PowerMockito.mock(ReconciliationRequestModel.class);
        reconciliationRepository = PowerMockito.mock(ReconciliationRepository.class);
        printerService = PowerMockito.mock(PrinterService.class);
        hostInfo = PowerMockito.mock(HostInfo.class);
        terminalInfo = PowerMockito.mock(TerminalInfo.class);
        settlementRequestSender = PowerMockito.mock(SettlementRequestSender.class);
        niblReconciliationReceipt = PowerMockito.mock(NiblReconciliationReceipt.class);
        niblReconciliationReceiptPrintHandler = PowerMockito.mock(NiblReconciliationReceiptPrintHandler.class);
        niblIsoMessageReceiptHandler = PowerMockito.mock(NiblIsoMessageReceiptHandler.class);
        connectionParam = PowerMockito.mock(ConnectionParam.class);
        requestContext = PowerMockito.mock(RequestContext.class);
        reconciliationHandler = PowerMockito.mock(NiblReconciliationHandler.class);
        niblReconciliationTotals = PowerMockito.mock(NiblReconciliationTotals.class);
        applicationRepository = PowerMockito.mock(ApplicationRepository.class);
        niblReconciliationReceiptMaker = PowerMockito.mock(NiblReconciliationReceiptMaker.class);
        PowerMockito.mockStatic(IsoMessageUtils.class);
    }

    @Test
    public void executeTest() throws Exception {
        PowerMockito.when(this.terminalRepository.getReconciliationBatchNumber())
                .thenReturn("001");
        PowerMockito.when(this.terminalRepository.findPrimaryHost())
                .thenReturn(this.hostInfo);
        PowerMockito.when(this.terminalRepository.findSecondaryHost())
                .thenReturn(this.hostInfo);
        PowerMockito.when(this.terminalInfo.isDebugModeEnabled()).thenReturn(true);
        PowerMockito.when(this.terminalRepository.findTerminalInfo())
                .thenReturn(this.terminalInfo);
        PowerMockito.when(this.terminalRepository.getSystemTraceAuditNumber())
                .thenReturn("000022");

        PowerMockito.whenNew(ConnectionParam.class).withAnyArguments()
                .thenReturn(this.connectionParam);
        PowerMockito.whenNew(RequestContext.class).withAnyArguments()
                .thenReturn(this.requestContext);

        PowerMockito.whenNew(SettlementRequestSender.class).withAnyArguments()
                .thenReturn(this.settlementRequestSender);
        PowerMockito.when(this.settlementRequestSender.send()).thenReturn(this.isoMessageResponse);

        PowerMockito.when(this.reconciliationRequestModel.getReconciliationRepository())
                .thenReturn(reconciliationRepository);
        PowerMockito.when(this.reconciliationRequestModel.getPrinterService())
                .thenReturn(this.printerService);

        PowerMockito.doNothing().when(this.niblIsoMessageReceiptHandler)
                .printIsoMessageReceipt(this.isoMessageResponse);
        PowerMockito.whenNew(NiblIsoMessageReceiptHandler.class).withAnyArguments()
                .thenReturn((NiblIsoMessageReceiptHandler) this.niblIsoMessageReceiptHandler);

        PowerMockito.when(this.reconciliationHandler.prepareReconciliationTotals())
                .thenReturn(this.niblReconciliationTotals);
        PowerMockito.whenNew(NiblReconciliationHandler.class).withAnyArguments()
                .thenReturn(this.reconciliationHandler);

        PowerMockito.doNothing().when(this.niblReconciliationReceiptPrintHandler)
                .printReceipt(this.niblReconciliationReceipt);
        PowerMockito.whenNew(NiblReconciliationReceiptPrintHandler.class).withAnyArguments()
                .thenReturn(this.niblReconciliationReceiptPrintHandler);

        PowerMockito.doNothing().when(notifier).notify(any(Notifier.EventType.class));
        PowerMockito.doNothing().when(notifier).notify(any(Notifier.EventType.class), anyString());

        PowerMockito.doNothing().when(this.reconciliationRepository).updateBatch("001", "", "");
        PowerMockito.doNothing().when(this.terminalRepository).incrementSystemTraceAuditNumber();
        PowerMockito.doNothing().when(this.terminalRepository).incrementReconciliationBatchNumber();

        PowerMockito.when(IsoMessageUtils.retrieveFromDataElementsAsInt(this.isoMessageResponse, 39))
                .thenReturn(0);

        PowerMockito.whenNew(NiblReconciliationReceiptMaker.class).withAnyArguments()
                .thenReturn(this.niblReconciliationReceiptMaker);
        PowerMockito.when(this.niblReconciliationReceiptMaker.prepare())
                .thenReturn(this.niblReconciliationReceipt);


        ReconciliationUseCase reconciliationUseCase = new ReconciliationUseCase(this.terminalRepository,
                this.notifier);
        ReconciliationResponseModel responseModel = reconciliationUseCase.execute(reconciliationRequestModel);

        assertNotNull(responseModel);
        assertTrue(responseModel.isSuccess());
        assertEquals("RECONCILIATION SUCCESS", responseModel.getMessage());
    }
}
