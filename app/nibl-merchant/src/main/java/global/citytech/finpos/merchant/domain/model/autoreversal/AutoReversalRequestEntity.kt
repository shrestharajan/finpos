package global.citytech.finpos.merchant.domain.model.autoreversal

import global.citytech.finposframework.hardware.io.printer.PrinterService
import global.citytech.finposframework.repositories.ApplicationRepository
import global.citytech.finposframework.repositories.TransactionRepository

/**
 * Created by Saurav Ghimire on 7/12/21.
 * sauravnghimire@gmail.com
 */


data class AutoReversalRequestEntity(
    var transactionRepository: TransactionRepository? = null,
    val printerService: PrinterService? = null,
    val applicationRepository: ApplicationRepository? = null
)