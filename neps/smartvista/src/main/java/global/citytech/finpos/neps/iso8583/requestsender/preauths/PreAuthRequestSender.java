package global.citytech.finpos.neps.iso8583.requestsender.preauths;

import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transactiontypesenders.PreAuthIsoMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transactiontypesenders.PurchaseIsoMessageUpdater;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;

public abstract class PreAuthRequestSender extends TransactionRequestSender {
    public PreAuthRequestSender(RequestContext context) {
        super(context);
    }

    @Override
    protected TransactionTypeISOMessageUpdater getTransactionTypeSender() {
        return new PreAuthIsoMessageUpdater();
    }

    @Override
    protected MTI.MessageFunction getMessageFunctionRequest() {
        return MTI.MessageFunction.Request;
    }

    @Override
    protected MTI.MessageClass getMessageClass() {
        return MTI.MessageClass.Financial;
    }


}
