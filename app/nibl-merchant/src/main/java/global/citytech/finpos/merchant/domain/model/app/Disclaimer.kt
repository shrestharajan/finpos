package global.citytech.finpos.merchant.domain.model.app

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import global.citytech.finpos.merchant.domain.model.app.Disclaimer.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class Disclaimer(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_ID)
    var disclaimerId: String,
    @ColumnInfo(name = COLUMN_SERVICE_ID)
    var serviceId: String,
    @ColumnInfo(name = COLUMN_SERVICE_NAME)
    var serviceName: String,
    @ColumnInfo(name = COLUMN_NOTIFICATION_TITLE)
    var notificationTitle: String,
    @ColumnInfo(name = COLUMN_NOTIFICATION_CONTENT)
    var notificationContent: String? = null,
    @ColumnInfo(name = COLUMN_DISCLAIMER_INFO)
    var disclaimerInfo: String? = null,
    @ColumnInfo(name = COLUMN_DISCLAIMER_CONTENT)
    var disclaimerContent: String,
    @ColumnInfo(name = COLUMN_REMARKS)
    var remarks: String? = null
) {

    companion object {
        const val TABLE_NAME = "disclaimer"
        const val COLUMN_ID = "disclaimer_id"
        const val COLUMN_SERVICE_ID = "service_id"
        const val COLUMN_SERVICE_NAME = "service_name"
        const val COLUMN_NOTIFICATION_TITLE = "notification_title"
        const val COLUMN_NOTIFICATION_CONTENT = "notification_content"
        const val COLUMN_DISCLAIMER_INFO = "disclaimer_info"
        const val COLUMN_DISCLAIMER_CONTENT = "disclaimer_content"
        const val COLUMN_REMARKS = "column_remarks"
    }
}