package global.citytech.finpos.device.weipass.emv.emvkeys

import android.text.TextUtils
import com.wiseasy.emvprocess.KernelInit
import com.wiseasy.emvprocess.bean.CAPKData
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import io.mockk.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by Rishav Chudal on 4/29/20.
 */
class WeipassEmvKeysServiceImplTest {

    private lateinit var SUT: WeipassEmvKeysServiceImpl
    private val exceptionMessage = "This is a test exception"

    @Before
    public fun setUp() {
        MockKAnnotations.init(this)
        this.SUT = WeipassEmvKeysServiceImpl()
        mockKernelInit()
        mockWeipassLogger()
        mockAndroidTextUitls()
    }

    private fun mockKernelInit() {
        mockkStatic(KernelInit::class)
    }

    private fun mockWeipassLogger() {
        mockkObject(WeipassLogger)
        mockkObject(WeipassLogger.Companion)
        every { WeipassLogger.log(any()) } just Runs
    }

    private fun mockAndroidTextUitls() {
        mockkStatic(TextUtils::class)
        every { TextUtils.isEmpty(any()) } returns false
    }

    private fun prepareMockEmvKeysInjectionRequest(): global.citytech.finposframework.hardware.emv.emvkeys.EmvKeysInjectionRequest {
        val emvKeysList = listOf(prepareMockCapk(), prepareMockCapk(), prepareMockCapk())
        return global.citytech.finposframework.hardware.emv.emvkeys.EmvKeysInjectionRequest(
            emvKeysList
        )
    }

    private fun prepareMockCapk(): global.citytech.finposframework.hardware.emv.emvkeys.EmvKey {
        return global.citytech.finposframework.hardware.emv.emvkeys.EmvKey(
            "A000000025",
            "28",
            "00",
            "03",
            "C05B993401063615EF211036DD8154066BE4BBEC7E93A82E83C65E2CFD76E498A6DBF6135C816F606B9564A30D259A9FD5463AA78261223FBB4718EAD4A17347E11BE475BF0DDD9BE9315DCF585D58863A7BCA0E67440586DA098E33047C0DF6F6A1D1BD081BF283321DDF248FDFFA9FB749D0FDA47ADE2E7C0AAA76B146A00A5EBDA270C52832E8132FBC631EAC1120F02215829EB1D852B1969F1C1504A659AB6057C92AF92D981C8171B68E3300E3",
            "01",
            "01",
            "F1B99C16FF415746FF423241E66F17AF0235B2C2",
            "171231"
        )
    }

    /*
     * Emv Keys Injection Tests
     */

    @Test
    public fun injectEmvKeys_WhenCapkListIsProvided_ReturnsSuccessResponse() {
        //Setup
        every { KernelInit.addCAPK(any()) } returns 0
        every { KernelInit.deleteCAPK() } returns 0

        //Run
        val response: DeviceResponse =  SUT.injectEmvKeys(prepareMockEmvKeysInjectionRequest())

        //Verify
        verify { KernelInit.deleteCAPK() }
        assertTrue(prepareMockCapk().rid.isNotEmpty())
        assertTrue(prepareMockCapk().index.isNotEmpty())
        assertTrue(prepareMockCapk().length.isNotEmpty())
        assertTrue(prepareMockCapk().keySignatureId.isNotEmpty())
        assertTrue(prepareMockCapk().hashId.isNotEmpty())
        assertTrue(prepareMockCapk().modulus.isNotEmpty())
        assertTrue(prepareMockCapk().exponent.isNotEmpty())
        assertTrue(prepareMockCapk().expiryDate.isNotEmpty())
        assertTrue(prepareMockCapk().checkSum.isNotEmpty())
        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun injectEmvKeys_WhenCapkListIsEmpty_ReturnsFailureResponse() {
        //Setup
        val injectEmvKeysRequest =
            global.citytech.finposframework.hardware.emv.emvkeys.EmvKeysInjectionRequest(
                emptyList()
            )
        //Run
        val response = SUT.injectEmvKeys(injectEmvKeysRequest)

        //Verify
        assertEquals(Result.FAILURE, response.result)
    }

    @Test
    fun injectEmvKeys_ForEachEmvKeyInjectionInCoreIsZero_ReturnSuccessResponse() {
        //Setup
        val injectEmvKeysRequest = prepareMockEmvKeysInjectionRequest()
        val emvKeyList = injectEmvKeysRequest.emvKeyList
        val slot = slot<CAPKData>()
        every { KernelInit.addCAPK(capture(slot)) } returns 0
        every { KernelInit.deleteCAPK() } returns 0

        //Run
        val response = SUT.injectEmvKeys(prepareMockEmvKeysInjectionRequest())

        //Verify
        verify { KernelInit.deleteCAPK() }
        verify(exactly = emvKeyList.size) { KernelInit.addCAPK(any())}
        val coreCapkData = slot.captured
        assertFalse(coreCapkData.rid.isNullOrEmptyOrBlank())
        assertTrue(coreCapkData.rid.equals(emvKeyList.get(0).rid))

        assertFalse(coreCapkData.publicKeyIndex.isNullOrEmptyOrBlank())
        assertTrue(coreCapkData.publicKeyIndex.equals(emvKeyList.get(0).index))

        assertFalse(coreCapkData.publicKeyAlgorithm.isNullOrEmptyOrBlank())
        assertTrue(coreCapkData.publicKeyAlgorithm.equals(emvKeyList.get(0).keySignatureId))

        assertFalse(coreCapkData.publicKeyHashAlgorithm.isNullOrEmptyOrBlank())
        assertTrue(coreCapkData.publicKeyHashAlgorithm.equals(emvKeyList.get(0).hashId))

        assertFalse(coreCapkData.publicKeyModulus.isNullOrEmptyOrBlank())
        assertTrue(coreCapkData.publicKeyModulus.equals(emvKeyList.get(0).modulus))

        assertFalse(coreCapkData.publicKeyExponent.isNullOrEmptyOrBlank())
        assertTrue(coreCapkData.publicKeyExponent.equals(emvKeyList.get(0).exponent))

        assertFalse(coreCapkData.publicKeyExpiredDate.isNullOrEmptyOrBlank())
        assertTrue(coreCapkData.publicKeyExpiredDate.equals(emvKeyList.get(0).expiryDate))

        assertFalse(coreCapkData.checkValue.isNullOrEmptyOrBlank())
        assertTrue(coreCapkData.checkValue.equals(emvKeyList.get(0).checkSum))

        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun injectEmvKeys_ExceptionInEmvKeysInjectionInCore_ReturnsSuccessResponse() {
        //Setup
        every { KernelInit.deleteCAPK() } returns 0
        every { KernelInit.addCAPK(any()) } throws Exception(exceptionMessage)

        //Run
        val response = SUT.injectEmvKeys(prepareMockEmvKeysInjectionRequest())

        //Verify
        verify { KernelInit.deleteCAPK() }
        verify (exactly = prepareMockEmvKeysInjectionRequest().emvKeyList.size){
            WeipassLogger.log("Exception in Emv Key injection ::: ".plus(exceptionMessage))
        }
        assertEquals(Result.SUCCESS, response.result)
    }

    /*
     * Emv Keys Erase Tests
     */
    @Test
    fun eraseAllEmvKeys_WhenEraseEmvKeysResultInCoreIsZero_ReturnsSuccessResponse() {
        //Setup
        every { KernelInit.deleteCAPK() } returns 0

        //Run
        val response = SUT.eraseAllEmvKeys()

        //Verify
        verify { KernelInit.deleteCAPK() }
        verify { WeipassLogger.log("Erase All Emv Keys result ::: ".plus(0))}
        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun eraseAllEmvKeys_WhenEraseEmvKeysResultInCoreIsNonZero_ReturnsFailureResponse() {
        //Setup
        every { KernelInit.deleteCAPK() } returns -1

        //Run
        val response = SUT.eraseAllEmvKeys()

        //Verify
        verify { KernelInit.deleteCAPK() }
        verify { WeipassLogger.log("Erase All Emv Keys result ::: ".plus(-1))}
        verify { WeipassLogger.log("Couldn't clear Emv Keys") }
        assertEquals(Result.FAILURE, response.result)
    }
}