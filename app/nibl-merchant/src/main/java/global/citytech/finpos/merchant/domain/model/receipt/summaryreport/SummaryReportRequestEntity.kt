package global.citytech.finpos.merchant.domain.model.receipt.summaryreport

import global.citytech.finposframework.hardware.io.printer.PrinterService
import global.citytech.finposframework.repositories.ApplicationRepository
import global.citytech.finposframework.repositories.ReconciliationRepository

/**
 * Created by Saurav Ghimire on 3/30/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


data class SummaryReportRequestEntity(
    var applicationRepository: ApplicationRepository? = null,
    var reconciliationRepository: ReconciliationRepository? = null,
    var printerService: PrinterService? = null
)
