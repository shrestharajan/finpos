package global.citytech.finpos.merchant.presentation.dtg

data class GpsLocation(
    private var northSouth: String? = null,
    private var latitudeDegree: String? = null,
    private var latitudeMinute: String? = null,
    private var latitudeSecond: String? = null,
    private var eastWest: String? = null,
    private var longitudeDegree: String? = null,
    private var longitudeMinute: String? = null,
    private var longitudeSecond: String? = null
) {

     fun getLatitude(): String {
        val stringBuilder = StringBuilder()
        stringBuilder.append(northSouth)
        stringBuilder.append(fillZero(latitudeDegree, 2))
        stringBuilder.append(fillZero(latitudeMinute, 2))
        stringBuilder.append(fillZero(latitudeSecond, 2))
        return stringBuilder.toString()
    }

    private fun fillZero(s: String?, requiredLength: Int): String? {
        return if (s!!.length < requiredLength) {
            val stringBuilder = StringBuilder()
            val numberOfZerosRequired = requiredLength - s.length
            for (i in 0 until numberOfZerosRequired) {
                stringBuilder.append("0")
            }
            stringBuilder.append(s)
            stringBuilder.toString()
        } else {
            s
        }
    }

     fun getLongitude(): String {
        val stringBuilder = StringBuilder()
        stringBuilder.append(eastWest)
        stringBuilder.append(fillZero(longitudeDegree, 3))
        stringBuilder.append(fillZero(longitudeMinute, 2))
        stringBuilder.append(fillZero(longitudeSecond, 2))
        return stringBuilder.toString()
    }

    fun getLocation(): String? {
        return getLatitude() + getLongitude()
    }
}