package global.citytech.finpos.nibl.iso8583.requestsender.purchase;

import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.iso8583.NIBLMessagePayloadBuilder;
import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.StringUtils;


/**
 * Modified by Rishav Chudal on 10/15/20.
 */
public abstract class PurchaseRequestSender extends NIBLMessageSenderTemplate {
    private Logger logger = Logger.getLogger(PurchaseRequestSender.class.getName());

    public PurchaseRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Financial,
                        MTI.MessageFunction.Request,
                        MTI.Originator.Acquirer);
        TransactionIsoRequest purchaseRequest = (TransactionIsoRequest) this.context.getRequest();
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);

        msg.addField(
                new DataElement(
                        DataElement.AMOUNT,
                        HelperUtils.toISOAmount(purchaseRequest.getTransactionAmount(), 12)
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE,
                        purchaseRequest.getPosConditionCode()
                )
        );
        if (!StringUtils.isEmpty(purchaseRequest.getPinBlock()))
            msg.addField(
                    new DataElement(
                            DataElement.PIN_BLOCK,
                            purchaseRequest.getPinBlock()
                    )
            );

        msg.addField(
                new DataElement(
                        DataElement.PRIVATE_USE_FIELD_62,
                        purchaseRequest.getInvoiceNumber()
                )
        );
        msg = addConditionalFields(msg, purchaseRequest);
        MessagePayload.Builder builder = new NIBLMessagePayloadBuilder(this.specInfo, msg);
        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());
        return builder.build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.PURCHASE.getCode();
    }

    @Override
    protected String getFunctionCode() {
        return "0011"; //TODO manage
    }

    public abstract Iso8583Msg addConditionalFields(
            Iso8583Msg msg,
            TransactionIsoRequest transactionIsoRequest
    );
}
