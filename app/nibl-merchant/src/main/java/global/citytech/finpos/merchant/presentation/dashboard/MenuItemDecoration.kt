package global.citytech.finpos.merchant.presentation.dashboard

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView


/**
 * Created by Saurav Ghimire on 1/19/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
class MenuItemDecoration : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        if (parent.getChildAdapterPosition(view) % 2 == 0) {
            outRect.right = 10
        }
    }
}