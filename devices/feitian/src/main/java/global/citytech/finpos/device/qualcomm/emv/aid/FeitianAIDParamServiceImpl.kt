package global.citytech.finpos.device.qualcomm.emv.aid

import android.content.Context
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.emv.IActionFlag
import com.ftpos.library.smartpos.errcode.ErrCode
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_EMPTY_AID_LIST
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_FAILURE_AID_ERASE
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_FAILURE_AID_INJECTION
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_SUCCESS_AID_ERASE
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_SUCCESS_AID_INJECTION
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result.FAILURE
import global.citytech.finposframework.hardware.common.Result.SUCCESS
import global.citytech.finposframework.hardware.emv.aid.*

/**
 * Created by Rishav Chudal on 4/5/21.
 */
class FeitianAIDParamServiceImpl constructor(private val context: Context): AidParametersService,
    FeitianLogger.Listener {

    private val emv = Emv.getInstance(context.applicationContext)
    private var finalSuccessCount = 0
    private lateinit var deviceResponse: DeviceResponse
    private lateinit var emvKernelConfigHandler: FeitianEMVKernelConfigHandler
    private lateinit var payWaveKernelConfigHandler: FeitianPayWaveKernelConfigHandler
    private lateinit var payPassKernelConfigHandler: FeitainPayPassKernelConfigHandler
    private lateinit var qpbocKernelConfigHandler: FeitianQPBOCKernelConfigHandler
    private val logger = FeitianLogger(this)

    override fun injectAid(request: AidInjectionRequest): DeviceResponse {
        deviceResponse = DeviceResponse(FAILURE, MSG_FAILURE_AID_INJECTION)
        if (request.aidParametersList.isEmpty()) {
            onAIDParametersListEmpty()
        } else {
            onAIDParametersListNotEmpty(request)
        }
        return deviceResponse
    }

    override fun eraseAllAid(): DeviceResponse {
        var eraseAIDResponse = DeviceResponse(FAILURE, MSG_FAILURE_AID_ERASE)
        logger.logWithClassName("***** Erasing all AID *****")
        val finalEraseResult = eraseEMVAppParameters() + eraseEMVCLAppParameters()
        if (finalEraseResult == ErrCode.ERR_SUCCESS) {
            logger.logWithClassName("Erase all AID success...")
            eraseAIDResponse = DeviceResponse(SUCCESS, MSG_SUCCESS_AID_ERASE)
        } else {
            logger.logWithClassName("Erase all AID failed...")
        }
        return eraseAIDResponse
    }

    private fun onAIDParametersListEmpty() {
        logger.logWithClassName("AID list is empty...")
        deviceResponse = DeviceResponse(FAILURE, MSG_EMPTY_AID_LIST)
    }

    private fun onAIDParametersListNotEmpty(request: AidInjectionRequest) {
        eraseAllAid()
        proceedForEachAIDInjection(request)
        if (finalSuccessCount > 0) {
            deviceResponse = DeviceResponse(SUCCESS, MSG_SUCCESS_AID_INJECTION)
        }
    }

    private fun eraseEMVAppParameters(): Int {
        val resultClearEMVAppParams = emv.manageEmvAppParameters(
            IActionFlag.CLEAR,
            null
        )
        logger.logWithClassName("Erase EMV App Params result ::: ".plus(resultClearEMVAppParams))
        return resultClearEMVAppParams
    }

    private fun eraseEMVCLAppParameters(): Int {
        val resultClearEMVCLAppParams = emv.manageEmvclAppParameters(
            IActionFlag.CLEAR,
            null
        )
        logger.logWithClassName("Erase EMV CL App Params result ::: ".plus(resultClearEMVCLAppParams))
        return resultClearEMVCLAppParams
    }

    private fun proceedForEachAIDInjection(injectionRequest: AidInjectionRequest) {
        for (aidParameters in injectionRequest.aidParametersList) {
            try {
                checkAndProceedWithCardBrand(aidParameters, injectionRequest)
            } catch (ex: Exception) {
                logger.logWithClassName("Exception in injecting AID ::: ".plus(aidParameters.aid))
                logger.logWithClassName("Exception Message ::: ".plus(ex.message))
            }
            logger.logWithClassName(
                "AID injected ::: "
                    .plus(finalSuccessCount)
                    .plus("/")
                    .plus(injectionRequest.aidParametersList.size)
            )
        }
    }

    private fun checkAndProceedWithCardBrand(
        aidParameters: AidParameters,
        injectionRequest: AidInjectionRequest
    ) {
        if (aidParameters.aid.contains(CardBrand.VISA.rid, true)) {
            proceedVisaAIDForInjection(aidParameters, injectionRequest)
        } else if (aidParameters.aid.contains(CardBrand.MASTERCARD.rid, true)) {
            proceedMasterCardAIDForInjection(aidParameters, injectionRequest)
        } else if (aidParameters.aid.contains(CardBrand.UNION_PAY.rid, true)) {
            proceedUnionPayAIDForInjection(aidParameters, injectionRequest)
        } else if (aidParameters.aid.contains(CardBrand.MADA.rid, true)) {
            proceedMadaAIDForInjection(aidParameters, injectionRequest)
        } else {
            proceedDefaultAIDForInjection(aidParameters, injectionRequest)
        }
    }

    /**
     * Visa
     */
    private fun proceedVisaAIDForInjection(
        aidParameters: AidParameters,
        injectionRequest: AidInjectionRequest
    ) {
        logger.logWithClassName("********** Visa AID **********")
        val resultEMVKernelConfig = proceedForEMVKernelConfigurationInjection(
            aidParameters,
            injectionRequest
        )
        if (resultEMVKernelConfig == ErrCode.ERR_SUCCESS) {
            proceedForPayWaveKernelConfigurationsInjection(aidParameters, injectionRequest)
        }
    }

    private fun proceedForPayWaveKernelConfigurationsInjection(
        aidParameters: AidParameters,
        injectionRequest: AidInjectionRequest
    ) {
        payWaveKernelConfigHandler = FeitianPayWaveKernelConfigHandler(
            context,
            emv,
            aidParameters,
            injectionRequest.emvParametersRequest,
            injectionRequest.supportedTransactionTypes
        )
        finalSuccessCount += payWaveKernelConfigHandler.injectPayWaveKernelConfigurations()
    }

    /**
     * MasterCard
     */
    private fun proceedMasterCardAIDForInjection(
        aidParameters: AidParameters,
        injectionRequest: AidInjectionRequest
    ) {
        logger.logWithClassName("********** MasterCard AID **********")
        val resultEMVKernelConfig = proceedForEMVKernelConfigurationInjection(
            aidParameters,
            injectionRequest
        )
        if (resultEMVKernelConfig == ErrCode.ERR_SUCCESS) {
            proceedForPayPassKernelConfigurationsInjection(aidParameters, injectionRequest)
        }
    }

    private fun proceedForPayPassKernelConfigurationsInjection(
        aidParameters: AidParameters,
        injectionRequest: AidInjectionRequest
    ) {
        payPassKernelConfigHandler = FeitainPayPassKernelConfigHandler(
            context,
            emv,
            aidParameters,
            injectionRequest.emvParametersRequest,
            injectionRequest.supportedTransactionTypes
        )
        finalSuccessCount += payPassKernelConfigHandler.injectPayPassKernelConfigurations()
    }

    /**
     * Union Pay
     */
    private fun proceedUnionPayAIDForInjection(
        aidParameters: AidParameters,
        injectionRequest: AidInjectionRequest
    ) {
        logger.logWithClassName("********** AID is Union Pay **********")
        val resultEMVKernelConfig = proceedForEMVKernelConfigurationInjection(
            aidParameters,
            injectionRequest
        )
        if (resultEMVKernelConfig == ErrCode.ERR_SUCCESS) {
            proceedForQPBOCKernelConfigurationsInjection(aidParameters, injectionRequest)
        }
    }

    private fun proceedForQPBOCKernelConfigurationsInjection(
        aidParameters: AidParameters,
        injectionRequest: AidInjectionRequest
    ) {
        qpbocKernelConfigHandler = FeitianQPBOCKernelConfigHandler(
            context,
            emv,
            aidParameters,
            injectionRequest.emvParametersRequest,
            injectionRequest.supportedTransactionTypes
        )
        finalSuccessCount += qpbocKernelConfigHandler.injectQPBOCKernelConfigurations()
    }

    /**
     * Mada
     */
    private fun proceedMadaAIDForInjection(
        aidParameters: AidParameters,
        injectionRequest: AidInjectionRequest
    ) {
        logger.logWithClassName("********** MADA AID **********")
        val resultEMVKernelConfig = proceedForEMVKernelConfigurationInjection(
            aidParameters,
            injectionRequest
        )
        if (resultEMVKernelConfig == ErrCode.ERR_SUCCESS) {
            proceedForMadaSupportedKernelsConfigurationInjection(aidParameters, injectionRequest)
        }
    }

    private fun proceedForMadaSupportedKernelsConfigurationInjection(
        aidParameters: AidParameters,
        injectionRequest: AidInjectionRequest
    ) {
        when (aidParameters.aid) {
            CardBrandAID.MADA_MC.aid -> proceedMadaMasterCardAIDForInjection(
                aidParameters,
                injectionRequest
            )
            CardBrandAID.MADA_VS.aid -> proceedMadaVisaAIDForInjection(
                aidParameters,
                injectionRequest
            )
        }
    }

    private fun proceedMadaMasterCardAIDForInjection(
        aidParameters: AidParameters,
        injectionRequest: AidInjectionRequest
    ) {
        logger.logWithClassName("Mada card co-branded with MasterCard...")
        proceedForPayPassKernelConfigurationsInjection(aidParameters, injectionRequest)
    }

    private fun proceedMadaVisaAIDForInjection(
        aidParameters: AidParameters,
        injectionRequest: AidInjectionRequest
    ) {
        logger.logWithClassName("Mada card co-branded with Visa...")
        proceedForPayWaveKernelConfigurationsInjection(aidParameters, injectionRequest)
    }

    /**
     * Default AID
     */
    private fun proceedDefaultAIDForInjection(
        aidParameters: AidParameters,
        injectionRequest: AidInjectionRequest
    ) {
        logger.logWithClassName("********** Unknown AID **********")
        val resultEMVKernelConfig = proceedForEMVKernelConfigurationInjection(
            aidParameters,
            injectionRequest
        )
        if (resultEMVKernelConfig == ErrCode.ERR_SUCCESS) {
            finalSuccessCount += 1
        }
    }

    private fun proceedForEMVKernelConfigurationInjection(
        aidParameters: AidParameters,
        injectionRequest: AidInjectionRequest
    ): Int {
        emvKernelConfigHandler = FeitianEMVKernelConfigHandler(
            context,
            emv,
            aidParameters,
            injectionRequest.emvParametersRequest
        )
        return emvKernelConfigHandler.injectEMVKernelConfigurations()
    }

    override fun getClassName(): String {
        return FeitianAIDParamServiceImpl::class.java.simpleName
    }
}