package global.citytech.nepsadmin.data.response

data class SwitchPublicKey(
    var index: String? = null,
    var key: String? = null
)