package global.citytech.finpos.device.qualcomm.emv.aid

import android.content.Context
import com.ftpos.library.smartpos.bean.CMastercardBean
import com.ftpos.library.smartpos.device.Device
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.emv.EmvTags
import com.ftpos.library.smartpos.emv.IActionFlag
import global.citytech.common.extensions.defaultOrEmptyValue
import global.citytech.common.extensions.isNullOrEmptyOrBlank
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.DO_NOT_CHECK_FLAG
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finpos.device.qualcomm.utils.FeitianUtils
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.PAYPASS_KERNEL_ID
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.TRANSACTION_TYPE_PURCHASE_SALE
import global.citytech.finposframework.hardware.emv.aid.AidParameters
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import global.citytech.finposframework.utility.AmountUtils

/**
 * Created by Rishav Chudal on 4/5/21.
 * PayPass Kernel - MasterCard Contactless Kernel - Kernel 02
 */
class FeitainPayPassKernelConfigHandler constructor(
    private val context: Context,
    private val emv: Emv,
    private val aidParameters: AidParameters,
    private val emvParameters: EmvParametersRequest,
    private val supportedTransactionTypes: List<String>
) : FeitianLogger.Listener {

    private var finalResult = 0
    private val logger = FeitianLogger(this)
    private val device: Device = Device.getInstance(context.applicationContext)

    fun injectPayPassKernelConfigurations(): Int {
        var successCount = 0
        supportedTransactionTypes.forEach {
            injectKernelConfigurationsForTransType(it)
        }
        if (finalResult == 0) {
            successCount++
        }
        return successCount
    }

    private fun injectKernelConfigurationsForTransType(transactionType: String) {
        val configurationsByteArray = preparePayPassKernelConfigurationsInByteArray(
            transactionType
        )
        val resultInjectCLParams = emv.manageEmvclAppParameters(
            IActionFlag.ADD,
            configurationsByteArray
        )
        logger.logWithClassName(
            "PayPass kernel config injection result ::: ".plus(resultInjectCLParams)
        )
        finalResult += resultInjectCLParams
    }

    /**
     * Note : Parameters that weren't set :::
     * - MSD CVM Capability Required
     * - MSD NO CVM Capability Required
     */
    private fun preparePayPassKernelConfigurationsInByteArray(
        transactionType: String
    ): ByteArray? {
        logger.logWithClassName("***** Contactless Params *****")
        val cMastercardBean = CMastercardBean()

        val aid9F06 = aidParameters.aid.defaultOrEmptyValue()
        logger.debugWithClassName("AID ::: ".plus(aid9F06))
        cMastercardBean.setmAID_9F06(aid9F06)

        val kernelId = PAYPASS_KERNEL_ID //hardcoded as the AID parameters don't have this parameter
        this.logger.debugWithClassName("Kernel ID ::: ".plus(kernelId))
        cMastercardBean.setmKernelID_1F60(kernelId)

        var transType = transactionType
        if (transType.isNullOrEmptyOrBlank()) {
            transType = TRANSACTION_TYPE_PURCHASE_SALE
        }
        logger.debugWithClassName("Transaction Type 9C ::: ".plus(transType))
        cMastercardBean.setmTransType_9C(transType)

        val appSelectionIndicator =
            DO_NOT_CHECK_FLAG //hardcoded as the AID parameters don't have this parameter
        logger.debugWithClassName("App Selection Indicator ::: ".plus(appSelectionIndicator))
        cMastercardBean.setmASI_1F14(appSelectionIndicator)

        val transCurrencyCode = emvParameters.transactionCurrencyCode.defaultOrEmptyValue()
        logger.debugWithClassName("Transaction Currency Code ::: ".plus(transCurrencyCode))
        cMastercardBean.setmTransCurrencyCode_5F2A(transCurrencyCode)

        val transCurrencyExponent = emvParameters.transactionCurrencyExponent.defaultOrEmptyValue()
        logger.debugWithClassName("Transaction Currency Exponent ::: ".plus(transCurrencyExponent))
        cMastercardBean.setmTransCurrencyExponent_5F36(transCurrencyExponent)

        val acquirerId = aidParameters.acquirerId.defaultOrEmptyValue()
        logger.debugWithClassName("Acquirer Id ::: ".plus(acquirerId))
        cMastercardBean.setmAcquirerID_9F01(acquirerId)

        val addTerminalCapabilities: String =
            emvParameters.additionalTerminalCapabilities.defaultOrEmptyValue()
        logger.debugWithClassName("Additional Terminal Capabilities ::: ".plus(addTerminalCapabilities))
        cMastercardBean.setmAdditionalTerminalCapabilities_9F40(addTerminalCapabilities)

        val appVersionNumber: String = aidParameters.terminalAidVersion.defaultOrEmptyValue()
        logger.debugWithClassName("Application Version Number ::: ".plus(appVersionNumber))
        cMastercardBean.setmAppVersionNumber_9F09(appVersionNumber)

        val cardDataInputCapability = aidParameters.clCardDataInputCapability.defaultOrEmptyValue()
        logger.debugWithClassName("Card Data Input Capability ::: ".plus(cardDataInputCapability))
        cMastercardBean.setmCardDataInputCapability_DF8117(cardDataInputCapability)

        val cvmCapabilityRequired = aidParameters.cvmCapCvmRequired.defaultOrEmptyValue()
        logger.debugWithClassName("CVM Capability Required ::: ".plus(cvmCapabilityRequired))
        cMastercardBean.setmCVMCapability_Required_DF8118(cvmCapabilityRequired)

        val cvmCapabilityNotRequired = aidParameters.cvmCapNoCvmRequired.defaultOrEmptyValue()
        logger.debugWithClassName("CVM Capability Not Required ::: ".plus(cvmCapabilityNotRequired))
        cMastercardBean.setmCVMCapability_NotRequired_DF8119(cvmCapabilityNotRequired)

        val defaultUdol = aidParameters.udol.defaultOrEmptyValue()
        logger.debugWithClassName("Default UDOL ::: ".plus(defaultUdol))
        cMastercardBean.setmDefaultUDOL_DF811A(defaultUdol)

        val ifdSerialNumber = FeitianUtils.retrieveDeviceSerialNumber(device)
        logger.debugWithClassName("IFD Serial Number ::: ".plus(ifdSerialNumber))
        cMastercardBean.setmIFDSerial_9F1E(ifdSerialNumber)

        val kernelConfig = aidParameters.kernelConfig.defaultOrEmptyValue()
        logger.debugWithClassName("Kernel Config ::: ".plus(kernelConfig))
        cMastercardBean.setmKernelConfiguration_DF811B(kernelConfig)

        val msdVersionNumber = aidParameters.msdAppVersion.defaultOrEmptyValue()
        logger.debugWithClassName("Mag Stripe Version Number ::: ".plus(msdVersionNumber))
        cMastercardBean.setmMagStripeApplicationVersionNumber_9F6D(msdVersionNumber)

        val merchantCategoryCode = emvParameters.merchantCategoryCode.defaultOrEmptyValue()
        logger.debugWithClassName("Merchant Category Code ::: ".plus(merchantCategoryCode))
        cMastercardBean.setmMerchantCategoryCode_9F15(merchantCategoryCode)

        val merchantId = emvParameters.merchantIdentifier.defaultOrEmptyValue()
        logger.debugWithClassName("Merchant Id ::: ".plus(merchantId))
        cMastercardBean.setmMerchantID_9F16(merchantId)

        val merchantNameLocation: String = emvParameters.merchantName.defaultOrEmptyValue()
        logger.debugWithClassName("Merchant Name and Location ::: ".plus(merchantNameLocation))
        cMastercardBean.setmMerchantNameLocation_9F4E(merchantNameLocation)

        var contactlessFloorLimit = aidParameters.contactlessFloorLimit.defaultOrEmptyValue()
        logger.debugWithClassName("Reader Contactless Floor Limit ::: ".plus(contactlessFloorLimit))
        contactlessFloorLimit = AmountUtils.toISOFormattedLowerCurrency(contactlessFloorLimit)
        logger.debugWithClassName(
            "Formatted Reader Contactless Floor Limit ::: ".plus(contactlessFloorLimit)
        )
        cMastercardBean.setmReaderContactlessFloorLimit_DF8123(contactlessFloorLimit)

        var onDeviceCvmTransactionLimit = aidParameters.cdCvmLimit.defaultOrEmptyValue()
        onDeviceCvmTransactionLimit =
            AmountUtils.toISOFormattedLowerCurrency(onDeviceCvmTransactionLimit)
        logger.debugWithClassName(
            "On Device CVM Transaction Limit ::: ".plus(onDeviceCvmTransactionLimit)
        )
        cMastercardBean.setmOndeviceCVMTransactionLimit_DF8125(onDeviceCvmTransactionLimit)

        var noOnDeviceCvmTransactionLimit = aidParameters.noCdCvmLimit.defaultOrEmptyValue()
        noOnDeviceCvmTransactionLimit =
            AmountUtils.toISOFormattedLowerCurrency(noOnDeviceCvmTransactionLimit)
        logger.debugWithClassName(
            "No On Device CVM Transaction Limit ::: ".plus(noOnDeviceCvmTransactionLimit)
        )
        cMastercardBean.setmNoOndeviceCVMTransactionLimit_DF8124(noOnDeviceCvmTransactionLimit)

        var readerCVMLimit = aidParameters.cvmLimit.defaultOrEmptyValue()
        logger.debugWithClassName("Reader Contactless CVM Limit ::: ".plus(readerCVMLimit))
        readerCVMLimit = AmountUtils.toISOFormattedLowerCurrency(readerCVMLimit)
        logger.debugWithClassName("Formatted Reader Contactless CVM Limit ::: ".plus(readerCVMLimit))
        cMastercardBean.setmCVMRequiredLimit_DF8126(readerCVMLimit)

        val securityCapability = aidParameters.clSecurityCapability.defaultOrEmptyValue()
        logger.debugWithClassName("Security Capability ::: ".plus(securityCapability))
        cMastercardBean.setmSecurityCapability_DF811F(securityCapability)

        val terminalCapabilities = emvParameters.terminalCapabilities.defaultOrEmptyValue()
        logger.debugWithClassName("Terminal Capabilities ::: ".plus(terminalCapabilities))
        cMastercardBean.setmTerminalCapabilities_9F33(terminalCapabilities)

        val terminalCountryCode = emvParameters.terminalCountryCode.defaultOrEmptyValue()
        logger.debugWithClassName("Terminal Country Code ::: ".plus(terminalCountryCode))
        cMastercardBean.setmTerminalCountryCode_9F1A(terminalCountryCode)

        val terminalId = emvParameters.terminalId.defaultOrEmptyValue()
        logger.debugWithClassName("Terminal Id ::: ".plus(terminalId))
        cMastercardBean.setmTerminalID_9F1C(terminalId)

        val terminalType = emvParameters.terminalType.defaultOrEmptyValue()
        logger.debugWithClassName("Terminal Type ::: ".plus(terminalType))
        cMastercardBean.setmTerminalType_9F35(terminalType)

        val contactlessTACDenial = aidParameters.clTacDenial.defaultOrEmptyValue()
        logger.debugWithClassName(
            "Contactless Terminal Action Code Denial ::: ".plus(contactlessTACDenial)
        )
        cMastercardBean.setmTerminalActionCodeDenial_DF8121(contactlessTACDenial)

        val contactlessTACOnline = aidParameters.clTacOnline.defaultOrEmptyValue()
        logger.debugWithClassName(
            "Contactless Terminal Action Code Online ::: ".plus(contactlessTACOnline)
        )
        cMastercardBean.setmTerminalActionCodeOnline_DF8122(contactlessTACOnline)

        val contactlessTACDefault = aidParameters.clTacDefault.defaultOrEmptyValue()
        logger.debugWithClassName(
            "Contactless Terminal Action Code Default ::: ".plus(contactlessTACDefault)
        )
        cMastercardBean.setmTerminalActionCodeDefault_DF8120(contactlessTACDefault)

        cMastercardBean.setmKernelID_DF810C(kernelId)

        val riskManagementData = aidParameters.terminalRiskManagementData.defaultOrEmptyValue()
        if (!riskManagementData.isNullOrEmptyOrBlank() && riskManagementData.length >= 16) {
            val riskManagementTLV = EmvTags.TAG_EMV_TERMINAL_RISK_MANAGEMENT_DATA
                .plus("08")
                .plus(riskManagementData.substring(0, 16))
            logger.debugWithClassName("Risk Management Data ::: ".plus(riskManagementData))
            cMastercardBean.setmExtraTags(riskManagementTLV)
        }

        return cMastercardBean.toTlvByteArray()
    }

    override fun getClassName(): String {
        return FeitainPayPassKernelConfigHandler::class.java.simpleName
    }
}