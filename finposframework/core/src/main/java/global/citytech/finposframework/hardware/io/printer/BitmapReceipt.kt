package global.citytech.finposframework.hardware.io.printer

/**
 * Created by Rishav Chudal on 6/19/20.
 */
class BitmapReceipt constructor(bitmap: Any?):
    Printable {
    private var bitmap: Any? = null
    private val isQrCode = false
    private val isAllow = true
    private val isImage = true
    private val isFeedPaper = false
    private val isBase64Image = false
    private val isBarCode = false

    init {
        this.bitmap = bitmap
    }

    override fun isAllow(): Boolean {
        return isAllow
    }

    override fun isImage(): Boolean {
        return isImage
    }

    override fun isQrCode(): Boolean {
        return isQrCode
    }

    override fun isFeedPaper(): Boolean {
        return isFeedPaper
    }

    override fun isBase64Image(): Boolean {
        return isBase64Image;
    }

    override fun isBarCode(): Boolean {
        return isBarCode
    }

    override fun getData(): Any? {
        return bitmap
    }
}