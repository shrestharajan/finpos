package global.citytech.finpos.merchant.presentation.alertdialogs

import android.app.Activity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import com.bumptech.glide.Glide
import com.google.android.material.button.MaterialButton
import global.citytech.finpos.merchant.R
import kotlinx.android.synthetic.main.dialog_pos_entry_selection.view.*
import java.lang.ref.WeakReference

/**
 * Created by Rishav Chudal on 6/26/20.
 */
class PosEntrySelectionDialog<T> constructor(
    val amount:Double,
    val purchaseType:String,
    val posEntrySelection: PosEntrySelection,
    val listener: Listener
) {
    private lateinit var tvMessage: TextView
    private lateinit var btnCancel: AppCompatTextView
    private lateinit var btnManual: MaterialButton
    private lateinit var tvAmount: AppCompatTextView
    private lateinit var tvPurchaseType: AppCompatTextView
    private lateinit var ivPICC: ImageView
    private var alertDialog: AlertDialog? = null

    fun show(activityWeakReference: WeakReference<Activity>) {
        val dialogBuilder = AlertDialog.Builder(activityWeakReference.get() as Activity,R.style.FullScreenDialog)
        val inflater = activityWeakReference.get()!!.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_pos_entry_selection, null)
        dialogBuilder.setView(dialogView)
        initViews(dialogView)
        setMessage()
        setPICCImage(activityWeakReference)
        setManualButton()
        handleClickEvents()
        alertDialog = dialogBuilder.create()
        alertDialog!!.window!!.attributes.windowAnimations = R.style.SlidingDialogAnimation
        alertDialog!!.setCancelable(false)
        alertDialog!!.show()
    }

    fun isStillVisible(): Boolean {
        return (alertDialog != null && alertDialog!!.isShowing)
    }

    fun hideDialog() {
        if (isStillVisible()) {
            alertDialog!!.dismiss()
        }
    }

    private fun initViews(dialog: View) {
        tvMessage = dialog.tv_progress_msg
        btnCancel = dialog.btn_cancel
        btnManual = dialog.btn_manual
        ivPICC = dialog.img_contactless
        tvAmount = dialog.tv_amount
        tvPurchaseType = dialog.tv_purchase_type
    }

    private fun setMessage() {
        tvMessage.text = posEntrySelection.posEntryMessage
        tvAmount.text = amount.toString()
        tvPurchaseType.text = purchaseType
    }

    private fun setPICCImage(activityWeakReference: WeakReference<Activity>) {
        if (posEntrySelection.isPICCAllowed) {
            ivPICC.visibility = View.VISIBLE
            Glide.with(activityWeakReference.get()!!).load("file:///android_asset/images/finpos_swipe.gif")
                .into(ivPICC)
        } else {
            ivPICC.visibility = View.GONE
        }
    }

    private fun setManualButton() {
        if (posEntrySelection.isManualAllowed) {
            btnManual.visibility = View.VISIBLE
        } else {
            btnManual.visibility = View.GONE
        }
    }

    private fun handleClickEvents() {
        btnManual.setOnClickListener {
            hideDialog()
            this.listener.onManualButtonClicked()
        }

        btnCancel.setOnClickListener {
            hideDialog()
            this.listener.onCancelButtonClicked()
        }
    }

    interface Listener {
        fun onManualButtonClicked()
        fun onCancelButtonClicked()
    }

    interface QrListener {
        fun onQrButtonClicked()
    }

    interface MobilePayListener {
        fun onMobilePayButtonClicked()
    }
}