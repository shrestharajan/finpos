package global.citytech.finpos.merchant.presentation.utils

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import global.citytech.finpos.merchant.R
import kotlinx.android.synthetic.main.custom_progress.view.*
import java.lang.ref.WeakReference

/**
 * Created by Saurav Ghimire on 6/22/21.
 * sauravnghimire@gmail.com
 */


class CustomProgressDialog(val context: WeakReference<Context>, val resTheme: Int? = null) {

    private var progressView: View? = null
    var progressDialog: AlertDialog? = null


    init {
        context.get().let {
            var dialogBuilder = AlertDialog.Builder(it!!)
            if(resTheme != null)
                dialogBuilder = AlertDialog.Builder(it!!, resTheme)
            progressView = LayoutInflater.from(it!!)
                .inflate(R.layout.custom_progress, null)
            Glide.with(it!!).load("file:///android_asset/images/loading.gif")
                .into(progressView!!.iv_progress)
            dialogBuilder.setView(progressView!!)
            dialogBuilder.setCancelable(false)
            progressDialog = dialogBuilder.create()
        }
    }

    fun show(message: String? = "") {
        if(progressDialog != null) {
            progressView!!.tv_message.text = message
            progressDialog!!.show()
        }
    }

    fun hide(){
        if(progressDialog !=null && progressDialog!!.isShowing){
            progressDialog!!.dismiss()
        }
    }

    fun dismissDialog() {
        hide()
        progressDialog = null
    }

}
