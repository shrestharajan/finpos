package global.citytech.finpos.nibl.admin.service

import android.content.Context
import android.os.Build
import android.util.Log
import global.citytech.finpos.core.api.CoreFactory
import global.citytech.finpos.core.api.DeviceServiceType
import global.citytech.finpos.core.api.common.DeviceService
import java.util.*

/**
 * Created by Rishav Chudal on 4/20/20.
 */

class AppCoreFactory {
    companion object {
        fun getCore(context: Context, coreServiceType: DeviceServiceType): DeviceService {
            val manufacturer = getDeviceManufacturer()
            val className: String = "global.citytech.finpos.core."
                .plus(manufacturer.toLowerCase(Locale.ROOT))
                .plus(".Device")
                .plus(manufacturer)
                .plus("Factory")

            try {
                val clazz: Class<*> = Class.forName(className)
                val coreFactory: CoreFactory = clazz.newInstance() as CoreFactory
                return coreFactory.getCore(context, coreServiceType)!!
            } catch (ex: Exception) {
                Log.i("corelogger", ex.message!!)
            }
            throw NullPointerException("Core is null")
        }

        private fun getDeviceManufacturer(): String {
            val manufacturer: String = Build.MANUFACTURER.toLowerCase(Locale.ROOT);
            Log.i("corelogger", "Manufacturer ::: ".plus(manufacturer))
            manufacturer.replace(" ", "")
            return manufacturer.substring(0, 1).toUpperCase(Locale.ROOT)
                .plus(manufacturer.substring(1))
        }


    }
}