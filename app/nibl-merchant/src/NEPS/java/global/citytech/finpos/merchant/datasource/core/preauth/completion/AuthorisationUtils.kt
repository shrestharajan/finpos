package global.citytech.finpos.merchant.datasource.core.preauth.completion

import global.citytech.finpos.merchant.presentation.model.preauth.completion.AuthorisationCompletionRequestItem
import global.citytech.finpos.processor.neps.auth.completion.AuthorisationCompletionRequest


/**
 * Created by Saurav Ghimire on 2/15/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
object AuthorisationUtils {
    fun prepareAuthorisationCompletionRequest(authorisationCompletionRequestItem: AuthorisationCompletionRequestItem): AuthorisationCompletionRequest {
        val authorisationCompletionRequest =
            AuthorisationCompletionRequest(authorisationCompletionRequestItem.transactionType)
        authorisationCompletionRequest.amount = authorisationCompletionRequestItem.amount
        authorisationCompletionRequest.originalRRN =
            authorisationCompletionRequestItem.originalData
        return authorisationCompletionRequest
    }
}