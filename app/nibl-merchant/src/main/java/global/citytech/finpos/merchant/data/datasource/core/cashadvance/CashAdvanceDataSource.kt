package global.citytech.finpos.merchant.data.datasource.core.cashadvance

import global.citytech.finpos.merchant.domain.model.cashadvance.CashAdvanceResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.cashadvance.CashAdvanceRequestItem
import global.citytech.finpos.merchant.presentation.model.cashadvance.ManualCashAdvanceRequestItem
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 2/4/2021.
 */
interface CashAdvanceDataSource {
    fun performCashAdvance(
        configurationItem: ConfigurationItem,
        cashAdvanceRequestItem: CashAdvanceRequestItem
    ): Observable<CashAdvanceResponseEntity>

    fun performManualCashAdvance(
        configurationItem: ConfigurationItem,
        manualCashAdvanceRequestItem: ManualCashAdvanceRequestItem
    ): Observable<CashAdvanceResponseEntity>
}