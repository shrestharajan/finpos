package global.citytech.finpos.processor.nibl.transaction;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.transaction.TransactionResponseParameter;

/**
 * Created by Saurav Ghimire on 2/3/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class TransactionResponseModel implements UseCase.Response, TransactionResponseParameter {
    private String debugRequestMessage;
    private String debugResponseMessage;
    private boolean approved;
    private String message;
    private Result result;
    private String approvalCode;
    private String miniStatementData;

    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }

    public boolean isApproved() {
        return approved;
    }

    public String getMessage() {
        return message;
    }

    public Result getResult() {
        return result;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public String getMiniStatementData(){
        return miniStatementData;
    }

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private boolean approved;
        private String message;
        private Result result;
        private String approvalCode;
        private String miniStatementData;

        private Builder() {
        }

        public static Builder createDefaultBuilder() {
            return new Builder();
        }

        public Builder debugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public Builder debugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public Builder approved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder result(Result result) {
            this.result = result;
            return this;
        }

        public Builder approvalCode(String approvalCode) {
            this.approvalCode = approvalCode;
            return this;
        }

        public Builder miniStatementData(String miniStatementData){
            this.miniStatementData = miniStatementData;
            return this;
        }

        public TransactionResponseModel build() {
            TransactionResponseModel TransactionResponseModel = new TransactionResponseModel();
            TransactionResponseModel.debugRequestMessage = this.debugRequestMessage;
            TransactionResponseModel.debugResponseMessage = this.debugResponseMessage;
            TransactionResponseModel.approved = this.approved;
            TransactionResponseModel.message = this.message;
            TransactionResponseModel.result = this.result;
            TransactionResponseModel.approvalCode = this.approvalCode;
            TransactionResponseModel.miniStatementData = this.miniStatementData;
            return TransactionResponseModel;
        }
    }

    @Override
    public String toString() {
        return "TransactionResponseModel{" +
                "debugRequestMessage='" + debugRequestMessage + '\'' +
                ", debugResponseMessage='" + debugResponseMessage + '\'' +
                ", success=" + approved +
                ", message=" + message +
                ", result=" + result +
                ", approvalCode=" + approvalCode +
                ", miniStatementData=" + miniStatementData +
                '}';
    }
}
