package global.citytech.finpos.device.weipass.emv.aid

import com.wiseasy.emvprocess.KernelInit
import com.wiseasy.emvprocess.bean.AIDData
import com.wiseasy.emvprocess.bean.BaseAIDData
import com.wiseasy.emvprocess.bean.CLPayWaveAIDData
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finpos.device.weipass.utils.defaultOrEmptyValue
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import sdk4.wangpos.libemvbinder.utils.MoneyUtil

/**
 * Created by Rishav Chudal on 5/4/20.
 */
class WeipassVisaAidManager constructor(val baseAIDData: BaseAIDData,
                                        val aidParameters: global.citytech.finposframework.hardware.emv.aid.AidParameters,
                                        val emvParametersRequest: EmvParametersRequest
) {
    fun injectVisaAidInCore(): Int {
        var successCount = 0
        val aidData = prepareSdkAidData()
        WeipassLogger.debug("Injecting the Visa AID ...")
        val result = KernelInit.addAID(aidData)
        WeipassLogger.log("AID ".plus(aidParameters.aid).plus(" Injection Result ::: ".plus(result)))
        if (result == 0) {
            successCount ++
        }
        return successCount
    }

    private fun prepareSdkAidData(): AIDData {
        val sdkAidData = AIDData()
        sdkAidData.baseAIDData = baseAIDData
        sdkAidData.clAIDData = prepareClPayWaveAIDData()
        return sdkAidData
    }

    private fun prepareClPayWaveAIDData(): CLPayWaveAIDData {
        WeipassLogger.debug("===== CLPayWaveAIDData ::: Contactless =====")
        val clPayWaveAIDData = CLPayWaveAIDData()

        var floorLimit = aidParameters.contactlessFloorLimit.defaultOrEmptyValue()
        floorLimit = MoneyUtil.toCent(floorLimit)
        clPayWaveAIDData.clFloorLimit = floorLimit
        WeipassLogger.debug("CL Floor Limit ::: ".plus(floorLimit))

        var transLimit = aidParameters.contactlessTransactionLimit.defaultOrEmptyValue()
        transLimit = MoneyUtil.toCent(transLimit)
        clPayWaveAIDData.clTransLimit = transLimit
        WeipassLogger.debug("CL Trans Limit ::: ".plus(transLimit))

        var cvmLimit = aidParameters.cvmLimit.defaultOrEmptyValue()
        cvmLimit = MoneyUtil.toCent(cvmLimit)
        clPayWaveAIDData.clcvmLimit = cvmLimit
        WeipassLogger.debug("CL CVM Limit ::: ".plus(cvmLimit))

        val enableZeroAmountFlag = "1"
        clPayWaveAIDData.enableAmountZeroOption = enableZeroAmountFlag
        WeipassLogger.debug("Enable Zero Amount Flag ::: ".plus(enableZeroAmountFlag))

        val amountZeroOption = "0"
        clPayWaveAIDData.amountZeroOption = amountZeroOption
        WeipassLogger.debug("Amount Zero Option ::: ".plus(amountZeroOption))

        val enableFloorLimit = "1"
        clPayWaveAIDData.enableFloorLimit = enableFloorLimit
        WeipassLogger.debug("Enable Floor Limit ::: ".plus(enableFloorLimit))

        val enableTransLimit = "1"
        clPayWaveAIDData.enableTransLimit = enableTransLimit
        WeipassLogger.debug("Enable Trans Limit ::: ".plus(enableTransLimit))

        val enableCvmLimit = "1"
        clPayWaveAIDData.enableCVMLimit = enableCvmLimit
        WeipassLogger.debug("Enable CVM Limit ::: ".plus(enableCvmLimit))

        val ttq = aidParameters.ttq.defaultOrEmptyValue()
        clPayWaveAIDData.ttQ_9F66 = ttq
        WeipassLogger.debug("TTQ ::: ".plus(ttq))

        return clPayWaveAIDData
    }
}