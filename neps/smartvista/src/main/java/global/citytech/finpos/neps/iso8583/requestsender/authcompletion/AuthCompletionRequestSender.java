package global.citytech.finpos.neps.iso8583.requestsender.authcompletion;

import global.citytech.finpos.neps.iso8583.requestsender.cardtypesenders.CardlessISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.CardTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transactiontypesenders.AuthCompletionIsoMessageUpdater;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;

/**
 * Created by Saurav Ghimire on 2/10/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class AuthCompletionRequestSender extends TransactionRequestSender {
    public AuthCompletionRequestSender(RequestContext context) {
        super(context);
    }


    @Override
    protected String getClassName() {
        return AuthCompletionRequestSender.class.getName();
    }

    @Override
    protected CardTypeISOMessageUpdater getCardTypeSender() {
        return new CardlessISOMessageUpdater();
    }

    @Override
    protected TransactionTypeISOMessageUpdater getTransactionTypeSender() {
        return new AuthCompletionIsoMessageUpdater();
    }

    @Override
    protected MTI.MessageFunction getMessageFunctionRequest() {
        return MTI.MessageFunction.Request;
    }

    @Override
    protected MTI.MessageClass getMessageClass() {
        return MTI.MessageClass.Financial;
    }

}
