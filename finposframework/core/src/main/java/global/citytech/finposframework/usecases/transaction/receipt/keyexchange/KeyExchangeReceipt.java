package global.citytech.finposframework.usecases.transaction.receipt.keyexchange;

import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;

/**
 * Created by Unique Shakya on 1/19/2021.
 */
public class KeyExchangeReceipt {

    private Retailer retailer;
    private Performance performance;
    private String message;

    private KeyExchangeReceipt() {
    }

    public Retailer getRetailer() {
        return retailer;
    }

    public Performance getPerformance() {
        return performance;
    }

    public String getMessage() {
        return message;
    }


    public static final class Builder {
        private Retailer retailer;
        private Performance performance;
        private String message;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withRetailer(Retailer retailer) {
            this.retailer = retailer;
            return this;
        }

        public Builder withPerformance(Performance performance) {
            this.performance = performance;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public KeyExchangeReceipt build() {
            KeyExchangeReceipt keyExchangeReceipt = new KeyExchangeReceipt();
            keyExchangeReceipt.retailer = this.retailer;
            keyExchangeReceipt.performance = this.performance;
            keyExchangeReceipt.message = this.message;
            return keyExchangeReceipt;
        }
    }
}
