package global.citytech.finpos.merchant.presentation.dashboard.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import global.citytech.common.extensions.openActivity
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.admin.AdminActivity
import global.citytech.finpos.merchant.presentation.appnotification.AppNotificationActivity
import global.citytech.finpos.merchant.presentation.autoreversal.AutoReversalActivity
import global.citytech.finpos.merchant.presentation.dashboard.menu.Menu
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuAdapter
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuItem
import global.citytech.finpos.merchant.presentation.idle.IdleActivity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.switches.posmode.PosMode

/**
 * Created by Rishav Chudal on 5/13/21.
 */
abstract class BaseDashboardActivity<T : ViewDataBinding, V : BaseDashboardViewModel> :
    AppBaseActivity<T, V>(),
    MenuAdapter.MenuAdapterListener {

    private val logger = Logger(BaseDashboardActivity::class.java.simpleName)
    protected var clickedMenuItem: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        handlePageItems()
    }

    override fun onResume() {
        super.onResume()
        getViewModel().checkForNotifications()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AutoReversalActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK)
                proceedWithMenuItemMenuHandler(clickedMenuItem!!)
            else
                getViewModel().message.value = getString(R.string.error_msg_auto_reversal_not_performed)
        }
    }

    protected fun startAppNotificationsActivity() {
        val intent = Intent(this, AppNotificationActivity::class.java)
        startActivity(intent)
    }

    private fun handlePageItems() {
        initViews()
        initLiveDataObservers()
        getViewModel().getAppVersion()
        getViewModel().getTerminalConfiguredStatusAndProceed()
    }

    private fun initLiveDataObservers() {
        observeAppConfigurationLiveData()
        observeAppVersionLiveData()
        observeTerminalIsConfiguredLiveData()
        observeMenusLiveData()
        observeForceSettlementMessageLiveData()
        observeBaseActivityLiveDatas()
        observeChildActivityObservers()
        observeNotificationObserver()
        observeAutoReversalObserver()
    }

    private fun observeAutoReversalObserver() {
        getViewModel().autoReversalPresent.observe(this, Observer {
            if (it)
                startAutoReversalActivity()
            else
                proceedWithMenuItemMenuHandler(clickedMenuItem!!)
        })
    }

    private fun startAutoReversalActivity() {
        val intent = Intent(this, AutoReversalActivity::class.java)
        startActivityForResult(intent, AutoReversalActivity.REQUEST_CODE)
    }

    private fun observeNotificationObserver() {
        getViewModel().noNotifications.observe(this, Observer {
            makeNotificationIconVisible(it)
        })
    }

    abstract fun makeNotificationIconVisible(noNotifications: Boolean)

    private fun observeAppConfigurationLiveData() {
        getViewModel().appConfiguration.observe(
            this,
            Observer {
                handleConfig(it)
            }
        )
    }

    private fun handleConfig(configurationItem: ConfigurationItem?) {
        this.updateReadyAndTerminalIdInTextViews(configurationItem)
        this.loadEnabledTransactionsForGivenPosMode()
    }

    private fun loadEnabledTransactionsForGivenPosMode() {
        this.getViewModel().loadEnabledTransactionsForGivenPosMode(getPosMode())
    }

    private fun observeAppVersionLiveData() {
        getViewModel().appVersion.observe(
            this,
            Observer {
                updateAppVersionTextView(it)
            }
        )
    }

    private fun observeTerminalIsConfiguredLiveData() {
        getViewModel().terminalIsConfigured.observe(
            this,
            Observer { onObserveTerminalIsConfigured(it) }
        )
    }

    private fun onObserveTerminalIsConfigured(terminalIsConfigured: Boolean) {
        this.logger.log(
            "onObserveTerminalIsConfigured ::: Terminal is configured? ::: ".plus(
                terminalIsConfigured
            )
        )
        if (terminalIsConfigured) {
            onTerminalIsConfigured()
        } else {
            onTerminalIsNotConfigured()
        }
    }

    private fun onTerminalIsConfigured() {
        this.logger.log("Terminal is configured...")
        this.getViewModel().getConfiguration()
    }

    private fun onTerminalIsNotConfigured() {
        this.logger.log("Terminal is not configured...")
        updateProgressBarVisibility(View.GONE)
        updateTextViewReadyWithTerminalNotConfiguredMessage(getString(R.string.title_device_not_configured))
        this.showNotConfiguredDialog()
    }

    private fun showNotConfiguredDialog() {
        showFailureMessage(
            MessageConfig.Builder()
                .message(getString(R.string.msg_not_configured))
                .positiveLabel("OK")
                .onPositiveClick {
                    openActivity(AdminActivity::class.java)
                    finish()
                })
    }

    private fun observeMenusLiveData() {
        this.getViewModel().menusMutableLiveData.observe(
            this,
            Observer { updateMenuRecyclerView(it) }
        )
    }

    private fun observeBaseActivityLiveDatas() {
        observeIsLoadingLiveData()
    }

    private fun observeIsLoadingLiveData() {
        this.getViewModel().isLoading.observe(
            this,
            Observer {
                if (it) {
                    updateProgressBarVisibility(View.VISIBLE)
                } else {
                    updateProgressBarVisibility(View.GONE)
                }
            }
        )

        getViewModel().message.observe(this, Observer {
            if (!it.isNullOrEmpty()) {
                val messageConfig = MessageConfig.Builder()
                    .message(it)
                    .positiveLabel(getString(R.string.title_ok))
                    .onPositiveClick { }
                showNormalMessage(messageConfig)
            }
        })
    }

    private fun observeForceSettlementMessageLiveData() {
        this.getViewModel().forceSettlementMessageMutableLiveData.observe(
            this,
            Observer { showForceSettlementMessage(it) }
        )
    }

    private fun showForceSettlementMessage(message: String?) {
        if (!message.isNullOrEmptyOrBlank()) {
            val messageConfig = MessageConfig.Builder()
                .message(message)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {}
            showNormalMessage(messageConfig)
        }
    }

    protected fun proceedWithMenuItemMenuHandler(menuItem: MenuItem) {
        menuItem.menuHandler.loadMenuItemPage(this as Activity)
    }

    protected fun returnToIdlePage() {
        openActivity(IdleActivity::class.java)
        finish()
    }

    override fun onMenuAdapterItemClicked(menuItem: MenuItem) {
        clickedMenuItem = menuItem
        if (menuItem.menu == Menu.ADMIN_MENU || menuItem.menu == Menu.MERCHANT_MENU)
            this.proceedWithMenuItemMenuHandler(menuItem)
        else
            getViewModel().checkForAutoReversal()
    }

    override fun onBackPressed() {
        returnToIdlePage()
    }

    protected abstract fun initViews()
    protected abstract fun observeChildActivityObservers()
    protected abstract fun updateMenuRecyclerView(menus: List<MenuItem>)
    protected abstract fun updateReadyAndTerminalIdInTextViews(configurationItem: ConfigurationItem?)
    protected abstract fun updateAppVersionTextView(appVersion: String)
    protected abstract fun updateTextViewReadyWithTerminalNotConfiguredMessage(message: String)
    protected abstract fun updateProgressBarVisibility(visibility: Int);
    protected abstract fun getPosMode(): PosMode
}