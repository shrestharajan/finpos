package global.citytech.finpos.merchant.framework.datasource.core.posmode

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.data.datasource.core.posmode.PosModeDataSource
import global.citytech.finpos.merchant.presentation.data.mapToUiModel
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finposframework.switches.ProcessorManager
import global.citytech.finposframework.switches.posmode.PosMode

/**
 * Created by Unique Shakya on 1/7/2021.
 */
class PosModeDataSourceImpl : PosModeDataSource {
    override fun getPosMode(merchantCategoryCode: String): PosMode {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val posModeRequester =
            ProcessorManager.getInterface(null, null)
                .posModeRequester
        val posModeRequestModel = mapToModel(merchantCategoryCode)
        return (posModeRequester.execute(posModeRequestModel)).mapToUiModel()
    }
}