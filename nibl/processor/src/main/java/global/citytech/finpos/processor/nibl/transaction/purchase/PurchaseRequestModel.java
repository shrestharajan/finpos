package global.citytech.finpos.processor.nibl.transaction.purchase;

import global.citytech.finpos.processor.nibl.transaction.TransactionRequestModel;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.io.sound.SoundService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;

public class PurchaseRequestModel extends TransactionRequestModel {

    public static final class Builder {
        TransactionRequest transactionRequest;
        TransactionRepository transactionRepository;
        ReadCardService readCardService;
        DeviceController deviceController;
        TransactionAuthenticator transactionAuthenticator;
        PrinterService printerService;
        ApplicationRepository applicationRepository;
        ReconciliationRepository reconciliationRepository;
        LedService ledService;
        SoundService soundService;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withTransactionRequest(TransactionRequest transactionRequest) {
            this.transactionRequest = transactionRequest;
            return this;
        }

        public Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public Builder withReadCardService(ReadCardService readCardService) {
            this.readCardService = readCardService;
            return this;
        }

        public Builder withDeviceController(DeviceController deviceController) {
            this.deviceController = deviceController;
            return this;
        }

        public Builder withTransactionAuthenticator(TransactionAuthenticator transactionAuthenticator) {
            this.transactionAuthenticator = transactionAuthenticator;
            return this;
        }

        public Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public Builder withReconciliationRepository(ReconciliationRepository reconciliationRepository) {
            this.reconciliationRepository = reconciliationRepository;
            return this;
        }

        public Builder withLedService(LedService ledService) {
            this.ledService = ledService;
            return this;
        }

        public Builder withSoundService(SoundService soundService) {
            this.soundService = soundService;
            return this;
        }

        public PurchaseRequestModel build() {
            PurchaseRequestModel purchaseRequestModel = new PurchaseRequestModel();
            purchaseRequestModel.readCardService = this.readCardService;
            purchaseRequestModel.reconciliationRepository = this.reconciliationRepository;
            purchaseRequestModel.transactionRepository = this.transactionRepository;
            purchaseRequestModel.printerService = this.printerService;
            purchaseRequestModel.deviceController = this.deviceController;
            purchaseRequestModel.transactionAuthenticator = this.transactionAuthenticator;
            purchaseRequestModel.transactionRequest = this.transactionRequest;
            purchaseRequestModel.applicationRepository = this.applicationRepository;
            purchaseRequestModel.ledService = this.ledService;
            purchaseRequestModel.soundService = this.soundService;
            return purchaseRequestModel;
        }
    }
}
