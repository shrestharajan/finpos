package global.citytech.finpos.merchant.presentation.dynamicqr.nqrsale

import android.app.Application
import android.content.Context
import android.content.Intent
import global.citytech.common.data.Response
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.domain.usecase.qr.nqr.NqrGenerateRequest
import global.citytech.finpos.merchant.domain.usecase.qr.nqr.NqrGenerator
import global.citytech.finpos.merchant.presentation.dynamicqr.base.DynamicQrBaseViewModel
import global.citytech.finposframework.log.Logger
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * @author sachin & siddhartha
 */
class DynamicNQrViewModel(val context: Application) : DynamicQrBaseViewModel(context) {
    private val logger = Logger.getLogger(DynamicNQrViewModel::class.java.name)

    var nqrGenerator = NqrGenerator()
    var nqrGenerateRequest: NqrGenerateRequest? = null

    fun generateNqr(nqrGenerateRequest: NqrGenerateRequest) {
        qrData.value = nqrGenerator.generate(nqrGenerateRequest).data
    }

    fun getBillingInvoiceNumber(): String {
        val invoiceNumber = qrPaymentUseCase.getBillingInvoiceNumber()
        qrPaymentUseCase.incrementBillingInvoiceNumber()
        return invoiceNumber
    }

    fun bindService() {
        compositeDisposable.add(
            Single.create(SingleOnSubscribe<Response> {
                val bindIntent = Intent()
                val packageName = "global.citytech.tms"
                val className = "global.citytech.tms.service.PlatformManagerService"
                bindIntent.setClassName(packageName, className)
                context.bindService(
                    bindIntent,
                    NiblMerchant.INSTANCE.getPlatformServiceConnection(),
                    Context.BIND_AUTO_CREATE
                )
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                }, {
                    it.printStackTrace()
                    isLoading.value = false
                    message.value = it.message
                })
        )
    }
}