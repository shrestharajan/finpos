package global.citytech.finpos.merchant.presentation.dashboard.cashmode

import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityCashModeDashboardBinding
import global.citytech.finpos.merchant.presentation.dashboard.base.BaseDashboardActivity
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuAdapter
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuItem
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuItemDecoration
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuUtils
import global.citytech.finpos.merchant.presentation.dashboard.retailer.RetailerModeActivity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.switches.posmode.PosMode
import kotlinx.android.synthetic.main.activity_cash_mode_dashboard.*
import kotlinx.android.synthetic.main.activity_cash_mode_dashboard.iv_app_notification
import kotlinx.android.synthetic.main.activity_cash_mode_dashboard.iv_back
import kotlinx.android.synthetic.main.activity_cash_mode_dashboard.progressbar
import kotlinx.android.synthetic.main.activity_cash_mode_dashboard.rv_menu
import kotlinx.android.synthetic.main.activity_cash_mode_dashboard.tv_application_version
import kotlinx.android.synthetic.main.activity_cash_mode_dashboard.tv_ready
import kotlinx.android.synthetic.main.activity_cash_mode_dashboard.tv_terminal_id
import kotlinx.android.synthetic.main.activity_retailer_dashboard.*

/**
 * Created by Unique Shakya on 1/6/2021.
 * Modified by Rishav Chudal on 05/12/2021
 */
class CashModeActivity :
    BaseDashboardActivity<ActivityCashModeDashboardBinding, CashModeViewModel>() {

    private val logger: Logger = Logger.getLogger(RetailerModeActivity::class.java.name)

    override fun getBindingVariable(): Int = BR.cashModeViewModel

    override fun getLayout(): Int = R.layout.activity_cash_mode_dashboard

    override fun getViewModel(): CashModeViewModel =
        ViewModelProviders.of(this)[CashModeViewModel::class.java]

    override fun getPosMode(): PosMode = PosMode.CASH

    override fun initViews() {
        initAndHandleButtonClicks()
        initAndHandlerCashAdvanceButton()
    }

    override fun observeChildActivityObservers() {
        observeShowCashAdvanceInMenuLiveData()
    }

    override fun updateMenuRecyclerView(menus: List<MenuItem>) {
        rv_menu.layoutManager = GridLayoutManager(this,2)
        rv_menu.adapter = MenuAdapter(menus, this)
        rv_menu.addItemDecoration(MenuItemDecoration())
    }

    override fun updateReadyAndTerminalIdInTextViews(configurationItem: ConfigurationItem?) {
        this.tv_ready.text = getString(R.string.title_ready)
        tv_terminal_id.text = getString(R.string.title_terminal_id)
            .plus(" : ")
            .plus(configurationItem!!.merchants!![0].terminalId)
    }

    override fun updateAppVersionTextView(appVersion: String) {
        tv_application_version.text = appVersion
    }

    override fun updateTextViewReadyWithTerminalNotConfiguredMessage(message: String) {
        this.tv_ready.text = message
    }

    override fun updateProgressBarVisibility(visibility: Int) {
        this.progressbar.visibility = visibility
    }

    override fun makeNotificationIconVisible(noNotifications: Boolean) {
        if (noNotifications)
            iv_app_notification.visibility = View.GONE
        else
            iv_app_notification.visibility = View.VISIBLE
    }

    private fun initAndHandleButtonClicks() {
        iv_back.setOnClickListener {
            returnToIdlePage()
        }

        iv_app_notification.setOnClickListener {
            startAppNotificationsActivity()
        }
    }

    private fun initAndHandlerCashAdvanceButton() {
        btn_cash_advance.visibility = View.GONE
        btn_cash_advance.setOnClickListener {
            clickedMenuItem = MenuUtils.menuItemCashAdvance()
            getViewModel().checkForAutoReversal()
        }
    }

    private fun observeShowCashAdvanceInMenuLiveData() {
        this.getViewModel().showCashAdvanceMenuLiveData.observe(
            this,
            Observer {
                if (it) {
                    btn_cash_advance.visibility = View.VISIBLE
                } else {
                    btn_cash_advance.visibility = View.GONE
                }
            }
        )
    }
}