package global.citytech.finpos.processor.nibl.receipt.detailreport;

import java.util.List;

import global.citytech.finpos.processor.nibl.receipt.summaryreport.SummaryReportRequestModel;
import global.citytech.finpos.processor.nibl.receipt.summaryreport.SummaryReportResponseModel;
import global.citytech.finpos.processor.nibl.receipt.summaryreport.SummaryReportUseCase;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.detailreport.DetailReportRequester;
import global.citytech.finposframework.usecases.transaction.TransactionLog;

/**
 * Created by Unique Shakya on 1/4/2021.
 */
public class DetailReportUseCase implements DetailReportRequester<DetailReportRequestModel, DetailReportResponseModel>,
        UseCase<DetailReportRequestModel, DetailReportResponseModel> {

    private TerminalRepository terminalRepository;
    private Notifier notifier;

    public DetailReportUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        this.terminalRepository = terminalRepository;
        this.notifier = notifier;
    }

    @Override
    public DetailReportResponseModel execute(DetailReportRequestModel detailReportRequestModel) {
        try {
            String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
            this.notifier.notify(Notifier.EventType.PREPARING_DETAIL_REPORT, Notifier.EventType.PREPARING_DETAIL_REPORT.getDescription());
            List<TransactionLog> transactionLogs = detailReportRequestModel.getTransactionRepository()
                    .getTransactionLogsByBatchNumber(batchNumber);
            if (transactionLogs.isEmpty())
                throw new PosException(PosError.DEVICE_ERROR_NO_RECENT_TRANSACTION);
            DetailReportReceiptMaker detailReportReceiptMaker = new DetailReportReceiptMaker(this.terminalRepository);
            DetailReportReceipt detailReportReceipt = detailReportReceiptMaker.prepare(batchNumber, transactionLogs);
            this.notifier.notify(Notifier.EventType.PRINTING_DETAIL_REPORT, Notifier.EventType.PRINTING_DETAIL_REPORT.getDescription());
            DetailReportReceiptHandler receiptHandler = new DetailReportReceiptHandler(detailReportRequestModel.getPrinterService());
            PrinterResponse detailReportPrintResponse = receiptHandler.print(detailReportReceipt);
            if (detailReportPrintResponse.getResult() != Result.SUCCESS)
                return new DetailReportResponseModel(detailReportPrintResponse.getResult(), detailReportPrintResponse.getMessage());
            if (detailReportRequestModel.isPrintSummaryReport()) {
                SummaryReportUseCase summaryReportUseCase = new SummaryReportUseCase(this.terminalRepository);
                SummaryReportResponseModel summaryReportResponseModel = summaryReportUseCase
                        .execute(SummaryReportRequestModel.Builder.newInstance()
                                .withReconciliationRepository(detailReportRequestModel.getReconciliationRepository())
                                .withPrinterService(detailReportRequestModel.getPrinterService())
                                .withApplicationRepository(detailReportRequestModel.getApplicationRepository())
                                .build());
                if (summaryReportResponseModel.getResult() != Result.SUCCESS)
                    return new DetailReportResponseModel(summaryReportResponseModel.getResult(),
                            summaryReportResponseModel.getMessage());
            }
            return new DetailReportResponseModel(detailReportPrintResponse.getResult(),
                    detailReportPrintResponse.getMessage());
        } catch (PosException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
    }
}
