package global.citytech.finpos.merchant.presentation.transactions.qr

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityQrTransactionDetailBinding
import global.citytech.finpos.merchant.domain.model.app.QrPayment
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.TRANSACTION_VOID_SALE
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.presentation.admin.login.LoginDialog
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmationDialog
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.dynamicqr.refund.QrRefundActivity
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finpos.merchant.utils.retrieveCurrencyName
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion
import global.citytech.finposframework.utility.StringUtils
import kotlinx.android.synthetic.main.activity_qr_transaction_detail.*

class QrTransactionDetailActivity :
    AppBaseActivity<ActivityQrTransactionDetailBinding, QrTransactionDetailViewModel>(),
    TransactionConfirmationDialog.Listener, LoginDialog.LoginDialogListener {

    companion object {

        private const val EXTRA_QR_PAYMENT = "extra.qr.payment"

        fun getLaunchIntent(context: Context, qrPayment: QrPayment): Intent {
            val intent = Intent(context, QrTransactionDetailActivity::class.java)
            intent.putExtra(EXTRA_QR_PAYMENT, qrPayment)
            return intent
        }
    }

    private var transactionConfirmationDialog: TransactionConfirmationDialog? = null
    private lateinit var loginDialog: LoginDialog
    private var originalQrPayment: QrPayment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel().getConfigurationItem()
        initObservers()
        (intent.getParcelableExtra(EXTRA_QR_PAYMENT) as? QrPayment)?.let {
            originalQrPayment = it
            initViews(it)
            handleClickEvents(it)
        }
    }

    private fun initObservers() {
        getViewModel().isLoading.observe(this, Observer {
            if (it)
                showProgress()
            else
                hideProgress()
        })

        getViewModel().message.observe(this, Observer {
            showNormalMessage(
                MessageConfig.Builder().message(it).positiveLabel(getString(R.string.title_ok))
                    .onPositiveClick { })
        })

        getViewModel().finishActivity.observe(this, Observer {
            if (it)
                finish()
        })

        getViewModel().transactionConfirmationLiveData.observe(this, Observer {
            showTransactionConfirmationDialog(it, getViewModel().updateTransactionConfirmation)
        })
    }

    private fun showTransactionConfirmationDialog(it: TransactionConfirmation, update: Boolean) {
        if (transactionConfirmationDialog == null)
            transactionConfirmationDialog = TransactionConfirmationDialog(this, this, it)
        if (update)
            transactionConfirmationDialog!!.update(it, this)
        transactionConfirmationDialog!!.show()
    }

    private fun handleClickEvents(qrPayment: QrPayment) {
        btn_print_duplicate.setOnClickListener {
            getViewModel().printDuplicateReceipt(qrPayment)
        }

        btn_cancellation.setOnClickListener {
            showMerchantLoginDialog(
                getString(R.string.msg_merchant_password),
                getViewModel().getMerchantCredential()
            )
        }

        iv_back.setOnClickListener {
            finish()
        }
    }

    private fun showMerchantLoginDialog(pageTitle: String, validCredential: String) {
        this.loginDialog = LoginDialog.newInstance(
            pageTitle = pageTitle,
            validCredential = validCredential
        )
        this.loginDialog.isCancelable = false
        this.loginDialog.show(
            supportFragmentManager,
            LoginDialog.TAG
        )
    }

    private fun startQrRefundActivity(qrPayment: QrPayment) {
        val intent = QrRefundActivity.getLaunchIntent(this, qrPayment.invoiceNumber)
        startActivityForResult(intent, QrRefundActivity.REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == QrRefundActivity.REQUEST_CODE)
            finish()
    }

    private fun initViews(qrPayment: QrPayment) {
        val initiatorId = qrPayment.initiatorId.toString()
        tv_merchant_name.text = DeviceConfiguration.get().merchantName
        tv_merchant_id.text = qrPayment.merchantId
        tv_terminal_id.text = qrPayment.terminalId
        tv_date.text = qrPayment.transactionDate
        tv_time.text = qrPayment.transactionTime
        tv_reference_number.text = qrPayment.referenceNumber
        tv_transaction_type.text =
            when (qrPayment.transactionType.lowercase()) {
                TRANSACTION_VOID_SALE -> TransactionType.VOID.displayName
                else -> qrPayment.transactionType
            }
        tv_transaction_amount.text = retrieveCurrencyName(qrPayment.transactionCurrency).plus(" ")
            .plus(StringUtils.formatAmountTwoDecimal(qrPayment.transactionAmount))
        tv_transaction_status.text = qrPayment.transactionStatus
        tv_payment_initiator.text = qrPayment.paymentInitiator
        tv_initiator_id.text =
            when (initiatorId.length) {
                15 -> StringUtils.maskPayerPanNumber(initiatorId)
                14 -> maskInitiatorId(initiatorId,8)
                13 -> maskInitiatorId(initiatorId,7)
                12 -> maskInitiatorId(initiatorId,6)
                11 -> maskInitiatorId(initiatorId,5)
                10 -> maskInitiatorId(initiatorId,4)
                else -> initiatorId
            }
        tv_approval_code.text = qrPayment.approvalCode
        if (isRefundOrRefundedQr(qrPayment) || isVoidOrVoidedQr(qrPayment))
            btn_cancellation.visibility = View.GONE
        else
            btn_cancellation.visibility = View.VISIBLE
    }

    private fun isRefundOrRefundedQr(qrPayment: QrPayment): Boolean {
        return qrPayment.isRefunded || qrPayment.transactionType.toLowerCase() == "refund sale" || qrPayment.transactionType.toLowerCase() == "refund_sale"
    }

    private fun isVoidOrVoidedQr(qrPayment: QrPayment): Boolean {
        return qrPayment.isVoided || qrPayment.transactionType.toLowerCase() == "void sale" || qrPayment.transactionType.toLowerCase() == "void_sale"
    }

    override fun getBindingVariable(): Int = BR.qrTransactionViewModel

    override fun getLayout(): Int = R.layout.activity_qr_transaction_detail

    override fun getViewModel(): QrTransactionDetailViewModel =
        ViewModelProviders.of(this)[QrTransactionDetailViewModel::class.java]

    override fun onPositiveButtonClick() {
        getViewModel().printQrReceipt(ReceiptVersion.CUSTOMER_COPY)
    }

    override fun onNegativeButtonClick() {
        finish()
    }

    override fun onLoginDialogCorrectCredentialEntered() {
        getViewModel().performVoid(originalQrPayment!!)
    }

    override fun onLoginDialogCancelButtonClicked() {
        // do nothing
    }

    override fun onResume() {
        super.onResume()
        registerReprintBroadcastReceivers()
    }

    override fun onPause() {
        unregisterReprintBroadCastReceivers()
        super.onPause()
    }
}