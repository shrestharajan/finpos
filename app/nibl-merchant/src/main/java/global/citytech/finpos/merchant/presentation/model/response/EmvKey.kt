package global.citytech.finpos.merchant.presentation.model.response

import global.citytech.common.data.LabelValue

data class EmvKey(
    var checkSum: LabelValue? = null,
    var expiryDate: LabelValue? = null,
    var exponent: LabelValue? = null,
    var hashId: LabelValue? = null,
    var index: LabelValue? = null,
    var keySignatureId: LabelValue? = null,
    var length: LabelValue? = null,
    var modules: LabelValue? = null,
    var rid: LabelValue? = null
) {

    companion object {

        val TABLE_NAME = "emv_key"

        const val COLUMN_ID = "id"
        const val COLUMN_RID = "rid"
        const val COLUMN_INDEX = "index"
        const val COLUMN_LENGTH = "length"
        const val COLUMN_EXPONENT = "exponent"
        const val COLUMN_MODULES = "modules"
        const val COLUMN_HASH_ID = "hash_id"
        const val COLUMN_KEY_SIGNATURE_ID = "key_signature_id"
        const val COLUMN_CHECKSUM = "checksum"
        const val COLUMN_EXPIRY_DATE = "expiry_date"
    }
}