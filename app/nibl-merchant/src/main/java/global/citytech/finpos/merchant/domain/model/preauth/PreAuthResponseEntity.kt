package global.citytech.finpos.merchant.domain.model.preauth


/**
 * Created by Saurav Ghimire on 2/4/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
data class PreAuthResponseEntity(
    val stan: String? = null,
    val message: String? = null,
    val isApproved: Boolean? = false,
    val shouldPrintCustomerCopy: Boolean? = false
)
