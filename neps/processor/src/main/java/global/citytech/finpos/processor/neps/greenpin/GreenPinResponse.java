package global.citytech.finpos.processor.neps.greenpin;

import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finposframework.hardware.common.Result;

public class GreenPinResponse extends TransactionResponse {

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private boolean approved;
        private String message;
        private String stan;
        private Result result;
        private String approvalCode;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static GreenPinResponse.Builder newInstance() {
            return new GreenPinResponse.Builder();
        }

        public GreenPinResponse.Builder withDebugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public GreenPinResponse.Builder withDebugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public GreenPinResponse.Builder withShouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }
        public GreenPinResponse.Builder withApproved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public GreenPinResponse.Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public GreenPinResponse.Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }

        public GreenPinResponse.Builder withResult(Result result) {
            this.result = result;
            return this;
        }

        public GreenPinResponse.Builder withApprovalCode(String approvalCode) {
            this.approvalCode = approvalCode;
            return this;
        }

        public GreenPinResponse build() {
            GreenPinResponse GreenPinResponse = new GreenPinResponse();
            GreenPinResponse.debugRequestMessage = this.debugRequestMessage;
            GreenPinResponse.debugResponseMessage = this.debugResponseMessage;
            GreenPinResponse.approved = this.approved;
            GreenPinResponse.message = this.message;
            GreenPinResponse.result = this.result;
            GreenPinResponse.approvalCode = this.approvalCode;
            GreenPinResponse.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            GreenPinResponse.stan = this.stan;
            return GreenPinResponse;
        }
    }


}
