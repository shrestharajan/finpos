package global.citytech.finpos.merchant.presentation.mobilenfcpayment.receipt

import global.citytech.finposframework.hardware.io.printer.Style

enum class MobileNfcPaymentReceiptStyle(
    val align: Style.Align,
    val fontSize: Style.FontSize,
    val columns: Int,
    val allCaps: Boolean,
    val bold: Boolean,
    val italic: Boolean,
    val underline: Boolean
) {
    RETAILER_NAME(Style.Align.CENTER, Style.FontSize.XLARGE, 1, true, true, false, false),
    RETAILER_ADDRESS(Style.Align.CENTER, Style.FontSize.SMALL, 1, false, true, false, false),
    DATE_TIME(Style.Align.LEFT, Style.FontSize.SMALL, 2, false, false, false, false),
    MID_TID(Style.Align.LEFT, Style.FontSize.SMALL, 2, true, false, false, false),
    INVOICE_NUMBER(Style.Align.LEFT, Style.FontSize.NORMAL, 2, false, false, false, false),
    REFERENCE_NUMBER(Style.Align.LEFT, Style.FontSize.NORMAL, 2, false, false, false, false),
    TRANSACTION_TYPE(Style.Align.CENTER, Style.FontSize.LARGE, 1, true, false, false, false),
    TRANSACTION_AMOUNT(Style.Align.CENTER, Style.FontSize.XLARGE, 1, true, true, false, false),
    TRANSACTION_RESULT(Style.Align.CENTER, Style.FontSize.LARGE, 1, true, false, false, false),
    PAYMENT_INITIATOR(Style.Align.LEFT, Style.FontSize.NORMAL, 2, false, false, false, false),
    INITIATOR_ID(Style.Align.LEFT, Style.FontSize.NORMAL, 2, false, false, false, false),
    APPROVAL_CODE(Style.Align.LEFT, Style.FontSize.NORMAL, 2, false, false, false, false),
    QR_CODE(Style.Align.CENTER, Style.FontSize.XLARGE, 1, false, false, false, false),
    THANK_YOU_MESSAGE(Style.Align.CENTER, Style.FontSize.NORMAL, 1, true, false, false, false),
    RECEIPT_VERSION(Style.Align.CENTER, Style.FontSize.LARGE, 1, true, true, false, false),
    DIVIDER(Style.Align.CENTER, Style.FontSize.SMALL, 1, true, true, false, false),
    ;

    fun getStyle(): Style {
        return Style.Builder()
            .alignment(align)
            .allCaps(allCaps)
            .bold(bold)
            .fontSize(fontSize)
            .italic(italic)
            .underline(underline)
            .multipleAlignment(columns >= 2)
            .build()
    }
}