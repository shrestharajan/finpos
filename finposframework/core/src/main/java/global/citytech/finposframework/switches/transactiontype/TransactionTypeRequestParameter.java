package global.citytech.finposframework.switches.transactiontype;

import global.citytech.finposframework.switches.RequestParameter;

/**
 * Created by Unique Shakya on 12/1/2020.
 */
public interface TransactionTypeRequestParameter extends RequestParameter {
}
