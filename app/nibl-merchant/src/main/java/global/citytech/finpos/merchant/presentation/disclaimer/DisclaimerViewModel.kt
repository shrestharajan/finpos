package global.citytech.finpos.merchant.presentation.disclaimer

import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import global.citytech.common.data.Response
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.AppNotificationRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.Disclaimer
import global.citytech.finpos.merchant.domain.usecase.app.AppNotificationUseCase
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.AppNotificationDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.presentation.model.disclaimer.DisclaimerAcknowledgementResponse
import global.citytech.finpos.merchant.presentation.model.disclaimer.DisclaimerAcknowledgmentRequest
import global.citytech.finpos.merchant.utils.ApiConstant
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DisclaimerViewModel(val context: Application) : BaseAndroidViewModel(context) {

    val closeActivity by lazy { MutableLiveData<Boolean>() }
    val disclaimer by lazy { MutableLiveData<Disclaimer>() }
    val errorMessage by lazy { MutableLiveData<String>() }

    private val localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    private val appNotificationUseCase = AppNotificationUseCase(
        AppNotificationRepositoryImpl(
            AppNotificationDataSourceImpl(context)
        )
    )

    fun acceptDisclaimer() {
        acknowledgeDisclaimerToWebService(
            disclaimer.value!!.disclaimerId,
            context.getString(R.string.action_accepted)
        )
    }

    fun acceptLater() {
        closeActivity.value = true
    }

    fun declineDisclaimer() {
        acknowledgeDisclaimerToWebService(
            disclaimer.value!!.disclaimerId,
            context.getString(R.string.action_rejected)
        )
    }

    fun bindService() {
        compositeDisposable.add(
            Single.create(SingleOnSubscribe<Response> { _ ->
                val bindIntent = Intent()
                val packageName = "global.citytech.tms"
                val className = "global.citytech.tms.service.PlatformManagerService"
                bindIntent.setClassName(packageName, className)
                context.bindService(
                    bindIntent,
                    NiblMerchant.INSTANCE.getPlatformServiceConnection(),
                    Context.BIND_AUTO_CREATE
                )
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {})
        )
    }

    private fun acknowledgeDisclaimerToWebService(disclaimerId: String, remarks: String) {
        isLoading.value = true
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<DisclaimerAcknowledgementResponse> {
                callAcknowledgeDisclaimerApi(
                    disclaimerId,
                    remarks,
                    it
                )
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    removeDisclaimerNotificationByReferenceId()
                    removeDisclaimerFromDatabase(disclaimerId, remarks)
                }, {
                    it.printStackTrace()
                    isLoading.value = false
                    errorMessage.value = if (remarks == context.getString(R.string.action_accepted))
                        context.getString(R.string.error_msg_accept_disclaimer)
                    else
                        context.getString(R.string.error_msg_decline_disclaimer)
                })
        )
    }

    private fun removeDisclaimerFromDatabase(disclaimerId: String, remarks: String) {
        isLoading.value = true
        compositeDisposable.add(
            localDataUseCase.deleteById(disclaimerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    if (remarks == context.getString(R.string.action_accepted)) {
                        message.value = context.getString(R.string.msg_success_disclaimer_accept)
                    } else {
                        closeActivity.value = true
                    }
                }, {
                    isLoading.value = false
                    removeDisclaimerFromDatabase(disclaimerId, remarks)
                })
        )
    }

    private fun callAcknowledgeDisclaimerApi(
        disclaimerId: String,
        remarks: String,
        it: ObservableEmitter<DisclaimerAcknowledgementResponse>
    ) {
        val request = DisclaimerAcknowledgmentRequest(
            disclaimerId = disclaimerId,
            remarks = remarks,
            serialNumber = NiblMerchant.INSTANCE.iPlatformManager.serialNumber
        )
        val jsonRequest = Jsons.toJsonObj(request)
        val jsonResponse =
            NiblMerchant.INSTANCE.iPlatformManager.post(
                ApiConstant.DISCLAIMER_ACK_URL,
                jsonRequest,
                true
            )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            val responseDataJson = Jsons.toJsonObj(response.data)
            val disclaimerResponse =
                Jsons.fromJsonToObj(responseDataJson, DisclaimerAcknowledgementResponse::class.java)
            it.onNext(disclaimerResponse)
        } else {
            it.onError(Exception("Error Code :: ${response.code}. ${response.message}"))
        }
        it.onComplete()
    }

    private fun removeDisclaimerNotificationByReferenceId() {
        appNotificationUseCase.removeNotificationByReferenceId(disclaimer.value!!.disclaimerId)
    }

    fun retrieveDisclaimer(disclaimerId: String) {
        isLoading.value = true
        compositeDisposable.add(
            localDataUseCase.retrieveDisclaimerById(disclaimerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    disclaimer.value = it!!
                }, {
                    isLoading.value = false
                    message.value = context.getString(R.string.error_msg_no_disclaimer)
                })
        )
    }
}