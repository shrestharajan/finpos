package global.citytech.finpos.nibl.iso8583;

import org.junit.Test;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.NIBLHostConfig;
import global.citytech.finpos.nibl.iso8583.requestsender.voidsale.NiblVoidSaleRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.voidsale.VoidSaleRequestSender;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.utility.HelperUtils;

import static org.junit.Assert.assertNotNull;

public class VoidSaleRequestSenderTest {
    private Logger logger = Logger.getLogger(VoidSaleRequestSenderTest.class.getName());

    @Test
    public void sendTest() {
        try {
            String terminalID = "99991002";
            String axquirerID = "405635000000002";
            TerminalInfo terminalInfo = new TerminalInfo(terminalID, axquirerID,"","Citytech","Kamaladi");
            HostInfo primaryHost = NIBLHostConfig.getPrimaryConfig();
            HostInfo secondaryHost = NIBLHostConfig.getSecondaryConfig();
            ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);
            RequestContext context =
                    new RequestContext(
                            "000405", terminalInfo, connectionParam);
            NiblVoidSaleRequest request = new NiblVoidSaleRequest();
            request.setEntryMode(PosEntryMode.ICC);
            request.setPan("4249720020000234");
            request.setPosConditionCode("00");
            request.setApprovalCode("230084");
            request.setInvoiceNumber("000108");
            request.setRetrievalReferenceNumber("021711388850");
            request.setExpireDate("2412");
            request.setLocalTime(HelperUtils.getDefaultLocaleTimeHhMmSs());
            request.setLocalDate(HelperUtils.getDefaultLocaleDateWithoutYear());
            context.setRequest(request);
            VoidSaleRequestSender sender = new VoidSaleRequestSender(new NIBLSpecInfo(), context);
            IsoMessageResponse msg = sender.send();
            assertNotNull(msg);
        } catch (FinPosException e) {
            logger.log(e.getMessage());
            logger.log(e.getDetailMessage());
        }
    }
}
