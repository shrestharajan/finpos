package global.citytech.neps

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.util.Log
import global.citytech.neps.datasource.pref.PreferenceManager


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
class App :Application(), Application.ActivityLifecycleCallbacks{
    companion object{
        lateinit var INSTANCE: App
        var activityContext: Context? = null
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        PreferenceManager.init(this)
        registerActivityLifecycleCallbacks(this)
    }

    override fun onActivityPaused(p0: Activity) {
        Log.i("corelogger", "Activity ::: ".plus(p0.javaClass.simpleName).plus(" is Paused"))
    }

    override fun onActivityStarted(p0: Activity) {
        Log.i("corelogger", "Activity ::: ".plus(p0.javaClass.simpleName).plus(" is Started"))
        activityContext = p0
    }

    override fun onActivityDestroyed(p0: Activity) {
        Log.i("corelogger", "Activity ::: ".plus(p0.javaClass.simpleName).plus(" is Destroyed"))
    }

    override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {
        Log.i("corelogger", "Activity ::: ".plus(p0.javaClass.simpleName).plus(" SaveInstanceState"))
    }

    override fun onActivityStopped(p0: Activity) {
        Log.i("corelogger", "Activity ::: ".plus(p0.javaClass.simpleName).plus(" is Stopped"))
    }

    override fun onActivityCreated(p0: Activity, p1: Bundle?) {
        Log.i("corelogger", "Activity ::: ".plus(p0.javaClass.simpleName).plus(" is Created"))
        activityContext = p0
    }

    override fun onActivityResumed(p0: Activity) {
        Log.i("corelogger", "Activity ::: ".plus(p0.localClassName).plus(" is Resumed"))
    }
}