package global.citytech.finpos.nibl.iso8583;

import org.junit.Test;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.NIBLHostConfig;
import global.citytech.finpos.nibl.iso8583.requestsender.logon.LogonRequestSender;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;

import static org.junit.Assert.assertNotNull;

public class LogonRequestSenderTest {
    private Logger logger = Logger.getLogger(LogonRequestSenderTest.class.getName());

    @Test
    public void sendTest() {
        try {
            String terminalID = "99991002";
            String axquirerID = "405635000000002";
            TerminalInfo terminalInfo = new TerminalInfo(terminalID, axquirerID,"","Citytech","Kamaladi");
            HostInfo primaryHost = NIBLHostConfig.getPrimaryConfig();
            HostInfo secondaryHost = NIBLHostConfig.getSecondaryConfig();
            ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);
            RequestContext context =
                    new RequestContext(
                            "000421", terminalInfo, connectionParam);
            LogonRequestSender sender = new LogonRequestSender(new NIBLSpecInfo(), context);
            IsoMessageResponse msg = sender.send();
            assertNotNull(msg);
        } catch (FinPosException e) {
            logger.log(e.getMessage());
            logger.log(e.getDetailMessage());
        }
    }
}
