package global.citytech.payment.sdk.core;

import global.citytech.payment.sdk.api.PaymentResult;

/**
 * Created by BikashShrestha on 2019-06-11.
 */
public class PaymentException extends RuntimeException {

    private final PaymentResult paymentResult;

    public PaymentException(PaymentResult paymentResult) {
        super(paymentResult.getMessage());
        this.paymentResult = paymentResult;
    }

    public PaymentResult getPaymentResult() {
        return paymentResult;
    }
}
