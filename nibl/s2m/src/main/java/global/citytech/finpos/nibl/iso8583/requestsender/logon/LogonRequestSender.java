package global.citytech.finpos.nibl.iso8583.requestsender.logon;

import global.citytech.finpos.nibl.iso8583.NIBLMessagePayloadBuilder;
import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.supports.MessagePayload;

/**
 * User: Surajchhetry Date: 2/28/20 Time: 3:53 PM
 */
public class LogonRequestSender extends NIBLMessageSenderTemplate {

    public LogonRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Network_Management,
                        MTI.MessageFunction.Request,
                        MTI.Originator.Acquirer);
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);
        MessagePayload.Builder builder = new NIBLMessagePayloadBuilder(this.specInfo, msg);
        return builder.build();
    }

    protected String getProcessingCode() {
        return ProcessingCode.LOG_ON.getCode();
    }

    protected String getFunctionCode() {
        return "0011";
    }
}
