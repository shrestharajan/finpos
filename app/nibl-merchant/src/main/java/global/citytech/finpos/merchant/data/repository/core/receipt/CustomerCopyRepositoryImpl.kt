package global.citytech.finpos.merchant.data.repository.core.receipt

import global.citytech.finpos.merchant.data.datasource.core.receipt.CustomerCopyDataSource
import global.citytech.finpos.merchant.domain.model.receipt.CustomerCopyResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.receipt.CustomerCopyRepository
import global.citytech.finposframework.usecases.transaction.data.StatementList
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/2/2020.
 */
class CustomerCopyRepositoryImpl(private val customerCopyDataSource: CustomerCopyDataSource)
    : CustomerCopyRepository{
    override fun print(): Observable<CustomerCopyResponseEntity> {
        return customerCopyDataSource.print()
    }
    override fun printStatement(statementList: StatementList):Observable<CustomerCopyResponseEntity> {
        return customerCopyDataSource.printStatement(statementList)
    }

}