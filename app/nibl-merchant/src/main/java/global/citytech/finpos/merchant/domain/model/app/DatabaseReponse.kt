package global.citytech.finpos.merchant.domain.model.app

import global.citytech.finpos.merchant.domain.model.device.DeviceResult

/**
 * Created by Saurav Ghimire on 4/26/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


data class DatabaseReponse(
    val result: DataBaseResult,
    val message: String
)

enum class DataBaseResult(
    val code: Int,
    val message: String
) {
    NONE(0, "None"),
    FAILURE(-1, "Failure"),
    SUCCESS(1, "Success");
}