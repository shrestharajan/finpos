package global.citytech.finpos.merchant.utils

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import android.provider.Settings
import android.util.Log
import global.citytech.finpos.merchant.presentation.amount.oldlayout.AmountActivity
import global.citytech.finposframework.comm.ConnectionParam
import global.citytech.finposframework.comm.HostInfo
import global.citytech.finposframework.comm.TcpClient
import global.citytech.finposframework.exceptions.FinPosException
import global.citytech.finposframework.log.Logger
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL


/**
 * Created by Unique Shakya on 7/7/2021.
 */

fun isConnectionAvailableWithHosts(
    primaryHostInfo: HostInfo,
    secondaryHostInfo: HostInfo
): Boolean {
    val connectionParam = ConnectionParam(primaryHostInfo, secondaryHostInfo)
    try {
        TcpClient(connectionParam).use { tcpClient ->
            val hostInfo = tcpClient.dial()
            Logger.getLogger("ConnectionUtils").log("::: SELECTED HOST ::: $hostInfo")
        }
    } catch (fe: FinPosException) {
        fe.printStackTrace()
        if (fe.type == FinPosException.ExceptionType.UNABLE_TO_CONNECT) return false
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return true
}

fun isConnectionAvailable(): Boolean {
    var p1:Process? = null
    try {
        p1 =  Runtime.getRuntime().exec("ping -c 1 8.8.8.8");
        val returnVal = p1.waitFor();
        val reachable = (returnVal == 0);
        return reachable;
    } catch (e: java.lang.Exception) {
        e.printStackTrace();
    } finally {
        if (p1 != null) {
            p1.inputStream.close()
            p1.outputStream.close()
            p1.errorStream.close()
            p1.destroy()
        }
    }
    return false;
}

fun isAirplaneModeOn(context: Context): Boolean {
    return Settings.System.getInt(
        context.contentResolver,
        Settings.Global.AIRPLANE_MODE_ON, 0
    ) != 0
}


