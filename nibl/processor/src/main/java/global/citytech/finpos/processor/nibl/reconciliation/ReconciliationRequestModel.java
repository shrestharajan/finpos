package global.citytech.finpos.processor.nibl.reconciliation;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.keymgmt.keys.KeyService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reconciliation.ReconciliationRequestParameter;

/**
 * Created by Unique Shakya on 9/22/2020.
 */
public class ReconciliationRequestModel implements UseCase.Request, ReconciliationRequestParameter {

    private final ReconciliationRepository reconciliationRepository;
    private final PrinterService printerService;
    private final ApplicationRepository applicationRepository;
    private final TransactionRepository transactionRepository;
    private String applicationPackageName;
    private KeyService hardwareKeyService;
    private final boolean previousSettlementActive;

    public ReconciliationRequestModel(
            ReconciliationRepository reconciliationRepository,
            PrinterService printerService,
            ApplicationRepository applicationRepository,
            TransactionRepository transactionRepository,
            boolean previousSettlementActive
    ) {
        this.reconciliationRepository = reconciliationRepository;
        this.printerService = printerService;
        this.applicationRepository = applicationRepository;
        this.transactionRepository = transactionRepository;
        this.previousSettlementActive = previousSettlementActive;
    }

    public boolean isPreviousSettlementActive() {
        return previousSettlementActive;
    }

    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public ReconciliationRepository getReconciliationRepository() {
        return reconciliationRepository;
    }

    public ApplicationRepository getApplicationRepository() {
        return applicationRepository;
    }

    public String getApplicationPackageName() {
        return applicationPackageName;
    }

    public void setApplicationPackageName(String applicationPackageName) {
        this.applicationPackageName = applicationPackageName;
    }

    public KeyService getHardwareKeyService() {
        return hardwareKeyService;
    }

    public void setHardwareKeyService(KeyService hardwareKeyService) {
        this.hardwareKeyService = hardwareKeyService;
    }
}
