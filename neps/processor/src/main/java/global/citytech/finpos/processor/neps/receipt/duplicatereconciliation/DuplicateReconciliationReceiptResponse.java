package global.citytech.finpos.processor.neps.receipt.duplicatereconciliation;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.duplicatereceipt.DuplicateReceiptResponseParameter;
import global.citytech.finposframework.switches.receipt.duplicatereconciliation.DuplicateReconciliationReceiptRequestParameter;
import global.citytech.finposframework.switches.receipt.duplicatereconciliation.DuplicateReconciliationReceiptResponseParameter;

/**
 * Created by Saurav Ghimire on 6/16/21.
 * sauravnghimire@gmail.com
 */


public class DuplicateReconciliationReceiptResponse implements UseCase.Response, DuplicateReconciliationReceiptResponseParameter {
    private Result result;
    private String message;

    public DuplicateReconciliationReceiptResponse(Result result, String message) {
        this.result = result;
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }
}
