package global.citytech.finpos.merchant.framework.datasource.app

import android.content.ContentValues
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.app.db.AppDatabase
import global.citytech.finpos.merchant.data.datasource.app.db.LocalDatabaseSource
import global.citytech.finpos.merchant.domain.model.app.*
import global.citytech.finpos.merchant.domain.model.device.DeviceResponse
import global.citytech.finpos.merchant.domain.model.device.DeviceResult
import global.citytech.finpos.merchant.domain.model.device.LoadParameterRequest
import global.citytech.finpos.merchant.extensions.putAsJson
import global.citytech.finpos.merchant.extensions.putValue
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.DeviceConfigurationSourceImpl
import global.citytech.finpos.merchant.presentation.model.ConfigResponse
import global.citytech.finpos.merchant.presentation.model.response.Crypto
import global.citytech.finpos.merchant.presentation.model.response.MessageCode
import global.citytech.finpos.merchant.presentation.model.response.Terminal
import global.citytech.finpos.merchant.presentation.model.response.TlsCertificate
import global.citytech.finpos.merchant.utils.JsonUtils
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.keymgmt.keys.KeyAlgorithm
import global.citytech.finposframework.hardware.keymgmt.keys.KeyType
import global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyInjectionRequest
import global.citytech.finposframework.hardware.utility.DeviceApiConstants
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.switches.posmode.PosMode
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.utility.AmountUtils
import global.citytech.finposframework.utility.StringUtils
import global.citytech.finposframework.utility.TMSConfigurationConstants.*
import io.reactivex.Observable
import org.json.JSONObject
import java.util.*


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 *
 * Modified by Rishav Chudal on 5/13/21.
 * Citytech
 * rishav.chudal@citytech.global
 */
class LocalDatabaseSourceImpl : LocalDatabaseSource {
    private val logger = Logger(LocalDatabaseSourceImpl::class.java.simpleName)

    override fun getHost(): Observable<List<Host>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getHostDao().getHostList()
        }
    }

    override fun getMerchant(): Observable<List<Merchant>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getMerchantDao().getMerchantList()
        }
    }

    override fun getLogo(): Observable<List<Logo>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getLogoDao().getLogoList()
        }
    }

    override fun getAidParams(): Observable<List<AidParam>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getAidParamDao().getAidParamList()
        }
    }

    override fun getCardScheme(): Observable<List<CardScheme>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getCardSchemeDao().getCardScheme()
        }
    }

    override fun getEmvKeys(): Observable<List<EmvKey>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getEmvKeyDao().getEmvKeys()
        }
    }

    override fun getEmvParams(): Observable<List<EmvParam>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getEmvParamDao().getEmvParams()
        }
    }

    override fun getMerchantTransactionLog(): Observable<List<MerchantTransactionLog>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getMerchantTransactionLogDao()
                .getMerchantTransactionLogs()
        }
    }

    override fun getActivityLog(): Observable<List<ActivityLog>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getActivityLogDao()
                .getActivityLogs()
        }
    }

    override fun getTransactionLogStan(stan: String): Observable<TransactionLog> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
                .getTransactionLogsByStan(stan)
        }
    }

    override fun getTransactionLogs(): Observable<List<TransactionLog>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
                .getApprovedTransactions()
        }
    }

    override fun getTransactionLogs(limit: Int, offset: Int): Observable<List<TransactionLog>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
                .getApprovedTransactions(limit, offset)
        }
    }

    override fun countApprovedTransactions(): Int {
        return AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .countApprovedTransactions()
    }

    override fun searchTransactionLogs(
        searchParam: String,
        limit: Int,
        offset: Int
    ): Observable<List<TransactionLog>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
                .searchApprovedTransactions(searchParam, limit, offset)
        }
    }

    override fun countSearchApprovedTransactions(searchParam: String): Int {
        return AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .countSearchApprovedTransactions(searchParam)
    }

    override fun getTransactionLogByInvoice(
        invoiceNumber: String,
        ignoreBatchNumber: Boolean
    ): Observable<TransactionLog> {
        return if (ignoreBatchNumber)
            Observable.fromCallable {
                AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
                    .getTransactionLogByInvoiceNumberWithoutBatchNumber(invoiceNumber)
            }
        else
            Observable.fromCallable {
                AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
                    .getTransactionLogBy(
                        invoiceNumber,
                        getFormattedSettlementBatchNumber()
                    )
            }
    }

    override fun getTransactionLogByRRN(rrn: String): Observable<TransactionLog> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
                .getTransactionLogByRRN(
                    rrn,
                    getFormattedSettlementBatchNumber()
                )
        }
    }

    override fun saveMerchantTransactionLog(merchantTransactionLog: MerchantTransactionLog): Observable<DatabaseReponse> {
        try {
            return Observable.fromCallable {
                AppDatabase.getInstance(NiblMerchant.INSTANCE).getMerchantTransactionLogDao()
                    .insert(merchantTransactionLog)
                DatabaseReponse(DataBaseResult.SUCCESS, "Success")
            }

        } catch (ex: Exception) {
            return Observable.fromCallable {
                DatabaseReponse(DataBaseResult.FAILURE, ex.message!!)
            }
        }

    }

    override fun nukeMerchantTransactionLog(): Observable<DatabaseReponse> {
        try {
            return Observable.fromCallable {
                AppDatabase.getInstance(NiblMerchant.INSTANCE).getMerchantTransactionLogDao()
                    .nukeTable()
                DatabaseReponse(DataBaseResult.SUCCESS, "Success")
            }
        } catch (ex: Exception) {
            return Observable.fromCallable {
                DatabaseReponse(DataBaseResult.FAILURE, ex.message!!)
            }
        }
    }

    override fun nukeActivityLog(): Observable<DatabaseReponse> {
        try {
            return Observable.fromCallable {
                AppDatabase.getInstance(NiblMerchant.INSTANCE).getActivityLogDao()
                    .nukeTable()
                DatabaseReponse(DataBaseResult.SUCCESS, "Success")
            }
        } catch (ex: Exception) {
            return Observable.fromCallable {
                DatabaseReponse(DataBaseResult.FAILURE, ex.message!!)
            }
        }
    }

    override fun deleteMerchantTransactionLogByStans(stans: List<String>): Observable<DatabaseReponse> {
        try {
            return Observable.fromCallable {
                AppDatabase.getInstance(NiblMerchant.INSTANCE).getMerchantTransactionLogDao()
                    .deleteByStans(stans)

                DatabaseReponse(DataBaseResult.SUCCESS, "Success")
            }
        } catch (ex: Exception) {
            return Observable.fromCallable {
                DatabaseReponse(DataBaseResult.FAILURE, ex.message!!)
            }
        }
    }

    override fun saveConfigResponseAndInjectKey(
        configResponse: ConfigResponse
    ): Observable<DeviceResponse> {
        return Observable.fromCallable {
            PreferenceManager.saveTerminalConfigPref(false)
            saveAndLoadConfigurations(configResponse)
        }
    }

    private fun saveAndLoadConfigurations(
        configResponse: ConfigResponse
    ): DeviceResponse {
        clearDataAndSaveConfigurations(configResponse)
        return loadTerminalParametersAndInjectTransactionKey(configResponse)
    }

    private fun clearDataAndSaveConfigurations(configResponse: ConfigResponse) {
        nukeTablesData()
        saveConfigurationsInDataBaseAndPreference(configResponse)
    }

    private fun loadTerminalParametersAndInjectTransactionKey(
        configResponse: ConfigResponse
    ): DeviceResponse {
        loadTerminalConfigurations()
        return loadTransactionKey(configResponse)
    }

    private fun loadTerminalConfigurations() {
        val deviceResponseForTerminalConfig = DeviceConfigurationSourceImpl(NiblMerchant.INSTANCE)
            .loadTerminalParameters(
                LoadParameterRequest(loadAllEMVConfigurationInHardware = true)
            ).blockingLast()
    }

    private fun loadTransactionKey(configResponse: ConfigResponse): DeviceResponse {
        val deviceResponse = DeviceConfigurationSourceImpl(NiblMerchant.INSTANCE)
            .inject(prepareKeyInjectionRequest(configResponse.crypto)).blockingLast()
        return checkDeviceResultOnLoadingTransactionKeyAndProceed(deviceResponse.result)
    }

    private fun checkDeviceResultOnLoadingTransactionKeyAndProceed(
        deviceResult: DeviceResult
    ): DeviceResponse {
        return if (deviceResult == DeviceResult.SUCCESS) {
            onSuccessLoadingTransactionKey()
        } else {
            onFailureLoadingTransactionKey()
        }
    }

    private fun onSuccessLoadingTransactionKey(): DeviceResponse {
        PreferenceManager.saveTerminalConfigPref(true)
        return DeviceResponse(
            DeviceResult.SUCCESS,
            "Terminal parameters are loaded and transaction keys are injected"
        )
    }

    private fun onFailureLoadingTransactionKey(): DeviceResponse {
        PreferenceManager.saveTerminalConfigPref(true)
        return DeviceResponse(
            DeviceResult.FAILURE,
            PosError.DEVICE_ERROR_DOWNLOAD_TMS_AGAIN.errorMessage
        )
    }

    private fun nukeTablesData(isReset: Boolean = false) {
        nukeHostTableData()
        nukeMerchantTableData()
        nukeLogoTableData()
        nukeAidParamTableData()
        nukeCardSchemeTableData()
        nukeEMVKeyTableData()
        nukeEMVParamTableData()
        if (isReset) {
            nukeTransactionConfigData()
            nukeConfigData()
        }
        nukeMessageCodeTableData()
        nukeDisclaimerTableData()
    }

    private fun saveConfigurationsInDataBaseAndPreference(configResponse: ConfigResponse) {
        saveDeviceConfigurationData(configResponse)
        saveTipLimitConfigurationByDefaultValue()
        saveHostsData(configResponse.hosts, configResponse.keys?.tlsCertificate)
        saveMerchant(configResponse.merchant, configResponse.terminal)
        saveLogo(configResponse.logo)
        saveAidParam(configResponse.terminalParameter!!.aidParameters)
        saveEmvParam(configResponse.terminalParameter!!.emvParameters)
        saveEmvKeys(configResponse.terminalParameter!!.emvKeys)
        saveCardScheme(configResponse.terminalParameter!!.cardSchemes)
        saveResponseCodes(configResponse.terminalParameter!!.messageCodes)
    }

    private fun nukeHostTableData() {
        val hostDao = AppDatabase.getInstance(NiblMerchant.INSTANCE).getHostDao()
        hostDao.nukeTableData()
    }

    private fun nukeMerchantTableData() {
        val merchantDao = AppDatabase.getInstance(NiblMerchant.INSTANCE).getMerchantDao()
        merchantDao.nukeTableData()
    }

    private fun nukeLogoTableData() {
        val logoDao = AppDatabase.getInstance(NiblMerchant.INSTANCE).getLogoDao()
        logoDao.nukeTableData()
    }

    private fun nukeAidParamTableData() {
        val aidParamDao = AppDatabase.getInstance(NiblMerchant.INSTANCE).getAidParamDao()
        aidParamDao.nukeTableData()
    }

    private fun nukeCardSchemeTableData() {
        val cardSchemeDao = AppDatabase.getInstance(NiblMerchant.INSTANCE).getCardSchemeDao()
        cardSchemeDao.nukeTableData()
    }

    private fun nukeEMVKeyTableData() {
        val emvKeysDao = AppDatabase.getInstance(NiblMerchant.INSTANCE).getEmvKeyDao()
        emvKeysDao.nukeTableData()
    }

    private fun nukeEMVParamTableData() {
        val emvParamDao = AppDatabase.getInstance(NiblMerchant.INSTANCE).getEmvParamDao()
        emvParamDao.nukeTableData()
    }

    private fun nukeTransactionConfigData() {
        val transactionConfigDao =
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionConfigDao()
        transactionConfigDao.nukeTableData()
    }

    private fun nukeConfigData() {
        val transactionConfigDao =
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getConfigDao()
        transactionConfigDao.nukeTableData()
    }


    private fun nukeMessageCodeTableData() {
        val messageCodeDao = AppDatabase.getInstance(NiblMerchant.INSTANCE).getMessageCodeDao()
        messageCodeDao.nukeTableData()
    }

    private fun nukeDisclaimerTableData() {
        val disclaimerDao = AppDatabase.getInstance(NiblMerchant.INSTANCE).getDisclaimerDao()
        disclaimerDao.deleteAll()
    }

    private fun saveConfigs(configResponse: ConfigResponse) {
        AppDatabase.getInstance(NiblMerchant.INSTANCE).getConfigDao()
            .insert(configResponse.mapToEntity())
    }

    override fun isTransactionPresent(invoiceNumber: String): Observable<Boolean> {
        return Observable.fromCallable {
            (AppDatabase
                .getInstance(NiblMerchant.INSTANCE)
                .getTransactionLogDao()
                .getTransactionLogBy(
                    invoiceNumber,
                    getFormattedSettlementBatchNumber(),
                ) != null)
        }
    }

    override fun isTransactionPresentByAuthCode(authCode: String): Observable<Boolean> {
        return Observable.fromCallable {

            AppDatabase.getInstance(NiblMerchant.INSTANCE)
                .getTransactionLogDao()
                .getTransactionLogByAuthCode(authCode) != null &&
                    AppDatabase.getInstance(NiblMerchant.INSTANCE)
                        .getTransactionLogDao()
                        .getTransactionLogByAuthCode(authCode)!!.transactionType == Jsons.toJsonObj(
                TransactionType.PRE_AUTH
            )
        }
    }

    override fun isTransactionPresentByRrn(rrn: String): Observable<Boolean> {
        return Observable.fromCallable {
            (AppDatabase
                .getInstance(NiblMerchant.INSTANCE)
                .getTransactionLogDao()
                .getTransactionLogByRRN(
                    rrn,
                    getFormattedSettlementBatchNumber()
                ) != null)
        }
    }

    private fun saveDeviceConfigurationData(configResponse: ConfigResponse) {
        val deviceConfiguration = DeviceConfiguration.get()
        deviceConfiguration.purchaseCardTypeMap = preparePurchaseCardTypeMap()
        deviceConfiguration.terminalId = configResponse.terminal!!.switchId
        deviceConfiguration.merchantId = configResponse.merchant!!.switchId
        deviceConfiguration.merchantLogo = configResponse.logo!!.printLogo
        deviceConfiguration.merchantName = configResponse.merchant!!.name
        deviceConfiguration.retailerName = configResponse.merchant!!.name
        deviceConfiguration.retailerAddress = configResponse.merchant!!.address
        deviceConfiguration.dashboardDisplayLogo = configResponse.logo?.displayLogo
        deviceConfiguration.retailerPhoneNumber = configResponse.merchant!!.phoneNumber
        deviceConfiguration.debugMode = PreferenceManager.getDebugModePref(false)
        deviceConfiguration.listOfHosts = configResponse.hosts
        deviceConfiguration.currencyCode =
            configResponse.terminalParameter!!.emvParameters!!.transactionCurrencyCode!!.value as String
        DeviceConfiguration.set(deviceConfiguration)
        PreferenceManager.saveDeviceConfiguration(Jsons.toJsonObj(DeviceConfiguration))
        AppDatabase.getInstance(NiblMerchant.INSTANCE).getConfigDao().insert(
            Config(
                "1",
                getAmountInLowerCurrencyLong(configResponse.amountLimitPerTransaction),
                configResponse.reconcileTime,
                Jsons.toJsonObj(configResponse.keys),
                ""
            )
        )
    }

    private fun getAmountInLowerCurrencyLong(amountInDouble: Double?): Long {
        var amountInLowerCurrencyLong = 0L
        amountInDouble?.apply {
            amountInLowerCurrencyLong = AmountUtils.toLowerCurrencyInLong(this)
        }
        return amountInLowerCurrencyLong
    }


    private fun saveTipLimitConfigurationByDefaultValue() {
        PreferenceManager.saveTipLimit(PreferenceManager.retrieveTipLimit())
    }

    private fun preparePurchaseCardTypeMap(): Map<TransactionType, List<CardType>>? {
        val purchaseCardTypeMap: HashMap<TransactionType, List<CardType>> = HashMap()
        enumValues<TransactionType>().forEach {
            purchaseCardTypeMap[it] = retrieveCardTypeForPurchaseType(it)
        }
        return purchaseCardTypeMap
    }

    private fun retrieveCardTypeForPurchaseType(transactionType: TransactionType): List<CardType> {
        val listOfCardType: ArrayList<CardType> = ArrayList()
        listOfCardType.add(CardType.ICC)
        listOfCardType.add(CardType.MAG)
        listOfCardType.add(CardType.PICC)
        addManualTransactionIfAllowedOnly(listOfCardType)
        return listOfCardType
    }

    private fun saveHostsData(
        hosts: List<global.citytech.finpos.nibl.merchant.presentation.model.response.Host>?,
        tlsCertificate: TlsCertificate?
    ) {
        if (!hosts.isNullOrEmpty()) {
            for ((index, host) in hosts.withIndex()) {
                val contentValues = ContentValues()
                contentValues.putValue(
                    global.citytech.finpos.nibl.merchant.presentation.model.response.Host.COLUMN_ID,
                    index.toString()
                )
                contentValues.putValue(
                    global.citytech.finpos.nibl.merchant.presentation.model.response.Host.COLUMN_IP,
                    host.ip
                )
                contentValues.putValue(
                    global.citytech.finpos.nibl.merchant.presentation.model.response.Host.COLUMN_PORT,
                    host.port
                )
                contentValues.putValue(
                    global.citytech.finpos.nibl.merchant.presentation.model.response.Host.COLUMN_NII,
                    host.nii
                )
                contentValues.putValue(
                    global.citytech.finpos.nibl.merchant.presentation.model.response.Host.COLUMN_CONNECTION_TIME_OUT,
                    host.connectionTimeout
                )
                contentValues.putValue(
                    global.citytech.finpos.nibl.merchant.presentation.model.response.Host.COLUMN_ORDER,
                    host.order
                )
                contentValues.putValue(
                    global.citytech.finpos.nibl.merchant.presentation.model.response.Host.COLUMN_RETRY_LIMIT,
                    host.retryLimit
                )

                tlsCertificate?.let {
                    contentValues.put(
                        TlsCertificate.COLUMN_TLS_CERTIFICATE,
                        it.mapToJson()
                    )
                }

                val hostDao = AppDatabase.getInstance(NiblMerchant.INSTANCE).getHostDao()
                hostDao.insert(Host.fromContentValues(contentValues))
            }
        }
    }

    private fun saveMerchant(
        merchant: global.citytech.finpos.merchant.presentation.model.response.Merchant?,
        terminal: Terminal?
    ) {
        if (merchant != null && terminal != null) {
            val contentValues = ContentValues()
            contentValues.putValue(
                global.citytech.finpos.merchant.presentation.model.response.Merchant.COLUMN_ID,
                "0"
            )
            contentValues.putValue(
                global.citytech.finpos.merchant.presentation.model.response.Merchant.COLUMN_ADDRESS,
                merchant.address
            )
            contentValues.putValue(
                global.citytech.finpos.merchant.presentation.model.response.Merchant.COLUMN_NAME,
                merchant.name
            )
            contentValues.putValue(
                global.citytech.finpos.merchant.presentation.model.response.Merchant.COLUMN_SWITCH_ID,
                merchant.switchId
            )
            contentValues.putValue(
                global.citytech.finpos.merchant.presentation.model.response.Merchant.COLUMN_PHONE_NUMBER,
                merchant.phoneNumber
            )
            contentValues.putValue(
                global.citytech.finpos.merchant.presentation.model.response.Merchant.COLUMN_TERMINAL_ID,
                terminal.switchId
            )

            val merchantDao = AppDatabase.getInstance(NiblMerchant.INSTANCE).getMerchantDao()
            merchantDao.insert(Merchant.fromContentValues(contentValues))
        }
    }

    private fun saveLogo(logo: global.citytech.finpos.nibl.merchant.presentation.model.response.Logo?) {
        if (logo != null) {
            val contentValues = ContentValues()
            contentValues.putValue(
                global.citytech.finpos.nibl.merchant.presentation.model.response.Logo.COLUMN_ID,
                "0"
            )
            contentValues.putValue(
                global.citytech.finpos.nibl.merchant.presentation.model.response.Logo.COLUMN_APP_WALLPAPER,
                logo.appWallpaper
            )
            contentValues.putValue(
                global.citytech.finpos.nibl.merchant.presentation.model.response.Logo.COLUMN_DISPLAY_LOGO,
                logo.displayLogo
            )
            contentValues.putValue(
                global.citytech.finpos.nibl.merchant.presentation.model.response.Logo.COLUMN_PRINT_LOGO,
                logo.printLogo
            )

            val logoDao = AppDatabase.getInstance(NiblMerchant.INSTANCE).getLogoDao()
            logoDao.insert(Logo.fromContentValues(contentValues))
        }
    }

    private fun saveAidParam(
        aidParams: List<global.citytech.finpos.merchant.presentation.model.response.AidParam>?
    ) {
        if (!aidParams.isNullOrEmpty()) {
            for ((index, aidParam) in aidParams.withIndex()) {
                val contentValues = ContentValues()
                contentValues.putValue(
                    global.citytech.finpos.merchant.presentation.model.response.AidParam.COLUMN_ID,
                    index.toString()
                )
                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.AidParam.COLUMN_AID,
                    aidParam.aid
                )
                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.AidParam.COLUMN_LABEL,
                    aidParam.label
                )
                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.AidParam.COLUMN_TERMINAL_AID_VERSION,
                    aidParam.terminalAidVersion
                )
                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.AidParam.COLUMN_DEFAULT_TDOL,
                    aidParam.defaultTDOL
                )
                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.AidParam.COLUMN_DEFAULT_DDOL,
                    aidParam.defaultDDOL
                )
                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.AidParam.COLUMN_DENIAL_ACTION_CODE,
                    aidParam.denialActionCode
                )
                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.AidParam.COLUMN_ONLINE_ACTION_CODE,
                    aidParam.onlineActionCode
                )
                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.AidParam.COLUMN_DEFAULT_ACTION_CODE,
                    aidParam.defaultActionCode
                )
                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.AidParam.COLUMN_TERMINAL_FLOOR_LIMIT,
                    aidParam.terminalFloorLimit
                )
                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.AidParam.COLUMN_CONTACTLESS_FLOOR_LIMIT,
                    aidParam.contactlessFloorLimit
                )
                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.AidParam.COLUMN_CONTACTLESS_TRANSACTION_LIMIT,
                    aidParam.contactlessTransactionLimit
                )
                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.AidParam.COLUMN_CVM_LIMIT,
                    aidParam.cvmLimit
                )
                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.AidParam.COLUMN_ADDITIONAL_DATA,
                    aidParam.additionalData
                )
                val aidParamsDao = AppDatabase.getInstance(NiblMerchant.INSTANCE).getAidParamDao()
                aidParamsDao.insert(AidParam.fromContentValues(contentValues))
            }
        }
    }

    private fun saveEmvParam(
        emvParameter: global.citytech.finpos.merchant.presentation.model.response.EmvParam?
    ) {
        if (emvParameter != null) {
            val contentValues = ContentValues()
            contentValues.putValue(Merchant.COLUMN_ID, "0")
            contentValues.putAsJson(
                global.citytech.finpos.merchant.presentation.model.response.EmvParam.COLUMN_TTQ,
                emvParameter.ttq
            )

            contentValues.putAsJson(
                global.citytech.finpos.merchant.presentation.model.response.EmvParam.COLUMN_TRANSACTION_CURRENCY_EXPONENT,
                emvParameter.transactionCurrencyExponent
            )

            contentValues.putAsJson(
                global.citytech.finpos.merchant.presentation.model.response.EmvParam.COLUMN_TRANSACTION_CURRENCY_CODE,
                emvParameter.transactionCurrencyCode
            )

            contentValues.putAsJson(
                global.citytech.finpos.merchant.presentation.model.response.EmvParam.COLUMN_TERMINAL_TYPE,
                emvParameter.terminalType
            )

            contentValues.putAsJson(
                global.citytech.finpos.merchant.presentation.model.response.EmvParam.COLUMN_TERMINAL_ID,
                emvParameter.terminalId
            )

            contentValues.putAsJson(
                global.citytech.finpos.merchant.presentation.model.response.EmvParam.COLUMN_TERMINAL_COUNTRY_CODE,
                emvParameter.terminalCountryCode
            )

            contentValues.putAsJson(
                global.citytech.finpos.merchant.presentation.model.response.EmvParam.COLUMN_TERMINAL_CAPABILITIES,
                emvParameter.terminalCapabilities
            )

            contentValues.putAsJson(
                global.citytech.finpos.merchant.presentation.model.response.EmvParam.COLUMN_MERCHANT_NAME,
                emvParameter.merchantName
            )

            contentValues.putAsJson(
                global.citytech.finpos.merchant.presentation.model.response.EmvParam.COLUMN_MERCHANT_IDENTIFIER,
                emvParameter.merchantIdentifier
            )

            contentValues.putAsJson(
                global.citytech.finpos.merchant.presentation.model.response.EmvParam.COLUMN_MERCHANT_CATEGORY_CODE,
                emvParameter.merchantCategoryCode
            )

            contentValues.putAsJson(
                global.citytech.finpos.merchant.presentation.model.response.EmvParam.COLUMN_FORCE_ONLINE_FLAG,
                emvParameter.forceOnlineFlag
            )

            contentValues.putAsJson(
                global.citytech.finpos.merchant.presentation.model.response.EmvParam.COLUMN_ADDITIONAL_TERMINAL_CAPABILITIES,
                emvParameter.additionalTerminalCapabilities
            )

            contentValues.putAsJson(
                global.citytech.finpos.merchant.presentation.model.response.EmvParam.COLUMN_ADDITIONAL_DATA,
                emvParameter.additionalData
            )

            val emvParamDao = AppDatabase.getInstance(NiblMerchant.INSTANCE).getEmvParamDao()
            emvParamDao.insert(EmvParam.fromContentValues(contentValues))
        }
    }

    private fun saveEmvKeys(
        emvKeys: List<global.citytech.finpos.merchant.presentation.model.response.EmvKey>?
    ) {
        if (!emvKeys.isNullOrEmpty()) {
            for ((index, emvKey) in emvKeys.withIndex()) {
                val contentValues = ContentValues()
                contentValues.putValue(EmvKey.COLUMN_ID, index.toString())
                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.EmvKey.COLUMN_RID,
                    emvKey.rid
                )

                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.EmvKey.COLUMN_INDEX,
                    emvKey.index
                )

                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.EmvKey.COLUMN_LENGTH,
                    emvKey.length
                )

                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.EmvKey.COLUMN_EXPONENT,
                    emvKey.exponent
                )

                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.EmvKey.COLUMN_MODULES,
                    emvKey.modules
                )

                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.EmvKey.COLUMN_HASH_ID,
                    emvKey.hashId
                )

                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.EmvKey.COLUMN_KEY_SIGNATURE_ID,
                    emvKey.keySignatureId
                )

                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.EmvKey.COLUMN_CHECKSUM,
                    emvKey.checkSum
                )

                contentValues.putAsJson(
                    global.citytech.finpos.merchant.presentation.model.response.EmvKey.COLUMN_EXPIRY_DATE,
                    emvKey.expiryDate
                )


                val emvKeyDao = AppDatabase.getInstance(NiblMerchant.INSTANCE).getEmvKeyDao()
                emvKeyDao.insert(EmvKey.fromContentValues(contentValues))
            }
        }
    }

    private fun saveCardScheme(
        cardSchemeParameters: ArrayList<Any>?
    ) {
        if (!cardSchemeParameters.isNullOrEmpty()) {
            val cardSchemeMaps = this.getSchemeAsMap(cardSchemeParameters)
            var id = 0
            for (cardSchemeMap in cardSchemeMaps) {
                val contentValues = ContentValues()
                for ((key, value) in cardSchemeMap) {
                    val cardSchemeId = cardSchemeMap[key]!!["cardSchemeId"]!!["value"]
                    contentValues.putValue(CardScheme.COLUMN_CARD_SCHEME_ID, cardSchemeId!!)
                    for ((innerKey, innerValue) in value) {
                        contentValues.putValue(
                            global.citytech.finpos.merchant.presentation.model.response.CardScheme.COLUMN_ID,
                            "${cardSchemeId}_${innerKey}"
                        )
                        contentValues.putValue(
                            global.citytech.finpos.merchant.presentation.model.response.CardScheme.COLUMN_CARD_SCHEME_ID,
                            cardSchemeId
                        )
                        contentValues.putValue(
                            global.citytech.finpos.merchant.presentation.model.response.CardScheme.COLUMN_ATTRIBUTE,
                            innerKey
                        )
                        contentValues.putValue(
                            global.citytech.finpos.merchant.presentation.model.response.CardScheme.COLUMN_DISPLAY_LABEL,
                            innerValue["displayLabel"]
                        )
                        contentValues.putValue(
                            global.citytech.finpos.merchant.presentation.model.response.CardScheme.COLUMN_PRINT_LABEL,
                            innerValue["printLabel"]
                        )
                        contentValues.putValue(
                            global.citytech.finpos.merchant.presentation.model.response.CardScheme.COLUMN_VALUE,
                            innerValue["value"]
                        )

                        val cardSchemeDao =
                            AppDatabase.getInstance(NiblMerchant.INSTANCE).getCardSchemeDao()
                        cardSchemeDao.insert(CardScheme.fromContentValues(contentValues))
                        id++
                    }
                }
            }
        }
    }

    private fun saveResponseCodes(messageCodes: List<MessageCode>?) {
        if (!messageCodes.isNullOrEmpty()) {
            for ((
                index,
                responseCode
            ) in messageCodes.withIndex()) {
                val responseCodeDao =
                    AppDatabase.getInstance(NiblMerchant.INSTANCE).getMessageCodeDao()
                responseCodeDao.insert(responseCode.mapToEntity(index))
            }
        }
    }

    private fun getSchemeAsMap(
        cardSchemeParameters: ArrayList<Any>
    ): List<Map<String, Map<String, HashMap<String, String>>>> {
        val cardSchemeMapList =
            arrayListOf<HashMap<String, HashMap<String, HashMap<String, String>>>>()
        for (cardSchemeParameter in cardSchemeParameters) {
            val cardSchemeParametersInString = Jsons.toJsonObj(cardSchemeParameter)
            val jsonObject = JSONObject(cardSchemeParametersInString)
            val cardSchemeSegmentsMap =
                JsonUtils.toMap(jsonObject) as HashMap<String, HashMap<String, HashMap<String, String>>>
            cardSchemeMapList.add(cardSchemeSegmentsMap)
        }
        return cardSchemeMapList
    }

    private fun prepareKeyInjectionRequest(crypto: Crypto?): KeyInjectionRequest {
        val key = crypto?.values?.key1.plus(crypto?.values?.key2)
        val keyInjectionRequest = KeyInjectionRequest(
            NiblMerchant.INSTANCE.packageName,
            KeyType.KEY_TYPE_TMK,
            KeyAlgorithm.TYPE_3DES,
            key.toUpperCase(Locale.getDefault()),
            true
        )
        keyInjectionRequest.keyIndex = DeviceApiConstants.MASTER_KEY_INDEX
        return keyInjectionRequest
    }

    override fun getAdminPassword(): Observable<String> {
        return Observable.fromCallable {
            val password = getAdminPasswordFromEMVParametersData()
            checkPasswordForEmptyOrNullValueAndProceed(password)
        }
    }

    private fun getAdminPasswordFromEMVParametersData(): String {
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
        return applicationRepository.retrieveFromAdditionalDataEmvParameters(ADMIN_CREDENTIALS)
    }

    private fun checkPasswordForEmptyOrNullValueAndProceed(adminPassword: String): String {
        return if (adminPassword.isNullOrEmptyOrBlank()) {
            defaultAdminPassword()
        } else {
            adminPassword
        }
    }

    private fun defaultAdminPassword(): String {
        return BuildConfig.ADMIN_DEFAULT_SANCHO
    }

    private fun addManualTransactionIfAllowedOnly(listOfCardType: ArrayList<CardType>) {
        if (manualTransactionIsAllowed()) {
            listOfCardType.add(CardType.MANUAL)
        }
    }

    private fun manualTransactionIsAllowed(): Boolean {
        val deviceConfigurationSource = DeviceConfigurationSourceImpl(NiblMerchant.INSTANCE)
        return deviceConfigurationSource.manualTransactionIsAllowed()
    }

    override fun getEnabledTransactionsForThePosMode(posMode: PosMode): Observable<List<TransactionType>> {
        return Observable.fromCallable {
            val transactionMenuConfiguration = getTransactionMenuConfigurationFromTMSData()
            validateConfigurationDataAndProceedForThePosMode(
                posMode,
                transactionMenuConfiguration
            )
        }
    }

    private fun getTransactionMenuConfigurationFromTMSData(): String {
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
        return applicationRepository.retrieveFromAdditionalDataEmvParameters(TRANSACTION_MENU)
    }

    private fun validateConfigurationDataAndProceedForThePosMode(
        posMode: PosMode,
        transactionMenConfiguration: String
    ): List<TransactionType> {
        return if (isTransactionMenuConfigurationValid(transactionMenConfiguration)) {
            prepareEnabledTransactionsForThePosModeWithDefaultMenuConfigurationData(posMode)
        } else {
            prepareEnabledTransactionsForThePosModeWithTMSMenuConfigurationData(
                posMode,
                transactionMenConfiguration
            )
        }
    }

    private fun isTransactionMenuConfigurationValid(transactionMenConfiguration: String): Boolean {
        return (transactionMenConfiguration.isNullOrEmptyOrBlank()
                || transactionMenConfiguration.length < 16)
    }

    private fun prepareEnabledTransactionsForThePosModeWithDefaultMenuConfigurationData(
        posMode: PosMode
    ): List<TransactionType> {
        return if (posMode == PosMode.RETAILER) {
            prepareEnabledTransactionsForRetailerModeWithDefaultConfigurationData()
        } else {
            prepareEnabledTransactionsForCashModeWithDefaultConfigurationData()
        }
    }

    private fun prepareEnabledTransactionsForRetailerModeWithDefaultConfigurationData(): List<TransactionType> {
        val defaultRetailerModeMenuConfiguration = DEFAULT_RETAILER_MODE_TRANSACTIONS_CONFIGURATION
        return checkConfigurationAndReturnEnabledTransactionsForRetailer(
            defaultRetailerModeMenuConfiguration
        )
    }

    private fun prepareEnabledTransactionsForCashModeWithDefaultConfigurationData(): List<TransactionType> {
        val defaultCashModeMenuConfiguration = DEFAULT_CASH_MODE_TRANSACTIONS_CONFIGURATION
        return checkConfigurationAndReturnEnabledTransactionsForCash(
            defaultCashModeMenuConfiguration
        )
    }


    private fun prepareEnabledTransactionsForThePosModeWithTMSMenuConfigurationData(
        posMode: PosMode,
        transactionMenConfiguration: String
    ): List<TransactionType> {
        return if (posMode == PosMode.RETAILER) {
            checkConfigurationAndReturnEnabledTransactionsForRetailer(transactionMenConfiguration)
        } else {
            checkConfigurationAndReturnEnabledTransactionsForCash(transactionMenConfiguration)
        }
    }

    private fun checkConfigurationAndReturnEnabledTransactionsForRetailer(
        transactionMenConfiguration: String
    ): List<TransactionType> {
        this.logger.debug(
            "Retailer Transaction Menu Configuration ::: ".plus(
                transactionMenConfiguration
            )
        )
        val enabledTransactions: ArrayList<TransactionType> = ArrayList()
        val configurationOffsetArray = transactionMenConfiguration.toCharArray()
        addRetailerModeTransactionIfEnabledInConfiguration(
            enabledTransactions,
            configurationOffsetArray
        )
        return enabledTransactions
    }

    private fun addRetailerModeTransactionIfEnabledInConfiguration(
        enabledTransactions: ArrayList<TransactionType>,
        configurationOffsetArray: CharArray
    ) {
        addTransactionTypeIfEnabledInConfigurationOffsetOnly(
            enabledTransactions,
            configurationOffsetArray,
            TransactionType.PURCHASE
        )

        addTransactionTypeIfEnabledInConfigurationOffsetOnly(
            enabledTransactions,
            configurationOffsetArray,
            TransactionType.VOID
        )

        addTransactionTypeIfEnabledInConfigurationOffsetOnly(
            enabledTransactions,
            configurationOffsetArray,
            TransactionType.PRE_AUTH
        )

        addTransactionTypeIfEnabledInConfigurationOffsetOnly(
            enabledTransactions,
            configurationOffsetArray,
            TransactionType.AUTH_COMPLETION
        )

        addTransactionTypeIfEnabledInConfigurationOffsetOnly(
            enabledTransactions,
            configurationOffsetArray,
            TransactionType.TIP_ADJUSTMENT
        )

        addTransactionTypeIfEnabledInConfigurationOffsetOnly(
            enabledTransactions,
            configurationOffsetArray,
            TransactionType.REFUND
        )
    }

    private fun checkConfigurationAndReturnEnabledTransactionsForCash(
        transactionMenConfiguration: String
    ): List<TransactionType> {
        this.logger.debug(
            "Cash Mode Transaction Menu Configuration ::: ".plus(
                transactionMenConfiguration
            )
        )
        val enabledTransactions: ArrayList<TransactionType> = ArrayList()
        val configurationOffsetArray = transactionMenConfiguration.toCharArray()
        addCashModeTransactionIfEnabledInConfiguration(
            enabledTransactions,
            configurationOffsetArray
        )
        return enabledTransactions
    }

    private fun addCashModeTransactionIfEnabledInConfiguration(
        enabledTransactions: ArrayList<TransactionType>,
        configurationOffsetArray: CharArray
    ) {
        addTransactionTypeIfEnabledInConfigurationOffsetOnly(
            enabledTransactions,
            configurationOffsetArray,
            TransactionType.CASH_ADVANCE
        )

        addTransactionTypeIfEnabledInConfigurationOffsetOnly(
            enabledTransactions,
            configurationOffsetArray,
            TransactionType.CASH_VOID
        )

        addTransactionTypeIfEnabledInConfigurationOffsetOnly(
            enabledTransactions,
            configurationOffsetArray,
            TransactionType.CASH_IN
        )

        addTransactionTypeIfEnabledInConfigurationOffsetOnly(
            enabledTransactions,
            configurationOffsetArray,
            TransactionType.BALANCE_INQUIRY
        )

        addTransactionTypeIfEnabledInConfigurationOffsetOnly(
            enabledTransactions,
            configurationOffsetArray,
            TransactionType.MINI_STATEMENT
        )
    }

    private fun addTransactionTypeIfEnabledInConfigurationOffsetOnly(
        enabledTransactions: ArrayList<TransactionType>,
        configurationOffsetArray: CharArray,
        transactionType: TransactionType
    ) {
        val transactionOffset = this.getTransactionOffset(transactionType)
        if (configurationOffsetArray[transactionOffset] == BuildConfig.TRANSACTION_ENABLED.single()) {
            enabledTransactions.add(transactionType)
        }
    }

    private fun getTransactionOffset(transactionType: TransactionType): Int {
        return when (transactionType) {
            TransactionType.PRE_AUTH -> PRE_AUTH_TRANSACTION_OFFSET
            TransactionType.VOID -> VOID_TRANSACTION_OFFSET
            TransactionType.REFUND -> REFUND_TRANSACTION_OFFSET
            TransactionType.AUTH_COMPLETION -> AUTH_COMPLETION_TRANSACTION_OFFSET
            TransactionType.TIP_ADJUSTMENT -> TIP_ADJUSTMENT_TRANSACTION_OFFSET
            TransactionType.CASH_ADVANCE -> CASH_ADVANCE_TRANSACTION_OFFSET
            TransactionType.CASH_IN -> CASH_DEPOSIT_TRANSACTION_OFFSET
            TransactionType.BALANCE_INQUIRY -> BALANCE_INQUIRY_TRANSACTION_OFFSET
            TransactionType.MINI_STATEMENT -> MINI_STATEMENT_TRANSACTION_OFFSET
            TransactionType.CASH_VOID -> CASH_VOID_TRANSACTION_OFFSET
            else -> SALES_TRANSACTION_OFFSET
        }
    }

    override fun updateReconciliationBatchNumber(batchNumber: Int) {
        logger.log("Batch Number to be updated ::: ".plus(batchNumber))
        AppDatabase.getInstance(NiblMerchant.INSTANCE)
            .getTransactionConfigDao().updateReconciliationBatchNumber(batchNumber)
    }

    override fun getExistingSettlementBatchNumber(): Observable<String> {
        return Observable.fromCallable {
            getFormattedSettlementBatchNumber()
        }
    }

    override fun getTipAdjustedTransactionLogOfInvoiceNumber(
        invoiceNumber: String
    ): Observable<TransactionLog> {
        //Not needed in Neps
        return Observable.empty()
    }

    override fun getDeviceConfiguration(): DeviceConfiguration {
        val json = PreferenceManager.getDeviceConfiguration()
        return Jsons.fromJsonToObj(json, DeviceConfiguration::class.java)
    }

    override fun deleteTransactionByStan(stan: String): Observable<Boolean> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
                .deleteByStan(stan)
            true
        }
    }

    override fun getInvoiceNumberByRrn(rrn: String): Observable<String> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
                .getInvoiceNumberByRrn(rrn)
        }
    }

    override fun insertToActivityLog(activityLog: ActivityLog): Observable<Boolean> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getActivityLogDao().insert(activityLog)
            true
        }
    }

    override fun addToDisclaimers(disclaimer: Disclaimer): Observable<Boolean> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getDisclaimerDao().insert(disclaimer)
            true
        }
    }

    override fun addListToDisclaimers(disclaimers: List<Disclaimer>): Observable<Boolean> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getDisclaimerDao()
                .insertAll(disclaimers)
            true
        }
    }

    override fun retrieveAllDisclaimers(): Observable<List<Disclaimer>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getDisclaimerDao().retrieveAll()
        }
    }

    override fun retrieveDisclaimerById(disclaimerId: String): Observable<Disclaimer> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getDisclaimerDao()
                .retrieveById(disclaimerId)
        }
    }

    override fun updateRemarksById(disclaimerId: String, remarks: String): Observable<Boolean> {
        return Observable.fromCallable {
            val count = AppDatabase.getInstance(NiblMerchant.INSTANCE).getDisclaimerDao()
                .updateRemarksById(disclaimerId, remarks)
            count == 1
        }
    }

    override fun deleteById(disclaimerId: String): Observable<Boolean> {
        return Observable.fromCallable {
            val count = AppDatabase.getInstance(NiblMerchant.INSTANCE).getDisclaimerDao()
                .deleteById(disclaimerId)
            count == 1
        }
    }

    private fun getFormattedSettlementBatchNumber(): String {
        val batchNumber = AppDatabase
            .getInstance(NiblMerchant.INSTANCE)
            .getTransactionConfigDao()
            .getReconciliationBatchNumber()
        logger.log("::: NEPS ::: RECONCILIATION BATCH NUMBER === $batchNumber")
        return if (batchNumber < 100) {
            StringUtils.ofRequiredLength(batchNumber, 3)
        } else {
            batchNumber.toString()
        }
    }

    override fun deleteMerchantTransactionLogsOfGivenCountFromInitial(count: Int): Observable<DatabaseReponse> {
        return try {
            Observable.fromCallable {
                AppDatabase.getInstance(NiblMerchant.INSTANCE)
                    .getMerchantTransactionLogDao()
                    .deleteMerchantTransactionLogsOfGivenCountFromInitial(count)
                DatabaseReponse(DataBaseResult.SUCCESS, "Success")
            }
        } catch (ex: Exception) {
            Observable.fromCallable {
                DatabaseReponse(DataBaseResult.FAILURE, ex.message!!)
            }
        }

    }

    override fun nukeAllDatabaseTable(): Observable<DatabaseReponse> {
        return try {
            Observable.fromCallable {
                nukeTablesData(true)
                DatabaseReponse(DataBaseResult.SUCCESS, "Success")
            }
        } catch (ex: Exception) {
            Observable.fromCallable {
                DatabaseReponse(DataBaseResult.FAILURE, ex.message!!)
            }
        }
    }

    override fun getAmountPerTransaction(): Observable<Long> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE).getConfigDao().getAmountPerTransaction()
        }
    }

    override fun getMerchantTransactionLogsOfGivenCount(count: Int): Observable<List<MerchantTransactionLog>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(NiblMerchant.INSTANCE)
                .getMerchantTransactionLogDao().getMerchantTransactionLogsOfGivenCount(count)
        }
    }
}