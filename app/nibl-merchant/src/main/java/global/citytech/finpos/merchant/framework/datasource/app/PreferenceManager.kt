package global.citytech.finpos.merchant.framework.datasource.app

import android.content.ContentValues
import android.content.Context
import global.citytech.easydroid.core.preference.SecurePreference
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.app.pref.SharedPreferenceSource
import global.citytech.finpos.merchant.presentation.model.setting.PushNotificationConfiguration
import global.citytech.finpos.merchant.presentation.model.setting.ReceiptConfiguration
import global.citytech.finpos.merchant.presentation.model.setting.TerminalSettingResponse
import global.citytech.finpos.merchant.presentation.purchase.PaymentItems
import global.citytech.finpos.merchant.presentation.utils.Constants
import global.citytech.finpos.merchant.presentation.utils.Constants.CONNECT_FONEPAY_PROVIDER_CONTENT_URI
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finposframework.utility.JsonUtils
import global.citytech.finposframework.utility.TMSConfigurationConstants.DEFAULT_TIP_LIMIT_CONFIGURATION_IN_PERCENTAGE
import java.io.*

object PreferenceManager : SharedPreferenceSource {

    private lateinit var pref: SecurePreference

    private const val PREF_NAME = "nibl.merchant"
    private const val IS_CONFIGURED = "is_configured"
    private const val IS_DEBUG_MODE = "is_debug_mode"
    private const val IS_SOUND_MODE = "is_sound_mode"
    private const val CARD_MONITORING = "is_card_monitoring"
    private const val FORCE_SETTLEMENT = "is_force_settlement"
    private const val POS_MODE = "pos_mode"
    private const val NUMPAD_TYPE = "numpad_type"
    private const val TIP_LIMIT = "tip_limit"
    private const val SETTLEMENT_TIME = "settlement_time"
    private const val LAST_SUCCESS_SETTLEMENT_TIME = "last_success_settlement_time"
    private const val DEVICE_CONFIGURATION = "device_configuration"
    private const val IS_CARD_CONFIRMATION_REQUIRED = "is_card_confirmation_required"
    private const val IS_AUTO_SETTLEMENT_TRIGGER = "is_auto_settlement_trigger"
    private const val IS_VAT_REFUND_SUPPORTED = "is_vat_refund_supported"
    private const val SHOULD_SHUFFLE_PIN_PAD = "should_shuffle_pin_pad"
    private const val MERCHANT_CREDENTIAL = "merchant_credential"
    private const val IS_SETTLEMENT_ACTIVE = "is_settlement_active"
    private const val TERMINAL_SETTINGS = "terminal_settings"
    private const val PAYMENT_INITIATOR_ITEMS = "payment_initiator_items"
    private const val BANNERS = "banners"
    private const val IS_DISCLAIMER_SHOWN = "is_disclaimer_shown"
    private const val EXTRA_APP_DOWNLOAD = "extra.app.download"
    private const val APP_VERSION = "app_version"
    private const val IS_SWITCH_CONFIG_ACTIVE = "is_switch_config_active"
    private const val IS_BROADCAST_ACKNOWLEDGE_ACTIVE = "is_broadcast_acknowledge_active"
    private const val IS_APP_REST_ACTIVE = "is_app_reset_active"
    private const val IS_FONEPAY_CONNECT_ACTIVE = "is_fonepay_active"
    private const val AMOUNT_LENGTH = "amount_length"
    private const val AID_RESPONSE = "aid_response"
    private var context: Context? = null
    private const val IS_GREEN_PIN_ACTIVE = "is_green_pin_active"

    fun init(context: Context) {
        this.context = context
        pref = SecurePreference(
            context,
            PREF_NAME
        )
    }

    override fun clearAllPreferences() {
        pref.clear()
    }

    override fun saveSettlementActivePref(value: Boolean) {
        pref.saveBoolean(IS_SETTLEMENT_ACTIVE, value)
    }

    override fun getSettlementActivePref(defaultValue: Boolean): Boolean =
        pref.retrieveBoolean(IS_SETTLEMENT_ACTIVE, defaultValue)

    override fun saveTerminalConfigPref(value: Boolean) {
        pref.saveBoolean(IS_CONFIGURED, value)
    }

    override fun getTerminalConfigPref(defaultValue: Boolean): Boolean =
        pref.retrieveBoolean(IS_CONFIGURED, defaultValue)

    override fun saveDebugModePref(value: Boolean) {
        pref.saveBoolean(IS_DEBUG_MODE, value)
        DeviceConfiguration.get().debugMode = value
    }

    override fun saveSoundModePref(value: Boolean) {
        pref.saveBoolean(IS_SOUND_MODE, value)
    }

    override fun getDebugModePref(defaultValue: Boolean): Boolean =
        pref.retrieveBoolean(IS_DEBUG_MODE, defaultValue)

    override fun getSoundModePref(defaultValue: Boolean): Boolean =
        pref.retrieveBoolean(IS_SOUND_MODE, defaultValue)

    override fun saveCardConfirmationRequiredPref(value: Boolean) {
        pref.saveBoolean(IS_CARD_CONFIRMATION_REQUIRED, value)
    }

    override fun getCardConfirmationRequiredPref(defaultValue: Boolean): Boolean =
        pref.retrieveBoolean(IS_CARD_CONFIRMATION_REQUIRED, defaultValue)

    override fun saveVatRefundSupportedPref(value: Boolean) {
        pref.saveBoolean(IS_VAT_REFUND_SUPPORTED, value)
    }

    override fun getVatRefundSupportedPref(defaultValue: Boolean): Boolean =
        pref.retrieveBoolean(IS_VAT_REFUND_SUPPORTED, defaultValue)

    override fun saveShouldShufflePinPadPref(value: Boolean) {
        pref.saveBoolean(SHOULD_SHUFFLE_PIN_PAD, value)
    }

    override fun getShouldShufflePinPadPref(defaultValue: Boolean): Boolean =
        pref.retrieveBoolean(SHOULD_SHUFFLE_PIN_PAD, defaultValue)

    override fun setCardMonitoringPreference(cardMonitoring: Boolean) {
        pref.saveBoolean(CARD_MONITORING, cardMonitoring)
    }

    override fun getCardMonitoringPreference(defaultValue: Boolean): Boolean =
        pref.retrieveBoolean(CARD_MONITORING, defaultValue)

    override fun setForceSettlementPreference(forceSettlement: Boolean) {
        pref.saveBoolean(FORCE_SETTLEMENT, forceSettlement)
    }

    override fun getForceSettlementPreference(defaultValue: Boolean): Boolean =
        pref.retrieveBoolean(FORCE_SETTLEMENT, defaultValue)

    override fun savePosMode(posMode: String) {
        pref.saveData(POS_MODE, posMode)
    }

    override fun getPosMode(defaultPosMode: String): String {
        return pref.retrieveData(POS_MODE, defaultPosMode)
    }

    override fun saveTipLimit(value: Int) {
        pref.saveInt(TIP_LIMIT, value)
    }

    override fun saveNumpadLayoutType(numpadLayoutType: String) {
        pref.saveData(NUMPAD_TYPE, numpadLayoutType)
    }

    override fun getNumpadLayoutType(defaultValue: String): String {
        return pref.retrieveData(NUMPAD_TYPE, defaultValue)
    }

    override fun retrieveTipLimit(): Int {
        return pref.retrieveInt(TIP_LIMIT, DEFAULT_TIP_LIMIT_CONFIGURATION_IN_PERCENTAGE)
    }

    override fun getSettlementTime(defaultValue: String): String {
        return pref.retrieveData(SETTLEMENT_TIME, defaultValue)
    }

    override fun storeSettlementTime(settlementTime: String) =
        pref.saveData(SETTLEMENT_TIME, settlementTime)

    override fun getLastSuccessSettlementTimeInMillis(defaultValue: Long): Long {
        return pref.retrieveLong(LAST_SUCCESS_SETTLEMENT_TIME, defaultValue)
    }

    override fun updateLastSuccessSettlementTimeInMillis(settledTime: Long) {
        pref.saveLong(LAST_SUCCESS_SETTLEMENT_TIME, settledTime)
    }

    fun saveDeviceConfiguration(deviceConfigurationJson: String) {
        pref.saveData(DEVICE_CONFIGURATION, deviceConfigurationJson)
    }

    fun getDeviceConfiguration(): String {
        return pref.retrieveData(DEVICE_CONFIGURATION, "{}")
    }

    override fun saveMerchantCredential(merchantCredential: String) {
        pref.saveData(MERCHANT_CREDENTIAL, merchantCredential)
    }

    override fun getMerchantCredential(defaultValue: String): String {
        return pref.retrieveData(MERCHANT_CREDENTIAL, defaultValue)

    }

    override fun saveBanners(banners: String) {
        pref.saveData(BANNERS, banners)
    }

    override fun getBanners(defaultValue: String): String {
        return pref.retrieveData(BANNERS, defaultValue)

    }

    override fun saveTerminalSetting(terminalSettingResponseJson: String) {
        pref.saveData(TERMINAL_SETTINGS, terminalSettingResponseJson)
    }

    override fun savePaymentInitiatiorItems(paymentListItems: String) {
        pref.saveData(PAYMENT_INITIATOR_ITEMS, paymentListItems)
    }

    override fun updateShowQrDisclaimerSetting(showDisclaimer: Boolean) {
        val terminalSettingResponse =
            JsonUtils.fromJsonToObj(getTerminalSetting(), TerminalSettingResponse::class.java)
        terminalSettingResponse.showQrDisclaimer = showDisclaimer
        saveTerminalSetting(JsonUtils.toJsonObj(terminalSettingResponse))
    }

    override fun getShowQrDisclaimerSetting(): Boolean {
        val terminalSettingResponse =
            JsonUtils.fromJsonToObj(getTerminalSetting(), TerminalSettingResponse::class.java)
        return terminalSettingResponse.showQrDisclaimer
    }

    override fun getTerminalSetting(): String {
        val terminalSettingResponse = TerminalSettingResponse(
            enableActivityLog = true,
            enableDebugLog = false,
            baseUrl = "",
            qrConfigs = ArrayList(),
            enableEmiScheme = false,
            disclaimers = emptyList(),
            receiptConfiguration = ReceiptConfiguration(true, true),
            pushNotificationConfiguration = PushNotificationConfiguration(),
            enableAutoSettlement = false,
            enableNfcTapAndPay = false,
            enableGreenPin = false,
            isSoundEnabled = false
        )
        return pref.retrieveData(TERMINAL_SETTINGS, Jsons.toJsonObj(terminalSettingResponse))
    }

    override fun getPaymentInitiatorItems(): String {
        return pref.retrieveData(
            PAYMENT_INITIATOR_ITEMS,
            Jsons.toJsonObj(mutableListOf<PaymentItems>())
        )
    }

    override fun isDisclaimerShown(): Boolean {
        return pref.retrieveBoolean(IS_DISCLAIMER_SHOWN, false)
    }

    override fun saveDisclaimerShownStatusPref(value: Boolean) {
        pref.saveBoolean(IS_DISCLAIMER_SHOWN, value)
    }

    override fun saveAppDownloadExtra(value: String) {
        pref.saveData(EXTRA_APP_DOWNLOAD, value)
    }

    override fun getAppDownloadExtra(): String {
        return pref.retrieveData(EXTRA_APP_DOWNLOAD, "")
    }

    override fun getStoredAppVersion(): Int {
        return pref.retrieveInt(APP_VERSION, 0)
    }

    override fun saveAppVersion(version: Int) {
        pref.saveInt(APP_VERSION, version)
    }

    override fun isSwitchConfigActive(): Boolean {
        return pref.retrieveBoolean(IS_SWITCH_CONFIG_ACTIVE, false)
    }

    override fun setSwitchConfigActive(active: Boolean) {
        pref.saveBoolean(IS_SWITCH_CONFIG_ACTIVE, active)
    }

    override fun isBroadcastAcknowledgeActive(): Boolean {
        return pref.retrieveBoolean(IS_BROADCAST_ACKNOWLEDGE_ACTIVE, false)
    }

    override fun setBroadcastAcknowledgeActive(active: Boolean) {
        pref.saveBoolean(IS_BROADCAST_ACKNOWLEDGE_ACTIVE, active)
    }

    override fun isAppResetActive(defaultValue: Boolean): Boolean {
        return pref.retrieveBoolean(IS_APP_REST_ACTIVE, defaultValue)
    }

    override fun setAppResetActive(value: Boolean) {
        pref.saveBoolean(IS_APP_REST_ACTIVE, value)
    }

    override fun setAmountLengthLimit(value: Int) {
        pref.saveInt(AMOUNT_LENGTH, value)
    }

    override fun getAmountLengthLimit(): Int = pref.retrieveInt(AMOUNT_LENGTH, 8)

    override fun getAidResponse(defaultValue: String): String =
        pref.retrieveData(AID_RESPONSE, defaultValue)

    override fun saveAidResponse(value: String) {
        pref.saveData(AID_RESPONSE, value)
    }

    override fun getFonePayLoginState(defaultValue: Boolean): Boolean {
        return try {
            /* val file = File(
                 this.context?.packageManager?.getPackageInfo(
                     Constants.CONNECT_FONEPAY_PACKAGE,
                     0
                 )?.applicationInfo?.dataDir
                         + "/app_flutter/message.txt"
             )
             readFromFileForConnectFonePay(file)*/
            readFromFileForConnectFonePay()
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    override fun saveFonePayLoginState(value: Boolean) {
        if (AppUtility.isAppInstalled(Constants.CONNECT_FONEPAY_PACKAGE, context!!)) {
            writeIntoFileForConnectFonePay(
                File(
                    this.context?.packageManager?.getPackageInfo(
                        Constants.CONNECT_FONEPAY_PACKAGE,
                        0
                    )?.applicationInfo?.dataDir
                            + Constants.CONNECT_FONEPAY_PROVIDER_FILE_PATH
                ),
                value
            )
        }
    }

    private fun writeIntoFileForConnectFonePay(file: File, value: Boolean) {
        val contentResolver = NiblMerchant.INSTANCE.contentResolver
        val values = ContentValues()
        values.put("status", value)
        contentResolver.insert(CONNECT_FONEPAY_PROVIDER_CONTENT_URI, values)
    }

    private fun readFromFileForConnectFonePay(): Boolean {
        val contentResolver = NiblMerchant.INSTANCE.contentResolver

        try {
            contentResolver.openInputStream(CONNECT_FONEPAY_PROVIDER_CONTENT_URI)
                .use { inputStream ->
                    BufferedReader(InputStreamReader(inputStream)).use { reader ->
                        val contentBuilder = StringBuilder()
                        var line: String?
                        while (reader.readLine().also { line = it } != null) {
                            contentBuilder.append(line)
                            contentBuilder.append("\n")
                        }
                        val fileContent = contentBuilder.toString()
                        return fileContent.trim().toBoolean()
                    }
                }
        } catch (e: IOException) {
            e.printStackTrace()
            return false
        }
    }


    override fun saveGreenPinStatus(value: Boolean) = pref.saveBoolean(IS_GREEN_PIN_ACTIVE, value)
    override fun getGreenPinStatus(): Boolean =
        pref.retrieveBoolean(IS_GREEN_PIN_ACTIVE, false)

}