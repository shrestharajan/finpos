package global.citytech.neps.datasource.core.transaction

import android.app.Application
import global.citytech.common.data.LabelValue
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.neps.data.db.AppDatabase
import global.citytech.neps.domain.model.AidParam
import global.citytech.neps.domain.model.CardScheme
import global.citytech.neps.domain.model.EmvKey
import global.citytech.neps.domain.model.EmvParam
import global.citytech.posswitchintregator.card.data.*
import global.citytech.posswitchintregator.card.data.FieldAttributes.*
import global.citytech.posswitchintregator.repo.TransactionRepository
import global.citytech.posswitchintregator.utils.PosError
import global.citytech.posswitchintregator.utils.PosException

class TransactionRepositoryImpl(val instance: Application):TransactionRepository {
    override fun retrieveAid(p0: AidRetrieveRequest?): AidRetrieveResponse {
        if (p0 != null) {
            val aidParamList = AppDatabase
                .getInstance(instance.applicationContext)
                .getAidParamDao()
                .getAidParamList()
            return iterateAidParamInList(p0, aidParamList)
        } else {
            throw PosException(PosError.DEVICE_ERROR_NO_SUCH_REQUEST)
        }
    }

    override fun retrieveEmvParameterRequest(): EmvParametersRequest {
        val emvParamList = AppDatabase
            .getInstance(instance.applicationContext)
            .getEmvParamDao()
            .getEmvParams()
        return prepareEmvParameterRequest(emvParamList)
    }

    override fun retrieveEmvKey(p0: EmvKeyRetrieveRequest?): EmvKeyRetrieveResponse {
        if (p0 != null) {
            val emvKeyList = AppDatabase
                .getInstance(instance.applicationContext)
                .getEmvKeyDao()
                .getEmvKeys()
            return iterateEmvKeysInList(p0, emvKeyList)
        } else {
            throw PosException(PosError.DEVICE_ERROR_NO_SUCH_REQUEST)
        }
    }

    override fun retrieveCardScheme(p0: CardSchemeRetrieveRequest?): CardSchemeRetrieveResponse {
        if (p0 != null) {
            val attribute = getRespectiveAttributeForCardScheme(p0)
            return prepareCardSchemeRetrieveResponse(p0, attribute)
        } else {
            throw PosException(PosError.DEVICE_ERROR_NO_SUCH_REQUEST)
        }
    }

    private fun iterateAidParamInList(
        request: AidRetrieveRequest,
        aidParams: List<AidParam>
    ): AidRetrieveResponse {
        var response = AidRetrieveResponse("")
        for (aidParam in aidParams) {
            val aidValue = getValueFromLabelValue(aidParam.aid)
            if (aidValue.equals(request.aid)) {
                val data = getRespectiveFieldFromAid(request, aidParam)
                response = AidRetrieveResponse(data)
                break
            }
        }
        return response
    }

    private fun getRespectiveFieldFromAid(
        request: AidRetrieveRequest,
        aidParam: AidParam
    ) : String {
        val result : String
        when(request.fieldAttributes) {
            AID_DATA_S1_F4_AID -> result = aidParam.aid

            DEVICE_SPECIFIC_S1_F4_4_TERMINAL_CONTACTLESS_FLOOR_LIMIT,
            DEVICE_SPECIFIC_S1_F4_8_TERMINAL_CONTACTLESS_FLOOR_LIMIT,
            DEVICE_SPECIFIC_S1_F4_12_TERMINAL_CONTACTLESS_FLOOR_LIMIT -> {
                result = getValueFromLabelValue(aidParam.contactlessFloorLimit)
            }

            DEVICE_SPECIFIC_S1_F4_2_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT,
            DEVICE_SPECIFIC_S1_F4_6_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT,
            DEVICE_SPECIFIC_S1_F4_10_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT ->  {
                result = getValueFromLabelValue(aidParam.contactlessTransactionLimit)
            }

            DEVICE_SPECIFIC_S1_F4_3_TERMINAL_CVM_REQUIRED_LIMIT,
            DEVICE_SPECIFIC_S1_F4_7_TERMINAL_CVM_REQUIRED_LIMIT,
            DEVICE_SPECIFIC_S1_F4_11_TERMINAL_CVM_REQUIRED_LIMIT -> {
                result = getValueFromLabelValue(aidParam.cvmLimit)
            }

            AID_DATA_S1_F24_DEFAULT_ACTION_CODE -> {
                result = getValueFromLabelValue(aidParam.defaultActionCode)
            }

            AID_DATA_S1_F16_DEFAULT_DDOL -> {
                result = getValueFromLabelValue(aidParam.defaultDDOL)
            }

            AID_DATA_S1_F14_DEFAULT_TDOL -> {
                result = getValueFromLabelValue(aidParam.defaultTDOL)
            }

            AID_DATA_S1_F20_DENIAL_ACTION_CODE -> {
                result = getValueFromLabelValue(aidParam.denialActionCode)
            }

            AID_DATA_S1_F6_AID_LABEL -> {
                result = getValueFromLabelValue(aidParam.label)
            }

            AID_DATA_S1_F22_ONLINE_ACTION_CODE -> {
                result = getValueFromLabelValue(aidParam.onlineActionCode)
            }

            AID_DATA_S1_F8_TERMINAL_AID_VERSION_NUMBERS -> {
                result = getValueFromLabelValue(aidParam.terminalAidVersion)
            }

            CARD_SCHEME_S2_F16_TERMINAL_FLOOR_LIMIT -> {
                result = getValueFromLabelValue(aidParam.terminalFloorLimit)
            }

            else -> result = ""
        }
        return result
    }

    private fun prepareEmvParameterRequest(
        emvParamList: List<EmvParam>
    ) : EmvParametersRequest {
        val emvParametersRequest = EmvParametersRequest()
        emvParametersRequest.terminalCountryCode =
            getValueFromLabelValue(emvParamList[0].terminalCountryCode)
        emvParametersRequest.transactionCurrencyCode =
            getValueFromLabelValue(emvParamList[0].transactionCurrencyCode)
        emvParametersRequest.transactionCurrencyExponent =
            getValueFromLabelValue(emvParamList[0].transactionCurrencyExponent)
        emvParametersRequest.terminalCapabilities =
            getValueFromLabelValue(emvParamList[0].terminalCapabilities)
        emvParametersRequest.terminalType =
            getValueFromLabelValue(emvParamList[0].terminalType)
        emvParametersRequest.merchantCategoryCode =
            getValueFromLabelValue(emvParamList[0].merchantCategoryCode)
        emvParametersRequest.terminalId =
            getValueFromLabelValue(emvParamList[0].terminalId)
        emvParametersRequest.merchantIdentifier =
            getValueFromLabelValue(emvParamList[0].merchantIdentifier)
        emvParametersRequest.merchantName = getValueFromLabelValue(emvParamList[0].merchantName)
        emvParametersRequest.ttq = getValueFromLabelValue(emvParamList[0].ttq)
        emvParametersRequest.additionalTerminalCapabilites =
            getValueFromLabelValue(emvParamList[0].additionalTerminalCapabilities)
        emvParametersRequest.forceOnlineFlag =
            getValueFromLabelValue(emvParamList[0].forceOnlineFlag)
        return emvParametersRequest
    }

    private fun iterateEmvKeysInList(
        request: EmvKeyRetrieveRequest,
        emvKeys: List<EmvKey>
    ): EmvKeyRetrieveResponse {
        var response = EmvKeyRetrieveResponse("")
        for (emvKey in emvKeys) {
            val ridValue = getValueFromLabelValue(emvKey.rid)
            val indexValue = getValueFromLabelValue(emvKey.index)
            if (ridValue.equals(request.rid) && indexValue.equals(request.index)) {
                val data = getRespectiveFieldFromEmvKey(request, emvKey)
                response = EmvKeyRetrieveResponse(data)
                break
            }
        }
        return response
    }

    private fun getRespectiveFieldFromEmvKey(
        request: EmvKeyRetrieveRequest,
        emvKey: EmvKey
    ): String {
        val result: String
        when(request.field) {
            EMV_PUBLIC_KEY_S1_F16_CHECK_SUM -> result = getValueFromLabelValue(emvKey.checkSum)

            EMV_PUBLIC_KEY_S1_F20_CA_PUBLIC_KEY_EXPIRY_DATE -> {
                result = getValueFromLabelValue(emvKey.expiryDate)
            }

            EMV_PUBLIC_KEY_S1_F14_EXPONENT -> result = getValueFromLabelValue(emvKey.exponent)

            EMV_PUBLIC_KEY_S1_F8_HASH_ID -> result = getValueFromLabelValue(emvKey.hashId)

            EMV_PUBLIC_KEY_S1_F6_KEY_INDEX -> result = getValueFromLabelValue(emvKey.index)

            EMV_PUBLIC_KEY_S1_F10_DIGITAL_SIG_ID -> {
                result = getValueFromLabelValue(emvKey.keySignatureId)
            }

            EMV_PUBLIC_KEY_S1_F18_CA_PUBLIC_KEY_LENGTH -> {
                result = getValueFromLabelValue(emvKey.length)
            }

            EMV_PUBLIC_KEY_S1_F12_PUBLIC_KEY -> result = getValueFromLabelValue(emvKey.modules)

            EMV_PUBLIC_KEY_S1_F4_RID -> result = getValueFromLabelValue(emvKey.rid)

            else -> result = ""
        }
        return result
    }

    private fun getRespectiveAttributeForCardScheme(
        p0: CardSchemeRetrieveRequest
    ): String {
        val attribute: String
        when (p0.fieldAttributes) {
            CARD_SCHEME_S1_F4_CARD_SCHEME_ID,
            CARD_SCHEME_S2_F4_CARD_SCHEME_ID,
            CARD_SCHEME_S3_F4_CARD_SCHEME_ID -> {
                attribute = "cardSchemeId"
            }

            CARD_SCHEME_S1_F6_CARD_SCHEME_NAME_ARABIC,
            CARD_SCHEME_S1_F8_CARD_SCHEME_NAME_ENGLISH -> {
                attribute = "cardSchemeName"
            }

            CARD_SCHEME_S1_F10_CARD_SCHEME_ACQUIRER_ID -> {
                attribute = "cardSchemeAcquirerId"
            }

            CARD_SCHEME_S1_F20_CHECK_SERVICE_CODE -> {
                attribute = "checkServiceCode"
            }

            CARD_SCHEME_S2_F6_TRANSACTIONS_ALLOWED -> {
                attribute = "transactionAllowed"
            }

            CARD_SCHEME_S2_F8_CARDHOLDER_AUTHENTICATION -> {
                attribute = "cardHolderAuthentication"
            }

            CARD_SCHEME_S2_F10_SUPERVISOR_FUNCTIONS -> {
                attribute = "supervisorFunction"
            }

            CARD_SCHEME_S2_F12_MANUAL_ENTRY_ALLOWED -> {
                attribute = "manualEntryAllowed"
            }

            CARD_SCHEME_S2_F20_MAXM_CASH_BACK -> {
                attribute = "maximumCashBack"
            }

            CARD_SCHEME_S2_F26_LUHN_CHECK -> {
                attribute = "luhnCheck"
            }

            CARD_SCHEME_S3_F6_CARD_RANGES -> {
                attribute = "cardRanges"
            }

            else -> attribute = ""
        }
        return attribute
    }

    private fun prepareCardSchemeRetrieveResponse(
        request: CardSchemeRetrieveRequest,
        attribute: String
    ):CardSchemeRetrieveResponse{
        var response = CardSchemeRetrieveResponse("")
        if (attribute.isNotEmpty()) {
            val data = getValueFieldFromCardSchemeTable(request, attribute)
            response = CardSchemeRetrieveResponse(data)
        }
        return response
    }

    private fun getValueFieldFromCardSchemeTable(
        request: CardSchemeRetrieveRequest,
        attribute: String
    ): String {
        var data = AppDatabase
            .getInstance(instance.applicationContext)
            .getCardSchemeDao()
            .getByCardSchemeTypeAndAttribute(
                request.cardSchemeType,
                attribute
            )
        if (data == null) {
            data = ""
        }
        return data!!
    }

    private fun getValueFromLabelValue(
        item: String?
    ): String {
        var value = ""
        if (item != null) {
            val labelValue = Jsons.fromJsonToObj(item, LabelValue::class.java)
            if (labelValue?.value != null) {
                value = labelValue.value.toString()
            }
        }
        return value
    }
}