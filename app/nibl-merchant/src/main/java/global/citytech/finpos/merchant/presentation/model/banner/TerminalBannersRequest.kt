package global.citytech.finpos.merchant.presentation.model.banner

/**
 * Created by Rishav Chudal on 11/3/21.
 */
data class TerminalBannersRequest(val serialNumber: String)
