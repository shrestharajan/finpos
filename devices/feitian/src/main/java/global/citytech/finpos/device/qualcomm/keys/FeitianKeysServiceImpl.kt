package global.citytech.finpos.device.qualcomm.keys

import android.content.Context
import com.ftpos.library.smartpos.datautils.BytesTypeValue
import com.ftpos.library.smartpos.errcode.ErrCode
import com.ftpos.library.smartpos.keymanager.KeyManager
import global.citytech.common.extensions.isNullOrEmptyOrBlank
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.KEY_INDEX_RANGE_MAX
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.KEY_INDEX_RANGE_MIN
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finpos.device.qualcomm.utils.FeitianUtils
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.KEY_INJECTION_KEY_INDEX_INVALID
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.KEY_INJECTION_PROTECT_KEY_INDEX_INVALID
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_FAILURE_EMV_KEYS_ERASE
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_FAILURE_INVALID_KEY
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_FAILURE_INVALID_PACKAGE_NAME
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_FAILURE_KEY_CHECK
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_FAILURE_KEY_INJECT
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_KEY_NOT_SUPPORT
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_SUCCESS_KEY_CHECK
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_SUCCESS_KEY_ERASE
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_SUCCESS_KEY_INJECT
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.common.Result.FAILURE
import global.citytech.finposframework.hardware.common.Result.SUCCESS
import global.citytech.finposframework.hardware.keymgmt.keys.KeyService
import global.citytech.finposframework.hardware.keymgmt.keys.KeyType.*
import global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyCheckRequest
import global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyInjectionRequest
import global.citytech.finposframework.utility.StringUtils

/**
 * Created by Rishav Chudal on 4/6/21.
 */
class FeitianKeysServiceImpl constructor(context: Context) : KeyService, FeitianLogger.Listener {

    private val keyManager = KeyManager.getInstance(context.applicationContext)
    private val logger = FeitianLogger(this)
    private lateinit var deviceResponse: DeviceResponse

    override fun injectKey(keyInjectionRequest: KeyInjectionRequest): DeviceResponse {
        this.deviceResponse = DeviceResponse(FAILURE, MSG_FAILURE_KEY_INJECT)
        this.printLogsOfKeyInjectionRequestItems(keyInjectionRequest)
        this.validateAndProceedKeyInjection(keyInjectionRequest)
        return this.deviceResponse
    }

    override fun eraseAllKeys(): DeviceResponse {
        var deviceResponse = DeviceResponse(FAILURE, MSG_FAILURE_EMV_KEYS_ERASE)
        this.logger.logWithClassName("***** Erasing all existing keys *****")
        val resultEraseAllKeys = keyManager.deleteAllKey()
        this.logger.logWithClassName("Erase all keys result ::: ".plus(resultEraseAllKeys))
        if (resultEraseAllKeys == ErrCode.ERR_SUCCESS) {
            this.logger.logWithClassName("Erase all keys success...")
            deviceResponse = DeviceResponse(SUCCESS, MSG_SUCCESS_KEY_ERASE)
        } else {
            this.logger.logWithClassName("Erase all keys failure...")
        }
        return deviceResponse
    }

    override fun checkKeyExist(keyCheckRequest: KeyCheckRequest): DeviceResponse {
        var deviceResponse = DeviceResponse(FAILURE, MSG_FAILURE_KEY_CHECK)
        if (!validKeyIndex(keyCheckRequest.keyIndex)) {
            this.logger.logWithClassName("checkKeyExist ::: Invalid Key Index...")
            deviceResponse = DeviceResponse(FAILURE, KEY_INJECTION_KEY_INDEX_INVALID)
        } else {
            this.logger.logWithClassName("***** Checking for key exist *****")
            val resultKeyStatus = keyManager.getKeyStatus(
                FeitianUtils.getDeviceKeyType(keyCheckRequest.keyType),
                keyCheckRequest.keyIndex!!
            )
            this.logger.logWithClassName("KeyCheckStatus result ::: ".plus(resultKeyStatus))
            if (resultKeyStatus == ErrCode.ERR_SUCCESS) {
                deviceResponse = DeviceResponse(SUCCESS, MSG_SUCCESS_KEY_CHECK)
            }
        }
        return deviceResponse
    }

    private fun validateAndProceedKeyInjection(keyInjectionRequest: KeyInjectionRequest) {
        if (keyInjectionRequest.key.isNullOrEmptyOrBlank()) {
            this.prepareKeyInjectionResponse(FAILURE, MSG_FAILURE_INVALID_KEY)
            return
        }

        if (keyInjectionRequest.packageName.isNullOrEmptyOrBlank()) {
            this.prepareKeyInjectionResponse(FAILURE, MSG_FAILURE_INVALID_PACKAGE_NAME)
            return
        }

        if (keyInjectionRequest.keyIndex == null || !validKeyIndex(keyInjectionRequest.keyIndex!!)) {
            this.prepareKeyInjectionResponse(FAILURE, KEY_INJECTION_KEY_INDEX_INVALID)
            return
        }

        proceedOnKeyTypeBasis(keyInjectionRequest)
    }

    private fun printLogsOfKeyInjectionRequestItems(keyInjectionRequest: KeyInjectionRequest) {
        this.logger.logWithClassName("Key Type ::: " + keyInjectionRequest.keyType)
        this.logger.logWithClassName("Key Algorithm ::: " + keyInjectionRequest.algorithm)
        this.logger.logWithClassName("Key ::: " + keyInjectionRequest.key)
        this.logger.logWithClassName("Key Check Value ::: " + keyInjectionRequest.keyKCV)
        this.logger.logWithClassName("Key Index ::: " + keyInjectionRequest.keyIndex)
        this.logger.logWithClassName("Protect Key Type ::: " + keyInjectionRequest.protectKeyType)
        this.logger.logWithClassName("Protect Key Index ::: " + keyInjectionRequest.protectKeyIndex)
        this.logger.logWithClassName("Key Is Plain Text ? ::: " + keyInjectionRequest.isPlainText)
        this.logger.logWithClassName("Package Name ::: " + keyInjectionRequest.packageName)
        this.logger.logWithClassName("KSN Data ::: " + keyInjectionRequest.ksn)
    }

    private fun validKeyIndex(keyIndex: Int?): Boolean {
        var valid = false
        if (keyIndex != null) {
            valid = (keyIndex in KEY_INDEX_RANGE_MIN..KEY_INDEX_RANGE_MAX)
        }
        return valid
    }

    private fun proceedOnKeyTypeBasis(keyInjectionRequest: KeyInjectionRequest) {
        when (keyInjectionRequest.keyType) {
            KEY_TYPE_DEK -> this.onKeyTypeDEK(keyInjectionRequest)
            KEY_TYPE_FIXED_KEY -> this.onKeyTypeFixedKey(keyInjectionRequest)
            KEY_TYPE_IPEK -> this.onKeyTypeIPEK(keyInjectionRequest)
            KEY_TYPE_KBPK -> this.onKeyTypeKBPK(keyInjectionRequest)
            KEY_TYPE_MEK -> this.onKeyTypeMEK(keyInjectionRequest)
            KEY_TYPE_TMK -> this.onKeyTypeMK(keyInjectionRequest)
            KEY_TYPE_PEK -> this.onKeyTypePEK(keyInjectionRequest)
            KEY_TYPE_RSA -> this.onKeyTypeRSA(keyInjectionRequest)
            else -> prepareKeyInjectionResponse(FAILURE, MSG_KEY_NOT_SUPPORT)
        }
    }

    private fun onKeyTypeDEK(keyInjectionRequest: KeyInjectionRequest) {
        this.logger.logWithClassName("*** Injecting Key Type DEK ***")
        this.logger.logWithClassName("Key Type is Session Key, used to encrypt data...")
        injectSessionKey(keyInjectionRequest)
    }

    private fun onKeyTypeFixedKey(keyInjectionRequest: KeyInjectionRequest) {
        this.logger.logWithClassName("*** Injecting Key Type FIXEDKEY ***")
        this.logger.logWithClassName("Key Type is FIXED Key...")
        injectNonSessionKey(keyInjectionRequest)
    }

    private fun onKeyTypeIPEK(keyInjectionRequest: KeyInjectionRequest) {
        this.logger.logWithClassName("*** Injecting Key Type IPEK ***")
        injectIpekKey(keyInjectionRequest)
    }

    private fun onKeyTypeKBPK(keyInjectionRequest: KeyInjectionRequest) {
        this.logger.logWithClassName("*** Injecting Key Type KBPK ***")
        this.logger.logWithClassName("Key Type is used to protect the TR31 data block...")
        injectKeyTypeKBPK(keyInjectionRequest)
    }

    private fun onKeyTypeMEK(keyInjectionRequest: KeyInjectionRequest) {
        this.logger.logWithClassName("*** Injecting Key Type MEK ***")
        this.logger.logWithClassName("Key Type is Session Key, use to calculate MAC(Response mode)...")
        injectSessionKey(keyInjectionRequest)
    }

    private fun onKeyTypeMK(keyInjectionRequest: KeyInjectionRequest) {
        this.logger.logWithClassName("*** Injecting Key Type MK(Master Key) ***")
        injectNonSessionKey(keyInjectionRequest)
    }

    private fun onKeyTypePEK(keyInjectionRequest: KeyInjectionRequest) {
        this.logger.logWithClassName("*** Injecting Key Type PEK ***")
        this.logger.logWithClassName("Key Type is Session Key, used to encrypt PIN...")
        injectSessionKey(keyInjectionRequest)
    }

    private fun onKeyTypeRSA(keyInjectionRequest: KeyInjectionRequest) {
        this.logger.logWithClassName("*** Injecting Key Type RSA ***")
        injectKeyTypeRSA(keyInjectionRequest)
    }

    private fun injectSessionKey(keyInjectionRequest: KeyInjectionRequest) {
        val keyIndex: Int = keyInjectionRequest.keyIndex!!
        val protectKeyIndex: Int? = keyInjectionRequest.protectKeyIndex
        if (validKeyIndex(protectKeyIndex) && keyIndex != protectKeyIndex) {
            injectSymmetricKey(
                keyInjectionRequest,
                FeitianUtils.getDeviceKeyType(keyInjectionRequest.protectKeyType),
                protectKeyIndex!!
            )
        } else {
            this.logger.logWithClassName("Protect Key (or Master Key) Index is Invalid...")
            prepareKeyInjectionResponse(
                FAILURE,
                KEY_INJECTION_PROTECT_KEY_INDEX_INVALID
            )
        }
    }

    private fun injectNonSessionKey(keyInjectionRequest: KeyInjectionRequest) {
        val protectKeyType = 0
        val protectKeyIndex = 0
        injectSymmetricKey(
            keyInjectionRequest,
            protectKeyType,
            protectKeyIndex
        )
    }

    /**
     * Symmetric Key Injection. For KeyType DEK, MEK, MK, PEK
     */
    private fun injectSymmetricKey(
        keyInjectionRequest: KeyInjectionRequest,
        protectKeyType: Int,
        protectKeyIndex: Int
    ) {
        try {
            setKeyGroupName(keyInjectionRequest.packageName)
            val keyDataInBytes: ByteArray = StringUtils.hexString2bytes(keyInjectionRequest.key)
            val bytesTypeValue = getBytesTypeValue(keyInjectionRequest.keyKCV)
            val result = keyManager.loadSymKey(
                FeitianUtils.getDeviceKeyType(keyInjectionRequest.keyType),
                keyInjectionRequest.keyIndex!!,
                FeitianUtils.getDeviceAlgorithmMode(keyInjectionRequest.algorithm),
                protectKeyType,
                protectKeyIndex,
                keyDataInBytes,
                keyDataInBytes.size,
                bytesTypeValue
            )
            verifyInjectionResult(result)
        } catch (ex: java.lang.Exception) {
            this.logger.logWithClassName("Exception in Key Injection...")
            ex.printStackTrace()
        }
    }

    /**
     * DUKPT Injection. For KeyType IPEK
     */
    private fun injectIpekKey(keyInjectionRequest: KeyInjectionRequest) {
        try {
            setKeyGroupName(keyInjectionRequest.packageName)
            val protectKeyType = 0
            val protectKeyIndex = 0
            val ipekData: ByteArray = StringUtils.hexString2bytes(keyInjectionRequest.key)
            val iKsnData: ByteArray = StringUtils.hexString2bytes(keyInjectionRequest.ksn)
            val bytesTypeValue = getBytesTypeValue(keyInjectionRequest.keyKCV)
            val result = keyManager.loadDukptIpek(
                FeitianUtils.getDeviceKeyType(keyInjectionRequest.keyType),
                keyInjectionRequest.keyIndex!!,
                FeitianUtils.getDeviceAlgorithmMode(keyInjectionRequest.algorithm),
                protectKeyType,
                protectKeyIndex,
                ipekData,
                iKsnData,
                bytesTypeValue
            )
            verifyInjectionResult(result)
        } catch (ex: java.lang.Exception) {
            this.logger.logWithClassName("Exception in IPEK Injection...")
            ex.printStackTrace()
        }
    }

    /**
     *
     * TR31key Injection. For Key Type KBPK
     */
    private fun injectKeyTypeKBPK(keyInjectionRequest: KeyInjectionRequest) {
        try {
            setKeyGroupName(keyInjectionRequest.packageName)
            val bytesTypeValue = getBytesTypeValue(keyInjectionRequest.keyKCV)
            val keyBlockData: ByteArray = StringUtils.hexString2bytes(keyInjectionRequest.key)
            val result = keyManager.loadTr31Key(
                keyBlockData,
                keyBlockData.size,
                bytesTypeValue
            )
            verifyInjectionResult(result)
        } catch (ex: java.lang.Exception) {
            this.logger.logWithClassName("Exception in KBPK Injection...")
            ex.printStackTrace()
        }
    }

    /**
     * RSA Public Key Injection. For Key Type RSA
     */
    private fun injectKeyTypeRSA(keyInjectionRequest: KeyInjectionRequest) {
        try {
            setKeyGroupName(keyInjectionRequest.packageName)
            val modulus: ByteArray = StringUtils.hexString2bytes(keyInjectionRequest.key)
            val result = keyManager.loadRsaPubKey(
                FeitianUtils.getDeviceKeyType(keyInjectionRequest.keyType),
                keyInjectionRequest.keyIndex!!,
                modulus.size,
                modulus,
                keyInjectionRequest.publicExponentRSA
            )
            verifyInjectionResult(result)
        } catch (ex: java.lang.Exception) {
            this.logger.logWithClassName("Exception in RSA Public Key Injection...")
            ex.printStackTrace()
        }
    }

    private fun setKeyGroupName(keyGroupName: String) {
        Thread.sleep(1000) //TODO Remove
        this.logger.logWithClassName("Key Group Name ::: ".plus(keyGroupName))
        val result = keyManager.setKeyGroupName(keyGroupName)
        this.logger.logWithClassName("Setting Key Group Name Result ::: ".plus(result))
    }

    private fun verifyInjectionResult(result: Int) {
        this.logger.logWithClassName("Key Injection Result ::: ".plus(result))
        if (result == ErrCode.ERR_SUCCESS) {
            prepareKeyInjectionResponse(
                SUCCESS,
                MSG_SUCCESS_KEY_INJECT
            )
        }
    }

    private fun prepareKeyInjectionResponse(result: Result, message: String) {
        this.logger.logWithClassName("Key Injection Response Result ::: ".plus(result))
        this.logger.logWithClassName("Key Injection Response Message ::: ".plus(message))
        this.deviceResponse = DeviceResponse(result, message)
    }

    private fun getBytesTypeValue(keyCheckValue: String?): BytesTypeValue {
        val bytesTypeValue = BytesTypeValue()
        if (!keyCheckValue.isNullOrEmptyOrBlank()) {
            val kcvData: ByteArray = StringUtils.hexString2bytes(keyCheckValue!!)
            bytesTypeValue.data = kcvData
        }
        return bytesTypeValue
    }

    override fun getClassName(): String = FeitianKeysServiceImpl::class.java.simpleName
}