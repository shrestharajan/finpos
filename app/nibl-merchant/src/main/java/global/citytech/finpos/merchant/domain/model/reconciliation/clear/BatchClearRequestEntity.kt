package global.citytech.finpos.merchant.domain.model.reconciliation.clear

import global.citytech.finposframework.hardware.io.printer.PrinterService
import global.citytech.finposframework.repositories.ReconciliationRepository
import global.citytech.finposframework.repositories.TransactionRepository

data class BatchClearRequestEntity(
    val reconciliationRepository: ReconciliationRepository,
    val printerService: PrinterService,
    val transactionRepository: TransactionRepository
)