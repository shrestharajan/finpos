package global.citytech.tms.operator.splash

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.common.extensions.openActivity
import global.citytech.easydroid.core.extension.showSuccessMessage
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.easydroid.core.utils.MessageConfig
import global.citytech.finpos.nibl.admin.AppConstant
import global.citytech.finpos.nibl.admin.BR
import global.citytech.finpos.nibl.admin.DeviceConfigurationStatus
import global.citytech.finpos.nibl.admin.R
import global.citytech.finpos.nibl.admin.databinding.ActivityMainBinding
import global.citytech.finpos.nibl.admin.presentation.main.MainViewModel
import global.citytech.finpos.nibl.admin.presentation.printparameter.PrintParameterActivity
import global.citytech.finpos.nibl.admin.presentation.setdatetimegps.SetDateTimeGpsActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppBaseActivity<ActivityMainBinding, MainViewModel>() {

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayout(): Int = R.layout.activity_main

    override fun getViewModel(): MainViewModel =
        ViewModelProviders.of(this).get(MainViewModel::class.java)

    lateinit var progressDialog: ProgressDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressDialog = ProgressDialog(this)
        progressDialog.setCancelable(false)
        progressDialog.setMessage(getString(R.string.please_wait))
        initObservers()
        getViewModel().bindService()
        btn_network_registration.setOnClickListener {
            getViewModel().register()
        }

        btn_set_date_time_gps.setOnClickListener {
            openActivity(SetDateTimeGpsActivity::class.java)
        }

        btn_tms_parameters.setOnClickListener {
            openActivity(PrintParameterActivity::class.java)
        }
    }

    private fun initObservers() {
        getViewModel().serviceLoaded.observe(this, Observer {
            Log.v("TAG:::", "Service Bound")
        })

        getViewModel().isLoading.observe(this, Observer { isLoading ->
            if (isLoading) {
                rl_progress.visibility = View.VISIBLE
            } else {
                rl_progress.visibility = View.GONE
            }
        })

        getViewModel().message.observe(this, Observer {
            showErrorMessage(it)
        })

        getViewModel().deviceConfigurationStatus.observe(this, Observer { status ->
            when (status) {
                DeviceConfigurationStatus.KeysInjectedSuccesfully -> {
                    progressDialog.dismiss()
                    showSuccessMessage(
                        MessageConfig.Builder()
                            .message("Terminal Configured Successfully")
                            .positiveLabel("GO TO MERCHANT APP")
                            .onPositiveClick { dialog, _ ->
                                openActivity(AppConstant.MERCHANT_PACKAGE_NAME)
                                dialog.dismiss()
                            })
                }
                DeviceConfigurationStatus.ConfigurationCanceled -> {
                    progressDialog.dismiss()
                }
                DeviceConfigurationStatus.ConfigurationFailed -> {
                    progressDialog.dismiss()
                    showErrorMessage("Terminal Configuration Failed")
                }
                else -> {
                    progressDialog.setTitle(status.message)
                    progressDialog.show()
                }
            }
        })
    }

    private fun showErrorMessage(it: String?) {
        showSuccessMessage(
            MessageConfig.Builder()
                .message(it)
                .positiveLabel("RETRY")
                .onPositiveClick { _, _ ->
                    btn_network_registration.performClick()
                }
                .negativeLabel("CANCEL")
                .onNegativeClick { dialog, _ ->
                    dialog.dismiss()
                }
        )

    }
}
