package global.citytech.finpos.processor.nibl.receipt.customercopy;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import global.citytech.finpos.processor.nibl.transaction.receipt.NiblTransactionReceiptPrintHandler;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

/**
 * Created by Unique Shakya on 12/16/2020.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({CustomerCopyRequestModel.class, CustomerCopyResponseModel.class, ReceiptLog.class,
        TransactionRepository.class, PrinterService.class, ReceiptHandler.TransactionReceiptHandler.class,
        NiblTransactionReceiptPrintHandler.class})
public class CustomerCopyUseCaseTest {

    CustomerCopyRequestModel customerCopyRequestModel;
    CustomerCopyResponseModel customerCopyResponseModel;
    ReceiptLog receiptLog;
    TransactionRepository transactionRepository;
    PrinterService printerService;
    ReceiptHandler.TransactionReceiptHandler transactionReceiptHandler;
    NiblTransactionReceiptPrintHandler niblTransactionReceiptPrintHandler;

    @Before
    public void setup() {
        customerCopyRequestModel = PowerMockito.mock(CustomerCopyRequestModel.class);
        customerCopyResponseModel = PowerMockito.mock(CustomerCopyResponseModel.class);
        receiptLog = PowerMockito.mock(ReceiptLog.class);
        transactionRepository = PowerMockito.mock(TransactionRepository.class);
        printerService = PowerMockito.mock(PrinterService.class);
        transactionReceiptHandler = PowerMockito.mock(ReceiptHandler.TransactionReceiptHandler.class);
        niblTransactionReceiptPrintHandler = PowerMockito.mock(NiblTransactionReceiptPrintHandler.class);
    }

    @Test
    public void printTest() throws Exception {
        PowerMockito.when(transactionRepository.getReceiptLog()).thenReturn(receiptLog);

        PowerMockito.when(customerCopyRequestModel.getTransactionRepository()).thenReturn(this.transactionRepository);
        PowerMockito.when(customerCopyRequestModel.getPrinterService()).thenReturn(this.printerService);

        PowerMockito.doNothing().when(this.niblTransactionReceiptPrintHandler)
                .printTransactionReceipt(any(), any());

        PowerMockito.whenNew(NiblTransactionReceiptPrintHandler.class).withAnyArguments()
                .thenReturn(this.niblTransactionReceiptPrintHandler);

        CustomerCopyUseCase customerCopyUseCase = new CustomerCopyUseCase();
        CustomerCopyResponseModel customerCopyResponseModel = customerCopyUseCase.execute(this.customerCopyRequestModel);

        assertNotNull(customerCopyResponseModel);
    }
}
