package global.citytech.finpos.merchant.domain.usecase.qr.nqr

import global.citytech.finpos.merchant.domain.usecase.qr.base.QrGenerateResponse
/**
 * @author sachin
 */
class NqrGenerateResponse(val data: String): QrGenerateResponse {
}