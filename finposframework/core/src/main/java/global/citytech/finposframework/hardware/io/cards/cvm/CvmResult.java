package global.citytech.finposframework.hardware.io.cards.cvm;

public enum CvmResult {

    UNKNOWN,
    FAILED,
    SUCCESSFUL
}
