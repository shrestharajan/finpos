package global.citytech.finposframework.hardware.emv.aid

/**
 * Created by Rishav Chudal on 5/3/20.
 */
class AidParameters(
    val aid: String,
    val label: String,
    val terminalAidVersion: String,
    val defaultTDOL: String,
    val defaultDDOL: String,
    val denialActionCode: String,
    val onlineActionCode: String,
    val defaultActionCode: String,
    val terminalFloorLimit: String,
    val contactlessFloorLimit: String,
    val contactlessTransactionLimit: String,
    val cvmLimit: String
) {
    var cdCvmLimit: String? = null
    var noCdCvmLimit: String? = null
    var cvmCapCvmRequired: String? = null
    var cvmCapNoCvmRequired: String? = null
    var terminalRiskManagementData: String? = null
    var clTacDenial: String? = null
    var clTacOnline: String? = null
    var clTacDefault: String? = null
    var acquirerId: String? = null
    var kernelConfig: String? = null
    var clCardDataInputCapability: String? = null
    var clSecurityCapability: String? = null
    var udol: String? = null
    var msdAppVersion: String? = null
    var ttq: String? = null
}