package global.citytech.finpos.merchant.presentation.model.qroperator

/**
 * @author sachin
 */
data class QrOperatorItem(
    val id: String,
    val name: String,
    val logo: String,
    var issuers: List<String>? = null,
    val acquirerCode: String,
    val host: String,
    val active: Boolean
)