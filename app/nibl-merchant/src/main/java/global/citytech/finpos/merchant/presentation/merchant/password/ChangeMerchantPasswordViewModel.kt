package global.citytech.finpos.merchant.presentation.merchant.password

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager

/**
 * Created by Unique Shakya on 8/2/2021.
 */
class ChangeMerchantPasswordViewModel(val context: Application) : BaseAndroidViewModel(context) {

    val invalidCurrentPassword by lazy { MutableLiveData<String>() }
    val invalidNewPassword by lazy { MutableLiveData<String>() }
    val invalidConfirmPassword by lazy { MutableLiveData<String>() }
    val passwordChangeSuccess by lazy { MutableLiveData<Boolean>() }

    val localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    fun changePassword(currentPassword: String?, newPassword: String?, confirmPassword: String?) {
        if (currentPassword.isNullOrEmpty()) {
            invalidCurrentPassword.value = context.getString(R.string.error_msg_empty_password)
            return
        }

        if (newPassword.isNullOrEmpty()) {
            invalidNewPassword.value = context.getString(R.string.error_msg_empty_password)
            return
        }

        if (confirmPassword.isNullOrEmpty()) {
            invalidConfirmPassword.value = context.getString(R.string.error_msg_empty_password)
            return
        }

        if (currentPassword.length != 4) {
            invalidCurrentPassword.value = context.getString(R.string.error_msg_invalid_length_password)
            return
        }

        if (newPassword.length != 4) {
            invalidNewPassword.value = context.getString(R.string.error_msg_invalid_length_password)
            return
        }

        if (confirmPassword.length != 4) {
            invalidConfirmPassword.value = context.getString(R.string.error_msg_invalid_length_password)
            return
        }


        if (currentPassword == localDataUseCase.getMerchantCredential()) {
            if (newPassword == confirmPassword) {
                if(newPassword == localDataUseCase.getMerchantCredential()){
                    invalidConfirmPassword.value = context.getString(R.string.error_msg_current_pin_matched_with_new_pin)
                    return
                }
                localDataUseCase.saveMerchantCredential(newPassword)
                passwordChangeSuccess.value = true
            } else {
                invalidConfirmPassword.value = context.getString(R.string.error_msg_password_mismatch)
            }
        } else {
            invalidCurrentPassword.value = context.getString(R.string.error_msg_wrong_password)
        }
    }
}