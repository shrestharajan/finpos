package global.citytech.finpos.merchant.presentation.menu

import android.app.Activity
import android.widget.Toast
import global.citytech.common.extensions.openActivity
import global.citytech.easydroid.core.extension.showToast
import global.citytech.finpos.merchant.presentation.admin.AdminActivity
import global.citytech.finpos.merchant.presentation.auth.completion.AuthorisationCompletionActivity
import global.citytech.finpos.merchant.presentation.balanceinquiry.BalanceInquiryActivity
import global.citytech.finpos.merchant.presentation.cashadvance.CashAdvanceActivity
import global.citytech.finpos.merchant.presentation.greenpin.GreenPinActivity
import global.citytech.finpos.merchant.presentation.merchant.MerchantActivity
import global.citytech.finpos.merchant.presentation.ministatement.MiniStatementActivity
import global.citytech.finpos.merchant.presentation.preauth.PreAuthActivity
import global.citytech.finpos.merchant.presentation.refund.RefundActivity
import global.citytech.finpos.merchant.presentation.voidsale.VoidActivity


/**
 * Created by Saurav Ghimire on 1/19/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
interface MenuHandler {
    fun performOnClick(context: Activity)
}

class PreAuthHandler : MenuHandler {
    override fun performOnClick(context: Activity) {
        context.openActivity(PreAuthActivity::class.java)
    }
}

class VoidHandler : MenuHandler {
    override fun performOnClick(context: Activity) {
        context.openActivity(VoidActivity::class.java)
    }
}

class AuthCompletionHandler : MenuHandler {
    override fun performOnClick(context: Activity) {
        context.openActivity(AuthorisationCompletionActivity::class.java)
    }
}

class CashAdvanceMenuHandler : MenuHandler {
    override fun performOnClick(context: Activity) {
        context.openActivity(CashAdvanceActivity::class.java)
    }
}

class RefundHandler : MenuHandler {
    override fun performOnClick(context: Activity) {
        context.openActivity(RefundActivity::class.java)
    }

}

class CashDepositHandler : MenuHandler {
    override fun performOnClick(context: Activity) {
        context.showToast("To be Implemented", Toast.LENGTH_SHORT)
    }

}

class MiniStatementHandler : MenuHandler {
    override fun performOnClick(context: Activity) {
        context.openActivity(MiniStatementActivity::class.java)
//        context.showToast("To be Implemented", Toast.LENGTH_SHORT)
    }

}

class BalanceInquiryHandler : MenuHandler {
    override fun performOnClick(context: Activity) {
        context.openActivity(BalanceInquiryActivity::class.java)
    }

}


class MerchantMenuHandler : MenuHandler {
    override fun performOnClick(context: Activity) {
        context.openActivity(MerchantActivity::class.java)
    }
}

class AdminMenuHandler : MenuHandler {
    override fun performOnClick(context: Activity) {
        context.openActivity(AdminActivity::class.java)
        context.finish()
    }
}

class GreenPinMenuHandler : MenuHandler {
    override fun performOnClick(context: Activity) {
        context.openActivity(GreenPinActivity::class.java)
    }
}