package global.citytech.finpos.processor.neps.receipt.detailreport;

/**
 * Created by Saurav Ghimire on 3/29/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public class DetailReport {

    private String cardNumber;
    private String cardScheme;
    private String invoiceNumber;
    private String approvalCode;
    private String transactionType;
    private String amount;
    private String transactionDate;
    private String transactionTime;

    public String getTransactionDate() {
        return transactionDate;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getCardScheme() {
        return cardScheme;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public String getAmount() {
        return amount;
    }

    public static final class Builder {
        private String cardNumber;
        private String cardScheme;
        private String invoiceNumber;
        private String approvalCode;
        private String transactionType;
        private String amount;
        private String transactionDate;
        private String transactionTime;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
            return this;
        }

        public Builder withCardScheme(String cardScheme) {
            this.cardScheme = cardScheme;
            return this;
        }

        public Builder withInvoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }

        public Builder withApprovalCode(String approvalCode) {
            this.approvalCode = approvalCode;
            return this;
        }

        public Builder withTransactionType(String transactionType) {
            this.transactionType = transactionType;
            return this;
        }

        public Builder withAmount(String amount) {
            this.amount = amount;
            return this;
        }

        public Builder withTransactionTime(String transactionTime) {
            this.transactionTime = transactionTime;
            return this;
        }

        public Builder withTransactionDate(String transactionDate) {
            this.transactionDate = transactionDate;
            return this;
        }

        public DetailReport build() {
            DetailReport detailReport = new DetailReport();
            detailReport.invoiceNumber = this.invoiceNumber;
            detailReport.cardScheme = this.cardScheme;
            detailReport.approvalCode = this.approvalCode;
            detailReport.amount = this.amount;
            detailReport.transactionType = this.transactionType;
            detailReport.cardNumber = this.cardNumber;
            detailReport.transactionTime = this.transactionTime;
            detailReport.transactionDate = this.transactionDate;
            return detailReport;
        }
    }
}
