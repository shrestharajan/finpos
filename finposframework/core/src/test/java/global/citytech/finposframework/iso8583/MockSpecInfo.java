package global.citytech.finposframework.iso8583;

/** User: Surajchhetry Date: 2/24/20 Time: 3:17 PM */
public class MockSpecInfo implements SpecInfo {

  @Override
  public DataElementConfigs getDEConfig() {
    return new MockDEConfig();
  }

  @Override
  public int getMessageHeaderLength() {
    return 2;
  }

  @Override
  public int getMTILength() {
    return 0;
  }
}
