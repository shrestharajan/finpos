package global.citytech.finpos.processor.neps.receipt.duplicate;

import global.citytech.finpos.processor.neps.transaction.NepsTransactionReceiptPrintHandler;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.switches.receipt.duplicatereceipt.DuplicateReceiptRequester;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
import global.citytech.finposframework.utility.StringUtils;

public class DuplicateReceiptUseCase implements DuplicateReceiptRequester<
        DuplicateReceiptRequestModel, DuplicateReceiptResponseModel> {
    private TransactionRepository transactionRepository;
    private PrinterService printerService;
    private DuplicateReceiptResponseModel duplicateReceiptResponseModel;

    @Override
    public DuplicateReceiptResponseModel execute(DuplicateReceiptRequestModel requestModel) {
        transactionRepository = requestModel.getTransactionRepository();
        printerService = requestModel.getPrinterService();
        ReceiptLog receiptLog = getReceiptLogBasedOnStan(requestModel.getStan());
        if (receiptLog == null)
            throw new PosException(PosError.DEVICE_ERROR_NO_RECENT_TRANSACTION);
        printDuplicateReceiptLog(receiptLog);
        return duplicateReceiptResponseModel;
    }

    private ReceiptLog getReceiptLogBasedOnStan(String stan) {
        if (StringUtils.isEmpty(stan)) {
            return transactionRepository.getReceiptLog();
        } else {
            return transactionRepository.getReceiptLogByStan(stan);
        }
    }

    private void printDuplicateReceiptLog(ReceiptLog receiptLog) {
        ReceiptHandler.TransactionReceiptHandler transactionReceiptHandler = new
                NepsTransactionReceiptPrintHandler(printerService);
        try {
            PrinterResponse printerResponse = transactionReceiptHandler.printTransactionReceipt(
                    receiptLog,
                    ReceiptVersion.DUPLICATE_COPY
            );
            assignDuplicateReceiptResponseBasedOnPrinterResponse(printerResponse);
        } catch (PosException e) {
            throw e;
        } catch (Exception e) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_PRINT);
        }

    }

    private void assignDuplicateReceiptResponseBasedOnPrinterResponse(PrinterResponse printerResponse) {
        Result printResponseResult = printerResponse.getResult();
        if (printResponseResult == Result.SUCCESS) {
            duplicateReceiptResponseModel = new DuplicateReceiptResponseModel(
                    printResponseResult,
                    "Duplicate Receipt Printed"
            );
        } else {
            duplicateReceiptResponseModel = new DuplicateReceiptResponseModel(
                    printResponseResult,
                    printerResponse.getMessage()
            );
        }
    }
}
