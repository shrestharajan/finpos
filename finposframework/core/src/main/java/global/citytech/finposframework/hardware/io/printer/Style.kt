package global.citytech.finposframework.hardware.io.printer

class Style {
    private var bold = false
    private var italic = false
    private var underline = false
    private var fontSize: FontSize? = null
    private var alignment: Align? = null
    private var multipleAlignment = false
    private var allCaps = false
    var qrWidth = 0
    var fontType =
        FontType.ARIAL

    enum class Align(val align: Int) {
        LEFT(0), CENTER(1), RIGHT(2);

    }

    enum class FontSize {
        NORMAL, XSMALL, SMALL, LARGE, XLARGE
    }

    enum class FontType {
        ARIAL, MONOSPACE
    }

    fun isAllCaps(): Boolean {
        return allCaps
    }

    fun setAllCaps(allCaps: Boolean) {
        this.allCaps = allCaps
    }

    fun isBold(): Boolean {
        return bold
    }

    fun setBold(bold: Boolean) {
        this.bold = bold
    }

    fun isItalic(): Boolean {
        return italic
    }

    fun setItalic(italic: Boolean) {
        this.italic = italic
    }

    fun isUnderline(): Boolean {
        return underline
    }

    fun setUnderline(underline: Boolean) {
        this.underline = underline
    }

    fun getFontSize(): FontSize? {
        return fontSize
        /*switch (this.fontSize) {
            case NORMAL:
                return 22;
            case SMALL:
                return 18;
            case LARGE:
                return 26;
            case XLARGE:
                return 32;
            default:
                return 20;
        }*/
    }

    fun setFontSize(fontSize: FontSize?) {
        this.fontSize = fontSize
    }

    fun getAlignment(): Align? {
        return alignment
    }

    fun setAlignment(alignment: Align?) {
        this.alignment = alignment
    }

    fun isMultipleAlignment(): Boolean {
        return multipleAlignment
    }

    fun setMultipleAlignment(multipleAlignment: Boolean) {
        this.multipleAlignment = multipleAlignment
    }

    class Builder {
        private var bold = false
        private var italic = false
        private var underline = false
        private var fontSize =
            FontSize.NORMAL
        private var alignment =
            Align.LEFT
        private var multipleAlignment = false
        private var allCaps = true
        fun allCaps(allCaps: Boolean): Builder {
            this.allCaps = allCaps
            return this
        }

        fun bold(bold: Boolean): Builder {
            this.bold = bold
            return this
        }

        fun italic(italic: Boolean): Builder {
            this.italic = italic
            return this
        }

        fun underline(underline: Boolean): Builder {
            this.underline = underline
            return this
        }

        fun fontSize(fontSize: FontSize): Builder {
            this.fontSize = fontSize
            return this
        }

        fun alignment(alignment: Align): Builder {
            this.alignment = alignment
            return this
        }

        fun multipleAlignment(multipleAlignment: Boolean): Builder {
            this.multipleAlignment = multipleAlignment
            return this
        }

        fun build(): Style {
            val style =
                Style()
            style.alignment = alignment
            style.bold = bold
            style.italic = italic
            style.fontSize = fontSize
            style.multipleAlignment = multipleAlignment
            style.underline = underline
            style.allCaps = allCaps
            return style
        }
    }

}
