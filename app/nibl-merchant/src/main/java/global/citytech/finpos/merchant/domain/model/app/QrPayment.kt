package global.citytech.finpos.merchant.domain.model.app

import android.os.Parcel
import android.os.Parcelable
import java.math.BigDecimal

data class QrPayment(
    val id: String = "",
    val merchantId: String,
    val terminalId: String,
    val referenceNumber: String,
    val invoiceNumber: String,
    val transactionType: String,
    val transactionDate: String,
    val transactionTime: String,
    val transactionCurrency: String,
    val transactionAmount: BigDecimal,
    var transactionStatus: String,
    val approvalCode: String? = "",
    val paymentInitiator: String? = "",
    val initiatorId: String? = "",
    var isRefunded: Boolean = false,
    var isVoided: Boolean = false
) :
    Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!.toBigDecimal(),
        parcel.readString()!!,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()!!.toBoolean(),
        parcel.readString()!!.toBoolean()
    )

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as QrPayment

        if (referenceNumber != other.referenceNumber) return false
        if (invoiceNumber != other.invoiceNumber) return false

        return true
    }

    override fun hashCode(): Int {
        var result = referenceNumber.hashCode()
        result = 31 * result + invoiceNumber.hashCode()
        return result
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(merchantId)
        parcel.writeString(terminalId)
        parcel.writeString(referenceNumber)
        parcel.writeString(invoiceNumber)
        parcel.writeString(transactionType)
        parcel.writeString(transactionDate)
        parcel.writeString(transactionTime)
        parcel.writeString(transactionCurrency)
        parcel.writeString(transactionAmount.toString())
        parcel.writeString(transactionStatus)
        parcel.writeString(approvalCode)
        parcel.writeString(paymentInitiator)
        parcel.writeString(initiatorId)
        parcel.writeString(isRefunded.toString())
        parcel.writeString(isVoided.toString())
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<QrPayment> {
        override fun createFromParcel(parcel: Parcel): QrPayment {
            return QrPayment(parcel)
        }

        override fun newArray(size: Int): Array<QrPayment?> {
            return arrayOfNulls(size)
        }
    }
}