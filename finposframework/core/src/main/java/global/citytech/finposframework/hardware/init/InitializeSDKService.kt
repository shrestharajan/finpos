package global.citytech.finposframework.hardware.init

import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.DeviceService

/**
 * Created by Rishav Chudal on 4/8/20.
 */
interface InitializeSDKService : DeviceService {
    fun initializeSDK() : DeviceResponse
}