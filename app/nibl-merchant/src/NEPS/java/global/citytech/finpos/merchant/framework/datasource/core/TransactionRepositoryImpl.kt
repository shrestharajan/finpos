package global.citytech.finpos.merchant.framework.datasource.core

import android.app.Application
import android.text.TextUtils
import global.citytech.common.extensions.rupeesToIsoAmountFormat
import global.citytech.easydroid.core.preference.SecurePreference
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.app.db.AppDatabase
import global.citytech.finpos.merchant.domain.model.app.TransactionLog
import global.citytech.finpos.merchant.domain.model.app.mapToUseCase
import global.citytech.finpos.merchant.domain.model.app.mapToFinPosFramework
import global.citytech.finpos.merchant.framework.datasource.app.AutoReversalQueueDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.presentation.model.setting.TerminalSettingResponse
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.repositories.TransactionRepository
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.data.AutoReversal
import global.citytech.finposframework.usecases.transaction.data.Bin
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog
import java.util.*
import kotlin.Exception

class TransactionRepositoryImpl(val instance: Application) : TransactionRepository {

    private val logger = Logger(TransactionRepositoryImpl::class.java.name)
    val PREF_RECENT_TRANSACTION: String = "pref_recent_transaction"
    val KEY_RECENT_TRANSACTION: String = "key_recent_transaction"

    override fun getReceiptLog(): ReceiptLog? {
        val securePreference = SecurePreference(
            instance,
            PREF_RECENT_TRANSACTION
        )
        val recentTransactionString = securePreference.retrieveData(
            KEY_RECENT_TRANSACTION,
            ""
        )
        if (TextUtils.isEmpty(recentTransactionString))
            return null
        return Jsons.fromJsonToObj(recentTransactionString, ReceiptLog::class.java)
    }

    override fun getAutoReversal(): AutoReversal? {
        logger.debug("::: AUTO REVERSAL ::: GETTING AUTO REVERSAL ")
        val autoReversalDataSource = AutoReversalQueueDataSourceImpl(instance.applicationContext)
        return autoReversalDataSource.getRecentRequest()
    }

    override fun saveAutoReversal(autoReversal: AutoReversal) {
        logger.debug("::: AUTO REVERSAL ::: SAVING AUTO REVERSAL ".plus(autoReversal.transactionType))
        val autoReversalDataSource = AutoReversalQueueDataSourceImpl(instance.applicationContext)
        autoReversalDataSource.addRequest(autoReversal)
    }

    override fun clearAutoReversal() {
        logger.debug("::: AUTO REVERSAL ::: CLEARING AUTO REVERSAL ")
        val autoReversalDataSource = AutoReversalQueueDataSourceImpl(instance.applicationContext)
        autoReversalDataSource.clear()
    }

    override fun updateTipAdjustedStatusForGivenTransactionDetails(
        tipAdjusted: Boolean,
        transactionType: TransactionType?,
        invoiceNumber: String?
    ) {

    }

    override fun getAutoReversalList(): MutableList<AutoReversal> {
        logger.debug("::: AUTO REVERSAL ::: GETTING ALL AUTO REVERSAL ")
        val autoReversalDataSource = AutoReversalQueueDataSourceImpl(instance.applicationContext)
        return autoReversalDataSource.getAllQueueRequests()
    }

    override fun incrementAutoReversalRetryCount(autoReversal: AutoReversal) {
        logger.debug("::: AUTO REVERSAL ::: INCREMENT AUTO REVERSAL COUNT")
        val autoReversalDataSource = AutoReversalQueueDataSourceImpl(instance.applicationContext)
        return autoReversalDataSource.incrementRetryCount(autoReversal)
    }

    override fun isAutoReversalPresent(): Boolean {
        val autoReversalDataSource = AutoReversalQueueDataSourceImpl(instance.applicationContext)
        return autoReversalDataSource.getSize() > 0
    }

    override fun changeAutoReversalStatus(
        autoReversal: AutoReversal,
        status: AutoReversal.Status
    ) {
        logger.debug("::: AUTO REVERSAL ::: CHANGE AUTO REVERSAL STATUS")
        val autoReversalDataSource = AutoReversalQueueDataSourceImpl(instance.applicationContext)
        return autoReversalDataSource.changeStatus(autoReversal, status)
    }

    override fun getReceiptLogByStan(stan: String): ReceiptLog {
        val transactionLog = AppDatabase.getInstance(instance)
            .getTransactionLogDao()
            .getTransactionLogsByStan(stan)
        return Jsons.fromJsonToObj(transactionLog.receiptLog, ReceiptLog::class.java)
    }

    override fun getTipTransactionLogWithInvoiceNumber(
        invoiceNumber: String,
        batchNumber: String
    ): global.citytech.finposframework.usecases.transaction.TransactionLog? {
        return null
    }

    override fun updateAuthorizationCompletedStatusByAuthCode(
        completed: Boolean, transationType: TransactionType,
        authCode: String
    ) {
        AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .updatePreAuthCompletionStatusByAuthCode(
                completed, Jsons.toJsonObj(transationType),
                authCode
            )
    }

    override fun updateAuthorizationCompletedStatusByRRN(
        completed: Boolean, transationType: TransactionType,
        rrn: String
    ) {
        AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .updatePreAuthCompletionStatusByRRN(
                completed, Jsons.toJsonObj(transationType),
                rrn
            )
    }

    override fun updateReceiptLog(receiptLog: ReceiptLog?) {
        val securePreference: SecurePreference = SecurePreference(
            instance, PREF_RECENT_TRANSACTION
        )
        val recentTransactionString = Jsons.toJsonObj(receiptLog)
        securePreference.saveData(KEY_RECENT_TRANSACTION, recentTransactionString)
    }

    override fun getRecentTransactionLog():
            global.citytech.finposframework.usecases.transaction.TransactionLog? {
        val transactionLog = AppDatabase
            .getInstance(instance)
            .getTransactionLogDao()
            .getRecentTransaction()
        if (transactionLog != null) {
            return transactionLog.mapToUseCase()
        }
        return null
    }

    override fun deleteTransactionLogByStan(stan: String) {
        AppDatabase.getInstance(instance).getTransactionLogDao().deleteByStan(stan)
    }

    override fun getTransactionLogsByBatchNumber(batchNumber: String): List<global.citytech.finposframework.usecases.transaction.TransactionLog> {
        val transactionLogs = AppDatabase.getInstance(instance)
            .getTransactionLogDao()
            .getTransactionLogsByBatchNumber(batchNumber)
        if (transactionLogs.isEmpty())
            return emptyList()
        val transactionLogList =
            ArrayList<global.citytech.finposframework.usecases.transaction.TransactionLog>()
        transactionLogs.forEach {
            transactionLogList.add(it.mapToUseCase())
        }
        return transactionLogList
    }

    override fun updateTransactionLog(
        transactionLog: global.citytech.finposframework.usecases.transaction.TransactionLog?
    ) {
        if (transactionLog == null)
            return
        val log = TransactionLog(
            stan = transactionLog.stan,
            terminalId = transactionLog.terminalId,
            merchantId = transactionLog.merchantId,
            invoiceNumber = transactionLog.invoiceNumber,
            rrn = transactionLog.rrn,
            authCode = transactionLog.authCode,
            transactionType = Jsons.toJsonObj(transactionLog.transactionType),
            origTransactionType = Jsons.toJsonObj(transactionLog.originalTransactionType),
            transactionAmount = transactionLog.transactionAmount.rupeesToIsoAmountFormat(),
            origTransactionAmount = transactionLog.originalTransactionAmount.rupeesToIsoAmountFormat(),
            transactionDate = transactionLog.transactionDate,
            transactionTime = transactionLog.transactionTime,
            transactionStatus = transactionLog.transactionStatus,
            posEntryMode = Jsons.toJsonObj(transactionLog.posEntryMode),
            origPosEntryMode = Jsons.toJsonObj(transactionLog.originalPosEntryMode),
            posConditionCode = transactionLog.posConditionCode,
            origPosConditionCode = transactionLog.originalPosConditionCode,
            reconcileStatus = transactionLog.reconcileStatus,
            reconcileTime = transactionLog.reconcileTime,
            reconcileDate = transactionLog.reconcileDate,
            reconcileBatchNo = transactionLog.reconcileBatchNo,
            readCardResponse = Jsons.toJsonObj(transactionLog.readCardResponse),
            processingCode = transactionLog.processingCode,
            originalTransactionReferenceNumber = transactionLog.originalTransactionReferenceNumber,
            receiptLog = Jsons.toJsonObj(transactionLog.receiptLog),
            responseCode = transactionLog.responseCode,
            authorizationCompleted = transactionLog.isAuthorizationCompleted,
            transactionVoided = transactionLog.isTransactionVoided,
            isEmiTransaction = transactionLog.isEmiTransaction,
            emiInfo = transactionLog.emiInfo,
            currencyCode = transactionLog.currencyCode,
            vatInfo = transactionLog.vatInfo
        )

        logger.debug(":::: READ CARD RESPONSE JSON STRING :::: " + Jsons.toJsonObj(transactionLog.readCardResponse))
        val transactionLogDao = AppDatabase.getInstance(instance).getTransactionLogDao()
        transactionLogDao.insert(log)
    }

    override fun getTransactionLogWithAuthCode(authCode: String): global.citytech.finposframework.usecases.transaction.TransactionLog? {
        logger.debug("AUTH CODE IN TRANSACTION REPOSITORY == $authCode")
        return AppDatabase.getInstance(instance)
            .getTransactionLogDao()
            .getTransactionLogByAuthCode(authCode)?.mapToUseCase()
    }

    override fun getTransactionLogWithRRN(rrn: String?): global.citytech.finposframework.usecases.transaction.TransactionLog? {
        println("RRN IN TRANSACTION REPOSITORY == $rrn")
        return AppDatabase.getInstance(instance)
            .getTransactionLogDao()
            .getTransactionLogByRrn(rrn!!)?.mapToUseCase()
    }

    override fun removeAutoReversal(autoReversal: AutoReversal) {
        logger.debug("::: AUTO REVERSAL ::: REMOVING AUTO REVERSAL ")
        val autoReversalDataSource = AutoReversalQueueDataSourceImpl(instance.applicationContext)
        autoReversalDataSource.removeRequest(autoReversal)
    }

    override fun getTransactionLogWithInvoiceNumber(
        invoiceNumber: String,
        batchNumber: String
    ): global.citytech.finposframework.usecases.transaction.TransactionLog? {
        logger.debug("INVOICE NUMBER IN TRANSACTION REPOSITORY ::: $invoiceNumber")
        val transactionLog = AppDatabase
            .getInstance(instance)
            .getTransactionLogDao()
            .getTransactionLogBy(invoiceNumber, batchNumber)

        if (transactionLog != null) return transactionLog.mapToUseCase()
        return null
    }

    override fun getTransactionLogWithInvoiceNumberWithoutBatchNumber(
        invoiceNumber: String
    ): global.citytech.finposframework.usecases.transaction.TransactionLog? {
        //Not need in Neps
        return null
    }

    override fun updateVoidStatusForGivenTransactionDetails(
        voided: Boolean,
        transactionType: TransactionType?,
        invoiceNumber: String
    ) {
        AppDatabase.getInstance(NiblMerchant.INSTANCE).getTransactionLogDao()
            .updateOnVoidStatusForGivenTransactionDetails(
                voided,
                Jsons.toJsonObj(transactionType),
                invoiceNumber
            )
    }

    override fun getTransactionBins(): List<Bin>? {
        val terminalSettingsJson = PreferenceManager.getTerminalSetting()
        return try {
            val terminalSettings = Jsons.fromJsonToObj(
                terminalSettingsJson,
                TerminalSettingResponse::class.java
            )
            terminalSettings.bins?.mapToFinPosFramework()
        } catch (ex: Exception) {
            ex.printStackTrace()
            null
        }
    }

    override fun clearSharedPreference() {
        val securePreference = SecurePreference(
            instance,
            PREF_RECENT_TRANSACTION
        )
        securePreference.clear()
    }
}