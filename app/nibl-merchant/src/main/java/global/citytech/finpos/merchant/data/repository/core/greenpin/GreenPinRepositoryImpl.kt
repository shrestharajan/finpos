package global.citytech.finpos.merchant.data.repository.core.greenpin

import global.citytech.finpos.merchant.data.datasource.core.greenpin.GreenPinDataSource
import global.citytech.finpos.merchant.domain.model.greenpin.GreenPinResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.greenpin.GreenPinRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.greenpin.GreenPinRequestItem
import io.reactivex.Observable

class GreenPinRepositoryImpl(private val otpRequestDataSource: GreenPinDataSource) :
    GreenPinRepository {
    override fun generateOtpRequest(
        configurationItem: ConfigurationItem,
        greenPinRequestItem: GreenPinRequestItem
    ): Observable<GreenPinResponseEntity> =
        otpRequestDataSource.generateOtpRequest(configurationItem, greenPinRequestItem)


}