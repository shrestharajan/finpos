package global.citytech.finposframework.usecases.transaction.data;

/**
 * Created by Rishav Chudal on 12/20/21.
 */
public class Bin {
    private final String id;
    private final String bin;
    private final String title;
    private final String type;
    private final String bank;
    private final boolean isActive;

    public Bin(Builder builder) {
        this.id = builder.id;
        this.bin = builder.bin;
        this.title = builder.title;
        this.type = builder.type;
        this.bank = builder.bank;
        this.isActive = builder.isActive;
    }

    public String getId() {
        return id;
    }

    public String getBin() {
        return bin;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public String getBank() {
        return bank;
    }

    public boolean isActive() {
        return isActive;
    }
    
    public static final class Builder {
        private String id = "";
        private String bin = "";
        private String title = "";
        private String type = "";
        private String bank = "";
        private boolean isActive = true;
        
        public Bin build() {
            return new Bin(this);
        }

        public Builder setId(String id) {
            this.id = id;
            return this;
        }

        public Builder setBin(String bin) {
            this.bin = bin;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setType(String type) {
            this.type = type;
            return this;
        }

        public Builder setBank(String bank) {
            this.bank = bank;
            return this;
        }

        public Builder setIsActive(boolean isActive) {
            this.isActive = isActive;
            return this;
        }
    }
}
