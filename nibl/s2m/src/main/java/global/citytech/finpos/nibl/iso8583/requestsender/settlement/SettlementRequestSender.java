package global.citytech.finpos.nibl.iso8583.requestsender.settlement;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.iso8583.NIBLMessagePayloadBuilder;
import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.utility.StringUtils;

/***
 * @author Surajchhetry
 */
public class SettlementRequestSender extends NIBLMessageSenderTemplate {
    private Logger logger = Logger.getLogger(SettlementRequestSender.class.getName());
    private boolean isSettlementClosure;

    public SettlementRequestSender(SpecInfo specInfo, RequestContext context, boolean isSettlementClosure) {
        super(specInfo, context);
        this.isSettlementClosure = isSettlementClosure;
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Reconciliation,
                        MTI.MessageFunction.Request,
                        MTI.Originator.Acquirer);
        SettlementRequest settlementRequest = (SettlementRequest) context.getRequest();
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);
        //   msg.addField(new DataElement(DataElement.AMOUNT, HelperUtils.toISOAmount(0, 12)));
        //   msg.addField(new DataElement(DataElement.POS_ENTRY_MODE, PosEntryModeConfig.getValue(voidSaleRequest.getEntryMode()))); // Based on transaction source like ICC,Manual
        //   msg.addField(new DataElement(DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE, voidSaleRequest.getPosConditionCode()));
        msg.addField(new DataElement(DataElement.PRIVATE_USE_FIELD_60, settlementRequest.getBatchNumber()));
        msg.addField(new DataElement(DataElement.PRIVATE_USE_FIELD_63, settlementRequest.getReconciliationTotals().toTotalsBlock()));
        MessagePayload.Builder builder = new NIBLMessagePayloadBuilder(this.specInfo, msg);
        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());
        return builder.build();
    }

    @Override
    protected String getProcessingCode() {
        if (this.isSettlementClosure)
            return ProcessingCode.SETTLEMENT_CLOSURE.getCode();
        return ProcessingCode.SETTLEMENT.getCode();
    }

    @Override
    protected String getFunctionCode() {
        return "0011";
    }
}
