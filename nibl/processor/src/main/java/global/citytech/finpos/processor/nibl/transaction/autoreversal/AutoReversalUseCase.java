package global.citytech.finpos.processor.nibl.transaction.autoreversal;

import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
import global.citytech.finpos.processor.nibl.transaction.reversal.ReversalRequestModel;
import global.citytech.finpos.processor.nibl.transaction.reversal.ReversalUseCase;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.autoreversal.AutoReversalRequester;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.JsonUtils;

/**
 * Created by Unique Shakya on 6/22/2021.
 */
public class AutoReversalUseCase implements AutoReversalRequester<AutoReversalRequestModel, AutoReversalResponseModel>,
        UseCase<AutoReversalRequestModel, AutoReversalResponseModel> {

    private TerminalRepository terminalRepository;
    private Notifier notifier;
    private AutoReversalRequestModel autoReversalRequestModel;
    private AutoReversal autoReversal;
    private Logger logger = Logger.getLogger(AutoReversalUseCase.class.getName());
    private static final int RETRY_LIMIT = 5;

    public AutoReversalUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        this.terminalRepository = terminalRepository;
        this.notifier = notifier;
    }

    @Override
    public AutoReversalResponseModel execute(AutoReversalRequestModel autoReversalRequestModel) {
        logger.log("::: AUTO REVERSAL ::: REACHED AUTO REVERSAL USE CASE");
        this.autoReversalRequestModel = autoReversalRequestModel;
        if (this.autoReversalRequestModel.getTransactionRepository().isAutoReversalPresent()) {
            logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL PRESENT ");
            this.autoReversal = this.autoReversalRequestModel.getTransactionRepository().getAutoReversal();
            if (this.autoReversal.getStatus() == AutoReversal.Status.ACTIVE) {
                try {
                    NiblReversalRequest niblReversalRequest = JsonUtils.fromJsonToObj(autoReversal.getReversalRequest(),
                            NiblReversalRequest.class);
                    return this.performReversal(niblReversalRequest, autoReversal.getTransactionType());
                } catch (Exception e) {
                    e.printStackTrace();
                    return new AutoReversalResponseModel(Result.FAILURE, "Could not complete auto reversal");
                }
            }
        }
        return new AutoReversalResponseModel(Result.SUCCESS, "No Auto Reversal Present");
    }

    private AutoReversalResponseModel performReversal(NiblReversalRequest niblReversalRequest,
                                                      TransactionType transactionType) {
        ReversalUseCase reversalUseCase = new ReversalUseCase(niblReversalRequest,
                this.terminalRepository,
                this.notifier
        );
        TransactionRequest transactionRequest = new TransactionRequest(transactionType);
        transactionRequest.setAdditionalAmount(niblReversalRequest.getAdditionalAmount());
        transactionRequest.setAmount(niblReversalRequest.getAmount());
        try {
            reversalUseCase.execute(prepareReversalRequestModel(transactionRequest));
            this.autoReversalRequestModel.getTransactionRepository().removeAutoReversal(this.autoReversal);
            return new AutoReversalResponseModel(Result.SUCCESS, "Auto Reversal Success");
        } catch (FinPosException e) {
            e.printStackTrace();
            if (!e.getMessage().equals(FinPosException.ExceptionType.UNABLE_TO_CONNECT.getMessage()) && !e.getMessage().equals(FinPosException.ExceptionType.READ_TIME_OUT.getMessage())) {
                if (this.autoReversal.getRetryCount() < RETRY_LIMIT) {
                    this.autoReversalRequestModel.getTransactionRepository().incrementAutoReversalRetryCount(this.autoReversal);
                } else {
                    this.incrementTransationIds();
                    this.autoReversalRequestModel.getTransactionRepository().changeAutoReversalStatus(this.autoReversal, AutoReversal.Status.INACTIVE);
                }
                return new AutoReversalResponseModel(Result.FAILURE, "Auto Reversal Failure");
            } else {
                var errorMessage = "";
                if (isFinPosExceptionTypeConnectionError(e)) {
                    if (e.getType() == FinPosException.ExceptionType.READ_TIME_OUT)
                        errorMessage = PosError.DEVICE_ERROR_TIMEOUT.getErrorMessage();
                    else
                        errorMessage = PosError.DEVICE_ERROR_SETTLEMENT_REQUEST_CONNECTION_ERROR.getErrorMessage();
                }
                return new AutoReversalResponseModel(Result.FAILURE, (errorMessage.isEmpty()) ? e.getMessage() : errorMessage);
            }
        }
    }

    private boolean isFinPosExceptionTypeConnectionError(FinPosException finPosException) {
        FinPosException.ExceptionType type = finPosException.getType();
        return (type == FinPosException.ExceptionType.UNABLE_TO_CONNECT ||
                type == FinPosException.ExceptionType.CONNECTION_ERROR ||
                type == FinPosException.ExceptionType.READ_TIME_OUT);
    }


    private void incrementTransationIds() {
        this.terminalRepository.incrementSystemTraceAuditNumber();
    }

    private ReversalRequestModel prepareReversalRequestModel(TransactionRequest transactionRequest) {
        return ReversalRequestModel.Builder.newInstance()
                .withApplicationRepository(this.autoReversalRequestModel.getApplicationRepository())
                .withPrinterService(this.autoReversalRequestModel.getPrinterService())
                .withTransactionRepository(this.autoReversalRequestModel.getTransactionRepository())
                .withTransactionRequest(transactionRequest)
                .withReadCardResponse(this.autoReversalRequestModel.getTransactionRepository().getAutoReversal().getReadCardResponse())
                .build();
    }
}
