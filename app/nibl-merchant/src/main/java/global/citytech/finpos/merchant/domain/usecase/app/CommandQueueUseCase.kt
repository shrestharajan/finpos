package global.citytech.finpos.merchant.domain.usecase.app

import global.citytech.finpos.merchant.domain.model.app.Command
import global.citytech.finpos.merchant.domain.model.app.Status
import global.citytech.finpos.merchant.domain.repository.app.CommandQueueRepository
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/20/2021.
 */
class CommandQueueUseCase(private val commandQueueRepository: CommandQueueRepository) {

    fun isQueueEmpty(): Boolean = commandQueueRepository.isQueueEmpty()

    fun hasActiveCommands(): Boolean = commandQueueRepository.hasActiveCommands()

    fun getCount(): Int = commandQueueRepository.getCount()

    fun getAllCommands(): MutableList<Command> = commandQueueRepository.getAllCommands()

    fun removeCommand(command: Command) {
        commandQueueRepository.removeCommand(command)
    }

    fun addCommand(command: Command) {
        commandQueueRepository.addCommand(command)
    }

    fun updateCommandStatus(command: Command, status: Status, remarks: String? = "") {
        commandQueueRepository.updateCommandStatus(command, status, remarks!!)
    }

    fun clear() {
        commandQueueRepository.clear()
    }
}