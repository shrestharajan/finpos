package global.citytech.finpos.processor.nibl.transaction.purchase;

import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finpos.nibl.iso8583.requestsender.purchase.ManualPurchaseRequestSender;
import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finpos.processor.nibl.transaction.template.ManualTransactionUseCase;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.utility.IsoMessageUtils;

/**
 * Created by Unique Shakya on 3/8/2021.
 */
public class ManualPurchaseUseCase extends ManualTransactionUseCase {

    public ManualPurchaseUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    protected String getTransactionTypeClassName() {
        return ManualPurchaseUseCase.class.getSimpleName();
    }


    @Override
    protected NIBLMessageSenderTemplate getRequestSender(RequestContext context) {
        return new ManualPurchaseRequestSender(new NIBLSpecInfo(), context);
    }

    @Override
    protected String getOriginalPosConditionCode() {
        return "00";
    }

    @Override
    protected PosEntryMode getOriginalPosEntryMode() {
        return PosEntryMode.MANUAL;
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.PURCHASE.getCode();
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy, String miniStatementData) {
        return null;
    }

    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy) {
        PurchaseResponseModel.Builder builder = PurchaseResponseModel.Builder.createDefaultBuilder();
        builder.isApproved(isApproved);
        builder.debugRequestMessage(isoMessageResponse.getDebugRequestString());
        builder.debugResponseMessage(isoMessageResponse.getDebugResponseString());
        builder.message(this.retrieveMessage(isoMessageResponse, isApproved));
        builder.stan(this.stan);
        builder.shouldPrintCustomerCopy(shouldPrintCustomerCopy);
        return builder.build();
    }

    @Override
    protected TransactionResponseModel prepareDeclinedTransactionResponse(String message,
                                                                          boolean shouldPrintReceipt) {
        return PurchaseResponseModel.Builder.createDefaultBuilder()
                .message(message)
                .isApproved(false)
                .shouldPrintCustomerCopy(shouldPrintReceipt)
                .stan(this.stan)
                .build();
    }

    @Override
    protected String getOriginalTransactionReferenceNumber() {
        return "";
    }

    @Override
    protected boolean isAuthorizationCompleted() {
        return true;
    }

    @Override
    protected void updateTransactionLogOfOriginalTransaction(boolean isApproved) {
        // do nothing
    }

    @Override
    protected boolean shouldIncrementInvoiceNumber(boolean isApproved) {
        return isApproved;
    }

    @Override
    protected TransactionLog retrieveTransactionLogToStore(boolean isApproved, IsoMessageResponse isoMessageResponse) {
        return TransactionLog.Builder.newInstance()
                .withStan(stan)
                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
                .withInvoiceNumber(invoiceNumber)
                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
                .withTransactionType(TransactionType.PURCHASE)
                .withOriginalTransactionType(TransactionType.PURCHASE)
                .withTransactionAmount(transactionRequest.getAmount())
                .withOriginalTransactionAmount(transactionRequest.getAmount())
                .withTransactionDate(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.LOCAL_DATE))
                .withTransactionTime(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.LOCAL_TIME))
                .withTransactionStatus(isApproved ? APPROVED : DECLINED)
                .withPosEntryMode(PosEntryMode.MANUAL)
                .withOriginalPosEntryMode(PosEntryMode.MANUAL)
                .withPosConditionCode(this.transactionIsoRequest.getPosConditionCode())
                .withOriginalPosConditionCode("")
                .withReconcileStatus("")
                .withReconcileTime("")
                .withReconcileDate("")
                .withReconcileBatchNo(batchNumber)
                .withReadCardResponse(readCardResponse)
                .withReceiptLog(this.receiptLog)
                .withAuthorizationCompleted(true)
                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                .withTransactionVoided(false)
                .withTipAdjusted(false)
                .withCurrencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .withVatInfo(transactionRequest.getVatInfo())
                .build();
    }
}
