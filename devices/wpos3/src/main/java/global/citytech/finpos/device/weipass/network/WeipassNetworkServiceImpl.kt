package global.citytech.finpos.device.weipass.network

import global.citytech.finpos.device.weipass.utils.WeipassConstant.NETWORK_DISABLED_SUCCESS
import global.citytech.finpos.device.weipass.utils.WeipassConstant.NETWORK_ENABLED_SUCCESS
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SETTING_APN_SUCCESS
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SETTING_DATA_TRANSMISSION_NETWORK_SUCCESS
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.network.NetworkService
import global.citytech.finposframework.hardware.network.NetworkType

/**
 * Created by Rishav Chudal on 6/7/20.
 */
class WeipassNetworkServiceImpl:
    NetworkService {
    override fun enableNetwork(network: NetworkType): DeviceResponse {
        return DeviceResponse(Result.SUCCESS, NETWORK_ENABLED_SUCCESS)
    }

    override fun disableNetwork(network: NetworkType): DeviceResponse {
        return DeviceResponse(Result.SUCCESS, NETWORK_DISABLED_SUCCESS)
    }

    override fun setDataTransmissionNetwork(network: NetworkType): DeviceResponse {
        return DeviceResponse(Result.SUCCESS, SETTING_DATA_TRANSMISSION_NETWORK_SUCCESS)
    }

    override fun setAPN(network: NetworkType): DeviceResponse {
        return DeviceResponse(Result.SUCCESS, SETTING_APN_SUCCESS)
    }
}