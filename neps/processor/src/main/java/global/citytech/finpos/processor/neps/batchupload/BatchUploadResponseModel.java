package global.citytech.finpos.processor.neps.batchupload;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.supports.UseCase;

/**
 * Created by Saurav Ghimire on 6/14/21.
 * sauravnghimire@gmail.com
 */


public class BatchUploadResponseModel implements UseCase.Response, global.citytech.finposframework.switches.batchupload.BatchUploadResponseParameter {
    private Result result;
    private String message;

    public BatchUploadResponseModel(Result result, String message) {
        this.result = result;
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }
}
