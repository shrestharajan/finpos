package global.citytech.finpos.nibl.iso8583.requestsender.refund;

import org.junit.Test;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.NIBLHostConfig;
import global.citytech.finpos.nibl.iso8583.MockValues;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;

import static org.junit.Assert.*;

/**
 * Created by Rishav Chudal on 10/16/20.
 */
public class PICCRefundRequestSenderTest {
    private Logger logger = Logger.getLogger(PICCRefundRequestSenderTest.class.getName());

    @Test
    public void send() {
        try {
            String terminalID = "99991002";
            String acquirerID = "405635000000002";
            TerminalInfo terminalInfo = new TerminalInfo(
                    terminalID,
                    acquirerID,"",
                    "Citytech",
                    "Kamaladi"
            );
            HostInfo primaryHost = NIBLHostConfig.getPrimaryConfig();
            HostInfo secondaryHost = NIBLHostConfig.getSecondaryConfig();
            ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);
            RequestContext context = new RequestContext(
                    "000405",
                    terminalInfo,
                    connectionParam
            );

            RefundRequest request = new RefundRequest();
            request.setTransactionAmount(105.0);
            request.setPosConditionCode("00");
            request.setOriginalRetrievalReferenceNumber("000000000001");
            request.setPinBlock("c247220c967c1d");
            request.setInvoiceNumber("000090");
            request.setEntryMode(PosEntryMode.ICC);
            request.setTrack2Data(MockValues.TRACK_2_DATA);
            request.setCardSequenceNumber("01");
            request.setEmvData("82023C009F02060000000000289F03060000000000004F07A0000000031010500B56495341204352454449549F120B56495341204352454449549F360200CA9F26085C4637D46AFA9A359F2701809F34034203008407A00000000310109F100706010A03A0A8009F1E0831313130303837379F3303E0F8C89F350122950508000400009F1A0205245F2A0205249A032008289C01009F370460C801959F090200969F4104000012345F340101");

            context.setRequest(request);

            PICCRefundRequestSender sender = new PICCRefundRequestSender(
                    new NIBLSpecInfo(),
                    context
            );

            IsoMessageResponse response = sender.send();
            assertNotNull(response);

        } catch (FinPosException e) {
            this.logger.log(e.getDetailMessage());
            this.logger.log(e.getMessage());
        }
    }
}