package global.citytech.finpos.merchant.presentation.model.pinchange

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

data class PinChangeRequestItem(
    val transactionType: TransactionType? = null,
    val amount: BigDecimal? = null
)