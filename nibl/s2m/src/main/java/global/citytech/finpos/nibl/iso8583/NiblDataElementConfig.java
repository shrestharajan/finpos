package global.citytech.finpos.nibl.iso8583;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.iso8583.DataConfig;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.DataElementConfig;
import global.citytech.finposframework.iso8583.DataElementConfigs;
import global.citytech.finposframework.iso8583.EncodingType;
import global.citytech.finposframework.iso8583.LengthConfig;
import global.citytech.finposframework.iso8583.PaddingType;


/**
 * Configuration data element of NIBL.
 * Note: if it is <p>BCD</p> encoded , the length should be half of given length.
 * @author : Surajchhetry
 * Date: 2/19/20 Time: 5:28 PM
 *
 */
public class NiblDataElementConfig extends DataElementConfigs {

    @Override
    protected List<DataElementConfig> config() {
        List<DataElementConfig> deConfig = new ArrayList<>();
        deConfig.add(new DataElementConfig(
                DataElement.PRIMARY_ACCOUNT_NUMBER,
                new DataConfig(DataConfig.DataType.NUMERIC,EncodingType.BCD),
                new LengthConfig(LengthConfig.LengthType.LLVAR,EncodingType.BCD),
                PaddingType.POSTFIX_WITH_CHAR_F
        ));
        deConfig.add(new DataElementConfig(
                DataElement.PROCESSING_CODE,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.BCD),
                new LengthConfig(6, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.AMOUNT,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.BCD),
                new LengthConfig(12, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.SYSTEM_TRACE_AUDIT_NUMBER,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.BCD),
                new LengthConfig(6, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.LOCAL_TIME,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.BCD),
                new LengthConfig(6, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.LOCAL_DATE,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.BCD),
                new LengthConfig(4, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.PAN_EXPIRE_DATE,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.BCD),
                new LengthConfig(4, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.POS_ENTRY_MODE,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.BCD),
                new LengthConfig(4, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.APPLICATION_PAN_SEQUENCE_NUMBER,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.BCD),
                new LengthConfig(4, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.FUNCTION_CODE_OR_NETWORK_INTERNATIONAL_IDENTIFIER,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.BCD),
                new LengthConfig(4, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.BCD),
                new LengthConfig(2, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.TRACK_II_DATA,
                new DataConfig(DataConfig.DataType.ALPHA, EncodingType.BCD,37),
                new LengthConfig(LengthConfig.LengthType.LLVAR,EncodingType.BCD),
                PaddingType.POSTFIX_WITH_CHAR_F
        ));
        deConfig.add(new DataElementConfig(
                DataElement.RETRIEVAL_REFERENCE_NUMBER,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(24, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(12, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.RESPONSE_CODE,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.ASCII),
                new LengthConfig(4, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.CARD_ACCEPTOR_TERMINAL_ID,
                new DataConfig(DataConfig.DataType.ALPHA_NUMERIC,EncodingType.ASCII),
                new LengthConfig(16, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.CARD_ACCEPTOR_ACQUIRER_ID,
                new DataConfig(DataConfig.DataType.ALPHA,EncodingType.ASCII),
                new LengthConfig(30, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.ADDITIONAL_RESPONSE_DATA,
                new DataConfig(DataConfig.DataType.ALPHA,EncodingType.ASCII),
                new LengthConfig(LengthConfig.LengthType.LLLVAR, EncodingType.BCD)
        ));

        deConfig.add(new DataElementConfig(
                DataElement.ADDITIONAL_DATA_PRIVATE,
                new DataConfig(DataConfig.DataType.ALPHA, EncodingType.ASCII),
                new LengthConfig(LengthConfig.LengthType.LLLVAR, EncodingType.BCD)
        ));

        deConfig.add(new DataElementConfig(
                DataElement.CURRENCY_CODE,
                new DataConfig(DataConfig.DataType.NUMERIC, EncodingType.BCD),
                new LengthConfig(2, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.PIN_BLOCK,
                new DataConfig(DataConfig.DataType.ALPHA,EncodingType.BCD),
                new LengthConfig(16, LengthConfig.LengthType.FIXED)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.ADDITIONAL_AMOUNT,
                new DataConfig(DataConfig.DataType.ALPHA,EncodingType.ASCII),
                new LengthConfig(LengthConfig.LengthType.LLLVAR, EncodingType.BCD)
        ));
        deConfig.add(new DataElementConfig(
                DataElement.ICC_SYSTEM_RELATED_DATA,
                new DataConfig(DataConfig.DataType.ALPHA,EncodingType.BCD),
                new LengthConfig(LengthConfig.LengthType.LLLVAR,EncodingType.BCD)
        ));

        deConfig.add(new DataElementConfig(
                DataElement.PRIVATE_USE_FIELD_60,
                new DataConfig(DataConfig.DataType.ALPHA),
                new LengthConfig(LengthConfig.LengthType.LLLVAR,EncodingType.BCD)
        ));
        // TODO : In case of Purchase field 62 type is ASCII but in other case not
        deConfig.add(new DataElementConfig(
                DataElement.PRIVATE_USE_FIELD_62,
                new DataConfig(DataConfig.DataType.ALPHA),
                new LengthConfig(LengthConfig.LengthType.LLLVAR,EncodingType.BCD)
        ));

        deConfig.add(new DataElementConfig(
                DataElement.PRIVATE_USE_FIELD_63,
                new DataConfig(DataConfig.DataType.ALPHA,EncodingType.ASCII),
                new LengthConfig(LengthConfig.LengthType.LLLVAR,EncodingType.BCD)
        ));

        return deConfig;
    }
/*
    private List<DataElementConfig> setupDataElement() {


        this.dataElements.put(
                1, new DataElement(1, 64, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                2, new DataElement(2, 19, DataElement.DataType.ALPHA, DataElement.LengthType.LLVAR));
        this.dataElements.put(
                3, new DataElement(3, 3, DataElement.DataType.BINARY, DataElement.LengthType.FIXED));
        this.dataElements.put(
                4, new DataElement(4, 16, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                5, new DataElement(5, 12, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));

        this.dataElements.put(
                6, new DataElement(6, 12, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                7, new DataElement(7, 10, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                8, new DataElement(8, 8, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                9, new DataElement(9, 8, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                10, new DataElement(10, 8, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                11, new DataElement(11, 3, DataElement.DataType.BINARY, DataElement.LengthType.FIXED));
        this.dataElements.put(
                12, new DataElement(12, 14, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                13, new DataElement(13, 4, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                14, new DataElement(14, 4, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                15, new DataElement(15, 4, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                16, new DataElement(16, 4, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                17, new DataElement(17, 8, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                18, new DataElement(18, 4, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                19, new DataElement(19, 3, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                20, new DataElement(20, 3, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                21, new DataElement(21, 3, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                22, new DataElement(22, 3, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                23, new DataElement(23, 3, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                24, new DataElement(24, 2, DataElement.DataType.BINARY, DataElement.LengthType.FIXED));
        this.dataElements.put(
                25, new DataElement(25, 2, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));

        this.dataElements.put(
                26, new DataElement(26, 2, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                27, new DataElement(27, 1, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                28, new DataElement(28, 8, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                29, new DataElement(29, 8, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                30, new DataElement(30, 8, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                31, new DataElement(31, 8, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                32, new DataElement(32, 11, DataElement.DataType.ALPHA, DataElement.LengthType.LLVAR));
        this.dataElements.put(
                33, new DataElement(33, 11, DataElement.DataType.ALPHA, DataElement.LengthType.LLVAR));
        this.dataElements.put(
                34, new DataElement(34, 28, DataElement.DataType.ALPHA, DataElement.LengthType.LLVAR));
        this.dataElements.put(
                35, new DataElement(35, 37, DataElement.DataType.ALPHA, DataElement.LengthType.LLVAR));
        this.dataElements.put(
                36, new DataElement(36, 104, DataElement.DataType.ALPHA, DataElement.LengthType.LLLVAR));
        this.dataElements.put(
                37, new DataElement(37, 12, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                38, new DataElement(38, 6, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                39, new DataElement(39, 2, DataElement.DataType.BINARY, DataElement.LengthType.FIXED));
        this.dataElements.put(
                40, new DataElement(40, 3, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                41, new DataElement(41, 8, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                42, new DataElement(42, 15, DataElement.DataType.ALPHA, DataElement.LengthType.FIXED));
        this.dataElements.put(
                53, new DataElement(53, 16, DataElement.DataType.BINARY, DataElement.LengthType.LLVAR));
        this.dataElements.put(
                62, new DataElement(62, 16, DataElement.DataType.BINARY, DataElement.LengthType.LLVAR));
    /*
    this.dataElements.put(3,new DataElement(43, 40, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(44, 25, DataElement.DataType.LLVAR));
    this.dataElements.put(3,new DataElement(45, 76, DataElement.DataType.LLVAR));
    this.dataElements.put(3,new DataElement(46, 300, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(47, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(48, 300, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(49, 3, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(50, 3, DataElement.DataType.ALPHA));

    this.dataElements.put(1, new DataElement(51, 3, DataElement.DataType.ALPHA));
    this.dataElements.put(2,new DataElement(52, 16, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(53, 18, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(54, 120, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(55, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(56, 43, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(57, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(58, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(59, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(60, 7, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(61, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(62, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(63, 999, DataElement.DataType.LLLVAR));
    this.dataElements.put(3,new DataElement(64, 16, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(65, 64, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(66, 1, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(67, 2, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(68, 3, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(69, 3, DataElement.DataType.ALPHA));
    this.dataElements.put(70,new DataElement(70, 3, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(71, 4, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(72, 999, DataElement.DataType.LLLVAR));

    this.dataElements.put(3,new DataElement(73, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(74, 10, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(75, 10, DataElement.DataType.ALPHA));

    this.dataElements.put(1, new DataElement(76, 10, DataElement.DataType.ALPHA));
    this.dataElements.put(2,new DataElement(77, 10, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(78, 10, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(80, 10, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(81, 10, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(82, 12, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(83, 12, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(84, 12, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(85, 12, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(86, 15, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(87, 15, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(88, 15, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(89, 15, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(90, 42, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(91, 1, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(92, 2, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));

    this.dataElements.put(1, new DataElement(1, 64, DataElement.DataType.ALPHA));
    this.dataElements.put(2,new DataElement(2, 19, DataElement.DataType.LLVAR));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));

    this.dataElements.put(1, new DataElement(1, 64, DataElement.DataType.ALPHA));
    this.dataElements.put(2,new DataElement(2, 19, DataElement.DataType.LLVAR));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));
    this.dataElements.put(3,new DataElement(3, 6, DataElement.DataType.ALPHA));


    } */

}