package global.citytech.neps.domain.model

import android.content.ContentValues
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "host")
data class Host(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_ID)
    var id: String,
    @ColumnInfo(name = COLUMN_CONNECTION_TIME_OUT)
    var connectionTimeout: String? = null,
    @ColumnInfo(name = COLUMN_IP)
    var ip: String? = null,
    @ColumnInfo(name = COLUMN_NII)
    var nii: String? = null,
    @ColumnInfo(name = COLUMN_ORDER)
    var order: Int? = null,
    @ColumnInfo(name = COLUMN_PORT)
    var port: String? = null,
    @ColumnInfo(name = COLUMN_RETRY_LIMIT)
    var retryLimit: String? = null
) {

    companion object {

        const val COLUMN_ID = "id"
        const val COLUMN_CONNECTION_TIME_OUT = "connection_time_out"
        const val COLUMN_IP = "ip"
        const val COLUMN_NII = "nii"
        const val COLUMN_ORDER = "order"
        const val COLUMN_PORT = "port"
        const val COLUMN_RETRY_LIMIT = "retry_limit"

        fun fromContentValues(values: ContentValues): Host {
            values.let {
                val host = Host(id= values.getAsString(COLUMN_ID))
                if (it.containsKey(COLUMN_ID)){
                    host.id = it.getAsString(COLUMN_ID)
                }
                if (it.containsKey(COLUMN_CONNECTION_TIME_OUT))
                    host.connectionTimeout = it.getAsString(COLUMN_CONNECTION_TIME_OUT)
                if (it.containsKey(COLUMN_IP))
                    host.ip = it.getAsString(COLUMN_IP)
                if (it.containsKey(COLUMN_NII))
                    host.nii = it.getAsString(COLUMN_NII)
                if (it.containsKey(COLUMN_ORDER))
                    host.order = it.getAsInteger(COLUMN_ORDER)
                if (it.containsKey(COLUMN_PORT))
                    host.port = it.getAsString(COLUMN_PORT)
                if (it.containsKey(COLUMN_RETRY_LIMIT))
                    host.retryLimit = it.getAsString(COLUMN_RETRY_LIMIT)
                return host
            }
        }

    }
}
