package global.citytech.finposframework.switches.posmode;

import global.citytech.finposframework.switches.RequestParameter;

/**
 * Created by Unique Shakya on 1/7/2021.
 */
public interface PosModeRequestParameter extends RequestParameter {
}
