package global.citytech.finpos.merchant.presentation.purchase


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.core.util.Predicate
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import global.citytech.common.Constants
import global.citytech.common.Constants.CARD
import global.citytech.common.Constants.FONEPAY
import global.citytech.common.Constants.MOBILEPAY
import global.citytech.common.Constants.NEPALPAY
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityBaseTransactionBinding
import global.citytech.finpos.merchant.presentation.alertdialogs.PosEntrySelectionDialog
import global.citytech.finpos.merchant.presentation.dashboard.temp.DashActivity
import global.citytech.finpos.merchant.presentation.dynamicqr.QrType
import global.citytech.finpos.merchant.presentation.dynamicqr.fonepayqrsale.DynamicFonepayQrActivity
import global.citytech.finpos.merchant.presentation.dynamicqr.nqrsale.DynamicNQrActivity
import global.citytech.finpos.merchant.presentation.mobilenfcpayment.MobileNfcPaymentActivity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.purchase.ManualPurchaseRequestItem
import global.citytech.finpos.merchant.presentation.payment.PaymentSdkSingleton
import global.citytech.finpos.merchant.presentation.purchase.PaymentMethodAdapter.PaymentItemClickListener
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.AppUtility.checkBatteryLevel
import global.citytech.finpos.merchant.utils.AppUtility.checkInternetConnection
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.usecases.TransactionType
import kotlinx.android.synthetic.main.activity_base_transaction.*
import java.io.File
import java.math.BigDecimal
import java.text.SimpleDateFormat

/**
 * Created by Unique Shakya on 8/26/2020.
 */
class PurchaseActivity :
    BaseTransactionActivity<ActivityBaseTransactionBinding, PurchaseViewModel>(),
    PosEntrySelectionDialog.QrListener, PosEntrySelectionDialog.MobilePayListener,
    PaymentItemClickListener {

    private val logger = Logger(PurchaseActivity::class.java)
    private lateinit var viewModel: PurchaseViewModel
    private var cardNumber: String = ""
    private var expiryDate: String = ""
    private var cvv: String = ""
    private val adapter = PaymentMethodAdapter(this, mutableListOf(), this)
    private var touchHelper: ItemTouchHelper? = null
    private var configurationItem: ConfigurationItem? = null
    var batLevel: Int = 0
    private lateinit var batteryBroadcast: BroadcastReceiver
    private var isCharging: Boolean = false

    private var intentFilter: IntentFilter = IntentFilter()

    //
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel()
        isFromPurchasePage = true
        initBaseViews()
        initObservers()
        getViewModel().initQrOptionConfig()
        getViewModel().initMobilePayOptionConfig()
        initTransaction()
        viewModel.checkForAutoReversal()

        val batteryManager = getSystemService(BATTERY_SERVICE) as BatteryManager
        batLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
        isCharging()
    }

    override fun onResume() {
        super.onResume()
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED)
        try {
            registerReceiver(batteryBroadcast, intentFilter)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onPause() {
        super.onPause()
        try {
            unregisterReceiver(batteryBroadcast)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun initTransaction() {
        PaymentSdkSingleton.getInstance().isFromPaymentSdk =
            intent.getBooleanExtra(Constants.EXTRA_FROM_CHECKOUT, false)
        isEmiTransaction = intent.getBooleanExtra(Constants.EXTRA_EMI_TRANSACTION, false)
        if (PaymentSdkSingleton.getInstance().isFromPaymentSdk) {
            amount = intent.getDoubleExtra(Constants.EXTRA_AMOUNT, 0.0).toBigDecimal()
            vatInfo = intent.getStringExtra(Constants.EXTRA_VAT_INFO)
            intent?.getStringExtra(Constants.EXTRA_ADDITIONAL_DATA)?.let {
                getViewModel().checkoutAdditionalData = it
            }
            displayAmount = amount
            if (amount == BigDecimal.ZERO)
                this.startAmountActivity()
            else {
                showHeaderView(displayAmount)
                viewModel.getConfigurationItem()
            }
        } else {
            this.startAmountActivity()
        }
    }

    private fun initObservers() {
        val data = getViewModel().getPaymentInitiatorItems()
        adapter.update(data)

        this.viewModel.configurationItem.observe(this, Observer {
            configurationItem = it
            if (isManualCardEntry) {
                purchase(it)
                return@Observer
            }
            data.add(PaymentItems(CARD, "", R.drawable.ic_card_black))
            val updatedData = data.distinctBy { it.paymentName }
            adapter.update(updatedData as MutableList<PaymentItems>)
            showPaymentItemsAvailableDialog()
        })

        this.viewModel.showQrOption.observe(this, Observer {
            var addFonePayItem = 0
            var addNepalPayItem = 0
            for (i in it.indices) {
                if (QrType.NQR == QrType.getByCodes(it[i].name)) {
                    data.add(PaymentItems(NEPALPAY, it[i].logo, R.drawable.ic_nepal_pay_qr))
                    val updatedData = data.distinctBy { it.paymentName }
                    adapter.update(updatedData as MutableList<PaymentItems>)
                    addNepalPayItem++
                }

                if (QrType.FONEPAY == QrType.getByCodes(it[i].name)) {
                    //comment these lines to show fonepay payment method without checking login state
                    if (AppUtility.isAppVariantNIBL()) {
                        data.add(PaymentItems(FONEPAY, it[i].logo, R.drawable.ic_fone_pay))
                        val updatedData = data.distinctBy { it.paymentName }
                        adapter.update(updatedData as MutableList<PaymentItems>)
                        addFonePayItem++
                    } else {
                        if (getViewModel().localDataUseCase.getFonePayLoginState(false)) {
                            data.add(PaymentItems(FONEPAY, it[i].logo, R.drawable.ic_fone_pay))
                            val updatedData = data.distinctBy { it.paymentName }
                            adapter.update(updatedData as MutableList<PaymentItems>)
                            addFonePayItem++
                        }
                    }
                }
            }

            if (addNepalPayItem < 1) {
                removePaymentItemFromList(data) { name: PaymentItems -> name.paymentName == NEPALPAY }
            }

            if (addFonePayItem < 1) {
                removePaymentItemFromList(data) { name: PaymentItems -> name.paymentName == FONEPAY }
            }
        })

        this.viewModel.showMobilePayOption.observe(this) {
            if (it) {
                data.add(PaymentItems(MOBILEPAY, "", R.drawable.ic_contactless))
                val updatedData = data.distinctBy { it.paymentName }
                adapter.update(updatedData as MutableList<PaymentItems>)
            } else {
                removePaymentItemFromList(data) { name: PaymentItems -> name.paymentName == MOBILEPAY }
            }
        }

        initBaseLiveDataObservers()

        this.viewModel.doQrTransaction.observe(this, Observer {

        })

        this.viewModel.doLoadNfcMobilePage.observe(this, Observer { })

        this.viewModel.doReturnToPaymentItemSelectionPage.observe(this) {
            showPaymentItemsAvailableDialog()
        }

        this.viewModel.autoReversalPresent.observe(this, Observer { reversal ->
            if (reversal) {
                showReversalDailog()
            } else {
                configurationItem?.let { purchase(it) }
            }
        })
    }

    fun showReversalDailog() {
        showNormalMessage(
            MessageConfig.Builder()
                .message("Reversal need to be performed. Redirect to Dashboard")
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    startActivity(Intent(this, DashActivity::class.java))
                    //checkForCardPresent()
                })
    }

    private fun purchase(it: ConfigurationItem) {
        hidePaymentItemsAvailableDialog()
        tv_payment_title.text = CARD
        tv_transaction_type.visibility = View.VISIBLE
        this.showProgressScreen("Initializing Transaction...")
        if (isManualCardEntry) {
            this.viewModel.doManualPurchase(
                configurationItem = it,
                manualPurchaseRequestItem = prepareManualPurchaseRequestItem()
            )
        } else {
            this.viewModel.purchase(
                it,
                amount,
                getTransactionType(),
                isEmiTransaction,
                emiInfo,
                vatInfo
            )
        }
    }

    private fun prepareManualPurchaseRequestItem(): ManualPurchaseRequestItem {
        return ManualPurchaseRequestItem(
            amount, getTransactionType(), cardNumber, expiryDate, cvv, vatInfo
        )
    }

    override fun getBindingVariable(): Int = BR.purchase_view_model

    override fun getViewModel(): PurchaseViewModel =
        ViewModelProviders.of(this)[PurchaseViewModel::class.java]

    override fun onPositiveButtonClick() {
        this.showProgressScreen("Printing Customer Copy..")
        this.viewModel.printCustomerCopy()
    }

    override fun onNegativeButtonClick() {
        checkForCardPresent()
    }

    override fun onManualButtonClicked() {
        this.viewModel.shouldDispose = true
        Log.i("remove", "1")
        hidePosEntrySelectionDialog()
        this.viewModel.doCleanUp(BaseTransactionViewModel.Task.MANUAL_TRANSACTION)
        Log.i("remove", "2")
        this.isManualCardEntry = true
    }

    override fun onCancelButtonClicked() {
        this.viewModel.shouldDispose = true
        updatePaymentCancelled()
        this.viewModel.doCleanUp(BaseTransactionViewModel.Task.CANCEL_CARD_TRANSACTION)
    }

    override fun onMobilePayButtonClicked() {
        this.viewModel.shouldDispose = true
        this.viewModel.doCleanUp(BaseTransactionViewModel.Task.MOBILE_TRANSACTION)
        hidePosEntrySelectionDialog()
    }

    override fun onQrButtonClicked() {
        this.viewModel.shouldDispose = true
        this.viewModel.doCleanUp(BaseTransactionViewModel.Task.QR_TRANSACTION)
        hidePosEntrySelectionDialog()
    }

    override fun getTransactionType(): TransactionType = TransactionType.PURCHASE

    override fun getTransactionViewModel(): BaseTransactionViewModel = this.viewModel

    override fun onAmountActivityResult(data: Intent?) {
        data?.let {
            val amountInString =
                data.getStringExtra(
                    if (isEmiTransaction) Constants.EMI_EXTRA_CONFIRM_AMOUNT else getString(
                        R.string.intent_confirmed_amount
                    )
                )
            amount = amountInString.toBigDecimal()
            viewModel.getConfigurationItem()
        }
    }

    override fun onManualActivityResult(data: Intent?) {
        data?.let {
            this.cardNumber = data.getStringExtra(getString(R.string.title_card_number))
            this.expiryDate = data.getStringExtra(getString(R.string.title_expiry_date))
            this.cardNumber = data.getStringExtra(getString(R.string.title_card_number))!!
            this.expiryDate = data.getStringExtra(getString(R.string.title_expiry_date))!!
            this.cvv = data.getStringExtra(getString(R.string.title_cvv))!!
            this.viewModel.getConfigurationItem()
        }
    }

    override fun getLayout() = R.layout.activity_base_transaction

    override fun initBaseViews(isFromPreAuthPage:Boolean) {
        initializePaymentItemsDialog()
        super.initBaseViews(isFromPreAuthPage)
    }


    override fun onItemClicked(paymentItems: PaymentItems) {

        when (paymentItems.paymentName) {

            CARD -> {
                if (checkBatteryLevel(
                        batLevel,
                        application,
                        container,
                        isCharging
                    ) && checkInternetConnection(application, container)
                )
                //configurationItem?.let { purchase(it) }
                    viewModel.checkForAutoReversal()
            }

            MOBILEPAY -> {

                if (checkBatteryLevel(
                        batLevel,
                        application,
                        container,
                        isCharging
                    ) && checkInternetConnection(application, container)
                ) {
                    onMobilePayButtonClicked()
                    val intent = Intent(this, MobileNfcPaymentActivity::class.java)
                    intent.putExtra(MobileNfcPaymentActivity.EXTRA_TRANSACTION_AMOUNT, amount)
                    startActivityForResult(intent, MobileNfcPaymentActivity.REQUEST_CODE)
                }

            }

            NEPALPAY -> {
                onQrButtonClicked()
                val intent = Intent(this, DynamicNQrActivity::class.java)
                intent.putExtra(DynamicNQrActivity.EXTRA_TRANSACTION_AMOUNT, amount)
                startActivityForResult(intent, DynamicNQrActivity.REQUEST_CODE)
            }

            FONEPAY -> {
                if (checkBatteryLevel(
                        batLevel,
                        application,
                        container,
                        isCharging
                    ) && checkInternetConnection(
                        application,
                        container
                    )
                ) {
                    onQrButtonClicked()
                    val intent = Intent(this, DynamicFonepayQrActivity::class.java)
                    intent.putExtra(DynamicFonepayQrActivity.EXTRA_TRANSACTION_AMOUNT, amount)
                    startActivityForResult(intent, DynamicFonepayQrActivity.REQUEST_CODE)
                }
            }
        }
    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder) {
        touchHelper?.startDrag(viewHolder)
    }

    override fun onReleaseItem(paymentItems: MutableList<PaymentItems>) {
        getViewModel().savePaymentInitiatorItems(paymentItems)
    }

    private fun initializePaymentItemsDialog() {
        val callback: ItemTouchHelper.Callback = ItemMoveCallbackListener(adapter)
        touchHelper = ItemTouchHelper(callback)
        touchHelper?.attachToRecyclerView(rv_payment_method_selection)
        val layoutManager = LinearLayoutManager(this)
        rv_payment_method_selection.layoutManager = layoutManager
        rv_payment_method_selection.adapter = adapter
    }

    private fun <T> removePaymentItemFromList(list: ArrayList<T>, predicate: Predicate<T>) {
        list.filter { predicate.test(it) }.forEach { list.remove(it) }
    }

    fun isCharging() {
        batteryBroadcast = object : BroadcastReceiver() {
            override fun onReceive(contxt: Context?, intent: Intent?) {
                val status = intent?.getIntExtra(BatteryManager.EXTRA_STATUS, -1)
                isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING
            }
        }
    }
}
