package global.citytech.finpos.neps.iso8583.requestsender.authcompletion;


import java.math.BigDecimal;

import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;

/**
 * Created by Saurav Ghimire on 1/21/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class AuthCompletionIsoRequest extends TransactionIsoRequest {

    private String originalRRN;

    public String getOriginalRRN() {
        return originalRRN;
    }


    public static final class Builder {
        private String pan;
        private BigDecimal transactionAmount;
        private String track2Data;
        private String additionalData;
        private String currencyCode;
        private String pinBlock;
        private String emvData;
        private boolean isFallbackFromIccToMag;
        private String localDateTime;
        private String originalRRN;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public Builder localDateTime(String localDataTime) {
            this.localDateTime = localDataTime;
            return this;
        }

        public Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public Builder additionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder pinBlock(String pinBlock) {
            this.pinBlock = pinBlock;
            return this;
        }

        public Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public Builder originalRRN(String originalRRN) {
            this.originalRRN = originalRRN;
            return this;
        }

        public Builder fallBackFromIccToMag(boolean isFallbackFromIccToMag) {
            this.isFallbackFromIccToMag = isFallbackFromIccToMag;
            return this;
        }

        public AuthCompletionIsoRequest build() {
            AuthCompletionIsoRequest authCompletionIsoRequest = new AuthCompletionIsoRequest();
            authCompletionIsoRequest.pinBlock = this.pinBlock;
            authCompletionIsoRequest.currencyCode = this.currencyCode;
            authCompletionIsoRequest.pan = this.pan;
            authCompletionIsoRequest.track2Data = this.track2Data;
            authCompletionIsoRequest.transactionAmount = this.transactionAmount;
            authCompletionIsoRequest.emvData = this.emvData;
            authCompletionIsoRequest.additionalData = this.additionalData;
            authCompletionIsoRequest.isFallbackFromIccToMag = this.isFallbackFromIccToMag;
            authCompletionIsoRequest.localDateTime = this.localDateTime;
            authCompletionIsoRequest.originalRRN = this.originalRRN;
            return authCompletionIsoRequest;
        }
    }
}
