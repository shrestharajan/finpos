package global.citytech.finpos.device.qualcomm

import android.content.Context
import global.citytech.finpos.device.qualcomm.emv.emvkeys.FeitianEMVKeysServiceImpl
import global.citytech.finposframework.hardware.DeviceFactory
import global.citytech.finposframework.hardware.DeviceServiceType
import global.citytech.finposframework.hardware.common.DeviceService
import global.citytech.finposframework.notifier.Notifier

/**
 * Created by Rishav Chudal on 4/5/21.
 */
class DeviceQualcommFactory: DeviceFactory {

    override fun getDevice(
        context: Any?,
        deviceServiceType: DeviceServiceType,
        notifier: Notifier
    ): DeviceService? = QualcommFactory.getService(context as Context, deviceServiceType, notifier)
}