package global.citytech.finposframework.switches.refund;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 11/3/2020.
 */
public interface ManualRefundRequester<T extends RefundRequestParameter, E extends RefundResponseParameter>
        extends Request<T, E> {
}
