package global.citytech.finpos.device.qualcomm.emv.aid

import android.content.Context
import com.ftpos.library.smartpos.bean.CUnionPayBean
import com.ftpos.library.smartpos.device.Device
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.emv.EmvTags
import com.ftpos.library.smartpos.emv.IActionFlag
import global.citytech.common.extensions.defaultOrEmptyValue
import global.citytech.common.extensions.isNullOrEmptyOrBlank
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finpos.device.qualcomm.utils.FeitianUtils
import global.citytech.finposframework.hardware.utility.DeviceApiConstants
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.TRANSACTION_TYPE_PURCHASE_SALE
import global.citytech.finposframework.hardware.emv.aid.AidParameters
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import global.citytech.finposframework.utility.AmountUtils

/**
 * Created by Rishav Chudal on 4/5/21.
 * QPBOC Kernel - QuickPass - UPI Contactless Kernel - Kernel 07
 */
class FeitianQPBOCKernelConfigHandler constructor(
    private val context: Context,
    private val emv: Emv,
    private val aidParameters: AidParameters,
    private val emvParameters: EmvParametersRequest,
    private val supportedTransactionTypes: List<String>
) : FeitianLogger.Listener {

    private var finalResult = 0
    private val logger = FeitianLogger(this)
    private val device: Device = Device.getInstance(context.applicationContext)

    fun injectQPBOCKernelConfigurations(): Int {
        var successCount = 0
        supportedTransactionTypes.forEach {
            injectKernelConfigurationsForTransType(it)
        }
        if (finalResult == 0) {
            successCount++
        }
        return successCount
    }

    private fun injectKernelConfigurationsForTransType(transactionType: String) {
        val configurationsByteArray = prepareQPBOCKernelConfigurationsInByteArray(
            transactionType
        )
        val resultInjectCLParams = emv.manageEmvclAppParameters(
            IActionFlag.ADD,
            configurationsByteArray
        )
        logger.logWithClassName(
            "QPBOC kernel config injection result ::: ".plus(resultInjectCLParams)
        )
        finalResult += resultInjectCLParams
    }

    private fun prepareQPBOCKernelConfigurationsInByteArray(
        transactionType: String
    ): ByteArray? {
        logger.logWithClassName("***** Contactless Params *****")
        val cUnionPayBean = CUnionPayBean()

        val aid9F06 = aidParameters.aid.defaultOrEmptyValue()
        logger.debugWithClassName("AID ::: ".plus(aid9F06))
        cUnionPayBean.aiD_9F06 = aid9F06

        val appSelectionIndicator =
            FeitianConstants.DO_NOT_CHECK_FLAG //hardcoded as the AID parameters don't have this parameter
        logger.debugWithClassName("App Selection Indicator ::: ".plus(appSelectionIndicator))
        cUnionPayBean.asI_1F14 = appSelectionIndicator

        var transType = transactionType
        if (transType.isNullOrEmptyOrBlank()) {
            transType = TRANSACTION_TYPE_PURCHASE_SALE
        }
        logger.debugWithClassName("Transaction Type 9C ::: ".plus(transType))
        cUnionPayBean.transType_9C = transType

        val kernelId =
            DeviceApiConstants.QPBOC_KERNEL_ID //hardcoded as the AID parameters don't have this parameter
        this.logger.debugWithClassName("Kernel ID ::: ".plus(kernelId))
        cUnionPayBean.kernelID_1F60 = kernelId

        val acquirerId = aidParameters.acquirerId.defaultOrEmptyValue()
        logger.debugWithClassName("Acquirer Id ::: ".plus(acquirerId))
        cUnionPayBean.acquirerID_9F01 = acquirerId

        val ifdSerialNumber = FeitianUtils.retrieveDeviceSerialNumber(device)
        logger.debugWithClassName("IFD Serial Number ::: ".plus(ifdSerialNumber))
        cUnionPayBean.ifdSerial_9F1E = ifdSerialNumber

        val appVersionNumber: String = aidParameters.terminalAidVersion.defaultOrEmptyValue()
        logger.debugWithClassName("Application Version Number ::: ".plus(appVersionNumber))
        cUnionPayBean.appVersionNumber_9F09 = appVersionNumber

        val terminalType = emvParameters.terminalType.defaultOrEmptyValue()
        logger.debugWithClassName("Terminal Type ::: ".plus(terminalType))
        cUnionPayBean.terminalType_9F35 = terminalType

        var statusCheck = ""
        logger.debugWithClassName("Status Check Flag ::: ".plus(statusCheck))
        if (statusCheck.isNullOrEmptyOrBlank()) {
            statusCheck = FeitianConstants.DO_NOT_CHECK_FLAG
        }
        logger.debugWithClassName("Formatted Status Check Flag ::: ".plus(statusCheck))
        cUnionPayBean.statusCheck_1F32 = statusCheck

        val zeroAmountAllowed = FeitianConstants.CHECK_FLAG //Setting by default
        logger.debugWithClassName("Zero amount allowed ::: ".plus(zeroAmountAllowed))
        cUnionPayBean.zeroAmountAllowed_1F33 = zeroAmountAllowed

        val transCurrencyCode = emvParameters.transactionCurrencyCode.defaultOrEmptyValue()
        logger.debugWithClassName("Transaction Currency Code ::: ".plus(transCurrencyCode))
        cUnionPayBean.transCurrencyCode_5F2A = transCurrencyCode

        val ttq = aidParameters.ttq.defaultOrEmptyValue()
        logger.debugWithClassName("Terminal Transaction Qualifier (TTQ) ::: ".plus(ttq))
        cUnionPayBean.terminalTransactionQualifier_9F66 = ttq

        var contactlessFloorLimit = aidParameters.contactlessFloorLimit.defaultOrEmptyValue()
        logger.debugWithClassName("Reader Contactless Floor Limit ::: ".plus(contactlessFloorLimit))
        contactlessFloorLimit = AmountUtils.toISOFormattedLowerCurrency(contactlessFloorLimit)
        logger.debugWithClassName(
            "Formatted Reader Contactless Floor Limit ::: ".plus(contactlessFloorLimit)
        )
        cUnionPayBean.readerContactlessFloorLimit_DF8123 = contactlessFloorLimit

        var readerCVMLimit = aidParameters.cvmLimit.defaultOrEmptyValue()
        logger.debugWithClassName("Reader Contactless CVM Limit ::: ".plus(readerCVMLimit))
        readerCVMLimit = AmountUtils.toISOFormattedLowerCurrency(readerCVMLimit)
        logger.debugWithClassName("Formatted Reader Contactless CVM Limit ::: ".plus(readerCVMLimit))
        cUnionPayBean.cvmRequiredLimit_DF8126 = readerCVMLimit

        var contactlessTransactionLimit =
            aidParameters.contactlessTransactionLimit.defaultOrEmptyValue()
        logger.debugWithClassName(
            "Contactless Transaction Limit ::: ".plus(contactlessTransactionLimit)
        )
        contactlessTransactionLimit =
            AmountUtils.toISOFormattedLowerCurrency(contactlessTransactionLimit)
        logger.debugWithClassName(
            "Formatted Contactless Transaction Limit ::: ".plus(contactlessTransactionLimit)
        )
        cUnionPayBean.readerContactlessTransactionLimit_DF8124 = contactlessTransactionLimit

        val riskManagementData = aidParameters.terminalRiskManagementData.defaultOrEmptyValue()
        if (!riskManagementData.isNullOrEmptyOrBlank() && riskManagementData.length >= 16) {
            val riskManagementTLV = EmvTags.TAG_EMV_TERMINAL_RISK_MANAGEMENT_DATA
                .plus("08")
                .plus(riskManagementData.substring(0, 16))
            logger.debugWithClassName("Risk Management Data ::: ".plus(riskManagementData))
            cUnionPayBean.extraTags = riskManagementTLV
        }

        return cUnionPayBean.toTlvByteArray()
    }

    override fun getClassName(): String {
        return FeitianQPBOCKernelConfigHandler::class.java.simpleName
    }
}