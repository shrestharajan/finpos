package global.citytech.neps.datasource.core.transaction

import android.content.Context
import global.citytech.finpos.core.api.DeviceServiceType
import global.citytech.finpos.core.api.cards.CardDetails
import global.citytech.finpos.core.api.cards.EmvTagCollection
import global.citytech.finpos.core.api.cards.PinBlock
import global.citytech.finpos.core.api.cards.SlotType
import global.citytech.finpos.core.api.cards.read.ReadCardResponse
import global.citytech.finpos.core.api.cards.read.ReadCardService
import global.citytech.finpos.core.api.common.DeviceResult
import global.citytech.finpos.core.api.emvparam.EmvParametersRequest
import global.citytech.neps.service.AppCoreFactory
import global.citytech.posswitchintregator.card.CardReader
import global.citytech.posswitchintregator.card.data.*
import java.util.*

class CardReaderImpl(private val instance: Context?) : CardReader {

    private var service: ReadCardService = AppCoreFactory.getCore(
        instance,
        DeviceServiceType.CARD
    ) as ReadCardService

    override fun processEmvNext(readCardRequest: ReadCardRequest?):
            global.citytech.posswitchintregator.card.data.ReadCardResponse {
        val request = prepareReadCardRequest(readCardRequest!!)
        val response = service.processEMV(request)
        return prepareReadCardResponse(response)
    }

    private fun prepareReadCardResponse(
        response: ReadCardResponse
    ): global.citytech.posswitchintregator.card.data.ReadCardResponse {
        return ReadCardResponse(
            getCardDetail(response.cardDetails),
            getResult(response.result),
            response.message
        )
    }

    private fun getResult(result: DeviceResult): Result? {
        return Result.getByCode(result.code)
    }

    private fun getCardDetail(cardDetails: CardDetails?): global.citytech.posswitchintregator.card.data.CardDetails? {
        if (cardDetails != null) {
            val cardDetailes = global.citytech.posswitchintregator.card.data.CardDetails()
            cardDetailes.cardType = prepareCardType(cardDetails.slotType!!)
            cardDetailes.tagCollection = prepareEmvTagCollection(cardDetails.emvTagCollection)
            cardDetailes.aid = cardDetails.aid
            cardDetailes.tag5F28 = cardDetails.tag5F28
            cardDetailes.tag9F42 = cardDetails.tag9F42
            cardDetailes.primaryAccountNumber = cardDetails.pan
            cardDetailes.expireDate = cardDetails.expiryDate
            cardDetailes.trackTwoData = cardDetails.trackTwoData
            cardDetailes.cardHolderName = cardDetails.cardHolderName
            cardDetailes.primaryAccountNumberSerialNumber = cardDetails.panSerialNumber
            cardDetailes.cardScheme = cardDetails.cardScheme
            cardDetailes.pinBlock = preparePinBlock(cardDetails.pinBlock!!)
            cardDetailes.iccDataBlock = cardDetails.iccData
            cardDetailes.amount = cardDetails.amount!!
            cardDetailes.additionalAmount = cardDetails.additionalAmount!!
            cardDetailes.isTransactionAcceptedByCard = cardDetails.transactionAcceptedByCard ?: false
            cardDetailes.transactionInitializeDateTime = cardDetails.transactionInitDateTime
            cardDetailes.transactionState = prepareTransactionSate(cardDetails.transactionState)
            return  cardDetailes
        }
        return null
    }

    private fun prepareTransactionSate(transactionState: global.citytech.finpos.core.api.cards.TransactionState?): TransactionState? {
        if (transactionState != null) {
            return when(transactionState){
                global.citytech.finpos.core.api.cards.TransactionState.ONLINE_APPROVED -> TransactionState.ONLINE_APPROVED
                global.citytech.finpos.core.api.cards.TransactionState.ONLINE_DECLINED -> TransactionState.ONLINE_DECLINED
                global.citytech.finpos.core.api.cards.TransactionState.OFFLINE_APPROVED -> TransactionState.OFFLINE_APPROVED
                global.citytech.finpos.core.api.cards.TransactionState.OFFLINE_DECLINED -> TransactionState.OFFLINE_DECLINED
                global.citytech.finpos.core.api.cards.TransactionState.ACCEPTED -> TransactionState.ACCEPTED
                else -> TransactionState.OFFLINE_DECLINED
            }
        }
        return null
    }

    private fun preparePinBlock(block: PinBlock): global.citytech.posswitchintregator.card.data.PinBlock? {
        return global.citytech.posswitchintregator.card.data.PinBlock(
            block.pin,block.offlinePin
        )
    }

    private fun prepareEmvTagCollection(emvTagCollection: EmvTagCollection?): EMVTagCollection? {
        if (emvTagCollection != null) {
            val tagCollection = EMVTagCollection()
            for((_,value) in emvTagCollection.getTags()){
                val emvTag = EmvTag(value.tag,value.data,value.size)
                tagCollection.add(emvTag)
            }
            return tagCollection
        }
        return null
    }

    override fun setOnlineResult(actionCode: Int, field55Data: String?): SetOnlineResultResponse? {
        val hostField55Data = "8A023030910A7E02A301ED06AFC93030" //TODO this is for temporary purpose
        val readCardResponse = service.setOnlineResult(actionCode, hostField55Data,false);
        return prepareSetOnlineResultResponse(readCardResponse)
    }

    private fun prepareSetOnlineResultResponse(readCardResponse: ReadCardResponse): SetOnlineResultResponse? {
        return SetOnlineResultResponse.SetOnlineResultResponseBuilder()
            .withResult(getResult(readCardResponse.result))
            .withMessage(readCardResponse.message)
            .withEmvTagCollection(prepareEmvTagCollection(readCardResponse.cardDetails?.emvTagCollection))
            .withIccDataBlock(readCardResponse.cardDetails?.iccData)
            .withTransactionState(prepareTransactionSate(readCardResponse.cardDetails?.transactionState))
            .build()
    }

    override fun read(readCardRequest: ReadCardRequest?): global.citytech.posswitchintregator.card.data.ReadCardResponse {
        val request = prepareReadCardRequest(readCardRequest!!)
        val readCardResponse = service.readCardDetails(request)
        return prepareReadCardResponse(readCardResponse)
    }


    private fun prepareReadCardRequest(request: ReadCardRequest): global.citytech.finpos.core.api.cards.read.ReadCardRequest {
        val readCardRequest = global.citytech.finpos.core.api.cards.read.ReadCardRequest(
            prepareSlotsToOpen(request.cardTypes!!),
            request.transactionType,
            prepareEmvParameterRequest(request.emvParametersRequest),
            instance!!.applicationContext.packageName
        )
        readCardRequest.amount = request.amount
        readCardRequest.additionalAmount = request.additionalAmount
        readCardRequest.stan = request.stan
        readCardRequest.transactionDate = request.transactionDate
        readCardRequest.transactionTime = request.transactionTime
        readCardRequest.forceOnlineForContact = request.isForceOnline
        readCardRequest.cardDetailsBeforeEmvNext = prepareHardwareApiCardDetailsBeforEmvNext(
            request.beforeEmvNextCardDetails
        )
        //TODO CardReadTimeout, PinLength
        return readCardRequest
    }

    private fun prepareSlotsToOpen(cardTypes: List<CardType>): List<SlotType> {
        val slotTypes: MutableList<SlotType> = ArrayList()
        for (cardType in cardTypes) {
            when {
                cardType === CardType.MAG -> slotTypes.add(
                    SlotType.MAGNETIC
                )
                cardType === CardType.ICC -> slotTypes.add(
                    SlotType.CONTACT
                )
                cardType === CardType.PICC -> slotTypes.add(
                    SlotType.CONTACTLESS
                )
            }
        }
        return slotTypes
    }

    private fun prepareCardType(slotType: SlotType): CardType {
        return when {
            slotType === SlotType.MAGNETIC -> CardType.MAG

            slotType === SlotType.CONTACT -> CardType.ICC

            slotType === SlotType.CONTACTLESS -> CardType.PICC

            else -> CardType.MANUAL
        }
    }

    private fun prepareSlotType(cardType: CardType): SlotType {
        return when {
            cardType === CardType.MAG -> SlotType.MAGNETIC

            cardType === CardType.ICC -> SlotType.CONTACT

            cardType === CardType.PICC -> SlotType.CONTACTLESS

            else -> SlotType.NONE
        }
    }


    private fun prepareEmvParameterRequest(request: global.citytech.posswitchintregator.card.data.EmvParametersRequest): EmvParametersRequest {
//        return EmvParametersRequest(
//            request.terminalCountryCode,
//            request.transactionCurrencyCode,
//            request.transactionCurrencyExponent,
//            request.terminalCapabilities,
//            request.terminalType,
//            request.merchantCategoryCode,
//            request.terminalId,
//            request.merchantIdentifier,
//            request.merchantName,
//            request.ttq,
//            request.additionalTerminalCapabilites,
//            request.forceOnlineFlag
//        )
        return EmvParametersRequest(
            "0524",
            "0524",
            request.transactionCurrencyExponent,
            request.terminalCapabilities,
            request.terminalType,
            request.merchantCategoryCode,
            request.terminalId,
            request.merchantIdentifier,
            request.merchantName,
            request.ttq,
            request.additionalTerminalCapabilites,
            request.forceOnlineFlag
        )

    }

    private fun prepareHardwareApiEmvTagCollection(
        emvTagCollection: EMVTagCollection?
    ): EmvTagCollection? {
        if (emvTagCollection != null) {
            val tagCollection = EmvTagCollection()
            for((_,value) in emvTagCollection.getTags()){
                val emvTag
                        = global.citytech.finpos.core.api.cards.EmvTag(
                    value.tag,
                    value.data,value.size
                )
                tagCollection.add(emvTag)
            }
            return tagCollection
        }
        return null
    }

    private fun prepareHardwareApiPinBlockForCardDetails(
        pinBlock1: global.citytech.posswitchintregator.card.data.PinBlock
    ): PinBlock {
        return PinBlock(pinBlock1.pin, pinBlock1.isOfflinePin)
    }

    private fun prepareHardwareApiTransactionSate(
        transactionState: TransactionState?
    ): global.citytech.finpos.core.api.cards.TransactionState? {
        if (transactionState != null) {
            return when(transactionState){
                TransactionState.ONLINE_APPROVED -> {
                    global.citytech.finpos.core.api.cards.TransactionState.ONLINE_APPROVED
                }
                TransactionState.ONLINE_DECLINED -> {
                    global.citytech.finpos.core.api.cards.TransactionState.ONLINE_DECLINED
                }
                TransactionState.OFFLINE_APPROVED -> {
                    global.citytech.finpos.core.api.cards.TransactionState.OFFLINE_APPROVED
                }
                TransactionState.OFFLINE_DECLINED -> {
                    global.citytech.finpos.core.api.cards.TransactionState.OFFLINE_DECLINED
                }
                TransactionState.ACCEPTED -> {
                    global.citytech.finpos.core.api.cards.TransactionState.ACCEPTED
                }
                else -> global.citytech.finpos.core.api.cards.TransactionState.OFFLINE_DECLINED
            }
        }
        return null
    }

    private fun prepareHardwareApiCardDetailsBeforEmvNext(
        details: global.citytech.posswitchintregator.card.data.CardDetails?
    ): CardDetails? {
        if (details != null) {
            val cardDetails = CardDetails();
            cardDetails.slotType = prepareSlotType(details.cardType)
            cardDetails.emvTagCollection = prepareHardwareApiEmvTagCollection(details.tagCollection)
            cardDetails.aid = details.aid
            cardDetails.tag5F28 = details.tag5F28
            cardDetails.tag9F42 = details.tag9F42
            cardDetails.pan = details.primaryAccountNumber
            cardDetails.expiryDate = details.expireDate
            cardDetails.trackTwoData = details.trackTwoData
            cardDetails.cardHolderName = details.cardHolderName
            cardDetails.panSerialNumber = details.primaryAccountNumberSerialNumber
            cardDetails.cardScheme = details.cardScheme
            cardDetails.pinBlock = prepareHardwareApiPinBlockForCardDetails(details.pinBlock)
            cardDetails.iccData = details.iccDataBlock
            cardDetails.amount = details.amount
            cardDetails.additionalAmount = details.additionalAmount
            cardDetails.transactionAcceptedByCard = details.isTransactionAcceptedByCard
            cardDetails.transactionState = prepareHardwareApiTransactionSate(details.transactionState)
            cardDetails.transactionInitDateTime = details.transactionInitializeDateTime
            return cardDetails
        }
        return null
    }

    override fun cleanUp() {
        service.cleanUp()
    }
}