package global.citytech.finpos.merchant.data.datasource.app.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import global.citytech.finpos.merchant.domain.model.app.MerchantTransactionLog

/**
 * Created by Saurav Ghimire on 4/26/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */

@Dao
interface MerchantTransactionLogDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(merchantTransactionLog: MerchantTransactionLog): Long

    @Query("SELECT stan, terminal_id, merchant_id, invoice_num, retrieval_reference_number, " +
            "transaction_date, transaction_time, transaction_type, transaction_amount, scheme, " +
            "transaction_status, latitude, longitude, approval_code, original_txn_reference, " +
            "pos_entry_mode, remark, bin, currency_code, vat_amount, vat_refund_amount, cvm_message, " +
            "CASE WHEN emi_info = '' THEN '{}' ELSE emi_info END AS emi_info, " +
            "CASE WHEN transaction_emv_info = '' THEN '{}' ELSE transaction_emv_info END AS transaction_emv_info " +
            "FROM ${MerchantTransactionLog.TABLE_NAME}")
    fun getMerchantTransactionLogs(): List<MerchantTransactionLog>


    @Query("DELETE FROM ${MerchantTransactionLog.TABLE_NAME}")
    fun nukeTable()

    @Query("DELETE FROM ${MerchantTransactionLog.TABLE_NAME} where ${MerchantTransactionLog.COLUMN_STAN} in (:stans)")
    fun deleteByStans(stans: List<String>)

    @Query("DELETE FROM ${MerchantTransactionLog.TABLE_NAME} WHERE stan IN (SELECT stan FROM ${MerchantTransactionLog.TABLE_NAME} ORDER BY stan ASC LIMIT :count)")
    fun deleteMerchantTransactionLogsOfGivenCountFromInitial(count: Int)

    @Query("SELECT stan, terminal_id, merchant_id, invoice_num, retrieval_reference_number, " +
            "transaction_date, transaction_time, transaction_type, transaction_amount, scheme, " +
            "transaction_status, latitude, longitude, approval_code, original_txn_reference, " +
            "pos_entry_mode, remark, bin, currency_code, vat_amount, vat_refund_amount, cvm_message, " +
            "CASE WHEN emi_info = '' THEN '{}' ELSE emi_info END AS emi_info, " +
            "CASE WHEN transaction_emv_info = '' THEN '{}' ELSE transaction_emv_info END AS transaction_emv_info " +
            "FROM ${MerchantTransactionLog.TABLE_NAME} ORDER BY stan ASC LIMIT :count")
    fun getMerchantTransactionLogsOfGivenCount(count: Int): List<MerchantTransactionLog>

}