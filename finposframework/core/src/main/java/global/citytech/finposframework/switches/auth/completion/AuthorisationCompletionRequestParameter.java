package global.citytech.finposframework.switches.auth.completion;

import global.citytech.finposframework.switches.RequestParameter;

/**
 * Created by Unique Shakya on 9/29/2020.
 */
public interface AuthorisationCompletionRequestParameter extends RequestParameter {
}
