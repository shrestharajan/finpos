package global.citytech.finpos.neps.iso8583.requestsender.cashadvance;

import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transactiontypesenders.CashAdvanceISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transactiontypesenders.PurchaseIsoMessageUpdater;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;

public abstract class CashAdvanceRequestSender extends TransactionRequestSender {
    public CashAdvanceRequestSender(RequestContext context) {
        super(context);
    }

    @Override
    protected TransactionTypeISOMessageUpdater getTransactionTypeSender() {
        return new CashAdvanceISOMessageUpdater();
    }

    @Override
    protected MTI.MessageFunction getMessageFunctionRequest() {
        return MTI.MessageFunction.Request;
    }

    @Override
    protected MTI.MessageClass getMessageClass() {
        return MTI.MessageClass.Financial;
    }


}

