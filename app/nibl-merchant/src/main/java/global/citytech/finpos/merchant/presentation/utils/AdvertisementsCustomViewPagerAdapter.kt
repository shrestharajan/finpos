package global.citytech.finpos.merchant.presentation.utils

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.model.banner.Banner


/**
 * Created by Unique Shakya on 6/17/2021.
 */
class AdvertisementsCustomViewPagerAdapter(
    val context: Context,
    private var photos: List<*>?
) : RecyclerView.Adapter<AdvertisementsCustomViewPagerAdapter.CustomViewPagerViewHolder>() {

    init {
        assignDefaultAdvertisementsIfListIsNull()
    }

    class CustomViewPagerViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewPagerViewHolder {
        Log.i("AdvertisementsViewPager", "onCreateViewHolder: ")
        return CustomViewPagerViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_photos, parent, false)
        )
    }

    override fun getItemCount(): Int {
        Log.i("AdvertisementsViewPager", "getItemCount: ")
        return photos!!.size
    }

    override fun onBindViewHolder(holder: CustomViewPagerViewHolder, position: Int) {
        Log.i("AdvertisementsViewPager", "onBindViewHolder: ")
        val advertisementImageView =
            holder.itemView.findViewById<ImageView>(R.id.iv_advertisement)
        val advertisements: List<Any>
        if (photos!![0] is Banner) {
            advertisements = photos as List<Banner>
            Log.i("AdvertisementsViewPager", "URL : " + advertisements[position].banner)
            val options: RequestOptions = RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.banner_loading)
                .error(R.drawable.finpos_banner_wallpaper)
            Glide.with(context.applicationContext)
                .load(advertisements[position].banner)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .apply(options)
                .into(advertisementImageView)
        } else {
            advertisements = photos as List<Int>
            advertisementImageView.setImageResource(advertisements[position])
        }
    }

    private fun assignDefaultAdvertisementsIfListIsNull() {
        if (photos == null || photos!!.isEmpty()) {
            photos = listOf(
                R.drawable.finpos_banner_wallpaper
            )
        }
    }
}