package global.citytech.finposframework.iso8583;

import global.citytech.finposframework.supports.Optional;

public class DataConfig {
    private DataType type;
    private EncodingType encodingType;
    private int maxDataSize;

    public DataConfig(DataType type, EncodingType encodingType) {
        this.type = type;
        this.encodingType = encodingType;
    }

    public DataConfig(DataType type, EncodingType encodingType, int maxDataSize) {
        this.type = type;
        this.encodingType = encodingType;
        this.maxDataSize = maxDataSize;
    }

    public DataConfig(DataType type) {
        this(type,EncodingType.UTF_8);
    }

    public DataType getType() {
        return type;
    }

    public EncodingType getEncodingType() {
        return encodingType;
    }

    public int getMaxDataSize() {
        return maxDataSize;
    }

    /*
    returns byte representation of data
     */
    public Optional<Byte> getBytes() {
        return Optional.ofNullable(null);
    }

    public enum DataType {
        NUMERIC,
        ALPHA,
        ALPHA_NUMERIC,
        DATE,
        DATETIME
    }

}
