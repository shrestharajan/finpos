package global.citytech.finpos.device.weipass.card.handler

import android.app.AlertDialog
import android.content.Context
import com.wiseasy.emvprocess.bean.OnlineResult
import global.citytech.finpos.device.weipass.utils.WeipassConstant
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finpos.device.weipass.utils.WeipassTransState
import global.citytech.finposframework.hardware.io.cards.CardSummary
import global.citytech.finposframework.hardware.io.cards.PinBlock
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import java.lang.ref.WeakReference
import java.util.concurrent.CountDownLatch

/**
 * Created by Rishav Chudal on 6/8/20.
 */
object WeipassStateHandler {
    var contextWeakReference: WeakReference<Context>?
            by SynchronizePropertyDelegate(null)

    var kernelCountDownLatch: CountDownLatch?
            by SynchronizePropertyDelegate(null)

    var serviceCountDownLatch: CountDownLatch?
            by SynchronizePropertyDelegate(null)

    var onlineResult: OnlineResult? by SynchronizePropertyDelegate(null)

    @get:Synchronized @set:Synchronized
    var ksn: String

    var readCardRequest: ReadCardRequest? by SynchronizePropertyDelegate(null)

    @get:Synchronized
    @set:Synchronized
    var cardSummary: CardSummary? by SynchronizePropertyDelegate(null)

    var readCardResponse: ReadCardResponse? by SynchronizePropertyDelegate(null)

    @get:Synchronized @set:Synchronized
    var isOnlineProcess = false

    @get:Synchronized @set:Synchronized
    var doProcessScript = false

    var transState: WeipassTransState? by SynchronizePropertyDelegate(null)

    var CURRENT_CARD_TYPE: CardType? by SynchronizePropertyDelegate(null)

    var issuerScriptResults: String? by SynchronizePropertyDelegate(null)

    var alertDialogWeakReference: WeakReference<AlertDialog>?
            by SynchronizePropertyDelegate(null)

    var pinpadMessage: String? by SynchronizePropertyDelegate(null)

    var pinPadAmountMessage: String? by SynchronizePropertyDelegate(null)

    @get:Synchronized @set:Synchronized
    var haltEmvProcess = false

    @get:Synchronized @set:Synchronized
    var pinBlock = PinBlock(
        "Offline",
        true
    )

    init {
        ksn = WeipassConstant.EMPTY_STRING
        onlineResult = OnlineResult()
        transState = WeipassTransState.NONE
    }

    fun initKernelCountDownLatch() {
        WeipassLogger.log("WeipassStateHandler ::: initializing Kernel CountDownLatch...")
        kernelCountDownLatch = CountDownLatch(1)
    }

    fun awaitKernelCountDownLatch() {
        if (kernelCountDownLatch == null) {
            initKernelCountDownLatch()
        }
        try {
            WeipassLogger.log("WeipassStateHandler ::: Awaiting KernelCountdownLatch ::: "
                .plus(kernelCountDownLatch))
            kernelCountDownLatch!!.await()
        } catch (ex: Exception) {
            WeipassLogger.log("Exception ::: ".plus(ex.message))
        }
    }

    fun releaseKernelCountDownLatch() {
        if (kernelCountDownLatch != null) {
            WeipassLogger.log("WeipassStateHandler ::: Releasing KernelCountdownLatch ::: "
                .plus(kernelCountDownLatch))
            kernelCountDownLatch!!.countDown()
        }
    }

    fun initServiceCountDownLatch() {
        WeipassLogger.log("WeipassStateHandler ::: initializing Service CountDownLatch...")
        serviceCountDownLatch = CountDownLatch(1)
    }

    fun awaitServiceCountDownLatch() {
        initServiceCountDownLatch()
        try {
            WeipassLogger.log("WeipassStateHandler ::: Awaiting ServiceCountdownLatch ::: "
                .plus(serviceCountDownLatch))
            serviceCountDownLatch!!.await()
        } catch (ex: Exception) {
            WeipassLogger.log("Exception ::: ".plus(ex.message))
        }
    }

    fun releaseServiceCountdownLatch() {
        if (serviceCountDownLatch != null) {
            WeipassLogger.log("WeipassStateHandler ::: Releasing ServiceCountdownLatch ::: "
                .plus(serviceCountDownLatch))
            serviceCountDownLatch!!.countDown()
        }
    }

    fun releaseAllCountdownLatches() {
        releaseKernelCountDownLatch()
        releaseServiceCountdownLatch()
    }

    fun clearPropertyInstances() {
        WeipassLogger.log("WeipassStateHandler ::: Clearing Earlier Variable Instances...")
        contextWeakReference = null
        onlineResult = null
        isOnlineProcess = false
        doProcessScript = false
        kernelCountDownLatch = null
        serviceCountDownLatch = null
        readCardRequest = null
        readCardResponse = null
        transState = WeipassTransState.NONE
        CURRENT_CARD_TYPE = CardType.UNKNOWN
        issuerScriptResults = null
        alertDialogWeakReference = null
        pinpadMessage = null
        pinPadAmountMessage = null
        haltEmvProcess = false
        pinBlock = PinBlock(
            "Offline",
            true
        )
        cardSummary = CardSummary(
            null,
            null,
            null,
            null
        )
    }
}