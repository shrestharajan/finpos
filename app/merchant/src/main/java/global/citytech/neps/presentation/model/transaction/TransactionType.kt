package global.citytech.neps.presentation.model.transaction

enum class TransactionType(val offset: Int, val processingCode: String) {
    PURCHASE(0, ProcessingCode.PURCHASE_FROM_DEFAULT_ACCOUNT.code);

}