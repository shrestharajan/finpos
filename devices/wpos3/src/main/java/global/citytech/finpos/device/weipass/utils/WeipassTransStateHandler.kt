package global.citytech.finpos.device.weipass.utils

import global.citytech.finpos.device.weipass.card.handler.WeipassStateHandler
import global.citytech.finpos.device.weipass.utils.SDKErrorCode.*
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_APP_SELECTION_CANCELLED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_PINPAD_BYPASS
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_PINPAD_FAILURE
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_PINPAD_TIMEOUT
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_PIN_ENTRY_CANCELLED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_PROCESSING_ERROR
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_READ_CARD_FAIL
import global.citytech.finpos.device.weipass.utils.WeipassTransState.*
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse

/**
 * Created by Rishav Chudal on 6/8/20.
 */
class WeipassTransStateHandler {
    companion object {
        fun doProceedFurther(
            transState: WeipassTransState,
            weipassStateHandler: WeipassStateHandler
        ): Boolean {
            val result: Boolean
            when (transState) {
                FAILURE -> result = manageForFailureCase(weipassStateHandler)
                SELECT_APPLICATION_CANCELLED -> result = manageForSelectApplicationCancelledCase(weipassStateHandler)
                PINPAD_CANCEL -> result = manageForPinpadCancelledCase(weipassStateHandler)
                PINPAD_ERROR -> result = manageForPinpadErrorCase(weipassStateHandler)
                PINPAD_TIMEOUT -> result = manageForPinpadTimeoutCase(weipassStateHandler)
                PROCESSING_ERROR -> result = manageForProcessingErrorCase(weipassStateHandler)
                PINPAD_BYPASS -> result =  manageForPinPadByPass(weipassStateHandler)
                else -> result = true
            }
            return result
        }

        private fun manageForFailureCase(
            weipassStateHandler: WeipassStateHandler
        ): Boolean {
            val readCardResponse =
                ReadCardResponse(
                    null,
                    Result.FAILURE,
                    MSG_READ_CARD_FAIL
                )
            weipassStateHandler.readCardResponse = readCardResponse
            return false
        }

        private fun manageForSelectApplicationCancelledCase(
            weipassStateHandler: WeipassStateHandler
        ) : Boolean {
            val readCardResponse =
                ReadCardResponse(
                    null,
                    Result.USER_CANCELLED,
                    MSG_APP_SELECTION_CANCELLED
                )
            weipassStateHandler.readCardResponse = readCardResponse
            return false
        }

        private fun manageForPinpadCancelledCase(
            weipassStateHandler: WeipassStateHandler
        ) : Boolean {
            val readCardResponse =
                ReadCardResponse(
                    null,
                    Result.USER_CANCELLED,
                    MSG_PIN_ENTRY_CANCELLED
                )
            weipassStateHandler.readCardResponse = readCardResponse
            return false
        }

        private fun manageForPinpadErrorCase(
            weipassStateHandler: WeipassStateHandler
        ) : Boolean {
            val readCardResponse =
                ReadCardResponse(
                    null,
                    Result.PINPAD_ERROR,
                    MSG_PINPAD_FAILURE
                )
            weipassStateHandler.readCardResponse = readCardResponse
            return false
        }

        private fun manageForPinpadTimeoutCase(
            weipassStateHandler: WeipassStateHandler
        ) : Boolean {
            val readCardResponse =
                ReadCardResponse(
                    null,
                    Result.TIMEOUT,
                    MSG_PINPAD_TIMEOUT
                )
            weipassStateHandler.readCardResponse = readCardResponse
            return false
        }

        private fun manageForProcessingErrorCase(
            weipassStateHandler: WeipassStateHandler
        ) : Boolean {
            val readCardResponse =
                ReadCardResponse(
                    null,
                    Result.PROCESSING_ERROR,
                    MSG_PROCESSING_ERROR
                )
            weipassStateHandler.readCardResponse = readCardResponse
            return false
        }

        private fun manageForPinPadByPass(
            weipassStateHandler: WeipassStateHandler
        ) : Boolean {
            val readCardResponse =
                ReadCardResponse(
                    null,
                    Result.PIN_BYPASSED,
                    MSG_PINPAD_BYPASS
                )
            weipassStateHandler.readCardResponse = readCardResponse
            return false
        }

        /**
         * checkForSdkErrorCode
         */
        fun checkForSdkErrorCode(result: Int, weipassStateHandler: WeipassStateHandler) {
            if (result != 0) {
                WeipassLogger.log("Check For SDK Error Code ::: result ::: "
                    .plus(Integer.toHexString(result)))
                when (result) {
                    TRANSPROCESS_PICC_TRANS_FAILED_EXCEPTION.errorCode -> manageForPICCFailedException(weipassStateHandler)
                    TRANSPROCESS_PICC_TRANS_PRE_PROCESS_FAILED.errorCode -> manageForPICCPreProcessFailed(weipassStateHandler)
                    TRANSPROCESS_PICC_TRANS_APPLICATION_SELECT_FAILED.errorCode -> manageForPICCAppSelectFailed(weipassStateHandler)
                    TRANSPROCESS_PICC_TRANS_READ_RECORD_FAILED.errorCode -> manageForPICCReadRecordFailed(weipassStateHandler)
                    TRANSPROCESS_PICC_TRANS_CARD_AUTHENTICATE_FAILED.errorCode -> manageForPICCCardAuthenticateFailed(weipassStateHandler)
                    TRANSPROCESS_PICC_TRANS_PARAMETER_NULL.errorCode -> manageForPICCParameterNull(weipassStateHandler)
                    TRANSPROCESS_PICC_TRANS_PARAMETER_TERMINAL_SETTING_NULL.errorCode -> manageForPICCParamTerminalSettingNull(weipassStateHandler)
                    TRANSPROCESS_PICC_TRANS_PARAMETER_TERMINALSETTING_ERROR.errorCode -> manageForPICCParamTerminalSettingError(weipassStateHandler)
                    TRANSPROCESS_PICC_TRANS_NO_APPLICATION.errorCode -> manageForPICCNoApplication(weipassStateHandler)
                    TRANSPROCESS_PICC_TRANS_APPLICATION_BLOCK.errorCode -> manageForPICCApplicationBlock(weipassStateHandler)
                    TRANSPROCESS_PICC_TRANS_CARD_BLOCK.errorCode -> manageForPICCCardBlock(weipassStateHandler)
                    TRANSPROCESS_PICC_TRANS_USE_OTHER_INTERFACE.errorCode -> manageForPICCUseOtherInterface(weipassStateHandler)
                    TRANSPROCESS_PICC_TRANS_NO_DETECT_CARD.errorCode -> manageForPICCNoDetectCard(weipassStateHandler)
                    TRANSPROCESS_PICC_TRANS_USE_CONTRACT.errorCode -> manageForPICCUseContract(weipassStateHandler)
                    TRANSPROCESS_PICC_TRANS_TRY_AGAIN.errorCode -> manageForPICCTryAgain(weipassStateHandler)
                    TRANSPROCESS_PICC_TRANS_OTHER_ERROR.errorCode -> manageForPICCOtherError(weipassStateHandler)
                    TRANSPROCESS_ICC_TRANS_FAILED.errorCode -> manageForICCTransFailed(weipassStateHandler)
                    TRANSPROCESS_ICC_TRANS_APPSEL_FAILED.errorCode -> manageForICCAppSelectFailed(weipassStateHandler)
                    TRANSPROCESS_ICC_TRANS_READAPPDATA_FAILED.errorCode -> manageForICCReadAppDataFailed(weipassStateHandler)
                    TRANSPROCESS_ICC_TRANS_CARDAUTH_FAILED.errorCode -> manageForICCCardAuthFailed(weipassStateHandler)
                    TRANSPROCESS_GETPANFROMEMVCARD_GETPAN_FAILED.errorCode -> manageForGetPanFailed(weipassStateHandler)
                    TRANSPROCESS_ICC_TRANS_ONLINE_FAILED.errorCode -> manageForICCOnlineFailed(weipassStateHandler)
                    TRANSPROCESS_ICC_TRANS_APPLICATION_BLOCK.errorCode -> manageForICCApplicationBlock(weipassStateHandler)
                    OUTCOME_FAILED.errorCode -> manageForOutcomeFailed(weipassStateHandler)
                }
            }
        }

        private fun manageForPICCFailedException(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_FAILED_EXCEPTION
            ))
            weipassStateHandler.transState = PROCESSING_ERROR
        }

        private fun manageForPICCPreProcessFailed(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_PRE_PROCESS_FAILED
            ))
            weipassStateHandler.transState = PICC_WAVE_AGAIN
        }

        private fun manageForPICCAppSelectFailed(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_APPLICATION_SELECT_FAILED
            ))
            weipassStateHandler.transState = PICC_WAVE_AGAIN
        }

        private fun manageForPICCReadRecordFailed(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_READ_RECORD_FAILED
            ))
            weipassStateHandler.transState = PICC_WAVE_AGAIN
        }

        private fun manageForPICCCardAuthenticateFailed(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_CARD_AUTHENTICATE_FAILED
            ))
            weipassStateHandler.transState = PROCESSING_ERROR
        }

        private fun manageForPICCParameterNull(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_PARAMETER_NULL
            ))
            weipassStateHandler.transState = PROCESSING_ERROR
        }

        private fun manageForPICCParamTerminalSettingNull(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_PARAMETER_TERMINAL_SETTING_NULL
            ))
            weipassStateHandler.transState = PROCESSING_ERROR
        }

        private fun manageForPICCParamTerminalSettingError(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_PARAMETER_TERMINALSETTING_ERROR
            ))
            weipassStateHandler.transState = PROCESSING_ERROR
        }

        private fun manageForPICCNoApplication(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_NO_APPLICATION
            ))
            weipassStateHandler.transState = PICC_FALLBACK
        }

        private fun manageForPICCApplicationBlock(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_APPLICATION_BLOCK
            ))
            weipassStateHandler.transState = APPLICATION_BLOCKED
        }

        private fun manageForPICCCardBlock(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_CARD_BLOCK
            ))
            weipassStateHandler.transState = PICC_FALLBACK
        }

        private fun manageForPICCUseOtherInterface(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_USE_OTHER_INTERFACE
            ))
            weipassStateHandler.transState = PICC_FALLBACK
        }

        private fun manageForPICCNoDetectCard(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_NO_DETECT_CARD
            ))
            weipassStateHandler.transState = PICC_WAVE_AGAIN
        }

        private fun manageForPICCUseContract(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_USE_CONTRACT
            ))
            weipassStateHandler.transState = PICC_FALLBACK
        }

        private fun manageForPICCTryAgain(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_TRY_AGAIN
            ))
            weipassStateHandler.transState = PICC_WAVE_AGAIN
        }

        private fun manageForPICCOtherError(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_PICC_TRANS_OTHER_ERROR
            ))
            weipassStateHandler.transState = PROCESSING_ERROR
        }

        private fun manageForICCTransFailed(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_ICC_TRANS_FAILED
            ))
            weipassStateHandler.transState = FAILURE
        }

        private fun manageForICCAppSelectFailed(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_ICC_TRANS_APPSEL_FAILED
            ))
            weipassStateHandler.transState = CARD_BLOCKED
        }

        private fun manageForICCReadAppDataFailed(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_ICC_TRANS_READAPPDATA_FAILED
            ))
            manageForICCFallback(weipassStateHandler)
        }

        private fun manageForICCCardAuthFailed(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_ICC_TRANS_CARDAUTH_FAILED
            ))
            manageForICCFallback(weipassStateHandler)
        }

        private fun manageForGetPanFailed(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_GETPANFROMEMVCARD_GETPAN_FAILED
            ))
            weipassStateHandler.transState = PROCESSING_ERROR
        }

        private fun manageForOutcomeFailed(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                OUTCOME_FAILED
            ))
            weipassStateHandler.transState = PROCESSING_ERROR
        }

        private fun manageForICCOnlineFailed(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_ICC_TRANS_ONLINE_FAILED
            ))
            weipassStateHandler.transState = ONLINE_SYNC_API_FAILED
        }

        private fun manageForICCFallback(weipassStateHandler: WeipassStateHandler) {
            if (WeipassDeviceUtils.isICCPresentInDevice()) {
                weipassStateHandler.transState = ICC_FALLBACK
            } else {
                weipassStateHandler.transState = PROCESSING_ERROR
            }
        }

        private fun manageForICCApplicationBlock(weipassStateHandler: WeipassStateHandler) {
            WeipassLogger.log("SDK Error Case ::: ".plus(
                TRANSPROCESS_ICC_TRANS_APPLICATION_BLOCK
            ))
            weipassStateHandler.transState = APPLICATION_BLOCKED
        }

    }
}