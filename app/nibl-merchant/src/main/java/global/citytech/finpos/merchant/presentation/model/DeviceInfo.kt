package global.citytech.finpos.merchant.presentation.model

data class DeviceInfo(
    var serialNumber: String? = null
)