package global.citytech.finpos.processor.nibl.transaction.cash.advance;

import java.math.BigDecimal;

import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finpos.nibl.iso8583.requestsender.cashadvance.IccCashAdvanceRequestSender;
import global.citytech.finpos.nibl.iso8583.requestsender.cashadvance.MagneticCashAdvanceRequestSender;
import global.citytech.finpos.nibl.iso8583.requestsender.cashadvance.NfcCashAdvanceRequestSender;
import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finpos.processor.nibl.transaction.template.CardTransactionUseCase;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.utility.IsoMessageUtils;

/**
 * Created by Unique Shakya on 3/16/2021.
 */
public class CashAdvanceUseCase extends CardTransactionUseCase {

    public CashAdvanceUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    protected String getTransactionTypeClassName() {
        return CashAdvanceUseCase.class.getSimpleName();
    }


    @Override
    protected NIBLMessageSenderTemplate getRequestSender(RequestContext context) {
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case MAG:
                return new MagneticCashAdvanceRequestSender(new NIBLSpecInfo(), context);
            case ICC:
                return new IccCashAdvanceRequestSender(new NIBLSpecInfo(), context);
            case PICC:
                return new NfcCashAdvanceRequestSender(new NIBLSpecInfo(), context);
            default:
                throw new IllegalArgumentException("Unknown card type");
        }
    }

    @Override
    protected String getOriginalPosConditionCode() {
        return null;
    }

    @Override
    protected PosEntryMode getOriginalPosEntryMode() {
        return null;
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.CASH_ADVANCE.getCode();
    }


    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy, String miniStatementData) {
        return null;
    }


    @Override
    protected TransactionResponseModel prepareTransactionResponse(IsoMessageResponse isoMessageResponse, boolean isApproved, boolean shouldPrintCustomerCopy) {
        return CashAdvanceResponseModel.Builder.newInstance()
                .message(this.retrieveMessage(isoMessageResponse, isApproved))
                .isApproved(isApproved)
                .debugRequestMessage(isoMessageResponse.getDebugRequestString())
                .debugResponseMessage(isoMessageResponse.getDebugResponseString())
                .stan(this.stan)
                .shouldPrintCustomerCopy(shouldPrintCustomerCopy)
                .build();
    }

    @Override
    protected TransactionResponseModel prepareDeclinedTransactionResponse(String message,
                                                                          boolean shouldPrintReceipt) {
        return CashAdvanceResponseModel.Builder.newInstance()
                .isApproved(false)
                .message(message)
                .stan(this.stan)
                .shouldPrintCustomerCopy(shouldPrintReceipt)
                .build();
    }

    @Override
    protected String getOriginalTransactionReferenceNumber() {
        return "";
    }

    @Override
    protected boolean isAuthorizationCompleted() {
        return true;
    }

    @Override
    protected void updateTransactionLogOfOriginalTransaction(boolean isApproved) {
        // do nothing
    }

    @Override
    protected boolean shouldIncrementInvoiceNumber(boolean isApproved) {
        return isApproved;
    }

    @Override
    protected TransactionLog retrieveTransactionLogToStore(boolean isApproved, IsoMessageResponse isoMessageResponse) {
        return TransactionLog.Builder.newInstance()
                .withStan(stan)
                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
                .withInvoiceNumber(invoiceNumber)
                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
                .withTransactionType(TransactionType.CASH_ADVANCE)
                .withOriginalTransactionType(TransactionType.CASH_ADVANCE)
                .withTransactionAmount(this.request.getTransactionRequest().getAmount())
                .withOriginalTransactionAmount(BigDecimal.ZERO)
                .withTransactionDate(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.LOCAL_DATE))
                .withTransactionTime(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.LOCAL_TIME))
                .withTransactionStatus(isApproved ? APPROVED : DECLINED)
                .withPosEntryMode(this.transactionIsoRequest.getPosEntryMode())
                .withOriginalPosEntryMode(this.getOriginalPosEntryMode())
                .withPosConditionCode(this.transactionIsoRequest.getPosConditionCode())
                .withOriginalPosConditionCode(this.getOriginalPosConditionCode())
                .withReconcileStatus("")
                .withReconcileTime("")
                .withReconcileDate("")
                .withReconcileBatchNo(batchNumber)
                .withReadCardResponse(readCardResponse)
                .withAuthorizationCompleted(true)
                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                .withReceiptLog(this.receiptLog)
                .withTransactionVoided(false)
                .withTipAdjusted(false)
                .withCurrencyCode(this.request.getTransactionRequest().getEmvParameterRequest().getTransactionCurrencyCode())
                .build();
    }
}
