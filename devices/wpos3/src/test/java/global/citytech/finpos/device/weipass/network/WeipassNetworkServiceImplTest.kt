package global.citytech.finpos.device.weipass.network

import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.network.NetworkType
import io.mockk.MockKAnnotations
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

/**
 * Created by Rishav Chudal on 6/7/20.
 */
class WeipassNetworkServiceImplTest {
    private lateinit var SUT: WeipassNetworkServiceImpl

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        SUT = WeipassNetworkServiceImpl()
    }

    @Test
    fun enableNetwork_OnNetworkTypeAny_ReturnsSuccessDeviceResponse() {
        //Setup
        val networkType: NetworkType = NetworkType.WIFI

        //Run
        val response = SUT.enableNetwork(networkType)

        //Verify
        assertEquals(NetworkType.WIFI, networkType)
        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun disableNetwork_OnNetworkTypeAny_ReturnsSuccessDeviceResponse() {
        //Setup
        val networkType: NetworkType = NetworkType.WIFI

        //Run
        val response = SUT.disableNetwork(networkType)

        //Verify
        assertEquals(NetworkType.WIFI, networkType)
        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun setDataTransmissionNetwork_OnNetworkTypeAny_ReturnsSuccessDeviceResponse() {
        //Setup
        val networkType: NetworkType = NetworkType.WIFI

        //Run
        val response = SUT.setDataTransmissionNetwork(networkType)

        //Verify
        assertEquals(NetworkType.WIFI, networkType)
        assertEquals(Result.SUCCESS, response.result)
    }

    @Test
    fun setAPN_OnNetworkTypeAny_ReturnsSuccessDeviceResponse() {
        //Setup
        val networkType: NetworkType = NetworkType.WIFI

        //Run
        val response = SUT.setAPN(networkType)

        //Verify
        assertEquals(NetworkType.WIFI, networkType)
        assertEquals(Result.SUCCESS, response.result)
    }
}