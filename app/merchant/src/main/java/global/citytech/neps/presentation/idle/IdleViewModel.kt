package global.citytech.neps.presentation.idle

import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseViewModel
import global.citytech.neps.data.repository.local.LocalRepositoryImpl
import global.citytech.neps.datasource.local.LocalConfigurationDataSourceImpl
import global.citytech.neps.domain.usecase.local.LocalDataUseCase
import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.neps.presentation.model.mapToPresentation
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class IdleViewModel : BaseViewModel() {

    val configuration by lazy { MutableLiveData<ConfigurationItem>() }
    var configurationUseCase: LocalDataUseCase =
        LocalDataUseCase(
            LocalRepositoryImpl(LocalConfigurationDataSourceImpl())
        )

    fun getConfiguration() =
        compositeDisposable.add(configurationUseCase.getWholeConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.mapToPresentation() }
            .subscribe({ configuration.value = it }, { message.value = it.message })
        )
}