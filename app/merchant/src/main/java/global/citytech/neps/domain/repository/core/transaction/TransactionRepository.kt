package global.citytech.neps.domain.repository.core.transaction

import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.neps.presentation.model.transaction.PurchaseRequestItem
import global.citytech.neps.presentation.model.transaction.PurchaseResponseItem
import global.citytech.posswitchintregator.neps.usecases.purchase.PurchaseResponseModel
import io.reactivex.Observable

interface TransactionRepository {
    fun purchase(configurationItem: ConfigurationItem,purchaseRequestItem: PurchaseRequestItem): Observable<PurchaseResponseModel>
    fun cleanUp()
}