package global.citytech.finpos.neps.iso8583;

import global.citytech.finpos.neps.iso8583.requestsender.purchases.PurchaseIsoRequest;
import global.citytech.finposframework.utility.HelperUtils;

public class MockPurchase {

    public static PurchaseIsoRequest getPurchaseIsoRequest() {
        return PurchaseIsoRequest.Builder.newInstance()
                .currencyCode("524")
                .emvData("5F2A0205245F34010082021C008407A00000000310108A025A31950580800400009A032101289C01009F02060000000005009F03060000000000009F090200029F100706010A03A0A0009F1A0205249F1E0830343130303238349F2608A55C290D16223A0A9F2701809F3303E0F8C89F34034203009F3501229F360203719F37046885C4CB9F4104000000494F07A0000000031010")
                .localDateTime(HelperUtils.getLocalTransactionTime())
                .pan("4438300123287540")
                .pinBlock("5ACB9BF2FFC04C48")
                .track2Data("4438300123287540D23076261000091099999")
               // .transactionAmount(56.00)
                .build();
    }
}
