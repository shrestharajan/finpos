package global.citytech.finposframework.switches.receipt.duplicatereconciliation;

import global.citytech.finposframework.switches.RequestParameter;

/**
 * Created by Unique Shakya on 12/16/2020.
 */
public interface DuplicateReconciliationReceiptRequestParameter extends RequestParameter {
}
