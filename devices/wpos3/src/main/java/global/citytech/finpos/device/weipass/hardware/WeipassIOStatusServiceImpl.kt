package global.citytech.finpos.device.weipass.hardware

import android.content.Context
import com.wiseasy.emvprocess.SDKInstance
import global.citytech.finpos.device.weipass.utils.WeipassDeviceUtils
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getDeviceStatus_PRINTER_STATUS_NOT_EXIST
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getDeviceStatus_PRINTER_STATUS_OK
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getDeviceStatus_PRINTER_STATUS_OUT_OF_PAPER
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getDeviceStatus_PRINTER_STATUS_UNKNOWN
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getPrinterStatus_PRINTER_NOT_AVAILABLE
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getPrinterStatus_PRINTER_OUT_OF_PAPER
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getPrinterStatus_PRINTER_OVER_HEAT
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getPrinterStatus_PRINTER_PARAMETER_ERROR
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getPrinterStatus_PRINTER_STATUS_OK
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_CONTACTLESS_READER_NOT_EXIST
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_CONTACTLESS_READER_OK
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_CONTACT_READER_NOT_EXIST
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_CONTACT_READER_OK
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_CONTACT_READER_UNKNOWN
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_MAG_READER_NOT_EXIST
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_MAG_READER_OK
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_MAG_READER_UNKNOWN
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_PICC_READER_NOT_SUPPORTED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_PICC_READER_OK
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_PICC_READER_OUT_OF_ORDER
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_ICC_READER_NOT_SUPPORTED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_ICC_READER_OK
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_ICC_READER_OUT_OF_ORDER
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_MAG_READER_NOT_SUPPORTED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_MAG_READER_OK
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_MAG_READER_OUT_OF_ORDER
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_PRINTER_NOT_AVAILABLE
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_PRINTER_OUT_OF_PAPER
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_PRINTER_PLAIN_RECEIPT_PAPER
import global.citytech.finposframework.hardware.io.status.IOStatusRequest
import global.citytech.finposframework.hardware.io.status.IOStatusResponse
import global.citytech.finposframework.hardware.io.status.IOStatusService

/**
 * Created by Rishav Chudal on 6/3/20.
 */
class WeipassIOStatusServiceImpl(val context: Context):
    IOStatusService {
    private lateinit var IOStatusResponse: IOStatusResponse
    private lateinit var outData: ByteArray
    private lateinit var outDataLen: IntArray

    override fun retrieveHardwareStatus(request: IOStatusRequest): IOStatusResponse {
        IOStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_NOT_AVAILABLE,
                CORE_MAG_READER_OUT_OF_ORDER,
                CORE_ICC_READER_OUT_OF_ORDER,
                CORE_PICC_READER_OUT_OF_ORDER
            )

        if (request.isPrinterOnly) {
            proceedForPrinterStatusOnly()
        } else {
            proceedForAllComponentStatus()
        }
        return IOStatusResponse
    }

    private fun proceedForPrinterStatusOnly() {
        IOStatusResponse =
            IOStatusResponse(
                proceedForPrinterStatus(),
                CORE_MAG_READER_OK,
                CORE_ICC_READER_OK,
                CORE_PICC_READER_OK
            )
    }

    private fun proceedForPrinterStatus(): String {
        var printerStatus = CORE_PRINTER_NOT_AVAILABLE
        try{
            WeipassLogger.log("Getting Printer status via api getPrinterStatus")
            val sdkStatus = IntArray(1);
            val result = SDKInstance.mPrinter.getPrinterStatus(sdkStatus)
            if (result == 0) {
                printerStatus = analyzePrinterStatus(sdkStatus[0])
            }
        } catch (exception: Exception) {
            WeipassLogger.log("Exception ::: ".plus(exception.message))
        }
        return printerStatus
    }

    private fun analyzePrinterStatus(sdkStatus: Int): String {
        return when(sdkStatus) {
            SDK_API_getPrinterStatus_PRINTER_STATUS_OK -> manageForPrinterStatusOk()
            SDK_API_getPrinterStatus_PRINTER_PARAMETER_ERROR -> manageForPrinterParameterError()
            SDK_API_getPrinterStatus_PRINTER_NOT_AVAILABLE -> manageForPrinterStatusNotAvailable()
            SDK_API_getPrinterStatus_PRINTER_OUT_OF_PAPER -> manageForPrinterOutOfPaper()
            SDK_API_getPrinterStatus_PRINTER_OVER_HEAT -> manageForPrinterOverheat()
            else -> manageForPrinterStatusUnknown()
        }
    }

    private fun proceedForAllComponentStatus() {
        WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext)
        proceedForDeviceStatus()
        IOStatusResponse =
            IOStatusResponse(
                analyzePrinterStatus(outData[3]),
                analyzeMagReaderStatus(outData[0]),
                analyzeContactReaderStatus(outData[2]),
                analyzePICCReaderStatus(outData[1])
            )
    }

    private fun proceedForDeviceStatus(): Int {
        WeipassLogger.log("Proceeding for SDK api getDeviceStatus...")
        var result = -1
        outData = ByteArray(8)
        outDataLen = IntArray(1)
        try{
            result = SDKInstance.mCore.getDeviceStatus(outData, outDataLen)
        } catch (exception: Exception) {
            WeipassLogger.log("Exception ::: ".plus(exception.message))
        }
        WeipassLogger.log("SDK api getDeviceStatus result ::: ".plus(result))
        return result
    }

    private fun analyzePrinterStatus(outDataBye: Byte): String {
      return  when(outDataBye){
          SDK_API_getDeviceStatus_PRINTER_STATUS_OK.toByte() -> manageForPrinterStatusOk()
          SDK_API_getDeviceStatus_PRINTER_STATUS_UNKNOWN.toByte() -> manageForPrinterStatusUnknown()
          SDK_API_getDeviceStatus_PRINTER_STATUS_OUT_OF_PAPER.toByte() -> manageForPrinterOutOfPaper()
          SDK_API_getDeviceStatus_PRINTER_STATUS_NOT_EXIST.toByte() -> manageForPrinterStatusNotAvailable()
          else -> manageForPrinterStatusUnknown()
        }
    }

    /**
     * Printer Status
     */
    private fun manageForPrinterStatusOk(): String {
        WeipassLogger.log("Printer Status Ok")
        return CORE_PRINTER_PLAIN_RECEIPT_PAPER
    }

    private fun manageForPrinterParameterError(): String {
        WeipassLogger.log("Printer Status Parameter Error")
        return CORE_PRINTER_NOT_AVAILABLE
    }

    private fun manageForPrinterStatusNotAvailable(): String{
        WeipassLogger.log("Printer Status Not Available")
        return CORE_PRINTER_NOT_AVAILABLE
    }

    private fun manageForPrinterOutOfPaper(): String {
        WeipassLogger.log("Printer Status Out of Paper")
        return CORE_PRINTER_OUT_OF_PAPER
    }

    private fun manageForPrinterOverheat(): String {
        WeipassLogger.log("Printer Status Overheat")
        return CORE_PRINTER_NOT_AVAILABLE
    }

    private fun manageForPrinterStatusUnknown(): String {
        WeipassLogger.log("Printer Status Unknown")
        return CORE_PRINTER_NOT_AVAILABLE
    }

    /**
     * Mag Reader Status
     */
    private fun analyzeMagReaderStatus(outDataBye: Byte): String {
        return when(outDataBye) {
            SDK_MAG_READER_OK.toByte() -> manageForMagReaderStatusOk()
            SDK_MAG_READER_UNKNOWN.toByte() -> manageForMagReaderUnknown()
            SDK_MAG_READER_NOT_EXIST.toByte() -> manageForMagReaderNotExist()
            else -> manageForMagReaderStatusOutOfOrder()
        }
    }

    private fun manageForMagReaderStatusOk(): String {
        WeipassLogger.log("Mag Reader Status Ok")
        return CORE_MAG_READER_OK
    }

    private fun manageForMagReaderUnknown(): String {
        WeipassLogger.log("Mag Reader Status Unknown")
        return CORE_MAG_READER_OUT_OF_ORDER
    }

    private fun manageForMagReaderNotExist(): String {
        WeipassLogger.log("Mag Reader Status Not Exist")
        return CORE_MAG_READER_NOT_SUPPORTED
    }

    private fun manageForMagReaderStatusOutOfOrder(): String {
        WeipassLogger.log("Mag Reader Status Out Of Order")
        return CORE_MAG_READER_OUT_OF_ORDER
    }

    /**
     * Contact Reader Status
     */
    private fun analyzeContactReaderStatus(outDataBye: Byte): String {
        return when(outDataBye) {
            SDK_CONTACT_READER_OK.toByte() -> manageForICCReaderStatusOk()
            SDK_CONTACT_READER_UNKNOWN.toByte() -> manageForICCReaderUnknown()
            SDK_CONTACT_READER_NOT_EXIST.toByte() -> manageForICCReaderNotExist()
            else -> manageForContactReaderStatusOutOfOrder()
        }
    }

    private fun manageForICCReaderStatusOk(): String {
        WeipassLogger.log("ICC Reader Status Ok")
        return CORE_ICC_READER_OK
    }

    private fun manageForICCReaderUnknown(): String {
        WeipassLogger.log("ICC Reader Status Unknown")
        return CORE_ICC_READER_OUT_OF_ORDER
    }

    private fun manageForICCReaderNotExist(): String {
        WeipassLogger.log("ICC Reader Status Not Exist")
        return CORE_ICC_READER_NOT_SUPPORTED
    }

    private fun manageForContactReaderStatusOutOfOrder(): String {
        WeipassLogger.log("ICC Reader Status Out Of Order")
        return CORE_ICC_READER_OUT_OF_ORDER
    }

    /**
     * Contactless Reader Status
     */
    private fun analyzePICCReaderStatus(outDataBye: Byte): String {
        return when(outDataBye) {
            SDK_CONTACTLESS_READER_OK.toByte() -> manageForPICCReaderStatusOk()
            SDK_CONTACT_READER_UNKNOWN.toByte() -> manageForPICCReaderUnknown()
            SDK_CONTACTLESS_READER_NOT_EXIST.toByte() -> manageForPICCReaderNotExist()
            else -> manageForPICCReaderStatusOutOfOrder()
        }
    }

    private fun manageForPICCReaderStatusOk(): String {
        WeipassLogger.log("PICC Reader Status Ok")
        return CORE_PICC_READER_OK
    }

    private fun manageForPICCReaderUnknown(): String {
        WeipassLogger.log("PICC Reader Status Unknown")
        return CORE_PICC_READER_OUT_OF_ORDER
    }

    private fun manageForPICCReaderNotExist(): String {
        WeipassLogger.log("PICC Reader Status Not Exist")
        return CORE_PICC_READER_NOT_SUPPORTED
    }

    private fun manageForPICCReaderStatusOutOfOrder(): String {
        WeipassLogger.log("PICC Reader Status Out of Order")
        return CORE_PICC_READER_OUT_OF_ORDER
    }

}