package global.citytech.nepsadmin.data.response

import android.net.Uri
import global.citytech.nepsadmin.presentation.main.MainViewModel

data class Logo(
    var appWallpaper: String? = null,
    var displayLogo: String? = null,
    var printLogo: String? = null
){
    companion object{
        val TABLE_NAME = "logo"
        val URI = Uri.parse("content://${MainViewModel.AUTHORITY}/$TABLE_NAME")
        const val COLUMN_ID = "id"
        const val COLUMN_APP_WALLPAPER = "app_wallpaper"
        const val COLUMN_DISPLAY_LOGO = "display_logo"
        const val COLUMN_PRINT_LOGO = "print_logo"
    }
}