package global.citytech.finposframework.switches.reconciliation;

import global.citytech.finposframework.switches.ResponseParameter;

/**
 * Created by Unique Shakya on 9/22/2020.
 */
public interface ReconciliationResponseParameter extends ResponseParameter {
}
