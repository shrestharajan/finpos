package global.citytech.finpos.processor.neps.reconciliation.check;

import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reconciliation.check.CheckReconciliationResponseParameter;

public class CheckReconciliationResponseModel implements CheckReconciliationResponseParameter, UseCase.Response {
    private final boolean reconciliationRequired;

    public CheckReconciliationResponseModel(boolean reconciliationRequired) {
        this.reconciliationRequired = reconciliationRequired;
    }

    public boolean isReconciliationRequired() {
        return reconciliationRequired;
    }
}
