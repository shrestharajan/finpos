package global.citytech.finposframework.iso8583;

public enum EncodingType {
    /*
    30313233 (ASCII for "0123")
     */
    ASCII,
    /*
    "000000" is represented with just three bytes whose hex values are "00 00 00"
     */
    BCD,
    /*
    Binary format for example decimal 2 = 10 in Binary
     */
    BINARY,
    /*
      In order to cover most data encoding type. Always take one byte of it's length
     */
    UTF_8,
    /*
        If contains unicode character normally has 4 byte
     */
    UNICODE
}
