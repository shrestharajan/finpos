package global.citytech.finpos.merchant.domain.usecase.core.refund

import global.citytech.finpos.merchant.domain.model.purchase.RefundResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.refund.RefundRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.refund.ManualRefundRequestItem
import global.citytech.finpos.merchant.presentation.model.refund.RefundRequestItem
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/28/2020.
 */
class CoreRefundUseCase(private val refundRepository: RefundRepository) {
    fun refund(configurationItem: ConfigurationItem, refundRequestItem: RefundRequestItem):
            Observable<RefundResponseEntity> {
        return refundRepository.refund(configurationItem, refundRequestItem)
    }

    fun manualRefund(configurationItem: ConfigurationItem, manualRefundRequestItem: ManualRefundRequestItem):
            Observable<RefundResponseEntity> {
        return refundRepository.manualRefund(configurationItem, manualRefundRequestItem)
    }
}