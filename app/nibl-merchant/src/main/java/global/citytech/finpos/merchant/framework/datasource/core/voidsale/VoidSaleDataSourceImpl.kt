package global.citytech.finpos.merchant.framework.datasource.core.voidsale

import android.app.Application
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.voidsale.VoidSaleDataSource
import global.citytech.finpos.merchant.data.datasource.device.CardSource
import global.citytech.finpos.merchant.data.datasource.device.PrinterSource
import global.citytech.finpos.merchant.datasource.core.voidsale.VoidSaleUtil
import global.citytech.finpos.merchant.domain.model.voidsale.VoidSaleRequestEntity
import global.citytech.finpos.merchant.domain.model.voidsale.VoidSaleResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionAuthenticatorImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.*
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.data.mapToVoidSaleResponseUIModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.voidsale.VoidSaleRequestItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finposframework.repositories.ApplicationRepository
import global.citytech.finposframework.repositories.TerminalRepository
import global.citytech.finposframework.repositories.TransactionRepository
import global.citytech.finposframework.switches.ProcessorManager
import global.citytech.finposframework.usecases.controllers.DeviceController
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator
import io.reactivex.Observable

/**
 * Created by Rishav Chudal on 9/18/20.
 */
class VoidSaleDataSourceImpl(val application: Application) : VoidSaleDataSource {

    override fun doVoidSale(
        configurationItem: ConfigurationItem,
        voidSaleRequestItem: VoidSaleRequestItem
    ): Observable<VoidSaleResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = prepareTerminalRepository(configurationItem)
        val transactionRequest = VoidSaleUtil.prepareVoidSaleTransactionRequest(voidSaleRequestItem)

        val voidSaleRequestEntity = VoidSaleRequestEntity(
            transactionRepository = prepareTransactionRepository(),
            transactionRequest = transactionRequest,
            deviceController = prepareDeviceController(),
            readCardService = prepareCardService(),
            transactionAuthenticator = prepareTransactionAuthenticator(),
            printerService = preparePrinterService(),
            applicationRepository = prepareApplicationRepository(),
            ledService = LedSourceImpl(NiblMerchant.INSTANCE),
            soundService = SoundSourceImpl(NiblMerchant.INSTANCE)

        )
        val voidSaleRequester = ProcessorManager.getInterface(
            terminalRepository,
            NotificationHandler
        ).voidSaleRequester
        return Observable.fromCallable {
            voidSaleRequester.execute(voidSaleRequestEntity.mapToModel())
                .mapToVoidSaleResponseUIModel()
        }
    }


    private fun prepareTerminalRepository(
        configurationItem: ConfigurationItem
    ): TerminalRepository {
        return TerminalRepositoryImpl(configurationItem)
    }

    private fun prepareTransactionRepository(): TransactionRepository {
        return TransactionRepositoryImpl(
            NiblMerchant.INSTANCE
        )
    }

    private fun prepareDeviceController(): DeviceController {
        return DeviceControllerImpl(NiblMerchant.INSTANCE)
    }

    private fun prepareCardService(): CardSource {
        return CardSourceImpl(NiblMerchant.getActivityContext())
    }

    private fun prepareTransactionAuthenticator(): TransactionAuthenticator {
        return TransactionAuthenticatorImpl()
    }

    private fun preparePrinterService(): PrinterSource {
        return PrinterSourceImpl(NiblMerchant.INSTANCE)
    }

    private fun prepareApplicationRepository(): ApplicationRepository {
        return ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
    }
}