package global.citytech.finpos.merchant.presentation.model.qr.refund

data class QrRefundRequest(
    val merchant_pan: String,
    val terminal_id: String,
    val txn_number: String,
    val amount: String
) {
}