package global.citytech.finpos.merchant.presentation.qroperators

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.dynamicqr.QrType
import global.citytech.finpos.merchant.presentation.model.qroperator.QrOperatorItem
import global.citytech.finpos.merchant.utils.AppConstant.FONE_PAY_ID
import global.citytech.finpos.merchant.utils.AppConstant.MOCO_ID

/**
 * @author sachin
 */
class QrOperatorsAdapter(
    private var qrOperators: List<QrOperatorItem>,
    private val listener: QrOperatorListener
) :
    RecyclerView.Adapter<QrOperatorsAdapter.QrOperatorViewHolder>() {

    lateinit var context: Context

    class QrOperatorViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QrOperatorViewHolder {
        context = parent.context
        return QrOperatorViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_qr_operator, parent, false)
        )
    }

    override fun onBindViewHolder(holder: QrOperatorViewHolder, position: Int) {
        val qrOperatorItem = qrOperators[position]

        val logoView = holder.itemView.findViewById<ImageView>(R.id.iv_qr_operator)
        if (qrOperatorItem.id.equals(FONE_PAY_ID)) {
            logoView.setImageResource(R.drawable.ic_fone_pay)
        }else {
            if (QrType.getByCodes(qrOperatorItem.name) == QrType.NQR){
                loadQrLogo(qrOperatorItem.logo, logoView, R.drawable.ic_nepal_pay_qr)
            }else if(QrType.getByCodes(qrOperatorItem.name) == QrType.FONEPAY){
                loadQrLogo(qrOperatorItem.logo, logoView, R.drawable.ic_fone_pay)
            }else{
                loadQrLogo(qrOperatorItem.logo, logoView, R.drawable.banner_loading)
            }
        }

        holder.itemView.findViewById<TextView>(R.id.tv_qr_operator_title)
            .setText(qrOperatorItem.name)

        holder.itemView.findViewById<RelativeLayout>(R.id.rl_qr_operator)
            .setOnClickListener{
                listener.onQrOperatorItemClicked(qrOperatorItem)
            }
    }

    private fun loadQrLogo(logoUrl: String, logoView: ImageView, qrDefaultLogo: Int){
        val options: RequestOptions = RequestOptions()
            .fitCenter()
            .placeholder(qrDefaultLogo)

        Glide.with(context)
            .load(logoUrl)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .apply(options)
            .into(logoView)
    }

    override fun getItemCount(): Int {
        return qrOperators.size
    }

    interface QrOperatorListener {
        fun onQrOperatorItemClicked(qrOperatorItem: QrOperatorItem)
    }
}