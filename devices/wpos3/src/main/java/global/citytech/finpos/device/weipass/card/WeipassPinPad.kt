package global.citytech.finpos.device.weipass.card

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.opengl.Visibility
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.material.button.MaterialButton
import com.wiseasy.emvprocess.bean.PinPadConfig
import com.wiseasy.emvprocess.utils.ByteUtil
import global.citytech.finpos.device.weipass.R
import global.citytech.finpos.device.weipass.card.handler.WeipassStateHandler
import global.citytech.finpos.device.weipass.utils.*
import global.citytech.finposframework.hardware.io.cards.PinBlock
import global.citytech.finposframework.utility.StringUtils
import wangpos.sdk4.libbasebinder.Core
import java.lang.ref.WeakReference
import java.util.concurrent.CountDownLatch

/**
 * Created by Rishav Chudal on 5/29/20.
 */
class WeipassPinPad {
    private var contextWeakReference: WeakReference<Context>? = null
    private var pinpadDialog: WeakReference<Dialog>? = null
    private var pinPadConfig: PinPadConfig? = null
    private var tvPinpadDisplay: WeakReference<TextView>? = null
    private var tvAmountDisplay: WeakReference<TextView>? = null
    private var ivCurrency: WeakReference<AppCompatTextView>? = null
    private var tvCardNumber: WeakReference<TextView>? = null
    private var tvExpiry: WeakReference<TextView>? = null
    private var tvScheme: WeakReference<TextView>? = null
    private var tvCardHolderName: WeakReference<TextView>? = null
    private var tvTxnType: WeakReference<TextView>? = null
    private var pinPadDisplayCount = 0
    private val pinPadMessage = "CUSTOMER PIN ENTRY"
    private val pinPadRetryMessage = "CUSTOMER PIN RETRY"

    /**
     *  func displayPinpad
     */
    fun displayPinpad() {
        WeipassLogger.log("WeipassPinPad ::: displayPinpad")
        contextWeakReference = WeipassStateHandler.contextWeakReference
        if (contextWeakReference!!.get() != null &&
            WeipassStateHandler.transState != WeipassTransState.FAILURE
        ) {
            val countDownLatch = CountDownLatch(1)
            (contextWeakReference!!.get()!! as Activity).runOnUiThread {
                displayPinpadItems(countDownLatch)
            }
            try {
                countDownLatch.await()
            } catch (ex: Exception) {
                WeipassLogger.log("Exception ::: ".plus(ex.message))
            }
        } else {
            WeipassLogger.log("WeipassPinPad ::: Context is null")
            WeipassStateHandler.transState = WeipassTransState.PROCESSING_ERROR
        }
    }

    private fun displayPinpadItems(countDownLatch: CountDownLatch) {
        val context = contextWeakReference!!.get()!!
        try {
            val view = LayoutInflater.from(context).inflate(R.layout.layout_pinpad, null)
            preparePinpadConfig(view)
            preparePinpadDialog(view)
        } catch (ex: Exception) {
            WeipassLogger.log("Exception ::: ".plus(ex.message))
            WeipassStateHandler.transState = WeipassTransState.PROCESSING_ERROR
            WeipassDeviceUtils.callForSdkBreakOffCommand(context)
        }
        countDownLatch.countDown()
    }

    private fun preparePinpadConfig(view: View) {
        pinPadConfig = PinPadConfig()
        pinPadConfig!!.btn0 = WeakReference(view.findViewById(R.id.button0))
        pinPadConfig!!.btn1 = WeakReference(view.findViewById(R.id.button1))
        pinPadConfig!!.btn2 = WeakReference(view.findViewById(R.id.button2))
        pinPadConfig!!.btn3 = WeakReference(view.findViewById(R.id.button3))
        pinPadConfig!!.btn4 = WeakReference(view.findViewById(R.id.button4))
        pinPadConfig!!.btn5 = WeakReference(view.findViewById(R.id.button5))
        pinPadConfig!!.btn6 = WeakReference(view.findViewById(R.id.button6))
        pinPadConfig!!.btn7 = WeakReference(view.findViewById(R.id.button7))
        pinPadConfig!!.btn8 = WeakReference(view.findViewById(R.id.button8))
        pinPadConfig!!.btn9 = WeakReference(view.findViewById(R.id.button9))
        pinPadConfig!!.viewCancel = WeakReference(view.findViewById(R.id.imgbtn_cancel))
        pinPadConfig!!.viewConfirm = WeakReference(view.findViewById(R.id.imgbtn_confirm))
        pinPadConfig!!.viewClean = WeakReference(view.findViewById(R.id.imgbtn_clean))
        view.findViewById<LinearLayout>(R.id.layoutPinpadRoot).visibility = View.INVISIBLE
        prepareForCancelPin()
        tvPinpadDisplay = WeakReference(view.findViewById(R.id.tv_display))
        tvAmountDisplay = WeakReference(view.findViewById(R.id.tv_amount))
        ivCurrency = WeakReference(view.findViewById(R.id.tv_currency))
        tvCardNumber = WeakReference(view.findViewById(R.id.tv_card_number))
        tvExpiry = WeakReference(view.findViewById(R.id.tv_expiry_date))
        tvScheme = WeakReference(view.findViewById(R.id.tv_scheme))
        tvCardHolderName = WeakReference(view.findViewById(R.id.tv_card_holder_name))
        tvTxnType = WeakReference(view.findViewById(R.id.tv_txn_type))
    }

    private fun preparePinpadDialog(view: View) {
        val context = contextWeakReference!!.get()!!
        pinpadDialog = WeakReference(
            Dialog(
                context,
                R.style.BaseDialog
            )
        )
        if (pinpadDialog!!.get() == null) {
            onPinpadDialogNull()
        } else {
            proceedToPrepareDialogWindow(view)
        }
    }

    private fun onPinpadDialogNull() {
        WeipassLogger.log("Pinpad Dialog is null")
        WeipassStateHandler.transState = WeipassTransState.PROCESSING_ERROR
    }

    private fun proceedToPrepareDialogWindow(view: View) {
        val dialogWindow = pinpadDialog!!.get()!!.window
        if (dialogWindow == null) {
            onDialogWindowNull()
        } else {
            prepareDialogWindow(view, dialogWindow)
        }
    }

    private fun onDialogWindowNull() {
        val context = contextWeakReference!!.get()!!
        WeipassLogger.log("Dialog Window is null")
        WeipassStateHandler.transState = WeipassTransState.PROCESSING_ERROR
        WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext)
    }

    private fun prepareDialogWindow(view: View, dialogWindow: Window) {
        dialogWindow.setGravity(Gravity.BOTTOM)
        dialogWindow.setBackgroundDrawable(null)
        val layoutParam = dialogWindow.attributes
        layoutParam.x = 0
        layoutParam.y = 0
        view.measure(0, 0)
        layoutParam.height = view.measuredHeight
        dialogWindow.attributes = layoutParam
        pinpadDialog!!.get()!!.setContentView(view)
        pinpadDialog!!.get()!!.show()
    }

    private fun prepareForCancelPin() {
        (pinPadConfig!!.viewClean.get() as MaterialButton)
            .setIconResource(R.drawable.ic_clear_white_48dp)

    }

    /**
     * func getPINPADButton
     */
    fun getPinPadButton(): PinPadConfig {
        WeipassLogger.log("WeipassPinPad ::: getPinPadButton")
        this.contextWeakReference = WeipassStateHandler.contextWeakReference
        return pinPadConfig!!
    }

    /**
     * func refreshPINPADLayout
     */
    fun refreshPINPADLayout(
        pinType: Int,
        totalTryForOfflinePin: Int,
        remainingTryForOfflinePin: Int,
        numberOrder: String
    ) {
        WeipassLogger.log("WeipassPinPad ::: refreshPINPADLayout")
        contextWeakReference = WeipassStateHandler.contextWeakReference
        if (contextWeakReference!!.get() != null &&
            WeipassStateHandler.transState !== WeipassTransState.PROCESSING_ERROR
        ) {
            WeipassLogger.log("refresh pin pad layout")
            val countDownLatch = CountDownLatch(1)
            (contextWeakReference!!.get() as Activity?)!!.runOnUiThread()
            {
                refreshPinPadItems(pinType, numberOrder, countDownLatch)
            }

            try {
                countDownLatch.await()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        } else {
            WeipassLogger.log("no refresh pin pad layout")
            WeipassStateHandler.transState = WeipassTransState.PROCESSING_ERROR
        }
    }

    private fun refreshPinPadItems(
        pinType: Int,
        numberOrder: String,
        countDownLatch: CountDownLatch
    ) {
        try {
            try {
                tvPinpadDisplay!!.get()!!.text = ""
                ivCurrency!!.get()!!.text = WeipassStateHandler.readCardRequest!!.currencyName
                if (WeipassStateHandler.pinPadAmountMessage.isNullOrEmptyOrBlank())
                    tvAmountDisplay!!.get()!!.text = retrieveAmountMessage()
                else
                    tvAmountDisplay!!.get()!!.text = WeipassStateHandler.pinPadAmountMessage
                tvCardNumber!!.get()!!.text = retrieveCardNumber()
                tvExpiry!!.get()!!.text = retrieveExpiry()
                tvScheme!!.get()!!.text = retrieveScheme()
                tvCardHolderName!!.get()!!.text = retrieveCardHolderName()
                tvTxnType!!.get()!!.text = retrieveTxnType()
            } catch (e: Exception) {
                WeipassLogger.log("EXCEPTION WHILE DISPLAYING CARD SUMMARY ::: " + e.localizedMessage)
            }

            pinPadDisplayCount++
            WeipassLogger.log("Pinpad Display count ::: $pinPadDisplayCount")
            manageForPinType(pinType)
            pinPadConfig!!.btn1.get()!!.text = numberOrder[0].toString() + ""
            pinPadConfig!!.btn2.get()!!.text = numberOrder[1].toString() + ""
            pinPadConfig!!.btn3.get()!!.text = numberOrder[2].toString() + ""
            pinPadConfig!!.btn4.get()!!.text = numberOrder[3].toString() + ""
            pinPadConfig!!.btn5.get()!!.text = numberOrder[4].toString() + ""
            pinPadConfig!!.btn6.get()!!.text = numberOrder[5].toString() + ""
            pinPadConfig!!.btn7.get()!!.text = numberOrder[6].toString() + ""
            pinPadConfig!!.btn8.get()!!.text = numberOrder[7].toString() + ""
            pinPadConfig!!.btn9.get()!!.text = numberOrder[8].toString() + ""
            pinPadConfig!!.btn0.get()!!.text = numberOrder[9].toString() + ""
            pinpadDialog!!.get()!!.findViewById<LinearLayout>(R.id.layoutPinpadRoot).visibility =
                View.VISIBLE
        } catch (ex: Exception) {
            WeipassLogger.log("Exception ::: ".plus(ex.message))
            WeipassStateHandler.transState = WeipassTransState.PROCESSING_ERROR
        }
        countDownLatch.countDown()
    }

    private fun manageForPinType(pinType: Int) {
        when (pinType) {
            1 -> manageForOnlinePin()
            2 -> manageForOfflinePin()
        }
    }

    private fun manageForOnlinePin() {
        WeipassLogger.log("Pin Type ::: Online Pin")
        val pinpadMsg = WeipassStateHandler.pinpadMessage
        if (pinPadDisplayCount > 1) {
            tvPinpadDisplay!!.get()!!.hint = pinPadRetryMessage
        } else {
            if (pinpadMsg.isNullOrEmptyOrBlank()) {
                tvPinpadDisplay!!.get()!!.hint = pinPadMessage
            } else {
                tvPinpadDisplay!!.get()!!.hint = pinpadMsg
            }
        }
    }

    private fun manageForOfflinePin() {
        WeipassLogger.log("Pin Type ::: Offline Pin")
        val pinpadMsg = WeipassStateHandler.pinpadMessage
        if (pinPadDisplayCount > 1) {
            tvPinpadDisplay!!.get()!!.hint = pinPadRetryMessage
        } else {
            if (pinpadMsg.isNullOrEmptyOrBlank()) {
                tvPinpadDisplay!!.get()!!.hint = pinPadMessage
            } else {
                tvPinpadDisplay!!.get()!!.hint = pinpadMsg
            }
        }
    }

    private fun retrieveAmountMessage(): String {
        if (WeipassStateHandler.readCardRequest!!.amount!! == "0.00".toBigDecimal()) {
            ivCurrency!!.get()!!.text = ""
            return ""
        } else {
            ivCurrency!!.get()!!.text = WeipassStateHandler.readCardRequest!!.currencyName
            return StringUtils.formatAmountTwoDecimal(WeipassStateHandler.readCardRequest!!.amount!!)
        }

    }


    private fun retrieveCardNumber(): String {
        return if (!WeipassStateHandler.cardSummary!!.primaryAccountNumber.isNullOrEmptyOrBlank()) {
            val pan =
                StringUtils.encodeAccountNumber(WeipassStateHandler.cardSummary!!.primaryAccountNumber)
            val n = 4
            val str =
                java.lang.StringBuilder(pan!!)
            var idx = str.length - n
            while (idx > 0) {
                str.insert(idx, " ")
                idx -= n
            }
            str.toString()
        } else {
            ""
        }

    }

    private fun retrieveExpiry(): String {
        return if (!WeipassStateHandler.cardSummary!!.expiryDate.isNullOrEmptyOrBlank()) {
            val expiryDate =
                WeipassStateHandler.cardSummary!!.expiryDate
            expiryDate!!.substring(2, 4).plus("/").plus(expiryDate.substring(0, 2))
        } else {
            ""
        }

    }

    private fun retrieveScheme(): String? {
        return if (!WeipassStateHandler.cardSummary!!.cardSchemeLabel.isNullOrEmptyOrBlank()) {
            WeipassStateHandler.cardSummary!!.cardSchemeLabel
        } else {
            ""
        }
    }

    private fun retrieveCardHolderName(): String {
        val cardHolderName =
            WeipassStateHandler.cardSummary!!.cardHolderName!!
        return if (!cardHolderName.isNullOrEmptyOrBlank()) {
            if (cardHolderName.contains("/")) {
                val names = cardHolderName.split("/")
                var fullName = ""
                for (name in names) {
                    fullName = fullName.plus(name).plus(" ")
                }
                fullName
            } else {
                cardHolderName
            }
        } else {
            ""
        }

    }

    private fun retrieveTxnType(): String {
        return try {
            WeipassStateHandler.readCardRequest!!.transactionType.displayName
        } catch (ex: Exception) {
            ""
        }
    }

    /**
     * func updatePINPAD
     */
    fun updatePINPAD(count: Int) {
        WeipassLogger.log("WeipassPinPad ::: updatePINPAD")
        contextWeakReference = WeipassStateHandler.contextWeakReference
        if (contextWeakReference!!.get() != null &&
            WeipassStateHandler.transState != WeipassTransState.PINPAD_ERROR
        ) {
            (contextWeakReference!!.get()!! as Activity).runOnUiThread {
                updatePinPadItems(count)
            }
        } else {
            WeipassStateHandler.transState = WeipassTransState.PINPAD_ERROR
        }
    }

    private fun updatePinPadItems(count: Int) {
        try {
            if (count == 0) {
                prepareForCancelPin()
            } else {
                prepareForClearPin()
            }
            val cross = StringBuilder()
            for (i in 0 until count) {
                cross.append(" X ")
            }
            tvPinpadDisplay!!.get()!!.text = cross.toString()
            if (count == 0 || count > 6)
                tvPinpadDisplay!!.get()!!.textSize = 24f
            else
                tvPinpadDisplay!!.get()!!.textSize = 36f
        } catch (ex: Exception) {
            WeipassLogger.log("Exception ::: ".plus(ex.message))
            WeipassStateHandler.transState = WeipassTransState.PINPAD_ERROR
            WeipassDeviceUtils.callForSdkBreakOffCommand(contextWeakReference!!.get()!!)
        }
    }

    private fun prepareForClearPin() {
        (pinPadConfig!!.viewClean.get() as MaterialButton)
            .setIconResource(R.drawable.ic_backspace_white_24dp)
    }

    /**
     * func dismissPINPAD
     */
    fun dismissPINPAD() {
        WeipassLogger.log("WeipassPinPad ::: dismissPINPAD")
        if (pinpadDialog!!.get() != null) {
            pinpadDialog!!.get()!!.dismiss()
        }
    }

    /**
     * func evaluatePinPadResult
     */
    fun evaluatePinPadResult(
        pinInputResult: Int,
        isPinUpload: Int,
        pinLen: Int,
        pin: ByteArray?
    ) {
        WeipassLogger.log("WeipassPinPad ::: evaluatePinPadResult")
        when (pinInputResult.toByte()) {
            Core.PIN_QUIT_SUCCESS -> manageForPinQuitSuccess(isPinUpload, pinLen, pin)
            Core.PIN_QUIT_CANCEL -> manageForPinQuitCancel()
            Core.PIN_QUIT_BYPASS -> manageForPinQuitBypass()
            Core.PIN_QUIT_ERROR -> manageForPinQuitError()
            Core.PIN_QUIT_TIMEOUT -> manageForPinQuitTimeout()
            Core.PIN_QUIT_ERRORPAN -> manageForPinQuitErrorPAN()
        }
    }

    private fun manageForPinQuitSuccess(isPinUpload: Int, pinLen: Int, pin: ByteArray?) {
        WeipassLogger.log("WeipassPinPad ::: PinPadResult ::: PIN_QUIT_SUCCESS")
        if (isPinUpload != 0x00 && pinLen > 0) {
            val pinBlock = ByteUtil.bytes2HexString(pin)
            WeipassLogger.debug("Generated PinBlock ::: ".plus(pinBlock))
            WeipassStateHandler.pinBlock =
                PinBlock(
                    pinBlock,
                    false
                )
        } else {
            WeipassStateHandler.pinBlock =
                PinBlock(
                    WeipassConstant.EMPTY_STRING,
                    true
                )
        }
        WeipassStateHandler.transState = WeipassTransState.PINPAD_SUCCESS
    }

    private fun manageForPinQuitCancel() {
        WeipassLogger.log("WeipassPinPad ::: PinPadResult ::: PIN_QUIT_CANCEL")
        WeipassStateHandler.pinBlock =
            PinBlock(
                WeipassConstant.MSG_PIN_ENTRY_CANCELLED,
                false
            )
        WeipassStateHandler.transState = WeipassTransState.PINPAD_CANCEL
    }

    private fun manageForPinQuitBypass() {
        WeipassLogger.log("WeipassPinPad ::: PinPadResult ::: PIN_QUIT_BYPASS")
        WeipassStateHandler.pinBlock =
            PinBlock(
                WeipassConstant.MSG_PINPAD_BYPASS,
                false
            )
        WeipassStateHandler.transState = WeipassTransState.PINPAD_BYPASS
    }

    private fun manageForPinQuitError() {
        WeipassLogger.log("WeipassPinPad ::: PinPadResult ::: PIN_QUIT_ERROR")
        WeipassStateHandler.pinBlock =
            PinBlock(
                WeipassConstant.MSG_PINPAD_FAILURE,
                false
            )
        WeipassStateHandler.transState = WeipassTransState.PROCESSING_ERROR
    }

    private fun manageForPinQuitTimeout() {
        WeipassLogger.log("WeipassPinPad ::: PinPadResult ::: PIN_QUIT_TIMEOUT")
        WeipassStateHandler.pinBlock =
            PinBlock(
                WeipassConstant.MSG_PINPAD_TIMEOUT,
                false
            )
        WeipassStateHandler.transState = WeipassTransState.PINPAD_TIMEOUT
    }

    private fun manageForPinQuitErrorPAN() {
        WeipassLogger.log("WeipassPinPad ::: PinPadResult ::: PIN_QUIT_ERRORPAN")
        WeipassStateHandler.pinBlock =
            PinBlock(
                WeipassConstant.MSG_PINPAD_PAN_ERROR,
                false
            )
        WeipassStateHandler.transState = WeipassTransState.PROCESSING_ERROR
    }
}