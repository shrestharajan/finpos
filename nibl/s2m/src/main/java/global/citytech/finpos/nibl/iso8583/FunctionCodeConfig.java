package global.citytech.finpos.nibl.iso8583;

import global.citytech.finposframework.switches.FunctionCode;

public class FunctionCodeConfig {
    public static String getValue(FunctionCode functionCode) {
        switch (functionCode) {
            case LOG_ON:
                return "801";
            case KEY_EXCHANGE:
                return "811";
            case TRANSACTION:
                return "200";
            case REVERSAL:
                return "400";
            case RECONCILICATION:
                return "504";
            case GREEN_PIN:
                return "100";
            case PIN_CHANGE:
                return "100";
            case BALANCE_INQUIRY:
                return "0011";
            case CASH_IN:
                return "0011";
            case MINI_STATEMENT:
                return "0011";
            default:
                return "000";
        }
    }
}
