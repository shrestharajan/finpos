package global.citytech.finposframework.iso8583;

public class LengthConfig {
    private LengthType lengthType;
    private EncodingType encodingType;
    // if the <p> lengthType is LLVAR or LLLVAR , the <p>length</p> variable represent length/size of transmitted data
    private int length;

    public LengthConfig(int length, LengthType lengthType, EncodingType encodingType) {
        this.lengthType = lengthType;
        this.encodingType = encodingType;
        this.length = length;
    }

    public LengthConfig(LengthType lengthType, EncodingType encodingType) {
        this.lengthType = lengthType;
        this.encodingType = encodingType;
    }

    /***
     * If no length encoding is provided default is UTF_8 which means 1 byte and total length = 1 byte * @param length
     * @param length
     * @param lengthType
     */
    public LengthConfig(int length, LengthType lengthType) {
        this(length, lengthType, EncodingType.UTF_8);
    }

    public LengthType getLengthType() {
        return lengthType;
    }

    public EncodingType getEncodingType() {
        return encodingType;
    }

    public int getLength() {
        return length;
    }

    public enum LengthType {
        FIXED,
        /**
         * A variable length alphanumeric value with a 2-digit header length.
         */
        LLVAR,
        /**
         * A variable length alphanumeric value with a 3-digit header length.
         */
        LLLVAR
    }
}
