package global.citytech.finpos.merchant.presentation.model.command.pull

import global.citytech.finpos.merchant.domain.model.app.Command
import global.citytech.finpos.merchant.domain.model.app.Status
import global.citytech.finpos.merchant.domain.model.app.getCommandType
import global.citytech.finposframework.utility.StringUtils

/**
 * Created by Unique Shakya on 9/21/2021.
 */
data class PullCommandResponse(
    var id: String,
    var command: String
)

fun PullCommandResponse.mapToCommand(): Command {
    return Command(
        id = this.id,
        commandType = this.command.getCommandType(),
        status = Status.OPEN,
        timeStamp = StringUtils.dateTimeStampUpToMillis(),
        remarks = ""
    )
}