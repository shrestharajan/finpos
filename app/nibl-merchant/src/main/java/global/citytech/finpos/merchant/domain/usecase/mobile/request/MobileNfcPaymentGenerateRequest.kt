package global.citytech.finpos.merchant.domain.usecase.mobile.request

/**
 * @author SIDDHARTHA GHIMIRE
 */
data class MobileNfcPaymentGenerateRequest(
    var merchantId: String,
    var terminalId: String,
    var amount: String,
    var transactionCurrency: String,
    var initiationMethod: String,
    var issuerCode: String,
    var payeerPan: String,
    var nfcToken: String
) {
    constructor(issuerCode: String, payeerPan: String, nfcToken: String) : this(
        "", "", "", "", "", issuerCode, payeerPan, nfcToken
    )
}

enum class Aid(
    val payeerPan:String,
    val aid: String,
    val ISRCode: String
) {
    FONEPAY("FONEPAY","F0010909070901", ""),
    NQR("NQR","F222222124", ""),
    MBL("MBL","A000029524", "NFCISR0001");

    companion object {
        private val aidMap = values().associateBy(Aid::aid)

        fun fromAid(aid: String): Aid? = aidMap[aid]
    }
}


