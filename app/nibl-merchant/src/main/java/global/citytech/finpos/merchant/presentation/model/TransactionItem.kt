package global.citytech.finpos.merchant.presentation.model

import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.domain.model.app.TransactionLog
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog

/**
 * Created by Saurav Ghimire on 5/5/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


data class TransactionItem(
    val stan: String? = null,
    val invoiceNumber: String? = null,
    val date: String? = null,
    val time: String? = null,
    val cardScheme: String? = null,
    val pan: String? = null,
    val cardHolderName: String? = null,
    val expiryDate: String? = null,
    val amount: String? = null,
    val currencyName: String? = null,
    val transactionType: String? = null,
    val isEmiTransaction: Boolean = false
)

fun TransactionLog.mapToPresentation(): TransactionItem {
    val readCardResponse = Jsons.fromJsonToObj(this.readCardResponse, ReadCardResponse::class.java)
    val receiptLog: ReceiptLog? = Jsons.fromJsonToObj(this.receiptLog, ReceiptLog::class.java)
    val transactionType =
        Jsons.fromJsonToObj(transactionType, TransactionType::class.java)
    val amount =
        if (transactionType == TransactionType.VOID) origTransactionAmount
        else transactionAmount
    return TransactionItem(
        stan = this.stan,
        invoiceNumber = this.invoiceNumber,
        date = if (receiptLog == null) "" else {
            receiptLog.date
        },
        time = if (receiptLog == null) "" else {
            receiptLog.time
        },
        cardScheme = readCardResponse.cardDetails!!.cardSchemeLabel,
        pan = readCardResponse.cardDetails!!.primaryAccountNumber,
        cardHolderName = readCardResponse.cardDetails!!.cardHolderName,
        expiryDate = readCardResponse.cardDetails!!.expiryDate,
        amount = amount.toString(),
        currencyName = if (receiptLog == null) "" else {
            receiptLog.transactionInfo.transactionCurrencyName
        },
        transactionType = this.transactionType,
        isEmiTransaction = this.isEmiTransaction ?: false
    )
}

fun List<TransactionLog>.mapToPresentation() = this.map { it.mapToPresentation() }
