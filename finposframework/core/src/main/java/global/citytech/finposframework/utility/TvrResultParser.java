package global.citytech.finposframework.utility;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.hardware.io.cards.tvr.TvrResults;

/**
 * Created by Unique Shakya on 11/24/2020.
 */
public class TvrResultParser {

    private TvrResultParser() {
    }

    public static List<TvrResults> parseTvr(String tvr) {
        List<TvrResults> tvrResults = new ArrayList<>();
        byte[] tvrBytes = HEX.hexToBytes(tvr);
        parseFirstByte(tvrResults, tvrBytes);
        parseSecondByte(tvrResults, tvrBytes);
        parseThirdByte(tvrResults, tvrBytes);
        parseForthByte(tvrResults, tvrBytes);
        parseFifthByte(tvrResults, tvrBytes);
        return tvrResults;
    }

    private static void parseFifthByte(List<TvrResults> tvrResults, byte[] tvrBytes) {
        char ucMask = 0x80;
        for (int i = 8; i >= 1; i--) {
            if ((tvrBytes[4] & ucMask) > 0) {
                switch (i) {
                    case 8:
                        tvrResults.add(TvrResults.BYTE_5_BIT_8);
                        break;

                    case 7:
                        tvrResults.add(TvrResults.BYTE_5_BIT_7);
                        break;

                    case 6:
                        tvrResults.add(TvrResults.BYTE_5_BIT_6);
                        break;

                    case 5:
                        tvrResults.add(TvrResults.BYTE_5_BIT_5);
                        break;

                    case 4:
                        tvrResults.add(TvrResults.BYTE_5_BIT_4);
                        break;

                    case 3:
                        tvrResults.add(TvrResults.BYTE_5_BIT_3);
                        break;

                    case 2:
                        tvrResults.add(TvrResults.BYTE_5_BIT_2);
                        break;

                    case 1:
                        tvrResults.add(TvrResults.BYTE_5_BIT_1);
                        break;

                    default:
                        break;
                }
            }
            ucMask >>= 1;
        }
    }

    private static void parseForthByte(List<TvrResults> tvrResults, byte[] tvrBytes) {
        char ucMask = 0x80;
        for (int i = 8; i >= 1; i--) {
            if ((tvrBytes[3] & ucMask) > 0) {
                switch (i) {
                    case 8:
                        tvrResults.add(TvrResults.BYTE_4_BIT_8);
                        break;

                    case 7:
                        tvrResults.add(TvrResults.BYTE_4_BIT_7);
                        break;

                    case 6:
                        tvrResults.add(TvrResults.BYTE_4_BIT_6);
                        break;

                    case 5:
                        tvrResults.add(TvrResults.BYTE_4_BIT_5);
                        break;

                    case 4:
                        tvrResults.add(TvrResults.BYTE_4_BIT_4);
                        break;

                    case 3:
                        tvrResults.add(TvrResults.BYTE_4_BIT_3);
                        break;

                    case 2:
                        tvrResults.add(TvrResults.BYTE_4_BIT_2);
                        break;

                    case 1:
                        tvrResults.add(TvrResults.BYTE_4_BIT_1);
                        break;

                    default:
                        break;
                }
            }
            ucMask >>= 1;
        }
    }

    private static void parseThirdByte(List<TvrResults> tvrResults, byte[] tvrBytes) {
        char ucMask = 0x80;
        for (int i = 8; i >= 1; i--) {
            if ((tvrBytes[2] & ucMask) > 0) {
                switch (i) {
                    case 8:
                        tvrResults.add(TvrResults.BYTE_3_BIT_8);
                        break;

                    case 7:
                        tvrResults.add(TvrResults.BYTE_3_BIT_7);
                        break;

                    case 6:
                        tvrResults.add(TvrResults.BYTE_3_BIT_6);
                        break;

                    case 5:
                        tvrResults.add(TvrResults.BYTE_3_BIT_5);
                        break;

                    case 4:
                        tvrResults.add(TvrResults.BYTE_3_BIT_4);
                        break;

                    case 3:
                        tvrResults.add(TvrResults.BYTE_3_BIT_3);
                        break;

                    case 2:
                        tvrResults.add(TvrResults.BYTE_3_BIT_2);
                        break;

                    case 1:
                        tvrResults.add(TvrResults.BYTE_3_BIT_1);
                        break;

                    default:
                        break;
                }
            }
            ucMask >>= 1;
        }
    }

    private static void parseSecondByte(List<TvrResults> tvrResults, byte[] tvrBytes) {
        char ucMask = 0x80;
        for (int i = 8; i >= 1; i--) {
            if ((tvrBytes[1] & ucMask) > 0) {
                switch (i) {
                    case 8:
                        tvrResults.add(TvrResults.BYTE_2_BIT_8);
                        break;

                    case 7:
                        tvrResults.add(TvrResults.BYTE_2_BIT_7);
                        break;

                    case 6:
                        tvrResults.add(TvrResults.BYTE_2_BIT_6);
                        break;

                    case 5:
                        tvrResults.add(TvrResults.BYTE_2_BIT_5);
                        break;

                    case 4:
                        tvrResults.add(TvrResults.BYTE_2_BIT_4);
                        break;

                    case 3:
                        tvrResults.add(TvrResults.BYTE_2_BIT_3);
                        break;

                    case 2:
                        tvrResults.add(TvrResults.BYTE_2_BIT_2);
                        break;

                    case 1:
                        tvrResults.add(TvrResults.BYTE_2_BIT_1);
                        break;

                    default:
                        break;
                }
            }
            ucMask >>= 1;
        }
    }

    private static void parseFirstByte(List<TvrResults> tvrResults, byte[] tvrBytes) {
        char ucMask = 0x80;
        for (int i = 8; i >= 1; i--) {
            if ((tvrBytes[0] & ucMask) > 0) {
                switch (i) {
                    case 8:
                        tvrResults.add(TvrResults.BYTE_1_BIT_8);
                        break;

                    case 7:
                        tvrResults.add(TvrResults.BYTE_1_BIT_7);
                        break;

                    case 6:
                        tvrResults.add(TvrResults.BYTE_1_BIT_6);
                        break;

                    case 5:
                        tvrResults.add(TvrResults.BYTE_1_BIT_5);
                        break;

                    case 4:
                        tvrResults.add(TvrResults.BYTE_1_BIT_4);
                        break;

                    case 3:
                        tvrResults.add(TvrResults.BYTE_1_BIT_3);
                        break;

                    case 2:
                        tvrResults.add(TvrResults.BYTE_1_BIT_2);
                        break;

                    case 1:
                        tvrResults.add(TvrResults.BYTE_1_BIT_1);
                        break;

                    default:
                        break;
                }
            }
            ucMask >>= 1;
        }
    }
}
