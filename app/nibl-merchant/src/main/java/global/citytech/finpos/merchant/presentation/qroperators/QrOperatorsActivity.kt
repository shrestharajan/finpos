package global.citytech.finpos.merchant.presentation.qroperators

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityQrOperatorsBinding
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.fonepayqr.FonePayQrActivity
import global.citytech.finpos.merchant.presentation.model.qroperator.QrOperatorItem
import global.citytech.finpos.merchant.presentation.model.setting.QrConfiguration
import global.citytech.finpos.merchant.presentation.staticqr.StaticQrActivity
import global.citytech.finpos.merchant.presentation.utils.SpaceItemDecoration
import global.citytech.finpos.merchant.utils.AppConstant.FONE_PAY_ID
import global.citytech.finpos.merchant.utils.AppConstant.MOCO_ID
import kotlinx.android.synthetic.main.activity_qr_operators.*

class QrOperatorsActivity : AppBaseActivity<ActivityQrOperatorsBinding, QrOperatorsViewModel>(),
    QrOperatorsAdapter.QrOperatorListener {

    private var qrOperators = ArrayList<QrOperatorItem>()
    private var qrConfigurations = ArrayList<QrConfiguration>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        initListeners()
        observeQrOperators()
        observeQrConfigurations()
        getViewModel().getQrOperators()
    }

    private fun observeQrConfigurations() {
        getViewModel().qrConfigs.observe(this, Observer {
            this.qrConfigurations = it as ArrayList<QrConfiguration>
        })
    }

    private fun initListeners() {
        iv_back.setOnClickListener {
            this.finish()
        }
    }

    private fun observeQrOperators() {
        getViewModel().qrOperators.observe(this, Observer {
            this.qrOperators.clear()
            this.qrOperators.addAll(it)
            rv_qr_operators.adapter?.notifyDataSetChanged()
        })
    }

    private fun initViews() {
        val layoutManager = GridLayoutManager(this, 3)
        rv_qr_operators.layoutManager = layoutManager
        rv_qr_operators.adapter = QrOperatorsAdapter(qrOperators, this)
        rv_qr_operators.addItemDecoration(SpaceItemDecoration(4, 32, false))
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayout(): Int {
        return R.layout.activity_qr_operators
    }

    override fun getViewModel(): QrOperatorsViewModel {
        return ViewModelProviders.of(this).get(QrOperatorsViewModel::class.java)
    }

    override fun onQrOperatorItemClicked(qrOperatorItem: QrOperatorItem) {
        val intent: Intent
        if (qrOperatorItem.id.equals(FONE_PAY_ID)) {
            intent = Intent(this, FonePayQrActivity::class.java)
        } else {
            intent = Intent(this, StaticQrActivity::class.java)
            intent.putExtra(StaticQrActivity.QR_OPERATOR_ITEM, Jsons.toJsonObj(qrOperatorItem))
            intent.putExtra(
                StaticQrActivity.QR_CONFIGURATION,
                Jsons.toJsonObj(getQrConfiguration(qrOperatorItem))
            )
        }
        startActivity(intent)
    }

    private fun getQrConfiguration(qrOperatorItem: QrOperatorItem): QrConfiguration? {
        val qrConfiguration = qrConfigurations
            .filter { it -> it.qrOperator.equals(qrOperatorItem.id) }
            .firstOrNull()
        return qrConfiguration
    }
}