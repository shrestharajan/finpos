package global.citytech.finpos.merchant.presentation.transactions.card

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.common.extensions.getBundle
import global.citytech.common.extensions.openActivity
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.datasource.app.db.AppDatabase
import global.citytech.finpos.merchant.databinding.ActivityTransactionDetailBinding
import global.citytech.finpos.merchant.domain.model.app.TransactionLog
import global.citytech.finpos.merchant.presentation.auth.completion.AuthorisationCompletionActivity
import global.citytech.finpos.merchant.presentation.autoreversal.AutoReversalActivity
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.tipadjustment.TipsAdjustmentActivity
import global.citytech.finpos.merchant.presentation.transactions.TransactionsViewModel
import global.citytech.finpos.merchant.presentation.voidsale.VoidActivity
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finpos.merchant.utils.setOnlineClickListener
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog
import kotlinx.android.synthetic.main.activity_transaction_detail.*

class CardTransactionDetailActivity :
    AppBaseActivity<ActivityTransactionDetailBinding, TransactionsViewModel>() {

    private lateinit var txnType: TransactionType
    private lateinit var transactionLog: TransactionLog
    private lateinit var binding: ActivityTransactionDetailBinding
    private var progressDialog: ProgressDialog? = null
    private lateinit var stan: String
    private val logger = Logger(CardTransactionDetailActivity::class.java.name)
    private var toPerformTransaction: TransactionType? = null

    private var voidAllowedInTerminal = false
    private var tipAdjustmentAllowedInTerminal = false


    companion object {
        const val STAN = "_stan"
        const val RESULT_FINISH_HOST_ACTIVITY = 9898
        const val REQUEST_CODE = 8989
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTransactionDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        stan = getBundle().getString(STAN)!!

        initViews()
        initObserver()
        binding.setVariable(BR.canBeVoided, false)
        binding.setVariable(BR.canAdjustTip, false)
        binding.setVariable(BR.isEmiTransaction, false)
    }

    override fun onResume() {
        super.onResume()
        registerReprintBroadcastReceivers()
        getViewModel().checkAdditionalTransactionsAllowed()
    }

    override fun onPause() {
        unregisterReprintBroadCastReceivers()
        super.onPause()
    }

    private fun initObserver() {
        getViewModel().transaction.observe(this, Observer {
            transactionLog = it
            txnType = Jsons.fromJsonToObj(it.transactionType, TransactionType::class.java)
            val isVoidableTransaction =
                txnType == TransactionType.PURCHASE || txnType == TransactionType.CASH_ADVANCE

            var isEmiTransaction = false
            it.isEmiTransaction?.let {
                isEmiTransaction = it
            }

            var canBeVoided = if (tipAdjustmentAllowedInTerminal) {
                isVoidableTransaction && !it.transactionVoided!! &&
                        voidAllowedInTerminal && !isEmiTransaction && !transactionLog.tipAdjusted!!
            } else {
                isVoidableTransaction && !it.transactionVoided!! &&
                        voidAllowedInTerminal && !isEmiTransaction
            }


            val canAdjustTip =
                txnType == TransactionType.PURCHASE && !it.transactionVoided!! && !it.tipAdjusted!! &&
                        tipAdjustmentAllowedInTerminal && !isEmiTransaction


            var receiptLog = Jsons.fromJsonToObj(it.receiptLog, ReceiptLog::class.java)
            if (txnType == TransactionType.PRE_AUTH)
                displayPreAuthorizationActions(it, receiptLog)
            if (receiptLog == null) {
                tv_no_txn.visibility = View.VISIBLE
            } else {
                binding.setVariable(BR.canBeVoided, canBeVoided)
                binding.setVariable(BR.canAdjustTip, canAdjustTip)
                binding.setVariable(BR.receiptLog, receiptLog)
                binding.setVariable(BR.isEmiTransaction, isEmiTransaction)
            }
        })

        getViewModel().voidAllowedInTerminal.observe(this, Observer {
            voidAllowedInTerminal = it
        })

        getViewModel().tipAdjustmentAllowedInTerminal.observe(this, Observer {
            tipAdjustmentAllowedInTerminal = it
            getViewModel().getTransaction(stan)
        })

        getViewModel().transactionAvailable.observe(this, Observer {
            if (!it) {
                tv_no_txn.visibility = View.VISIBLE
            }
        })

        getViewModel().settlementActive.observe(this, Observer {
            if (it)
                showStartSettlementConfirmationDialog()
            else
                startSelectedTransaction()
        })

        getViewModel().isLoadingMessage.observe(this, Observer {
            it?.let {
                showProgress(it)
            }
        })

        getViewModel().isLoading.observe(this, Observer {
            if (it)
                showProgress()
            else
                hideProgress()

        })

        getViewModel().message.observe(this, Observer {
            showNormalMessage(
                MessageConfig.Builder()
                    .message(it)
                    .positiveLabel("OK")
                    .onPositiveClick { })
        })

        getViewModel().autoReversalPresent.observe(this, Observer {
            if (it)
                startAutoReversalActivity()
            else
                getViewModel().checkSettlementTime()
        })

        getViewModel().authorizationDeleted.observe(this, Observer {
            if (it) {
                showToast(getString(R.string.msg_success_pre_auth_delete))
                finish()
            }
        })

        getViewModel().canDeleteAuthorization.observe(this, Observer {
            binding.setVariable(BR.canDeletePreAuthorization, it)
        })
    }

    private fun displayPreAuthorizationActions(
        it: TransactionLog,
        receiptLog: ReceiptLog?
    ) {
        if (it.authorizationCompleted!!) {
            binding.setVariable(BR.canCompletePreAuthorization, false)
            binding.setVariable(BR.canDeletePreAuthorization, false)
        } else {
            binding.setVariable(BR.canCompletePreAuthorization, true)
            getViewModel().isPreAuthorizationExpired(receiptLog)
        }
    }

    private fun startAutoReversalActivity() {
        val intent = Intent(this, AutoReversalActivity::class.java)
        startActivityForResult(intent, AutoReversalActivity.REQUEST_CODE)
    }

    private fun showStartSettlementConfirmationDialog() {
        showNormalMessage(
            MessageConfig.Builder()
                .message(getString(R.string.msg_settlement_active))
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick { }
        )
    }

    private fun startSelectedTransaction() {
        toPerformTransaction?.let {
            when (it) {
                TransactionType.VOID -> startVoidActivity()
                TransactionType.TIP_ADJUSTMENT -> startTipAdjustmentActivity()
                TransactionType.AUTH_COMPLETION -> startAuthCompletionActivity()
                else -> logger.log("Unsupported transaction type")
            }
        }
    }

    private fun startVoidActivity() {
        val bundle = Bundle()
        if (txnType == TransactionType.PURCHASE) {
            bundle.putString(VoidActivity.MODE, VoidActivity.SALE)
        } else {
            bundle.putString(VoidActivity.MODE, VoidActivity.CASH)
        }
        bundle.putString(VoidActivity.INVOICE, transactionLog.invoiceNumber)
        bundle.putBoolean(VoidActivity.FROM_TRANSACTION_DETAIL, true)
        openActivity(VoidActivity::class.java, bundle)
        setResult(RESULT_FINISH_HOST_ACTIVITY)
        finish()
    }

    private fun startTipAdjustmentActivity() {
        val bundle = Bundle()
        bundle.putString(TipsAdjustmentActivity.INVOICE, transactionLog.invoiceNumber)
        bundle.putBoolean(TipsAdjustmentActivity.FROM_TRANSACTION_DETAIL, true)
        openActivity(TipsAdjustmentActivity::class.java, bundle)
        setResult(RESULT_FINISH_HOST_ACTIVITY)
        finish()
    }

    private fun startAuthCompletionActivity() {
        val bundle = Bundle()
        bundle.putString(AuthorisationCompletionActivity.INVOICE, transactionLog.invoiceNumber)
        bundle.putBoolean(AuthorisationCompletionActivity.FROM_TRANSACTION_DETAIL, true)
        openActivity(AuthorisationCompletionActivity::class.java, bundle)
        setResult(RESULT_FINISH_HOST_ACTIVITY)
        finish()
    }

    private fun initViews() {
        iv_back.setOnClickListener {
            onBackPressed()
        }

        btn_duplicate_receipt.setOnClickListener {
            getViewModel().duplicateReceipt(stan)
        }

        btn_void_txn.setOnlineClickListener {
            getViewModel().getConfiguration()
            toPerformTransaction = TransactionType.VOID
        }

        btn_tips_adjustment.setOnlineClickListener {
            getViewModel().getConfiguration()
            toPerformTransaction = TransactionType.TIP_ADJUSTMENT
        }

        btn_auth_completion.setOnlineClickListener {
            getViewModel().getConfiguration()
            toPerformTransaction = TransactionType.AUTH_COMPLETION
        }

        btn_auth_delete.setOnClickListener {
            showConfirmationMessage(MessageConfig.Builder()
                .message(getString(R.string.msg_confirmation_pre_auth_delete))
                .positiveLabel(getString(R.string.action_confirm))
                .onPositiveClick {
                    getViewModel().deletePreAuthorization(transactionLog)
                }
                .negativeLabel(getString(R.string.action_cancel))
                .onNegativeClick {

                })
        }
    }

    override fun getBindingVariable() = BR.viewModel

    override fun getLayout() = R.layout.activity_transaction_detail

    override fun getViewModel() = ViewModelProviders.of(this)[TransactionsViewModel::class.java]

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AutoReversalActivity.REQUEST_CODE)
            handleAutoReversalResult(resultCode)
    }

    private fun handleAutoReversalResult(resultCode: Int) {
        if (resultCode == Activity.RESULT_OK)
            getViewModel().checkSettlementTime()
        else
            getViewModel().message.value = getString(R.string.error_msg_auto_reversal_not_performed)
    }
}