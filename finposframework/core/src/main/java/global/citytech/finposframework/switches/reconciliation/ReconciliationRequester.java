package global.citytech.finposframework.switches.reconciliation;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 9/22/2020.
 */
public interface ReconciliationRequester<T extends ReconciliationRequestParameter,
        E extends ReconciliationResponseParameter> extends Request<T, E> {
}
