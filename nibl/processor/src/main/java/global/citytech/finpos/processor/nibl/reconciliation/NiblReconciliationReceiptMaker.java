package global.citytech.finpos.processor.nibl.reconciliation;

import global.citytech.finpos.nibl.iso8583.requestsender.settlement.SettlementRequest;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.switches.posmode.PosMode;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Unique Shakya on 9/22/2020.
 */
public class NiblReconciliationReceiptMaker {

    private RequestContext requestContext;
    private IsoMessageResponse isoMessageResponse;

    public NiblReconciliationReceiptMaker(RequestContext requestContext, IsoMessageResponse isoMessageResponse) {
        this.requestContext = requestContext;
        this.isoMessageResponse = isoMessageResponse;
    }

    public NiblReconciliationReceipt prepare(PosMode posMode) {
        SettlementRequest settlementRequest = (SettlementRequest) this.requestContext.getRequest();
        return NiblReconciliationReceipt.Builder.newInstance()
                .withRetailer(this.prepareRetailerInfo(requestContext))
                .withPerformance(this.preparePerformanceInfo(isoMessageResponse))
                .withReconciliationBatchNumber(settlementRequest.getBatchNumber())
                .withReconciliationTotals((NiblSettlementTotals) settlementRequest.getReconciliationTotals())
                .withReconciliationResult(this.prepareReconciliationResult(isoMessageResponse))
                .withHost("000048") // TODO change after confirmation from ishang
                .withApplicationVersion(settlementRequest.getApplicationVersion())
                .withPosMode(posMode)
                .build();
    }

    private String prepareReconciliationResult(IsoMessageResponse isoMessageResponse) {
        int responseCode = IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, 39);
        return responseCode == 0 ? ReconciliationReceiptLabels.SETTLEMENT_SUCCESS : ReconciliationReceiptLabels.SETTLEMENT_FAILURE;
    }

    private Performance preparePerformanceInfo(IsoMessageResponse isoMessageResponse) {
        return Performance.Builder.defaultBuilder()
                .withStartDateTime(StringUtils.dateTimeStampYYYYMMddHHmmss())
                .withEndDateTime(IsoMessageUtils.retrieveDateTime(isoMessageResponse))
                .build();
    }

    private Retailer prepareRetailerInfo(RequestContext requestContext) {
        TerminalInfo terminalInfo = requestContext.getTerminalInfo();
        return Retailer.Builder.defaultBuilder()
                .withRetailerLogo(terminalInfo.getMerchantPrintLogo())
                .withRetailerName(terminalInfo.getMerchantName())
                .withRetailerAddress(terminalInfo.getMerchantAddress())
                .withMerchantId(terminalInfo.getMerchantID())
                .withTerminalId(terminalInfo.getTerminalID())
                .build();
    }
}
