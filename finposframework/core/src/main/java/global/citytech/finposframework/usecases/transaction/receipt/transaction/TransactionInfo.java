package global.citytech.finposframework.usecases.transaction.receipt.transaction;

import global.citytech.finposframework.utility.StringUtils;

public class TransactionInfo {
    private String transactionCurrencyName;
    private String batchNumber;
    private String stan;
    private String rrn;
    private String invoiceNumber;
    private String cardScheme;
    private String purchaseType;
    private String cardHolderName;
    private String cardNumber;
    private String expireDate;
    private String purchaseAmount;
    private String transactionStatus;
    private String transactionMessage;
    private String approvalCode;
    private boolean signatureRequired;
    private boolean debitAcknowledgementRequired;
    private String baseAmount;
    private boolean tipEnabled;
    private boolean isEmiTransaction;
    private String emiInfoJson;

    public String getTransactionCurrencyName() {
        return transactionCurrencyName;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public String getStan() {
        return stan;
    }

    public String getRrn() {
        return rrn;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public String getCardScheme() {
        return cardScheme;
    }

    public String getPurchaseType() {
        return purchaseType;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public String getPurchaseAmount() {
        return purchaseAmount;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public String getTransactionMessage() {
        return transactionMessage;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public boolean isSignatureRequired() {
        return signatureRequired;
    }

    public boolean isDebitAcknowledgementRequired() {
        return debitAcknowledgementRequired;
    }

    public String getBaseAmount() {
        return baseAmount;
    }

    public boolean isTipEnabled() {
        return tipEnabled;
    }

    public boolean isEmiTransaction() {
        return isEmiTransaction;
    }

    public String getTotalAmount() {
        return StringUtils.formatAmountTwoDecimal(Double.parseDouble(purchaseAmount) + Double.parseDouble(baseAmount));
    }

    public String getEmiInfoJson() {
        return emiInfoJson;
    }

    public void setEmiInfoJson(String emiInfoJson) {
        this.emiInfoJson = emiInfoJson;
    }

    public static final class Builder {
        private String transactionCurrencyName;
        private String batchNumber;
        private String stan;
        private String rrn;
        private String invoiceNumber;
        private String cardScheme;
        private String purchaseType;
        private String cardHolderName;
        private String cardNumber;
        private String expireDate;
        private String purchaseAmount;
        private String transactionStatus;
        private String transactionMessage;
        private String approvalCode;
        private boolean signatureRequired;
        private boolean debitAcknowledgementRequired;
        private String baseAmount;
        private boolean tipEnabled;
        private boolean isEmiTransaction;
        private String emiInfoJson;

        public Builder() {
        }

        public static Builder defaultBuilder() {
            return new Builder();
        }

        public Builder withTransactionCurrencyName(String transactionCurrencyName) {
            this.transactionCurrencyName = transactionCurrencyName;
            return this;
        }

        public Builder withBatchNumber(String batchNumber) {
            this.batchNumber = batchNumber;
            return this;
        }

        public Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder withRrn(String rrn) {
            this.rrn = rrn;
            return this;
        }

        public Builder withInvoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }

        public Builder withCardScheme(String cardScheme) {
            this.cardScheme = cardScheme;
            return this;
        }

        public Builder withPurchaseType(String purchaseType) {
            this.purchaseType = purchaseType;
            return this;
        }

        public Builder withCardHolderName(String cardHolderName) {
            this.cardHolderName = cardHolderName;
            return this;
        }

        public Builder withCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
            return this;
        }

        public Builder withExpireDate(String expireDate) {
            this.expireDate = expireDate;
            return this;
        }

        public Builder withPurchaseAmount(String purchaseAmount) {
            this.purchaseAmount = purchaseAmount;
            return this;
        }

        public Builder withTransactionStatus(String transactionStatus) {
            this.transactionStatus = transactionStatus;
            return this;
        }

        public Builder withTransactionMessage(String transactionMessage) {
            this.transactionMessage = transactionMessage;
            return this;
        }

        public Builder withApprovalCode(String approvalCode) {
            this.approvalCode = approvalCode;
            return this;
        }

        public Builder withBaseAmount(String baseAmount) {
            this.baseAmount = baseAmount;
            return this;
        }

        public Builder withSignatureRequired(boolean signatureRequired) {
            this.signatureRequired = signatureRequired;
            return this;
        }

        public Builder withDebitAcknowledgementRequired(boolean debitAcknowledgementRequired) {
            this.debitAcknowledgementRequired = debitAcknowledgementRequired;
            return this;
        }

        public Builder withTipEnabled(boolean tipEnabled) {
            this.tipEnabled = tipEnabled;
            return this;
        }

        public Builder isEmiTransaction(boolean isEmiTransaction) {
            this.isEmiTransaction = isEmiTransaction;
            return this;
        }

        public Builder withEmiInfo(String emiInfoJson) {
            this.emiInfoJson = emiInfoJson;
            return this;
        }

        public TransactionInfo build() {
            TransactionInfo transactionInfo = new TransactionInfo();
            transactionInfo.transactionCurrencyName = this.transactionCurrencyName;
            transactionInfo.expireDate = this.expireDate;
            transactionInfo.purchaseAmount = this.purchaseAmount;
            transactionInfo.stan = this.stan;
            transactionInfo.batchNumber = this.batchNumber;
            transactionInfo.invoiceNumber = this.invoiceNumber;
            transactionInfo.cardHolderName = this.cardHolderName;
            transactionInfo.cardNumber = this.cardNumber;
            transactionInfo.approvalCode = this.approvalCode;
            transactionInfo.rrn = this.rrn;
            transactionInfo.transactionStatus = this.transactionStatus;
            transactionInfo.purchaseType = this.purchaseType;
            transactionInfo.transactionMessage = this.transactionMessage;
            transactionInfo.cardScheme = this.cardScheme;
            transactionInfo.signatureRequired = this.signatureRequired;
            transactionInfo.debitAcknowledgementRequired = this.debitAcknowledgementRequired;
            transactionInfo.baseAmount = this.baseAmount;
            transactionInfo.tipEnabled = this.tipEnabled;
            transactionInfo.isEmiTransaction = this.isEmiTransaction;
            transactionInfo.emiInfoJson = this.emiInfoJson;
            return transactionInfo;
        }


    }
}
