package global.citytech.finpos.merchant.framework.datasource.core.printparameter;

import global.citytech.finposframework.hardware.io.printer.Style;

/**
 * Created by Rishav Chudal on 11/6/20.
 */
public enum TmsLogsStyle {

    HEADER(Style.FontSize.NORMAL, Style.Align.CENTER, 1, true, true, false, false),
    SUB_HEADER(Style.FontSize.SMALL, Style.Align.CENTER, 1, true, true, false, false),
    PARAMETERS(Style.FontSize.SMALL, Style.Align.LEFT, 2, true, true, false, false),
    DIVIDER(Style.FontSize.XSMALL, Style.Align.CENTER, 1, true, true, false, false);

    private Style.FontSize fontSize;
    private Style.Align alignment;
    private int columns;
    private boolean allCaps;
    private boolean bold;
    private boolean italic;
    private boolean underline;

    TmsLogsStyle(
            Style.FontSize fontSize,
            Style.Align alignment,
            int columns,
            boolean allCaps,
            boolean bold,
            boolean italic,
            boolean underline
    ) {
        this.fontSize = fontSize;
        this.alignment = alignment;
        this.columns = columns;
        this.allCaps = allCaps;
        this.bold = bold;
        this.italic = italic;
        this.underline = underline;
    }

    public Style getStyle() {
        return new Style.Builder()
                .fontSize(this.fontSize)
                .alignment(this.alignment)
                .multipleAlignment(this.ifMultipleAlignment())
                .bold(this.bold)
                .italic(this.italic)
                .underline(this.underline)
                .allCaps(this.allCaps).build();
    }

    private boolean ifMultipleAlignment() {
        return this.columns > 1;
    }

    public int[] getColumnWidths() {
        int num = 100 / this.columns;
        int[] columnWidths = new int[this.columns];
        for (int i = 0; i < this.columns; i++) {
            columnWidths[i] = num;
        }
        return columnWidths;
    }

    public int[] getColumnWidths(int firstColumnWidth) {
        int num = (100 - firstColumnWidth) / (this.columns - 1);
        int[] columnWidths = new int[this.columns];
        for (int i = 0; i < this.columns; i++) {
            columnWidths[i] = i == 0 ? firstColumnWidth : num;
        }
        return columnWidths;
    }
}
