package global.citytech.finpos.merchant.domain.repository.core.voidsale

import global.citytech.finpos.merchant.domain.model.voidsale.VoidSaleResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.voidsale.VoidSaleRequestItem
import global.citytech.finpos.processor.nibl.transaction.voidsale.VoidSaleResponseModel
import io.reactivex.Observable

/**
 * Created by Rishav Chudal on 9/18/20.
 */
interface VoidSaleRepository {
    fun doVoidSale(
        configurationItem: ConfigurationItem,
        voidSaleRequestItem: VoidSaleRequestItem
    ): Observable<VoidSaleResponseEntity>
}