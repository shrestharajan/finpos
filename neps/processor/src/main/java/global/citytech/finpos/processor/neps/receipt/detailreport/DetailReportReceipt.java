package global.citytech.finpos.processor.neps.receipt.detailreport;

import java.util.List;

import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;

/**
 * Created by Saurav Ghimire on 3/29/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public class DetailReportReceipt {
    private Retailer retailer;
    private Performance performance;
    private String reconciliationBatchNumber;
    private String host;
    private List<DetailReport> detailReports;

    public Retailer getRetailer() {
        return retailer;
    }

    public Performance getPerformance() {
        return performance;
    }

    public String getReconciliationBatchNumber() {
        return reconciliationBatchNumber;
    }

    public String getHost() {
        return host;
    }

    public List<DetailReport> getDetailReports() {
        return detailReports;
    }

    public static final class Builder {
        private Retailer retailer;
        private Performance performance;
        private String reconciliationBatchNumber;
        private String host;
        private List<DetailReport> detailReports;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withRetailer(Retailer retailer) {
            this.retailer = retailer;
            return this;
        }

        public Builder withPerformance(Performance performance) {
            this.performance = performance;
            return this;
        }

        public Builder withReconciliationBatchNumber(String reconciliationBatchNumber) {
            this.reconciliationBatchNumber = reconciliationBatchNumber;
            return this;
        }

        public Builder withHost(String host) {
            this.host = host;
            return this;
        }

        public Builder withDetailReports(List<DetailReport> detailReports) {
            this.detailReports = detailReports;
            return this;
        }

        public DetailReportReceipt build() {
            DetailReportReceipt detailReportReceipt = new DetailReportReceipt();
            detailReportReceipt.performance = this.performance;
            detailReportReceipt.host = this.host;
            detailReportReceipt.retailer = this.retailer;
            detailReportReceipt.detailReports = this.detailReports;
            detailReportReceipt.reconciliationBatchNumber = this.reconciliationBatchNumber;
            return detailReportReceipt;
        }
    }
}
