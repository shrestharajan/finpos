package global.citytech.crypto;

/**
 * Created by Rishav Chudal on 6/17/21.
 */
public interface PosCryptoService {
    String decrypt(String publicKey, String data);

    String encrypt(String privateKey, String data);
}
