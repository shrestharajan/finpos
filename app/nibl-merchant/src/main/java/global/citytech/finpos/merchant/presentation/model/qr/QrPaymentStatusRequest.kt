package global.citytech.finpos.merchant.presentation.model.qr

data class QrPaymentStatusRequest(
    val merchant_id: String,
    val terminal_id: String,
    val reference_number: String
)