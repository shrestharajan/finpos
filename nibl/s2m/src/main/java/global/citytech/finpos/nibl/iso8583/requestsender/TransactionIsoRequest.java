package global.citytech.finpos.nibl.iso8583.requestsender;

import java.math.BigDecimal;

import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.transaction.Request;

/**
 * Created by Unique Shakya on 3/5/2021.
 */
public class TransactionIsoRequest implements Request {

    private String pan;
    private BigDecimal transactionAmount = BigDecimal.ZERO;
    private BigDecimal originalAmount = BigDecimal.ZERO;
    private BigDecimal tipAmount = BigDecimal.ZERO;
    private String localTime;
    private String localDate;
    private PosEntryMode posEntryMode;
    private String posConditionCode;
    private String track2Data;
    private String additionalData;
    private String currencyCode;
    private String pinBlock;
    private String emvData;
    private String expireDate;
    private String cardSequenceNumber;
    private String invoiceNumber;
    private String retrievalReferenceNumber;
    private boolean isFallbackFromIccToMag;
    private String approvalCode;
    private String originalInvoiceNumber;
    private String originalRetrievalReferenceNumber;
    private String originalApprovalCode;
    private String originalResponseCode;
    private String cvv;
    private String otp;

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getPan() {
        return pan;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public BigDecimal getOriginalAmount() {
        return originalAmount;
    }

    public BigDecimal getTipAmount() {
        return tipAmount;
    }

    public String getLocalTime() {
        return localTime;
    }

    public String getLocalDate() {
        return localDate;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public String getTrack2Data() {
        return track2Data;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getPinBlock() {
        return pinBlock;
    }

    public String getEmvData() {
        return emvData;
    }

    public boolean isFallbackFromIccToMag() {
        return isFallbackFromIccToMag;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public String getCardSequenceNumber() {
        return cardSequenceNumber;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public String getOriginalInvoiceNumber() {
        return originalInvoiceNumber;
    }

    public String getOriginalRetrievalReferenceNumber() {
        return originalRetrievalReferenceNumber;
    }

    public String getOriginalApprovalCode() {
        return originalApprovalCode;
    }

    public String getOriginalResponseCode() {
        return originalResponseCode;
    }
    public String getOtp() {
        return otp;
    }

    public static final class Builder {
        private String pan;
        private BigDecimal transactionAmount = BigDecimal.ZERO;
        private BigDecimal originalAmount = BigDecimal.ZERO;
        private BigDecimal tipAmount = BigDecimal.ZERO;
        private String localTime;
        private String localDate;
        private PosEntryMode posEntryMode;
        private String posConditionCode;
        private String track2Data;
        private String additionalData;
        private String currencyCode;
        private String pinBlock;
        private String emvData;
        private String expireDate;
        private String cardSequenceNumber;
        private String invoiceNumber;
        private String originalInvoiceNumber;
        private String retrievalReferenceNumber;
        private String originalRetrievalReferenceNumber;
        private String approvalCode;
        private String originalApprovalCode;
        private String originalResponseCode;
        private boolean isFallbackFromIccToMag;
        private String cvv;
        private String otp;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public Builder originalAmount(BigDecimal originalAmount) {
            this.originalAmount = originalAmount;
            return this;
        }

        public Builder tipAmount(BigDecimal tipAmount) {
            this.tipAmount = tipAmount;
            return this;
        }

        public Builder localTime(String localTime) {
            this.localTime = localTime;
            return this;
        }

        public Builder localDate(String localDate) {
            this.localDate = localDate;
            return this;
        }

        public Builder posEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public Builder posConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public Builder additionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder pinBlock(String pinBlock) {
            this.pinBlock = pinBlock;
            return this;
        }

        public Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public Builder expireDate(String expireDate) {
            this.expireDate = expireDate;
            return this;
        }

        public Builder cardSequenceNumber(String cardSequenceNumber) {
            this.cardSequenceNumber = cardSequenceNumber;
            return this;
        }

        public Builder invoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }

        public Builder retrievalReferenceNumber(String retrievalReferenceNumber) {
            this.retrievalReferenceNumber = retrievalReferenceNumber;
            return this;
        }

        public Builder approvalCode(String approvalCode) {
            this.approvalCode = approvalCode;
            return this;
        }

        public Builder originalInvoiceNumber(String originalInvoiceNumber) {
            this.originalInvoiceNumber = originalInvoiceNumber;
            return this;
        }

        public Builder originalRetrievalReferenceNumber(String originalRetrievalReferenceNumber) {
            this.originalRetrievalReferenceNumber = originalRetrievalReferenceNumber;
            return this;
        }

        public Builder originalApprovalCode(String originalApprovalCode) {
            this.originalApprovalCode = originalApprovalCode;
            return this;
        }

        public Builder originalResponseCode(String originalResponseCode) {
            this.originalResponseCode = originalResponseCode;
            return this;
        }

        public Builder isFallbackFromIccToMag(boolean isFallbackFromIccToMag) {
            this.isFallbackFromIccToMag = isFallbackFromIccToMag;
            return this;
        }

        public Builder cvv(String cvv) {
            this.cvv = cvv;
            return this;
        }

        public Builder otp(String otp) {
            this.otp = otp;
            return this;
        }

        public TransactionIsoRequest build() {
            TransactionIsoRequest transactionIsoRequest = new TransactionIsoRequest();
            transactionIsoRequest.cardSequenceNumber = this.cardSequenceNumber;
            transactionIsoRequest.pan = this.pan;
            transactionIsoRequest.emvData = this.emvData;
            transactionIsoRequest.isFallbackFromIccToMag = this.isFallbackFromIccToMag;
            transactionIsoRequest.posEntryMode = this.posEntryMode;
            transactionIsoRequest.currencyCode = this.currencyCode;
            transactionIsoRequest.localTime = this.localTime;
            transactionIsoRequest.track2Data = this.track2Data;
            transactionIsoRequest.retrievalReferenceNumber = this.retrievalReferenceNumber;
            transactionIsoRequest.approvalCode = this.approvalCode;
            transactionIsoRequest.transactionAmount = this.transactionAmount;
            transactionIsoRequest.originalAmount = this.originalAmount;
            transactionIsoRequest.pinBlock = this.pinBlock;
            transactionIsoRequest.expireDate = this.expireDate;
            transactionIsoRequest.invoiceNumber = this.invoiceNumber;
            transactionIsoRequest.posConditionCode = this.posConditionCode;
            transactionIsoRequest.localDate = this.localDate;
            transactionIsoRequest.additionalData = this.additionalData;
            transactionIsoRequest.originalApprovalCode = this.originalApprovalCode;
            transactionIsoRequest.originalRetrievalReferenceNumber = this.originalRetrievalReferenceNumber;
            transactionIsoRequest.originalInvoiceNumber = this.originalInvoiceNumber;
            transactionIsoRequest.originalResponseCode = this.originalResponseCode;
            transactionIsoRequest.tipAmount = this.tipAmount;
            transactionIsoRequest.cvv = this.cvv;
            transactionIsoRequest.otp = this.otp;
            return transactionIsoRequest;
        }
    }
}
