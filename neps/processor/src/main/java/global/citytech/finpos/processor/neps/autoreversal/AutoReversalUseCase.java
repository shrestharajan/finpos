package global.citytech.finpos.processor.neps.autoreversal;

import global.citytech.finpos.neps.iso8583.requestsender.reversal.ReversalIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.voidsale.VoidSaleIsoRequest;
import global.citytech.finpos.processor.neps.reversal.ReversalRequest;
import global.citytech.finpos.processor.neps.reversal.ReversalUseCase;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.autoreversal.AutoReversalRequester;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.JsonUtils;

/**
 * Created by Saurav Ghimire on 7/12/21.
 * sauravnghimire@gmail.com
 */


public class AutoReversalUseCase implements AutoReversalRequester<AutoReversalRequest, AutoReversalResponse>,
        UseCase<AutoReversalRequest, AutoReversalResponse> {
    private TerminalRepository terminalRepository;
    private Notifier notifier;
    private AutoReversalRequest autoReversalRequest;
    private AutoReversal autoReversal;
    private Logger logger = Logger.getLogger(AutoReversalUseCase.class.getName());
    private static final int RETRY_LIMIT = 5;
    private VoidSaleIsoRequest reversalIsoRequest;

    public AutoReversalUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        this.terminalRepository = terminalRepository;
        this.notifier = notifier;
    }

    @Override
    public AutoReversalResponse execute(AutoReversalRequest autoReversalRequest) {
        logger.log("::: AUTO REVERSAL ::: REACHED AUTO REVERSAL USE CASE");
        this.autoReversalRequest = autoReversalRequest;
        if (this.autoReversalRequest.getTransactionRepository().isAutoReversalPresent()) {
            logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL PRESENT ");
            this.autoReversal = this.autoReversalRequest.getTransactionRepository().getAutoReversal();
            if (this.autoReversal.getStatus() == AutoReversal.Status.ACTIVE) {
                try {
                    reversalIsoRequest = JsonUtils.fromJsonToObj(autoReversal.getReversalRequest(),
                            VoidSaleIsoRequest.class);
                    return this.performReversal(reversalIsoRequest, autoReversal.getTransactionType());
                } catch (Exception e) {
                    e.printStackTrace();
                    return new AutoReversalResponse(Result.FAILURE, "Could not complete auto reversal");
                }
            }
        }
        return new AutoReversalResponse(Result.SUCCESS, "No Auto Reversal Present");
    }

    private AutoReversalResponse performReversal(VoidSaleIsoRequest reversalIsoRequest,
                                                 TransactionType transactionType) {
        ReversalUseCase reversalUseCase = new ReversalUseCase(this.terminalRepository,
                reversalIsoRequest,
                this.notifier
        );
        TransactionRequest transactionRequest = new TransactionRequest(transactionType);
        transactionRequest.setAmount(reversalIsoRequest.getTransactionAmount());
        try {
            reversalUseCase.execute(prepareReversalRequestModel(transactionRequest));
            this.autoReversalRequest.getTransactionRepository().removeAutoReversal(this.autoReversal);
            return new AutoReversalResponse(Result.SUCCESS, "Auto Reversal Success");
        } catch (FinPosException e) {
            e.printStackTrace();
             if (!e.getMessage().equals(FinPosException.ExceptionType.UNABLE_TO_CONNECT.getMessage()) && !e.getMessage().equals(FinPosException.ExceptionType.READ_TIME_OUT.getMessage())) {
                if (this.autoReversal.getRetryCount() < RETRY_LIMIT) {
                    this.autoReversalRequest.getTransactionRepository().incrementAutoReversalRetryCount(this.autoReversal);
                } else {
                    this.incrementTransationIds();
                    this.autoReversalRequest.getTransactionRepository().changeAutoReversalStatus(this.autoReversal, AutoReversal.Status.INACTIVE);
                }
                return new AutoReversalResponse(Result.FAILURE, "Auto Reversal Failure");
            } else {
                var errorMessage = "";
                if (isFinPosExceptionTypeConnectionError(e)) {
                    if(e.getType() == FinPosException.ExceptionType.READ_TIME_OUT) errorMessage = PosError.DEVICE_ERROR_TIMEOUT.getErrorMessage();
                    else errorMessage = PosError.DEVICE_ERROR_SETTLEMENT_REQUEST_CONNECTION_ERROR.getErrorMessage();
                }
                return new AutoReversalResponse(Result.FAILURE, (errorMessage.isEmpty()) ? e.getMessage() : errorMessage);
            }
        }
    }

    private boolean isFinPosExceptionTypeConnectionError(FinPosException finPosException) {
        FinPosException.ExceptionType type = finPosException.getType();
        return (type == FinPosException.ExceptionType.UNABLE_TO_CONNECT ||
                type == FinPosException.ExceptionType.CONNECTION_ERROR ||
                type == FinPosException.ExceptionType.READ_TIME_OUT);
    }

    private void incrementTransationIds() {
        this.terminalRepository.incrementSystemTraceAuditNumber();
    }

    private ReversalRequest prepareReversalRequestModel(TransactionRequest transactionRequest) {
        return ReversalRequest.Builder.newInstance()
                .withStan(reversalIsoRequest.getStan())
                .withApplicationRepository(this.autoReversalRequest.getApplicationRepository())
                .withPrinterService(this.autoReversalRequest.getPrinterService())
                .withTransactionRepository(this.autoReversalRequest.getTransactionRepository())
                .withTransactionRequest(transactionRequest)
                .withReadCardResponse(autoReversal.getReadCardResponse())
                .build();
    }
}
