package global.citytech.finpos.nibl.iso8583;


import global.citytech.finposframework.usecases.TransactionStatusFromHost;

/**
 * Created by Unique Shakya on 8/31/2020.
 */
public enum S2MActionCode {
    ACTION_CODE_00(0, "APPROVED", TransactionStatusFromHost.ONLINE_APPROVED),
    ACTION_CODE_01(1, "CALL ISSUER", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_02(2, "CALL ISSUER", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_03(3, "Contact VI", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_04(4, "DECLINED, PICK UP CARD", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_05(5, "DO NOT HONOR, TRANS. DECLINED", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_06(6, "CONTACT VI #06", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_07(7, "PICK UP # 07", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_08(8, "APPROVED VERIFY ID & SIGNATURE", TransactionStatusFromHost.ONLINE_APPROVED),
    ACTION_CODE_09(9, "PLEASE WAIT", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_10(10, "APPROVED IN PART", TransactionStatusFromHost.ONLINE_APPROVED),
    ACTION_CODE_11(11, "APPROVED", TransactionStatusFromHost.ONLINE_APPROVED),
    ACTION_CODE_12(12, "INVALID TXN", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_13(13, "INVALID AMOUNT", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_14(14, "DECLINED # 14", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_15(15, "DECLINED # 15", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_17(17, "DECLINED # 17", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_18(18, "DECLINED # 18", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_19(19, "RETRY # 19", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_20(20, "DECLINED # 20", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_21(21, "DECLINED # 21", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_22(22, "RETRY # 22", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_23(23, "DECLINE # 23", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_24(24, "DECLINE # 24", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_25(25, "RETRY # 25", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_26(26, "DECLINED #26", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_27(27, "RETRY # 27", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_28(28, "RETRY # 28", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_29(29, "RETRY # 29", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_30(30, "RETRY # 30", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_31(31, "RETRY # 31", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_32(32, "RETRY # 32", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_33(33, "EXPIRED CARD #33", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_34(34, "DECLINED # 34", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_35(35, "CONTACT VI # 35", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_36(36, "RESTRICTED CARD", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_37(37, "CONTACT VI # 37", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_38(38, "EXCESS PIN TRIES", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_39(39, "DECLINED # 39", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_40(40, "DECLINED # 40", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_41(41, "PICK UP # 41", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_42(42, "DECLINED # 42", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_43(43, "PICK UP # 43", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_44(44, "DECLINED # 44", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_45(45, "RETRY # 45", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_46(46, "RETRY # 46", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_47(47, "RETRY # 47", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_48(48, "RETRY # 48", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_49(49, "RETRY # 49", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_50(50, "RETRY # 50", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_51(51, "DECLINED # 51", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_52(52, "NO CHK ACCT", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_53(53, "NO SAVINGS ACCT", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_54(54, "EXPIRED CARD", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_55(55, "INCORRECT PIN", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_56(56, "DECLINED # 56", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_57(57, "DECLINED # 57", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_58(58, "DECLINED # 58", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_59(59, "DECLINED # 59", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_60(60, "DECLINED # 60", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_61(61, "EXCEEDS AMT LMT", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_62(62, "RESTRICTED CARD", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_63(63, "SECURITY ERR # 63", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_64(64, "RETRY # 64", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_65(65, "EXCEEDS COUNT", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_66(66, "DECLINE # 66", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_67(67, "PICK UP # 67", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_68(68, "RETRY # 68", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_69(69, "RETRY # 69", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_70(70, "RETRY # 70", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_71(71, "RETRY # 71", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_72(72, "RETRY # 72", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_73(73, "RETRY # 73", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_74(74, "RETRY # 74", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_75(75, "EX-PIN TRIES #75", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_76(76, "RETRY # 76", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_77(77, "RETRY # 77", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_78(78, "OLD ROC NOT FOUND", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_79(79, "BATCH OPEN # 79", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_80(80, "BAD BATCH NO #80", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_81(81, "PVT ERROR # 81", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_82(82, "PVT ERROR # 82", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_83(83, "PVT ERROR # 83", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_84(84, "PVT ERROR # 84", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_85(85, "PVT ERROR # 85", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_86(86, "PVT ERROR # 86", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_87(87, "PVT ERROR # 87", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_88(88, "PVT ERROR # 88", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_89(89, "CONTACT VI # 89", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_90(90, "CUTOFF IN PROCES", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_91(91, "TRY AFTER 5MIN #91", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_92(92, "TRY AFTER 5MIN #92", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_93(93, "DECLINED # 93", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_94(94, "CONTACT VI # 94", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_95(95, "RETRY REC-ER #95", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_96(96, "CALL ISSUER #96", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_97(97, "PVT ERROR # 97", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_98(98, "PVT ERROR # 98", TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_99(99, "PVT ERROR # 99", TransactionStatusFromHost.ONLINE_DECLINED);

    private int actionCode;
    private String description;
    private TransactionStatusFromHost transactionStatusFromHost;

    S2MActionCode(int actionCode, String description, TransactionStatusFromHost transactionStatusFromHost) {
        this.actionCode = actionCode;
        this.description = description;
        this.transactionStatusFromHost = transactionStatusFromHost;
    }

    public TransactionStatusFromHost getTransactionStatusFromHost() {
        return transactionStatusFromHost;
    }

    public int getActionCode() {
        return actionCode;
    }

    public String getDescription() {
        return description;
    }

    public static S2MActionCode getByActionCode(int actionCode) {
        S2MActionCode[] dataTypes = values();
        S2MActionCode[] var2 = dataTypes;
        int var3 = dataTypes.length;

        for (int var4 = 0; var4 < var3; ++var4) {
            S2MActionCode dataType = var2[var4];
            if (dataType.getActionCode() == actionCode) {
                return dataType;
            }
        }
        throw new IllegalArgumentException("No such action code");
    }
}
