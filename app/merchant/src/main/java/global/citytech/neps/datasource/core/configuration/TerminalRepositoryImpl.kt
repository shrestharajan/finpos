package global.citytech.neps.datasource.core.configuration

import android.util.Log
import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.neps.presentation.model.HostItem
import global.citytech.posswitchintregator.comm.ConnectionEvent
import global.citytech.posswitchintregator.comm.HostInfo
import global.citytech.posswitchintregator.repo.TerminalRepository
import global.citytech.posswitchintregator.supports.Context
import global.citytech.posswitchintregator.terminal.TerminalInfo

class TerminalRepositoryImpl(configurationItem: ConfigurationItem?):TerminalRepository {

    val hosts = configurationItem!!.hosts
    val merchant = configurationItem!!.merchants!![0]



    override fun findSecondaryHost(): HostInfo {
        val host = hosts!![1]
        Log.v("SECONDARY HOST TPDU::::",host.nii)
        return HostInfo.Builder.createDefaultBuilder(
            host.ip,
            host.port!!.toInt()
        ).timeOut(host.connectionTimeout!!.toInt()).retryLimit(host.retryLimit!!.toInt()).tpduString(this.getTPDUString(host))
            .addEventListener { event: ConnectionEvent?, context: Context ->
                println(
                    context["MSG"].toString()
                )
            }.build()
    }

    override fun updateAndGetSystemTraceAudit(): String {
        return "1234"
    }

    override fun findPrimaryHost(): HostInfo {
        val host = hosts!![0]
        Log.v("PRIMARY HOST TPDU::::",host.nii)
        return HostInfo.Builder.createDefaultBuilder(
            host.ip,
            host.port!!.toInt()
        ).timeOut(host.connectionTimeout!!.toInt()).retryLimit(host.retryLimit!!.toInt()).tpduString(this.getTPDUString(host))
            .addEventListener { event: ConnectionEvent?, context: Context ->
                println(
                    context["MSG"].toString()
                )
            }.build()

    }

    override fun findTerminalInfo(): TerminalInfo {
        return TerminalInfo(merchant.terminalId, merchant.switchId)
    }

    private fun getTPDUString(hostItem: HostItem): String? {
        Log.v("NII:::","${hostItem.nii}")
        return hostItem.nii
    }

}