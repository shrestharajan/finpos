package global.citytech.finpos.merchant.presentation.dynamicqr.refund

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.common.data.Response
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.QrPaymentRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.QrPayment
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.app.QrPaymentUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.app.QrPaymentDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.dynamicqr.receipt.DynamicQrReceiptGenerator
import global.citytech.finpos.merchant.presentation.dynamicqr.receipt.DynamicQrTransactionReceiptPrintHandler
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.presentation.model.qr.refund.QrRefundRequest
import global.citytech.finpos.merchant.presentation.model.qr.refund.QrRefundResponse
import global.citytech.finposframework.hardware.io.printer.PrinterResponse
import global.citytech.finposframework.repositories.TerminalRepository
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion
import global.citytech.finposframework.utility.StringUtils
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*

class QrRefundViewModel(val context: Application) : BaseAndroidViewModel(context) {

    val originalQrPayment by lazy { MutableLiveData<QrPayment>() }
    val refundQrPayment by lazy { MutableLiveData<QrPayment>() }
    val finishActivity by lazy { MutableLiveData<Boolean>() }
    var configurationItem: ConfigurationItem? = null
    val transactionConfirmationLiveData by lazy { MutableLiveData<TransactionConfirmation>() }
    var updateTransactionConfirmation = false

    val qrPaymentUseCase = QrPaymentUseCase(
        QrPaymentRepositoryImpl(
            QrPaymentDataSourceImpl(context)
        )
    )

    val localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    var terminalRepository: TerminalRepository? = null

    val printerService = PrinterSourceImpl(context)

    fun getConfigurationItem() {
        isLoading.value = true
        compositeDisposable.add(
            localDataUseCase.getWholeConfiguration()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe(
                    {
                        isLoading.value = false
                        configurationItem = it
                        terminalRepository = TerminalRepositoryImpl(configurationItem)
                    },
                    {
                        isLoading.value = false
                    }
                )
        )
    }

    fun onInvoiceNumberRetrieved(invoiceNumber: String) {
        isLoading.value = true
        compositeDisposable.add(Observable.create(ObservableOnSubscribe<QrPayment> {
            val qrPayment = qrPaymentUseCase.getQrPaymentByInvoiceNumber(invoiceNumber)
            qrPayment?.let { qrPayment ->
                it.onNext(qrPayment)
            } ?: kotlin.run {
                throw IllegalArgumentException("No transactions found")
            }
        }).subscribe({
            isLoading.value = false
            if (it.isRefunded)
                message.value = "Transaction already refunded"
            else
                originalQrPayment.value = it
        }, {
            isLoading.value = false
            message.value = it.message
        }))
    }

    fun performRefundTransaction() {
        isLoading.value = true
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<QrRefundResponse> {
                onQrRefundSubscribe(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val isApproved = true
                    updateTransactionConfirmation = false
                    transactionConfirmationLiveData.value = TransactionConfirmation.Builder()
                        .amount(StringUtils.formatAmountTwoDecimal(originalQrPayment.value!!.transactionAmount))
                        .imageId(if (isApproved) R.drawable.approved else R.drawable.declined)
                        .message(if (isApproved) "Approval Code: ".plus(it.requestNumber) else "DECLINED")
                        .title(if (isApproved) "APPROVED" else "DECLINED")
                        .positiveLabel("")
                        .negativeLabel("")
                        .qrImageString("")
                        .transactionStatus("")
                        .build()
                    storeTransactionLog(originalQrPayment.value!!, isApproved, it)
                }, {
                    isLoading.value = false
                    message.value = "Could not perform refund. Please try again."
                })
        )
    }

    private fun storeTransactionLog(qrPayment: QrPayment, isApproved: Boolean, qrRefundResponse: QrRefundResponse) {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<QrPayment> {
                val responseDateTimeStamp = Date("12345567776413222".toLong())
                val dateFormatter = SimpleDateFormat("yyyyMMddHHmmssSSS")
                val dateTime = dateFormatter.format(responseDateTimeStamp)
                val transactionStatus = if (isApproved) "APPROVED" else "DECLINED"
                val refundQrPayment = QrPayment(
                    merchantId = qrPayment.merchantId,
                    terminalId = qrPayment.terminalId,
                    referenceNumber = qrPayment.referenceNumber,
                    invoiceNumber = qrPayment.invoiceNumber,
                    transactionType = "QR REFUND SALE",
                    transactionDate = dateTime.substring(0, 8),
                    transactionTime = dateTime.substring(8),
                    transactionCurrency = qrPayment.transactionCurrency,
                    transactionAmount = qrPayment.transactionAmount,
                    transactionStatus = transactionStatus,
                    approvalCode = qrRefundResponse.requestNumber,
                    paymentInitiator = qrPayment.paymentInitiator,
                    initiatorId = qrPayment.initiatorId
                )
                qrPaymentUseCase.updateQrPaymentRefunded(qrPayment)
                qrPaymentUseCase.addQrPayment(refundQrPayment)
                it.onNext(refundQrPayment)
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    refundQrPayment.value = it
                    printQrRefundReceipt(it, ReceiptVersion.MERCHANT_COPY)
                }, {
                    isLoading.value = false
                    it.printStackTrace()
                })
        )
    }

    fun printQrRefundReceipt(qrPayment: QrPayment?, receiptVersion: ReceiptVersion) {
        if (receiptVersion == ReceiptVersion.CUSTOMER_COPY)
            isLoading.value = true
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<PrinterResponse> {
                val dynamicReceipt =
                    DynamicQrReceiptGenerator(terminalRepository!!).generate(
                        qrPayment!!
                    )
                val printerResponse =
                    DynamicQrTransactionReceiptPrintHandler(printerService).printReceipt(
                        dynamicReceipt,
                        receiptVersion
                    )
                it.onNext(printerResponse)
                it.onComplete()
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    updateTransactionConfirmation = true
                    if (receiptVersion == ReceiptVersion.MERCHANT_COPY) {
                        val transactionConfirmation = transactionConfirmationLiveData.value
                        transactionConfirmation!!.positiveLabel = "PRINT CUSTOMER COPY"
                        transactionConfirmation.negativeLabel = "RETURN TO TRANSACTIONS"
                        transactionConfirmationLiveData.value = transactionConfirmation
                    } else {
                        finishActivity.value = true
                    }
                }, {
                    isLoading.value = false
                    it.printStackTrace()
                })
        )
    }

    private fun onQrRefundSubscribe(it: ObservableEmitter<QrRefundResponse>) {
        val qrRefundRequest = QrRefundRequest(
            originalQrPayment.value!!.merchantId,
            originalQrPayment.value!!.terminalId,
            originalQrPayment.value!!.referenceNumber,
            StringUtils.formatAmountTwoDecimal(originalQrPayment.value!!.transactionAmount)
        )
        val jsonRequest = Jsons.toJsonObj(qrRefundRequest)
        val jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.post(
            "/device-payment-processor/refund/",
            jsonRequest,
            true
        )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            val responseDataJson = Jsons.toJsonObj(response.data)
            it.onNext(Jsons.fromJsonToObj(responseDataJson, QrRefundResponse::class.java))
        } else {
            it.onError(Exception("Error Code :: ${response.code}. ${response.message}"))
        }
    }

    fun getMerchantCredential(): String = localDataUseCase.getMerchantCredential()
}