package global.citytech.finpos.merchant.presentation.purchase

data class PaymentItems(
    var paymentName: String,
    var paymentLogo: String,
    var placeHolder: Int
)
