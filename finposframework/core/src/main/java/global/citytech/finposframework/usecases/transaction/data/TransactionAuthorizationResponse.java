package global.citytech.finposframework.usecases.transaction.data;

public class TransactionAuthorizationResponse {
    boolean success;

    public TransactionAuthorizationResponse(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }
}
