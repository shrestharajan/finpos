package global.citytech.finpos.merchant.presentation.data

import global.citytech.finpos.merchant.domain.model.auth.completion.AuthorisationCompletionResponseEntity
import global.citytech.finpos.merchant.domain.model.autoreversal.AutoReversalResponseEntity
import global.citytech.finpos.merchant.domain.model.balanceinquiry.BalanceInquiryResponseEntity
import global.citytech.finpos.merchant.domain.model.cashadvance.CashAdvanceResponseEntity
import global.citytech.finpos.merchant.domain.model.cashin.CashInResponseEntity
import global.citytech.finpos.merchant.domain.model.greenpin.GreenPinResponseEntity
import global.citytech.finpos.merchant.domain.model.logon.LogonResponseEntity
import global.citytech.finpos.merchant.domain.model.pinchange.PinChangeResponseEntity
import global.citytech.finpos.merchant.domain.model.ministatement.MiniStatementResponseEntity
import global.citytech.finpos.merchant.domain.model.preauth.PreAuthResponseEntity
import global.citytech.finpos.merchant.domain.model.purchase.PurchaseResponseEntity
import global.citytech.finpos.merchant.domain.model.purchase.RefundResponseEntity
import global.citytech.finpos.merchant.domain.model.receipt.CustomerCopyResponseEntity
import global.citytech.finpos.merchant.domain.model.receipt.detailreport.DetailReportResponseEntity
import global.citytech.finpos.merchant.domain.model.receipt.duplicate.DuplicateReceiptResponseEntity
import global.citytech.finpos.merchant.domain.model.receipt.duplicatereconciliation.DuplicateReconciliationReceiptResponseEntity
import global.citytech.finpos.merchant.domain.model.receipt.summaryreport.SummaryReportResponseEntity
import global.citytech.finpos.merchant.domain.model.reconciliation.ReconciliationResponseEntity
import global.citytech.finpos.merchant.domain.model.reconciliation.check.CheckReconciliationResponseEntity
import global.citytech.finpos.merchant.domain.model.reconciliation.clear.BatchClearResponseEntity
import global.citytech.finpos.merchant.domain.model.tip.TipAdjustmentResponseEntity
import global.citytech.finpos.merchant.domain.model.voidsale.VoidSaleResponseEntity
import global.citytech.finpos.processor.nibl.balanceinquiry.BalanceInquiryResponse
import global.citytech.finpos.processor.nibl.cashin.CashInResponse
import global.citytech.finpos.processor.nibl.greenpin.GreenPinResponse
import global.citytech.finpos.processor.nibl.pinchange.PinChangeResponse
import global.citytech.finpos.processor.nibl.logon.LogOnResponseModel
import global.citytech.finpos.processor.nibl.ministatement.MiniStatementResponse
import global.citytech.finpos.processor.nibl.posmode.PosModeResponseModel
import global.citytech.finpos.processor.nibl.receipt.customercopy.CustomerCopyResponseModel
import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportResponseModel
import global.citytech.finpos.processor.nibl.receipt.duplicatereceipt.DuplicateReceiptResponseModel
import global.citytech.finpos.processor.nibl.receipt.duplicatereconciliation.DuplicateReconciliationReceiptResponseModel
import global.citytech.finpos.processor.nibl.receipt.summaryreport.SummaryReportResponseModel
import global.citytech.finpos.processor.nibl.reconciliation.ReconciliationResponseModel
import global.citytech.finpos.processor.nibl.reconciliation.check.CheckReconciliationResponseModel
import global.citytech.finpos.processor.nibl.reconciliation.clear.BatchClearResponseModel
import global.citytech.finpos.processor.nibl.transaction.authorisation.completion.AuthorisationCompletionResponseModel
import global.citytech.finpos.processor.nibl.transaction.authorisation.preauth.PreAuthResponseModel
import global.citytech.finpos.processor.nibl.transaction.autoreversal.AutoReversalResponseModel
import global.citytech.finpos.processor.nibl.transaction.cash.advance.CashAdvanceResponseModel
import global.citytech.finpos.processor.nibl.transaction.purchase.PurchaseResponseModel
import global.citytech.finpos.processor.nibl.transaction.refund.RefundResponseModel
import global.citytech.finpos.processor.nibl.transaction.tipadjustment.TipAdjustmentResponseModel
import global.citytech.finpos.processor.nibl.transaction.voidsale.VoidSaleResponseModel
import global.citytech.finpos.processor.nibl.transactiontype.TransactionTypeResponseModel
import global.citytech.finposframework.switches.autoreversal.AutoReversalResponseParameter
import global.citytech.finposframework.switches.logon.LogOnResponseParameter
import global.citytech.finposframework.switches.posmode.PosMode
import global.citytech.finposframework.switches.posmode.PosModeResponseParameter
import global.citytech.finposframework.switches.receipt.customercopy.CustomerCopyResponseParameter
import global.citytech.finposframework.switches.receipt.detailreport.DetailReportResponseParameter
import global.citytech.finposframework.switches.receipt.duplicatereceipt.DuplicateReceiptResponseParameter
import global.citytech.finposframework.switches.receipt.duplicatereconciliation.DuplicateReconciliationReceiptResponseParameter
import global.citytech.finposframework.switches.receipt.summaryreport.SummaryReportResponseParameter
import global.citytech.finposframework.switches.reconciliation.ReconciliationResponseParameter
import global.citytech.finposframework.switches.reconciliation.check.CheckReconciliationResponseParameter
import global.citytech.finposframework.switches.reconciliation.clear.BatchClearResponseParameter
import global.citytech.finposframework.switches.transaction.TransactionResponseParameter
import global.citytech.finposframework.switches.transactiontype.TransactionTypeResponseParameter

/**
 * Created by Unique Shakya on 1/19/2021.
 */
fun LogOnResponseParameter.mapToUiModel(): LogonResponseEntity {
    this as LogOnResponseModel
    return LogonResponseEntity(
        this.isSuccess
    )
}

fun CustomerCopyResponseParameter.mapToUiModel(): CustomerCopyResponseEntity {
    this as CustomerCopyResponseModel
    return CustomerCopyResponseEntity(
        this.result,
        this.message
    )
}

fun PosModeResponseParameter.mapToUiModel(): PosMode {
    this as PosModeResponseModel
    return this.posMode
}

fun TransactionTypeResponseParameter.mapToUiModel(): List<String> {
    this as TransactionTypeResponseModel
    return this.supportedTransactionTypes
}

fun TransactionResponseParameter.mapToPurchaseResponseUiModel(): PurchaseResponseEntity {
    this as PurchaseResponseModel
    return PurchaseResponseEntity(
        stan = stan,
        message = message,
        isApproved = isApproved,
        shouldPrintCustomerCopy = shouldPrintCustomerCopy()
    )
}

fun TransactionResponseParameter.mapToGreenPinResponseUiModel(): GreenPinResponseEntity {
    this as GreenPinResponse
    return GreenPinResponseEntity(
        stan = stan,
        message = message,
        isApproved = isApproved
    )
}

fun TransactionResponseParameter.mapToPinChangeResponseUiModel(): PinChangeResponseEntity {
    this as PinChangeResponse
    return PinChangeResponseEntity(
        stan = stan,
        message = message,
        isApproved =  isApproved
    )
}

fun TransactionResponseParameter.mapToBalanceInquiryResponseUiModel(): BalanceInquiryResponseEntity {
    this as BalanceInquiryResponse
    println("stannn....." + stan)
    println("message....." + message)
    return BalanceInquiryResponseEntity(
        stan = stan,
        message = message,
        additionalAmount = additionalAmount,
        approve = isApproved
    )
}

fun TransactionResponseParameter.mapToCashInResponseUiModel(): CashInResponseEntity {
    this as CashInResponse
    println(" looking for isApproved" + isApproved)
    return CashInResponseEntity(
        stan = stan,
        message = message,
        isApproved = isApproved
    )
}

fun TransactionResponseParameter.mapToMiniStatementResponseUiModel(): MiniStatementResponseEntity {
    this as MiniStatementResponse
    return MiniStatementResponseEntity(
        stan = stan,
        message = message,
        miniStatementData= miniStatementData,
        isApproved = isApproved,

    )
}

fun TransactionResponseParameter.mapToPreAuthResponseUiModel(): PreAuthResponseEntity {
    this as PreAuthResponseModel
    return PreAuthResponseEntity(
        stan = stan,
        message = message,
        isApproved = isApproved,
        shouldPrintCustomerCopy = shouldPrintCustomerCopy()
    )
}

fun TransactionResponseParameter.mapToVoidSaleResponseUIModel(): VoidSaleResponseEntity {
    this as VoidSaleResponseModel
    return VoidSaleResponseEntity(
        stan = stan,
        message = message,
        isApproved = isApproved,
        shouldPrintCustomerCopy = shouldPrintCustomerCopy()
    )
}

fun TransactionResponseParameter.mapToAuthorisationCompletionUiModel(): AuthorisationCompletionResponseEntity {
    this as AuthorisationCompletionResponseModel
    return AuthorisationCompletionResponseEntity(
        stan = stan,
        message = message,
        isApproved = isApproved,
        shouldPrintCustomerCopy = shouldPrintCustomerCopy()
    )
}

fun TransactionResponseParameter.mapToCashAdvanceResponseUiModel(): CashAdvanceResponseEntity {
    this as CashAdvanceResponseModel
    return CashAdvanceResponseEntity(
        stan = stan,
        message = message,
        isApproved = isApproved,
        shouldPrintCustomerCopy = shouldPrintCustomerCopy()
    )
}

fun TransactionResponseParameter.mapToRefundResponseUiModel(): RefundResponseEntity {
    this as RefundResponseModel
    return RefundResponseEntity(
        stan = stan,
        message = message,
        isApproved = isApproved,
        shouldPrintCustomerCopy = shouldPrintCustomerCopy()
    )
}

fun TransactionResponseParameter.mapToTipAdjustmentResponseUiModel(): TipAdjustmentResponseEntity {
    this as TipAdjustmentResponseModel
    return TipAdjustmentResponseEntity(
        stan = stan,
        message = message,
        approved = isApproved,
        shouldPrintCustomerCopy = isShouldPrintCustomerCopy
    )
}

fun DuplicateReceiptResponseParameter.mapToDuplicateReceiptResponseUiModel(): DuplicateReceiptResponseEntity {
    this as DuplicateReceiptResponseModel
    return DuplicateReceiptResponseEntity(
        result, message
    )
}

fun DetailReportResponseParameter.mapToDetailReportResponseUIModel(): DetailReportResponseEntity {
    this as DetailReportResponseModel
    return DetailReportResponseEntity(result, message)
}

fun SummaryReportResponseParameter.mapToSummaryReportResponseUIModel(): SummaryReportResponseEntity {
    this as SummaryReportResponseModel
    return SummaryReportResponseEntity(result, message)
}

fun ReconciliationResponseParameter.mapToReconciliationUIModel(): ReconciliationResponseEntity {
    this as ReconciliationResponseModel
    return ReconciliationResponseEntity(debugRequestString, debutResponseString, message, isSuccess)
}

fun DuplicateReconciliationReceiptResponseParameter.mapToDuplicateReconciliationReceiptUIModel(): DuplicateReconciliationReceiptResponseEntity {
    this as DuplicateReconciliationReceiptResponseModel
    return DuplicateReconciliationReceiptResponseEntity(result, message)
}

fun AutoReversalResponseParameter.mapToAutoReversalResponseUIModel(): AutoReversalResponseEntity {
    this as AutoReversalResponseModel
    return AutoReversalResponseEntity(result, message)
}

fun BatchClearResponseParameter.mapToBatchClearResponseUIModel(): BatchClearResponseEntity {
    this as BatchClearResponseModel
    return BatchClearResponseEntity(isSuccess, message)
}

fun CheckReconciliationResponseParameter.mapToCheckReconciliationResponseUIModel(): CheckReconciliationResponseEntity {
    return CheckReconciliationResponseEntity((this as CheckReconciliationResponseModel).isReconciliationRequired)
}
