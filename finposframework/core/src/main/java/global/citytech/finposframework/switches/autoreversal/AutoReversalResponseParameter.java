package global.citytech.finposframework.switches.autoreversal;

import global.citytech.finposframework.switches.ResponseParameter;

/**
 * Created by Unique Shakya on 6/22/2021.
 */
public interface AutoReversalResponseParameter extends ResponseParameter {
}
