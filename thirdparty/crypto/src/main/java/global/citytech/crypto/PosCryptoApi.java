package global.citytech.crypto;

public class PosCryptoApi implements PosCryptoService {
    private final PosCryptoImpl posCrypto;

    public PosCryptoApi() {
        posCrypto = new PosCryptoImpl();
    }

    @Override
    public String decrypt(String publicKey, String data) {
        return posCrypto.generatePublicKeyAndDecrypt(publicKey, data);
    }

    @Override
    public String encrypt(String privateKey, String data) {
        return posCrypto.generatePrivateKeyAndEncrypt(privateKey, data);
    }
}