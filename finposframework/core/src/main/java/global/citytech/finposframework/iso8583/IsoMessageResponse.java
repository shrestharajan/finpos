package global.citytech.finposframework.iso8583;

import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.supports.MessagePayload;

/** User: Surajchhetry Date: 2/27/20 Time: 5:37 PM */
public class IsoMessageResponse {
  private Iso8583Msg msg;
  private HostInfo usedHost;
  private String debugRequestString;
  private String debugResponseString;
  private int responseLength;
  private MessagePayload requestPayload;

  public IsoMessageResponse(Builder builder) {
    this.msg = builder.msg;
    this.usedHost = builder.usedHost;
    this.debugRequestString = builder.debugRequestString;
    this.debugResponseString = builder.debugResponseString;
    this.responseLength = builder.responseLength;
    this.requestPayload = builder.requestPayload;
  }

  public Iso8583Msg getMsg() {
    return msg;
  }

  public HostInfo getUsedHost() {
    return usedHost;
  }

  public String getDebugRequestString() {
    return debugRequestString;
  }

  public String getDebugResponseString() {
    return debugResponseString;
  }

  public int getResponseLength() {
    return responseLength;
  }

  public MessagePayload getRequestPayload() {
    return requestPayload;
  }

  public static final class Builder {
    private Iso8583Msg msg;
    private HostInfo usedHost;
    private String debugRequestString;
    private String debugResponseString;
    private int responseLength;
    private MessagePayload requestPayload;

    private Builder(Iso8583Msg msg, HostInfo usedHost, MessagePayload requestPayload) {
      this.msg = msg;
      this.usedHost = usedHost;
      this.requestPayload = requestPayload;
    }

    public static Builder createDefaultBuilder(
            Iso8583Msg msg,
            HostInfo usedHost,
            MessagePayload requestPayload
    ) {
      return new Builder(msg, usedHost, requestPayload);
    }

    public Builder debugRequestString(String debugRequestString) {
      this.debugRequestString = debugRequestString;
      return this;
    }

    public Builder debugResponseString(String debugResponseString) {
      this.debugResponseString = debugResponseString;
      return this;
    }
    
    public Builder responseLength(int responseLength) {
      this.responseLength = responseLength;
      return this;
    }
    
    public IsoMessageResponse build() {
      return new IsoMessageResponse(this);
    }
  }
}
