package global.citytech.finpos.merchant.presentation.ministatement

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.ministatement.MiniStatementRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.receipt.CustomerCopyRepositoryImpl
import global.citytech.finpos.merchant.domain.model.receipt.CustomerCopyResponseEntity
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.core.ministatement.CoreMiniStatementUseCase
import global.citytech.finpos.merchant.domain.usecase.core.receipt.CoreCustomerCopyUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.ministatement.MiniStatementDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.receipt.CustomerCopyDataSourceImpl
import global.citytech.finpos.merchant.presentation.greenpin.GreenPinBaseTransactionViewModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.ministatement.MiniStatementRequestItem
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.data.StatementList
import global.citytech.finposframework.usecases.transaction.data.StatementItems
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MiniStatementViewModel(val instance: Application) :
    GreenPinBaseTransactionViewModel(instance) {

    private var miniStatementUseCase: CoreMiniStatementUseCase = CoreMiniStatementUseCase(
        MiniStatementRepositoryImpl(MiniStatementDataSourceImpl())

    )
    val miniStatementResult by lazy { MutableLiveData<Boolean>() }
    val miniStatementSuccess by lazy { MutableLiveData<Boolean>() }
    var miniStatementData = arrayListOf<StatementItems>()

    var localDataUseCase: LocalDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )
    private var customerCopyUseCase: CoreCustomerCopyUseCase =
        CoreCustomerCopyUseCase(CustomerCopyRepositoryImpl(CustomerCopyDataSourceImpl()))

    fun generateMiniStatementRequest(
        configurationItem: ConfigurationItem,
        transactionType: TransactionType
    ) {
        compositeDisposable.add(
            miniStatementUseCase.generateMiniStatementRequest(
                configurationItem,
                MiniStatementRequestItem(
                    transactionType = transactionType,
                    amount = "0.00".toBigDecimal()
                ),
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    miniStatementSuccess.value = it.isApproved
                    if (!miniStatementSuccess.value!!) {
                        miniStatementResult.value = false
                        failureMessage.value = it.message
                    } else {
                        parseIsoDataOfMiniStatement(it.miniStatementData)
                        miniStatementResult.value = true
                    }
                }, {
                    miniStatementSuccess.value = false
                    message.value = it.message
                })
        )
    }

    private fun parseIsoDataOfMiniStatement(miniStatementDataResponse: String) {
//        val rawDataFromIso = """
//        20220908             PUR TANGAL BBSM TEST MERCHANTKAT        D            10.0020220908             PUR TANGAL BBSM TEST MERCHANTKAT        D            10.0020220908             PUR TANGAL BBSM TEST MERCHANTKAT        D            10.0020220908             PUR TANGAL BBSM TEST MERCHANTKAT        D            10.0020220908             PUR TANGAL BBSM TEST MERCHANTKAT        D            10.0020190318             NIBSF1-DIV74/75                         C          3968.3420190315             CASH WD NIBL DURBARMARG I      KATHMANDUD          5000.0020190315             usha (doc fee + medicine)               D          3203.0020190310             CASH WD NIBL-PULCHOWK-II       LALITPUR D         10000.0020190310             CASH WD NIBL JAWALAKHEL          LALITPUD         10000.00
//    """.trimIndent()

        val rawDataFromIso = miniStatementDataResponse.trimIndent()

        val lines = rawDataFromIso.chunked(79)

        for (line in lines) {
            if (line.length >= 79) {
                val date = line.substring(0, 8)
                val lastSixCharacters = date.substring(date.length - 6)
                val formattedDate = "${lastSixCharacters.substring(4)}/${
                    lastSixCharacters.substring(
                        2,
                        4
                    )
                }/${lastSixCharacters.substring(0, 2)}".trim()
                val description = line.substring(21, 61).trim()
                val dbCrIndicator = line.substring(61, 62).trim()
                var dbCrModifiedIndicator = ""
                if (dbCrIndicator == "D") {
                    dbCrModifiedIndicator = "Dr".trim()
                } else {
                    dbCrModifiedIndicator = "Cr".trim()
                }
                val amount = line.substring(62, 79).trim()
                val dataEntry =
                    StatementItems(formattedDate, description, dbCrModifiedIndicator, amount)
                miniStatementData.add(dataEntry)
            }
        }
        miniStatementResult.value = true
    }

    fun printMiniStatement() {
        isLoading.value = true
        if (miniStatementData != {}) {
            compositeDisposable.add(
                customerCopyUseCase.printStatement(
                    StatementList(
                        miniStatementData,
                        "",
                        "",
                        "",
                        AppUtility.checkCurrencyCode(currencyCode)
                    )
                )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        onCustomerCopyResponseEntityReceived(it)
                    }, {
                        customerCopyPrintMessage.postValue("Customer Print Failure")
                        isLoading.value = false
                    })
            )
        }

    }

    private fun onCustomerCopyResponseEntityReceived(it: CustomerCopyResponseEntity) {
        isLoading.value = false
        if (it.result == Result.SUCCESS || it.result == Result.CANCEL) {
            checkForCardPresent()
            return
        }
        customerCopyPrintMessage.postValue(it.message)
    }

}