package global.citytech.finpos.merchant.data.repository.core.purchase

import global.citytech.finpos.merchant.data.datasource.core.purchase.PurchaseDataSource
import global.citytech.finpos.merchant.domain.model.purchase.PurchaseResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.purchase.PurchaseRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.purchase.ManualPurchaseRequestItem
import global.citytech.finpos.merchant.presentation.model.purchase.PurchaseRequestItem
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 8/26/2020.
 */
class PurchaseRepositoryImpl(private val purchaseDataSource: PurchaseDataSource) : PurchaseRepository {
    override fun purchase(
        configurationItem: ConfigurationItem,
        purchaseRequestItem: PurchaseRequestItem
    ): Observable<PurchaseResponseEntity> {
        return purchaseDataSource.purchase(configurationItem, purchaseRequestItem)
    }

    override fun manualPurchase(
        configurationItem: ConfigurationItem,
        manualPurchaseRequestItem: ManualPurchaseRequestItem
    ): Observable<PurchaseResponseEntity> {
        return purchaseDataSource.manualPurchase(
            configurationItem,
            manualPurchaseRequestItem
        )
    }
}