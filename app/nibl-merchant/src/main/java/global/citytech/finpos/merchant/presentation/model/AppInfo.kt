package global.citytech.finpos.merchant.presentation.model

data class AppInfo(
    var id: String? = null,
    var version: Int? = null
)