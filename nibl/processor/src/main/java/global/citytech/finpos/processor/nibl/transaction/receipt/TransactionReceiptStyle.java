package global.citytech.finpos.processor.nibl.transaction.receipt;


import global.citytech.finposframework.hardware.io.printer.Style;

public enum TransactionReceiptStyle {
    RETAILER_NAME(Style.Align.CENTER, Style.FontSize.XLARGE, 1, true, true, false, false),
    RETAILER_ADDRESS(Style.Align.CENTER, Style.FontSize.SMALL, 1, false, false, false, false),
    START_DATE_TIME(Style.Align.LEFT, Style.FontSize.SMALL, 2, true, false, false, false),
    MID_TID(Style.Align.LEFT, Style.FontSize.SMALL, 2, true, false, false, false),
    RRN_STAN(Style.Align.LEFT, Style.FontSize.SMALL, 2, true, false, false, false),
    BATCH_NUMBER(Style.Align.LEFT, Style.FontSize.NORMAL, 2, true, false, false, false),
    INVOICE_NUMBER(Style.Align.LEFT, Style.FontSize.NORMAL, 2, true, false, false, false),
    CARD_SCHEME(Style.Align.LEFT, Style.FontSize.NORMAL, 2, false, false, false, false),
    EMI_DETAIL(Style.Align.LEFT, Style.FontSize.NORMAL, 2, false, false, false, false),
    AID(Style.Align.LEFT, Style.FontSize.NORMAL, 2, false, false, false, false),
    PURCHASE_TYPE(Style.Align.CENTER, Style.FontSize.XLARGE, 1, true, true, false, false),
    CARD_NUMBER(Style.Align.LEFT, Style.FontSize.NORMAL, 2, false, true, false, false),
    PURCHASE_AMOUNT_LABEL(Style.Align.LEFT, Style.FontSize.XLARGE, 1, true, true, false, false),
    PURCHASE_AMOUNT(Style.Align.CENTER, Style.FontSize.XLARGE, 1, true, true, false, false),
    AMOUNT(Style.Align.LEFT, Style.FontSize.LARGE, 2, true, false, false, false),
    TRANSACTION_STATUS(Style.Align.CENTER, Style.FontSize.LARGE, 1, true, false, false, false),
    TRANSACTION_MESSAGE(Style.Align.CENTER, Style.FontSize.SMALL, 1, true, false, false, false),
    SIGNATURE_MESSAGE(Style.Align.LEFT, Style.FontSize.SMALL, 1, true, false, false, false),
    SIGNATURE_LINE(Style.Align.CENTER, Style.FontSize.XLARGE, 1, true, false, false, false),
    DEBIT_CREDIT_ACK(Style.Align.LEFT, Style.FontSize.SMALL, 1, true, false, false, false),
    CARD_HOLDER_NAME(Style.Align.CENTER, Style.FontSize.NORMAL, 1, true, false, false, false),
    APPROVAL_CODE(Style.Align.LEFT, Style.FontSize.NORMAL, 2, true, false, false, false),
    END_DATE_TIME(Style.Align.LEFT, Style.FontSize.SMALL, 2, true, false, false, false),
    THANK_YOU_MESSAGE(Style.Align.CENTER, Style.FontSize.SMALL, 1, true, false, false, false),
    RETAIN_RECEIPT_MESSAGE(Style.Align.CENTER, Style.FontSize.SMALL, 1, true, false, false, false),
    RECEIPT_VERSION(Style.Align.CENTER, Style.FontSize.LARGE, 1, true, false, false, false),
    EMV_TAG_DATA(Style.Align.LEFT, Style.FontSize.SMALL, 1, true, false, false, false),
    DIVIDER(Style.Align.CENTER, Style.FontSize.XSMALL, 1, false, false, false, false),
    DEBUG_LOG(Style.Align.LEFT, Style.FontSize.SMALL, 1, true, false, false, false),
    DATA_ELEMENT(Style.Align.LEFT, Style.FontSize.SMALL, 3, true, false, false, false),
    EMV_TAG_LABEL_VALUE(Style.Align.LEFT, Style.FontSize.SMALL, 1, true, false, false, false),
    BAR_CODE_LABEL(Style.Align.CENTER, Style.FontSize.LARGE, 1, true, false, false, false),
    MINISTATEMENT_AMOUNT(Style.Align.RIGHT, Style.FontSize.SMALL, 2, true, false, false, false),
    LARGE(Style.Align.CENTER, Style.FontSize.LARGE, 1, false, false, false, false),
    LARGE_BOLD(Style.Align.CENTER, Style.FontSize.LARGE, 1, false, true, false, false),
    LARGE_ITALIC(Style.Align.CENTER, Style.FontSize.LARGE, 1, false, false, true, false),
    NORMAL(Style.Align.LEFT, Style.FontSize.NORMAL, 1, false, false, false, false),
    NORMAL_BOLD(Style.Align.LEFT, Style.FontSize.NORMAL, 1, false, true, false, false),
    SMALL(Style.Align.LEFT, Style.FontSize.SMALL, 1, false, false, false, false),
    NORMAL_ITALIC(Style.Align.LEFT, Style.FontSize.NORMAL, 2, false, false, true, false);

    private Style.Align alignment;
    private Style.FontSize fontSize;
    private int columns;
    private boolean allCaps;
    private boolean bold;
    private boolean italic;
    private boolean underline;

    TransactionReceiptStyle(Style.Align alignment, Style.FontSize fontSize, int columns, boolean allCaps, boolean bold, boolean italic, boolean underline) {
        this.alignment = alignment;
        this.fontSize = fontSize;
        this.columns = columns;
        this.allCaps = allCaps;
        this.bold = bold;
        this.italic = italic;
        this.underline = underline;
    }

    public Style getStyle() {
        return new Style.Builder()
                .alignment(this.alignment)
                .allCaps(this.allCaps)
                .bold(this.bold)
                .fontSize(this.fontSize)
                .italic(this.italic)
                .multipleAlignment(this.isMultipleAlignment())
                .underline(this.underline)
                .build();
    }

    public int[] columnWidths() {
        int num = 100 / this.columns;
        int[] columnWidths = new int[this.columns];
        for (int i = 0; i < this.columns; i++) {
            columnWidths[i] = num;
        }
        return columnWidths;
    }

    private boolean isMultipleAlignment() {
        return this.columns > 1;
    }
}
