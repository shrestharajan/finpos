package global.citytech.finposframework.comm;

import org.junit.Assert;
import org.junit.Test;

/** User: Surajchhetry Date: 2/28/20 Time: 9:52 AM */
public class TPDUTest {

  @Test
  public void fromHexStringTest() {
    String tpduStrin = "03030000";
    TPDU tpdu = TPDU.fromHexString(tpduStrin);
    byte[] tpduBytes = {0x60, 0x03, 0x03, 0x00, 0x00};
    Assert.assertArrayEquals(tpduBytes, tpdu.getAsBytes());
  }
}
