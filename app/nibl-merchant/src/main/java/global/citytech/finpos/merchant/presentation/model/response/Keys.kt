package global.citytech.finpos.nibl.merchant.presentation.model.response

import global.citytech.finpos.merchant.presentation.model.response.TlsCertificate

data class Keys(
    var devicePrivateKey: List<DevicePrivateKey>? = null,
    var switchPublicKey: List<SwitchPublicKey>? = null,
    var tlsCertificate: TlsCertificate? = null
)