package global.citytech.finpos.merchant.framework.broadcasts

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Environment
import global.citytech.finpos.merchant.presentation.dashboard.temp.DashViewModel
import global.citytech.finposframework.log.Logger
import java.io.File
import java.text.SimpleDateFormat

class LogPushAlarmReceiver : BroadcastReceiver() {

    private val logger = Logger(LogPushAlarmReceiver::class.java)
    private val LOGS_FOLDER_PATH = "/CityTechLogcat/finPOS-Cashier/"
    override fun onReceive(context: Context?, intent: Intent?) {
        if (context != null) {
            if (isInternetAvailable(context)) {
                val folderPath =
                    Environment.getExternalStorageDirectory().absolutePath + LOGS_FOLDER_PATH
                val collectedFilePaths = collectFilePathsFromDirectory(folderPath)
                if (collectedFilePaths.isNotEmpty()) {
                    val application = context.applicationContext as Application
                    val viewModel = DashViewModel(application)
                    viewModel.pushLogFileToServer(collectedFilePaths)
                    for (filePath in collectedFilePaths) {
                        logger.log(filePath)
                    }
                } else {
                    logger.log("No files were found in the directory.")
                }

            } else {
                logger.log("There is no internet connection")
            }
        }
    }

    private fun isInternetAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected
    }

    private fun collectFilePathsFromDirectory(folderPath: String): List<String> {
        val directory = File(folderPath)
        val directoryPaths: ArrayList<String> = ArrayList()
        val filePaths: ArrayList<String> = ArrayList()

        if (directory.exists() && directory.isDirectory) {
            val subdirectories = directory.listFiles()

            if (subdirectories != null) {
                val dateFormatter = SimpleDateFormat("yyyy-MM-dd")

                for (subdirectory in subdirectories) {
                    if (subdirectory.isDirectory) {
                        val subdirectoryDate = subdirectory.name

                        try {
                            val date = dateFormatter.parse(subdirectoryDate)
                            if (date != null) {
                                val fileName = "logcat_" + subdirectoryDate + ".txt"
                                val file = File(subdirectory, fileName)

                                if (file.exists() && file.isFile) {
                                    filePaths.add(file.absolutePath)
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        directoryPaths.add(subdirectory.absolutePath)
                    }
                }
            }
        }
        return filePaths
    }
}
