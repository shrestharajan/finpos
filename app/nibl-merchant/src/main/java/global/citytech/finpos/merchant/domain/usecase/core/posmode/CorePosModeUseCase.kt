package global.citytech.finpos.merchant.domain.usecase.core.posmode

import global.citytech.finpos.merchant.domain.repository.core.posmode.PosModeRepository
import global.citytech.finposframework.switches.posmode.PosMode

/**
 * Created by Unique Shakya on 1/7/2021.
 */
class CorePosModeUseCase(private val posModeRepository: PosModeRepository) {
    fun getPosMode(merchantCategoryCode: String): PosMode {
        return posModeRepository.getPosMode(merchantCategoryCode)
    }
}