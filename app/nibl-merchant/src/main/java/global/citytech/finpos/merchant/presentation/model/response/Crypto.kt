package global.citytech.finpos.merchant.presentation.model.response

import global.citytech.finpos.nibl.merchant.presentation.model.response.Values


data class Crypto(
    var type: String? = null,
    var values: Values? = null
)