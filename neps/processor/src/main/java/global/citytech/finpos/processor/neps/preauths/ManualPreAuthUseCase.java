package global.citytech.finpos.processor.neps.preauths;

import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.ProcessingCode;
import global.citytech.finpos.neps.iso8583.requestsender.preauths.ManualPreAuthRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.preauths.PreAuthIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.processor.neps.transaction.ManualTransactionUseCase;
import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.DateUtils;

/**
 * Created by Saurav Ghimire on 2/5/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class ManualPreAuthUseCase extends ManualTransactionUseCase {
    public ManualPreAuthUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    protected TransactionType getOriginalTransactionType() {
        return TransactionType.PRE_AUTH;
    }

    @Override
    protected TransactionType getTransactionType() {
        return TransactionType.PRE_AUTH;
    }


    @Override
    protected String getClassName() {
        return ManualPreAuthUseCase.class.getName();
    }

    @Override
    protected TransactionIsoRequest prepareIsoRequest(ReadCardResponse readCardResponse, TransactionRequest transactionRequest) {
        return PreAuthIsoRequest.Builder.newInstance()
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .transactionAmount(readCardResponse.getCardDetails().getAmount())
                .cvv(readCardResponse.getCardDetails().getCvv())
                .localDateTime(DateUtils.yyMMddHHmmssDate())
                .currencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .pinBlock(shouldSkipPinBlockFromCard(readCardResponse) ? "" : readCardResponse.getCardDetails().getPinBlock().getPin())
                .build();
    }


    @Override
    protected NepsMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context) {
        return new ManualPreAuthRequestSender(context);
    }


    @Override
    protected TransactionResponse prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository, boolean shouldPrint) {
        return PreAuthResponse.Builder.createDefaultBuilder()
                .stan(stan)
                .approved(transactionApproved)
                .shouldPrintCustomerCopy(shouldPrint)
                .debugRequestMessage(isoMessageResponse.getDebugRequestString())
                .debugResponseMessage(isoMessageResponse.getDebugResponseString())
                .message(this.retrieveMessage(isoMessageResponse, transactionApproved, applicationRepository))
                .build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.PRE_AUTHORIZATION.getCode();
    }

    @Override
    protected TransactionResponse prepareErrorResponse(String responseMessage, boolean shouldPrint) {
        return PreAuthResponse.Builder.createDefaultBuilder()
                .stan(this.stan)
                .message(responseMessage)
                .approved(false)
                .shouldPrintCustomerCopy(shouldPrint)
                .build();
    }
}
