package global.citytech.finpos.processor.nibl.logon;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.hardware.io.printer.PrintMessage;
import global.citytech.finposframework.hardware.io.printer.Printable;
import global.citytech.finposframework.hardware.io.printer.PrinterRequest;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.receipt.logon.MerchantLogonReceipt;

public class NiblMerchantLogonReceiptPrintHandler implements ReceiptHandler.MerchantLogonReceiptHandler {

    private PrinterService printerService;

    public NiblMerchantLogonReceiptPrintHandler(PrinterService printerService) {
        this.printerService = printerService;
    }

    @Override
    public void printLogonReceipt(MerchantLogonReceipt merchantLogonReceipt) {
        PrinterRequest printerRequest = this.preparePrinterRequest(merchantLogonReceipt);
        printerService.print(printerRequest);
    }

    private PrinterRequest preparePrinterRequest(MerchantLogonReceipt merchantLogonReceipt) {
        List<Printable> printableList = new ArrayList<>();
        printableList.add(PrintMessage.Companion.getInstance(NiblMerchantLogonReceiptLabels.DIVIDER, NiblMerchantLogonReceiptStyle.DIVIDER.getStyle()));
        printableList.add(PrintMessage.Companion.getInstance(merchantLogonReceipt.getMessage(), NiblMerchantLogonReceiptStyle.LOGON_MESSAGE.getStyle()));
        return new PrinterRequest(printableList);
    }
}
