package global.citytech.neps.datasource.core.configuration

import global.citytech.neps.App
import global.citytech.neps.data.datasource.core.configuration.ConfigurationDataSource
import global.citytech.neps.datasource.core.transaction.PrinterServiceImpl
import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.posswitchintregator.api.SwitchManager
import global.citytech.posswitchintregator.api.logon.LogOnResponseParameter
import global.citytech.posswitchintregator.neps.usecases.keyexchange.KeyExchangeRequestModel
import global.citytech.posswitchintregator.neps.usecases.keyexchange.KeyExchangeResponseModel
import global.citytech.posswitchintregator.neps.usecases.logon.LogOnRequestModel
import io.reactivex.Observable

class ConfigurationDataSourceImpl :
    ConfigurationDataSource {
    override fun logOn(configurationItem: ConfigurationItem): Observable<LogOnResponseParameter> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepo =
            TerminalRepositoryImpl(
                configurationItem
            )
        val printerService = PrinterServiceImpl(App.activityContext)
        val logOnRequester = SwitchManager.getInterface(terminalRepo)!!.logOnRequest
        return Observable.fromCallable {
            logOnRequester.execute(LogOnRequestModel(printerService))
        }
    }

    override fun exchangeKey(configurationItem: ConfigurationItem): Observable<KeyExchangeResponseModel> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepo =
            TerminalRepositoryImpl(
                configurationItem
            )
        val keyExchangeRequester = SwitchManager.getInterface(terminalRepo)!!.keyExchangeRequester
        return Observable.fromCallable {
            keyExchangeRequester.execute(KeyExchangeRequestModel()) as KeyExchangeResponseModel
        }
    }
}