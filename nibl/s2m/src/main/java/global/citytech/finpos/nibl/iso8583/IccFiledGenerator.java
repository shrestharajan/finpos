package global.citytech.finpos.nibl.iso8583;

public interface IccFiledGenerator {

    public byte[] generate();
}
