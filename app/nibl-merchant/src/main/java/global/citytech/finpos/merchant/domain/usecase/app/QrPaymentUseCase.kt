package global.citytech.finpos.merchant.domain.usecase.app

import global.citytech.finpos.merchant.domain.model.app.QrPayment
import global.citytech.finpos.merchant.domain.repository.app.QrPaymentRepository

class QrPaymentUseCase(private val qrPaymentRepository: QrPaymentRepository) {

    fun getAllQrPayments(): MutableList<QrPayment> = qrPaymentRepository.getAllQrPayments()

    fun getLimitedQrPayments(pageNumber: Int, offset: Int): MutableList<QrPayment> =
        qrPaymentRepository.getLimitedQrPayments(pageNumber, offset)

    fun getLimitedQrPaymentsBySearchParam(
        searchParam: String,
        pageSize: Int,
        offset: Int
    ): MutableList<QrPayment> =
        qrPaymentRepository.getLimitedQrPaymentsBySearchParam(searchParam, pageSize, offset)

    fun getCount(): Long = qrPaymentRepository.getCount()

    fun getCountBySearchParam(searchParam: String): Long =
        qrPaymentRepository.getCountBySearchParam(searchParam)

    fun addQrPayment(qrPayment: QrPayment) = qrPaymentRepository.addQrPayment(qrPayment)

    fun removeQrPayment(qrPayment: QrPayment) =
        qrPaymentRepository.removeQrPayment(qrPayment)

    fun updateQrPaymentStatus(status: String, qrPayment: QrPayment) =
        qrPaymentRepository.updateQrPaymentStatus(status, qrPayment)

    fun getLastUpdateDateTime(): String = qrPaymentRepository.getLastUpdateDateTime()

    fun getBillingInvoiceNumber(): String = qrPaymentRepository.getBillingInvoiceNumber()

    fun incrementBillingInvoiceNumber() =
        qrPaymentRepository.incrementBillingInvoiceNumber()

    fun getQrPaymentByInvoiceNumber(invoiceNumber: String): QrPayment? =
        qrPaymentRepository.getQrPaymentByInvoiceNumber(invoiceNumber)

    fun updateQrPaymentRefunded(qrPayment: QrPayment) =
        qrPaymentRepository.updateQrPaymentRefunded(qrPayment)

    fun updateQrPaymentVoided(qrPayment: QrPayment) =
        qrPaymentRepository.updateQrPaymentVoided(qrPayment)

    fun clear() = qrPaymentRepository.clear()

    fun saveBillingInvoiceNumber(invoiceNumber:String) = qrPaymentRepository.saveBillingInvoiceNumber(invoiceNumber)

    fun generateFonepayQrIdentifiers(key: String): String = qrPaymentRepository.generateFonepayQrIdentifiers(key)
}