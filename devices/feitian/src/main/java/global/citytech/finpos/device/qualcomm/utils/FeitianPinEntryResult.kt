package global.citytech.finpos.device.qualcomm.utils

import global.citytech.finposframework.hardware.common.Result

/**
 * Created by Rishav Chudal on 3/16/21.
 */
data class FeitianPinEntryResult(
    val result: Result,
    val tlvData: String
)