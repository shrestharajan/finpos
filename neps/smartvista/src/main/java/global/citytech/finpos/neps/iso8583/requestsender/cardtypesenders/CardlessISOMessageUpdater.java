package global.citytech.finpos.neps.iso8583.requestsender.cardtypesenders;

import global.citytech.finpos.neps.iso8583.requestsender.transaction.CardTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finposframework.iso8583.Iso8583Msg;

public class CardlessISOMessageUpdater implements CardTypeISOMessageUpdater {
    @Override
    public Iso8583Msg update(TransactionIsoRequest transactionIsoRequest,Iso8583Msg msg) {
        return msg;
    }
}
