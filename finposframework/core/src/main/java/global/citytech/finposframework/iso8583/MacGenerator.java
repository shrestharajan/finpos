package global.citytech.finposframework.iso8583;

/** User: Surajchhetry Date: 2/28/20 Time: 5:01 PM */
public interface MacGenerator {

  byte[] generate(byte[] mackingdata);
}
