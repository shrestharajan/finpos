package global.citytech.finpos.merchant.data.repository.device

import global.citytech.finpos.merchant.data.datasource.device.DeviceConfigurationSource
import global.citytech.finpos.merchant.domain.model.device.DeviceResponse
import global.citytech.finpos.merchant.domain.model.device.LoadParameterRequest
import global.citytech.finpos.merchant.domain.repository.device.DeviceRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyInjectionRequest
import global.citytech.finposframework.utility.PosResponse
import io.reactivex.Observable

/**
 * Created by Rishav Chudal on 8/10/20.
 */
class DeviceRepositoryImpl(
    private val deviceConfigurationSource: DeviceConfigurationSource
): DeviceRepository {

    override fun inject(keyInjectionRequest: KeyInjectionRequest): Observable<DeviceResponse> {
       return this.deviceConfigurationSource.inject(keyInjectionRequest)
    }

    override fun initSdk(): Observable<DeviceResponse> {
        return this.deviceConfigurationSource.initSdk()
    }

    override fun loadTerminalParameters(
        loadParameterRequest: LoadParameterRequest
    ): Observable<DeviceResponse> {
        return this.deviceConfigurationSource.loadTerminalParameters(loadParameterRequest)
    }

    override fun closeUpIO() {
        this.deviceConfigurationSource.closeUpIO()
    }

    override fun cardPresentInIccReader(): Observable<Boolean> {
        return this.deviceConfigurationSource.cardPresentInIccReader()
    }

    override fun enableHardwareButtons(): Observable<DeviceResponse> {
        return this.deviceConfigurationSource.enableHardwareButtons()
    }

    override fun disableHardwareButtons(): Observable<DeviceResponse> {
        return this.deviceConfigurationSource.disableHardwareButtons()
    }

    override fun readCardDetails(configurationItem: ConfigurationItem): Observable<ReadCardResponse> {
        return this.deviceConfigurationSource.readCardDetails(configurationItem)
    }

    override fun posReady(cardTypeList: MutableList<CardType>?): Observable<PosResponse> {
        return this.deviceConfigurationSource.posReady(cardTypeList)
    }
}