package global.citytech.finpos.merchant.data.datasource.app.db.dao

import androidx.room.*
import global.citytech.finpos.merchant.domain.model.app.EmvKey

@Dao
interface EmvKeysDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(emvKeys: List<EmvKey>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(emvKey: EmvKey): Long

    @Query("SELECT id, rid, `index`, length, exponent, modules, hash_id, key_signature_id, checksum, expiry_date FROM emv_keys")
    fun getEmvKeys(): List<EmvKey>

    @Update
    fun update(emvKey: EmvKey): Int

    @Query("DELETE FROM emv_keys WHERE id = :id")
    fun deleteById(id: Long): Int

    @Query("DELETE FROM emv_keys")
    fun nukeTableData()
}