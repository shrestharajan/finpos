package global.citytech.finposframework.usecases.activity;


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/11/08 - 12:15 PM
 */

public enum ActivityLogStatus {
    SUCCESS,
    FAILED
}
