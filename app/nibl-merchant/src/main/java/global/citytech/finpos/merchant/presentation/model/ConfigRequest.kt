package global.citytech.finpos.merchant.presentation.model

data class ConfigRequest(
    var appInfo: AppInfo? = null,
    var deviceInfo: DeviceInfo? = null
)