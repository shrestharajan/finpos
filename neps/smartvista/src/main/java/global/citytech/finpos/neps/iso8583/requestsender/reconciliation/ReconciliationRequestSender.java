package global.citytech.finpos.neps.iso8583.requestsender.reconciliation;

import global.citytech.finpos.neps.iso8583.FunctionCodeConfig;
import global.citytech.finpos.neps.iso8583.NepsMessagePayloadBuilder;
import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.ProcessingCode;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.switches.FunctionCode;

public class ReconciliationRequestSender extends NepsMessageSenderTemplate {

    private final Logger logger = Logger.getLogger(ReconciliationRequestSender.class.getName());
    private final boolean isTrailer;

    public ReconciliationRequestSender(RequestContext context, boolean isTrailer) {
        super(context);
        this.isTrailer = isTrailer;
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Reconciliation,
                        MTI.MessageFunction.Advice,
                        MTI.Originator.Acquirer);
        ReconciliationISORequest settlementRequest = (ReconciliationISORequest) context.getRequest();
        Iso8583Msg msg;
        if (isTrailer) {
            msg = getTemplateIsoMessageForReconciliationTrailer(mti);
        } else {
            msg = getTemplateIsoMessageForReconciliation(mti);
            msg.addField(
                    new DataElement(
                            DataElement.AMOUNT_SETTLEMENT,
                            settlementRequest.getReconciliationTotals().toTotalsBlock()
                    )
            );
        }
        msg.addField(new DataElement(DataElement.SETTLEMENT_DATE, settlementRequest.getDate()));
        MessagePayload.Builder builder = new NepsMessagePayloadBuilder(this.specInfo, msg);
        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());
        return builder.build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.SETTLEMENT.getCode();
    }

    @Override
    protected String getFunctionCode() {
        return FunctionCodeConfig.getValue(FunctionCode.RECONCILICATION);
    }
}
