package global.citytech.finpos.nibl.iso8583.requestsender.cashin;

import global.citytech.finpos.nibl.iso8583.FunctionCodeConfig;
import global.citytech.finpos.nibl.iso8583.NIBLMessagePayloadBuilder;
import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.PosEntryModeConfig;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.switches.FunctionCode;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.StringUtils;

public class CashInRequestSender extends NIBLMessageSenderTemplate {

    private final Logger logger = Logger.getLogger(CashInRequestSender.class.getName());
    public CashInRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo,context);
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Financial,
                        MTI.MessageFunction.Request,
                        MTI.Originator.Acquirer);
        TransactionIsoRequest pinSetRequest = (TransactionIsoRequest) this.context.getRequest();
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);
        msg.addField(
                new DataElement(
                        DataElement.PRIMARY_ACCOUNT_NUMBER,
                        pinSetRequest.getPan()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.AMOUNT,
                        HelperUtils.toISOAmount(pinSetRequest.getTransactionAmount(), 12)
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.LOCAL_TIME,
                        pinSetRequest.getLocalTime()
                )
        );
        logger.debug("LOCAL_DATE" + DataElement.LOCAL_DATE);
        msg.addField(
                new DataElement(
                        DataElement.LOCAL_DATE,
                        pinSetRequest.getLocalDate()
                )

        );
        msg.addField(
                new DataElement(
                        DataElement.PAN_EXPIRE_DATE,
                        pinSetRequest.getExpireDate()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(pinSetRequest.getPosEntryMode())
                )
        );// Based on transaction source like ICC,Manual

        msg.addField(
                new DataElement(
                        DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE,
                        pinSetRequest.getPosConditionCode()
                )
        );

        /*msg.addField(
                new DataElement(
                        DataElement.CURRENCY_CODE,
                        pinSetRequest.getCurrencyCode()
                )
        );*/

        msg.addField(
                new DataElement(
                        DataElement.TRACK_II_DATA,
                        pinSetRequest.getTrack2Data()
                )
        );

//        msg.addField(
//                new DataElement(
//                        DataElement.RETRIEVAL_REFERENCE_NUMBER,
//                        pinSetRequest.getRetrievalReferenceNumber()
//                )
//        );
        msg.addField(
                new DataElement(
                        DataElement.PIN_BLOCK,
                        pinSetRequest.getPinBlock()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.PRIVATE_USE_FIELD_62,
                        pinSetRequest.getInvoiceNumber()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.ICC_SYSTEM_RELATED_DATA,
                        pinSetRequest.getEmvData()
                )
        );

//        msg.addField(new DataElement(DataElement.APPLICATION_PAN_SEQUENCE_NUMBER,
//                StringUtils.ofRequiredLength(
//                        pinSetRequest.getCardSequenceNumber(),
//                        4
//                )));

        MessagePayload.Builder builder = new NIBLMessagePayloadBuilder(this.specInfo, msg);
        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());
        return builder.build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.CASH_IN.getCode();
    }

    @Override
    protected String getFunctionCode() {
        return FunctionCodeConfig.getValue(FunctionCode.CASH_IN);
    }
}
