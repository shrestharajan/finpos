package global.citytech.neps.data.db.dao

import android.database.Cursor
import androidx.room.*
import global.citytech.neps.domain.model.CardScheme
import global.citytech.neps.domain.model.EmvKey

@Dao
interface EmvKeyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(emvKeys: List<EmvKey>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(emvKey: EmvKey): Long

    @Query("SELECT * FROM emv_key")
    fun get(): Cursor

    @Query("SELECT * FROM emv_key")
    fun getEmvKeys(): List<EmvKey>

    @Query("SELECT * FROM emv_key where id = :id")
    fun getById(id: Long): Cursor?

    @Update
    fun update(emvKey: EmvKey):Int

    @Query("DELETE FROM emv_key WHERE id = :id")
    fun deleteById(id: Long): Int
}