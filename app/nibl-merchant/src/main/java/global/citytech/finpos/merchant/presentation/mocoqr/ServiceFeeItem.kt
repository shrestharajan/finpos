package global.citytech.finpos.merchant.presentation.mocoqr

data class ServiceFeeItem(
    val paymentNetwork: String,
    val domesticRate: String,
    val internationalRate: String
)
