package global.citytech.finpos.merchant.domain.utils;

//import com.ftpos.library.smartpos.util.TLV;
//import com.ftpos.library.smartpos.util.TlvUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import global.citytech.finpos.device.qualcomm.utils.FeitianLogger;
import global.citytech.finpos.merchant.domain.usecase.mobile.prepare.MobileNfcPaymentDataFormat;
import global.citytech.finpos.merchant.domain.usecase.mobile.prepare.MobileNfcPaymentDataObject;
import global.citytech.finposframework.hardware.utility.DeviceBytesUtil;
import global.citytech.finposframework.utility.HexString;


/**
 * @author SUSHIL THAPA
 */
public class MobileNfcTlvDataParser {
    private Map<String, MobileNfcPaymentDataObject> dataObjectMapNFC = new HashMap<>();
    Map<String, String> contentMap = new HashMap<>();

    public MobileNfcTlvDataParser() {
        this.setUpDataObjectNFC();
    }

    private void setUpDataObjectNFC() {
        ArrayList<MobileNfcPaymentDataObject> mobileNfcPaymentDataObjects = new ArrayList<MobileNfcPaymentDataObject>();
        MobileNfcPaymentDataObject.DataObjectBuilder dataObjectBuilder = new MobileNfcPaymentDataObject.DataObjectBuilder();
        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("FP01").format(MobileNfcPaymentDataFormat.N).length(10).presence(MobileNfcPaymentDataObject.PresenceType.M).build());
        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("FP02").format(MobileNfcPaymentDataFormat.N).length(13).presence(MobileNfcPaymentDataObject.PresenceType.M).build());
        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("FP03").format(MobileNfcPaymentDataFormat.N).length(150).presence(MobileNfcPaymentDataObject.PresenceType.M).build());
        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("FP04").format(MobileNfcPaymentDataFormat.N).length(64).presence(MobileNfcPaymentDataObject.PresenceType.M).build());

//        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("85").format(MobileNfcPaymentDataFormat.N).name("Payload Indicator").length(5).presence(MobileNfcPaymentDataObject.PresenceType.M).value("").childTagPresence(false).build());
//        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("61").format(MobileNfcPaymentDataFormat.N).name("Application Template ").presence(MobileNfcPaymentDataObject.PresenceType.M).value("").childTagPresence(true).build());
//        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("4F").format(MobileNfcPaymentDataFormat.N).name("Application Definition Filename").length(10).presence(MobileNfcPaymentDataObject.PresenceType.M).value("").childTagPresence(false).build());
//        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("50").format(MobileNfcPaymentDataFormat.N).name("Application Label").length(8).presence(MobileNfcPaymentDataObject.PresenceType.M).value("").childTagPresence(false).build());
//        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("62").format(MobileNfcPaymentDataFormat.N).name("Common Data Template").presence(MobileNfcPaymentDataObject.PresenceType.M).value("").childTagPresence(true).build());
//        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("5F20").format(MobileNfcPaymentDataFormat.N).name("Payer PAN").length(10).presence(MobileNfcPaymentDataObject.PresenceType.M).value("").childTagPresence(false).build());
//        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("5F2D").format(MobileNfcPaymentDataFormat.N).name("Language Preference").length(2).presence(MobileNfcPaymentDataObject.PresenceType.M).value("").childTagPresence(false).build());
//        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("64").format(MobileNfcPaymentDataFormat.N).name("Common Data Transparent Template").presence(MobileNfcPaymentDataObject.PresenceType.M).value("").childTagPresence(true).build());
//        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("9F19").format(MobileNfcPaymentDataFormat.N).name("Issuer Id").length(8).presence(MobileNfcPaymentDataObject.PresenceType.M).value("").childTagPresence(false).build());
//        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("9F24").format(MobileNfcPaymentDataFormat.N).name("Token").length(50).presence(MobileNfcPaymentDataObject.PresenceType.M).value("").childTagPresence(false).build());
//        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("9F36").format(MobileNfcPaymentDataFormat.N).name("Transaction Counter").length(2).presence(MobileNfcPaymentDataObject.PresenceType.M).value("").childTagPresence(false).build());
//        mobileNfcPaymentDataObjects.add(dataObjectBuilder.id("9F37").format(MobileNfcPaymentDataFormat.N).name(" T-OTP").length(6).presence(MobileNfcPaymentDataObject.PresenceType.M).value("").childTagPresence(false).build());

        for (MobileNfcPaymentDataObject obj :
                mobileNfcPaymentDataObjects) {
            this.dataObjectMapNFC.put(obj.getId(), obj);
        }

    }

    public Map parse(String content) {
        Map<String, String> contentMap = new HashMap<>();
        int index = 0;
        int contentLength = content.length();
        String objectId, objectValue = null;
        int objectLength;
        while (index < contentLength) {
            objectId = substring(content, index, 4);
            if (FixedSizedObjects.checkIfFixedSize(objectId)) {
                objectLength = dataObjectMapNFC.get(objectId).getLength();
                objectValue = substring(content, index + 4, objectLength);
                index = index + 4 + objectLength;
            } else {
                String objectLengthHex = substring(content, index + 4, 2);
                objectLength = Integer.decode("0x" + objectLengthHex);
                objectValue = substring(content, index + 4 + 2, objectLength);
                index = index + 4 + 2 + objectLength;
            }
            contentMap.put(objectId, objectValue);

        }
        return contentMap;
    }

//    public Map parse1(String content) {
//        FeitianLogger.debugStatic("ParserData::: " + content);
//        List<TLV> tlvList = TlvUtil.getTLVs(DeviceBytesUtil.hexString2Bytes(content));
//        if (tlvList.isEmpty()) {
//            for (int i = 0; i <= tlvList.size(); i++) {
//                TLV tlv = tlvList.get(i);
//                try {
//                    FeitianLogger.logStatic("***** TLV *****");
//                    String tag = tlv.getTagBytesAsHex().substring(2);
//                    FeitianLogger.debugStatic("TAG ::: " + tag);
//                    int tagLength = tlv.getLength();
//                    FeitianLogger.debugStatic("LENGTH ::: " + tagLength);
//                    String tagValue = DeviceBytesUtil.bytes2HexString(tlv.getValueBytes());
//                    FeitianLogger.debugStatic("VALUE ::: " + tagValue);
//                    contentMap.put(tag, hexToAscii(tagValue));
//                    if (dataObjectMapNFC.containsKey(tag)) {
//                        MobileNfcPaymentDataObject mobileNfcPaymentDataObject = dataObjectMapNFC.get(tag);
//                        mobileNfcPaymentDataObject.setLength(tagLength);
//                        mobileNfcPaymentDataObject.setValue(HexString.hexToString(tagValue));
//                        if (mobileNfcPaymentDataObject.getChildTagPresence()) {
//                            parse1(hexToAscii(tagValue));
//                        }
//                    }
//                } catch (Exception ex) {
//                    FeitianLogger.logStatic("Exception in getting TAG ::: " + tlv.getTagBytesAsHex());
//                    ex.printStackTrace();
//                }
//
//            }
//        }
//        return contentMap;
//    }

    private String hexToAscii(String hexStr) {
        StringBuilder output = new StringBuilder("");
        int i = 0;
        while (i < hexStr.length()) {
            String str = hexStr.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));
            i += 2;
        }
        return output.toString();
    }

    public enum FixedSizedObjects {
        FP01,
        FP02;

        public static boolean checkIfFixedSize(String id) {
            for (FixedSizedObjects object :
                    FixedSizedObjects.values()) {
                if (object.name().equalsIgnoreCase(id)) {
                    return true;
                }
            }
            return false;
        }

    }

    public static String substring(String content, int beginIndex, int length) {
        return content.substring(beginIndex, beginIndex + length);
    }
}
