package global.citytech.finpos.merchant.domain.usecase.qr
/**
 * @author sachin
 */
class QrDataObjectContentDumper {

    fun dumpQrContent(qrDataObjects: List<QrDataObject>): String {
        val stringBuilder: StringBuilder = StringBuilder("")
        qrDataObjects.forEach { it ->
            run {
                stringBuilder.append(getQrContent(it))
            }
        }
        return stringBuilder.toString()
    }

    private fun getQrContent(qrDataObject: QrDataObject): String {
        return qrDataObject.getFieldId() + qrDataObject.getFieldContentLength() + qrDataObject.getFieldContent()
    }

}