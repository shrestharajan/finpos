package global.citytech.finpos.device.weipass.pinpad

import android.app.Activity
import android.content.Context
import com.wiseasy.emvprocess.Acquirer
import com.wiseasy.emvprocess.bean.PINSetting
import global.citytech.finpos.device.weipass.card.handler.ProcessInterfaceHandler
import global.citytech.finpos.device.weipass.card.handler.WeipassStateHandler
import global.citytech.finpos.device.weipass.utils.WeipassDeviceUtils
import global.citytech.finpos.device.weipass.utils.WeipassTransState
import global.citytech.finpos.device.weipass.utils.WeipassTransStateHandler
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse
import global.citytech.finposframework.hardware.keymgmt.pin.PinService
import java.lang.ref.WeakReference

/**
 * Created by Rishav Chudal on 6/9/20.
 */
class WeipassPinPadServiceImpl constructor(val context: Context):
    PinService {
    private var contextWeakReference = WeakReference(context)
    private var processInterfaceHandler = ProcessInterfaceHandler()

    override fun showPinPadAndCalculatePinBlock(pinRequest: PinRequest): PinResponse {
        WeipassStateHandler.transState = WeipassTransState.NONE
        var pinResponse =
            PinResponse(
                Result.PINPAD_ERROR,
                null
            )
        if (contextWeakReference.get() != null && pinRequest.packageName != null) {
            WeipassStateHandler.cardSummary = pinRequest.cardSummary
            WeipassStateHandler.pinpadMessage = pinRequest.pinPadMessage
            WeipassStateHandler.pinPadAmountMessage = pinRequest.amountMessage
            WeipassStateHandler.readCardRequest!!.isPinpadRequired= pinRequest.isByPass
            val pinSetting = preparePINSetting(pinRequest)
            if(!pinSetting.isBypass){
                Acquirer.getInstance().startPINInput(
                    (contextWeakReference.get() as Activity),
                    pinSetting,
                    processInterfaceHandler
                )
                val transState = WeipassStateHandler.transState!!
                pinResponse = analyzePinResponseBasedOn(transState)
            }else{
                val transState= WeipassTransState.PINPAD_BYPASS
                pinResponse= analyzePinResponseBasedOn(transState);
            }

        }
        return pinResponse
    }

    private fun preparePINSetting(
        pinRequest: PinRequest
        ) : PINSetting {

        val pinSetting = PINSetting()
        pinSetting.packageName = pinRequest.packageName
        pinSetting.isBypass = pinRequest.isByPass!!
        pinSetting.minPINLength = WeipassDeviceUtils.getMinmPinLengthOrDefault(pinRequest.pinMinLength)
        pinSetting.maxPINLength = WeipassDeviceUtils.getMinmPinLengthOrDefault(pinRequest.pinMaxLength)
        pinSetting.timeOut = WeipassDeviceUtils.getPinpadTimeoutOrDefault(pinRequest.pinPadTimeout)
        pinSetting.isDUKPT = false
        pinSetting.isFixLayout = pinRequest.pinPadFixedLayout
        pinSetting.pan = pinRequest.primaryAccountNumber
        pinSetting.pinBlockFormat = WeipassDeviceUtils.getPinBlockFormatOrDefault(
            pinRequest.pinBlockFormat
        )
        return pinSetting
    }

    private fun analyzePinResponseBasedOn(transState: WeipassTransState): PinResponse {
        val pinResponse: PinResponse
        if (WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler)) {
            pinResponse =
                PinResponse(
                    Result.SUCCESS,
                    WeipassStateHandler.pinBlock
                )
        } else {
            val readCardResponse = WeipassStateHandler.readCardResponse
            pinResponse =
                PinResponse(
                    readCardResponse!!.result,
                    null
                )
        }
        return pinResponse
    }
}