package global.citytech.nepsadmin.data.response

import android.net.Uri
import global.citytech.nepsadmin.presentation.main.MainViewModel


data class TerminalParameter(
    var aidParameters: ArrayList<AidParam>? = null,
    var emvKeys: ArrayList<EmvKey>? = null,
    var emvParameters: EmvParam? = null,
    var cardSchemes: ArrayList<Any>? = null

)