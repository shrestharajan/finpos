package global.citytech.payment.sdk.api;

/**
 * Created by BikashShrestha on 5/31/19.
 */
public enum PaymentResult {
    SUCCESS(0, "Success"),
    DECLINED(-100, "Declined"),
    FAILED(-101, "Failed"),
    CANCEL(-102, "Cancel"),
    TIME_OUT(-103, "Timeout"),
    INVALID_AMOUNT(-104, "Invalid amount"),
    INVALID_CURRENCY(-110, "Invalid Currency"),
    INVALID_INVOICE_NUMBER(-109, "Invalid invoice number"),
    INVALID_DATA(-105, "Invalid data"),
    INVALID_APPLICATION_IDENTIFIER(-106, "Invalid Application Identifier"),
    VOID_TRANSACTION_NOT_FOUND(-107, "Transaction not found"),
    VOID_TRANSACTION_ALREADY_VOID(-108, "Transaction already void"),
    VOID_INVALID_RRN(-109, "Invalid RRN"),
    CANCELLATION_INVALID_PAYMENT_REQUEST_NUMBER(-110, "Invalid Payment Request for Cancellation"),
    CANCELLATION_INVALID_REQUESTER(-111, "Invalid Requester for Cancellation"),
    PAYMENT_IN_PROGRESS(-112, "Payment in Progress"),
    INVALID_REQUEST_NUMBER(-113, "Request Number for Cancellation is Invalid"),
    QR_PAYMENT_NOT_FOUND(-114, "QR Payment not found"),
    NFC_PAYMENT_ERROR(-115, "Nfc Payment Service Unavailable"),

    MAC_ERROR(-201, "MAC error"),
    MAC_INVALID_KEY(-202, "MAC invalid key"),
    MAC_INVALID_ALGORITHM(-203, "MAC invalid algorithm"),

    PRINTER_OUT_OF_PAPER(-301, "Out of paper"),
    PRINTER_ERROR(-302, "Unable to connect printer"),

    POS_NOT_CONFIGURED(-8, "POS application is not configured"),
    POS_PAYMENT_SERVICE_NOT_AVAILABLE(-9, "POS payment service not available"),
    POS_PAYMENT_FEATURE_NOT_ALLOWED(-10, "Pos payment feature not allowed"),
    POS_PAYMENT_INVALID_APPLICATION_IDENTIFIER(-11, "Invalid application identifier"),
    POS_PAYMENT_KEYS_NOT_CONFIGURE(-12, "POS Payment key is not configured"),

    POS_AUTO_REVERSAL_FAILED(-401, "Previous Reversal Failed. POS requires previous reversal to be cleared before transaction can be performed."),
    POS_SETTLEMENT_FAILED(-402, "Settlement could not be performed. POS requires settlement before transaction can be performed."),

    SDK_PROCESS_ERROR(-500, "SDK processing error");

    private final int resultCode;
    private final String message;

    PaymentResult(int resultCode, String message) {
        this.resultCode = resultCode;
        this.message = message;
    }

    public int getResultCode() {
        return resultCode;
    }

    public String getMessage() {
        return message;
    }
}
