package global.citytech.finpos.device.weipass.hardware

import android.content.Context
import com.wiseasy.emvprocess.LibInit
import com.wiseasy.emvprocess.SDKInstance
import global.citytech.finpos.device.weipass.utils.WeipassDeviceUtils
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getDeviceStatus_PRINTER_STATUS_NOT_EXIST
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getDeviceStatus_PRINTER_STATUS_OK
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getDeviceStatus_PRINTER_STATUS_OUT_OF_PAPER
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getDeviceStatus_PRINTER_STATUS_UNKNOWN
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getPrinterStatus_PRINTER_NOT_AVAILABLE
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getPrinterStatus_PRINTER_OUT_OF_PAPER
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getPrinterStatus_PRINTER_OVER_HEAT
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getPrinterStatus_PRINTER_PARAMETER_ERROR
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_API_getPrinterStatus_PRINTER_STATUS_OK
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_CONTACTLESS_READER_NOT_EXIST
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_CONTACTLESS_READER_OK
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_CONTACTLESS_READER_UNKNOWN
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_CONTACT_READER_NOT_EXIST
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_CONTACT_READER_OK
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_CONTACT_READER_UNKNOWN
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_MAG_READER_NOT_EXIST
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_MAG_READER_OK
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SDK_MAG_READER_UNKNOWN
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_PICC_READER_OK
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_PICC_READER_OUT_OF_ORDER
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_ICC_READER_NOT_SUPPORTED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_ICC_READER_OK
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_ICC_READER_OUT_OF_ORDER
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_MAG_READER_NOT_SUPPORTED
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_MAG_READER_OK
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_MAG_READER_OUT_OF_ORDER
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_PRINTER_NOT_AVAILABLE
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_PRINTER_OUT_OF_PAPER
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_PRINTER_PLAIN_RECEIPT_PAPER
import global.citytech.finposframework.hardware.io.status.IOStatusRequest
import global.citytech.finposframework.hardware.io.status.IOStatusResponse
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import wangpos.sdk4.libbasebinder.BankCard
import wangpos.sdk4.libbasebinder.Core
import wangpos.sdk4.libbasebinder.Printer

/**
 * Created by Rishav Chudal on 6/1/20.
 */
class WeipassIOStatusServiceImplTest {
    private lateinit var SUT: WeipassIOStatusServiceImpl

    @MockK
    private lateinit var mockedPrinter: Printer

    @MockK
    private lateinit var mockedBankCard: BankCard

    @MockK
    private lateinit var mockedCore: Core

    @MockK
    private lateinit var context: Context

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        SUT = WeipassIOStatusServiceImpl(context)
        mockSDKInstance()
        mockWeipassLogger()
        mockLibInit()
        mockkDeviceUtils()
    }

    /**
     * Only Printer Status Request cases
     */
    @Test
    fun retrieveHardwareStatus_OnPrinterStatusRequestOnlyAndApiGetPrinterStatusResultIsZeroAndPrinterStatusOk_ReturnsHardwareStatusResponseOfPrinterOk() {
        //Setup
        val request =
            IOStatusRequest(
                true
            )
        val sdkStatusArg = slot<IntArray>()
        every {
            SDKInstance.mPrinter.getPrinterStatus(capture(sdkStatusArg))
        } returns 0

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Veriy
        assertTrue(response is IOStatusResponse)

        assertTrue(request.isPrinterOnly)

        verify { SDKInstance.mPrinter.getPrinterStatus(any()) }
        verify { WeipassLogger.log("Printer Status Ok") }

        assertTrue(response.printerStatus == (CORE_PRINTER_PLAIN_RECEIPT_PAPER))
        assertTrue(response.magReaderStatus == (CORE_MAG_READER_OK))
        assertTrue(response.iccReaderStatus  == (CORE_ICC_READER_OK))
        assertTrue(response.piccReaderStatus == (CORE_PICC_READER_OK))
    }

    @Test
    fun retrieveHardwareStatus_OnPrinterStatusRequestOnlyAndApiGetPrinterStatusResultIsZeroAndPrinterStatusParameterError_ReturnsHardwareStatusResponseOfPrinterNotAvailable() {
        //Setup
        val request =
            IOStatusRequest(
                true
            )
        val sdkStatusArg = slot<IntArray>()
        every {
            SDKInstance.mPrinter.getPrinterStatus(capture(sdkStatusArg))
        } answers {
            sdkStatusArg.captured[0] = SDK_API_getPrinterStatus_PRINTER_PARAMETER_ERROR
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Veriy
        assertTrue(response is IOStatusResponse)

        assertTrue(request.isPrinterOnly)
        assertTrue(sdkStatusArg.isCaptured)

        assertEquals(SDK_API_getPrinterStatus_PRINTER_PARAMETER_ERROR, sdkStatusArg.captured[0])

        verify { SDKInstance.mPrinter.getPrinterStatus(any()) }
        verify { WeipassLogger.log("Printer Status Parameter Error") }

        assertTrue(response.printerStatus == (CORE_PRINTER_NOT_AVAILABLE))
        assertTrue(response.magReaderStatus == (CORE_MAG_READER_OK))
        assertTrue(response.iccReaderStatus  == (CORE_ICC_READER_OK))
        assertTrue(response.piccReaderStatus == (CORE_PICC_READER_OK))
    }

    @Test
    fun retrieveHardwareStatus_OnPrinterStatusRequestOnlyAndApiGetPrinterStatusResultIsZeroAndPrinterStatusNotAvailable_ReturnsHardwareStatusResponseOfPrinterNotAvailable() {
        //Setup
        val request =
            IOStatusRequest(
                true
            )
        val sdkStatusArg = slot<IntArray>()
        every {
            SDKInstance.mPrinter.getPrinterStatus(capture(sdkStatusArg))
        } answers {
            sdkStatusArg.captured[0] = SDK_API_getPrinterStatus_PRINTER_NOT_AVAILABLE
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Veriy
        assertTrue(response is IOStatusResponse)

        assertTrue(request.isPrinterOnly)
        assertTrue(sdkStatusArg.isCaptured)

        assertEquals(SDK_API_getPrinterStatus_PRINTER_NOT_AVAILABLE, sdkStatusArg.captured[0])

        verify { SDKInstance.mPrinter.getPrinterStatus(any()) }
        verify { WeipassLogger.log("Printer Status Not Available") }

        assertTrue(response.printerStatus == CORE_PRINTER_NOT_AVAILABLE)
        assertTrue(response.magReaderStatus == (CORE_MAG_READER_OK))
        assertTrue(response.iccReaderStatus  == (CORE_ICC_READER_OK))
        assertTrue(response.piccReaderStatus == (CORE_PICC_READER_OK))
    }

    @Test
    fun retrieveHardwareStatus_OnPrinterStatusRequestOnlyAndApiGetPrinterStatusResultIsZeroAndPrinterStatusOutOfPaper_ReturnsHardwareStatusResponseOfPrinterOutOfPaper() {
        //Setup
        val request =
            IOStatusRequest(
                true
            )
        val sdkStatusArg = slot<IntArray>()
        every {
            SDKInstance.mPrinter.getPrinterStatus(capture(sdkStatusArg))
        } answers {
            sdkStatusArg.captured[0] = SDK_API_getPrinterStatus_PRINTER_OUT_OF_PAPER
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Veriy
        assertTrue(response is IOStatusResponse)

        assertTrue(request.isPrinterOnly)
        assertTrue(sdkStatusArg.isCaptured)

        assertEquals(SDK_API_getPrinterStatus_PRINTER_OUT_OF_PAPER, sdkStatusArg.captured[0])

        verify { SDKInstance.mPrinter.getPrinterStatus(any()) }
        verify { WeipassLogger.log("Printer Status Out of Paper") }

        assertTrue(response.printerStatus == (CORE_PRINTER_OUT_OF_PAPER))
        assertTrue(response.magReaderStatus == (CORE_MAG_READER_OK))
        assertTrue(response.iccReaderStatus  == (CORE_ICC_READER_OK))
        assertTrue(response.piccReaderStatus == (CORE_PICC_READER_OK))
    }

    @Test
    fun retrieveHardwareStatus_OnPrinterStatusRequestOnlyAndApiGetPrinterStatusResultIsZeroAndPrinterStatusOverheat_ReturnsHardwareStatusResponseOfPrinterNotAvailable() {
        //Setup
        val request =
            IOStatusRequest(
                true
            )
        val sdkStatusArg = slot<IntArray>()
        every {
            SDKInstance.mPrinter.getPrinterStatus(capture(sdkStatusArg))
        } answers {
            sdkStatusArg.captured[0] = SDK_API_getPrinterStatus_PRINTER_OVER_HEAT
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Veriy
        assertTrue(response is IOStatusResponse)
        assertTrue(request.isPrinterOnly)
        assertTrue(sdkStatusArg.isCaptured)

        assertEquals(SDK_API_getPrinterStatus_PRINTER_OVER_HEAT, sdkStatusArg.captured[0])

        verify { SDKInstance.mPrinter.getPrinterStatus(any()) }
        verify { WeipassLogger.log("Printer Status Overheat") }

        assertTrue(response.printerStatus == (CORE_PRINTER_NOT_AVAILABLE))
        assertTrue(response.magReaderStatus == (CORE_MAG_READER_OK))
        assertTrue(response.iccReaderStatus  == (CORE_ICC_READER_OK))
        assertTrue(response.piccReaderStatus == CORE_PICC_READER_OK)
    }

    @Test
    fun retrieveHardwareStatus_OnPrinterStatusRequestOnlyAndApiGetPrinterStatusResultIsZeroAndPrinterStatusUnknown_ReturnsHardwareStatusResponseOfPrinterNotAvailable() {
        //Setup
        val request =
            IOStatusRequest(
                true
            )
        val sdkStatusArg = slot<IntArray>()
        every {
            SDKInstance.mPrinter.getPrinterStatus(capture(sdkStatusArg))
        } answers {
            sdkStatusArg.captured[0] = 0x33
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Veriy
        assertTrue(response is IOStatusResponse)
        assertTrue(request.isPrinterOnly)
        assertTrue(sdkStatusArg.isCaptured)

        assertNotEquals(SDK_API_getPrinterStatus_PRINTER_STATUS_OK, sdkStatusArg.captured[0])
        assertNotEquals(SDK_API_getPrinterStatus_PRINTER_OVER_HEAT, sdkStatusArg.captured[0])
        assertNotEquals(SDK_API_getPrinterStatus_PRINTER_OUT_OF_PAPER, sdkStatusArg.captured[0])
        assertNotEquals(SDK_API_getPrinterStatus_PRINTER_NOT_AVAILABLE, sdkStatusArg.captured[0])
        assertNotEquals(SDK_API_getPrinterStatus_PRINTER_PARAMETER_ERROR, sdkStatusArg.captured[0])

        verify { SDKInstance.mPrinter.getPrinterStatus(any()) }
        verify { WeipassLogger.log("Printer Status Unknown") }

        assertTrue(response.printerStatus == (CORE_PRINTER_NOT_AVAILABLE))
        assertTrue(response.magReaderStatus == (CORE_MAG_READER_OK))
        assertTrue(response.iccReaderStatus  == (CORE_ICC_READER_OK))
        assertTrue(response.piccReaderStatus == (CORE_PICC_READER_OK))
    }

    @Test
    fun retrieveHardwareStatus_OnPrinterStatusRequestOnlyAndApiGetPrinterStatusResultIsNotZeroAndPrinterStatusOk_ReturnsHardwareStatusResponseOfPrinterNotAvailable() {
        //Setup
        val request =
            IOStatusRequest(
                true
            )
        val sdkStatusArg = slot<IntArray>()
        every {
            SDKInstance.mPrinter.getPrinterStatus(capture(sdkStatusArg))
        } returns -1

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Veriy
        assertTrue(response is IOStatusResponse)
        assertTrue(request.isPrinterOnly)

        verify { SDKInstance.mPrinter.getPrinterStatus(any()) }

        assertTrue(response.printerStatus == (CORE_PRINTER_NOT_AVAILABLE))
        assertTrue(response.magReaderStatus == (CORE_MAG_READER_OK))
        assertTrue(response.iccReaderStatus  == (CORE_ICC_READER_OK))
        assertTrue(response.piccReaderStatus == (CORE_PICC_READER_OK))
    }

    @Test
    fun retrieveHardwareStatus_OnPrinterStatusRequestOnlyAndApiGetPrinterStatusThrowsException_ReturnsHardwareStatusResponseOfPrinterNotAvailable() {
        //Setup
        val request =
            IOStatusRequest(
                true
            )
        val sdkStatusArg = slot<IntArray>()
        every {
            SDKInstance.mPrinter.getPrinterStatus(capture(sdkStatusArg))
        } throws Exception("Test Exception")

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Veriy
        assertTrue(response is IOStatusResponse)
        assertTrue(request.isPrinterOnly)

        verify { SDKInstance.mPrinter.getPrinterStatus(any()) }

        assertTrue(response.printerStatus == (CORE_PRINTER_NOT_AVAILABLE))
        assertTrue(response.magReaderStatus == (CORE_MAG_READER_OK))
        assertTrue(response.iccReaderStatus  == (CORE_ICC_READER_OK))
        assertTrue(response.piccReaderStatus == (CORE_PICC_READER_OK))
    }

    /**
     * All Readers and Printer Status Request cases
     */
    //Printer
    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndPrinterIsOk_ReturnsHardwareStatusResponseWithPrinterOkAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[3] = 0x00
            outDataArg.captured[0] = 0x00
            outDataArg.captured[2] = 0x00
            outDataArg.captured[1] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }

        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OK.toByte(), outDataArg.captured[3])

        assertEquals(SDK_MAG_READER_OK.toByte(), outDataArg.captured[0])

        assertEquals(SDK_CONTACT_READER_OK.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_OK.toByte(), outDataArg.captured[1])

        assertEquals(CORE_PRINTER_PLAIN_RECEIPT_PAPER, response.printerStatus)
        assertEquals(CORE_MAG_READER_OK, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_OK, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OK, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_PLAIN_RECEIPT_PAPER,
                CORE_MAG_READER_OK,
                CORE_ICC_READER_OK,
                CORE_PICC_READER_OK
            )
        assertEquals(hardwareStatusResponse, response)
    }

    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndPrinterStatusUnknown_ReturnsHardwareStatusResponseWithPrinterNotAvailableAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[3] = 0x01
            outDataArg.captured[0] = 0x00
            outDataArg.captured[2] = 0x00
            outDataArg.captured[1] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }

        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_UNKNOWN.toByte(), outDataArg.captured[3])

        assertEquals(SDK_MAG_READER_OK.toByte(), outDataArg.captured[0])

        assertEquals(SDK_CONTACT_READER_OK.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_OK.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Printer Status Unknown") }

        verify { WeipassLogger.log("Mag Reader Status Ok") }

        verify { WeipassLogger.log("Contact Reader Status Ok") }

        verify { WeipassLogger.log("Contactless Reader Status Ok") }

        assertEquals(CORE_PRINTER_NOT_AVAILABLE, response.printerStatus)
        assertEquals(CORE_MAG_READER_OK, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_OK, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OK, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_NOT_AVAILABLE,
                CORE_MAG_READER_OK,
                CORE_ICC_READER_OK,
                CORE_PICC_READER_OK
            )
        assertEquals(hardwareStatusResponse, response)
    }

    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndPrinterOutOfPaper_ReturnsHardwareStatusResponseWithPrinterOutOfPaperAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[3] = 0x10
            outDataArg.captured[0] = 0x00
            outDataArg.captured[2] = 0x00
            outDataArg.captured[1] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }

        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OUT_OF_PAPER.toByte(), outDataArg.captured[3])

        assertEquals(SDK_MAG_READER_OK.toByte(), outDataArg.captured[0])

        assertEquals(SDK_CONTACT_READER_OK.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_OK.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Printer Status Out of Paper") }

        verify { WeipassLogger.log("Mag Reader Status Ok") }

        verify { WeipassLogger.log("Contact Reader Status Ok") }

        verify { WeipassLogger.log("Contactless Reader Status Ok") }

        assertEquals(CORE_PRINTER_OUT_OF_PAPER, response.printerStatus)
        assertEquals(CORE_MAG_READER_OK, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_OK, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OK, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_OUT_OF_PAPER,
                CORE_MAG_READER_OK,
                CORE_ICC_READER_OK,
                CORE_PICC_READER_OK
            )
        assertEquals(hardwareStatusResponse, response)
    }

    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndPrinterNotExist_ReturnsHardwareStatusResponseWithPrinterNotAvailableAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[3] = 0xFF.toByte()
            outDataArg.captured[0] = 0x00
            outDataArg.captured[2] = 0x00
            outDataArg.captured[1] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }

        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_NOT_EXIST.toByte(), outDataArg.captured[3])

        assertEquals(SDK_MAG_READER_OK.toByte(), outDataArg.captured[0])

        assertEquals(SDK_CONTACT_READER_OK.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_OK.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Printer Status Not Available") }

        verify { WeipassLogger.log("Mag Reader Status Ok") }

        verify { WeipassLogger.log("Contact Reader Status Ok") }

        verify { WeipassLogger.log("Contactless Reader Status Ok") }

        assertEquals(CORE_PRINTER_NOT_AVAILABLE, response.printerStatus)
        assertEquals(CORE_MAG_READER_OK, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_OK, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OK, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_NOT_AVAILABLE,
                CORE_MAG_READER_OK,
                CORE_ICC_READER_OK,
                CORE_PICC_READER_OK
            )
        assertEquals(hardwareStatusResponse, response)
    }

    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndPrinterUnknownCode_ReturnsHardwareStatusResponseWithPrinterUnknownAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[3] = 0x33.toByte()
            outDataArg.captured[0] = 0x00
            outDataArg.captured[2] = 0x00
            outDataArg.captured[1] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }

        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertNotEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OK, outDataArg.captured[3])
        assertNotEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_NOT_EXIST.toByte(), outDataArg.captured[3])
        assertNotEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OUT_OF_PAPER.toByte(), outDataArg.captured[3])
        assertNotEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_NOT_EXIST.toByte(), outDataArg.captured[3])

        assertEquals(SDK_MAG_READER_OK.toByte(), outDataArg.captured[0])

        assertEquals(SDK_CONTACT_READER_OK.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_OK.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Printer Status Unknown") }

        verify { WeipassLogger.log("Mag Reader Status Ok") }

        verify { WeipassLogger.log("Contact Reader Status Ok") }

        verify { WeipassLogger.log("Contactless Reader Status Ok") }

        assertEquals(CORE_PRINTER_NOT_AVAILABLE, response.printerStatus)
        assertEquals(CORE_MAG_READER_OK, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_OK, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OK, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_NOT_AVAILABLE,
                CORE_MAG_READER_OK,
                CORE_ICC_READER_OK,
                CORE_PICC_READER_OK
            )
        assertEquals(hardwareStatusResponse, response)
    }

    //Mag Reader
    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndMagStatusOk_ReturnsHardwareStatusResponseWithMagStatusOkAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[0] = 0x00
            outDataArg.captured[1] = 0x00
            outDataArg.captured[2] = 0x00
            outDataArg.captured[3] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }
        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OK.toByte(), outDataArg.captured[3])

        assertEquals(SDK_MAG_READER_OK.toByte(), outDataArg.captured[0])

        assertEquals(SDK_CONTACT_READER_OK.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_OK.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Mag Reader Status Ok") }

        verify { WeipassLogger.log("Contact Reader Status Ok") }

        verify { WeipassLogger.log("Contactless Reader Status Ok") }

        verify { WeipassLogger.log("Printer Status Ok") }

        assertEquals(CORE_PRINTER_PLAIN_RECEIPT_PAPER, response.printerStatus)
        assertEquals(CORE_MAG_READER_OK, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_OK, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OK, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_PLAIN_RECEIPT_PAPER,
                CORE_MAG_READER_OK,
                CORE_ICC_READER_OK,
                CORE_PICC_READER_OK
            )
        assertEquals(hardwareStatusResponse, response)
    }

    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndMagStatusUnknown_ReturnsHardwareStatusResponseWithMagStatusOutOfOrderAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[0] = 0x01
            outDataArg.captured[1] = 0x00
            outDataArg.captured[2] = 0x00
            outDataArg.captured[3] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }

        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OK.toByte(), outDataArg.captured[3])

        assertEquals(SDK_MAG_READER_UNKNOWN.toByte(), outDataArg.captured[0])

        assertEquals(SDK_CONTACT_READER_OK.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_OK.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Mag Reader Status Unknown") }

        verify { WeipassLogger.log("Contact Reader Status Ok") }

        verify { WeipassLogger.log("Contactless Reader Status Ok") }

        verify { WeipassLogger.log("Printer Status Ok") }

        assertEquals(CORE_PRINTER_PLAIN_RECEIPT_PAPER, response.printerStatus)
        assertEquals(CORE_MAG_READER_OUT_OF_ORDER, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_OK, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OK, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_PLAIN_RECEIPT_PAPER,
                CORE_MAG_READER_OUT_OF_ORDER,
                CORE_ICC_READER_OK,
                CORE_PICC_READER_OK
            )
        assertEquals(hardwareStatusResponse, response)
    }

    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndMagStatusNotExist_ReturnsHardwareStatusResponseWithMagStatusNotSupportedAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[0] = 0xFF.toByte()
            outDataArg.captured[1] = 0x00
            outDataArg.captured[2] = 0x00
            outDataArg.captured[3] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }

        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OK.toByte(), outDataArg.captured[3])

        assertEquals(SDK_MAG_READER_NOT_EXIST.toByte(), outDataArg.captured[0])

        assertEquals(SDK_CONTACT_READER_OK.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_OK.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Mag Reader Status Not Exist") }

        verify { WeipassLogger.log("Contact Reader Status Ok") }

        verify { WeipassLogger.log("Contactless Reader Status Ok") }

        verify { WeipassLogger.log("Printer Status Ok") }

        assertEquals(CORE_PRINTER_PLAIN_RECEIPT_PAPER, response.printerStatus)
        assertEquals(CORE_MAG_READER_NOT_SUPPORTED, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_OK, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OK, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_PLAIN_RECEIPT_PAPER,
                CORE_MAG_READER_NOT_SUPPORTED,
                CORE_ICC_READER_OK,
                CORE_PICC_READER_OK
            )
        assertEquals(hardwareStatusResponse, response)
    }

    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndMagStatusAny_ReturnsHardwareStatusResponseWithMagStatusOutOfOrderAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[0] = 0x33.toByte()
            outDataArg.captured[1] = 0x00
            outDataArg.captured[2] = 0x00
            outDataArg.captured[3] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }

        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OK.toByte(), outDataArg.captured[3])

        assertNotEquals(SDK_MAG_READER_OK.toByte(), outDataArg.captured[0])

        assertNotEquals(SDK_MAG_READER_UNKNOWN.toByte(), outDataArg.captured[0])

        assertNotEquals(SDK_MAG_READER_NOT_EXIST.toByte(), outDataArg.captured[0])

        assertEquals(SDK_CONTACT_READER_OK.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_OK.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Mag Reader Status Out Of Order") }

        verify { WeipassLogger.log("Contact Reader Status Ok") }

        verify { WeipassLogger.log("Contactless Reader Status Ok") }

        verify { WeipassLogger.log("Printer Status Ok") }

        assertEquals(CORE_PRINTER_PLAIN_RECEIPT_PAPER, response.printerStatus)
        assertEquals(CORE_MAG_READER_OUT_OF_ORDER, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_OK, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OK, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_PLAIN_RECEIPT_PAPER,
                CORE_MAG_READER_OUT_OF_ORDER,
                CORE_ICC_READER_OK,
                CORE_PICC_READER_OK
            )
        assertEquals(hardwareStatusResponse, response)
    }

    //Contact Reader
    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndContactStatusOk_ReturnsHardwareStatusResponseWithContactStatusOkAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[0] = 0x00
            outDataArg.captured[1] = 0x00
            outDataArg.captured[2] = 0x00
            outDataArg.captured[3] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }
        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OK.toByte(), outDataArg.captured[3])

        assertEquals(SDK_MAG_READER_OK.toByte(), outDataArg.captured[0])

        assertEquals(SDK_CONTACT_READER_OK.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_OK.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Contact Reader Status Ok") }

        verify { WeipassLogger.log("Mag Reader Status Ok") }

        verify { WeipassLogger.log("Contactless Reader Status Ok") }

        verify { WeipassLogger.log("Printer Status Ok") }

        assertEquals(CORE_PRINTER_PLAIN_RECEIPT_PAPER, response.printerStatus)
        assertEquals(CORE_MAG_READER_OK, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_OK, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OK, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_PLAIN_RECEIPT_PAPER,
                CORE_MAG_READER_OK,
                CORE_ICC_READER_OK,
                CORE_PICC_READER_OK
            )
        assertEquals(hardwareStatusResponse, response)
    }

    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndContactStatusUnknown_ReturnsHardwareStatusResponseWithContactStatusOutOfOrderAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[0] = 0x00
            outDataArg.captured[1] = 0x00
            outDataArg.captured[2] = 0x01
            outDataArg.captured[3] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }
        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OK.toByte(), outDataArg.captured[3])

        assertEquals(SDK_MAG_READER_OK.toByte(), outDataArg.captured[0])

        assertEquals(SDK_CONTACT_READER_UNKNOWN.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_OK.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Contact Reader Status Unknown") }

        verify { WeipassLogger.log("Mag Reader Status Ok") }

        verify { WeipassLogger.log("Contactless Reader Status Ok") }

        verify { WeipassLogger.log("Printer Status Ok") }

        assertEquals(CORE_PRINTER_PLAIN_RECEIPT_PAPER, response.printerStatus)
        assertEquals(CORE_MAG_READER_OK, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_OUT_OF_ORDER, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OK, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_PLAIN_RECEIPT_PAPER,
                CORE_MAG_READER_OK,
                CORE_ICC_READER_OUT_OF_ORDER,
                CORE_PICC_READER_OK
            )
        assertEquals(hardwareStatusResponse, response)
    }

    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndContactStatusNotExist_ReturnsHardwareStatusResponseWithContactStatusNotSupportedAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[0] = 0xFF.toByte()
            outDataArg.captured[1] = 0x00
            outDataArg.captured[2] = 0xFF.toByte()
            outDataArg.captured[3] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }

        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OK.toByte(), outDataArg.captured[3])

        assertEquals(SDK_MAG_READER_NOT_EXIST.toByte(), outDataArg.captured[0])

        assertEquals(SDK_CONTACT_READER_NOT_EXIST.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_OK.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Mag Reader Status Not Exist") }

        verify { WeipassLogger.log("Contact Reader Status Not Exist") }

        verify { WeipassLogger.log("Contactless Reader Status Ok") }

        verify { WeipassLogger.log("Printer Status Ok") }

        assertEquals(CORE_PRINTER_PLAIN_RECEIPT_PAPER, response.printerStatus)
        assertEquals(CORE_MAG_READER_NOT_SUPPORTED, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_NOT_SUPPORTED, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OK, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_PLAIN_RECEIPT_PAPER,
                CORE_MAG_READER_NOT_SUPPORTED,
                CORE_ICC_READER_NOT_SUPPORTED,
                CORE_PICC_READER_OK
            )
        assertEquals(hardwareStatusResponse, response)
    }

    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndContactStatusAny_ReturnsHardwareStatusResponseWithContactStatusOutOfOrderAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[0] = 0x33.toByte()
            outDataArg.captured[1] = 0x00
            outDataArg.captured[2] = 0x33.toByte()
            outDataArg.captured[3] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }

        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OK.toByte(), outDataArg.captured[3])

        assertNotEquals(SDK_MAG_READER_OK.toByte(), outDataArg.captured[0])
        assertNotEquals(SDK_MAG_READER_UNKNOWN.toByte(), outDataArg.captured[0])
        assertNotEquals(SDK_MAG_READER_NOT_EXIST.toByte(), outDataArg.captured[0])

        assertNotEquals(SDK_CONTACT_READER_OK.toByte(), outDataArg.captured[2])
        assertNotEquals(SDK_CONTACT_READER_UNKNOWN.toByte(), outDataArg.captured[2])
        assertNotEquals(SDK_CONTACT_READER_NOT_EXIST.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_OK.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Mag Reader Status Out Of Order") }
        verify { WeipassLogger.log("Contact Reader Status Out Of Order") }
        verify { WeipassLogger.log("Contactless Reader Status Ok") }
        verify { WeipassLogger.log("Printer Status Ok") }

        assertEquals(CORE_PRINTER_PLAIN_RECEIPT_PAPER, response.printerStatus)
        assertEquals(CORE_MAG_READER_OUT_OF_ORDER, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_OUT_OF_ORDER, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OK, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_PLAIN_RECEIPT_PAPER,
                CORE_MAG_READER_OUT_OF_ORDER,
                CORE_ICC_READER_OUT_OF_ORDER,
                CORE_PICC_READER_OK
            )
        assertEquals(hardwareStatusResponse, response)
    }

    //Contactless Reader
    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndContactlessStatusOk_ReturnsHardwareStatusResponseWithContactlessStatusOkAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[0] = 0x00
            outDataArg.captured[1] = 0x00
            outDataArg.captured[2] = 0x00
            outDataArg.captured[3] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }

        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OK.toByte(), outDataArg.captured[3])

        assertEquals(SDK_MAG_READER_OK.toByte(), outDataArg.captured[0])

        assertEquals(SDK_CONTACT_READER_OK.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_OK.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Mag Reader Status Ok") }
        verify { WeipassLogger.log("Contact Reader Status Ok") }
        verify { WeipassLogger.log("Contactless Reader Status Ok") }
        verify { WeipassLogger.log("Printer Status Ok") }

        assertEquals(CORE_PRINTER_PLAIN_RECEIPT_PAPER, response.printerStatus)
        assertEquals(CORE_MAG_READER_OK, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_OK, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OK, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_PLAIN_RECEIPT_PAPER,
                CORE_MAG_READER_OK,
                CORE_ICC_READER_OK,
                CORE_PICC_READER_OK
            )
        assertEquals(hardwareStatusResponse, response)
    }

    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndContactlessStatusUnknown_ReturnsHardwareStatusResponseWithContactlessStatusOutOfOrderAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[0] = 0x00
            outDataArg.captured[1] = 0x01
            outDataArg.captured[2] = 0x01
            outDataArg.captured[3] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }
        
        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OK.toByte(), outDataArg.captured[3])

        assertEquals(SDK_MAG_READER_OK.toByte(), outDataArg.captured[0])

        assertEquals(SDK_CONTACT_READER_UNKNOWN.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_UNKNOWN.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Contact Reader Status Unknown") }
        verify { WeipassLogger.log("Contactless Reader Status Unknown") }
        verify { WeipassLogger.log("Mag Reader Status Ok") }
        verify { WeipassLogger.log("Printer Status Ok") }

        assertEquals(CORE_PRINTER_PLAIN_RECEIPT_PAPER, response.printerStatus)
        assertEquals(CORE_MAG_READER_OK, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_OUT_OF_ORDER, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OUT_OF_ORDER, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_PLAIN_RECEIPT_PAPER,
                CORE_MAG_READER_OK,
                CORE_ICC_READER_OUT_OF_ORDER,
                CORE_PICC_READER_OUT_OF_ORDER
            )
        assertEquals(hardwareStatusResponse, response)
    }

    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndContactlessStatusNotExist_ReturnsHardwareStatusResponseWithContactlessStatusNotSupportedAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[0] = 0xFF.toByte()
            outDataArg.captured[1] = 0xFF.toByte()
            outDataArg.captured[2] = 0xFF.toByte()
            outDataArg.captured[3] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }

        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OK.toByte(), outDataArg.captured[3])

        assertEquals(SDK_MAG_READER_NOT_EXIST.toByte(), outDataArg.captured[0])

        assertEquals(SDK_CONTACT_READER_NOT_EXIST.toByte(), outDataArg.captured[2])

        assertEquals(SDK_CONTACTLESS_READER_NOT_EXIST.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Mag Reader Status Not Exist") }
        verify { WeipassLogger.log("Contact Reader Status Not Exist") }
        verify { WeipassLogger.log("Contactless Reader Status Not Exist") }
        verify { WeipassLogger.log("Printer Status Ok") }

        assertEquals(CORE_PRINTER_PLAIN_RECEIPT_PAPER, response.printerStatus)
        assertEquals(CORE_MAG_READER_NOT_SUPPORTED, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_NOT_SUPPORTED, response.iccReaderStatus)
        assertEquals(CORE_ICC_READER_NOT_SUPPORTED, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_PLAIN_RECEIPT_PAPER,
                CORE_MAG_READER_NOT_SUPPORTED,
                CORE_ICC_READER_NOT_SUPPORTED,
                CORE_ICC_READER_NOT_SUPPORTED
            )
        assertEquals(hardwareStatusResponse, response)
    }

    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultIsZeroAndContactlessStatusAny_ReturnsHardwareStatusResponseWithContactlessStatusOutOfOrderAndOtherStatusAny() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[0] = 0x33.toByte()
            outDataArg.captured[1] = 0x33.toByte()
            outDataArg.captured[2] = 0x33.toByte()
            outDataArg.captured[3] = 0x00
            0
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }

        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        assertEquals(SDK_API_getDeviceStatus_PRINTER_STATUS_OK.toByte(), outDataArg.captured[3])

        assertNotEquals(SDK_MAG_READER_OK.toByte(), outDataArg.captured[0])
        assertNotEquals(SDK_MAG_READER_UNKNOWN.toByte(), outDataArg.captured[0])
        assertNotEquals(SDK_MAG_READER_NOT_EXIST.toByte(), outDataArg.captured[0])

        assertNotEquals(SDK_CONTACT_READER_OK.toByte(), outDataArg.captured[2])
        assertNotEquals(SDK_CONTACT_READER_UNKNOWN.toByte(), outDataArg.captured[2])
        assertNotEquals(SDK_CONTACT_READER_NOT_EXIST.toByte(), outDataArg.captured[2])

        assertNotEquals(SDK_CONTACTLESS_READER_OK.toByte(), outDataArg.captured[1])
        assertNotEquals(SDK_CONTACTLESS_READER_UNKNOWN.toByte(), outDataArg.captured[1])
        assertNotEquals(SDK_CONTACT_READER_NOT_EXIST.toByte(), outDataArg.captured[1])

        verify { WeipassLogger.log("Mag Reader Status Out Of Order") }
        verify { WeipassLogger.log("Contact Reader Status Out Of Order") }
        verify { WeipassLogger.log("Contactless Reader Status Out of Order") }
        verify { WeipassLogger.log("Printer Status Ok") }

        assertEquals(CORE_PRINTER_PLAIN_RECEIPT_PAPER, response.printerStatus)
        assertEquals(CORE_MAG_READER_OUT_OF_ORDER, response.magReaderStatus)
        assertEquals(CORE_ICC_READER_OUT_OF_ORDER, response.iccReaderStatus)
        assertEquals(CORE_PICC_READER_OUT_OF_ORDER, response.piccReaderStatus)

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_PLAIN_RECEIPT_PAPER,
                CORE_MAG_READER_OUT_OF_ORDER,
                CORE_ICC_READER_OUT_OF_ORDER,
                CORE_PICC_READER_OUT_OF_ORDER
            )
        assertEquals(hardwareStatusResponse, response)
    }

    @Test
    fun retrieveHardwareStatus_OnAllReadersAndPrinterStatusRequestAndApiGetDeviceStatusResultThrowsExceptionAndContactlessStatusAny_ReturnsHardwareStatusResponseWithAllOutOfOrder() {
        //Setup
        val request =
            IOStatusRequest(
                false
            )

        every { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) } just Runs

        val outDataArg = slot<ByteArray>()
        val outDatLenArg = slot<IntArray>()
        every {
            SDKInstance.mCore.getDeviceStatus(capture(outDataArg), capture(outDatLenArg))
        } answers {
            outDataArg.captured[0] = 0x33.toByte()
            outDataArg.captured[1] = 0x33.toByte()
            outDataArg.captured[2] = 0x33.toByte()
            outDataArg.captured[3] = 0x33.toByte()
            throw Exception("Test Exception")
        }

        //Run
        val response = SUT.retrieveHardwareStatus(request)

        //Verify
        verify { WeipassDeviceUtils.callForSdkBreakOffCommand(context.applicationContext) }

        assertFalse(request.isPrinterOnly)

        verify { SDKInstance.mCore.getDeviceStatus(any(), any())}

        verify { WeipassLogger.log("Exception ::: Test Exception") }

        val hardwareStatusResponse =
            IOStatusResponse(
                CORE_PRINTER_NOT_AVAILABLE,
                CORE_MAG_READER_OUT_OF_ORDER,
                CORE_ICC_READER_OUT_OF_ORDER,
                CORE_PICC_READER_OUT_OF_ORDER
            )

        assertEquals(hardwareStatusResponse, response)

    }

    private fun mockSDKInstance() {
        mockkStatic(SDKInstance::class)
        SDKInstance.mPrinter = mockedPrinter
        SDKInstance.mBankCard = mockedBankCard
        SDKInstance.mCore = mockedCore
    }

    private fun mockWeipassLogger() {
        mockkObject(WeipassLogger)
        mockkObject(WeipassLogger.Companion)
        every { WeipassLogger.log(any()) } just Runs
    }

    private fun mockLibInit() {
        mockkStatic(LibInit::class)
        every { LibInit.init(any()) } returns 0
    }

    private fun mockkDeviceUtils() {
        mockkObject(WeipassDeviceUtils)
        mockkObject(WeipassDeviceUtils.Companion)
    }
}