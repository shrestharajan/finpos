package global.citytech.finpos.merchant.domain.model.ministatement

data class MiniStatementResponseEntity(
    val stan: String = "",
    val message: String = "",
    val miniStatementData: String = "",
    val isApproved: Boolean

)