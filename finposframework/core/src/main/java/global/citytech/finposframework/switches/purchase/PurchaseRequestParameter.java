package global.citytech.finposframework.switches.purchase;

import global.citytech.finposframework.switches.RequestParameter;

/** @author rajudhital on 3/29/20 */
public interface PurchaseRequestParameter extends RequestParameter {}
