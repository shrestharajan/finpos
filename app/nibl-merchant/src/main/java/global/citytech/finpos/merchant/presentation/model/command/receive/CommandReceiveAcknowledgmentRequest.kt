package global.citytech.finpos.merchant.presentation.model.command.receive

/**
 * Created by Unique Shakya on 9/21/2021.
 */
data class CommandReceiveAcknowledgmentRequest (
    var serialNumber: String,
    var commands: List<String>
)