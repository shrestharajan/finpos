package global.citytech.finpos.merchant.framework.datasource.core.printparameter

import global.citytech.finpos.merchant.domain.model.app.AidParam
import global.citytech.finpos.merchant.domain.model.app.CardScheme
import global.citytech.finpos.merchant.domain.model.app.EmvKey
import global.citytech.finpos.merchant.domain.model.app.Host
import global.citytech.finposframework.usecases.terminal.TerminalInfo

/**
 * Created by Rishav Chudal on 11/6/20.
 */
class TmsLogs {
    var title: String? = null
    var terminalInfo: TerminalInfo? = null
    var hosts: List<Host>? = null
    var aidParams: List<AidParam>? = null
    var emvKeys: List<EmvKey>? = null
    var cardSchemes: List<CardScheme>? = null
}