package global.citytech.finpos.processor.nibl;

import org.junit.Test;
import org.mockito.Mockito;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.processor.nibl.transaction.authorisation.preauth.PreAuthRequestModel;
import global.citytech.finposframework.repositories.TerminalRepository;

import static org.mockito.Mockito.when;

public class PreAuthUseCaseTest {

    private Logger logger = Logger.getLogger(PreAuthUseCaseTest.class.getName());

    @Test
    public void executeTest(){
        TerminalRepository repository = Mockito.mock(TerminalRepository.class);
        when(repository.findPrimaryHost()).thenReturn(NIBLHostConfig.getPrimaryConfig());
        when(repository.findTerminalInfo()).thenReturn(NIBLHostConfig.getTerminalInfo());
        when(repository.getSystemTraceAuditNumber()).thenReturn("001010");
        PreAuthUseCase useCase = new PreAuthUseCase(repository, null); // TODO mock notifier
        useCase.execute(this.getModel());

    }
    private PreAuthRequestModel getModel(){
//        PreAuthRequestModel model = new PreAuthRequestModel();
//        model.setPan("4249720010000129");
//        model.setTransactionAmount(10);
//        model.setEntryMode(PosEntryMode.ICC);
//        model.setPosConditionCode("00");
//        model.setTrack2Data("4249720010000129d24116261861919299999");
//        model.setPinBlock("c247220c967c1d");
//        model.setEmvData("9F26084266DA1FF65DFD3F9F2701809F100706011203A000009F3704FA0077CA9F36020002950502800088009A032008119C01009F02060000001200005F2A020524820258009F1A0205249F03060000000000009F3303E0F0C89F34031E03009F3501229F1E0845303139353438398407A00000000310109F090201409F4104000000045F340101");
        return PreAuthRequestModel.Builder.newInstance().build(); // TODO create mock model class
    }
}
