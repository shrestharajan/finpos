package global.citytech.finpos.processor.nibl.pinchange;

import global.citytech.finpos.processor.nibl.transaction.TransactionRequestModel;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.io.sound.SoundService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;

public class PinChangeRequest extends TransactionRequestModel {
    public static  class Builder {
        private TransactionRepository transactionRepository;
        private ReadCardService readCardService;
        private DeviceController deviceController;
        private TransactionAuthenticator transactionAuthenticator;
        private PrinterService printerService;
        private ApplicationRepository applicationRepository;
        private global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest;
        private LedService ledService;
        private SoundService soundService;


        public static PinChangeRequest.Builder newInstance() {
            return new PinChangeRequest.Builder();
        }

        public PinChangeRequest.Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public PinChangeRequest.Builder withReadCardService(ReadCardService readCardService) {
            this.readCardService = readCardService;
            return this;
        }

        public PinChangeRequest.Builder withDeviceController(DeviceController deviceController) {
            this.deviceController = deviceController;
            return this;
        }

        public PinChangeRequest.Builder withTransactionAuthenticator(TransactionAuthenticator transactionAuthenticator) {
            this.transactionAuthenticator = transactionAuthenticator;
            return this;
        }

        public PinChangeRequest.Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public PinChangeRequest.Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public PinChangeRequest.Builder withTransactionRequest(global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest) {
            this.transactionRequest = transactionRequest;
            return this;
        }

        public PinChangeRequest.Builder withLedService(LedService ledService) {
            this.ledService = ledService;
            return this;
        }

        public PinChangeRequest.Builder withSoundService(SoundService soundService) {
            this.soundService = soundService;
            return this;
        }

        public PinChangeRequest build() {
            PinChangeRequest PinChangeRequest = new PinChangeRequest();
            PinChangeRequest.transactionAuthenticator = this.transactionAuthenticator;
            PinChangeRequest.transactionRepository = this.transactionRepository;
            PinChangeRequest.readCardService = this.readCardService;
            PinChangeRequest.deviceController = this.deviceController;
            PinChangeRequest.transactionRequest = this.transactionRequest;
            PinChangeRequest.printerService = this.printerService;
            PinChangeRequest.applicationRepository = this.applicationRepository;
            PinChangeRequest.ledService = this.ledService;
            PinChangeRequest.soundService = this.soundService;
            return PinChangeRequest;
        }
    }
}
