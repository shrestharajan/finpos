package global.citytech.finpos.merchant.data.repository.core.configuration

import global.citytech.finpos.merchant.data.datasource.core.configuration.ConfigurationDataSource
import global.citytech.finpos.merchant.domain.repository.core.configuration.ConfigurationRepository
import global.citytech.finpos.merchant.domain.model.logon.LogonResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import io.reactivex.Observable

class ConfigurationRepositoryImpl(private val isoDataSource: ConfigurationDataSource) :
    ConfigurationRepository {
    override fun logOn(configurationItem: ConfigurationItem): Observable<LogonResponseEntity> =
        isoDataSource.logOn(configurationItem)
}