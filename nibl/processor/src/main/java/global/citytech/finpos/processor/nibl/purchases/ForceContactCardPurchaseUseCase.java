//package global.citytech.finpos.processor.nibl.purchases;
//
//import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
//import global.citytech.finpos.nibl.iso8583.ProcessingCode;
//import global.citytech.finpos.nibl.iso8583.requestsender.purchase.IccPurchaseRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.purchase.MagPurchaseRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.purchase.PICCPurchaseRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.purchase.PurchaseRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
//import global.citytech.finpos.processor.nibl.reversal.ReversalRequestModel;
//import global.citytech.finpos.processor.nibl.reversal.ReversalUseCase;
//import global.citytech.finpos.processor.nibl.transaction.NiblTransactionReceiptPrintHandler;
//import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
//import global.citytech.finpos.processor.nibl.transaction.ReversedTransactionReceiptHandler;
//import global.citytech.finpos.processor.nibl.transaction.TransactionUseCase;
//import global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor;
//import global.citytech.finpos.processor.nibl.transaction.card.MagneticCardProcessor;
//import global.citytech.finpos.processor.nibl.transaction.card.PICCCardProcessor;
//import global.citytech.finposframework.exceptions.FinPosException;
//import global.citytech.finposframework.exceptions.PosError;
//import global.citytech.finposframework.exceptions.PosException;
//import global.citytech.finposframework.hardware.io.cards.CardDetails;
//import global.citytech.finposframework.hardware.io.cards.CardType;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
//import global.citytech.finposframework.hardware.io.led.LedAction;
//import global.citytech.finposframework.hardware.io.led.LedLight;
//import global.citytech.finposframework.hardware.io.led.LedRequest;
//import global.citytech.finposframework.hardware.io.led.LedService;
//import global.citytech.finposframework.iso8583.DataElement;
//import global.citytech.finposframework.iso8583.IsoMessageResponse;
//import global.citytech.finposframework.iso8583.RequestContext;
//import global.citytech.finposframework.notifier.Notifier;
//import global.citytech.finposframework.repositories.ApplicationRepository;
//import global.citytech.finposframework.repositories.TerminalRepository;
//import global.citytech.finposframework.repositories.TransactionRepository;
//import global.citytech.finposframework.switches.PosEntryMode;
//import global.citytech.finposframework.usecases.MagneticSwipe;
//import global.citytech.finposframework.usecases.PICCWave;
//import global.citytech.finposframework.usecases.TransactionType;
//import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
//import global.citytech.finposframework.usecases.transaction.TransactionLog;
//import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
//import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
//import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
//import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
//import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
//import global.citytech.finposframework.utility.HelperUtils;
//import global.citytech.finposframework.utility.IsoMessageUtils;
//import global.citytech.finposframework.utility.JsonUtils;
//import global.citytech.finposframework.utility.StringUtils;
//
///**
// * Created by Unique Shakya on 1/18/2021.
// */
//public class ForceContactCardPurchaseUseCase extends TransactionUseCase {
//
//    private global.citytech.finpos.nibl.iso8583.requestsender.purchase.PurchaseRequest purchaseRequest;
//
//    public ForceContactCardPurchaseUseCase(TerminalRepository terminalRepository, Notifier notifier) {
//        super(terminalRepository, notifier);
//    }
//
//    @Override
//    protected String getClassName() {
//        return ForceContactCardPurchaseUseCase.class.getSimpleName();
//    }
//
//    public PurchaseResponseModel execute(PurchaseRequestModel purchaseRequestModel) {
//        try {
//            logger.log("::: REACHED FORCE CONTACT CARD PURCHASE USE CASE :::");
//            String stan = this.terminalRepository.getSystemTraceAuditNumber();
//            String invoiceNumber = this.terminalRepository.getInvoiceNumber();
//            String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
//            TransactionRequest transactionRequest = purchaseRequestModel.getTransactionRequest();
//            this.transactionRepository = purchaseRequestModel.getTransactionRepository();
//            if (!this.isDeviceReady(purchaseRequestModel.getDeviceController(), transactionRequest.getTransactionType()))
//                throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
//            this.initTransaction(purchaseRequestModel.getApplicationRepository(), purchaseRequestModel.getTransactionRequest());
//            ReadCardRequest readCardRequest = this.prepareForceContactCardReadCardRequest(stan, purchaseRequestModel.getApplicationRepository(),
//                    transactionRequest);
//            this.notifier.notify(Notifier.EventType.FORCE_ICC_CARD, "EXCEEDS COUNT\nINSERT CARD");
//            ReadCardResponse readCardResponse = this.detectCard(readCardRequest, purchaseRequestModel.getReadCardService(),
//                    purchaseRequestModel.getLedService());
//            this.notifier.notify(Notifier.EventType.READING_CARD, "Reading Card..");
//            this.cardProcessor = new IccCardProcessor(purchaseRequestModel.getReadCardService(),
//                    purchaseRequestModel.getTransactionRepository(), purchaseRequestModel.getApplicationRepository(),
//                    purchaseRequestModel.getTransactionAuthenticator(), notifier, this.terminalRepository);
//            readCardResponse = this.cardProcessor.validateReadCardResponse(readCardRequest, readCardResponse);
//            this.logger.debug("::: CARD TYPE AFTER VALIDATE READ CARD RESPONSE === " + readCardResponse.getCardDetails().getCardType());
//            this.logger.debug("::: FALLBACK TO ICC VALIDATE READ CARD RESPONSE === " + readCardResponse.getFallbackIccToMag());
//            this.logger.debug("::: CARD SCHEME AFTER VALIDATE READ CARD RESPONSE ::: " + readCardResponse.getCardDetails().getCardScheme());
//            this.checkForCardTypeChange(readCardResponse.getCardDetails().getCardType(), purchaseRequestModel);
//            if (!this.isValidTransaction(
//                    purchaseRequestModel.getApplicationRepository(),
//                    purchaseRequestModel.getTransactionRequest(),
//                    readCardResponse)
//            )
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
//            if (!this.isTransactionAuthorizationSuccess(purchaseRequestModel.getTransactionAuthenticator(),
//                    purchaseRequestModel.getApplicationRepository(), transactionRequest.getTransactionType(),
//                    readCardResponse))
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
//            global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest =
//                    preparePurchaseCardRequest(transactionRequest, readCardResponse.getCardDetails(),
//                            readCardResponse.getFallbackIccToMag());
//            readCardResponse = this.cardProcessor.processCard(stan, purchaseRequest);
//            this.logger.debug("::: FALLBACK TO ICC VALIDATE READ CARD RESPONSE === " + readCardResponse.getFallbackIccToMag());
//            logger.debug("::: READ CARD RESPONSE ::: " + readCardResponse.toString());
//            this.notifier.notify(Notifier.EventType.TRANSMITTING_REQUEST, "Transmitting Request..");
//            try {
//                IsoMessageResponse purchaseIsoMessageResponse = this.sendIsoMessage(stan, invoiceNumber, readCardResponse, purchaseRequestModel);
//                this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait..");
//                InvalidResponseHandlerResponse invalidResponseHandlerResponse = this.checkForInvalidResponseFromHost(purchaseIsoMessageResponse);
//                if (invalidResponseHandlerResponse.isInvalid())
//                    return this.handleInvalidResponseFromHost(transactionRequest,
//                            invalidResponseHandlerResponse, batchNumber, stan, invoiceNumber,
//                            readCardResponse, purchaseRequestModel);
//                boolean isPurchaseApproved = this.isTransactionApprovedByActionCode(purchaseIsoMessageResponse);
//                this.notifier.notify(Notifier.EventType.PROCESSING_ISSUER_SCRIPT, "Processing..");
//                readCardResponse = this.cardProcessor.processOnlineResult(readCardResponse, purchaseIsoMessageResponse);
//                if (this.isTransactionDeclineByCardEvenIfSuccessOnSwitch(purchaseIsoMessageResponse, readCardResponse))
//                    return this.handleApprovedTransactionDeclinedByCard(stan, batchNumber, invoiceNumber,
//                            readCardResponse, purchaseRequestModel, transactionRequest);
//                this.notifyTransactionCompletion(stan, batchNumber, invoiceNumber, isPurchaseApproved,
//                        transactionRequest, purchaseRequestModel.getTransactionRepository(), purchaseIsoMessageResponse,
//                        readCardResponse, purchaseRequestModel.getApplicationRepository(), purchaseRequestModel.getLedService());
//                boolean shouldPrint = this.printReceipt(batchNumber, stan, invoiceNumber,
//                        purchaseRequestModel.getPrinterService(),
//                        purchaseRequestModel.getTransactionRequest(),
//                        purchaseRequestModel.getTransactionRepository(),
//                        readCardResponse,
//                        purchaseIsoMessageResponse,
//                        purchaseRequestModel.getApplicationRepository()
//                );
//                return this.prepareResponse(purchaseIsoMessageResponse, isPurchaseApproved,
//                        purchaseRequestModel.getApplicationRepository(), shouldPrint);
//            } catch (FinPosException e) {
//                if (this.isResponseNotReceivedException(e)) {
//                    return this.handleTimeoutTransaction(transactionRequest, batchNumber, stan,
//                            invoiceNumber, readCardResponse, purchaseRequestModel);
//                } else if (e.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT) {
//                    logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED UNABLE TO CONNECT");
//                    transactionRepository.removeAutoReversal(this.autoReversal);
//                    throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
//                } else {
//                    throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//                }
//            }
//        } catch (PosException ex) {
//            ex.printStackTrace();
//            throw ex;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        } finally {
//            purchaseRequestModel.getReadCardService().cleanUp();
//            purchaseRequestModel.getLedService().doTurnLedWith(new LedRequest(LedLight.ALL, LedAction.OFF));
//            PICCWave.getInstance().clear();
//            MagneticSwipe.getInstance().clear();
//        }
//    }
//
//    private PurchaseResponseModel handleTimeoutTransaction(TransactionRequest transactionRequest,
//                                                           String batchNumber, String stan,
//                                                           String invoiceNumber,
//                                                           ReadCardResponse readCardResponse,
//                                                           PurchaseRequestModel purchaseRequestModel) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, "TIMEOUT");
//        this.setUnableToGoOnlineResultToCard(readCardResponse, purchaseRequestModel.getReadCardService());
//        this.performReversal(stan, readCardResponse, purchaseRequestModel);
//        boolean shouldPrint = !this.approvePrintOnly(purchaseRequestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest,
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.TIMEOUT, "Timeout"));
//            new NiblTransactionReceiptPrintHandler(purchaseRequestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            purchaseRequestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PurchaseResponseModel.Builder.createDefaultBuilder()
//                .message("TIMEOUT")
//                .isApproved(false)
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private PurchaseResponseModel prepareResponse(IsoMessageResponse purchaseIsoMessageResponse,
//                                                  boolean isPurchaseApproved,
//                                                  ApplicationRepository applicationRepository,
//                                                  boolean shouldPrint) {
//        PurchaseResponseModel.Builder builder = PurchaseResponseModel.Builder.createDefaultBuilder();
//        builder.isApproved(isPurchaseApproved);
//        builder.debugRequestMessage(purchaseIsoMessageResponse.getDebugRequestString());
//        builder.debugResponseMessage(purchaseIsoMessageResponse.getDebugResponseString());
//        builder.message(this.retrieveMessage(purchaseIsoMessageResponse, isPurchaseApproved, applicationRepository));
//        builder.shouldPrintCustomerCopy(shouldPrint);
//        return builder.build();
//    }
//
//    private void notifyTransactionCompletion(String stan, String batchNumber, String invoiceNumber,
//                                             boolean isPurchaseApproved,
//                                             TransactionRequest transactionRequest,
//                                             TransactionRepository transactionRepository,
//                                             IsoMessageResponse purchaseIsoMessageResponse,
//                                             ReadCardResponse readCardResponse,
//                                             ApplicationRepository applicationRepository,
//                                             LedService ledService) {
//        this.updateTransactionLog(
//                stan,
//                batchNumber,
//                invoiceNumber,
//                isPurchaseApproved,
//                transactionRequest,
//                transactionRepository,
//                purchaseIsoMessageResponse,
//                readCardResponse,
//                applicationRepository
//        );
//        this.updateTransactionIds(isPurchaseApproved);
//        this.notifyTransactionStatus(
//                isPurchaseApproved,
//                purchaseIsoMessageResponse,
//                applicationRepository,
//                ledService,
//                readCardResponse.getCardDetails().getCardType() == CardType.PICC
//        );
//    }
//
//    private void updateTransactionLog(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean purchaseApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository
//    ) {
//        this.logger.log("::: NIBL ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
//        this.logger.log("::: NIBL ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
//        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
//                .withInvoiceNumber(invoiceNumber)
//                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
//                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
//                .withTransactionType(TransactionType.PURCHASE)
//                .withOriginalTransactionType(TransactionType.PURCHASE)
//                .withTransactionAmount(transactionRequest.getAmount())
//                .withOriginalTransactionAmount(transactionRequest.getAmount())
//                .withTransactionDate(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_DATE))
//                .withTransactionTime(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_TIME))
//                .withTransactionStatus(purchaseApproved ? "APPROVED" : "DECLINED")
//                .withPosEntryMode(PosEntryMode.ICC)
//                .withOriginalPosEntryMode(PosEntryMode.ICC)
//                .withPosConditionCode(this.purchaseRequest.getPosConditionCode())
//                .withOriginalPosConditionCode("")
//                .withReconcileStatus("")
//                .withReconcileTime("")
//                .withReconcileDate("")
//                .withReconcileBatchNo(batchNumber)
//                .withReadCardResponse(readCardResponse)
//                .withAuthorizationCompleted(true)
//                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
//                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse,
//                        applicationRepository, batchNumber, stan, invoiceNumber))
//                .withMerchantId(this.terminalRepository.findTerminalInfo().getMerchantID())
//                .withTerminalId(this.terminalRepository.findTerminalInfo().getTerminalID())
//                .withTransactionVoided(false)
//                .withTipAdjusted(false)
//                .withStan(stan)
//                .build();
//        transactionRepository.updateTransactionLog(transactionLog);
//
//    }
//
//    private PurchaseResponseModel handleApprovedTransactionDeclinedByCard(String stan, String batchNumber,
//                                                                          String invoiceNumber,
//                                                                          ReadCardResponse readCardResponse,
//                                                                          PurchaseRequestModel purchaseRequestModel,
//                                                                          TransactionRequest transactionRequest) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, ReasonForReversal.Type.DECLINED_BY_CARD.getDescription());
//        this.performReversal(stan, readCardResponse, purchaseRequestModel);
//        boolean shouldPrint = !this.approvePrintOnly(purchaseRequestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest, readCardResponse,
//                    this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.DECLINED_BY_CARD,
//                            ReasonForReversal.Type.DECLINED_BY_CARD.getDescription()));
//            new NiblTransactionReceiptPrintHandler(purchaseRequestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            purchaseRequestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PurchaseResponseModel.Builder.createDefaultBuilder()
//                .isApproved(false)
//                .message(ReasonForReversal.Type.DECLINED_BY_CARD.getDescription())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private PurchaseResponseModel handleInvalidResponseFromHost(TransactionRequest transactionRequest,
//                                                                InvalidResponseHandlerResponse invalidResponseHandlerResponse,
//                                                                String batchNumber, String stan,
//                                                                String invoiceNumber,
//                                                                ReadCardResponse readCardResponse,
//                                                                PurchaseRequestModel purchaseRequestModel) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, invalidResponseHandlerResponse.getMessage());
//        this.performReversal(stan, readCardResponse, purchaseRequestModel);
//        boolean shouldPrint = !this.approvePrintOnly(purchaseRequestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest,
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.INVALID_RESPONSE,
//                            invalidResponseHandlerResponse.getMessage()));
//            new NiblTransactionReceiptPrintHandler(purchaseRequestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            purchaseRequestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PurchaseResponseModel.Builder.createDefaultBuilder()
//                .isApproved(false)
//                .message(invalidResponseHandlerResponse.getMessage())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private void performReversal(String stan, ReadCardResponse readCardResponse, PurchaseRequestModel purchaseRequestModel) {
//        ReversalUseCase reversalUseCase = new ReversalUseCase(
//                this.prepareReversalRequest(stan, readCardResponse, purchaseRequestModel),
//                this.terminalRepository,
//                this.notifier
//        );
//        try {
//            reversalUseCase.execute(
//                    prepareReversalRequestModel(
//                            purchaseRequestModel,
//                            readCardResponse
//                    )
//            );
//            this.transactionRepository.removeAutoReversal(this.autoReversal);
//        } catch (FinPosException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private NiblReversalRequest prepareReversalRequest(
//            String stan,
//            ReadCardResponse readCardResponse,
//            PurchaseRequestModel purchaseRequestModel
//    ) {
//        NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
//        niblReversalRequest.setProcessingCode(ProcessingCode.PURCHASE.getCode());
//        niblReversalRequest.setAmount(
//                purchaseRequestModel.getTransactionRequest().getAmount()
//        );
//        niblReversalRequest.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        niblReversalRequest.setExpiryDate(readCardResponse.getCardDetails().getExpiryDate());
//        niblReversalRequest.setPosEntryMode(this.purchaseRequest.getPosEntryMode());
//        niblReversalRequest.setPosConditionCode(this.purchaseRequest.getPosConditionCode());
//        niblReversalRequest.setTrack2Data(this.purchaseRequest.getTrack2Data());
//        niblReversalRequest.setPinBlock(this.purchaseRequest.getPinBlock());
//        niblReversalRequest.setInvoiceNumber(this.purchaseRequest.getInvoiceNumber());
//        niblReversalRequest.setStan(stan);
//        return niblReversalRequest;
//    }
//
//    private ReversalRequestModel prepareReversalRequestModel(
//            PurchaseRequestModel purchaseRequestModel,
//            ReadCardResponse readCardResponse
//    ) {
//        return ReversalRequestModel.Builder.newInstance()
//                .withTransactionRequest(
//                        purchaseRequestModel.getTransactionRequest()
//                )
//                .withTransactionRepository(
//                        purchaseRequestModel.getTransactionRepository()
//                )
//                .withPrinterService(
//                        purchaseRequestModel.getPrinterService()
//                )
//                .withApplicationRepository(
//                        purchaseRequestModel.getApplicationRepository()
//                )
//                .withReadCardResponse(readCardResponse)
//                .build();
//    }
//
//    private IsoMessageResponse sendIsoMessage(String stan, String invoiceNumber, ReadCardResponse readCardResponse, PurchaseRequestModel purchaseRequestModel) {
//        RequestContext context = this.prepareTemplateRequestContext(stan);
//        logger.debug(String.format(" Request Context ::%s", context));
//        this.purchaseRequest = preparePurchaseRequest(invoiceNumber, readCardResponse);
//        context.setRequest(this.purchaseRequest);
//        this.autoReversal = new AutoReversal(TransactionType.PURCHASE,
//                JsonUtils.toJsonObj(prepareReversalRequest(stan, readCardResponse, purchaseRequestModel)),
//                0,
//                AutoReversal.Status.ACTIVE,
//                StringUtils.dateTimeStamp());
//        this.transactionRepository.saveAutoReversal(this.autoReversal);
//        PurchaseRequestSender sender;
//        switch (readCardResponse.getCardDetails().getCardType()) {
//            case MAG:
//                sender = new MagPurchaseRequestSender(new NIBLSpecInfo(), context);
//                break;
//            case ICC:
//            default:
//                sender = new IccPurchaseRequestSender(new NIBLSpecInfo(), context);
//                break;
//            case PICC:
//                sender = new PICCPurchaseRequestSender(new NIBLSpecInfo(), context);
//                break;
//        }
//        IsoMessageResponse isoMessageResponse = sender.send();
//        this.transactionRepository.removeAutoReversal(this.autoReversal);
//        return isoMessageResponse;
//    }
//
//    private global.citytech.finpos.nibl.iso8583.requestsender.purchase.PurchaseRequest preparePurchaseRequest(String invoiceNumber, ReadCardResponse readCardResponse) {
//        global.citytech.finpos.nibl.iso8583.requestsender.purchase.PurchaseRequest request = new global.citytech.finpos.nibl.iso8583.requestsender.purchase.PurchaseRequest();
//        request.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        request.setTransactionAmount(readCardResponse.getCardDetails().getAmount());
//        request.setPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()));
//        request.setPosConditionCode("00"); //TODO Change later
//        request.setTrack2Data(readCardResponse.getCardDetails().getTrackTwoData());
//        if (readCardResponse.getCardDetails().getPinBlock().getOfflinePin() ||
//                StringUtils.isEmpty(readCardResponse.getCardDetails().getPinBlock().getPin()) ||
//                readCardResponse.getCardDetails().getPinBlock().getPin().equalsIgnoreCase("offline"))
//            request.setPinBlock("");
//        else
//            request.setPinBlock(readCardResponse.getCardDetails().getPinBlock().getPin());
//        request.setEmvData(readCardResponse.getCardDetails().getIccDataBlock());
//        request.setExpireDate(readCardResponse.getCardDetails().getExpiryDate());
//        request.setCardSequenceNumber(readCardResponse.getCardDetails().getPrimaryAccountNumberSerialNumber());
//        request.setLocalTime(HelperUtils.getDefaultLocaleTimeHhMmSs());
//        request.setLocalDate(HelperUtils.getDefaultLocaleDateWithoutYear());
//        request.setInvoiceNumber(invoiceNumber);
//        this.logger.debug("::: FALLBACK TO ICC PURCHASE REQUEST === " + readCardResponse.getFallbackIccToMag());
//        request.setFallbackFromIccToMag(readCardResponse.getFallbackIccToMag());
//        return request;
//    }
//
//    private PurchaseRequest preparePurchaseCardRequest(TransactionRequest transactionRequest,
//                                                       CardDetails cardDetails,
//                                                       boolean fallbackIccToMag) {
//        global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest
//                = new global.citytech.finposframework.usecases.transaction.data.PurchaseRequest(
//                transactionRequest.getTransactionType(),
//                transactionRequest.getAmount(),
//                transactionRequest.getAdditionalAmount()
//        );
//        cardDetails.setAmount(transactionRequest.getAmount());
//        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
//        purchaseRequest.setCardDetails(cardDetails);
//        purchaseRequest.setFallbackIccToMag(fallbackIccToMag);
//        return purchaseRequest;
//    }
//
//    private void checkForCardTypeChange(CardType cardType, PurchaseRequestModel purchaseRequestModel) {
//        switch (cardType) {
//            case MAG:
//                this.cardProcessor = new MagneticCardProcessor(
//                        purchaseRequestModel.getTransactionRepository(),
//                        purchaseRequestModel.getTransactionAuthenticator(),
//                        purchaseRequestModel.getApplicationRepository(),
//                        purchaseRequestModel.getReadCardService(),
//                        this.notifier,
//                        this.terminalRepository
//                );
//                break;
//            case ICC:
//                this.cardProcessor = new global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor(purchaseRequestModel.getReadCardService(),
//                        purchaseRequestModel.getTransactionRepository(), purchaseRequestModel.getApplicationRepository(),
//                        purchaseRequestModel.getTransactionAuthenticator(), notifier,
//                        this.terminalRepository);
//                break;
//            case PICC:
//                this.cardProcessor = new PICCCardProcessor(purchaseRequestModel.getTransactionRepository(),
//                        purchaseRequestModel.getApplicationRepository(), purchaseRequestModel.getReadCardService(), this.notifier,
//                        purchaseRequestModel.getTransactionAuthenticator(), purchaseRequestModel.getLedService(), purchaseRequestModel.getSoundService(),
//                        this.terminalRepository);
//                break;
//            default:
//                break;
//        }
//    }
//}
