package global.citytech.finpos.device.qualcomm.system

import android.content.Context
import android.os.RemoteException
import com.ft.possystemapi.server.ServiceManager
import com.ft.possystemapi.server.device.DeviceManager
import com.ft.possystemapi.server.system.SystemManager
import com.ft.possystemapi.server.utils.Constants
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.SYSTEM_API_INVOKE_SUCCESS
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.system.SystemService
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_SYSTEM_SERVICE_FAILURE
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_SYSTEM_SERVICE_SUCCESS
import global.citytech.finposframework.utility.StringUtils

/**
 * Created by Rishav Chudal on 4/13/21.
 */
class FeitianSystemServiceImpl(val context: Context): SystemService, FeitianLogger.Listener {

    private val serviceManager = ServiceManager.getInstance(this.context.applicationContext)
    private val systemBinder = this.serviceManager.systemManager
    private val systemManager = SystemManager.asInterface(this.systemBinder)
    private val deviceBinder = this.serviceManager.deviceManager
    private val deviceManager = DeviceManager.asInterface(this.deviceBinder)
    private val logger = FeitianLogger(this)
    private var deviceResponse = DeviceResponse(
        Result.FAILURE,
        MSG_SYSTEM_SERVICE_FAILURE
    )

    override fun enableADB(): DeviceResponse {
        if (systemManager != null) {
            this.logDeviceAndServiceInfo()
            try {
                val adbEnabled = systemManager.adbEnabled
                this.logger.logWithClassName("Is ADB already enabled ? ::: ".plus(adbEnabled))
                if (!adbEnabled) {
                    this.logger.logWithClassName("***** Enabling ADB *****")
                    val resultADBEnabled = systemManager.setAdbEnabled(true)
                    if (resultADBEnabled == SYSTEM_API_INVOKE_SUCCESS) {
                        this.logger.logWithClassName("Enabling ADB Success...")
                        this.deviceResponse = DeviceResponse(
                            Result.SUCCESS,
                            MSG_SYSTEM_SERVICE_SUCCESS
                        )
                    } else {
                        this.logger.logWithClassName("Enabling ADB Failed...")
                    }
                } else {
                    this.deviceResponse = DeviceResponse(
                        Result.SUCCESS,
                        MSG_SYSTEM_SERVICE_SUCCESS
                    )
                }
            } catch (exception: Exception) {
                this.logger.logWithClassName("Exception in Enabling ADB...")
                exception.printStackTrace()
            }
        }
        return this.deviceResponse
    }

    override fun disableADB(): DeviceResponse {
        if (systemManager != null) {
            logDeviceAndServiceInfo()
            try {
                val adbEnabled = systemManager.adbEnabled
                this.logger.logWithClassName("Is ADB already enabled ? ::: ".plus(adbEnabled))
                if (adbEnabled) {
                    this.logger.logWithClassName("***** Disabling ADB *****")
                    val resultADBDisabled = systemManager.setAdbEnabled(false)
                    if (resultADBDisabled == SYSTEM_API_INVOKE_SUCCESS) {
                        this.logger.logWithClassName("Disabling ADB Success...")
                        this.deviceResponse = DeviceResponse(
                            Result.SUCCESS,
                            MSG_SYSTEM_SERVICE_SUCCESS
                        )
                    } else {
                        this.logger.logWithClassName("Disabling ADB Failed...")
                    }
                } else {
                    this.deviceResponse = DeviceResponse(
                        Result.SUCCESS,
                        MSG_SYSTEM_SERVICE_SUCCESS
                    )
                }
            } catch (exception: Exception) {
                this.logger.logWithClassName("Exception in Disabling ADB...")
                exception.printStackTrace()
            }
        }
        return this.deviceResponse
    }

    override fun doFactoryReset(): DeviceResponse {
        TODO("Not yet implemented")
    }

    override fun doSystemUpdate(firmwareFilePath: String): DeviceResponse {
        TODO("Not yet implemented")
    }

    override fun enableOTG(): DeviceResponse {
        if (systemManager != null) {
            logDeviceAndServiceInfo()
            try {
                val otgDisabled = systemManager.otgDisabled
                this.logger.logWithClassName("Is OTG already disabled ? ::: ".plus(otgDisabled))
                if (otgDisabled) {
                    this.logger.logWithClassName("***** Enabling OTG *****")
                    val resultOTGEnabled = systemManager.setOTGDisabled(false)
                    if (resultOTGEnabled == SYSTEM_API_INVOKE_SUCCESS) {
                        this.logger.logWithClassName("Enabling OTG Success...")
                        this.deviceResponse = DeviceResponse(
                            Result.SUCCESS,
                            MSG_SYSTEM_SERVICE_SUCCESS
                        )
                    } else {
                        this.logger.logWithClassName("Enabling OTG Failed...")
                    }
                } else {
                    this.deviceResponse = DeviceResponse(
                        Result.SUCCESS,
                        MSG_SYSTEM_SERVICE_SUCCESS
                    )
                }
            } catch (exception: Exception) {
                this.logger.logWithClassName("Exception in Enabling OTG...")
                exception.printStackTrace()
            }
        }
        return this.deviceResponse
    }

    override fun disableOTG(): DeviceResponse {
        if (systemManager != null) {
            logDeviceAndServiceInfo()
            try {
                val otgDisabled = systemManager.otgDisabled
                this.logger.logWithClassName("Is OTG already disabled ? ::: ".plus(otgDisabled))
                if (!otgDisabled) {
                    this.logger.logWithClassName("***** Disabling OTG *****")
                    val resultOTGDisabled = systemManager.setOTGDisabled(true)
                    if (resultOTGDisabled == SYSTEM_API_INVOKE_SUCCESS) {
                        this.logger.logWithClassName("Disabling OTG Success...")
                        this.deviceResponse = DeviceResponse(
                            Result.SUCCESS,
                            MSG_SYSTEM_SERVICE_SUCCESS
                        )
                    } else {
                        this.logger.logWithClassName("Disabling OTG Failed...")
                    }
                } else {
                    this.deviceResponse = DeviceResponse(
                        Result.SUCCESS,
                        MSG_SYSTEM_SERVICE_SUCCESS
                    )
                }
            } catch (exception: Exception) {
                this.logger.logWithClassName("Exception in Disabling OTG...")
                exception.printStackTrace()
            }
        }
        return this.deviceResponse
    }

    override fun enableMicroSD(): DeviceResponse {
        TODO("Not yet implemented")
    }

    override fun disableMicroSD(): DeviceResponse {
        TODO("Not yet implemented")
    }

    override fun enableHomeButton(): DeviceResponse {
        if (systemManager != null) {
            logDeviceAndServiceInfo()
            try {
                this.logger.logWithClassName("***** Enabling Home Button *****")
                val resultHomeButtonEnabled =
                    systemManager.setNavBarDisabled(Constants.DISABLE_NONE)
                if (resultHomeButtonEnabled == SYSTEM_API_INVOKE_SUCCESS) {
                    this.logger.logWithClassName("Enabling Home Button Success...")
                    this.deviceResponse = DeviceResponse(
                        Result.SUCCESS,
                        MSG_SYSTEM_SERVICE_SUCCESS
                    )
                } else {
                    this.logger.logWithClassName("Enabling Home Button Failed...")
                }
            } catch (exception: Exception) {
                this.logger.logWithClassName("Exception in Enabling Home Button...")
                exception.printStackTrace()
            }
        }
        return this.deviceResponse
    }

    override fun disableHomeButton(): DeviceResponse {
        if (systemManager != null) {
            logDeviceAndServiceInfo()
            try {
                this.logger.logWithClassName("***** Disabling Home Button *****")
                val resultHomeButtonDisabled =
                    systemManager.setNavBarDisabled(Constants.DISABLE_HOME)
                if (resultHomeButtonDisabled == SYSTEM_API_INVOKE_SUCCESS) {
                    this.logger.logWithClassName("Disabling Home Button Success...")
                    this.deviceResponse = DeviceResponse(
                        Result.SUCCESS,
                        MSG_SYSTEM_SERVICE_SUCCESS
                    )
                } else {
                    this.logger.logWithClassName("Disabling Home Button Failed...")
                }
            } catch (exception: Exception) {
                this.logger.logWithClassName("Exception in Disabling Home Button...")
                exception.printStackTrace()
            }
        }
        return this.deviceResponse
    }

    override fun enablePowerButton(): DeviceResponse {
        if (systemManager != null) {
            logDeviceAndServiceInfo()
            try {
                val powerKeyDisabled = systemManager.powerKeyDisabled
                this.logger.logWithClassName(
                    "Is Power Key already disabled ? ::: ".plus(powerKeyDisabled)
                )
                if (powerKeyDisabled) {
                    this.logger.logWithClassName("***** Enabling Power Key *****")
                    val resultPowerKeyEnabled = systemManager.setPowerKeyDisabled(false)
                    if (resultPowerKeyEnabled == SYSTEM_API_INVOKE_SUCCESS) {
                        this.logger.logWithClassName("Enabling Power Key Success...")
                        this.deviceResponse = DeviceResponse(
                            Result.SUCCESS,
                            MSG_SYSTEM_SERVICE_SUCCESS
                        )
                    } else {
                        this.logger.logWithClassName("Enabling Power Key Failed...")
                    }
                } else {
                    this.deviceResponse = DeviceResponse(
                        Result.SUCCESS,
                        MSG_SYSTEM_SERVICE_SUCCESS
                    )
                }
            } catch (exception: Exception) {
                this.logger.logWithClassName("Exception in Enabling Power Key...")
                exception.printStackTrace()
            }
        }
        return this.deviceResponse
    }

    override fun disablePowerButton(): DeviceResponse {
        if (systemManager != null) {
            logDeviceAndServiceInfo()
            try {
                val powerKeyDisabled = systemManager.powerKeyDisabled
                this.logger.logWithClassName(
                    "Is Power Key already disabled ? ::: ".plus(powerKeyDisabled)
                )
                if (!powerKeyDisabled) {
                    this.logger.logWithClassName("***** Disabling Power Key *****")
                    val resultPowerKeyDisabled = systemManager.setPowerKeyDisabled(true)
                    if (resultPowerKeyDisabled == SYSTEM_API_INVOKE_SUCCESS) {
                        this.logger.logWithClassName("Disabling Power Key Success...")
                        this.deviceResponse = DeviceResponse(
                            Result.SUCCESS,
                            MSG_SYSTEM_SERVICE_SUCCESS
                        )
                    } else {
                        this.logger.logWithClassName("Disabling Power Key Failed...")
                    }
                } else {
                    this.deviceResponse = DeviceResponse(
                        Result.SUCCESS,
                        MSG_SYSTEM_SERVICE_SUCCESS
                    )
                }
            } catch (exception: Exception) {
                this.logger.logWithClassName("Exception in Disabling Power Key...")
                exception.printStackTrace()
            }
        }
        return this.deviceResponse
    }

    override fun enableRecentButton(): DeviceResponse {
        if (systemManager != null) {
            logDeviceAndServiceInfo()
            try {
                this.logger.logWithClassName("***** Enabling Recent Button *****")
                val resultRecentButtonEnabled =
                    systemManager.setNavBarDisabled(Constants.DISABLE_NONE)
                if (resultRecentButtonEnabled == SYSTEM_API_INVOKE_SUCCESS) {
                    this.logger.logWithClassName("Enabling Recent Button Success...")
                    this.deviceResponse = DeviceResponse(
                        Result.SUCCESS,
                        MSG_SYSTEM_SERVICE_SUCCESS
                    )
                } else {
                    this.logger.logWithClassName("Enabling Recent Button Failed...")
                }
            } catch (exception: Exception) {
                this.logger.logWithClassName("Exception in Enabling Recent Button...")
                exception.printStackTrace()
            }
        }
        return this.deviceResponse
    }

    override fun disableRecentButton(): DeviceResponse {
        if (systemManager != null) {
            logDeviceAndServiceInfo()
            try {
                this.logger.logWithClassName("***** Disabling Recent Button *****")
                val resultRecentButtonDisabled =
                    systemManager.setNavBarDisabled(Constants.DISABLE_RECENT)
                if (resultRecentButtonDisabled == SYSTEM_API_INVOKE_SUCCESS) {
                    this.logger.logWithClassName("Disabling Recent Button Success...")
                    this.deviceResponse = DeviceResponse(
                        Result.SUCCESS,
                        MSG_SYSTEM_SERVICE_SUCCESS
                    )
                } else {
                    this.logger.logWithClassName("Disabling Recent Button Failed...")
                }
            } catch (exception: Exception) {
                this.logger.logWithClassName("Exception in Disabling Recent Button...")
                exception.printStackTrace()
            }
        }
        return this.deviceResponse
    }

    override fun enableBluetooth(): DeviceResponse {
        if (systemManager != null) {
            logDeviceAndServiceInfo()
            try {
                val bluetoothDisabled = systemManager.bluetoothDisabled
                this.logger.logWithClassName(
                    "Is Bluetooth already disabled ? ::: ".plus(bluetoothDisabled)
                )
                if (bluetoothDisabled) {
                    this.logger.logWithClassName("=== Enabling Bluetooth ===")
                    val resultBluetoothEnabled = systemManager.setBluetoothDisabled(false)
                    if (resultBluetoothEnabled == SYSTEM_API_INVOKE_SUCCESS) {
                        this.logger.logWithClassName("Enabling Bluetooth Success...")
                        this.deviceResponse = DeviceResponse(
                            Result.SUCCESS,
                            MSG_SYSTEM_SERVICE_SUCCESS
                        )
                    } else {
                        this.logger.logWithClassName("Enabling Bluetooth Failed...")
                    }
                } else {
                    this.deviceResponse = DeviceResponse(
                        Result.SUCCESS,
                        MSG_SYSTEM_SERVICE_SUCCESS
                    )
                }
            } catch (exception: Exception) {
                this.logger.logWithClassName("Exception in Enabling Bluetooth...")
                exception.printStackTrace()
            }
        }
        return this.deviceResponse
    }

    override fun disableBluetooth(): DeviceResponse {
        if (systemManager != null) {
            logDeviceAndServiceInfo()
            try {
                val bluetoothDisabled = systemManager.bluetoothDisabled
                this.logger.logWithClassName(
                    "Is Bluetooth already disabled ? ::: ".plus(bluetoothDisabled)
                )
                if (!bluetoothDisabled) {
                    this.logger.logWithClassName("=== Disabling Bluetooth ===")
                    val resultBluetoothDisabled = systemManager.setBluetoothDisabled(true)
                    if (resultBluetoothDisabled == SYSTEM_API_INVOKE_SUCCESS) {
                        this.logger.logWithClassName("Disabling Bluetooth Success...")
                        this.deviceResponse = DeviceResponse(
                            Result.SUCCESS,
                            MSG_SYSTEM_SERVICE_SUCCESS
                        )
                    } else {
                        this.logger.logWithClassName("Disabling Bluetooth Failed...")
                    }
                } else {
                    this.deviceResponse = DeviceResponse(
                        Result.SUCCESS,
                        MSG_SYSTEM_SERVICE_SUCCESS
                    )
                }
            } catch (exception: Exception) {
                this.logger.logWithClassName("Exception in Disabling Bluetooth...")
                exception.printStackTrace()
            }
        }
        return this.deviceResponse
    }

    override fun enableWifi(): DeviceResponse {
        if (systemManager != null) {
            logDeviceAndServiceInfo()
            try {
                val wifiDisabled = systemManager.wifiDisabled
                this.logger.logWithClassName(
                    "Is Wifi already disabled ? ::: ".plus(wifiDisabled)
                )
                if (wifiDisabled) {
                    this.logger.logWithClassName("***** Enabling Wifi *****")
                    val resultWifiEnabled = systemManager.setWifiDisabled(false)
                    if (resultWifiEnabled == SYSTEM_API_INVOKE_SUCCESS) {
                        this.logger.logWithClassName("Enabling Wifi Success...")
                        this.deviceResponse = DeviceResponse(
                            Result.SUCCESS,
                            MSG_SYSTEM_SERVICE_SUCCESS
                        )
                    } else {
                        this.logger.logWithClassName("Enabling Wifi Failed...")
                    }
                } else {
                    this.deviceResponse = DeviceResponse(
                        Result.SUCCESS,
                        MSG_SYSTEM_SERVICE_SUCCESS
                    )
                }
            } catch (exception: Exception) {
                this.logger.logWithClassName("Exception in Enabling Wifi...")
                exception.printStackTrace()
            }
        }
        return this.deviceResponse
    }

    override fun disableWifi(): DeviceResponse {
        if (systemManager != null) {
            logDeviceAndServiceInfo()
            try {
                val wifiDisabled = systemManager.wifiDisabled
                this.logger.logWithClassName(
                    "Is Wifi already disabled ? ::: ".plus(wifiDisabled)
                )
                if (!wifiDisabled) {
                    this.logger.logWithClassName("=== Disabling Wifi ===")
                    val resultWifiDisabled = systemManager.setWifiDisabled(true)
                    if (resultWifiDisabled == SYSTEM_API_INVOKE_SUCCESS) {
                        this.logger.logWithClassName("Disabling Wifi Success...")
                        this.deviceResponse = DeviceResponse(
                            Result.SUCCESS,
                            MSG_SYSTEM_SERVICE_SUCCESS
                        )
                    } else {
                        this.logger.logWithClassName("Disabling Wifi Failed...")
                    }
                } else {
                    this.deviceResponse = DeviceResponse(
                        Result.SUCCESS,
                        MSG_SYSTEM_SERVICE_SUCCESS
                    )
                }
            } catch (exception: Exception) {
                this.logger.logWithClassName("Exception in Disabling Wifi...")
                exception.printStackTrace()
            }
        }
        return this.deviceResponse
    }

    private fun logDeviceAndServiceInfo() {
        this.logger.logWithClassName("********** Logging Device And Service Info...")
        logSDKVersion()
        this.logServiceSDKVersion()
        this.logOSVersion()
    }

    private fun logSDKVersion() {
        try {
            val sdkVersion = deviceManager.sdkVersion
            if (!StringUtils.isEmpty(sdkVersion)) {
                this.logger.logWithClassName("PosSysAPISdkVersion ::: ".plus(sdkVersion))
            }
        } catch (e: RemoteException) {
            this.logger.logWithClassName("Exception in logging SDK Version...")
            e.printStackTrace()
        }
    }

    private fun logServiceSDKVersion() {
        val serviceSDKVersion = serviceManager.version
        if (!StringUtils.isEmpty(serviceSDKVersion)) {
            this.logger.logWithClassName("PosSysAPIServiceSdkVersion ::: ".plus(serviceSDKVersion))
        }
    }

    private fun logOSVersion() {
        try {
            val osVersion = deviceManager.osVersion
            if (!StringUtils.isEmpty(osVersion)) {
                this.logger.logWithClassName("OS Version ::: ".plus(osVersion))
            }
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    override fun getClassName() = FeitianSystemServiceImpl::class.java.simpleName
}