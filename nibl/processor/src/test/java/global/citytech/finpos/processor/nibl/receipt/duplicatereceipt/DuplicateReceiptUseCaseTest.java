package global.citytech.finpos.processor.nibl.receipt.duplicatereceipt;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import global.citytech.finpos.processor.nibl.transaction.receipt.NiblTransactionReceiptPrintHandler;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

/**
 * Created by Unique Shakya on 12/16/2020.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({PrinterService.class, ReceiptLog.class, NiblTransactionReceiptPrintHandler.class, TransactionRepository.class})
public class DuplicateReceiptUseCaseTest {

    PrinterService printerService;
    ReceiptLog receiptLog;
    NiblTransactionReceiptPrintHandler niblTransactionReceiptPrintHandler;
    TransactionRepository transactionRepository;

    @Before
    public void setup() {
        printerService = PowerMockito.mock(PrinterService.class);
        receiptLog = PowerMockito.mock(ReceiptLog.class);
        niblTransactionReceiptPrintHandler = PowerMockito.mock(NiblTransactionReceiptPrintHandler.class);
        transactionRepository = PowerMockito.mock(TransactionRepository.class);
    }

    @Test
    public void printTest() throws Exception {
        PowerMockito.when(printerService.print(any())).thenReturn(new PrinterResponse(Result.SUCCESS, "SUCCESS"));
        PowerMockito.when(receiptLog.getRetailer()).thenReturn(new Retailer());
        PowerMockito.when(transactionRepository.getReceiptLog()).thenReturn(receiptLog);

        PowerMockito.whenNew(NiblTransactionReceiptPrintHandler.class).withArguments(printerService)
                .thenReturn(niblTransactionReceiptPrintHandler);
        PowerMockito.doNothing().when(niblTransactionReceiptPrintHandler).printTransactionReceipt(receiptLog,
                ReceiptVersion.DUPLICATE_COPY);

        DuplicateReceiptRequestModel duplicateReceiptRequestModel = new DuplicateReceiptRequestModel(transactionRepository, printerService);
        DuplicateReceiptUseCase duplicateReceiptUseCase = new DuplicateReceiptUseCase();
        DuplicateReceiptResponseModel duplicateReceiptResponseModel = duplicateReceiptUseCase
                .execute(duplicateReceiptRequestModel);

        assertNotNull(duplicateReceiptResponseModel);
    }
}
