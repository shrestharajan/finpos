package global.citytech.finpos.neps.iso8583;

import global.citytech.finposframework.iso8583.DataElementConfigs;
import global.citytech.finposframework.iso8583.SpecInfo;

/** User: Surajchhetry Date: 2/19/20 Time: 5:29 PM */
public class NepsSpecInfo implements SpecInfo {

  private DataElementConfigs collection;

  public NepsSpecInfo() {
    this.collection = new NepsDataElementConfig();
  }

  public NepsSpecInfo(DataElementConfigs collection) {
    super();
    this.collection = collection;
  }

  @Override
  public DataElementConfigs getDEConfig() {
    return collection;
  }

  /*
      @Override
      public TPDU getTPDU() {
          byte[] destination = {0x03, 0x03};
          byte[] originator = {0x00, 0x00};
          return new TPDU((byte) 0x60, destination, originator);
      }

      @Override
      public boolean containsTPDU() {
          return true;
      }
  */
  @Override
  public int getMessageHeaderLength() {
    return 2;
  }

  @Override
  public int getMTILength() {
    return 4;
  }
}
