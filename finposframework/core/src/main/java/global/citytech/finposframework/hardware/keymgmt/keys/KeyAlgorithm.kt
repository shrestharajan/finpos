package global.citytech.finposframework.hardware.keymgmt.keys

/**
 * Created by Rishav Chudal on 4/9/20.
 */
enum class KeyAlgorithm {
    TYPE_3DES,
    TYPE_AES,
    TYPE_SM4,
    TYPE_DES,
    TYPE_RSA
}