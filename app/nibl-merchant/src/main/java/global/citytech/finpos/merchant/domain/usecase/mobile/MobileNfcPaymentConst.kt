package global.citytech.finpos.merchant.domain.usecase.mobile

/**
 * @author SIDDHARTHA GHIMIRE
 */
object MobileNfcPaymentConst {
    const val READ_NFC_MOBILE: String = "Reading Data..."
    const val ERROR_MOBILE_NFC: String = "Unable to read nfc"
    const val DEVICE_WEIPASS: String = "weipass"
    const val PROCESSING_MOBILE_NFC: String = "Processing Data..."
    const val ERROR_PROCESSING_MOBILE: String = "Code: 102 \nPROCESSING ERROR"
    const val ERROR_TIMEOUT: String = "Timeout"
    const val ERROR_PAYMENT_STATUS_MOBILE: String = "ERROR ! \nProceed to transaction menu to confirm the status of the payment."
    const val ERROR_NOT_VALID_NFC = "Source not found"
    const val INITIATION_MOBILE_NFC: String = "NFC"
    const val PROCESSING_PAYMENT_MOBILE: String = "PROCESSING"
    const val APPROVED_PAYMENT_MOBILE: String = "APPROVED"
    const val DECLINED_PAYMENT_MOBILE: String = "DECLINED"
    const val INVALID_SOURCE_CODE_MOBILE: String = "6999"
    const val INVALID_SOURCE_CODE_CARD: String = "6A82"
    const val INVALID_NFC_CARD: String = "6999"
    const val DATA_PARSER_FIRST = "FP01"
    const val DATA_PARSER_SECOND = "FP02"
    const val DATA_PARSER_THIRD = "FP03"
    const val INITIATOR_MOBILE = "MobilePay"
    const val INITIATOR_QR = "QR"
    const val FAILED_TRANSACTION = "failed"
    const val TRANSACTION_VOID_SALE = "void_sale"
    const val PAYMENT_REQUEST_NUMBER = "REQ"
    const val PAYMENT_REFERENCE_NUMBER = "REF"
    const val PAYMENT_BILLING_NUMBER = "BIL"
}