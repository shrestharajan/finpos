package global.citytech.finposframework.hardware.emv.emvkeys

/**
 * Created by Rishav Chudal on 4/30/20.
 */
data class EmvKeysInjectionRequest(val emvKeyList: List<EmvKey>)