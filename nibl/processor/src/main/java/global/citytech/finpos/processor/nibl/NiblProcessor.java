package global.citytech.finpos.processor.nibl;

import global.citytech.finpos.processor.nibl.balanceinquiry.BalanceInquiryUseCase;
import global.citytech.finpos.processor.nibl.cashin.CashInUseCase;
import global.citytech.finpos.processor.nibl.greenpin.GreenPinUseCase;
import global.citytech.finpos.processor.nibl.logon.MerchantLogOnUseCase;
import global.citytech.finpos.processor.nibl.pinchange.PinChangeUseCase;
import global.citytech.finpos.processor.nibl.ministatement.MiniStatementUseCase;
import global.citytech.finpos.processor.nibl.posmode.PosModeUseCase;
import global.citytech.finpos.processor.nibl.receipt.customercopy.CustomerCopyUseCase;
import global.citytech.finpos.processor.nibl.receipt.detailreport.DetailReportUseCase;
import global.citytech.finpos.processor.nibl.receipt.duplicatereceipt.DuplicateReceiptUseCase;
import global.citytech.finpos.processor.nibl.receipt.duplicatereconciliation.DuplicateReconciliationReceiptUseCase;
import global.citytech.finpos.processor.nibl.receipt.ministatement.StatementPrintUseCase;
import global.citytech.finpos.processor.nibl.receipt.summaryreport.SummaryReportUseCase;
import global.citytech.finpos.processor.nibl.receipt.tmslogs.PrintParameterUseCase;
import global.citytech.finpos.processor.nibl.reconciliation.ReconciliationUseCase;
import global.citytech.finpos.processor.nibl.reconciliation.check.CheckReconciliationUseCase;
import global.citytech.finpos.processor.nibl.reconciliation.clear.BatchClearUseCase;
import global.citytech.finpos.processor.nibl.transaction.authorisation.completion.PreAuthCompletionUseCase;
import global.citytech.finpos.processor.nibl.transaction.authorisation.preauth.CardPreAuthUseCase;
import global.citytech.finpos.processor.nibl.transaction.authorisation.preauth.ManualPreAuthUseCase;
import global.citytech.finpos.processor.nibl.transaction.autoreversal.AutoReversalUseCase;
import global.citytech.finpos.processor.nibl.transaction.cash.advance.CashAdvanceUseCase;
import global.citytech.finpos.processor.nibl.transaction.purchase.CardPurchaseUseCase;
import global.citytech.finpos.processor.nibl.transaction.purchase.ManualPurchaseUseCase;
import global.citytech.finpos.processor.nibl.transaction.purchase.PurchaseRequestModel;
import global.citytech.finpos.processor.nibl.transaction.purchase.PurchaseResponseModel;
import global.citytech.finpos.processor.nibl.transaction.refund.CardRefundUseCase;
import global.citytech.finpos.processor.nibl.transaction.refund.ManualRefundUseCase;
import global.citytech.finpos.processor.nibl.transaction.tipadjustment.TipAdjustmentUseCase;
import global.citytech.finpos.processor.nibl.transaction.voidsale.VoidSaleUseCase;
import global.citytech.finpos.processor.nibl.transactiontype.TransactionTypeUseCase;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.switches.Processor;
import global.citytech.finposframework.switches.ProcessorManager;
import global.citytech.finposframework.switches.autoreversal.AutoReversalRequester;
import global.citytech.finposframework.switches.keyexchange.KeyExchangeRequester;
import global.citytech.finposframework.switches.logon.LogOnRequester;
import global.citytech.finposframework.switches.posmode.PosModeRequester;
import global.citytech.finposframework.switches.receipt.customercopy.CustomerCopyRequester;
import global.citytech.finposframework.switches.receipt.detailreport.DetailReportRequester;
import global.citytech.finposframework.switches.receipt.duplicatereceipt.DuplicateReceiptRequester;
import global.citytech.finposframework.switches.receipt.duplicatereconciliation.DuplicateReconciliationReceiptRequester;
import global.citytech.finposframework.switches.receipt.ministatement.StatementRequester;
import global.citytech.finposframework.switches.receipt.summaryreport.SummaryReportRequester;
import global.citytech.finposframework.switches.receipt.tmslogs.TmsLogsRequester;
import global.citytech.finposframework.switches.reconciliation.ReconciliationRequester;
import global.citytech.finposframework.switches.reconciliation.check.CheckReconciliationRequester;
import global.citytech.finposframework.switches.reconciliation.clear.BatchClearRequester;
import global.citytech.finposframework.switches.transaction.TransactionRequester;
import global.citytech.finposframework.switches.transactiontype.TransactionTypeRequester;

public class NiblProcessor implements Processor {
    static {
        ProcessorManager.register(new NiblProcessor());
    }

    private TerminalRepository terminalRepository;
    private Notifier notifier;

    @Override
    public void init(TerminalRepository terminalRepository, Notifier notifier) {
        if (terminalRepository != null)
            this.terminalRepository = terminalRepository;
        if (notifier != null)
            this.notifier = notifier;
    }

    @Override
    public LogOnRequester getLogOnRequest() {
        return new MerchantLogOnUseCase(this.terminalRepository);
    }

    @Override
    public KeyExchangeRequester getKeyExchangeRequester() {
        return null;
    }

    @Override
    public TransactionRequester getPurchaseRequester() {
        return new CardPurchaseUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getPreAuthRequester() {
        return new CardPreAuthUseCase(terminalRepository, this.notifier);
    }

    @Override
    public CustomerCopyRequester getCustomerCopyRequester() {
        return new CustomerCopyUseCase();
    }

    @Override
    public StatementRequester getStatementPrintRequester(){
        return new StatementPrintUseCase();
    }


    @Override
    public ReconciliationRequester getReconciliationRequester() {
        return new ReconciliationUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getVoidSaleRequester() {
        return new VoidSaleUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getRefundRequester() {
        return new CardRefundUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getManualRefundRequester() {
        return new ManualRefundUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getAuthorisationCompletionRequester() {
        return new PreAuthCompletionUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getManualPurchaseRequester() {
        return new ManualPurchaseUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getManualPreAuthRequester() {
        return new ManualPreAuthUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TmsLogsRequester getTmsLogsRequester() {
        return new PrintParameterUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionTypeRequester getTransactionTypeRequester() {
        return new TransactionTypeUseCase();
    }

    @Override
    public DuplicateReceiptRequester getDuplicateReceiptRequester() {
        return new DuplicateReceiptUseCase();
    }

    @Override
    public DuplicateReconciliationReceiptRequester getDuplicateReconciliationReceiptRequester() {
        return new DuplicateReconciliationReceiptUseCase();
    }

    @Override
    public PosModeRequester getPosModeRequester() {
        return new PosModeUseCase();
    }

    @Override
    public TransactionRequester getCashAdvanceRequester() {
        return new CashAdvanceUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getManualCashAdvanceRequester() {
        return null;
    }

    @Override
    public TransactionRequester getTipAdjustmentRequester() {
        return new TipAdjustmentUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public AutoReversalRequester getAutoReversalRequester() {
        return new AutoReversalUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public CheckReconciliationRequester getCheckReconciliationRequester() {
        return new CheckReconciliationUseCase(this.terminalRepository);
    }

    @Override
    public TransactionRequester<PurchaseRequestModel, PurchaseResponseModel> getIdlePurchaseRequester() {
        return null;
    }

    @Override
    public DetailReportRequester getDetailReportRequester() {
        return new DetailReportUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public SummaryReportRequester getSummaryReportRequester() {
        return new SummaryReportUseCase(this.terminalRepository);
    }

    @Override
    public BatchClearRequester getBatchClearRequester() {
        return new BatchClearUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getGreenPinRequester() {
        return new GreenPinUseCase(this.terminalRepository,this.notifier);
    }

    @Override
    public TransactionRequester getPinChangeRequester() {
        return new PinChangeUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getBalanceInquiryRequester() {
        return new BalanceInquiryUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getCashInRequester() {
        return new CashInUseCase(this.terminalRepository, this.notifier);
    }

    @Override
    public TransactionRequester getMiniStatementRequester() {
        return new MiniStatementUseCase(this.terminalRepository,this.notifier);
    }

}
