package global.citytech.finpos.processor.nibl.balanceinquiry;

import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;

public class BalanceInquiryResponse extends TransactionResponseModel {
    private String debugRequestMessage;
    private String debugResponseMessage;
    private String message;
    private String additionalAmount;
    private String stan;
    private boolean approved;
    private boolean shouldPrintCustomerCopy;

    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }


    public String getAdditionalAmount() {
        return additionalAmount;
    }

    public String getMessage() {
        return message;
    }

    public String getStan() {
        return stan;
    }


    public boolean isApproved() {
        return approved;
    }

    public boolean isShouldPrintCustomerCopy() {
        return shouldPrintCustomerCopy;
    }

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private String message;
        private String stan;
        private String additionalAmount;
        private boolean approved;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static BalanceInquiryResponse.Builder newInstance() {
            return new BalanceInquiryResponse.Builder();
        }

        public BalanceInquiryResponse.Builder withDebugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public BalanceInquiryResponse.Builder withDebugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public BalanceInquiryResponse.Builder withShouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }
        public BalanceInquiryResponse.Builder withApproved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public BalanceInquiryResponse.Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public BalanceInquiryResponse.Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }

        public BalanceInquiryResponse.Builder withAdditionalAmount(String additionalAmount) {
            this.additionalAmount = additionalAmount;
            return this;
        }

        public BalanceInquiryResponse build() {
            BalanceInquiryResponse BalanceInquiryResponse = new BalanceInquiryResponse();
            BalanceInquiryResponse.debugRequestMessage = this.debugRequestMessage;
            BalanceInquiryResponse.debugResponseMessage = this.debugResponseMessage;
            BalanceInquiryResponse.approved = this.approved;
            BalanceInquiryResponse.message = this.message;
            BalanceInquiryResponse.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            BalanceInquiryResponse.stan = this.stan;
            BalanceInquiryResponse.additionalAmount = this.additionalAmount;
            return BalanceInquiryResponse;
        }
    }
}
