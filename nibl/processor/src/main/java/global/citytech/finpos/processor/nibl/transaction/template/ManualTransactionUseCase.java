package global.citytech.finpos.processor.nibl.transaction.template;

import java.util.List;

import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finpos.processor.nibl.NiblConstants;
import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;
import global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier;
import global.citytech.finpos.processor.nibl.transaction.TransactionSetTableVerifier;
import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardSummary;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.PinBlock;
import global.citytech.finposframework.hardware.io.cards.cvm.Cvm;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat;
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest;
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Unique Shakya on 3/8/2021.
 */
public abstract class ManualTransactionUseCase extends TransactionUseCaseTemplate {

    public ManualTransactionUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    protected String retrieveInvoiceNumber() {
        return this.terminalRepository.getInvoiceNumber();
    }

    @Override
    protected void retrieveCardDetails(List<CardType> allowedCardTypes, boolean isForceContactCardPrompt) {
        this.readCardResponse = this.prepareManualReadCardResponse();
        this.identifyCardScheme();
    }

    @Override
    protected void processCard() {
        PurchaseRequest purchaseRequest = this.preparePurchaseRequest();
        this.retrievePinBlockIfRequired(purchaseRequest);
    }

    @Override
    protected void processOnlineResultToCard(IsoMessageResponse isoMessageResponse) {
        // not needed for manual entry
    }

    @Override
    protected void processUnableToGoOnlineResultToCard() {
        // not needed for manual entry
    }

    @Override
    protected boolean checkTransactionDeclinedByCard(IsoMessageResponse isoMessageResponse) {
        return false;
    }

    @Override
    protected void notifyWithLed(boolean isApproved) {
        // not needed for manual entry
    }

    @Override
    protected TransactionIsoRequest prepareTransactionIsoRequest() {
        return TransactionIsoRequest.Builder.newInstance()
                .pan(this.readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .expireDate(this.readCardResponse.getCardDetails().getExpiryDate())
                .posConditionCode("05")
                .transactionAmount(this.readCardResponse.getCardDetails().getAmount())
                .localTime(HelperUtils.getDefaultLocaleTimeHhMmSs())
                .localDate(HelperUtils.getDefaultLocaleDateWithoutYear())
                .invoiceNumber(this.invoiceNumber)
                .posEntryMode(PosEntryMode.MANUAL)
                .currencyCode(this.transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .pinBlock(this.shouldSkipPinBlockFromCard() ? "" : readCardResponse.getCardDetails().getPinBlock().getPin())
                .originalRetrievalReferenceNumber(this.transactionRequest.getOriginalRetrievalReferenceNumber())
                .cvv(this.readCardResponse.getCardDetails().getCvv())
                .build();
    }

    @Override
    protected boolean isForceContactCardPromptActionCode(IsoMessageResponse isoMessageResponse) {
        return false;
    }

    private void retrievePinBlockIfRequired(PurchaseRequest purchaseRequest) {
        PinBlock pinBlock = new PinBlock("", false);
        Cvm cvm = this.retrieveCvm(purchaseRequest);
        boolean pinRequired = cvm.equals(Cvm.ENCIPHERED_PIN_ONLINE);
        if (pinRequired) {
            String pinPadAmountMessage = TransactionUtils.retrievePinPadMessageWithAmount(purchaseRequest.getAmount(),
                    this.terminalRepository.getTerminalTransactionCurrencyName());
            pinBlock = this.retrievePinBlock(purchaseRequest.getCardDetails(),
                    pinPadAmountMessage);
        }
        this.readCardResponse.setCvm(cvm);
        this.readCardResponse.getCardDetails().setPinBlock(pinBlock);
        this.readCardResponse.getCardDetails().setTransactionInitializeDateTime(
                DateUtils.yyMMddHHmmssSSSDate()
        );
    }

    private PinBlock retrievePinBlock(CardDetails cardDetails, String pinPadAmountMessage) {
        String primaryAccountNumber = cardDetails.getPrimaryAccountNumber();
        if (!StringUtils.isEmpty(primaryAccountNumber)) {
            PinRequest pinRequest = new PinRequest(
                    primaryAccountNumber,
                    NiblConstants.PIN_PAD_MESSAGE,
                    PinBlockFormat.ISO9564_FORMAT_0,
                    4,
                    12,
                    30,
                    pinPadAmountMessage,
                    !this.terminalRepository.shouldShufflePinPad(),
                   false,
                    "Enter PIN"
            );
            CardSummary cardSummary = new CardSummary(cardDetails.getCardSchemeLabel(),
                    primaryAccountNumber,cardDetails.getCardHolderName(),DateUtils.toYYMMFormat(cardDetails.getExpiryDate()));
            pinRequest.setCardSummary(cardSummary);
            PinResponse pinResponse = this.request.getTransactionAuthenticator()
                    .authenticateUser(pinRequest);
            if (pinResponse.getResult() == Result.SUCCESS)
                return pinResponse.getPinBlock();
            else if (pinResponse.getResult() == Result.USER_CANCELLED)
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            else if (pinResponse.getResult() == Result.TIMEOUT)
                throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
            else
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    private Cvm retrieveCvm(PurchaseRequest purchaseRequest) {
        String shortCardScheme = purchaseRequest.getCardDetails().getCardScheme().getCardSchemeId();
        TransactionSetTableVerifier transactionSetTableVerifier = new TransactionSetTableVerifier(
                this.request.getApplicationRepository(),
                shortCardScheme,
                TransactionUtils.mapPurchaseTypeWithTransactionType(this.transactionRequest.getTransactionType())
        );
        String cvmToPerform = transactionSetTableVerifier.getCardHolderVerificationMethod();
        switch (cvmToPerform) {
            case "1":
                return Cvm.SIGNATURE;
            case "2":
            case "3":
                return Cvm.ENCIPHERED_PIN_ONLINE;
            default:
                return Cvm.NO_CVM;
        }
    }

    private void identifyCardScheme() {
        CardIdentifier cardIdentifier = new CardIdentifier(this.request.getTransactionRepository(),
                this.request.getApplicationRepository());
        cardIdentifier.identify(this.readCardResponse);
    }

    private ReadCardResponse prepareManualReadCardResponse() {
        CardDetails cardDetails = new CardDetails();
        cardDetails.setCardType(CardType.MANUAL);
        cardDetails.setPrimaryAccountNumber(this.transactionRequest.getCardNumber());
        cardDetails.setExpiryDate(this.transactionRequest.getExpiryDate());
        cardDetails.setAmount(this.transactionRequest.getAmount());
        cardDetails.setCashBackAmount(this.transactionRequest.getAdditionalAmount());
        cardDetails.setCvv(this.transactionRequest.getCvv());
        return new ReadCardResponse(cardDetails, Result.SUCCESS, "Manual Read Card Response Success");
    }
}
