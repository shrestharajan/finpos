package global.citytech.finposframework.iso8583;

import java.util.ArrayList;
import java.util.List;
import global.citytech.finposframework.log.Logger;;

import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.utility.ByteUtils;
import global.citytech.finposframework.utility.EncodingUtils;
import global.citytech.finposframework.utility.HexDump;

/**
 * User: Surajchhetry Date: 2/25/20 Time: 3:22 PM
 */
abstract class MessageParserTemplate implements MessageParser {
    protected SpecInfo specInfo = null;
    private Logger logger = Logger.getLogger(MessageParserTemplate.class.getName());

    public MessageParserTemplate(SpecInfo specInfo) {
        this.specInfo = specInfo;
    }

    @Override
    public Iso8583Msg parse(byte[] data) {
        // Check if data is null
        if (data == null)
            throw new FinPosException(
                    FinPosException.ExceptionType.INVALID_PARAMETER_SUPPLIED, "Parameter is null");
        // 1. parse MTI
        MTI mti = this.parseMTI(data);
        // 2. remove MTI from trimData
        byte[] onlyBitmapAndDE = ByteUtils.remove(data, this.specInfo.getMTILength());
        // 3. parse Bitmap
        BitMap bitMap = BitMap.parseFromHexASCII(onlyBitmapAndDE);
        logger.debug(bitMap.printField());
        Iso8583Msg msg = new Iso8583Msg(mti);
        // Remove Bitmap from data
        byte[] dataOnly = ByteUtils.remove(onlyBitmapAndDE, BitMap.LENGTH_IN_BYTES);
        msg.addFields(this.splitDataFromDataStream(bitMap, dataOnly));
        return msg;
    }

    protected MTI parseMTI(byte[] data) {
        if (data.length < 4) {
            throw new IllegalArgumentException("MTI size is too small");
        }
        logger.debug(HexDump.dumpHexString(data));
        String header = HexDump.toHexString(ByteUtils.subByte(data, 0, this.specInfo.getMTILength()));
        logger.debug("MTI :::" + header);
        return new MTI(header);
    }

    protected List<DataElement> splitDataFromDataStream(BitMap bitMap, byte[] originalData) {
        List<DataElement> elements = new ArrayList<>();

        String hexData = HexDump.toHexString(originalData);
        logger.debug(hexData);
        String value;

        int hexStringFactor = 2;//Since all data converted into HEX String

        int actualLength = 0;
        for (Integer index : bitMap.getFields()) {
            DataElementConfig deConfig = specInfo.getDEConfig().getByFieldNumber(index);
            if (deConfig.getLengthConfig().getLengthType() == LengthConfig.LengthType.FIXED) {
                actualLength = deConfig.getLengthConfig().getLength();
                if (deConfig.getLengthConfig().getEncodingType() == EncodingType.BCD || deConfig.getLengthConfig().getEncodingType() == EncodingType.ASCII) {
                    logger.debug("BCD LENGTH IN FIXED");
                    actualLength = actualLength * 2;
                }
                value = hexData.substring(0, actualLength);
                logger.debug(String.format("VALUE :: %s", value));
                if (deConfig.getDataConfig().getEncodingType() == EncodingType.ASCII)
                    value = EncodingUtils.hexEncodedToBinaryString(value);
                hexData = hexData.substring(actualLength);
            } else {
                int varLength = (deConfig.getLengthConfig().getLengthType() == LengthConfig.LengthType.LLVAR ? 2 : 4);
                if (deConfig.getLengthConfig().getEncodingType() == EncodingType.ASCII) {
                    varLength = (deConfig.getLengthConfig().getLengthType() == LengthConfig.LengthType.LLVAR ? 4 : 6);
                }
                String strDataLength = hexData.substring(0, varLength);
                if(deConfig.getLengthConfig().getEncodingType()==EncodingType.ASCII){
                    strDataLength = EncodingUtils.hexToASCII(strDataLength);
                }
                int dataLength = Integer.parseInt(strDataLength);
                logger.debug(String.format("strDataLength = %s, Length :: %d", strDataLength, dataLength));
                if (deConfig.getLengthConfig().getEncodingType() == EncodingType.BCD || deConfig.getLengthConfig().getEncodingType() == EncodingType.ASCII) {
                    logger.debug("BCD LENGTH IN VAR");
                    dataLength = dataLength * 2;
                }
                // Since no data left
                if (hexData.length() < varLength + dataLength)
                    break;
                value = hexData.substring(varLength, varLength + dataLength);
                logger.debug(String.format("VALUE :: %s", value));
                if (deConfig.getDataConfig().getEncodingType() == EncodingType.ASCII)
                    value = EncodingUtils.hexEncodedToBinaryString(value);
                hexData = hexData.substring(varLength + dataLength);
            }
            elements.add(new DataElement(index, value));
            logger.debug(String.format(" [ index %d value %s ]", index, value));
            // Since no data left
            if (hexData.length() <= 0)
                break;

        }
        return elements;
    }

}