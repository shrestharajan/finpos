package global.citytech.finpos.merchant.domain.usecase.qr
/**
 * @author sachin
 */
enum class LengthType {
    FIXED,
    LLVAR
}