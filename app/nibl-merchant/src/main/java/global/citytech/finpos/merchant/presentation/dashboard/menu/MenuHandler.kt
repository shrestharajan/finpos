package global.citytech.finpos.merchant.presentation.dashboard.menu

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import global.citytech.common.Constants
import global.citytech.common.extensions.openActivity
import global.citytech.easydroid.core.extension.showToast
import global.citytech.finpos.merchant.presentation.auth.completion.AuthorisationCompletionActivity
import global.citytech.finpos.merchant.presentation.balanceinquiry.BalanceInquiryActivity
import global.citytech.finpos.merchant.presentation.cashadvance.CashAdvanceActivity
import global.citytech.finpos.merchant.presentation.cashin.CashInActivity
import global.citytech.finpos.merchant.presentation.dashboard.temp.DashActivity
import global.citytech.finpos.merchant.presentation.greenpin.GreenPinActivity
import global.citytech.finpos.merchant.presentation.merchant.MerchantActivity
import global.citytech.finpos.merchant.presentation.merchantqr.MerchantQrActivity
import global.citytech.finpos.merchant.presentation.pinchange.PinChangeActivity
import global.citytech.finpos.merchant.presentation.ministatement.MiniStatementActivity
import global.citytech.finpos.merchant.presentation.preauth.PreAuthActivity
import global.citytech.finpos.merchant.presentation.purchase.PurchaseActivity
import global.citytech.finpos.merchant.presentation.qroperators.QrOperatorsActivity
import global.citytech.finpos.merchant.presentation.refund.RefundActivity
import global.citytech.finpos.merchant.presentation.support.SupportActivity
import global.citytech.finpos.merchant.presentation.tipadjustment.TipsAdjustmentActivity
import global.citytech.finpos.merchant.presentation.transactions.TransactionsActivity
import global.citytech.finpos.merchant.presentation.voidsale.VoidActivity
import global.citytech.finpos.merchant.utils.AppUtility


/**
 * Created by Saurav Ghimire on 1/19/21.
 * Citytech
 * saurav.ghimire@citytech.global
 *
 * Modified by Rishav Chudal on 5/10/21
 * Citytech
 * rishav.chudal@citytech.global
 */
interface MenuHandler {
    fun loadMenuItemPage(context: Activity)
}

class PurchaseHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(PurchaseActivity::class.java)
    }
}

class EMIHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        val intent = Intent(context, PurchaseActivity::class.java)
        intent.putExtra(Constants.EXTRA_EMI_TRANSACTION, true)
        context.startActivity(intent)
    }
}

class PreAuthHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(PreAuthActivity::class.java)
    }
}

class VoidSaleHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        val bundle = Bundle()
        bundle.putString(VoidActivity.MODE, VoidActivity.SALE)
        context.openActivity(VoidActivity::class.java, bundle)
    }
}

class AuthCompletionHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(AuthorisationCompletionActivity::class.java)
    }
}

class RefundHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(RefundActivity::class.java)
    }

}

class TipAdjustmentHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(TipsAdjustmentActivity::class.java)
    }
}

class MerchantMenuHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(MerchantActivity::class.java)
    }
}

class AdminMenuHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        (context as DashActivity).getViewModel().retrieveAdminCredential()
    }
}

class CashAdvanceMenuHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(CashAdvanceActivity::class.java)
    }
}

class CashInHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(CashInActivity::class.java)
    }

}

class CashVoidHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        val bundle = Bundle()
        bundle.putString(VoidActivity.MODE, VoidActivity.CASH)
        context.openActivity(VoidActivity::class.java, bundle)
    }
}

class BalanceInquiryHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(BalanceInquiryActivity:: class.java)
    }

}

class MiniStatementHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(MiniStatementActivity::class.java)
    }
}

class TransactionMenuHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(TransactionsActivity::class.java)
    }
}

class MerchantQrMenuHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(MerchantQrActivity::class.java)
    }

}

class SupportMenuHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(SupportActivity::class.java)
    }
}

class TransactionsHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(TransactionsActivity::class.java)
    }
}

class KeyExchangeHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        (context as DashActivity).getViewModel().logOn()
    }
}

class SettlementHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        (context as DashActivity).startSettlementActivity(
            isManualSettlement = true,
            isSettlementConfirmationRequired = true
        )
    }
}

class QrCodeHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(QrOperatorsActivity::class.java)
    }
}

class ConnectFonePayHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        try {
            val launchIntent =
                context.application.packageManager.getLaunchIntentForPackage(
                    global.citytech.finpos.merchant.presentation.utils.Constants.CONNECT_FONEPAY_PACKAGE
                )
            context.startActivity(launchIntent)
        } catch (e: Exception) {
            e.printStackTrace()
            context.showToast("Feature is not available...", Toast.LENGTH_SHORT)
        }
    }
}

class PinChangeHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
        context.openActivity(PinChangeActivity::class.java)
    }
}

class GreenPinHandler : MenuHandler {
    override fun loadMenuItemPage(context: Activity) {
       context.openActivity(GreenPinActivity:: class.java)
    }
}
