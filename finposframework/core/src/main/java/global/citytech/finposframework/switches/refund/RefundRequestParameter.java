package global.citytech.finposframework.switches.refund;

import global.citytech.finposframework.switches.RequestParameter;

/**
 * Created by Unique Shakya on 9/28/2020.
 */
public interface RefundRequestParameter extends RequestParameter {
}
