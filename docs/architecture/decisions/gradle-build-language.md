# Title
Gradle build language

## Status

Decided . We are open to change to alternative if found any issue with selected one.

## Context

Gradle supports two programming languages for witting build script Groovy and Kotlin. So we have to
choose one of them for witting build script.

## Decision

We have selected Kotlin for witting gradle build script.

## Arguments
- Since we are using Kotlin as our primary language for android development so it will be easier to
 write script in Kotlin than groovy
 

## Consequences

- There may not be enough documents regarding Kotlin dsl as groovy so sometime it may take long time.


## Notes
