package global.citytech.nepsadmin.data

data class ConfigRequest(
    var appInfo: AppInfo? = null,
    var deviceInfo: DeviceInfo? = null
)