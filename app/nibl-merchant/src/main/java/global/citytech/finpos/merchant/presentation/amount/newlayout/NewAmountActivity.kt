package global.citytech.finpos.merchant.presentation.amount.newlayout

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.TableRow
import androidx.core.content.ContextCompat
import androidx.core.view.children
import androidx.core.view.forEach
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.button.MaterialButton
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityAmountBinding
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finpos.merchant.utils.loadCurrency
import global.citytech.finposframework.log.Logger
import kotlinx.android.synthetic.main.activity_amount.*
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.presentation.amount.oldlayout.AmountActivity

class NewAmountActivity : AppBaseActivity<ActivityAmountBinding, NewAmountViewModel>(),
    View.OnClickListener,
    View.OnLongClickListener {

    companion object {
        const val REQUEST_CODE = 1001
        const val EXTRA_LIMIT = "extra_limit"
    }

    private val logger = Logger(NewAmountActivity::class.java.name)
    private lateinit var viewModel: NewAmountViewModel
    private var intentFilter: IntentFilter = IntentFilter()
    private lateinit var batteryBroadcast: BroadcastReceiver
    private var batteryLowDialogCount = 0
    private var batteryLevel: Int = 0
    private var isCharging: Boolean = false
    private var extraLimit: String = "0.00"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        getDataFromBundle()
        viewModel = getViewModel()
        initObservers()
        viewModel.retrieveAmountLengthLimit()
        viewModel.getConfiguration()
        //viewModel.checkInternetConnection()
        batteryLevelCheck()
    }

    private fun getDataFromBundle() {
        val intent = getIntent()
        if(intent != null && intent.hasExtra(AmountActivity.EXTRA_LIMIT)){
            extraLimit = intent.getStringExtra(
                AmountActivity.EXTRA_LIMIT
            )
        }
    }

    private fun batteryLevelCheck() {
        batteryBroadcast = object : BroadcastReceiver() {
            override fun onReceive(contxt: Context?, intent: Intent?) {
                val batteryManager =
                    applicationContext.getSystemService(BATTERY_SERVICE) as BatteryManager
                batteryLevel =
                    batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)

                val status = intent?.getIntExtra(BatteryManager.EXTRA_STATUS, -1)

                isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING
                        || status == BatteryManager.BATTERY_STATUS_FULL

                if (Intent.ACTION_BATTERY_CHANGED == intent?.action) {
                    batteryLevel = intent.getIntExtra("level", 0)
                }

                if ((batteryLevel > 10 && batteryLowDialogCount != 0) || isCharging) {
                    batteryLowDialogCount = 0
                    hideConfirmationMessage()
                } else if (batteryLevel <= 10 && batteryLowDialogCount == 0) {
                    batteryLowDialogCount++
                    showFailureMessage(
                        MessageConfig.Builder()
                            .title(getString(R.string.title_battery_low))
                            .message(getString(R.string.msg_battery_low_desc))
                            .positiveLabel(getString(R.string.title_ok))
                            .onPositiveClick {
                                exitAmountPageOnCancel()
                            }
                    )
                }
            }
        }

    }

    private fun initViews() {
        btn_digit_one.setOnClickListener(this)
        btn_digit_two.setOnClickListener(this)
        btn_digit_three.setOnClickListener(this)
        btn_digit_four.setOnClickListener(this)
        btn_digit_five.setOnClickListener(this)
        btn_digit_six.setOnClickListener(this)
        btn_digit_seven.setOnClickListener(this)
        btn_digit_eight.setOnClickListener(this)
        btn_digit_nine.setOnClickListener(this)
        btn_digit_zero.setOnClickListener(this)
        btn_digit_double_zero.setOnClickListener(this)
        img_btn_clear.setOnClickListener(this)
        btn_confirm.setOnClickListener(this)
        btn_cancel.setOnClickListener(this)
        img_btn_clear.setOnLongClickListener(this)
        iv_back.setOnClickListener(this)
        ll_no_connection.setOnClickListener(this)
        tv_currency_new.loadCurrency()

        rl_actions.background = null
        ll_actions.background =
            ContextCompat.getDrawable(this, R.drawable.bg_round_corner_numpad)
        ll_amount_old.visibility = View.GONE
        ll_amount_new.visibility = View.VISIBLE

        tl_number_pad.forEach {
            (it as TableRow).children.forEach {
                (it as MaterialButton).cornerRadius = 50
            }
        }

        btn_digit_double_zero.text = getString(R.string.digit_dot)
    }

    private fun initObservers() {
        viewModel.amount.observe(this) {
            if (it.length <= 6) {
                tv_amount_new.textSize = 56f
            } else if (it.length in 6..8) {
                tv_amount_new.textSize = 44f
            } else {
                tv_amount_new.textSize = 34f
            }

            if (it.isNotEmpty()){
                tv_amount_new.text = it
            }else{
                tv_amount_new.text = "0"
            }
        }

        viewModel.isLoading.observe(
            this,
            Observer {
                if (it) {
                    btn_confirm.isEnabled = false
                    btn_confirm.backgroundTintList =
                        ContextCompat.getColorStateList(this, R.color.finpos_purple_light)
                    progress_bar.visibility = View.VISIBLE
                } else {
                    btn_confirm.isEnabled = true
                    btn_confirm.backgroundTintList =
                        ContextCompat.getColorStateList(this, R.color.finpos_purple_dark)
                    progress_bar.visibility = View.INVISIBLE
                }
            }
        )

        viewModel.validAmount.observe(this, Observer {
            if (it) {
                onAmountConfirmed(
                    tv_amount_new.text.toString()
                )
            } else {
                onAmountLimitExceeded()
            }
        })

        /*viewModel.internetConnected.observe(
            this,
            Observer {
                val checkTime: Long
                if (it && (batteryLevel > 10 || isCharging)) {
                    ll_no_connection.visibility = View.GONE
                    progress_bar.visibility = View.INVISIBLE
                    btn_confirm.backgroundTintList =
                        ContextCompat.getColorStateList(this, R.color.finpos_purple_dark)
                    btn_confirm.isEnabled = true
                    checkTime = 2000
                } else {
                    if (batteryLevel <= 10 && isCharging) {
                        ll_no_connection.visibility = View.VISIBLE
                    } else if (batteryLevel > 10) {
                        ll_no_connection.visibility = View.VISIBLE
                    } else {
                        ll_no_connection.visibility = View.GONE
                    }
                    btn_confirm.isEnabled = false
                    btn_confirm.backgroundTintList =
                        ContextCompat.getColorStateList(this, R.color.finpos_purple_light)
                    checkTime = 5000
                }
                Handler().postDelayed({ viewModel.checkInternetConnection() }, checkTime)
            }
        )*/
    }

    private fun onAmountLimitExceeded() {
        val builder = MessageConfig.Builder()
            .title(getString(R.string.title_confirm_amount))
            .message(
                viewModel.maxAmountMessage.value.plus("\n").plus(
                    getString(R.string.error_msg_max_amount_exceeded)
                )
            )
            .positiveLabel(getString(R.string.title_ok))
            .onPositiveClick { enableOrDisableButtons(true) }
        showFailureMessage(builder)
    }

    override fun onClick(view: View?) {
        return when (view!!.id) {
            R.id.img_btn_clear -> manageForClearButtonClicked()

            R.id.btn_cancel, R.id.iv_back -> exitAmountPageOnCancel()

            R.id.btn_confirm -> {
                manageForConfirmButtonClicked(
                    tv_amount_new.text.toString()
                )
            }

            R.id.btn_digit_double_zero -> {
                getViewModel().onNumericButtonPressedForNewLayout(
                    (view as Button).text.toString(),
                    viewModel.amountLengthLimit
                )
            }

//            R.id.ll_no_connection -> {
//                getViewModel().checkInternetConnection()
//            }

            else -> {
                getViewModel().onNumericButtonPressedForNewLayout(
                    (view as Button).text.toString(),
                    viewModel.amountLengthLimit
                )
            }
        }
    }

    override fun onLongClick(view: View?): Boolean {
        when (view!!.id) {
            R.id.img_btn_clear -> manageForClearButtonLongClicked()
        }
        return true
    }

    private fun manageForClearButtonClicked() {
        viewModel.onClearButtonClickedForNewLayout()
    }

    private fun manageForConfirmButtonClicked(amount: String) {
        enableOrDisableButtons(false)
        if (amount.isNotEmpty()) {
            if (amount.toDouble() > 0) {
                //val amountLimit = intent.getDoubleExtra(EXTRA_LIMIT, 0.0)
                val amountLimit = extraLimit.toDouble()
                getViewModel().checkMaximumAmountAllowed(amount.toDouble(), amountLimit)
            } else {
                enableButtonsAndShowToast(getString(R.string.error_msg_amount_zero))
            }
        } else {
            enableButtonsAndShowToast(getString(R.string.error_msg_amount_empty))
        }

    }

    private fun manageForClearButtonLongClicked() {
        viewModel.onClearButtonLongClicked()
    }

    private fun onAmountConfirmed(amount: String) {
        logger.log("Confirmed Amount ::: ".plus(amount))
        val intent = Intent()
        intent.putExtra(
            getString(R.string.intent_confirmed_amount),
            getViewModel().prepareAmountToSendDuringConfirmation(amount.toDouble())
        )
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun exitAmountPageOnCancel() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    override fun onBackPressed() {
    }

    private fun onCancelledClicked() {
        enableOrDisableButtons(true)
    }

    private fun enableOrDisableButtons(doEnable: Boolean) {
        btn_digit_one.isEnabled = doEnable
        btn_digit_two.isEnabled = doEnable
        btn_digit_three.isEnabled = doEnable
        btn_digit_four.isEnabled = doEnable
        btn_digit_five.isEnabled = doEnable
        btn_digit_six.isEnabled = doEnable
        btn_digit_seven.isEnabled = doEnable
        btn_digit_eight.isEnabled = doEnable
        btn_digit_nine.isEnabled = doEnable
        btn_digit_zero.isEnabled = doEnable
        btn_digit_double_zero.isEnabled = doEnable
        img_btn_clear.isEnabled = doEnable
    }

    private fun enableButtonsAndShowToast(message: String) {
        enableOrDisableButtons(true)
        showToast(message)
    }

    override fun onResume() {
        super.onResume()
        logger.debug(":::OnResume Called:::")
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED)
        try {
            registerReceiver(batteryBroadcast, intentFilter)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onPause() {
        super.onPause()
        logger.debug(":::OnPause Called:::")
        try {
            unregisterReceiver(batteryBroadcast)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun getLayout() = R.layout.activity_amount

    override fun getViewModel() = ViewModelProviders.of(this)[NewAmountViewModel::class.java]

    override fun getBindingVariable(): Int = BR.newAmountViewModel
}