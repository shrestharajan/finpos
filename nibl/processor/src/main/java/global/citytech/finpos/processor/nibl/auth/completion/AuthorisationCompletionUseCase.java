//package global.citytech.finpos.processor.nibl.auth.completion;
//
//import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
//import global.citytech.finpos.nibl.iso8583.ProcessingCode;
//import global.citytech.finpos.nibl.iso8583.requestsender.authcompletion.AuthorisationCompletionRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
//import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
//import global.citytech.finpos.processor.nibl.transaction.TransactionUseCase;
//import global.citytech.finpos.processor.nibl.transaction.receipt.NiblTransactionReceiptPrintHandler;
//import global.citytech.finpos.processor.nibl.transaction.receipt.ReversedTransactionReceiptHandler;
//import global.citytech.finpos.processor.nibl.transaction.reversal.ReversalRequestModel;
//import global.citytech.finpos.processor.nibl.transaction.reversal.ReversalUseCase;
//import global.citytech.finposframework.exceptions.FinPosException;
//import global.citytech.finposframework.exceptions.PosError;
//import global.citytech.finposframework.exceptions.PosException;
//import global.citytech.finposframework.hardware.common.Result;
//import global.citytech.finposframework.hardware.io.cards.CardDetails;
//import global.citytech.finposframework.hardware.io.cards.CardType;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
//import global.citytech.finposframework.hardware.io.led.LedAction;
//import global.citytech.finposframework.hardware.io.led.LedLight;
//import global.citytech.finposframework.hardware.io.led.LedRequest;
//import global.citytech.finposframework.hardware.io.led.LedService;
//import global.citytech.finposframework.iso8583.DataElement;
//import global.citytech.finposframework.iso8583.IsoMessageResponse;
//import global.citytech.finposframework.iso8583.RequestContext;
//import global.citytech.finposframework.notifier.Notifier;
//import global.citytech.finposframework.repositories.ApplicationRepository;
//import global.citytech.finposframework.repositories.TerminalRepository;
//import global.citytech.finposframework.repositories.TransactionRepository;
//import global.citytech.finposframework.supports.UseCase;
//import global.citytech.finposframework.switches.PosEntryMode;
//import global.citytech.finposframework.switches.auth.completion.AuthorisationCompletionRequester;
//import global.citytech.finposframework.usecases.MagneticSwipe;
//import global.citytech.finposframework.usecases.PICCWave;
//import global.citytech.finposframework.usecases.TransactionType;
//import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
//import global.citytech.finposframework.usecases.transaction.TransactionLog;
//import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
//import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
//import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
//import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
//import global.citytech.finposframework.utility.DateUtils;
//import global.citytech.finposframework.utility.HelperUtils;
//import global.citytech.finposframework.utility.IsoMessageUtils;
//import global.citytech.finposframework.utility.JsonUtils;
//import global.citytech.finposframework.utility.StringUtils;
//
///**
// * Created by Unique Shakya on 9/29/2020.
// */
//public class AuthorisationCompletionUseCase extends TransactionUseCase implements UseCase<AuthorisationCompletionRequestModel, AuthorisationCompletionResponseModel>,
//        AuthorisationCompletionRequester<AuthorisationCompletionRequestModel, AuthorisationCompletionResponseModel> {
//
//    private global.citytech.finpos.nibl.iso8583.requestsender.authcompletion.AuthorisationCompletionRequest authorisationCompletionRequest;
//    private TransactionLog transactionLog;
//
//    public AuthorisationCompletionUseCase(TerminalRepository terminalRepository, Notifier notifier) {
//        super(terminalRepository, notifier);
//    }
//
//    @Override
//    public AuthorisationCompletionResponseModel execute(AuthorisationCompletionRequestModel request) {
//        try {
//            logger.log("::: REACHED AUTHORISATION COMPLETION USE CASE :::");
//            AuthorisationCompletionRequest authorisationCompletionRequest = request.getAuthorisationCompletionRequest();
//            String stan = this.terminalRepository.getSystemTraceAuditNumber();
//            String invoiceNumber = request.getAuthorisationCompletionRequest().getOriginalInvoiceNumber();
//            String batchNumber = this.terminalRepository.getReconciliationBatchNumber();
//            this.transactionRepository = request.getTransactionRepository();
//            transactionLog = request.getTransactionRepository()
//                    .getTransactionLogWithInvoiceNumberWithoutBatchNumber(invoiceNumber);
//            if (transactionLog == null)
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_FOUND);
//            if (!this.isDeviceReady(request.getDeviceController(), authorisationCompletionRequest.getTransactionType()))
//                throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
//            this.initTransaction(request.getApplicationRepository(), authorisationCompletionRequest);
//            ReadCardResponse readCardResponse = this.prepareReadCardResponseFromTransactionLog(
//                    authorisationCompletionRequest, transactionLog);
//            /*this.identifyCard(request.getTransactionRepository(), request.getApplicationRepository(),
//                    authorisationCompletionRequest, readCardResponse);*/ // TODO add after card scheme retriever is implemented
//            if (!this.isTransactionAuthorizationSuccess(request.getTransactionAuthenticator(),
//                    request.getApplicationRepository(), authorisationCompletionRequest.getTransactionType(),
//                    readCardResponse))
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
//            logger.debug("::: READ CARD RESPONSE ::: " + readCardResponse.toString());
//            this.notifier.notify(Notifier.EventType.TRANSMITTING_REQUEST, "Transmitting Request..");
//            try {
//                IsoMessageResponse response = this.sendIsoMessage(stan, authorisationCompletionRequest,
//                        transactionLog, readCardResponse, request);
//                this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait..");
//                InvalidResponseHandlerResponse invalidResponseHandlerResponse = this.checkForInvalidResponseFromHost(response);
//                if (invalidResponseHandlerResponse.isInvalid())
//                    return this.handleInvalidResponseFromHost(authorisationCompletionRequest,
//                            invalidResponseHandlerResponse, batchNumber, stan, invoiceNumber,
//                            readCardResponse, request);
//                boolean isAuthCompletionApproved = this.isTransactionApprovedByActionCode(response);
//                this.notifyTransactionCompletion(stan, batchNumber, invoiceNumber, isAuthCompletionApproved,
//                        authorisationCompletionRequest, request.getTransactionRepository(), response,
//                        readCardResponse, request.getApplicationRepository(), request.getLedService(), authorisationCompletionRequest);
//                boolean shouldPrint = this.printReceipt(batchNumber, stan, invoiceNumber, request.getPrinterService(),
//                        authorisationCompletionRequest, request.getTransactionRepository(),
//                        readCardResponse, response, request.getApplicationRepository());
//                return this.prepareResponse(stan, response, isAuthCompletionApproved, request.getApplicationRepository(),
//                        shouldPrint);
//            } catch (FinPosException e) {
//                e.printStackTrace();
//                if (this.isResponseNotReceivedException(e)) {
//                    return this.handleTimeoutTransaction(authorisationCompletionRequest, stan,
//                            batchNumber, invoiceNumber, readCardResponse, request);
//                } else if (e.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT) {
//                    logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED UNABLE TO CONNECT");
//                    transactionRepository.removeAutoReversal(this.autoReversal);
//                    throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
//                } else {
//                    throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//                }
//            }
//        } catch (PosException e) {
//            e.printStackTrace();
//            throw e;
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        } finally {
//            request.getReadCardService().cleanUp();
//            request.getLedService().doTurnLedWith(new LedRequest(LedLight.ALL, LedAction.OFF));
//            PICCWave.getInstance().clear();
//            MagneticSwipe.getInstance().clear();
//        }
//    }
//
//    private AuthorisationCompletionResponseModel handleTimeoutTransaction(TransactionRequest authorisationCompletionRequest,
//                                                                          String stan, String batchNumber,
//                                                                          String invoiceNumber,
//                                                                          ReadCardResponse readCardResponse,
//                                                                          AuthorisationCompletionRequestModel authorisationCompletionRequestModel) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, "TIMEOUT");
//        this.setUnableToGoOnlineResultToCard(readCardResponse, authorisationCompletionRequestModel.getReadCardService());
//        this.performReversal(stan, readCardResponse, authorisationCompletionRequestModel);
//        boolean shouldPrint = !this.approvePrintOnly(authorisationCompletionRequestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(authorisationCompletionRequest,
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.TIMEOUT, "Timeout"));
//            new NiblTransactionReceiptPrintHandler(authorisationCompletionRequestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            authorisationCompletionRequestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return AuthorisationCompletionResponseModel.Builder.newInstance()
//                .withMessage("TIMEOUT")
//                .withIsApproved(false)
//                .withShouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private AuthorisationCompletionResponseModel handleInvalidResponseFromHost(
//            AuthorisationCompletionRequest authorisationCompletionRequest,
//            InvalidResponseHandlerResponse invalidResponseHandlerResponse,
//            String batchNumber, String stan, String invoiceNumber, ReadCardResponse readCardResponse,
//            AuthorisationCompletionRequestModel authorisationCompletionRequestModel) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, invalidResponseHandlerResponse.getMessage());
//        this.performReversal(stan, readCardResponse, authorisationCompletionRequestModel);
//        boolean shouldPrint = !this.approvePrintOnly(authorisationCompletionRequestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(authorisationCompletionRequest,
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.INVALID_RESPONSE,
//                            invalidResponseHandlerResponse.getMessage()));
//            new NiblTransactionReceiptPrintHandler(authorisationCompletionRequestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            authorisationCompletionRequestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return AuthorisationCompletionResponseModel.Builder.newInstance()
//                .withIsApproved(false)
//                .withMessage(invalidResponseHandlerResponse.getMessage())
//                .withShouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private AuthorisationCompletionResponseModel prepareResponse(String stan, IsoMessageResponse response,
//                                                                 boolean isAuthCompletionApproved,
//                                                                 ApplicationRepository applicationRepository,
//                                                                 boolean shouldPrint) {
//        AuthorisationCompletionResponseModel.Builder builder = AuthorisationCompletionResponseModel.Builder
//                .newInstance();
//        builder.withIsApproved(isAuthCompletionApproved);
//        builder.withDebugRequestMessage(response.getDebugRequestString());
//        builder.withDebugResponseMessage(response.getDebugResponseString());
//        builder.withStan(stan);
//        builder.withMessage(this.retrieveMessage(response, isAuthCompletionApproved, applicationRepository));
//        builder.withShouldPrintCustomerCopy(shouldPrint);
//        return builder.build();
//    }
//
//    private IsoMessageResponse sendIsoMessage(String stan, AuthorisationCompletionRequest authorisationCompletionRequest,
//                                              TransactionLog transactionLog, ReadCardResponse readCardResponse,
//                                              AuthorisationCompletionRequestModel request) {
//        RequestContext requestContext = this.prepareTemplateRequestContext(stan);
//        requestContext.setRequest(this.prepareCoreAuthorisationCompletionRequest(authorisationCompletionRequest,
//                transactionLog, readCardResponse));
//        this.autoReversal = new AutoReversal(TransactionType.AUTH_COMPLETION,
//                JsonUtils.toJsonObj(prepareReversalRequest(stan, readCardResponse, request)),
//                0,
//                AutoReversal.Status.ACTIVE,
//                StringUtils.dateTimeStamp());
//        this.transactionRepository.saveAutoReversal(this.autoReversal);
//        AuthorisationCompletionRequestSender sender = new AuthorisationCompletionRequestSender(
//                new NIBLSpecInfo(), requestContext);
//        IsoMessageResponse isoMessageResponse = sender.send();
//        this.transactionRepository.removeAutoReversal(this.autoReversal);
//        return isoMessageResponse;
//    }
//
//    private global.citytech.finpos.nibl.iso8583.requestsender.authcompletion.AuthorisationCompletionRequest
//    prepareCoreAuthorisationCompletionRequest(AuthorisationCompletionRequest authorisationCompletionRequest,
//                                              TransactionLog originalTransactionLog,
//                                              ReadCardResponse readCardResponse) {
//        this.authorisationCompletionRequest
//                = global.citytech.finpos.nibl.iso8583.requestsender.authcompletion.AuthorisationCompletionRequest.Builder
//                .newInstance()
//                .withTransactionAmount(authorisationCompletionRequest.getAmount())
//                .withPosEntryMode(PosEntryMode.MANUAL)
//                .withPosConditionCode("05") // TODO change
//                .withPan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
//                .withOriginalRrn(originalTransactionLog.getRrn())
//                .withOriginalResponseCode("00")
//                .withOriginalInvoiceNumber(originalTransactionLog.getInvoiceNumber())
//                .withOriginalApprovalCode(originalTransactionLog.getAuthCode())
//                .withLocalTransactionTime(HelperUtils.getDefaultLocaleTimeHhMmSs())
//                .withLocalTransactionDate(HelperUtils.getDefaultLocaleDateWithoutYear())
//                .withExpirationDate(readCardResponse.getCardDetails().getExpiryDate())
//                .build();
//        return this.authorisationCompletionRequest;
//    }
//
//    private ReadCardResponse prepareReadCardResponseFromTransactionLog(AuthorisationCompletionRequest authorisationCompletionRequest,
//                                                                       TransactionLog transactionLog) {
//        CardDetails cardDetails = new CardDetails();
//        cardDetails.setAmount(authorisationCompletionRequest.getAmount());
//        cardDetails.setCashBackAmount(authorisationCompletionRequest.getAdditionalAmount());
//        cardDetails.setPrimaryAccountNumber(transactionLog.getReadCardResponse().getCardDetails().getPrimaryAccountNumber());
//        cardDetails.setExpiryDate(transactionLog.getReadCardResponse().getCardDetails().getExpiryDate());
//        cardDetails.setCardType(CardType.MANUAL);
//        cardDetails.setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
//        cardDetails.setCardScheme(transactionLog.getReadCardResponse().getCardDetails().getCardScheme());
//        return new ReadCardResponse(cardDetails, Result.SUCCESS, "SUCCESS");
//    }
//
//    private void performReversal(
//            String stan,
//            ReadCardResponse readCardResponse,
//            AuthorisationCompletionRequestModel authorisationCompletionRequestModel
//    ) {
//        ReversalUseCase reversalUseCase = new ReversalUseCase(
//                this.prepareReversalRequest(stan, readCardResponse, authorisationCompletionRequestModel),
//                this.terminalRepository,
//                this.notifier
//        );
//        try {
//            reversalUseCase.execute(
//                    prepareReversalRequestModel(
//                            authorisationCompletionRequestModel,
//                            readCardResponse
//                    )
//            );
//            this.transactionRepository.removeAutoReversal(this.autoReversal);
//        } catch (FinPosException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private NiblReversalRequest prepareReversalRequest(
//            String stan,
//            ReadCardResponse readCardResponse,
//            AuthorisationCompletionRequestModel authorisationCompletionRequestModel
//    ) {
//        NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
//        niblReversalRequest.setProcessingCode(ProcessingCode.PRE_AUTHORIZATION_COMPLETION.getCode());
//        niblReversalRequest.setAmount(
//                authorisationCompletionRequestModel.getAuthorisationCompletionRequest().getAmount()
//        );
//        niblReversalRequest.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        niblReversalRequest.setExpiryDate(readCardResponse.getCardDetails().getExpiryDate());
//        niblReversalRequest.setPosEntryMode(this.authorisationCompletionRequest.getPosEntryMode());
//        niblReversalRequest.setPosConditionCode(this.authorisationCompletionRequest.getPosConditionCode());
//        niblReversalRequest.setTrack2Data(readCardResponse.getCardDetails().getTrackTwoData());
//        niblReversalRequest.setPinBlock(readCardResponse.getCardDetails().getPinBlock().getPin());
//        niblReversalRequest.setInvoiceNumber(this.authorisationCompletionRequest.getOriginalInvoiceNumber());
//        niblReversalRequest.setStan(stan);
//        return niblReversalRequest;
//    }
//
//    private ReversalRequestModel prepareReversalRequestModel(
//            AuthorisationCompletionRequestModel authorisationCompletionRequestModel,
//            ReadCardResponse readCardResponse
//    ) {
//        return ReversalRequestModel.Builder.newInstance()
//                .withTransactionRequest(
//                        authorisationCompletionRequestModel.getAuthorisationCompletionRequest()
//                )
//                .withTransactionRepository(
//                        authorisationCompletionRequestModel.getTransactionRepository()
//                )
//                .withPrinterService(
//                        authorisationCompletionRequestModel.getPrinterService()
//                )
//                .withApplicationRepository(
//                        authorisationCompletionRequestModel.getApplicationRepository()
//                )
//                .withReadCardResponse(readCardResponse)
//                .build();
//    }
//
//    private void notifyTransactionCompletion(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean authCompletionApproved,
//            AuthorisationCompletionRequest authorisationCompletionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository,
//            LedService ledService,
//            TransactionRequest transactionRequest
//    ) {
//        this.updateTransactionLog(
//                stan,
//                batchNumber,
//                invoiceNumber,
//                authCompletionApproved,
//                authorisationCompletionRequest,
//                transactionRepository,
//                isoMessageResponse,
//                readCardResponse,
//                applicationRepository,
//                transactionRequest
//        );
//        this.updateTransactionIds(false);
//        this.notifyTransactionStatus(
//                authCompletionApproved,
//                isoMessageResponse,
//                applicationRepository,
//                ledService,
//                readCardResponse.getCardDetails().getCardType() == CardType.PICC
//        );
//    }
//
//    private void updateTransactionLog(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean authCompletionApproved,
//            AuthorisationCompletionRequest authorisationCompletionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository,
//            TransactionRequest transactionRequest
//    ) {
//        this.logger.log("::: NIBL ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
//        this.logger.log("::: NIBL ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
//
//        String transactionDate = IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.LOCAL_DATE);
//        if (StringUtils.isEmpty(transactionDate))
//            transactionDate = IsoMessageUtils.retrieveFromRequestDataElementAsString(isoMessageResponse, DataElement.LOCAL_DATE);
//
//        String transactionTime = IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.LOCAL_TIME);
//        if (StringUtils.isEmpty(transactionTime))
//            transactionTime = IsoMessageUtils.retrieveFromRequestDataElementAsString(isoMessageResponse, DataElement.LOCAL_TIME);
//
//        TransactionLog authCompletionTransactionLog = TransactionLog.Builder.newInstance()
//                .withStan(stan)
//                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
//                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
//                .withInvoiceNumber(invoiceNumber)
//                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
//                .withAuthCode(IsoMessageUtils.retrieveFromRequestDataElementAsString(
//                        isoMessageResponse,
//                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
//                .withTransactionType(TransactionType.AUTH_COMPLETION)
//                .withOriginalTransactionType(TransactionType.PRE_AUTH)
//                .withTransactionAmount(authorisationCompletionRequest.getAmount())
//                .withOriginalTransactionAmount(authorisationCompletionRequest.getAmount())
//                .withTransactionDate(transactionDate)
//                .withTransactionTime(transactionTime)
//                .withTransactionStatus(authCompletionApproved ? "APPROVED" : "DECLINED")
//                .withPosEntryMode(this.authorisationCompletionRequest.getPosEntryMode())
//                .withOriginalPosEntryMode(this.transactionLog.getPosEntryMode())
//                .withPosConditionCode(this.authorisationCompletionRequest.getPosConditionCode())
//                .withOriginalPosConditionCode(this.transactionLog.getPosConditionCode())
//                .withReconcileStatus("")
//                .withReconcileTime("")
//                .withReconcileDate("")
//                .withReconcileBatchNo(batchNumber)
//                .withReadCardResponse(readCardResponse)
//                .withAuthorizationCompleted(true)
//                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
//                .withOriginalTransactionReferenceNumber(transactionLog.getAuthCode())
//                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse,
//                        applicationRepository, batchNumber, stan, invoiceNumber))
//                .withTransactionVoided(false)
//                .withTipAdjusted(false)
//                .build();
//        transactionRepository.updateTransactionLog(authCompletionTransactionLog);
//        transactionRepository.updateAuthorizationCompletedStatusByAuthCode(authCompletionApproved, TransactionType.PRE_AUTH,
//                transactionLog.getAuthCode());
//    }
//
//    @Override
//    protected String getClassName() {
//        return AuthorisationCompletionUseCase.class.getName();
//    }
//}
