package global.citytech.finposframework.hardware.system

import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.DeviceService

/**
 * Created by Rishav Chudal on 6/7/20.
 */
interface SystemService: DeviceService {
    fun enableADB(): DeviceResponse

    fun disableADB(): DeviceResponse

    fun doFactoryReset(): DeviceResponse

    fun doSystemUpdate(firmwareFilePath: String): DeviceResponse

    fun enableOTG(): DeviceResponse

    fun disableOTG(): DeviceResponse

    fun enableMicroSD(): DeviceResponse

    fun disableMicroSD(): DeviceResponse

    fun enableHomeButton(): DeviceResponse

    fun disableHomeButton(): DeviceResponse

    fun enablePowerButton(): DeviceResponse

    fun disablePowerButton(): DeviceResponse

    fun enableRecentButton(): DeviceResponse

    fun disableRecentButton(): DeviceResponse

    fun enableBluetooth(): DeviceResponse

    fun disableBluetooth(): DeviceResponse

    fun enableWifi(): DeviceResponse

    fun disableWifi(): DeviceResponse
}