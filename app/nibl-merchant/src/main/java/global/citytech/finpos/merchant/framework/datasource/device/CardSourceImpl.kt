package global.citytech.finpos.merchant.framework.datasource.device

import android.content.Context
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.device.CardSource
import global.citytech.finpos.merchant.service.AppDeviceFactory
import global.citytech.finposframework.hardware.DeviceServiceType
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService
import global.citytech.finposframework.repositories.TerminalRepository

class CardSourceImpl(private val instance: Context?) : CardSource {

    private var service: ReadCardService = AppDeviceFactory.getCore(
        instance,
        DeviceServiceType.CARD
    ) as ReadCardService

    override fun readCardDetails(readCardRequest: ReadCardRequest): ReadCardResponse {
        readCardRequest.packageName = NiblMerchant.INSTANCE.packageName
        return service.readCardDetails(readCardRequest)
    }

    override fun readEmv(readCardRequest: ReadCardRequest): ReadCardResponse {
        readCardRequest.packageName = NiblMerchant.INSTANCE.packageName
        return service.readEmv(readCardRequest)
    }

    override fun processEMV(readCardRequest: ReadCardRequest): ReadCardResponse {
        readCardRequest.packageName = NiblMerchant.INSTANCE.packageName
        return service.processEMV(readCardRequest)
    }

    override fun setOnlineResult(
        responseCode: Int,
        onlineResult: ReadCardService.OnlineResult,
        responseField55Data: String,
        onlineAfterTcAac: Boolean
    ): ReadCardResponse {
        return service.setOnlineResult(responseCode, onlineResult, responseField55Data, onlineAfterTcAac)
    }

    override fun getIccCardStatus(): ReadCardResponse {
        return service.getIccCardStatus()
    }

    override fun cleanUp() {
        service.cleanUp()
    }
}