package global.citytech.finpos.merchant.presentation.voidsale

import global.citytech.common.extensions.isAlphaNumeric
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.presentation.model.TransactionItem
import global.citytech.finposframework.utility.HelperUtils


/**
 * Created by Saurav Ghimire on 2/15/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
object VoidSaleValidator {
    fun validateData(invoice: String) = invoice.isAlphaNumeric() && invoice.length == 6
    fun getTransactionPresenceValidatorObservable(localDataUseCase: LocalDataUseCase, invoiceNumber: String) =
        localDataUseCase.isTransactionPresent(invoiceNumber)
    fun getTransactionValidatorObservable(localDataUseCase: LocalDataUseCase, invoiceNumber: String) =
        localDataUseCase.getTransactionByInvoiceNumber(invoiceNumber, false)
}