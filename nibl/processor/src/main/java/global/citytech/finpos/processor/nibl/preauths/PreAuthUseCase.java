//package global.citytech.finpos.processor.nibl.preauths;
//
//import java.util.concurrent.CountDownLatch;
//
//import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
//import global.citytech.finpos.nibl.iso8583.ProcessingCode;
//import global.citytech.finpos.nibl.iso8583.requestsender.preauth.IccPreAuthRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.preauth.MagPreAuthRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.preauth.PICCPreAuthRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.preauth.PreAuthRequest;
//import global.citytech.finpos.nibl.iso8583.requestsender.preauth.PreAuthRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
//import global.citytech.finpos.processor.nibl.reversal.ReversalRequestModel;
//import global.citytech.finpos.processor.nibl.reversal.ReversalUseCase;
//import global.citytech.finpos.processor.nibl.transaction.NiblTransactionReceiptPrintHandler;
//import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
//import global.citytech.finpos.processor.nibl.transaction.ReversedTransactionReceiptHandler;
//import global.citytech.finpos.processor.nibl.transaction.TransactionUseCase;
//import global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor;
//import global.citytech.finpos.processor.nibl.transaction.card.MagneticCardProcessor;
//import global.citytech.finpos.processor.nibl.transaction.card.PICCCardProcessor;
//import global.citytech.finposframework.exceptions.FinPosException;
//import global.citytech.finposframework.exceptions.PosError;
//import global.citytech.finposframework.exceptions.PosException;
//import global.citytech.finposframework.hardware.io.cards.CardDetails;
//import global.citytech.finposframework.hardware.io.cards.CardType;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
//import global.citytech.finposframework.hardware.io.led.LedAction;
//import global.citytech.finposframework.hardware.io.led.LedLight;
//import global.citytech.finposframework.hardware.io.led.LedRequest;
//import global.citytech.finposframework.hardware.io.led.LedService;
//import global.citytech.finposframework.iso8583.DataElement;
//import global.citytech.finposframework.iso8583.IsoMessageResponse;
//import global.citytech.finposframework.iso8583.RequestContext;
//import global.citytech.finposframework.listeners.CardConfirmationListener;
//import global.citytech.finposframework.notifier.Notifier;
//import global.citytech.finposframework.repositories.ApplicationRepository;
//import global.citytech.finposframework.repositories.TerminalRepository;
//import global.citytech.finposframework.repositories.TransactionRepository;
//import global.citytech.finposframework.supports.UseCase;
//import global.citytech.finposframework.switches.preauths.PreAuthRequester;
//import global.citytech.finposframework.usecases.MagneticSwipe;
//import global.citytech.finposframework.usecases.PICCWave;
//import global.citytech.finposframework.usecases.TransactionType;
//import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
//import global.citytech.finposframework.usecases.transaction.TransactionLog;
//import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
//import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
//import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
//import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
//import global.citytech.finposframework.utility.HelperUtils;
//import global.citytech.finposframework.utility.IsoMessageUtils;
//import global.citytech.finposframework.utility.JsonUtils;
//import global.citytech.finposframework.utility.StringUtils;
//
//public class PreAuthUseCase extends TransactionUseCase implements UseCase<PreAuthRequestModel, PreAuthResponseModel>,
//        PreAuthRequester<PreAuthRequestModel, PreAuthResponseModel> {
//
//    static CountDownLatch countDownLatch;
//    PreAuthRequestModel preAuthRequestModel;
//    ReadCardResponse readCardResponse;
//    String stan;
//    String invoiceNumber;
//    TransactionRequest transactionRequest;
//    String batchNumber;
//    private global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest;
//    private PreAuthRequest isoPreAuthRequest;
//
//    public PreAuthUseCase(TerminalRepository repository, Notifier notifier) {
//        super(repository, notifier);
//    }
//
//    @Override
//    protected String getClassName() {
//        return PreAuthUseCase.class.getName();
//    }
//
//    @Override
//    public PreAuthResponseModel execute(PreAuthRequestModel requestModel) {
//        this.preAuthRequestModel = requestModel;
//        logger.log("::: PRE AUTH USE CASE :::");
//        if (!isDeviceReady(requestModel.getDeviceController(),
//                requestModel.getTransactionRequest().getTransactionType())) {
//            throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
//        }
//
//        return initiateTransaction(requestModel);
//    }
//
//    private PreAuthRequest preparePreAuthRequest(String invoiceNumber, ReadCardResponse readCardResponse) {
//        this.isoPreAuthRequest = new PreAuthRequest();
//        this.isoPreAuthRequest.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        this.isoPreAuthRequest.setTransactionAmount(readCardResponse.getCardDetails().getAmount());
//        this.isoPreAuthRequest.setPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()));
//        this.isoPreAuthRequest.setPosConditionCode("00"); //TODO Change later
//        this.isoPreAuthRequest.setTrack2Data(readCardResponse.getCardDetails().getTrackTwoData());
//        if (readCardResponse.getCardDetails().getPinBlock().getOfflinePin() ||
//                StringUtils.isEmpty(readCardResponse.getCardDetails().getPinBlock().getPin()) ||
//                readCardResponse.getCardDetails().getPinBlock().getPin().equalsIgnoreCase("offline"))
//            isoPreAuthRequest.setPinBlock("");
//        else
//            isoPreAuthRequest.setPinBlock(readCardResponse.getCardDetails().getPinBlock().getPin());
//        this.isoPreAuthRequest.setEmvData(readCardResponse.getCardDetails().getIccDataBlock());
//        this.isoPreAuthRequest.setExpireDate(readCardResponse.getCardDetails().getExpiryDate());
//        this.isoPreAuthRequest.setCardSequenceNumber(readCardResponse.getCardDetails().getPrimaryAccountNumberSerialNumber());
//        this.isoPreAuthRequest.setInvoiceNumber(invoiceNumber);
//        this.isoPreAuthRequest.setLocalTime(HelperUtils.getDefaultLocaleTimeHhMmSs());
//        this.isoPreAuthRequest.setLocalDate(HelperUtils.getDefaultLocaleDateWithoutYear());
//        this.isoPreAuthRequest.setFallbackIccToMag(readCardResponse.getFallbackIccToMag());
//        return this.isoPreAuthRequest;
//    }
//
//    private PreAuthResponseModel initiateTransaction(PreAuthRequestModel requestModel) {
//        try {
//            logger.log(":: REACHED PRE AUTH USE CASE ::");
//            stan = this.terminalRepository.getSystemTraceAuditNumber();
//            invoiceNumber = this.terminalRepository.getInvoiceNumber();
//            batchNumber = this.terminalRepository.getReconciliationBatchNumber();
//            transactionRequest = requestModel.getTransactionRequest();
//            this.transactionRepository = requestModel.getTransactionRepository();
//            this.validateRequest(transactionRequest, requestModel.getApplicationRepository());
//            ReadCardRequest readCardRequest = prepareReadCardRequest(stan, requestModel.getApplicationRepository(),
//                    transactionRequest);
//            this.notifier.notify(Notifier.EventType.STARTING_READ_CARD);
//            ReadCardResponse readCardResponse = this.detectCard(readCardRequest, requestModel.getReadCardService(),
//                    requestModel.getLedService());
//            this.notifier.notify(Notifier.EventType.READING_CARD, "Reading Card..");
//            CardType detectedCardType = readCardResponse.getCardDetails().getCardType();
//            this.logger.debug("::: PRE AUTH USE CASE ::: CARD TYPE === " + detectedCardType);
//            this.logger.debug("::: CARD SCHEME AFTER VALIDATE READ CARD RESPONSE ::: " + readCardResponse.getCardDetails().getCardScheme());
//            boolean isCardConfirmationRequired;
//            switch (detectedCardType) {
//                case MAG:
//                    isCardConfirmationRequired = true;
//                    this.cardProcessor = new MagneticCardProcessor(
//                            requestModel.getTransactionRepository(),
//                            requestModel.getTransactionAuthenticator(),
//                            requestModel.getApplicationRepository(),
//                            requestModel.getReadCardService(),
//                            this.notifier,
//                            this.terminalRepository
//                    );
//                    break;
//                case ICC:
//                    isCardConfirmationRequired = true;
//                    this.cardProcessor = new IccCardProcessor(
//                            requestModel.getReadCardService(),
//                            requestModel.getTransactionRepository(),
//                            requestModel.getApplicationRepository(),
//                            requestModel.getTransactionAuthenticator(),
//                            this.notifier,
//                            this.terminalRepository
//                    );
//                    break;
//                case PICC:
//                    isCardConfirmationRequired = false;
//                    this.cardProcessor = new PICCCardProcessor(
//                            requestModel.getTransactionRepository(),
//                            requestModel.getApplicationRepository(),
//                            requestModel.getReadCardService(),
//                            this.notifier,
//                            requestModel.getTransactionAuthenticator(),
//                            requestModel.getLedService(),
//                            requestModel.getSoundService(),
//                            this.terminalRepository
//                    );
//                    break;
//                default:
//                    isCardConfirmationRequired = false;
//                    break;
//            }
//            readCardResponse = this.cardProcessor.validateReadCardResponse(readCardRequest, readCardResponse);
//            this.checkForCardTypeChange(readCardResponse.getCardDetails().getCardType(), requestModel);
//            if (!isValidTransaction(
//                    requestModel.getApplicationRepository(),
//                    transactionRequest,
//                    readCardResponse
//            )) {
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
//            }
//            if (!isTransactionAuthorizationSuccess(
//                    requestModel.getTransactionAuthenticator(),
//                    requestModel.getApplicationRepository(),
//                    transactionRequest.getTransactionType(),
//                    readCardResponse
//            )) {
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
//            }
//            purchaseRequest
//                    = this.preparePurchaseCardRequest(
//                    transactionRequest,
//                    readCardResponse.getCardDetails(),
//                    readCardResponse.getFallbackIccToMag()
//            );
//            if (isCardConfirmationRequired) {
//                final boolean[] isConfirm = {false};
//                countDownLatch = new CountDownLatch(1);
//                this.notifier.notify(Notifier.EventType.CARD_CONFIRMATION_EVENT,
//                        readCardResponse.getCardDetails(),
//                        (CardConfirmationListener) confirmed -> {
//                            isConfirm[0] = confirmed;
//                            countDownLatch.countDown();
//                        });
//                countDownLatch.await();
//                if (!isConfirm[0]) {
//                    throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
//                }
//            }
//            return processCardAndProceed();
//        } catch (PosException ex) {
//            ex.printStackTrace();
//            throw ex;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        } finally {
//            requestModel.getReadCardService().cleanUp();
//            requestModel.getLedService().doTurnLedWith(new LedRequest(LedLight.ALL, LedAction.OFF));
//            PICCWave.getInstance().clear();
//            MagneticSwipe.getInstance().clear();
//        }
//    }
//
//    private PreAuthResponseModel processCardAndProceed() {
//        readCardResponse = this.cardProcessor.processCard(stan, purchaseRequest);
//        logger.debug("::: READ CARD RESPONSE ::: " + readCardResponse.toString());
//        this.notifier.notify(
//                Notifier.EventType.TRANSMITTING_REQUEST,
//                "Transmitting Request.."
//        );
//        try {
//            IsoMessageResponse isoMessageResponse = this.sendIsoMessage(
//                    stan,
//                    invoiceNumber,
//                    readCardResponse,
//                    preAuthRequestModel
//            );
//            this.notifier.notify(
//                    Notifier.EventType.RESPONSE_RECEIVED,
//                    "Please wait.."
//            );
//            InvalidResponseHandlerResponse invalidResponseHandlerResponse =
//                    this.checkForInvalidResponseFromHost(isoMessageResponse);
//            if (invalidResponseHandlerResponse.isInvalid())
//                return this.handleInvalidResponseFromHost(transactionRequest,
//                        invalidResponseHandlerResponse, batchNumber, stan, invoiceNumber,
//                        readCardResponse, preAuthRequestModel);
//            boolean isPreAuthApproved = this.isTransactionApprovedByActionCode(isoMessageResponse);
//            this.notifier.notify(Notifier.EventType.PROCESSING_ISSUER_SCRIPT, "Processing..");
//            this.setOnlineResultToCard(
//                    readCardResponse,
//                    preAuthRequestModel.getReadCardService(),
//                    isoMessageResponse
//            );
//            if (this.isTransactionDeclineByCardEvenIfSuccessOnSwitch(isoMessageResponse, readCardResponse))
//                return this.handleApprovedTransactionDeclinedByCard(stan, batchNumber, invoiceNumber,
//                        readCardResponse, preAuthRequestModel, transactionRequest);
//            if (this.isForceContactCardPromptActionCode(readCardResponse, isoMessageResponse))
//                return this.handleForceContactCardPromptFromHost(preAuthRequestModel);
//            this.notifyTransactionCompletion(
//                    stan,
//                    batchNumber,
//                    invoiceNumber,
//                    isPreAuthApproved,
//                    transactionRequest,
//                    preAuthRequestModel.getTransactionRepository(),
//                    isoMessageResponse,
//                    readCardResponse,
//                    preAuthRequestModel.getApplicationRepository(),
//                    preAuthRequestModel.getLedService()
//            );
//            boolean shouldPrint = this.printReceipt(
//                    batchNumber,
//                    stan,
//                    invoiceNumber,
//                    preAuthRequestModel.getPrinterService(),
//                    transactionRequest,
//                    preAuthRequestModel.getTransactionRepository(),
//                    readCardResponse,
//                    isoMessageResponse,
//                    preAuthRequestModel.getApplicationRepository()
//            );
//            return this.preparePreAuthResponse(stan, isoMessageResponse, isPreAuthApproved,
//                    preAuthRequestModel.getApplicationRepository(), shouldPrint);
//        } catch (FinPosException e) {
//            e.printStackTrace();
//            if (this.isResponseNotReceivedException(e)) {
//                return this.handleTimeoutTransaction(transactionRequest, batchNumber, stan,
//                        invoiceNumber, readCardResponse, preAuthRequestModel);
//            } else if (e.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT) {
//                logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED UNABLE TO CONNECT");
//                transactionRepository.removeAutoReversal(this.autoReversal);
//                throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
//            } else {
//                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//            }
//        }
//    }
//
//    private PreAuthResponseModel handleForceContactCardPromptFromHost(PreAuthRequestModel request) {
//        this.updateTransactionIds(false);
//        request.getReadCardService().cleanUp();
//        ForceContactCardPreAuthUseCase forceContactCardPreAuthUseCase = new ForceContactCardPreAuthUseCase(
//                this.terminalRepository,
//                this.notifier
//        );
//        return forceContactCardPreAuthUseCase.execute(request);
//    }
//
//    private PreAuthResponseModel handleApprovedTransactionDeclinedByCard(String stan, String batchNumber,
//                                                                         String invoiceNumber,
//                                                                         ReadCardResponse readCardResponse,
//                                                                         PreAuthRequestModel requestModel,
//                                                                         TransactionRequest transactionRequest) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, ReasonForReversal.Type.DECLINED_BY_CARD.getDescription());
//        this.performReversal(stan, readCardResponse, requestModel);
//        boolean shouldPrint = !this.approvePrintOnly(requestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest, readCardResponse,
//                    this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.DECLINED_BY_CARD,
//                            ReasonForReversal.Type.DECLINED_BY_CARD.getDescription()));
//            new NiblTransactionReceiptPrintHandler(requestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            requestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PreAuthResponseModel.Builder.createDefaultBuilder()
//                .approved(false)
//                .message(ReasonForReversal.Type.DECLINED_BY_CARD.getDescription())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private void checkForCardTypeChange(CardType cardType, PreAuthRequestModel requestModel) {
//        switch (cardType) {
//            case MAG:
//                this.cardProcessor = new global.citytech.finpos.processor.nibl.transaction.card.MagneticCardProcessor(
//                        requestModel.getTransactionRepository(),
//                        requestModel.getTransactionAuthenticator(),
//                        requestModel.getApplicationRepository(),
//                        requestModel.getReadCardService(),
//                        this.notifier,
//                        this.terminalRepository
//                );
//                break;
//            case ICC:
//                this.cardProcessor = new global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor(
//                        requestModel.getReadCardService(),
//                        requestModel.getTransactionRepository(),
//                        requestModel.getApplicationRepository(),
//                        requestModel.getTransactionAuthenticator(),
//                        this.notifier,
//                        this.terminalRepository
//                );
//                break;
//            case PICC:
//                this.cardProcessor = new PICCCardProcessor(
//                        requestModel.getTransactionRepository(),
//                        requestModel.getApplicationRepository(),
//                        requestModel.getReadCardService(),
//                        this.notifier,
//                        requestModel.getTransactionAuthenticator(),
//                        requestModel.getLedService(),
//                        requestModel.getSoundService(),
//                        this.terminalRepository
//                );
//                break;
//            default:
//                break;
//        }
//    }
//
//    private PreAuthResponseModel handleInvalidResponseFromHost(TransactionRequest transactionRequest,
//                                                               InvalidResponseHandlerResponse invalidResponseHandlerResponse,
//                                                               String batchNumber, String stan,
//                                                               String invoiceNumber, ReadCardResponse readCardResponse,
//                                                               PreAuthRequestModel requestModel) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, invalidResponseHandlerResponse.getMessage());
//        this.performReversal(stan, readCardResponse, requestModel);
//        boolean shouldPrint = !this.approvePrintOnly(requestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest,
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.INVALID_RESPONSE,
//                            invalidResponseHandlerResponse.getMessage()));
//            new NiblTransactionReceiptPrintHandler(requestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            requestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PreAuthResponseModel.Builder.createDefaultBuilder()
//                .stan(stan)
//                .approved(false)
//                .message(invalidResponseHandlerResponse.getMessage())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private PreAuthResponseModel handleTimeoutTransaction(TransactionRequest transactionRequest,
//                                                          String batchNumber, String stan,
//                                                          String invoiceNumber, ReadCardResponse readCardResponse,
//                                                          PreAuthRequestModel requestModel) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, "TIMEOUT");
//        this.setUnableToGoOnlineResultToCard(readCardResponse, requestModel.getReadCardService());
//        this.performReversal(stan, readCardResponse, requestModel);
//        boolean shouldPrint = !this.approvePrintOnly(requestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest,
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.TIMEOUT, "Timeout"));
//            new NiblTransactionReceiptPrintHandler(requestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            requestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PreAuthResponseModel.Builder.createDefaultBuilder()
//                .message("TIMEOUT")
//                .approved(false)
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private global.citytech.finposframework.usecases.transaction.data.PurchaseRequest
//    preparePurchaseCardRequest(
//            TransactionRequest transactionRequest,
//            CardDetails cardDetails,
//            boolean fallbackIccToMag
//    ) {
//        global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest
//                = new global.citytech.finposframework.usecases.transaction.data.PurchaseRequest(
//                transactionRequest.getTransactionType(),
//                transactionRequest.getAmount(),
//                transactionRequest.getAdditionalAmount()
//        );
//        cardDetails.setAmount(transactionRequest.getAmount());
//        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
//        purchaseRequest.setCardDetails(cardDetails);
//        purchaseRequest.setFallbackIccToMag(fallbackIccToMag);
//        return purchaseRequest;
//    }
//
//    private IsoMessageResponse sendIsoMessage(String stan, String invoiceNumber, ReadCardResponse readCardResponse, PreAuthRequestModel requestModel) {
//        RequestContext requestContext = this.prepareTemplateRequestContext(stan);
//        logger.debug(String.format(" Request Context ::%s", requestContext));
//        this.isoPreAuthRequest = preparePreAuthRequest(invoiceNumber, readCardResponse);
//        requestContext.setRequest(this.isoPreAuthRequest);
//        CardType detectedCardType = readCardResponse.getCardDetails().getCardType();
//        if (detectedCardType == null) {
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        }
//        this.autoReversal = new AutoReversal(
//                TransactionType.PRE_AUTH,
//                JsonUtils.toJsonObj(prepareReversalRequest(stan, readCardResponse, requestModel)),
//                0,
//                AutoReversal.Status.ACTIVE,
//                StringUtils.dateTimeStamp()
//        );
//        this.transactionRepository.saveAutoReversal(this.autoReversal);
//        PreAuthRequestSender sender;
//        switch (detectedCardType) {
//            case MAG:
//                sender = new MagPreAuthRequestSender(new NIBLSpecInfo(), requestContext);
//                break;
//
//            case PICC:
//                sender = new PICCPreAuthRequestSender(new NIBLSpecInfo(), requestContext);
//                break;
//
//            default:
//                sender = new IccPreAuthRequestSender(new NIBLSpecInfo(), requestContext);
//                break;
//        }
//        IsoMessageResponse isoMessageResponse = sender.send();
//        this.transactionRepository.removeAutoReversal(this.autoReversal);
//        return isoMessageResponse;
//    }
//
//    private PreAuthResponseModel preparePreAuthResponse(
//            String stan,
//            IsoMessageResponse response,
//            boolean isPurchaseApproved,
//            ApplicationRepository applicationRepository,
//            boolean shouldPrint
//    ) {
//        PreAuthResponseModel.Builder builder = PreAuthResponseModel.Builder.createDefaultBuilder();
//        builder.approved(isPurchaseApproved);
//        builder.stan(stan);
//        builder.debugRequestMessage(response.getDebugRequestString());
//        builder.debugResponseMessage(response.getDebugResponseString());
//        builder.message(retrieveMessage(response, isPurchaseApproved, applicationRepository));
//        builder.shouldPrintCustomerCopy(shouldPrint);
//        return builder.build();
//    }
//
//    private void performReversal(
//            String stan,
//            ReadCardResponse readCardResponse,
//            PreAuthRequestModel preAuthRequestModel
//    ) {
//        ReversalUseCase reversalUseCase = new ReversalUseCase(
//                this.prepareReversalRequest(stan, readCardResponse, preAuthRequestModel),
//                this.terminalRepository,
//                this.notifier
//        );
//        try {
//            reversalUseCase.execute(
//                    prepareReversalRequestModel(
//                            preAuthRequestModel,
//                            readCardResponse
//                    )
//            );
//            this.transactionRepository.removeAutoReversal(this.autoReversal);
//        } catch (FinPosException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private NiblReversalRequest prepareReversalRequest(
//            String stan,
//            ReadCardResponse readCardResponse,
//            PreAuthRequestModel preAuthRequestModel
//    ) {
//        NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
//        niblReversalRequest.setProcessingCode(ProcessingCode.PRE_AUTHORIZATION.getCode());
//        niblReversalRequest.setAmount(
//                this.isoPreAuthRequest.getTransactionAmount()
//        );
//        niblReversalRequest.setPan(this.isoPreAuthRequest.getPan());
//        niblReversalRequest.setExpiryDate(this.isoPreAuthRequest.getExpireDate());
//        niblReversalRequest.setPosEntryMode(this.isoPreAuthRequest.getPosEntryMode());
//        niblReversalRequest.setPosConditionCode(this.isoPreAuthRequest.getPosConditionCode());
//        niblReversalRequest.setTrack2Data(this.isoPreAuthRequest.getTrack2Data());
//        niblReversalRequest.setPinBlock(this.isoPreAuthRequest.getPinBlock());
//        niblReversalRequest.setInvoiceNumber(this.isoPreAuthRequest.getInvoiceNumber());
//        niblReversalRequest.setStan(stan);
//        return niblReversalRequest;
//    }
//
//    private ReversalRequestModel prepareReversalRequestModel(
//            PreAuthRequestModel preAuthRequestModel,
//            ReadCardResponse readCardResponse
//    ) {
//        return ReversalRequestModel.Builder.newInstance()
//                .withTransactionRequest(
//                        preAuthRequestModel.getTransactionRequest()
//                )
//                .withTransactionRepository(
//                        preAuthRequestModel.getTransactionRepository()
//                )
//                .withPrinterService(
//                        preAuthRequestModel.getPrinterService()
//                )
//                .withApplicationRepository(
//                        preAuthRequestModel.getApplicationRepository()
//                )
//                .withReadCardResponse(readCardResponse)
//                .build();
//    }
//
//    private void notifyTransactionCompletion(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean authCompletionApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository,
//            LedService ledService
//    ) {
//        this.updateTransactionLog(
//                stan,
//                batchNumber,
//                invoiceNumber,
//                authCompletionApproved,
//                transactionRequest,
//                transactionRepository,
//                isoMessageResponse,
//                readCardResponse,
//                applicationRepository
//        );
//        this.updateTransactionIds(authCompletionApproved);
//        this.notifyTransactionStatus(
//                authCompletionApproved,
//                isoMessageResponse,
//                applicationRepository,
//                ledService,
//                readCardResponse.getCardDetails().getCardType() == CardType.PICC
//        );
//    }
//
//    private void updateTransactionLog(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean authCompletionApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository
//    ) {
//        this.logger.log("::: NIBL ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
//        this.logger.log("::: NIBL ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
//        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
//                .withStan(stan)
//                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
//                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
//                .withInvoiceNumber(invoiceNumber)
//                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
//                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
//                .withTransactionType(TransactionType.PRE_AUTH)
//                .withOriginalTransactionType(TransactionType.PRE_AUTH)
//                .withTransactionAmount(transactionRequest.getAmount())
//                .withOriginalTransactionAmount(transactionRequest.getAmount())
//                .withTransactionDate(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_DATE))
//                .withTransactionTime(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_TIME))
//                .withTransactionStatus(authCompletionApproved ? "APPROVED" : "DECLINED")
//                .withPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
//                .withOriginalPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
//                .withPosConditionCode(this.isoPreAuthRequest.getPosConditionCode())
//                .withOriginalPosConditionCode("")
//                .withReconcileStatus("")
//                .withReconcileTime("")
//                .withReconcileDate("")
//                .withReconcileBatchNo(batchNumber)
//                .withReadCardResponse(readCardResponse)
//                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse,
//                        applicationRepository, batchNumber, stan, invoiceNumber))
//                .withAuthorizationCompleted(false)
//                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
//                .withTransactionVoided(false)
//                .withTipAdjusted(false)
//                .build();
//        transactionRepository.updateTransactionLog(transactionLog);
//    }
//}