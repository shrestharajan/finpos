package global.citytech.finpos.processor.neps.cardprocessor;

import global.citytech.finpos.neps.iso8583.SmartVistaActionCode;
import global.citytech.finpos.neps.iso8583.SmartVistaIccDataList;
import global.citytech.finpos.processor.neps.NepsConstant;
import global.citytech.finpos.processor.neps.transaction.TransactionUtils;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.TransactionStatusFromHost;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.ChipCardProcessorInterface;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Saurav Ghimire on 1/27/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public abstract class ChipCardProcessor extends CardProcessor implements ChipCardProcessorInterface {


    public ChipCardProcessor(
            TransactionRepository transactionRepository,
            TransactionAuthenticator transactionAuthenticator,
            ApplicationRepository applicationRepository,
            ReadCardService readCardService,
            Notifier notifier,
            TerminalRepository terminalRepository
    ) {
        super(
                transactionRepository,
                transactionAuthenticator,
                applicationRepository,
                readCardService,
                notifier,
                terminalRepository
        );
    }

    protected abstract ReadCardResponse readCardFromFallbackProcessor(ReadCardRequest readCardRequest);

    protected abstract boolean checkFallback(ReadCardResponse readCardResponse);

    @Override
    protected void validateByRespectiveProcessor(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse) {
        if (readCardResponse.getResult() == Result.APPLICATION_BLOCKED) {
            throw new PosException(PosError.DEVICE_ERROR_APPLICATION_BLOCKED);
        }
        if (readCardResponse.getResult() == Result.CARD_BLOCKED) {
            throw new PosException(PosError.DEVICE_ERROR_CARD_BLOCKED);
        }
        if (readCardResponse.getResult() == Result.USER_CANCELLED) {
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        }
        if (readCardResponse.getResult() == Result.PROCESSING_ERROR) {
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
    }

    @Override
    public ReadCardResponse processCard(String stan, PurchaseRequest purchaseRequest) {
        ReadCardRequest readCardRequest = this.prepareReadCardRequest(stan, purchaseRequest);
        ReadCardResponse readCardResponse = readCardService.processEMV(readCardRequest);
        if (readCardResponse.getResult() != Result.FAILURE) {
            if (readCardResponse.getResult() == Result.PROCESSING_ERROR) {
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
            } else if (readCardResponse.getResult() == Result.PINPAD_ERROR) {
                throw new PosException(PosError.DEVICE_ERROR_PINPAD_ERROR);
            }

            handleErrorsByProcessor(readCardResponse);

            handleTransactionDeclinedCases(readCardResponse);

            readCardResponse.getCardDetails().setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
            return readCardResponse;
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }

    }

    protected abstract void handleTransactionDeclinedCases(ReadCardResponse readCardResponse);

    protected abstract void handleErrorsByProcessor(ReadCardResponse readCardResponse);

    private ReadCardRequest prepareReadCardRequest(String stan, PurchaseRequest purchaseRequest) {
        ReadCardRequest readCardRequest = new ReadCardRequest(
                TransactionUtils.getAllowedCardEntryModes(purchaseRequest.getTransactionType()),
                purchaseRequest.getTransactionType(),
                TransactionUtils.retrieveTransactionType9C(purchaseRequest.getTransactionType()),
                this.applicationRepository.retrieveEmvParameterRequest()
        );
        readCardRequest.setStan(stan);
        readCardRequest.setAmount(purchaseRequest.getAmount());
        readCardRequest.setCashBackAmount(purchaseRequest.getAdditionalAmount());
        readCardRequest.setPinpadRequired(checkIfPinRequired(purchaseRequest.getTransactionType(),
                purchaseRequest
                        .getCardDetails()
                        .getCardScheme().getCardSchemeId()));
        readCardRequest.setPrimaryAccountNumber(purchaseRequest.getCardDetails().getPrimaryAccountNumber());
        readCardRequest.setCardDetailsBeforeEmvNext(purchaseRequest.getCardDetails());
        readCardRequest.setTransactionDate(DateUtils.HHmmssTime());
        readCardRequest.setTransactionTime(DateUtils.yyMMddDate());
        readCardRequest.setIccDataList(SmartVistaIccDataList.get());
        readCardRequest.setCurrencyName(NepsConstant.PIN_PAD_CURRENCY);
        readCardRequest.setPinPadFixedLayout(!terminalRepository.shouldShufflePinPad());
        return readCardRequest;
    }

    protected abstract Boolean checkIfPinRequired(TransactionType transactionType, String cardSchemeId);

    @Override
    public ReadCardResponse processOnlineResult(ReadCardResponse readCardResponse, IsoMessageResponse isoMessageResponse) {
        if (this.isArqcTransaction(readCardResponse)) {
            int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, 39);
            ReadCardResponse afterSetOnlineResultResponse = readCardService.setOnlineResult(
                    actionCode,
                    mapTransactionHostStatus(SmartVistaActionCode.getByActionCode(actionCode)
                            .getTransactionStatusFromHost()),
                    this.prepareField55DataForOnlineResult(actionCode, IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, 55)),
                    false);
            readCardResponse.getCardDetails().setIccDataBlock(afterSetOnlineResultResponse
                    .getCardDetails().getIccDataBlock());
            readCardResponse.getCardDetails().setTagCollection(afterSetOnlineResultResponse
                    .getCardDetails().getTagCollection());
            readCardResponse.getCardDetails().setTransactionState(afterSetOnlineResultResponse
                    .getCardDetails().getTransactionState());
        }
        return readCardResponse;
    }

    private String prepareField55DataForOnlineResult(int actionCode, String field55FromHost) {
        StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isEmpty(field55FromHost) || !field55FromHost.contains("8A02")) {
            stringBuilder.append("8A02");
            stringBuilder.append(StringUtils.toHexaDecimal(StringUtils.ofRequiredLength(String.valueOf(actionCode), 2)));
        }
        if (!StringUtils.isEmpty(field55FromHost))
            stringBuilder.append(field55FromHost);
        return stringBuilder.toString();
    }

    private ReadCardService.OnlineResult mapTransactionHostStatus(TransactionStatusFromHost transactionStatusFromHost) {
        switch (transactionStatusFromHost) {
            case ONLINE_APPROVED:
                return ReadCardService.OnlineResult.ONLINE_APPROVED;
            case ONLINE_DECLINED:
                return ReadCardService.OnlineResult.ONLINE_DECLINED;
            default:
                return ReadCardService.OnlineResult.UNABLE_TO_GO_ONLINE;
        }
    }

    private boolean isArqcTransaction(ReadCardResponse readCardResponse) {
        try {
            return readCardResponse
                    .getCardDetails()
                    .getTagCollection()
                    .get(IccData.CID.getTag())
                    .getData()
                    .equals("80");
        } catch (Exception e) {
            return false;
        }
    }
}
