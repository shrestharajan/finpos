package global.citytech.finposframework.hardware.keymgmt.keys.requests

import global.citytech.finposframework.hardware.keymgmt.keys.KeyAlgorithm
import global.citytech.finposframework.hardware.keymgmt.keys.KeyType

/**
 * Created by Rishav Chudal on 4/8/20.
 */
data class KeyInjectionRequest(
    val packageName: String,
    val keyType: KeyType,
    val algorithm: KeyAlgorithm,
    val key: String,
    val isPlainText: Boolean
) {
    var keyIndex: Int? = null
    var protectKeyType: KeyType? = null
    var protectKeyIndex: Int? = null
    var keyKCV: String? = null
    var ksn: String? = null
    var publicExponentRSA = 0
}