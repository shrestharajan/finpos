package global.citytech.finpos.neps.iso8583.requestsender.preauths;

import global.citytech.finpos.neps.iso8583.requestsender.cardtypesenders.ManualISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.CardTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transactiontypesenders.PreAuthIsoMessageUpdater;
import global.citytech.finposframework.iso8583.RequestContext;

/**
 * Created by Saurav Ghimire on 2/2/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class ManualPreAuthRequestSender extends PreAuthRequestSender {
    public ManualPreAuthRequestSender( RequestContext context) {
        super( context);
    }

    @Override
    protected String getClassName() {
        return ManualPreAuthRequestSender.class.getName();
    }

    @Override
    protected CardTypeISOMessageUpdater getCardTypeSender() {
        return new ManualISOMessageUpdater();
    }

}
