package global.citytech.payment.sdk.api;

/**
 * Created by Unique Shakya on 8/20/2021.
 */
public class VoidPaymentRequest {
    private String invoiceNumber;
    private double billedAmount;
    private String additionalData;

    public VoidPaymentRequest(String invoiceNumber, double billedAmount) {
        this.billedAmount = billedAmount;
        this.invoiceNumber = invoiceNumber;
    }

    public double getBilledAmount() {
        return billedAmount;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }
}
