package global.citytech.finpos.processor.nibl.logon;

import java.sql.Timestamp;
import java.util.Date;

import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finpos.nibl.iso8583.requestsender.logon.LogonRequestSender;
import global.citytech.finpos.processor.nibl.transaction.receipt.NiblIsoMessageReceiptHandler;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.hardware.common.DeviceResponse;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.keymgmt.keys.KeyAlgorithm;
import global.citytech.finposframework.hardware.keymgmt.keys.KeyType;
import global.citytech.finposframework.hardware.keymgmt.keys.requests.KeyInjectionRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.logon.LogOnRequester;
import global.citytech.finposframework.switches.logon.LogOnResponseParameter;
import global.citytech.finposframework.usecases.activity.ActivityLog;
import global.citytech.finposframework.usecases.activity.ActivityLogStatus;
import global.citytech.finposframework.usecases.activity.ActivityLogType;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.receipt.logon.MerchantLogonReceipt;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finposframework.hardware.utility.DeviceApiConstants.MASTER_KEY_INDEX;
import static global.citytech.finposframework.hardware.utility.DeviceApiConstants.PEK_KEY_INDEX;

public class MerchantLogOnUseCase implements UseCase<LogOnRequestModel, LogOnResponseModel>,
        LogOnRequester<LogOnRequestModel, LogOnResponseParameter> {

    private final Logger logOnUseCaseLogger = Logger.getLogger(MerchantLogOnUseCase.class.getSimpleName());
    private final TerminalRepository repository;
    private final Logger logger = Logger.getLogger(MerchantLogOnUseCase.class.getName());
    private LogOnRequestModel logOnRequestModel;
    private LogOnResponseModel logOnResponseModel;
    private IsoMessageResponse logOnIsoMessageResponse;

    public MerchantLogOnUseCase(TerminalRepository repository) {
        this.repository = repository;
    }

    @Override
    public LogOnResponseModel execute(LogOnRequestModel requestModel) {
        try {
            this.logOnRequestModel = requestModel;
            this.logOnIsoMessageResponse = this.sendLogOnRequestToHostAndReturnResponse();
            updateActivityLog(logOnIsoMessageResponse);
            this.increaseSystemTraceAuditNumber();
            this.validateLogOnIsoResponseAndProceed();
            return this.logOnResponseModel;
        } catch (FinPosException e) {
            updateActivityLog(e);
        } catch (Exception e) {
            updateActivityLog(e.getLocalizedMessage());
        }
        returnNullLogonResponse();
        return this.logOnResponseModel;
    }

    private IsoMessageResponse sendLogOnRequestToHostAndReturnResponse() {
        RequestContext logOnRequestContext = this.prepareRequestContextForLogOn();
        logger.debug(String.format("LogOn Request Context ::%s", logOnRequestContext));
        LogonRequestSender logonRequestSender = new LogonRequestSender(
                new NIBLSpecInfo(),
                logOnRequestContext
        );
        IsoMessageResponse isoMessageResponse = logonRequestSender.send();
        return isoMessageResponse;
    }

    private void updateActivityLog(IsoMessageResponse isoMessageResponse) {
        try {
            TerminalInfo terminalInfo = this.repository.findTerminalInfo();
            String responseCode = IsoMessageUtils.retrieveFromDataElementsAsString(
                    isoMessageResponse,
                    DataElement.RESPONSE_CODE
            );
            String isoResponse = "";
            ActivityLogStatus status = ActivityLogStatus.SUCCESS;
            if (!responseCode.equals("00")) {
                status = ActivityLogStatus.FAILED;
                isoResponse = isoMessageResponse.getDebugRequestString();
            }
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.repository.getSystemTraceAuditNumber())
                    .withType(ActivityLogType.LOGON)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber("")
                    .withAmount(0L)
                    .withResponseCode(responseCode)
                    .withIsoRequest(isoMessageResponse.getDebugRequestString())
                    .withIsoResponse(isoResponse)
                    .withStatus(status)
                    .withRemarks("")
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(StringUtils.prepareAdditionalDataForActivityLog(this.repository.getTerminalSerialNumber()))
                    .build();
            this.repository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update LOGON activity log :: " + e.getMessage());
        }
    }

    private void updateActivityLog(String remarks) {
        try {
            TerminalInfo terminalInfo = this.repository.findTerminalInfo();
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.repository.getSystemTraceAuditNumber())
                    .withType(ActivityLogType.LOGON)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber("")
                    .withAmount(0L)
                    .withResponseCode("")
                    .withIsoRequest("")
                    .withIsoResponse("")
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(remarks)
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(StringUtils.prepareAdditionalDataForActivityLog(this.repository.getTerminalSerialNumber()))
                    .build();
            this.repository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update LOGON activity log :: " + e.getMessage());
        }
    }

    private void updateActivityLog(FinPosException fe) {
        try {
            TerminalInfo terminalInfo = this.repository.findTerminalInfo();
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.repository.getSystemTraceAuditNumber())
                    .withType(ActivityLogType.LOGON)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber("")
                    .withAmount(0L)
                    .withResponseCode("")
                    .withIsoRequest(fe.getIsoRequest())
                    .withIsoResponse("")
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(fe.getLocalizedMessage() + " : " + fe.getDetailMessage())
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(StringUtils.prepareAdditionalDataForActivityLog(this.repository.getTerminalSerialNumber()))
                    .build();
            this.repository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update LOGON activity log :: " + e.getMessage());
        }
    }

    private RequestContext prepareRequestContextForLogOn() {
        HostInfo primaryHostInfo = this.repository.findPrimaryHost();
        HostInfo secondaryHostInfo = this.repository.findSecondaryHost();
        TerminalInfo terminalInfo = this.repository.findTerminalInfo();
        ConnectionParam connectionParam = new ConnectionParam(primaryHostInfo, secondaryHostInfo);
        return new RequestContext(
                this.repository.getSystemTraceAuditNumber(),
                terminalInfo,
                connectionParam
        );
    }

    private void validateLogOnIsoResponseAndProceed() {
        this.logOnUseCaseLogger.debug("Log On Iso Response ::: " + this.logOnIsoMessageResponse);
        if (this.logOnIsoMessageResponse != null) {
            validateField62FromResponseAndProceed();
        } else {
            this.logOnUseCaseLogger.debug("Log On Iso Response is null...");
            returnNullLogonResponse();
        }
    }

    private void returnNullLogonResponse() {
        this.logOnResponseModel = LogOnResponseModel.Builder.createDefaultBuilder()
                .success(false)
                .build();
    }

    private void increaseSystemTraceAuditNumber() {
        this.repository.incrementSystemTraceAuditNumber();
    }

    private void printDebugResponseIfDebugMode(PrinterService printerService) {
        boolean isDebugVersion = this.repository.findTerminalInfo().isDebugModeEnabled();
        if (isDebugVersion) {
            printDebugResponse(printerService);
        }
    }

    private void printDebugResponse(PrinterService printerService) {
        ReceiptHandler.IsoMessageReceiptHandler isoMessageReceiptHandler
                = new NiblIsoMessageReceiptHandler(printerService);
        isoMessageReceiptHandler.printIsoMessageReceipt(this.logOnIsoMessageResponse);
    }

    private void validateField62FromResponseAndProceed() {
        String pinEncryptionKey = IsoMessageUtils.retrieveFromDataElementsAsString(
                this.logOnIsoMessageResponse,
                DataElement.PRIVATE_USE_FIELD_62
        );
        this.logOnUseCaseLogger.debug("PEK ::: " + pinEncryptionKey);
        if (StringUtils.isEmpty(pinEncryptionKey)) {
            returnInvalidLogOnResponse();
        } else {
            preparePEKInjectionRequestAndInjectKey(pinEncryptionKey);
        }
    }

    private void returnInvalidLogOnResponse() {
//        this.logOnIsoMessageResponse = null;
        this.prepareAndPrintReceipt();
    }

    private void preparePEKInjectionRequestAndInjectKey(String pinEncryptionKey) {
        KeyInjectionRequest keyInjectionRequest = prepareKeyInjectionRequest(pinEncryptionKey);
        DeviceResponse keyInjectionResponse
                = this.logOnRequestModel.getHardwareKeyService().injectKey(keyInjectionRequest);
        validateKeyInjectionResponseAndProceed(keyInjectionResponse);
    }

    private void validateKeyInjectionResponseAndProceed(DeviceResponse keyInjectionResponse) {
        this.logOnUseCaseLogger.debug("Key Injection Result ::: " + keyInjectionResponse.getResult());
        this.logOnUseCaseLogger.debug("Key Injection Message ::: " + keyInjectionResponse.getMessage());
        if (keyInjectionResponse.getResult() == Result.SUCCESS) {
            prepareAndPrintReceipt();
        } else {
            returnInvalidLogOnResponse();
        }
    }

    private KeyInjectionRequest prepareKeyInjectionRequest(String key) {
        KeyInjectionRequest keyInjectionRequest = new KeyInjectionRequest(
                this.logOnRequestModel.getApplicationPackageName(),
                KeyType.KEY_TYPE_PEK,
                KeyAlgorithm.TYPE_3DES,
                key.toUpperCase(),
                false
        );
        keyInjectionRequest.setKeyIndex(PEK_KEY_INDEX);
        keyInjectionRequest.setProtectKeyType(KeyType.KEY_TYPE_TMK);
        keyInjectionRequest.setProtectKeyIndex(MASTER_KEY_INDEX);
        return keyInjectionRequest;
    }

    private void prepareAndPrintReceipt() {
        MerchantLogonReceipt merchantLogonReceipt = prepareMerchantLogOnReceipt();
        printLogOnReceipt(merchantLogonReceipt);
    }

    private void printLogOnReceipt(MerchantLogonReceipt merchantLogonReceipt) {
        ReceiptHandler.MerchantLogonReceiptHandler receiptHandler
                = new NiblMerchantLogonReceiptPrintHandler(this.logOnRequestModel.getPrinterService());
        receiptHandler.printLogonReceipt(merchantLogonReceipt);
        assignResponseModelOnFinishPrinting();
    }

    private MerchantLogonReceipt prepareMerchantLogOnReceipt() {
        this.printDebugResponseIfDebugMode(this.logOnRequestModel.getPrinterService());
        return new NiblMerchantLogonReceiptHandler(this.logOnIsoMessageResponse).prepare();
    }

    private void assignResponseModelOnFinishPrinting() {
        LogOnResponseModel.Builder builder = LogOnResponseModel.Builder.createDefaultBuilder();
        builder.success(isSuccessLogOn(this.logOnIsoMessageResponse));
        builder.debugRequestMessage(this.logOnIsoMessageResponse.getDebugRequestString());
        builder.debugResponseMessage(this.logOnIsoMessageResponse.getDebugResponseString());
        this.logOnResponseModel = builder.build();
    }

    private boolean isSuccessLogOn(IsoMessageResponse logOnIsoMessageResponse) {
        int responseCode = IsoMessageUtils.retrieveFromDataElementsAsInt(
                logOnIsoMessageResponse, DataElement.RESPONSE_CODE
        );
        return responseCode == 0;
    }

}