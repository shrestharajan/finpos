package global.citytech.finpos.processor.neps.reconciliation;

import global.citytech.finposframework.usecases.CardSchemeType;

public class NepsCardSchemeTotals {
    private String transactionCurrencyName;
    private CardSchemeType cardSchemeType;
    private long salesCount;
    private long salesAmount;
    private long salesRefundCount;
    private long salesRefundAmount;
    private long authCount;
    private long authAmount;
    private long authRefundCount;
    private long authRefundAmount;
    private long voidCount;
    private long voidAmount;

    public String getTransactionCurrencyName() {
        return transactionCurrencyName;
    }

    public CardSchemeType getCardSchemeType() {
        return cardSchemeType;
    }

    public long getSalesCount() {
        return salesCount;
    }

    public long getSalesAmount() {
        return salesAmount;
    }

    public long getSalesRefundCount() {
        return salesRefundCount;
    }

    public long getSalesRefundAmount() {
        return salesRefundAmount;
    }

    public long getAuthCount() {
        return authCount;
    }

    public long getAuthAmount() {
        return authAmount;
    }

    public long getAuthRefundCount() {
        return authRefundCount;
    }

    public long getAuthRefundAmount() {
        return authRefundAmount;
    }

    public long getVoidCount() {
        return voidCount;
    }

    public long getVoidAmount() {
        return voidAmount;
    }

    public long getTotalCount() {
        return salesCount + salesRefundCount + authCount + authRefundCount +voidCount;
    }

    public long getTotalAmount() {
        return salesAmount + salesRefundAmount + authAmount + authRefundAmount ;
    }

    public static final class Builder {
        private String transactionCurrencyName;
        private CardSchemeType cardSchemeType;
        private long salesCount;
        private long salesAmount;
        private long salesRefundCount;
        private long salesRefundAmount;
        private long authCount;
        private long authAmount;
        private long authRefundCount;
        private long authRefundAmount;
        private long voidCount;
        private long voidAmount;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withTransactionCurrencyName(String transactionCurrencyName) {
            this.transactionCurrencyName = transactionCurrencyName;
            return this;
        }

        public Builder withCardSchemeType(CardSchemeType cardSchemeType) {
            this.cardSchemeType = cardSchemeType;
            return this;
        }

        public Builder withSalesCount(long salesCount) {
            this.salesCount = salesCount;
            return this;
        }

        public Builder withSalesAmount(long salesAmount) {
            this.salesAmount = salesAmount;
            return this;
        }

        public Builder withSalesRefundCount(long salesRefundCount) {
            this.salesRefundCount = salesRefundCount;
            return this;
        }
        
        public Builder withSalesRefundAmount(long salesRefundAmount) {
            this.salesRefundAmount = salesRefundAmount;
            return this;
        }

        public Builder withAuthCount(long authCount) {
            this.authCount = authCount;
            return this;
        }

        public Builder withAuthAmount(long authAmount) {
            this.authAmount = authAmount;
            return this;
        }

        public Builder withAuthRefundCount(long authRefundCount) {
            this.authRefundCount = authRefundCount;
            return this;
        }

        public Builder withAuthRefundAmount(long authRefundAmount) {
            this.authRefundAmount = authRefundAmount;
            return this;
        }

        public Builder withVoidCount(long voidCount) {
            this.voidCount = voidCount;
            return this;
        }

        public Builder withVoidAmount(long voidAmount) {
            this.voidAmount = voidAmount;
            return this;
        }



        public NepsCardSchemeTotals build() {
            NepsCardSchemeTotals nepsCardSchemeTotals = new NepsCardSchemeTotals();
            nepsCardSchemeTotals.transactionCurrencyName = this.transactionCurrencyName;
            nepsCardSchemeTotals.cardSchemeType = this.cardSchemeType;
            nepsCardSchemeTotals.salesCount = this.salesCount;
            nepsCardSchemeTotals.salesAmount = this.salesAmount;
            nepsCardSchemeTotals.salesRefundCount = this.salesRefundCount;
            nepsCardSchemeTotals.salesRefundAmount = this.salesRefundAmount;
            nepsCardSchemeTotals.authCount = this.authCount;
            nepsCardSchemeTotals.authAmount = this.authAmount;
            nepsCardSchemeTotals.authRefundAmount = this.authRefundAmount;
            nepsCardSchemeTotals.authRefundCount = this.authRefundCount;
            nepsCardSchemeTotals.voidAmount = this.voidAmount;
            nepsCardSchemeTotals.voidCount = this.voidCount;
            return nepsCardSchemeTotals;
        }
    }
}
