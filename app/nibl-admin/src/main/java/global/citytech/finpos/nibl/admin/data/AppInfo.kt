package global.citytech.finpos.nibl.admin.data

data class AppInfo(
    var id: String? = null,
    var version: Int? = null
)