package global.citytech.finpos.merchant.presentation.dashboard.base

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.finpos.merchant.data.repository.app.AppNotificationRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.AutoReversalQueueRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.device.DeviceRepositoryImpl
import global.citytech.finpos.merchant.domain.usecase.app.AppNotificationUseCase
import global.citytech.finpos.merchant.domain.usecase.app.CoreAutoReversalQueueUseCase
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.device.DeviceUseCase
import global.citytech.finpos.merchant.framework.datasource.app.AppNotificationDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.AutoReversalQueueDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.device.DeviceConfigurationSourceImpl
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuItem
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuUtils
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.switches.posmode.PosMode
import global.citytech.finposframework.usecases.TransactionType
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Rishav Chudal on 5/13/21.
 */
abstract class BaseDashboardViewModel(val context: Application) : BaseAndroidViewModel(context) {

    private val logger = Logger(BaseDashboardViewModel::class.java.simpleName)

    val appConfiguration by lazy { MutableLiveData<ConfigurationItem>() }
    val appVersion by lazy { MutableLiveData<String>() }
    val terminalIsConfigured by lazy { MutableLiveData<Boolean>() }
    val menusMutableLiveData by lazy { MutableLiveData<List<MenuItem>>() }
    val forceSettlementMessageMutableLiveData by lazy { MutableLiveData<String>() }
    val noNotifications by lazy { MutableLiveData<Boolean>() }
    val autoReversalPresent by lazy { MutableLiveData<Boolean>() }

    var localDataUseCase: LocalDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    private val deviceConfigurationUseCase = DeviceUseCase(
        DeviceRepositoryImpl(
            DeviceConfigurationSourceImpl(context)
        )
    )

    private val appNotificationUseCase = AppNotificationUseCase(
        AppNotificationRepositoryImpl(
            AppNotificationDataSourceImpl(context.applicationContext)
        )
    )

    private val coreAutoReversalQueueUseCase = CoreAutoReversalQueueUseCase(
        AutoReversalQueueRepositoryImpl(
            AutoReversalQueueDataSourceImpl(context)
        )
    )

    fun checkForNotifications() {
        noNotifications.value = appNotificationUseCase.noNotifications()
    }

    fun getAppVersion() {
        val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        val version = packageInfo.versionName
        appVersion.postValue("v".plus(version))
    }

    fun getConfiguration(): Boolean {
        this.isLoading.value = true
        return compositeDisposable.add(
            localDataUseCase.getWholeConfiguration()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe({
                    appConfiguration.value = it
                    doCleanUpAtStart()
                },
                    {
                        this.isLoading.value = false
                        message.value = it.message
                    })
        )
    }

    private fun doCleanUpAtStart() = Completable
        .fromAction { deviceConfigurationUseCase.closeUpIO() }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            this::onCleanUpComplete,
            this::onCleanUpError
        )

    private fun onCleanUpComplete() {
        logger.log("Clean Up Complete")
    }

    private fun onCleanUpError(throwable: Throwable) {
        this.isLoading.value = false
        logger.log("Clean Up Error ::: ".plus(throwable.message))
    }

    fun getTerminalConfiguredStatusAndProceed() {
        this.terminalIsConfigured.value =
            localDataUseCase.isTerminalConfigured(defaultValue = false)
    }

    fun loadEnabledTransactionsForGivenPosMode(posMode: PosMode) {
        this.isLoading.value = true
        this.compositeDisposable.add(
            this.localDataUseCase.getEnabledTransactionsForGivenPosMode(posMode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        onNextLoadingEnabledTransactions(it)
                    },
                    {
                        onErrorLoadingEnabledTransactions(it)
                    }
                )
        )
    }

    private fun onNextLoadingEnabledTransactions(enabledTransactions: List<TransactionType>) {
        this.proceedWithEnabledTransactions(enabledTransactions)
        val menus = MenuUtils.getMenuBasedOnEnabledTransactions(enabledTransactions)
        this.menusMutableLiveData.value = menus
        this.isLoading.value = false
    }

    private fun onErrorLoadingEnabledTransactions(throwable: Throwable) {
        this.logger.debug("Exception in Loading Allowed Transactions ::: ".plus(throwable.message))
        val menus = MenuUtils.getMenuBasedOnEnabledTransactions(emptyList())
        this.menusMutableLiveData.value = menus
        this.isLoading.value = false
    }

    protected abstract fun proceedWithEnabledTransactions(enabledTransactions: List<TransactionType>)

    fun checkForAutoReversal() {
        if (coreAutoReversalQueueUseCase.isQueueEmpty()) {
            Logger.getLogger("AutoReversal").log("::: AUTO REVERSAL ::: QUEUE IS EMPTY")
            autoReversalPresent.value = false
        } else {
            Logger.getLogger("AutoReversal").log("::: AUTO REVERSAL ::: QUEUE IS NOT EMPTY")
            autoReversalPresent.value = coreAutoReversalQueueUseCase.isActive()
        }
    }
}