package global.citytech.finpos.processor.nibl.reconciliation;

import java.util.Map;

import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationTotals;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Unique Shakya on 11/12/2020.
 */
public class NiblSettlementTotals implements ReconciliationTotals {

    private Map<CardSchemeType, NiblCardSchemeTotals> totalsMap;
    private String transactionCurrencyName;
    private String totalsBlockString;
    private long salesCount;
    private long salesAmount;
    private long salesRefundCount;
    private long salesRefundAmount;
    private long debitCount;
    private long debitAmount;
    private long debitRefundCount;
    private long debitRefundAmount;
    private long authCount;
    private long authAmount;
    private long authRefundCount;
    private long authRefundAmount;
    private long voidCount;
    private long voidAmount;

    public NiblSettlementTotals(String transactionCurrencyName, Map<CardSchemeType, NiblCardSchemeTotals> totalsMap) {
        this.totalsMap = totalsMap;
        this.transactionCurrencyName = transactionCurrencyName;
    }

    public Map<CardSchemeType, NiblCardSchemeTotals> getTotalsMap() {
        return totalsMap;
    }

    public long getSalesCount() {
        return salesCount;
    }

    public long getSalesAmount() {
        return salesAmount;
    }

    public long getSalesRefundCount() {
        return salesRefundCount;
    }

    public long getSalesRefundAmount() {
        return salesRefundAmount;
    }

    public long getDebitCount() {
        return debitCount;
    }

    public long getDebitAmount() {
        return debitAmount;
    }

    public long getDebitRefundCount() {
        return debitRefundCount;
    }

    public long getDebitRefundAmount() {
        return debitRefundAmount;
    }

    public long getAuthCount() {
        return authCount;
    }

    public long getAuthAmount() {
        return authAmount;
    }

    public long getAuthRefundCount() {
        return authRefundCount;
    }

    public long getAuthRefundAmount() {
        return authRefundAmount;
    }

    public long getVoidCount() {
        return voidCount;
    }

    public long getVoidAmount() {
        return voidAmount;
    }

    public String getTransactionCurrencyName() {
        return transactionCurrencyName;
    }

    public long getTotalCountReceipt() {
        return salesCount;
    }

    public long getTotalAmountReceipt() {
        return salesAmount;
    }

    @Override
    public String toTotalsBlock() {
        if(StringUtils.isEmpty(totalsBlockString)) {
            for (Map.Entry<CardSchemeType, NiblCardSchemeTotals> niblCardSchemeTotals : this.totalsMap.entrySet()) {
                this.salesCount = this.salesCount + niblCardSchemeTotals.getValue().getSalesCount();
                this.salesAmount = this.salesAmount + niblCardSchemeTotals.getValue().getSalesAmount();
                this.salesRefundCount = this.salesRefundCount + niblCardSchemeTotals.getValue().getSalesRefundCount();
                this.salesRefundAmount = this.salesRefundAmount + niblCardSchemeTotals.getValue().getSalesRefundAmount();
                this.debitCount = this.debitCount + niblCardSchemeTotals.getValue().getDebitCount();
                this.debitAmount = this.debitAmount + niblCardSchemeTotals.getValue().getDebitAmount();
                this.debitRefundCount = this.debitRefundCount + niblCardSchemeTotals.getValue().getDebitRefundCount();
                this.debitRefundAmount = this.debitRefundAmount + niblCardSchemeTotals.getValue().getDebitRefundAmount();
                this.authCount = this.authCount + niblCardSchemeTotals.getValue().getAuthCount();
                this.authAmount = this.authAmount + niblCardSchemeTotals.getValue().getAuthAmount();
                this.authRefundCount = this.authRefundCount + niblCardSchemeTotals.getValue().getAuthRefundCount();
                this.authRefundAmount = this.authRefundAmount + niblCardSchemeTotals.getValue().getAuthRefundAmount();
                this.voidCount = this.voidCount + niblCardSchemeTotals.getValue().getVoidCount();
                this.voidAmount = this.voidAmount + niblCardSchemeTotals.getValue().getVoidAmount();
            }
            totalsBlockString = this.prepareTotalsBlock();
        }
        return totalsBlockString;
    }

    private String prepareTotalsBlock() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(StringUtils.ofRequiredLength(salesCount, 3));
        stringBuilder.append(StringUtils.ofRequiredLength(salesAmount, 12));
        stringBuilder.append(StringUtils.ofRequiredLength(salesRefundCount, 3));
        stringBuilder.append(StringUtils.ofRequiredLength(salesRefundAmount, 12));
        stringBuilder.append(StringUtils.ofRequiredLength(debitCount, 3));
        stringBuilder.append(StringUtils.ofRequiredLength(debitAmount, 12));
        stringBuilder.append(StringUtils.ofRequiredLength(debitRefundCount, 3));
        stringBuilder.append(StringUtils.ofRequiredLength(debitRefundAmount, 12));
        stringBuilder.append(StringUtils.ofRequiredLength(authCount, 3));
        stringBuilder.append(StringUtils.ofRequiredLength(authAmount, 12));
        stringBuilder.append(StringUtils.ofRequiredLength(authRefundCount, 3));
        stringBuilder.append(StringUtils.ofRequiredLength(authRefundAmount, 12));
        return stringBuilder.toString();
    }
}
