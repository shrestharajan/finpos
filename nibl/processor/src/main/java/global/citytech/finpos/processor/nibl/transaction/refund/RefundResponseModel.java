package global.citytech.finpos.processor.nibl.transaction.refund;

import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;

/**
 * Created by Unique Shakya on 9/28/2020.
 */
public class RefundResponseModel extends TransactionResponseModel {

    private String debugRequestMessage;
    private String debugResponseMessage;
    private String message;
    private boolean isApproved;
    private String stan;
    private boolean shouldPrintCustomerCopy;

    public RefundResponseModel(RefundResponseModel.Builder builder) {
        this.debugRequestMessage = builder.debugRequestMessage;
        this.debugResponseMessage = builder.debugResponseMessage;
        this.isApproved = builder.isApproved;
        this.shouldPrintCustomerCopy = builder.shouldPrintCustomerCopy;
        this.message = builder.message;
        this.stan = builder.stan;
    }

    public boolean shouldPrintCustomerCopy() {
        return shouldPrintCustomerCopy;
    }

    public String getMessage() {
        return message;
    }

    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public String getStan() {
        return stan;
    }

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private String message;
        private String stan;
        private boolean isApproved;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static RefundResponseModel.Builder createDefaultBuilder() {
            return new RefundResponseModel.Builder();
        }

        public RefundResponseModel.Builder debugRequestMessage(String debugRequestString) {
            this.debugRequestMessage = debugRequestString;
            return this;
        }

        public RefundResponseModel.Builder debugResponseMessage(String debugResponseString) {
            this.debugResponseMessage = debugResponseString;
            return this;
        }

        public RefundResponseModel.Builder message(String message) {
            this.message = message;
            return this;
        }

        public RefundResponseModel.Builder stan(String stan) {
            this.stan = stan;
            return this;
        }

        public RefundResponseModel.Builder isApproved(boolean isApproved) {
            this.isApproved = isApproved;
            return this;
        }

        public RefundResponseModel.Builder shouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public RefundResponseModel build() {
            return new RefundResponseModel(this);
        }
    }
}
