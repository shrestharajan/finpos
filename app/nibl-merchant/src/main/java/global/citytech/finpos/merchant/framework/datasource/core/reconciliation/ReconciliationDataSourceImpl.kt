package global.citytech.finpos.merchant.framework.datasource.core.reconciliation


import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.reconciliation.ReconciliationDataSource
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.model.reconciliation.ReconciliationRequestEntity
import global.citytech.finpos.merchant.domain.model.reconciliation.ReconciliationResponseEntity
import global.citytech.finpos.merchant.domain.model.reconciliation.check.CheckReconciliationRequestEntity
import global.citytech.finpos.merchant.domain.model.reconciliation.clear.BatchClearRequestEntity
import global.citytech.finpos.merchant.domain.model.reconciliation.clear.BatchClearResponseEntity
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.ReconciliationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.DeviceConfigurationSourceImpl
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.presentation.data.mapToBatchClearResponseUIModel
import global.citytech.finpos.merchant.presentation.data.mapToCheckReconciliationResponseUIModel
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.data.mapToReconciliationUIModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finposframework.switches.ProcessorManager
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/23/2020.
 */
class ReconciliationDataSourceImpl : ReconciliationDataSource {

    override fun reconcile(
        configurationItem: ConfigurationItem,
        settlementActive: Boolean
    ): Observable<ReconciliationResponseEntity> {

        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val notifier = NotificationHandler
        val reconciliationRepository = ReconciliationRepositoryImpl()
        val printerService = PrinterSourceImpl(NiblMerchant.INSTANCE)
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
        val transactionRepository = TransactionRepositoryImpl(NiblMerchant.INSTANCE)
        val keyService = DeviceConfigurationSourceImpl(NiblMerchant.INSTANCE).keyService
        val reconciliationRequestEntity = ReconciliationRequestEntity(
            reconciliationRepository,
            printerService,
            applicationRepository,
            transactionRepository,
            NiblMerchant.INSTANCE.packageName,
            keyService,
            settlementActive
        )
        val reconciliationRequester =
            ProcessorManager.getInterface(terminalRepository, notifier)
                .reconciliationRequester
        return Observable.fromCallable {
            (reconciliationRequester.execute(reconciliationRequestEntity.mapToModel())).mapToReconciliationUIModel()
        }
    }

    override fun checkReconciliationRequired(configurationItem: ConfigurationItem): Observable<Boolean> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
        val reconciliationRepository = ReconciliationRepositoryImpl()
        val checkReconciliationRequestEntity =
            CheckReconciliationRequestEntity(reconciliationRepository, applicationRepository)
        val checkReconciliationRequester =
            ProcessorManager.getInterface(terminalRepository, NotificationHandler)
                .checkReconciliationRequester
        val localDataUseCase =
            LocalDataUseCase(LocalRepositoryImpl(LocalDatabaseSourceImpl(), PreferenceManager))
        return Observable.fromCallable {
            val isReconciliationRequired =
                checkReconciliationRequester.execute(checkReconciliationRequestEntity.mapToModel())
                    .mapToCheckReconciliationResponseUIModel()
                    .isReconciliationRequired
            isReconciliationRequired || localDataUseCase.isSettlementActive(false)
        }
    }

    override fun clearBatch(configurationItem: ConfigurationItem): Observable<BatchClearResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val reconciliationRepository = ReconciliationRepositoryImpl()
        val printerService = PrinterSourceImpl(NiblMerchant.INSTANCE, true)
        val transactionRepository = TransactionRepositoryImpl(NiblMerchant.INSTANCE)
        val batchClearRequestEntity =
            BatchClearRequestEntity(reconciliationRepository, printerService, transactionRepository)
        val batchClearRequester =
            ProcessorManager.getInterface(terminalRepository, NotificationHandler)
                .batchClearRequester
        return Observable.fromCallable {
            (batchClearRequester.execute(batchClearRequestEntity.mapToModel())).mapToBatchClearResponseUIModel()
        }
    }
}