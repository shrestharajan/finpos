package global.citytech.finpos.merchant.presentation.model.cashadvance

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

/**
 * Created by Unique Shakya on 2/4/2021.
 */
data class CashAdvanceRequestItem(
    val amount: BigDecimal?,
    val transactionType: TransactionType?
) {
}