package global.citytech.neps.data.db.dao

import android.database.Cursor
import androidx.room.*
import global.citytech.neps.domain.model.EmvKey
import global.citytech.neps.domain.model.EmvParam

@Dao
interface EmvParamDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(emvParams: List<EmvParam>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(emvParam: EmvParam): Long

    @Query("SELECT * FROM emv_param")
    fun get(): Cursor

    @Query("SELECT * FROM emv_param")
    fun getEmvParams(): List<EmvParam>

    @Query("SELECT * FROM emv_param where id = :id")
    fun getById(id: Long): Cursor?

    @Update
    fun update(emvParam: EmvParam):Int

    @Query("DELETE FROM emv_param WHERE id = :id")
    fun deleteById(id: Long): Int

}