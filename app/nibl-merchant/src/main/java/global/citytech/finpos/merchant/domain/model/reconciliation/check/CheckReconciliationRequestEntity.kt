package global.citytech.finpos.merchant.domain.model.reconciliation.check

import global.citytech.finposframework.repositories.ApplicationRepository
import global.citytech.finposframework.repositories.ReconciliationRepository

data class CheckReconciliationRequestEntity(
    val reconciliationRepository: ReconciliationRepository,
    val applicationRepository: ApplicationRepository
)