package global.citytech.neps.presentation.merchant

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.neps.data.device.configuration.ConfigurationImpl
import global.citytech.neps.data.repository.local.LocalRepositoryImpl
import global.citytech.neps.data.repository.core.configuration.ConfigurationRepositoryImpl
import global.citytech.neps.datasource.local.LocalConfigurationDataSourceImpl
import global.citytech.neps.domain.usecase.core.configuration.CoreConfigurationUseCase
import global.citytech.neps.domain.usecase.device.configuration.DeviceConfigurationUseCase
import global.citytech.neps.domain.usecase.local.LocalDataUseCase
import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.neps.presentation.model.mapToPresentation
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MerchantViewModel(val context:Application) : BaseAndroidViewModel(context) {

    val logonResponse by lazy { MutableLiveData<Boolean>() }
    val exchangeKeysResponse by lazy { MutableLiveData<Boolean>() }
    val configurationItem by lazy { MutableLiveData<ConfigurationItem>() }
    var configurationUseCase:CoreConfigurationUseCase=
        CoreConfigurationUseCase(
            ConfigurationRepositoryImpl(
                global.citytech.neps.datasource.core.configuration.ConfigurationDataSourceImpl()
            )
        )

    var deviceConfigurationUseCase : DeviceConfigurationUseCase =
        DeviceConfigurationUseCase(ConfigurationImpl(context))

    var localDataUseCase: LocalDataUseCase =
        LocalDataUseCase(
            LocalRepositoryImpl(LocalConfigurationDataSourceImpl())
        )


    fun getConfigurationItem() {
        compositeDisposable.add(localDataUseCase.getWholeConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.mapToPresentation() }
            .subscribe({ configurationItem.value = it }, { message.value = it.message })
        )
    }

    fun logOn(configurationItem: ConfigurationItem) {
        isLoading.value = true
        compositeDisposable.add(
            configurationUseCase.logOn(configurationItem)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    logonResponse.value = true
                }, {
                    isLoading.value = false
                    message.value = it.message
                })
        )
    }

    fun exchangeKeys(configurationItem: ConfigurationItem) {
        isLoading.value = true
        compositeDisposable.add(
            configurationUseCase.keyExchange(configurationItem)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    this.injectIntoDevice(it.communicationKey)
                }, {
                    isLoading.value = false
                    message.value = it.message
                })
        )
    }

    private fun injectIntoDevice(communicationKey: String) {
        compositeDisposable.add(
            deviceConfigurationUseCase.injectKey(communicationKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    exchangeKeysResponse.value = true
                },{
                    isLoading.value = false
                    message.value = it.message
                })
        )
    }
}