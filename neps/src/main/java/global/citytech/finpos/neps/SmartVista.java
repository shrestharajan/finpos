package global.citytech.finpos.neps;

import global.citytech.finpos.neps.usecases.keyexchange.MerchantKeyExchangeUseCase;
import global.citytech.finpos.neps.usecases.logon.MerchantLogOnUseCase;
import global.citytech.finpos.neps.usecases.purchase.PurchaseUseCase;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.switches.Processor;
import global.citytech.finposframework.switches.ProcessorManager;
import global.citytech.finposframework.switches.auth.completion.AuthorisationCompletionRequester;
import global.citytech.finposframework.switches.autoreversal.AutoReversalRequester;
import global.citytech.finposframework.switches.cashadvance.CashAdvanceRequester;
import global.citytech.finposframework.switches.cashadvance.ManualCashAdvanceRequester;
import global.citytech.finposframework.switches.keyexchange.KeyExchangeRequester;
import global.citytech.finposframework.switches.logon.LogOnRequester;
import global.citytech.finposframework.switches.posmode.PosModeRequester;
import global.citytech.finposframework.switches.preauths.ManualPreAuthRequester;
import global.citytech.finposframework.switches.preauths.PreAuthRequester;
import global.citytech.finposframework.switches.purchase.IdlePurchaseRequester;
import global.citytech.finposframework.switches.purchase.ManualPurchaseRequester;
import global.citytech.finposframework.switches.purchase.PurchaseRequester;
import global.citytech.finposframework.switches.receipt.customercopy.CustomerCopyRequester;
import global.citytech.finposframework.switches.receipt.detailreport.DetailReportRequester;
import global.citytech.finposframework.switches.receipt.duplicatereceipt.DuplicateReceiptRequester;
import global.citytech.finposframework.switches.receipt.duplicatereconciliation.DuplicateReconciliationReceiptRequester;
import global.citytech.finposframework.switches.receipt.summaryreport.SummaryReportRequester;
import global.citytech.finposframework.switches.receipt.tmslogs.TmsLogsRequester;
import global.citytech.finposframework.switches.reconciliation.ReconciliationRequester;
import global.citytech.finposframework.switches.reconciliation.check.CheckReconciliationRequester;
import global.citytech.finposframework.switches.reconciliation.clear.BatchClearRequester;
import global.citytech.finposframework.switches.refund.ManualRefundRequester;
import global.citytech.finposframework.switches.refund.RefundRequester;
import global.citytech.finposframework.switches.tipadjustment.TipAdjustmentRequester;
import global.citytech.finposframework.switches.transactiontype.TransactionTypeRequester;
import global.citytech.finposframework.switches.voidsale.VoidSaleRequester;


/**
 * @author rajudhital on 4/3/20
 */
public class SmartVista implements Processor {

    static {
        ProcessorManager.register(new SmartVista());
    }

    private TerminalRepository terminalRepository;
    private Notifier notifier;

    @Override
    public void init(TerminalRepository terminalRepository, Notifier notifier) {
        if (terminalRepository != null) {
            this.terminalRepository = terminalRepository;
        }
        if (notifier != null)
            this.notifier = notifier;
    }

    @Override
    public LogOnRequester getLogOnRequest() {
        return new MerchantLogOnUseCase(this.terminalRepository);
    }

    @Override
    public KeyExchangeRequester getKeyExchangeRequester() {
        return new MerchantKeyExchangeUseCase(this.terminalRepository);
    }

    @Override
    public PurchaseRequester getPurchaseRequester() {
        return new PurchaseUseCase(this.terminalRepository);
    }

    @Override
    public ManualPurchaseRequester getManualPurchaseRequester() {
        return null;
    }

    @Override
    public IdlePurchaseRequester getIdlePurchaseRequester() {
        return null;
    }

    @Override
    public PreAuthRequester getPreAuthRequester() {
        return null;
    }

    @Override
    public ManualPreAuthRequester getManualPreAuthRequester() {
        return null;
    }

    @Override
    public CustomerCopyRequester getCustomerCopyRequester() {
        return null;
    }

    @Override
    public ReconciliationRequester getReconciliationRequester() {
        return null;
    }

    @Override
    public VoidSaleRequester getVoidSaleRequester() {
        return null;
    }

    @Override
    public RefundRequester getRefundRequester() {
        return null;
    }

    @Override
    public ManualRefundRequester getManualRefundRequester() {
        return null;
    }

    @Override
    public AuthorisationCompletionRequester getAuthorisationCompletionRequester() {
        return null;
    }

    @Override
    public TmsLogsRequester getTmsLogsRequester() {
        return null;
    }

    @Override
    public TransactionTypeRequester getTransactionTypeRequester() {
        return null;
    }

    @Override
    public DuplicateReceiptRequester getDuplicateReceiptRequester() {
        return null;
    }

    @Override
    public DuplicateReconciliationReceiptRequester getDuplicateReconciliationReceiptRequester() {
        return null;
    }

    @Override
    public DetailReportRequester getDetailReportRequester() {
        return null;
    }

    @Override
    public SummaryReportRequester getSummaryReportRequester() {
        return null;
    }

    @Override
    public PosModeRequester getPosModeRequester() {
        return null;
    }

    @Override
    public CashAdvanceRequester getCashAdvanceRequester() {
        return null;
    }

    @Override
    public ManualCashAdvanceRequester getManualCashAdvanceRequester() {
        return null;
    }

    @Override
    public TipAdjustmentRequester getTipAdjustmentRequester() {
        return null;
    }

    @Override
    public AutoReversalRequester getAutoReversalRequester() {
        return null;
    }

    @Override
    public CheckReconciliationRequester getCheckReconciliationRequester() {
        return null;
    }

    @Override
    public BatchClearRequester getBatchClearRequester() {
        return null;
    }
}
