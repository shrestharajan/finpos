package global.citytech.finposframework.switches.autoreversal;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 6/22/2021.
 */
public interface AutoReversalRequester<T extends AutoReversalRequestParameter, E extends AutoReversalResponseParameter>
        extends Request<T, E> {
}
