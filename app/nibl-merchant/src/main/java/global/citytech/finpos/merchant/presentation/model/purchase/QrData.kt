package global.citytech.finpos.merchant.presentation.model.purchase

class QrData(
    val merchantId: String = "",
    val terminalId: String = "",
    val rrn: String = "",
    val stan: String = "",
    val transactionDate: String = "",
    val transactionTime: String = "",
    val transactionType: String = ""
) {
}