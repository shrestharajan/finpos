package global.citytech.finpos.processor.nibl.transaction;

import global.citytech.finpos.nibl.iso8583.ProcessingCode;

public enum TransactionType {
    //TODO may need to change order
    PURCHASE(0, ProcessingCode.PURCHASE.getCode()),
    LOG_ON(1, ProcessingCode.LOG_ON.getCode()),
    PURCHASE_WITH_CASHBACK(2, ProcessingCode.PURCHASE_WITH_CASHBACK.getCode()),
    PRE_AUTH(3, ProcessingCode.PRE_AUTHORIZATION.getCode()),
    REFUND(4, ProcessingCode.REFUND.getCode()),
    CASH_ADVANCE(5, ProcessingCode.CASH_ADVANCE.getCode()),
    PRE_AUTH_COMPLETION(6, ProcessingCode.PRE_AUTHORIZATION_COMPLETION.getCode()),
    REVERSAL(7, ProcessingCode.REVERSAL.getCode()),
    SETTLEMENT(8, ProcessingCode.SETTLEMENT.getCode()),
    VOID(9, ProcessingCode.VOID_SALE.getCode()),
    TIP_ADJUSTMENT(10, ProcessingCode.TIP_ADJUSTMENT.getCode()),
    GREEN_PIN(11, ProcessingCode.GREEN_PIN.getCode()),
    PIN_CHANGE(12, ProcessingCode.PIN_CHANGE.getCode()),
    BALANCE_INQUIRY(13, ProcessingCode.BALANCE_INQUIRY.getCode()),
    CASH_IN(15, ProcessingCode.CASH_IN.getCode()),
    MINI_STATEMENT(14, ProcessingCode.MINI_STATEMENT.getCode());

    private int offset;
    private String processingCode;

    TransactionType(int offset, String processingCode) {
        this.offset = offset;
        this.processingCode = processingCode;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public int getOffset() {
        return offset;
    }
}