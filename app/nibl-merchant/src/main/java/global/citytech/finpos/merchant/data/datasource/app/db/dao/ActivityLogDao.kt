package global.citytech.finpos.merchant.data.datasource.app.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import global.citytech.finpos.merchant.domain.model.app.ActivityLog


/**
 * @author BikashShrestha
 * @project finpos
 * @created 2021/11/03 - 11:51 AM
*/

@Dao
interface ActivityLogDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(activityLof: ActivityLog): Long

    @Query("SELECT * FROM ${ActivityLog.TABLE_NAME}")
    fun getActivityLogs(): List<ActivityLog>


    @Query("DELETE FROM ${ActivityLog.TABLE_NAME}")
    fun nukeTable()

    @Query("DELETE FROM ${ActivityLog.TABLE_NAME} where ${ActivityLog.COLUMN_ID} in (:ids)")
    fun deleteByIds(ids: List<String>)
}