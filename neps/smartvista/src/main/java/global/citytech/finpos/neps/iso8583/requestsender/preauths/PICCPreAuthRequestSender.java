package global.citytech.finpos.neps.iso8583.requestsender.preauths;

import global.citytech.finpos.neps.iso8583.requestsender.cardtypesenders.PICCISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.CardTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionTypeISOMessageUpdater;
import global.citytech.finpos.neps.iso8583.requestsender.transactiontypesenders.PreAuthIsoMessageUpdater;
import global.citytech.finposframework.iso8583.RequestContext;

/**
 * Created by Saurav Ghimire on 2/1/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class PICCPreAuthRequestSender extends PreAuthRequestSender {
    public PICCPreAuthRequestSender(RequestContext context) {
        super( context);
    }

    @Override
    protected String getClassName() {
        return PICCPreAuthRequestSender.class.getName();
    }

    @Override
    protected CardTypeISOMessageUpdater getCardTypeSender() {
        return new PICCISOMessageUpdater();
    }

}
