package global.citytech.finpos.device.qualcomm.io.printer

import android.text.Layout
import global.citytech.finposframework.hardware.io.printer.Style

/**
 * Created by BikashShrestha on 2/28/19.
 */
class PrinterUtils private constructor() {
    companion object {
        fun getAlignmentForBitmap(alignment: Style.Align?): Layout.Alignment {
            return when (alignment) {
                Style.Align.CENTER -> Layout.Alignment.ALIGN_CENTER
                Style.Align.RIGHT -> Layout.Alignment.ALIGN_OPPOSITE
                else -> Layout.Alignment.ALIGN_NORMAL
            }
        }
    }

    init {
        throw IllegalStateException("Printer Utils has collection of static members only, instantiating not allowed.")
    }
}