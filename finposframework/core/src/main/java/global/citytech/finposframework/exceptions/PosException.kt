package global.citytech.finposframework.exceptions

/**
 * Created by Rishav Chudal on 6/19/20.
 */
class PosException (val posError: PosError) : RuntimeException(posError.errorMessage){

}