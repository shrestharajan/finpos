package global.citytech.finpos.merchant.domain.model.receipt.detailreport

import global.citytech.finposframework.hardware.io.printer.PrinterService
import global.citytech.finposframework.repositories.ApplicationRepository
import global.citytech.finposframework.repositories.ReconciliationRepository
import global.citytech.finposframework.repositories.TransactionRepository

/**
 * Created by Saurav Ghimire on 3/29/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


data class DetailReportRequestEntity(
    val transactionRepository: TransactionRepository? = null,
    val printerService: PrinterService? = null,
    var printSummaryReport: Boolean = false,
    val applicationRepository: ApplicationRepository? =null,
    val reconciliationRepository: ReconciliationRepository? =null
)
