package global.citytech.finpos.merchant.presentation.dynamicqr.base

import android.app.Application
import android.os.CountDownTimer
import android.util.Log
import androidx.lifecycle.MutableLiveData
import global.citytech.common.Base64
import global.citytech.common.data.Response
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.QrPaymentRepositoryImpl
import global.citytech.finpos.merchant.data.repository.device.DeviceRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.QrPayment
import global.citytech.finpos.merchant.domain.model.app.QrStatus
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.app.QrPaymentUseCase
import global.citytech.finpos.merchant.domain.usecase.device.DeviceUseCase
import global.citytech.finpos.merchant.domain.usecase.qr.fonepayqr.FonepayQrGenerateRequest
import global.citytech.finpos.merchant.domain.usecase.qr.nqr.NqrGenerateRequest
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.app.QrPaymentDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.DeviceConfigurationSourceImpl
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.dynamicqr.QrConst.QR_TYPE_FONEPAY
import global.citytech.finpos.merchant.presentation.dynamicqr.QrConst.QR_TYPE_NQR
import global.citytech.finpos.merchant.presentation.dynamicqr.QrType
import global.citytech.finpos.merchant.presentation.dynamicqr.nqrsale.DynamicNQrActivity
import global.citytech.finpos.merchant.presentation.dynamicqr.receipt.DynamicQrReceiptGenerator
import global.citytech.finpos.merchant.presentation.dynamicqr.receipt.DynamicQrTransactionReceiptPrintHandler
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.presentation.model.qr.QrPaymentStatusRequest
import global.citytech.finpos.merchant.presentation.model.qr.QrPaymentStatusResponse
import global.citytech.finpos.merchant.presentation.model.qroperator.QrOperatorItem
import global.citytech.finpos.merchant.presentation.model.setting.QrConfiguration
import global.citytech.finpos.merchant.service.NetworkConnectionReceiver
import global.citytech.finpos.merchant.utils.AppConstant
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.SoundUtils
import global.citytech.finpos.merchant.utils.VoiceMessageUtils
import global.citytech.finpos.merchant.utils.getMinutesAndSecondsDataFromMillis
import global.citytech.finposframework.hardware.io.printer.PrinterResponse
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.repositories.TerminalRepository
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion
import global.citytech.payment.sdk.utils.StringUtils
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import kotlin.collections.ArrayList

abstract class DynamicQrBaseViewModel constructor(application: android.app.Application) :
    BaseAndroidViewModel(application) {
    private val logger = Logger.getLogger(DynamicQrBaseViewModel::class.java.name)
    val qrStatusCode by lazy { MutableLiveData<String>() }
    val qrStatusMessage by lazy { MutableLiveData<String>() }
    val showAlertDialog by lazy { MutableLiveData<Boolean>() }
    val appConfiguration by lazy { MutableLiveData<ConfigurationItem>() }
    val qrData by lazy { MutableLiveData<String>() }
    val transactionConfirmationLiveData by lazy { MutableLiveData<TransactionConfirmation>() }
    val noInternetConnection by lazy { MutableLiveData<Boolean>() }
    var updateTransactionConfirmation = false
    lateinit var terminalRepository: TerminalRepository
    private var qrPaymentStatusRequest: QrPaymentStatusRequest? = null
    var qrPayment: QrPayment? = null
    val printerService = PrinterSourceImpl(application.applicationContext)
    val qrConfigs by lazy { MutableLiveData<List<QrConfiguration>>() }
    val qrConfiguration by lazy { MutableLiveData<QrConfiguration>() }
    val qrOperator by lazy { MutableLiveData<QrOperatorItem>() }
    val terminalIsConfigured by lazy { MutableLiveData<Boolean>() }
    val defaultBankDisplayImage by lazy { MutableLiveData<Boolean>() }
    val bankDisplayImage by lazy { MutableLiveData<ByteArray>() }

    val qrPaymentUseCase = QrPaymentUseCase(
        QrPaymentRepositoryImpl(
            QrPaymentDataSourceImpl(application.applicationContext)
        )
    )

    var localDataUseCase: LocalDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    var deviceConfigurationUseCase = DeviceUseCase(
        DeviceRepositoryImpl(DeviceConfigurationSourceImpl(application))
    )

    fun getTerminalConfiguredStatusAndProceed() {
        this.terminalIsConfigured.value =
            localDataUseCase.isTerminalConfigured(defaultValue = false)
    }

    private fun getQrConfigurationByName(
        qrConfigurationList: List<QrConfiguration>,
        qrType: QrType
    ): QrConfiguration? {
        for (i in qrConfigurationList.indices) {
            qrType.codes.forEach {
                if (it.equals(qrConfigurationList[i].qrOperatorInfo.name, true))
                    return qrConfigurationList[i]
            }
        }
        return null
    }

    private val qrTransactionWaitingTimer =
        object : CountDownTimer(AppConstant.QR_TRANSACTION_WAITING_INTERVAL_MILLIS, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                logger.debug(
                    "Time left show Qr dialog ::: "
                        .plus(millisUntilFinished.getMinutesAndSecondsDataFromMillis())
                )

            }

            override fun onFinish() {
                enableAlertDialogFlag()
            }
        }

    fun getConfiguration(): Boolean {
        this.isLoading.value = true
        return compositeDisposable.add(
            localDataUseCase.getWholeConfiguration()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe({
                    isLoading.value = false
                    appConfiguration.value = it
                    terminalRepository = TerminalRepositoryImpl(appConfiguration.value)
                    NetworkConnectionReceiver.networkConfiguration = it
                    doCleanUpAtStart()
                    retrieveBankDisplayImage(it)
                },
                    {
                        this.isLoading.value = false
                        message.value = it.message
                    })
        )
    }

    fun checkQrPaymentStatus(request: Any, sleepTime: Long, type: String) {
        compositeDisposable.add(
            Single.create(SingleOnSubscribe<QrPaymentStatusResponse> {
                qrPaymentStatusSubscribe(it, sleepTime, request, type)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    qrStatusMessage.value = QrStatus.QR000.message
                    qrStatusCode.value = QrStatus.QR000.code
                    showAlertDialog.value = false
                    noInternetConnection.value = false

                    val isApproved = !StringUtils.isEmpty(it.authorizationCode)
                    updateTransactionConfirmation = false

                    transactionConfirmationLiveData.value = TransactionConfirmation.Builder()
                        .amount(
                            global.citytech.finposframework.utility.StringUtils.formatAmountTwoDecimal(
                                it.amount
                            )
                        )
                        .imageId(if (isApproved) R.drawable.approved else R.drawable.declined)
                        .message(if (isApproved) "Approval Code: ${it.authorizationCode}" else "DECLINED")
                        .title(if (isApproved) "APPROVED" else "DECLINED")
                        .positiveLabel(""/*"PRINT CUSTOMER COPY"*/)
                        .negativeLabel(""/*"RETURN TO DASHBOARD"*/)
                        .isApprove(isApproved)
                        .qrImageString("")
                        .transactionStatus("")
                        .build()

                    storeTransactionLog(isApproved, it)

                    playSound(isApproved, it.amount?.toDouble())

                }, {
                    it.printStackTrace()
                    when {
                        it.localizedMessage.contains(QrStatus.QR001.code) -> {
                            qrStatusMessage.value = QrStatus.QR001.message
                            qrStatusCode.value = QrStatus.QR001.code
                        }

                        it.localizedMessage.contains(QrStatus.QR002.code) -> {
                            qrStatusMessage.value = QrStatus.QR002.message
                            qrStatusCode.value = QrStatus.QR002.code
                        }

                        it.localizedMessage.contains(QrStatus.QR003.code) -> {
                            qrStatusMessage.value = QrStatus.QR003.message
                            qrStatusCode.value = QrStatus.QR003.code
                        }
                    }
                    noInternetConnection.value = it.localizedMessage?.contains("404")!!
                    restartQrPaymentStatusCheck(request, type)
                })
        )
    }


    private fun restartQrPaymentStatusCheck(request: Any, type: String) {
        if (!compositeDisposable.isDisposed)
            checkQrPaymentStatus(request, 2000, type)
    }

    private fun storeTransactionLog(
        isApproved: Boolean,
        qrPaymentStatusResponse: QrPaymentStatusResponse
    ) {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<QrPayment> {
                val responseDateTimeStamp = qrPaymentStatusResponse.localDateTime
                val transactionStatus = if (isApproved) "APPROVED" else "DECLINED"
                qrPayment = QrPayment(
                    merchantId = qrPaymentStatusResponse.merchantId,
                    terminalId = qrPaymentStatusResponse.terminalId,
                    referenceNumber = qrPaymentStatusResponse.rrn,
                    invoiceNumber = "",
                    transactionType = "SALE",
                    transactionDate = responseDateTimeStamp.substring(0, 10),
                    transactionTime = responseDateTimeStamp.substring(10),
                    transactionCurrency = qrConfiguration.value?.currency.toString(),
                    transactionAmount = BigDecimal(qrPaymentStatusResponse.amount),
                    transactionStatus = transactionStatus,
                    approvalCode = qrPaymentStatusResponse.authorizationCode,
                    paymentInitiator = qrPaymentStatusResponse.network,
                    initiatorId = qrPaymentStatusResponse.payerPAN
                )
                qrPayment?.let { content ->
                    qrPaymentUseCase.addQrPayment(content)
                    it.onNext(content)
                }
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    qrPayment = it
                    printPaymentReceipt(ReceiptVersion.MERCHANT_COPY)
                }, {
                    it.printStackTrace()
                })
        )
    }

    fun printPaymentReceipt(
        receiptVersion: ReceiptVersion
    ) {
        stopTimer()
        if (receiptVersion == ReceiptVersion.CUSTOMER_COPY)
            isLoading.value = true
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<PrinterResponse> {
                val dynamicReceipt =
                    qrPayment?.let { it1 ->
                        DynamicQrReceiptGenerator(terminalRepository).generate(
                            it1
                        )
                    }
                val printerResponse =
                    dynamicReceipt?.let { it1 ->
                        DynamicQrTransactionReceiptPrintHandler(printerService).printReceipt(
                            it1,
                            receiptVersion
                        )
                    }
                if (printerResponse != null) {
                    it.onNext(printerResponse)
                }
                it.onComplete()
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    updateTransactionConfirmation = true
                    if (receiptVersion == ReceiptVersion.MERCHANT_COPY) {

                        val transactionConfirmation = transactionConfirmationLiveData.value
                        transactionConfirmation!!.positiveLabel = "PRINT CUSTOMER COPY"
                        transactionConfirmation.negativeLabel = "RETURN TO DASHBOARD"
                        transactionConfirmationLiveData.value = transactionConfirmation
                    } else {
                        message.value = it.message
                    }
                }, {
                    isLoading.value = false
                    it.printStackTrace()
                })
        )
    }

    private fun qrPaymentStatusSubscribe(
        it: SingleEmitter<QrPaymentStatusResponse>,
        sleepTime: Long,
        request: Any,
        type: String
    ) {
        Thread.sleep(sleepTime)
        if (type == QR_TYPE_NQR) {
            qrPaymentStatusRequest = generateNqrRequest(request as NqrGenerateRequest)
        } else if (type == QR_TYPE_FONEPAY) {
            qrPaymentStatusRequest = generateFonepayQrRequest(request as FonepayQrGenerateRequest)
        }
//        else if (type == QR_TYPE_SMARTQR) {
//            qrPaymentStatusRequest = generateSmartQrRequest(request as SmartQrGenerateRequest)
//        }
        val jsonRequest = Jsons.toJsonObj(qrPaymentStatusRequest)
        val jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.post(
            "/device-payment-processor-endpoints/reports/status/",
            jsonRequest,
            true
        )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)

        when {
            response.data != null -> {
                val responseDataJson = Jsons.toJsonObj(response.data)
                it.onSuccess(
                    Jsons.fromJsonToObj(
                        responseDataJson,
                        QrPaymentStatusResponse::class.java
                    )
                )
            }

            response.message == "Please check your internet connection and try again." -> {
                it.onError(Exception("404 ${response.message}"))
            }

            response.code == QrStatus.QR001.code -> {
                it.onError(Exception("${response.code}. ${response.message}"))
            }

            response.code == QrStatus.QR002.code -> {
                it.onError(Exception("${response.code}. ${response.message}"))
            }

            response.code == QrStatus.QR003.code -> {
                it.onError(Exception("${response.code}. ${response.message}"))
            }

            else -> {
                it.onError(Exception("Error Code :: ${response.code}. ${response.message}"))
            }
        }
    }

    fun getQrOperators(qrType: QrType) {
        val terminalSettingsResponse = this.localDataUseCase.getTerminalSetting()
        val qrOperatorList = ArrayList<QrOperatorItem>()
        qrConfigs.value = terminalSettingsResponse.qrConfigs
        terminalSettingsResponse.qrConfigs
            ?.forEach { it ->
                run {
                    if (it.enableDynamicQr && it.qrOperatorInfo.active) {
                        qrOperatorList.add(it.qrOperatorInfo)
                    }
                }
            }
        qrConfiguration.value =
            terminalSettingsResponse.qrConfigs?.let { getQrConfigurationByName(it, qrType) }
    }

    private fun generateFonepayQrRequest(fonepayQrGenerateRequest: FonepayQrGenerateRequest): QrPaymentStatusRequest {
        val qrPaymentStatusRequest = QrPaymentStatusRequest(
            fonepayQrGenerateRequest.merchantId,
            fonepayQrGenerateRequest.terminalId,
            fonepayQrGenerateRequest.merchantBillNumber
        )
        Log.d("FONEPAY", "Check payment status: " + Jsons.toJsonObj(qrPaymentStatusRequest))
        return qrPaymentStatusRequest
    }

    private fun generateNqrRequest(nqrGenerateRequest: NqrGenerateRequest): QrPaymentStatusRequest {
        return QrPaymentStatusRequest(
            nqrGenerateRequest.merchantCode,
            nqrGenerateRequest.terminalId,
            nqrGenerateRequest.billNumber
        )
    }

//    private fun generateSmartQrRequest(smartQrGenerateRequest: SmartQrGenerateRequest): QrPaymentStatusRequest {
//        return QrPaymentStatusRequest(
//            smartQrGenerateRequest.merchantId,
//            smartQrGenerateRequest.terminalId,
//            smartQrGenerateRequest.merchantBillNumber
//        )
//    }

    fun startTimer() {
        qrTransactionWaitingTimer.start()
    }

    fun stopTimer() {
        qrTransactionWaitingTimer.cancel()
    }

    fun enableAlertDialogFlag() {
        stopTimer()
        showAlertDialog.value = true
    }

    fun disableAlertDialogFlag() {
        showAlertDialog.value = false
    }

    fun dispose() {
        compositeDisposable.clear()
        compositeDisposable.dispose()
    }

    private fun clearCompose() {
        if (compositeDisposable != null) {
            compositeDisposable.clear()
        }
    }

    private fun doCleanUpAtStart() = Completable
        .fromAction { deviceConfigurationUseCase.closeUpIO() }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            this::onCleanUpComplete,
            this::onCleanUpError
        )

    private fun onCleanUpComplete() {
        this.isLoading.value = false
    }

    private fun onCleanUpError(throwable: Throwable) {
        this.isLoading.value = false
    }

    private fun retrieveBankDisplayImage(it: ConfigurationItem?) {
        it?.let { configItem ->
            configItem.logos?.let {
                if (it.isNotEmpty()) {
                    it[0].displayLogo?.let { s ->
                        retrieveBankDisplayImageByteArray(s)
                    } ?: run {
                        defaultBankDisplayImage.value = true
                    }
                } else {
                    defaultBankDisplayImage.value = true
                }
            } ?: run {
                defaultBankDisplayImage.value = true
            }
        } ?: run {
            defaultBankDisplayImage.value = true
        }
    }

    private fun retrieveBankDisplayImageByteArray(s: String) {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<ByteArray> {
                it.onNext(Base64.decode(s))
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    bankDisplayImage.value = it
                }, {
                    isLoading.value = false
                    defaultBankDisplayImage.value = true
                })
        )
    }

    private fun playSound(isApproved: Boolean, amount: Double) {
        if (isApproved) {
            /** APPROVED */
            if (localDataUseCase.isSoundMode(false))
                SoundUtils.play(
                    getApplication<Application?>().applicationContext, amount, "blank", AppUtility.getCurrencyCode()
                )
            Logger.getLogger(DynamicNQrActivity::class.simpleName)
                .debug("transaction approved")
        } else {
            /** DECLINED */
            if (localDataUseCase.isSoundMode(false))
                VoiceMessageUtils.playFail(getApplication<Application?>().applicationContext)
            Logger.getLogger(DynamicNQrActivity::class.simpleName)
                .debug("transaction declined")
        }
    }
}