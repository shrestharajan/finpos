package global.citytech.finpos.merchant.framework.datasource.core.receipt

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.receipt.CustomerCopyDataSource
import global.citytech.finpos.merchant.domain.model.receipt.CustomerCopyRequestEntity
import global.citytech.finpos.merchant.domain.model.receipt.CustomerCopyResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.data.mapToUiModel
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finposframework.switches.ProcessorManager
import global.citytech.finposframework.usecases.transaction.data.StatementList
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/2/2020.
 */
class CustomerCopyDataSourceImpl : CustomerCopyDataSource {

    override fun print(): Observable<CustomerCopyResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val transactionRepository =
            TransactionRepositoryImpl(
                NiblMerchant.INSTANCE
            )
        val printerService = PrinterSourceImpl(NiblMerchant.INSTANCE)
        val customerCopyRequestEntity = CustomerCopyRequestEntity.Builder.newInstance()
            .withPrinterService(printerService)
            .withTransactionRepository(transactionRepository)
            .build()
        val customerCopyRequester =
            ProcessorManager.getInterface(null, NotificationHandler).customerCopyRequester
        return Observable.fromCallable {
            (customerCopyRequester.execute(customerCopyRequestEntity.mapToModel()).mapToUiModel())
        }
    }

    override fun printStatement(statementList: StatementList): Observable<CustomerCopyResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
//        val inquiryList = InquiryList(inquiryList.miniStatementItems,"","","")
        val transactionRepository = TransactionRepositoryImpl(NiblMerchant.INSTANCE)
        val printerService = PrinterSourceImpl(NiblMerchant.INSTANCE)
        val miniStatementRequestEntity = CustomerCopyRequestEntity.Builder.newInstance()
            .withPrinterService(printerService)
            .withMiniStatementData(statementList)
            .withTransactionRepository(transactionRepository)
            .build()
        val miniStatementRequester = ProcessorManager.getInterface(null, NotificationHandler).statementPrintRequester
        return Observable.fromCallable{
            (miniStatementRequester.execute(miniStatementRequestEntity.mapToModel()).mapToUiModel())
        }
    }
}