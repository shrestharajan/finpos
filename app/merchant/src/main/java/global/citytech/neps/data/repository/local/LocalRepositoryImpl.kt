package global.citytech.neps.data.repository.local

import global.citytech.neps.data.datasource.local.LocalDataSource
import global.citytech.neps.domain.model.*
import global.citytech.neps.domain.repository.LocalRepository
import io.reactivex.Observable


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
class LocalRepositoryImpl(val configurationDataSource: LocalDataSource):LocalRepository {
    override fun getHost(): Observable<List<Host>> = configurationDataSource.getHost()
    override fun getMerchant(): Observable<List<Merchant>> = configurationDataSource.getMerchant()
    override fun getLogo(): Observable<List<Logo>> = configurationDataSource.getLogo()
    override fun getAidParams(): Observable<List<AidParam>> = configurationDataSource.getAidParams()
    override fun getCardScheme(): Observable<List<CardScheme>> = configurationDataSource.getCardScheme()
    override fun getEmvKeys(): Observable<List<EmvKey>> = configurationDataSource.getEmvKeys()
    override fun getEmvParams(): Observable<List<EmvParam>> = configurationDataSource.getEmvParams()
}
