package global.citytech.finposframework.switches.transaction;

import global.citytech.finposframework.switches.RequestParameter;
import global.citytech.finposframework.switches.ResponseParameter;

/**
 * Created by Saurav Ghimire on 2/3/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public interface TransactionResponseParameter extends ResponseParameter { }
