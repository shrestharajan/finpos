package global.citytech.finpos.merchant.presentation.model.response

class CardScheme {
    companion object {

        val CARD_SCHEME_TABLE_NAME = "card_scheme"

        const val COLUMN_ID = "id"
        const val COLUMN_CARD_SCHEME_ID = "card_scheme_id"
        const val COLUMN_ATTRIBUTE = "attribute"
        const val COLUMN_DISPLAY_LABEL = "display_label"
        const val COLUMN_PRINT_LABEL = "print_Label"
        const val COLUMN_VALUE = "value"
    }
}