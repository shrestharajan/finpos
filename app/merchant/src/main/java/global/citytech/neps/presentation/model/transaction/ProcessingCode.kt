package global.citytech.neps.presentation.model.transaction

enum class ProcessingCode(val code: String, val description: String) {
    PURCHASE_FROM_DEFAULT_ACCOUNT(
        "000000",
        "Purchase from Default Account"
    ),
    PURCHASE_WITH_SAVING_ACCOUNT(
        "001000",
        "Purchase with Saving Account"
    ),
    PURCHASE_WITH_CHECKING_ACCOUNT(
        "002000",
        "Purchase with Checking Account"
    ),
    PURCHASE_WITH_CREDIT_ACCOUNT(
        "003000",
        "Purchase with Credit Account"
    ),
    PURCHASE_WITH_LOYALTY_ACCOUNT(
        "006000",
        "Purchase with Loyalty Account"
    ),
    PURCHASE_WITH_OFFLIE_WALLET_ACCOUNT("007000", "Purchase with Offline Wallet Account");

    companion object {
        fun getByCode(code: String?): ProcessingCode {
            val dataTypes =
                values()
            val var3 = dataTypes.size
            for (var4 in 0 until var3) {
                val dataType = dataTypes[var4]
                if (dataType.code.equals(code, ignoreCase = true)) {
                    return dataType
                }
            }
            throw IllegalArgumentException(
                String.format(
                    " Code %d has not been mapped ",
                    code
                )
            )
        }
    }

}