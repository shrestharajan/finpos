package global.citytech.common.data

data class LabelValue(
    var displayLabel : String? = null,
    var value : Any? = null,
    var printLabel: String? = null

)