package global.citytech.finpos.merchant.presentation.card.check

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.finpos.merchant.framework.datasource.device.DeviceControllerImpl
import global.citytech.finpos.merchant.framework.datasource.device.SoundSourceImpl
import global.citytech.finposframework.hardware.io.sound.Sound
import global.citytech.finposframework.log.Logger
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Unique Shakya on 11/5/2020.
 */
class CardCheckViewModel(application: Application) : BaseAndroidViewModel(application) {

    private val logger  = Logger(CardCheckViewModel::class.java.name)
    val cardPresent by lazy { MutableLiveData<Boolean>() }

    private val deviceController = DeviceControllerImpl(application)
    private val soundSourceImpl = SoundSourceImpl(application)

    fun checkForCardPresent() {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Boolean> {
                isCardPresentTask(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    cardPresent.value = it
                }, {
                    it.printStackTrace()
                    checkForCardPresent()
                })
        )
    }

    private fun isCardPresentTask(it: ObservableEmitter<Boolean>) {
        it.onNext(deviceController.isIccCardPresent)
        it.onComplete()
    }

    fun playAlertSound() {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Boolean> {
                playAlertSoundTask(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    logger.log("::: PLAY SOUND SUCCESS :::")
                }, {
                    it.printStackTrace()
                })
        )
    }

    private fun playAlertSoundTask(it: ObservableEmitter<Boolean>) {
        soundSourceImpl.playSound(Sound.FAILURE)
        it.onNext(true)
        it.onComplete()
    }

}