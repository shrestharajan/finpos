package global.citytech.finpos.processor.nibl.logon;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.keymgmt.keys.KeyService;
import global.citytech.finposframework.switches.logon.LogOnRequestParameter;
import global.citytech.finposframework.supports.UseCase;

/***
 * @author Surajchhetry
 */
public class LogOnRequestModel implements UseCase.Request, LogOnRequestParameter {

    private final PrinterService printerService;
    private final KeyService hardwareKeyService;
    private final String applicationPackageName;

    public LogOnRequestModel(
            PrinterService printerService,
            KeyService keyService,
            String applicationPackageName
    ) {
        this.printerService = printerService;
        this.hardwareKeyService = keyService;
        this.applicationPackageName = applicationPackageName;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public KeyService getHardwareKeyService() {
        return hardwareKeyService;
    }

    public String getApplicationPackageName() {
        return applicationPackageName;
    }
}