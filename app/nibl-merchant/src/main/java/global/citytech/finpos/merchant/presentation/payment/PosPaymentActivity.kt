package global.citytech.finpos.merchant.presentation.payment

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.common.Constants
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityPaymentBinding
import global.citytech.finpos.merchant.presentation.autoreversal.AutoReversalActivity
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.model.purchase.BillingRequest
import global.citytech.finpos.merchant.presentation.purchase.PurchaseActivity
import global.citytech.finpos.merchant.presentation.settlement.SettlementActivity
import global.citytech.finpos.merchant.presentation.voidsale.VoidActivity
import global.citytech.finpos.merchant.utils.AppConstant.PAYMENT_TIME_OUT
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finpos.merchant.utils.setOnlineClickListener
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.receipt.transaction.VatInfo
import global.citytech.payment.sdk.api.PaymentResponse
import global.citytech.payment.sdk.api.PaymentResult
import global.citytech.payment.sdk.core.PaymentConstants
import global.citytech.payment.sdk.core.PosPaymentImpl.*
import kotlinx.android.synthetic.main.activity_payment.*

/**
 * Created by Unique Shakya on 8/18/2021.
 */
class PosPaymentActivity : AppBaseActivity<ActivityPaymentBinding, PosPaymentViewModel>() {

    private lateinit var viewModel: PosPaymentViewModel
    private var vendorName = ""
    private val logger = Logger(PosPaymentActivity::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        logger.log("PosPaymentActivity Instance ::: ".plus(PosPaymentActivity::class.java))
        registerReceiver(
            paymentCancellationBroadCastReceiver,
            IntentFilter(ACTION_CANCEL_PAYMENT_REQUEST)
        )
        viewModel = getViewModel()
        PaymentSdkSingleton.getInstance().isFromPaymentSdk = true
        this.setUpAndStartIdleTime(PAYMENT_TIME_OUT, 1000)
        initViews()
        initObservers()
        retrieveFromIntent()
    }

    private fun setUpAndStartIdleTime(idleTime: Long, countDownInterval: Int) {
        NiblMerchant.INSTANCE.setUpIdleTime(idleTime, countDownInterval.toLong())
        NiblMerchant.INSTANCE.startIdleTimeCountDown()
    }

    private fun initViews() {
        btn_payment_confirm.setOnlineClickListener {
            startTransactionActivity()
        }

        btn_payment_cancel.setOnClickListener {
            handleFailedPayment(PaymentResult.CANCEL)
        }

        iv_payment_back.setOnClickListener {
            handleFailedPayment(PaymentResult.CANCEL)
        }
    }

    private fun startTransactionActivity() {
        when (viewModel.paymentTransactionType) {
            TransactionType.PURCHASE -> startPurchaseActivity()
            TransactionType.VOID -> startVoidActivity()
            else -> viewModel.paymentResultLiveData.value =
                PaymentResult.POS_PAYMENT_SERVICE_NOT_AVAILABLE
        }
    }

    private fun startVoidActivity() {
        val intent = Intent(this, VoidActivity::class.java)
        intent.putExtra(Constants.EXTRA_FROM_CHECKOUT, true)
        intent.putExtra(
            Constants.EXTRA_REFERENCE_NUMBER,
            viewModel.paymentRequest!!.retrievalReferenceNumber
        )
        intent.putExtra(Constants.EXTRA_ADDITIONAL_DATA, viewModel.paymentRequest!!.additionalData)
        startActivityForResult(intent, Constants.TRANSACTION_REQUEST_CODE)
    }

    private fun startPurchaseActivity() {
        val intent = Intent(this, PurchaseActivity::class.java)
        intent.putExtra(Constants.EXTRA_FROM_CHECKOUT, true)
        intent.putExtra(Constants.EXTRA_AMOUNT, viewModel.paymentRequest!!.transactionAmount)

        val billingRequest = Jsons.fromJsonToObj(
            viewModel.paymentRequest!!.additionalData,
            BillingRequest::class.java
        )

        intent.putExtra(
            Constants.EXTRA_VAT_INFO,
            Jsons.toJsonObj(
                VatInfo(
                    billingRequest.data.vat_amount,
                    billingRequest.data.vat_refund_amount
                )
            )
        )
        intent.putExtra(Constants.EXTRA_ADDITIONAL_DATA, viewModel.paymentRequest!!.additionalData)
        startActivityForResult(intent, Constants.TRANSACTION_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            AutoReversalActivity.REQUEST_CODE -> handleAutoReversalActivityResult(resultCode)
            SettlementActivity.REQUEST_CODE -> handleSettlementActivityResult(resultCode)
            Constants.TRANSACTION_REQUEST_CODE -> handleTransactionActivityResult(resultCode, data)
        }
    }

    private fun handleTransactionActivityResult(resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK)
            handleCompletedTransaction(data!!)
        else
            handleFailedTransaction(data!!)
    }

    private fun handleFailedTransaction(data: Intent) {
        data.extras?.let {
            val paymentResponse = PaymentResponse(PaymentResult.FAILED)
            paymentResponse.message = it.getString(Constants.EXTRA_MESSAGE)
            paymentResponse.resultCode = it.getInt(Constants.EXTRA_CODE)
            PaymentSdkSingleton.getInstance().updatePaymentResponse(paymentResponse)
            PaymentSdkSingleton.getInstance().returnActivityResultWithExtraData(this)
        }
    }

    private fun handleCompletedTransaction(data: Intent) {
        data.extras?.let {
            val paymentResponse: PaymentResponse
            if (it.getBoolean(Constants.EXTRA_APPROVED)) {
                paymentResponse = PaymentResponse(PaymentResult.SUCCESS)
                paymentResponse.authCode = it.getString(Constants.EXTRA_AUTH_CODE)
                paymentResponse.cardNumber = it.getString(Constants.EXTRA_CARD_NUMBER)
                paymentResponse.retrievalReferenceNumber = it.getString(Constants.EXTRA_RRN)
                paymentResponse.systemTraceAuditNumber = it.getString(Constants.EXTRA_STAN)
                paymentResponse.transactionTimeStamp =
                    it.getString(Constants.EXTRA_TRANSACTION_TIME)
                paymentResponse.paymentMode = it.getString(Constants.EXTRA_PAYMENT_MODE)
                paymentResponse.paymentNetwork = it.getString(Constants.EXTRA_PAYMENT_NETWORK)
            } else {
                paymentResponse = PaymentResponse(PaymentResult.DECLINED)
            }
            paymentResponse.message = it.getString(Constants.EXTRA_MESSAGE)
            PaymentSdkSingleton.getInstance().updatePaymentResponse(paymentResponse)
            PaymentSdkSingleton.getInstance().returnActivityResultWithExtraData(this)
        }
    }

    private fun handleSettlementActivityResult(resultCode: Int) {
        if (resultCode == Activity.RESULT_OK)
            showPaymentLayout()
        else
            handleFailedPayment(PaymentResult.POS_SETTLEMENT_FAILED)
    }

    private fun handleAutoReversalActivityResult(resultCode: Int) {
        if (resultCode == Activity.RESULT_OK)
            viewModel.checkSettlementTime()
        else
            handleFailedPayment(PaymentResult.POS_AUTO_REVERSAL_FAILED)
    }

    private fun retrieveFromIntent() {
        vendorName = intent.getStringExtra(PaymentConstants.EXTRA_VENDOR_NAME_DATA)!!
        val intentData = intent.getStringExtra(PaymentConstants.EXTRA_DATA)
        viewModel.checkPaymentRequestValidityAndProceed(intentData)
    }

    private fun initObservers() {
        viewModel.isLoading.observe(this, Observer {
            if (it)
                showProgress()
            else
                hideProgress()
        })

        NiblMerchant.INSTANCE.isTerminalConfigured.observe(this, Observer {
            if (it) {
                viewModel.getConfigurationItem()
            } else {
                viewModel.initializeSdk()
            }
        })

        viewModel.configurationItem.observe(this, Observer {
            viewModel.checkForAutoReversal()
        })

        viewModel.paymentResultLiveData.observe(this, Observer {
            if (it != PaymentResult.SUCCESS) {
                handleFailedPayment(it)
            }
        })

        viewModel.autoReversalPresent.observe(this, Observer {
            if (it)
                startAutoReversalActivity()
            else
                viewModel.checkSettlementTime()
        })

        viewModel.settlementTimeElapsed.observe(this, Observer {
            if (it)
                showStartSettlementConfirmationDialog()
            else
                showPaymentLayout()
        })
    }

    private fun showPaymentLayout() {
        tv_originator_value.text = vendorName
        tv_invoice_detail.text = viewModel.getInvoiceDetail()
        tv_amount.text = viewModel.getAmount()
    }

    private fun showStartSettlementConfirmationDialog() {
        showConfirmationMessage(
            MessageConfig.Builder()
                .title(getString(R.string.msg_settlement_confirmation))
                .message(getString(R.string.msg_settlement_time_elapsed))
                .positiveLabel(getString(R.string.action_confirm))
                .onPositiveClick {
                    startSettlementActivity()
                }
                .negativeLabel(getString(R.string.action_cancel))
                .onNegativeClick {
                    viewModel.addSettlementPendingNotification()
                    handleFailedPayment(PaymentResult.CANCEL)
                })
    }

    private fun startSettlementActivity() {
        SettlementActivity.getLaunchIntent(
            this,
            isManualSettlement = false,
            isSettlementSuccessConfirmationRequired = false
        )
    }

    private fun startAutoReversalActivity() {
        val intent = Intent(this, AutoReversalActivity::class.java)
        startActivityForResult(intent, AutoReversalActivity.REQUEST_CODE)
    }

    private fun handleFailedPayment(it: PaymentResult?) {
        val paymentResponse = PaymentResponse(it)
        PaymentSdkSingleton.getInstance().updatePaymentResponse(paymentResponse)
        PaymentSdkSingleton.getInstance().returnActivityResultWithExtraData(this)
    }

    override fun getBindingVariable(): Int = 0

    override fun getLayout(): Int = R.layout.activity_payment

    override fun getViewModel(): PosPaymentViewModel =
        ViewModelProviders.of(this)[PosPaymentViewModel::class.java]

    override fun onDestroy() {
        NiblMerchant.INSTANCE.stopIdleTimeCountDown()
        PaymentSdkSingleton.getInstance().cleanup()
        unregisterReceiver(paymentCancellationBroadCastReceiver)
        super.onDestroy()
    }

    private val paymentCancellationBroadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, broadCastIntent: Intent?) {
            logger.log("Payment Cancellation BroadCast Received...")
            broadCastIntent?.let {
                broadCastIntent.action?.let {
                    proceedForPaymentCancellation(broadCastIntent)
                }
            }
        }
    }

    private fun proceedForPaymentCancellation(broadCastIntent: Intent) {
        val paymentCancellationRequestJson = broadCastIntent
            .getStringExtra(EXTRAS_PAYMENT_CANCELLATION_REQUEST)
        logger.log("Payment Cancellation Request Json ::: ".plus(paymentCancellationRequestJson))
        handleFailedPayment(PaymentResult.CANCEL)
    }
}