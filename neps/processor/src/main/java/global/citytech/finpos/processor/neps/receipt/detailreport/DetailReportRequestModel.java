package global.citytech.finpos.processor.neps.receipt.detailreport;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.detailreport.DetailReportRequestParameter;

/**
 * Created by Saurav Ghimire on 3/29/21.
 * Citytech
 */
public class DetailReportRequestModel implements UseCase.Request, DetailReportRequestParameter {

    private TransactionRepository transactionRepository;
    private PrinterService printerService;
    private boolean printSummaryReport;
    private ApplicationRepository applicationRepository;
    private ReconciliationRepository reconciliationRepository;

    public DetailReportRequestModel() {
    }

    public DetailReportRequestModel(TransactionRepository transactionRepository, PrinterService printerService, boolean printSummaryReport, ApplicationRepository applicationRepository, ReconciliationRepository reconciliationRepository) {
        this.transactionRepository = transactionRepository;
        this.printerService = printerService;
        this.printSummaryReport = printSummaryReport;
        this.applicationRepository = applicationRepository;
        this.reconciliationRepository = reconciliationRepository;
    }

    public boolean isPrintSummaryReport() {
        return printSummaryReport;
    }

    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public ApplicationRepository getApplicationRepository() {
        return applicationRepository;
    }

    public ReconciliationRepository getReconciliationRepository() {
        return reconciliationRepository;
    }

    public static final class Builder {
        private TransactionRepository transactionRepository;
        private PrinterService printerService;
        private boolean printSummaryReport;
        private ApplicationRepository applicationRepository;
        private ReconciliationRepository reconciliationRepository;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder transactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public Builder printerService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public Builder printSummaryReport(boolean printSummaryReport) {
            this.printSummaryReport = printSummaryReport;
            return this;
        }

        public Builder applicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public Builder reconciliationRepository(ReconciliationRepository reconciliationRepository) {
            this.reconciliationRepository = reconciliationRepository;
            return this;
        }

        public DetailReportRequestModel build() {
            DetailReportRequestModel detailReportRequestModel = new DetailReportRequestModel();
            detailReportRequestModel.reconciliationRepository = this.reconciliationRepository;
            detailReportRequestModel.transactionRepository = this.transactionRepository;
            detailReportRequestModel.printerService = this.printerService;
            detailReportRequestModel.printSummaryReport = this.printSummaryReport;
            detailReportRequestModel.applicationRepository = this.applicationRepository;
            return detailReportRequestModel;
        }
    }
}
