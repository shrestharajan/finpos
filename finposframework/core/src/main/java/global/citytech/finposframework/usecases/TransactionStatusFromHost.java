package global.citytech.finposframework.usecases;

/**
 * Created by Unique Shakya on 9/9/2020.
 */
public enum TransactionStatusFromHost {
    ONLINE_APPROVED,
    ONLINE_DECLINED,
    UNABLE_TO_GO_ONLINE
}
