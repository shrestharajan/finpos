package global.citytech.finposframework.notifier;

import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.listeners.CardConfirmationListener;
import global.citytech.finposframework.listeners.CardConfirmationListenerForGreenPin;
import global.citytech.finposframework.listeners.OtpConfirmationListener;
import global.citytech.finposframework.listeners.SettlementConfirmationListener;

/**
 * Created by Unique Shakya on 8/31/2020.
 */
public interface Notifier {

    void notify(EventType eventType, String message);

    void notify(EventType eventType, String message, String transactionType);

    void notify(EventType eventType, CardDetails cardDetails, CardConfirmationListener cardConfirmationListener);

    void notify(EventType eventType, CardDetails cardDetails, OtpConfirmationListener otpConfirmationListener);

    void notify(EventType eventType, String stan,String message,boolean isApproved);

    void notify(EventType eventType);

    void notify(EventType eventType, CardConfirmationListenerForGreenPin cardConfirmationListenerForGreenPin);

    void notify(EventType eventType, String jsonData, SettlementConfirmationListener settlementConfirmationListener);

    enum TransactionType {
        CARD("CARD"),
        VOID("VOID"),
        MOBILE_PAY("MOBILE_PAY"),
        QR("QR"),
        OTHER("OTHER");


        private final String description;


        TransactionType(String description) {
            this.description = description;
        }


        public String getDescription() {
            return description;
        }
    }

    enum EventType {
        STARTING_READ_MOBILE_NFC("Read NFC Mobile"),
        DETECT_MOBILE_NFC_ERROR("Error NFC Mobile"),
        PROCESSING_MOBILE_NFC_DATA("Process NFC Mobile"),
        NFC_MOBILE_TIMEOUT("Time out"),
        NFC_SOURCE_INVALID("Invalid source"),
        NFC_WAVE_AGAIN("Wave again for nfc mobile"),
        STARTING_READ_CARD("Start Read Card"),
        READING_CARD("Reading Card"),
        CONNECTING_TO_SWITCH("Connecting to switch"),
        TRANSMITTING_REQUEST("Transmitting Request"),
        RESPONSE_RECEIVED("Response Received"),
        PROCESSING_ISSUER_SCRIPT("Processing Issuer Script"),
        TRANSACTION_APPROVED("Transaction Approved"),
        TRANSACTION_DECLINED("Transaction Declined"),
        PREPARING_RECONCILIATION_TOTALS("Preparing Reconciliation Totals"),
        PERFORMING_REVERSAL("Performing Reversal"),
        REVERSAL_COMPLETED("Reversal Completed"),
        WAVE_AGAIN("Wave Again"),
        SWIPE_AGAIN("Swipe Again"),
        SWITCH_PICC_TO_ICC("Please insert card"),
        SWITCH_ICC_TO_MAGNETIC("Chip can not be read.\nUse Magnetic Stripe"),
        SWITCH_MAGNETIC_TO_ICC("Card contains chip.\nPlease insert card"),
        INSERT_CARD_PROPERLY("Card can not be read.\nPLease insert card properly"),
        DETECTED_MULTIPLE_AID("Detected Multiple AID"),
        FORCE_ICC_CARD("Force Contact Card"),
        BATCH_UPLOADING("Batch Uploading"),
        BATCH_UPLOAD_PROGRESS("Batch Upload Progress"),
        PREPARING_DETAIL_REPORT("Preparing Details Report"),
        PRINTING_DETAIL_REPORT("Printing Details Report"),
        PRINTING_SETTLEMENT_REPORT("Printing Settlement Report"),
        CARD_CONFIRMATION_EVENT("Card Confirmation"),
        SETTLEMENT_CONFIRMATION_EVENT("Settlement Confirmation"),
        SETTLEMENT_SUCCESS_EVENT("Settlement Success"),
        DETECT_CARD_ERROR("Please wait"),
        PUSH_MERCHANT_LOG("Push Merchant Log"),
        PUSH_REVERSAL_MERCHANT_LOG("Push Reversal Merchant Log"),
        OTP_CONFIRMATION_EVENT("Otp Confirmation"),
        RETURN_TO_DASHBOARD("Return dashboard"),
        PIN_CHANGE_EVENT("Pin Change"),
        CASH_IN_EVENT("Cash In"),
        CASH_IN_PROCESSING_EVENT("Cash In Processing"),
        CASH_IN_SUCCESS_EVENT("Cash In Success"),
        PIN_UNMATCH(" Pin Unmatch"),
        PROCESSING("Processing"),
        CHECK_IF_CARD_PRESENT("Please do not remove your card"),
        HIDE_PROGRESS_SCREEN("Hide progress screen");

        private final String description;

        EventType(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }
    }
}
