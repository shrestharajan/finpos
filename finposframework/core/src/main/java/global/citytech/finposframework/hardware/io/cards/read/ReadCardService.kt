package global.citytech.finposframework.hardware.io.cards.read

import global.citytech.finposframework.hardware.common.DeviceService
import global.citytech.finposframework.repositories.TerminalRepository

/**
 * Created by Rishav Chudal on 6/6/20.
 */
interface ReadCardService : DeviceService {

    fun readCardDetails(readCardRequest: ReadCardRequest): ReadCardResponse

    fun readEmv(readCardRequest: ReadCardRequest): ReadCardResponse

    fun processEMV(readCardRequest: ReadCardRequest): ReadCardResponse

    fun setOnlineResult(
        responseCode: Int,
        onlineResult: OnlineResult,
        responseField55Data: String,
        onlineAfterTcAac: Boolean
    ): ReadCardResponse

    fun getIccCardStatus(): ReadCardResponse

    fun cleanUp()

    enum class OnlineResult {
        ONLINE_APPROVED,
        ONLINE_DECLINED,
        UNABLE_TO_GO_ONLINE
    }
}