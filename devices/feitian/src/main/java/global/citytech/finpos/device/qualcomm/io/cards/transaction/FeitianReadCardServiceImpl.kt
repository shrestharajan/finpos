package global.citytech.finpos.device.qualcomm.io.cards.transaction

import android.content.Context
import global.citytech.finpos.device.qualcomm.io.cards.handler.FeitianTransactionHandler
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService
import global.citytech.finposframework.notifier.Notifier
import global.citytech.finposframework.repositories.TerminalRepository

/**
 * Created by Rishav Chudal on 4/7/21.
 */
class FeitianReadCardServiceImpl constructor(context: Context, notifier: Notifier) :
    FeitianTransactionHandler(context, notifier),
    ReadCardService {

    override fun readCardDetails(readCardRequest: ReadCardRequest): ReadCardResponse {
        return executeReadCardDetails(readCardRequest)
    }

    override fun readEmv(readCardRequest: ReadCardRequest): ReadCardResponse {
        return executeReadEMV(readCardRequest)
    }

    override fun processEMV(readCardRequest: ReadCardRequest): ReadCardResponse {
        return executeProcessEMV(readCardRequest)
    }

    override fun setOnlineResult(
        responseCode: Int,
        onlineResult: ReadCardService.OnlineResult,
        responseField55Data: String,
        onlineAfterTcAac: Boolean
    ): ReadCardResponse {
        return executeEMVOnlineResultSet(
            responseCode,
            onlineResult,
            responseField55Data
        )
    }

    override fun getIccCardStatus(): ReadCardResponse {
        return executeCheckICCPresentStatus()
    }

    override fun cleanUp() {
        return executeCleanUp()
    }
}