package global.citytech.finpos.merchant.domain.model.receipt.duplicatereconciliation

import global.citytech.finposframework.hardware.common.Result

/**
 * Created by Saurav Ghimire on 6/16/21.
 * sauravnghimire@gmail.com
 */


data class DuplicateReconciliationReceiptResponseEntity(
    val result: Result? = null,
    val message: String? = null
)