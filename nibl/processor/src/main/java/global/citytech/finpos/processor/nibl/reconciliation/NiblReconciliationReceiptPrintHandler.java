package global.citytech.finpos.processor.nibl.reconciliation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import global.citytech.finposframework.hardware.io.printer.Printable;
import global.citytech.finposframework.hardware.io.printer.PrinterRequest;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.switches.posmode.PosMode;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finposframework.utility.ReceiptUtils.addBase64Image;
import static global.citytech.finposframework.utility.ReceiptUtils.addDoubleColumnString;
import static global.citytech.finposframework.utility.ReceiptUtils.addMultiColumnString;
import static global.citytech.finposframework.utility.ReceiptUtils.addSingleColumnString;

/**
 * Created by Unique Shakya on 9/22/2020.
 */
public class NiblReconciliationReceiptPrintHandler {

    private PrinterService printerService;

    public NiblReconciliationReceiptPrintHandler(PrinterService printerService) {
        this.printerService = printerService;
    }

    public PrinterResponse printReceipt(NiblReconciliationReceipt niblReconciliationReceipt, ReceiptVersion receiptVersion) {
        PrinterRequest printerRequest = this.preparePrinterRequest(niblReconciliationReceipt, receiptVersion);
        printerRequest.setFeedPaperAfterFinish(receiptVersion == ReceiptVersion.DUPLICATE_COPY);
        return this.printerService.print(printerRequest);
    }

    private PrinterRequest preparePrinterRequest(NiblReconciliationReceipt niblReconciliationReceipt, ReceiptVersion receiptVersion) {
        List<Printable> printableList = new ArrayList<>();
        if (niblReconciliationReceipt.getRetailer().getRetailerLogo() != null)
            addBase64Image(printableList, niblReconciliationReceipt.getRetailer().getRetailerLogo());
        addSingleColumnString(printableList, niblReconciliationReceipt.getRetailer().getRetailerName(), ReconciliationReceiptStyle.RETAILER_NAME.getStyle());
        addSingleColumnString(printableList, niblReconciliationReceipt.getRetailer().getRetailerAddress(), ReconciliationReceiptStyle.RETAILER_ADDRESS.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
        if (receiptVersion == ReceiptVersion.DUPLICATE_COPY)
            addSingleColumnString(printableList, receiptVersion.getVersionLabel(), ReconciliationReceiptStyle.RECEIPT_VERSION.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.SETTLEMENT_REPORT, ReconciliationReceiptStyle.RECONCILIATION_REPORT.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
        addDoubleColumnString(printableList, ReconciliationReceiptLabels.TERMINAL_ID, niblReconciliationReceipt.getRetailer().getTerminalId(), ReconciliationReceiptStyle.TERMINAL_ID.getStyle());
        addDoubleColumnString(printableList, ReconciliationReceiptLabels.MERCHANT_ID, niblReconciliationReceipt.getRetailer().getMerchantId(), ReconciliationReceiptStyle.MERCHANT_ID.getStyle());
        addDoubleColumnString(printableList, ReconciliationReceiptLabels.DATE_TIME, this.receiptFormatDate(niblReconciliationReceipt.getPerformance().getStartDateTime()), ReconciliationReceiptStyle.START_DATE_TIME.getStyle());
        addDoubleColumnString(printableList, ReconciliationReceiptLabels.BATCH_NUMBER, niblReconciliationReceipt.getReconciliationBatchNumber(), ReconciliationReceiptStyle.BATCH_NUMBER.getStyle());
        addDoubleColumnString(printableList, ReconciliationReceiptLabels.HOST, niblReconciliationReceipt.getHost(), ReconciliationReceiptStyle.HOST.getStyle());
        addDoubleColumnString(printableList, ReconciliationReceiptLabels.APPLICATION_VERSION, niblReconciliationReceipt.getApplicationVersion(), ReconciliationReceiptStyle.APPLICATION_VERSION.getStyle());
        addSingleColumnString(printableList, "  ", ReconciliationReceiptStyle.DIVIDER.getStyle());
        this.addTotalsTable(printableList, niblReconciliationReceipt);
        this.addCumulativeTotalsTable(printableList, niblReconciliationReceipt);
        addSingleColumnString(printableList, niblReconciliationReceipt.getReconciliationResult(), ReconciliationReceiptStyle.RECONCILIATION_RESULT.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
        return new PrinterRequest(printableList);
    }

    private void addCumulativeTotalsTable(List<Printable> printableList, NiblReconciliationReceipt niblReconciliationReceipt) {
        NiblSettlementTotals niblSettlementTotals = (NiblSettlementTotals) niblReconciliationReceipt.getReconciliationTotals();
        addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.CUMULATIVE, ReconciliationReceiptStyle.CUMULATIVE.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
        addMultiColumnString(printableList, this.prepareCumulativeSalesRow(niblReconciliationReceipt.getPosMode(), niblSettlementTotals.getTransactionCurrencyName(), niblSettlementTotals), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
        addMultiColumnString(printableList, this.prepareCumulativeSalesRefundRow(niblSettlementTotals.getTransactionCurrencyName(), niblSettlementTotals), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
        addMultiColumnString(printableList, this.prepareCumulativeVoidRow(niblReconciliationReceipt.getPosMode(), niblSettlementTotals.getTransactionCurrencyName(), niblSettlementTotals), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.DIVIDER, ReconciliationReceiptStyle.DIVIDER.getStyle());
        addMultiColumnString(printableList, this.prepareCumulativeTotalsRow(niblSettlementTotals.getTransactionCurrencyName(), niblSettlementTotals), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.DIVIDER, ReconciliationReceiptStyle.DIVIDER.getStyle());
    }

    private List<String> prepareCumulativeTotalsRow(String transactionCurrencyName, NiblSettlementTotals niblSettlementTotals) {
        List<String> strings = new ArrayList<>();
        strings.add(ReconciliationReceiptLabels.TOTAL);
        strings.add(this.receiptFormatCount(niblSettlementTotals.getTotalCountReceipt()));
        strings.add(transactionCurrencyName);
        strings.add(this.receiptFormatAmount(niblSettlementTotals.getTotalAmountReceipt()));
        return strings;
    }

    private List<String> prepareCumulativeVoidRow(PosMode posMode, String transactionCurrencyName, NiblSettlementTotals niblSettlementTotals) {
        List<String> strings = new ArrayList<>();
        if (posMode == PosMode.RETAILER)
            strings.add(ReconciliationReceiptLabels.VOID);
        else
            strings.add(ReconciliationReceiptLabels.CASH_VOID);
        strings.add(this.receiptFormatCount(niblSettlementTotals.getVoidCount()));
        strings.add(transactionCurrencyName);
        strings.add(this.receiptFormatAmount(niblSettlementTotals.getVoidAmount()));
        return strings;
    }

    private List<String> prepareCumulativeSalesRefundRow(String transactionCurrencyName, NiblSettlementTotals niblSettlementTotals) {
        List<String> strings = new ArrayList<>();
        strings.add(ReconciliationReceiptLabels.REFUND);
        strings.add(this.receiptFormatCount(niblSettlementTotals.getSalesRefundCount()));
        strings.add(transactionCurrencyName);
        strings.add(this.receiptFormatAmount(niblSettlementTotals.getSalesRefundAmount()));
        return strings;
    }

    private List<String> prepareCumulativeSalesRow(PosMode posMode, String transactionCurrencyName, NiblSettlementTotals niblSettlementTotals) {
        List<String> strings = new ArrayList<>();
        if (posMode == PosMode.RETAILER)
            strings.add(ReconciliationReceiptLabels.SALE);
        else
            strings.add(ReconciliationReceiptLabels.CASH_ADVANCE);
        strings.add(this.receiptFormatCount(niblSettlementTotals.getSalesCount()));
        strings.add(transactionCurrencyName);
        strings.add(this.receiptFormatAmount(niblSettlementTotals.getSalesAmount()));
        return strings;
    }

    private String receiptFormatDate(String dateTime) {
        try {
            Date date = new SimpleDateFormat("yyyyMMddHHmmss").parse(dateTime);
            return new SimpleDateFormat("dd/MM/yyyy, HH:mm:ss").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTime;
    }

    private void addTotalsTable(List<Printable> printableList, NiblReconciliationReceipt niblReconciliationReceipt) {
        NiblSettlementTotals niblSettlementTotals = (NiblSettlementTotals) niblReconciliationReceipt.getReconciliationTotals();
        for (Map.Entry<CardSchemeType, NiblCardSchemeTotals> niblCardSchemeTotals : niblSettlementTotals.getTotalsMap().entrySet()) {
            if (niblCardSchemeTotals.getValue().getTotalCount() != 0) {
                addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
                addSingleColumnString(printableList, niblCardSchemeTotals.getKey().getCardScheme(), ReconciliationReceiptStyle.CARD_SCHEME.getStyle());
                addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
                addMultiColumnString(printableList, this.prepareSalesRow(niblReconciliationReceipt.getPosMode(), niblCardSchemeTotals.getValue()), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
                addMultiColumnString(printableList, this.prepareSalesRefundRow(niblCardSchemeTotals.getValue()), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
                addMultiColumnString(printableList, this.prepareVoidRow(niblReconciliationReceipt.getPosMode(), niblCardSchemeTotals.getValue()), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
                addSingleColumnString(printableList, ReconciliationReceiptLabels.DIVIDER, ReconciliationReceiptStyle.DIVIDER.getStyle());
                addMultiColumnString(printableList, this.prepareTotalsRow(niblCardSchemeTotals.getValue()), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
                addSingleColumnString(printableList, ReconciliationReceiptLabels.DIVIDER, ReconciliationReceiptStyle.DIVIDER.getStyle());
            }
        }
    }

    private List<String> prepareTotalsRow(NiblCardSchemeTotals niblCardSchemeTotals) {
        List<String> strings = new ArrayList<>();
        strings.add(ReconciliationReceiptLabels.TOTAL);
        strings.add(this.receiptFormatCount(niblCardSchemeTotals.getTotalCountReceipt()));
        strings.add(niblCardSchemeTotals.getTransactionCurrencyName());
        strings.add(this.receiptFormatAmount(niblCardSchemeTotals.getTotalAmountReceipt()));
        return strings;
    }

    private List<String> prepareVoidRow(PosMode posMode, NiblCardSchemeTotals niblCardSchemeTotals) {
        List<String> strings = new ArrayList<>();
        if (posMode == PosMode.CASH)
            strings.add(ReconciliationReceiptLabels.CASH_VOID);
        else
            strings.add(ReconciliationReceiptLabels.VOID);
        strings.add(this.receiptFormatCount(niblCardSchemeTotals.getVoidCount()));
        strings.add(niblCardSchemeTotals.getTransactionCurrencyName());
        strings.add(this.receiptFormatAmount(niblCardSchemeTotals.getVoidAmount()));
        return strings;
    }

    private List<String> prepareSalesRefundRow(NiblCardSchemeTotals niblCardSchemeTotals) {
        List<String> strings = new ArrayList<>();
        strings.add(ReconciliationReceiptLabels.REFUND);
        strings.add(this.receiptFormatCount(niblCardSchemeTotals.getSalesRefundCount()));
        strings.add(niblCardSchemeTotals.getTransactionCurrencyName());
        strings.add(this.receiptFormatAmount(niblCardSchemeTotals.getSalesRefundAmount()));
        return strings;
    }

    private List<String> prepareSalesRow(PosMode posMode, NiblCardSchemeTotals niblCardSchemeTotals) {
        List<String> strings = new ArrayList<>();
        if (posMode == PosMode.CASH)
            strings.add(ReconciliationReceiptLabels.CASH_ADVANCE);
        else
            strings.add(ReconciliationReceiptLabels.SALE);
        strings.add(this.receiptFormatCount(niblCardSchemeTotals.getSalesCount()));
        strings.add(niblCardSchemeTotals.getTransactionCurrencyName());
        strings.add(this.receiptFormatAmount(niblCardSchemeTotals.getSalesAmount()));
        return strings;
    }

    private String receiptFormatAmount(long amount) {
        return StringUtils.formatAmountTwoDecimal(HelperUtils.fromIsoAmount(String.valueOf(amount)));
    }

    private String receiptFormatCount(long count) {
        return StringUtils.ofRequiredLength(String.valueOf(count), 4);
    }
}
