package global.citytech.finpos.merchant.presentation.auth.completion

import global.citytech.common.extensions.isAlphaNumeric
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase


/**
 * Created by Saurav Ghimire on 2/15/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
object AuthCompletionValidator {
    fun validateData(authCode: String) = authCode.isAlphaNumeric() && authCode.length == 6
    fun getTransactionValidatorObservable(localDataUseCase: LocalDataUseCase, authCode: String) =
        localDataUseCase.isTransactionPresentByAuthCode(authCode)
    fun getTransactionByDataObservable(localDataUseCase: LocalDataUseCase, invoiceNumber: String) =
        localDataUseCase.getTransactionByInvoiceNumber(invoiceNumber, true)
}