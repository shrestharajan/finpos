//package global.citytech.finpos.processor.nibl.cashadvance;
//
//import java.util.concurrent.CountDownLatch;
//
//import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
//import global.citytech.finpos.nibl.iso8583.ProcessingCode;
//import global.citytech.finpos.nibl.iso8583.requestsender.cashadvance.CashAdvanceRequest;
//import global.citytech.finpos.nibl.iso8583.requestsender.cashadvance.CashAdvanceRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.cashadvance.ManualCashAdvanceRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
//import global.citytech.finpos.processor.nibl.reversal.ReversalRequestModel;
//import global.citytech.finpos.processor.nibl.reversal.ReversalUseCase;
//import global.citytech.finpos.processor.nibl.transaction.ManualTransactionUseCase;
//import global.citytech.finpos.processor.nibl.transaction.NiblTransactionReceiptPrintHandler;
//import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
//import global.citytech.finpos.processor.nibl.transaction.ReversedTransactionReceiptHandler;
//import global.citytech.finposframework.exceptions.FinPosException;
//import global.citytech.finposframework.exceptions.PosError;
//import global.citytech.finposframework.exceptions.PosException;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
//import global.citytech.finposframework.iso8583.DataElement;
//import global.citytech.finposframework.iso8583.IsoMessageResponse;
//import global.citytech.finposframework.iso8583.RequestContext;
//import global.citytech.finposframework.listeners.CardConfirmationListener;
//import global.citytech.finposframework.notifier.Notifier;
//import global.citytech.finposframework.repositories.ApplicationRepository;
//import global.citytech.finposframework.repositories.TerminalRepository;
//import global.citytech.finposframework.supports.UseCase;
//import global.citytech.finposframework.switches.PosEntryMode;
//import global.citytech.finposframework.switches.cashadvance.ManualCashAdvanceRequester;
//import global.citytech.finposframework.usecases.TransactionType;
//import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
//import global.citytech.finposframework.usecases.transaction.TransactionLog;
//import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
//import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
//import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
//import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
//import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
//import global.citytech.finposframework.utility.HelperUtils;
//import global.citytech.finposframework.utility.IsoMessageUtils;
//import global.citytech.finposframework.utility.JsonUtils;
//import global.citytech.finposframework.utility.StringUtils;
//
///**
// * Created by Unique Shakya on 2/5/2021.
// */
//public class ManualCashAdvanceUseCase extends ManualTransactionUseCase implements ManualCashAdvanceRequester<CashAdvanceRequestModel, CashAdvanceResponseModel>,
//        UseCase<CashAdvanceRequestModel, CashAdvanceResponseModel> {
//
//    private CashAdvanceRequestModel cashAdvanceRequestModel;
//    private CashAdvanceRequest cashAdvanceRequest;
//    private TransactionRequest transactionRequest;
//    private ApplicationRepository applicationRepository;
//    static CountDownLatch countDownLatch;
//    ReadCardResponse readCardResponse;
//    String stan;
//    String invoiceNumber;
//    String batchNumber;
//    private global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest;
//
//    public ManualCashAdvanceUseCase(TerminalRepository terminalRepository, Notifier notifier) {
//        super(terminalRepository, notifier);
//    }
//
//    @Override
//    protected String getClassName() {
//        return ManualCashAdvanceUseCase.class.getSimpleName();
//    }
//
//    @Override
//    public CashAdvanceResponseModel execute(CashAdvanceRequestModel cashAdvanceRequestModel) {
//        logger.log("::: REACHED CASH ADVANCE USE CASE :::");
//        this.cashAdvanceRequestModel = cashAdvanceRequestModel;
//        try {
//            if (!this.deviceIsReady(this.cashAdvanceRequestModel.getDeviceController(),
//                    this.cashAdvanceRequestModel.getTransactionRequest().getTransactionType()))
//                throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
//            stan = this.terminalRepository.getSystemTraceAuditNumber();
//            invoiceNumber = this.terminalRepository.getInvoiceNumber();
//            batchNumber = this.terminalRepository.getReconciliationBatchNumber();
//            transactionRequest = cashAdvanceRequestModel.getTransactionRequest();
//            applicationRepository = cashAdvanceRequestModel.getApplicationRepository();
//            this.transactionRepository = cashAdvanceRequestModel.getTransactionRepository();
//            this.initTransaction(applicationRepository, transactionRequest);
//            readCardResponse = this.prepareManualReadCardResponse(this.cashAdvanceRequestModel.getTransactionRequest());
//            return this.initializeTransactionWithPreEnteredCardDetails(
//                    readCardResponse);
//        } catch (PosException pe) {
//            pe.printStackTrace();
//            throw pe;
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        }
//    }
//
//    private CashAdvanceResponseModel initializeTransactionWithPreEnteredCardDetails(ReadCardResponse readCardResponse) throws InterruptedException {
//        this.identifyAndSetCardScheme(readCardResponse, this.cashAdvanceRequestModel.getTransactionRepository(),
//                this.cashAdvanceRequestModel.getApplicationRepository());
//        if (!this.validTransaction(this.cashAdvanceRequestModel.getApplicationRepository(),
//                this.cashAdvanceRequestModel.getTransactionRequest(), readCardResponse))
//            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
//        if (!this.transactionAuthorizationSucceeds(this.cashAdvanceRequestModel.getTransactionAuthenticator(),
//                this.cashAdvanceRequestModel.getApplicationRepository(), this.cashAdvanceRequestModel.getTransactionRequest()
//                        .getTransactionType(), readCardResponse))
//            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
//        purchaseRequest = this.prepareManualCashAdvancePurchaseRequest(readCardResponse);
//
//        final boolean[] isConfirm = {false};
//        countDownLatch = new CountDownLatch(1);
//        this.notifier.notify(Notifier.EventType.CARD_CONFIRMATION_EVENT,
//                readCardResponse.getCardDetails(),
//                (CardConfirmationListener) confirmed -> {
//                    isConfirm[0] = confirmed;
//                    countDownLatch.countDown();
//                });
//        countDownLatch.await();
//        if (!isConfirm[0]) {
//            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
//        }
//        return processCardAndProceed();
//
//    }
//
//    private CashAdvanceResponseModel processCardAndProceed() {
//        readCardResponse = this.retrievePinBlockIfRequired(purchaseRequest, this.cashAdvanceRequestModel.getApplicationRepository(),
//                this.cashAdvanceRequestModel.getTransactionAuthenticator());
//        return this.performTransactionWithHost(stan, invoiceNumber, batchNumber, readCardResponse);
//    }
//
//    private CashAdvanceResponseModel performTransactionWithHost(String stan, String invoiceNumber,
//                                                                String batchNumber,
//                                                                ReadCardResponse readCardResponse) {
//        this.notifier.notify(Notifier.EventType.TRANSMITTING_REQUEST, "Transmitting Request..");
//        try {
//            IsoMessageResponse isoMessageResponse = this.sendIsoMessage(stan, invoiceNumber, readCardResponse);
//            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait..");
//            InvalidResponseHandlerResponse invalidResponseHandlerResponse = this
//                    .checkForInvalidResponseFromHost(isoMessageResponse);
//            if (invalidResponseHandlerResponse.isInvalid())
//                return this.handleInvalidResponseFromHost(invalidResponseHandlerResponse, stan,
//                        batchNumber, invoiceNumber, readCardResponse);
//            boolean isCashAdvanceApproved = this.transactionApprovedByActionCode(isoMessageResponse);
//            this.notifyTransactionCompletion(stan, batchNumber, invoiceNumber, isCashAdvanceApproved,
//                    isoMessageResponse, readCardResponse);
//            boolean shouldPrint = this.printReceipt(batchNumber, stan, invoiceNumber, this.cashAdvanceRequestModel.getPrinterService(),
//                    this.cashAdvanceRequestModel.getApplicationRepository(),
//                    this.cashAdvanceRequestModel.getTransactionRepository(),
//                    this.cashAdvanceRequestModel.getTransactionRequest(),
//                    readCardResponse, isoMessageResponse);
//            return this.prepareCashAdvanceResponse(stan, isoMessageResponse, isCashAdvanceApproved, shouldPrint);
//        } catch (FinPosException fe) {
//            fe.printStackTrace();
//            if (this.isResponseNotReceivedException(fe)) {
//                return this.handleResponseNotReceivedTransaction(batchNumber, stan, invoiceNumber,
//                        readCardResponse);
//            } else if (fe.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT) {
//                logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED UNABLE TO CONNECT");
//                transactionRepository.removeAutoReversal(this.autoReversal);
//                throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
//            } else {
//                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//            }
//        }
//    }
//
//    private CashAdvanceResponseModel handleResponseNotReceivedTransaction(String batchNumber, String stan,
//                                                                          String invoiceNumber,
//                                                                          ReadCardResponse readCardResponse) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, "TIMEOUT");
//        this.performReversal(stan, readCardResponse);
//        boolean shouldPrint = !this.approvePrintOnly(this.cashAdvanceRequestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(this.cashAdvanceRequestModel.getTransactionRequest(),
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.TIMEOUT, "Timeout"));
//            new NiblTransactionReceiptPrintHandler(this.cashAdvanceRequestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            this.cashAdvanceRequestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return CashAdvanceResponseModel.Builder.newInstance()
//                .message("TIMEOUT")
//                .isApproved(false)
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private void performReversal(String stan, ReadCardResponse readCardResponse) {
//        ReversalUseCase reversalUseCase = new ReversalUseCase(this.prepareReversalRequest(stan, readCardResponse),
//                this.terminalRepository, this.notifier);
//        try {
//            reversalUseCase.execute(prepareReversalRequestModel(readCardResponse));
//            this.transactionRepository.removeAutoReversal(this.autoReversal);
//        } catch (FinPosException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private ReversalRequestModel prepareReversalRequestModel(ReadCardResponse readCardResponse) {
//        return ReversalRequestModel.Builder.newInstance()
//                .withTransactionRequest(this.cashAdvanceRequestModel.getTransactionRequest())
//                .withTransactionRepository(this.cashAdvanceRequestModel.getTransactionRepository())
//                .withPrinterService(this.cashAdvanceRequestModel.getPrinterService())
//                .withApplicationRepository(this.cashAdvanceRequestModel.getApplicationRepository())
//                .withReadCardResponse(readCardResponse)
//                .build();
//    }
//
//    private NiblReversalRequest prepareReversalRequest(String stan, ReadCardResponse readCardResponse) {
//        NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
//        niblReversalRequest.setProcessingCode(ProcessingCode.PURCHASE.getCode());
//        niblReversalRequest.setAmount(this.cashAdvanceRequestModel.getTransactionRequest().getAmount());
//        niblReversalRequest.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        niblReversalRequest.setExpiryDate(readCardResponse.getCardDetails().getExpiryDate());
//        niblReversalRequest.setPosEntryMode(this.cashAdvanceRequest.getEntryMode());
//        niblReversalRequest.setPosConditionCode(this.cashAdvanceRequest.getPosConditionCode());
//        niblReversalRequest.setTrack2Data(this.cashAdvanceRequest.getTrack2Data());
//        niblReversalRequest.setPinBlock(this.cashAdvanceRequest.getPinBlock());
//        niblReversalRequest.setInvoiceNumber(this.cashAdvanceRequest.getInvoiceNumber());
//        niblReversalRequest.setStan(stan);
//        return niblReversalRequest;
//    }
//
//    private CashAdvanceResponseModel prepareCashAdvanceResponse(String stan, IsoMessageResponse isoMessageResponse,
//                                                                boolean isCashAdvanceApproved,
//                                                                boolean shouldPrint) {
//        return CashAdvanceResponseModel.Builder.newInstance()
//                .stan(stan)
//                .isApproved(isCashAdvanceApproved)
//                .message(this.retrieveMessage(isoMessageResponse, isCashAdvanceApproved, this.cashAdvanceRequestModel.getApplicationRepository()))
//                .debugRequestMessage(isoMessageResponse.getDebugRequestString())
//                .debugResponseMessage(isoMessageResponse.getDebugResponseString())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private void notifyTransactionCompletion(String stan, String batchNumber, String invoiceNumber,
//                                             boolean isCashAdvanceApproved,
//                                             IsoMessageResponse isoMessageResponse,
//                                             ReadCardResponse readCardResponse) {
//        this.updateTransactionLog(stan, batchNumber, invoiceNumber, isCashAdvanceApproved, isoMessageResponse, readCardResponse);
//        this.updateTransactionIds(isCashAdvanceApproved);
//        this.notifyTransactionStatus(isCashAdvanceApproved, isoMessageResponse,
//                this.cashAdvanceRequestModel.getApplicationRepository());
//    }
//
//    private void updateTransactionLog(String stan, String batchNumber, String invoiceNumber,
//                                      boolean isCashAdvanceApproved,
//                                      IsoMessageResponse isoMessageResponse,
//                                      ReadCardResponse readCardResponse) {
//        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
//                .withStan(stan)
//                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
//                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
//                .withInvoiceNumber(invoiceNumber)
//                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
//                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
//                .withTransactionType(TransactionType.CASH_ADVANCE)
//                .withOriginalTransactionType(TransactionType.CASH_ADVANCE)
//                .withTransactionAmount(this.cashAdvanceRequestModel.getTransactionRequest().getAmount())
//                .withOriginalTransactionAmount(this.cashAdvanceRequestModel.getTransactionRequest().getAmount())
//                .withTransactionDate(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_DATE))
//                .withTransactionTime(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_TIME))
//                .withTransactionStatus(isCashAdvanceApproved ? "APPROVED" : "DECLINED")
//                .withPosEntryMode(PosEntryMode.MANUAL)
//                .withOriginalPosEntryMode(PosEntryMode.MANUAL)
//                .withPosConditionCode(this.cashAdvanceRequest.getPosConditionCode())
//                .withOriginalPosConditionCode("")
//                .withReconcileStatus("")
//                .withReconcileTime("")
//                .withReconcileDate("")
//                .withReconcileBatchNo(batchNumber)
//                .withReadCardResponse(readCardResponse)
//                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse,
//                        applicationRepository, batchNumber, stan, invoiceNumber))
//                .withAuthorizationCompleted(true)
//                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
//                .withTransactionVoided(false)
//                .withTipAdjusted(false)
//                .build();
//        this.cashAdvanceRequestModel.getTransactionRepository().updateTransactionLog(transactionLog);
//
//    }
//
//    private CashAdvanceResponseModel handleInvalidResponseFromHost(InvalidResponseHandlerResponse invalidResponseHandlerResponse,
//                                                                   String stan, String batchNumber,
//                                                                   String invoiceNumber,
//                                                                   ReadCardResponse readCardResponse) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, invalidResponseHandlerResponse.getMessage());
//        this.performReversal(stan, readCardResponse);
//        boolean shouldPrint = !this.approvePrintOnly(this.cashAdvanceRequestModel.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(this.cashAdvanceRequestModel.getTransactionRequest(),
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.INVALID_RESPONSE,
//                            invalidResponseHandlerResponse.getMessage()));
//            new NiblTransactionReceiptPrintHandler(this.cashAdvanceRequestModel.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            this.cashAdvanceRequestModel.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return CashAdvanceResponseModel.Builder.newInstance()
//                .isApproved(false)
//                .message(invalidResponseHandlerResponse.getMessage())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private IsoMessageResponse sendIsoMessage(String stan, String invoiceNumber, ReadCardResponse readCardResponse) {
//        RequestContext context = this.prepareManualTemplateRequestContext(stan);
//        this.cashAdvanceRequest = prepareIsoCashAdvanceRequest(invoiceNumber, readCardResponse);
//        context.setRequest(cashAdvanceRequest);
//        this.autoReversal = new AutoReversal(
//                TransactionType.CASH_ADVANCE,
//                JsonUtils.toJsonObj(prepareReversalRequest(stan, readCardResponse)),
//                0,
//                AutoReversal.Status.ACTIVE,
//                StringUtils.dateTimeStamp()
//        );
//        this.transactionRepository.saveAutoReversal(this.autoReversal);
//        CashAdvanceRequestSender sender = new ManualCashAdvanceRequestSender(new NIBLSpecInfo(), context);
//        IsoMessageResponse isoMessageResponse = sender.send();
//        this.transactionRepository.removeAutoReversal(this.autoReversal);
//        return isoMessageResponse;
//    }
//
//    private CashAdvanceRequest prepareIsoCashAdvanceRequest(String invoiceNumber,
//                                                            ReadCardResponse readCardResponse) {
//        return CashAdvanceRequest.Builder.newInstance()
//                .withPan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
//                .withTransactionAmount(readCardResponse.getCardDetails().getAmount())
//                .withPosConditionCode("00")
//                .withEntryMode(PosEntryMode.MANUAL)
//                .withPinBlock(this.retrievePinBlock(readCardResponse))
//                .withExpireDate(readCardResponse.getCardDetails().getExpiryDate())
//                .withLocalDate(HelperUtils.getDefaultLocaleDateWithoutYear())
//                .withLocalTime(HelperUtils.getDefaultLocaleTimeHhMmSs())
//                .withInvoiceNumber(invoiceNumber)
//                .build();
//    }
//
//    private String retrievePinBlock(ReadCardResponse readCardResponse) {
//        String pin = readCardResponse.getCardDetails().getPinBlock().getPin();
//        if (readCardResponse.getCardDetails().getPinBlock().getOfflinePin() ||
//                StringUtils.isEmpty(pin) || pin.equalsIgnoreCase("offline"))
//            return "";
//        else
//            return pin;
//    }
//
//    private PurchaseRequest prepareManualCashAdvancePurchaseRequest(ReadCardResponse readCardResponse) {
//        PurchaseRequest purchaseRequest = new PurchaseRequest(
//                this.cashAdvanceRequestModel.getTransactionRequest().getTransactionType(),
//                this.cashAdvanceRequestModel.getTransactionRequest().getAmount(),
//                this.cashAdvanceRequestModel.getTransactionRequest().getAdditionalAmount());
//        readCardResponse.getCardDetails().setAmount(this.cashAdvanceRequestModel.getTransactionRequest().getAmount());
//        readCardResponse.getCardDetails().setAdditionalAmount(this.cashAdvanceRequestModel.getTransactionRequest().getAdditionalAmount());
//        purchaseRequest.setCardDetails(readCardResponse.getCardDetails());
//        return purchaseRequest;
//    }
//}
