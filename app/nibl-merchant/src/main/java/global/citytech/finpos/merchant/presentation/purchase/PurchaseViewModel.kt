package global.citytech.finpos.merchant.presentation.purchase

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.finpos.merchant.data.repository.app.AutoReversalQueueRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.purchase.PurchaseRepositoryImpl
import global.citytech.finpos.merchant.domain.model.purchase.PurchaseResponseEntity
import global.citytech.finpos.merchant.domain.usecase.app.CoreAutoReversalQueueUseCase
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.core.purchase.CorePurchaseUseCase
import global.citytech.finpos.merchant.framework.datasource.app.AutoReversalQueueDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.purchase.PurchaseDataSourceImpl
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.purchase.ManualPurchaseRequestItem
import global.citytech.finpos.merchant.presentation.model.purchase.PurchaseRequestItem
import global.citytech.finpos.merchant.presentation.model.qroperator.QrOperatorItem
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.usecases.TransactionType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal

/**
 * Created by Unique Shakya on 8/26/2020.
 */
class PurchaseViewModel(application: Application) : BaseTransactionViewModel(application) {

    val showQrOption by lazy { MutableLiveData<ArrayList<QrOperatorItem>>() }
    val showMobilePayOption by lazy { MutableLiveData<Boolean>(false) }
    val autoReversalPresent by lazy { MutableLiveData<Boolean>() }

    private var purchaseUseCase: CorePurchaseUseCase =
        CorePurchaseUseCase(
            PurchaseRepositoryImpl(PurchaseDataSourceImpl())
        )

    var localDataUseCase: LocalDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )
    private val coreAutoReversalQueueUseCase = CoreAutoReversalQueueUseCase(
        AutoReversalQueueRepositoryImpl(
            AutoReversalQueueDataSourceImpl(context)
        )
    )

    fun checkForAutoReversal() {
        if (coreAutoReversalQueueUseCase.isQueueEmpty()) {
            autoReversalPresent.value = false
        } else {
            autoReversalPresent.value = coreAutoReversalQueueUseCase.isActive()
        }
    }

    fun purchase(
        configurationItem: ConfigurationItem,
        amount: BigDecimal,
        transactionType: TransactionType,
        isEmiTransaction: Boolean, emiInfo: String, vatInfo: String
    ) {
        isLoading.value = true
        compositeDisposable.add(
            purchaseUseCase.purchase(
                configurationItem, preparePurchaseRequest(
                    amount,
                    transactionType,
                    isEmiTransaction,
                    emiInfo,
                    vatInfo
                )
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    if (!shouldDispose) {
                        handlePurchaseResponse(it)
                    } else {
                        completePreviousTask()
                    }
                }, {
                    isLoading.value = false
                    if (!shouldDispose) {
                        transactionComplete.value = false
                        message.value = it.message
                        updatePaymentError(it)
                    } else {
                        completePreviousTask()
                    }
                })
        )
    }

    private fun handlePurchaseResponse(purchaseResponseEntity: PurchaseResponseEntity) {
        showTransactionConfirmationDialog(purchaseResponseEntity)
    }

    private fun showTransactionConfirmationDialog(purchaseResponseModel: PurchaseResponseEntity) {
        prepareBase64UrlToDisplayQr(
            purchaseResponseModel.isApproved,
            purchaseResponseModel.message,
            purchaseResponseModel.shouldPrintCustomerCopy!!,
            purchaseResponseModel.stan!!
        )
    }

    private fun preparePurchaseRequest(
        amount: BigDecimal,
        transactionType: TransactionType,
        isEmiTransaction: Boolean,
        emiInfo: String,
        vatInfo: String
    ): PurchaseRequestItem {
        return PurchaseRequestItem(
            amount,
            transactionType,
            isEmiTransaction,
            emiInfo,
            vatInfo
        )
    }

    fun doManualPurchase(
        configurationItem: ConfigurationItem,
        manualPurchaseRequestItem: ManualPurchaseRequestItem
    ) {
        isLoading.value = true
        compositeDisposable.add(
            this.purchaseUseCase.manualPurchase(
                configurationItem,
                manualPurchaseRequestItem
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    handlePurchaseResponse(it)
                }, {
                    it.printStackTrace()
                    isLoading.value = false
                    transactionComplete.value = false
                    message.value = it.message
                })
        )
    }

    fun initMobilePayOptionConfig() {
        showMobilePayOption.value = this.localDataUseCase.getTerminalSetting().enableNfcTapAndPay
    }

    fun initQrOptionConfig() {
        val settings = this.localDataUseCase.getTerminalSetting()
        val qrOperatorList = ArrayList<QrOperatorItem>()
        settings.qrConfigs
            ?.forEach { it ->
                run {
                    if (it.enableDynamicQr && it.qrOperatorInfo.active) {
                        qrOperatorList.add(it.qrOperatorInfo)
                    }
                }
            }
        showQrOption.value = qrOperatorList
    }

    fun savePaymentInitiatorItems(paymentItems: MutableList<PaymentItems>) {
        localDataUseCase.savePaymentInitiatorItems(paymentItems)
    }

    fun getPaymentInitiatorItems(): ArrayList<PaymentItems> =
        localDataUseCase.getPaymentInitiatorItems()
}