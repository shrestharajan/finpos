package global.citytech.finpos.merchant.framework.datasource.core.cashin

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.cashin.CashInDataSource
import global.citytech.finpos.merchant.domain.model.cashin.CashInRequestEntity
import global.citytech.finpos.merchant.domain.model.cashin.CashInResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionAuthenticatorImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.*
import global.citytech.finpos.merchant.presentation.data.mapToBalanceInquiryResponseUiModel
import global.citytech.finpos.merchant.presentation.data.mapToCashInResponseUiModel
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.cashin.CashInRequestItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finposframework.switches.ProcessorManager
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest
import io.reactivex.Observable

class CashInDataSourceImpl : CashInDataSource {

    override fun generateCashInRequest(
        configurationItem: ConfigurationItem,
        cashInRequestItem: CashInRequestItem
    ): Observable<CashInResponseEntity> {

        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val transactionRepository =
            TransactionRepositoryImpl(
                NiblMerchant.INSTANCE
            )
        val transactionRequest = prepareTransactionRequest(cashInRequestItem)
        val deviceController = DeviceControllerImpl(NiblMerchant.INSTANCE)
        val cardReader = CardSourceImpl(NiblMerchant.getActivityContext())
        val transactionAuthenticator =
            TransactionAuthenticatorImpl()
        val printerServiceImpl = PrinterSourceImpl(NiblMerchant.INSTANCE)
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)

        val cashInRequestEntity = CashInRequestEntity(
            readCardService = cardReader,
            deviceController = deviceController,
            printerService = printerServiceImpl,
            transactionAuthenticator = transactionAuthenticator,
            transactionRepository = transactionRepository,
            transactionRequest = transactionRequest,
            applicationRepository = applicationRepository,
            ledService = LedSourceImpl(NiblMerchant.INSTANCE),
            soundService = SoundSourceImpl(NiblMerchant.INSTANCE)
        )


        val cashInRequester = ProcessorManager.getInterface(
            terminalRepository,
            NotificationHandler
        ).cashInRequester

        return Observable.fromCallable {
            cashInRequester.execute(cashInRequestEntity.mapToModel())
                .mapToCashInResponseUiModel()
        }
    }

    private fun prepareTransactionRequest(cashInRequestItem: CashInRequestItem): TransactionRequest {
        val transactionRequest = TransactionRequest(
            cashInRequestItem.transactionType,
        )
        transactionRequest.amount = cashInRequestItem.amount
        return transactionRequest
    }
}