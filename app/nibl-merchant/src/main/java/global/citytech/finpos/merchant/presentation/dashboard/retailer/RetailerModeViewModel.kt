package global.citytech.finpos.merchant.presentation.dashboard.retailer

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.finpos.merchant.presentation.dashboard.base.BaseDashboardViewModel
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.usecases.TransactionType

/**
 * Created by Rishav Chudal on 5/11/21.
 */
class RetailerModeViewModel(context: Application): BaseDashboardViewModel(context) {

    private val logger = Logger(RetailerModeViewModel::class.java.name)

    val showPurchaseTransactionLiveData by lazy { MutableLiveData<Boolean>() }

    override fun proceedWithEnabledTransactions(enabledTransactions: List<TransactionType>) {
        showPurchaseTransactionLiveData.value = enabledTransactions.contains(TransactionType.PURCHASE)
    }
}