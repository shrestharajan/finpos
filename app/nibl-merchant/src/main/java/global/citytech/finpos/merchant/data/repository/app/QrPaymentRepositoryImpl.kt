package global.citytech.finpos.merchant.data.repository.app

import global.citytech.finpos.merchant.data.datasource.app.qr.QrPaymentDataSource
import global.citytech.finpos.merchant.domain.model.app.QrPayment
import global.citytech.finpos.merchant.domain.repository.app.QrPaymentRepository

class QrPaymentRepositoryImpl(private val qrPaymentDataSource: QrPaymentDataSource) :
    QrPaymentRepository {
    override fun getAllQrPayments(): MutableList<QrPayment> = qrPaymentDataSource.getAllQrPayments()

    override fun getCount(): Long = qrPaymentDataSource.getCount()

    override fun addQrPayment(qrPayment: QrPayment) = qrPaymentDataSource.addQrPayment(qrPayment)

    override fun removeQrPayment(qrPayment: QrPayment) =
        qrPaymentDataSource.removeQrPayment(qrPayment)

    override fun updateQrPaymentStatus(status: String, qrPayment: QrPayment) =
        qrPaymentDataSource.updateQrPaymentStatus(status, qrPayment)

    override fun getLastUpdateDateTime(): String = qrPaymentDataSource.getLastUpdateDateTime()

    override fun getBillingInvoiceNumber(): String = qrPaymentDataSource.getBillingInvoiceNumber()

    override fun incrementBillingInvoiceNumber() =
        qrPaymentDataSource.incrementBillingInvoiceNumber()

    override fun getLimitedQrPayments(pageNumber: Int, offset: Int): MutableList<QrPayment> =
        qrPaymentDataSource.getLimitedQrPayments(pageNumber, offset)

    override fun getLimitedQrPaymentsBySearchParam(
        searchParam: String,
        pageNumber: Int,
        pageSize: Int
    ): MutableList<QrPayment> =
        qrPaymentDataSource.getLimitedQrPaymentsBySearchParam(searchParam, pageNumber, pageSize)

    override fun getCountBySearchParam(searchParam: String): Long =
        qrPaymentDataSource.getCountBySearchParam(searchParam)

    override fun getQrPaymentByInvoiceNumber(invoiceNumber: String): QrPayment? =
        qrPaymentDataSource.getQrPaymentByInvoiceNumber(invoiceNumber)

    override fun updateQrPaymentRefunded(qrPayment: QrPayment) =
        qrPaymentDataSource.updateQrPaymentRefunded(qrPayment)

    override fun updateQrPaymentVoided(qrPayment: QrPayment) =
        qrPaymentDataSource.updateQrPaymentVoided(qrPayment)

    override fun clear() =
        qrPaymentDataSource.clear()

    override fun saveBillingInvoiceNumber(invoiceNumber:String) =
        qrPaymentDataSource.saveBillingInvoiceNumber(invoiceNumber)

    override fun generateFonepayQrIdentifiers(key: String): String {
        return qrPaymentDataSource.generateFonepayQrIdentifiers(key)
    }
}