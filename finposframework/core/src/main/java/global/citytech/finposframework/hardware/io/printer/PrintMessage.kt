package global.citytech.finposframework.hardware.io.printer


class PrintMessage :
    Printable {
    var label: String? = null
    var value: String? = null
    var textList: List<String>? = null
    var columnWidths: IntArray? = null
    private var style: Style
    var isHasMultiString = false
    private var isQrCode = false
    private var isAllow = true
    private var isImage = false
    private val isFeedPaper = false
    private val isBase64Image = false
    private val isBarCode = false

    private constructor(label: String, value: String, style: Style) {
        this.label = label
        this.value = value
        this.style = style
    }

    private constructor(
        textList: List<String>,
        columnWidths: IntArray,
        style: Style
    ) {
        this.textList = textList
        this.columnWidths = columnWidths
        this.style = style
        isHasMultiString = true
    }

    fun getStyle(): Style {
        return style
    }

    fun setStyle(style: Style) {
        this.style = style
    }

    fun hasMultiString(): Boolean {
        return isHasMultiString
    }

    override fun isAllow(): Boolean {
        return isAllow
    }

    fun setAllow(allow: Boolean) {
        isAllow = allow
    }

    override fun isImage(): Boolean {
        return isImage
    }

    fun setImage(image: Boolean) {
        isImage = image
    }

    override fun isQrCode(): Boolean {
        return isQrCode
    }

    override fun isFeedPaper(): Boolean {
        return isFeedPaper
    }

    override fun isBase64Image(): Boolean {
        return isBase64Image
    }

    override fun isBarCode(): Boolean {
        return isBarCode
    }

    fun setQrCode(qrCode: Boolean) {
        isQrCode = qrCode
    }

    override fun getData(): Any {
        return this
    }

    companion object {
        fun getInstance(value: String, style: Style): PrintMessage {
            return PrintMessage(
                "",
                value,
                style
            )
        }

        fun getDoubleColumnInstance(
            label: String,
            value: String,
            style: Style
        ): PrintMessage {
            return PrintMessage(
                label,
                value,
                style
            )
        }

        fun getMultiColumnInstance(
            textList: List<String>,
            columnWidths: IntArray,
            style: Style
        ): PrintMessage {
            return PrintMessage(
                textList,
                columnWidths,
                style
            )
        }

        fun getQRCodeInstance(value: String, width: Int, align: Style.Align?): PrintMessage {
            val style =
                Style()
            style.qrWidth = width
            style.setAlignment(align)
            val printMessage =
                PrintMessage(
                    "",
                    value,
                    style
                )
            printMessage.isQrCode = true
            return printMessage
        }

        fun getBase64ImageInstance(base64String: Any): Printable{
            return Base64ImageReceipt(base64String)
        }
    }
}
