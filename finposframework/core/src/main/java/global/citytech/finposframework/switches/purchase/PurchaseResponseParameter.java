package global.citytech.finposframework.switches.purchase;

import global.citytech.finposframework.switches.ResponseParameter;

/** @author rajudhital on 3/29/20 */
public interface PurchaseResponseParameter extends ResponseParameter {}
