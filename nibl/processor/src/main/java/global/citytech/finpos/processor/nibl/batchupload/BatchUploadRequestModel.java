package global.citytech.finpos.processor.nibl.batchupload;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.batchupload.BatchUploadRequestParameter;

/**
 * Created by Unique Shakya on 5/11/2021.
 */
public class BatchUploadRequestModel implements UseCase.Request, BatchUploadRequestParameter {

    private TransactionRepository transactionRepository;
    private PrinterService printerService;

    public BatchUploadRequestModel(TransactionRepository transactionRepository, PrinterService printerService) {
        this.transactionRepository = transactionRepository;
        this.printerService = printerService;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }
}
