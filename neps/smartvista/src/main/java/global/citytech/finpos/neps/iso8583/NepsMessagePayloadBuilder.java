package global.citytech.finpos.neps.iso8583;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.supports.MessagePayload;

/** User: Surajchhetry Date: 2/19/20 Time: 5:22 PM */
public class NepsMessagePayloadBuilder implements MessagePayload.Builder {

  private SpecInfo specInfo;
  private Iso8583Msg request;

  public NepsMessagePayloadBuilder(SpecInfo specInfo, Iso8583Msg request) {
    this.specInfo = specInfo;
    this.request = request;
  }

  @Override
  public MessagePayload build() {
    try (ByteArrayOutputStream messagePacket = new ByteArrayOutputStream()) {
      byte[] finalDataElements = request.getDataElementsAsByteArray(this.specInfo);
      // 1. Write MTI
      messagePacket.write(this.request.getMti().getAsBytes());
      // 2. Write BitMap
      messagePacket.write(request.getBitMap().asHexASCII());
      // 3. Write Request Data
      messagePacket.write(finalDataElements);
      return new MessagePayload.Default(messagePacket.toByteArray(), request);

    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }
}
