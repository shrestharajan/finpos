package global.citytech.finpos.merchant.presentation.dynamicqr.dialog

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.model.qroperator.QrOperatorItem

/**
 * @author sachin
 */
class QrOperatorOptionAdapter(
    private val qrOperatorItems: List<QrOperatorItem>,
    private val listener: QrOperatorsDialog.Listener
) : RecyclerView.Adapter<QrOperatorOptionAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val qrOperatorTitle: TextView = itemView.findViewById(R.id.tv_qr_operator_title)
        var cardView : CardView = itemView.findViewById(R.id.cv_qr_operator)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val listItem: View = layoutInflater.inflate(R.layout.item_qr_operator_option, parent, false)
        return ViewHolder(listItem)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val qrOperatorItem: QrOperatorItem = qrOperatorItems[position]
        holder.qrOperatorTitle.text = qrOperatorItem.name
        holder.cardView.setOnClickListener {
            listener.onSelectItem(qrOperatorItems[position])
        }
    }

    override fun getItemCount(): Int {
        return qrOperatorItems.size
    }
}
