package global.citytech.finpos.nibl.iso8583.requestsender.cashadvance;

import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.transaction.Request;

/**
 * Created by Unique Shakya on 2/4/2021.
 */
public class CashAdvanceRequest implements Request {
    private String pan;
    private double transactionAmount;
    private String localTime;
    private String localDate;
    private PosEntryMode entryMode;
    private String posConditionCode;
    private String track2Data;
    private String additionalData;
    private String currencyCode;
    private String pinBlock;
    private String emvData;
    private String expireDate;
    private String cardSequenceNumber;
    private String invoiceNumber;
    private String retrievalReferenceNumber;
    private boolean fallbackFromIccToMag;

    public String getPan() {
        return pan;
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public String getLocalTime() {
        return localTime;
    }

    public String getLocalDate() {
        return localDate;
    }

    public PosEntryMode getEntryMode() {
        return entryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public String getTrack2Data() {
        return track2Data;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getPinBlock() {
        return pinBlock;
    }

    public String getEmvData() {
        return emvData;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public String getCardSequenceNumber() {
        return cardSequenceNumber;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public boolean isFallbackFromIccToMag() {
        return fallbackFromIccToMag;
    }

    public static final class Builder {
        private String pan;
        private double transactionAmount;
        private String localTime;
        private String localDate;
        private PosEntryMode entryMode;
        private String posConditionCode;
        private String track2Data;
        private String additionalData;
        private String currencyCode;
        private String pinBlock;
        private String emvData;
        private String expireDate;
        private String cardSequenceNumber;
        private String invoiceNumber;
        private String retrievalReferenceNumber;
        private boolean fallbackFromIccToMag;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withPan(String pan) {
            this.pan = pan;
            return this;
        }

        public Builder withTransactionAmount(double transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public Builder withLocalTime(String localTime) {
            this.localTime = localTime;
            return this;
        }

        public Builder withLocalDate(String localDate) {
            this.localDate = localDate;
            return this;
        }

        public Builder withEntryMode(PosEntryMode entryMode) {
            this.entryMode = entryMode;
            return this;
        }

        public Builder withPosConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public Builder withTrack2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public Builder withAdditionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public Builder withCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder withPinBlock(String pinBlock) {
            this.pinBlock = pinBlock;
            return this;
        }

        public Builder withEmvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public Builder withExpireDate(String expireDate) {
            this.expireDate = expireDate;
            return this;
        }

        public Builder withCardSequenceNumber(String cardSequenceNumber) {
            this.cardSequenceNumber = cardSequenceNumber;
            return this;
        }

        public Builder withInvoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }

        public Builder withRetrievalReferenceNumber(String retrievalReferenceNumber) {
            this.retrievalReferenceNumber = retrievalReferenceNumber;
            return this;
        }

        public Builder withFallbackFromIccToMag(boolean fallbackFromIccToMag) {
            this.fallbackFromIccToMag = fallbackFromIccToMag;
            return this;
        }

        public CashAdvanceRequest build() {
            CashAdvanceRequest cashAdvanceRequest = new CashAdvanceRequest();
            cashAdvanceRequest.track2Data = this.track2Data;
            cashAdvanceRequest.transactionAmount = this.transactionAmount;
            cashAdvanceRequest.cardSequenceNumber = this.cardSequenceNumber;
            cashAdvanceRequest.additionalData = this.additionalData;
            cashAdvanceRequest.fallbackFromIccToMag = this.fallbackFromIccToMag;
            cashAdvanceRequest.expireDate = this.expireDate;
            cashAdvanceRequest.localDate = this.localDate;
            cashAdvanceRequest.localTime = this.localTime;
            cashAdvanceRequest.invoiceNumber = this.invoiceNumber;
            cashAdvanceRequest.entryMode = this.entryMode;
            cashAdvanceRequest.posConditionCode = this.posConditionCode;
            cashAdvanceRequest.pan = this.pan;
            cashAdvanceRequest.pinBlock = this.pinBlock;
            cashAdvanceRequest.currencyCode = this.currencyCode;
            cashAdvanceRequest.retrievalReferenceNumber = this.retrievalReferenceNumber;
            cashAdvanceRequest.emvData = this.emvData;
            return cashAdvanceRequest;
        }
    }
}
