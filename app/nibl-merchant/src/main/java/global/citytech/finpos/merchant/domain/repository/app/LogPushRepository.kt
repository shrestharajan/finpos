package global.citytech.finpos.merchant.domain.repository.app

import global.citytech.common.data.Response
import global.citytech.finpos.merchant.presentation.dashboard.temp.LogResponse
import global.citytech.finpos.merchant.presentation.dashboard.temp.LogFileUploadResponse

interface LogPushRepository {
    fun getLogsList(serialNumber: Any,filePaths:String): LogResponse
    fun pushLogFileToServer(logFileUploadResponse: LogFileUploadResponse, filePath: String, serialNumber: Any): Response

}