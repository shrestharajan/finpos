package global.citytech.neps.datasource.model

import global.citytech.neps.domain.model.Host

data class HostEntity(
    val id: String,
    val connectionTimeout: String? = null,
    val ip: String? = null,
    val nii: String? = null,
    val order: Int? = null,
    val port: String? = null,
    val retryLimit: String? = null
)

fun HostEntity.mapToDomain(): Host = Host(id,connectionTimeout, ip, nii, order, port, retryLimit)
fun List<HostEntity>.mapToDomain(): List<Host> = map { it.mapToDomain() }