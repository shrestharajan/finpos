package global.citytech.finposframework.iso8583;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Saurav Ghimire on 1/26/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class MessageParserTemplateTest {

    MessageParserTemplate SUT;
    SpecInfo specInfo;

    @Before
    public void setup(){
        SUT = new DefaultMessageParser(specInfo);
    }

    @Test
    public void splitDataFromDataStream() {
        BitMap bitMap = new BitMap();
        bitMap.set(3);
        bitMap.set(11);
        bitMap.set(24);
        bitMap.set(39);
        bitMap.set(41);
        bitMap.set(42);
        bitMap.set(62);
        List<DataElement> elements = new ArrayList<>();

        String hexData = "01B56003030000303230307230058020C092003136443830012328754030303030303030303030303030303536303030313235303931323434303030303031210125145744053230300033374438300123287540D23076261000091099999F3431323334303237313030353132333435363738393130052431363831333335433436313935314134343932393835463241303230353234354633343031303038323032314330303834303741303030303030303033313031303841303235413331393530353830383030343030303039413033323130313235394330313030394630323036303030303030303030353435394630333036303030303030303030303030394630393032303030323946313030373036303130413033413041383030394631413032303532343946314530383330333433313330333033323338333439463236303834334635463638303345373233464533394632373031383039463333303345304638433839463334303334323033303039463335303132323946333630323033323639463337303446303538344642423946343130343030303030303031344630374130303030303030303331303130";

        elements = SUT.splitDataFromDataStream(bitMap,hextToByte(hexData));
        elements.forEach((element)->{
            System.out.println(element.getIndex()+" ::: "+element.getData().toString());
        });
    }

    public byte[] hextToByte(String str){
        byte[] val = new byte[str.length() / 2];
        for (int i = 0; i < val.length; i++) {
            int index = i * 2;
            int j = Integer.parseInt(str.substring(index, index + 2), 16);
            val[i] = (byte) j;
        }
        return val;
    }
}