package global.citytech.neps.domain.usecase.core.transaction

import global.citytech.neps.domain.repository.core.transaction.TransactionRepository
import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.neps.presentation.model.transaction.PurchaseRequestItem
import global.citytech.neps.presentation.model.transaction.PurchaseResponseItem
import global.citytech.posswitchintregator.neps.usecases.purchase.PurchaseResponseModel
import io.reactivex.Observable

class CoreTransactionUseCase(val transactionRepository: TransactionRepository) {
    fun purchase(configurationItem:ConfigurationItem,purchaseRequestItem: PurchaseRequestItem): Observable<PurchaseResponseModel> =
        transactionRepository.purchase(configurationItem,purchaseRequestItem)

    fun cleanUp() = transactionRepository.cleanUp()
}