package global.citytech.finpos.merchant.presentation.appnotification

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.domain.model.app.AppNotification
import kotlinx.android.synthetic.main.item_notification.view.*

/**
 * Created by Unique Shakya on 5/31/2021.
 */
class AppNotificationAdapter(
    private val appNotifications: List<AppNotification>,
    private val listener: Listener
) : RecyclerView.Adapter<AppNotificationAdapter.AppNotificationViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppNotificationViewHolder {
        context = parent.context
        return AppNotificationViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_notification, parent, false)
        )
    }

    override fun getItemCount(): Int = appNotifications.size

    override fun onBindViewHolder(holder: AppNotificationViewHolder, position: Int) {
        val appNotification = appNotifications[position]
        if (appNotification.title.isNullOrEmptyOrBlank())
            holder.itemView.tv_title.visibility = View.GONE
        else
            holder.itemView.tv_title.text = appNotification.title
        holder.itemView.tv_body.text = appNotification.body
        holder.itemView.tv_time.text = retrieveNotificationTime(appNotification.timeStamp)
        holder.itemView.iv_type.setImageResource(appNotification.type.displayImageId)
        holder.itemView.setOnClickListener {
            listener.onAppNotificationClicked(appNotification)
        }
    }

    private fun retrieveNotificationTime(timeStamp: String): String? {
        if (timeStamp.length != 12)
            return ""
        val hour = timeStamp.subSequence(6, 8).toString()
        val minute = timeStamp.subSequence(8, 10).toString()
        return hour.plus(":").plus(minute)
    }

    class AppNotificationViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    }

    interface Listener {
        fun onAppNotificationClicked(appNotification: AppNotification)
    }
}