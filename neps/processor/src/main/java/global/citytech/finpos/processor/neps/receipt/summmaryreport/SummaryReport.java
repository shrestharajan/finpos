package global.citytech.finpos.processor.neps.receipt.summmaryreport;

import java.util.Map;

import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Saurav Ghimire on 3/30/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public class SummaryReport {
    private Map<CardSchemeType, CardSchemeSummary> map;
    private String transactionCurrencyName;
    private long salesCount;
    private double salesAmount;
    private long voidCount;
    private double voidAmount;
    private long refundCount;
    private double refundAmount;
    private long totalCount;
    private double totalAmount;

    public SummaryReport(String transactionCurrencyName, Map<CardSchemeType, CardSchemeSummary> map) {
        this.transactionCurrencyName = transactionCurrencyName;
        this.map = map;
        this.prepareTotals();
    }

    private void prepareTotals() {
        for (Map.Entry<CardSchemeType, CardSchemeSummary> entry : this.map.entrySet()) {
            this.salesCount = this.salesCount + entry.getValue().getSalesCount();
            this.salesAmount = this.salesAmount + entry.getValue().getSalesAmount();
            this.voidCount = this.voidCount + entry.getValue().getVoidCount();
            this.voidAmount = this.voidAmount + entry.getValue().getVoidAmount();
            this.refundCount = this.refundCount + entry.getValue().getRefundCount();
            this.refundAmount = this.refundAmount + entry.getValue().getRefundAmount();
        }
        this.totalCount = this.salesCount;
        this.totalAmount = this.salesAmount;
    }

    public Map<CardSchemeType, CardSchemeSummary> getMap() {
        return map;
    }

    public String getTransactionCurrencyName() {
        return transactionCurrencyName;
    }

    public String getSalesCount() {
        return StringUtils.ofRequiredLength(salesCount, 4);
    }

    public String getSalesAmount() {
        return StringUtils.formatAmountTwoDecimal(HelperUtils.fromIsoAmount(String.valueOf(salesAmount)));
    }

    public String getVoidCount() {
         return StringUtils.ofRequiredLength(voidCount, 4);
    }

    public String  getVoidAmount() {
        return StringUtils.formatAmountTwoDecimal(HelperUtils.fromIsoAmount(String.valueOf(voidAmount)));
    }

    public String getRefundCount() {
        return StringUtils.ofRequiredLength(refundCount, 4);
    }

    public String  getRefundAmount() {
        return StringUtils.formatAmountTwoDecimal(HelperUtils.fromIsoAmount(String.valueOf(refundAmount)));
    }

    public String getTotalCount() {
        return StringUtils.ofRequiredLength(totalCount, 4);
    }

    public String getTotalAmount() {
        return StringUtils.formatAmountTwoDecimal(HelperUtils.fromIsoAmount(String.valueOf(totalAmount)));
    }
}
