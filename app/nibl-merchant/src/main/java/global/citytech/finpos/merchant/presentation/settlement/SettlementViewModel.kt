package global.citytech.finpos.merchant.presentation.settlement

import android.app.AlarmManager
import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import global.citytech.common.extensions.isNullOrEmptyOrBlank
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.AppNotificationRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.AutoReversalQueueRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.reconciliation.ReconciliationRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.Action
import global.citytech.finpos.merchant.domain.model.app.AppNotification
import global.citytech.finpos.merchant.domain.model.app.Type
import global.citytech.finpos.merchant.domain.model.reconciliation.ReconciliationResponseEntity
import global.citytech.finpos.merchant.domain.usecase.app.AppNotificationUseCase
import global.citytech.finpos.merchant.domain.usecase.app.CoreAutoReversalQueueUseCase
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.core.reconciliation.CoreReconciliationUseCase
import global.citytech.finpos.merchant.framework.datasource.app.AppNotificationDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.AutoReversalQueueDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.reconciliation.ReconciliationDataSourceImpl
import global.citytech.finpos.merchant.presentation.alarms.SettlementAlarmSetter
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.service.ActivityLogPushService
import global.citytech.finpos.merchant.utils.AppConstant
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finposframework.utility.StringUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

/**
 * Created by Unique Shakya on 5/31/2021.
 */
class SettlementViewModel(val context: Application) : BaseAndroidViewModel(context) {

    private val TAG = SettlementViewModel::class.java.name
    val successMessage by lazy { MutableLiveData<String>() }
    val errorMessage by lazy { MutableLiveData<String>() }
    val configurationItem by lazy { MutableLiveData<ConfigurationItem>() }
    val autoReversalPresent by lazy { MutableLiveData<Boolean>() }
    val settlementSuccess by lazy { MutableLiveData<Boolean>() }

    private val coreAutoReversalQueueUseCase = CoreAutoReversalQueueUseCase(
        AutoReversalQueueRepositoryImpl(
            AutoReversalQueueDataSourceImpl(context)
        )
    )

    private val reconciliationUseCase = CoreReconciliationUseCase(
        ReconciliationRepositoryImpl(
            ReconciliationDataSourceImpl()
        )
    )

    private val localDataUseCase: LocalDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    private val appNotificationUseCase = AppNotificationUseCase(
        AppNotificationRepositoryImpl(
            AppNotificationDataSourceImpl(
                context.applicationContext
            )
        )
    )

    fun checkForAutoReversal() {
        if (coreAutoReversalQueueUseCase.isQueueEmpty()) {
            printLog(TAG, "::: AUTO REVERSAL ::: QUEUE IS EMPTY")
            autoReversalPresent.value = false
        } else {
            printLog(TAG, "::: AUTO REVERSAL ::: QUEUE IS NOT EMPTY")
            autoReversalPresent.value = coreAutoReversalQueueUseCase.isActive()
        }
    }

    fun getConfigurationItem() {
        compositeDisposable.add(localDataUseCase.getWholeConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.mapToPresentation() }
            .subscribe({ configurationItem.value = it }, {
                printLog(TAG, it.message.toString())
                it.printStackTrace()
                errorMessage.value = it.message
            })
        )
    }

    fun setSettlementIsActive() {
        localDataUseCase.saveSettlementActiveStatus(true)
    }

    fun reconciliation(it: ConfigurationItem) {
        isLoading.value = true
        compositeDisposable.add(
            reconciliationUseCase.reconcile(it, localDataUseCase.isSettlementActive(false))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    ActivityLogPushService.pushActivityLog()
                    isLoading.value = false
                    handleSettlementResponse(it!!)
                }, {
                    printLog(TAG, it.message.toString())
                    isLoading.value = false
                    ActivityLogPushService.pushActivityLog()
                    handleSettlementError(it)
                })
        )
    }

    fun rescheduleSettlementTimeAlarm() {
        val settlementAlarmSetter = SettlementAlarmSetter(context.applicationContext)
        val settlementTime = localDataUseCase.getSettlementTime("")
        if (settlementTime.isNullOrEmptyOrBlank()) {
            return
        }
        val timeInMillisecondFromCalendar =
            settlementAlarmSetter.getSettlementTimeCalendar(settlementTime).timeInMillis
        val timeInNextDay = timeInMillisecondFromCalendar.plus(AlarmManager.INTERVAL_DAY)
        settlementAlarmSetter.rescheduleAlarmForGivenTime(timeInNextDay)

    }

    private fun handleSettlementResponse(it: ReconciliationResponseEntity) {
        if (it.isSuccess!!) {
            successMessage.value = it.message
            localDataUseCase.saveSettlementActiveStatus(false)
            updateLastSettlementTimeStamp()
            appNotificationUseCase.removeNotificationByAction(Action.SETTLEMENT)
            //cancelRescheduledSettlement()
        } else {
            printLog(TAG, it.message.toString())
            errorMessage.value = it.message
            rescheduleForceSettlement(it.message!!)
        }
        localDataUseCase.setForceSettlementPreference(false)
        settlementSuccess.value = it.isSuccess
    }

    private fun handleSettlementError(it: Throwable) {
        printLog(TAG, it.message.toString())
        it.printStackTrace()
        errorMessage.value = it.message
        rescheduleForceSettlement(it.message!!)
        localDataUseCase.setForceSettlementPreference(false)
        settlementSuccess.value = false
    }

    private fun updateLastSettlementTimeStamp() {
        val currentTimeInMilis = Calendar.getInstance().timeInMillis;
        localDataUseCase.updateLastSuccessSettlementTimeInMillis(currentTimeInMilis)
    }

    fun cancelRescheduledSettlement() {
        NiblMerchant.INSTANCE.cancelRescheduledSettlement()
    }

    fun rescheduleForceSettlement(message: String) {
        if (localDataUseCase.getForceSettlementPreference(false)) {
            appNotificationUseCase.addNotification(
                AppNotification(
                    title = context.getString(R.string.title_settlement_pending),
                    body = message,
                    action = Action.SETTLEMENT,
                    read = false,
                    type = Type.WARNING,
                    timeStamp = StringUtils.dateTimeStamp()
                )
            )
        }
    }

    fun sendAppInstallBroadCastToPlatform(context: Context, message: String) {
        if (localDataUseCase.isBroadcastAcknowledgeActive()) {
            localDataUseCase.setBroadcastAcknowledgeActive(false)
            val appDownloadExtra = localDataUseCase.getAppDownloadExtra()
            val intent = Intent("android.intent.action.application.updated")
            intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
            intent.putExtra(AppConstant.EXTRA_APP_DOWNLOAD, appDownloadExtra)
            intent.putExtra(AppConstant.EXTRA_APP_DOWNLOAD_PRE_ACTION_RESULT, message)
            intent.setPackage("global.citytech.tms")
            context.sendBroadcast(intent)
        }
    }
}