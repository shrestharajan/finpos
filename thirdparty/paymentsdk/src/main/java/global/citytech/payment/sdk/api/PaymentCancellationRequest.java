package global.citytech.payment.sdk.api;

/**
 * Created by Rishav Chudal on 04/03/2022.
 */
public class PaymentCancellationRequest {
    private final String paymentRequestNumber;
    private final String requestedBy;
    private String additionalData;

    public PaymentCancellationRequest(String paymentRequestNumber, String requestedBy) {
        this.paymentRequestNumber = paymentRequestNumber;
        this.requestedBy = requestedBy;
    }

    public String getPaymentRequestNumber() {
        return paymentRequestNumber;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }
}
