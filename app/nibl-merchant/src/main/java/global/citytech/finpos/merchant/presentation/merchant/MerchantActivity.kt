package global.citytech.finpos.merchant.presentation.merchant

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import global.citytech.common.Constants
import global.citytech.common.extensions.openActivity
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityMerchantBinding
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.merchant.numpad.NumpadLayoutActivity
import global.citytech.finpos.merchant.presentation.merchant.password.ChangeMerchantPasswordActivity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.settlement.SettlementActivity
import global.citytech.finpos.merchant.presentation.transactions.TransactionsActivity
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finpos.merchant.utils.handleDebounce
import global.citytech.finpos.merchant.utils.loadCurrency
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.notifier.Notifier
import kotlinx.android.synthetic.main.activity_admin.switch_vat_refund
import kotlinx.android.synthetic.main.activity_merchant.*

/**
 * Created by Unique Shakya on 8/12/2020.
 */
class MerchantActivity : AppBaseActivity<ActivityMerchantBinding, MerchantViewModel>() {

    lateinit var configurationItem: ConfigurationItem
    private val logger = Logger(MerchantActivity::class.java.name)

    private var broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            logger.log("BROADCAST :::: BROADCAST RECEIVED IN TRANSACTION ACTIVITY ::: ".plus(intent?.action))
            if (intent?.action == Constants.INTENT_ACTION_NOTIFICATION) {
                handleNotificationBroadcast(intent)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        initObservers()
        getViewModel().getConfigurationItem()
        initSoundMode()
    }

    override fun onResume() {
        super.onResume()
        registerBroadCastReceivers()
    }

    override fun onPause() {
        unregisterBroadcastReceivers()
        super.onPause()
    }

    private fun unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        unregisterReprintBroadCastReceivers()
    }

    private fun registerBroadCastReceivers() {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, IntentFilter(Constants.INTENT_ACTION_NOTIFICATION))
        registerReprintBroadcastReceivers()
    }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayout(): Int = R.layout.activity_merchant

    override fun getViewModel(): MerchantViewModel =
        ViewModelProviders.of(this)[MerchantViewModel::class.java]

    private fun initObservers() {

        getViewModel().isLoading.observe(this, Observer {
            if (it) {
                iv_back.isEnabled = false
                rl_progress.visibility = View.VISIBLE
            } else {
                rl_progress.visibility = View.GONE
                iv_back.isEnabled = true
                hideProgressDialog()
            }

        })



        getViewModel().message.observe(this, Observer {
            hideProgress()
            showNormalMessage(
                MessageConfig.Builder()
                    .message(it)
                    .positiveLabel("OK")
                    .onPositiveClick {})
        })

        getViewModel().loadingMessage.observe(this, Observer {
            if (it.isNullOrEmptyOrBlank()) {
                hideProgress()
            } else {
                showProgress(it)
            }
        })

        getViewModel().successMessage.observe(this, Observer {
            hideProgress()
            showSuccessMessage(
                MessageConfig.Builder()
                    .message(it)
                    .positiveLabel("OK")
                    .onPositiveClick {})
        })

        getViewModel().failureMessage.observe(this, Observer {
            hideProgress()
            showFailureMessage(
                MessageConfig.Builder()
                    .message(it)
                    .positiveLabel("OK")
                    .onPositiveClick {})
        })

        getViewModel().configurationItem.observe(this, Observer {
            this.configurationItem = it
        })

        getViewModel().logonResponse.observe(this, Observer {
            if (it)
                showSuccessMessage(
                    MessageConfig.Builder()
                        .message("LOG ON SUCCESSFUL")
                        .positiveLabel("OK")
                        .onPositiveClick {})
        })

        getViewModel().exchangeKeysResponse.observe(this, Observer {
            if (it)
                showSuccessMessage(
                    MessageConfig.Builder()
                        .message("KEY EXCHANGE SUCCESSFUL")
                        .positiveLabel("OK")
                        .onPositiveClick {})
        })

    }

    private fun initViews() {

        iv_back.setOnClickListener {
            finish()
        }

        btn_reconciliation.setOnClickListener {
            btn_reconciliation.handleDebounce()
            configurationItem.let {
                startSettlementActivity()
            }
        }

        btn_duplicate_receipt.setOnClickListener {
            btn_duplicate_receipt.handleDebounce()
            showConfirmationMessage(MessageConfig.Builder()
                .message(getString(R.string.msg_duplicate_receipt_confirmation))
                .positiveLabel(getString(R.string.action_yes))
                .onPositiveClick { getViewModel().duplicateReceipt() }
                .negativeLabel(getString(R.string.action_no))
                .onNegativeClick { })
        }

        btn_duplicate_reconciliation_receipt.setOnClickListener {
            btn_duplicate_reconciliation_receipt.handleDebounce()
            showConfirmationMessage(MessageConfig.Builder()
                .message(getString(R.string.msg_duplicate_settlement_confirmation))
                .positiveLabel(getString(R.string.action_yes))
                .onPositiveClick {
                    getViewModel().duplicateReconciliationReceipt()
                }
                .negativeLabel(getString(R.string.action_no))
                .onNegativeClick { })
        }

        btn_detail_report.setOnClickListener {
            btn_detail_report.handleDebounce()
            configurationItem.let {
                showConfirmationMessage(MessageConfig.Builder()
                    .message(getString(R.string.msg_detail_report_confirmation))
                    .positiveLabel(getString(R.string.action_yes))
                    .onPositiveClick {
                        getViewModel().detailReport(configurationItem)
                    }
                    .negativeLabel(getString(R.string.action_no))
                    .onNegativeClick { })
            }
        }

        btn_summary.setOnClickListener {
            btn_summary.handleDebounce()
            configurationItem.let {
                showConfirmationMessage(MessageConfig.Builder()
                    .message(getString(R.string.msg_summary_report_confirmation))
                    .positiveLabel(getString(R.string.action_yes))
                    .onPositiveClick {
                        getViewModel().summaryReport(configurationItem)
                    }
                    .negativeLabel(getString(R.string.action_no))
                    .onNegativeClick { })
            }
        }

        btn_txns.setOnClickListener {
            btn_txns.handleDebounce()
            openActivity(TransactionsActivity::class.java)
        }

        btn_change_password.setOnClickListener {
            btn_change_password.handleDebounce()
            openActivity(ChangeMerchantPasswordActivity::class.java)
        }

        btn_numpad_layout.setOnClickListener {
            btn_numpad_layout.handleDebounce()
            openActivity(NumpadLayoutActivity::class.java)
        }
    }

    private fun startSettlementActivity() {
        SettlementActivity.getLaunchIntent(
            this,
            isManualSettlement = true,
            isSettlementSuccessConfirmationRequired = true
        )
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun handleNotificationBroadcast(intent: Intent) {
        val eventType =
            intent.getSerializableExtra(Constants.EXTRA_EVENT_TYPE) as Notifier.EventType
        logger.log("BROADCAST :::: EVENT TYPE IN BROADCAST ::: $eventType.name")
        val message = intent.getStringExtra(Constants.EXTRA_NOTIFICATION_MESSAGE)
        logger.log("BROADCAST :::: MESSAGE IN BROADCAST ::: $message")
        when (eventType) {
            Notifier.EventType.PREPARING_RECONCILIATION_TOTALS,
            Notifier.EventType.PREPARING_DETAIL_REPORT,
            Notifier.EventType.CONNECTING_TO_SWITCH,
            Notifier.EventType.RESPONSE_RECEIVED,
            Notifier.EventType.BATCH_UPLOADING,
            Notifier.EventType.BATCH_UPLOAD_PROGRESS,
            Notifier.EventType.PRINTING_SETTLEMENT_REPORT -> showProgressDialog(message)
            else -> logger.log("NOT SUPPORTED ACTION")
        }
    }

    private fun showProgressDialog(message: String?) {
        showProgress(message)
    }

    private fun hideProgressDialog() {
        hideProgress()
    }

    private fun initSoundMode() {
        val isSoundModeOn = getViewModel().getSoundModeStatus()
        switch_sound_mode.isChecked = isSoundModeOn
        switch_sound_mode.setOnCheckedChangeListener { _, isChecked ->
            getViewModel().saveSoundModePreference(isChecked)
        }
    }

}