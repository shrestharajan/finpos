package global.citytech.finposframework.utility;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {
    public static final String DATE_FORMAT_FINPOS_TXN_LOG = "MMddyyyy";
    public static final String TIME_FORMAT_FINPOS_TXN_LOG = "HHmmss";
    public static final String DATE_FORMAT_ONE = "MMdd";
    public static final String DATE_FORMAT_TWO = "yyMMdd";


    public static String HHmmssTime() {
        return new SimpleDateFormat("HHmmss").format(new Date());
    }

    public static String yyMMddDate() {
        Date mCurrentDate = Calendar.getInstance().getTime();
        String myFormat = "yyMMdd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        return sdf.format(mCurrentDate.getTime());
    }

    public static String yyMMddHHmmssSSSDate() {
        return new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date());
    }

    public static String yyMMddHHmmssDate() {
        return new SimpleDateFormat("yyMMddHHmmss").format(new Date());
    }

    public static String MMDDhhmmssDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("MMDDhhmmss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.format(new Date());
    }

    public static String yymmDate() {
        Date mCurrentDate = Calendar.getInstance().getTime();
        String myFormat = "yyMM";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        return sdf.format(mCurrentDate.getTime());
    }

    public static String yyMMddDateWithDivider() {
        Date mCurrentDate = Calendar.getInstance().getTime();
        String myFormat = "yyyy/MM/dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        return sdf.format(mCurrentDate.getTime());
    }

    public static String HHmmssTimeWithDivider() {
        return new SimpleDateFormat("HH:mm:ss").format(new Date());
    }

    public static void main(String[] args) {
        System.out.println(HHmmssTime());
        System.out.println(yyMMddDate());
        System.out.println(yyMMddHHmmssSSSDate());
        System.out.println(yyMMddHHmmssDate());
        System.out.println(MMDDhhmmssDate());
    }


    public static String toYYMMFormat(String MMYYDate) {
        return MMYYDate.substring(2, 4).concat(MMYYDate.substring(0, 2));
    }

    public static String getCurrentYearLastTwoDigits() {
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        return String.valueOf(currentYear).substring(2, 4);
    }

    public static String getCurrentMonth() {
        Calendar calendar = Calendar.getInstance();
        int currentMonth = calendar.get(Calendar.MONTH) + 1;
        return String.format("%02d", currentMonth);
    }

    public static Timestamp toTimestamp(String MMdd, String hhmmss) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
            Calendar calendar = Calendar.getInstance();
            int currentYear = calendar.get(Calendar.YEAR);
            Date parsedDate = dateFormat.parse(currentYear + MMdd + hhmmss);
            Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
            return timestamp;
        } catch (Exception e) {
            return null;
        }
    }

    public static String getCurrentYear() {
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        return String.valueOf(currentYear);
    }

    public static String yyyyMMddHHmmssDate() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }
}
