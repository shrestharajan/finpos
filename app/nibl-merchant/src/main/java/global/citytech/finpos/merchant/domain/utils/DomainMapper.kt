package global.citytech.finpos.merchant.domain.utils

import global.citytech.finpos.merchant.domain.model.device.DeviceResponse
import global.citytech.finpos.merchant.domain.model.device.DeviceResult

/**
 * Created by Rishav Chudal on 8/10/20.
 */
fun global.citytech.finposframework.hardware.common.DeviceResponse.mapToDomain():
        DeviceResponse {
    val deviceResult: DeviceResult;
    when (result) {
        global.citytech.finposframework.hardware.common.Result.NONE -> {
            deviceResult = DeviceResult.NONE
        }
        global.citytech.finposframework.hardware.common.Result.SUCCESS -> {
            deviceResult = DeviceResult.SUCCESS
        }
        else -> {
            deviceResult = DeviceResult.FAILURE
        }
    }
    return DeviceResponse(deviceResult, message)
}
