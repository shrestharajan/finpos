package global.citytech.finpos.processor.nibl.transaction.card;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finpos.nibl.iso8583.S2MActionCode;
import global.citytech.finpos.nibl.iso8583.S2MIccDataList;
import global.citytech.finpos.processor.nibl.transaction.TransactionSetTableVerifier;
import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils;import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.cards.tvr.TvrResults;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.IdentifyCardResponse;
import global.citytech.finposframework.usecases.TransactionStatusFromHost;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.CardProcessor;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StringUtils;
import global.citytech.finposframework.utility.TvrResultParser;

import static global.citytech.finposframework.utility.TMSConfigurationConstants.*;


public class IccCardProcessor implements CardProcessor {

    ReadCardService readCardService;
    TransactionRepository transactionRepository;
    ApplicationRepository applicationRepository;
    TransactionAuthenticator transactionAuthenticator;
    Notifier notifier;
    TerminalRepository terminalRepository;

    public IccCardProcessor(
            ReadCardService readCardService,
            TransactionRepository transactionRepository,
            ApplicationRepository applicationRepository,
            TransactionAuthenticator transactionAuthenticator,
            Notifier notifier,
            TerminalRepository terminalRepository
    ) {
        this.readCardService = readCardService;
        this.transactionRepository = transactionRepository;
        this.applicationRepository = applicationRepository;
        this.transactionAuthenticator = transactionAuthenticator;
        this.notifier = notifier;
        this.terminalRepository = terminalRepository;
    }

    @Override
    public ReadCardResponse validateReadCardResponse(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse) {
        if (readCardResponse == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (readCardResponse.getResult() == Result.TIMEOUT) {
            throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
        }
        if (readCardResponse.getResult() == Result.APPLICATION_BLOCKED) {
            throw new PosException(PosError.DEVICE_ERROR_APPLICATION_BLOCKED);
        }
        if (readCardResponse.getResult() == Result.CARD_BLOCKED) {
            throw new PosException(PosError.DEVICE_ERROR_CARD_BLOCKED);
        }
        if (readCardResponse.getResult() == Result.USER_CANCELLED) {
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        }
        if (readCardResponse.getResult() == Result.PROCESSING_ERROR) {
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
        if (readCardResponse.getResult() == Result.ICC_FALLBACK_TO_MAGNETIC) {
            return this.handleICCToMagneticFallbackCases(readCardRequest);
        }
        if (readCardResponse.getResult() == Result.ICC_CARD_DETECT_ERROR) {
            return this.handleICCToMagneticFallbackCases(readCardRequest);
        }
        if (readCardResponse.getResult() == Result.CALLBACK_AMOUNT) {
            // TODO implement amount callback notify UI and HW SDK
        }

        if (readCardResponse.getResult() == Result.OFFLINE_DECLINED) {
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_DECLINED);
        }

        if (readCardResponse.getResult() == Result.FAILURE || readCardResponse.getCardDetails() == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (StringUtils.isEmpty(readCardResponse.getCardDetails().getPrimaryAccountNumber()) ||
                StringUtils.isEmpty(readCardResponse.getCardDetails().getTrackTwoData())) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        CardIdentifier cardIdentifier = new CardIdentifier(transactionRepository, applicationRepository);
        IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(readCardResponse);
        readCardResponse.getCardDetails().setCardScheme(identifyCardResponse.getCardDetails().getCardScheme());
        return readCardResponse;
    }

    @Override
    public ReadCardResponse validateReadCardResponseForICC(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse) {
        if (readCardResponse == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (readCardResponse.getResult() == Result.TIMEOUT) {
            throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
        }
        if (readCardResponse.getResult() == Result.APPLICATION_BLOCKED) {
            throw new PosException(PosError.DEVICE_ERROR_APPLICATION_BLOCKED);
        }
        if (readCardResponse.getResult() == Result.CARD_BLOCKED) {
            throw new PosException(PosError.DEVICE_ERROR_CARD_BLOCKED);
        }
        if (readCardResponse.getResult() == Result.USER_CANCELLED) {
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        }
        if (readCardResponse.getResult() == Result.PROCESSING_ERROR) {
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
        if (readCardResponse.getResult() == Result.ICC_FALLBACK_TO_MAGNETIC) {
            return this.handleICCToMagneticFallbackCases(readCardRequest);
        }
        if (readCardResponse.getResult() == Result.ICC_CARD_DETECT_ERROR) {
          throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (readCardResponse.getResult() == Result.CALLBACK_AMOUNT) {
            // TODO implement amount callback notify UI and HW SDK
        }
        if (readCardResponse.getResult() == Result.FAILURE || readCardResponse.getCardDetails() == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (StringUtils.isEmpty(readCardResponse.getCardDetails().getPrimaryAccountNumber()) ||
                StringUtils.isEmpty(readCardResponse.getCardDetails().getTrackTwoData())) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        CardIdentifier cardIdentifier = new CardIdentifier(transactionRepository, applicationRepository);
        IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(readCardResponse);
        readCardResponse.getCardDetails().setCardScheme(identifyCardResponse.getCardDetails().getCardScheme());
        return readCardResponse;
    }

    private ReadCardResponse handleICCToMagneticFallbackCases(ReadCardRequest readCardRequest) {
        this.readCardService.cleanUp();
        this.notifier.notify(Notifier.EventType.SWITCH_ICC_TO_MAGNETIC,
                Notifier.EventType.SWITCH_ICC_TO_MAGNETIC.getDescription());
        ReadCardRequest contactFallbackReadCardRequest = this.prepareICCFallbackReadCardRequest(readCardRequest);
        ReadCardResponse readCardResponse = this.readCardService.readCardDetails(contactFallbackReadCardRequest);
        readCardResponse.setFallbackIccToMag(true);
        return new MagneticCardProcessor(this.transactionRepository, transactionAuthenticator, applicationRepository,
                readCardService, notifier, this.terminalRepository)
                .validateReadCardResponse(readCardRequest, readCardResponse);
    }


    private ReadCardRequest prepareICCFallbackReadCardRequest(ReadCardRequest readCardRequest) {
        List<CardType> cardTypeList = new ArrayList<>();
        cardTypeList.add(CardType.MAG);
        ReadCardRequest fallbackRequest = new ReadCardRequest(cardTypeList, readCardRequest.getTransactionType(),
                readCardRequest.getTransactionType9C(), readCardRequest.getEmvParametersRequest());
        fallbackRequest.setCashBackAmount(readCardRequest.getCashBackAmount());
        fallbackRequest.setAmount(readCardRequest.getAmount());
        fallbackRequest.setCardDetailsBeforeEmvNext(readCardRequest.getCardDetailsBeforeEmvNext());
        fallbackRequest.setCardReadTimeOut(readCardRequest.getCardReadTimeOut());
        fallbackRequest.setFallbackFromIccToMag(readCardRequest.getFallbackFromIccToMag());
        fallbackRequest.setForceOnlineForICC(readCardRequest.getForceOnlineForICC());
        fallbackRequest.setForceOnlineForPICC(readCardRequest.getForceOnlineForPICC());
        fallbackRequest.setIccDataList(readCardRequest.getIccDataList());
        fallbackRequest.setMaxmPinLength(readCardRequest.getMaxmPinLength());
        fallbackRequest.setMinmPinLength(readCardRequest.getMinmPinLength());
        fallbackRequest.setMultipleAidSelectionListener(readCardRequest.getMultipleAidSelectionListener());
        fallbackRequest.setPackageName(readCardRequest.getPackageName());
        fallbackRequest.setPinBlockFormat(readCardRequest.getPinBlockFormat());
        fallbackRequest.setPinpadRequired(readCardRequest.isPinpadRequired());
        fallbackRequest.setPinpadTimeout(readCardRequest.getPinpadTimeout());
        fallbackRequest.setStan(readCardRequest.getStan());
        fallbackRequest.setPrimaryAccountNumber(readCardRequest.getPrimaryAccountNumber());
        fallbackRequest.setTransactionDate(DateUtils.yyMMddDate());
        fallbackRequest.setTransactionTime(DateUtils.HHmmssTime());
        fallbackRequest.setCurrencyName(readCardRequest.getCurrencyName());
        fallbackRequest.setPinPadFixedLayout(readCardRequest.getPinPadFixedLayout());
        return fallbackRequest;
    }

    @Override
    public ReadCardResponse processCard(String stan, PurchaseRequest purchaseRequest) {
        ReadCardRequest readCardRequest = this.prepareReadCardRequest(stan, purchaseRequest);
        ReadCardResponse readCardResponse = readCardService.processEMV(readCardRequest);
        if (readCardResponse.getResult() != Result.FAILURE) {
            if (readCardResponse.getResult() == Result.PROCESSING_ERROR) {
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
            } else if (readCardResponse.getResult() == Result.PINPAD_ERROR) {
                throw new PosException(PosError.DEVICE_ERROR_PINPAD_ERROR);
            } else if (readCardResponse.getResult() == Result.USER_CANCELLED) {
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            } else if (readCardResponse.getResult() == Result.TIMEOUT) {
                throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
            } else if (this.transactionDeclined(readCardResponse)) {
                this.handleTransactionDeclinedCases(readCardResponse);
            }
            readCardResponse.getCardDetails().setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
            System.out.println("CARD Response is "+ readCardResponse);
            return readCardResponse;
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    private void handleTransactionDeclinedCases(ReadCardResponse readCardResponse) {
        String tvr = readCardResponse.getCardDetails().getTagCollection().get(IccData.TVR.getTag()).getData();
        List<TvrResults> tvrResults = TvrResultParser.parseTvr(tvr);
        if (tvrResults.contains(TvrResults.BYTE_2_BIT_5))
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_DECLINED_SERVICE_NOT_ALLOWED_FOR_CARD);
        else
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_DECLINED);
    }

    private boolean transactionDeclined(ReadCardResponse readCardResponse) {
        try {
            return readCardResponse.getCardDetails().getTagCollection().get(IccData.CID.getTag())
                    .getData().equals("00");
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }

    @Override
    public ReadCardResponse processOnlineResult(ReadCardResponse readCardResponse, IsoMessageResponse isoMessageResponse) {
        if (this.isArqcTransaction(readCardResponse)) {
            int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, 39);
            ReadCardResponse afterSetOnlineResultResponse = readCardService.setOnlineResult(
                    actionCode,
                    mapTransactionHostStatus(S2MActionCode.getByActionCode(actionCode)
                            .getTransactionStatusFromHost()),
                    this.prepareField55DataForOnlineResult(actionCode, IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, 55)),
                    false);
            readCardResponse.getCardDetails().setIccDataBlock(afterSetOnlineResultResponse
                    .getCardDetails().getIccDataBlock());
            readCardResponse.getCardDetails().setTagCollection(afterSetOnlineResultResponse
                    .getCardDetails().getTagCollection());
            readCardResponse.getCardDetails().setTransactionState(afterSetOnlineResultResponse
                    .getCardDetails().getTransactionState());
            return readCardResponse;
        }
        return readCardResponse;
    }

    @Override
    public ReadCardResponse processUnableToGoOnlineResult(ReadCardResponse readCardResponse) {
        if (this.isArqcTransaction(readCardResponse)) {
            ReadCardResponse afterSetOnlineResultResponse = readCardService.setOnlineResult(
                    -99, ReadCardService.OnlineResult.UNABLE_TO_GO_ONLINE,
                    "", false);
            readCardResponse.getCardDetails().setIccDataBlock(afterSetOnlineResultResponse
                    .getCardDetails().getIccDataBlock());
            readCardResponse.getCardDetails().setTagCollection(afterSetOnlineResultResponse
                    .getCardDetails().getTagCollection());
            readCardResponse.getCardDetails().setTransactionState(afterSetOnlineResultResponse
                    .getCardDetails().getTransactionState());
        }
        return readCardResponse;
    }

    private String prepareField55DataForOnlineResult(int actionCode, String field55FromHost) {
        StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isEmpty(field55FromHost) || !field55FromHost.contains("8A02")) {
            stringBuilder.append("8A02");
            stringBuilder.append(StringUtils.toHexaDecimal(StringUtils.ofRequiredLength(String.valueOf(actionCode), 2)));
        }
        if (!StringUtils.isEmpty(field55FromHost))
            stringBuilder.append(field55FromHost);
        return stringBuilder.toString();
    }

    private ReadCardService.OnlineResult mapTransactionHostStatus(TransactionStatusFromHost transactionStatusFromHost) {
        switch (transactionStatusFromHost) {
            case ONLINE_APPROVED:
                return ReadCardService.OnlineResult.ONLINE_APPROVED;
            case ONLINE_DECLINED:
                return ReadCardService.OnlineResult.ONLINE_DECLINED;
            default:
                return ReadCardService.OnlineResult.UNABLE_TO_GO_ONLINE;
        }
    }

    private boolean isArqcTransaction(ReadCardResponse readCardResponse) {
        try {
            return readCardResponse
                    .getCardDetails()
                    .getTagCollection()
                    .get(IccData.CID.getTag())
                    .getData()
                    .equals("80");
        } catch (Exception e) {
            return false;
        }
    }

    private ReadCardRequest prepareReadCardRequest(String stan, PurchaseRequest purchaseRequest) {
        ReadCardRequest readCardRequest = new ReadCardRequest(
                TransactionUtils.getAllowedCardEntryModes(purchaseRequest.getTransactionType()),
                purchaseRequest.getTransactionType(),
                TransactionUtils.retrieveTransactionType9C(purchaseRequest.getTransactionType()),
                this.applicationRepository.retrieveEmvParameterRequest()
        );
        readCardRequest.setStan(stan);
        readCardRequest.setAmount(purchaseRequest.getAmount());
        readCardRequest.setCashBackAmount(purchaseRequest.getAdditionalAmount());
        readCardRequest.setPinpadRequired(
                this.retrievePinRequired(
                        purchaseRequest.getTransactionType(),
                        purchaseRequest
                                .getCardDetails()
                                .getCardScheme().getCardSchemeId()
                )
        );
        readCardRequest.setPrimaryAccountNumber(purchaseRequest.getCardDetails().getPrimaryAccountNumber());
        readCardRequest.setCardDetailsBeforeEmvNext(purchaseRequest.getCardDetails());
        readCardRequest.setTransactionDate(this.retrieveTransactionDate());
        readCardRequest.setTransactionTime(this.retrieveTransactionTime());
        readCardRequest.setIccDataList(S2MIccDataList.get());
        readCardRequest.setCurrencyName(
                this.applicationRepository.retrieveFromAdditionalDataEmvParameters(
                        KEY_TRANSACTION_CURRENCY_NAME
                )
        );
        readCardRequest.setPinPadFixedLayout(!this.terminalRepository.shouldShufflePinPad());
        return readCardRequest;
    }

    private boolean retrievePinRequired(TransactionType transactionType, String cardScheme) {
        TransactionSetTableVerifier verifier = new TransactionSetTableVerifier(
                this.applicationRepository,
                cardScheme,
                TransactionUtils.mapPurchaseTypeWithTransactionType(transactionType));
        return verifier.isPinVerificationNecessary();
    }

    private String retrieveTransactionTime() {
        return DateUtils.HHmmssTime();
    }

    private String retrieveTransactionDate() {
        return DateUtils.yyMMddDate();
    }
}