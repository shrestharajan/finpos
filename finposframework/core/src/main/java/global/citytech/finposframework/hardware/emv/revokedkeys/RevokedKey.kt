package global.citytech.finposframework.hardware.emv.revokedkeys

/**
 * Created by Rishav Chudal on 4/30/20.
 */
data class RevokedKey(val rid: String,
                      val index: String,
                      val certificateSerialNumber: String)