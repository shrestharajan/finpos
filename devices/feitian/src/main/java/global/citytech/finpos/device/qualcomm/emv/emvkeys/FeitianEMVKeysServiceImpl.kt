package global.citytech.finpos.device.qualcomm.emv.emvkeys

import android.content.Context
import com.ftpos.library.smartpos.emv.CAPublicKeyInfo
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.emv.IActionFlag
import com.ftpos.library.smartpos.errcode.ErrCode
import global.citytech.common.extensions.defaultOrEmptyValue
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_EMPTY_EMV_KEYS_LIST
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_FAILURE_EMV_KEYS_ERASE
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_FAILURE_EMV_KEYS_INJECTION
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_SUCCESS_EMV_KEYS_ERASE
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_SUCCESS_EMV_KEYS_INJECTION
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result.FAILURE
import global.citytech.finposframework.hardware.common.Result.SUCCESS
import global.citytech.finposframework.hardware.emv.emvkeys.EmvKey
import global.citytech.finposframework.hardware.emv.emvkeys.EmvKeysInjectionRequest
import global.citytech.finposframework.hardware.emv.emvkeys.EmvKeysService
import global.citytech.finposframework.utility.StringUtils

/**
 * Created by Rishav Chudal on 4/5/21.
 */
class FeitianEMVKeysServiceImpl constructor(context: Context): EmvKeysService,
    FeitianLogger.Listener {

    private val emv = Emv.getInstance(context.applicationContext)
    private lateinit var deviceResponse: DeviceResponse
    private val logger = FeitianLogger(this)
    private var finalSuccessCount = 0

    override fun injectEmvKeys(injectionRequest: EmvKeysInjectionRequest): DeviceResponse {
        this.deviceResponse = DeviceResponse(FAILURE, MSG_FAILURE_EMV_KEYS_INJECTION)
        if (injectionRequest.emvKeyList.isEmpty()) {
            onEMVKeysListEmpty()
        } else {
            onEMVKeysListNotEmpty(injectionRequest)
        }
        return this.deviceResponse
    }

    override fun eraseAllEmvKeys(): DeviceResponse {
        logger.logWithClassName("***** Erasing all EMV Keys *****")
        var deviceResponse = DeviceResponse(FAILURE, MSG_FAILURE_EMV_KEYS_ERASE)
        val resultClearEMVKeys = emv.manageCAPubKey(
            IActionFlag.CLEAR,
            null
        )
        if (resultClearEMVKeys == ErrCode.ERR_SUCCESS) {
            logger.logWithClassName("Erase EMV keys success...")
            deviceResponse = DeviceResponse(SUCCESS, MSG_SUCCESS_EMV_KEYS_ERASE)
        } else {
            logger.logWithClassName("Erase EMV keys failure...")
        }
        return deviceResponse
    }

    private fun onEMVKeysListEmpty() {
        logger.logWithClassName("EMV keys list is empty...")
        deviceResponse = DeviceResponse(FAILURE, MSG_EMPTY_EMV_KEYS_LIST)
    }

    private fun onEMVKeysListNotEmpty(injectionRequest: EmvKeysInjectionRequest) {
        eraseAllEmvKeys()
        proceedEMVKeysForInjection(injectionRequest)
        if (finalSuccessCount > 0) {
            this.deviceResponse = DeviceResponse(SUCCESS, MSG_SUCCESS_EMV_KEYS_INJECTION)
        }
    }
    
    private fun proceedEMVKeysForInjection(injectionRequest: EmvKeysInjectionRequest) {
        for (emvKey in injectionRequest.emvKeyList) {
            try {
                prepareAndInjectEmvKeys(emvKey)
            } catch (ex: Exception) {
                logger.logWithClassName("Exception in injecting EMV Key ::: ".plus(emvKey.rid))
                ex.printStackTrace()
            }
            logger.logWithClassName(
                "EMV Keys injected ::: "
                    .plus(finalSuccessCount)
                    .plus("/")
                    .plus(injectionRequest.emvKeyList.size)
            )
        }
    }
    
    private fun prepareAndInjectEmvKeys(emvKey: EmvKey) {
        this.logger.logWithClassName("***** Injecting EMV Keys *****")
        val caPublicKeyInfo = prepareCAPublicKeyInfo(emvKey)
        val resultEMVKeyInject = emv.manageCAPubKey(
            IActionFlag.ADD,
            caPublicKeyInfo
        )
        this.logger.logWithClassName("EMV Key injection result ::: ".plus(resultEMVKeyInject))
        if (resultEMVKeyInject == ErrCode.ERR_SUCCESS) {
            this.finalSuccessCount += 1
        }
    }
    
    private fun prepareCAPublicKeyInfo(emvKey: EmvKey): CAPublicKeyInfo {
        val caPublicKeyInfo = CAPublicKeyInfo()

        val rid = emvKey.rid.defaultOrEmptyValue()
        this.logger.debugWithClassName("********** RID ::: ".plus(rid).plus(" **********"))
        caPublicKeyInfo.rid = StringUtils.hexString2bytes(rid)

        val exponent = emvKey.exponent.defaultOrEmptyValue()
        this.logger.debugWithClassName("Exponent ::: ".plus(exponent))
        caPublicKeyInfo.exponent = StringUtils.hexString2bytes(exponent)

        var expiryDate: String = emvKey.expiryDate.defaultOrEmptyValue()
        if (expiryDate.length > 6) {
            expiryDate = expiryDate.substring(2)
        }
        this.logger.debugWithClassName("Expiry Date ::: ".plus(expiryDate))
        caPublicKeyInfo.expDate = StringUtils.hexString2bytes(expiryDate)

        val checkSum: String = emvKey.checkSum.defaultOrEmptyValue()
        this.logger.debugWithClassName("CheckSum ::: ".plus(checkSum))
        val checkSumInByteArray: ByteArray = StringUtils.hexString2bytes(checkSum)
        if (checkSumInByteArray.isEmpty()) {
            caPublicKeyInfo.digestFlag = 0x00.toByte()
        } else {
            caPublicKeyInfo.digestFlag = 0x01.toByte()
            caPublicKeyInfo.digest = checkSumInByteArray
        }

        val keyIndex: String = emvKey.index.defaultOrEmptyValue()
        this.logger.debugWithClassName("Key Index ::: ".plus(keyIndex))
        caPublicKeyInfo.index = StringUtils.hexString2bytes(keyIndex)[0]

        val keyModulus: String = emvKey.modulus.defaultOrEmptyValue()
        this.logger.debugWithClassName("Key Modulus ::: ".plus(keyModulus))
        caPublicKeyInfo.pubKey = StringUtils.hexString2bytes(keyModulus)

        val algorithm: String = emvKey.hashId.defaultOrEmptyValue()
        this.logger.debugWithClassName("Hash Algorithm ::: ".plus(algorithm))
        caPublicKeyInfo.alg = algorithm.toByte()

        return caPublicKeyInfo
    }

    override fun getClassName(): String {
        return FeitianEMVKeysServiceImpl::class.java.simpleName
    }
}