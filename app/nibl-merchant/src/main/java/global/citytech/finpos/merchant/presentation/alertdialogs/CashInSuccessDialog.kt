package global.citytech.finpos.merchant.presentation.alertdialogs

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.DialogFragment
import com.google.android.material.button.MaterialButton
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.dashboard.temp.DashActivity

class CashInSuccessDialog constructor(var amount: String, var context: Activity, val cashInDialogListener: CashInListener) : DialogFragment() {
    val TAG = CashInSuccessDialog::class.java

    companion object {
        const val TAG = " CashInSuccessDialog"
    }

    private lateinit var tvTitle: TextView
    private lateinit var tvAmount: AppCompatTextView
    private lateinit var tvCurrency: TextView
    private lateinit var imgTransactionResult: ImageView
    private lateinit var printButton: MaterialButton
    var cashInAmount: String = ""
    private lateinit var backbutton: ImageView



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_cash_in_deposited_successful, null)


    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
        setViewAttributes()
        handleClickEvents()
        view.findViewById<AppCompatImageView>(R.id.iv_back).setOnClickListener {
            startActivity(Intent(view.context, DashActivity::class.java))
        }
        val button = view.findViewById<Button>(R.id.btn_print_receipt)
        button.setOnClickListener{
            cashInDialogListener.printCashIn()
            dismiss()
        }

    }

    private fun initViews(dialogView: View) {
//        this.listener= listener
        tvTitle = dialogView.findViewById(R.id.tv_title)
        tvCurrency = dialogView.findViewById(R.id.tv_currency)
        tvAmount = dialogView.findViewById(R.id.tv_amount)
        imgTransactionResult = dialogView.findViewById(R.id.img_transaction_result)
        printButton = dialogView.findViewById(R.id.btn_print_receipt)
        backbutton = dialogView.findViewById(R.id.iv_back)
        cashInAmount = amount

    }

    private fun setViewAttributes() {
        tvAmount.text = cashInAmount
    }


    private fun handleClickEvents() {
        this.printButton.setOnClickListener {
            this.hide()
//
        }
    }

    fun hide() {
        dialog?.dismiss()
    }

    override fun onResume() {
        super.onResume()
        setDialogLayoutParams()
    }

    private fun setDialogLayoutParams() {
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog?.window?.attributes = layoutParams
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    override fun getTheme(): Int {
        return R.style.FullScreenDialog
    }

    interface CashInListener{
        fun printCashIn()
    }

}

