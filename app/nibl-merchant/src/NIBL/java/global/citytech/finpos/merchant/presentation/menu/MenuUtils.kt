package global.citytech.finpos.merchant.presentation.menu

import android.app.Activity
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.dashboard.menu.PinChangeHandler


/**
 * Created by Saurav Ghimire on 1/19/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */

class MenuUtils {

    companion object {
        fun getRetailerMenus(): List<MenuItem> {
            val menus = ArrayList<MenuItem>()
            menus.add(MenuItem(menu = Menu.PRE_AUTHORIZATION, menuHandler = PreAuthHandler()))
            menus.add(MenuItem(menu = Menu.VOID, menuHandler = VoidHandler()))
            menus.add(MenuItem(menu = Menu.AUTHORIZATION_COMPLETION, menuHandler = AuthCompletionHandler()))
            menus.add(MenuItem(menu = Menu.ADMIN_MENU, menuHandler = AdminMenuHandler()))
            return menus
        }

        fun getCashModeMenus(): List<MenuItem> {
            val menus = ArrayList<MenuItem>()
            menus.add(MenuItem(menu = Menu.CASH_DEPOSIT, menuHandler = CashDepositHandler()))
            menus.add(MenuItem(menu = Menu.VOID, menuHandler = VoidHandler()))
            menus.add(MenuItem(menu = Menu.BALANCE_INQUIRY, menuHandler = BalanceInquiryHandler()))
            menus.add(MenuItem(menu = Menu.MINI_STATEMENT, menuHandler = MiniStatementHandler()))
            menus.add(MenuItem(menu = Menu.MERCHANT_MENU, menuHandler = MerchantMenuHandler()))
            menus.add(MenuItem(menu = Menu.ADMIN_MENU, menuHandler = AdminMenuHandler()))
            return menus
        }
    }
}

data class MenuItem(
    val menu: Menu? = null,
    val menuHandler: MenuHandler
)

enum class Menu(val title: Int,val icon: Int) {

    PRE_AUTHORIZATION(R.string.title_pre_auth, R.drawable.ic_book_black_24dp),
    VOID(R.string.title_auth_void, R.drawable.ic_cancel_black_24dp),
    AUTHORIZATION_COMPLETION(R.string.title_auth_completion, R.drawable.ic_auth_completion_24dp),
    REFUND(R.string.title_refund, R.drawable.ic_refresh_black_24dp),
    MERCHANT_MENU(R.string.title_merchant_menu, R.drawable.ic_store),
    ADMIN_MENU(R.string.title_admin_menu, R.drawable.ic_settings_black_24dp),
    CASH_DEPOSIT(R.string.title_cash_in, R.drawable.ic_cash_deposit),
    BALANCE_INQUIRY(R.string.title_balance_inquiry, R.drawable.ic_baseline_account_balance_wallet_24),
    MINI_STATEMENT(R.string.title_mini_statement, R.drawable.ic_baseline_receipt_24),
    PIN_CHANGE(R.string.pin_change, R.drawable.ic_pin_change),
    GREEN_PIN(R.string.green_pin, R.drawable.ic_green_pin_icon)


}