package global.citytech.finpos.device.qualcomm

import android.content.Context
import global.citytech.finpos.device.qualcomm.emv.aid.FeitianAIDParamServiceImpl
import global.citytech.finpos.device.qualcomm.emv.emvkeys.FeitianEMVKeysServiceImpl
import global.citytech.finpos.device.qualcomm.emv.revokedkeys.FeitianRevokedKeysServiceImpl
import global.citytech.finpos.device.qualcomm.emv.terminalsettings.FeitianEMVParamHandler
import global.citytech.finpos.device.qualcomm.init.FeitianInitSDKServiceImpl
import global.citytech.finpos.device.qualcomm.io.cards.transaction.FeitianReadCardServiceImpl
import global.citytech.finpos.device.qualcomm.io.led.FeitianLedServiceImpl
import global.citytech.finpos.device.qualcomm.io.pinpad.FeitianPinPadServiceImpl
import global.citytech.finpos.device.qualcomm.io.printer.FeitianPrinterServiceImpl
import global.citytech.finpos.device.qualcomm.io.status.FeitianIOStatusServiceImpl
import global.citytech.finpos.device.qualcomm.keys.FeitianKeysServiceImpl
import global.citytech.finpos.device.qualcomm.io.sound.FeitianSoundServiceImpl
import global.citytech.finpos.device.qualcomm.system.FeitianSystemServiceImpl
import global.citytech.finposframework.hardware.DeviceServiceType
import global.citytech.finposframework.hardware.DeviceServiceType.*
import global.citytech.finposframework.hardware.common.DeviceService
import global.citytech.finposframework.notifier.Notifier
import java.lang.Exception

/**
 * Created by Rishav Chudal on 4/5/21.
 */
class QualcommFactory {
    companion object {
        fun getService(
            context: Context,
            deviceServiceType: DeviceServiceType,
            notifier: Notifier
        ): DeviceService {
            return when(deviceServiceType) {
                INIT -> FeitianInitSDKServiceImpl(context)
                AID_PARAM -> FeitianAIDParamServiceImpl(context)
                EMV_KEYS -> FeitianEMVKeysServiceImpl(context)
                EMV_PARAM -> FeitianEMVParamHandler(context)
                KEYS -> FeitianKeysServiceImpl(context)
                CARD -> FeitianReadCardServiceImpl(context, notifier)
                PRINTER -> FeitianPrinterServiceImpl(context)
                HARDWARE_STATUS -> FeitianIOStatusServiceImpl(context)
                SYSTEM -> FeitianSystemServiceImpl(context)
                SOUND -> FeitianSoundServiceImpl(context)
                LED -> FeitianLedServiceImpl(context)
                REVOKED_KEYS -> FeitianRevokedKeysServiceImpl(context)
                PINPAD -> FeitianPinPadServiceImpl(context)
                else -> throw Exception("No such device service")
            }
        }
    }
}