package global.citytech.finpos.merchant.domain.usecase.qr

/**
 * @author sachin
 */
object NqrConstants {
    val NCHL_TAG: String = "NCHL"
    val PAYLOAD_FORMAT_INDICATOR: String = "01"
    val STATIC_QR = "11"
    val DYNAMIC_QR = "12"
    val MAX_MERCHANT_NAME_LENGTH = 25
    val MAX_MERCHANT_CITY_NAME_LENGTH = 15
    val MAX_ADDITIONAL_DATA_LENGTH = 999
}