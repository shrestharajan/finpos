package global.citytech.finpos.merchant.domain.model.app

data class AppDownload(
    val appId: String,
    val downloadId: Long,
    val path: String,
    val name: String,
    val packageName: String
)