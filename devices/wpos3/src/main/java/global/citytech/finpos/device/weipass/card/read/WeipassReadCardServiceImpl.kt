package global.citytech.finpos.device.weipass.card.read

import android.content.Context

import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService
import global.citytech.finposframework.notifier.Notifier

/**
 * Created by Rishav Chudal on 6/6/20.
 */
class WeipassReadCardServiceImpl constructor(
    context: Context,
    notifier: Notifier
) : WeipassReadCardTemplate(context, notifier),
    ReadCardService {

    override fun readCardDetails(readCardRequest: ReadCardRequest): ReadCardResponse {
        WeipassLogger.log("========== WeipassReadCardServiceImpl ::: readCardDetails ==========")
        return executeReadCard(readCardRequest)
    }

    override fun readEmv(readCardRequest: ReadCardRequest): ReadCardResponse {
        WeipassLogger.log("========== WeipassReadCardServiceImpl ::: readEmv ==========")
        return executeReadEmv(readCardRequest)
    }

    override fun processEMV(readCardRequest: ReadCardRequest): ReadCardResponse {
        WeipassLogger.log("========== WeipassReadCardServiceImpl ::: processEMV ==========")
        return executeProcessEmv(readCardRequest)
    }

    override fun setOnlineResult(
        responseCode: Int,
        onlineResult: ReadCardService.OnlineResult,
        responseField55Data: String,
        onlineAfterTcAac: Boolean
    ): ReadCardResponse {
        WeipassLogger.log("========== WeipassReadCardServiceImpl ::: setOnlineResult ==========")
        return executeEmvOnlineResult(responseCode, onlineResult, responseField55Data)
    }

    override fun getIccCardStatus(): ReadCardResponse {
        WeipassLogger.log("========== WeipassReadCardServiceImpl ::: getIccCardStatus ==========")
        return executeIccCardStatus()
    }

    override fun cleanUp() {
        WeipassLogger.log("========== WeipassReadCardServiceImpl ::: cleanUp ==========")
        executeCleanUp()
    }
}