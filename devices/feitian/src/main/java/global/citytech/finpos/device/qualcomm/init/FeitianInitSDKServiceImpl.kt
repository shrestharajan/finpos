package global.citytech.finpos.device.qualcomm.init

import android.content.Context
import com.ftpos.library.smartpos.emv.Emv
import com.ftpos.library.smartpos.servicemanager.OnServiceConnectCallback
import com.ftpos.library.smartpos.servicemanager.ServiceManager
import global.citytech.finpos.device.qualcomm.utils.FeitianConstants.SWITCH_DEBUG_FLAG
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_SUCCESS_SDK_INIT
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.init.InitializeSDKService
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.MSG_FAILURE_SDK_INIT
import java.util.concurrent.CountDownLatch

/**
 * Created by Rishav Chudal on 4/5/21.
 */
class FeitianInitSDKServiceImpl constructor(private val context: Context) : InitializeSDKService,
    FeitianLogger.Listener, OnServiceConnectCallback{

    private lateinit var deviceResponse: DeviceResponse
    private var logger : FeitianLogger = FeitianLogger(this)
    private lateinit var emv : Emv
    private val countDownLatch = CountDownLatch(1)

    override fun initializeSDK(): DeviceResponse {
        deviceResponse = DeviceResponse(Result.FAILURE, MSG_FAILURE_SDK_INIT)
        logger.logWithClassName("Initializing Feitian SDK...")
        this.unBindPosServer()
        this.bindPosServer()
        this.countDownLatch.await()
        return deviceResponse
    }

    private fun unBindPosServer() {
        logger.logWithClassName("Unbinding the POS Server...")
        ServiceManager.unbindPosServer()
    }

    private fun bindPosServer() {
        logger.logWithClassName("Binding POS Server...")
        ServiceManager.bindPosServer(
            context.applicationContext,
            this
        )
    }

    override fun onSuccess() {
        logger.logWithClassName("Success in binding Feitian POS Server...")
        emv = Emv.getInstance(context.applicationContext)
        emv.switchDebug(SWITCH_DEBUG_FLAG)
        deviceResponse = DeviceResponse(Result.SUCCESS, MSG_SUCCESS_SDK_INIT)
        this.countDownLatch.countDown()
    }

    override fun onFail(errorCode: Int) {
        logger.logWithClassName("Failure in binding Feitian POS Server...")
        logger.logWithClassName("Error Code ::: ".plus(errorCode))
        this.countDownLatch.countDown()
    }

    override fun getClassName(): String {
        return FeitianInitSDKServiceImpl::class.java.simpleName
    }
}