package global.citytech.finpos.processor.nibl.transaction.card;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finpos.nibl.iso8583.S2MActionCode;
import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedAction;
import global.citytech.finposframework.hardware.io.led.LedLight;
import global.citytech.finposframework.hardware.io.led.LedRequest;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.sound.Sound;
import global.citytech.finposframework.hardware.io.sound.SoundService;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.PICCWave;
import global.citytech.finposframework.usecases.IdentifyCardResponse;
import global.citytech.finposframework.usecases.TransactionStatusFromHost;
import global.citytech.finposframework.usecases.transaction.CardProcessor;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finposframework.utility.TMSConfigurationConstants.*;

/**
 * Created by Unique Shakya on 10/16/2020.
 */
public class PICCCardProcessor implements CardProcessor {

    private TransactionRepository transactionRepository;
    private ApplicationRepository applicationRepository;
    private ReadCardService readCardService;
    private Notifier notifier;
    private TransactionAuthenticator transactionAuthenticator;
    private LedService ledService;
    private SoundService soundService;
    private TerminalRepository terminalRepository;

    public PICCCardProcessor(TransactionRepository transactionRepository,
                            ApplicationRepository applicationRepository,
                            ReadCardService readCardService,
                            Notifier notifier,
                            TransactionAuthenticator transactionAuthenticator,
                            LedService ledService,
                            SoundService soundService,
                            TerminalRepository terminalRepository) {
        this.transactionRepository = transactionRepository;
        this.applicationRepository = applicationRepository;
        this.readCardService = readCardService;
        this.notifier = notifier;
        this.transactionAuthenticator = transactionAuthenticator;
        this.ledService = ledService;
        this.soundService = soundService;
        this.terminalRepository = terminalRepository;
    }

    @Override
    public ReadCardResponse validateReadCardResponse(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse) {
        if (readCardResponse == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (readCardResponse.getResult() == Result.WAVE_AGAIN) {
            readCardResponse = this.handleWaveAgain(readCardRequest);
        }
        if (readCardResponse.getResult() == Result.PICC_FALLBACK_TO_ICC) {
            readCardResponse = this.handlePiccFallback(readCardRequest);
        }
        this.checkForExceptionCases(readCardResponse);
        this.identifyCardScheme(readCardResponse);
        return readCardResponse;
    }

    @Override
    public ReadCardResponse validateReadCardResponseForICC(ReadCardRequest readCardRequest, ReadCardResponse readCardResponse) {
        if (readCardResponse == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (readCardResponse.getResult() == Result.TIMEOUT) {
            throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
        }
        if (readCardResponse.getResult() == Result.APPLICATION_BLOCKED) {
            throw new PosException(PosError.DEVICE_ERROR_APPLICATION_BLOCKED);
        }
        if (readCardResponse.getResult() == Result.CARD_BLOCKED) {
            throw new PosException(PosError.DEVICE_ERROR_CARD_BLOCKED);
        }
        if (readCardResponse.getResult() == Result.USER_CANCELLED) {
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        }
        if (readCardResponse.getResult() == Result.PROCESSING_ERROR) {
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
        if (readCardResponse.getResult() == Result.ICC_FALLBACK_TO_MAGNETIC) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (readCardResponse.getResult() == Result.ICC_CARD_DETECT_ERROR) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (readCardResponse.getResult() == Result.CALLBACK_AMOUNT) {
            // TODO implement amount callback notify UI and HW SDK
        }
        if (readCardResponse.getResult() == Result.FAILURE || readCardResponse.getCardDetails() == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (StringUtils.isEmpty(readCardResponse.getCardDetails().getPrimaryAccountNumber()) ||
                StringUtils.isEmpty(readCardResponse.getCardDetails().getTrackTwoData())) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        CardIdentifier cardIdentifier = new CardIdentifier(transactionRepository, applicationRepository);
        IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(readCardResponse);
        readCardResponse.getCardDetails().setCardScheme(identifyCardResponse.getCardDetails().getCardScheme());
        return readCardResponse;
    }

    private ReadCardResponse handlePiccFallback(ReadCardRequest readCardRequest) {
        this.readCardService.cleanUp();
        this.notifier.notify(Notifier.EventType.SWITCH_PICC_TO_ICC,
                Notifier.EventType.SWITCH_PICC_TO_ICC.getDescription());
        ReadCardRequest piccFallbackReadCardRequest = this.preparePICCFallbackReadCardRequest(readCardRequest);
        ReadCardResponse readCardResponse = this.readCardService.readCardDetails(piccFallbackReadCardRequest);
        return new IccCardProcessor(this.readCardService, transactionRepository, applicationRepository,
                transactionAuthenticator, notifier, this.terminalRepository).validateReadCardResponse(readCardRequest, readCardResponse);
    }

    private ReadCardRequest preparePICCFallbackReadCardRequest(ReadCardRequest readCardRequest) {
        List<CardType> cardTypeList = new ArrayList<>();
        cardTypeList.add(CardType.ICC);
        ReadCardRequest fallbackRequest = new ReadCardRequest(cardTypeList, readCardRequest.getTransactionType(),
                readCardRequest.getTransactionType9C(), readCardRequest.getEmvParametersRequest());
        fallbackRequest.setCashBackAmount(readCardRequest.getCashBackAmount());
        fallbackRequest.setAmount(readCardRequest.getAmount());
        fallbackRequest.setCardDetailsBeforeEmvNext(readCardRequest.getCardDetailsBeforeEmvNext());
        fallbackRequest.setCardReadTimeOut(readCardRequest.getCardReadTimeOut());
        fallbackRequest.setFallbackFromIccToMag(readCardRequest.getFallbackFromIccToMag());
        fallbackRequest.setForceOnlineForICC(readCardRequest.getForceOnlineForICC());
        fallbackRequest.setForceOnlineForPICC(readCardRequest.getForceOnlineForPICC());
        fallbackRequest.setIccDataList(readCardRequest.getIccDataList());
        fallbackRequest.setMaxmPinLength(readCardRequest.getMaxmPinLength());
        fallbackRequest.setMinmPinLength(readCardRequest.getMinmPinLength());
        fallbackRequest.setMultipleAidSelectionListener(readCardRequest.getMultipleAidSelectionListener());
        fallbackRequest.setPackageName(readCardRequest.getPackageName());
        fallbackRequest.setPinBlockFormat(readCardRequest.getPinBlockFormat());
        fallbackRequest.setPinpadRequired(readCardRequest.isPinpadRequired());
        fallbackRequest.setPinpadTimeout(readCardRequest.getPinpadTimeout());
        fallbackRequest.setStan(readCardRequest.getStan());
        fallbackRequest.setPrimaryAccountNumber(readCardRequest.getPrimaryAccountNumber());
        fallbackRequest.setTransactionDate(DateUtils.yyMMddDate());
        fallbackRequest.setTransactionTime(DateUtils.HHmmssTime());
        fallbackRequest.setCurrencyName(readCardRequest.getCurrencyName());
        fallbackRequest.setPinPadFixedLayout(readCardRequest.getPinPadFixedLayout());
        return fallbackRequest;
    }

    private void checkForExceptionCases(ReadCardResponse readCardResponse) {
        if (readCardResponse.getResult() == Result.APPLICATION_BLOCKED) {
            throw new PosException(PosError.DEVICE_ERROR_APPLICATION_BLOCKED);
        }
        if (readCardResponse.getResult() == Result.USER_CANCELLED) {
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        }
        if (readCardResponse.getResult() == Result.TIMEOUT) {
            throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
        }
        if (readCardResponse.getResult() == Result.PINPAD_ERROR) {
            throw new PosException(PosError.DEVICE_ERROR_PINPAD_ERROR);
        }
        if (readCardResponse.getResult() == Result.PROCESSING_ERROR) {
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
        if (readCardResponse.getResult() == Result.SEE_PHONE) {
            throw new PosException(PosError.DEVICE_ERROR_SEE_PHONE);
        }
        if (readCardResponse.getResult() == Result.FAILURE || readCardResponse.getCardDetails() == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (StringUtils.isEmpty(readCardResponse.getCardDetails().getPrimaryAccountNumber()) ||
                StringUtils.isEmpty(readCardResponse.getCardDetails().getTrackTwoData())) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        PICCWave.getInstance().onPICCWaveSuccess();
    }

    private ReadCardResponse handleWaveAgain(ReadCardRequest readCardRequest) {
        if (PICCWave.getInstance().isPICCWaveAgainAllowed()) {
            PICCWave.getInstance().onPICCWaveFailure();
            this.readCardService.cleanUp();
            this.ledService.doTurnLedWith(new LedRequest(LedLight.YELLOW, LedAction.ON));
            this.soundService.playSound(Sound.FAILURE);
            this.notifier.notify(
                    Notifier.EventType.WAVE_AGAIN,
                    Notifier.EventType.WAVE_AGAIN.getDescription()
            );
            ReadCardResponse readCardResponse = this.readCardService.readCardDetails(
                    this.prepareWaveAgainReadCardRequest(readCardRequest));
            if (readCardResponse.getResult() == Result.WAVE_AGAIN)
                return this.handleWaveAgain(readCardRequest);
            this.checkForExceptionCases(readCardResponse);
            return readCardResponse;
        } else {
            PICCWave.getInstance().onPICCWaveFailureLimitReached();
            this.ledService.doTurnLedWith(new LedRequest(LedLight.RED, LedAction.ON));
            this.soundService.playSound(Sound.FAILURE);
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    private ReadCardRequest prepareWaveAgainReadCardRequest(ReadCardRequest readCardRequest) {
        List<CardType> cardTypeList = new ArrayList<>();
        cardTypeList.add(CardType.PICC);
        ReadCardRequest waveAgainReadCardRequest = new ReadCardRequest(cardTypeList, readCardRequest.getTransactionType(),
                readCardRequest.getTransactionType9C(), readCardRequest.getEmvParametersRequest());
        waveAgainReadCardRequest.setCashBackAmount(readCardRequest.getCashBackAmount());
        waveAgainReadCardRequest.setAmount(readCardRequest.getAmount());
        waveAgainReadCardRequest.setCardDetailsBeforeEmvNext(readCardRequest.getCardDetailsBeforeEmvNext());
        waveAgainReadCardRequest.setCardReadTimeOut(readCardRequest.getCardReadTimeOut());
        waveAgainReadCardRequest.setFallbackFromIccToMag(readCardRequest.getFallbackFromIccToMag());
        waveAgainReadCardRequest.setForceOnlineForICC(readCardRequest.getForceOnlineForICC());
        waveAgainReadCardRequest.setForceOnlineForPICC(readCardRequest.getForceOnlineForPICC());
        waveAgainReadCardRequest.setIccDataList(readCardRequest.getIccDataList());
        waveAgainReadCardRequest.setMaxmPinLength(readCardRequest.getMaxmPinLength());
        waveAgainReadCardRequest.setMinmPinLength(readCardRequest.getMinmPinLength());
        waveAgainReadCardRequest.setMultipleAidSelectionListener(readCardRequest.getMultipleAidSelectionListener());
        waveAgainReadCardRequest.setPackageName(readCardRequest.getPackageName());
        waveAgainReadCardRequest.setPinBlockFormat(readCardRequest.getPinBlockFormat());
        waveAgainReadCardRequest.setPinpadRequired(readCardRequest.isPinpadRequired());
        waveAgainReadCardRequest.setPinpadTimeout(readCardRequest.getPinpadTimeout());
        waveAgainReadCardRequest.setStan(readCardRequest.getStan());
        waveAgainReadCardRequest.setPrimaryAccountNumber(readCardRequest.getPrimaryAccountNumber());
        waveAgainReadCardRequest.setTransactionDate(DateUtils.yyMMddDate());
        waveAgainReadCardRequest.setTransactionTime(DateUtils.HHmmssTime());
        waveAgainReadCardRequest.setCurrencyName(readCardRequest.getCurrencyName());
        waveAgainReadCardRequest.setPinPadFixedLayout(readCardRequest.getPinPadFixedLayout());
        return waveAgainReadCardRequest;
    }

    private void identifyCardScheme(ReadCardResponse readCardResponse) {
        global.citytech.finpos.processor.nibl.transaction.card.CardIdentifier cardIdentifier = new CardIdentifier(transactionRepository, applicationRepository);
        IdentifyCardResponse identifyCardResponse = cardIdentifier.identify(readCardResponse);
        readCardResponse.getCardDetails().setCardScheme(identifyCardResponse.getCardDetails().getCardScheme());
    }

    @Override
    public ReadCardResponse processCard(String stan, PurchaseRequest purchaseRequest) {
        ReadCardRequest readCardRequest = this.prepareReadCardRequestForPICC(stan, purchaseRequest);
        ReadCardResponse readCardResponse = readCardService.processEMV(readCardRequest);
        if (readCardResponse.getResult() != Result.FAILURE) {
            if (readCardResponse.getResult() == Result.PROCESSING_ERROR) {
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
            } else if (readCardResponse.getResult() == Result.PINPAD_ERROR) {
                throw new PosException(PosError.DEVICE_ERROR_PINPAD_ERROR);
            }
            readCardResponse.getCardDetails().setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
            return readCardResponse;
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    private ReadCardRequest prepareReadCardRequestForPICC(String stan, PurchaseRequest purchaseRequest) {
        ReadCardRequest readCardRequest = new ReadCardRequest(
                TransactionUtils.getAllowedCardEntryModes(purchaseRequest.getTransactionType()),
                purchaseRequest.getTransactionType(),
                TransactionUtils.retrieveTransactionType9C(purchaseRequest.getTransactionType()),
                this.applicationRepository.retrieveEmvParameterRequest()
        );
        readCardRequest.setStan(stan);
        readCardRequest.setAmount(purchaseRequest.getAmount());
        readCardRequest.setCashBackAmount(purchaseRequest.getAdditionalAmount());
        readCardRequest.setPinpadRequired(true);
        readCardRequest.setPrimaryAccountNumber(purchaseRequest.getCardDetails().getPrimaryAccountNumber());
        readCardRequest.setCardDetailsBeforeEmvNext(purchaseRequest.getCardDetails());
        readCardRequest.setTransactionDate(this.retrieveTransactionDate());
        readCardRequest.setTransactionTime(this.retrieveTransactionTime());
        readCardRequest.setCurrencyName(
                this.applicationRepository.retrieveFromAdditionalDataEmvParameters(
                        KEY_TRANSACTION_CURRENCY_NAME
                )
        );
        readCardRequest.setPinPadFixedLayout(!this.terminalRepository.shouldShufflePinPad());
        return readCardRequest;
    }

    private String retrieveTransactionTime() {
        return DateUtils.HHmmssTime();
    }

    private String retrieveTransactionDate() {
        return DateUtils.yyMMddDate();
    }

    @Override
    public ReadCardResponse processOnlineResult(ReadCardResponse readCardResponse, IsoMessageResponse isoMessageResponse) {
        if (this.isArqcTransaction(readCardResponse)) {
            int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, 39);
            ReadCardResponse afterSetOnlineResultResponse = readCardService.setOnlineResult(
                    actionCode,
                    mapTransactionHostStatus(S2MActionCode.getByActionCode(actionCode)
                            .getTransactionStatusFromHost()),
                    this.prepareField55DataForOnlineResult(actionCode, IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, 55)),
                    false);
            readCardResponse.getCardDetails().setIccDataBlock(afterSetOnlineResultResponse
                    .getCardDetails().getIccDataBlock());
            readCardResponse.getCardDetails().setTagCollection(afterSetOnlineResultResponse
                    .getCardDetails().getTagCollection());
            readCardResponse.getCardDetails().setTransactionState(afterSetOnlineResultResponse
                    .getCardDetails().getTransactionState());
        }
        return readCardResponse;
    }

    @Override
    public ReadCardResponse processUnableToGoOnlineResult(ReadCardResponse readCardResponse) {
        if (this.isArqcTransaction(readCardResponse)) {
            ReadCardResponse afterSetOnlineResultResponse = readCardService.setOnlineResult(
                    -99, ReadCardService.OnlineResult.UNABLE_TO_GO_ONLINE,
                    "", false);
            readCardResponse.getCardDetails().setIccDataBlock(afterSetOnlineResultResponse
                    .getCardDetails().getIccDataBlock());
            readCardResponse.getCardDetails().setTagCollection(afterSetOnlineResultResponse
                    .getCardDetails().getTagCollection());
            readCardResponse.getCardDetails().setTransactionState(afterSetOnlineResultResponse
                    .getCardDetails().getTransactionState());
        }
        return readCardResponse;
    }

    private String prepareField55DataForOnlineResult(int actionCode, String field55FromHost) {
        StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isEmpty(field55FromHost) || !field55FromHost.contains("8A02")) {
            stringBuilder.append("8A02");
            stringBuilder.append(StringUtils.toHexaDecimal(StringUtils.ofRequiredLength(String.valueOf(actionCode), 2)));
        }
        if (!StringUtils.isEmpty(field55FromHost))
            stringBuilder.append(field55FromHost);
        return stringBuilder.toString();
    }

    private ReadCardService.OnlineResult mapTransactionHostStatus(TransactionStatusFromHost transactionStatusFromHost) {
        switch (transactionStatusFromHost) {
            case ONLINE_APPROVED:
                return ReadCardService.OnlineResult.ONLINE_APPROVED;
            case ONLINE_DECLINED:
                return ReadCardService.OnlineResult.ONLINE_DECLINED;
            default:
                return ReadCardService.OnlineResult.UNABLE_TO_GO_ONLINE;
        }
    }

    private boolean isArqcTransaction(ReadCardResponse readCardResponse) {
        try {
            return readCardResponse
                    .getCardDetails()
                    .getTagCollection()
                    .get(IccData.CID.getTag())
                    .getData()
                    .equals("80");
        } catch (Exception e) {
            return false;
        }
    }
}
