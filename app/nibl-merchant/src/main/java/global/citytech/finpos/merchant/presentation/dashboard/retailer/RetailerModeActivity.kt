package global.citytech.finpos.merchant.presentation.dashboard.retailer

import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityRetailerDashboardBinding
import global.citytech.finpos.merchant.presentation.dashboard.base.BaseDashboardActivity
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuAdapter
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuItem
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuItemDecoration
import global.citytech.finpos.merchant.presentation.dashboard.menu.MenuUtils
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.switches.posmode.PosMode
import kotlinx.android.synthetic.main.activity_retailer_dashboard.*
import kotlinx.android.synthetic.main.activity_retailer_dashboard.rv_menu

/**
 * Created by Rishav Chudal on 5/11/21.
 */
class RetailerModeActivity :
    BaseDashboardActivity<ActivityRetailerDashboardBinding, RetailerModeViewModel>() {

    private val logger: Logger = Logger.getLogger(RetailerModeActivity::class.java.simpleName)

    override fun getBindingVariable(): Int = BR.retailerModeViewModel

    override fun getLayout(): Int = R.layout.activity_retailer_dashboard

    override fun getViewModel(): RetailerModeViewModel =
        ViewModelProviders.of(this)[RetailerModeViewModel::class.java]

    override fun getPosMode(): PosMode = PosMode.RETAILER

    override fun initViews() {
        this.initAndHandleButtonClicks()
        this.initAndHandlerPurchaseButton()
    }

    override fun observeChildActivityObservers() {
        observeShowPurchaseTransactionLiveData()
    }

    override fun updateMenuRecyclerView(menus: List<MenuItem>) {
        rv_menu.layoutManager = GridLayoutManager(this,2)
        rv_menu.adapter = MenuAdapter(menus,this)
        rv_menu.addItemDecoration(MenuItemDecoration())
    }

    override fun updateReadyAndTerminalIdInTextViews(configurationItem: ConfigurationItem?) {
        this.tv_ready.text = getString(R.string.title_ready)
        tv_terminal_id.text = getString(R.string.title_terminal_id)
            .plus(" : ")
            .plus(configurationItem!!.merchants!![0].terminalId)
    }

    override fun updateAppVersionTextView(appVersion: String) {
        tv_application_version.text = appVersion
    }

    override fun updateTextViewReadyWithTerminalNotConfiguredMessage(message: String) {
        this.tv_ready.text = message
    }

    override fun updateProgressBarVisibility(visibility: Int) {
        this.progressbar.visibility = visibility
    }

    private fun initAndHandleButtonClicks() {
        iv_back.setOnClickListener {
            returnToIdlePage()
        }

        iv_app_notification.setOnClickListener {
            startAppNotificationsActivity()
        }
    }

    private fun initAndHandlerPurchaseButton() {
        btn_purchase.visibility = View.GONE
        btn_purchase.setOnClickListener {
            clickedMenuItem = MenuUtils.menuItemPurchase()
            getViewModel().checkForAutoReversal()
        }
    }

    private fun observeShowPurchaseTransactionLiveData() {
        this.getViewModel().showPurchaseTransactionLiveData.observe(
            this,
            Observer {
                if (it) {
                    btn_purchase.visibility = View.VISIBLE
                } else {
                    btn_purchase.visibility = View.GONE
                }
            }
        )
    }

    override fun makeNotificationIconVisible(noNotifications: Boolean) {
        if (noNotifications)
            iv_app_notification.visibility = View.GONE
        else
            iv_app_notification.visibility = View.VISIBLE
    }
}