package global.citytech.finpos.merchant.presentation.alertdialogs

/**
 * Created by Unique Shakya on 5/5/2021.
 */
data class SliderAttributes(
    val title: String,
    val minLimit: Int,
    val maxLimit: Int,
    val stepSize: Int,
    val configuredValue: Int,
    val textInputHint: String,
    val invalidInputMessage: String,
    val emptyInputMessage: String,
    val defaultConfigurationMessage: String
)