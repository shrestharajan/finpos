package global.citytech.finpos.merchant.presentation.dashboard.temp

import com.google.gson.annotations.SerializedName


data class LogResponse(
    val code: String,
    val message: String,
    val data: Data,
)

data class Data(
    val url: String,
    val requestNumber: String,
    val properties: Properties,
)

data class Properties(
    @SerializedName("x-amz-date")
    val xAmzDate: String,
    @SerializedName("success_action_status")
    val successActionStatus: String,
    @SerializedName("x-amz-signature")
    val xAmzSignature: String,
    @SerializedName("response-content-type")
    val responseContentType: String,
    val key: String,
    @SerializedName("x-amz-algorithm")
    val xAmzAlgorithm: String,
    @SerializedName("Content-Type")
    val contentType: String,
    @SerializedName("x-amz-credential")
    val xAmzCredential: String,
    val policy: String,
)
