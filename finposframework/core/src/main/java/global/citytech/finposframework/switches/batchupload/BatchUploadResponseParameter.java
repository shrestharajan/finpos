package global.citytech.finposframework.switches.batchupload;

import global.citytech.finposframework.switches.ResponseParameter;

/**
 * Created by Unique Shakya on 5/11/2021.
 */
public interface BatchUploadResponseParameter extends ResponseParameter {
}
