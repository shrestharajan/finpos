package global.citytech.nepsadmin.data.response

import android.net.Uri
import global.citytech.nepsadmin.presentation.main.MainViewModel

data class Merchant(
    var address: String? = null,
    var name: String? = null,
    var phoneNumber: String? = null,
    var switchId: String? = null
){
    companion object{
        val TABLE_NAME = "merchant"
        val URI = Uri.parse("content://${MainViewModel.AUTHORITY}/$TABLE_NAME")
        const val COLUMN_ID = "id"
        const val COLUMN_NAME = "merchant_name"
        const val COLUMN_ADDRESS = "merchant_address"
        const val COLUMN_PHONE_NUMBER = "merchant_phone"
        const val COLUMN_SWITCH_ID = "merchant_switch_id"
        const val COLUMN_TERMINAL_ID = "merchant_terminal_id"
    }
}