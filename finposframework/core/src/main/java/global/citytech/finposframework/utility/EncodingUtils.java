package global.citytech.finposframework.utility;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

/** User: Surajchhetry Date: 2/18/20 Time: 8:35 PM */
public final class EncodingUtils {

  public static String asciiRepresentation(String text) {
    StringBuilder hexData = new StringBuilder();
    for (int index = 0; index < text.length(); index++) {
      hexData.append(Long.toHexString((int) text.charAt(index)));
    }
    return hexData.toString().toUpperCase();
  }
  /*
      public static String toHexEncoded(long number) {
          String strNumber = String.valueOf(number);
          StringBuilder hexData = new StringBuilder();
          for (int index = 0; index < strNumber.length(); index++) {
              hexData.append(Long.toHexString((int) strNumber.charAt(index)));
          }
          return hexData.toString().toUpperCase();
      }
  */
  public static String hexEncodedToBinaryString(String hexEncoded) {
    return new String(hexEncodedToBytes(hexEncoded));
  }

  public static String fromHexValueToBinary(String hex) {
    return new BigInteger(hex, 16).toString(2);
  }

  /**
   * Converts a hex string into a byte array
   *
   * @param s source string (with Hex representation)
   * @return byte array
   */
  public static byte[] hex2Bytes(String s) {
    if (s == null) return new byte[1];
    if (s.length() % 2 == 0) {
      return hex2Bytes(s.getBytes(StandardCharsets.UTF_8), 0, s.length() >> 1);
    } else {
      return hex2Bytes("0" + s);
    }
  }

  /**
   * @param b source byte array
   * @param offset starting offset
   * @param len number of bytes in destination (processes len*2)
   * @return byte[len]
   */
  public static byte[] hex2Bytes(byte[] b, int offset, int len) {
    byte[] d = new byte[len];
    for (int i = 0; i < len * 2; i++) {
      int shift = i % 2 == 1 ? 0 : 4;
      if(Character.isLetterOrDigit((char) b[offset + i]))
        d[i >> 1] |= Character.digit((char) b[offset + i], 16) << shift;
      else
        d[i >> 1] |= (char) b[offset + i] << shift;
    }
    return d;
  }

  private static int toByte(char c) {
    if (c >= '0' && c <= '9') return (c - '0');
    if (c >= 'A' && c <= 'F') return (c - 'A' + 10);
    if (c >= 'a' && c <= 'f') return (c - 'a' + 10);
    throw new RuntimeException("Invalid hex char '" + c + "'");
  }

  public static byte[] hexEncodedToBytes(String hexString) {
    int length = hexString.length();
    byte[] buffer = new byte[length / 2];
    for (int i = 0; i < length; i += 2) {
      buffer[i / 2] = (byte) ((toByte(hexString.charAt(i)) << 4) | toByte(hexString.charAt(i + 1)));
    }
    return buffer;
  }

  public static byte[] asciiRepresentationAsByte(String str){
    return str.getBytes(StandardCharsets.US_ASCII);
  }

  public static String hexToASCII(String hexValue)
  {
    StringBuilder output = new StringBuilder("");
    for (int i = 0; i < hexValue.length(); i += 2)
    {
      String str = hexValue.substring(i, i + 2);
      output.append((char) Integer.parseInt(str, 16));
    }
    return output.toString();
  }


}
