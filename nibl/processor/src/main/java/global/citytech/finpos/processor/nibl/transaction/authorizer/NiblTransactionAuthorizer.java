package global.citytech.finpos.processor.nibl.transaction.authorizer;

import global.citytech.finpos.processor.nibl.transaction.TransactionSetTableVerifier;
import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.TransactionAuthorizer;
import global.citytech.finposframework.usecases.transaction.data.TransactionAuthorizationResponse;
import global.citytech.finposframework.utility.CardUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Unique Shakya on 8/27/2020.
 */
public class NiblTransactionAuthorizer implements TransactionAuthorizer {

    private TransactionAuthenticator transactionAuthenticator;
    private ApplicationRepository applicationRepository;

    public NiblTransactionAuthorizer(
            TransactionAuthenticator transactionAuthenticator,
            ApplicationRepository applicationRepository
    ) {
        this.transactionAuthenticator = transactionAuthenticator;
        this.applicationRepository = applicationRepository;
    }

    @Override
    public TransactionAuthorizationResponse authorizeUser(TransactionType transactionType, String cardScheme) {
        boolean authorizationRequired = false;
        if (!StringUtils.isEmpty(cardScheme)) {
            TransactionSetTableVerifier verifier = new TransactionSetTableVerifier(
                    applicationRepository,
                    CardUtils.getShortAidLabel(cardScheme),
                    TransactionUtils.mapPurchaseTypeWithTransactionType(transactionType)
            );
            authorizationRequired =  verifier.isSupervisorPasswordRequired();
        }
        if(authorizationRequired){
            return transactionAuthenticator.authenticate();
        }else{
            return new TransactionAuthorizationResponse(true);
        }
    }
}
