package global.citytech.finpos.merchant.framework.datasource.core.printparameter

import com.google.gson.JsonParser
import global.citytech.common.extensions.toJsonString
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.presentation.utils.StringUtils.Companion.createStringList
import global.citytech.finpos.merchant.presentation.utils.StringUtils.Companion.getDividerFor
import global.citytech.finpos.merchant.presentation.utils.StringUtils.Companion.getLabelFromLabelValue
import global.citytech.finpos.merchant.presentation.utils.StringUtils.Companion.getValueFromLabelValue
import global.citytech.finpos.merchant.presentation.utils.StringUtils.Companion.getValueOrEmpty
import global.citytech.finposframework.hardware.io.printer.PrintMessage
import global.citytech.finposframework.hardware.io.printer.Printable
import global.citytech.finposframework.hardware.io.printer.PrinterRequest
import global.citytech.finposframework.hardware.io.printer.Style
import global.citytech.finposframework.utility.DateUtils
import global.citytech.finposframework.utility.StringUtils

/**
 * Created by Rishav Chudal on 11/6/20.
 */
class TmsLogsDecorator(private val tmsLogs: TmsLogs) {

    fun preparePrintRequest(): PrinterRequest {
        val printMessages = ArrayList<Printable>()
        addTitleInPrintRequest(printMessages)
        addTerminalInfoInPrintRequest(printMessages)
        addSwitchParametersInPrintRequest(printMessages)
        addAidParametersInPrintRequest(printMessages)
        addEmvKeyParametersInPrintRequest(printMessages)
        addCardSchemeParametersInPrintRequest(printMessages)
        return PrinterRequest(printMessages)
    }

    private fun addTitleInPrintRequest(printMessages: MutableList<Printable>) {
        if (tmsLogs.title != null) {
            addSingleColumnString(printMessages, tmsLogs.title!!, TmsLogsStyle.HEADER.style)
        }
    }

    private fun addTerminalInfoInPrintRequest(printMessages: MutableList<Printable>) {
        addSingleColumnString(
            printMessages,
            "    ",
            TmsLogsStyle.DIVIDER.style
        )

        addDoubleColumnString(
            printMessages,
            createStringList(
                DateUtils.yyMMddDateWithDivider(),
                DateUtils.HHmmssTimeWithDivider()
            ),
            TmsLogsStyle.PARAMETERS.style
        )

        addSingleColumnString(
            printMessages,
            TmsParametersLabels.TERMINAL_ID
                .plus(TmsParametersLabels.SEMICOLON)
                .plus(tmsLogs.terminalInfo!!.terminalID),
            TmsLogsStyle.PARAMETERS.style
        )

        addSingleColumnString(
            printMessages,
            TmsParametersLabels.MERCHANT_ID
                .plus(TmsParametersLabels.SEMICOLON)
                .plus(tmsLogs.terminalInfo!!.merchantID),
            TmsLogsStyle.PARAMETERS.style
        )

        val packageInfo = NiblMerchant.INSTANCE.packageManager.getPackageInfo(
            NiblMerchant.INSTANCE.packageName,
            0
        )
        val version = packageInfo.versionName
        addSingleColumnString(
            printMessages,
            TmsParametersLabels.APP_VERSION
                .plus(TmsParametersLabels.SEMICOLON)
                .plus(getValueOrEmpty(version)),
            TmsLogsStyle.PARAMETERS.style
        )
    }

    private fun addSwitchParametersInPrintRequest(printMessages: MutableList<Printable>) {
        val hosts = tmsLogs.hosts
        if (hosts != null && hosts.isNotEmpty()) {
            addSingleColumnString(
                printMessages,
                "    ",
                TmsLogsStyle.DIVIDER.style
            )

            addSingleColumnString(
                printMessages,
                TmsParametersLabels.SWITCH_PARAMETERS,
                TmsLogsStyle.SUB_HEADER.style
            )
            addSingleColumnString(
                printMessages,
                getDividerFor(TmsParametersLabels.SWITCH_PARAMETERS),
                TmsLogsStyle.DIVIDER.style
            )
            for (host in hosts) {
                addSingleColumnString(printMessages, "    ", TmsLogsStyle.DIVIDER.style)
                addMultiColumnString(
                    printMessages,
                    createStringList(
                        TmsParametersLabels.IP,
                        TmsParametersLabels.SEMICOLON.plus(getValueOrEmpty(host.ip!!))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        TmsParametersLabels.PORT,
                        TmsParametersLabels.SEMICOLON.plus(getValueOrEmpty(host.port!!))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        TmsParametersLabels.NII,
                        TmsParametersLabels.SEMICOLON.plus(getValueOrEmpty(host.nii!!))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        TmsParametersLabels.ORDER,
                        TmsParametersLabels.SEMICOLON.plus(host.order)
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        TmsParametersLabels.TIMEOUT,
                        TmsParametersLabels.SEMICOLON.plus(getValueOrEmpty(host.connectionTimeout!!))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        TmsParametersLabels.RETRY_LIMIT,
                        TmsParametersLabels.SEMICOLON.plus(getValueOrEmpty(host.retryLimit!!))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )
            }
        }
    }

    private fun addAidParametersInPrintRequest(printMessages: MutableList<Printable>) {
        val aidParams = tmsLogs.aidParams
        if (aidParams != null && aidParams.isNotEmpty()) {
            addSingleColumnString(
                printMessages,
                "    ",
                TmsLogsStyle.DIVIDER.style
            )

            addSingleColumnString(
                printMessages,
                TmsParametersLabels.AID_DATA,
                TmsLogsStyle.SUB_HEADER.style
            )

            addSingleColumnString(
                printMessages,
                getDividerFor(TmsParametersLabels.AID_DATA),
                TmsLogsStyle.DIVIDER.style
            )

            for (aidParam in aidParams) {
                addSingleColumnString(printMessages, "    ", TmsLogsStyle.DIVIDER.style)
                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(aidParam.aid),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(aidParam.aid))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(aidParam.label),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(aidParam.label))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(aidParam.terminalAidVersion),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(aidParam.terminalAidVersion))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(aidParam.defaultTDOL),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(aidParam.defaultTDOL))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(aidParam.defaultDDOL),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(aidParam.defaultDDOL))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(aidParam.denialActionCode),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(aidParam.denialActionCode))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(aidParam.onlineActionCode),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(aidParam.onlineActionCode))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(aidParam.defaultActionCode),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(aidParam.defaultActionCode))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(aidParam.terminalFloorLimit),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(aidParam.terminalFloorLimit))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(aidParam.contactlessFloorLimit),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(aidParam.contactlessFloorLimit))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(aidParam.contactlessTransactionLimit),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(aidParam.contactlessTransactionLimit))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(aidParam.cvmLimit),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(aidParam.cvmLimit))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                if (!aidParam.additionalData.isNullOrEmptyOrBlank()) {
                    val jsonParser = JsonParser()
                    try {
                        val additionalDataObject =
                            jsonParser.parse(aidParam.additionalData).asJsonObject
                        for (keyIndex in additionalDataObject.keySet()) {
                            val labelValueDataObject =
                                additionalDataObject.getAsJsonObject(keyIndex)
                            val labelValueDataString = labelValueDataObject.toJsonString()
                            val printLabel = getLabelFromLabelValue(labelValueDataString)
                            val printValue = getValueFromLabelValue(labelValueDataString)
                            addMultiColumnString(
                                printMessages,
                                createStringList(
                                    printLabel,
                                    TmsParametersLabels.SEMICOLON.plus(
                                        printValue
                                    )
                                ),
                                TmsLogsStyle.PARAMETERS.columnWidths,
                                TmsLogsStyle.PARAMETERS.style
                            )
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    private fun addEmvKeyParametersInPrintRequest(printMessages: MutableList<Printable>) {
        val emvKeys = tmsLogs.emvKeys
        if (emvKeys != null && emvKeys.isNotEmpty()) {
            addSingleColumnString(
                printMessages,
                "    ",
                TmsLogsStyle.DIVIDER.style
            )

            addSingleColumnString(
                printMessages,
                TmsParametersLabels.EMV_KEYS,
                TmsLogsStyle.SUB_HEADER.style
            )

            addSingleColumnString(
                printMessages,
                getDividerFor(TmsParametersLabels.EMV_KEYS),
                TmsLogsStyle.DIVIDER.style
            )

            for (emvKey in emvKeys) {
                addSingleColumnString(
                    printMessages,
                    "    ",
                    TmsLogsStyle.DIVIDER.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(emvKey.rid),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(emvKey.rid))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(emvKey.index),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(emvKey.index))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(emvKey.length),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(emvKey.length))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(emvKey.exponent),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(emvKey.exponent))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(emvKey.modules),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(emvKey.modules))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(emvKey.hashId),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(emvKey.hashId))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(emvKey.keySignatureId),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(emvKey.keySignatureId))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(emvKey.checkSum),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(emvKey.checkSum))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )

                addMultiColumnString(
                    printMessages,
                    createStringList(
                        getLabelFromLabelValue(emvKey.expiryDate),
                        TmsParametersLabels.SEMICOLON.plus(getValueFromLabelValue(emvKey.expiryDate))
                    ),
                    TmsLogsStyle.PARAMETERS.columnWidths,
                    TmsLogsStyle.PARAMETERS.style
                )
            }
        }
    }

    private fun addCardSchemeParametersInPrintRequest(printMessages: MutableList<Printable>) {
        val cardSchemes = tmsLogs.cardSchemes
        if (cardSchemes != null && cardSchemes.isNotEmpty()) {
            addSingleColumnString(
                printMessages,
                "    ",
                TmsLogsStyle.DIVIDER.style
            )

            addSingleColumnString(
                printMessages,
                TmsParametersLabels.CARD_SCHEME,
                TmsLogsStyle.SUB_HEADER.style
            )

            addSingleColumnString(
                printMessages,
                getDividerFor(TmsParametersLabels.CARD_SCHEME),
                TmsLogsStyle.DIVIDER.style
            )

            for (i in cardSchemes.indices) {
                if (i != 0 && cardSchemes[i - 1].cardSchemeId!! == cardSchemes[i].cardSchemeId
                ) {
                    addMultiColumnString(
                        printMessages,
                        createStringList(
                            getValueOrEmpty(cardSchemes[i].attribute),
                            TmsParametersLabels.SEMICOLON.plus(getValueOrEmpty(cardSchemes[i].value))
                        ),
                        TmsLogsStyle.PARAMETERS.columnWidths,
                        TmsLogsStyle.PARAMETERS.style
                    )
                } else {
                    addSingleColumnString(
                        printMessages,
                        "    ",
                        TmsLogsStyle.DIVIDER.style
                    )

                    addMultiColumnString(
                        printMessages,
                        createStringList(
                            TmsParametersLabels.CARD_SCHEME_ID,
                            TmsParametersLabels.SEMICOLON.plus(getValueOrEmpty(cardSchemes[i].cardSchemeId!!))
                        ),
                        TmsLogsStyle.PARAMETERS.columnWidths,
                        TmsLogsStyle.PARAMETERS.style
                    )

                    addMultiColumnString(
                        printMessages,
                        createStringList(
                            getValueOrEmpty(cardSchemes[i].attribute),
                            TmsParametersLabels.SEMICOLON.plus(getValueOrEmpty(cardSchemes[i].value))
                        ),
                        TmsLogsStyle.PARAMETERS.columnWidths,
                        TmsLogsStyle.PARAMETERS.style
                    )
                }
            }
        }
    }

    private fun addSingleColumnString(
        printMessages: MutableList<Printable>,
        value: String,
        style: Style
    ) {
        if (!StringUtils.isEmpty(value)) {
            printMessages.add(PrintMessage.getInstance(value, style))
        }
    }

    private fun addDoubleColumnString(
        printMessages: MutableList<Printable>,
        stringList: MutableList<String>,
        style: Style
    ) {
        printMessages.add(
            PrintMessage.getDoubleColumnInstance(stringList[0], stringList[1], style)
        )
    }

    private fun addMultiColumnString(
        printMessages: MutableList<Printable>,
        values: List<String>,
        columnWidths: IntArray,
        style: Style
    ) {
        if (!values.isEmpty()) {
            printMessages.add(PrintMessage.getMultiColumnInstance(values, columnWidths, style))
        }
    }
}