package global.citytech.common

/**
 * Created by Rishav Chudal on 6/15/20.
 */
object Constants {

    /**
     * PAYMENT ITEM Constants
     */
    const val CARD = "Card"
    const val FONEPAY = "fonePay"
    const val NEPALPAY = "NepalPay"
    const val MOBILEPAY = "Mobile Pay"


    /**
     * INTENT Constants
     * */
    const val KEY_AMOUNT = "amount"
    const val KEY_PURCHASE_TYPE = "purchase_type"
    const val KEY_POS_ENTRY_SELECTION = "pos_entry_selection"

    /**
     * BroadCast Constants
     */
    const val INTENT_ACTION_POS_CALLBACK = "action.pos.callback.broadcast"
    const val INTENT_ACTION_SAF = "action.saf.broadcast"
    const val INTENT_ACTION_REPRINT_UI = "action.reprint.ui.broadcast"
    const val KEY_REPRINT_MESSAGE = "key_reprint_message"
    const val INTENT_ACTION_REPRINT_SERVICE = "action.reprint.service.broadcast"
    const val KEY_BROADCAST_MESSAGE = "broadcast_message"
    const val KEY_BROADCAST_EXTRA_MESSAGE = "broadcast_extra_message"
    const val KEY_BROADCAST_EXTRA_REPRINT = "broadcast_extra_reprint"
    const val SUCCESS = "SUCCESS"
    const val FIRST_TIME = "first_time"
    const val INTENT_ACTON_LED_SOUND = "action_led_sound"
    const val INTENT_ACTION_TRANSACTION_RESULT = "action_transaction_result"
    const val KEY_TRANSACTION_RESULT = "transaction_result"
    const val KEY_AUTH_CODE = "auth_Code"
    const val KEY_SLOT_TYPE = "slot_type"
    const val KEY_LED_REQUEST = "led_request"
    const val KEY_SOUND_REQUEST = "sound_request"
    const val INTENT_ACTION_STOP_IDLE_TIME = "action_stop_idle_time"
    const val INTENT_ACTION_NOTIFICATION = "action.intent.notification"
    const val EXTRA_NOTIFICATION_MESSAGE = "extra_notification_message"
    const val EXTRA_EVENT_TYPE = "extra_event_type"
    const val EXTRA_TRANSACTION_TYPE = "extra_transaction_type"
    const val EXTRA_CARD_CONFIRMATION_LISTENER = "extra_card_confirmation_listener"
    const val EXTRA_OTP_CONFIRMATION_LISTENER = "extra_otp_confirmation_listener"
    const val EXTRA_CASH_IN_AMOUNT_LISTENER = "extra_cash_in_amount_listener"
    const val EXTRA_CASH_IN_PROCESSING_LISTENER = "extra_cash_in_amount_listener"
    const val EXTRA_CARD_NUMBER = "extra_card_number"
    const val EXTRA_CARD_SCHEME = "extra_card_scheme"
    const val EXTRA_CARD_HOLDER_NAME = "extra_card_holder_name"
    const val EXTRA_CARD_EXPIRY = "extra_card_expiry"
    const val INTENT_ACTION_CONNECTION_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE"

    const val EXTRA_SETTLEMENT_CONFIRMATION_LISTENER = "extra_settlement_confirmation_listener"
    const val EXTRA_SETTLEMENT_CONFIRMATION_JSON_DATA = "extra_settlement_confirmation_json_data"

    const val TRANSACTION_REQUEST_CODE = 3229
    const val EXTRA_FROM_CHECKOUT = "extra_from_checkout"
    const val EXTRA_EMI_TRANSACTION = "extra_EMI_TRANSACTION"
    const val EXTRA_AMOUNT = "extra_amount"
    const val EXTRA_VAT_INFO = "extra_vat_info"
    const val EXTRA_ADDITIONAL_DATA = "extra_additional_data"
    const val EXTRA_REFERENCE_NUMBER = "extra_reference_number"
    const val EXTRA_OTHER_AMOUNT = "extra_other_amount"
    const val EXTRA_PAYMENT_RESULT = "extra_payment_result"
    const val EXTRA_AUTH_CODE = "extra_auth_code"
    const val EXTRA_CODE = "extra_code"
    const val EXTRA_MESSAGE = "extra_message"
    const val EXTRA_APPROVED = "extra_approved"
    const val EXTRA_RRN = "extra_rrn"
    const val EXTRA_STAN = "extra_stan"
    const val EXTRA_TRANSACTION_TIME = "extra_transaction_time"
    const val EXTRA_PAYMENT_MODE = "extra_payment_mode"
    const val EXTRA_PAYMENT_NETWORK = "extra_payment_network"

    const val EMI_ACTIVITY_PACKAGE_NAME = "global.citytech.finpos.emi"
    const val EMI_REQUEST_CODE = 7389
    const val EMI_EXTRA_LIMIT = "extra_limit"
    const val EMI_EXTRA_CURRENCY_CODE = "extra_currency_code"
    const val EMI_EXTRA_CONFIRM_AMOUNT = "extra_confirm_amount"
    const val EMI_EXTRA_EMI_INFO = "extra_emi_option"

    const val KEY_CD_CVM_LIMIT = "cdCvmLimit"
    const val KEY_NO_CD_CVM_LIMIT = "noCdCvmLimit"
    const val KEY_CVM_CAP_CVM_REQUIRED = "cvmCapCvmRequired"
    const val KEY_CVM_CAP_NO_CVM_REQUIRED = "cvmCapNoCvmRequired"
    const val KEY_TERMINAL_RISK_MGMT_DATA = "terminalRiskManagementData"
    const val KEY_CL_TAC_DENIAL = "clTacDenial"
    const val KEY_CL_TAC_ONLINE = "clTacOnline"
    const val KEY_CL_TAC_DEFAULT = "clTacDefault"
    const val KEY_ACQUIRER_ID = "acquirerId"
    const val KEY_KERNEL_CONFIG = "kernelConfig"
    const val KEY_CL_CARD_DATA_INPUT_CAP = "clCardDataInputCapability"
    const val KEY_CL_SECURITY_CAP = "clSecurityCapability"
    const val KEY_UDOL = "udol"
    const val KEY_MSD_APP_VERSION = "msdAppVersion"
    const val KEY_TTQ = "ttq"
    const val KEY_APPROVE_PRINT_ONLY = "approvePrintOnly"
    const val KEY_TRANSACTION_CURRENCY_NAME = "transactionCurrencyName"
    const val MAX_BATCH_NUMBER_FOR_SETTLEMENT = 99999999


    const val KEY_STAN = "stan"
    const val KEY_MESSAGE = "message"
    const val KEY_IS_APPROVED = "isApproved"

    /**
     * Font Constants
     */
    const val ARIAL_FONT_SIZE_XSMALL = 16
    const val ARIAL_FONT_SIZE_SMALL = 19
    const val ARIAL_FONT_SIZE_NORMAL = 22
    const val ARIAL_FONT_SIZE_LARGE = 30
    const val ARIAL_FONT_SIZE_XLARGE = 36
    const val MONOSPACE_FONT_SIZE_XSMALL = 18
    const val MONOSPACE_FONT_SIZE_SMALL = 20
    const val MONOSPACE_FONT_SIZE_NORMAL = 22
    const val MONOSPACE_FONT_SIZE_LARGE = 26
    const val MONOSPACE_FONT_SIZE_XLARGE = 30

    /*const val ARIAL_FONT_SIZE_XSMALL = 18
    const val ARIAL_FONT_SIZE_SMALL = 22
    const val ARIAL_FONT_SIZE_NORMAL = 25
    const val ARIAL_FONT_SIZE_LARGE = 34
    const val ARIAL_FONT_SIZE_XLARGE = 42


    const val MONOSPACE_FONT_SIZE_XSMALL = 18
    const val MONOSPACE_FONT_SIZE_SMALL = 20
    const val MONOSPACE_FONT_SIZE_NORMAL = 22
    const val MONOSPACE_FONT_SIZE_LARGE = 26
    const val MONOSPACE_FONT_SIZE_XLARGE = 30
    */

    /**
     * FinPos cryptography key pair
     * TODO remove later
     */
    const val FINPOS_PRIVATE_KEY =
        "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAOtqKeCzDGwTypzOyzHfxtg0ihXQnG4W3yH2iVjBytRm8nPAyu2djjGfIPp5yo1cIXxb+9X6sXUmSy4NXBGdhKnJ4YxmLAhXMkZ5/e8uNXAtbhAt6045z7E2AZA5b6lJpGNct7btztOnWer3ZfmJho6CleF9vLTU/B6Vuo088FSpAgMBAAECgYBgtqC1u1kjHY5jlWFNAA4EzLDd/XIGmEbbARmz6QmW8SQyAcZckBnWsPUvcdGQ+YSYPoHbir5UzoKcTMjCQ+A3xup3qN1sHm5300eugocKTgI1FztBK03yASC3Mwp/lyDRJpOtnjDFlXAWPlC255yCU/0d9sNcvJsGz9TaXjXWcQJBAP7fvYleanOvtzzM75++cOHTDSU35Q7Ywx8cI31dBBgOGhS2NQsbAcQpBRpFb1ljTdaEgXpTuBH5UwdyUL7Am5cCQQDsdGo7zJ5Ys/sfOeGHCJ4ccJNbADvO5Sf8BN/6FqbaCACRZtNL4QFU5MwEPCPgT+KJKKuzOVY3NDSdHkPXmZm/AkEAj2ZBFK3bhPVjSkEfvSgACP6e0cbuCHlq5vEFAr630/TzNZ4CoBn013Ig3dQdPxTBMaXh13qNAQffcDwgNALYRwJANUsLwoycx9tQ5znRdfDgSgKpg6Iq2LfXajrjDGfclVmmXx8w0nJVLYAf53hykZUPtuA+yrdKkkim7x6qJcbN2wJBAI1wM3Lr7pZxp6KarQFyd1kKMbRBEErZHL3VtJF82N4CCLKHfhv7WX61C094v+WSTfKa1zFqz5nILQIIl5pBbZM="
    const val FINPOS_PUBLIC_KEY =
        "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDraingswxsE8qczssx38bYNIoV0JxuFt8h9olYwcrUZvJzwMrtnY4xnyD6ecqNXCF8W/vV+rF1JksuDVwRnYSpyeGMZiwIVzJGef3vLjVwLW4QLetOOc+xNgGQOW+pSaRjXLe27c7Tp1nq92X5iYaOgpXhfby01PwelbqNPPBUqQIDAQAB"

    /**
     * Vendor cryptography key pair
     * TODO remove later
     */
    const val VENDOR_PRIVATE_KEY =
        "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAIZCaRRWa5K06oMcKzPS2O933sVnRaRcOriNlzg0yRNynaj31LrRxi2D0npfy4VEx3lToukl8vpbqfp2cCig5AIVQNXAXCk/SH5NfQPU+x9p9AOyMmz3HfCt6/7dpqkG7OaIO04mbu3Y9kgD8vWMZI7i5kBJu5ddge97eBvQOg0NAgMBAAECgYEAhTkK2gJzFg1tkxH1kKQNXXod1NYIFfLq1cyyHDTMvf6CkXLyZTrZIrrmtUWvRxubsDmcsytT8rfYcE7sNnSIKlTNcGKQ2brr70q7OHxaiKjPOg8q9Us0WV6uPE/TBp7vvu2bG3vR+QmR0rjU4Ly2m/QzKsqicnsjrAgHS0KZZIECQQDAVH65ovCygXmfhrIuTzqHqQrhYimuXdaZVLQA8ECF9+6MXDAnawW7o5DKH2E0Sv5HOe574omxVf4dXbgHQ4uhAkEAsrSR0rRerztR45qfjMPE9/3j5CRfvrsx02cXyeb218fgKokiSo95k7pthcfPj1ImkK2CKG6BX9MvGzESPTUp7QJAMM3AEm8MUN+V8ysEz8d+/KmifyqflQVuzR23R32vgc82ExvZhEcd/000EnX//nsBJyXGJF2JvxwpDV6Ysg8rwQJATlwRSSQgFJRdTwEhDvdU6i0g/YvZk+e/bGRbMQQenvWc3Onu6GTLObTYi33XDJBIKs56MUJrZlpS4Ih+5+nk7QJADyUHgcJumbLvVdzLrLvOVN706mINd+k+w2UWM6AnZAHiGBGiQd12gBQErG3rZAVJ4uemJe5sk4EPjMf63Ya5MA=="
    const val VENDOR_PUBLIC_KEY =
        "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCGQmkUVmuStOqDHCsz0tjvd97FZ0WkXDq4jZc4NMkTcp2o99S60cYtg9J6X8uFRMd5U6LpJfL6W6n6dnAooOQCFUDVwFwpP0h+TX0D1PsfafQDsjJs9x3wrev+3aapBuzmiDtOJm7t2PZIA/L1jGSO4uZASbuXXYHve3gb0DoNDQIDAQAB"
    const val DEFAULT_PAGE_SIZE = 15

}