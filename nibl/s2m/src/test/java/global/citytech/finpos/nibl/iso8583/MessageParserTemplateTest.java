package global.citytech.finpos.nibl.iso8583;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.iso8583.BitMap;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.DataElementConfig;
import global.citytech.finposframework.iso8583.EncodingType;
import global.citytech.finposframework.iso8583.LengthConfig;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.utility.EncodingUtils;


public class MessageParserTemplateTest {

    private Logger logger = Logger.getLogger(MessageParserTemplateTest.class.getName());

   // @Test
    public void hexDataParserTest() {
        BitMap bitMap = new BitMap();
        bitMap.set(3);
        bitMap.set(11);
        bitMap.set(24);
        bitMap.set(39);
        bitMap.set(41);
        bitMap.set(42);
        bitMap.set(62);
        NIBLSpecInfo specInfo = new NIBLSpecInfo();
        List<DataElement> elements = new ArrayList<>();

        String hexData = "920000-000002-0011-3030-3030303031353234-343035363335303030303035373633-0016459C138D5BBFED6C236E8F5D52751F92".replace("-", "");
        logger.log(hexData);
        String value = null;

        int bcdFactor = 2;
        int actualLength = 0;
        for (Integer index : bitMap.getFields()) {
            DataElementConfig deConfig = specInfo.getDEConfig().getByFieldNumber(index);
            if (deConfig.getLengthConfig().getLengthType() == LengthConfig.LengthType.FIXED) {
                actualLength = deConfig.getLengthConfig().getLength() * bcdFactor;
                value = hexData.substring(0, actualLength);
                if (deConfig.getDataConfig().getEncodingType() == EncodingType.ASCII)
                    value = EncodingUtils.hexEncodedToBinaryString(value);
                hexData = hexData.substring(actualLength);
            } else {
                int varLength = (deConfig.getLengthConfig().getLengthType() == LengthConfig.LengthType.LLVAR ? 2 : 3) * bcdFactor;
                String strDataLength = hexData.substring(0, varLength);
                int dataLength = Integer.parseInt(strDataLength) * bcdFactor;
                value = hexData.substring(varLength, varLength+dataLength);
                if (deConfig.getDataConfig().getEncodingType() == EncodingType.ASCII)
                    value = EncodingUtils.hexEncodedToBinaryString(value);
                hexData = hexData.substring(varLength + dataLength);
            }
            elements.add(new DataElement(index, value));
            logger.log(String.format(" [ index %d value %s ]", index, value));

        }

    }
}
