package global.citytech.finpos.merchant.presentation.merchant.numpad

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager

class NumpadLayoutViewModel(val context: Application) : BaseAndroidViewModel(context) {
    val numpadLayoutType by lazy { MutableLiveData<String>() }

    val localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    fun saveNumpadLayoutType(numpadLayoutType: String){
        localDataUseCase.saveNumpadLayoutType(numpadLayoutType)
    }

    fun getNumpadLayoutType(defaultValue: String){
        numpadLayoutType.value = localDataUseCase.getNumpadLayoutType(defaultValue)
    }
}