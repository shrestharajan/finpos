package global.citytech.finpos.neps.receipt.keyexchange;

import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.usecases.transaction.receipt.keyexchange.KeyExchangeReceipt;
import global.citytech.finposframework.utility.IsoMessageUtils;

/**
 * Created by Unique Shakya on 1/19/2021.
 */
public class NepsKeyExchangeReceiptMaker {

    private IsoMessageResponse isoMessageResponse;

    public NepsKeyExchangeReceiptMaker(IsoMessageResponse isoMessageResponse) {
        this.isoMessageResponse = isoMessageResponse;
    }

    public KeyExchangeReceipt prepare(){
        return KeyExchangeReceipt.Builder.newInstance()
                .withMessage(this.prepareKeyExchangeResultMessage())
                .build();
    }

    private String prepareKeyExchangeResultMessage() {
        if (IsoMessageUtils.retrieveFromDataElementsAsInt(this.isoMessageResponse,
                39) == 0)
            return NepsKeyExchangeReceiptLabels.SUCCESS_MESSAGE;
        else
            return NepsKeyExchangeReceiptLabels.FAILURE_MESSAGE;
    }
}
