package global.citytech.finposframework.usecases.terminal;

/**
 * User: Surajchhetry Date: 2/26/20 Time: 9:47 AM
 */
public class TerminalInfo {
    private final String terminalID;
    private final String merchantID;
    private final String merchantPrintLogo;
    private final String merchantName;
    private final String merchantAddress;
    private final boolean debugModeEnabled;

    public TerminalInfo(String terminalID, String merchantID, String merchantPrintLogo, String merchantName, String merchantAddress) {
        this.terminalID = terminalID;
        this.merchantID = merchantID;
        this.merchantName = merchantName;
        this.merchantPrintLogo = merchantPrintLogo;
        this.merchantAddress = merchantAddress;
        this.debugModeEnabled = false;
    }

    public TerminalInfo(String terminalID, String merchantID, String merchantPrintLogo, String merchantName, String merchantAddress,
                        boolean debugModeEnabled) {
        this.terminalID = terminalID;
        this.merchantID = merchantID;
        this.merchantName = merchantName;
        this.merchantPrintLogo = merchantPrintLogo;
        this.merchantAddress = merchantAddress;
        this.debugModeEnabled = debugModeEnabled;
    }

    public String getMerchantPrintLogo() {
        return merchantPrintLogo;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public String getMerchantAddress() {
        return merchantAddress;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public boolean isDebugModeEnabled() {
        return debugModeEnabled;
    }

    @Override
    public String toString() {
        return "TerminalInfo{" +
                "terminalID='" + terminalID + '\'' +
                ", merchantID='" + merchantID + '\'' +
                ", merchantName='" + merchantName + '\'' +
                ", merchantAddress='" + merchantAddress + '\'' +
                ", debugModeEnabled=" + debugModeEnabled +
                '}';
    }
}
