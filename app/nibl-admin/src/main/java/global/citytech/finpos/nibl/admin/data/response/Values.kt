package global.citytech.finpos.nibl.admin.data.response

data class Values(
    var key1: String? = null,
    var key2: String? = null
)