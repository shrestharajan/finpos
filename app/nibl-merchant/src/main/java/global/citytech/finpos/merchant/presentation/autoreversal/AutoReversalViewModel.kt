package global.citytech.finpos.merchant.presentation.autoreversal

import android.app.Application
import android.content.Intent
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import global.citytech.common.data.Response
import global.citytech.common.extensions.hasInternetConnection
import global.citytech.common.extensions.isNullOrEmptyOrBlank
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.autoreversal.AutoReversalRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.DataBaseResult
import global.citytech.finpos.merchant.domain.model.app.MerchantTransactionLog
import global.citytech.finpos.merchant.domain.model.app.TransactionLog
import global.citytech.finpos.merchant.domain.model.autoreversal.AutoReversalResponseEntity
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.core.autoreversal.CoreAutoReversalUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.autoreversal.AutoReversalDataSourceImpl
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.presentation.model.merchanttransactionlog.MerchantTransactionLogResponse
import global.citytech.finpos.merchant.presentation.payment.PaymentSdkSingleton
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finpos.merchant.presentation.utils.Constants
import global.citytech.finpos.merchant.service.ActivityLogPushService
import global.citytech.finpos.merchant.service.TransactionPushService
import global.citytech.finpos.merchant.utils.*
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.exceptions.PosException
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.switches.PosEntryMode
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog
import global.citytech.finposframework.usecases.transaction.receipt.transaction.TransactionEmvInfo
import global.citytech.finposframework.usecases.transaction.receipt.transaction.TransactionReadCardResponse
import global.citytech.finposframework.usecases.transaction.receipt.transaction.VatInfo
import global.citytech.finposframework.utility.DateUtils
import global.citytech.finposframework.utility.HelperUtils
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Unique Shakya on 6/22/2021.
 */
class AutoReversalViewModel(val context: Application) : BaseAndroidViewModel(context) {

    private val logger = Logger.getLogger(AutoReversalViewModel::class.java.name)
    val TAG = AutoReversalViewModel::class.java.name
    val configurationItem by lazy { MutableLiveData<ConfigurationItem>() }
    val autoReversalSuccess by lazy { MutableLiveData<Boolean>() }
    val autoReversalResponseEntity by lazy { MutableLiveData<AutoReversalResponseEntity>() }
    var reversalMessage = ""

    var checkoutAdditionalData: String? = ""
    val mapOfTransactionLog by lazy { MutableLiveData<Map<String, Any>>() }

    private val localDataUseCase: LocalDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    private var localRepository = LocalRepositoryImpl(
        LocalDatabaseSourceImpl(),
        PreferenceManager
    )

    private val autoReversalUseCase = CoreAutoReversalUseCase(
        AutoReversalRepositoryImpl(
            AutoReversalDataSourceImpl()
        )
    )

    fun getConfigurationItem() {
        isLoading.value = true
        compositeDisposable.add(localDataUseCase.getWholeConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.mapToPresentation() }
            .subscribe({ configurationItem.value = it }, {
                printLog(TAG, "Error in configuration Item ::: " + it.message)
                message.value = it.message
            })
        )
    }

    fun performAutoReversal(it: ConfigurationItem) {
        isLoading.value = true
        compositeDisposable.add(
            autoReversalUseCase.performAutoReversal(it)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    ActivityLogPushService.pushActivityLog()
                    handleAutoReversalResponse(it!!)
                }, {
                    printLog(TAG, "Error in auto reversal ::: " + it.message)
                    isLoading.value = false
                    ActivityLogPushService.pushActivityLog()
                    it.printStackTrace()
                    autoReversalSuccess.value = false
                })
        )
    }

    private fun handleAutoReversalResponse(it: AutoReversalResponseEntity) {
        reversalMessage = it.message.toString()
        autoReversalResponseEntity.value = it
        if (it.result != Result.SUCCESS) {
            isLoading.value = false
            autoReversalSuccess.value = it.result == Result.SUCCESS
        }
    }

    fun pushMerchantTransactionLog(intent: Intent) {
        //isLoading.value = true
        val stan = intent.getStringExtra(global.citytech.common.Constants.KEY_STAN)
        val message = intent.getStringExtra(global.citytech.common.Constants.KEY_MESSAGE)
        val isApproved =
            intent.getBooleanExtra(global.citytech.common.Constants.KEY_IS_APPROVED, false)

        updateMerchantTransactionLog(stan, message, isApproved)
    }

    private fun updateMerchantTransactionLog(
        stan: String?,
        message: String?,
        approved: Boolean
    ) {
        //isLoading.value = true
        stan?.let {
            compositeDisposable.add(
                localRepository.getTransactionLogByStan(stan)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        saveMerchantTransactionLog(it, message!!, approved)
                    }, {
                        isLoading.value = false
                        it.printStackTrace()
                        it?.let {
                            logger.log(it.message)
                        }
                    })
            )
        }

    }

    private fun saveMerchantTransactionLog(
        transactionLog: TransactionLog,
        message: String,
        approved: Boolean
    ) {
        logger.log("::: SAVING MERCHANT LOG :::")
        logger.log("::: Transaction Status  ::: ${transactionLog.transactionStatus}")

        //isLoading.value = true

        updatePaymentTransaction(transactionLog, message, approved)
        val readCardResponse =
            Jsons.fromJsonToObj(transactionLog.readCardResponse, ReadCardResponse::class.java)
        var cardScheme = ""
        var cardSchemeLabel = ""
        if (readCardResponse.cardDetails!!.cardScheme != null) {
            cardScheme = readCardResponse.cardDetails!!.cardScheme!!.cardSchemeId

            var transactionReadCardResponse: TransactionReadCardResponse = Jsons.fromJsonToObj(
                transactionLog.readCardResponse,
                TransactionReadCardResponse::class.java
            )

            cardSchemeLabel = transactionReadCardResponse.cardDetails.cardSchemeLabel
        }

        val binNumber = readCardResponse.cardDetails!!.primaryAccountNumber!!.substring(0, 6)
        var transactionType =
            Jsons.fromJsonToObj(transactionLog.transactionType, TransactionType::class.java)
        transactionLog.isEmiTransaction?.let {
            if (it)
                transactionType = TransactionType.EMI_PURCHASE
        }
        val amount =
            if (transactionType == TransactionType.VOID) transactionLog.origTransactionAmount
            else transactionLog.transactionAmount
        val posEntryMode =
            Jsons.fromJsonToObj(transactionLog.posEntryMode, PosEntryMode::class.java)
         val transactionDate = prepareTransactionDateBasedOnFormats(transactionLog)
        logger.log("Formatted Date ::: ".plus(transactionDate))
        val transactionTime = transactionLog.transactionTime
        logger.log("Formatted Time ::: ".plus(transactionTime))

        val vatInfo: VatInfo = prepareVatInfo(transactionLog)

        val merchantTransactionLog = MerchantTransactionLog(
            stan = transactionLog.stan,
            terminalId = transactionLog.terminalId,
            merchantId = transactionLog.merchantId,
            invoiceNumber = transactionLog.invoiceNumber,
            rrn = transactionLog.rrn,
            txnDate = transactionDate,
            txnTime = transactionTime,
            txnType = transactionType.name,
            amount = amount,
            scheme = cardScheme,
            txnStatus = transactionLog.transactionStatus,
            approvalCode = transactionLog.authCode,
            posEntryMode = posEntryMode.entryMode,
            originalTxnReference = transactionLog.originalTransactionReferenceNumber,
            remarks = JsonObject().toString(),
            bin = binNumber,
            emiInfo = transactionLog.emiInfo,
            currencyCode = transactionLog.currencyCode,
            vatAmount = vatInfo.vatAmount,
            vatRefundAmount = vatInfo.vatRefundAmount,
            cvmMessage = "",
            transactionEmvInfo = JsonObject().toString()
        )
        compositeDisposable.add(
            localRepository.saveMerchantTransactionLog(merchantTransactionLog)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.result == DataBaseResult.SUCCESS) {
                        getMerchantTransactionLogsForPush()
                    }
                }, {
                    isLoading.value = false
                    logger.log(it.message)
                })
        )
    }

    private fun updatePaymentTransaction(
        transactionLog: TransactionLog,
        message: String,
        approved: Boolean
    ) {
        mapOfTransactionLog.value = prepareMapOfTransactionLog(transactionLog, message, approved)
    }

    private fun prepareMapOfTransactionLog(
        transactionLog: TransactionLog,
        message: String,
        approved: Boolean
    ): Map<String, Any>? {
        val map = mutableMapOf<String, Any>()
//        val readCardResponse = Jsons
//            .fromJsonToObj(transactionLog.readCardResponse, ReadCardResponse::class.java)
        map[global.citytech.common.Constants.EXTRA_AUTH_CODE] = transactionLog.authCode!!
//        map[global.citytech.common.Constants.EXTRA_CARD_NUMBER] = readCardResponse.let {
//            readCardResponse
//                .cardDetails!!.primaryAccountNumber!!
//        }
        map[global.citytech.common.Constants.EXTRA_MESSAGE] = message
        map[global.citytech.common.Constants.EXTRA_APPROVED] = approved
        map[global.citytech.common.Constants.EXTRA_RRN] = transactionLog.rrn!!
        map[global.citytech.common.Constants.EXTRA_STAN] = transactionLog.stan
        map[global.citytech.common.Constants.EXTRA_TRANSACTION_TIME] =
            transactionLog.transactionTime!!
        map[global.citytech.common.Constants.EXTRA_PAYMENT_MODE] = "CARD"
//        map[global.citytech.common.Constants.EXTRA_PAYMENT_NETWORK] =
//            readCardResponse.let {
//                readCardResponse.cardDetails!!.cardScheme!!.cardScheme
//            }
        return map
    }

    private fun prepareTransactionDateBasedOnFormats(transactionLog: TransactionLog): String {
        var transactionDate: String? = transactionLog.transactionDate ?: ""
        if (transactionDate.isNullOrEmptyOrBlank()) {
            return ""
        }
        if (transactionDate!!.isTransactionDateOrTimeOfGivenFormat(DateUtils.DATE_FORMAT_ONE)) {
            transactionDate = transactionDate.parseDateInFormatOneForMerchantLog()
        } else if (transactionDate.isTransactionDateOrTimeOfGivenFormat(DateUtils.DATE_FORMAT_TWO)) {
            transactionDate = transactionDate.parseDateInFormatTwoForMerchantLog()
        }
        return transactionDate
    }

    private fun prepareVatInfo(transactionLog: TransactionLog): VatInfo {
        return if (transactionLog.vatInfo.isNullOrEmpty()) {
            VatInfo()
        } else {
            Jsons.fromJsonToObj(transactionLog.vatInfo, VatInfo::class.java)
        }
    }

    private fun checkCustomerDetailFlag(): Boolean {
        localDataUseCase.getTerminalSetting().receiptConfiguration?.enableCustomerDetails?.let {
            return it
        }
        return false
    }

    private fun checkCardDetailFlag(): Boolean {
        localDataUseCase.getTerminalSetting().receiptConfiguration?.enableCardDetails?.let {
            return it
        }
        return false
    }

    fun getMerchantTransactionLogsForPush() {
        //isLoading.value = true
        compositeDisposable.add(
            localRepository.getMerchantTransactionLogsOfGivenCount(
                AppConstant.MAX_TRANSACTIONS_COUNT_FOR_BATCH_PUSH
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    updateMerchantTransactionLogsToFinpos(it)
                }, {
                    isLoading.value = false
                    logger.log(it.message)
                })
        )
    }

    private fun updateMerchantTransactionLogsToFinpos(merchantTransactionLogs: List<MerchantTransactionLog>?) {
        if (context.hasInternetConnection()) {
            TransactionPushService.isLogPushInProgress = true
            compositeDisposable.add(
                Observable.create(ObservableOnSubscribe<Boolean> {
                    onSubscribe(merchantTransactionLogs, it)
                })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onNext, this::onError)
            )
        }
    }

    private fun onNext(responseSuccess: Boolean) {
       // isLoading.value = true
        TransactionPushService.isLogPushInProgress = false
        if (responseSuccess) {
            compositeDisposable.add(
                localRepository.deleteMerchantTransactionLogsOfGivenCountFromInitial(
                    AppConstant.MAX_TRANSACTIONS_COUNT_FOR_BATCH_PUSH
                )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        isLoading.value = false
                        autoReversalSuccess.value =
                            autoReversalResponseEntity.value?.result == Result.SUCCESS
                        logger.log("${AppConstant.MAX_TRANSACTIONS_COUNT_FOR_BATCH_PUSH} Merchant Transaction Logs Cleared From Database...")
                    }, {
                        isLoading.value = false
                        logger.log(it.message)
                    })
            )
        } else {
            logger.log("Some Error occurred during pushing merchant transaction log.")
        }

    }

    private fun onError(throwable: Throwable) {
        isLoading.value = false
        TransactionPushService.isLogPushInProgress = false
        logger.log(throwable.message)
    }


    private fun onSubscribe(
        merchantTransactionLogs: List<MerchantTransactionLog>?,
        emitter: ObservableEmitter<Boolean>
    ) {
        val jsonRequest = Jsons.toJsonObj(merchantTransactionLogs)
        logger.debug("Merchant Transaction Log Request::: $jsonRequest")
        val jsonResponse =
            NiblMerchant.INSTANCE.iPlatformManager.post(
                ApiConstant.UPDATE_MERCHANT_TRANSACTION_LOG_URL,
                jsonRequest,
                true
            )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            emitter.onNext(response.message == Result.SUCCESS.message || response.message == Result.SUCCESS.message.toUpperCase())
        } else {
            if (response.code == "401")
                emitter.onError(PosException(PosError.DEVICE_ERROR_OPERATOR_LOGIN_REQUIRED))
            else
                emitter.onError(Exception("Error Code :: ${response.code}. ${response.message}"))
        }
        emitter.onComplete()
    }

}