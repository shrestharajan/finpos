package global.citytech.finpos.merchant.domain.repository.core.greenpin

import global.citytech.finpos.merchant.domain.model.greenpin.GreenPinResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.greenpin.GreenPinRequestItem
import io.reactivex.Observable

interface GreenPinRepository {
    fun generateOtpRequest(
        configurationItem: ConfigurationItem,
        greenPinRequestItem: GreenPinRequestItem
    ): Observable<GreenPinResponseEntity>
}