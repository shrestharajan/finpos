package global.citytech.finposframework.usecases.transaction.data;

import java.util.Objects;

import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.usecases.TransactionType;

/**
 * Created by Unique Shakya on 6/22/2021.
 */
public class AutoReversal {
    private TransactionType transactionType;
    private String reversalRequest;
    private int retryCount;
    private Status status;
    private String timeStamp;
    private ReadCardResponse readCardResponse;

    public AutoReversal(TransactionType transactionType, String reversalRequest, int retryCount, Status status, String timeStamp) {
        this.transactionType = transactionType;
        this.reversalRequest = reversalRequest;
        this.retryCount = retryCount;
        this.status = status;
        this.timeStamp = timeStamp;
    }

    public AutoReversal(TransactionType transactionType, String reversalRequest, int retryCount, Status status, String timeStamp,ReadCardResponse readCardResponse) {
        this.transactionType = transactionType;
        this.reversalRequest = reversalRequest;
        this.retryCount = retryCount;
        this.status = status;
        this.timeStamp = timeStamp;
        this.readCardResponse = readCardResponse;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public String getReversalRequest() {
        return reversalRequest;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public Status getStatus() {
        return status;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public ReadCardResponse getReadCardResponse() {
        return readCardResponse;
    }

    public enum Status {
        ACTIVE, INACTIVE
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AutoReversal that = (AutoReversal) o;
        return retryCount == that.retryCount &&
                transactionType == that.transactionType &&
                Objects.equals(reversalRequest, that.reversalRequest) &&
                status == that.status &&
                Objects.equals(timeStamp, that.timeStamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionType, reversalRequest, retryCount, status, timeStamp);
    }
}
