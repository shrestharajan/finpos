package global.citytech.finpos.processor.nibl.transaction.reversal;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.reversal.ReversalRequestSender;
import global.citytech.finpos.processor.nibl.transaction.TransactionUseCase;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.listeners.CardConfirmationListener;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reversal.ReversalRequester;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.activity.ActivityLog;
import global.citytech.finposframework.usecases.activity.ActivityLogStatus;
import global.citytech.finposframework.usecases.activity.ActivityLogType;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
import global.citytech.finposframework.utility.AmountUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.JsonUtils;
import global.citytech.finposframework.utility.StackTraceUtil;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Rishav Chudal on 9/29/20.
 */
public class ReversalUseCase extends TransactionUseCase implements
        UseCase<ReversalRequestModel, global.citytech.finpos.processor.nibl.transaction.reversal.ReversalResponseModel>,
        ReversalRequester<ReversalRequestModel, global.citytech.finpos.processor.nibl.transaction.reversal.ReversalResponseModel> {

    private final String stan;
    private NiblReversalRequest niblReversalRequest;
    private ReversalRequestModel reversalRequestModel;

    public ReversalUseCase(
            NiblReversalRequest niblReversalRequest,
            TerminalRepository terminalRepository,
            Notifier notifier
    ) {
        super(terminalRepository, notifier);
        this.niblReversalRequest = niblReversalRequest;
        this.stan = this.niblReversalRequest.getStan();
    }

    @Override
    public ReversalResponseModel execute(ReversalRequestModel request){
        try {
            this.reversalRequestModel = request;
            this.logger.log("::: REVERSAL USE CASE :::");
            this.notifier.notify(
                    Notifier.EventType.PERFORMING_REVERSAL,
                    Notifier.EventType.PERFORMING_REVERSAL.getDescription()
            );
            IsoMessageResponse isoMessageResponse = sendIsoMessage();
            this.printDebugReceiptIfDebugMode(request.getPrinterService(), isoMessageResponse);
            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait..");
            boolean isReversalApproved = this.isTransactionApprovedByActionCode(isoMessageResponse);
            this.notifyTransactionCompletion(stan, isReversalApproved, isoMessageResponse);

            String message = retrieveMessage(isoMessageResponse, isReversalApproved, request.getApplicationRepository());
            this.notifier.notify(Notifier.EventType.PUSH_MERCHANT_LOG, this.stan, message, isReversalApproved);

            this.notifier.notify(Notifier.EventType.REVERSAL_COMPLETED,
                    Notifier.EventType.REVERSAL_COMPLETED.getDescription());
            updateActivityLog(niblReversalRequest, isoMessageResponse, isReversalApproved);
            return this.prepareResponse(stan, isoMessageResponse, isReversalApproved);
        } catch (FinPosException e) {
            if (!e.getMessage().equals(FinPosException.ExceptionType.UNABLE_TO_CONNECT.getMessage()) &&
                    !e.getMessage().equals(FinPosException.ExceptionType.READ_TIME_OUT.getMessage())) {
                updateActivityLog(niblReversalRequest, e);
            }
            throw e;
        } catch (Exception e) {
            updateActivityLog(niblReversalRequest, StackTraceUtil.getStackTraceAsString(e));
            throw e;
        }
    }

    private void updateActivityLog(NiblReversalRequest niblReversalRequest, IsoMessageResponse isoMessageResponse, boolean isReversalApproved) {
        try {
            TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
            String responseCode = IsoMessageUtils.retrieveFromDataElementsAsString(
                    isoMessageResponse,
                    DataElement.RESPONSE_CODE
            );
            BigDecimal totalAmount = niblReversalRequest.getAmount().add(niblReversalRequest.getAdditionalAmount());
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(niblReversalRequest.getStan())
                    .withType(ActivityLogType.REVERSAL)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(this.terminalRepository.getReconciliationBatchNumber())
                    .withAmount(AmountUtils.toLowerCurrencyFromBigDecimalInLong(totalAmount))
                    .withResponseCode(responseCode)
                    .withIsoRequest("")
                    .withIsoResponse(isoMessageResponse.getDebugResponseString())
                    .withStatus(isReversalApproved ? ActivityLogStatus.SUCCESS : ActivityLogStatus.FAILED)
                    .withRemarks("")
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(StringUtils.prepareAdditionalDataForActivityLog(this.terminalRepository.getTerminalSerialNumber()))
                    .build();
            this.terminalRepository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update REVERSAL activity log :: " + e.getMessage());
        }
    }

    private void updateActivityLog(NiblReversalRequest niblReversalRequest, FinPosException e) {
        try {
            TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
            BigDecimal totalAmount = niblReversalRequest.getAmount().add(niblReversalRequest.getAdditionalAmount());
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(niblReversalRequest.getStan())
                    .withType(ActivityLogType.REVERSAL)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(this.terminalRepository.getReconciliationBatchNumber())
                    .withAmount(AmountUtils.toLowerCurrencyFromBigDecimalInLong(totalAmount))
                    .withResponseCode("")
                    .withIsoRequest(e.getIsoRequest())
                    .withIsoResponse("")
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(e.getLocalizedMessage() + " : " + e.getDetailMessage())
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(StringUtils.prepareAdditionalDataForActivityLog(this.terminalRepository.getTerminalSerialNumber()))
                    .build();
            this.terminalRepository.updateActivityLog(activityLog);
        } catch (Exception ex) {
            this.logger.log("Unable to update REVERSAL activity log :: " + ex.getMessage());
        }
    }

    private void updateActivityLog(NiblReversalRequest niblReversalRequest, String remarks) {
        try {
            TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
            BigDecimal totalAmount = niblReversalRequest.getAmount().add(niblReversalRequest.getAdditionalAmount());
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.terminalRepository.getSystemTraceAuditNumber())
                    .withType(ActivityLogType.REVERSAL)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(this.terminalRepository.getReconciliationBatchNumber())
                    .withAmount(AmountUtils.toLowerCurrencyFromBigDecimalInLong(totalAmount))
                    .withResponseCode("")
                    .withIsoRequest("")
                    .withIsoResponse("")
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(remarks)
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(StringUtils.prepareAdditionalDataForActivityLog(this.terminalRepository.getTerminalSerialNumber()))
                    .build();
            this.terminalRepository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update REVERSAL activity log :: " + e.getMessage());
        }
    }

    private IsoMessageResponse sendIsoMessage() {
        RequestContext requestContext = prepareRequestContext();
        this.logger.debug(String.format("Request Context ::: %s", requestContext));
        ReversalRequestSender reversalRequestSender = new ReversalRequestSender(
                new NIBLSpecInfo(),
                requestContext
        );
        this.autoReversal = new AutoReversal(
                TransactionType.REVERSAL,
                JsonUtils.toJsonObj((NiblReversalRequest) requestContext.getRequest()),
                0,
                AutoReversal.Status.ACTIVE,
                StringUtils.dateTimeStamp()
        );
        IsoMessageResponse isoMessageResponse = reversalRequestSender.send();
        return isoMessageResponse;
    }

    private RequestContext prepareRequestContext() {
        HostInfo primaryHostInfo = this.terminalRepository.findPrimaryHost();
        HostInfo secondaryHostInfo = this.terminalRepository.findSecondaryHost();
        TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
        ConnectionParam connectionParam = new ConnectionParam(primaryHostInfo, secondaryHostInfo);
        RequestContext context =
                new RequestContext(
                        this.niblReversalRequest.getStan(),
                        terminalInfo,
                        connectionParam
                );
        context.setRequest(this.niblReversalRequest);
        return context;
    }

    private void notifyTransactionCompletion(
            String stan,
            boolean reversalApproved,
            IsoMessageResponse isoMessageResponse
    ) {
        this.updateTransactionLog(
                stan,
                this.terminalRepository.getReconciliationBatchNumber(),
                this.terminalRepository.getInvoiceNumber(),
                reversalApproved,
                isoMessageResponse
        );
        this.updateTransactionIds(reversalApproved);
    }

    private void updateTransactionLog(
            String stan,
            String batchNumber,
            String invoiceNumber,
            boolean isReversalApproved,
            IsoMessageResponse isoMessageResponse
    ) {
        this.logger.log("REVERSAL ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
        this.logger.log("REVERSAL ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
        this.logger.log("STAN >>> " + stan);
        this.logger.log("ReceiptLog >>> " + this.reversalRequestModel.getReceiptLog());
        this.logger.log("Currency Code >>> " + this.reversalRequestModel.getCurrencyCode());

        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
                .withStan(stan)
                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
                .withInvoiceNumber(invoiceNumber)
                .withRrn(
                        IsoMessageUtils.retrieveFromDataElementsAsString(
                                isoMessageResponse,
                                DataElement.RETRIEVAL_REFERENCE_NUMBER
                        )
                )
                .withAuthCode(
                        IsoMessageUtils.retrieveFromDataElementsAsString(
                                isoMessageResponse,
                                DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE
                        )
                )
                .withTransactionType(TransactionType.REVERSAL)
                .withOriginalTransactionType(
                        this.reversalRequestModel.getTransactionRequest().getTransactionType()
                )
                .withTransactionAmount(
                        this.reversalRequestModel.getTransactionRequest().getAmount()
                )
                .withOriginalTransactionAmount(
                        this.reversalRequestModel.getTransactionRequest().getAmount()
                )
                .withTransactionDate(
                        IsoMessageUtils.retrieveFromDataElementsAsString(
                                isoMessageResponse,
                                DataElement.LOCAL_DATE
                        )
                )
                .withTransactionTime(
                        IsoMessageUtils.retrieveFromDataElementsAsString(
                                isoMessageResponse,
                                DataElement.LOCAL_TIME
                        )
                )
                .withTransactionStatus(isReversalApproved ? "ACCEPTED" : "DECLINED")
                .withPosEntryMode(this.niblReversalRequest.getPosEntryMode())
                .withOriginalPosEntryMode(this.niblReversalRequest.getPosEntryMode())
                .withPosConditionCode(this.niblReversalRequest.getPosConditionCode())
                .withOriginalPosConditionCode(this.niblReversalRequest.getPosConditionCode())
                .withReconcileStatus("") //TODO
                .withReconcileTime("") //TODO
                .withReconcileDate("") //TODO
                .withReconcileBatchNo(batchNumber)
                .withAuthorizationCompleted(true)
                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                .withReadCardResponse(this.reversalRequestModel.getReadCardResponse())
                .withCurrencyCode(this.reversalRequestModel.getCurrencyCode())
                .withReceiptLog(this.reversalRequestModel.getReceiptLog())
                .build();

        this.reversalRequestModel.getTransactionRepository().updateTransactionLog(transactionLog);
    }

    private ReversalResponseModel prepareResponse(
            String stan,
            IsoMessageResponse isoMessageResponse,
            boolean isReversalApproved
    ) {
        ReversalResponseModel.Builder builder = ReversalResponseModel.Builder.newInstance();
        builder.withStan(stan);
        builder.withApproved(isReversalApproved);
        builder.withDebugRequestMessage(isoMessageResponse.getDebugRequestString());
        builder.withDebugResponseMessage(isoMessageResponse.getDebugResponseString());
        builder.withMessage(
                this.retrieveMessage(
                        isoMessageResponse,
                        isReversalApproved,
                        this.reversalRequestModel.getApplicationRepository()
                )
        );
        return builder.build();
    }

    @Override
    protected String getClassName() {
        return ReversalUseCase.class.getName();
    }
}
