package global.citytech.finpos.processor.nibl.transaction;

import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.List;

import global.citytech.finpos.nibl.iso8583.S2MActionCode;
import global.citytech.finpos.nibl.iso8583.S2MIccDataList;
import global.citytech.finpos.processor.nibl.transaction.authorizer.NiblTransactionAuthorizer;
import global.citytech.finpos.processor.nibl.transaction.initializer.NiblTransactionInitializer;
import global.citytech.finpos.processor.nibl.transaction.receipt.NiblIsoMessageReceiptHandler;
import global.citytech.finpos.processor.nibl.transaction.receipt.NiblTransactionReceiptPrintHandler;
import global.citytech.finpos.processor.nibl.transaction.receipt.TransactionReceiptHandler;
import global.citytech.finpos.processor.nibl.transaction.responsehandler.NiblInvalidResponseHandler;
import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils;
import global.citytech.finpos.processor.nibl.transaction.validator.NiblTransactionValidator;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedAction;
import global.citytech.finposframework.hardware.io.led.LedLight;
import global.citytech.finposframework.hardware.io.led.LedRequest;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.TransactionStatusFromHost;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.CardProcessor;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandler;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.TransactionAuthorizer;
import global.citytech.finposframework.usecases.transaction.TransactionInitializer;
import global.citytech.finposframework.usecases.transaction.TransactionValidator;
import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
import global.citytech.finposframework.usecases.transaction.data.Bin;
import global.citytech.finposframework.usecases.transaction.data.TransactionAuthorizationResponse;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.PosResponse;
import global.citytech.finposframework.utility.PosResult;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finposframework.utility.TMSConfigurationConstants.APPROVED_PRINT_ONLY;

/**
 * Created by Unique Shakya on 9/28/2020.
 */
public abstract class TransactionUseCase {

    protected final TerminalRepository terminalRepository;
    protected final Notifier notifier;
    protected final Logger logger;
    protected TransactionValidator transactionValidator;
    protected CardProcessor cardProcessor;
    protected AutoReversal autoReversal;
    protected TransactionRepository transactionRepository;
    protected final ReadCardRequest.MultipleAidSelectionListener multipleAidSelectionListener = new ReadCardRequest.MultipleAidSelectionListener() {
        @Override
        public int onMultipleAid(@NotNull String[] aids) {
            notifier.notify(Notifier.EventType.DETECTED_MULTIPLE_AID, "Processing..");
            return -1;
        }
    };

    public TransactionUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        this.terminalRepository = terminalRepository;
        this.notifier = notifier;
        this.logger = new Logger(this.getClassName());
    }

    protected abstract String getClassName();

    protected boolean isDeviceReady(DeviceController deviceController, TransactionType transactionType) {
        PosResponse deviceReadyResponse = deviceController.isReady(global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils
                .getAllowedCardEntryModes(transactionType));
        if (deviceReadyResponse.getPosResult() == PosResult.SUCCESS)
            return true;
        else
            throw new PosException(deviceReadyResponse.getPosError());
    }

    protected void initTransaction(ApplicationRepository applicationRepository, TransactionRequest transactionRequest) {
        TransactionInitializer transactionInitializer = new NiblTransactionInitializer(applicationRepository);
        TransactionRequest afterInitTransactionRequest = transactionInitializer.initialize(transactionRequest);
        this.validateRequest(afterInitTransactionRequest, applicationRepository);
    }

    protected ReadCardResponse detectCard(ReadCardRequest readCardRequest, ReadCardService readCardService,
                                          LedService ledService) {
        ledService.doTurnLedWith(new LedRequest(LedLight.BLUE, LedAction.ON));
        ReadCardResponse readCardResponse = readCardService.readCardDetails(readCardRequest);
        this.validateReadCardResponse(readCardResponse);
        return readCardResponse;
    }

    protected void validateReadCardResponse(ReadCardResponse readCardResponse) {
        if (readCardResponse == null)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        if (readCardResponse.getResult() == Result.TIMEOUT)
            throw new PosException(PosError.DEVICE_ERROR_CARD_READ_TIMEOUT);
        if (readCardResponse.getResult() == Result.USER_CANCELLED || readCardResponse.getResult() == Result.CANCEL)
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        if (readCardResponse.getResult() == Result.FAILURE)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
    }

    protected boolean isValidTransaction(ApplicationRepository applicationRepository,
                                         TransactionRequest transactionRequest,
                                         ReadCardResponse readCardResponse) {
        transactionValidator = new global.citytech.finpos.processor.nibl.transaction.validator.NiblTransactionValidator(applicationRepository);
        return transactionValidator.isValidTransaction(transactionRequest, readCardResponse,
                readCardResponse.getCardDetails().getCardScheme().getCardSchemeId());
    }

    protected boolean isTransactionAuthorizationSuccess(TransactionAuthenticator transactionAuthenticator,
                                                        ApplicationRepository applicationRepository,
                                                        TransactionType transactionType,
                                                        ReadCardResponse readCardResponse) {
        TransactionAuthorizer transactionAuthorizer = new NiblTransactionAuthorizer(transactionAuthenticator,
                applicationRepository);
        TransactionAuthorizationResponse transactionAuthorizationResponse = transactionAuthorizer
                .authorizeUser(transactionType, readCardResponse.getCardDetails().getCardScheme().getCardSchemeId());
        return transactionAuthorizationResponse.isSuccess();
    }

    protected boolean isTransactionApprovedByActionCode(IsoMessageResponse isoMessageResponse) {
        int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, 39);
        return S2MActionCode.getByActionCode(actionCode).getTransactionStatusFromHost() == TransactionStatusFromHost.ONLINE_APPROVED;
    }

    protected void setOnlineResultToCard(ReadCardResponse readCardResponse, ReadCardService readCardService, IsoMessageResponse isoMessageResponse) {
        if (this.isArqcTransaction(readCardResponse)) {
            int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, 39);
            ReadCardResponse afterSetOnlineResultResponse = readCardService.setOnlineResult(
                    actionCode,
                    mapTransactionHostStatus(S2MActionCode.getByActionCode(actionCode)
                            .getTransactionStatusFromHost()),
                    this.prepareField55DataForOnlineResult(actionCode, IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, 55)),
                    false);
            readCardResponse.getCardDetails().setIccDataBlock(afterSetOnlineResultResponse
                    .getCardDetails().getIccDataBlock());
            readCardResponse.getCardDetails().setTagCollection(afterSetOnlineResultResponse
                    .getCardDetails().getTagCollection());
            readCardResponse.getCardDetails().setTransactionState(afterSetOnlineResultResponse
                    .getCardDetails().getTransactionState());
        }
    }

    private String prepareField55DataForOnlineResult(int actionCode, String field55FromHost) {
        StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isEmpty(field55FromHost) || !field55FromHost.contains("8A02")) {
            stringBuilder.append("8A02");
            stringBuilder.append(StringUtils.toHexaDecimal(StringUtils.ofRequiredLength(String.valueOf(actionCode), 2)));
        }
        if (!StringUtils.isEmpty(field55FromHost))
            stringBuilder.append(field55FromHost);
        return stringBuilder.toString();
    }

    protected void notifyTransactionStatus(
            boolean transactionApproved,
            IsoMessageResponse isoMessageResponse,
            ApplicationRepository applicationRepository,
            LedService ledService,
            boolean turnLedOn
    ) {
        if (transactionApproved) {
            if (turnLedOn)
                ledService.doTurnLedWith(new LedRequest(LedLight.GREEN, LedAction.ON));
            this.notifier.notify(
                    Notifier.EventType.TRANSACTION_APPROVED,
                    this.approvedMessage(isoMessageResponse)
            );
        } else {
            if (turnLedOn)
                ledService.doTurnLedWith(new LedRequest(LedLight.RED, LedAction.ON));
            this.notifier.notify(
                    Notifier.EventType.TRANSACTION_DECLINED,
                    this.declinedMessage(
                            isoMessageResponse,
                            applicationRepository
                    )
            );
        }
    }

    protected boolean printReceipt(String batchNumber, String stan, String invoiceNumber,
                                   PrinterService printerService, TransactionRequest transactionRequest,
                                   TransactionRepository transactionRepository,
                                   ReadCardResponse readCardResponse,
                                   IsoMessageResponse isoMessageResponse,
                                   ApplicationRepository applicationRepository) {
        printDebugReceiptIfDebugMode(printerService, isoMessageResponse);
        boolean approved = this.isTransactionApprovedByActionCode(isoMessageResponse);
        boolean approvePrintOnly = this.approvePrintOnly(applicationRepository);
        boolean shouldPrintReceipt = approved || !approvePrintOnly;
        logger.log("::: TRANSACTION USE CASE ::: SHOULD PRINT RECEIPT == " + shouldPrintReceipt);
        if (shouldPrintReceipt) {
            ReceiptHandler.TransactionReceiptHandler receiptHandler
                    = new NiblTransactionReceiptPrintHandler(printerService);
            ReceiptLog receiptLog = getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse, applicationRepository, batchNumber, stan, invoiceNumber);

            transactionRepository.updateReceiptLog(receiptLog);
            receiptHandler.printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
        }
        return shouldPrintReceipt;
    }

    protected ReceiptLog getReceiptLog(TransactionRequest transactionRequest, ReadCardResponse readCardResponse, IsoMessageResponse isoMessageResponse, ApplicationRepository applicationRepository, String batchNumber, String stan, String invoiceNumber) {
        return new TransactionReceiptHandler(transactionRequest, readCardResponse,
                isoMessageResponse, terminalRepository, applicationRepository).prepare(batchNumber, stan, invoiceNumber);
    }

    protected String retrieveMessage(IsoMessageResponse response, boolean isTransactionApproved,
                                     ApplicationRepository applicationRepository) {
        if (isTransactionApproved)
            return this.approvedMessage(response);
        else
            return this.declinedMessage(response, applicationRepository);
    }

    protected RequestContext prepareTemplateRequestContext(String stan) {
        HostInfo primaryHostInfo = this.terminalRepository.findPrimaryHost();
        HostInfo secondaryHostInfo = this.terminalRepository.findSecondaryHost();
        TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
        ConnectionParam connectionParam = new ConnectionParam(primaryHostInfo, secondaryHostInfo);
        return new RequestContext(stan, terminalInfo, connectionParam);
    }

    protected void printDebugReceiptIfDebugMode(PrinterService printerService, IsoMessageResponse isoMessageResponse) {
        boolean isDebugVersion = this.terminalRepository.findTerminalInfo().isDebugModeEnabled();
        if (isDebugVersion) {
            ReceiptHandler.IsoMessageReceiptHandler isoMessageReceiptHandler
                    = new NiblIsoMessageReceiptHandler(printerService);
            isoMessageReceiptHandler.printIsoMessageReceipt(isoMessageResponse);
        }
    }

    protected void updateTransactionIds(boolean incrementInvoiceNumber) {
        this.terminalRepository.incrementSystemTraceAuditNumber();
        this.terminalRepository.incrementRetrievalReferenceNumber();
        if (incrementInvoiceNumber)
            this.terminalRepository.incrementInvoiceNumber();
    }

    private String declinedMessage(IsoMessageResponse isoMessageResponse,
                                   ApplicationRepository applicationRepository) {
        int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, 39);
        if (actionCode < 0)
            throw new IllegalArgumentException("Invalid Action Code");
        String messageText = applicationRepository.getMessageTextByActionCode(actionCode);
        if (StringUtils.isEmpty(messageText))
            messageText = S2MActionCode.getByActionCode(actionCode).getDescription();
        return messageText;
    }

    private String approvedMessage(IsoMessageResponse isoMessageResponse) {
        StringBuilder stringBuilder = new StringBuilder();
        String approvalCode = IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse,
                DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE);
        if (StringUtils.isEmpty(approvalCode))
            approvalCode = IsoMessageUtils.retrieveFromRequestDataElementAsString(isoMessageResponse,
                    DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE);
        stringBuilder.append("Approval Code : "+approvalCode);
        return stringBuilder.toString();
    }

    private ReadCardService.OnlineResult mapTransactionHostStatus(TransactionStatusFromHost transactionStatusFromHost) {
        switch (transactionStatusFromHost) {
            case ONLINE_APPROVED:
                return ReadCardService.OnlineResult.ONLINE_APPROVED;
            case ONLINE_DECLINED:
                return ReadCardService.OnlineResult.ONLINE_DECLINED;
            default:
                return ReadCardService.OnlineResult.UNABLE_TO_GO_ONLINE;
        }
    }

    private boolean isArqcTransaction(ReadCardResponse readCardResponse) {
        try {
            return readCardResponse
                    .getCardDetails()
                    .getTagCollection()
                    .get(IccData.CID.getTag())
                    .getData()
                    .equals("80");
        } catch (Exception e) {
            return false;
        }
    }

    protected void validateRequest(TransactionRequest transactionRequest, ApplicationRepository applicationRepository) {
        transactionValidator = new NiblTransactionValidator(applicationRepository);
        transactionValidator.validateRequest(transactionRequest);
    }

    protected ReadCardRequest prepareReadCardRequest(String stan, ApplicationRepository applicationRepository,
                                                     TransactionRequest transactionRequest) {
        ReadCardRequest readCardRequest = new ReadCardRequest(
                global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils.getAllowedCardEntryModes(transactionRequest.getTransactionType()),
                transactionRequest.getTransactionType(),
                global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils.retrieveTransactionType9C(transactionRequest.getTransactionType()),
                applicationRepository.retrieveEmvParameterRequest()
        );
        readCardRequest.setStan(stan);
        readCardRequest.setAmount(transactionRequest.getAmount());
        readCardRequest.setCashBackAmount(transactionRequest.getAdditionalAmount());
        readCardRequest.setTransactionDate(this.retrieveTransactionDate());
        readCardRequest.setTransactionTime(this.retrieveTransactionTime());
        readCardRequest.setMultipleAidSelectionListener(this.multipleAidSelectionListener);
        readCardRequest.setIccDataList(S2MIccDataList.get());
        readCardRequest.setCurrencyName(this.terminalRepository.getTerminalTransactionCurrencyName());
        readCardRequest.setPinPadFixedLayout(!this.terminalRepository.shouldShufflePinPad());
        return readCardRequest;
    }

    protected String retrieveTransactionDate() {
        return DateUtils.yyMMddDate();
    }

    protected String retrieveTransactionTime() {
        return DateUtils.HHmmssTime();
    }

    protected void setUnableToGoOnlineResultToCard(ReadCardResponse readCardResponse, ReadCardService readCardService) {
        if (this.isArqcTransaction(readCardResponse)) {
            ReadCardResponse afterSetOnlineResultResponse = readCardService.setOnlineResult(
                    -99, ReadCardService.OnlineResult.UNABLE_TO_GO_ONLINE,
                    "", false);
            readCardResponse.getCardDetails().setIccDataBlock(afterSetOnlineResultResponse
                    .getCardDetails().getIccDataBlock());
            readCardResponse.getCardDetails().setTagCollection(afterSetOnlineResultResponse
                    .getCardDetails().getTagCollection());
            readCardResponse.getCardDetails().setTransactionState(afterSetOnlineResultResponse
                    .getCardDetails().getTransactionState());
        }
    }

    protected InvalidResponseHandlerResponse checkForInvalidResponseFromHost(IsoMessageResponse response) {
        InvalidResponseHandler invalidResponseHandler = new NiblInvalidResponseHandler();
        return invalidResponseHandler.isInvalidResponse(response);
    }

    protected boolean isTransactionDeclineByCardEvenIfSuccessOnSwitch(
            IsoMessageResponse isoMessageResponse,
            ReadCardResponse readCardResponse
    ) {
        return transactionValidator.isTransactionDeclineByCardEvenIfSuccessOnSwitch(
                isoMessageResponse,
                readCardResponse
        );
    }

    protected boolean isForceContactCardPromptActionCode(ReadCardResponse readCardResponse, IsoMessageResponse purchaseIsoMessageResponse) {
        if (readCardResponse.getCardDetails().getCardType() == CardType.PICC)
            return IsoMessageUtils.retrieveFromDataElementsAsInt(purchaseIsoMessageResponse, 39) == 65;
        else
            return false;
    }

    protected ReadCardRequest prepareForceContactCardReadCardRequest(String stan, ApplicationRepository applicationRepository, TransactionRequest transactionRequest) {
        ReadCardRequest readCardRequest = new ReadCardRequest(Collections.singletonList(CardType.ICC),
                transactionRequest.getTransactionType(),
                TransactionUtils.retrieveTransactionType9C(transactionRequest.getTransactionType()),
                applicationRepository.retrieveEmvParameterRequest()
        );
        readCardRequest.setStan(stan);
        readCardRequest.setAmount(transactionRequest.getAmount());
        readCardRequest.setCashBackAmount(transactionRequest.getAdditionalAmount());
        readCardRequest.setTransactionDate(this.retrieveTransactionDate());
        readCardRequest.setTransactionTime(this.retrieveTransactionTime());
        readCardRequest.setMultipleAidSelectionListener(this.multipleAidSelectionListener);
        readCardRequest.setIccDataList(S2MIccDataList.get());
        readCardRequest.setPinPadFixedLayout(!this.terminalRepository.shouldShufflePinPad());
        return readCardRequest;
    }

    protected boolean isResponseNotReceivedException(FinPosException fe) {
        return fe.getType() == FinPosException.ExceptionType.READ_TIME_OUT ||
                fe.getType() == FinPosException.ExceptionType.CONNECTION_ERROR ||
                fe.getType() == FinPosException.ExceptionType.WRITE_EXCEPTION;
    }

    protected boolean approvePrintOnly(ApplicationRepository applicationRepository) {
        String approvedPrintOnly = applicationRepository
                .retrieveFromAdditionalDataEmvParameters(APPROVED_PRINT_ONLY);
        logger.log("::: TRANSACTION USE CASE ::: APPROVED PRINT ONLY FROM EMV PARAMETERS === " + approvedPrintOnly);
        if (StringUtils.isEmpty(approvedPrintOnly))
            return false;
        return Boolean.parseBoolean(approvedPrintOnly);
    }

    protected PosEntryMode getPosEntryMode(CardType cardType) {
        switch (cardType) {
            case ICC:
                return PosEntryMode.ICC;
            case PICC:
                return PosEntryMode.PICC;
            case MAG:
                return PosEntryMode.MAGNETIC_STRIPE;
            case MANUAL:
                return PosEntryMode.MANUAL;
            case UNKNOWN:
            default:
                return PosEntryMode.UNSPECIFIED;
        }
    }

    protected void validateForAllowedBin(String primaryAccountNumber, List<Bin> transactionBins) {
        try {
            String binRetrievedFromCard = primaryAccountNumber.substring(0, 6);
            for (Bin bin: transactionBins) {
                if (bin.getBin().equalsIgnoreCase(binRetrievedFromCard) && bin.isActive()) {
                    return;
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        throw new PosException(PosError.DEVICE_ERROR_EMI_NOT_SUPPORTED_BY_CARD);
    }
}
