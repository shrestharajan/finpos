package global.citytech.finpos.merchant.presentation.model.response

data class Terminal(
    var switchId: String? = null
)