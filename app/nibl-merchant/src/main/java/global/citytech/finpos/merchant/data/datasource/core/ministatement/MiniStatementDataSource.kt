package global.citytech.finpos.merchant.data.datasource.core.ministatement

import global.citytech.finpos.merchant.domain.model.ministatement.MiniStatementResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.ministatement.MiniStatementRequestItem
import io.reactivex.Observable

interface MiniStatementDataSource {
    fun generateMiniStatementRequest(
        configurationItem: ConfigurationItem,
        miniStatementRequestItem: MiniStatementRequestItem
    ) : Observable<MiniStatementResponseEntity>
}