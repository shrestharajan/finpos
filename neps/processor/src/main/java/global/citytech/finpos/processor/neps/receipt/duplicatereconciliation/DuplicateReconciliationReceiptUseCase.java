package global.citytech.finpos.processor.neps.receipt.duplicatereconciliation;

import global.citytech.finpos.processor.neps.reconciliation.NepsReconciliationReceipt;
import global.citytech.finpos.processor.neps.reconciliation.NepsReconciliationReceiptPrintHandler;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.duplicatereconciliation.DuplicateReconciliationReceiptRequester;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.utility.JsonUtils;

/**
 * Created by Saurav Ghimire on 6/16/21.
 * sauravnghimire@gmail.com
 */


public class DuplicateReconciliationReceiptUseCase implements DuplicateReconciliationReceiptRequester<DuplicateReconciliationReceiptRequest,DuplicateReconciliationReceiptResponse>,
        UseCase<DuplicateReconciliationReceiptRequest,DuplicateReconciliationReceiptResponse> {



    @Override
    public DuplicateReconciliationReceiptResponse execute(DuplicateReconciliationReceiptRequest duplicateReconciliationReceiptRequest) {
        String reconciliationReceiptString = duplicateReconciliationReceiptRequest
                .getReconciliationRepository().getReconciliationReceipt();
        if (reconciliationReceiptString.isEmpty())
            throw new PosException(PosError.DEVICE_ERROR_NO_RECENT_RECONCILIATION);
        NepsReconciliationReceipt reconciliationReceipt = JsonUtils
                .fromJsonToObj(reconciliationReceiptString, NepsReconciliationReceipt.class);
        if (reconciliationReceipt == null)
            throw new PosException(PosError.DEVICE_ERROR_NO_RECENT_RECONCILIATION);
        NepsReconciliationReceiptPrintHandler reconciliationReceiptPrintHandler = new
                NepsReconciliationReceiptPrintHandler(duplicateReconciliationReceiptRequest
                .getPrinterService());
        try {
            reconciliationReceiptPrintHandler
                    .printReceipt(reconciliationReceipt, ReceiptVersion.DUPLICATE_COPY);
        } catch (PosException e) {
            throw e;
        } catch (Exception e) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_PRINT);
        }
        return new DuplicateReconciliationReceiptResponse(Result.SUCCESS,
                "Duplicate Settlement Receipt print success");
    }
}
