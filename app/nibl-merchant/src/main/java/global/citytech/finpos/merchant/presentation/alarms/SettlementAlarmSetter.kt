package global.citytech.finpos.merchant.presentation.alarms

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import global.citytech.common.extensions.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.broadcasts.DeviceBootReceiver
import global.citytech.finpos.merchant.framework.broadcasts.SettlementAlarmReceiver
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finposframework.log.Logger
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Unique Shakya on 4/28/2021.
 * Modified by Rishav Chudal on 5/17/2021.
 */
class SettlementAlarmSetter(val context: Context) {

    val logger: Logger = Logger.getLogger(SettlementAlarmSetter::class.java.name)
    private val alarmManager = this.context.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
    private lateinit var pendingIntentBroadcast: PendingIntent
    private lateinit var settlementTime: String
    private val localDataUseCase: LocalDataUseCase =
        LocalDataUseCase(
            LocalRepositoryImpl(
                LocalDatabaseSourceImpl(),
                PreferenceManager
            )
        )

    fun setRepeatingAlarm(settlementTime: String) {
        logger.log("::: SET ALARM FOR TIME ::: ".plus(settlementTime))
        if (!settlementTime.isNullOrEmptyOrBlank()) {
            this.settlementTime = settlementTime
            this.assignPendingIntentBroadCast()
            this.setUpAlarmWithAlarmManager()
            this.enableDeviceBootReceiver()
        }
    }

    private fun assignPendingIntentBroadCast() {
        val intent = Intent(this.context, SettlementAlarmReceiver::class.java)
        intent.action = SettlementAlarmReceiver.ACTION_ALARM_SETTLEMENT
        this.pendingIntentBroadcast = PendingIntent.getBroadcast(
            this.context,
            SettlementAlarmReceiver.REQUEST_CODE_ALARM_SETTLEMENT,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    private fun setUpAlarmWithAlarmManager() {
        setAlarmForGivenTime(alarmManager!!)
    }

    private fun setAlarmForGivenTime(alarmManager: AlarmManager) {
        alarmManager.cancel(this.pendingIntentBroadcast)
        val triggerTime = triggerTimeInMillis()

        alarmManager.setExact(
            AlarmManager.RTC_WAKEUP,
            triggerTime,
           /* AlarmManager.INTERVAL_DAY,*/
            this.pendingIntentBroadcast
        )

        logger.log("::: ALARM SET FOR TIME IN MILLIS ::: ".plus(triggerTime))
        logger.log("::: ALARM SET FOR DATE :::".plus(this.getDate(triggerTime)))
        logger.log("::: LAST SETTLEMENT DATE ::: " + getLastSettlementTime())
    }

    fun rescheduleAlarmForGivenTime(triggerTime: Long) {
        assignPendingIntentBroadCast()
        alarmManager?.setExact(
            AlarmManager.RTC_WAKEUP,
            triggerTime,
            /* AlarmManager.INTERVAL_DAY,*/
            this.pendingIntentBroadcast
        )

        logger.log("::: ALARM SET FOR TIME IN MILLIS ::: ".plus(triggerTime))
        logger.log("::: ALARM SET FOR DATE :::".plus(this.getDate(triggerTime)))
        logger.log("::: LAST SETTLEMENT DATE ::: " + getLastSettlementTime())
    }

    private fun triggerTimeInMillis(): Long {
        val calendar = this.getSettlementTimeCalendar(settlementTime)
        return this.retrieveRepeatingAlarmTimeInMillis(calendar)
    }


    fun getSettlementTimeCalendar(settlementTime: String): Calendar {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        calendar.set(Calendar.HOUR_OF_DAY, this.retrieveAlarmHour(settlementTime))
        calendar.set(Calendar.MINUTE, this.retrieveAlarmMinute(settlementTime))
        calendar.set(Calendar.SECOND, 0)
        return calendar
    }

    private fun retrieveAlarmMinute(settlementTime: String): Int {
        val settlementMinute = if (settlementTime.contains(":"))
            settlementTime.subSequence(3, 5) as String
        else
            settlementTime.subSequence(2, 4) as String
        return settlementMinute.toInt()
    }

    private fun retrieveAlarmHour(settlementTime: String): Int {
        val settlementHour = settlementTime.subSequence(0, 2)
        return (settlementHour as String).toInt()
    }

    private fun retrieveRepeatingAlarmTimeInMillis(calendar: Calendar): Long {
        var effectiveAlarm = calendar.timeInMillis
        if (hasAlarmTimeCrossed(calendar)) {
            if (getLastSettlementTime() >= effectiveAlarm || localDataUseCase.getSettlementTime("").isEmpty()) {
                effectiveAlarm += AlarmManager.INTERVAL_DAY
            }
        }
        return effectiveAlarm
    }

    private fun hasAlarmTimeCrossed(calendar: Calendar): Boolean {
        val calendarTime = this.getDate(calendar.timeInMillis)
        logger.log("::: CALENDAR TIME ::: ".plus(calendarTime))

        val systemTime = this.getDate(System.currentTimeMillis())
        logger.log("::: SYSTEM TIME ::: ".plus(systemTime))
        val formatter = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        return try {
            val calendarDate = formatter.parse(calendarTime)
            val systemDate = formatter.parse(systemTime)
            systemDate!!.time >= calendarDate!!.time
        } catch (e: ParseException) {
            e.printStackTrace()
            true
        }
    }

    private fun getDate(timeInMillis: Long): String {
        val formatter = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = timeInMillis
        return formatter.format(calendar.time)
    }

    private fun getLastSettlementTime(): Long {
        return localDataUseCase.getLastSuccessSettlementTimeInMillis(0)
    }

    private fun enableDeviceBootReceiver() {
        val receiver = ComponentName(context, DeviceBootReceiver::class.java)
        context.packageManager.setComponentEnabledSetting(
            receiver,
            PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
            PackageManager.DONT_KILL_APP
        )
    }
}