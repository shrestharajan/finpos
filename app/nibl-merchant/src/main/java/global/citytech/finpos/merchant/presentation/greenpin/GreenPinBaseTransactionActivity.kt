package global.citytech.finpos.merchant.presentation.greenpin

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bumptech.glide.Glide
import global.citytech.common.Constants
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.presentation.alertdialogs.*
import global.citytech.finpos.merchant.presentation.amount.oldlayout.AmountActivity
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.card.check.CardCheckActivity
import global.citytech.finpos.merchant.utils.*
import global.citytech.finposframework.listeners.CardConfirmationListener
import global.citytech.finposframework.listeners.CardConfirmationListenerForGreenPin
import global.citytech.finposframework.listeners.CashInAmountConfirmationListener
import global.citytech.finposframework.listeners.OtpConfirmationListener
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.notifier.Notifier
import global.citytech.finposframework.usecases.TransactionType
import kotlinx.android.synthetic.main.activity_green_pin.*
import kotlinx.android.synthetic.main.activity_green_pin.btn_cancel
import kotlinx.android.synthetic.main.activity_green_pin.tv_message
import kotlinx.android.synthetic.main.layout_otp_pin_block.*
import kotlinx.android.synthetic.main.layout_otp_pin_block.iv_back
import java.math.BigDecimal

abstract class GreenPinBaseTransactionActivity<T : ViewDataBinding, E : GreenPinBaseTransactionViewModel> :
    AppBaseActivity<T, E>(),
    PosEntrySelectionDialog.Listener {

    private lateinit var cardConfirmationListener: CardConfirmationListener
    private lateinit var cardConfirmationListenerForGreenPin: CardConfirmationListenerForGreenPin
    private lateinit var otpConfirmationListener: OtpConfirmationListener
    private lateinit var cashInConfirmationListener: CashInAmountConfirmationListener
    protected var displayAmount: BigDecimal = BigDecimal.ZERO
    private val logger = Logger(GreenPinActivity::class.java.name)
    private var progressDialog: Dialog? = null
    private var progressView: View? = null
    private var cashInConfirmationDialog: CashInConfirmationAlertDialog? = null
    private var bankLogo: String = ""


    protected open fun initBaseViews() {
        Glide.with(this).load("file:///android_asset/images/loading.gif")
            .into(iv_progress)

        btn_cancel.setOnClickListener {
            btn_cancel.handleDebounce()
            onCancelButtonClicked()
            finish()
        }
        iv_back.setOnClickListener {
            getViewModel().doCleanUp(GreenPinBaseTransactionViewModel.Task.GREEN_PIN)
            finish()

        }

        NiblMerchant.INSTANCE.networkConnectionAvailable.observe(
            this,
            Observer {
                if (it) {
                    hideConnectionLostView()
                    enableButtons()
                } else {
                    showConnectionLostView()
                    disableButtons()
                }
            }
        )
        initBaseLiveDataObservers()
        this.getGreenPinBaseTransactionViewModel().getTerminalConfiguredStatusAndProceed()
        observeTerminalIsConfiguredLiveData()
    }

    private fun enableButtons() {
        //btn_tips_confirm.enableMe()
    }

    private fun disableButtons() {
        //btn_tips_confirm.disableMe()
    }

    private fun showConnectionLostView() {
        //ll_refund_no_connection.visibility = View.VISIBLE
    }

    private fun hideConnectionLostView() {
        //ll_refund_no_connection.visibility = View.GONE
    }


    private fun showCashInTransactionConfirmationDialog() {
        cashInConfirmationDialog!!.show()
    }

    private fun hideCashInTransactionConfirmationDialog() {
        cashInConfirmationDialog?.hide()
    }

    private var broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            logger.log("BROADCAST :::: BROADCAST RECEIVED IN GREENPINBASE TRANSACTION ACTIVITY ::: $intent.name")
            if (intent!!.action == Constants.INTENT_ACTION_NOTIFICATION) {
                handleNotificationBroadcast(intent)

            }
        }
    }

    private fun handleNotificationBroadcast(intent: Intent) {
        val eventType =
            intent.getSerializableExtra(Constants.EXTRA_EVENT_TYPE) as Notifier.EventType
        logger.log("BROADCAST :::: EVENT TYPE IN BROADCAST ::: ${eventType.name}")
        val message = intent.getStringExtra(Constants.EXTRA_NOTIFICATION_MESSAGE)
        logger.log("BROADCAST :::: MESSAGE IN BROADCAST ::: $message")
        when (eventType) {
            Notifier.EventType.STARTING_READ_CARD -> {
                this.hideProgressScreen()
                this.getGreenPinBaseTransactionViewModel()
                    .retrieveAllowedCardTypes(this.getTransactionType())
            }
            Notifier.EventType.READING_CARD -> {
                btn_cancel.handleDebounce()
                this.hidePosEntrySelectionDialog()
                this.showProgressScreen(message!!)
            }

            Notifier.EventType.DETECT_CARD_ERROR -> {
                this.hidePosEntrySelectionDialog()
            }
            Notifier.EventType.CASH_IN_PROCESSING_EVENT -> {
                Log.d("", "CASH PROCESSING EVENT")
                handleCashInProcessingEvent()
            }
            Notifier.EventType.CONNECTING_TO_SWITCH,
            Notifier.EventType.TRANSMITTING_REQUEST,
            Notifier.EventType.RESPONSE_RECEIVED -> {
                hideCashInTransactionConfirmationDialog()
                hideProgressScreen()
            }
            Notifier.EventType.PROCESSING -> {
                this.showProgressScreen(message)
            }
            Notifier.EventType.PROCESSING_ISSUER_SCRIPT,
            Notifier.EventType.DETECTED_MULTIPLE_AID -> {
                this.hidePosEntrySelectionDialog()
                this.showProgressScreen(message!!)
            }
            Notifier.EventType.TRANSACTION_DECLINED -> {
                this.hidePosEntrySelectionDialog()
                this.hideProgressScreen()
            }
            Notifier.EventType.CARD_CONFIRMATION_EVENT -> {
                handleCardConfirmationEvent(intent)
            }
            Notifier.EventType.OTP_CONFIRMATION_EVENT -> {
                handleOtpConfirmationEvent(intent)
            }
            Notifier.EventType.RETURN_TO_DASHBOARD -> {
                returnToDashboard()
            }
            Notifier.EventType.PIN_CHANGE_EVENT -> {
                showToast("Please enter the PIN Number");
            }
            Notifier.EventType.CASH_IN_EVENT -> {
                Log.d("", "CASH IN EVENT")
                handleCashInAmountEvent(intent)
            }
            Notifier.EventType.PIN_UNMATCH -> {
                showProgressScreen(message)
                showToast("Pin doesn't match")
            }
            Notifier.EventType.CHECK_IF_CARD_PRESENT -> {
                checkForCardPresentForPinSet(intent)
            }
            Notifier.EventType.HIDE_PROGRESS_SCREEN -> {
                hideProgressScreen()
            }
        }
    }

    private fun handleCardConfirmationEvent(intent: Intent) {
        cardConfirmationListener =
            intent.getSerializableExtra(Constants.EXTRA_CARD_CONFIRMATION_LISTENER) as CardConfirmationListener
        cardConfirmationListener.onResult(true)
    }

    private fun handleOtpConfirmationEvent(intent: Intent) {
        otpConfirmationListener =
            intent.getSerializableExtra(Constants.EXTRA_OTP_CONFIRMATION_LISTENER) as OtpConfirmationListener
        hideProgressScreen()
        hidePosEntrySelectionDialog()
        showOTPPinBlock()
        val cardDataModel = CardDataModel(
            intent.getStringExtra(Constants.EXTRA_CARD_NUMBER),
            intent.getStringExtra(Constants.EXTRA_CARD_SCHEME),
            intent.getStringExtra(Constants.EXTRA_CARD_HOLDER_NAME),
            intent.getStringExtra(Constants.EXTRA_CARD_EXPIRY)
        )
        setCardDetails(cardDataModel)
    }

    private fun handleCashInAmountEvent(intent: Intent) {
        Log.d("handleCashInAmountEvent", "CASH IN EVENT")
        cashInConfirmationListener =
            intent.getSerializableExtra(Constants.EXTRA_CASH_IN_AMOUNT_LISTENER) as CashInAmountConfirmationListener
        hideProgressScreen()
        hidePosEntrySelectionDialog()
        showCashInAmountUI()
    }

    private fun handleCashInProcessingEvent() {
        Log.d("handleCashInAmountEvent", "CASH IN PROCESSING EVENT")
        cashInConfirmationDialog = getViewModel().bankDisplayImage.value?.let {
            CashInConfirmationAlertDialog(
                this,
                displayAmount.toString(),
                it,
                getViewModel().terminalRepository
            )
        }
        showCashInTransactionConfirmationDialog()


    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)


    }

    private fun observeTerminalIsConfiguredLiveData() {
        getViewModel().terminalIsConfigured.observe(
            this,
            Observer { onObserveTerminalIsConfigured(it) }
        )
    }

    private fun onObserveTerminalIsConfigured(terminalIsConfigured: Boolean) {
        if (terminalIsConfigured) {
            onTerminalIsConfigured()
        }
    }

    private fun onTerminalIsConfigured() {
        this.getGreenPinBaseTransactionViewModel().getConfiguration()
    }

    override fun onDestroy() {
        getViewModel().enableNetworkPing()
        super.onDestroy()
    }


    abstract fun getGreenPinBaseTransactionViewModel(): GreenPinBaseTransactionViewModel


    override fun onResume() {
        super.onResume()
        getViewModel().transactionType = getTransactionType()
        this.registerBroadCastReceivers()
    }

    protected fun initBaseLiveDataObservers() {

        this.getGreenPinBaseTransactionViewModel().posEntrySelection.observe(this, Observer {
            showPosEntrySelectionDialog(it)
        })

        this.getGreenPinBaseTransactionViewModel().isLoading.observe(this, Observer {
            if (it) {
                showProgressScreen(getString(R.string.please_wait))
            } else {
                hideProgressScreen()
            }
        })

        this.getGreenPinBaseTransactionViewModel().message.observe(this, Observer {
            if (cashInConfirmationDialog != null) {
                hideCashInTransactionConfirmationDialog()
            }
            showNormalMessage(it)
        })

        this.getGreenPinBaseTransactionViewModel().successMessage.observe(this, Observer {
            showSuccessMessage(it)
        })

        this.getGreenPinBaseTransactionViewModel().failureMessage.observe(this, Observer {
            showFailureMessage(it)
        })

        this.getGreenPinBaseTransactionViewModel().errorMessage.observe(this, Observer {
            showFailureMessage(it)
        })

        this.getGreenPinBaseTransactionViewModel().cardPresent.observe(this, Observer {
            if (it)
                this.startCardCheckActivity()
            else
                checkTransactionCompletion()
        })

        this.getGreenPinBaseTransactionViewModel().cardPresentForPinSet.observe(this, Observer {
            if (it)
                cardConfirmationListenerForGreenPin.onResult(true)
            else {
                cardConfirmationListenerForGreenPin.onResult(false)
            }

        })
    }


    private fun registerBroadCastReceivers() {
        registerReprintBroadcastReceivers()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                broadcastReceiver,
                IntentFilter(Constants.INTENT_ACTION_NOTIFICATION)
            )
    }

    override fun onPause() {
        this.unregisterBroadCastReceivers()
        super.onPause()
    }

    private fun unregisterBroadCastReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        unregisterReprintBroadCastReceivers()
    }

    private fun showPosEntrySelectionDialog(posEntrySelection: PosEntrySelection) {
        cl_pos_entry.visibility = View.VISIBLE
        Glide.with(this).load(PosEntryMode.ACCEPT_ICC_ONLY.loaderImage).into(image)
    }

    protected fun showProgressScreen(message: String) {
        ll_progress.visibility = View.VISIBLE
        tv_message.text = message
    }

    protected fun hideProgressScreen() {
        ll_progress.visibility = View.GONE
    }

    protected fun hidePosEntrySelectionDialog() {
        cl_pos_entry.visibility = View.GONE
    }

    protected fun showOTPPinBlock() {
        layout_otp_pin_block.visibility = View.VISIBLE
    }

    protected fun hidePinBlock(layout_pin_block: View?) {
        layout_pin_block?.visibility = View.GONE
    }

    protected fun showCashInAmountUI() {
        val intent = Intent(this, AmountActivity::class.java)
        startActivityForResult(intent, AmountActivity.REQUEST_CODE)
    }


    private fun showCustomerCopyResponseDialog(it: String?) {
        val messageConfig = MessageConfig.Builder()
            .message("CUSTOMER COPY")
            .message(it).positiveLabel(getString(R.string.title_ok))
            .onPositiveClick {
                checkForCardPresent()
            }
        showNormalMessage(messageConfig)
    }

    protected fun showSuccessMessage(it: String?) {
        if (!it.isNullOrEmpty()) {
            val messageConfig = MessageConfig.Builder()
                .message(it)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    checkForCardPresent()
                }
            showGreenPinSuccessMessage(messageConfig)
        }
    }

    protected fun showNormalMessage(it: String?) {
        if (!it.isNullOrEmpty()) {
            val messageConfig = MessageConfig.Builder()
                .message(it)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    checkForCardPresent()
                }
            showNormalMessage(messageConfig)
        }
    }

    protected fun showUnableToReadMessage(it: String?) {
        if (!it.isNullOrEmpty()) {
            val messageConfig = MessageConfig.Builder()
                .message(it)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    returnToDashboard()
                }
            showNormalMessage(messageConfig)
        }
    }

    protected fun showFailureMessage(it: String?) {
        if (!it.isNullOrEmpty()) {
            val messageConfig = MessageConfig.Builder()
                .message(it)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    checkForCardPresent()
                }
            showGreenPinFailureMessage(messageConfig)
        }
    }

    protected fun checkForCardPresent() {
        showProgressScreen(" ")
        this.getGreenPinBaseTransactionViewModel().checkForCardPresent()
    }

    protected fun checkForCardPresentForPinSet(intent: Intent) {
        cardConfirmationListenerForGreenPin =
            intent.getSerializableExtra(Constants.EXTRA_CARD_CONFIRMATION_LISTENER) as CardConfirmationListenerForGreenPin
        this.getGreenPinBaseTransactionViewModel().checkForCardPresentForPinSet()
    }

    protected fun startCardCheckActivity() {
        val intent = Intent(this, CardCheckActivity::class.java)
        startActivityForResult(intent, CardCheckActivity.REQUEST_CODE)
    }

    protected fun returnToDashboard() {
        hidePosEntrySelectionDialog()
        hidePinBlock(layout_otp_pin_block)
        hideProgressScreen()
        finish()

    }

    override fun onBackPressed() {
        if (cl_pos_entry.visibility == View.VISIBLE) {
            return
        }
        if (ll_progress.visibility != View.VISIBLE) {
            returnToDashboard()
        }
    }

    fun checkTransactionCompletion() {
        this.returnToDashboard()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                CardCheckActivity.REQUEST_CODE -> {
                    checkTransactionCompletion()
                }
            }
        } else if (resultCode == RESULT_CANCELED) {
            finish()
        }
    }

    private fun getProgressMessage() = tv_message.text.toString()
    abstract fun onAmountActivityResult(data: Intent?)
    abstract fun onManualActivityResult(data: Intent?)
    abstract fun getTransactionType(): TransactionType

    private fun setCardDetails(cardDataModel: CardDataModel) {
        tv_card_number.text = cardDataModel.CardNumber
        tv_card_holder_name.text = cardDataModel.HolderName
        tv_scheme.text = cardDataModel.CardScheme
        tv_expiry_date.text = cardDataModel.Expiry


    }

    fun submitOtp(otp: String?) {
        hidePinBlock(layout_otp_pin_block)
        hidePinBlock(layout_otp_pin_block)
        showProgressScreen("")
        otpConfirmationListener.onResult(true, otp)
    }
}

