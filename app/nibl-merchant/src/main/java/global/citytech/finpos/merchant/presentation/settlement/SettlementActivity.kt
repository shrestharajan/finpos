package global.citytech.finpos.merchant.presentation.settlement

import android.app.Activity
import android.app.AlarmManager
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import global.citytech.common.Constants
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivitySettlementBinding
import global.citytech.finpos.merchant.presentation.autoreversal.AutoReversalActivity
import global.citytech.finpos.merchant.utils.*
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.listeners.SettlementConfirmationListener
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.notifier.Notifier

/**
 * Created by Unique Shakya on 5/31/2021.
 */
class SettlementActivity : AppBaseActivity<ActivitySettlementBinding, SettlementViewModel>(),
    SettlementConfirmationDialog.Listener {

    private var isManualSettlement: Boolean = true
    private var isSettlementFromCommand: Boolean = false
    private var justUpdateButtonLabelOfSuccessDialog: Boolean = false
    private var isSettlementSuccessDialogRequired = true
    private var settlementConfirmationListener: SettlementConfirmationListener? = null
    private lateinit var settlementConfirmationDialog: SettlementConfirmationDialog

    companion object {
        const val REQUEST_CODE = 7388
        const val BUNDLE_DATA_IS_MANUAL_SETTLEMENT = "is manual settlement"
        const val BUNDLE_DATA_IS_SETTLEMENT_FROM_COMMAND = "is settlement from command"
        const val BUNDLE_DATA_IS_SETTLEMENT_SUCCESS_DIALOG_REQUIRED =
            "is settlement success dialog required"
        var canLaunchSettlement = true

        fun getLaunchIntent(
            context: Context,
            isManualSettlement: Boolean? = true,
            isSettlementSuccessConfirmationRequired: Boolean? = true,
            isSettlementFromCommand: Boolean? = false
        ) {

            Log.d("", "isManualSettlement ===> ".plus(isManualSettlement))
            Log.d(
                "",
                "isSettlementSuccessConfirmationRequired ===> ".plus(
                    isSettlementSuccessConfirmationRequired
                )
            )
            Log.d("", "isSettlementFromCommand ===> ".plus(isSettlementFromCommand))

            if (isManualSettlement!! || canLaunchSettlementActivity()) {
                val intent = Intent(context, SettlementActivity::class.java)
                intent.putExtra(BUNDLE_DATA_IS_MANUAL_SETTLEMENT, isManualSettlement)
                intent.putExtra(
                    BUNDLE_DATA_IS_SETTLEMENT_SUCCESS_DIALOG_REQUIRED,
                    isSettlementSuccessConfirmationRequired
                )
                intent.putExtra(
                    BUNDLE_DATA_IS_SETTLEMENT_FROM_COMMAND,
                    isSettlementFromCommand
                )
                (context as Activity).startActivityForResult(intent, REQUEST_CODE)
            }
        }

        private fun canLaunchSettlementActivity(): Boolean {
            if (canLaunchSettlement) {
                canLaunchSettlement = false
                Handler().postDelayed({ canLaunchSettlement = true }, 60000)
                return true
            }
            return false
        }
    }

    private var progressDialog: ProgressDialog? = null
    private val logger = Logger.getLogger(SettlementActivity::class.java.name)

    private var broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            logger.log("BroadCast ::: SettlementActivity ::: ".plus(intent))
            when {
                intent!!.action == Constants.INTENT_ACTION_REPRINT_UI -> {
                    handleReprintReceiptBroadcast(intent)
                    justUpdateButtonLabelOfSuccessDialog = false
                }
                intent.action == Constants.INTENT_ACTION_NOTIFICATION -> {
                    handleNotificationBroadcast(intent)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObservers()
        getDataFromBundle()
        getViewModel().checkForAutoReversal()
    }

    private fun getDataFromBundle() {
        isManualSettlement = intent.getBooleanExtra(
            BUNDLE_DATA_IS_MANUAL_SETTLEMENT, true
        )
        isSettlementSuccessDialogRequired = intent.getBooleanExtra(
            BUNDLE_DATA_IS_SETTLEMENT_SUCCESS_DIALOG_REQUIRED, true
        )
        isSettlementFromCommand = intent.getBooleanExtra(
            BUNDLE_DATA_IS_SETTLEMENT_FROM_COMMAND, false
        )
    }

    override fun onResume() {
        super.onResume()
        registerBroadcastReceivers()
        NiblMerchant.INSTANCE.disableNetworkPing = true
    }

    override fun onPause() {
        unregisterBroadCastReceivers()
        NiblMerchant.INSTANCE.disableNetworkPing = false
        super.onPause()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AutoReversalActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK)
                getViewModel().getConfigurationItem()
            else {
                canLaunchSettlement = true
                getViewModel().rescheduleForceSettlement(getString(R.string.error_msg_auto_reversal_not_performed));
                getViewModel().message.value =
                    getString(R.string.error_msg_auto_reversal_not_performed)
            }
        }
    }

    private fun handleNotificationBroadcast(intent: Intent) {
        val eventType =
            intent.getSerializableExtra(Constants.EXTRA_EVENT_TYPE) as Notifier.EventType
        logger.log("BROADCAST :::: EVENT TYPE IN BROADCAST ::: $eventType.name")
        val message = intent.getStringExtra(Constants.EXTRA_NOTIFICATION_MESSAGE)
        logger.log("BROADCAST :::: MESSAGE IN BROADCAST ::: $message")
        when (eventType) {
            Notifier.EventType.CONNECTING_TO_SWITCH,
            Notifier.EventType.TRANSMITTING_REQUEST,
            Notifier.EventType.RESPONSE_RECEIVED,
            Notifier.EventType.PREPARING_RECONCILIATION_TOTALS,
            Notifier.EventType.PREPARING_DETAIL_REPORT,
            Notifier.EventType.BATCH_UPLOADING,
            Notifier.EventType.BATCH_UPLOAD_PROGRESS,
            Notifier.EventType.PRINTING_DETAIL_REPORT,
            Notifier.EventType.PRINTING_SETTLEMENT_REPORT -> {
                this.showProgressScreen(message!!)
            }
            Notifier.EventType.SETTLEMENT_SUCCESS_EVENT -> {
                this.showSuccessMessage(Notifier.EventType.SETTLEMENT_SUCCESS_EVENT.description, "")
            }
            Notifier.EventType.SETTLEMENT_CONFIRMATION_EVENT -> {
                handleSettlementConfirmationEvent(intent)
            }
            else -> this.hideProgressScreen()
        }
    }

    private fun handleSettlementConfirmationEvent(intent: Intent) {
        settlementConfirmationListener =
            intent.getSerializableExtra(Constants.EXTRA_SETTLEMENT_CONFIRMATION_LISTENER) as SettlementConfirmationListener
        if (isManualSettlement) {
            try {
                val jsonData =
                    intent.getStringExtra(Constants.EXTRA_SETTLEMENT_CONFIRMATION_JSON_DATA)
                val settlementConfirmationTotals =
                    Jsons.fromJsonToObj(jsonData, SettlementConfirmationTotals::class.java)
                showSettlementConfirmationDialog(settlementConfirmationTotals)
            } catch (e: Exception) {
                logger.log("::: EXCEPTION WHILE PARSING SETTLEMENT CONFIRMATION JSON ::: ${e.message}")
                settlementConfirmationListener!!.onResult(true)
            }
        } else {
            getViewModel().setSettlementIsActive()
            settlementConfirmationListener!!.onResult(true)
        }
    }

    private fun showSettlementConfirmationDialog(settlementConfirmationTotals: SettlementConfirmationTotals) {
        settlementConfirmationDialog =
            SettlementConfirmationDialog.newInstance(settlementConfirmationTotals)
        settlementConfirmationDialog.isCancelable = false
        settlementConfirmationDialog.show(
            supportFragmentManager,
            SettlementConfirmationDialog.TAG
        )
    }

    private fun hideSettlementConfirmationDialog() {
        if (settlementConfirmationDialog != null && settlementConfirmationDialog.isVisible)
            settlementConfirmationDialog.dismiss()
    }

    private fun showProgressScreen(message: String) {
        showProgress(message)
    }

    private fun hideProgressScreen() {
        hideProgress()
    }

    private fun registerBroadcastReceivers() {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, IntentFilter(Constants.INTENT_ACTION_REPRINT_UI))
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, IntentFilter(Constants.INTENT_ACTION_NOTIFICATION))
    }

    private fun unregisterBroadCastReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }

    private fun initObservers() {
        getViewModel().configurationItem.observe(this, Observer {
            getViewModel().reconciliation(it)
        })

        getViewModel().message.observe(this, Observer {
            showNormalMessage(it)
        })

        getViewModel().errorMessage.observe(this, Observer {
            canLaunchSettlement = true
            if (PosError.DEVICE_ERROR_USER_CANCELLED.errorMessage == it) {
                finish()
            } else {
                showFailureMessage(it)
            }
        })
        getViewModel().successMessage.observe(this, Observer {
            logger.log("IS SETTLEMENT SUCCESS DIALOG REQUIRED ::: $isSettlementSuccessDialogRequired")
            if (isSettlementSuccessDialogRequired) {
                if (justUpdateButtonLabelOfSuccessDialog) {
                    updateButtonLabel(getString(R.string.title_ok))
                } else {
                    showSuccessMessage(it, getString(R.string.title_ok))
                }
            }

        })

        getViewModel().settlementSuccess.observe(this, Observer {
            canLaunchSettlement = true
            if (isSettlementFromCommand) {
                if (it) {
                    getViewModel().cancelRescheduledSettlement()
                }
                val intent = Intent()
                intent.putExtra("SETTLEMENT_RESULT", it)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }

            if (it) {
                getViewModel().cancelRescheduledSettlement()
                getViewModel().sendAppInstallBroadCastToPlatform(this, "SUCCESS")
                setResult(Activity.RESULT_OK)
                logger.log("IS SETTLEMENT SUCCESS DIALOG REQUIRED ::: $isSettlementSuccessDialogRequired")
                if (!isSettlementSuccessDialogRequired) {
                    logger.log("::: FINISHING ACTIVITY AS IS SETTLEMENT SUCCESS DIALOG REQUIRED IS FALSE")
                    finish()
                }
            } else {
                checkAutoSettlementAndRescheduleAlarmForNextDay()
                getViewModel().sendAppInstallBroadCastToPlatform(this, "FAILED")
                setResult(Activity.RESULT_CANCELED)
            }
        })

        getViewModel().autoReversalPresent.observe(this, Observer {
            if (it)
                startAutoReversalActivity()
            else
                getViewModel().getConfigurationItem()
        })

        getViewModel().isLoading.observe(this, Observer {
            if (it) {
                showProgress()
            } else {
                hideProgress()
            }
        })
    }

    private fun checkAutoSettlementAndRescheduleAlarmForNextDay(){
        if(!isManualSettlement){
            NiblMerchant.INSTANCE.cancelRescheduledSettlement()
            getViewModel().rescheduleSettlementTimeAlarm()
        }
    }

    protected fun showSuccessMessage(it: String?, buttonLabel: String) {
        if (!it.isNullOrEmpty()) {
            justUpdateButtonLabelOfSuccessDialog = true
            val messageConfig = MessageConfig.Builder()
                .message(it)
                .positiveLabel(buttonLabel)
                .onPositiveClick {
                    finish()
                }
            showSuccessMessage(messageConfig)
        }
    }

    protected fun showNormalMessage(it: String?) {
        if (!it.isNullOrEmpty()) {
            val messageConfig = MessageConfig.Builder()
                .message(it)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    finish()
                }
            showNormalMessage(messageConfig)
        }
    }


    protected fun showFailureMessage(it: String?) {
        if (!it.isNullOrEmpty()) {
            val messageConfig = MessageConfig.Builder()
                .message(it)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    finish()
                }
            showFailureMessage(messageConfig)
        }
    }

    private fun startAutoReversalActivity() {
        val intent = Intent(this, AutoReversalActivity::class.java)
        startActivityForResult(intent, AutoReversalActivity.REQUEST_CODE)
    }

    override fun getBindingVariable(): Int = BR.settlementViewModel

    override fun getLayout(): Int = R.layout.activity_settlement

    override fun getViewModel(): SettlementViewModel =
        ViewModelProviders.of(this)[SettlementViewModel::class.java]

    override fun onConfirmButtonClicked() {
        hideSettlementConfirmationDialog()
        getViewModel().setSettlementIsActive()
        settlementConfirmationListener?.onResult(true)
    }

    override fun onCancelButtonClicked() {
        hideSettlementConfirmationDialog()
        settlementConfirmationListener?.onResult(false)
    }
}