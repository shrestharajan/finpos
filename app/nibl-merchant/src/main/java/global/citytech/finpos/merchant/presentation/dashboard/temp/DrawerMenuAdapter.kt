package global.citytech.finpos.merchant.presentation.dashboard.temp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import global.citytech.finpos.merchant.R
import kotlinx.android.synthetic.main.item_drawer_menu.view.*

/**
 * Created by Unique Shakya on 6/18/2021.
 */
class DrawerMenuAdapter(
    private val context: Context,
    private val drawerMenuItems: MutableList<DrawerMenuItem>) : RecyclerView.Adapter<DrawerMenuAdapter.DrawerMenuViewHolder>() {

    class DrawerMenuViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrawerMenuViewHolder =
        DrawerMenuViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_drawer_menu, parent, false)
        )

    override fun getItemCount(): Int = drawerMenuItems.size

    override fun onBindViewHolder(holder: DrawerMenuViewHolder, position: Int) {
        val drawerMenuItem = drawerMenuItems[position]
        holder.itemView.btn_menu.setIconResource(drawerMenuItem.iconId)
        holder.itemView.btn_menu.text = drawerMenuItem.title
        holder.itemView.btn_menu.setOnClickListener {
            (context as DashActivity).handleDrawerMenuItemClicked(drawerMenuItem)
        }
        (context as DashActivity).getViewModel().isLoading.observe(context, Observer {
            if(it){
                holder.itemView.btn_menu.isEnabled = false
                holder.itemView.btn_menu.setTextColor(context.resources.getColor(R.color.text_light_grey))
            } else {
                holder.itemView.btn_menu.isEnabled = true
                holder.itemView.btn_menu.setTextColor(context.resources.getColor(R.color.black))
            }
        })
    }

    interface Listener {
        fun onDrawerMenuItemClicked(drawerMenuItem: DrawerMenuItem)
    }
}