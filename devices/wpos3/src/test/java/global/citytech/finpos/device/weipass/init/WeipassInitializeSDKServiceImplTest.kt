package global.citytech.finpos.device.weipass.init

import android.content.Context
import com.wiseasy.emvprocess.LibInit
import com.wiseasy.emvprocess.SDKInstance
import global.citytech.finpos.device.weipass.utils.WeipassLogger
import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.Result
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import wangpos.sdk4.libbasebinder.BankCard

/**
 * Created by Rishav Chudal on 4/20/20.
 */
class WeipassInitializeSDKServiceImplTest {

    @MockK
    private lateinit var context: Context

    private lateinit var SUT: WeipassInitializeSDKServiceImpl
    private lateinit var deviceResponse: DeviceResponse

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        mockWeipassLogger()
        mockSDKInstance()
        mockLibInit()
        SUT = WeipassInitializeSDKServiceImpl(context)
    }

    /*
     * Mocking the CoreLogger class. Since its a class using the companion object, we
     * used "mockkObject" for the class as well as companion. And for every log function we just
     *  mocked its execution using "every { //fun } just Runs"
     */
    private fun mockWeipassLogger() {
        mockkObject(WeipassLogger)
        mockkObject(WeipassLogger.Companion)
        every { WeipassLogger.log(any()) } just Runs
    }

    /*
     * Mocking the SDKInstance class. Since its a class using static functions, we used
     * "mockkStatic" to mock the class and for the initSdk function, we just mocked its execution
     * using "every { //fun } justRuns"
     */
    private fun mockSDKInstance() {
        mockkStatic(SDKInstance::class)
        every { SDKInstance.initSDK(context.applicationContext) } just Runs
    }

    /*
     * Mocking the LibInit class. Since its a class using static functions, we used
     * "mockkStatic" to mock the class and for the init function, we just mocked its execution
     * using "every { //fun } returns 0". So whenever it runs, we will get return value as 0
     */
    private fun mockLibInit() {
        mockkStatic(LibInit::class)
        every { LibInit.init(true) } returns 0
    }

    @Test
    fun initializeSdk_OnBankCardClassNotNull_ReturnsSuccessResponse() {
        //Setup
        val mockedBankCard = mockk<BankCard>()
        SDKInstance.mBankCard = mockedBankCard

        //Run
        this.deviceResponse = SUT.initializeSDK()

        //Verify
        assertEquals(Result.SUCCESS, this.deviceResponse.result)
    }

    @Test
    fun initializeSdk_OnBankCardClassNull_ReturnsFailureResponse() {
        //Setup
        SDKInstance.mBankCard == null

        //Run
        this.deviceResponse = SUT.initializeSDK()

        //Verify
        assertEquals(Result.FAILURE, this.deviceResponse.result)
    }
}