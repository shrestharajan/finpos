package global.citytech.finposframework.repositories;


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/11/08 - 11:57 AM
 */

import java.util.List;

import global.citytech.finposframework.usecases.activity.ActivityLog;

public interface ActivityLogRepository {

    void updateActivityLog(ActivityLog activityLog);

}
