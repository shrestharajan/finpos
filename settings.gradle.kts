
include(":app")
include(":app:nibl-merchant")  //TODO need to refactor the module name and package name
include(":app:vendor")
include(":app:serviceapp")
include(":thirdparty:crypto")
include(":thirdparty:paymentsdk")
include(":thirdparty")
include(":common")
include(":devices")
include(":devices:wpos3")
include(":devices:wpos3-app")
include(":finposframework")
include(":finposframework:core")
include(":nibl:s2m")
include(":nibl:processor")
include(":neps")
include(":neps:smartvista")
include(":neps:processor")
include(":devices:feitian")
//Note: TODO Temporarily deleted until final testing is completed for merged application (admin-merchant-app)
//include ':app:nibl-admin'
//include ':app:admin'
//include ':app:merchant'
//include("finposframework:devices")
//include(":finposframework:switches")
//include(":switches")

