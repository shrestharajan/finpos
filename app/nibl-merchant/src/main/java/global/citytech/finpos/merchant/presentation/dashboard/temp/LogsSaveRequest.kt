package global.citytech.finpos.merchant.presentation.dashboard.temp

data class LogsSaveRequest (
    val serialNumber: String,
    val etag: String,
    val objectId: String,
    val storagePath: String,
    val uploadDate: String,
    val expires: Boolean,
    val fileName: String,
    val logDate: String
    )