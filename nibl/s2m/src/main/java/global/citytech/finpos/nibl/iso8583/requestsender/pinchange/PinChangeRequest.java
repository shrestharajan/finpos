package global.citytech.finpos.nibl.iso8583.requestsender.pinchange;


import java.math.BigDecimal;

import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.greenpin.PinSetRequest;
import global.citytech.finposframework.switches.PosEntryMode;

public class PinChangeRequest extends TransactionIsoRequest {
    private String pan;
    private BigDecimal transactionAmount = BigDecimal.ZERO;
    private String localTime;
    private String localDate;
    private String expireDate;
    private PosEntryMode posEntryMode;
    private String posConditionCode;
    private String track2Data;
    private String additionalData;
    private String currencyCode;
    private String pinBlock;
    private String emvData;
    private String retrievalReferenceNumber;
    private String cardSequenceNumber;


    public String getPan() {
        return pan;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }


    public String getLocalTime() {
        return localTime;
    }

    public String getLocalDate() {
        return localDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public String getTrack2Data() {
        return track2Data;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getPinBlock() {
        return pinBlock;
    }

    public String getEmvData() {
        return emvData;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }
    public String getCardSequenceNumber() {
        return cardSequenceNumber;
    }

    public static final class Builder {
        private String pan;
        private BigDecimal transactionAmount = BigDecimal.ZERO;
        private String localTime;
        private String localDate;
        private String expireDate;
        private PosEntryMode posEntryMode;
        private String posConditionCode;
        private String track2Data;
        private String additionalData;
        private String currencyCode;
        private String pinBlock;
        private String emvData;
        private String retrievalReferenceNumber;
        private String cardSequenceNumber;

        private Builder() {
        }

        public static PinChangeRequest.Builder newInstance() {
            return new PinChangeRequest.Builder();
        }


        public PinChangeRequest.Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public PinChangeRequest.Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public PinChangeRequest.Builder localTime(String localTime) {
            this.localTime = localTime;
            return this;
        }

        public PinChangeRequest.Builder localDate(String localDate) {
            this.localDate = localDate;
            return this;
        }

        public PinChangeRequest.Builder expireDate(String expiryDate) {
            this.expireDate = expiryDate;
            return this;
        }


        public PinChangeRequest.Builder posEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public PinChangeRequest.Builder posConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public PinChangeRequest.Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public PinChangeRequest.Builder additionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public PinChangeRequest.Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public PinChangeRequest.Builder pinBlock(String pinBlock) {
            this.pinBlock = pinBlock;
            return this;
        }


        public PinChangeRequest.Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public PinChangeRequest.Builder rrn(String rrn) {
            this.retrievalReferenceNumber = rrn;
            return this;
        }
        public PinChangeRequest.Builder cardSequenceNumber(String cardSequenceNumber) {
            this.cardSequenceNumber = cardSequenceNumber;
            return this;
        }


        public PinChangeRequest build() {
            PinChangeRequest PinChangeRequest = new PinChangeRequest();
            PinChangeRequest.pan = this.pan;
            PinChangeRequest.transactionAmount = this.transactionAmount;
            PinChangeRequest.localDate = this.localDate;
            PinChangeRequest.localTime = this.localTime;
            PinChangeRequest.expireDate = this.expireDate;
            PinChangeRequest.posEntryMode = this.posEntryMode;
            PinChangeRequest.posConditionCode = this.posConditionCode;
            PinChangeRequest.track2Data = this.track2Data;
            PinChangeRequest.additionalData = this.additionalData;
            PinChangeRequest.currencyCode = this.currencyCode;
            PinChangeRequest.pinBlock = this.pinBlock;
            PinChangeRequest.emvData = this.emvData;
            PinChangeRequest.retrievalReferenceNumber = this.retrievalReferenceNumber;
            PinChangeRequest.cardSequenceNumber = this.cardSequenceNumber;
            return PinChangeRequest;
        }
    }
}
