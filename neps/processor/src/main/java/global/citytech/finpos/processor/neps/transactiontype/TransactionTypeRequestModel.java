package global.citytech.finpos.processor.neps.transactiontype;

import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.transactiontype.TransactionTypeRequestParameter;

/**
 * Created by Unique Shakya on 1/20/2021.
 */
public class TransactionTypeRequestModel implements TransactionTypeRequestParameter, UseCase.Request {
}
