package global.citytech.finpos.processor.nibl.transaction.purchase;

import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;

public class PurchaseResponseModel extends TransactionResponseModel {

    private String debugRequestMessage;
    private String debugResponseMessage;
    private String message;
    private String stan;
    private boolean isApproved;
    private boolean shouldPrintCustomerCopy;

    public PurchaseResponseModel(PurchaseResponseModel.Builder builder) {
        this.debugRequestMessage = builder.debugRequestMessage;
        this.debugResponseMessage = builder.debugResponseMessage;
        this.isApproved = builder.isApproved;
        this.stan = builder.stan;
        this.shouldPrintCustomerCopy = builder.shouldPrintCustomerCopy;
        this.message = builder.message;
    }

    public String getStan() {
        return stan;
    }


    public String getMessage() {
        return message;
    }

    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public boolean shouldPrintCustomerCopy() {
        return shouldPrintCustomerCopy;
    }

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private String message;
        private String stan;
        private boolean isApproved;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static PurchaseResponseModel.Builder createDefaultBuilder() {
            return new PurchaseResponseModel.Builder();
        }

        public PurchaseResponseModel.Builder debugRequestMessage(String debugRequestString) {
            this.debugRequestMessage = debugRequestString;
            return this;
        }

        public PurchaseResponseModel.Builder debugResponseMessage(String debugResponseString) {
            this.debugResponseMessage = debugResponseString;
            return this;
        }

        public PurchaseResponseModel.Builder message(String message) {
            this.message = message;
            return this;
        }

        public PurchaseResponseModel.Builder stan(String stan) {
            this.stan = stan;
            return this;
        }

        public PurchaseResponseModel.Builder isApproved(boolean isApproved) {
            this.isApproved = isApproved;
            return this;
        }

        public PurchaseResponseModel.Builder shouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public PurchaseResponseModel build() {
            return new PurchaseResponseModel(this);
        }
    }
}
