package global.citytech.finpos.merchant.data.datasource.core.refund

import global.citytech.finpos.merchant.domain.model.purchase.RefundResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.refund.ManualRefundRequestItem
import global.citytech.finpos.merchant.presentation.model.refund.RefundRequestItem
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/28/2020.
 */
interface RefundDataSource {
    fun refund(
        configurationItem: ConfigurationItem,
        refundRequestItem: RefundRequestItem
    ): Observable<RefundResponseEntity>

    fun manualRefund(
        configurationItem: ConfigurationItem,
        manualRefundRequestItem: ManualRefundRequestItem
    ): Observable<RefundResponseEntity>
}