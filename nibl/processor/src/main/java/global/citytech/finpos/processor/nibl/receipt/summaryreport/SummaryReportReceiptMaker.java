package global.citytech.finpos.processor.nibl.receipt.summaryreport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.switches.posmode.PosMode;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;
import global.citytech.finposframework.utility.DateUtils;

/**
 * Created by Unique Shakya on 1/5/2021.
 * Modified by Rishav Chudal on 5/6/2021
 */
public class SummaryReportReceiptMaker {

    private TerminalRepository terminalRepository;
    private ReconciliationRepository reconciliationRepository;

    public SummaryReportReceiptMaker(TerminalRepository terminalRepository,
                                     ReconciliationRepository reconciliationRepository) {
        this.terminalRepository = terminalRepository;
        this.reconciliationRepository = reconciliationRepository;
    }

    public SummaryReportReceipt prepare(String batchNumber, List<CardSchemeType> cardSchemeTypes, PosMode posMode) {
        return SummaryReportReceipt.Builder.newInstance()
                .withRetailer(this.prepareRetailerInfo(this.terminalRepository.findTerminalInfo()))
                .withHost("000048") // TODO change after confirmation from ishang
                .withPerformance(this.preparePerformanceInfo())
                .withReconciliationBatchNumber(batchNumber)
                .withSummaryReport(this.prepareSummaryReport(batchNumber, cardSchemeTypes))
                .withPosMode(posMode)
                .build();
    }

    private SummaryReport prepareSummaryReport(String batchNumber, List<CardSchemeType> cardSchemeTypes) {
        Map<CardSchemeType, CardSchemeSummary> cardSchemeSummaryMap = new HashMap<>();
        for (CardSchemeType cardSchemeType : cardSchemeTypes) {
            addCardSchemeOnlyIfTransactionWasPerformed(
                    cardSchemeSummaryMap,
                    batchNumber,
                    cardSchemeType
            );
        }
        return new SummaryReport(this.terminalRepository.getTerminalTransactionCurrencyName(), cardSchemeSummaryMap);
    }

    private void addCardSchemeOnlyIfTransactionWasPerformed(
            Map<CardSchemeType, CardSchemeSummary> cardSchemeSummaryMap,
            String batchNumber,
            CardSchemeType cardSchemeType
    ) {
        CardSchemeSummary cardSchemeSummary = prepareSummaryWithRespectToCardScheme(
                batchNumber, cardSchemeType
        );
        checkTotalCountForTheCardSchemeAndProceed(cardSchemeType, cardSchemeSummary, cardSchemeSummaryMap);
    }

    private void checkTotalCountForTheCardSchemeAndProceed(
            CardSchemeType cardSchemeType,
            CardSchemeSummary cardSchemeSummary,
            Map<CardSchemeType, CardSchemeSummary> cardSchemeSummaryMap
    ) {
        if (cardSchemeSummary.getTotalCount() > 0) {
            cardSchemeSummaryMap.put(cardSchemeType, cardSchemeSummary);
        }
    }

    private CardSchemeSummary prepareSummaryWithRespectToCardScheme(String batchNumber, CardSchemeType cardSchemeType) {
        return CardSchemeSummary.Builder.newInstance()
                .withRefundAmount(this.prepareRefundAmount(batchNumber, cardSchemeType))
                .withRefundCount(this.prepareRefundCount(batchNumber, cardSchemeType))
                .withSalesAmount(this.prepareSalesAmount(batchNumber, cardSchemeType))
                .withSalesCount(this.prepareSalesCount(batchNumber, cardSchemeType))
                .withVoidAmount(this.prepareVoidAmount(batchNumber, cardSchemeType))
                .withVoidCount(this.prepareVoidCount(batchNumber, cardSchemeType))
                .build();
    }

    private long prepareVoidAmount(String batchNumber, CardSchemeType cardSchemeType) {
        return this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardSchemeType,
                batchNumber, TransactionType.VOID, TransactionType.PURCHASE) +
                this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardSchemeType,
                        batchNumber, TransactionType.VOID, TransactionType.CASH_ADVANCE);
    }

    private long prepareVoidCount(String batchNumber, CardSchemeType cardSchemeType) {
        return this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardSchemeType,
                batchNumber, TransactionType.VOID);
    }

    private long prepareRefundAmount(String batchNumber, CardSchemeType cardScheme) {
        return this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme,
                batchNumber, TransactionType.REFUND) -
                this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardScheme,
                        batchNumber, TransactionType.VOID, TransactionType.REFUND);
    }

    private long prepareRefundCount(String batchNumber, CardSchemeType cardScheme) {
        return this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardScheme, batchNumber, TransactionType.REFUND) -
                this.reconciliationRepository.getCountByOriginalTransactionTypeWithCardScheme(cardScheme,
                        batchNumber, TransactionType.VOID, TransactionType.REFUND);
    }

    private long prepareSalesAmount(String batchNumber, CardSchemeType cardScheme) {
        return this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme, batchNumber, TransactionType.PURCHASE) +
                this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme, batchNumber, TransactionType.CASH_ADVANCE) +
                this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme, batchNumber, TransactionType.TIP_ADJUSTMENT) +
                this.reconciliationRepository.getTotalAmountByTransactionTypeWithCardScheme(cardScheme, batchNumber, TransactionType.AUTH_COMPLETION) -
                this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardScheme, batchNumber, TransactionType.VOID, TransactionType.PURCHASE) -
                this.reconciliationRepository.getTotalAmountByOriginalTransactionTypeWithCardScheme(cardScheme, batchNumber, TransactionType.VOID, TransactionType.CASH_ADVANCE);
    }

    private long prepareSalesCount(String batchNumber, CardSchemeType cardScheme) {
        return this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardScheme, batchNumber, TransactionType.PURCHASE) +
                this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardScheme, batchNumber, TransactionType.CASH_ADVANCE) +
                this.reconciliationRepository.getCountByTransactionTypeWithCardScheme(cardScheme, batchNumber, TransactionType.AUTH_COMPLETION) -
                this.reconciliationRepository.getCountByOriginalTransactionTypeWithCardScheme(cardScheme, batchNumber, TransactionType.VOID, TransactionType.PURCHASE) -
                this.reconciliationRepository.getCountByOriginalTransactionTypeWithCardScheme(cardScheme, batchNumber, TransactionType.VOID, TransactionType.CASH_ADVANCE);
    }

    private Performance preparePerformanceInfo() {
        return Performance.Builder.defaultBuilder()
                .withEndDateTime(DateUtils.yyMMddHHmmssSSSDate())
                .withStartDateTime(DateUtils.yyMMddHHmmssSSSDate())
                .build();
    }

    private Retailer prepareRetailerInfo(TerminalInfo terminalInfo) {
        return Retailer.Builder.defaultBuilder()
                .withRetailerLogo(terminalInfo.getMerchantPrintLogo())
                .withRetailerName(terminalInfo.getMerchantName())
                .withRetailerAddress(terminalInfo.getMerchantAddress())
                .withMerchantId(terminalInfo.getMerchantID())
                .withTerminalId(terminalInfo.getTerminalID())
                .build();
    }
}
