package global.citytech.finposframework.switches.purchase;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Rishav Chudal on 11/2/20.
 */
public interface ManualPurchaseRequester<
        T extends PurchaseRequestParameter,E extends PurchaseResponseParameter>
        extends Request<T, E> {
}
