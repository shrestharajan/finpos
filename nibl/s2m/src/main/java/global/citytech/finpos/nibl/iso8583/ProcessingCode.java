package global.citytech.finpos.nibl.iso8583;

public enum ProcessingCode {
    //TODO Code based on NIBL, description may subject to change
    PURCHASE("000000", "Purchase"),
    LOG_ON("920000", "Log On"),
    PURCHASE_WITH_CASHBACK("090000", "Purchase With Cashback"),
    PRE_AUTHORIZATION("300000", "Pre Authorization"),
    REFUND("200000", "Refund"),
    CASH_ADVANCE("010000", "Cash Advance"),
    PRE_AUTHORIZATION_COMPLETION("000000", "Pre Authorization Completion"),
    REVERSAL("", "Reversal"),
    SETTLEMENT("920000", "Settlement"),
    SETTLEMENT_CLOSURE("960000", "Settlement Closure"),
    VOID_SALE("020000", "Void Sale"),
    TIP_ADJUSTMENT("020000", "Tip Adjustment"),
    GREEN_PIN("930000", "Green Pin"),
    PIN_CHANGE("910000", "Pin Change"),
    BALANCE_INQUIRY("310000", "Balance Inquiry"),
    CASH_IN("250000", "Cash In"),
    MINI_STATEMENT("380000", "Mini Statement");



    private String code;
    private String description;

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

    private ProcessingCode(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public static ProcessingCode getByCode(String code) {
        ProcessingCode[] dataTypes = values();
        ProcessingCode[] var2 = dataTypes;
        int var3 = dataTypes.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            ProcessingCode dataType = var2[var4];
            if (dataType.getCode().equalsIgnoreCase(code)) {
                return dataType;
            }
        }

        throw new IllegalArgumentException(String.format(" Code %d has not been mapped ", code));
    }
}
