package global.citytech.finpos.merchant.framework.qr;

import org.jetbrains.annotations.NotNull;

import global.citytech.finpos.merchant.domain.usecase.qr.CrcCalculator;

/**
 * @author sachin
 */
public class CrcCalculatorImpl implements CrcCalculator {
    @NotNull
    @Override
    public String calcualteCrc(@NotNull byte[] bytes) {
        int i;
        int crc_value = 0xFFFF;
        for (int len = 0; len < bytes.length; len++) {
            for (i = 0x80; i != 0; i >>= 1) {
                if ((crc_value & 0x8000) != 0) {
                    crc_value = (crc_value << 1) ^ 0x1021;
                } else {
                    crc_value = crc_value << 1;
                }
                if ((bytes[len] & i) != 0) {
                    crc_value ^= 0x1021;
                }
            }
        }
        return Integer.toHexString(crc_value).substring(0,4);
    }
}
