package global.citytech.finpos.merchant.presentation.greenpin

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.common.extensions.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.data.repository.core.greenpin.GreenPinRepositoryImpl
import global.citytech.finpos.merchant.domain.model.greenpin.GreenPinRequestEntity
import global.citytech.finpos.merchant.domain.model.greenpin.GreenPinResponseEntity
import global.citytech.finpos.merchant.domain.usecase.core.greenpin.CoreGreenPinUseCase
import global.citytech.finpos.merchant.framework.datasource.core.greenpin.GreenPinDataSourceImpl
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.greenpin.GreenPinRequestItem
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.usecases.TransactionType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GreenPinViewModel(val instance: Application) : GreenPinBaseTransactionViewModel(instance) {

    var otp = MutableLiveData<String>()
    private var currentOtp: String = ""
    private var newOtp: String = ""
    var dismissActivity = MutableLiveData<Boolean>()
    val greenPinResult by lazy { MutableLiveData<Boolean>() }

    private var otpUseCase: CoreGreenPinUseCase =
        CoreGreenPinUseCase(GreenPinRepositoryImpl(GreenPinDataSourceImpl()))

    fun generateOtpRequest(configurationItem: ConfigurationItem, transactionType: TransactionType) {
        compositeDisposable.add(
            otpUseCase.generateOtpRequest(
                configurationItem,
                GreenPinRequestItem(
                    transactionType = transactionType,
                    amount = "0.00".toBigDecimal()
                )
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    greenPinResult.value= it.isApproved
                    if (greenPinResult.value!!) {
                        successMessage.value = it.message
                    }else{
                        failureMessage.value = it.message
                    }

                }, {
                    it.printStackTrace()
                    isLoading.value= false
                    if(!shouldDispose){
                        if (it.message == PosError.USER_BACK_PRESSED.errorMessage) {
                            dismissActivity.value = true
                        } else {

                            message.value = it.message
                        }
                    }else{
                        completePreviousTask()
                    }
                })
        )
    }

    fun onNumericButtonPressed(buttonText: String, limit: Int) {
        currentOtp = otp.value ?: ""
        newOtp = currentOtp + buttonText
        if (newOtp.length <= limit) {
            otp.value = newOtp
        }
    }

    fun onClearButtonClicked() {
        if (otp.value?.length!! > 0) {
            otp.value = otp.value?.dropLast(1)
        }
    }
}