package global.citytech.finpos.merchant.utils

import android.view.View
import android.widget.ImageButton
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.google.android.material.button.MaterialButton
import global.citytech.easydroid.core.extension.showToast
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.switches.PosEntryMode
import global.citytech.finposframework.utility.DateUtils
import global.citytech.finposframework.utility.StringUtils
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Saurav Ghimire on 5/3/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */

fun PosEntryMode.getCardType(): String {
    return when (this) {
        PosEntryMode.ICC -> CardType.ICC.posEntryMode
        PosEntryMode.MANUAL -> CardType.MANUAL.posEntryMode
        PosEntryMode.MAGNETIC_STRIPE, PosEntryMode.ICC_FALLBACK_TO_MAGNETIC -> CardType.MAG.posEntryMode
        PosEntryMode.PICC -> CardType.PICC.posEntryMode
        else -> CardType.UNKNOWN.posEntryMode
    }
}

fun String?.maskCardNumber(): String {
    return if (this != null) {
        val pan = StringUtils.encodeAccountNumber(this)
        val n = 4
        val str =
            java.lang.StringBuilder(pan!!)
        var idx = str.length - n
        while (idx > 0) {
            str.insert(idx, " ")
            idx -= n
        }
        str.toString()
    } else {
        ""
    }

}

fun String.formatCardNumber(): String {
    return if (this != null) {
        val pan = this
        val n = 4
        val str =
            java.lang.StringBuilder(pan!!)
        var idx = str.length - n
        while (idx > 0) {
            str.insert(idx, " ")
            idx -= n
        }
        str.toString()
    } else {
        ""
    }

}

fun String?.formatExpiryDate(): String {
    return if (this.isNullOrEmpty() or (this!!.length < 4)) {
        return ""
    } else {
        this.substring(2, 4).plus("/").plus(this.substring(0, 2))
    }
}

fun String.formatCardHolderName(): String {
    val cardHolderName = this
    return if (cardHolderName != null) {
        if (cardHolderName.contains("/")) {
            val names = cardHolderName.split("/")
            var fullName = ""
            for (name in names) {
                fullName = fullName.plus(name).plus(" ")
            }
            fullName
        } else {
            cardHolderName
        }
    } else {
        ""
    }

}

fun MaterialButton.handleDebounce() {
    this.isEnabled = false
    android.os.Handler().postDelayed({
        this.isEnabled = true
    }, 500)
}

fun View.handleDebounce() {
    this.isEnabled = false
    android.os.Handler().postDelayed({
        this.isEnabled = true
    }, 500)
}

fun ImageButton.handleDebounce() {
    this.isEnabled = false
    android.os.Handler().postDelayed({
        this.isEnabled = true
    }, 500)
}

fun MaterialButton.setOnlineClickListener(function: () -> Unit) {
    this.setOnClickListener {
        this.handleDebounce()
        if (NiblMerchant.INSTANCE.networkConnectionAvailable.value!!) {
            function()
        } else {
            context.showToast("No connection available")
        }
    }
}

fun AppCompatButton.handleDebounce() {
    this.isEnabled = false
    android.os.Handler().postDelayed({
        this.isEnabled = true
    }, 500)
}


fun AppCompatButton.setOnlineClickListener(function: () -> Unit) {
    this.setOnClickListener {
        this.handleDebounce()
        if (NiblMerchant.INSTANCE.networkConnectionAvailable.value!!) {
            function()
        } else {
            context.showToast("No connection available")
        }
    }
}

fun AppCompatImageView.loadCurrencyIcon() {
    if (DeviceConfiguration.get().currencyCode == "524")
        this.setImageResource(R.drawable.ic_rupee)
    else if (DeviceConfiguration.get().currencyCode == "840")
        this.setImageResource(R.drawable.ic_dollar)
}

fun AppCompatTextView.loadCurrency() {
    if (DeviceConfiguration.get().currencyCode == "524")
        this.text = "NPR"
    else if (DeviceConfiguration.get().currencyCode == "840")
        this.text = "USD"
}

fun MaterialButton.enableMe() {
    this.isEnabled = true
    this.isClickable = true
    this.backgroundTintList =
        ContextCompat.getColorStateList(NiblMerchant.INSTANCE, R.color.finpos_purple_dark)
}

fun MaterialButton.disableMe() {
    this.isEnabled = false
    this.isClickable = false
    this.backgroundTintList =
        ContextCompat.getColorStateList(NiblMerchant.INSTANCE, R.color.finpos_purple_light)
}

fun String.isTransactionDateOrTimeOfGivenFormat(format: String): Boolean {
    var date: Date? = null
    try {
        val simpleDateFormat = SimpleDateFormat(format, Locale.getDefault())
        simpleDateFormat.isLenient = false
        date = simpleDateFormat.parse(this)
    } catch (exception: Exception) {
        exception.printStackTrace()
    }
    return date != null
}

fun String.parseDateInFormatTwoForMerchantLog(): String {
    if (this.length != 6) {
        return ""
    }
    val inputDateFormat = SimpleDateFormat(DateUtils.DATE_FORMAT_TWO, Locale.getDefault())
    val outputDateFormat =
        SimpleDateFormat(DateUtils.DATE_FORMAT_FINPOS_TXN_LOG, Locale.getDefault())
    var outputDate = ""
    try {
        val date: Date? = inputDateFormat.parse(this)
        if (date != null)
            outputDate = outputDateFormat.format(date)
    } catch (exception: Exception) {
        exception.printStackTrace()
    }
    return outputDate
}

fun String.parseDateInFormatOneForMerchantLog(): String {
    if (this.length != 4) {
        return ""
    }
    val inputDateFormat = SimpleDateFormat(DateUtils.DATE_FORMAT_ONE, Locale.getDefault())
    var outputDate = ""
    try {
        val date: Date? = inputDateFormat.parse(this)
        val currentYear = DateUtils.getCurrentYear()
        if (date != null)
            outputDate = this.plus(currentYear)
    } catch (exception: Exception) {
        exception.printStackTrace()
    }
    return outputDate
}

fun retrieveCurrencyName(currencyCode: String): String =
    when (currencyCode) {
        "524" -> "NPR"
        "840" -> "USD"
        else -> currencyCode
    }

fun Long.getMinutesAndSecondsDataFromMillis(): String {
    return "".plus((this / 1000) / (60))
        .plus(" minutes, ")
        .plus((this / 1000) % (60))
        .plus(" seconds")
}
