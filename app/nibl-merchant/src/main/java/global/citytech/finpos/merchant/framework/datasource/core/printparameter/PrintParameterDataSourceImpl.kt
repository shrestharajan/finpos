package global.citytech.finpos.merchant.framework.datasource.core.printparameter

import android.app.Application
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.datasource.core.printparameter.PrintParameterDataSource
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.DeviceControllerImpl
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.printparameter.PrintParameterResponse
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.printer.PrinterRequest
import global.citytech.finposframework.switches.receipt.tmslogs.TmsLogsParameter
import global.citytech.finposframework.utility.PosResult
import io.reactivex.Observable

/**
 * Created by Rishav Chudal on 11/5/20.
 */
class PrintParameterDataSourceImpl(val application: Application): PrintParameterDataSource {
    override fun prepareAndPrintLogs(
        configurationItem: ConfigurationItem,
        parameters: ArrayList<TmsLogsParameter>
        ): Observable<PrintParameterResponse> {
        return Observable.fromCallable {
            deviceReady(configurationItem, parameters)
        }
    }

    private fun deviceReady(
        configurationItem: ConfigurationItem,
        parameters: ArrayList<TmsLogsParameter>
    ): PrintParameterResponse {
        val deviceController = DeviceControllerImpl(application)
        val posResponse = deviceController.isReady(null)
        return if (posResponse.posResult == PosResult.SUCCESS ) {
            val tmsLog = prepareTmsLogs(configurationItem, parameters)
            val printRequest = TmsLogsDecorator(tmsLog).preparePrintRequest()
            printTmsLogs(printRequest)
        } else {
            PrintParameterResponse(
                false,
                posResponse.posError.errorMessage
            )
        }
    }

    private fun prepareTmsLogs(
        configurationItem: ConfigurationItem,
        parameters: ArrayList<TmsLogsParameter>
    ): TmsLogs {
        val tmsLogs = TmsLogs()
        tmsLogs.title = "TERMINAL PARAMETERS REPORTS"
        tmsLogs.terminalInfo = TerminalRepositoryImpl(configurationItem).findTerminalInfo()

        val localDatabaseSourceImpl = LocalDatabaseSourceImpl()
        if (parameters.contains(TmsLogsParameter.SWITCH_PARAMETERS)) {
            tmsLogs.hosts = localDatabaseSourceImpl.getHost().blockingLast()
        }
        if (parameters.contains(TmsLogsParameter.AID)) {
            tmsLogs.aidParams = localDatabaseSourceImpl.getAidParams().blockingLast()
        }
        if (parameters.contains(TmsLogsParameter.EMV_KEYS)) {
            tmsLogs.emvKeys = localDatabaseSourceImpl.getEmvKeys().blockingLast()
        }
        if (parameters.contains(TmsLogsParameter.CARD_SCHEME)) {
            tmsLogs.cardSchemes = localDatabaseSourceImpl.getCardScheme().blockingLast()
        }
        return tmsLogs
    }

    private fun printTmsLogs(printRequest: PrinterRequest): PrintParameterResponse {
        val printerService = PrinterSourceImpl(application)
        val printerResponse = printerService.print(printRequest)
        return if (printerResponse.result == Result.SUCCESS) {
            PrintParameterResponse(
                true,
                application.getString(R.string.msg_success_print)
            )
        } else {
            PrintParameterResponse(
                false,
                printerResponse.message
            )
        }

    }

}