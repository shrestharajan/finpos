package global.citytech.finpos.processor.neps.reconciliation;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import global.citytech.finpos.neps.iso8583.SmartVistaActionCode;
import global.citytech.finpos.neps.iso8583.requestsender.reconciliation.ReconciliationISORequest;
import global.citytech.finpos.neps.iso8583.requestsender.reconciliation.ReconciliationRequestSender;
import global.citytech.finpos.processor.neps.batchupload.BatchUploadRequestModel;
import global.citytech.finpos.processor.neps.batchupload.BatchUploadResponseModel;
import global.citytech.finpos.processor.neps.batchupload.BatchUploadUseCase;
import global.citytech.finpos.processor.neps.logon.LogOnRequestModel;
import global.citytech.finpos.processor.neps.logon.MerchantLogOnUseCase;
import global.citytech.finpos.processor.neps.receipt.detailreport.DetailReportRequestModel;
import global.citytech.finpos.processor.neps.receipt.detailreport.DetailReportUseCase;
import global.citytech.finpos.processor.neps.transaction.NepsIsoMessageReceiptHandler;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reconciliation.ReconciliationRequester;
import global.citytech.finposframework.usecases.activity.ActivityLog;
import global.citytech.finposframework.usecases.activity.ActivityLogStatus;
import global.citytech.finposframework.usecases.activity.ActivityLogType;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationHandler;
import global.citytech.finposframework.usecases.reconciliation.ReconciliationTotals;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.JsonUtils;
import global.citytech.finposframework.utility.StackTraceUtil;
import global.citytech.finposframework.utility.StringUtils;

public class ReconciliationUseCase implements UseCase<ReconciliationRequestModel, ReconciliationResponseModel>,
        ReconciliationRequester<ReconciliationRequestModel, ReconciliationResponseModel> {

    ReconciliationRequestModel reconciliationRequestModel;
    private final TerminalRepository terminalRepository;
    private final Notifier notifier;
    private final Logger logger;
    private IsoMessageResponse reconciliationIsoMessageResponse;
    private ReconciliationRequestSender reconciliationRequestSender;
    private RequestContext requestContext;
    private ReconciliationTotals reconciliationTotals;
    private String currentBatch;
    private boolean isReconciliationAtInitialSuccess;
    private boolean isBatchUploadSuccess;
    private boolean isReconciliationSuccess;
    private CountDownLatch countDownLatch;
    private final boolean[] isConfirm = {false};
    private ActivityLogType currentActivityLogType = ActivityLogType.SETTLEMENT;

    public ReconciliationUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        this.terminalRepository = terminalRepository;
        this.notifier = notifier;
        this.logger = new Logger(ReconciliationUseCase.class.getName());
    }

    @Override
    public ReconciliationResponseModel execute(ReconciliationRequestModel request) {
        logger.debug("::: REACHED RECONCILIATION USE CASE :::");
        reconciliationRequestModel = request;
        currentBatch = this.terminalRepository.getReconciliationBatchNumber();
        try {
            if (isCurrentBatchTransactionsEmpty()) {
                return noNeedSettlementResponse();
            }
            proceedForReconciliation();
        } catch (FinPosException finPosException) {
            onFinPosExceptionDuringReconciliation(finPosException);
        } catch (PosException posException) {
            if (posException.getPosError() == PosError.DEVICE_ERROR_ZERO_AMOUNT_SETTLEMENT) {
                return noNeedSettlementResponse();
            }
            onPosExceptionDuringReconciliation(posException);
        }
        catch (Exception exception) {
            onOtherExceptionDuringReconciliation(exception);
        }
        return ReconciliationResponseModel.Builder.newInstance()
                .withIsSuccess(isReconciliationSuccess)
                .withMessage(this.prepareMessage(isReconciliationSuccess))
                .build();
    }

    private boolean isCurrentBatchTransactionsEmpty() {
        ReconciliationRepository repository = reconciliationRequestModel.getReconciliationRepository();
        long transactionCount = repository.getTransactionCountByBatchNumber(currentBatch);
        return transactionCount <= 0;
    }

    private ReconciliationResponseModel noNeedSettlementResponse() {
        return ReconciliationResponseModel.Builder.newInstance()
                .withMessage("No need for settlement")
                .withIsSuccess(true)
                .build();
    }

    private boolean isFinPosExceptionTypeConnectionError(FinPosException finPosException) {
        FinPosException.ExceptionType type = finPosException.getType();
        return (type == FinPosException.ExceptionType.UNABLE_TO_CONNECT ||
                type == FinPosException.ExceptionType.CONNECTION_ERROR ||
                type == FinPosException.ExceptionType.WRITE_EXCEPTION ||
                type == FinPosException.ExceptionType.READ_TIME_OUT);
    }


    private void proceedForReconciliation() {
        this.notifier.notify(
                Notifier.EventType.PREPARING_RECONCILIATION_TOTALS,
                Notifier.EventType.PREPARING_RECONCILIATION_TOTALS.getDescription()
        );
        requestContext = prepareRequestContext();
        performReconciliationInitialPhase();
        if (!isReconciliationAtInitialSuccess) {
            onReconciliationFailedAtInitial();
        } else {
            isReconciliationSuccess = true;
        }
        updateBatchDataIfReconciliationSuccess();
        performKeyExchangeIfReconciliationSuccess();
    }

    private void printDetailReport() {
        DetailReportUseCase detailReportUseCase = new DetailReportUseCase(
                this.terminalRepository,
                this.notifier
        );
        DetailReportRequestModel requestModel = DetailReportRequestModel.Builder.newInstance()
                .transactionRepository(reconciliationRequestModel.getTransactionRepository())
                .printerService(reconciliationRequestModel.getPrinterService())
                .printSummaryReport(false)
                .applicationRepository(reconciliationRequestModel.getApplicationRepository())
                .reconciliationRepository(reconciliationRequestModel.getReconciliationRepository())
                .build();
        detailReportUseCase.execute(requestModel);
    }

    private void performReconciliationInitialPhase() {
        currentActivityLogType = ActivityLogType.SETTLEMENT;
        reconciliationRequestSender = new ReconciliationRequestSender(requestContext, false);
        terminalRepository.incrementSystemTraceAuditNumber();
        isReconciliationSuccess = isReconciliationAtInitialSuccess = isReconciliationSuccessWithSwitch();
        updateActivityLogOnNormalFlow();
        if (isReconciliationSuccess) {
            printDetailReport();
        }
        printDebugReceiptIfDebugMode(
                reconciliationRequestModel.getPrinterService(),
                reconciliationIsoMessageResponse
        );
        printReconciliationReceiptIfRequired(
                requestContext,
                reconciliationIsoMessageResponse,
                reconciliationRequestModel.getPrinterService(),
                reconciliationRequestModel.getReconciliationRepository(),
                isReconciliationAtInitialSuccess
        );
    }

    private void onReconciliationFailedAtInitial() {
        performBatchUpload();
        if (isBatchUploadSuccess) {
            sendReconciliationTrailer();
        }
    }

    private void performBatchUpload() {
        BatchUploadUseCase batchUploadUseCase = new BatchUploadUseCase(
                terminalRepository,
                notifier
        );
        BatchUploadRequestModel requestModel = new BatchUploadRequestModel(
                reconciliationRequestModel.getApplicationRepository(),
                reconciliationRequestModel.getTransactionRepository(),
                reconciliationRequestModel.getPrinterService()
        );
        BatchUploadResponseModel batchUploadResponseModel = batchUploadUseCase.execute(requestModel);
        isBatchUploadSuccess = (batchUploadResponseModel.getResult() == Result.SUCCESS);
    }

    private void sendReconciliationTrailer() {
        currentActivityLogType = ActivityLogType.SETTLEMENT_CLOSURE;
        reconciliationRequestSender = prepareReconciliationTrailerRequestSender();
        boolean isReconciliationTrailerSuccess = isReconciliationSuccessWithSwitch();
        logger.log(":: IS RECONCILIATION TRAILER SUCCESS ? :: " + isReconciliationTrailerSuccess);
        terminalRepository.incrementSystemTraceAuditNumber();
        isReconciliationSuccess = isBatchUploadSuccess && isReconciliationTrailerSuccess;
        updateActivityLogOnNormalFlow();
        printDetailReport();
        printDebugReceiptIfDebugMode(
                reconciliationRequestModel.getPrinterService(),
                reconciliationIsoMessageResponse
        );
        this.printReconciliationReceiptIfRequired(
                requestContext,
                reconciliationIsoMessageResponse,
                reconciliationRequestModel.getPrinterService(),
                reconciliationRequestModel.getReconciliationRepository(),
                true
        );
    }

    private boolean isReconciliationSuccessWithSwitch() {
        this.notifier.notify(
                Notifier.EventType.CONNECTING_TO_SWITCH,
                Notifier.EventType.CONNECTING_TO_SWITCH.getDescription()
        );
        reconciliationIsoMessageResponse = reconciliationRequestSender.send();
        this.notifier.notify(
                Notifier.EventType.RESPONSE_RECEIVED,
                "Please wait..."
        );
        int responseCode = IsoMessageUtils.retrieveFromDataElementsAsInt(
                reconciliationIsoMessageResponse,
                DataElement.RESPONSE_CODE
        );
        return responseCode == SmartVistaActionCode.ACTION_CODE_000.getActionCode();
    }

    private ReconciliationRequestSender prepareReconciliationTrailerRequestSender() {
        requestContext.setRequest(prepareReconciliationTrailerRequest());
        reconciliationRequestSender = new ReconciliationRequestSender(prepareRequestContextForTrailer(),true);
        return reconciliationRequestSender;
    }

    private ReconciliationISORequest prepareReconciliationTrailerRequest() {
        return ReconciliationISORequest.Builder.newInstance()
                .withReconciliationTotals(reconciliationTotals)
                .withDate(DateUtils.yyMMddDate())
                .build();
    }

    private void onFinPosExceptionDuringReconciliation(FinPosException finPosException) {
        updateFinPosExceptionEncounteredActivityLog(finPosException);
        finPosException.printStackTrace();
        if (isFinPosExceptionTypeConnectionError(finPosException)) {
            throw new PosException(PosError.DEVICE_ERROR_SETTLEMENT_REQUEST_CONNECTION_ERROR);
        }
        throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
    }

    private void onPosExceptionDuringReconciliation(PosException posException) {
        updateOtherExceptionEncounteredActivityLog(posException.getLocalizedMessage());
        posException.printStackTrace();
        throw posException;
    }

    private void onOtherExceptionDuringReconciliation(Exception exception) {
        updateOtherExceptionEncounteredActivityLog(StackTraceUtil.getStackTraceAsString(exception));
        exception.printStackTrace();
        throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
    }


    private void updateBatchDataIfReconciliationSuccess() {
        if (isReconciliationSuccess) {
            terminalRepository.incrementReconciliationBatchNumber();
            reconciliationRequestModel.getReconciliationRepository().updateBatch(
                    currentBatch,
                    "",
                    ""
            );
        }
    }

    private void performKeyExchangeIfReconciliationSuccess() {
        if (isReconciliationSuccess) {
            MerchantLogOnUseCase merchantLogOnUseCase = new MerchantLogOnUseCase(this.terminalRepository);
            merchantLogOnUseCase.execute(prepareLogOnRequestModel());
        }
    }

    private LogOnRequestModel prepareLogOnRequestModel() {
        return new LogOnRequestModel(
                reconciliationRequestModel.getPrinterService(),
                reconciliationRequestModel.getHardwareKeyService(),
                reconciliationRequestModel.getApplicationPackageName()
        );
    }

    private void printReconciliationReceiptIfRequired(
            RequestContext requestContext,
            IsoMessageResponse isoMessageResponse,
            PrinterService printerService,
            ReconciliationRepository reconciliationRepository,
            boolean isPrintingRequired
    ) {
        if (isPrintingRequired) {
            NepsReconciliationReceipt nepsReconciliationReceipt = new
                    NepsReconciliationReceiptMaker(requestContext, isoMessageResponse).prepare();
            String reconciliationReceiptString = JsonUtils.toJsonObj(nepsReconciliationReceipt);
            reconciliationRepository.updateReconciliationReceipt(reconciliationReceiptString);
            NepsReconciliationReceiptPrintHandler reconciliationReceiptPrintHandler = new
                    NepsReconciliationReceiptPrintHandler(printerService);
            reconciliationReceiptPrintHandler
                    .printReceipt(nepsReconciliationReceipt, ReceiptVersion.MERCHANT_COPY);
        }
    }

    private void printDebugReceiptIfDebugMode(PrinterService printerService, IsoMessageResponse isoMessageResponse) {
        boolean isDebugVersion = this.terminalRepository.findTerminalInfo().isDebugModeEnabled();
        if (isDebugVersion) {
            ReceiptHandler.IsoMessageReceiptHandler isoMessageReceiptHandler
                    = new NepsIsoMessageReceiptHandler(printerService);
            isoMessageReceiptHandler.printIsoMessageReceipt(isoMessageResponse);
        }
    }

    private String prepareMessage(boolean isReconciliationSuccess) {
        return isReconciliationSuccess ? "SETTLEMENT SUCCESS" : "SETTLEMENT FAILURE";
    }

    private RequestContext prepareRequestContext() {
        HostInfo primaryHostInfo = this.terminalRepository.findPrimaryHost();
        HostInfo secondaryHostInfo = this.terminalRepository.findSecondaryHost();
        TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
        ConnectionParam connectionParam = new ConnectionParam(primaryHostInfo, secondaryHostInfo);
        RequestContext context =
                new RequestContext(
                        this.terminalRepository.getSystemTraceAuditNumber(),
                        terminalInfo,
                        connectionParam
                );
        context.setRequest(this.prepareReconciliationRequest());
        return context;
    }

    private RequestContext prepareRequestContextForTrailer() {
        HostInfo primaryHostInfo = this.terminalRepository.findPrimaryHost();
        HostInfo secondaryHostInfo = this.terminalRepository.findSecondaryHost();
        TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
        ConnectionParam connectionParam = new ConnectionParam(primaryHostInfo, secondaryHostInfo);
        RequestContext context =
                new RequestContext(
                        this.terminalRepository.getSystemTraceAuditNumber(), terminalInfo, connectionParam);
        context.setRequest(this.prepareReconciliationTrailerRequest());
        return context;
    }

    private ReconciliationISORequest prepareReconciliationRequest() {
        ReconciliationHandler reconciliationHandler = new NepsReconciliationHandler(
                terminalRepository,
                reconciliationRequestModel.getReconciliationRepository(),
                currentBatch,
                reconciliationRequestModel.getApplicationRepository()
        );
        reconciliationTotals = reconciliationHandler.prepareReconciliationTotals();
        logger.log("::: NEPS ::: RECONCILIATION TOTALS ::: " + reconciliationTotals.toString());
        if (isZeroAmountSettlement()) {
            logger.log("::: Zero Amount Settlement :::");
            throw new PosException(PosError.DEVICE_ERROR_ZERO_AMOUNT_SETTLEMENT);
        }
        notifySettlementConfirmationEvent();
        return ReconciliationISORequest.Builder.newInstance()
                .withBatchNumber(currentBatch)
                .withReconciliationTotals(reconciliationTotals)
                .withDate(DateUtils.yyMMddDate())
                .withCurrencyCode(reconciliationRequestModel
                        .getApplicationRepository()
                        .retrieveEmvParameterRequest()
                        .getTransactionCurrencyCode())
                .withApplicationVersion(terminalRepository.getApplicationVersion())
                .build();
    }

    private boolean isZeroAmountSettlement() {
        String totalsBlock = reconciliationTotals.toTotalsBlock();
        String totalReconcileAmount = totalsBlock.substring(1);
        return totalReconcileAmount.isEmpty() || StringUtils.isAllZero(totalReconcileAmount);
    }
    
    private void notifySettlementConfirmationEvent() {
        NepsReconciliationTotals nepsReconciliationTotals = (NepsReconciliationTotals) reconciliationTotals; 
        String jsonData = this.prepareSettlementConfirmationJsonData(nepsReconciliationTotals);
        countDownLatch = new CountDownLatch(1);
        this.notifier.notify(Notifier.EventType.SETTLEMENT_CONFIRMATION_EVENT, jsonData,
                confirmed -> {
                    isConfirm[0] = confirmed;
                    countDownLatch.countDown();
                });
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!isConfirm[0])
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
    }
    
    private String prepareSettlementConfirmationJsonData(
            NepsReconciliationTotals nepsReconciliationTotals
    ) {
        Map<String, String> map = new HashMap<>();
        map.put("currencyName", nepsReconciliationTotals.getTransactionCurrencyName());
        map.put("salesCount", String.valueOf(nepsReconciliationTotals.getSalesCount()));
        map.put("salesAmount", String.valueOf(nepsReconciliationTotals.getSalesAmount()));
        map.put("refundCount", String.valueOf(nepsReconciliationTotals.getSalesRefundCount()));
        map.put("refundAmount", String.valueOf(nepsReconciliationTotals.getSalesRefundAmount()));
        map.put("voidCount", String.valueOf(nepsReconciliationTotals.getVoidCount()));
        map.put("voidAmount", String.valueOf(nepsReconciliationTotals.getVoidAmount()));
        map.put("authCount", String.valueOf(nepsReconciliationTotals.getAuthCount()));
        map.put("authAmount", String.valueOf(nepsReconciliationTotals.getAuthAmount()));
        map.put("totalCount", String.valueOf(nepsReconciliationTotals.getTotalCountReceipt()));
        map.put("totalAmount", String.valueOf(nepsReconciliationTotals.getTotalAmountReceipt()));
        return JsonUtils.toJsonObj(map);
    }

    private void updateFinPosExceptionEncounteredActivityLog(FinPosException finPosException) {
        try {
            ReconciliationISORequest request = (ReconciliationISORequest) requestContext.getRequest();
            TerminalInfo terminalInfo = terminalRepository.findTerminalInfo();
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(terminalRepository.getSystemTraceAuditNumber())
                    .withType(currentActivityLogType)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(request.getBatchNumber())
                    .withAmount(
                            ((NepsReconciliationTotals) request
                                    .getReconciliationTotals())
                                    .getTotalAmountReceipt())
                    .withResponseCode("")
                    .withIsoRequest(finPosException.getIsoRequest())
                    .withIsoResponse(finPosException.getIsoResponse())
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(
                            finPosException.getLocalizedMessage() +
                                    " : " +
                                    finPosException.getDetailMessage())
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .build();
            this.terminalRepository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update " +
                    currentActivityLogType
                    + " activity log :: "
                    + e.getMessage());
        }

    }

    private void updateOtherExceptionEncounteredActivityLog(String remarks) {
        try {
            ReconciliationISORequest request = (ReconciliationISORequest) requestContext.getRequest();
            TerminalInfo terminalInfo = terminalRepository.findTerminalInfo();
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(terminalRepository.getSystemTraceAuditNumber())
                    .withType(currentActivityLogType)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(request.getBatchNumber())
                    .withAmount(
                            ((NepsReconciliationTotals) request
                                    .getReconciliationTotals())
                                    .getTotalAmountReceipt())
                    .withResponseCode("")
                    .withIsoRequest("")
                    .withIsoResponse("")
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(remarks)
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .build();
            terminalRepository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update " +
                    currentActivityLogType
                    + " activity log :: "
                    + e.getMessage());
        }
    }

    private void updateActivityLogOnNormalFlow() {
        try {
            ReconciliationISORequest request = (ReconciliationISORequest) requestContext.getRequest();
            TerminalInfo terminalInfo = terminalRepository.findTerminalInfo();
            String responseCode = IsoMessageUtils.retrieveFromDataElementsAsString(
                    reconciliationIsoMessageResponse,
                    DataElement.RESPONSE_CODE
            );
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(terminalRepository.getSystemTraceAuditNumber())
                    .withType(currentActivityLogType)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(currentBatch)
                    .withAmount( ((NepsReconciliationTotals) request
                            .getReconciliationTotals())
                            .getTotalAmountReceipt())
                    .withResponseCode(responseCode)
                    .withIsoRequest(reconciliationIsoMessageResponse.getDebugRequestString())
                    .withIsoResponse(reconciliationIsoMessageResponse.getDebugResponseString())
                    .withStatus(isReconciliationSuccess ? ActivityLogStatus.SUCCESS : ActivityLogStatus.FAILED)
                    .withRemarks("")
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .build();
            terminalRepository.updateActivityLog(activityLog);
        } catch (Exception e) {
            logger.log("Unable to update " +
                    currentActivityLogType
                    + " activity log :: "
                    + e.getMessage());
        }
    }

}
