package global.citytech.finposframework.switches.reversal;

import global.citytech.finposframework.switches.RequestParameter;

/**
 * Created by Rishav Chudal on 9/29/20.
 */
public interface ReversalRequestParameter extends RequestParameter {
}
