package global.citytech.finpos.neps.iso8583;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.iso8583.BitMap;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.DefaultMessageParser;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.SpecInfo;

import static org.junit.Assert.*;

/**
 * Created by Saurav Ghimire on 1/26/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class NepsMessageSenderTemplateTest {

    SpecInfo specInfo = new NepsSpecInfo();
    DefaultMessageParser messageParser;

    @Before
    public void setup(){
        messageParser = new DefaultMessageParser(specInfo);
    }

    @Test
    public void testParse(){

        String hexData = "30323130723000000A8082003136343433383330303132333238373534303030303030303030303030303030303230303031323731323138323830303030323932313031323731323138323530303033303037393933363239323034313233343032373532340016910A8AED7D4B020B3F2D30358A023035";

        Iso8583Msg message = messageParser.parse(hextToByte(hexData));
        System.out.println("ISO MESSAGE :::: "+message.getPrintableData());

    }

    public byte[] hextToByte(String str){
        byte[] val = new byte[str.length() / 2];
        for (int i = 0; i < val.length; i++) {
            int index = i * 2;
            int j = Integer.parseInt(str.substring(index, index + 2), 16);
            val[i] = (byte) j;
        }
        return val;
    }
}