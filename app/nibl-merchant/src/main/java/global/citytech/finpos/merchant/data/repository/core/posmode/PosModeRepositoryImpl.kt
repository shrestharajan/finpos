package global.citytech.finpos.merchant.data.repository.core.posmode

import global.citytech.finpos.merchant.data.datasource.core.posmode.PosModeDataSource
import global.citytech.finpos.merchant.domain.repository.core.posmode.PosModeRepository
import global.citytech.finposframework.switches.posmode.PosMode

/**
 * Created by Unique Shakya on 1/7/2021.
 */
class PosModeRepositoryImpl(private val posModeDataSource: PosModeDataSource) : PosModeRepository {
    override fun getPosMode(merchantCategoryCode: String): PosMode {
        return posModeDataSource.getPosMode(merchantCategoryCode)
    }
}