package global.citytech.finpos.processor.nibl.receipt.duplicatereconciliation;

import global.citytech.finpos.processor.nibl.reconciliation.NiblReconciliationReceipt;
import global.citytech.finpos.processor.nibl.reconciliation.NiblReconciliationReceiptPrintHandler;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.duplicatereconciliation.DuplicateReconciliationReceiptRequester;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.utility.JsonUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Unique Shakya on 12/16/2020.
 */
public class DuplicateReconciliationReceiptUseCase implements DuplicateReconciliationReceiptRequester<
        DuplicateReconciliationReceiptRequestModel, DuplicateReconciliationReceiptResponseModel>,
        UseCase<DuplicateReconciliationReceiptRequestModel, DuplicateReconciliationReceiptResponseModel> {
    @Override
    public DuplicateReconciliationReceiptResponseModel execute(DuplicateReconciliationReceiptRequestModel
                                                                       duplicateReconciliationReceiptRequestModel) {
        String reconciliationReceiptString = duplicateReconciliationReceiptRequestModel
                .getReconciliationRepository().getReconciliationReceipt();
        if (StringUtils.isEmpty(reconciliationReceiptString) || !JsonUtils.isValidJsonString(reconciliationReceiptString))
            throw new PosException(PosError.DEVICE_ERROR_NO_RECENT_RECONCILIATION);
        NiblReconciliationReceipt niblReconciliationReceipt = JsonUtils
                .fromJsonToObj(reconciliationReceiptString, NiblReconciliationReceipt.class);
        if (niblReconciliationReceipt == null)
            throw new PosException(PosError.DEVICE_ERROR_NO_RECENT_RECONCILIATION);
        NiblReconciliationReceiptPrintHandler reconciliationReceiptPrintHandler = new
                NiblReconciliationReceiptPrintHandler(duplicateReconciliationReceiptRequestModel
                .getPrinterService());
        try {
            PrinterResponse printerResponse = reconciliationReceiptPrintHandler.printReceipt(niblReconciliationReceipt, ReceiptVersion.DUPLICATE_COPY);
            return new DuplicateReconciliationReceiptResponseModel(printerResponse.getResult(),
                    printerResponse.getMessage());
        } catch (PosException e) {
            throw e;
        } catch (Exception e) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_PRINT);
        }
    }
}
