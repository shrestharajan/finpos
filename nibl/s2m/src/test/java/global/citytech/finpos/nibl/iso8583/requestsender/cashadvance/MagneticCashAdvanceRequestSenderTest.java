package global.citytech.finpos.nibl.iso8583.requestsender.cashadvance;

import org.junit.Test;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.NIBLHostConfig;
import global.citytech.finpos.nibl.iso8583.MockValues;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.utility.IsoMessageUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Unique Shakya on 2/4/2021.
 */
public class MagneticCashAdvanceRequestSenderTest {

    private Logger logger = Logger.getLogger(MagneticCashAdvanceRequestSenderTest.class.getName());

    @Test
    public void send() {
        try {
            String terminalID = "99991002";
            String acquirerID = "405635000000002";
            TerminalInfo terminalInfo = new TerminalInfo(terminalID, acquirerID,"","Citytech","Kamaladi");
            HostInfo primaryHost = NIBLHostConfig.getPrimaryConfig();
            HostInfo secondaryHost = NIBLHostConfig.getSecondaryConfig();
            ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);
            RequestContext context =
                    new RequestContext(
                            "000405", terminalInfo, connectionParam);
            CashAdvanceRequest request = CashAdvanceRequest.Builder.newInstance()
                    .withTransactionAmount(105.0)
                    .withPosConditionCode("00")
                    .withPinBlock("7C8DE0C78AEA2044")
                    .withInvoiceNumber("000108")
                    .withTrack2Data(MockValues.TRACK_2_DATA)
                    .build();
            context.setRequest(request);
            MagneticCashAdvanceRequestSender sender = new MagneticCashAdvanceRequestSender(
                    new NIBLSpecInfo(),
                    context
            );
            IsoMessageResponse response = sender.send();
            assertNotNull(response);
            assertEquals(0, IsoMessageUtils.retrieveFromDataElementsAsInt(response, DataElement.RESPONSE_CODE));

        } catch (FinPosException ex) {
            logger.log(ex.getMessage());
            logger.log(ex.getDetailMessage());
        }
    }
}
