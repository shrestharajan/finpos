package global.citytech.finposframework.switches.receipt.detailreport;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 1/4/2021.
 */
public interface DetailReportRequester<T extends DetailReportRequestParameter,
        E extends DetailReportResponseParameter> extends Request<T, E> {
}
