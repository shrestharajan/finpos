package global.citytech.finposframework.usecases.transaction.data;

public enum PurchaseResult {

    SUCCESS,
    FAILURE,
    TIMEOUT,
    APPROVED,
    DECLINED,
    ACCEPTED,
    REJECTED,
    INCORRECT_PIN,
    INCORRECT_MAC,
    UNABLE_TO_PARSE_RESPONSE,
    STORED_IN_SAF,
    RESET_PWD
}
