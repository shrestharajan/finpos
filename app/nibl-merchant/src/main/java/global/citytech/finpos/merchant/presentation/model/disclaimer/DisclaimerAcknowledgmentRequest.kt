package global.citytech.finpos.merchant.presentation.model.disclaimer

data class DisclaimerAcknowledgmentRequest(
    val disclaimerId: String,
    val remarks: String,
    val serialNumber: String
)