package global.citytech.finpos.nibl.admin.presentation.printparameter

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.nibl.admin.BR
import global.citytech.finpos.nibl.admin.R
import global.citytech.finpos.nibl.admin.constants.Parameter.AID_PARAM
import global.citytech.finpos.nibl.admin.constants.Parameter.CARD_SCHEME
import global.citytech.finpos.nibl.admin.constants.Parameter.EMV_KEYS
import global.citytech.finpos.nibl.admin.constants.Parameter.SWITCH_PARAM
import global.citytech.finpos.nibl.admin.databinding.ActivityPrintParameterBinding
import kotlinx.android.synthetic.main.activity_print_parameter.*

class PrintParameterActivity :
    AppBaseActivity<ActivityPrintParameterBinding, PrintParameterViewModel>() {

    private val parameterMap: HashMap<String, String> = HashMap()
    private val parameters = arrayListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initViews()
        initObservers()
    }

    private fun initObservers() {

    }

    private fun initViews() {
        cb_switch_param.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                parameters.add(SWITCH_PARAM)
            } else {
                parameters.remove(SWITCH_PARAM)
            }
        }
        cb_aid.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                parameters.add(AID_PARAM)
            } else {
                parameters.remove(AID_PARAM)
            }
        }
        cb_emv.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                parameters.add(EMV_KEYS)
            } else {
                parameters.remove(EMV_KEYS)
            }
        }
        cb_card_scheme.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                parameters.add(CARD_SCHEME)
            } else {
                parameters.remove(CARD_SCHEME)
            }
        }

        btn_print.setOnClickListener {
            getViewModel().printParameter(parameters)
        }

    }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayout(): Int = R.layout.activity_print_parameter

    override fun getViewModel(): PrintParameterViewModel =
        ViewModelProviders.of(this)[PrintParameterViewModel::class.java]
}
