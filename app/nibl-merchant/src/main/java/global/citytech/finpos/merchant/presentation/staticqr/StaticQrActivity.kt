package global.citytech.finpos.merchant.presentation.staticqr

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.google.android.flexbox.*
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityStaticQrBinding
import global.citytech.finpos.merchant.domain.model.app.QrStatus
import global.citytech.finpos.merchant.domain.usecase.qr.NqrConstants
import global.citytech.finpos.merchant.domain.usecase.qr.nqr.NqrGenerateRequest
import global.citytech.finpos.merchant.framework.qr.QrHelper
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmationDialog
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.dynamicqr.QrType
import global.citytech.finpos.merchant.presentation.model.qroperator.QrOperatorItem
import global.citytech.finpos.merchant.presentation.model.setting.QrConfiguration
import global.citytech.finpos.merchant.presentation.utils.QrIssuersAdapter
import global.citytech.finpos.merchant.utils.AppConstant
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion
import global.citytech.finposframework.utility.DateUtils
import kotlinx.android.synthetic.main.activity_dynamic_qr.*
import kotlinx.android.synthetic.main.activity_static_qr.*
import kotlinx.android.synthetic.main.activity_static_qr.cl_qr
import kotlinx.android.synthetic.main.activity_static_qr.iv_back
import kotlinx.android.synthetic.main.activity_static_qr.iv_loading
import kotlinx.android.synthetic.main.activity_static_qr.iv_qr_content
import kotlinx.android.synthetic.main.activity_static_qr.iv_qr_operator_logo
import kotlinx.android.synthetic.main.activity_static_qr.rv_issuers
import kotlinx.android.synthetic.main.activity_static_qr.tv_issuers
import kotlinx.android.synthetic.main.activity_static_qr.tv_merchant_address
import kotlinx.android.synthetic.main.activity_static_qr.tv_merchant_name
import kotlinx.android.synthetic.main.activity_static_qr.tv_qr_operator_title
import kotlinx.android.synthetic.main.activity_static_qr.view_issuers

class StaticQrActivity : AppBaseActivity<ActivityStaticQrBinding, StaticQrViewModel>(),
    TransactionConfirmationDialog.Listener {

    private var qrOperatorItem: QrOperatorItem? = null
    private var qrConfiguration: QrConfiguration? = null
    private var merchantName: String = ""
    private var merchantAddress: String = ""
    lateinit var qrHelper: QrHelper
    private var transactionConfirmationDialog: TransactionConfirmationDialog? = null

    companion object {
        val QR_OPERATOR_ITEM = "qrOperatorItem"
        val QR_CONFIGURATION = "qrConfiguration"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
        initViews()
        initListeners()
        observeAppConfigurationLiveData()
        observeQrData()
        observeTransactionConfirmation()
        observeBaseLiveData()
        getViewModel().bindService()
        getViewModel().getConfiguration()
        observeNoInternetConnection()
        observeQRStatusCode()
    }

    private fun observeNoInternetConnection() {
        getViewModel().noInternetConnection.observe(this, Observer {
            if (it) {
                tv_network_connection_message.visibility = View.VISIBLE
            } else
                tv_network_connection_message.visibility = View.GONE
        })
    }

    private fun observeQRStatusCode() {
        getViewModel().showAlertDialog.value = false
        getViewModel().startTimer()
        getViewModel().qrStatusCode.observe(this, Observer {
            if (getViewModel().showAlertDialog.value == true) {
                hideProgress()
            } else {
                hideConfirmationMessage()
                when {
                    it.equals(QrStatus.QR001.code) -> {
                        val msg = getViewModel().qrStatusMessage.value
                        showProgress(msg)
                    }
                    it.equals(QrStatus.QR002.code) -> {
                        val msg = getViewModel().qrStatusMessage.value
                        showProgress(msg)
                    }
                    it.equals(QrStatus.QR003.code) -> {
                        val msg = getViewModel().qrStatusMessage.value
                        hideProgress()
                    }
                    it.equals(QrStatus.QR000.code) -> {
                        hideProgress()
                    }
                    else -> {
                        hideProgress()
                    }
                }
            }
        })

        getViewModel().showAlertDialog.observe(this, Observer {
            if (it) {
                hideProgress()
                showConfirmationMessage(MessageConfig.Builder()
                    .title(getString(R.string.time_out))
                    .message(getString(R.string.msg_confirmation_continue_qr_payments))
                    .positiveLabel("OK")
                    .onPositiveClick {
                        getViewModel().startTimer()
                        getViewModel().disableAlertDialogFlag()
                    }
                    .negativeLabel("CANCEL")
                    .onNegativeClick {
                        setResult(RESULT_CANCELED)
                        finish()
                    })
            } else {
                hideConfirmationMessage()
            }
        })
    }

    private fun observeBaseLiveData() {
        getViewModel().isLoading.observe(this, Observer {
            if (it)
                showProgress()
            else
                hideProgress()
        })

        getViewModel().message.observe(this, Observer {
            showNormalMessage(
                MessageConfig.Builder()
                    .message(it)
                    .positiveLabel(getString(R.string.title_ok))
                    .onPositiveClick {
                        finish()
                    })
        })
    }

    private fun observeTransactionConfirmation() {
        getViewModel().transactionConfirmationLiveData.observe(this, Observer {
            showTransactionConfirmationDialog(it, getViewModel().updateTransactionConfirmation)
        })
    }

    private fun showTransactionConfirmationDialog(it: TransactionConfirmation, update: Boolean) {
        if (transactionConfirmationDialog == null)
            transactionConfirmationDialog = TransactionConfirmationDialog(this, this, it)
        if (update)
            transactionConfirmationDialog!!.update(it, this)
        transactionConfirmationDialog!!.show()
    }

    private fun observeQrData() {
        getViewModel().qrData.observe(this, Observer {
            setQrCode(it)
        })
    }

    private fun setQrCode(qrData: String) {
        val qrBitmap = qrHelper.generateQrBitmap(qrData)
        if (qrBitmap != null) {
            iv_qr_content.setImageBitmap(qrBitmap)
            getViewModel().checkQrPaymentStatus(getViewModel().nqrGenerateRequest!!, 2000)
        }
    }

    private fun prepareNqrGenerateRequest(): NqrGenerateRequest {
        return NqrGenerateRequest(
            initiationMethod = NqrConstants.STATIC_QR,
            acquirerCode = qrOperatorItem?.acquirerCode!!,
            merchantCode = qrConfiguration?.merchantId!!,
            merchantCategoryCode = qrConfiguration?.merchantCategoryCode!!,
            countryCode = AppConstant.COUNTRY_CODE_NP,
            merchantName = qrHelper.toNqrMerchantName(merchantName),
            merchantCity = qrHelper.toNqrMerchantCityName(merchantAddress),
            transactionCurrency = qrConfiguration?.currency!!,
            transactionAmount = "",
            terminalId = qrConfiguration?.terminalId!!,
            billNumber = DateUtils.yyMMddDate().plus(getViewModel().getBillingInvoiceNumber())
        )
    }

    private fun initData() {
        if (!intent.getStringExtra(QR_OPERATOR_ITEM).isNullOrEmpty()) {
            qrOperatorItem = Jsons.fromJsonToObj(
                intent.getStringExtra(QR_OPERATOR_ITEM),
                QrOperatorItem::class.java
            )
        }
        if (!intent.getStringExtra(QR_CONFIGURATION).isNullOrEmpty()) {
            qrConfiguration = Jsons.fromJsonToObj(
                intent.getStringExtra(QR_CONFIGURATION),
                QrConfiguration::class.java
            )
        }

        qrHelper = QrHelper()
    }

    private fun initViews() {
        tv_qr_operator_title.setText(qrOperatorItem?.name)

        if (QrType.getByCodes(qrOperatorItem?.name.toString()) == QrType.NQR){
            loadQrLogo(R.drawable.ic_nepal_pay_qr)
        }else if(QrType.getByCodes(qrOperatorItem?.name.toString()) == QrType.FONEPAY){
            loadQrLogo(R.drawable.ic_fone_pay)
        }else{
            loadQrLogo(R.drawable.banner_loading)
        }

        iv_loading.visibility = View.GONE
        cl_qr.visibility = View.VISIBLE
        qrOperatorItem?.let { item ->
            if (!item.issuers.isNullOrEmpty()){
                val qrIssuersAdapter = QrIssuersAdapter(item.issuers!!)
                rv_issuers.adapter = qrIssuersAdapter
                val layoutManager = FlexboxLayoutManager(this)
                layoutManager.flexDirection = FlexDirection.ROW
                layoutManager.alignItems = AlignItems.CENTER
                layoutManager.flexWrap = FlexWrap.WRAP
                layoutManager.justifyContent = JustifyContent.CENTER
                rv_issuers.layoutManager = layoutManager
                cl_footer_static_qr.visibility = View.VISIBLE
            }else{
                cl_footer_static_qr.visibility = View.INVISIBLE
            }
        } ?: kotlin.run {
            cl_footer_static_qr.visibility = View.INVISIBLE
        }
    }

    private fun loadQrLogo(placeholder: Int){
        Glide.with(this)
            .load(qrOperatorItem?.logo)
            .placeholder(placeholder)
            .into(iv_qr_operator_logo)
    }

    private fun initListeners() {
        iv_back.setOnClickListener {
            getViewModel().stopTimer()
            finish()
        }
    }

    private fun observeAppConfigurationLiveData() {
        getViewModel().appConfiguration.observe(
            this,
            Observer {
                merchantName = it.merchants?.get(0)?.name!!
                merchantAddress = it.merchants?.get(0)?.address!!
                tv_merchant_name.setText(merchantName)
                tv_merchant_address.setText(merchantAddress)
                getViewModel().generateNqr(prepareNqrGenerateRequest())
            }
        )
    }

    override fun onDestroy() {
        getViewModel().dispose()
        super.onDestroy()
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayout(): Int {
        return R.layout.activity_static_qr
    }

    override fun getViewModel(): StaticQrViewModel {
        return ViewModelProviders.of(this).get(StaticQrViewModel::class.java)
    }

    override fun onPositiveButtonClick() {
        getViewModel().printPaymentReceipt(ReceiptVersion.CUSTOMER_COPY)
    }

    override fun onNegativeButtonClick() {
        finish()
    }
}