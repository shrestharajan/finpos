package global.citytech.finpos.merchant.framework.datasource.core

import android.app.Application
import com.google.gson.JsonParser
import global.citytech.common.Constants
import global.citytech.common.data.LabelValue
import global.citytech.common.extensions.toJsonString
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.data.datasource.app.db.AppDatabase
import global.citytech.finpos.merchant.domain.model.app.AidParam
import global.citytech.finpos.merchant.domain.model.app.EmvKey
import global.citytech.finpos.merchant.domain.model.app.EmvParam
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.exceptions.PosException
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.repositories.*
import global.citytech.finposframework.repositories.FieldAttributes.*
import global.citytech.finposframework.usecases.CardSchemeType
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.utility.CardUtils
import global.citytech.finposframework.utility.StringUtils
import global.citytech.finposframework.utility.TMSConfigurationConstants


/**
 * Created by Rishav Chudal on 9/15/20.
 */
class ApplicationRepositoryImpl(val instance: Application) : ApplicationRepository {

    private val logger =  Logger(ApplicationRepositoryImpl::class.java.name)

    override fun retrieveAid(request: AidRetrieveRequest?): AidRetrieveResponse {
        if (request != null) {
            val aidParamList = AppDatabase
                .getInstance(instance.applicationContext)
                .getAidParamDao()
                .getAidParamList()
            return iterateAidParamInList(request, aidParamList)
        } else {
            throw PosException(
                PosError.DEVICE_ERROR_NO_SUCH_REQUEST
            )
        }
    }

    override fun getSupportedCardSchemes(): MutableList<CardSchemeType> {
        val cardSchemes = AppDatabase.getInstance(instance.applicationContext)
            .getCardSchemeDao().getCardScheme()
        val cardSchemeTypeList = ArrayList<CardSchemeType>()
        cardSchemes.forEach {
            cardSchemeTypeList.add(CardUtils.retrieveCardSchemeType(it.cardSchemeId))
        }
        return cardSchemeTypeList
    }

    override fun retrieveEmvParameterRequest(): EmvParametersRequest {
        val emvParamList = AppDatabase
            .getInstance(instance.applicationContext)
            .getEmvParamDao()
            .getEmvParams()
        return prepareEmvParameterRequest(emvParamList)
    }

    override fun isTransactionTypeEnabled(transactionType: TransactionType): Boolean {
        val menuConfiguration = retrieveFromAdditionalDataEmvParameters(TMSConfigurationConstants.TRANSACTION_MENU)
        val configurationOffsetArray = menuConfiguration.toCharArray()
        val transactionOffset = this.getTransactionOffset(transactionType)
        return (configurationOffsetArray[transactionOffset] == BuildConfig.TRANSACTION_ENABLED.single())
    }

    private fun getTransactionOffset(transactionType: TransactionType): Int {
        return when (transactionType) {
            TransactionType.PRE_AUTH -> TMSConfigurationConstants.PRE_AUTH_TRANSACTION_OFFSET
            TransactionType.VOID -> TMSConfigurationConstants.VOID_TRANSACTION_OFFSET
            TransactionType.REFUND -> TMSConfigurationConstants.REFUND_TRANSACTION_OFFSET
            TransactionType.AUTH_COMPLETION -> TMSConfigurationConstants.AUTH_COMPLETION_TRANSACTION_OFFSET
            TransactionType.TIP_ADJUSTMENT -> TMSConfigurationConstants.TIP_ADJUSTMENT_TRANSACTION_OFFSET
            TransactionType.CASH_ADVANCE -> TMSConfigurationConstants.CASH_ADVANCE_TRANSACTION_OFFSET
            TransactionType.CASH_IN -> TMSConfigurationConstants.CASH_DEPOSIT_TRANSACTION_OFFSET
            TransactionType.BALANCE_INQUIRY -> TMSConfigurationConstants.BALANCE_INQUIRY_TRANSACTION_OFFSET
            TransactionType.MINI_STATEMENT -> TMSConfigurationConstants.MINI_STATEMENT_TRANSACTION_OFFSET
            TransactionType.CASH_VOID -> TMSConfigurationConstants.CASH_VOID_TRANSACTION_OFFSET
            else -> TMSConfigurationConstants.SALES_TRANSACTION_OFFSET
        }
    }

    override fun retrieveEmvKey(request: EmvKeyRetrieveRequest?): EmvKeyRetrieveResponse {
        if (request != null) {
            val emvKeyList = AppDatabase
                .getInstance(instance.applicationContext)
                .getEmvKeyDao()
                .getEmvKeys()
            return iterateEmvKeysInList(request, emvKeyList)
        } else {
            throw PosException(
                PosError.DEVICE_ERROR_NO_SUCH_REQUEST
            )
        }
    }

    override fun getMessageTextByActionCode(actionCode: Int): String {
        return AppDatabase.getInstance(instance)
            .getMessageCodeDao()
            .getMessageTextByActionCode(StringUtils.ofRequiredLength(actionCode.toString(), 2))
    }

    override fun retrieveCardScheme(request: CardSchemeRetrieveRequest?): CardSchemeRetrieveResponse {
        if (request != null) {
            val attribute = getRespectiveAttributeForCardScheme(request)
            return prepareCardSchemeRetrieveResponse(request, attribute)
        } else {
            throw PosException(
                PosError.DEVICE_ERROR_NO_SUCH_REQUEST
            )
        }
    }

    override fun retrieveFromAdditionalDataEmvParameters(attribute: String?): String {
        val emvParametersRequest = AppDatabase.getInstance(instance.applicationContext)
            .getEmvParamDao().getEmvParams()
        if (emvParametersRequest.isNotEmpty()) {
            val additionalData = emvParametersRequest[0].additionalData
            if (!additionalData.isNullOrEmptyOrBlank()) {
                val value = getValueFromAdditionalData(additionalData, attribute)
                logger.log(
                    "::: ATTRIBUTE ::: ".plus(attribute).plus("=== VALUE === ").plus(value)
                )
                return value
            }
        }
        return ""
    }

    private fun iterateAidParamInList(
        request: AidRetrieveRequest,
        aidParams: List<AidParam>
    ): AidRetrieveResponse {
        var response =
            AidRetrieveResponse(
                ""
            )
        for (aidParam in aidParams) {
            val aidValue = getValueFromLabelValue(aidParam.aid)
            if (aidValue.equals(request.aid)) {
                val data = getRespectiveFieldFromAid(request, aidParam)
                response =
                    AidRetrieveResponse(
                        data
                    )
                break
            }
        }
        return response
    }

    private fun getRespectiveFieldFromAid(
        request: AidRetrieveRequest,
        aidParam: AidParam
    ): String {
        val result: String
        when (request.fieldAttributes) {
            AID_DATA_S1_F4_AID -> result = aidParam.aid

            DEVICE_SPECIFIC_S1_F4_4_TERMINAL_CONTACTLESS_FLOOR_LIMIT,
            DEVICE_SPECIFIC_S1_F4_8_TERMINAL_CONTACTLESS_FLOOR_LIMIT,
            DEVICE_SPECIFIC_S1_F4_12_TERMINAL_CONTACTLESS_FLOOR_LIMIT -> {
                result = getValueFromLabelValue(aidParam.contactlessFloorLimit)
            }

            DEVICE_SPECIFIC_S1_F4_2_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT,
            DEVICE_SPECIFIC_S1_F4_6_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT,
            DEVICE_SPECIFIC_S1_F4_10_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT -> {
                result = getValueFromLabelValue(aidParam.contactlessTransactionLimit)
            }

            DEVICE_SPECIFIC_S1_F4_3_TERMINAL_CVM_REQUIRED_LIMIT,
            DEVICE_SPECIFIC_S1_F4_7_TERMINAL_CVM_REQUIRED_LIMIT,
            DEVICE_SPECIFIC_S1_F4_11_TERMINAL_CVM_REQUIRED_LIMIT -> {
                result = getValueFromLabelValue(aidParam.cvmLimit)
            }

            AID_DATA_S1_F24_DEFAULT_ACTION_CODE -> {
                result = getValueFromLabelValue(aidParam.defaultActionCode)
            }

            AID_DATA_S1_F16_DEFAULT_DDOL -> {
                result = getValueFromLabelValue(aidParam.defaultDDOL)
            }

            AID_DATA_S1_F14_DEFAULT_TDOL -> {
                result = getValueFromLabelValue(aidParam.defaultTDOL)
            }

            AID_DATA_S1_F20_DENIAL_ACTION_CODE -> {
                result = getValueFromLabelValue(aidParam.denialActionCode)
            }

            AID_DATA_S1_F6_AID_LABEL -> {
                result = getValueFromLabelValue(aidParam.label)
            }

            AID_DATA_S1_F22_ONLINE_ACTION_CODE -> {
                result = getValueFromLabelValue(aidParam.onlineActionCode)
            }

            AID_DATA_S1_F8_TERMINAL_AID_VERSION_NUMBERS -> {
                result = getValueFromLabelValue(aidParam.terminalAidVersion)
            }

            CARD_SCHEME_S2_F16_TERMINAL_FLOOR_LIMIT -> {
                result = getValueFromLabelValue(aidParam.terminalFloorLimit)
            }

            AID_DATA_S1_F32_CD_CVM_LIMIT,
            AID_DATA_S1_F34_NO_CD_CVM_LIMIT,
            AID_DATA_S1_F36_CVM_CAP_CVM_REQUIRED,
            AID_DATA_S1_F38_CVM_CAP_NO_CVM_REQUIRED,
            AID_DATA_S1_F40_TERMINAL_RISK_MANAGEMENT_DATA,
            AID_DATA_S1_F42_CL_TAC_DENIAL,
            AID_DATA_S1_F44_CL_TAC_ONLINE,
            AID_DATA_S1_F46_CL_TAC_DEFAULT,
            AID_DATA_S1_F48_ACQUIRER_ID,
            AID_DATA_S1_F50_KERNEL_CONFIG,
            AID_DATA_S1_F52_CL_CARD_DATA_INPUT_CAP,
            AID_DATA_S1_F54_CL_SECURITY_CAP,
            AID_DATA_S1_F56_UDOL,
            AID_DATA_S1_F58_MSD_APP_VERSION -> {
                result = getValueFromAdditionalData(
                    aidParam.additionalData,
                    getJsonObjectNameOfAdditionalData(request.fieldAttributes)
                )
            }

            else -> result = ""
        }
        return result
    }

    private fun getJsonObjectNameOfAdditionalData(fieldAttributes: FieldAttributes?): String {
        return when (fieldAttributes) {
            AID_DATA_S1_F32_CD_CVM_LIMIT -> Constants.KEY_CD_CVM_LIMIT
            AID_DATA_S1_F34_NO_CD_CVM_LIMIT -> Constants.KEY_NO_CD_CVM_LIMIT
            AID_DATA_S1_F36_CVM_CAP_CVM_REQUIRED -> Constants.KEY_CVM_CAP_CVM_REQUIRED
            AID_DATA_S1_F38_CVM_CAP_NO_CVM_REQUIRED -> Constants.KEY_CVM_CAP_NO_CVM_REQUIRED
            AID_DATA_S1_F40_TERMINAL_RISK_MANAGEMENT_DATA -> Constants.KEY_TERMINAL_RISK_MGMT_DATA
            AID_DATA_S1_F42_CL_TAC_DENIAL -> Constants.KEY_CL_TAC_DENIAL
            AID_DATA_S1_F44_CL_TAC_ONLINE -> Constants.KEY_CL_TAC_ONLINE
            AID_DATA_S1_F46_CL_TAC_DEFAULT -> Constants.KEY_CL_TAC_DEFAULT
            AID_DATA_S1_F48_ACQUIRER_ID -> Constants.KEY_ACQUIRER_ID
            AID_DATA_S1_F50_KERNEL_CONFIG -> Constants.KEY_KERNEL_CONFIG
            AID_DATA_S1_F52_CL_CARD_DATA_INPUT_CAP -> Constants.KEY_CL_CARD_DATA_INPUT_CAP
            AID_DATA_S1_F54_CL_SECURITY_CAP -> Constants.KEY_CL_SECURITY_CAP
            AID_DATA_S1_F56_UDOL -> Constants.KEY_UDOL
            AID_DATA_S1_F58_MSD_APP_VERSION -> Constants.KEY_MSD_APP_VERSION
            AID_DATA_S1_F60_TTQ -> Constants.KEY_TTQ
            else -> ""
        }
    }

    private fun getValueFromAdditionalData(
        additionalData: String?,
        parameterToRetrieve: String?
    ): String {
        if (parameterToRetrieve.isNullOrEmptyOrBlank() || additionalData.isNullOrEmptyOrBlank())
            return ""
        try {
            val jsonParser = JsonParser()
            val additionalDataObject = jsonParser.parse(additionalData).asJsonObject
            if (!additionalDataObject.has(parameterToRetrieve))
                return ""
            val labelValueDataObject = additionalDataObject.getAsJsonObject(parameterToRetrieve)
            return getValueFromLabelValue(labelValueDataObject.toJsonString())
        } catch (e: Exception){
            return ""
        }
    }

    private fun prepareEmvParameterRequest(
        emvParamList: List<EmvParam>
    ): EmvParametersRequest {
        return EmvParametersRequest(
            getValueFromLabelValue(emvParamList[0].terminalCountryCode),
            getValueFromLabelValue(emvParamList[0].transactionCurrencyCode),
            getValueFromLabelValue(emvParamList[0].transactionCurrencyExponent),
            getValueFromLabelValue(emvParamList[0].terminalCapabilities),
            getValueFromLabelValue(emvParamList[0].terminalType),
            getValueFromLabelValue(emvParamList[0].merchantCategoryCode),
            getValueFromLabelValue(emvParamList[0].terminalId),
            getValueFromLabelValue(emvParamList[0].merchantIdentifier),
            getValueFromLabelValue(emvParamList[0].merchantName),
            getValueFromLabelValue(emvParamList[0].ttq),
            getValueFromLabelValue(emvParamList[0].additionalTerminalCapabilities),
            getValueFromLabelValue(emvParamList[0].forceOnlineFlag)
        )
    }

    private fun iterateEmvKeysInList(
        request: EmvKeyRetrieveRequest,
        emvKeys: List<EmvKey>
    ): EmvKeyRetrieveResponse {
        var response =
            EmvKeyRetrieveResponse(
                ""
            )
        for (emvKey in emvKeys) {
            val ridValue = getValueFromLabelValue(emvKey.rid)
            val indexValue = getValueFromLabelValue(emvKey.index)
            if (ridValue.equals(request.rid) && indexValue.equals(request.index)) {
                val data = getRespectiveFieldFromEmvKey(request, emvKey)
                response =
                    EmvKeyRetrieveResponse(
                        data
                    )
                break
            }
        }
        return response
    }

    private fun getRespectiveFieldFromEmvKey(
        request: EmvKeyRetrieveRequest,
        emvKey: EmvKey
    ): String {
        val result: String
        when (request.field) {
            EMV_PUBLIC_KEY_S1_F16_CHECK_SUM -> result = getValueFromLabelValue(emvKey.checkSum)

            EMV_PUBLIC_KEY_S1_F20_CA_PUBLIC_KEY_EXPIRY_DATE -> {
                result = getValueFromLabelValue(emvKey.expiryDate)
            }

            EMV_PUBLIC_KEY_S1_F14_EXPONENT -> result = getValueFromLabelValue(emvKey.exponent)

            EMV_PUBLIC_KEY_S1_F8_HASH_ID -> result = getValueFromLabelValue(emvKey.hashId)

            EMV_PUBLIC_KEY_S1_F6_KEY_INDEX -> result = getValueFromLabelValue(emvKey.index)

            EMV_PUBLIC_KEY_S1_F10_DIGITAL_SIG_ID -> {
                result = getValueFromLabelValue(emvKey.keySignatureId)
            }

            EMV_PUBLIC_KEY_S1_F18_CA_PUBLIC_KEY_LENGTH -> {
                result = getValueFromLabelValue(emvKey.length)
            }

            EMV_PUBLIC_KEY_S1_F12_PUBLIC_KEY -> result = getValueFromLabelValue(emvKey.modules)

            EMV_PUBLIC_KEY_S1_F4_RID -> result = getValueFromLabelValue(emvKey.rid)

            else -> result = ""
        }
        return result
    }

    private fun getRespectiveAttributeForCardScheme(
        p0: CardSchemeRetrieveRequest
    ): String {
        val attribute: String
        when (p0.fieldAttributes) {
            CARD_SCHEME_S1_F4_CARD_SCHEME_ID,
            CARD_SCHEME_S2_F4_CARD_SCHEME_ID,
            CARD_SCHEME_S3_F4_CARD_SCHEME_ID -> {
                attribute = "cardSchemeId"
            }

            CARD_SCHEME_S1_F6_CARD_SCHEME_NAME_ARABIC,
            CARD_SCHEME_S1_F8_CARD_SCHEME_NAME_ENGLISH -> {
                attribute = "cardSchemeName"
            }

            CARD_SCHEME_S1_F10_CARD_SCHEME_ACQUIRER_ID -> {
                attribute = "cardSchemeAcquirerId"
            }

            CARD_SCHEME_S1_F20_CHECK_SERVICE_CODE -> {
                attribute = "checkServiceCode"
            }

            CARD_SCHEME_S2_F6_TRANSACTIONS_ALLOWED -> {
                attribute = "transactionAllowed"
            }

            CARD_SCHEME_S2_F8_CARDHOLDER_AUTHENTICATION -> {
                attribute = "cardHolderAuthentication"
            }

            CARD_SCHEME_S2_F10_SUPERVISOR_FUNCTIONS -> {
                attribute = "supervisorFunction"
            }

            CARD_SCHEME_S2_F12_MANUAL_ENTRY_ALLOWED -> {
                attribute = "manualEntryAllowed"
            }

            CARD_SCHEME_S2_F20_MAXM_CASH_BACK -> {
                attribute = "maximumCashBack"
            }

            CARD_SCHEME_S2_F26_LUHN_CHECK -> {
                attribute = "luhnCheck"
            }

            CARD_SCHEME_S3_F6_CARD_RANGES -> {
                attribute = "cardRanges"
            }

            CARD_SCHEME_S3_F10_IIN -> {
                attribute = "iin"
            }

            else -> attribute = ""
        }
        return attribute
    }

    private fun prepareCardSchemeRetrieveResponse(
        request: CardSchemeRetrieveRequest,
        attribute: String
    ): CardSchemeRetrieveResponse {
        var response =
            CardSchemeRetrieveResponse(
                ""
            )
        if (attribute.isNotEmpty()) {
            val data = getValueFieldFromCardSchemeTable(request, attribute)
            response =
                CardSchemeRetrieveResponse(
                    data
                )
        }
        return response
    }

    private fun getValueFieldFromCardSchemeTable(
        request: CardSchemeRetrieveRequest,
        attribute: String
    ): String {
        var data = AppDatabase
            .getInstance(instance.applicationContext)
            .getCardSchemeDao()
            .getByCardSchemeTypeAndAttribute(
                request.cardSchemeType,
                attribute
            )
        if (data == null) {
            data = ""
        }
        return data
    }

    private fun getValueFromLabelValue(
        item: String?
    ): String {
        var value = ""
        if (item != null) {
            val labelValue = Jsons.fromJsonToObj(item, LabelValue::class.java)
            if (labelValue?.value != null) {
                value = labelValue.value.toString()
            }
        }
        return value
    }
}