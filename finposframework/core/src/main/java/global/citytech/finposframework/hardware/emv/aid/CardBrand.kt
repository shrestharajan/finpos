package global.citytech.finposframework.hardware.emv.aid

/**
 * Created by Rishav Chudal on 5/3/20.
 */
enum class CardBrand(val rid: String) {
    VISA("A000000003"),
    AMEX("A000000025"),
    MASTERCARD("A000000004"),
    UNION_PAY("A000000333"),
    MADA("A000000228")
}

enum class CardBrandAID(val aid: String) {
    MADA_MC("A0000002281010"),
    MADA_VS("A0000002282010")
}