package global.citytech.finpos.merchant.presentation.admin.batchconfigure

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.R
import global.citytech.finposframework.log.Logger
import kotlinx.android.synthetic.main.dialog_login.*
import kotlinx.android.synthetic.main.dialog_login.btn_cancel
import kotlinx.android.synthetic.main.dialog_login.btn_confirm
import kotlinx.android.synthetic.main.dialog_login.tv_title
import kotlinx.android.synthetic.main.dialog_batch_configure.*

/**
 * Created by Rishav Chudal on 6/24/21.
 */
class SettlementBatchConfigureDialog: DialogFragment() {
    private var existingBatchNumber: String? = null
    private lateinit var listener: SettlementBatchConfigureDialogListener
    private val logger = Logger(SettlementBatchConfigureDialog::class.java)
    private var configuringBatchNumber: Int = 0

    companion object {
        const val TAG = "SettlementBatchConfigureDialog"
        const val BUNDLE_DATA_CURRENT_BATCH = "current batch number"

        fun newInstance(currentBatchNumber: String): SettlementBatchConfigureDialog {
            val settlementBatchConfigureDialog = SettlementBatchConfigureDialog()
            val bundle = Bundle()
            bundle.putString(BUNDLE_DATA_CURRENT_BATCH, currentBatchNumber)
            settlementBatchConfigureDialog.arguments = bundle
            return settlementBatchConfigureDialog
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        hideDialogDefaultTitle()
        return inflater.inflate(R.layout.dialog_batch_configure, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handlePageItems()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as SettlementBatchConfigureDialogListener
        } catch (e: ClassCastException) {
            this.logger.log("ClassCastException ::: Activity Should implement SettlementBatchConfigureDialogListener")
        }
    }

    override fun onResume() {
        super.onResume()
        setDialogLayoutParams()
    }

    private fun hideDialogDefaultTitle() {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
    }

    private fun setDialogLayoutParams() {
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
        layoutParams.dimAmount = 0.5f
        dialog?.window?.attributes = layoutParams
        dialog?.window?.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
    }

    private fun handlePageItems() {
        getBundleData()
        initViews()
    }

    private fun getBundleData() {
        val bundle = arguments
        checkIfBundleIsNullAndProceed(bundle)
    }

    private fun checkIfBundleIsNullAndProceed(bundle: Bundle?) {
        if (bundle != null) {
            getBatchNumberFromBundleData(bundle)
        } else {
            assignDefaultBatchNumber()
        }
    }

    private fun getBatchNumberFromBundleData(bundle: Bundle) {
        this.existingBatchNumber = bundle.getString(
            BUNDLE_DATA_CURRENT_BATCH,
            getString(R.string.msg_default_existing_batch_number)
        )
    }

    private fun assignDefaultBatchNumber() {
        this.existingBatchNumber = getString(R.string.msg_default_existing_batch_number)
    }

    private fun initViews() {
        initBatchNumberTextView()
        initPageButtons()
    }

    private fun initBatchNumberTextView() {
        tv_message.text = getString(R.string.msg_current_batch_number).plus(existingBatchNumber)
    }

    private fun initPageButtons() {
        initAndHandleCancelButton()
        initAndHandleConfirmButton()
    }

    private fun initAndHandleCancelButton() {
        this.btn_cancel.setOnClickListener {
            dismiss()
        }
    }

    private fun initAndHandleConfirmButton() {
        this.btn_confirm.setOnClickListener {
            validateInputBatchNumber(text_input_et_batch_number.text.toString().trim())
        }
    }

    fun updateDialogTitle(pageTitle: String) {
        this.tv_title.text = pageTitle
    }

    private fun validateInputBatchNumber(inputBatchNumber: String) {
        when {
            inputBatchNumber.isNullOrEmptyOrBlank() -> {
                showToast(getString(R.string.msg_batch_number_cannot_be_empty))
            }
            inputBatchNumberIsZero(inputBatchNumber) -> {
                showToast(getString(R.string.msg_enter_valid_batch_number))
            }
            inputBatchNumberIsLessThanExisting(inputBatchNumber) -> {
                showToast(getString(R.string.msg_enter_valid_batch_number))
            }
            else -> dismissDialogAndProceed(inputBatchNumber)
        }
    }

    private fun inputBatchNumberIsZero(inputBatchNumber: String): Boolean {
        val batchNumber = inputBatchNumber.toInt()
        return batchNumber == 0
    }

    private fun inputBatchNumberIsLessThanExisting(inputBatchNumber: String): Boolean {
        val existingBatchNumber = existingBatchNumber!!.toInt()
        val currentBatchNumber = inputBatchNumber.toInt()
        return (currentBatchNumber < existingBatchNumber)
    }

    private fun dismissDialogAndProceed(inputBatchNumber: String) {
        dismiss()
        listener.onBatchConfigureConfirmButtonClicked(inputBatchNumber.toInt())
    }

    private fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    interface SettlementBatchConfigureDialogListener {
        fun onBatchConfigureConfirmButtonClicked(batchNumber: Int)
    }
}