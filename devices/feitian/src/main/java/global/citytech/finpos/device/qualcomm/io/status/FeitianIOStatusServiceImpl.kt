package global.citytech.finpos.device.qualcomm.io.status

import android.content.Context
import com.ftpos.library.smartpos.errcode.ErrCode
import com.ftpos.library.smartpos.icreader.IcReader
import com.ftpos.library.smartpos.magreader.MagReader
import com.ftpos.library.smartpos.nfcreader.NfcReader
import com.ftpos.library.smartpos.printer.PrintStatus
import com.ftpos.library.smartpos.printer.Printer
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finposframework.hardware.io.status.IOStatusRequest
import global.citytech.finposframework.hardware.io.status.IOStatusResponse
import global.citytech.finposframework.hardware.io.status.IOStatusService
import global.citytech.finposframework.hardware.utility.DeviceApiConstants
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_ICC_READER_OK
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_ICC_READER_OUT_OF_ORDER
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_MAG_READER_OK
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_MAG_READER_OUT_OF_ORDER
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_PICC_READER_OK
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_PICC_READER_OUT_OF_ORDER
import global.citytech.finposframework.hardware.utility.DeviceApiConstants.CORE_PRINTER_NOT_AVAILABLE

/**
 * Created by Rishav Chudal on 4/13/21.
 */
class FeitianIOStatusServiceImpl(val context: Context):  IOStatusService, FeitianLogger.Listener{

    private val printer = Printer.getInstance(this.context.applicationContext)
    private lateinit var magReader: MagReader
    private lateinit var iccReader: IcReader
    private lateinit var piccReader: NfcReader
    private val logger = FeitianLogger(this)
    private lateinit var ioStatusResponse: IOStatusResponse

    override fun retrieveHardwareStatus(request: IOStatusRequest): IOStatusResponse {
        this.logger.logWithClassName("Retrieving hardware status...")
        this.ioStatusResponse = IOStatusResponse(
            CORE_PRINTER_NOT_AVAILABLE,
            CORE_MAG_READER_OUT_OF_ORDER,
            CORE_ICC_READER_OUT_OF_ORDER,
            CORE_PICC_READER_OUT_OF_ORDER
        )
        if (request.isPrinterOnly) {
            retrievePrinterStatusOnly()
        } else {
            retrieveAllComponentStatus()
        }
        return this.ioStatusResponse
    }

    private fun retrievePrinterStatusOnly() {
        this.logger.logWithClassName("Retrieving the printer status only...")
        val printerStatus = retrievePrinterStatus()
        this.ioStatusResponse = IOStatusResponse(
            printerStatus,
            CORE_MAG_READER_OK,
            CORE_ICC_READER_OK,
            CORE_PICC_READER_OK
        )
    }

    private fun retrieveAllComponentStatus() {
        this.logger.logWithClassName("Retrieving all component status...")
        val printerStatus = retrievePrinterStatus()
        val magReaderStatus = retrieveMagReaderStatus()
        val iccReaderStatus = retrieveICCReaderStatus()
        val piccReaderStatus = retrievePICCReaderStatus()
        this.ioStatusResponse = IOStatusResponse(
            printerStatus,
            magReaderStatus,
            iccReaderStatus,
            piccReaderStatus
        )
    }

    private fun retrievePrinterStatus(): String {
        this.logger.logWithClassName("Retrieving Printer Status...")
        var status: String = CORE_PRINTER_NOT_AVAILABLE
        val printStatus = PrintStatus()
        val resultPrinterStatusApi = printer.getStatus(printStatus)
        if (resultPrinterStatusApi == ErrCode.ERR_SUCCESS) {
            if (printStatus.getmIsHavePaper()) {
                status = DeviceApiConstants.CORE_PRINTER_PLAIN_RECEIPT_PAPER
                this.logger.logWithClassName("Printer Status is Ok...")
            } else {
                status = DeviceApiConstants.CORE_PRINTER_OUT_OF_PAPER
                this.logger.logWithClassName("Printer Status is Out of Paper...")
            }
        }
        return status
    }

    private fun retrieveMagReaderStatus(): String {
        this.magReader = MagReader.getInstance(this.context.applicationContext)
        this.logger.logWithClassName("Retrieving Mag Reader Status...")
        var status: String = CORE_MAG_READER_OUT_OF_ORDER
        val resultMagStatusApi = magReader.checkMagCardreader()
        if (resultMagStatusApi == ErrCode.ERR_SUCCESS) {
            status = CORE_MAG_READER_OK
            this.logger.logWithClassName("Mag Reader Status is Ok...")
        }
        return status
    }

    private fun retrieveICCReaderStatus(): String {
        this.iccReader = IcReader.getInstance(this.context.applicationContext)
        this.logger.logWithClassName("Retrieving ICC Reader Status...")
        var status: String = CORE_ICC_READER_OUT_OF_ORDER
        val resultMagStatusApi = iccReader.checkICReader()
        if (resultMagStatusApi == ErrCode.ERR_SUCCESS) {
            status = CORE_ICC_READER_OK
            this.logger.logWithClassName("ICC Reader Status is Ok...")
        }
        return status
    }

    private fun retrievePICCReaderStatus(): String {
        this.piccReader = NfcReader.getInstance(this.context.applicationContext)
        this.logger.logWithClassName("Retrieving PICC Reader Status...")
        var status: String = CORE_PICC_READER_OUT_OF_ORDER
        val resultMagStatusApi = piccReader.checkNFCCardreader()
        if (resultMagStatusApi == ErrCode.ERR_SUCCESS) {
            status = CORE_PICC_READER_OK
            this.logger.logWithClassName("PICC Reader Status is Ok...")
        }
        return status
    }

    override fun getClassName() = FeitianIOStatusServiceImpl::class.java.simpleName
}