package global.citytech.finpos.merchant.presentation.mobilenfcpayment.receipt

import global.citytech.finpos.merchant.domain.model.app.MobileNfcPayment
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.FAILED_TRANSACTION
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.TRANSACTION_VOID_SALE
import global.citytech.finpos.merchant.utils.retrieveCurrencyName
import global.citytech.finposframework.repositories.TerminalRepository
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.receipt.Performance
import global.citytech.finposframework.usecases.transaction.receipt.Retailer
import global.citytech.finposframework.utility.StringUtils

class MobileNfcPaymentReceiptGenerator(val terminalRepository: TerminalRepository) {
    private lateinit var initiatorId: String

    fun generate(mobileNfcPayment: MobileNfcPayment): MobileNfcPaymentReceipt =
        MobileNfcPaymentReceipt(
            prepareRetailerInfo(mobileNfcPayment),
            preparePerformanceInfo(mobileNfcPayment),
            prepareMobileNfcTransactionDetail(mobileNfcPayment),
            prepareThankYouMessage()
        )

    private fun prepareThankYouMessage(): String = MobileNfcPaymentReceiptLabel.THANK_YOU_MESSAGE

    private fun prepareMobileNfcTransactionDetail(
        mobileNfcPayment: MobileNfcPayment
    ): MobileNfcTransactionDetail {
        var approvalCode = ""
        var transactionType: String

        mobileNfcPayment.let { response ->
            if (response.transactionStatus.lowercase() != FAILED_TRANSACTION) {
                approvalCode = response.approvalCode.toString()
            }

            transactionType =
                if (response.transactionType.lowercase() == TRANSACTION_VOID_SALE) {
                    TransactionType.VOID.printName
                } else {
                    response.transactionType
                }

            initiatorId = response.initiatorId.toString()

            initiatorId =
                when (initiatorId.length) {
                    15 -> StringUtils.maskPayerPanNumber(initiatorId)
                    14 -> maskInitiatorId(8)
                    13 -> maskInitiatorId(7)
                    12 -> maskInitiatorId(6)
                    11 -> maskInitiatorId(5)
                    10 -> maskInitiatorId(4)
                    else -> initiatorId
                }

            return MobileNfcTransactionDetail(
                response.referenceNumber,
                transactionType,
                retrieveCurrencyName(mobileNfcPayment.transactionCurrency),
                StringUtils.formatAmountTwoDecimal(mobileNfcPayment.transactionAmount),
                response.transactionStatus,
                response.paymentInitiator.toString(),
                initiatorId,
                approvalCode
            )
        }
    }

    private fun maskInitiatorId(start: Int): String {
        return StringUtils.maskString(initiatorId, start, initiatorId.length - 2, 'X')
    }

    private fun preparePerformanceInfo(mobileNfcPayment: MobileNfcPayment): Performance =
        Performance.Builder()
            .withStartDateTime(mobileNfcPayment.transactionDate.plus(mobileNfcPayment.transactionTime))
            .withEndDateTime(mobileNfcPayment.transactionDate.plus(mobileNfcPayment.transactionTime))
            .build()

    private fun prepareRetailerInfo(mobileNfcPayment: MobileNfcPayment): Retailer =
        Retailer.Builder()
            .withRetailerLogo(this.terminalRepository.findTerminalInfo().merchantPrintLogo)
            .withRetailerName(this.terminalRepository.findTerminalInfo().merchantName)
            .withRetailerAddress(this.terminalRepository.findTerminalInfo().merchantAddress)
            .withMerchantId(mobileNfcPayment.merchantId)
            .withTerminalId(mobileNfcPayment.terminalId)
            .build()
}