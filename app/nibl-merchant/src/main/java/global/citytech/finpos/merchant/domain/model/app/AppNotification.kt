package global.citytech.finpos.merchant.domain.model.app

import global.citytech.finpos.merchant.R

/**
 * Created by Unique Shakya on 5/28/2021.
 */
data class AppNotification(
    val title: String? = "",
    val body: String? = null,
    val action: Action,
    val read: Boolean,
    val type: Type,
    val timeStamp: String,
    val referenceId: String? = ""
)

enum class Action(val positiveAction: String, val negativeAction: String) {
    SETTLEMENT("Retry", "Cancel"),
    DEVICE_CONFIGURATION("Reconfigure", "Cancel"),
    TIP_LIMIT_CONFIGURATION("Reconfigure", "Cancel"),
    GENERAL("OK", ""),
    QR_DISCLAIMER("ACCEPT","MAY BE LATER"),
    DISCLAIMER("ACCEPT", "MAY BE LATER")
}

enum class Type(val displayImageId: Int) {
    WARNING(R.drawable.ic_baseline_warning_24),
    SUCCESS(R.drawable.ic_baseline_check_circle_24),
    FAILED(R.drawable.ic_baseline_error_24)
}