package global.citytech.finpos.merchant.domain.model.purchase


/**
 * Created by Saurav Ghimire on 1/20/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
data class RefundResponseEntity(
    val stan: String? = null,
    val message: String? = null,
    val shouldPrintCustomerCopy: Boolean? = false,
    val isApproved: Boolean? = false
)