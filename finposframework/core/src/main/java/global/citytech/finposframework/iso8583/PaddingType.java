package global.citytech.finposframework.iso8583;

public enum PaddingType {
    NONE,
    PREFIX_WITH_CHAR_ZERO,
    PREFIX_WITH_CHAR_F,
    POSTFIX_WITH_CHAR_ZERO,
    POSTFIX_WITH_CHAR_F;

    public static class PaddingChar {
        public static String CHAR_ZERO = "0";
        public static String CHAR_F = "f";
    }
}
