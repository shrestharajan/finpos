package global.citytech.finpos.processor.nibl.transaction.cash.advance;

import global.citytech.finpos.processor.nibl.transaction.TransactionRequestModel;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.io.sound.SoundService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;

/**
 * Created by Unique Shakya on 2/4/2021.
 */

public class CashAdvanceRequestModel extends TransactionRequestModel {

    public static final class Builder {
        private TransactionRequest transactionRequest;
        private TransactionRepository transactionRepository;
        private ReadCardService readCardService;
        private DeviceController deviceController;
        private TransactionAuthenticator transactionAuthenticator;
        private PrinterService printerService;
        private ApplicationRepository applicationRepository;
        private ReconciliationRepository reconciliationRepository;
        private LedService ledService;
        private SoundService soundService;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withTransactionRequest(TransactionRequest transactionRequest) {
            this.transactionRequest = transactionRequest;
            return this;
        }

        public Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public Builder withReadCardService(ReadCardService readCardService) {
            this.readCardService = readCardService;
            return this;
        }

        public Builder withDeviceController(DeviceController deviceController) {
            this.deviceController = deviceController;
            return this;
        }

        public Builder withTransactionAuthenticator(TransactionAuthenticator transactionAuthenticator) {
            this.transactionAuthenticator = transactionAuthenticator;
            return this;
        }

        public Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public Builder withReconciliationRepository(ReconciliationRepository reconciliationRepository) {
            this.reconciliationRepository = reconciliationRepository;
            return this;
        }

        public Builder withLedService(LedService ledService) {
            this.ledService = ledService;
            return this;
        }

        public Builder withSoundService(SoundService soundService) {
            this.soundService = soundService;
            return this;
        }

        public CashAdvanceRequestModel build() {
            CashAdvanceRequestModel cashAdvanceRequestModel = new CashAdvanceRequestModel();
            cashAdvanceRequestModel.applicationRepository = this.applicationRepository;
            cashAdvanceRequestModel.deviceController = this.deviceController;
            cashAdvanceRequestModel.transactionAuthenticator = this.transactionAuthenticator;
            cashAdvanceRequestModel.soundService = this.soundService;
            cashAdvanceRequestModel.transactionRepository = this.transactionRepository;
            cashAdvanceRequestModel.reconciliationRepository = this.reconciliationRepository;
            cashAdvanceRequestModel.readCardService = this.readCardService;
            cashAdvanceRequestModel.printerService = this.printerService;
            cashAdvanceRequestModel.ledService = this.ledService;
            cashAdvanceRequestModel.transactionRequest = this.transactionRequest;
            return cashAdvanceRequestModel;
        }
    }
}
