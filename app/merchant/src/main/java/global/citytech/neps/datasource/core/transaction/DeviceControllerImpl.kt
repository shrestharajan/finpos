package global.citytech.neps.datasource.core.transaction

import android.app.Application
import global.citytech.finpos.core.api.DeviceApiConstants
import global.citytech.finpos.core.api.DeviceServiceType
import global.citytech.finpos.core.api.hardware.HardwareStatusRequest
import global.citytech.finpos.core.api.hardware.HardwareStatusResponse
import global.citytech.finpos.core.api.hardware.HardwareStatusService
import global.citytech.neps.service.AppCoreFactory
import global.citytech.posswitchintregator.card.data.CardType
import global.citytech.posswitchintregator.device.DeviceController
import global.citytech.posswitchintregator.utils.PosError
import global.citytech.posswitchintregator.utils.PosResponse
import global.citytech.posswitchintregator.utils.PosResult

class DeviceControllerImpl(val instance: Application) :DeviceController {
    override fun isReady(p0: MutableList<CardType>?): PosResponse {
        val request = prepareHardwareStatusRequest(p0)
        val service = AppCoreFactory.getCore(
            instance.applicationContext,
            DeviceServiceType.HARDWARE_STATUS
        ) as HardwareStatusService
        val hardwareStatusResponse = service.retrieveHardwareStatus(request)
        return preparePosResponse(p0, hardwareStatusResponse)
    }

    private fun prepareHardwareStatusRequest(
        cardTypeList: MutableList<CardType>?
    ): HardwareStatusRequest {
        var hardwareStatusRequest = HardwareStatusRequest(true)
        if (cardTypeList != null
            && doCardTypeListContainAnyOfTheHardwareCardReader(cardTypeList)
        ) {
            hardwareStatusRequest = HardwareStatusRequest(false)
        }
        return hardwareStatusRequest
    }

    private fun preparePosResponse(
        cardTypeList: MutableList<CardType>?,
        hardwareStatusResponse: HardwareStatusResponse
    ): PosResponse {
        var posResponse = getFailurePosResponse()

        if (cardTypeList == null
            || !doCardTypeListContainAnyOfTheHardwareCardReader(cardTypeList)) {
            if (isPrinterOkay(hardwareStatusResponse)) {
                posResponse = getSuccessPosResponse()

            }
        } else {
            posResponse = manageIfCardTypeListIsNotNull(cardTypeList, hardwareStatusResponse)
        }
        return posResponse
    }

    private fun manageIfCardTypeListIsNotNull(
        cardTypeList: MutableList<CardType>,
        hardwareStatusResponse: HardwareStatusResponse
    ) : PosResponse{
        var posResponse = getFailurePosResponse()
        if (isPrinterOkay(hardwareStatusResponse)) {
            posResponse = iterateForCardReaderStatus(cardTypeList, hardwareStatusResponse)
        }
        return posResponse
    }

    private fun iterateForCardReaderStatus(
        cardTypeList: MutableList<CardType>,
        hardwareStatusResponse: HardwareStatusResponse
    ): PosResponse {
        var posResponse = getFailurePosResponse()
        iteration@for (cardType in cardTypeList) {
            when (cardType) {
                CardType.MAG -> {
                    posResponse = manageOnCardTypeMag(hardwareStatusResponse)
                    if (posResponse.posResult == PosResult.FAILURE) {
                        break@iteration
                    }
                }
                CardType.ICC -> {
                    posResponse = manageOnCardTypeIcc(hardwareStatusResponse)
                    if (posResponse.posResult == PosResult.FAILURE) {
                        break@iteration
                    }
                }
                CardType.PICC -> {
                    posResponse = manageOnCardTypePicc(hardwareStatusResponse)
                    if (posResponse.posResult == PosResult.FAILURE) {
                        break@iteration
                    }
                }
                else -> posResponse = getFailurePosResponse()
            }
        }
        return posResponse
    }

    private fun manageOnCardTypeMag(hardwareStatusResponse: HardwareStatusResponse): PosResponse {
        return if (isMagneticReaderOkay(hardwareStatusResponse)) {
            getSuccessPosResponse()
        } else {
            getFailurePosResponse()
        }
    }

    private fun manageOnCardTypeIcc(hardwareStatusResponse: HardwareStatusResponse): PosResponse {
        return if (isICCReaderOkay(hardwareStatusResponse)) {
            getSuccessPosResponse()
        } else {
            getFailurePosResponse()
        }
    }

    private fun manageOnCardTypePicc(hardwareStatusResponse: HardwareStatusResponse): PosResponse {
        return if (isPICCReaderOkay(hardwareStatusResponse)) {
            getSuccessPosResponse()
        } else {
            getFailurePosResponse()
        }
    }

    private fun doCardTypeListContainAnyOfTheHardwareCardReader(
        cardTypeList: MutableList<CardType>
    ) : Boolean {
        var result = false
        if (cardTypeList.contains(CardType.MAG)
            || cardTypeList.contains(CardType.ICC)
            || cardTypeList.contains(CardType.PICC)) {
            result = true
        }
        return result
    }

    private fun isPrinterOkay(hardwareStatusResponse: HardwareStatusResponse): Boolean {
        var result = false
        if(hardwareStatusResponse.printerStatus.equals(
                DeviceApiConstants.CORE_PRINTER_PLAIN_RECEIPT_PAPER
            )) {
            result = true
        }
        return result
    }

    private fun isMagneticReaderOkay(
        hardwareStatusResponse: HardwareStatusResponse
    ): Boolean {
        var result = false
        if(hardwareStatusResponse.magReaderStatus.equals(
                DeviceApiConstants.CORE_MAG_READER_OK
            )) {
            result = true
        }
        return result
    }

    private fun isICCReaderOkay(
        hardwareStatusResponse: HardwareStatusResponse
    ): Boolean {
        var result = false
        if(hardwareStatusResponse.contactReaderStatus.equals(
                DeviceApiConstants.CORE_CONTACT_READER_OK
            )) {
            result = true
        }
        return result
    }

    private fun isPICCReaderOkay(
        hardwareStatusResponse: HardwareStatusResponse
    ): Boolean {
        var result = false
        if(hardwareStatusResponse.contactlessReaderStatus.equals(
                DeviceApiConstants.CORE_CONTACTLESS_READER_OK
            )) {
            result = true
        }
        return result
    }

    private fun getSuccessPosResponse() = PosResponse(
        PosResult.SUCCESS,
        0,
        "Pos is Ready"
    )

    private fun getFailurePosResponse() = PosResponse(
        PosResult.FAILURE,
        PosError.DEVICE_ERROR_NOT_READY.errorCode,
        PosError.DEVICE_ERROR_NOT_READY.errorMsg
    )

}