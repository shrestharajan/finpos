package global.citytech.finpos.merchant.presentation.config

import android.app.Application
import android.os.Build
import androidx.lifecycle.MutableLiveData
import global.citytech.common.data.Response
import global.citytech.common.extensions.hasInternetConnection
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.AppNotificationRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.MobileNfcPaymentRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.QrPaymentRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.configuration.ConfigurationRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.posmode.PosModeRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.Action
import global.citytech.finpos.merchant.domain.model.app.AppNotification
import global.citytech.finpos.merchant.domain.model.app.Disclaimer
import global.citytech.finpos.merchant.domain.model.app.Type
import global.citytech.finpos.merchant.domain.model.device.DeviceResponse
import global.citytech.finpos.merchant.domain.model.device.DeviceResult
import global.citytech.finpos.merchant.domain.usecase.app.AppNotificationUseCase
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.app.MobilePaymentUseCase
import global.citytech.finpos.merchant.domain.usecase.app.QrPaymentUseCase
import global.citytech.finpos.merchant.domain.usecase.core.configuration.CoreConfigurationUseCase
import global.citytech.finpos.merchant.domain.usecase.core.posmode.CorePosModeUseCase
import global.citytech.finpos.merchant.framework.datasource.app.*
import global.citytech.finpos.merchant.framework.datasource.core.configuration.ConfigurationDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.posmode.PosModeDataSourceImpl
import global.citytech.finpos.merchant.presentation.admin.AdminViewModel
import global.citytech.finpos.merchant.presentation.admin.DeviceConfigurationStatus
import global.citytech.finpos.merchant.presentation.alarms.SettlementAlarmSetter
import global.citytech.finpos.merchant.presentation.model.*
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.exceptions.PosException
import global.citytech.finposframework.log.Logger
import global.citytech.finpos.merchant.presentation.model.setting.TerminalSettingResponse
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finpos.merchant.utils.AppUtility.savePushNotificationConfiguration
import global.citytech.finposframework.utility.StringUtils
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ConfigViewModel(val context: Application) : BaseAndroidViewModel(context) {

    private val logger = Logger(AdminViewModel::class.java.name)
    private val TAG = ConfigViewModel::class.java.name
    val serviceLoaded by lazy { MutableLiveData<Boolean>() }
    val registrationComplete by lazy { MutableLiveData<Boolean>() }
    val deviceConfigurationStatus by lazy { MutableLiveData<DeviceConfigurationStatus>() }
    val navigateToOperatorLoginMessage by lazy { MutableLiveData<String>() }

    var localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    private val qrPaymentUseCase = QrPaymentUseCase(
        QrPaymentRepositoryImpl(
            QrPaymentDataSourceImpl(context)
        )
    )

    private val mobileNfcPaymentUseCase = MobilePaymentUseCase(
        MobileNfcPaymentRepositoryImpl(
            MobileNfcPaymentDataSourceImpl(
                context
            )
        )
    )

    var posModelUseCase = CorePosModeUseCase(PosModeRepositoryImpl(PosModeDataSourceImpl()))

    private val CONFIG_URL = "/devices-terminal/v1/configs/public/switch"
    private fun prepareConfigRequest(): ConfigRequest {
        return ConfigRequest(
            AppInfo(
                "NiblAdmin",
                1
            ), DeviceInfo(NiblMerchant.INSTANCE.iPlatformManager.serialNumber)
        )
    }

    var configurationUseCase: CoreConfigurationUseCase =
        CoreConfigurationUseCase(
            ConfigurationRepositoryImpl(
                ConfigurationDataSourceImpl()
            )
        )

    val appNotificationUseCase = AppNotificationUseCase(
        AppNotificationRepositoryImpl(
            AppNotificationDataSourceImpl(context)
        )
    )


    private val configRequest =
        ConfigRequest(
            AppInfo(
                "NiblAdmin",
                1
            ),
            DeviceInfo(
                Build.SERIAL
            )
        )

    fun register() {
        isLoading.value = true
        deviceConfigurationStatus.value = DeviceConfigurationStatus.DownloadingConfiguration
        if (!context.hasInternetConnection()) {
            printLog(TAG, "No Internet Connection")
            message.value = "No Internet Connection"
            deviceConfigurationStatus.value = DeviceConfigurationStatus.ConfigurationCanceled
            return
        }
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<ConfigResponse> {
                onSubscribe(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onNext, this::onError)
        )
    }

    private fun onSubscribe(emitter: ObservableEmitter<ConfigResponse>) {
        val jsonRequest = Jsons.toJsonObj(prepareConfigRequest())
        val jsonResponse =
            NiblMerchant.INSTANCE.iPlatformManager.post(CONFIG_URL, jsonRequest, true)
        printLog(TAG, "::: PATH >>> $, CONFIG_URL", true)
        printLog(TAG, "::: REQUEST >>> $jsonRequest", true)
        printLog(TAG, "::: RESPONSE >>> $jsonResponse", true)
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            prepareResponse(emitter, response.data as Any)
        } else {
            if (response.code == "401")
                emitter.onError(PosException(PosError.DEVICE_ERROR_OPERATOR_LOGIN_REQUIRED))
            else
                emitter.onError(Exception("Error Code :: ${response.code}. ${response.message}"))
        }
        emitter.onComplete()
    }

    private fun prepareResponse(emitter: ObservableEmitter<ConfigResponse>, data: Any) {
        val responseJson = Jsons.toJsonObj(data)
        val response = Jsons.fromJsonToObj(responseJson, ConfigResponse::class.java)
        emitter.onNext(response)
    }


    private fun onNext(configResponse: ConfigResponse) {
        deviceConfigurationStatus.value = DeviceConfigurationStatus.SavingParameters
        this.assignPosMode(configResponse)
        saveInMerchantDatabase(configResponse)
        saveBanners(configResponse)
        if (configResponse.terminalSettings?.enableAutoSettlement == true
            && !configResponse.terminalSettings?.settlementTime.isNullOrEmpty()
        ) {
            setSettlementTimeAlarm(configResponse.terminalSettings?.settlementTime)
        } else {
            PreferenceManager.storeSettlementTime("")
            NiblMerchant.INSTANCE.cancelRescheduledSettlement()
        }
        saveTerminalSettings(configResponse.terminalSettings)
        configResponse.terminalSettings?.let {
            it.pushNotificationConfiguration?.let { it1 ->
                savePushNotificationConfiguration(
                    it1
                )
            }
        }
        registrationComplete.value = true
    }


    private fun saveTerminalSettings(terminalSettings: TerminalSettingResponse?) {
        if (terminalSettings != null) {
            localDataUseCase.saveTerminalSetting(terminalSettings)
            addQrDisclaimerNotification(terminalSettings)
            handleDisclaimerList(terminalSettings.disclaimers)
            localDataUseCase.saveDebugModeStatus(terminalSettings.enableDebugLog)
            terminalSettings.qrBillNumber?.let { qrPaymentUseCase.saveBillingInvoiceNumber(it) }
            logger.log("::Nfc bill number :: " + terminalSettings.nfcBillNumber.toString())
            terminalSettings.nfcBillNumber?.let {
                mobileNfcPaymentUseCase.saveNfcPaymentIdentifiers(
                    it
                )
            }
            localDataUseCase.saveGreenPinStatus(terminalSettings.enableGreenPin)
            localDataUseCase.saveSoundModeStatus(terminalSettings.isSoundEnabled)
        }
    }

    private fun handleDisclaimerList(disclaimerList: List<Disclaimer>?) {
        if (disclaimerList != null && disclaimerList.isNotEmpty()) {
            compositeDisposable.add(
                localDataUseCase.addListToDisclaimers(disclaimerList)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        disclaimerList.forEach {
                            appNotificationUseCase.addNotification(
                                AppNotification(
                                    title = it.notificationTitle,
                                    body = it.notificationContent,
                                    type = Type.WARNING,
                                    action = Action.DISCLAIMER,
                                    read = false,
                                    timeStamp = StringUtils.dateTimeStamp(),
                                    referenceId = it.disclaimerId
                                )
                            )
                        }
                    }, {
                        printLog(TAG, "Error in handling disclaimer list ::: " + it.message)
                        it.printStackTrace()
                    })
            )
        }
    }

    private fun addQrDisclaimerNotification(terminalSettings: TerminalSettingResponse) {
        if (terminalSettings.showQrDisclaimer) {
            appNotificationUseCase.addNotification(
                AppNotification(
                    title = "",
                    body = context.getString(R.string.msg_qr_disclaimer_notification),
                    timeStamp = StringUtils.dateTimeStamp(),
                    action = Action.QR_DISCLAIMER,
                    read = false,
                    type = Type.WARNING
                )
            )
        } else {
            appNotificationUseCase.removeNotificationByAction(Action.QR_DISCLAIMER)
        }
    }

    private fun saveBanners(configResponse: ConfigResponse) {
        configResponse.logo?.let { logo ->
            logo.banners?.let {
                localDataUseCase.setBanners(it)
            }
        }
    }

    private fun assignPosMode(configResponse: ConfigResponse) {
        val posMode =
            posModelUseCase.getPosMode(configResponse.terminalParameter!!.emvParameters!!.merchantCategoryCode!!.value as String)
        localDataUseCase.savePosMode(posMode)
    }

    private fun saveInMerchantDatabase(configResponse: ConfigResponse) {
        deviceConfigurationStatus.value = DeviceConfigurationStatus.ConfiguringTerminal
        compositeDisposable.add(
            localDataUseCase.saveConfigResponseAndInjectKey(configResponse)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onInjected, this::onError)
        )
    }

    private fun setSettlementTimeAlarm(reconcileTime: String?) {
        reconcileTime?.let {
            val settlementAlarmSetter = SettlementAlarmSetter(context.applicationContext)
            settlementAlarmSetter.setRepeatingAlarm(it)
            this.localDataUseCase.storeSettlementTime(reconcileTime)
        }
    }

    private fun onInjected(deviceResponse: DeviceResponse) {
        isLoading.value = false
        deviceConfigurationStatus.value = DeviceConfigurationStatus.ParametersSaved
        if (deviceResponse.result == DeviceResult.SUCCESS) {
            getConfiguration()
        } else {
            deviceConfigurationStatus.value = DeviceConfigurationStatus.ConfigurationFailed
        }
    }

    private fun getConfiguration(): Boolean {
        this.isLoading.value = true
        return compositeDisposable.add(
            localDataUseCase.getWholeConfiguration()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.mapToPresentation() }
                .subscribe({
                    logOn(it!!)
                }, {
                    printLog(TAG, "Get Configuration error :::" + it.message)
                    isLoading.value = false
                    deviceConfigurationStatus.value =
                        DeviceConfigurationStatus.TerminalConfigured
                })
        )
    }

    private fun logOn(it: ConfigurationItem) {
        isLoading.value = true
        compositeDisposable.add(
            configurationUseCase.logOn(it)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    deviceConfigurationStatus.value = DeviceConfigurationStatus.TerminalConfigured
                }, {
                    printLog(TAG, "LOGON ERROR :: " + it.message.toString())
                    isLoading.value = false
                    it.printStackTrace()
                    deviceConfigurationStatus.value = DeviceConfigurationStatus.TerminalConfigured
                })
        )
    }

    private fun onError(throwable: Throwable) {
        throwable.printStackTrace()
        isLoading.value = false
        deviceConfigurationStatus.value = DeviceConfigurationStatus.ConfigurationCanceled
        if (throwable is PosException) {
            val posError = throwable.posError
            printLog(TAG, "LOGON ERROR :: " + posError.errorMessage)
            if (posError == PosError.DEVICE_ERROR_OPERATOR_LOGIN_REQUIRED)
                navigateToOperatorLoginMessage.value = posError.errorMessage
            else
                message.value = posError.errorMessage
        } else {
            printLog(TAG, throwable.message.toString())
            message.value = throwable.message
        }
    }

    fun setSwitchConfigActive(active: Boolean) {
        localDataUseCase.setSwitchConfigActive(active)
    }

}