package global.citytech.finposframework.hardware.io.printer

interface Printable {
    fun isAllow(): Boolean

    fun isImage(): Boolean

    fun isQrCode(): Boolean

    fun isFeedPaper(): Boolean

    fun isBase64Image(): Boolean

    fun isBarCode(): Boolean

    fun getData(): Any?
}