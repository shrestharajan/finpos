package global.citytech.finpos.nibl.iso8583.requestsender.balanceinquiry;

import global.citytech.finpos.nibl.iso8583.FunctionCodeConfig;
import global.citytech.finpos.nibl.iso8583.NIBLMessagePayloadBuilder;
import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.PosEntryModeConfig;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.switches.FunctionCode;

public class BalanceInquiryRequestSender extends NIBLMessageSenderTemplate {

    private final Logger logger = Logger.getLogger(BalanceInquiryRequestSender.class.getName());

    public BalanceInquiryRequestSender(SpecInfo specInfo, RequestContext context){
        super(specInfo, context);
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Authorization,
                        MTI.MessageFunction.Request,
                        MTI.Originator.Acquirer);
        TransactionIsoRequest balanceInquiryRequest= (TransactionIsoRequest) this.context.getRequest();
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);
        msg.addField(
                new DataElement(
                        DataElement.PRIMARY_ACCOUNT_NUMBER,
                        balanceInquiryRequest.getPan()
                )
        );
//        msg.addField(
//                new DataElement(
//                        DataElement.AMOUNT,
//                        balanceInquiryRequest.getTransactionAmount()
//                )
//        );
        msg.addField(
                new DataElement(
                        DataElement.LOCAL_TIME,
                        balanceInquiryRequest.getLocalTime()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.LOCAL_DATE,
                        balanceInquiryRequest.getLocalDate()
                )

        );
        msg.addField(
                new DataElement(
                        DataElement.PAN_EXPIRE_DATE,
                        balanceInquiryRequest.getExpireDate()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(balanceInquiryRequest.getPosEntryMode())
                )
        );

        msg.addField(
                new DataElement(
                        DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE,
                        balanceInquiryRequest.getPosConditionCode()
                )
        );
//        msg.addField(
//                new DataElement(
//                        DataElement.RETRIEVAL_REFERENCE_NUMBER,
//                        balanceInquiryRequest.getRetrievalReferenceNumber()
//                )
//        );
        msg.addField(
                new DataElement(
                        DataElement.PIN_BLOCK,
                        balanceInquiryRequest.getPinBlock()
                )
        );
//        msg.addField(
//                new DataElement(
//                        DataElement.PRIVATE_USE_FIELD_63,
//                        balanceInquiryRequest.getPinBlock()
//                )
//        );
        msg.addField(
                new DataElement(
                        DataElement.ICC_SYSTEM_RELATED_DATA,
                        balanceInquiryRequest.getEmvData()
                )
        );


        MessagePayload.Builder builder = new NIBLMessagePayloadBuilder(this.specInfo, msg);
        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());
        return  builder.build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.BALANCE_INQUIRY.getCode();
    }

    @Override
    protected String getFunctionCode() {
        return FunctionCodeConfig.getValue(FunctionCode.BALANCE_INQUIRY);
    }
}
