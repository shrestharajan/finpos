package global.citytech.finposframework.switches;

import global.citytech.finposframework.hardware.io.cards.CardType;

/***
 * Based on switch implementation it can be change.
 *
 * @author Surajchhetry
 *
 */
public enum PosEntryMode {
    ICC("ICC"),
    MANUAL("MANUAL"),
    MANUAL_WITHOUT_PIN("MANUAL"),
    PICC("CONTACT_LESS"),
    MAGNETIC_STRIPE("MAGNETIC_STRIPE"),
    ICC_FALLBACK_TO_MAGNETIC("MAGNETIC_STRIPE"),
    UNSPECIFIED("UNKNOWN");

    private String entryMode;

    PosEntryMode(String entryMode) {
        this.entryMode = entryMode;
    }

    public String getEntryMode() {
        return entryMode;
    }
}