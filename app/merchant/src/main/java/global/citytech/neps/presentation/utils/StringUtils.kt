package global.citytech.neps.presentation.utils

import java.text.DecimalFormat

/**
 * Created by Rishav Chudal on 6/17/20.
 */
class StringUtils {
    companion object {
        fun decimalFormatterFor(enteredAmount: Long): String {
            val doubleDF = DecimalFormat("#0.00")
            return doubleDF.format(enteredAmount / 100.00)
        }
    }
}