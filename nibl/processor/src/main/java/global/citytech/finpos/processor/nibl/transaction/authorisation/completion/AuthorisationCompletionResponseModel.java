package global.citytech.finpos.processor.nibl.transaction.authorisation.completion;

import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;

/**
 * Created by Unique Shakya on 9/29/2020.
 */
public class AuthorisationCompletionResponseModel extends TransactionResponseModel {

    private String debugRequestMessage;
    private String debugResponseMessage;
    private String message;
    private String stan;
    private boolean isApproved;
    private boolean shouldPrintCustomerCopy;

    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }

    public String getMessage() {
        return message;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public boolean shouldPrintCustomerCopy() {
        return shouldPrintCustomerCopy;
    }

    public String getStan() {
        return stan;
    }

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private String message;
        private String stan;
        private boolean isApproved;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withDebugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public Builder withDebugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder withIsApproved(boolean isApproved) {
            this.isApproved = isApproved;
            return this;
        }

        public Builder withShouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public AuthorisationCompletionResponseModel build() {
            AuthorisationCompletionResponseModel authorisationCompletionResponseModel = new AuthorisationCompletionResponseModel();
            authorisationCompletionResponseModel.debugRequestMessage = this.debugRequestMessage;
            authorisationCompletionResponseModel.debugResponseMessage = this.debugResponseMessage;
            authorisationCompletionResponseModel.isApproved = this.isApproved;
            authorisationCompletionResponseModel.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            authorisationCompletionResponseModel.message = this.message;
            authorisationCompletionResponseModel.stan = this.stan;
            return authorisationCompletionResponseModel;
        }
    }
}
