package global.citytech.finpos.merchant.presentation.model.cashadvance

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

/**
 * Created by Unique Shakya on 2/4/2021.
 */
data class ManualCashAdvanceRequestItem(
    val amount: BigDecimal? = null,
    val transactionType: TransactionType? = null,
    val cardNumber: String? = "",
    val expiryDate: String? = ""
) {
}