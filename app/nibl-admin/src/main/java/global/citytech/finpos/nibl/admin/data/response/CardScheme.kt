package global.citytech.finpos.nibl.admin.data.response

import android.net.Uri
import global.citytech.finpos.nibl.admin.presentation.main.MainViewModel

class CardScheme {
    companion object {

        val CARD_SCHEME_TABLE_NAME = "card_scheme"
        val URI = Uri.parse("content://${MainViewModel.AUTHORITY}/$CARD_SCHEME_TABLE_NAME")

        const val COLUMN_ID = "id"
        const val COLUMN_CARD_SCHEME_ID = "card_scheme_id"
        const val COLUMN_ATTRIBUTE = "attribute"
        const val COLUMN_DISPLAY_LABEL = "display_label"
        const val COLUMN_PRINT_LABEL = "print_Label"
        const val COLUMN_VALUE = "value"
    }
}