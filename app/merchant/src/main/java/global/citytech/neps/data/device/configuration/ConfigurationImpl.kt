package global.citytech.neps.data.device.configuration

import android.app.Application
import global.citytech.common.data.LabelValue
import global.citytech.common.exception.PosError
import global.citytech.common.exception.PosException
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.core.api.DeviceServiceType
import global.citytech.finpos.core.api.aid.AidInjectionRequest
import global.citytech.finpos.core.api.aid.AidParameters
import global.citytech.finpos.core.api.aid.AidParametersService
import global.citytech.finpos.core.api.common.DeviceResponse
import global.citytech.finpos.core.api.common.DeviceResult
import global.citytech.finpos.core.api.emvkeys.EmvKey
import global.citytech.finpos.core.api.emvkeys.EmvKeysInjectionRequest
import global.citytech.finpos.core.api.emvkeys.EmvKeysService
import global.citytech.finpos.core.api.emvparam.EmvParametersRequest
import global.citytech.finpos.core.api.emvparam.EmvParametersService
import global.citytech.finpos.core.api.init.InitializeSdkService
import global.citytech.finpos.core.api.keys.KeyAlgorithm
import global.citytech.finpos.core.api.keys.KeyService
import global.citytech.finpos.core.api.keys.KeyType
import global.citytech.finpos.core.api.keys.requests.KeyInjectionRequest
import global.citytech.neps.data.db.AppDatabase
import global.citytech.neps.domain.device.configuration.Configuration
import global.citytech.neps.domain.model.LoadParameterRequest
import global.citytech.neps.service.AppCoreFactory
import io.reactivex.Observable
import java.util.*

class ConfigurationImpl(val context: Application) :
    Configuration {
    override fun inject(key: String): Observable<DeviceResponse> {
        val keyService = AppCoreFactory.getCore(
            context,
            DeviceServiceType.KEYS
        ) as KeyService
        return Observable.fromCallable {
            keyService.injectKey(prepareKeyInjectionRequest(key))
        }
    }

    override fun initSdk(): Observable<DeviceResponse> {
        val initializeSdkService = AppCoreFactory
            .getCore(
                context,
                DeviceServiceType.INIT
            ) as InitializeSdkService
        return Observable.fromCallable {
            initializeSdkService.initializeSdk()
        }
    }

    override fun LoadTerminalParameters(
        loadParameterRequest: LoadParameterRequest
    ): Observable<DeviceResponse> {
        return Observable.fromCallable {
            retrieveAndInjectEmvKeys()
            val emvParametersRequest = retrieveAndInjectEmvParam()
            retrieveAndInjectAidParam(emvParametersRequest)
            DeviceResponse(DeviceResult.SUCCESS, "Terminal Parameters Loaded")
        }
    }

    private fun prepareKeyInjectionRequest(key: String): KeyInjectionRequest {
        return KeyInjectionRequest(
            context.packageName,
            KeyType.KEY_TYPE_PEK,
            KeyAlgorithm.TYPE_3DES,
            key.toUpperCase(Locale.getDefault()),
            null,
            null,
            false
        )
    }

    private fun retrieveAndInjectEmvKeys() {
        val emvKeys = AppDatabase
            .getInstance(context)
            .getEmvKeyDao()
            .getEmvKeys()
        val emvKeysList = emvKeys.map {
            EmvKey(
                getValueFromLabelValue(it.rid),
                getValueFromLabelValue(it.index),
                getValueFromLabelValue(it.length),
                getValueFromLabelValue(it.exponent),
                getValueFromLabelValue(it.modules),
                getValueFromLabelValue(it.hashId),
                getValueFromLabelValue(it.keySignatureId),
                getValueFromLabelValue( it.checkSum),
                getValueFromLabelValue(it.expiryDate)
            ) }
        injectEmvKeys(emvKeysList)
    }

    private fun injectEmvKeys(emvKeysList: List<EmvKey>) {
        val emvKeyService = AppCoreFactory.getCore(
            context,
            DeviceServiceType.EMV_KEYS
        ) as EmvKeysService
        val deviceResponse = emvKeyService.injectEmvKeys(EmvKeysInjectionRequest(emvKeysList))
        if (deviceResponse.result != DeviceResult.SUCCESS) {
            throw PosException(PosError.DEVICE_ERROR_DOWNLOAD_TMS_AGAIN)
        }
    }

    private fun retrieveAndInjectEmvParam(): EmvParametersRequest {
        val emvParams = AppDatabase.getInstance(context).getEmvParamDao().getEmvParams()
        val emvParameterRequest = EmvParametersRequest(
            getValueFromLabelValue(emvParams[0].terminalCountryCode),
            getValueFromLabelValue(emvParams[0].transactionCurrencyCode),
            getValueFromLabelValue(emvParams[0].transactionCurrencyExponent),
            getValueFromLabelValue(emvParams[0].terminalCapabilities),
            getValueFromLabelValue(emvParams[0].terminalType),
            getValueFromLabelValue(emvParams[0].merchantCategoryCode),
            getValueFromLabelValue(emvParams[0].terminalId),
            getValueFromLabelValue(emvParams[0].merchantIdentifier),
            getValueFromLabelValue(emvParams[0].merchantName),
            getValueFromLabelValue(emvParams[0].ttq),
            getValueFromLabelValue(emvParams[0].additionalTerminalCapabilities),
            getValueFromLabelValue(emvParams[0].forceOnlineFlag)
        )
        return injectEmvParam(emvParameterRequest)
    }

    private fun injectEmvParam(
        emvParametersRequest: EmvParametersRequest
    ): EmvParametersRequest {
        val emvParamService = AppCoreFactory
            .getCore(
                context,
                DeviceServiceType.EMV_PARAM
            ) as EmvParametersService
        val deviceResponse = emvParamService.injectEmvParameters(emvParametersRequest)
        if (deviceResponse.result != DeviceResult.SUCCESS) {
            throw PosException(PosError.DEVICE_ERROR_DOWNLOAD_TMS_AGAIN)
        }
        return emvParametersRequest
    }

    private fun retrieveAndInjectAidParam(emvParametersRequest: EmvParametersRequest) {
        val aidParams = AppDatabase
            .getInstance(context)
            .getAidParamDao()
            .getAidParamList()
        val aidParamsList = aidParams.map {
            AidParameters(
                getValueFromLabelValue(it.aid),
                getValueFromLabelValue(it.label),
                getValueFromLabelValue(it.terminalAidVersion),
                getValueFromLabelValue(it.defaultTDOL),
                getValueFromLabelValue(it.defaultDDOL),
                getValueFromLabelValue(it.denialActionCode),
                getValueFromLabelValue(it.onlineActionCode),
                getValueFromLabelValue(it.defaultActionCode),
                getValueFromLabelValue(it.terminalFloorLimit),
                getValueFromLabelValue(it.contactlessFloorLimit),
                getValueFromLabelValue(it.contactlessTransactionLimit),
                getValueFromLabelValue(it.cvmLimit)
            )
        }
        injectAidParam(aidParamsList, emvParametersRequest)
    }

    private fun injectAidParam(
        aidParamsList: List<AidParameters>,
        emvParametersRequest: EmvParametersRequest
    ) {
        val aidService = AppCoreFactory
            .getCore(
                context,
                DeviceServiceType.AID_PARAM
            ) as AidParametersService
        val deviceResponse = aidService.injectAid(
            AidInjectionRequest(
            aidParamsList,
            emvParametersRequest)
        )
        if (deviceResponse.result != DeviceResult.SUCCESS) {
            throw PosException(PosError.DEVICE_ERROR_DOWNLOAD_TMS_AGAIN)
        }
    }

    private fun getValueFromLabelValue(
        item: String?
    ): String {
        var value = ""
        if (item != null) {
            val labelValue = Jsons.fromJsonToObj(item, LabelValue::class.java)
            if (labelValue?.value != null) {
                value = labelValue.value.toString()
            }
        }
        return value
    }
}