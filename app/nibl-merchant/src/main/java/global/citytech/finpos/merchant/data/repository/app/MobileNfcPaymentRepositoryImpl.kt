package global.citytech.finpos.merchant.data.repository.app

import global.citytech.common.data.Response
import global.citytech.finpos.merchant.data.datasource.app.mobile.MobileNfcPaymentDataSource
import global.citytech.finpos.merchant.domain.repository.app.MobilePaymentRepository
import global.citytech.finpos.merchant.domain.usecase.mobile.request.MobileNfcPaymentGenerateRequest
import global.citytech.finpos.merchant.domain.usecase.mobile.request.MobileNfcPaymentStatusRequest
import global.citytech.finpos.merchant.domain.usecase.mobile.response.MobileNfcPaymentStatusResponse
import global.citytech.finpos.merchant.presentation.mobilenfcpayment.NfcResponse
import global.citytech.finposframework.hardware.io.led.LedLight

/**
 * @author SIDDHARTHA GHIMIRE
 */
class MobileNfcPaymentRepositoryImpl(private val mobileNfcPaymentDataSource: MobileNfcPaymentDataSource) :
    MobilePaymentRepository {
    override fun getNfcData(): NfcResponse? {
        return mobileNfcPaymentDataSource.getNfcData()
    }

    override fun requestMobileNfcPaymentTransaction(mobileNfcPaymentStatusRequest: MobileNfcPaymentStatusRequest): Response {
        return mobileNfcPaymentDataSource.addMobileNfcData(mobileNfcPaymentStatusRequest)
    }

    override fun checkMobileNfcPaymentStatus(response: Any): MobileNfcPaymentStatusResponse {
        return mobileNfcPaymentDataSource.checkMobileNfcPaymentStatus(response)
    }

    override fun parseNfcData(nfcData: ByteArray, aid:String): MobileNfcPaymentGenerateRequest {
        return mobileNfcPaymentDataSource.parseNfcData(nfcData, aid)
    }

    override fun prepareMobileNfcPaymentRequest(
        mobileNfcPaymentGenerateRequest: MobileNfcPaymentGenerateRequest
    ): MobileNfcPaymentStatusRequest {
        return mobileNfcPaymentDataSource.prepareMobileNfcPaymentRequest(mobileNfcPaymentGenerateRequest)
    }

    override fun incrementNfcPaymentIdentifiers() {
        mobileNfcPaymentDataSource.incrementNfcPaymentIdentifiers()
    }

    override fun cancelTimer() {
        mobileNfcPaymentDataSource.cancelTimer()
    }

    override fun displaySoundAndLedWithRespectiveDevice(ledLight: LedLight) {
        mobileNfcPaymentDataSource.displaySoundAndLedWithRespectiveDevice(ledLight)
    }

    override fun hideAlertLed() {
        mobileNfcPaymentDataSource.hideAlertLed()
    }

    override fun cleanMobileNfcTask() {
        mobileNfcPaymentDataSource.cleanMobileNfcTask()
    }

    override fun disableNfcReaderMode() {
        mobileNfcPaymentDataSource.disableNfcReaderMode()
    }

    override fun saveNfcPaymentIdentifiers(nfcPaymentIdentifiers: String) {
        mobileNfcPaymentDataSource.saveNfcPaymentIdentifiers(nfcPaymentIdentifiers)
    }
}