package global.citytech.finpos.nibl.iso8583;

import global.citytech.finposframework.switches.PosEntryMode;

public class PosEntryModeConfig {

    public static String getValue(PosEntryMode entryMode) {
        switch (entryMode) {
            case ICC:
                return "0051";
            case MANUAL:
                return "0010";
            case MAGNETIC_STRIPE:
                return "0901";
            case ICC_FALLBACK_TO_MAGNETIC:
                return "0801";
            case PICC:
                return "0071";
            default:
                return "0000";
        }
    }
}
