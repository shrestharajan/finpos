package global.citytech.finposframework.switches.voidsale;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Rishav Chudal on 9/17/20.
 */
public interface VoidSaleRequester<
        T extends VoidSaleRequestParameter, E extends VoidSaleResponseParameter>
        extends Request<T, E> {
}
