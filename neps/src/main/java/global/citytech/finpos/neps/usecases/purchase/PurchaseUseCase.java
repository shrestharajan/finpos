package global.citytech.finpos.neps.usecases.purchase;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finpos.neps.cards.CardIdentifier;
import global.citytech.finpos.neps.cards.CardSchemeRetriever;
import global.citytech.finpos.neps.iso8583.NepsSpecInfo;
import global.citytech.finpos.neps.iso8583.PurchaseRequestSender;
import global.citytech.finpos.neps.repo.TerminalRepositoryDecorator;
import global.citytech.finpos.neps.transaction.ContactlessCardPurchaseProcessor;
import global.citytech.finpos.neps.transaction.IccCardPurchaseProcessor;
import global.citytech.finpos.neps.transaction.MagCardPurchaseProcessor;
import global.citytech.finpos.neps.transaction.ManualPurchaseProcessor;
import global.citytech.finpos.neps.transaction.NepsTransactionAuthorizer;
import global.citytech.finpos.neps.transaction.NepsTransactionInitializer;
import global.citytech.finpos.neps.transaction.NepsTransactionReceiptPrintHandler;
import global.citytech.finpos.neps.transaction.NepsTransactionValidator;
import global.citytech.finpos.neps.transaction.TransactionReceiptHandler;
import global.citytech.finpos.neps.transaction.TransactionUtils;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.AidRetrieveRequest;
import global.citytech.finposframework.repositories.FieldAttributes;
import global.citytech.finposframework.usecases.IdentifyCardResponse;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.SetOnlineResultResponse;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.switches.purchase.PurchaseRequester;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.usecases.transaction.data.TransactionAuthorizationResponse;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.utility.PosResponse;
import global.citytech.finposframework.utility.PosResult;
import global.citytech.finposframework.supports.UseCase;

public class PurchaseUseCase implements UseCase<PurchaseRequestModel, PurchaseResponseModel>,
        PurchaseRequester<PurchaseRequestModel, PurchaseResponseModel> {

    private final TerminalRepositoryDecorator repository;
    private NepsTransactionValidator transactionValidator;

    private final ReadCardRequest.MultipleAidSelectionListener multipleAidSelectionListener = aids -> {
        for (int i = 0; i < aids.length; i++) { // TODO change logic; this is for mada only
            if (aids[i].toLowerCase().contains("mada") ||
                    aids[i].toLowerCase().contains("span"))
                return i;
        }
        return -1;
    };

    public PurchaseUseCase(TerminalRepository repository) {
        this.repository = new TerminalRepositoryDecorator(repository);
    }

    @Override
    public PurchaseResponseModel execute(PurchaseRequestModel request) {

        try {

            PosResponse deviceReadyResponse = request.deviceController.isReady(
                    TransactionUtils
                            .getCardTypesFromTransaction(
                                    request.transactionRequest.getTransactionType()
                            )
            );

            if (deviceReadyResponse.getPosResult() == PosResult.SUCCESS) {

                //Retrieve and Emv Params in transaction request
                TransactionRequest transactionRequest = this.initialize(request);

                //Validate Transaction Request
                this.validateRequest(transactionRequest, request.applicationRepository);

                //Obtain Read Card Response from Device
                ReadCardResponse readCardResponse = this.readCard(
                        request.applicationRepository,
                        request.readCardService,
                        request.transactionRequest
                );
                String cardScheme = getCardScheme(
                        readCardResponse,
                        request.applicationRepository
                );

                //Identify Card on the basis of read card response
                CardIdentifier cardIdentifier = new CardIdentifier(
                        request.transactionRepository,
                        cardScheme,
                        request.applicationRepository
                );
                IdentifyCardResponse identifyCardResponse = cardIdentifier
                        .identify(
                                transactionRequest,
                                readCardResponse
                        );

                //ToDo Handle Wave Again
                if (identifyCardResponse.isWaveAgain()) {

                }
                //ToDo Handle Swipe Again
                if (identifyCardResponse.isSwipeAgain()) {

                }

                //Validate Read Card Response Obtained from Device
                if (this.isValidReadCardResponse(transactionRequest, readCardResponse, cardScheme)) {

                    //Authorize Transaction with merchant if necessary
                    NepsTransactionAuthorizer authorizer = new NepsTransactionAuthorizer(
                            request.transactionAuthenticator,
                            request.applicationRepository
                    );
                    TransactionAuthorizationResponse transactionAuthorizationResponse
                            = authorizer
                            .authorizeUser(
                                    request.transactionRequest.getTransactionType(),
                                    cardScheme
                            );

                    //If Transaction authorized successfully
                    if (transactionAuthorizationResponse.isSuccess()) {

                        PurchaseRequest purchaseRequest = preparePurchaseRequest(
                                transactionRequest,
                                readCardResponse.getCardDetails()
                        );
                        ReadCardResponse purchaseResponse = this.purchaseOnBasisOfCardType(
                                purchaseRequest,
                                request
                        );

                        RequestContext requestContext = this.prepare();
                        requestContext.setRequest(
                                preparePurchaseIsoRequest(
                                        transactionRequest,
                                        purchaseResponse
                                )
                        );
                        PurchaseRequestSender sender = new PurchaseRequestSender(
                                new NepsSpecInfo(),
                                requestContext
                        );
//                        IsoMessageResponse isoMessageResponse = sender.send();  //TODO Mocked used for now
                        IsoMessageResponse isoMessageResponse = MockIsoResponseMessage
                                .prepareMockIsoMessageResponse(readCardResponse);

                        boolean isApprovedPurchase;

                        // TODO check for pin retry transaction

                        boolean isSuccessTransaction = true;

                        if (this.isEmvTransaction(readCardResponse) && this.isArqcTransaction(readCardResponse)) {
                            ReadCardResponse readCardResponse1 = request
                                    .readCardService
                                    .setOnlineResult(
                                            isoMessageResponse
                                                    .getMsg()
                                                    .getDataElementByIndex(39).get()
                                                    .getValueAsInt(),
                                            null, // TODO change according to NEPS implementation
                                            (String) isoMessageResponse
                                                    .getMsg()
                                                    .getDataElementByIndex(55).get()
                                                    .getData(), false);

                            SetOnlineResultResponse setOnlineResultResponse =
                                    new SetOnlineResultResponse
                                            .SetOnlineResultResponseBuilder()
                                            .withResult(readCardResponse1.getResult())
                                            .withMessage(readCardResponse1.getMessage())
                                            .withIccDataBlock(
                                                    readCardResponse1
                                                            .getCardDetails()
                                                            .getIccDataBlock()
                                            )
                                            .withEmvTagCollection(
                                                    readCardResponse1
                                                            .getCardDetails()
                                                            .getTagCollection()
                                            )
                                            .withTransactionState(
                                                    readCardResponse1
                                                            .getCardDetails()
                                                            .getTransactionState()
                                            )
                                            .build();


                            readCardResponse
                                    .getCardDetails()
                                    .setIccDataBlock(
                                            setOnlineResultResponse.getIccDataBlock()
                                    );
                            readCardResponse
                                    .getCardDetails()
                                    .setTagCollection(
                                            setOnlineResultResponse.getEmvTagCollection()
                                    );
                            readCardResponse
                                    .getCardDetails()
                                    .setTransactionState(
                                            setOnlineResultResponse.getTransactionState()
                                    );
                            isSuccessTransaction
                                    = setOnlineResultResponse.getResult()
                                    == Result.SUCCESS;
                        }
                        System.out.println("::: IS SUCCESS TRANSACTION :::" + isSuccessTransaction);
                        if (isSuccessTransaction) {
                            isApprovedPurchase = this.transactionValidator
                                    .isTransactionDeclineByCardEvenIfSuccessOnSwitch(
                                            isoMessageResponse,
                                            readCardResponse
                                    );
                        } else {
                            isApprovedPurchase = false;
                            // TODO perform reversal transaction
                        }

                        // TODO update Reconciliation Count

                        ReceiptHandler.TransactionReceiptHandler receiptHandler = new NepsTransactionReceiptPrintHandler(request.printerService);

                        ReceiptLog receiptLog = new TransactionReceiptHandler(
                                transactionRequest,
                                purchaseRequest,
                                purchaseResponse,
                                isoMessageResponse
                        ).prepare();
                        receiptHandler.printTransactionReceipt(receiptLog, ReceiptVersion.RETAILER_COPY);

                        if (isApprovedPurchase) {
                            NepsPurchaseResponse nepsPurchaseResponse
                                    = this.prepareNepsPurchaseResponse(isoMessageResponse);
                        }

                        PurchaseResponseModel.Builder builder
                                = PurchaseResponseModel.Builder.createDefaultBuilder();
                        builder.approved(true);
                        builder.result(Result.SUCCESS);
                        builder.debugRequestMessage(isoMessageResponse.getDebugRequestString());
                        builder.debugResponseMessage(isoMessageResponse.getDebugResponseString());
                        builder.approvalCode(
                                (String) isoMessageResponse
                                        .getMsg()
                                        .getDataElementByIndex(38).get()
                                        .getData()
                        );
                        builder.message(
                                this.getMessageByActionCode(
                                        request.transactionRepository,
                                        isoMessageResponse
                                                .getMsg()
                                                .getDataElementByIndex(39).get()
                                                .getValueAsInt()
                                )
                        );
                        return builder.build();
                    } else {
                        return handleException(new PosException(PosError.DEVICE_ERROR_INVALID_AMOUNT));
                    }

                } else {
                    //ToDo Change this
                    return handleException(new PosException(PosError.DEVICE_ERROR_INVALID_AMOUNT));
                }

            } else {
                return handleException(new PosException(PosError.DEVICE_ERROR_NOT_READY));
            }
        } catch (PosException exception) {
            return handleException(exception);
        } finally {
            doCleanUp(request.readCardService);
        }
    }

    private boolean isArqcTransaction(ReadCardResponse readCardResponse) {
        try {
            return readCardResponse.getCardDetails().getTagCollection().get(IccData.CID.getTag()).getData().equals("80");
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isEmvTransaction(ReadCardResponse readCardResponse) {
        return readCardResponse.getCardDetails().getCardType() == CardType.ICC ||
                readCardResponse.getCardDetails().getCardType() == CardType.PICC;
    }


    private String getMessageByActionCode(TransactionRepository transactionRepository, int actionCode) {
        // TODO retrieve from transaction repository
        if (actionCode == 0)
            return "APPROVED";
        else
            return "DECLINED";
    }

    private PurchaseIsoRequest preparePurchaseIsoRequest(
            TransactionRequest transactionRequest,
            ReadCardResponse readCardResponse
    ) {
        return PurchaseIsoRequest.Builder.createDefaultBuilder()
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .transactionAmount(String.valueOf(transactionRequest.getAmount()))
                .transmissionDateTime(DateUtils.MMDDhhmmssDate())
                .localDataTime(DateUtils.YYMMDDhhmmssDate())
                .track2Data(readCardResponse.getCardDetails().getTrackTwoData())
                .currencyCode(transactionRequest.getEmvParameterRequest().getTerminalCountryCode())
                .pinBlock(readCardResponse.getCardDetails().getPinBlock().getPin())
                .emvData(readCardResponse.getCardDetails().getIccDataBlock())
                .build();

    }

    private NepsPurchaseResponse prepareNepsPurchaseResponse(IsoMessageResponse response) {

        return null;
    }

    private boolean isSuccessfulPurchase(IsoMessageResponse response) {
        return response.getMsg().getDataElementByIndex(39).get().getData().equals("000");
    }

    private PurchaseResponseModel handleException(PosException posException) {
        PurchaseResponseModel.Builder builder = PurchaseResponseModel.Builder.createDefaultBuilder();
        builder.approved(false);
        builder.result(Result.FAILURE);
        builder.message(posException.getPosError().getErrorMessage());
        builder.debugResponseMessage(posException.getPosError().getErrorMessage());
        builder.message(posException.getPosError().getErrorMessage());
        return builder.build();
    }

    private ReadCardResponse purchaseOnBasisOfCardType(
            PurchaseRequest purchaseRequest,
            PurchaseRequestModel requestModel
    ) {
        ReadCardResponse readCardResponse;
        switch (purchaseRequest.getCardDetails().getCardType()) {
            case MAG:
                MagCardPurchaseProcessor magCardPurchaseProcessor = new MagCardPurchaseProcessor(
                        requestModel.transactionRepository,
                        requestModel.transactionAuthenticator,
                        requestModel.applicationRepository,
                        repository
                );
                readCardResponse = magCardPurchaseProcessor
                        .purchase(
                                repository.getSystemTraceAuditNumber(),
                                purchaseRequest
                        );
                break;
            case ICC:
                IccCardPurchaseProcessor iccCardPurchaseProcessor = new IccCardPurchaseProcessor(
                        requestModel.readCardService,
                        requestModel.transactionRepository,
                        requestModel.applicationRepository
                );
                readCardResponse = iccCardPurchaseProcessor.purchase(
                        repository.getSystemTraceAuditNumber(),
                        purchaseRequest
                );
                break;
            case PICC:
                ContactlessCardPurchaseProcessor contactlessCardPurchaseProcessor
                        = new ContactlessCardPurchaseProcessor(
                        requestModel.readCardService,
                        requestModel.transactionRepository,
                        requestModel.transactionAuthenticator,
                        requestModel.applicationRepository,
                        this.repository
                );
                readCardResponse = contactlessCardPurchaseProcessor.purchase(
                        repository.getSystemTraceAuditNumber(),
                        purchaseRequest
                );
                break;
            case MANUAL:
                ManualPurchaseProcessor manualPurchaseProcessor = new ManualPurchaseProcessor(
                        requestModel.transactionRepository,
                        requestModel.transactionAuthenticator,
                        requestModel.applicationRepository,
                        repository
                );
                readCardResponse = manualPurchaseProcessor.purchase(
                        repository.getSystemTraceAuditNumber(),
                        purchaseRequest
                );
                break;
            default:
                throw new PosException(PosError.DEVICE_ERROR_NO_SUCH_CARD_TYPE);
        }
        return readCardResponse;
    }

    private PurchaseRequest preparePurchaseRequest(
            TransactionRequest transactionRequest,
            CardDetails cardDetails
    ) {
        PurchaseRequest purchaseRequest = new PurchaseRequest(
                transactionRequest.getTransactionType(),
                transactionRequest.getAmount(),
                transactionRequest.getAdditionalAmount()
        );
        cardDetails.setAmount(transactionRequest.getAmount());
        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
        purchaseRequest.setCardDetails(cardDetails);
        purchaseRequest.setFallbackIccToMag(transactionRequest.isFallbackIccToMag());
        return purchaseRequest;
    }

    private boolean isValidReadCardResponse(
            TransactionRequest request,
            ReadCardResponse readCardResponse,
            String cardScheme
    ) {
        return transactionValidator.isValidTransaction(
                request,
                readCardResponse,
                cardScheme
        );
    }

    private ReadCardResponse readCard(
            ApplicationRepository applicationRepository,
            ReadCardService readCardService,
            TransactionRequest transactionRequest
    ) {
        ReadCardRequest readCardRequest = prepareReadCardRequest(
                applicationRepository,
                transactionRequest
        );
        return readCardService.readCardDetails(readCardRequest);
    }

    private ReadCardRequest prepareReadCardRequest(
            ApplicationRepository applicationRepository,
            TransactionRequest transactionRequest
    ) {
        ReadCardRequest readCardRequest = new ReadCardRequest(
                TransactionUtils.getCardTypesFromTransaction(transactionRequest.getTransactionType()),
                transactionRequest.getTransactionType(),
                this.getTransactionType(transactionRequest.getTransactionType()),
                getEmvParams(applicationRepository)
        );
        readCardRequest.setStan(repository.getSystemTraceAuditNumber());
        readCardRequest.setAmount(transactionRequest.getAmount());
        readCardRequest.setCashBackAmount(transactionRequest.getAdditionalAmount());
        readCardRequest.setTransactionDate(this.retrieveTransactionDate());
        readCardRequest.setTransactionTime(this.retrieveTransactionTime());
        readCardRequest.setMultipleAidSelectionListener(this.multipleAidSelectionListener);
        readCardRequest.setPinPadFixedLayout(!this.repository.shouldShufflePinPad());
        return readCardRequest;
    }

    private String retrieveTransactionDate() {
        return DateUtils.yyMMddDate();
    }

    private String retrieveTransactionTime() {
        return DateUtils.hhmmssTime();
    }

    private EmvParametersRequest getEmvParams(ApplicationRepository applicationRepository) {
        return applicationRepository.retrieveEmvParameterRequest();
    }

    private String getTransactionType(TransactionType purchaseType) {
        String processingCode;
        global.citytech.finpos.neps.transaction.TransactionType transactionType = TransactionUtils.mapPurchaseTypeWithTransactionType(purchaseType);
        processingCode = transactionType.getProcessingCode().substring(0, 2);
        return processingCode;
    }

    private void validateRequest(
            TransactionRequest transactionRequest,
            ApplicationRepository applicationRepository
    ) {
        transactionValidator = new NepsTransactionValidator(applicationRepository);
        transactionValidator.validateRequest(transactionRequest);
    }

    private TransactionRequest initialize(PurchaseRequestModel requestModel) {
        NepsTransactionInitializer initializer
                = new NepsTransactionInitializer(requestModel.applicationRepository);
        return initializer.initialize(requestModel.transactionRequest);
    }

    private String getCardScheme(
            ReadCardResponse readCardResponse,
            ApplicationRepository applicationRepository
    ) {
        if (readCardResponse != null) {
            if (readCardResponse.getCardDetails().getCardType() == CardType.MAG)
                return this.cardSchemeForMagnetic(
                        applicationRepository,
                        readCardResponse.getCardDetails().getPrimaryAccountNumber());
            else
                return this.cardSchemeForEmv(applicationRepository, readCardResponse);
        }
        return "";
    }

    private String cardSchemeForMagnetic(
            ApplicationRepository applicationRepository,
            String primaryAccountNumber
    ) {
        CardSchemeRetriever cardSchemeRetriever = new CardSchemeRetriever(applicationRepository);
        return cardSchemeRetriever.retrieveCardScheme(primaryAccountNumber).getCardScheme();
    }

    public String cardSchemeForEmv(
            ApplicationRepository applicationRepository,
            ReadCardResponse readCardResponse
    ) {
        AidRetrieveRequest aidRetrieveRequest = new AidRetrieveRequest(
                readCardResponse
                        .getCardDetails()
                        .getAid(),
                FieldAttributes.AID_DATA_S1_F6_AID_LABEL
        );
        return applicationRepository.retrieveAid(aidRetrieveRequest).getData();
    }

    private RequestContext prepare() {
        HostInfo primaryHostInfo = this.repository.findPrimaryHost();
        HostInfo secondaryHostInfo = this.repository.findSecondaryHost();
        TerminalInfo terminalInfo = this.repository.findTerminalInfo();
        ConnectionParam connectionParam = new ConnectionParam(
                primaryHostInfo,
                secondaryHostInfo
        );
        return new RequestContext(
                this.repository.getSystemTraceAuditNumber(),
                terminalInfo,
                connectionParam
        );
    }

    private void doCleanUp(ReadCardService readCardService) {
        readCardService.cleanUp();
    }
}
