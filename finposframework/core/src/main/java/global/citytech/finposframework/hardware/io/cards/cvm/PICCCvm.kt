package global.citytech.finposframework.hardware.io.cards.cvm

/**
 * Created by Rishav Chudal on 6/8/20.
 */
enum class PICCCvm {
    NO_CVM,
    SIGNATURE,
    CD_CVM,
    DEVICE_OWNER,
    ONLINE_PIN;
}