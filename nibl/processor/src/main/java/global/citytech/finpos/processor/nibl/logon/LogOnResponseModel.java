package global.citytech.finpos.processor.nibl.logon;

import global.citytech.finposframework.switches.logon.LogOnResponseParameter;
import global.citytech.finposframework.supports.UseCase;

public final class LogOnResponseModel implements UseCase.Response, LogOnResponseParameter {
    private final String debugRequestMessage;
    private final String debugResponseMessage;
    private final String pekKey;
    private final boolean success;

    public LogOnResponseModel(Builder builder) {
        this.debugRequestMessage = builder.debugRequestMessage;
        this.debugResponseMessage = builder.debugResponseMessage;
        this.success = builder.success;
        this.pekKey = builder.pekKey;
    }

    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getPekKey() {
        return pekKey;
    }

    @Override
    public String toString() {
        return "LogOnResponseModel{" +
                "debugRequestMessage='" + debugRequestMessage + '\'' +
                ", debugResponseMessage='" + debugResponseMessage + '\'' +
                ", pekKey='" + pekKey + '\'' +
                ", success=" + success +
                '}';
    }

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private boolean success;
        private String pekKey;

        private Builder() {
        }

        public static Builder createDefaultBuilder() {
            return new Builder();
        }

        public Builder debugRequestMessage(String debugRequestString) {
            this.debugRequestMessage = debugRequestString;
            return this;
        }

        public Builder debugResponseMessage(String debugResponseString) {
            this.debugResponseMessage = debugResponseString;
            return this;
        }

        public Builder success(boolean success) {
            this.success = success;
            return this;
        }

        public Builder pekKey(String pekKey) {
            this.pekKey = pekKey;
            return this;
        }

        public LogOnResponseModel build() {
            return new LogOnResponseModel(this);
        }
    }
}
