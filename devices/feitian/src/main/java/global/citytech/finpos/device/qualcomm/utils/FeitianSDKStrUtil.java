/**
 * 版权所有：(C)飞天诚信科技股份有限公司 
 *  包名称 ： [com.ftsafe.epaypos.example] 
 *  类名称 ： [FTStrUtil]
 *  类描述 ： [一句话描述该类]
 * 其他说明： [附加说明]
 * 作       者： [zhldxy]
 * 完成日期： [2016年11月21日 下午1:58:39]
 * 当前版本： [当前版本号]
 * 修改记录1：修改历史记录，包括修改日期、修改者及修改内容
 *            修改日期：
 *            版 本 号：
 *            修 改 人：
 *            修改内容：改原因以及修改内容说明
 */
package global.citytech.finpos.device.qualcomm.utils;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Original zhldxy
 * Taken from CashierDemo
 */
public class FeitianSDKStrUtil {
	private final static char[] HEX = { '0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	private FeitianSDKStrUtil() {
	}

	public static byte[] to4Bytes(int a) {
		return new byte[] { (byte) (0x000000ff & (a >>> 24)),
				(byte) (0x000000ff & (a >>> 16)),
				(byte) (0x000000ff & (a >>> 8)),
				(byte) (0x000000ff & (a)) };
	}

	public static byte[] toBytes(int a) {
		byte[] value;
		if (a < Math.pow(2, 7) - 1) {  //127
			value = new byte[1];
			value[0] = (byte) (a & 0xFF);
		} else if (a < Math.pow(2, 15) - 1) {  //32767
			value = new byte[2];
			value[0] = (byte) ((a >> 8 )& 0xFF);
			value[1] = (byte) (a & 0xFF);
		} else if (a < Math.pow(2, 23) - 1) {
			value = new byte[3];
			value[2] = (byte) ((a >> 16 )& 0xFF);
			value[1] = (byte) ((a >> 8 )& 0xFF);
			value[0] = (byte) (a & 0xFF);
		} else {
			value = new byte[4];
			value[3] = (byte) ((a >> 24 )& 0xFF);
			value[2] = (byte) ((a >> 16 )& 0xFF);
			value[1] = (byte) ((a >> 8 )& 0xFF);
			value[0] = (byte) (a & 0xFF);
		}
		return value;
	}

	public static boolean testBit(byte data, int bit) {
		final byte mask = (byte) ((1 << bit) & 0x000000FF);

		return (data & mask) == mask;
	}

	public static int toInt(byte[] b, int s, int n) {
		int ret = 0;

		final int e = s + n;
		for (int i = s; i < e; ++i) {
			ret <<= 8;
			ret |= b[i] & 0xFF;
		}
		return ret;
	}

	public static int toIntR(byte[] b, int s, int n) {
		int ret = 0;

		for (int i = s; (i >= 0 && n > 0); --i, --n) {
			ret <<= 8;
			ret |= b[i] & 0xFF;
		}
		return ret;
	}

	public static int toInt(byte... b) {
		int ret = 0;
		for (final byte a : b) {
			ret <<= 8;
			ret |= a & 0xFF;
		}
		return ret;
	}

	public static int toIntR(byte... b) {
		return toIntR(b, b.length - 1, b.length);
	}

	public static String toHexString(byte... d) {
		return (d == null || d.length == 0) ? "" : toHexString(d, 0, d.length);
	}

	public static String toHexString(byte[] d, int s, int n) {
		final char[] ret = new char[n * 2];
		final int e = s + n;

		int x = 0;
		for (int i = s; i < e; ++i) {
			final byte v = d[i];
			ret[x++] = HEX[0x0F & (v >> 4)];
			ret[x++] = HEX[0x0F & v];
		}
		return new String(ret);
	}

	public static String toHexStringR(byte[] d, int s, int n) {
		final char[] ret = new char[n * 2];

		int x = 0;
		for (int i = s + n - 1; i >= s; --i) {
			final byte v = d[i];
			ret[x++] = HEX[0x0F & (v >> 4)];
			ret[x++] = HEX[0x0F & v];
		}
		return new String(ret);
	}

	public static String ensureString(String str) {
		return str == null ? "" : str;
	}

	public static String toStringR(int n) {
		final StringBuilder ret = new StringBuilder(16).append('0');

		long N = 0xFFFFFFFFL & n;
		while (N != 0) {
			ret.append((int) (N % 100));
			N /= 100;
		}

		return ret.toString();
	}
	
	public static String toString(byte[] n) {
		final StringBuilder ret = new StringBuilder(n.length);
		int num;
		for (int i = 0; i < n.length; i++ ) {
			num = (int)(n[i] & (byte)0xFF);
			ret.append(HEX[num]);
		}
		
		return ret.toString();
	}

	public static int parseInt(String txt, int radix, int def) {
		int ret;
		try {
			ret = Integer.valueOf(txt, radix);
		} catch (Exception e) {
			ret = def;
		}

		return ret;
	}

	public static int BCDtoInt(byte[] b, int s, int n) {
		int ret = 0;

		final int e = s + n;
		for (int i = s; i < e; ++i) {
			int h = (b[i] >> 4) & 0x0F;
			int l = b[i] & 0x0F;

			if (h > 9 || l > 9)
				return -1;

			ret = ret * 100 + h * 10 + l;
		}

		return ret;
	}

	public static int BCDtoInt(byte... b) {
		return BCDtoInt(b, 0, b.length);
	}
	
	/**
     * 十六进制转二进制码表
     */
    static byte[] hex2bin_table = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0x00 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0x10 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0x20 */
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, /* 0x30 */
    0000, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0000, /* 0x40 */
    0000, 0000, 0000, 0000, 0000, 0000, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0x50 */
    0000, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0000, /* 0x60 */
    0000, 0000, 0000, 0000, 0000, 0000, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0x70 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0x80 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0x90 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0xa0 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0xb0 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0xc0 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0xd0 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0xe0 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 /* 0xf0 */
    };

    /**
     * 将十六进制字符串转换成二进制字节数组
     * 如："2B44EFD9" --> byte[]{0x2B, 0x44, 0xEF, 0xD9}
     */
    public static byte[] hexStringToBytes(String strHexs) {
        if (strHexs == null || strHexs.length() % 2 != 0) {
            return null;
        }

        int len = strHexs.length() / 2;
        byte[] ret = new byte[len];
        for (int i = 0; i < len; i++) {
            ret[i] = (byte) (hex2bin_table[strHexs.charAt(i * 2)] << 4 | hex2bin_table[strHexs.charAt(i * 2 + 1)]);
        }

        return ret;
    }

    /**
     * byte数组转换成16进制表示的字符串
     */
    public static String bytesToHexs(byte[] bs) {
        if (bs == null) {
            return "";
        }

        StringBuffer result = new StringBuffer();
        for (int i = 0; i < bs.length; i++) {
            result.append(toHexString(bs[i]));
        }
        return result.toString();
    }

	/**
	 * 判断字符串是否只含数字
	 * @param str
	 * @return
     */
	public static boolean isNumeric(String str){
		Pattern pattern = Pattern.compile("[0-9]*");
		return pattern.matcher(str).matches();
	}

	/**
	 * 字符串转ASCII字符串
	 * @param value
	 * @return
     */
	public static String stringToAscii(String value) {
		StringBuffer sbu = new StringBuffer();

		char[] chars = value.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			sbu.append((int)chars[i]);
		}

		return sbu.toString();
	}


	/**
	 * 检测是否是16进制字符串
	 * @param value
	 * @return
     */
	public static boolean checkHexString(String value) {
		if (TextUtils.isEmpty(value)) {
			return false;
		}
		if (value.length() % 2 != 0) {
			return false;
		}
		Pattern pattern = Pattern.compile("^[0-9a-fA-F]+$");
		Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }


	public static boolean checkIntString(String value) {
		if (TextUtils.isEmpty(value)) {
			return false;
		}
		Pattern pattern = Pattern.compile("[1-9]^[0-9]+$");
		Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }
	
	public static String doubletoStrMoney(String money) {
		if (money == null || money.equals("")) {//某些手机上出现小数点移位的问题，不使用大数分解，使用字符串匹配
			return null;
		}
		// 0 for interger, 1 for decimal
		String[] str = new String[2];
		if (!money.contains(".")) {
			str[0] = money;
			str[1] = "00";
		} else {
			str = money.split("\\.");
		}
		StringBuffer moneyBuffer = new StringBuffer("000000000000");
		int buflen = moneyBuffer.length();
		int fmtLen = str[0].length();
		if (fmtLen > 12 || fmtLen < 1) {
			return null;
		}
		// form interger
		moneyBuffer.replace(buflen - 2 - fmtLen, buflen - 2, str[0]);
		fmtLen = str[1].length();
		if (fmtLen > 0) {
			if (fmtLen > 2) {
				fmtLen = 2;
			}
			moneyBuffer.replace(buflen - 2, buflen - 2 + fmtLen, str[1]);
		}
		System.out.println(moneyBuffer);
		return moneyBuffer.toString();
    }

	public static String TrimSpace(String tmp) {
		return tmp.replace(" ", "")
				.replace("\t","")
				.replace("\r","")
				.replace("\n","");
	}

	public static String TrimInvalidSpace(String szSrc)
	{
		int nPos1, nPos2;
		int nLen;
		int i;

		nLen = szSrc.length();
		if (nLen == 0)
		{
			return "";
		}

		char[] cc = szSrc.toCharArray();
		for (i = 0; i<cc.length; i++)
		{
			if (cc[i] != ' ' && cc[i] != '\t')
			{
				break;
			}
		}
		nPos1 = i;

		//???????????????????'\r', '\n', '\t', ' '?????
		for (i = cc.length - 1; i >= nPos1; i--)
		{
			if (cc[i] != ' ' && cc[i] != '\t' && cc[i] != '\r' && cc[i] != '\n')
			{
				break;
			}
		}
		nPos2 = i;
		if((nPos1 == 0)&&(nPos2 == (cc.length-1))){
			return szSrc;
		}

		//??nPos1 - nPos2??Χ?????????
		for (i = 0; i<(nPos2 - nPos1 + 1); i++)
		{
			cc[i] = cc[nPos1 + i];
		}
		cc[nPos2 - nPos1 + 1] = '\0';


		return new String(cc);
	}

	public  static   int __toupper(char ch)
	{
		if (((ch >= 'a') && (ch <= 'z')) )
		{
			ch -= 0x20;
		}

		return Integer.parseInt(String.valueOf(ch));
	}

	public static byte[] utlStrToHex(byte[] pbDest, char[] szSrc, short wLen)
	{
		char h1,h2;
		byte s1,s2;
		short i;

		for (i=0; i<wLen; i++)
		{
			h1 = szSrc[2*i];
			h2 = szSrc[2*i+1];

			s1 = (byte)(__toupper(h1) - 0x30);
			if (s1 > 9)
			{
				s1 -= 7;
			}

			s2 = (byte)(__toupper(h2) - 0x30);
			if (s2 > 9)
			{
				s2 -= 7;
			}

			pbDest[i] = (byte)(s1*16 + s2);
		}

		return  pbDest;
	}

	public static int  ProcessPadding(byte flag, byte[] pszData, int wMaxLen, byte bPaddingChar,byte[] outdata)
	{
		int wLength;

		wLength = pszData.length;
		if (wLength > wMaxLen)
		{
			return 1;
		}

		if(flag == 0) {
			if (wLength < wMaxLen) {
				System.arraycopy(pszData, 0, outdata, wMaxLen - wLength, wLength);
				for (int i = 0; i < (wMaxLen - wLength); i++) {
					outdata[i] = bPaddingChar;
				}
			}
		}
		else{
			if (wLength < wMaxLen) {
				System.arraycopy(pszData, 0, pszData, 0, wLength);
				for (int i = wLength; i < (wMaxLen - wLength); i++) {
					pszData[i] = bPaddingChar;
				}
			}
		}
		outdata[wMaxLen] = '\0';

		return 0;
	}

	/**
	 * @功能: BCD码转为10进制串(阿拉伯数据)
	 * @参数: BCD码
	 * @结果: 10进制串
	 */
	public static String bcd2Str(byte[] bytes) {
		StringBuffer temp = new StringBuffer(bytes.length * 2);
		for (int i = 0; i < bytes.length; i++) {
			temp.append((byte) ((bytes[i] & 0xf0) >>> 4));
			temp.append((byte) (bytes[i] & 0x0f));
		}
		return temp.toString().substring(0, 1).equalsIgnoreCase("0") ? temp
				.toString().substring(1) : temp.toString();
	}
	/**
	 * @功能: 10进制串转为BCD码
	 * @参数: 10进制串
	 * @结果: BCD码
	 */
	public static byte[] str2Bcd(String asc) {
		int len = asc.length();
		int mod = len % 2;
		if (mod != 0) {
			asc = "0" + asc;
			len = asc.length();
		}
		byte[] abt = new byte[len];
		if (len >= 2) {
			len = len / 2;
		}
		byte[] bbt = new byte[len];
		abt = asc.getBytes();
		int j, k;
		for (int p = 0; p < asc.length() / 2; p++) {
			if ((abt[2 * p] >= '0') && (abt[2 * p] <= '9')) {
				j = abt[2 * p] - '0';
			} else if ((abt[2 * p] >= 'a') && (abt[2 * p] <= 'z')) {
				j = abt[2 * p] - 'a' + 0x0a;
			} else {
				j = abt[2 * p] - 'A' + 0x0a;
			}
			if ((abt[2 * p + 1] >= '0') && (abt[2 * p + 1] <= '9')) {
				k = abt[2 * p + 1] - '0';
			} else if ((abt[2 * p + 1] >= 'a') && (abt[2 * p + 1] <= 'z')) {
				k = abt[2 * p + 1] - 'a' + 0x0a;
			} else {
				k = abt[2 * p + 1] - 'A' + 0x0a;
			}
			int a = (j << 4) + k;
			byte b = (byte) a;
			bbt[p] = b;
		}
		return bbt;
	}

	public static String Bytes2HexString(byte[] b) {
		String ret = "";

		for (int i = 0; i < b.length; i++) {
			String hex = Integer.toHexString(b[i] & 0xFF);

			if (hex.length() == 1) {
				hex = '0' + hex;
			}

			ret += hex.toUpperCase();
		}
		return ret;
	}

}
