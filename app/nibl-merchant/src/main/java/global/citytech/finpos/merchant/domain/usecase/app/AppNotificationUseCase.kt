package global.citytech.finpos.merchant.domain.usecase.app

import global.citytech.finpos.merchant.domain.model.app.Action
import global.citytech.finpos.merchant.domain.model.app.AppNotification
import global.citytech.finpos.merchant.domain.repository.app.AppNotificationRepository

/**
 * Created by Unique Shakya on 5/28/2021.
 */
class AppNotificationUseCase(private val appNotificationRepository: AppNotificationRepository) {
    fun noNotifications(): Boolean {
        return appNotificationRepository.noNotifications()
    }

    fun getNotificationCount(): Long {
        return appNotificationRepository.getNotificationCount()
    }

    fun getAllNotifications(): List<AppNotification> {
        return appNotificationRepository.getAllNotifications()
    }

    fun removeNotification(appNotification: AppNotification) {
        appNotificationRepository.removeNotification(appNotification)
    }

    fun removeNotificationByAction(action: Action){
        appNotificationRepository.removeNotificationByAction(action)
    }

    fun removeNotificationByReferenceId(referenceId: String) =
        appNotificationRepository.removeNotificationByReferenceId(referenceId)

    fun addNotification(appNotification: AppNotification) {
        appNotificationRepository.addNotification(appNotification)
    }

    fun clearSharedPreference() {
        appNotificationRepository.clearSharedPreference()
    }
}