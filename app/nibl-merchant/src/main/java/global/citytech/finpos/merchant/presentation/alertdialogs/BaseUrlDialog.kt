package global.citytech.finpos.merchant.presentation.alertdialogs

import android.app.Activity
import android.app.AlertDialog
import android.view.View
import android.widget.Button
import com.google.android.material.textfield.TextInputEditText
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/11/19 - 12:09 PM
*/

class BaseUrlDialog constructor(private val activity: Activity) {

    private var alertDialog: AlertDialog
    private lateinit var etBaseUrl: TextInputEditText
    private lateinit var btnSave: Button
    private lateinit var btnCancel: Button


    init {
        val alertDialogBuilder = AlertDialog.Builder(activity)
        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_base_url, null)
        alertDialogBuilder.setView(view)
        initViews(view)
        setViewAttributes()
        handleClickEvents()
        alertDialog = alertDialogBuilder.create()
        alertDialog.setCancelable(false)
    }

    private fun handleClickEvents() {
        btnSave.setOnClickListener {
            hide()
        }
        btnCancel.setOnClickListener {
            hide()
        }
    }

    private fun setViewAttributes() {
    }

    private fun initViews(view: View) {
        etBaseUrl = view.findViewById(R.id.text_input_et_base_url)
        btnSave = view.findViewById(R.id.btn_save)
        btnCancel = view.findViewById(R.id.btn_cancel)
    }

    fun show() {
        alertDialog.show()
    }

    fun hide() {
        alertDialog.dismiss()
    }
}