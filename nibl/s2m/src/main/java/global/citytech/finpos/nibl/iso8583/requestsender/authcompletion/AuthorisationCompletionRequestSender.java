package global.citytech.finpos.nibl.iso8583.requestsender.authcompletion;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.iso8583.NIBLMessagePayloadBuilder;
import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.PosEntryModeConfig;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.utility.HelperUtils;

/**
 * Created by Unique Shakya on 9/29/2020.
 */
public class AuthorisationCompletionRequestSender extends NIBLMessageSenderTemplate {

    private Logger logger = Logger.getLogger(AuthorisationCompletionRequestSender.class.getName());

    public AuthorisationCompletionRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti = new MTI(MTI.Version.Version_1987, MTI.MessageClass.Financial, MTI.MessageFunction.Advice,
                MTI.Originator.Acquirer);
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);
        TransactionIsoRequest request = (TransactionIsoRequest) this.context.getRequest();
        msg.addField(new DataElement(DataElement.PRIMARY_ACCOUNT_NUMBER, request.getPan()));
        msg.addField(new DataElement(DataElement.AMOUNT, HelperUtils.toISOAmount(request.getTransactionAmount(),12)));
        msg.addField(new DataElement(DataElement.LOCAL_TIME, request.getLocalTime()));
        msg.addField(new DataElement(DataElement.LOCAL_DATE, request.getLocalDate()));
        msg.addField(new DataElement(DataElement.PAN_EXPIRE_DATE, request.getExpireDate()));
        msg.addField(new DataElement(DataElement.POS_ENTRY_MODE, PosEntryModeConfig.getValue(request.getPosEntryMode())));
        msg.addField(new DataElement(DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE, request.getPosConditionCode()));
        msg.addField(new DataElement(DataElement.RETRIEVAL_REFERENCE_NUMBER, request.getOriginalRetrievalReferenceNumber()));
        msg.addField(new DataElement(DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE,request.getOriginalApprovalCode()));
        msg.addField(new DataElement(DataElement.RESPONSE_CODE, request.getOriginalResponseCode()));
        msg.addField(new DataElement(DataElement.PRIVATE_USE_FIELD_62, request.getOriginalInvoiceNumber()));
        MessagePayload.Builder builder = new NIBLMessagePayloadBuilder(this.specInfo, msg);
        logger.debug(":::: AUTH COMPLETION REQUEST ::::");
        logger.debug(msg.getPrintableData());
        return builder.build();
    }

    @Override
    protected String getProcessingCode() {
        return "000000";
    }

    @Override
    protected String getFunctionCode() {
        return "0011"; // TODO retrieve from host parameter from request context
    }
}
