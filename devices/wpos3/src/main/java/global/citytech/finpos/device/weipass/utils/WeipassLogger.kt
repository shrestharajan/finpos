package global.citytech.finpos.device.weipass.utils

import android.util.Log
import global.citytech.finposframework.log.AppState

/**
 * Created by Rishav Chudal on 4/9/20.
 */
class WeipassLogger {
    companion object {
        @JvmStatic fun log(message: String) {
            if (!message.isBlank() && message.isNotEmpty()) {
                Log.i(WeipassConstant.LOGGER_TAG, message)
            }
        }

        @JvmStatic fun debug(message: String) {
            if (!message.isBlank() && message.isNotEmpty() && AppState.getInstance().isDebug) {
                Log.i(WeipassConstant.LOGGER_TAG, message)
            }
        }
    }
}