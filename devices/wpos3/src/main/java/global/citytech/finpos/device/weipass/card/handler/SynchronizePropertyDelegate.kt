package global.citytech.finpos.device.weipass.card.handler

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * Created by Rishav Chudal on 6/8/20.
 */
class SynchronizePropertyDelegate<T>(defaultValue: T): ReadWriteProperty<Any, T> {
    private var backingField = defaultValue

    override fun getValue(thisRef: Any, property: KProperty<*>): T {
        return synchronized(this) {
            backingField
        }
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        return synchronized(this) {
            backingField = value
        }
    }

}