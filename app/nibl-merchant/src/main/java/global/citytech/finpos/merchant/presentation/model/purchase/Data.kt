package global.citytech.finpos.merchant.presentation.model.purchase

class Data(
    val vat_amount: String,
    val vat_refund_amount: String
) {
}