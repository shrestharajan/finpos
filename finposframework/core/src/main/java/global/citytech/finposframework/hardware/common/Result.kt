package global.citytech.finposframework.hardware.common

/**
 * Created by Rishav Chudal on 4/8/20.
 */
enum class Result(
    val code : Int,
    val message : String
) {
    NONE(0, "None"),
    FAILURE(-1, "Failure"),
    SUCCESS(1, "Success"),
    CANCEL(2, "Cancel"),
    TIMEOUT(-4, "Timeout"),
    OFFLINE_APPROVED(111, "Offline Approved"),
    ONLINE_APPROVED(112, "Online Approved"),
    OFFLINE_DECLINED(113, "Offline Declined"),
    ONLINE_DECLINED(114, "Online Declined"),
    ICC_CARD_PRESENT(121, "Icc Present in Device"),
    ICC_CARD_NOT_PRESENT(122, "Icc not Present in Device"),
    WAVE_AGAIN(301, "Wave Again"),
    PICC_FALLBACK_TO_ICC(302, "Fallback From PICC to ICC"),
    ICC_FALLBACK_TO_MAGNETIC(303, "Fallback From ICC to Magnetic"),
    USER_CANCELLED(304, "User Cancelled"),
    APPLICATION_BLOCKED(305, "Application Blocked"),
    PINPAD_ERROR(306, "Pinpad Error"),
    SWIPE_AGAIN(307, "Case Swipe Again"),
    PROCESSING_ERROR(308, "Processing Error"),
    CALLBACK_AMOUNT(309, "Callback Amount"),
    ICC_CARD_DETECT_ERROR(310, "Card Detect Error in Icc Reader"),
    SEE_PHONE(311, "See Phone"),
    CARD_BLOCKED(312, "Card Blocked"),
    PIN_BYPASSED(313, "PIN ByPassed"),
    CARD_NOT_SUPPORTED(314, "Card Not Supported"),
    PRINTER_ERROR(315, "Printer Error"),
    BACK_PRESSED(216, "Back Press");

    companion object {
        public fun getByCode(code: Int): Result {
            val results = values()
            for (result in results) {
                if (result.code == code) {
                    return result
                }
            }
            return NONE
        }
    }
}