package global.citytech.finpos.processor.nibl.receipt.summaryreport;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.utility.JsonUtils;

import static org.mockito.ArgumentMatchers.any;

/**
 * Created by Unique Shakya on 1/5/2021.
 */
@RunWith(PowerMockRunner.class)
public class SummaryReportReceiptHandlerTest {

    private PrinterService printerService;

    @Before
    public void setUp() throws Exception {
        this.printerService = PowerMockito.mock(PrinterService.class);
    }

    @Test
    public void printTest(){
        PowerMockito.when(this.printerService.print(any())).thenReturn(new PrinterResponse(Result.SUCCESS, "SUCCESS"));

        String summaryReportReceiptString = "{\"retailer\":{\"retailerName\":\"CityTech\",\"retailerAddress\":\"Kamaladi\",\"merchantId\":\"123456123456\",\"terminalId\":\"1122334455\"},\"performance\":{\"startDateTime\":\"210105142659360\",\"endDateTime\":\"210105142659360\"},\"reconciliationBatchNumber\":\"0001\",\"host\":\"000048\",\"summaryReport\":{\"map\":{\"VISA_DEBIT\":{\"salesCount\":19,\"salesAmount\":1300,\"voidCount\":20,\"voidAmount\":2500,\"refundCount\":19,\"refundAmount\":1300},\"VISA_CREDIT\":{\"salesCount\":19,\"salesAmount\":1300,\"voidCount\":20,\"voidAmount\":2500,\"refundCount\":19,\"refundAmount\":1300},\"MASTERCARD\":{\"salesCount\":19,\"salesAmount\":1300,\"voidCount\":20,\"voidAmount\":2500,\"refundCount\":19,\"refundAmount\":1300}}}}";
        SummaryReportReceipt summaryReportReceipt = JsonUtils.fromJsonToObj(summaryReportReceiptString, SummaryReportReceipt.class);

        SummaryReportReceiptHandler summaryReportReceiptHandler = new SummaryReportReceiptHandler(this.printerService);
        summaryReportReceiptHandler.print(summaryReportReceipt);
    }
}
