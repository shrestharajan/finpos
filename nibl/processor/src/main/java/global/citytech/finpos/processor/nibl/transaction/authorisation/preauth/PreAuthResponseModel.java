package global.citytech.finpos.processor.nibl.transaction.authorisation.preauth;

import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;

public class PreAuthResponseModel extends TransactionResponseModel {

    private String debugRequestMessage;
    private String debugResponseMessage;
    private boolean approved;
    private String message;
    private String stan;
    private boolean shouldPrintCustomerCopy;

    private PreAuthResponseModel(PreAuthResponseModel.Builder builder) {
        this.debugRequestMessage = builder.debugRequestMessage;
        this.debugResponseMessage = builder.debugResponseMessage;
        this.approved = builder.approved;
        this.shouldPrintCustomerCopy = builder.shouldPrintCustomerCopy;
        this.message = builder.message;
        this.stan = builder.stan;
    }

    public boolean shouldPrintCustomerCopy() {
        return shouldPrintCustomerCopy;
    }

    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }

    public boolean isApproved() {
        return approved;
    }

    public String getMessage() {
        return message;
    }

    public String getStan() {
        return stan;
    }

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private boolean approved;
        private String message;
        private String stan;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static PreAuthResponseModel.Builder createDefaultBuilder() {
            return new PreAuthResponseModel.Builder();
        }

        public PreAuthResponseModel.Builder debugRequestMessage(String debugRequestString) {
            this.debugRequestMessage = debugRequestString;
            return this;
        }

        public PreAuthResponseModel.Builder debugResponseMessage(String debugResponseString) {
            this.debugResponseMessage = debugResponseString;
            return this;
        }

        public PreAuthResponseModel.Builder approved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public PreAuthResponseModel.Builder shouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }

        public PreAuthResponseModel.Builder message(String  message) {
            this.message = message;
            return this;
        }

        public PreAuthResponseModel.Builder stan(String  stan) {
            this.stan = stan;
            return this;
        }

        public PreAuthResponseModel build() {
            return new PreAuthResponseModel(this);
        }
    }
}
