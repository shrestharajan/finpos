package global.citytech.finpos.processor.neps.idlepage;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.PosConditionCode;
import global.citytech.finpos.neps.iso8583.ProcessingCode;
import global.citytech.finpos.neps.iso8583.SmartVistaIccDataList;
import global.citytech.finpos.neps.iso8583.requestsender.purchases.IccPurchaseRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.purchases.PurchaseIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.processor.neps.NepsConstant;
import global.citytech.finpos.processor.neps.cardprocessor.ICCCardProcessor;
import global.citytech.finpos.processor.neps.purchases.PurchaseResponse;
import global.citytech.finpos.processor.neps.transaction.TransactionExecutorTemplate;
import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finpos.processor.neps.transaction.TransactionUtils;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedAction;
import global.citytech.finposframework.hardware.io.led.LedLight;
import global.citytech.finposframework.hardware.io.led.LedRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.ChipCardProcessorInterface;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;

/**
 * Created by Saurav Ghimire on 4/13/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public class IdlePageTransactionUseCase extends TransactionExecutorTemplate {

    public IdlePageTransactionUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    public TransactionResponse execute(global.citytech.finpos.processor.neps.transaction.TransactionRequest request) {
        setUpRequiredTransactionFields(request);
        this.retrieveCardResponse();
        try {
            return checkTransactionValidity();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        }
    }

    @Override
    protected void retrieveCardResponse() {
        ReadCardRequest readCardRequest = prepareIdleReadCardRequest(
                stan,
                request.getApplicationRepository(),
                transactionRequest
        );
        this.notifier.notify(
                Notifier.EventType.READING_CARD,
                "Reading Card..."
        );
        this.readCardResponse = this.readEmv(
                readCardRequest,
                request.getReadCardService()
        );
        CardType detectedCardType = readCardResponse.getCardDetails().getCardType();
        this.logger.debug("::: IDLE PAGE USE CASE ::: CARD TYPE === " + detectedCardType);
        if (detectedCardType == CardType.ICC) {
            this.cardProcessor = new ICCCardProcessor(
                    request.getTransactionRepository(),
                    request.getTransactionAuthenticator(),
                    request.getApplicationRepository(),
                    request.getReadCardService(),
                    notifier,
                    terminalRepository
            );
        } else {
            throw new PosException(PosError.DEVICE_ERROR_INVALID_CARD);
        }
        this.readCardResponse = this.cardProcessor.validateReadCardResponse(readCardRequest, readCardResponse);
    }

    private ReadCardRequest prepareIdleReadCardRequest(String stan, ApplicationRepository applicationRepository, TransactionRequest transactionRequest) {
        List<CardType> cardTypeList = new ArrayList<>();
        cardTypeList.add(CardType.ICC);
        ReadCardRequest readCardRequest = new ReadCardRequest(
                cardTypeList,
                transactionRequest.getTransactionType(),
                TransactionUtils.retrieveTransactionType9C(transactionRequest.getTransactionType()),
                applicationRepository.retrieveEmvParameterRequest()
        );
        readCardRequest.setStan(stan);
        readCardRequest.setAmount(transactionRequest.getAmount());
        readCardRequest.setCashBackAmount(transactionRequest.getAdditionalAmount());
        readCardRequest.setTransactionDate(DateUtils.yyMMddDate());
        readCardRequest.setTransactionTime(DateUtils.HHmmssTime());
        readCardRequest.setMultipleAidSelectionListener(this.multipleAidSelectionListener);
        readCardRequest.setIccDataList(SmartVistaIccDataList.get());
        readCardRequest.setCurrencyName(NepsConstant.PIN_PAD_CURRENCY);
        readCardRequest.setPinPadFixedLayout(!terminalRepository.shouldShufflePinPad());
        return readCardRequest;
    }

    protected final ReadCardRequest.MultipleAidSelectionListener multipleAidSelectionListener = aids -> {
        notifier.notify(Notifier.EventType.DETECTED_MULTIPLE_AID, "Processing..");
        return -1;
    };

    private ReadCardResponse readEmv(ReadCardRequest readCardRequest, ReadCardService readCardService) {
        ReadCardResponse readCardResponse = readCardService.readEmv(readCardRequest);
        this.validateReadCardResponse(readCardResponse);
        return readCardResponse;
    }

    private void validateReadCardResponse(ReadCardResponse readCardResponse) {
        if (readCardResponse == null)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        if (readCardResponse.getResult() == Result.TIMEOUT)
            throw new PosException(PosError.DEVICE_ERROR_CARD_READ_TIMEOUT);
        if (readCardResponse.getResult() == Result.USER_CANCELLED)
            throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
        if (readCardResponse.getResult() == Result.FAILURE)
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
    }

    @Override
    protected void processCard() {
        global.citytech.finposframework.usecases
                .transaction
                .data
                .PurchaseRequest purchaseRequest = preparePurchaseCardRequest(
                transactionRequest,
                readCardResponse.getCardDetails()
        );
        readCardResponse = this.cardProcessor.processCard(stan, purchaseRequest);
        logger.debug("::: READ CARD RESPONSE ::: " + readCardResponse.toString());
        this.notifier.notify(
                Notifier.EventType.TRANSMITTING_REQUEST,
                "Transmitting Request.."
        );
    }

    private PurchaseRequest preparePurchaseCardRequest(TransactionRequest transactionRequest, CardDetails cardDetails) {
        PurchaseRequest purchaseRequest = super.preparePurchaseRequest(transactionRequest, cardDetails);
        purchaseRequest.setFallbackIccToMag(transactionRequest.isFallbackIccToMag());
        return purchaseRequest;
    }

    @Override
    protected String getClassName() {
        return IdlePageTransactionUseCase.class.getName();
    }

    @Override
    protected TransactionIsoRequest prepareIsoRequest(ReadCardResponse readCardResponse, TransactionRequest transactionRequest) {
        String pin = shouldSkipPinBlockFromCard(readCardResponse) ? "" : readCardResponse.getCardDetails().getPinBlock().getPin();
        logger.debug("PIN BLOCK :::: " + pin);
        return PurchaseIsoRequest.Builder.newInstance()
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .transactionAmount(readCardResponse.getCardDetails().getAmount())
                .localDateTime(DateUtils.yyMMddHHmmssDate())
                .track2Data(readCardResponse.getCardDetails().getTrackTwoData())
                .currencyCode(this.request.getApplicationRepository().retrieveEmvParameterRequest().getTransactionCurrencyCode())
                .pinBlock(shouldSkipPinBlockFromCard(readCardResponse) ? "" : readCardResponse.getCardDetails().getPinBlock().getPin())
                .emvData(readCardResponse.getCardDetails().getIccDataBlock())
                .fallBackFromIccToMag(readCardResponse.getFallbackIccToMag())
                .build();
    }

    @Override
    protected NepsMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context) {
        return new IccPurchaseRequestSender(context);
    }

    @Override
    protected void processOnlineResult(IsoMessageResponse isoMessageResponse) {
        this.notifier.notify(Notifier.EventType.PROCESSING_ISSUER_SCRIPT, "Processing..");
        readCardResponse = ((ChipCardProcessorInterface) this.cardProcessor).processOnlineResult(readCardResponse, isoMessageResponse);
    }

    @Override
    protected void updateTransactionLog(boolean transactionApproved, TransactionRequest transactionRequest, TransactionRepository transactionRepository, IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse) {
        System.out.println("::: NEPS ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
        System.out.println("::: NEPS ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
        String timeStamp = IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.LOCAL_TIME);
        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
                .withStan(stan)
                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
                .withInvoiceNumber(invoiceNumber)
                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
                .withTransactionType(global.citytech.finposframework.usecases.TransactionType.PURCHASE)
                .withOriginalTransactionType(TransactionType.PURCHASE)
                .withTransactionAmount(transactionRequest.getAmount())
                .withOriginalTransactionAmount(transactionRequest.getAmount())
                .withTransactionDate(TransactionUtils.getDateOrDefaultFromTimeStamp(timeStamp))
                .withTransactionTime(TransactionUtils.getTimeOrDefaultFromTimeStamp(timeStamp))
                .withTransactionStatus(transactionApproved ? "APPROVED" : "DECLINED")
                .withPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
                .withOriginalPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
                .withPosConditionCode(PosConditionCode.ATTENDANT_TERMINAL.getValue())
                .withOriginalPosConditionCode(PosConditionCode.ATTENDANT_TERMINAL.getValue())
                .withReconcileStatus("") //TODO
                .withReconcileTime("") //TODO
                .withReconcileDate("") //TODO
                .withReconcileBatchNo(batchNumber)
                .withReadCardResponse(readCardResponse)
                .withAuthorizationCompleted(true)
                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse, applicationRepository, batchNumber, stan, invoiceNumber))
                .withProcessingCode(getProcessingCode())
                .withReadCardResponse(readCardResponse)
                .withCurrencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .withVatInfo(transactionRequest.getVatInfo())
                .build();
        transactionRepository.updateTransactionLog(transactionLog);
    }

    @Override
    protected void updateTransactionIdsAndNotifyTransactionStatus(boolean transactionApproved, IsoMessageResponse isoMessageResponse, global.citytech.finpos.processor.neps.transaction.TransactionRequest request, ReadCardResponse readCardResponse) {
        this.updateTransactionIds(transactionApproved);
        if (transactionApproved) {
            if (readCardResponse.getCardDetails().getCardType() == CardType.PICC)
                request.getLedService().doTurnLedWith(new LedRequest(LedLight.GREEN, LedAction.ON));
            this.notifier.notify(
                    Notifier.EventType.TRANSACTION_APPROVED,
                    this.approvedMessage(isoMessageResponse)
            );
        } else {
            if (readCardResponse.getCardDetails().getCardType() == CardType.PICC)
                request.getLedService().doTurnLedWith(new LedRequest(LedLight.RED, LedAction.ON));
            this.notifier.notify(
                    Notifier.EventType.TRANSACTION_DECLINED,
                    this.declinedMessage(
                            isoMessageResponse,
                            request.getApplicationRepository()
                    )
            );
        }
    }

    @Override
    protected TransactionResponse prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository, boolean shouldPrint) {
        return PurchaseResponse.Builder.createDefaultBuilder()
                .approved(transactionApproved)
                .stan(stan)
                .shouldPrintCustomerCopy(shouldPrint)
                .debugRequestMessage(isoMessageResponse.getDebugRequestString())
                .debugResponseMessage(isoMessageResponse.getDebugResponseString())
                .message(this.retrieveMessage(isoMessageResponse, transactionApproved, applicationRepository))
                .build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.PURCHASE.getCode();
    }

    @Override
    protected TransactionResponse prepareErrorResponse(String responseMessage, boolean shouldPrint) {
        return PurchaseResponse.Builder.createDefaultBuilder()
                .stan(this.stan)
                .message(responseMessage)
                .approved(false)
                .shouldPrintCustomerCopy(shouldPrint)
                .build();
    }

    @Override
    protected TransactionResponse promptToPerformIccTransaction() {
        return null;
    }

}
