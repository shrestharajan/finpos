package global.citytech.finpos.merchant.domain.usecase.app

import global.citytech.common.data.Response
import global.citytech.finpos.merchant.domain.repository.app.LogPushRepository
import global.citytech.finpos.merchant.presentation.dashboard.temp.LogResponse
import global.citytech.finpos.merchant.presentation.dashboard.temp.LogFileUploadResponse

class LogSendUseCase(private val logSendRepository: LogPushRepository) {

    fun getLogsList(serialNumber: Any,filePaths: String): LogResponse {
        return logSendRepository.getLogsList(serialNumber,filePaths)
    }

    fun saveLogToDB(logFileUploadResponse: LogFileUploadResponse, filePath: String, serialNumber: Any): Response {
        return logSendRepository.pushLogFileToServer(logFileUploadResponse, filePath, serialNumber)
    }

}