package global.citytech.finpos.merchant.presentation.preauth

import android.app.Application
import global.citytech.finpos.merchant.data.repository.core.preauth.PreAuthRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.preauth.PreAuthDataSourceImpl
import global.citytech.finpos.merchant.domain.model.preauth.PreAuthResponseEntity
import global.citytech.finpos.merchant.domain.usecase.core.preauth.CorePreAuthUseCase
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.preauth.ManualPreAuthRequestItem
import global.citytech.finpos.merchant.presentation.model.preauth.PreAuthRequestItem
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finposframework.usecases.TransactionType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal

/**
 * Created by Rishav Chudal on 8/31/20.
 */
class PreAuthViewModel(val instance: Application) : BaseTransactionViewModel(instance) {

    var preAuthUseCase: CorePreAuthUseCase = CorePreAuthUseCase(
        PreAuthRepositoryImpl(
            PreAuthDataSourceImpl()
        )
    )

    fun doPreAuth(
        configurationItem: ConfigurationItem,
        amount: BigDecimal,
        transactionType: TransactionType
    ) {
        isLoading.value = true
        compositeDisposable.add(
            preAuthUseCase.doPreAuth(
                configurationItem,
                PreAuthRequestItem(
                    amount,
                    transactionType
                )
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onDoPreAuthNext(it)
                }, {
                    onDoPreAuthError(it)
                })
        )
    }

    private fun onDoPreAuthNext(preAuthResponseEntity: PreAuthResponseEntity) {
        isLoading.value = false
        if (!shouldDispose) {
            showTransactionConfirmationDialog(preAuthResponseEntity)
        } else {
            completePreviousTask()
        }
    }

    private fun showTransactionConfirmationDialog(it: PreAuthResponseEntity) {
        prepareBase64UrlToDisplayQr(
            it.isApproved!!,
            it.message!!,
            it.shouldPrintCustomerCopy!!,
            it.stan!!
        )
    }

    private fun onDoPreAuthError(throwable: Throwable) {
        throwable.printStackTrace()
        isLoading.value = false
        if (!shouldDispose) {
            transactionComplete.value = false
            message.value = throwable.message
        } else {
            completePreviousTask()
        }
    }

    fun doManualPreAuth(
        configurationItem: ConfigurationItem,
        manualPreAuthRequestItem: ManualPreAuthRequestItem
    ) {
        isLoading.value = true
        compositeDisposable.add(
            this.preAuthUseCase.doManualPreAuth(
                configurationItem,
                manualPreAuthRequestItem
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onManualPreAuthNext(it)
                }, {
                    onManualPreAuthError(it)
                })
        )
    }

    private fun onManualPreAuthNext(preAuthResponseEntity: PreAuthResponseEntity) {
        prepareBase64UrlToDisplayQr(
            preAuthResponseEntity.isApproved!!,
            preAuthResponseEntity.message!!,
            preAuthResponseEntity.shouldPrintCustomerCopy!!,
            preAuthResponseEntity.stan!!
        )
    }

    private fun onManualPreAuthError(it: Throwable) {
        it.printStackTrace()
        isLoading.value = false
        transactionComplete.value = false
        message.value = it.message
    }
}