package global.citytech.finpos.merchant.domain.usecase.core.receipt

import global.citytech.finpos.merchant.domain.model.receipt.CustomerCopyResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.receipt.CustomerCopyRepository
import global.citytech.finposframework.usecases.transaction.data.StatementList
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/2/2020.
 */
class CoreCustomerCopyUseCase(private val customerCopyRepository: CustomerCopyRepository) {
    fun print():Observable<CustomerCopyResponseEntity> {
        return customerCopyRepository.print()
    }
    fun printStatement(statementList: StatementList):Observable<CustomerCopyResponseEntity>{
        return customerCopyRepository.printStatement(statementList)
    }
}