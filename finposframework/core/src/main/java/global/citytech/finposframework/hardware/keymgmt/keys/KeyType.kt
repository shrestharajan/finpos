package global.citytech.finposframework.hardware.keymgmt.keys

/**
 * Created by Rishav Chudal on 4/9/20.
 */
enum class KeyType {
    KEY_TYPE_TLK,
    KEY_TYPE_TMK,
    KEY_TYPE_DEK,
    KEY_TYPE_PEK,
    KEY_TYPE_IPEK,
    KEY_TYPE_MAK,
    KEY_TYPE_KBPK,
    KEY_TYPE_DDEK,
    KEY_TYPE_DMAK,
    KEY_TYPE_FIXED_KEY,
    KEY_TYPE_MEK,
    KEY_TYPE_RSA
}