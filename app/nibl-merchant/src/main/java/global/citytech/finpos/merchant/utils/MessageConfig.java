package global.citytech.finpos.merchant.utils;

import android.content.DialogInterface;
import android.view.View.OnClickListener;

import global.citytech.easydroid.core.utils.HelperUtils;

/**
 * Created by Saurav Ghimire on 6/21/21.
 * sauravnghimire@gmail.com
 */


public class MessageConfig {
    private String title;
    private String message;
    private String positiveLabel;
    private String negativeLabel;
    private String neutralLabel;
    private OnClickListener onPositiveClick;
    private OnClickListener onNegativeClick;
    private OnClickListener onNeutralClick;

    private MessageConfig(Builder builder) {
        this.title = builder.title;
        this.message = builder.message;
        this.positiveLabel = builder.positiveLabel;
        this.negativeLabel = builder.negativeLabel;
        this.neutralLabel = builder.neutralLabel;
        this.onPositiveClick = builder.onPositiveClick;
        this.onNegativeClick = builder.onNegativeClick;
        this.onNeutralClick = builder.onNeutralClick;
    }

    public String getTitle() {
        return this.title;
    }

    public String getMessage() {
        return this.message;
    }

    public String getPositiveLabel() {
        return this.positiveLabel;
    }

    public String getNegativeLabel() {
        return HelperUtils.isBlankOrNull(this.negativeLabel) ? "NO" : this.negativeLabel;
    }

    public String getNeutralLabel() {
        return this.neutralLabel;
    }

    public OnClickListener getOnPositiveClick() {
        return this.onPositiveClick;
    }

    public OnClickListener getOnNegativeClick() {
        return this.onNegativeClick;
    }

    public OnClickListener getOnNeutralClick() {
        return this.onNeutralClick;
    }

    public boolean hasNegativeButton() {
        if (!HelperUtils.isBlankOrNull(this.negativeLabel)) {
            return true;
        } else {
            return this.getOnNegativeClick() != null;
        }
    }

    public static class Builder {
        private String title;
        private String message;
        private String positiveLabel;
        private String negativeLabel;
        private String neutralLabel;
        private OnClickListener onPositiveClick;
        private OnClickListener onNegativeClick;
        private OnClickListener onNeutralClick;

        public Builder() {
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder positiveLabel(String positiveLabel) {
            this.positiveLabel = positiveLabel;
            return this;
        }

        public Builder negativeLabel(String negativeLabel) {
            this.negativeLabel = negativeLabel;
            return this;
        }

        public Builder neutralLabel(String neutralLabel) {
            this.neutralLabel = neutralLabel;
            return this;
        }

        public Builder onPositiveClick(OnClickListener onPositiveClick) {
            this.onPositiveClick = onPositiveClick;
            return this;
        }

        public Builder onNegativeClick(OnClickListener onNegativeClick) {
            this.onNegativeClick = onNegativeClick;
            return this;
        }

        public Builder onNeutralClick(OnClickListener onNeutralClick) {
            this.onNeutralClick = onNeutralClick;
            return this;
        }

        public MessageConfig build() {
            return new MessageConfig(this);
        }
    }
}
