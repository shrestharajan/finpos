package global.citytech.finpos.merchant.presentation.balanceinquiry

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import global.citytech.finpos.merchant.data.repository.core.balanceinquiry.BalanceInquiryRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.receipt.CustomerCopyRepositoryImpl
import global.citytech.finpos.merchant.domain.model.receipt.CustomerCopyResponseEntity
import global.citytech.finpos.merchant.domain.model.balanceinquiry.BalanceInquiryModel
import global.citytech.finpos.merchant.domain.usecase.core.balanceinquiry.CoreBalanceInquiryUseCase
import global.citytech.finpos.merchant.domain.usecase.core.receipt.CoreCustomerCopyUseCase
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.balanceinquiry.BalanceInquiryDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.receipt.CustomerCopyDataSourceImpl
import global.citytech.finpos.merchant.presentation.greenpin.GreenPinBaseTransactionViewModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.balanceinquiry.BalanceInquiryRequestItem
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.repositories.TerminalRepository
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.data.StatementList
import global.citytech.finposframework.usecases.transaction.data.StatementItems
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class BalanceInquiryViewModel(instance: Application) : GreenPinBaseTransactionViewModel(instance) {

    private var balanceInquiryUseCase: CoreBalanceInquiryUseCase =
        CoreBalanceInquiryUseCase(BalanceInquiryRepositoryImpl(BalanceInquiryDataSourceImpl()))
    private var customerCopyUseCase: CoreCustomerCopyUseCase =
        CoreCustomerCopyUseCase(CustomerCopyRepositoryImpl(CustomerCopyDataSourceImpl()))
    var availableBalance = ""
    var currentBalance = ""
    val balanceInquirySuccess by lazy { MutableLiveData<Boolean>() }
    val additionalAmount by lazy { MutableLiveData<BalanceInquiryModel>() }

    fun generateBalanceInquiry(
        configurationItem: ConfigurationItem,
        transactionType: TransactionType
    ) {
        terminalRepository= TerminalRepositoryImpl(configurationItem)
        compositeDisposable.add(
            balanceInquiryUseCase.generateBalanceInquiryRequest(
                configurationItem,
                BalanceInquiryRequestItem(
                    transactionType = transactionType,
                    amount = "0.00".toBigDecimal()
                )
            )

                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    if (it.approve) {
                        if(it.additionalAmount.length== 20){
                            getAvailableBalance(it.additionalAmount)
                        }else{
                            getAvailableAndCurrrentBalance(it.additionalAmount)
                        }

                        balanceInquirySuccess.value = true
                    } else {
                        balanceInquirySuccess.value = false
                        failureMessage.value = it.message
                    }

                }, {
                    message.value = it.message
                    balanceInquirySuccess.value = false
                })
        )
    }

    fun printBalanceInquiry() {
        isLoading.value = true
        val emptyListOfStatementItems: List<StatementItems> = emptyList()
            compositeDisposable.add(
                customerCopyUseCase.printStatement(
                    StatementList(
                        emptyListOfStatementItems,
                        availableBalance,
                        currentBalance,
                        "",
                        AppUtility.checkCurrencyCode(currencyCode)
                    )
                )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        onCustomerCopyResponseEntityReceived(it)
                    }, {
                        customerCopyPrintMessage.postValue("Customer Print Failure")
                        isLoading.value = false
                    })
            )

    }

    private fun onCustomerCopyResponseEntityReceived(it: CustomerCopyResponseEntity) {
        isLoading.value = false
        if (it.result == Result.SUCCESS || it.result == Result.CANCEL) {
            checkForCardPresent()
            return
        }
        customerCopyPrintMessage.postValue(it.message)
    }

    fun getAvailableBalance(additionalAmount:String){
        var startingRange = 0;
        currencyCode= additionalAmount.substring(
            startingRange + 4,
            startingRange + 7
        )

        println("currencyCode::"+ currencyCode)

        val aBalance = additionalAmount.substring(
            startingRange + 8,
            startingRange + 18
        ) + "." + additionalAmount.substring(startingRange + 18, startingRange + 20)
        availableBalance = aBalance.replace("^0+(?!$)".toRegex(), "")
        this.additionalAmount.value = BalanceInquiryModel(availableBalance, "")

    }
    
    fun getAvailableAndCurrrentBalance(additionalAmount: String) {
        var startingRange = 0;
        currencyCode= additionalAmount.substring(
            startingRange + 4,
            startingRange + 7
        )

        val aBalance = additionalAmount.substring(
            startingRange + 8,
            startingRange + 18
        ) + "." + additionalAmount.substring(startingRange + 18, startingRange + 20)
        availableBalance = aBalance.replace("^0+(?!$)".toRegex(), "")

        val cBalance= additionalAmount.substring(
            startingRange + 28,
            startingRange + 38
        ) + "." + additionalAmount.substring(startingRange + 38, startingRange + 40)

        currentBalance = cBalance.replace("^0+(?!$)".toRegex(), "")
        this.additionalAmount.value = BalanceInquiryModel(availableBalance, currentBalance)
        Log.d("availableBalanceTag", "$availableBalance // $currentBalance")
    }
}