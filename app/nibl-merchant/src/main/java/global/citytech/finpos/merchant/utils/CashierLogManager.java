package global.citytech.finpos.merchant.utils;

import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import global.citytech.finpos.merchant.NiblMerchant;
import global.citytech.finpos.merchant.presentation.log.LogPushAlarmScheduler;


/**
 * Created by BikashShrestha on 2020-01-01.
 * Modified by Rishav Chudal & Rajan Shrestha on 2023-05-21
 */
public class CashierLogManager {
    private static final String TAG = CashierLogManager.class.getName();
    private static final String ADB_COMMAND_PREFIX = "logcat -v time";
    private static final String ADB_COMMAND_SUFFIX = "";
    private static final String ADB_COMMAND_TO_CLEAR_BUFFER = "logcat -b all -c";
    private static CashierLogManager mInstance;
    private String logFileAbsolutePath;
    private boolean isLogCaptureRunning = false;
    private Process process;
    private static final int MINIMUM_MEMORY_REQUIRED_FOR_A_DAY_LOG_IN_MB = 200;
    private static final String LOGS_FOLDER_PATH = "/CityTechLogcat/finPOS-Cashier/";
    private File logFileDirectory;
    private static final long SEVEN_DAYS_DURATION_IN_MILLIS = 7 * 24 * 60 * 60 * 1000;
    private static final long THREE_DAYS_DURATION_IN_MILLIS = 3 * 24 * 60 * 60 * 1000;


    private CashierLogManager() {
    }

    public static CashierLogManager getInstance() {
        if (mInstance == null) {
            mInstance = new CashierLogManager();
        }
        return mInstance;
    }


    public synchronized void initLog() {
        LogPushAlarmScheduler logPushAlarmScheduler = new LogPushAlarmScheduler(NiblMerchant.INSTANCE);
        logPushAlarmScheduler.triggerAlarm();
        deleteLogsOlderThanSevenDays();
        if (isLogCaptureRunning) {
            Log.i(TAG, "initLog: Log Capture is Already Running...");
            return;
        }

        if (!isLogcatEnabled()) {
            Log.i(TAG, "initLog: LogCat is not enabled...");
            return;
        }

        if (!isMinimumMemoryAvailableForLog()) {
            Log.i(TAG, "initLog: Minimum storage memory not available for Log...");
            clearingLogDataOlderThanThreeDays();
        }

        isLogCaptureRunning = true;
        String fileName = "logcat_" + getCurrentTime().substring(0, 10) + ".txt";
        logFileDirectory = new File(
                Environment.getExternalStorageDirectory().getAbsolutePath() +
                        LOGS_FOLDER_PATH +
                        getCurrentTime().substring(0,10)
        );

        if (!logFileDirectory.exists()) {
            logFileDirectory.mkdirs();
        }
        logFileAbsolutePath = logFileDirectory.getAbsolutePath() + "/" + fileName;
        clearAdbBuffer();
        startLog();
    }


    public void stopLog() {
        if (process != null) {
            process.destroy();
            isLogCaptureRunning = false;
            process = null;
        }
    }

    private String getCurrentTime() {
        SimpleDateFormat dF = new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss", Locale.getDefault());
        return dF.format(new Date());
    }

    private void clearAdbBuffer() {
        try {
            Runtime.getRuntime().exec(ADB_COMMAND_TO_CLEAR_BUFFER);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void startLog() {
        if (isLogcatEnabled()) {
            try {
                File prevLogFile = new File(logFileAbsolutePath);
                if (!prevLogFile.exists()) {
                    prevLogFile.createNewFile();
                }
                process = Runtime.getRuntime().exec(
                        ADB_COMMAND_PREFIX
                );
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(process.getInputStream()));
                FileWriter fileWriter = null;
                StringBuilder logBuilder=new StringBuilder();
                String line;
                int lines = 0;
                while ((line = bufferedReader.readLine()) != null) {
                    lines++;
                    logBuilder.append(line).append("\n");
                    if (isDayChangedComparedToFileName(logFileDirectory.getName())) {
                        Log.i(TAG, "startLog: Day Changed...");
                        break;
                    }

                    if (lines == 200) {
                        fileWriter = new FileWriter(prevLogFile, true);
                        fileWriter.write(logBuilder.toString());
                        lines = 0;
                        logBuilder.setLength(0);
                    }

                }
                bufferedReader.close();
                if (fileWriter != null) {
                    fileWriter.flush();
                    fileWriter.close();
                }
                isLogCaptureRunning = false;
                if (isDayChangedComparedToFileName(logFileDirectory.getName())) {
                    initLog();
                    return;
                }
                Log.d(TAG, "startLog: Came Here Again...");
            } catch (IOException exception) {
                Log.d(TAG, "initLogCat: failed" + exception);
                isLogCaptureRunning = false;
                initLog();
            }
        }
    }

    private boolean isLogcatEnabled() {
        //return PreferenceHelper.getInstance().isLogcatEnable();
        return true;
    }

    private void deleteLogsOlderThanSevenDays() {
        File logsFolder = new File(
                Environment.getExternalStorageDirectory().getAbsolutePath() +
                        LOGS_FOLDER_PATH
        );
        long currentTimeInMillis = System.currentTimeMillis();
        File[] folders = logsFolder.listFiles();
        if (folders == null || folders.length < 1) {
            return;
        }

        for (File folder : folders) {
            long lastModifiedDateOfFolderInMillis = folder.lastModified();
            if (lastModifiedDateOfFolderInMillis + SEVEN_DAYS_DURATION_IN_MILLIS < currentTimeInMillis) {
                Log.i(
                        TAG,
                        "deleteLogsOlderThanSevenDays: Deleting Files ::: " +
                                folder.getName()
                );
                deleteFileOrFolder(folder);
            }
        }
    }

    private void clearingLogDataOlderThanThreeDays() {
        File logsFolder = new File(
                Environment.getExternalStorageDirectory().getAbsolutePath() +
                        LOGS_FOLDER_PATH
        );
        long currentTimeInMillis = System.currentTimeMillis();
        File[] folders = logsFolder.listFiles();
        if (folders == null || folders.length < 1) {
            return;
        }

        for (File folder : folders) {
            long lastModifiedDateOfFolderInMillis = folder.lastModified();
            if (lastModifiedDateOfFolderInMillis + THREE_DAYS_DURATION_IN_MILLIS < currentTimeInMillis) {
                Log.i(
                        TAG,
                        "deleteLogsOlderThanThreeDays: Deleting Files ::: " +
                                folder.getName()
                );
                deleteFileOrFolder(folder);
            }
        }
    }

    private boolean isMinimumMemoryAvailableForLog() {
        long totalStorageMemory = getTotalStorageMemory();
        long availableStorageMemory = getAvailableStorageMemory();
        return availableStorageMemory >= MINIMUM_MEMORY_REQUIRED_FOR_A_DAY_LOG_IN_MB;
    }

    private long getTotalStorageMemory() {
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable;
        bytesAvailable = statFs.getBlockSizeLong() * statFs.getBlockCountLong();
        long totalStorageMemoryInMB = bytesAvailable / (1024 * 1024);
        Log.i(TAG, "getTotalStorageMemory: Total Memory in MB ::: " + totalStorageMemoryInMB);
        return totalStorageMemoryInMB;
    }

    private long getAvailableStorageMemory() {
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable;
        bytesAvailable = statFs.getBlockSizeLong() * statFs.getAvailableBlocksLong();
        long availableStorageMemoryInMB = bytesAvailable / (1024 * 1024);
        Log.i(TAG, "getTotalStorageMemory: Available Memory in MB ::: " + availableStorageMemoryInMB);
        return availableStorageMemoryInMB;
    }

    private boolean isDayChangedComparedToFileName(String fileName) {
        String todaysDate = getCurrentTime().substring(0, 10);
        return !todaysDate.contains(fileName);
    }

    private void deleteFileOrFolder(File file) {
        if (file.isDirectory() && file.exists()) {
            for (File internalFile: file.listFiles()) {
                deleteFileOrFolder(internalFile);
            }
        }
        file.delete();
    }

}
