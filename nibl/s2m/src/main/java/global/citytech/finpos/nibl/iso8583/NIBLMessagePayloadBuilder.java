package global.citytech.finpos.nibl.iso8583;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import global.citytech.finposframework.log.Logger;;

import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.utility.EncodingUtils;
import global.citytech.finposframework.supports.MessagePayload;

/**
 * User: Surajchhetry Date: 2/19/20 Time: 5:22 PM
 */
public class NIBLMessagePayloadBuilder implements MessagePayload.Builder {
    private Logger logger = Logger.getLogger(NIBLMessagePayloadBuilder.class.getName());

    private SpecInfo specInfo;
    private Iso8583Msg request;

    public NIBLMessagePayloadBuilder(SpecInfo specInfo, Iso8583Msg request) {
        this.specInfo = specInfo;
        this.request = request;
    }

    @Override
    public MessagePayload build() {
        try (ByteArrayOutputStream messagePacket = new ByteArrayOutputStream()) {
            byte[] finalDataElements = request.getDataElementsAsByteArray(this.specInfo);
            // 1. Write MTI
            messagePacket.write(EncodingUtils.hex2Bytes(this.request.getMti().toString()));
            // 2. Write BitMap
            messagePacket.write(request.getBitMap().asHexASCII());
            // 3. Write Request Data
            messagePacket.write(finalDataElements);
            return new MessagePayload.Default(
                    messagePacket.toByteArray(),
                    request
            );

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
