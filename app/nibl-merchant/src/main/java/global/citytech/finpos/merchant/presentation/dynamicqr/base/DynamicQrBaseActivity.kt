package global.citytech.finpos.merchant.presentation.dynamicqr.base

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.google.android.flexbox.*
import global.citytech.easydroid.core.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityDynamicQrBinding
import global.citytech.finpos.merchant.domain.model.app.QrStatus
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.dynamicqr.QrType
import global.citytech.finpos.merchant.presentation.model.qroperator.QrOperatorItem
import global.citytech.finpos.merchant.presentation.model.setting.QrConfiguration
import global.citytech.finpos.merchant.presentation.utils.QrIssuersAdapter
import global.citytech.finpos.merchant.utils.MessageConfig
import kotlinx.android.synthetic.main.activity_dynamic_qr.*

abstract class DynamicQrBaseActivity :
    AppBaseActivity<ActivityDynamicQrBinding, DynamicQrBaseViewModel>() {

    var qrOperatorItem: QrOperatorItem? = null
    var qrConfiguration: QrConfiguration? = null

    companion object {
        const val EXTRA_QR_PAYMENT = "extra.qr.payment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel().getConfiguration()
        initOnClick()
        observeBaseLiveData()
        observeNoInternetConnection()
        initViews()
        observeQRStatusCode()
        observeQrStatusMessage()
    }

    private fun observeQrStatusMessage(){
        getViewModel().showAlertDialog.observe(this, Observer {
            if (it) {
                hideProgress()
                showConfirmationMessage(MessageConfig.Builder()
                    .title(getString(R.string.time_out))
                    .message(getString(R.string.msg_confirmation_continue_qr_payments))
                    .positiveLabel(getString(R.string.title_ok))
                    .onPositiveClick {
                        getViewModel().startTimer()
                        getViewModel().disableAlertDialogFlag()
                    }
                    .negativeLabel(getString(R.string.action_cancel))
                    .onNegativeClick {
                        setResult(RESULT_OK)
                        finish()
                    })
            } else {
                hideConfirmationMessage()
            }
        })
    }

    fun updateViews(qrOperatorItem: QrOperatorItem?) {
        tv_qr_operator_title.setText(qrOperatorItem?.name)

        if (QrType.getByCodes(qrOperatorItem?.name.toString()) == QrType.NQR){
            loadQrLogo(R.drawable.ic_nepal_pay_qr)
        }else if(QrType.getByCodes(qrOperatorItem?.name.toString()) == QrType.FONEPAY){
            loadQrLogo(R.drawable.ic_fone_pay)
        }

        iv_loading.visibility = View.GONE
        cl_qr.visibility = View.VISIBLE

        qrOperatorItem?.let { item ->
            if (!item.issuers.isNullOrEmpty()){
                val qrIssuersAdapter = QrIssuersAdapter(item.issuers!!)
                rv_issuers.adapter = qrIssuersAdapter
                val layoutManager = FlexboxLayoutManager(this)
                layoutManager.flexDirection = FlexDirection.ROW
                layoutManager.alignItems = AlignItems.CENTER
                layoutManager.flexWrap = FlexWrap.WRAP
                layoutManager.justifyContent = JustifyContent.CENTER
                rv_issuers.layoutManager = layoutManager
                cl_footer_dynamic_qr.visibility = View.VISIBLE
            }else{
                cl_footer_dynamic_qr.visibility = View.INVISIBLE
            }
        } ?: kotlin.run {
            cl_footer_dynamic_qr.visibility = View.INVISIBLE
        }
    }

    private fun loadQrLogo(placeholder: Int){
        Glide.with(this)
            .load(qrOperatorItem?.logo)
            .placeholder(placeholder)
            .into(iv_qr_operator_logo)
    }

    private fun initViews() {
        Glide.with(this).load("file:///android_asset/images/loading.gif").into(iv_loading)
        cl_qr.visibility = View.GONE
        iv_loading.visibility = View.VISIBLE
    }

    private fun observeNoInternetConnection() {
        getViewModel().noInternetConnection.observe(this, Observer {
            if (it) {
                tv_connection_message.visibility = View.VISIBLE
            } else {
                tv_connection_message.visibility = View.GONE
            }
        })
    }

    private fun observeQRStatusCode() {
        getViewModel().showAlertDialog.value = false
        getViewModel().qrStatusCode.observe(this, Observer {
            if (getViewModel().showAlertDialog.value == true) {
                hideProgress()
            } else {
                hideConfirmationMessage()
                when {
                    it.equals(QrStatus.QR001.code) -> {
                        val msg = getViewModel().qrStatusMessage.value
                        showProgress(msg)
                    }
                    it.equals(QrStatus.QR002.code) -> {
                        val msg = getViewModel().qrStatusMessage.value
                        showProgress(msg)
                    }
                    it.equals(QrStatus.QR003.code) -> {
                        val msg = getViewModel().qrStatusMessage.value
                        hideProgress()
                    }
                    it.equals(QrStatus.QR000.code) -> {
                        hideProgress()
                    }
                    else -> {
                        hideProgress()
                    }
                }
            }
        })
    }

    private fun observeBaseLiveData() {
        getViewModel().isLoading.observe(this, Observer {
            if (it)
                showProgress()
            else
                hideProgress()
        })

        getViewModel().message.observe(this, Observer {
            showNormalMessage(
                MessageConfig.Builder()
                    .message(it)
                    .positiveLabel(getString(R.string.title_ok))
                    .onPositiveClick {
                        setResultAndFinish()
                    })
        })
    }

    private fun initOnClick() {
        iv_back.setOnClickListener {
            getViewModel().stopTimer()
            setResult(RESULT_OK)
            finish()
        }
    }

    abstract fun setResultAndFinish()

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayout(): Int {
        return R.layout.activity_dynamic_qr
    }

    override fun getViewModel(): DynamicQrBaseViewModel {
        return ViewModelProviders.of(this).get(DynamicQrBaseViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        registerReprintBroadcastReceivers()
    }

    override fun onStop() {
        unregisterReprintBroadCastReceivers()
        super.onStop()
    }

    override fun onDestroy() {
        getViewModel().dispose()
        super.onDestroy()
    }

    override fun onBackPressed() {
        getViewModel().stopTimer()
        setResult(RESULT_OK)
        finish()
        super.onBackPressed()
    }
}