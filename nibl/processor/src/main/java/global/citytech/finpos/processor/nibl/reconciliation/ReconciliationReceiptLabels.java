package global.citytech.finpos.processor.nibl.reconciliation;

/**
 * Created by Unique Shakya on 9/22/2020.
 */
public class ReconciliationReceiptLabels {

    public static final String SETTLEMENT_REPORT = "SETTLEMENT REPORT";
    public static final String CUMULATIVE = "CUMULATIVE REPORT";
    public static final String TERMINAL_ID = "TERMINAL ID";
    public static final String MERCHANT_ID = "MERCHANT ID";
    public static final String DATE_TIME = "DATE / TIME";
    public static final String BATCH_NUMBER = "BATCH NO";
    public static final String HOST = "HOST";
    public static final String APPLICATION_VERSION = "APP VERSION";
    public static final String SALE = "SALE";
    public static final String CASH_ADVANCE = "CASH";
    public static final String REFUND = "REFUND";
    public static final String VOID = "VOID";
    public static final String CASH_VOID = "VOID CASH ADVANCE";
    public static final String TOTAL = "TOTAL";
    public static final String DIVIDER = "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --";
    public static final String LINE_BREAK = "  ";
    public static final String SETTLEMENT_SUCCESS = "SETTLEMENT SUCCESSFUL";
    public static final String SETTLEMENT_FAILURE = "SETTLEMENT UNSUCCESSFUL";
}
