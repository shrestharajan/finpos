package global.citytech.finpos.merchant.domain.model.receipt.duplicate

import global.citytech.finposframework.hardware.io.printer.PrinterService
import global.citytech.finposframework.repositories.TransactionRepository

data class DuplicateReceiptRequestEntity(
    var transactionRepository: TransactionRepository? = null,
    val printerService: PrinterService? = null,
    val stan:String? = null
)
