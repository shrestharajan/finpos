package global.citytech.finpos.processor.nibl.receipt.summaryreport;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.summaryreport.SummaryReportRequestParameter;

/**
 * Created by Unique Shakya on 1/5/2021.
 */
public class SummaryReportRequestModel implements UseCase.Request, SummaryReportRequestParameter {

    private ApplicationRepository applicationRepository;
    private ReconciliationRepository reconciliationRepository;
    private PrinterService printerService;

    public ApplicationRepository getApplicationRepository() {
        return applicationRepository;
    }

    public ReconciliationRepository getReconciliationRepository() {
        return reconciliationRepository;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public static final class Builder {
        private ApplicationRepository applicationRepository;
        private ReconciliationRepository reconciliationRepository;
        private PrinterService printerService;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public Builder withReconciliationRepository(ReconciliationRepository reconciliationRepository) {
            this.reconciliationRepository = reconciliationRepository;
            return this;
        }

        public Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public SummaryReportRequestModel build() {
            SummaryReportRequestModel summaryReportRequestModel = new SummaryReportRequestModel();
            summaryReportRequestModel.printerService = this.printerService;
            summaryReportRequestModel.applicationRepository = this.applicationRepository;
            summaryReportRequestModel.reconciliationRepository = this.reconciliationRepository;
            return summaryReportRequestModel;
        }
    }
}
