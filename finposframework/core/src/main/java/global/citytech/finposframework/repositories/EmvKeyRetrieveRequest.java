package global.citytech.finposframework.repositories;

public class EmvKeyRetrieveRequest {
    String rid;
    String index;
    FieldAttributes field;

    public EmvKeyRetrieveRequest(String rid, String index, FieldAttributes field) {
        this.rid = rid;
        this.index = index;
        this.field = field;
    }

    public String getRid() {
        return rid;
    }

    public String getIndex() {
        return index;
    }

    public FieldAttributes getField() {
        return field;
    }
}
