package global.citytech.neps.data.datasource.local

import global.citytech.neps.domain.model.*
import io.reactivex.Observable


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
interface LocalDataSource {
    fun getHost(): Observable<List<Host>>
    fun getMerchant(): Observable<List<Merchant>>
    fun getLogo(): Observable<List<Logo>>
    fun getAidParams():Observable<List<AidParam>>
    fun getCardScheme():Observable<List<CardScheme>>
    fun getEmvKeys():Observable<List<EmvKey>>
    fun getEmvParams():Observable<List<EmvParam>>
}