package global.citytech.finpos.processor.nibl.transaction.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Unique Shakya on 8/27/2020.
 */
public class TransactionUtils {

    private TransactionUtils() {
    }

    public static global
            .citytech
            .finpos.processor
            .nibl
            .transaction
            .TransactionType mapPurchaseTypeWithTransactionType(TransactionType transactionType) {
        if (transactionType == null)
            return null;

        switch (transactionType) {
            case PURCHASE:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType.PURCHASE;
            case PRE_AUTH:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType.PRE_AUTH;
            case VOID:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType.VOID;
            case REFUND:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType.REFUND;
            case AUTH_COMPLETION:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType.PRE_AUTH_COMPLETION;
            case CASH_ADVANCE:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType.CASH_ADVANCE;
            case CASH_VOID:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType.VOID;
            case TIP_ADJUSTMENT:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType.TIP_ADJUSTMENT;
            case GREEN_PIN:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType.GREEN_PIN;
            case PIN_CHANGE:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType.PIN_CHANGE;
            case BALANCE_INQUIRY:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType.BALANCE_INQUIRY;
            case CASH_IN:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType.CASH_IN;
            case MINI_STATEMENT:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType.MINI_STATEMENT;
            default:
                return null;
        }
    }

    public static List<CardType> getAllowedCardEntryModes(TransactionType transactionType) {
        return getAllowedCardEntryModesForPurchase(transactionType);
    }

    private static List<CardType> getAllowedCardEntryModesForPurchase(TransactionType transactionType) {
        List<CardType> cardTypes = new ArrayList<>();
        switch (transactionType) {
            case GREEN_PIN:
            case PIN_CHANGE:
            case MINI_STATEMENT:
            case CASH_IN:
            case BALANCE_INQUIRY:
                cardTypes.add(CardType.ICC);
                break;
            default:
                cardTypes.add(CardType.MAG);
                cardTypes.add(CardType.ICC);
                cardTypes.add(CardType.PICC);
                break;
        }
        return cardTypes;
    }

    public static String retrieveTransactionType9C(TransactionType transactionType) {
        switch (transactionType) {
            case PRE_AUTH:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType
                        .PRE_AUTH
                        .getProcessingCode()
                        .substring(0, 2);

            case REFUND:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType
                        .REFUND
                        .getProcessingCode()
                        .substring(0, 2);

            case VOID:
            case CASH_VOID:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType
                        .VOID
                        .getProcessingCode()
                        .substring(0, 2);

            case PURCHASE:
            default:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType
                        .PURCHASE
                        .getProcessingCode()
                        .substring(0, 2);

            case CASH_ADVANCE:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType
                        .CASH_ADVANCE
                        .getProcessingCode()
                        .substring(0, 2);
            case GREEN_PIN:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType
                        .GREEN_PIN
                        .getProcessingCode()
                        .substring(0, 2);
            case PIN_CHANGE:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType
                        .PIN_CHANGE
                        .getProcessingCode()
                        .substring(0, 2);
            case BALANCE_INQUIRY:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType
                        .BALANCE_INQUIRY
                        .getProcessingCode()
                        .substring(0, 2);
            case CASH_IN:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType
                        .CASH_IN
                        .getProcessingCode()
                        .substring(0, 2);
            case MINI_STATEMENT:
                return global.citytech.finpos.processor.nibl.transaction.TransactionType
                        .MINI_STATEMENT
                        .getProcessingCode()
                        .substring(0, 2);
        }
    }

    public static boolean isEmvTransaction(ReadCardResponse readCardResponse) {
        return readCardResponse.getCardDetails().getCardType() == CardType.ICC ||
                readCardResponse.getCardDetails().getCardType() == CardType.PICC;
    }

    public static String retrievePinPadMessageWithAmount(BigDecimal amount, String currencyName) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(StringUtils.formatAmountTwoDecimal(amount));
        return stringBuilder.toString();
    }

    public static String retrieveMtiForTransactionType(global.citytech.finpos.processor.nibl.transaction.TransactionType transactionType) {
        switch (transactionType) {
            case PURCHASE:
            case CASH_ADVANCE:
            case VOID:
            case REFUND:
            case MINI_STATEMENT:
            case PURCHASE_WITH_CASHBACK:
                return "0200";
            case GREEN_PIN:
            case PIN_CHANGE:
            case BALANCE_INQUIRY:
            case CASH_IN:
            case PRE_AUTH:
                return "0100";
            case PRE_AUTH_COMPLETION:
            case TIP_ADJUSTMENT:
                return "0220";
            case REVERSAL:
                return "0420";
            case SETTLEMENT:
                return "0520";
            default:
                return "";
        }
    }
}
