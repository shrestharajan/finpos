package global.citytech.finposframework.usecases.transaction.receipt.transaction

class TransactionReadCardResponse(
    val cardDetails: CardDetail
) {

}


class CardDetail(
    val cardSchemeLabel
    : String = "",
    val cardScheme: String = ""
) {

}