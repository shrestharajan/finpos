package global.citytech.finpos.merchant.presentation.model.setting

data class ReceiptConfiguration (
    val enableCustomerDetails: Boolean,
    val enableCardDetails: Boolean
)