package global.citytech.finpos.neps.iso8583.requestsender.voidsale;

import java.math.BigDecimal;

import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finposframework.switches.PosEntryMode;

public class VoidSaleIsoRequest extends TransactionIsoRequest {
    private String stan;
    private String processingCode;
    private String rrn;
    private PosEntryMode posEntryMode;
    private String posConditionCode;

    public String getStan() {
        return stan;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public String getRrn() {
        return rrn;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public static final class Builder {
        private String pan;
        private BigDecimal transactionAmount;
        private String localDateTime;
        private String track2Data;
        private String additionalData;
        private String currencyCode;
        private String pinBlock;
        private String emvData;
        private String cvv;
        private boolean isFallbackFromIccToMag;
        private String stan;
        private String processingCode;
        private String rrn;
        private PosEntryMode posEntryMode;
        private String posConditionCode;

        private  Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder stan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public Builder localDateTime(String localDataTime) {
            this.localDateTime = localDataTime;
            return this;
        }

        public Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public Builder additionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder pinBlock(String pinBlock) {
            this.pinBlock = pinBlock;
            return this;
        }

        public Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public Builder processingCode(String processingCode) {
            this.processingCode = processingCode;
            return this;
        }

        public Builder rrn(String rrn) {
            this.rrn = rrn;
            return this;
        }

        public Builder fallBackFromIccToMag(boolean isFallbackFromIccToMag) {
            this.isFallbackFromIccToMag = isFallbackFromIccToMag;
            return this;
        }

        public Builder posEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public Builder posConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public VoidSaleIsoRequest build() {
            VoidSaleIsoRequest voidSaleIsoRequest = new VoidSaleIsoRequest();
            voidSaleIsoRequest.pinBlock = this.pinBlock;
            voidSaleIsoRequest.currencyCode = this.currencyCode;
            voidSaleIsoRequest.pan = this.pan;
            voidSaleIsoRequest.track2Data = this.track2Data;
            voidSaleIsoRequest.transactionAmount = this.transactionAmount;
            voidSaleIsoRequest.emvData = this.emvData;
            voidSaleIsoRequest.additionalData = this.additionalData;
            voidSaleIsoRequest.isFallbackFromIccToMag = this.isFallbackFromIccToMag;
            voidSaleIsoRequest.localDateTime = this.localDateTime;
            voidSaleIsoRequest.processingCode = this.processingCode;
            voidSaleIsoRequest.rrn = this.rrn;
            voidSaleIsoRequest.stan = this.stan;
            voidSaleIsoRequest.posEntryMode = this.posEntryMode;
            voidSaleIsoRequest.posConditionCode = this.posConditionCode;
            return voidSaleIsoRequest;
        }
    }
}
