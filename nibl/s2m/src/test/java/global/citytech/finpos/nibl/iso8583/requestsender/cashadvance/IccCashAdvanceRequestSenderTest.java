package global.citytech.finpos.nibl.iso8583.requestsender.cashadvance;

import org.junit.Test;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.NIBLHostConfig;
import global.citytech.finpos.nibl.iso8583.MockValues;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.utility.IsoMessageUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Unique Shakya on 2/4/2021.
 */
public class IccCashAdvanceRequestSenderTest {

    private Logger logger = Logger.getLogger(IccCashAdvanceRequestSenderTest.class.getName());
    
    @Test
    public void send() {
        try {
            String terminalID = "99991002";
            String acquirerID = "405635000000002";
            TerminalInfo terminalInfo = new TerminalInfo(terminalID, acquirerID,"","Citytech","Kamaladi");
            HostInfo primaryHost = NIBLHostConfig.getPrimaryConfig();
            HostInfo secondaryHost = NIBLHostConfig.getSecondaryConfig();
            ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);
            RequestContext context =
                    new RequestContext(
                            "000405", terminalInfo, connectionParam);
            CashAdvanceRequest request = CashAdvanceRequest.Builder.newInstance()
                    .withTransactionAmount(105.0)
                    .withPosConditionCode("00")
                    .withPinBlock("7C8DE0C78AEA2044")
                    .withInvoiceNumber("000108")
                    .withCardSequenceNumber("01")
                    .withTrack2Data(MockValues.TRACK_2_DATA)
                    .withEmvData("82023C009F02060000000000289F03060000000000004F07A0000000031010500B56495341204352454449549F120B56495341204352454449549F360200CA9F26085C4637D46AFA9A359F2701809F34034203008407A00000000310109F100706010A03A0A8009F1E0831313130303837379F3303E0F8C89F350122950508000400009F1A0205245F2A0205249A032008289C01009F370460C801959F090200969F4104000012345F340101")
                    .build();
            context.setRequest(request);
            IccCashAdvanceRequestSender sender = new IccCashAdvanceRequestSender(
                    new NIBLSpecInfo(),
                    context
            );
            IsoMessageResponse response = sender.send();
            assertNotNull(response);
            assertEquals(0, IsoMessageUtils.retrieveFromDataElementsAsInt(response, DataElement.RESPONSE_CODE));
        } catch (FinPosException ex) {
            logger.log(ex.getMessage());
            logger.log(ex.getDetailMessage());
        }
    }
}
