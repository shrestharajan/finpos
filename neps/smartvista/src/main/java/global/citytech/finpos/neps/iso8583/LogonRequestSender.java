package global.citytech.finpos.neps.iso8583;


import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.switches.FunctionCode;

/**
 * User: Surajchhetry Date: 2/28/20 Time: 3:53 PM
 */
public class LogonRequestSender extends NepsMessageSenderTemplate {

    private Logger logger = Logger.getLogger(LogonRequestSender.class.getName());
    public LogonRequestSender(RequestContext context) {
        super(context);
    }

    @Override
    protected MessagePayload prepareRequestPayload() {

        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Network_Management,
                        MTI.MessageFunction.Request,
                        MTI.Originator.Acquirer);
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);
        MessagePayload.Builder builder = new NepsMessagePayloadBuilder(this.specInfo, msg);
        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());
        return builder.build();
    }

    protected String getProcessingCode() {
        return ProcessingCode.LOG_ON.getCode();
    }

    protected String getFunctionCode() {
        return FunctionCodeConfig.getValue(FunctionCode.LOG_ON);
    }
}
