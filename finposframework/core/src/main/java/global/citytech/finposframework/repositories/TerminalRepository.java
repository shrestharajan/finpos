package global.citytech.finposframework.repositories;

import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.usecases.activity.ActivityLog;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;

/** User: Surajchhetry Date: 2/28/20 Time: 3:33 PM */
public interface TerminalRepository {
  String getSystemTraceAuditNumber();
  void incrementSystemTraceAuditNumber();
  TerminalInfo findTerminalInfo();
  HostInfo findPrimaryHost();
  HostInfo findSecondaryHost();
  String getReconciliationBatchNumber();
  void incrementReconciliationBatchNumber();
  String getRetrievalReferenceNumber();
  void incrementRetrievalReferenceNumber();
  String getInvoiceNumber();
  void incrementInvoiceNumber();
  String getTerminalTransactionCurrencyName();
  String getApplicationVersion();
  boolean isVatRefundSupported();
  boolean shouldShufflePinPad();
  void updateActivityLog(ActivityLog activityLog);
  String getTerminalSerialNumber();
}
