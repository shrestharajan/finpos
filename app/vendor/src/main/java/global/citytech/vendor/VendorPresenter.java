package global.citytech.vendor;

import java.util.logging.Logger;

import citytech.global.easydroid.concurrent.LongRunningTask;
import global.citytech.common.Constants;
import global.citytech.easydroid.core.utils.MessageConfig;
import global.citytech.payment.sdk.api.PaymentResponse;
import global.citytech.payment.sdk.api.PaymentResult;
import global.citytech.payment.sdk.api.PaymentService;
import global.citytech.payment.sdk.api.PosPaymentApi;
import global.citytech.payment.sdk.api.PosPaymentConfiguration;
import global.citytech.payment.sdk.api.PurchasePaymentRequest;
import global.citytech.payment.sdk.api.VoidPaymentRequest;

/**
 * Created by Rishav Chudal on 6/9/21.
 */
class VendorPresenter {
    private final VendorView vendorView;
    private static final String TRANSACTION_CURRENCY_CODE = "0682";
    private static final String VENDOR_PRIVATE_KEY = Constants.VENDOR_PRIVATE_KEY;

    private static final String FINPOS_PUBLIC_KEY = Constants.FINPOS_PUBLIC_KEY;
    private final Logger logger;

    public VendorPresenter(VendorView vendorView) {
        this.vendorView = vendorView;
        logger = Logger.getLogger(VendorPresenter.class.getSimpleName());
    }

    public void checkoutToCashierForPurchasePayment(PaymentDetails paymentDetails) {
        vendorView.showProgressDialogWithMessage("Proceeding Checkout...");
        LongRunningTask longRunningTask = new LongRunningTask.Builder()
                .withTask(() -> taskPurchasePayment(paymentDetails))
                .observeInUIThread(vendorView.getUiContext())
                .onCompleted(this::onCheckoutResponseReceived)
                .onError(this::handleErrorInPaymentCheckout)
                .build();
        longRunningTask.execute();
    }

    private PaymentResponse taskPurchasePayment(PaymentDetails paymentDetails) {
        PurchasePaymentRequest purchasePaymentRequest = preparePurchasePaymentRequest(paymentDetails);
        PosPaymentConfiguration paymentConfiguration = getPosPaymentConfiguration(paymentDetails);
        PaymentService paymentService = PosPaymentApi.getInstance(
                vendorView.getUiContext(),
                paymentConfiguration
        );
        return paymentService.doPurchase(purchasePaymentRequest);
    }

    private PurchasePaymentRequest preparePurchasePaymentRequest(PaymentDetails paymentDetails) {
        double transactionAmount = formattedAmountInDouble(paymentDetails.getAmount());
        return new PurchasePaymentRequest(
                transactionAmount,
                TRANSACTION_CURRENCY_CODE
        );
    }

    private PosPaymentConfiguration getPosPaymentConfiguration(PaymentDetails paymentDetails) {
        return new PosPaymentConfiguration(
                paymentDetails.getApplicationIdentifier(),
                paymentDetails.getPaymentOriginator(),
                FINPOS_PUBLIC_KEY,
                VENDOR_PRIVATE_KEY
        );
    }

    private double formattedAmountInDouble(String amount) {
        return Double.parseDouble(amount);
    }

    private void onCheckoutResponseReceived(Object response) {
        vendorView.hideProgressDialog();
        validateCheckoutResponseAndProceed(response);
    }

    private void validateCheckoutResponseAndProceed(Object response) {
        if (response instanceof PaymentResponse) {
            PaymentResponse paymentResponse = (PaymentResponse) response;
            checkForResponseResultAndProceed(paymentResponse);
        }
    }

    private void checkForResponseResultAndProceed(PaymentResponse paymentResponse) {
        vendorView.hideProgressDialog();
        if (paymentResponse.getResultCode() == PaymentResult.SUCCESS.getResultCode()) {
            onCheckoutResultCodeSuccess(paymentResponse);
        } else {
            onCheckoutResultCodeNotSuccess(paymentResponse);
        }
    }

    private void onCheckoutResultCodeSuccess(PaymentResponse paymentResponse) {
        MessageConfig.Builder builder = new MessageConfig.Builder();
        builder.message(
                "Payment Success" +
                        "\n Card Number ::: " + paymentResponse.getCardNumber() +
                        "\n Auth Code ::: " + paymentResponse.getAuthCode() +
                        "\n Message ::: " + paymentResponse.getMessage()
        );
        builder.positiveLabel("OK");
        builder.onPositiveClick((dialogInterface, i) -> dialogInterface.dismiss());
        vendorView.displayAlertMessage(builder);
    }

    private void onCheckoutResultCodeNotSuccess(PaymentResponse paymentResponse) {
        MessageConfig.Builder builder = new MessageConfig.Builder();
        builder.message(
                "Payment Failed" +
                        "\n\nMessage ::: " + paymentResponse.getMessage()
        );
        builder.positiveLabel("OK");
        builder.onPositiveClick((dialogInterface, i) -> dialogInterface.dismiss());
        vendorView.displayAlertMessage(builder);
    }

    private void handleErrorInPaymentCheckout(Throwable throwable) {
        vendorView.hideProgressDialog();
        logger.info("Throwable Error ::: " + throwable.getMessage());
        MessageConfig.Builder builder = new MessageConfig.Builder();
        builder.message(
                "Payment Failed" +
                        "\n\nMessage ::: " + throwable.getMessage()
        );
        builder.positiveLabel("OK");
        builder.onPositiveClick((dialogInterface, i) -> dialogInterface.dismiss());
        vendorView.displayAlertMessage(builder);
    }

    public void checkoutToCashierForVoidPayment(PaymentDetails paymentDetails) {
        vendorView.showProgressDialogWithMessage("Proceeding Checkout for Void...");
        LongRunningTask longRunningTask = new LongRunningTask.Builder()
                .withTask(() -> taskVoid(paymentDetails))
                .observeInUIThread(vendorView.getUiContext())
                .onCompleted(this::onCheckoutResponseReceived)
                .onError(this::handleErrorInPaymentCheckout)
                .build();
        longRunningTask.execute();
    }

    private PaymentResponse taskVoid(PaymentDetails paymentDetails) {
        VoidPaymentRequest voidPaymentRequest = prepareVoidPaymentRequest(paymentDetails);
        PosPaymentConfiguration paymentConfiguration = getPosPaymentConfiguration(paymentDetails);
        PaymentService paymentService = PosPaymentApi.getInstance(
                vendorView.getUiContext(),
                paymentConfiguration
        );
        return paymentService.performVoid(voidPaymentRequest);
    }

    private VoidPaymentRequest prepareVoidPaymentRequest(PaymentDetails paymentDetails) {
        double billedAmount = formattedAmountInDouble(paymentDetails.getAmount());
        return new VoidPaymentRequest(
                paymentDetails.getRetrievalReferenceNumber(),
                billedAmount
        );
    }
}
