package global.citytech.finpos.neps.iso8583.requestsender.transaction;

import global.citytech.finpos.neps.iso8583.FunctionCodeConfig;
import global.citytech.finpos.neps.iso8583.NepsMessagePayloadBuilder;
import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.PosConditionCode;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.switches.FunctionCode;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Saurav Ghimire on 2/3/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public abstract class TransactionRequestSender extends NepsMessageSenderTemplate {

    private final Logger logger = Logger.getLogger(TransactionRequestSender.class.getSimpleName());
    protected TransactionIsoRequest transactionIsoRequest;

    public TransactionRequestSender(RequestContext context) {
        super(context);

    }

    protected abstract String getClassName();

    protected abstract CardTypeISOMessageUpdater getCardTypeSender();

    protected abstract TransactionTypeISOMessageUpdater getTransactionTypeSender();

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        getMessageClass(),
                        getMessageFunctionRequest(),
                        MTI.Originator.Acquirer);
        transactionIsoRequest = (TransactionIsoRequest) this.context.getRequest();
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);


        msg.addField(
                new DataElement(
                        DataElement.PRIMARY_ACCOUNT_NUMBER,
                        transactionIsoRequest.getPan())
        );

        msg.addField(
                new DataElement(
                        DataElement.AMOUNT,
                        HelperUtils.toISOAmount(transactionIsoRequest.getTransactionAmount(), 12)
                )
        );

        msg.addField(
                new DataElement(
                        DataElement.LOCAL_TIME,
                        transactionIsoRequest.getLocalDateTime())
        );

        msg.addField(
                new DataElement(
                        DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE,
                        PosConditionCode.ATTENDANT_TERMINAL.getValue()
                )
        );

        if (transactionIsoRequest.getAdditionalData() != null) {
            msg.addField(
                    new DataElement(
                            DataElement.ADDITIONAL_DATA_PRIVATE,
                            transactionIsoRequest.getAdditionalData()
                    )
            );
        }

        msg.addField(
                new DataElement(
                        DataElement.CURRENCY_CODE,
                        transactionIsoRequest.getCurrencyCode())
        );


        if (!StringUtils.isEmpty(transactionIsoRequest.getPinBlock()))
            msg.addField(
                    new DataElement(
                            DataElement.PIN_BLOCK,
                            transactionIsoRequest.getPinBlock()
                    )
            );

        if (!StringUtils.isEmpty(transactionIsoRequest.getCvv()))
            msg.addField(
                    new DataElement(
                            DataElement.ADDITIONAL_DATA_PRIVATE,
                            transactionIsoRequest.getCvv()
                    )
            );

        IsoMessageUpdater isoMessageUpdater = new IsoMessageUpdater(getCardTypeSender(), getTransactionTypeSender());
        msg = isoMessageUpdater.update(transactionIsoRequest, msg);

        MessagePayload.Builder builder = new NepsMessagePayloadBuilder(this.specInfo, msg);
        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());
        return builder.build();
    }

    protected abstract MTI.MessageFunction getMessageFunctionRequest();

    protected abstract MTI.MessageClass getMessageClass();


    @Override
    protected String getProcessingCode() {
        return getTransactionTypeSender().getProcessingCode();
    }

    @Override
    protected String getFunctionCode() {
        return FunctionCodeConfig.getValue(FunctionCode.TRANSACTION);
    }
}
