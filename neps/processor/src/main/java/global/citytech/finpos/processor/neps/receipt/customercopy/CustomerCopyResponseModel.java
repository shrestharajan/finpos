package global.citytech.finpos.processor.neps.receipt.customercopy;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.customercopy.CustomerCopyResponseParameter;

public class CustomerCopyResponseModel implements UseCase.Response, CustomerCopyResponseParameter {
    private final Result result;
    private final String message;

    public CustomerCopyResponseModel(Result result, String message) {
        this.result = result;
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }
}