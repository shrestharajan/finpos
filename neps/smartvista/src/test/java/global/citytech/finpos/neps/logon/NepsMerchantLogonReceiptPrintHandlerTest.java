package global.citytech.finpos.neps.logon;

import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import global.citytech.finpos.neps.receipt.logon.NepsMerchantLogonReceiptLabels;
import global.citytech.finpos.neps.receipt.logon.NepsMerchantLogonReceiptPrintHandler;
import global.citytech.finposframework.hardware.io.printer.PrintMessage;
import global.citytech.finposframework.hardware.io.printer.PrinterRequest;
import global.citytech.finposframework.hardware.io.printer.PrinterResponse;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.io.printer.Style;
import global.citytech.finposframework.usecases.transaction.receipt.logon.MerchantLogonReceipt;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;

public class NepsMerchantLogonReceiptPrintHandlerTest {

    PrinterService printerService;
    PrinterResponse printerResponse;
    MerchantLogonReceipt merchantLogonReceipt;

    //   @Before
    public void setup() {
        printerService = PowerMockito.mock(PrinterService.class);
        printerResponse = PowerMockito.mock(PrinterResponse.class);
        merchantLogonReceipt = PowerMockito.mock(MerchantLogonReceipt.class);
    }

    //   @Test
    public void printLogonReceiptWhenSuccess() {
        NepsMerchantLogonReceiptPrintHandler printHandler = new NepsMerchantLogonReceiptPrintHandler(printerService);
        PowerMockito.when(merchantLogonReceipt.getMessage()).thenReturn("LOGON SUCCESSFUL");

        ArgumentCaptor<PrinterRequest> argumentCaptor = ArgumentCaptor.forClass(PrinterRequest.class);
        printHandler.printLogonReceipt(merchantLogonReceipt);

        Mockito.verify(this.printerService).print(argumentCaptor.capture());
        assertEquals(NepsMerchantLogonReceiptLabels.DIVIDER, ((PrintMessage) argumentCaptor.getValue().getPrintableList().get(0).getData()).getValue());
        assertEquals(Style.Align.CENTER, ((PrintMessage) argumentCaptor.getValue().getPrintableList().get(0).getData()).getStyle().getAlignment());
        assertEquals(Style.FontSize.NORMAL, ((PrintMessage) argumentCaptor.getValue().getPrintableList().get(0).getData()).getStyle().getFontSize());
        assertFalse(((PrintMessage) argumentCaptor.getValue().getPrintableList().get(0).getData()).getStyle().isAllCaps());
        assertEquals(NepsMerchantLogonReceiptLabels.SUCCESS_MESSAGE, ((PrintMessage) argumentCaptor.getValue().getPrintableList().get(1).getData()).getValue());
    }

    //  @Test
    public void printLogonReceiptWhenFailure() {
        NepsMerchantLogonReceiptPrintHandler printHandler = new NepsMerchantLogonReceiptPrintHandler(printerService);
        PowerMockito.when(merchantLogonReceipt.getMessage()).thenReturn("LOGON FAILURE");

        ArgumentCaptor<PrinterRequest> argumentCaptor = ArgumentCaptor.forClass(PrinterRequest.class);
        printHandler.printLogonReceipt(merchantLogonReceipt);

        Mockito.verify(this.printerService).print(argumentCaptor.capture());
        assertEquals(NepsMerchantLogonReceiptLabels.DIVIDER, ((PrintMessage) argumentCaptor.getValue().getPrintableList().get(0).getData()).getValue());
        assertEquals(Style.Align.CENTER, ((PrintMessage) argumentCaptor.getValue().getPrintableList().get(0).getData()).getStyle().getAlignment());
        assertEquals(Style.FontSize.NORMAL, ((PrintMessage) argumentCaptor.getValue().getPrintableList().get(0).getData()).getStyle().getFontSize());
        assertFalse(((PrintMessage) argumentCaptor.getValue().getPrintableList().get(0).getData()).getStyle().isAllCaps());
        assertEquals(NepsMerchantLogonReceiptLabels.FAILURE_MESSAGE, ((PrintMessage) argumentCaptor.getValue().getPrintableList().get(1).getData()).getValue());
    }
}
