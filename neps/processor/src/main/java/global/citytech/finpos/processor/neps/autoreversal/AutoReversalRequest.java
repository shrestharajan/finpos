package global.citytech.finpos.processor.neps.autoreversal;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.autoreversal.AutoReversalRequestParameter;

/**
 * Created by Saurav Ghimire on 7/12/21.
 * sauravnghimire@gmail.com
 */


public class AutoReversalRequest implements UseCase.Request, AutoReversalRequestParameter {
    private TransactionRepository transactionRepository;
    private PrinterService printerService;
    private ApplicationRepository applicationRepository;

    public PrinterService getPrinterService() {
        return printerService;
    }

    public ApplicationRepository getApplicationRepository() {
        return applicationRepository;
    }

    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }


    public static final class Builder {
        private TransactionRepository transactionRepository;
        private PrinterService printerService;
        private ApplicationRepository applicationRepository;

        public Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder transactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public Builder printerService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public Builder applicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public AutoReversalRequest build() {
            AutoReversalRequest autoReversalRequest = new AutoReversalRequest();
            autoReversalRequest.applicationRepository = this.applicationRepository;
            autoReversalRequest.transactionRepository = this.transactionRepository;
            autoReversalRequest.printerService = this.printerService;
            return autoReversalRequest;
        }
    }
}
