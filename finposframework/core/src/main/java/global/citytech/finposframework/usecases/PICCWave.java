package global.citytech.finposframework.usecases;

public class PICCWave {

    private static PICCWave INSTANCE = null;

    private int count;

    private PICCWave() {
    }

    public static PICCWave getInstance() {
        if (INSTANCE == null)
            INSTANCE = new PICCWave();
        return INSTANCE;
    }

    public boolean isPICCWaveAgainAllowed() {
        return this.count < 3;
    }

    public void onPICCWaveFailure() {
        this.count++;
    }

    public void onPICCWaveSuccess() {
        this.count = 0;
    }

    public void onPICCWaveFailureLimitReached() {
        this.count = 0;
    }

    public void clear(){
        INSTANCE = null;
    }
}
