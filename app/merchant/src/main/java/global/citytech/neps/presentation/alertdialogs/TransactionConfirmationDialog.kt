package global.citytech.neps.presentation.alertdialogs

import android.app.Activity
import android.app.AlertDialog
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import global.citytech.finpos.core.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.neps.R

class TransactionConfirmationDialog constructor(
    activity: Activity, listener: Listener, transactionConfirmation: TransactionConfirmation
) {
    private lateinit var tvTitle: TextView
    private lateinit var tvMessage: TextView
    private lateinit var imgResult: ImageView
    private lateinit var btnPositive: Button
    private lateinit var btnNegative: Button
    private lateinit var llButtons: LinearLayout

    private var alertDialog: AlertDialog

    private var listener: Listener
    private var transactionConfirmation: TransactionConfirmation

    init {
        this.listener = listener
        this.transactionConfirmation = transactionConfirmation
        val alertDialogBuilder = AlertDialog.Builder(activity)
        val inflater = activity.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_transaction_confirmation, null)
        alertDialogBuilder.setView(dialogView)
        initViews(dialogView)
        setViewAttributes()
        handleClickEvents()
        alertDialog = alertDialogBuilder.create()
        alertDialog.setCancelable(false)
    }

    fun show() {
        alertDialog.show()
    }

    fun hide() {
        alertDialog.hide()
    }

    fun update(title: String, authorizationCode: String, listener: Listener) {
        showButtons()
        tvTitle.text = title
        tvMessage.text = authorizationCode
        this.listener = listener
    }

    private fun showButtons() {
        llButtons.visibility = View.VISIBLE
        btnNegative.visibility = View.VISIBLE
        btnPositive.visibility = View.VISIBLE
    }

    private fun handleClickEvents() {
        this.btnPositive.setOnClickListener {
            this.hide()
            this.listener.onPositiveButtonClick()
        }

        this.btnNegative.setOnClickListener {
            this.hide()
            this.listener.onNegativeButtonClick()
        }
    }

    private fun setViewAttributes() {
        tvTitle.text = this.transactionConfirmation.title
        showButtons()
        if (!this.transactionConfirmation.negativeLabel.isNullOrEmptyOrBlank()) {
            btnNegative.text = this.transactionConfirmation.negativeLabel
            btnNegative.visibility = View.VISIBLE
        }
        if (!this.transactionConfirmation.positiveLabel.isNullOrEmptyOrBlank()) {
            btnPositive.text = this.transactionConfirmation.positiveLabel
            btnPositive.visibility = View.VISIBLE
        }
        tvMessage.text = this.transactionConfirmation.message
        imgResult.setImageResource(this.transactionConfirmation.imageId)
    }

    private fun initViews(dialogView: View) {
        llButtons = dialogView.findViewById(R.id.ll_buttons)
        tvTitle = dialogView.findViewById(R.id.tv_title)
        tvMessage = dialogView.findViewById(R.id.tv_message)
        imgResult = dialogView.findViewById(R.id.img_transaction_result)
        btnPositive = dialogView.findViewById(R.id.btn_positive)
        btnNegative = dialogView.findViewById(R.id.btn_negative)
    }

    interface Listener {
        fun onPositiveButtonClick()
        fun onNegativeButtonClick()
    }
}