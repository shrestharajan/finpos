package global.citytech.payment.sdk.core;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.gson.Gson;

import java.util.logging.Logger;

import global.citytech.crypto.PosCryptoApi;
import global.citytech.payment.sdk.api.PaymentResponse;
import global.citytech.payment.sdk.api.PaymentResult;
import global.citytech.payment.sdk.api.PosPaymentConfiguration;
import global.citytech.payment.sdk.utils.JsonUtils;
import global.citytech.payment.sdk.utils.StringUtils;

/**
 * Created by BikashShrestha on 6/5/19.
 */
public final class SdkPaymentActivity extends Activity {

    public static final String PAYMENT_COMPLETION_LISTENER = "extra_listener";
    private static final int PAYMENT_REQUEST_CODE = 1011;
    private static final String PAYMENT_ACTIVITY_PACKAGE_NAME = "global.citytech.payment.sdk.core";
    private final Gson gson = new Gson();
    private final Logger logger = Logger.getLogger(SdkPaymentActivity.class.getSimpleName());
    private PaymentCompletionListener paymentCompletionListener;
    private PaymentResponse paymentResponse = new PaymentResponse(PaymentResult.FAILED);
    private PosPaymentConfiguration posPaymentConfiguration;

    public static Intent prepareIntentForPaymentActivity(
            Context context,
            PosPaymentConfiguration posPaymentConfiguration,
            PaymentRequest paymentRequest,
            PaymentCompletionListener paymentCompletionListener
    ) {
        String paymentJson = JsonUtils.toJsonObj(paymentRequest);
        String configurationJson = JsonUtils.toJsonObj(posPaymentConfiguration);
        Intent intent = new Intent(context, SdkPaymentActivity.class);
        intent.putExtra(PaymentConstants.EXTRA_DATA, paymentJson);
        intent.putExtra(PaymentConstants.EXTRA_CONFIGURATION_DATA, configurationJson);
        intent.putExtra(PAYMENT_COMPLETION_LISTENER, paymentCompletionListener);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assignPaymentCompletionListenerWithReceivedIntentData();
        assignPaymentConfigurationOrNullOnException();
        proceedForPaymentServiceIfValidPosConfiguration();
    }

    private void assignPaymentCompletionListenerWithReceivedIntentData() {
        paymentCompletionListener = getIntent()
                .getParcelableExtra(PAYMENT_COMPLETION_LISTENER);
    }

    private void assignPaymentConfigurationOrNullOnException() {
        try {
            assignPaymentConfigurationWithReceivedIntentData();
        } catch (Exception exception) {
            exception.printStackTrace();
            posPaymentConfiguration = null;
        }
    }

    private void assignPaymentConfigurationWithReceivedIntentData() {
        String extraData = getExtraConfigurationDataFromReceivedIntentData();
        posPaymentConfiguration =
                JsonUtils.fromJsonToObj(extraData, PosPaymentConfiguration.class);
    }

    private void proceedForPaymentServiceIfValidPosConfiguration() {
        if (posPaymentConfiguration != null) {
            encryptExtraDataAndProceedPaymentServiceOnValidEncryptedData();
        } else {
            assignPaymentResponseWithGivenResultAndProceed(PaymentResult.FAILED);
        }
    }

    private void encryptExtraDataAndProceedPaymentServiceOnValidEncryptedData() {
        String encryptedData = encryptedExtraData();
        if (StringUtils.isEmpty(encryptedData)) {
            assignPaymentResponseWithGivenResultAndProceed(PaymentResult.FAILED);
        } else {
            startPaymentServiceWithEncryptedData(encryptedData);
        }
    }

    private String encryptedExtraData() {
        String extraData = getExtraPaymentDataFromReceivedIntentData();
        PosCryptoApi posCryptoApi = new PosCryptoApi();
        return posCryptoApi.encrypt(posPaymentConfiguration.getVendorPrivateKey(), extraData);
    }

    private String getExtraPaymentDataFromReceivedIntentData() {
        return getIntent().getStringExtra(PaymentConstants.EXTRA_DATA);
    }

    private String getExtraConfigurationDataFromReceivedIntentData() {
        return getIntent().getStringExtra(PaymentConstants.EXTRA_CONFIGURATION_DATA);
    }

    private void startPaymentServiceWithEncryptedData(String encryptedData) {
        try {
            startRemotePaymentActivityWithEncryptedData(encryptedData);
        } catch (ActivityNotFoundException activityNotFoundException) {
            this.logger.info("PAYMENT SERVICE NOT AVAILABLE Exception ::: " + activityNotFoundException.getMessage());
            assignPaymentResponseWithGivenResultAndProceed(PaymentResult.POS_PAYMENT_SERVICE_NOT_AVAILABLE);
        } catch (Exception exception) {
            this.logger.info("Exception ::: " + exception.getMessage());
            assignPaymentResponseWithGivenResultAndProceed(PaymentResult.FAILED);
        }
    }

    private void startRemotePaymentActivityWithEncryptedData(String encryptedData) {
        Intent i = new Intent(PAYMENT_ACTIVITY_PACKAGE_NAME);
        i.putExtra(PaymentConstants.EXTRA_DATA, encryptedData);
        i.putExtra(PaymentConstants.EXTRA_APP_ID_DATA, this.getPackageName());
        this.startActivityForResult(i, PAYMENT_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYMENT_REQUEST_CODE) {
            checkOnActivityResultCodeAndProceedWithIntentData(resultCode, data);
        } else {
            this.logger.info("PAYMENT SERVICE NOT AVAILABLE Exception ::: REQUEST CODE NO PAYMENT REQUEST CODE");
            paymentResponse = new PaymentResponse(PaymentResult.POS_PAYMENT_SERVICE_NOT_AVAILABLE);
            returnPaymentResponseOnPaymentCompletion();
        }
    }

    private void checkOnActivityResultCodeAndProceedWithIntentData(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_CANCELED) {
            assignPaymentResponseWithGivenResultAndProceed(PaymentResult.CANCEL);
        } else {
            if (JsonUtils.isValidJsonString(data.getStringExtra(PaymentConstants.EXTRA_DATA))) {
                paymentResponse = JsonUtils.fromJsonToObj(data.getStringExtra(PaymentConstants.EXTRA_DATA), PaymentResponse.class);
                returnPaymentResponseOnPaymentCompletion();
            } else {
                decryptNonNullIntentDataOnly(data);
            }
        }
    }

    private void decryptNonNullIntentDataOnly(Intent data) {
        if (data != null) {
            decryptExtraDataAndReturnPaymentResponse(data);
        } else {
            assignPaymentResponseWithGivenResultAndProceed(PaymentResult.INVALID_DATA);
        }
    }

    private void decryptExtraDataAndReturnPaymentResponse(Intent data) {
        String decryptedData = decryptedExtraData(data);
        if (StringUtils.isEmpty(decryptedData)) {
            assignPaymentResponseWithGivenResultAndProceed(PaymentResult.INVALID_DATA);
        } else {
            getPaymentResponseFromTheDecryptedDataAndReturn(decryptedData);
        }
    }

    private String decryptedExtraData(Intent data) {
        String extraData = data.getStringExtra(PaymentConstants.EXTRA_DATA);
        this.logger.info("Response Encrypted Data: " + extraData);
        PosCryptoApi posCryptoApi = new PosCryptoApi();
        return posCryptoApi.decrypt(posPaymentConfiguration.getPosPublicKey(), extraData);
    }

    private void getPaymentResponseFromTheDecryptedDataAndReturn(String decryptedData) {
        try {
            this.logger.info("Decrypted Data: " + decryptedData);
            paymentResponse = gson.fromJson(decryptedData, PaymentResponse.class);
            returnPaymentResponseOnPaymentCompletion();
        } catch (Exception ex) {
            ex.printStackTrace();
            assignPaymentResponseWithGivenResultAndProceed(PaymentResult.FAILED);
        }
    }

    private void assignPaymentResponseWithGivenResultAndProceed(PaymentResult paymentResult) {
        paymentResponse = new PaymentResponse(paymentResult);
        returnPaymentResponseOnPaymentCompletion();
    }

    private void returnPaymentResponseOnPaymentCompletion() {
        paymentCompletionListener.onPaymentResponseReceived(paymentResponse);
        finish();
    }
}
