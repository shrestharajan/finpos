package global.citytech.finpos.merchant.data.repository.core.receipt

import global.citytech.finpos.merchant.data.datasource.core.receipt.DuplicateReconciliationReceiptDataSource
import global.citytech.finpos.merchant.domain.model.receipt.duplicatereconciliation.DuplicateReconciliationReceiptResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.receipt.DuplicateReconciliationReceiptRepository
import global.citytech.finpos.processor.nibl.receipt.duplicatereconciliation.DuplicateReconciliationReceiptResponseModel
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 12/16/2020.
 */
class DuplicateReconciliationReceiptRepositoryImpl(
    private val duplicateReconciliationReceiptDataSource:
    DuplicateReconciliationReceiptDataSource
) :
    DuplicateReconciliationReceiptRepository {
    override fun printReceipt(): Observable<DuplicateReconciliationReceiptResponseEntity> {
        return duplicateReconciliationReceiptDataSource.printReceipt()
    }
}