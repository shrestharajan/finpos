package global.citytech.finpos.merchant.data.repository.core.preauth.completion

import global.citytech.finpos.merchant.data.datasource.core.preauth.completion.AuthorisationCompletionDataSource
import global.citytech.finpos.merchant.domain.model.auth.completion.AuthorisationCompletionResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.preauth.completion.AuthorisationCompletionRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.preauth.completion.AuthorisationCompletionRequestItem
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/29/2020.
 */
class AuthorisationCompletionRepositoryImpl(private val authorisationCompletionDataSource: AuthorisationCompletionDataSource) :
    AuthorisationCompletionRepository {
    override fun doAuthorisationCompletion(
        configurationItem: ConfigurationItem,
        authorisationCompletionRequestItem: AuthorisationCompletionRequestItem
    ): Observable<AuthorisationCompletionResponseEntity> {
        return authorisationCompletionDataSource.doAuthorisationCompletion(
            configurationItem,
            authorisationCompletionRequestItem
        )
    }
}