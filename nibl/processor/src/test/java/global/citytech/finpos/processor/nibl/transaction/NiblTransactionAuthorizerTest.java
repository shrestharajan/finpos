package global.citytech.finpos.processor.nibl.transaction;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import global.citytech.finpos.processor.nibl.transaction.authorizer.NiblTransactionAuthorizer;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.data.TransactionAuthorizationResponse;

import static org.junit.Assert.assertTrue;

/**
 * Created by Unique Shakya on 10/9/2020.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({TransactionAuthenticator.class, ApplicationRepository.class, global.citytech.finpos.processor.nibl.transaction.authorizer.NiblTransactionAuthorizer.class,
        TransactionSetTableVerifier.class})
public class NiblTransactionAuthorizerTest {

    TransactionAuthenticator transactionAuthenticator;
    ApplicationRepository applicationRepository;
    TransactionSetTableVerifier transactionSetTableVerifier;

    @Before
    public void setup() {
        transactionAuthenticator = PowerMockito.mock(TransactionAuthenticator.class);
        applicationRepository = PowerMockito.mock(ApplicationRepository.class);
        transactionSetTableVerifier = PowerMockito.mock(TransactionSetTableVerifier.class);
    }

    @Test
    public void authorizeUserTestWhenSuperVisorPasswordRequired() throws Exception {
        PowerMockito.whenNew(TransactionSetTableVerifier.class).withAnyArguments()
                .thenReturn(this.transactionSetTableVerifier);
        PowerMockito.when(this.transactionSetTableVerifier.isSupervisorPasswordRequired())
                .thenReturn(true);
        PowerMockito.when(this.transactionAuthenticator.authenticate())
                .thenReturn(new TransactionAuthorizationResponse(true));

        global.citytech.finpos.processor.nibl.transaction.authorizer.NiblTransactionAuthorizer niblTransactionAuthorizer = new global.citytech.finpos.processor.nibl.transaction.authorizer.NiblTransactionAuthorizer(
                this.transactionAuthenticator, this.applicationRepository
        );
        TransactionAuthorizationResponse response = niblTransactionAuthorizer.authorizeUser(TransactionType.PURCHASE,
                "VC");

        assertTrue(response.isSuccess());
    }

    @Test
    public void authorizeUserTestWhenSuperVisorPasswordNotRequired() throws Exception {
        PowerMockito.whenNew(TransactionSetTableVerifier.class).withAnyArguments()
                .thenReturn(this.transactionSetTableVerifier);
        PowerMockito.when(this.transactionSetTableVerifier.isSupervisorPasswordRequired())
                .thenReturn(true);
        PowerMockito.when(this.transactionAuthenticator.authenticate())
                .thenReturn(new TransactionAuthorizationResponse(true));

        global.citytech.finpos.processor.nibl.transaction.authorizer.NiblTransactionAuthorizer niblTransactionAuthorizer = new NiblTransactionAuthorizer(
                this.transactionAuthenticator, this.applicationRepository
        );
        TransactionAuthorizationResponse response = niblTransactionAuthorizer.authorizeUser(TransactionType.PURCHASE,
                "VC");

        assertTrue(response.isSuccess());
    }
}
