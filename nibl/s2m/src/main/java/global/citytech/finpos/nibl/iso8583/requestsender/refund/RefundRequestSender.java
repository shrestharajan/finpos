package global.citytech.finpos.nibl.iso8583.requestsender.refund;

import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.iso8583.NIBLMessagePayloadBuilder;
import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Unique Shakya on 9/28/2020.
 */
public abstract class RefundRequestSender extends NIBLMessageSenderTemplate {

    private Logger logger = Logger.getLogger(RefundRequestSender.class.getName());

    public RefundRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Financial,
                        MTI.MessageFunction.Request,
                        MTI.Originator.Acquirer);
        TransactionIsoRequest refundRequest = (TransactionIsoRequest) this.context.getRequest();
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);

        msg.addField(
                new DataElement(
                        DataElement.AMOUNT,
                        HelperUtils.toISOAmount(refundRequest.getTransactionAmount(), 12)
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE,
                        refundRequest.getPosConditionCode()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.RETRIEVAL_REFERENCE_NUMBER,
                        refundRequest.getOriginalRetrievalReferenceNumber()
                )
        );
        if (!StringUtils.isEmpty(refundRequest.getPinBlock()))
            msg.addField(
                    new DataElement(
                            DataElement.PIN_BLOCK,
                            refundRequest.getPinBlock()
                    )
            );
        msg.addField(
                new DataElement(
                        DataElement.PRIVATE_USE_FIELD_62,
                        refundRequest.getInvoiceNumber()
                )
        );
        addConditionalFields(msg, refundRequest);
        MessagePayload.Builder builder = new NIBLMessagePayloadBuilder(this.specInfo, msg);
        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());
        return builder.build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.REFUND.getCode();
    }

    @Override
    protected String getFunctionCode() {
        return "0011";
    }

    public abstract void addConditionalFields(
            Iso8583Msg msg,
            TransactionIsoRequest refundRequest
    );
}
