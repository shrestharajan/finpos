package global.citytech.finpos.processor.neps.transaction;

import global.citytech.finpos.processor.neps.NepsConstant;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardSummary;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.PinBlock;
import global.citytech.finposframework.hardware.io.cards.cvm.Cvm;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.keymgmt.pin.PinBlockFormat;
import global.citytech.finposframework.hardware.keymgmt.pin.PinRequest;
import global.citytech.finposframework.hardware.keymgmt.pin.PinResponse;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Saurav Ghimire on 2/2/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public abstract class ManualTransactionUseCase extends TransactionExecutorTemplate {

    protected ManualTransactionUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    protected void retrieveCardResponse() {
        readCardResponse
                = this.prepareManualReadCardResponse(transactionRequest);
        readCardResponse.getCardDetails().setAmount(transactionRequest.getAmount());
        readCardResponse.getCardDetails().setCashBackAmount(transactionRequest.getAdditionalAmount());
        this.identifyAndSetCardScheme(
                readCardResponse,
                request.getTransactionRepository(),
                request.getApplicationRepository()
        );
    }

    @Override
    protected void processCard() {
        PurchaseRequest purchaseRequest = preparePurchaseRequest(
                transactionRequest,
                readCardResponse.getCardDetails()
        );
        readCardResponse = this.retrievePinBlockIfRequired(purchaseRequest);
    }

    @Override
    protected void processOnlineResult(IsoMessageResponse isoMessageResponse) {
        // No need in case of Mag
    }

    @Override
    protected void updateTransactionLog(boolean transactionApproved, global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest, TransactionRepository transactionRepository, IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse) {
        System.out.println("::: NEPS ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
        System.out.println("::: NEPS ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
        String timeStamp = IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.LOCAL_TIME);
        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
                .withStan(stan)
                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
                .withInvoiceNumber(invoiceNumber)
                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
                .withTransactionType(getTransactionType())
                .withOriginalTransactionType(getOriginalTransactionType())
                .withTransactionAmount(transactionRequest.getAmount())
                .withOriginalTransactionAmount(transactionRequest.getAmount())
                .withTransactionDate(TransactionUtils.getDateOrDefaultFromTimeStamp(timeStamp))
                .withTransactionTime(TransactionUtils.getTimeOrDefaultFromTimeStamp(timeStamp))
                .withTransactionStatus(transactionApproved ? "APPROVED" : "DECLINED")
                .withPosEntryMode(PosEntryMode.MANUAL)
                .withOriginalPosEntryMode(PosEntryMode.MANUAL)
                .withPosConditionCode("05")
                .withOriginalPosConditionCode("05")
                .withReconcileStatus("") //TODO
                .withReconcileTime("") //TODO
                .withReconcileDate("") //TODO
                .withReconcileBatchNo(batchNumber)
                .withReadCardResponse(readCardResponse)
                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse,
                        applicationRepository, batchNumber, stan, invoiceNumber))
                .withAuthorizationCompleted(true)
                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                .withTransactionVoided(false)
                .withProcessingCode(getProcessingCode())
                .withOriginalTransactionReferenceNumber(transactionRequest.getOriginalRetrievalReferenceNumber())
                .withReadCardResponse(readCardResponse)
                .withCurrencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .withVatInfo(transactionRequest.getVatInfo())
                .build();
        transactionRepository.updateTransactionLog(transactionLog);
    }

    protected abstract TransactionType getOriginalTransactionType();

    protected abstract TransactionType getTransactionType();

    @Override
    protected void updateTransactionIdsAndNotifyTransactionStatus(boolean transactionApproved, IsoMessageResponse isoMessageResponse, TransactionRequest request, ReadCardResponse readCardResponse) {
        this.updateTransactionIds(transactionApproved);
        if (transactionApproved) {
            this.notifier.notify(
                    Notifier.EventType.TRANSACTION_APPROVED,
                    this.approvedMessage(isoMessageResponse),
                    Notifier.TransactionType.CARD.name()
            );
        } else
            this.notifier.notify(
                    Notifier.EventType.TRANSACTION_DECLINED,
                    this.declinedMessage(
                            isoMessageResponse,
                            request.getApplicationRepository()
                    ),
                    Notifier.TransactionType.CARD.name()
            );

    }

    protected ReadCardResponse prepareManualReadCardResponse(global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest) {
        CardDetails cardDetails = new CardDetails();
        cardDetails.setCardType(CardType.MANUAL);
        cardDetails.setPrimaryAccountNumber(transactionRequest.getCardNumber());
        cardDetails.setExpiryDate(transactionRequest.getExpiryDate());
        cardDetails.setAmount(transactionRequest.getAmount());
        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
        cardDetails.setCvv(transactionRequest.getCvv());
        return new ReadCardResponse(
                cardDetails,
                Result.SUCCESS,
                "Manual Read Card Response Success"
        );
    }

    protected void identifyAndSetCardScheme(ReadCardResponse readCardResponse, TransactionRepository transactionRepository, ApplicationRepository applicationRepository) {
        CardIdentifier cardIdentifier = new CardIdentifier(
                transactionRepository,
                applicationRepository
        );
        cardIdentifier.identify(readCardResponse);
    }

    protected ReadCardResponse retrievePinBlockIfRequired(PurchaseRequest purchaseRequest) {
        PinBlock pinBlock = new PinBlock("", true);
        Cvm cvm = this.retrieveCvm(
                purchaseRequest,
                request.getApplicationRepository()
        );
        boolean pinRequired = cvm.equals(Cvm.ENCIPHERED_PIN_ONLINE);
        String pinPadAmountMessage = TransactionUtils.retrievePinPadMessageWithAmount(purchaseRequest.getAmount());
        if (pinRequired) {
            pinBlock = this.retrievePinBlock(
                    purchaseRequest
                            .getCardDetails()
                            .getPrimaryAccountNumber(),
                    pinPadAmountMessage
            );
        }
        ReadCardResponse readCardResponse = new ReadCardResponse(
                purchaseRequest.getCardDetails(),
                Result.SUCCESS,
                "Successful"
        );
        readCardResponse.setCvm(cvm);
        readCardResponse.getCardDetails().setPinBlock(pinBlock);
        readCardResponse
                .getCardDetails()
                .setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
        readCardResponse.setFallbackIccToMag(purchaseRequest.isFallbackIccToMag());
        return readCardResponse;
    }

    private PinBlock retrievePinBlock(String primaryAccountNumber, String pinPadAmountMessage) {
        if (!StringUtils.isEmpty(primaryAccountNumber)) {
            CardSummary cardSummary = new CardSummary(readCardResponse.getCardDetails().getCardSchemeLabel(),
                    primaryAccountNumber,
                    readCardResponse.getCardDetails().getCardHolderName(),
                    readCardResponse.getCardDetails().getExpiryDate());
            PinRequest pinRequest = new PinRequest(
                    primaryAccountNumber,
                    NepsConstant.PIN_PAD_MESSAGE,
                    PinBlockFormat.ISO9564_FORMAT_0,
                    4,
                    12,
                    30,
                    pinPadAmountMessage,
                    !this.terminalRepository.shouldShufflePinPad(),
                    false,
                    "Enter PIN"
            );
            pinRequest.setCardSummary(cardSummary);
            PinResponse response = request.getTransactionAuthenticator().authenticateUser(pinRequest);
            this.logger.debug("::: PIN RESPONSE ::: " + response.toString());
            if (response.getResult() == Result.SUCCESS)
                return response.getPinBlock();
            else if (response.getResult() == Result.USER_CANCELLED)
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            else if (response.getResult() == Result.TIMEOUT)
                throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
            else
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    private Cvm retrieveCvm(PurchaseRequest purchaseRequest, ApplicationRepository applicationRepository) {
        if (purchaseRequest.getCardDetails() == null ||
                purchaseRequest.getCardDetails().getCardScheme() == null) {
            return Cvm.NO_CVM;
        }
        String shortCardScheme = purchaseRequest.getCardDetails().getCardScheme()
                .getCardSchemeId();
        TransactionSetTableVerifier verifier = new TransactionSetTableVerifier(
                applicationRepository,
                shortCardScheme,
                TransactionUtils
                        .mapPurchaseTypeWithTransactionType(purchaseRequest.getTransactionType()));
        String cvmToPerform = verifier.getCardHolderVerificationMethod();
        switch (cvmToPerform) {
            case "1":
                return Cvm.SIGNATURE;
            case "2":
            case "3":
                return Cvm.ENCIPHERED_PIN_ONLINE;
            default:
                return Cvm.NO_CVM;
        }
    }

    @Override
    protected TransactionResponse promptToPerformIccTransaction() {
        return null;
    }

}
