package global.citytech.finposframework.utility;

/**
 * Created by Rishav Chudal on 5/13/21.
 */
public class TMSConfigurationConstants {
    /**
     * EMV Parameters : Additional Data Constants
     */
    public static final String APPROVED_PRINT_ONLY = "approvePrintOnly";
    public static final String KEY_TRANSACTION_CURRENCY_NAME = "transactionCurrencyName";
    public static final String ADMIN_CREDENTIALS = "adminPassword";
    public static final String MANUAL_TRANSACTION = "manualTransaction";
    public static final String TRANSACTION_MENU = "transactionMenu";

    /**
     * Menu Transactions Offset
     */
    public static final int SALES_TRANSACTION_OFFSET = 0;
    public static final int VOID_TRANSACTION_OFFSET = 1;
    public static final int PRE_AUTH_TRANSACTION_OFFSET = 2;
    public static final int AUTH_COMPLETION_TRANSACTION_OFFSET = 3;
    public static final int REFUND_TRANSACTION_OFFSET = 4;
    public static final int CASH_ADVANCE_TRANSACTION_OFFSET = 5;
    public static final int BALANCE_INQUIRY_TRANSACTION_OFFSET = 6;
    public static final int MINI_STATEMENT_TRANSACTION_OFFSET = 7;
    public static final int TIP_ADJUSTMENT_TRANSACTION_OFFSET = 8;
    public static final int CASH_VOID_TRANSACTION_OFFSET = 9;
    public static final int CASH_DEPOSIT_TRANSACTION_OFFSET = 10;

    /**
     * Default Transaction Configurations
     */
    public static final String DEFAULT_RETAILER_MODE_TRANSACTIONS_CONFIGURATION = "1111100000000000";
    public static final String DEFAULT_CASH_MODE_TRANSACTIONS_CONFIGURATION = "0000011101100000";

    /**
     *  Other Default Configurations
     */
    public static final int MAX_TIP_LIMIT_CONFIGURATION_IN_PERCENTAGE = 100;
    public static final int DEFAULT_STEP_SIZE_FOR_TIP_LIMIT_CONFIGURATION = 5;
    public static final int DEFAULT_TIP_LIMIT_CONFIGURATION_IN_PERCENTAGE = 10;

    public static final int MAX_AMOUNT_LENGTH = 12;
    public static final int MIN_AMOUNT_LENGTH = 6;
    public static final int AMOUNT_LENGTH_STEP_SIZE = 1;
}
