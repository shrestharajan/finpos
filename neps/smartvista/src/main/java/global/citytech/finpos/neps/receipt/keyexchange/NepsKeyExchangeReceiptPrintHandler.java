package global.citytech.finpos.neps.receipt.keyexchange;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.hardware.io.printer.PrintMessage;
import global.citytech.finposframework.hardware.io.printer.Printable;
import global.citytech.finposframework.hardware.io.printer.PrinterRequest;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.receipt.keyexchange.KeyExchangeReceipt;

/**
 * Created by Unique Shakya on 1/19/2021.
 */
public class NepsKeyExchangeReceiptPrintHandler implements ReceiptHandler.KeyExchangeReceiptHandler {

    private PrinterService printerService;

    public NepsKeyExchangeReceiptPrintHandler(PrinterService printerService) {
        this.printerService = printerService;
    }


    @Override
    public void printKeyExchangeReceipt(KeyExchangeReceipt keyExchangeReceipt) {
        PrinterRequest printerRequest = this.preparePrinterRequest(keyExchangeReceipt);
        printerService.print(printerRequest);
    }

    private PrinterRequest preparePrinterRequest(KeyExchangeReceipt keyExchangeReceipt) {
        List<Printable> printableList = new ArrayList<>();
        printableList.add(PrintMessage.Companion.getInstance(keyExchangeReceipt.getMessage(), NepsKeyExchangeReceiptStyle.KEY_EXCHANGE_MESSAGE.getStyle()));
        return new PrinterRequest(printableList);
    }
}
