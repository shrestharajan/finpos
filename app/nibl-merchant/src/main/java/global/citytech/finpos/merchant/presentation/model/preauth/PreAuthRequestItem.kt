package global.citytech.finpos.merchant.presentation.model.preauth

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

/**
 * Created by Rishav Chudal on 8/31/20.
 */
data class PreAuthRequestItem(
    val amount: BigDecimal? = null,
    val transactionType: TransactionType? = null
)