package global.citytech.finposframework.switches.refund;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 9/28/2020.
 */
public interface RefundRequester<T extends RefundRequestParameter, E extends RefundResponseParameter>
        extends Request<T, E> {
}
