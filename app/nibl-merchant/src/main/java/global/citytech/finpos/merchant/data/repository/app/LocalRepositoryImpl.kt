package global.citytech.finpos.merchant.data.repository.app

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.data.datasource.app.db.LocalDatabaseSource
import global.citytech.finpos.merchant.data.datasource.app.pref.SharedPreferenceSource
import global.citytech.finpos.merchant.domain.model.app.*
import global.citytech.finpos.merchant.domain.model.device.DeviceResponse
import global.citytech.finpos.merchant.domain.repository.app.LocalRepository
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.presentation.model.ConfigResponse
import global.citytech.finpos.merchant.presentation.model.banner.Banner
import global.citytech.finpos.merchant.presentation.model.setting.TerminalSettingResponse
import global.citytech.finpos.merchant.presentation.purchase.PaymentItems
import global.citytech.finposframework.switches.posmode.PosMode
import global.citytech.finposframework.usecases.TransactionType
import io.reactivex.Observable


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
class LocalRepositoryImpl(
    private val localDatabaseSource: LocalDatabaseSource,
    private val sharedPreferenceSource: SharedPreferenceSource
) : LocalRepository {
    override fun getHost(): Observable<List<Host>> = localDatabaseSource.getHost()
    override fun getMerchant(): Observable<List<Merchant>> = localDatabaseSource.getMerchant()
    override fun getLogo(): Observable<List<Logo>> = localDatabaseSource.getLogo()
    override fun getAidParams(): Observable<List<AidParam>> = localDatabaseSource.getAidParams()
    override fun getCardScheme(): Observable<List<CardScheme>> =
        localDatabaseSource.getCardScheme()

    override fun getEmvKeys(): Observable<List<EmvKey>> = localDatabaseSource.getEmvKeys()
    override fun getEmvParams(): Observable<List<EmvParam>> = localDatabaseSource.getEmvParams()
    override fun getMerchantTransactionLog() = localDatabaseSource.getMerchantTransactionLog()
    override fun getActivityLog(): Observable<List<ActivityLog>> =
        localDatabaseSource.getActivityLog()

    override fun getTransactionLogByStan(stan: String) =
        localDatabaseSource.getTransactionLogStan(stan)

    override fun getTransactionLogByInvoice(invoiceNumber: String, ignoreBatchNumber: Boolean) =
        localDatabaseSource.getTransactionLogByInvoice(invoiceNumber, ignoreBatchNumber)

    override fun getTransactionLogByRRN(rrn: String) =
        localDatabaseSource.getTransactionLogByRRN(rrn)

    override fun getTransactionLogs() = localDatabaseSource.getTransactionLogs()
    override fun getTransactionLogs(limit: Int, offset: Int): Observable<List<TransactionLog>> =
        localDatabaseSource.getTransactionLogs(limit, offset)

    override fun countTransactionLogs(): Int = localDatabaseSource.countApprovedTransactions()
    override fun countSearchTransactionLogs(searchParam: String): Int =
        localDatabaseSource.countSearchApprovedTransactions(searchParam)

    override fun searchTransactionLogs(
        searchParam: String,
        limit: Int,
        offset: Int
    ): Observable<List<TransactionLog>> =
        localDatabaseSource.searchTransactionLogs(searchParam, limit, offset)

    override fun saveMerchantTransactionLog(merchantTransactionLog: MerchantTransactionLog) =
        localDatabaseSource.saveMerchantTransactionLog(merchantTransactionLog)

    override fun nukeActivityLog(): Observable<DatabaseReponse> =
        localDatabaseSource.nukeActivityLog()

    override fun nukeMerchantTransactionLog() = localDatabaseSource.nukeMerchantTransactionLog()
    override fun deleteMerchantTransactionLogByStans(stans: List<String>) =
        localDatabaseSource.deleteMerchantTransactionLogByStans(stans)

    override fun saveConfigResponseAndInjectKey(
        configResponse: ConfigResponse
    ): Observable<DeviceResponse> =
        localDatabaseSource.saveConfigResponseAndInjectKey(configResponse)

    override fun saveSettlementActivePref(value: Boolean) {
        sharedPreferenceSource.saveSettlementActivePref(value)
    }

    override fun getSettlementActivePref(defaultValue: Boolean): Boolean =
        sharedPreferenceSource.getSettlementActivePref(defaultValue)

    override fun saveTerminalConfigPref(value: Boolean) {
        sharedPreferenceSource.saveTerminalConfigPref(value)
    }

    override fun getTerminalConfigPref(defaultValue: Boolean): Boolean =
        sharedPreferenceSource.getTerminalConfigPref(defaultValue)

    override fun saveDebugModePreference(isDebugModeOn: Boolean) {
        sharedPreferenceSource.saveDebugModePref(isDebugModeOn)
    }

    override fun saveSoundModePreference(isSoundModeOn: Boolean) {
        sharedPreferenceSource.saveSoundModePref(isSoundModeOn)
    }

    override fun getDebugModePreference(defaultValue: Boolean): Boolean = sharedPreferenceSource.getDebugModePref(defaultValue)
    override fun getSoundModePreference(defaultValue: Boolean): Boolean = sharedPreferenceSource.getSoundModePref(defaultValue)

    override fun clearPreferences() {
        sharedPreferenceSource.clearAllPreferences()
    }

    override fun isTransactionPresent(invoiceNumber: String): Observable<Boolean> {
        return localDatabaseSource.isTransactionPresent(invoiceNumber)
    }

    override fun isTransactionPresentByAuthCode(authCode: String): Observable<Boolean> {
        return localDatabaseSource.isTransactionPresentByAuthCode(authCode)
    }

    override fun isTransactionPresentByRrn(rrn: String): Observable<Boolean> {
        return localDatabaseSource.isTransactionPresentByRrn(rrn)
    }

    override fun setCardMonitoringPreference(cardMonitoring: Boolean) {
        sharedPreferenceSource.setCardMonitoringPreference(cardMonitoring)
    }

    override fun getCardMonitoringPreference(defaultValue: Boolean): Boolean {
        return sharedPreferenceSource.getCardMonitoringPreference(defaultValue)
    }

    override fun setForceSettlementPreference(forceSettlement: Boolean) {
        sharedPreferenceSource.setForceSettlementPreference(forceSettlement)
    }

    override fun getForceSettlementPreference(defaultValue: Boolean): Boolean {
        return sharedPreferenceSource.getForceSettlementPreference(defaultValue)
    }

    override fun savePosMode(posMode: PosMode) {
        sharedPreferenceSource.savePosMode(posMode.name)
    }

    override fun getPosMode(defaultPosMode: PosMode): PosMode {
        val posModeString = sharedPreferenceSource.getPosMode(defaultPosMode.name)
        return PosMode.valueOf(posModeString)
    }

    override fun saveTipLimit(value: Int) {
        sharedPreferenceSource.saveTipLimit(value)
    }

    override fun saveNumpadLayoutType(numpadLayoutType: String){
        sharedPreferenceSource.saveNumpadLayoutType(numpadLayoutType)
    }

    override fun getNumpadLayoutType(defaultValue: String): String {
        return sharedPreferenceSource.getNumpadLayoutType(defaultValue)
    }

    override fun retrieveTipLimit(): Int {
        return sharedPreferenceSource.retrieveTipLimit()
    }

    override fun getAdminPassword(): Observable<String> {
        return localDatabaseSource.getAdminPassword()
    }

    override fun getEnabledTransactionsForThePosMode(posMode: PosMode): Observable<List<TransactionType>> =
        localDatabaseSource.getEnabledTransactionsForThePosMode(posMode)

    override fun getSettlementTime(defaultValue: String): String =
        sharedPreferenceSource.getSettlementTime(defaultValue)

    override fun storeSettlementTime(settlementTime: String) {
        sharedPreferenceSource.storeSettlementTime(settlementTime)
    }

    override fun getLastSuccessSettlementTimeInMillis(defaultValue: Long): Long {
        return sharedPreferenceSource.getLastSuccessSettlementTimeInMillis(defaultValue)
    }

    override fun updateLastSuccessSettlementTimeInMillis(settledTime: Long) {
        sharedPreferenceSource.updateLastSuccessSettlementTimeInMillis(settledTime)
    }

    override fun updateReconciliationBatchNumber(batchNumber: Int) =
        localDatabaseSource.updateReconciliationBatchNumber(batchNumber)


    override fun getExistingSettlementBatchNumber(): Observable<String> {
        return localDatabaseSource.getExistingSettlementBatchNumber()
    }

    override fun getTipAdjustedTransactionLogOfInvoiceNumber(invoiceNumber: String): Observable<TransactionLog> {
        return localDatabaseSource.getTipAdjustedTransactionLogOfInvoiceNumber(invoiceNumber)
    }

    override fun getDeviceConfiguration(): DeviceConfiguration {
        return localDatabaseSource.getDeviceConfiguration()
    }

    override fun saveCardConfirmationRequiredPref(value: Boolean) {
        sharedPreferenceSource.saveCardConfirmationRequiredPref(value)
    }

    override fun getCardConfirmationRequiredPref(defaultValue: Boolean): Boolean =
        sharedPreferenceSource.getCardConfirmationRequiredPref(defaultValue)

    override fun saveVatRefundSupportedPref(value: Boolean) {
        sharedPreferenceSource.saveVatRefundSupportedPref(value)
    }

    override fun getVatRefundSupportedPref(defaultValue: Boolean): Boolean =
        sharedPreferenceSource.getVatRefundSupportedPref(defaultValue)

    override fun saveShouldShufflePinPadPref(value: Boolean) {
        sharedPreferenceSource.saveShouldShufflePinPadPref(value)
    }

    override fun getShouldShufflePinPadPref(defaultValue: Boolean): Boolean =
        sharedPreferenceSource.getShouldShufflePinPadPref(defaultValue)

    override fun saveMerchantCredential(value: String) {
        sharedPreferenceSource.saveMerchantCredential(value)
    }

    override fun getMerchantCredential(): String {
        return sharedPreferenceSource.getMerchantCredential(BuildConfig.MERCHANT_DEFAULT_SANCHO)
    }

    override fun getBanners(): List<Banner> {
        val data = sharedPreferenceSource.getBanners("[]")
        return Jsons.fromJsonToList(data, Array<Banner>::class.java)
    }

    override fun setBanners(banners: List<Banner>) {
        val data = Jsons.toJsonObj(banners)
        sharedPreferenceSource.saveBanners(data)
    }

    override fun deleteTransactionByStan(stan: String): Observable<Boolean> {
        return localDatabaseSource.deleteTransactionByStan(stan)
    }

    override fun saveTerminalSettings(terminalSettingResponse: TerminalSettingResponse) {
        val data = Jsons.toJsonObj(terminalSettingResponse)
        sharedPreferenceSource.saveTerminalSetting(data)
    }

    override fun savePaymentInitiatorItems(paymentItems: MutableList<PaymentItems>) {
        val data = Jsons.toJsonObj(paymentItems)
        sharedPreferenceSource.savePaymentInitiatiorItems(data)
    }

    override fun updateShowQrDisclaimerSetting(showDisclaimer: Boolean) {
        sharedPreferenceSource.updateShowQrDisclaimerSetting(showDisclaimer)
    }

    override fun getShowQrDisclaimerSetting(): Boolean =
        sharedPreferenceSource.getShowQrDisclaimerSetting()

    override fun getTerminalSettings(): TerminalSettingResponse {
        val data = sharedPreferenceSource.getTerminalSetting()
        return Jsons.fromJsonToObj(data, TerminalSettingResponse::class.java)
    }

    override fun getPaymentInitiatorItems(): ArrayList<PaymentItems> {
        val data = sharedPreferenceSource.getPaymentInitiatorItems()
        val list: ArrayList<PaymentItems> =  Gson().fromJson(
            data,
            object : TypeToken<List<PaymentItems>>() {}.type)
        return list
    }

    override fun isDisclaimerShown(): Boolean {
        return sharedPreferenceSource.isDisclaimerShown()
    }

    override fun saveDisclaimerShownStatusPref(value: Boolean) {
        sharedPreferenceSource.saveDisclaimerShownStatusPref(value)
    }

    override fun insertToActivityLog(activityLog: ActivityLog): Observable<Boolean> =
        localDatabaseSource.insertToActivityLog(activityLog)

    override fun addToDisclaimers(disclaimer: Disclaimer): Observable<Boolean> =
        localDatabaseSource.addToDisclaimers(disclaimer)

    override fun addListToDisclaimers(disclaimers: List<Disclaimer>): Observable<Boolean> =
        localDatabaseSource.addListToDisclaimers(disclaimers)

    override fun retrieveAllDisclaimers(): Observable<List<Disclaimer>> =
        localDatabaseSource.retrieveAllDisclaimers()

    override fun retrieveDisclaimerById(disclaimerId: String): Observable<Disclaimer> =
        localDatabaseSource.retrieveDisclaimerById(disclaimerId)

    override fun updateRemarksById(disclaimerId: String, remarks: String): Observable<Boolean> =
        localDatabaseSource.updateRemarksById(disclaimerId, remarks)

    override fun deleteById(disclaimerId: String): Observable<Boolean> =
        localDatabaseSource.deleteById(disclaimerId)

    override fun getInvoiceNumberByRrn(rrn: String): Observable<String> =
        localDatabaseSource.getInvoiceNumberByRrn(rrn)

    override fun deleteMerchantTransactionLogsOfGivenCountFromInitial(count: Int): Observable<DatabaseReponse> =
        localDatabaseSource.deleteMerchantTransactionLogsOfGivenCountFromInitial(count)

    override fun saveAppDownloadExtra(value: String) {
        sharedPreferenceSource.saveAppDownloadExtra(value)
    }

    override fun getAppDownloadExtra(): String =
        sharedPreferenceSource.getAppDownloadExtra()

    override fun getStoredAppVersion() = sharedPreferenceSource.getStoredAppVersion()

    override fun saveAppVersion(version: Int) = sharedPreferenceSource.saveAppVersion(version)

    override fun isSwitchConfigActive(): Boolean = sharedPreferenceSource.isSwitchConfigActive()

    override fun setSwitchConfigActive(active: Boolean) =
        sharedPreferenceSource.setSwitchConfigActive(active)

    override fun isBroadcastAcknowledgeActive(): Boolean =
        sharedPreferenceSource.isBroadcastAcknowledgeActive()

    override fun setBroadcastAcknowledgeActive(active: Boolean) =
        sharedPreferenceSource.setBroadcastAcknowledgeActive(active)

    override fun isAppResetActive(active: Boolean): Boolean =
        sharedPreferenceSource.isAppResetActive(active)


    override fun setAppResetActive(active: Boolean) =
        sharedPreferenceSource.setAppResetActive(active)

    override fun nukeAllDatabaseTable(): Observable<DatabaseReponse> =
        localDatabaseSource.nukeAllDatabaseTable()

    override fun getAmountPerTransaction() = localDatabaseSource.getAmountPerTransaction()

    override fun getAmountLengthLimit(): Int = sharedPreferenceSource.getAmountLengthLimit()

    override fun saveAmountLengthLimit(value: Int) = sharedPreferenceSource.setAmountLengthLimit(value)

    override fun getMerchantTransactionLogsOfGivenCount(count: Int): Observable<List<MerchantTransactionLog>> =
        localDatabaseSource.getMerchantTransactionLogsOfGivenCount(count)

    override fun getFonePayLoginState(defaultValue: Boolean): Boolean = sharedPreferenceSource.getFonePayLoginState(defaultValue)

    override fun saveFonePayLoginState(value: Boolean) {
        sharedPreferenceSource.saveFonePayLoginState(value)
    }

    override fun saveGreenPinStatus(value: Boolean) = sharedPreferenceSource.saveGreenPinStatus(value);
    override fun getGreenPinStatus(): Boolean = sharedPreferenceSource.getGreenPinStatus()

}
