package global.citytech.finpos.processor.neps.posmode;

import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.posmode.PosMode;
import global.citytech.finposframework.switches.posmode.PosModeRequester;

/**
 * Created by Unique Shakya on 1/20/2021.
 */
public class PosModeUseCase implements PosModeRequester<PosModeRequestModel, PosModeResponseModel>,
        UseCase<PosModeRequestModel, PosModeResponseModel> {

    private static final String CASH_MODE_MCC_NIBL = "6010";

    @Override
    public PosModeResponseModel execute(PosModeRequestModel posModeRequestModel) {
        PosMode posMode;
        if (posModeRequestModel.getMerchantCategoryCode().equals(CASH_MODE_MCC_NIBL))
            posMode = PosMode.CASH;
        else
            posMode = PosMode.RETAILER;
        return new PosModeResponseModel(posMode);
    }
}
