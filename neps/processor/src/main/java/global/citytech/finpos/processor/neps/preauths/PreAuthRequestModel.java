package global.citytech.finpos.processor.neps.preauths;

import global.citytech.finpos.processor.neps.transaction.TransactionRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.io.sound.SoundService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;

/**
 * Created by Saurav Ghimire on 2/3/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class PreAuthRequestModel extends TransactionRequest {
    
    public static  class Builder {
        private TransactionRepository transactionRepository;
        private ReadCardService readCardService;
        private DeviceController deviceController;
        private TransactionAuthenticator transactionAuthenticator;
        private PrinterService printerService;
        private ApplicationRepository applicationRepository;
        private global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest;
        private LedService ledService;
        private SoundService soundService;


        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public Builder withReadCardService(ReadCardService readCardService) {
            this.readCardService = readCardService;
            return this;
        }

        public Builder withDeviceController(DeviceController deviceController) {
            this.deviceController = deviceController;
            return this;
        }

        public Builder withTransactionAuthenticator(TransactionAuthenticator transactionAuthenticator) {
            this.transactionAuthenticator = transactionAuthenticator;
            return this;
        }

        public Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public Builder withTransactionRequest(global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest) {
            this.transactionRequest = transactionRequest;
            return this;
        }

        public Builder withLedService(LedService ledService) {
            this.ledService = ledService;
            return this;
        }

        public Builder withSoundService(SoundService soundService) {
            this.soundService = soundService;
            return this;
        }

        public PreAuthRequestModel build() {
            PreAuthRequestModel preAuthRequestModel = new PreAuthRequestModel();
            preAuthRequestModel.applicationRepository = this.applicationRepository;
            preAuthRequestModel.transactionRequest = this.transactionRequest;
            preAuthRequestModel.transactionRepository = this.transactionRepository;
            preAuthRequestModel.readCardService = this.readCardService;
            preAuthRequestModel.transactionAuthenticator = this.transactionAuthenticator;
            preAuthRequestModel.deviceController = this.deviceController;
            preAuthRequestModel.printerService = this.printerService;
            preAuthRequestModel.ledService = this.ledService;
            preAuthRequestModel.soundService = this.soundService;
            return preAuthRequestModel;
        }
    }

}
