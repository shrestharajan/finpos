package global.citytech.finpos.merchant.presentation.idle

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.AppNotificationRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.idle.IdlePurchaseRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.receipt.CustomerCopyRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.reconciliation.ReconciliationRepositoryImpl
import global.citytech.finpos.merchant.data.repository.device.DeviceRepositoryImpl
import global.citytech.finpos.merchant.domain.model.device.DeviceResponse
import global.citytech.finpos.merchant.domain.model.purchase.PurchaseResponseEntity
import global.citytech.finpos.merchant.domain.usecase.app.AppNotificationUseCase
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.core.idle.CoreIdlePurchaseUseCase
import global.citytech.finpos.merchant.domain.usecase.core.receipt.CoreCustomerCopyUseCase
import global.citytech.finpos.merchant.domain.usecase.core.reconciliation.CoreReconciliationUseCase
import global.citytech.finpos.merchant.domain.usecase.device.DeviceUseCase
import global.citytech.finpos.merchant.framework.datasource.app.AppNotificationDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.idle.IdlePurchaseDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.receipt.CustomerCopyDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.reconciliation.ReconciliationDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.device.DeviceConfigurationSourceImpl
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.model.CardReadError
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.presentation.model.purchase.PurchaseRequestItem
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.exceptions.PosException
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.switches.posmode.PosMode
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.utility.PosResponse
import global.citytech.finposframework.utility.PosResult
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.util.*

class IdleViewModel(context: Application) : BaseTransactionViewModel(context) {

    private val logger = Logger(IdleViewModel::class.java.name)
    val configuration by lazy { MutableLiveData<ConfigurationItem>() }
    val showActionMenu by lazy { MutableLiveData<Boolean>() }
    val loadCardCheckActivity by lazy { MutableLiveData<Boolean>() }
    val hideTerminalId by lazy { MutableLiveData<Boolean>() }
    val tvReadyMessage by lazy { MutableLiveData<String?>() }
    val cardReadError by lazy { MutableLiveData<CardReadError>() }
    val loadAmountPage by lazy { MutableLiveData<Boolean>() }
    val startDashboard by lazy { MutableLiveData<PosMode>() }
    val tvPosStatusMessage by lazy { MutableLiveData<String>() }
    val startSettlement by lazy { MutableLiveData<Boolean>() }
    val noNotifications by lazy { MutableLiveData<Boolean>() }

    var cardAlreadyRead: Boolean = false

    var localDataUseCase: LocalDataUseCase =
        LocalDataUseCase(
            LocalRepositoryImpl(
                LocalDatabaseSourceImpl(),
                PreferenceManager
            )
        )

    var deviceUseCase = DeviceUseCase(
        DeviceRepositoryImpl(
            DeviceConfigurationSourceImpl(context)
        )
    )

    var idlePurchaseUseCase = CoreIdlePurchaseUseCase(
        IdlePurchaseRepositoryImpl(
            IdlePurchaseDataSourceImpl()
        )
    )

    var reconciliationUseCase = CoreReconciliationUseCase(
        ReconciliationRepositoryImpl(
            ReconciliationDataSourceImpl()
        )
    )

    private var customerCopyUseCase: CoreCustomerCopyUseCase =
        CoreCustomerCopyUseCase(
            CustomerCopyRepositoryImpl(
                CustomerCopyDataSourceImpl()
            )
        )

    private val appNotificationUseCase = AppNotificationUseCase(
        AppNotificationRepositoryImpl(
            AppNotificationDataSourceImpl(context.applicationContext)
        )
    )

    fun checkForNotifications() {
        noNotifications.value = appNotificationUseCase.noNotifications()
    }

    private fun retrieveTerminalConfiguration() = compositeDisposable
        .add(localDataUseCase.getWholeConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.mapToPresentation() }
            .subscribe(
                {
                    configuration.value = it
                    checkForForceRequestFlag()
                },
                {
                    hideTerminalId.value = true
                    showActionMenu.value = true
                    tvReadyMessage.value = context.getString(R.string.title_device_not_configured)
                }
            )
        )

    fun setTerminalConfigurationStatus(isConfigured: Boolean) {
        localDataUseCase.saveTerminalConfigurationStatus(isConfigured)
    }

    fun getTerminalConfigurationStatus(): Boolean =
        localDataUseCase.isTerminalConfigured(defaultValue = false)

    fun disableCardMonitoring(task: TASK) {
        logger.log("disableCardMonitor ::: Task ::: ".plus(task))
        setCardMonitoringPreference(false)
        stopCardInsertMonitoring(task)
    }

    private fun setCardMonitoringPreference(cardMonitoring: Boolean) {
        this.localDataUseCase.setCardMonitoringPreference(cardMonitoring)
    }

    private fun stopCardInsertMonitoring(task: TASK) {
        logger.log("stopCardInsertMonitoring ::: Task ::: ".plus(task))
        showActionMenu.value = false
        tvReadyMessage.value = context.getString(R.string.please_wait)
        this.cardAlreadyRead = false
        Completable
            .fromAction { deviceUseCase.closeUpIO() }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { onCleanUpComplete(task) },
                { throwable: Throwable -> onCleanUpError(throwable, task) }
            )

    }

    private fun onCleanUpComplete(task: TASK) {
        logger.log("Clean Up Complete ::: Task ::: ".plus(task))
        when (task) {
            TASK.CHECK_IF_CARD_PRESENT -> checkIfCardIsPresent()
            TASK.CHECK_CARD_AND_FORCE_REQUEST_FLAG -> checkCardAndForceRequestFlag()
            TASK.START_CARD_MONITORING -> startCardInsertMonitoring()
            TASK.RECONCILIATION -> TODO()
            TASK.START_DASHBOARD -> this.startDashboard.value =
                localDataUseCase.getPosMode(PosMode.RETAILER)
            else -> logger.log("Task ::: ".plus(task))
        }
    }

    private fun onCleanUpError(throwable: Throwable, task: TASK) {
        logger.log("FinPos-Log Clean Up Error ::: ".plus(throwable.message))
        showActionMenu.value = true
        stopCardInsertMonitoring(task)
    }

    private fun checkCardAndForceRequestFlag() {
        logger.log("CheckCardAndForceRequestFlag...")
        if (!NiblMerchant.INSTANCE.idleActivityVisible())
            return

        compositeDisposable.add(
            deviceUseCase.checkIccCardPresentInReader()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::onCheckCardAndForceRequestFlagCompleted,
                    this::onCheckCardAndForceRequestFlagError
                )
        )
    }

    private fun onCheckCardAndForceRequestFlagError(throwable: Throwable) {
        logger.log("onCheckCardAndForceRequestFlagError ::: error ::: ".plus(throwable.message))
        throwable.printStackTrace()
        this.showActionMenu.value = true
        stopCardInsertMonitoring(TASK.CHECK_CARD_AND_FORCE_REQUEST_FLAG)
    }

    private fun onCheckCardAndForceRequestFlagCompleted(cardPresent: Boolean) {
        logger.log("onCheckCardAndForceRequestFlagCompleted...")
        if (cardPresent) {
            logger.log("Card is present in the reader...")
            this.loadCardCheckActivity.value = true
        } else {
            logger.log("Card is not present in the reader...")
            this.loadCardCheckActivity.value = false
            retrieveTerminalConfiguration()
        }
    }

    private fun checkForForceRequestFlag() {
        logger.log("checkForForceRequestFlag...")
        if (localDataUseCase.getForceSettlementPreference(false)) {
            logger.log("::: FORCE SETTLEMENT ::: ")
            startSettlement.value = true
        } else {
            logger.log("::: FORCE SETTLEMENT FALSE ::: ")
            enableHardwareButtons()
            enableCardMonitoring()
        }
    }

    fun enableHardwareButtons() {
        compositeDisposable.add(
            deviceUseCase.enableHardwareButtons()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::onNextEnableHardwareButtons,
                    this::onErrorEnableHardwareButtons
                )
        )
    }

    private fun onNextEnableHardwareButtons(deviceResponse: DeviceResponse) {
        logger.log(
            "onNextEnableHardwareButtons ::: DeviceResponse Result::: "
                .plus(deviceResponse.result)
        )
        logger.log(
            "onNextEnableHardwareButtons ::: DeviceResponse Message::: "
                .plus(deviceResponse.message)
        )
    }

    private fun onErrorEnableHardwareButtons(throwable: Throwable) {
        logger.log("onErrorEnableHardwareButtons ::: Error ::: ".plus(throwable.message))
    }

    private fun disableHardwareButtons() {
        compositeDisposable.add(
            deviceUseCase.disableHardwareButtons()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::onNextDisableHardwareButtons,
                    this::onErrorDisableHardwareButtons
                )
        )
    }

    private fun onNextDisableHardwareButtons(deviceResponse: DeviceResponse) {
        logger.log(
            "onNextDisableHardwareButtons ::: DeviceResponse Result::: "
                .plus(deviceResponse.result)
        )
        logger.log(
            "onNextDisableHardwareButtons ::: DeviceResponse Message::: "
                .plus(deviceResponse.message)
        )
    }

    private fun onErrorDisableHardwareButtons(throwable: Throwable) {
        logger.log("onErrorDisableHardwareButtons ::: Error ::: ".plus(throwable.message))
    }

    fun enableCardMonitoring() {
        if (NiblMerchant.INSTANCE.idleActivityVisible()) {
            logger.log("enableCardMonitoring...")
            setCardMonitoringPreference(true)
            cleanUpAndCheckIfCardInserted()
        }
    }

    fun cleanUpAndCheckIfCardInserted() {
        logger.log("cleanUpAndCheckIfCardInserted...")
        stopCardInsertMonitoring(TASK.CHECK_IF_CARD_PRESENT)
    }

    private fun checkIfCardIsPresent() {
        if (!NiblMerchant.INSTANCE.idleActivityVisible()) {
            return
        }
        logger.log("checkIfCardIsPresent...")
        compositeDisposable.add(
            deviceUseCase.checkIccCardPresentInReader()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::onNextCheckIfCardPresent,
                    this::onErrorCheckIfCardPresent
                )
        )
    }

    private fun onErrorCheckIfCardPresent(throwable: Throwable) {
        logger.log("onErrorCheckIfCardPresent ::: Error ::: ".plus(throwable.message))
    }

    private fun onNextCheckIfCardPresent(cardPresent: Boolean) {
        logger.log("onNextCheckIfCardPresent...")
        if (cardPresent) {
            logger.log("Card Present in the reader...")
            this.loadCardCheckActivity.value = true
        } else {
            logger.log("::: Card not present in the reader...")
            setCardMonitoringPreference(true)
            if (deviceConfigured()) {
                checkPosReady()
            } else {
                showActionMenu.value = true
                tvReadyMessage.value = context.getString(R.string.title_device_not_configured)
            }
        }
    }

    private fun checkPosReady() {
        compositeDisposable.add(
            deviceUseCase.posReady(
                TransactionUtils.getAllowedCardEntryModes(TransactionType.PURCHASE) // TODO need to change transaction utils
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::onNextCheckPosReady,
                    this::onErrorCheckPosReady
                )
        )
    }

    private fun onNextCheckPosReady(posResponse: PosResponse) {
        logger.log("onNextCheckPosReady...")
        if (posResponse.posResult == PosResult.SUCCESS) {
            tvPosStatusMessage.value = ""
            startCardInsertMonitoring()
        } else {
            showActionMenu.value = true
            tvReadyMessage.value = PosError.DEVICE_ERROR_NOT_READY.errorMessage
            tvPosStatusMessage.value = PosError.DEVICE_ERROR_NOT_READY.errorMessage
            if (getCardMonitoringEnabledPreference()) {
                cleanUpAndCheckIfCardInserted()
            }
        }
    }

    private fun onErrorCheckPosReady(throwable: Throwable) {
        throwable.printStackTrace()
        logger.log("onErrorCheckPosReady ::: Error ::: ".plus(throwable.message))
        tvReadyMessage.value = PosError.DEVICE_ERROR_NOT_READY.errorMessage
        if (getCardMonitoringEnabledPreference()) {
            cleanUpAndCheckIfCardInserted()
        }
    }

    private fun startCardInsertMonitoring() {
        if (!NiblMerchant.INSTANCE.idleActivityVisible())
            return

        logger.log("startCardInsertMonitoring...")
        val cardMonitoringEnabled = getCardMonitoringEnabledPreference()
        logger.log("Card Monitoring Enabled ? ::: ".plus(cardMonitoringEnabled))
        showActionMenu.value = true
        if (cardMonitoringEnabled && configuration.value != null) {
            tvReadyMessage.value = context.getString(R.string.title_ready)
            this.cardAlreadyRead = false
            compositeDisposable.add(
                deviceUseCase.readCardDetails(configuration.value!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        this::onNextCardInsertMonitoring,
                        this::onErrorCardInsertMonitoring
                    )
            )
        }
    }

    private fun onErrorCardInsertMonitoring(throwable: Throwable) {
        logger.log("onErrorCardInsertMonitoring ::: Error ::: ".plus(throwable.message))
        isLoading.value = false
        if ((throwable is PosException) &&
            throwable.posError == PosError.DEVICE_ERROR_APPLICATION_BLOCKED
        ) {
            cardReadError.value = CardReadError(
                context.getString(R.string.title_error).toUpperCase(Locale.getDefault()),
                throwable.message!!,
                false
            )
        } else {
            if (getCardMonitoringEnabledPreference())
                cleanUpAndCheckIfCardInserted()
        }
    }

    private fun onNextCardInsertMonitoring(readCardResponse: ReadCardResponse?) {
        logger.log("onNextCardInsertMonitoring...")
        isLoading.value = false
        this.cardAlreadyRead = true
        when {
            readCardResponse == null -> {
                cleanUpAndCheckIfCardInserted()
            }
            readCardResponse.result == Result.TIMEOUT -> {
                stopCardInsertMonitoring(TASK.CHECK_CARD_AND_FORCE_REQUEST_FLAG)
            }
            readCardResponse.result == Result.USER_CANCELLED -> {
                message.value = PosError.DEVICE_ERROR_USER_CANCELLED.errorMessage
            }
            readCardResponse.result == Result.CALLBACK_AMOUNT -> {
                loadAmountPage.value = true
            }
            readCardResponse.result == Result.CANCEL -> {
                logger.log("::: ON NEXT CARD INSERT MONITORING ::: READ CARD CANCEL >>>")
            }
            else -> {
                cleanUpAndCheckIfCardInserted()
            }
        }
    }

    fun getCardMonitoringEnabledPreference(): Boolean {
        return this.localDataUseCase.getCardMonitoringPreference(false)
    }

    private fun deviceConfigured(): Boolean {
        return this.localDataUseCase.isTerminalConfigured(defaultValue = false)
    }

    fun startTransaction(amount: BigDecimal) {
        compositeDisposable.add(
            idlePurchaseUseCase.purchase(
                configuration.value!!,
                PurchaseRequestItem(amount, TransactionType.PURCHASE)
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::onNextStartTransaction,
                    this::onErrorStartTransaction
                )
        )
    }

    private fun onNextStartTransaction(purchaseResponseEntity: PurchaseResponseEntity) {
        logger.log(
            "onNextStartTransaction ::: PurchaseResponseModel ::: Is Approved? ::: "
                .plus(purchaseResponseEntity.isApproved)
        )
        logger.log(
            "onNextStartTransaction ::: PurchaseResponseModel ::: Message ::: "
                .plus(purchaseResponseEntity.message)
        )
        isLoading.value = false
        showTransactionConfirmationDialog(purchaseResponseEntity)
        updateMerchantTransactionLog(
            purchaseResponseEntity.stan,
            purchaseResponseEntity.message,
            purchaseResponseEntity.isApproved
        )
    }

    private fun onErrorStartTransaction(throwable: Throwable) {
        logger.log("onErrorStartTransaction ::: Error ::: ".plus(throwable.message))
        throwable.printStackTrace()
        isLoading.value = false
        transactionComplete.value = false
        message.value = throwable.message
    }

    private fun showTransactionConfirmationDialog(it: PurchaseResponseEntity) {
        val transactionConfirmation = this.retrieveTransactionConfirmation(
            it.isApproved!!, it.message
        )
        transactionConfirmationData.value = transactionConfirmation
    }

    private fun retrieveTransactionConfirmationMessage(
        approved: Boolean,
        message: String?
    ): String {
        return if (approved)
            message.plus("\n\nPRINT CUSTOMER COPY?")
        else
            message!!
    }

    private fun retrievePositiveLabel(approved: Boolean): String {
        return if (approved)
            "Print"
        else
            ""
    }

    private fun retrieveNegativeLabel(approved: Boolean): String {
        return if (approved)
            "Cancel"
        else
            "OK"
    }

    private fun retrieveTransactionConfirmation(
        isApproved: Boolean,
        message: String?
    ): TransactionConfirmation? {
        return TransactionConfirmation.Builder()
            .imageId(retrieveImageId(isApproved))
            .title(retrieveTitle(isApproved))
            .message(
                this.retrieveTransactionConfirmationMessage(
                    isApproved,
                    message
                )
            )
            .positiveLabel(this.retrievePositiveLabel(isApproved))
            .negativeLabel(this.retrieveNegativeLabel(isApproved))
            .build()
    }

    private fun retrieveImageId(approved: Boolean): Int {
        return if (approved)
            R.drawable.ic_check_circle_green_64dp
        else
            R.drawable.ic_failure_red_64dp
    }

    private fun retrieveTitle(approved: Boolean): String {
        return if (approved)
            context.getString(R.string.title_approved)
                .toUpperCase(Locale.getDefault())
        else
            context.getString(R.string.title_declined)
                .toUpperCase(Locale.getDefault())
    }

    enum class TASK {
        NONE,
        CHECK_IF_CARD_PRESENT,
        CHECK_CARD_AND_FORCE_REQUEST_FLAG,
        START_CARD_MONITORING,
        START_DASHBOARD,
        RECONCILIATION
    }
}