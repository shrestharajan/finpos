package global.citytech.finpos.processor.nibl.transaction.validator;

import java.math.BigDecimal;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.iso8583.S2MActionCode;
import global.citytech.finpos.processor.nibl.transaction.TransactionSetTableVerifier;
import global.citytech.finpos.processor.nibl.transaction.utils.TransactionUtils;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.usecases.TransactionStatusFromHost;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.TransactionValidator;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.CardUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Unique Shakya on 8/27/2020.
 */
public class NiblTransactionValidator implements TransactionValidator {

    private ApplicationRepository applicationRepository;

    public NiblTransactionValidator(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    @Override
    public void validateRequest(TransactionRequest transactionRequest) {
        this.checkAmount(transactionRequest.getAmount());
    }

    private void checkAmount(BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) < 0)
            throw new PosException(PosError.DEVICE_ERROR_INVALID_AMOUNT);
    }

    @Override
    public boolean isValidTransaction(TransactionRequest transactionRequest,
                                      ReadCardResponse readCardResponse, String cardScheme) {
        return this.isTransactionAllowed(transactionRequest, readCardResponse, cardScheme);
    }

    @Override
    public boolean isTransactionDeclineByCardEvenIfSuccessOnSwitch(IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse) {
        //TODO : effected with new change in messaging api  ( need to fixed by Suraj )
        boolean isApprovedByActionCode = this.isApprovedByActionCode(IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, 39));
        if (readCardResponse.getCardDetails().getCardType() == CardType.MAG ||
                readCardResponse.getCardDetails().getCardType() == CardType.PICC)
            return false;
        return isApprovedByActionCode && this.declinedByCard(readCardResponse);
    }

    private boolean declinedByCard(ReadCardResponse readCardResponse) {
        try {
            return readCardResponse.getCardDetails().getTagCollection().get(IccData.CID.getTag()).getData().equals("00");
        } catch (Exception e) {
            Logger.getLogger(e.getLocalizedMessage());
            return false;
        }
    }

    private boolean isApprovedByActionCode(int actionCode) {
        return S2MActionCode.getByActionCode(actionCode).getTransactionStatusFromHost() == TransactionStatusFromHost.ONLINE_APPROVED;
    }

    private boolean isTransactionAllowed(TransactionRequest transactionRequest, ReadCardResponse readCardResponse,
                                         String cardScheme) {
        TransactionType transactionType = transactionRequest.getTransactionType();
        return this.isTransactionAllowedForIccFallbackToMag(transactionRequest, readCardResponse) &&
                this.isTransactionAllowedByTms(transactionType, cardScheme);
    }

    private boolean isTransactionAllowedByTms(TransactionType transactionType, String cardScheme) {
        if (!StringUtils.isEmpty(cardScheme)) {
            TransactionSetTableVerifier verifier = new TransactionSetTableVerifier(
                    applicationRepository,
                    CardUtils.getShortAidLabel(cardScheme),
                    TransactionUtils.mapPurchaseTypeWithTransactionType(transactionType)
            );
            return verifier.isTransactionAllowed();
        }
        return false;
    }

    private boolean isTransactionAllowedForIccFallbackToMag(TransactionRequest transactionRequest, ReadCardResponse readCardResponse) {
        return true;
    }
}
