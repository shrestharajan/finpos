package global.citytech.finpos.processor.neps.transaction;

import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.repositories.AidRetrieveRequest;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.CardSchemeRetrieveRequest;
import global.citytech.finposframework.repositories.FieldAttributes;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.IdentifyCardResponse;
import global.citytech.finposframework.utility.CardUtils;
import global.citytech.finposframework.utility.LuhnCheck;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Saurav Ghimire on 1/21/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class CardIdentifier {
    TransactionRepository transactionRepository;
    ApplicationRepository applicationRepository;


    public CardIdentifier(TransactionRepository transactionRepository, ApplicationRepository applicationRepository) {
        this.transactionRepository = transactionRepository;
        this.applicationRepository = applicationRepository;
    }

    public IdentifyCardResponse identify(ReadCardResponse readCardResponse) {
        if (readCardResponse == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        return this.prepareIdentifyCardResponse(readCardResponse);
    }

    private IdentifyCardResponse prepareIdentifyCardResponse(ReadCardResponse readCardResponse) {
        if (readCardResponse.getCardDetails().getCardType() == CardType.MAG &&
                StringUtils.isEmpty(readCardResponse.getCardDetails().getTrackTwoData())) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        String cardScheme = this.getCardScheme(
                readCardResponse,
                this.applicationRepository
        );
        System.out.println("RETRIEVED CARD SCHEME === " + cardScheme);
        readCardResponse.getCardDetails().setCardScheme(CardUtils.retrieveCardSchemeType(cardScheme));
        if (StringUtils.isEmpty(readCardResponse.getCardDetails().getCardSchemeLabel()))
            readCardResponse.getCardDetails().setCardSchemeLabel(cardScheme);
        if (!this.isValidCard(cardScheme, readCardResponse.getCardDetails().getPrimaryAccountNumber())) {
            throw new PosException(PosError.DEVICE_ERROR_INVALID_CARD);
        }
        return new IdentifyCardResponse.Builder()
                .cardDetails(this.prepareCardDetails(readCardResponse, cardScheme))
                .build();
    }

    private CardDetails prepareCardDetails(ReadCardResponse readCardResponse, String cardScheme) {
        if (readCardResponse == null) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
        if (readCardResponse.getResult() == Result.FAILURE) {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }

        CardDetails cardDetails = readCardResponse.getCardDetails();
        cardDetails.setCardScheme(CardUtils.retrieveCardSchemeType(cardScheme));
        return cardDetails;
    }

    private boolean isValidCard(String cardScheme, String primaryAccountNumber) {
        if (this.isLuhnCheckRequired(cardScheme))
            return LuhnCheck.getInstance().isValidCardNumber(primaryAccountNumber);
        return true;
    }

    private boolean isLuhnCheckRequired(String cardScheme) {
        CardSchemeRetrieveRequest cardSchemeRetrieveRequest = new CardSchemeRetrieveRequest(
                CardUtils.retrieveCardSchemeType(cardScheme).getCardSchemeId(),
                FieldAttributes.CARD_SCHEME_S2_F26_LUHN_CHECK
        );
        String luhnCheck = this.applicationRepository
                .retrieveCardScheme(cardSchemeRetrieveRequest)
                .getData();
        if (!StringUtils.isEmpty(luhnCheck))
            return luhnCheck.equals("1");
        return false;
    }

    private String getCardScheme(ReadCardResponse readCardResponse, ApplicationRepository applicationRepository) {
        if (readCardResponse != null && readCardResponse.getCardDetails() != null) {
            if (readCardResponse.getCardDetails().getCardType() == CardType.ICC ||
                    readCardResponse.getCardDetails().getCardType() == CardType.PICC)
                return this.cardSchemeForEmv(applicationRepository, readCardResponse);
            else
                return new CardSchemeRetriever(applicationRepository).retrieveCardScheme(readCardResponse
                        .getCardDetails().getPrimaryAccountNumber()).getCardScheme();
        }
        throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
    }

    private String cardSchemeForEmv(ApplicationRepository applicationRepository, ReadCardResponse readCardResponse) {
        AidRetrieveRequest aidRetrieveRequest = new AidRetrieveRequest(
                readCardResponse.getCardDetails().getAid(),
                FieldAttributes.AID_DATA_S1_F6_AID_LABEL
        );
        return applicationRepository.retrieveAid(aidRetrieveRequest).getData();
    }
}
