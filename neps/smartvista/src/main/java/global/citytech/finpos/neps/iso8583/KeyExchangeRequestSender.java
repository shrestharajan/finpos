package global.citytech.finpos.neps.iso8583;

import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.switches.FunctionCode;

/**
 * User: Surajchhetry Date: 2/28/20 Time: 4:20 PM
 */
public class KeyExchangeRequestSender extends NepsMessageSenderTemplate {

    public KeyExchangeRequestSender(RequestContext context) {
        super(context);
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Network_Management,
                        MTI.MessageFunction.Request,
                        MTI.Originator.Acquirer);
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);

        MessagePayload.Builder builder = new NepsMessagePayloadBuilder(this.specInfo, msg);
        return builder.build();
    }

    protected String getProcessingCode() {
        return ProcessingCode.KEY_EXCHANGE.getCode();
    }

    protected String getFunctionCode() {
        return FunctionCodeConfig.getValue(FunctionCode.KEY_EXCHANGE);
    }
}
