package global.citytech.finpos.processor.neps.transaction;

import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.CardSchemeRetrieveRequest;
import global.citytech.finposframework.repositories.CardSchemeRetrieveResponse;
import global.citytech.finposframework.repositories.FieldAttributes;
import global.citytech.finposframework.utility.StringUtils;

public class TransactionSetTableVerifier {

    private String cardScheme;
    private ApplicationRepository applicationRepository;
    private int offset;

    public TransactionSetTableVerifier(
            ApplicationRepository applicationRepository,
            String cardScheme,
            TransactionType transactionType
    ) {
        this.applicationRepository = applicationRepository;
        this.offset = transactionType.getOffset();
        this.cardScheme = cardScheme;
    }

    private String retrieveCardSchemeParameters(FieldAttributes fieldAttributes) {
        CardSchemeRetrieveRequest cardSchemeRetrieveRequest = new CardSchemeRetrieveRequest(this.cardScheme, fieldAttributes);
        CardSchemeRetrieveResponse response = applicationRepository.retrieveCardScheme(cardSchemeRetrieveRequest);
        return response.getData();
    }

    /* ----- Transaction Allow ----- */

    public boolean isTransactionAllowed() {
        boolean isAllowed;
        if (isDefaultConfigurationOffset(this.offset)) {
            isAllowed = this.transactionAllowCheckForDefaultConfigurationOffset(this.offset, this.cardScheme);
        } else {
            isAllowed = this.transactionAllowCheckForOtherConfigurationOffset();
        }
        return isAllowed;
    }

    private boolean transactionAllowCheckForDefaultConfigurationOffset(int offset, String cardScheme) {
        boolean isAllowed = false;
        if (offset == 9) {
            isAllowed = true;
        }
        return isAllowed;
    }

    private boolean transactionAllowCheckForOtherConfigurationOffset() {
        boolean isAllowed = false;
        String transactionAllowed = this.retrieveCardSchemeParameters(FieldAttributes.CARD_SCHEME_S2_F6_TRANSACTIONS_ALLOWED);
        if (!StringUtils.isEmpty(transactionAllowed))
            isAllowed = String.valueOf(transactionAllowed.charAt(offset)).equals("1");
        return isAllowed;
    }

    /* ----- Pin Verification Necessary ----- */

    public boolean isPinVerificationNecessary() {
        boolean isNecessary;
        if (!isDefaultConfigurationOffset(this.offset)) {
            isNecessary = true;
        } else {
            isNecessary = pinVerificationNecessityCheckForOtherConfigurationOffset();
        }
        return isNecessary;
    }


    private boolean pinVerificationNecessityCheckForOtherConfigurationOffset() {
        boolean isNecessary = true;
        String cardholderAuthentication = this.retrieveCardSchemeParameters(FieldAttributes.CARD_SCHEME_S2_F8_CARDHOLDER_AUTHENTICATION);
        System.out.println("Card Holder auth :::"+ cardholderAuthentication);
        if (!StringUtils.isEmpty(cardholderAuthentication)) {
            isNecessary = (String.valueOf(cardholderAuthentication.charAt(offset)).equals("2") ||
                    String.valueOf(cardholderAuthentication.charAt(offset)).equals("3"));
        }
        return isNecessary;
    }

    /* ----- Signature Verification Necessary ----- */

    public boolean isSignatureVerification() {
        boolean isNecessary;
        if (isDefaultConfigurationOffset(this.offset)) {
            isNecessary = true;
        } else {
            isNecessary = this.signatureVerificationCheckForOtherConfigurationOffset();
        }
        return isNecessary;
    }

    private boolean signatureVerificationCheckForOtherConfigurationOffset() {
        boolean isNecessary = false;
        String cardholderAuthentication = this.retrieveCardSchemeParameters(FieldAttributes.CARD_SCHEME_S2_F8_CARDHOLDER_AUTHENTICATION);
        if (!StringUtils.isEmpty(cardholderAuthentication)) {
            isNecessary = (String.valueOf(cardholderAuthentication.charAt(offset)).equals("1") ||
                    String.valueOf(cardholderAuthentication.charAt(offset)).equals("3"));
        }
        return isNecessary;
    }

    /* ----- CardHolder Verification ----- */

    public String getCardHolderVerificationMethod() {
        String value;
        if (isDefaultConfigurationOffset(this.offset)) {
            value = this.cardHolderVerificationForDefaultConfigurationOffset(this.offset, this.cardScheme);
        } else {
            value = this.cardHolderVerificationForOtherConfigurationOffset();
        }
        return value;
    }

    private String cardHolderVerificationForDefaultConfigurationOffset(int offset, String cardScheme) {
        String value = "";
        switch (offset) {
            case 7:
            case 8:
                value = "0";
                break;
            case 9:
                value = "3";
                break;
            default:
                break;
        }
        return value;
    }

    private String cardHolderVerificationForOtherConfigurationOffset() {
        String value = "";
        String cardholderAuthentication = this.retrieveCardSchemeParameters(FieldAttributes.CARD_SCHEME_S2_F8_CARDHOLDER_AUTHENTICATION);
        if (!StringUtils.isEmpty(cardholderAuthentication)) {
            value = String.valueOf(cardholderAuthentication.charAt(offset));
        }
        return value;
    }

    /* ----- Supervisor Function Check ----- */

    public boolean isSupervisorPasswordRequired() {
        boolean isRequired = false;
        if (!isDefaultConfigurationOffset(this.offset)) {
            isRequired = this.supervisorFunctionCheckForOtherConfigurationOffset();
        }
        return isRequired;
    }

    private boolean supervisorFunctionCheckForOtherConfigurationOffset() {
        boolean isRequired = false;
        String supervisorFunctions = this.retrieveCardSchemeParameters(FieldAttributes.CARD_SCHEME_S2_F10_SUPERVISOR_FUNCTIONS);
        if (!StringUtils.isEmpty(supervisorFunctions)) {
            isRequired = String.valueOf(supervisorFunctions.charAt(offset)).equals("1");
        }
        return isRequired;
    }

    /* ----- Manual Entry Allow ----- */

    public boolean isManualEntryAllowed() {
        boolean isAllowed;
        if (this.isDefaultConfigurationOffset(this.offset)) {
            isAllowed = true;
        } else {
            isAllowed = this.manualEntryAllowCheckForOtherConfigurationOffset();
        }
        return isAllowed;
    }


    private boolean manualEntryAllowCheckForOtherConfigurationOffset() {
        boolean isAllowed = false;
        String manualEntryAllowed = this.retrieveCardSchemeParameters(FieldAttributes.CARD_SCHEME_S2_F12_MANUAL_ENTRY_ALLOWED);
        if (!StringUtils.isEmpty(manualEntryAllowed)) {
            isAllowed = String.valueOf(manualEntryAllowed.charAt(offset)).equals("1");
        }
        return isAllowed;
    }

    /* ----- Max Amount Allow ----- */

    public boolean shouldCheckMaxAmountAllowed() {
        boolean isAllowed = false;
        if (!this.isDefaultConfigurationOffset(this.offset)) {
            isAllowed = this.maxAmountAllowCheckForOtherConfigurationOffset();
        }
        return isAllowed;
    }

    private boolean maxAmountAllowCheckForOtherConfigurationOffset() {
        boolean isAllowed = true;
        String maxAmountIndicator = this.retrieveCardSchemeParameters(FieldAttributes.CARD_SCHEME_S2_F22_MAXM_TRANSACTION_AMOUNT_INDICATOR);
        if (!StringUtils.isEmpty(maxAmountIndicator)) {
            isAllowed = String.valueOf(maxAmountIndicator.charAt(offset)).equals("1");
        }
        return isAllowed;
    }

    private boolean isDefaultConfigurationOffset(int offset) {
        return (offset == 7 || offset == 8 || offset == 9 || offset == 11 ||  offset == 10);

    }

}
