package global.citytech.finpos.processor.neps.logon;

import java.sql.Timestamp;
import java.util.Date;

import global.citytech.finpos.neps.iso8583.LogonRequestSender;
import global.citytech.finpos.neps.receipt.logon.NepsMerchantLogonReceiptHandler;
import global.citytech.finpos.neps.receipt.logon.NepsMerchantLogonReceiptPrintHandler;
import global.citytech.finpos.processor.neps.keyexchange.KeyExchangeRequestModel;
import global.citytech.finpos.processor.neps.keyexchange.KeyExchangeResponseModel;
import global.citytech.finpos.processor.neps.keyexchange.MerchantKeyExchangeUseCase;
import global.citytech.finpos.processor.neps.transaction.NepsIsoMessageReceiptHandler;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.logon.LogOnRequester;
import global.citytech.finposframework.switches.logon.LogOnResponseParameter;
import global.citytech.finposframework.usecases.activity.ActivityLog;
import global.citytech.finposframework.usecases.activity.ActivityLogStatus;
import global.citytech.finposframework.usecases.activity.ActivityLogType;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.receipt.logon.MerchantLogonReceipt;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StringUtils;

/**
 * User: Surajchhetry Date: 2/28/20 Time: 6:02 PM
 * Modified : Rishav Chudal Date: 10/04/21 Time: 12:44 PM
 */
public class MerchantLogOnUseCase
        implements UseCase<LogOnRequestModel, LogOnResponseModel>,
        LogOnRequester<LogOnRequestModel, LogOnResponseParameter> {

    private final TerminalRepository repository;
    private final Logger logger = Logger.getLogger(MerchantLogOnUseCase.class.getSimpleName());
    private LogOnRequestModel logOnRequestModel;
    private LogOnResponseModel logOnResponseModel;
    private IsoMessageResponse logOnIsoMessageResponse;

    public MerchantLogOnUseCase(TerminalRepository repository) {
        this.repository = repository;
    }

    private RequestContext prepareLogOnRequestContext() {
        HostInfo primaryHostInfo = this.repository.findPrimaryHost();
        HostInfo secondaryHostInfo = this.repository.findSecondaryHost();
        TerminalInfo terminalInfo = this.repository.findTerminalInfo();
        ConnectionParam connectionParam = new ConnectionParam(primaryHostInfo, secondaryHostInfo);
        return new RequestContext(this.repository.getSystemTraceAuditNumber(), terminalInfo, connectionParam);
    }

    @Override
    public LogOnResponseModel execute(LogOnRequestModel logOnRequestModel) {
        this.logOnRequestModel = logOnRequestModel;
        try {
            sendLogOnRequestToHost();
            updateActivityLogWithIsoMessageResponse(logOnIsoMessageResponse);
            increaseSystemTraceAuditNumber();
            validateLogOnIsoResponse();
            return logOnResponseModel;
        } catch (FinPosException finPosException) {
            finPosException.printStackTrace();
            updateActivityLogFailedWithFinPosException(finPosException);
        } catch (Exception exception) {
            exception.printStackTrace();
            updateActivityLogFailedWithRemarks(exception.getMessage());
        }
        assignNullLogOnResponse();
        return logOnResponseModel;
    }

    private void sendLogOnRequestToHost() {
        RequestContext logOnRequestContext = prepareLogOnRequestContext();
        logger.debug(String.format("LogOn Request Context ::%s", logOnRequestContext));
        LogonRequestSender logonRequestSender = new LogonRequestSender(logOnRequestContext);
        logOnIsoMessageResponse = logonRequestSender.send();
    }

    private void increaseSystemTraceAuditNumber() {
        repository.incrementSystemTraceAuditNumber();
    }

    private void validateLogOnIsoResponse() {
        logger.debug("Log On Iso Response ::: " + this.logOnIsoMessageResponse);
        if (logOnIsoMessageResponse != null) {
            printMerchantLogOnReceipt();
            performKeyExchangeIfSuccessLogOn();
        } else {
            logger.debug("Log On Iso Message Response is null...");
            assignNullLogOnResponse();
        }
    }

    private void printMerchantLogOnReceipt() {
        printDebugReceiptIfDebugMode();
        MerchantLogonReceipt merchantLogonReceipt =
                new NepsMerchantLogonReceiptHandler(logOnIsoMessageResponse).prepare();
        ReceiptHandler.MerchantLogonReceiptHandler receiptHandler =
                new NepsMerchantLogonReceiptPrintHandler(logOnRequestModel.getPrinterService());
        receiptHandler.printLogonReceipt(merchantLogonReceipt);
    }

    private void printDebugReceiptIfDebugMode() {
        if (isAdminDebugMode()) {
            ReceiptHandler.IsoMessageReceiptHandler debugReceiptHandler =
                    new NepsIsoMessageReceiptHandler(logOnRequestModel.getPrinterService());
            debugReceiptHandler.printIsoMessageReceipt(logOnIsoMessageResponse);
        }
    }

    private boolean isAdminDebugMode() {
        return repository.findTerminalInfo().isDebugModeEnabled();
    }

    private void performKeyExchangeIfSuccessLogOn() {
        if (isLogOnSuccess()) {
            performKeyExchange();
        } else {
            assignFailureLogOnResponse();
        }
    }

    private boolean isLogOnSuccess() {
        int responseCode = IsoMessageUtils.retrieveFromDataElementsAsInt(
                logOnIsoMessageResponse,
                DataElement.RESPONSE_CODE
        );
        return responseCode == 0;
    }

    private void performKeyExchange() {
        MerchantKeyExchangeUseCase merchantKeyExchangeUseCase =
                new MerchantKeyExchangeUseCase(repository);
        KeyExchangeRequestModel keyExchangeRequestModel = new KeyExchangeRequestModel(
                logOnRequestModel.getPrinterService(),
                logOnRequestModel.getHardwareKeyService(),
                logOnRequestModel.getApplicationPackageName()
        );
        KeyExchangeResponseModel keyExchangeResponseModel = merchantKeyExchangeUseCase
                .execute(keyExchangeRequestModel);
        assignLogOnResponseBasedOnKeyExchangeResponse(keyExchangeResponseModel);
    }

    private void assignFailureLogOnResponse() {
        logOnResponseModel = LogOnResponseModel.Builder.createDefaultBuilder()
                .success(false)
                .debugRequestMessage(logOnIsoMessageResponse.getDebugRequestString())
                .debugResponseMessage(logOnIsoMessageResponse.getDebugResponseString())
                .build();
    }

    private void assignNullLogOnResponse() {
        logOnResponseModel = LogOnResponseModel.Builder.createDefaultBuilder()
                .success(false)
                .build();
    }

    private void assignLogOnResponseBasedOnKeyExchangeResponse(
            KeyExchangeResponseModel keyExchangeResponseModel
    ) {
        LogOnResponseModel.Builder builder = LogOnResponseModel.Builder.createDefaultBuilder();
        builder.success(keyExchangeResponseModel.isSuccess());
        builder.debugRequestMessage(logOnIsoMessageResponse.getDebugRequestString());
        builder.debugResponseMessage(logOnIsoMessageResponse.getDebugResponseString());
        logOnResponseModel = builder.build();
    }

    private void updateActivityLogWithIsoMessageResponse(IsoMessageResponse isoMessageResponse) {
        try {
            TerminalInfo terminalInfo = this.repository.findTerminalInfo();
            String responseCode = IsoMessageUtils.retrieveFromDataElementsAsString(
                    isoMessageResponse,
                    DataElement.RESPONSE_CODE
            );
            String isoResponse = "";
            ActivityLogStatus status = ActivityLogStatus.SUCCESS;
            if (!responseCode.equals("000")) {
                status = ActivityLogStatus.FAILED;
                isoResponse = isoMessageResponse.getDebugRequestString();
            }
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.repository.getSystemTraceAuditNumber())
                    .withType(ActivityLogType.LOGON)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber("")
                    .withAmount(0L)
                    .withResponseCode(responseCode)
                    .withIsoRequest(isoMessageResponse.getDebugRequestString())
                    .withIsoResponse(isoResponse)
                    .withStatus(status)
                    .withRemarks("")
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(
                            StringUtils.prepareAdditionalDataForActivityLog(
                                    this.repository.getTerminalSerialNumber()
                            )
                    )
                    .build();
            this.repository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update LOGON activity log :: " + e.getMessage());
        }
    }

    private void updateActivityLogFailedWithFinPosException(FinPosException finPosException) {
        try {
            TerminalInfo terminalInfo = this.repository.findTerminalInfo();
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.repository.getSystemTraceAuditNumber())
                    .withType(ActivityLogType.LOGON)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber("")
                    .withAmount(0L)
                    .withResponseCode("")
                    .withIsoRequest(finPosException.getIsoRequest())
                    .withIsoResponse("")
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(
                            finPosException.getLocalizedMessage() +
                                    " : " +
                                    finPosException.getDetailMessage()
                    )
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(
                            StringUtils.prepareAdditionalDataForActivityLog(
                                    this.repository.getTerminalSerialNumber()
                            )
                    )
                    .build();
            this.repository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update LOGON activity log :: " + e.getMessage());
            updateActivityLogFailedWithRemarks(e.getMessage());
        }
    }

    private void updateActivityLogFailedWithRemarks(String remarks) {
        try {
            TerminalInfo terminalInfo = this.repository.findTerminalInfo();
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(this.repository.getSystemTraceAuditNumber())
                    .withType(ActivityLogType.LOGON)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber("")
                    .withAmount(0L)
                    .withResponseCode("")
                    .withIsoRequest("")
                    .withIsoResponse("")
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(remarks)
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .withAdditionalData(
                            StringUtils.prepareAdditionalDataForActivityLog(
                                    this.repository.getTerminalSerialNumber()
                            )
                    )
                    .build();
            this.repository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update LOGON activity log :: " + e.getMessage());
        }
    }
}
