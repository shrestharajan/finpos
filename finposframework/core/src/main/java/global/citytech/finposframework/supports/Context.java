package global.citytech.finposframework.supports;

import java.util.HashMap;
import java.util.Map;

/** User: Surajchhetry Date: 2/20/20 Time: 11:24 AM */
public interface Context {
  void add(String key, Object value);

  Object get(String key);

  class WithValue implements Context {
    private final Map<String, Object> holder;

    public WithValue() {
      this.holder = new HashMap<>();
    }

    @Override
    public void add(String key, Object value) {
      this.holder.put(key, value);
    }

    @Override
    public Object get(String key) {
      return this.holder.get(key);
    }
  }

  class Factory {
    public Context createContextWithValue() {
      return new WithValue();
    }
  }
}
