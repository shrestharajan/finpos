package global.citytech.finpos.merchant.framework.broadcasts

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import global.citytech.common.Constants
import global.citytech.common.data.PosCallback
import global.citytech.common.data.PosMessage
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finposframework.log.Logger

/**
 * Created by Unique Shakya on 4/28/2021.
 */
class SettlementAlarmReceiver : BroadcastReceiver() {

    val logger: Logger = Logger.getLogger(SettlementAlarmReceiver::class.java.name)
    var localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    companion object {
        const val ACTION_ALARM_SETTLEMENT = "action.alarm.settlement"
        const val ACTION_ALARM_SETTLEMENT_POSTPONE = "action.alarm.settlement.postpone"
        const val REQUEST_CODE_ALARM_SETTLEMENT = 7266
        const val REQUEST_CODE_ALARM_SETTLEMENT_POSTPONE = 7267
    }

    override fun onReceive(p0: Context?, p1: Intent?) {
        logger.log("::: ALARM BROADCAST RECEIVED ::: " + p1!!.action)
        if (p1.action == ACTION_ALARM_SETTLEMENT || p1.action == ACTION_ALARM_SETTLEMENT_POSTPONE)
            this.startSettlement(p0)
    }

    private fun startSettlement(p0: Context?) {
        logger.log("::: SETTLEMENT TIME REACHED :::")
        if (localDataUseCase.getTerminalSetting().enableAutoSettlement) {
            this.markForceSettlement()
            if (NiblMerchant.INSTANCE.isDashActivityVisible()) {
                logger.log("::: DASH ACTIVITY VISIBLE ::: START SETTLEMENT >>>")
                val intent = Intent(Constants.INTENT_ACTION_POS_CALLBACK)
                intent.putExtra(
                    Constants.KEY_BROADCAST_MESSAGE,
                    PosCallback(PosMessage.POS_MESSAGE_START_SETTLEMENT)
                )
                LocalBroadcastManager.getInstance(p0!!).sendBroadcast(intent)
            }
        } else {
            NiblMerchant.INSTANCE.cancelRescheduledSettlement()
        }
    }

    private fun markForceSettlement() {
        logger.log("::: MARKING FORCE SETTLEMENT :::")
        PreferenceManager.setForceSettlementPreference(true)
    }
}