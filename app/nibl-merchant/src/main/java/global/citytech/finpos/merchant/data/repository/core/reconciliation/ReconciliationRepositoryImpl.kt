package global.citytech.finpos.merchant.data.repository.core.reconciliation

import global.citytech.finpos.merchant.data.datasource.core.reconciliation.ReconciliationDataSource
import global.citytech.finpos.merchant.domain.model.reconciliation.ReconciliationResponseEntity
import global.citytech.finpos.merchant.domain.model.reconciliation.clear.BatchClearResponseEntity
import global.citytech.finpos.merchant.domain.repository.core.reconciliation.ReconciliationRepository
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 9/23/2020.
 */
class ReconciliationRepositoryImpl(
    private val reconciliationDataSource: ReconciliationDataSource
) : ReconciliationRepository {

    override fun reconcile(
        configurationItem: ConfigurationItem,
        settlementActive: Boolean
    ): Observable<ReconciliationResponseEntity> {
        return reconciliationDataSource.reconcile(configurationItem, settlementActive)
    }

    override fun checkReconciliationRequired(configurationItem: ConfigurationItem): Observable<Boolean> {
        return reconciliationDataSource.checkReconciliationRequired(configurationItem)
    }

    override fun clearBatch(configurationItem: ConfigurationItem): Observable<BatchClearResponseEntity> {
        return reconciliationDataSource.clearBatch(configurationItem)
    }
}