package global.citytech.finpos.merchant.presentation.cashadvance

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityBaseTransactionBinding
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionActivity
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finposframework.usecases.TransactionType

/**
 * Created by Unique Shakya on 2/4/2021.
 */
class CashAdvanceActivity :
    BaseTransactionActivity<ActivityBaseTransactionBinding, CashAdvanceViewModel>() {

    private lateinit var viewModel: CashAdvanceViewModel
    private var manualCashAdvance: Boolean = false
    private var cardNumber: String = ""
    private var expiryDate: String = ""
    private var cvv: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBaseViews()
        this.viewModel = getViewModel()
        initObservers()
        this.startAmountActivity()
    }

    private fun initObservers() {
        this.viewModel.configurationItem.observe(this, Observer { performCashAdvance(it) })
        initBaseLiveDataObservers()
    }

    private fun performCashAdvance(it: ConfigurationItem?) {
        this.showProgressScreen("Initializing Transaction...")
        if (manualCashAdvance) {
            this.viewModel.performManualCashAdvance(it!!, amount, getTransactionType(), cardNumber, expiryDate)
        } else {
            this.viewModel.performCashAdvance(it!!, amount, getTransactionType())
        }
    }

    override fun getTransactionType(): TransactionType = TransactionType.CASH_ADVANCE

    override fun getTransactionViewModel(): BaseTransactionViewModel = this.viewModel

    override fun onAmountActivityResult(data: Intent?) {
        data?.let {
            val amountInString =
                data.getStringExtra(getString(R.string.intent_confirmed_amount))
            amount = amountInString?.toBigDecimal()!!
            viewModel.getConfigurationItem()
        }
    }

    override fun onManualActivityResult(data: Intent?) {
        data?.let {
            this.cardNumber = data.getStringExtra(getString(R.string.title_card_number))
            this.expiryDate = data.getStringExtra(getString(R.string.title_expiry_date))
            this.cardNumber = data.getStringExtra(getString(R.string.title_card_number))!!
            this.expiryDate = data.getStringExtra(getString(R.string.title_expiry_date))!!
            this.cvv = data.getStringExtra(getString(R.string.title_cvv))!!
            this.viewModel.getConfigurationItem()
        }
    }

    override fun getBindingVariable(): Int = BR.cash_advance_view_model

    override fun getLayout(): Int = R.layout.activity_base_transaction

    override fun getViewModel(): CashAdvanceViewModel =
        ViewModelProviders.of(this)[CashAdvanceViewModel::class.java]

    override fun onPositiveButtonClick() {
        this.showProgressScreen("Printing Customer Copy..")
        this.viewModel.printCustomerCopy()
    }

    override fun onNegativeButtonClick() {
        checkForCardPresent()
    }

    override fun onManualButtonClicked() {
        this.viewModel.shouldDispose = true
        this.viewModel.doCleanUp(BaseTransactionViewModel.Task.MANUAL_TRANSACTION)
        this.manualCashAdvance = true
    }

    override fun onCancelButtonClicked() {
        this.viewModel.shouldDispose = true
        this.viewModel.doCleanUp(BaseTransactionViewModel.Task.RETURN_TO_DASHBOARD)
    }
}