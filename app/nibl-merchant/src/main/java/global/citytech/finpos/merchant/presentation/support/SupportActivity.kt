package global.citytech.finpos.merchant.presentation.support

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import global.citytech.finpos.merchant.R
import kotlinx.android.synthetic.main.activity_support.*

class SupportActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_support)
        iv_back.setOnClickListener {
            onBackPressed()
        }
    }
}