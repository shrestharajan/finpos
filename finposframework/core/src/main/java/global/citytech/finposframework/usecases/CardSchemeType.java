package global.citytech.finposframework.usecases;

public enum CardSchemeType {

    MADA("P1", "mada"),
    VISA_CREDIT("VC", "Visa Credit"),
    VISA_DEBIT("VD", "Visa Debit"),
    MASTERCARD("MC", "Mastercard"),
    UNION_PAY("UP", "Union Pay"),
    MAESTRO("DM", "Maestro"),
    AMEX("AX", "American Express"),
    GCCNET("GN", "GCCNET"),
    NONE("", "");

    private final String cardSchemeId;
    private final String cardScheme;

    CardSchemeType(String cardSchemeId, String cardScheme) {
        this.cardSchemeId = cardSchemeId;
        this.cardScheme = cardScheme;
    }


    public String getCardSchemeId() {
        return cardSchemeId;
    }

    public String getCardScheme() {
        return cardScheme;
    }

    public static CardSchemeType getByCardSchemeId(String cardSchemeId) {
        CardSchemeType[] dataTypes = values();
        CardSchemeType[] var2 = dataTypes;
        int var3 = dataTypes.length;

        for (int var4 = 0; var4 < var3; ++var4) {
            CardSchemeType dataType = var2[var4];
            if (dataType.getCardSchemeId().equalsIgnoreCase(cardSchemeId)) {
                return dataType;
            }
        }
        return NONE;
    }
}
