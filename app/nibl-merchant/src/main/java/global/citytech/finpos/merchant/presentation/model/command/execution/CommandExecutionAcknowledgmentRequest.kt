package global.citytech.finpos.merchant.presentation.model.command.execution

/**
 * Created by Unique Shakya on 9/21/2021.
 */
data class CommandExecutionAcknowledgmentRequest(
    var serialNumber: String,
    var terminalCommandId: String,
    var status: String,
    var remarks: String
)