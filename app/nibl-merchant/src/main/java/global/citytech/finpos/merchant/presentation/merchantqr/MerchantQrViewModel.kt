package global.citytech.finpos.merchant.presentation.merchantqr

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.common.Base64
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.framework.qr.QrHelper
import global.citytech.finpos.merchant.utils.ApiConstant
import global.citytech.finposframework.utility.StringUtils
import io.reactivex.Single
import io.reactivex.SingleEmitter
import io.reactivex.SingleOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MerchantQrViewModel(val context: Application) : BaseAndroidViewModel(context) {

    val merchantQr by lazy { MutableLiveData<MerchantQr>() }

    fun retrieveQrData() {
        isLoading.value = true
        compositeDisposable.add(
            Single.create(SingleOnSubscribe<MerchantQr> {
                retrieveQrTask(it)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    merchantQr.value = it
                }, {
                    isLoading.value = false
                })
        )
    }

    private fun retrieveQrTask(it: SingleEmitter<MerchantQr>) {
        val terminalId = DeviceConfiguration.get().terminalId
        val merchantId = DeviceConfiguration.get().merchantId
        val serialNumber = NiblMerchant.INSTANCE.iPlatformManager.serialNumber
        if (StringUtils.isEmpty(terminalId) || StringUtils.isEmpty(merchantId) || StringUtils.isEmpty(
                serialNumber
            )
        )
            it.onError(IllegalArgumentException("Values missing to retrieve QR"))
        val map = HashMap<String, String>()
        map["terminalId"] = terminalId!!
        map["merchantId"] = merchantId!!
        map["serialNumber"] = serialNumber!!
        map["baseUrl"] =
            NiblMerchant.INSTANCE.iPlatformManager.hostDomain + ApiConstant.MERCHANT_APP_BASE_URL
        val qrData = Jsons.toJsonObj(map)
        val qrBitmap = QrHelper().generateQrBitmap(qrData, width = 400, height = 400)
        val retailerLogo = Base64.decode(DeviceConfiguration.get().dashboardDisplayLogo)
        it.onSuccess(
            MerchantQr(
                retailerLogo,
                DeviceConfiguration.get().retailerName!!,
                DeviceConfiguration.get().retailerAddress!!,
                qrBitmap!!
            )
        )
    }
}