package global.citytech.finposframework.utility;

/** User: Surajchhetry Date: 2/14/20 Time: 7:25 AM */
public final class ByteUtils {
  private ByteUtils() {}

  public static byte[] subByte(byte[] data, int startingIndex, int length) {
    byte[] newData = new byte[length];
    if (data.length < (startingIndex + length)) return subByte(data, startingIndex);
    System.arraycopy(data, startingIndex, newData, 0, length);
    return newData;
  }

  public static byte[] subByte(byte[] data, int startingIndex) {
    int length = data.length - startingIndex;
    byte[] newData = new byte[length];
    System.arraycopy(data, startingIndex, newData, 0, length);
    return newData;
  }

  public static byte[] remove(byte[] data, int startingIndex) {
    if (data == null) throw new IllegalArgumentException("parameter is invalid");
    int length = data.length - startingIndex;
    byte[] newData = new byte[length];
    System.arraycopy(data, startingIndex, newData, 0, length);
    return newData;
  }

  public static String toBinary(byte[] bytes) {
    StringBuilder sb = new StringBuilder(bytes.length * Byte.SIZE);
    for (int i = 0; i < Byte.SIZE * bytes.length; i++)
      sb.append((bytes[i / Byte.SIZE] << i % Byte.SIZE & 0x80) == 0 ? '0' : '1');
    return sb.toString();
  }
}
