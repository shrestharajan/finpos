package global.citytech.finpos.merchant.domain.model.tip

import global.citytech.finposframework.hardware.io.led.LedService
import global.citytech.finposframework.hardware.io.printer.PrinterService
import global.citytech.finposframework.repositories.ApplicationRepository
import global.citytech.finposframework.repositories.TransactionRepository
import global.citytech.finposframework.usecases.controllers.DeviceController
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest

data class TipAdjustmentRequestEntity(
    var transactionRequest: TransactionRequest,
    var transactionRepository: TransactionRepository,
    var deviceController: DeviceController,
    var printerService: PrinterService,
    var applicationRepository: ApplicationRepository,
    var ledService: LedService
) {
}