package global.citytech.finpos.merchant.presentation.model

import global.citytech.finpos.merchant.presentation.model.response.Crypto
import global.citytech.finpos.merchant.presentation.model.response.Merchant
import global.citytech.finpos.merchant.presentation.model.response.Terminal
import global.citytech.finpos.merchant.presentation.model.response.TerminalParameter
import global.citytech.finpos.merchant.presentation.model.setting.TerminalSettingResponse
import global.citytech.finpos.nibl.merchant.presentation.model.response.Host
import global.citytech.finpos.nibl.merchant.presentation.model.response.Keys
import global.citytech.finpos.nibl.merchant.presentation.model.response.Logo

data class ConfigResponse(
    var amountLimitPerTransaction: Double? = null,
    var crypto: Crypto? = null,
    var hosts: List<Host>? = null,
    var keys: Keys? = null,
    var logo: Logo? = null,
    var merchant: Merchant? = null,
    var reconcileTime: String? = null,
    var terminal: Terminal? = null,
    var terminalParameter: TerminalParameter? = null,
    var terminalSettings: TerminalSettingResponse?
)