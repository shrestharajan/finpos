package global.citytech.finposframework.comm;

/** User: Surajchhetry Date: 2/20/20 Time: 7:15 PM */
public class Request {
  private final byte[] data;

  public Request(byte[] data) {
    this.data = data;
  }

  public byte[] getData() {
    return data;
  }
}
