package global.citytech.finposframework.iso8583;

/** User: Surajchhetry Date: 2/25/20 Time: 3:21 PM */
public interface MessageParser {
  Iso8583Msg parse(byte[] data);
}
