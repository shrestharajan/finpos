package global.citytech.finposframework.hardware.keymgmt.pin

import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.PinBlock

/**
 * Created by Rishav Chudal on 6/19/20.
 */
data class PinResponse(
    val result: Result,
    val pinBlock: PinBlock?
){
    override fun toString(): String {
        return "PinResponse(result=$result, pinBlock=${pinBlock.toString()})"
    }
}