package global.citytech.finpos.nibl.iso8583.requestsender.authcompletion;

import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.transaction.Request;

/**
 * Created by Unique Shakya on 9/29/2020.
 */
public class AuthorisationCompletionRequest implements Request {

    private String pan;
    private double transactionAmount;
    private String localTransactionTime;
    private String localTransactionDate;
    private String expirationDate;
    private PosEntryMode posEntryMode;
    private String posConditionCode;
    private String originalRrn;
    private String originalApprovalCode;
    private String originalResponseCode;
    private String originalInvoiceNumber;

    public String getPan() {
        return pan;
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public String getLocalTransactionTime() {
        return localTransactionTime;
    }

    public String getLocalTransactionDate() {
        return localTransactionDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public String getOriginalRrn() {
        return originalRrn;
    }

    public String getOriginalApprovalCode() {
        return originalApprovalCode;
    }

    public String getOriginalResponseCode() {
        return originalResponseCode;
    }

    public String getOriginalInvoiceNumber() {
        return originalInvoiceNumber;
    }

    public static final class Builder {
        private String pan;
        private double transactionAmount;
        private String localTransactionTime;
        private String localTransactionDate;
        private String expirationDate;
        private PosEntryMode posEntryMode;
        private String posConditionCode;
        private String originalRrn;
        private String originalApprovalCode;
        private String originalResponseCode;
        private String originalInvoiceNumber;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withPan(String pan) {
            this.pan = pan;
            return this;
        }

        public Builder withTransactionAmount(double transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public Builder withLocalTransactionTime(String localTransactionTime) {
            this.localTransactionTime = localTransactionTime;
            return this;
        }

        public Builder withLocalTransactionDate(String localTransactionDate) {
            this.localTransactionDate = localTransactionDate;
            return this;
        }

        public Builder withExpirationDate(String expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public Builder withPosEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public Builder withPosConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public Builder withOriginalRrn(String originalRrn) {
            this.originalRrn = originalRrn;
            return this;
        }

        public Builder withOriginalApprovalCode(String originalApprovalCode) {
            this.originalApprovalCode = originalApprovalCode;
            return this;
        }

        public Builder withOriginalResponseCode(String originalResponseCode) {
            this.originalResponseCode = originalResponseCode;
            return this;
        }

        public Builder withOriginalInvoiceNumber(String originalInvoiceNumber) {
            this.originalInvoiceNumber = originalInvoiceNumber;
            return this;
        }

        public AuthorisationCompletionRequest build() {
            AuthorisationCompletionRequest authorisationCompletionRequest = new AuthorisationCompletionRequest();
            authorisationCompletionRequest.pan = this.pan;
            authorisationCompletionRequest.posConditionCode = this.posConditionCode;
            authorisationCompletionRequest.originalApprovalCode = this.originalApprovalCode;
            authorisationCompletionRequest.originalRrn = this.originalRrn;
            authorisationCompletionRequest.transactionAmount = this.transactionAmount;
            authorisationCompletionRequest.originalResponseCode = this.originalResponseCode;
            authorisationCompletionRequest.localTransactionTime = this.localTransactionTime;
            authorisationCompletionRequest.localTransactionDate = this.localTransactionDate;
            authorisationCompletionRequest.originalInvoiceNumber = this.originalInvoiceNumber;
            authorisationCompletionRequest.posEntryMode = this.posEntryMode;
            authorisationCompletionRequest.expirationDate = this.expirationDate;
            return authorisationCompletionRequest;
        }
    }
}
