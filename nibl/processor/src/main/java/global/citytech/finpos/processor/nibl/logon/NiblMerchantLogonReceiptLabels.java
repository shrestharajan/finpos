package global.citytech.finpos.processor.nibl.logon;

public class NiblMerchantLogonReceiptLabels {
    private NiblMerchantLogonReceiptLabels() {
    }

    public static final String DIVIDER = "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --";
    public static final String SUCCESS_MESSAGE = "LOGON SUCCESSFUL";
    public static final String FAILURE_MESSAGE = "LOGON FAILURE";
}
