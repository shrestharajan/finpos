package global.citytech.finpos.merchant.framework.datasource.core.balanceinquiry

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.balanceinquiry.BalanceInquiryDataSource
import global.citytech.finpos.merchant.domain.model.balanceinquiry.BalanceInquiryRequestEntity
import global.citytech.finpos.merchant.domain.model.balanceinquiry.BalanceInquiryResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionAuthenticatorImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.*
import global.citytech.finpos.merchant.presentation.data.mapToBalanceInquiryResponseUiModel
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.balanceinquiry.BalanceInquiryRequestItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler
import global.citytech.finposframework.switches.ProcessorManager
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest
import io.reactivex.Observable

class BalanceInquiryDataSourceImpl : BalanceInquiryDataSource {
    override fun generateBalanceInquiryRequest(
        configurationItem: ConfigurationItem,
        balanceInquiryRequestItem: BalanceInquiryRequestItem
    ): Observable<BalanceInquiryResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val transactionRepository =
            TransactionRepositoryImpl(
                NiblMerchant.INSTANCE
            )
        val transactionRequest = prepareTransactionRequest(balanceInquiryRequestItem)

        val deviceController = DeviceControllerImpl(NiblMerchant.INSTANCE)
        val cardReader = CardSourceImpl(NiblMerchant.getActivityContext())
        val transactionAuthenticator =
            TransactionAuthenticatorImpl()
        val printerServiceImpl = PrinterSourceImpl(NiblMerchant.INSTANCE)
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)

        val balanceInquiryRequestEntity = BalanceInquiryRequestEntity(
            readCardService = cardReader,
            deviceController = deviceController,
            printerService = printerServiceImpl,
            transactionAuthenticator = transactionAuthenticator,
            transactionRepository = transactionRepository,
            transactionRequest = transactionRequest,
            applicationRepository = applicationRepository,
            ledService = LedSourceImpl(NiblMerchant.INSTANCE),
            soundService = SoundSourceImpl(NiblMerchant.INSTANCE)
        )

        val balanceInquiryRequester = ProcessorManager.getInterface(
            terminalRepository,
            NotificationHandler
        ).balanceInquiryRequester

        return Observable.fromCallable {
            balanceInquiryRequester.execute(balanceInquiryRequestEntity.mapToModel())
                .mapToBalanceInquiryResponseUiModel()
        }

    }

    private fun prepareTransactionRequest(balanceInquiryRequestItem: BalanceInquiryRequestItem): TransactionRequest {
        val transactionRequest = TransactionRequest(
            balanceInquiryRequestItem.transactionType,
        )
        transactionRequest.amount = balanceInquiryRequestItem.amount
        return transactionRequest
    }
}