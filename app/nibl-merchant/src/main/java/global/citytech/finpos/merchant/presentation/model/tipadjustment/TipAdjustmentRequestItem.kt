package global.citytech.finpos.merchant.presentation.model.tipadjustment

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

/**
 * Created by Unique Shakya on 5/4/2021.
 */
data class TipAdjustmentRequestItem(
    val originalAmount: BigDecimal,
    val tipAmount: BigDecimal,
    val transactionType: TransactionType,
    val originalInvoiceNumber: String
) {
}