package global.citytech.finpos.weipass.integration.test

import android.content.Context
import android.os.Build
import android.util.Log
import global.citytech.finposframework.hardware.common.DeviceService
import java.util.*

/**
 * Created by Rishav Chudal on 4/20/20.
 */
class AppDeviceFactory {
    companion object {
        fun getCore(context: Context, coreServiceType: global.citytech.finposframework.hardware.DeviceServiceType): DeviceService {
            val manufacturer = getDeviceManufacturer()
            val className: String = "global.citytech.finpos.device."
                .plus(manufacturer.toLowerCase(Locale.ROOT))
                .plus(".Device")
                .plus(manufacturer)
                .plus("Factory")

            try {
                val clazz: Class<*> = Class.forName(className)
                val deviceFactory: global.citytech.finposframework.hardware.DeviceFactory = clazz.newInstance() as global.citytech.finposframework.hardware.DeviceFactory
                return deviceFactory.getDevice(context, coreServiceType, TestNotificationHandler())!!
            } catch (ex: Exception) {
                Log.i("corelogger", ex.message!!)
            }
            throw NullPointerException("Core is null")
        }

        private fun getDeviceManufacturer(): String {
            val manufacturer: String = Build.MANUFACTURER.toLowerCase(Locale.ROOT);
            Log.i("corelogger", "Manufacturer ::: ".plus(manufacturer))
            manufacturer.replace(" ", "")
            return manufacturer.substring(0,1).toUpperCase(Locale.ROOT)
                .plus(manufacturer.substring(1))
        }


    }
}