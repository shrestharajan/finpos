package global.citytech.finpos.merchant.presentation.transactions.mobile

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import global.citytech.common.Constants
import global.citytech.common.extensions.hideKeyboard
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.domain.model.app.MobileNfcPayment
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.INITIATOR_MOBILE
import global.citytech.finpos.merchant.presentation.base.AppBaseFragment
import global.citytech.finpos.merchant.presentation.transactions.TransactionsViewModel
import global.citytech.finposframework.log.Logger
import kotlinx.android.synthetic.main.fragment_mobile_nfc_transactions.*
import java.util.*

/**
 * @author SIDDHARTHA GHIMIRE
 */
class MobileNfcTransactionFragment : AppBaseFragment(), MobileNfcTransactionAdapter.Listener {
    override fun getLayoutId(): Int = R.layout.fragment_mobile_nfc_transactions

    private val logger: Logger = Logger.getLogger(MobileNfcTransactionFragment::class.java.name)
    private var mobileNfcPayments = mutableListOf<MobileNfcPayment>()
    private val adapter = MobileNfcTransactionAdapter(mutableListOf(), this)
    lateinit var textWatcher: TextWatcher
    lateinit var onEditActionListener: TextView.OnEditorActionListener
    var pageNumber = 1
    var searchPageNumber = 1
    lateinit var scrollListener: RecyclerView.OnScrollListener
    var transactionsCount = 0
    var searchTransactionsCount = 0
    val pageSize = Constants.DEFAULT_PAGE_SIZE
    var searchParam = ""
    var alreadySwipedForRefresh = false

    companion object {

        private var instance: MobileNfcTransactionFragment? = null

        fun getInstance(): MobileNfcTransactionFragment {
            if (instance == null)
                instance = MobileNfcTransactionFragment()
            return instance as MobileNfcTransactionFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel().getConfigurationForMobilePay()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
        hideSoftKeyboard(requireActivity(),requireView())
    }

    override fun onResume() {
        super.onResume()
        et_reference_number.text?.clear()
        activity?.hideKeyboard(et_reference_number)
        et_reference_number.isFocusable = false
        pageNumber = 1
        getViewModel().appConfigurationForMobilePay.observe(this) {
            if (it.merchants?.get(0) != null) {
                getViewModel().getTransactionsEitherQrOrMobilePay(
                    pageNumber,
                    pageSize,
                    false,
                    INITIATOR_MOBILE
                )
            }
        }
        println("::: MOBILE PAY TRANSACTION FRAGMENT >>> ON RESUME CALLED <<<")
    }

    override fun onPause() {
        et_reference_number.isFocusable = false
        activity?.hideKeyboard(et_reference_number)
        println("::: MOBILE PAY TRANSACTION FRAGMENT >>> ON PAUSE CALLED <<<")
        super.onPause()
    }

    private fun initObservers() {
        getViewModel().transactionAvailableEitherQrOrMobilePay.observe(viewLifecycleOwner) {
            swipe_refresh_layout_fragment_qr.isRefreshing = false
            alreadySwipedForRefresh = false
            if (it) {
                et_reference_number.visibility = View.VISIBLE
                tv_no_txn.visibility = View.INVISIBLE
                cv_rv_wrapper.visibility = View.VISIBLE
                rv_transactions.visibility - View.VISIBLE
            } else {
                et_reference_number.visibility = View.GONE
                tv_no_txn.visibility = View.VISIBLE
                cv_rv_wrapper.visibility = View.INVISIBLE
                rv_transactions.visibility - View.INVISIBLE
            }
        }

        getViewModel().noInternet.observe(viewLifecycleOwner){
            if (it){
                tv_no_txn.text  = getString(R.string.no_internet_transactions)
            }else{
                tv_no_txn.text  = getString(R.string.title_no_txns)
            }
        }

        getViewModel().paymentsEitherQrOrMobilePay.observe(viewLifecycleOwner) {
            if (isResumed) {
                this.mobileNfcPayments = Gson().fromJson(Gson().toJson(it),
                    object : TypeToken<List<MobileNfcPayment>>() {}.type
                )
                adapter.updateAll(mobileNfcPayments)
            }
        }

        getViewModel().isPaginatingEitherQrOrMobilePay.observe(viewLifecycleOwner) {
            if (it) {
                iv_progress_pagination.visibility = View.VISIBLE
            } else {
                iv_progress_pagination.visibility = View.GONE
            }
        }

        getViewModel().paymentCountEitherQrOrMobilePay.observe(viewLifecycleOwner) {
            transactionsCount = it
        }

        getViewModel().searchedPaymentCountEitherQrOrMobilePay.observe(viewLifecycleOwner) {
            searchTransactionsCount = it
        }

        getViewModel().isLoading.observe(viewLifecycleOwner) {
            if (it) {
                showProgress()
            } else {
                hideProgress()
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initViews() {
        swipe_refresh_layout_fragment_qr.setOnRefreshListener {
            if (alreadySwipedForRefresh) {
                logger.log("Already Swipe Refresh Layout Refreshing...")
                return@setOnRefreshListener
            }
            alreadySwipedForRefresh = true
            pageNumber = 1
            activity?.hideKeyboard(et_reference_number)
            et_reference_number.text?.clear()
            et_reference_number.isFocusable = false
            getViewModel().getTransactionsEitherQrOrMobilePay(
                pageNumber,
                pageSize,
                true,
                INITIATOR_MOBILE
            )
        }

        textWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // do nothing
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchParam = s.toString().lowercase()
                if ((s.toString()).isEmpty()) {
                    pageNumber = 1
                    logger.log("getMobilePayTransactions from text change listener")
                    getViewModel().getTransactionsEitherQrOrMobilePay(
                        pageNumber,
                        pageSize,
                        true,
                        INITIATOR_MOBILE
                    )
                    return
                } else {
                    searchPageNumber = 1
                    pageNumber = 1
                    val searchedList = mobileNfcPayments.filter { it.referenceNumber.lowercase().contains(searchParam) }
                    adapter.update(searchedList.reversed().toMutableList())
                }
            }

            override fun afterTextChanged(s: Editable?) {
                // do nothing
            }
        }

        onEditActionListener = TextView.OnEditorActionListener { v, actionId, event ->
            if ((event != null && (event.keyCode == KeyEvent.KEYCODE_NUMPAD_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                activity?.hideKeyboard(et_reference_number)
            }
            false;
        }

        Glide.with(this).load("file:///android_asset/images/loading.gif")
            .into(iv_progress_pagination)

        et_reference_number.setOnEditorActionListener(onEditActionListener)
        et_reference_number.addTextChangedListener(textWatcher)

        et_reference_number.setOnTouchListener { view, motionEvent ->
            et_reference_number.isFocusableInTouchMode = true
            val drawableRight = 2
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                if (motionEvent.rawX >= (et_reference_number.right - et_reference_number.compoundDrawables[drawableRight].bounds.width())) {
                    activity?.hideKeyboard(et_reference_number)
                }
            }
            false
        }

        val layoutManager = LinearLayoutManager(activity)
        rv_transactions.layoutManager = layoutManager
        scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) { //check for scroll down
                    val visibleItemCount = layoutManager.childCount;
                    val totalItemCount = layoutManager.itemCount;
                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
                    var availablePages = transactionsCount / pageSize
                    var availableSearchPages = searchTransactionsCount / pageSize
                    if (transactionsCount % pageSize != 0) {
                        availablePages++
                    }
                    if (searchTransactionsCount % pageSize != 0) {
                        availableSearchPages++
                    }
                    activity?.hideKeyboard(et_reference_number)
                    et_reference_number.isFocusable = false

                    if (et_reference_number.text.toString().isEmpty()) {
                        if (((visibleItemCount + pastVisiblesItems) >= (totalItemCount - 5)) && !(getViewModel().isPaginatingEitherQrOrMobilePay.value)!! && (++pageNumber <= availablePages)) {
                            logger.log("getMobilePayTransactions from on scroll listener")
                            getViewModel().getTransactionsEitherQrOrMobilePay(
                                pageNumber,
                                pageSize,
                                initiator = INITIATOR_MOBILE
                            )
                        }
                    } else {
                        if (((visibleItemCount + pastVisiblesItems) >= (totalItemCount - 5)) && !(getViewModel().isPaginatingEitherQrOrMobilePay.value)!! && (++searchPageNumber <= availableSearchPages)) {
                            getViewModel().searchTransactionsEitherQrOrMobilePay(
                                searchParam = searchParam,
                                pageNumber = searchPageNumber,
                                pageSize = pageSize,
                                initiator = INITIATOR_MOBILE
                            )
                        }
                    }
                }
            }

        }
        rv_transactions.addOnScrollListener(scrollListener)
        rv_transactions.adapter = adapter
    }

    fun getViewModel() = ViewModelProviders.of(requireActivity())[TransactionsViewModel::class.java]

    override fun onDestroyView() {
        et_reference_number.removeTextChangedListener(textWatcher)
        et_reference_number.setOnEditorActionListener(null)
        rv_transactions.removeOnScrollListener(scrollListener)
        rv_transactions.adapter = null
        super.onDestroyView()
    }

    override fun onItemClicked(mobileNfcPayment: MobileNfcPayment) {
        val intent =
            MobileNfcTransactionDetailActivity.getLaunchIntent(requireActivity(), mobileNfcPayment)
        startActivity(intent)
    }
}