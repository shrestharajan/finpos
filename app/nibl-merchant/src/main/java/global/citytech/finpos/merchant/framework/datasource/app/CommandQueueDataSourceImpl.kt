package global.citytech.finpos.merchant.framework.datasource.app

import android.content.Context
import global.citytech.easydroid.core.preference.SecurePreference
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.data.datasource.app.commands.CommandQueueDataSource
import global.citytech.finpos.merchant.domain.model.app.Command
import global.citytech.finpos.merchant.domain.model.app.Status
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.utility.StringUtils
import java.util.*

/**
 * Created by Unique Shakya on 9/20/2021.
 */
class CommandQueueDataSourceImpl(context: Context) : CommandQueueDataSource {

    private var securePreference: SecurePreference
    private val logger = Logger.getLogger(CommandQueueDataSourceImpl::class.java.name)

    companion object {
        const val PREFERENCE_NAME = "PREF-COMMAND"
        const val PREFERENCE_KEY = "KEY-COMMAND-QUEUE"
    }

    init {
        securePreference = SecurePreference(context.applicationContext, PREFERENCE_NAME)
    }

    override fun isQueueEmpty(): Boolean = getCount() == 0

    override fun hasActiveCommands(): Boolean {
        logger.log("::: CHECKING FOR ACTIVE COMMANDS :::")
        val commands = getAllCommands()
        if (commands.isNotEmpty()) {
            commands.forEach {
                if (it.status == Status.OPEN)
                    return true
            }
        }
        return false
    }

    override fun getCount(): Int = getAllCommands().size

    override fun getAllCommands(): MutableList<Command> {
        logger.log("::: GETTING ALL COMMANDS :::")
        val data = getAsString()
        data?.let {
            val commands = Jsons.fromJsonToList<Command>(data, Array<Command>::class.java)
                .toMutableList()
            commands.sortBy { it.timeStamp }
            return commands
        }
        return mutableListOf()
    }

    override fun removeCommand(command: Command) {
        logger.log("::: REMOVING COMMAND ::: $command")
        val list = getAllCommands()
        if (list.isNotEmpty()) {
            val copyList = copy(list)
            copyList.remove(command)
            update(copyList)
        }
    }

    override fun addCommand(command: Command) {
        logger.log("::: ADDING COMMAND ::: $command")
        val list = getAllCommands()
        val copyList = mutableListOf<Command>()
        list.forEach {
            when {
                it.id == command.id -> {
                    return
                }
                it.commandType == command.commandType -> {
                    if (it.status == Status.OPEN) {
                        command.status = Status.FAILED
                        command.remarks = "Duplicate Command"
                    }
                    copyList.add(it)
                }
                else -> {
                    copyList.add(it)
                }
            }
        }
        copyList.add(command)
        copyList.sortWith(CommandListComparator())
        update(copyList)
    }

    override fun updateCommandStatus(command: Command, status: Status, remarks: String) {
        logger.log("::: UPDATING COMMAND >> $command WITH STATUS >> $status AND REMARKS >> $remarks")
        val list = getAllCommands()
        if (list.isNotEmpty()) {
            val copyList = copy(list)
            list.forEach {
                if (it.id == command.id) {
                    copyList.remove(it)
                    copyList.add(
                        Command(
                            id = it.id,
                            commandType = it.commandType,
                            status = status,
                            timeStamp = StringUtils.dateTimeStamp(),
                            remarks = remarks
                        )
                    )
                }
            }
            update(copyList)
        }
    }

    override fun clear() {
        securePreference.clear()
    }

    private fun copy(list: MutableList<Command>): MutableList<Command> {
        val copyList = mutableListOf<Command>()
        list.forEach {
            copyList.add(it)
        }
        return copyList.toMutableList()
    }

    private fun getAsString(): String? {
        val data = securePreference.retrieveData(PREFERENCE_KEY, null)
        logger.log("::: GETTING COMMANDS ::: $data")
        return data
    }

    private fun update(list: MutableList<Command>) {
        val data = Jsons.toJsonList(list)
        logger.log("::: UPDATING COMMANDS ::: $data")
        securePreference.saveData(PREFERENCE_KEY, data)
    }

    internal class CommandListComparator : Comparator<Command> {
        override fun compare(p0: Command?, p1: Command?): Int {
            val timeStamp1 = p0!!.timeStamp.toLong()
            val timeStamp2 = p1!!.timeStamp.toLong()
            return (timeStamp2 - timeStamp1).toInt()
        }
    }
}