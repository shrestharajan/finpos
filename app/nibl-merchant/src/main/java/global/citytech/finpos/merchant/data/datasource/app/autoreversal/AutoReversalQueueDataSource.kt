package global.citytech.finpos.merchant.data.datasource.app.autoreversal

import global.citytech.finposframework.usecases.transaction.data.AutoReversal

/**
 * Created by Unique Shakya on 6/22/2021.
 */
interface AutoReversalQueueDataSource {

    fun isQueueEmpty(): Boolean

    fun isActive(): Boolean

    fun getSize(): Long

    fun getAllQueueRequests(): MutableList<AutoReversal>

    fun getRecentRequest(): AutoReversal?

    fun removeRequest(autoReversal: AutoReversal)

    fun addRequest(autoReversal: AutoReversal)

    fun incrementRetryCount(autoReversal: AutoReversal)

    fun changeStatus(autoReversal: AutoReversal, status: AutoReversal.Status)
    
    fun clear()
}