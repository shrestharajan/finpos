package global.citytech.finpos.merchant.utils

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.BatteryManager
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import global.citytech.common.Base64
import global.citytech.common.extensions.hasInternetConnection
import global.citytech.common.extensions.isNullOrEmptyOrBlank
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.domain.utils.Currency
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.presentation.admin.AdminViewModel
import global.citytech.finpos.merchant.presentation.model.mobile.list.AidParameterResponse
import global.citytech.finpos.merchant.presentation.model.setting.PushNotificationConfiguration
import global.citytech.finposframework.log.Logger
import kotlinx.android.synthetic.main.activity_base_transaction.*
import java.net.InetAddress
import java.net.NetworkInterface
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.*

object AppUtility {
    fun prepareQrBase64String(plainText: String): String {
        val byte = plainText.toByteArray(charset("UTF-8"))
        return Base64.encodeToString(byte, false)
    }

    fun prepareQrUrlString(base64String: String): String {
        return NiblMerchant.INSTANCE.iPlatformManager.hostDomain + ApiConstant.CUSTOMER_RECEIPT_URL + base64String
    }

    fun isTransactionApprovedAndDeclined(transactionStatus: String): Boolean {
        return transactionStatus == "APPROVED" || transactionStatus == "DECLINED"
    }

    fun savePushNotificationConfiguration(pushNotificationConfiguration: PushNotificationConfiguration) {
        if (pushNotificationConfiguration != null) {
            NiblMerchant.INSTANCE.iPlatformManager.savePushNotificationConfiguration(
                Jsons.toJsonObj(pushNotificationConfiguration)
            )
            handleMqttConnection(getPushNotificationConfiguration())

        }
    }

    fun getPushNotificationConfiguration(): PushNotificationConfiguration {
        val pushNotificationConfiguration =
            NiblMerchant.INSTANCE.iPlatformManager.pushNotificationConfiguration
        pushNotificationConfiguration?.let {
            return Jsons.fromJsonToObj(
                NiblMerchant.INSTANCE.iPlatformManager.pushNotificationConfiguration,
                PushNotificationConfiguration::class.java
            )
        }
        return PushNotificationConfiguration()
    }

    fun isMqttUrlAvailable(pushNotificationConfiguration: PushNotificationConfiguration): Boolean {
        if (!pushNotificationConfiguration.ip.isNullOrEmptyOrBlank() &&
            !pushNotificationConfiguration.port.isNullOrEmptyOrBlank()
        ) {
            return true
        }
        return false
    }

    fun handleMqttConnection(pushNotificationConfiguration: PushNotificationConfiguration) {
        if (!NiblMerchant.INSTANCE.mqttClient.isConnected()) {
            NiblMerchant.INSTANCE.mqttClient.connectToFinPosMqttServer()
        } else {
            NiblMerchant.INSTANCE.mqttClient.subscribeToFinPosCashierAppTopic(
                pushNotificationConfiguration.ip!!.trim().plus(":")
                    .plus(pushNotificationConfiguration.port!!.trim())
            )
        }

    }

    fun sendAppResetBroadcastToTMSPlatform(context: Context) {
        //val intent = Intent("android.intent.action.application.updated")
        val intent = Intent("global.citytech.tms.RESET.ACKNOWLEDGEMENT.APPLICATION")
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
        intent.setPackage("global.citytech.tms")
        context.sendBroadcast(intent)
        Log.d("AppUtility", "BROADCASTING RESET APPLICATION >> ${intent.action}")
    }


    fun printLog(tag: String, message: String, debug: Boolean = false) {
        if (debug) {
            Logger.getLogger(tag).debug(message)
        } else {
            Logger.getLogger(tag).log(" >>>> $message")
        }
    }

    fun getNetworkType(context: Context): String {

        // ConnectionManager instance
        val mConnectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val mInfo = mConnectivityManager.activeNetworkInfo

        // If not connected, "-" will be displayed
        if (mInfo == null || !mInfo.isConnected) return "-"

        // If Connected to Wifi
        if (mInfo.type == ConnectivityManager.TYPE_WIFI) return "WIFI"

        // If Connected to Mobile
        if (mInfo.type == ConnectivityManager.TYPE_MOBILE) {
            return when (mInfo.subtype) {
                TelephonyManager.NETWORK_TYPE_GPRS,
                TelephonyManager.NETWORK_TYPE_EDGE,
                TelephonyManager.NETWORK_TYPE_CDMA,
                TelephonyManager.NETWORK_TYPE_1xRTT,
                TelephonyManager.NETWORK_TYPE_IDEN,
                TelephonyManager.NETWORK_TYPE_GSM -> "MOBILE DATA - " + "2G"
                TelephonyManager.NETWORK_TYPE_UMTS,
                TelephonyManager.NETWORK_TYPE_EVDO_0,
                TelephonyManager.NETWORK_TYPE_EVDO_A,
                TelephonyManager.NETWORK_TYPE_HSDPA,
                TelephonyManager.NETWORK_TYPE_HSUPA,
                TelephonyManager.NETWORK_TYPE_HSPA,
                TelephonyManager.NETWORK_TYPE_EVDO_B,
                TelephonyManager.NETWORK_TYPE_EHRPD,
                TelephonyManager.NETWORK_TYPE_HSPAP,
                TelephonyManager.NETWORK_TYPE_TD_SCDMA -> "MOBILE DATA - " + "3G"
                TelephonyManager.NETWORK_TYPE_LTE,
                TelephonyManager.NETWORK_TYPE_IWLAN, 19 -> "MOBILE DATA - " + "4G"
                TelephonyManager.NETWORK_TYPE_NR -> "MOBILE DATA - " + "5G"
                else -> "?"
            }
        }
        return "?"
    }

    fun getIPAddress(useIPv4: Boolean): String {
        try {
            val networkInterfaces: List<NetworkInterface> =
                Collections.list(NetworkInterface.getNetworkInterfaces())
            for (networkInterface in networkInterfaces) {
                val inetAddresses: List<InetAddress> =
                    Collections.list(networkInterface.inetAddresses)
                for (inetAddress in inetAddresses) {
                    if (!inetAddress.isLoopbackAddress) {
                        val hostAddress: String = inetAddress.hostAddress
                        val isIPv4 = hostAddress.indexOf(':') < 0
                        if (useIPv4) {
                            if (isIPv4) return hostAddress
                        } else {
                            if (!isIPv4) {
                                val delim = hostAddress.indexOf('%') // drop ip6 zone suffix
                                return if (delim < 0) hostAddress.uppercase(Locale.getDefault()) else hostAddress.substring(
                                    0,
                                    delim
                                ).uppercase(
                                    Locale.getDefault()
                                )
                            }
                        }
                    }
                }
            }
        } catch (ignored: Exception) {
        }
        return ""
    }

    fun currentDateTime(): String {
        return SimpleDateFormat("yyyy:MM:dd:HH:mm:ssSSS").format(Date())
    }


    fun getAidList(): List<String> {
        val aidResponse =  PreferenceManager.getAidResponse("")
        if(!aidResponse.isNullOrEmptyOrBlank() && aidResponse != "null"){
            val response = Jsons.fromJsonToObj(
                PreferenceManager.getAidResponse(""),
                AidParameterResponse::class.java
            )
            return response.aids
        }
        return arrayListOf()
    }

    fun isLcRequiredForNfcData(aid: String): Boolean {
        val response = Jsons.fromJsonToObj(
            PreferenceManager.getAidResponse(""),
            AidParameterResponse::class.java
        )

        // Specify the aid for which you want to find the provider mapping and lcRequired value
        val desiredAid = aid

        val providerMapping = response.providerMapping[desiredAid]
        val lcRequired = response.networkConfig[providerMapping]?.lcRequired

        return if (providerMapping != null && lcRequired != null) {
            Log.d("AppUtility", "Provider Mapping for $desiredAid: $providerMapping")
            Log.d("AppUtility", "lcRequired for $desiredAid: $lcRequired")
            lcRequired
        } else {
            Log.d("AppUtility", "No Provider Mapping or lcRequired found for $desiredAid")
            false
        }
    }

    fun isAppInstalled(packageName: String, context: Context): Boolean {
        Log.d("AppUtility", "PackageName >>> ".plus(packageName))
        val pm = context.packageManager
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
            return true
        } catch (e: PackageManager.NameNotFoundException) {
        }
        return false
    }


    fun checkInternetConnection(context: Application ,container: LinearLayout): Boolean {
        if (!context.hasInternetConnection()) {
            if (isAirplaneModeOn(context)) {
                Snackbar.make(
                    container,
                    context.getString(R.string.turn_off_airplane),
                    Snackbar.LENGTH_LONG
                ).show()
            } else {
                Snackbar.make(
                    container,
                    context.getString(R.string.msg_turn_on_data_or_wifi),
                    Snackbar.LENGTH_LONG
                ).show()
            }
            return false
        } else {
            return true
        }
    }

    fun checkBatteryLevel(batLevel:Int = 0,context: Application ,container: LinearLayout,isCharging:Boolean=false): Boolean {
        return if (batLevel < 7) {
            if(isCharging){
                true
            }else{
                Snackbar.make(
                    container,
                    context.getString(R.string.msg_battery_low_nfc_desc),
                    Snackbar.LENGTH_LONG
                ).show()
                false
            }

        } else {
            true
        }
    }

    fun isAppVariantNIBL(): Boolean {
        return BuildConfig.FLAVOR == "NIBL"
    }

    fun getCurrencyCode():String {
        if (DeviceConfiguration.get().currencyCode == "524")
            return "NPR"
        // else if (DeviceConfiguration.get().currencyCode == "840")
        else
            return "USD"
    }

    fun checkCurrencyCode(currencyCode: String): String {
        val result = when (currencyCode) {
            Currency.NPR.getCurrencyCode() -> Currency.NPR.getCurrencyName()
            Currency.USD.getCurrencyCode() -> Currency.USD.getCurrencyName()
            else -> Currency.NPR.getCurrencyName()

        }
        return result
    }

}