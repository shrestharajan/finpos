package global.citytech.finposframework.utility;

public class ServiceCodeUtils {

    public static boolean isPinRequired(String trackTwoData) {
        String fieldSeparator = retrieveFieldSeparator(trackTwoData);
        String[] fieldSeparatorSeparated = trackTwoData.split(fieldSeparator);
        String serviceCodeThirdDigit = fieldSeparatorSeparated[1].substring(6, 7);
        return serviceCodeThirdDigit.equals("0") || serviceCodeThirdDigit.equals("3")
                || serviceCodeThirdDigit.equals("5") || serviceCodeThirdDigit.equals("6") ||
                serviceCodeThirdDigit.equals("7");
    }

    public static boolean isOfflineTransactionAllowed(String trackTwoData) {
        if (StringUtils.isEmpty(trackTwoData))
            return false;
        String fieldSeparator = retrieveFieldSeparator(trackTwoData);
        String[] fieldSeparatorSeparated = trackTwoData.split(fieldSeparator);
        String serviceCode = fieldSeparatorSeparated[1].substring(4, 7);
        return serviceCode.equals("101") || serviceCode.equals("102") || serviceCode.equals("104");
    }

    public static boolean isCashTransactionAllowed(String trackTwoData) {
        if (StringUtils.isEmpty(trackTwoData))
            return false;
        String fieldSeparator = retrieveFieldSeparator(trackTwoData);
        String[] fieldSeparatorSeparated = trackTwoData.split(fieldSeparator);
        String serviceCode = fieldSeparatorSeparated[1].substring(4, 7);
        String thirdDigit = String.valueOf(serviceCode.charAt(2));
        return thirdDigit.equalsIgnoreCase("0") || thirdDigit.equalsIgnoreCase("1")
                || thirdDigit.equalsIgnoreCase("4") || thirdDigit.equalsIgnoreCase("6");
    }

    public static boolean isPurchaseAndServicesAllowed(String trackTwoData) {
        if (StringUtils.isEmpty(trackTwoData))
            return false;
        String fieldSeparator = retrieveFieldSeparator(trackTwoData);
        String[] fieldSeparatorSeparated = trackTwoData.split(fieldSeparator);
        String serviceCode = fieldSeparatorSeparated[1].substring(4, 7);
        String thirdDigit = String.valueOf(serviceCode.charAt(2));
        return thirdDigit.equalsIgnoreCase("0") || thirdDigit.equalsIgnoreCase("1")
                || thirdDigit.equalsIgnoreCase("2") || thirdDigit.equalsIgnoreCase("5")
                || thirdDigit.equalsIgnoreCase("6") || thirdDigit.equalsIgnoreCase("7");
    }

    private static String retrieveFieldSeparator(String trackTwoData) {
        if (trackTwoData.contains("D"))
            return "D";
        else
            return "=";
    }

    public static boolean doesContainChipCard(String trackTwoData) {
        String fieldSeparator = retrieveFieldSeparator(trackTwoData);
        String[] fieldSeparatorSeparated = trackTwoData.split(fieldSeparator);
        String serviceCode = fieldSeparatorSeparated[1].substring(4, 7);
        return serviceCode.startsWith("2") || serviceCode.startsWith("6");
    }
}