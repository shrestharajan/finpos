package global.citytech.finpos.device.qualcomm.io.cards.detect

import com.ftpos.library.smartpos.emv.TrackData
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.CardType
import global.citytech.finposframework.hardware.io.cards.detect.DetectCardResponse

/**
 * Created by Rishav Chudal on 4/7/21.
 */
class FeitianDetectCardResponse(
    result: Result,
    message: String,
    cardDetected: CardType
): DetectCardResponse(result, message, cardDetected) {
    var trackData: TrackData?= null
}