package global.citytech.finposframework.hardware.emv.aid

import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest

/**
 * Created by Rishav Chudal on 5/3/20.
 */
data class AidInjectionRequest (val aidParametersList: List<AidParameters>,
                                val emvParametersRequest: EmvParametersRequest,
                                val supportedTransactionTypes: List<String>
)