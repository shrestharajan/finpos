package global.citytech.finpos.merchant.framework.datasource.device

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import global.citytech.common.Constants
import global.citytech.finpos.merchant.data.datasource.device.PrinterSource
import global.citytech.finpos.merchant.service.AppDeviceFactory
import global.citytech.finposframework.hardware.DeviceServiceType
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.printer.PrinterRequest
import global.citytech.finposframework.hardware.io.printer.PrinterResponse
import global.citytech.finposframework.hardware.io.printer.PrinterService
import global.citytech.finposframework.log.Logger
import java.lang.ref.WeakReference
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * Created by Rishav Chudal on 8/27/20.
 * Modified by Unique Shakya on 09/01/2020.
 */
class PrinterSourceImpl(instance: Context?, private val provideOptionsToReprint: Boolean = true) :
    PrinterSource {

    private var countDownLatch: CountDownLatch = CountDownLatch(1)
    private var result: Result = Result.FAILURE
    private var message: String? = ""
    private var reprint: Boolean = false
    private var fromBroadCast: Boolean = false
    private var timeOut: Long = 30;
    private val contextWeakReference = WeakReference(instance)
    private val logger = Logger.getLogger(PrinterSourceImpl::class.java.name)

    private var service: PrinterService =
        AppDeviceFactory.getCore(
            instance,
            DeviceServiceType.PRINTER
        ) as PrinterService

    private var broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent!!.action.equals(Constants.INTENT_ACTION_REPRINT_SERVICE)) {
                fromBroadCast = true
                reprint = intent.getBooleanExtra(Constants.KEY_BROADCAST_EXTRA_REPRINT, true)
                if (!reprint) {
                    result = Result.CANCEL
                }
                countDownLatch.countDown()
            } else {
                fromBroadCast = false
                reprint = false
                result = Result.FAILURE
                message = "FAILURE"
                countDownLatch.countDown()
            }
        }
    }

    override fun print(printerRequest: PrinterRequest?): PrinterResponse {
        this.contextWeakReference.get()?.let {
            LocalBroadcastManager.getInstance(it)
                .registerReceiver(
                    broadcastReceiver,
                    IntentFilter(Constants.INTENT_ACTION_REPRINT_SERVICE)
                )
        }
        val printerResponse = service.print(printerRequest)
        if (printerResponse.result == Result.SUCCESS) {
            result = Result.SUCCESS
            message = printerResponse.message
        } else if (provideOptionsToReprint) {
            result = Result.FAILURE
            message = printerResponse.message
            logger.log("::: PRINTER RESPONSE MESSAGE $message")
            this.sendBroadCast(message!!)
            try {
                this.countDownLatch.await(this.timeOut, TimeUnit.SECONDS)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            if (this.reprint)
                return this.reprintTask(printerRequest)
        }
        this.cleanUp()
        return PrinterResponse(
            result,
            message!!
        )
    }

    private fun cleanUp() {
        LocalBroadcastManager.getInstance(contextWeakReference.get()!!)
            .unregisterReceiver(broadcastReceiver)
    }

    private fun reprintTask(printerRequest: PrinterRequest?): PrinterResponse {
        val broadCast = this.fromBroadCast
        this.countDownLatch = CountDownLatch(1)
        val printerResponse = this.service.print(printerRequest)
        if (printerResponse.result == Result.SUCCESS) {
            result = Result.SUCCESS
            message = printerResponse.message
        } else {
            result = Result.FAILURE
            message = printerResponse.message
            logger.log("::: PRINTER RESPONSE MESSAGE $message")
            this.sendBroadCast(message!!)
            try {
                this.countDownLatch.await(this.timeOut, TimeUnit.SECONDS)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            if (broadCast && reprint)
                return this.reprintTask(printerRequest)
            else
                return PrinterResponse(
                    result,
                    message!!
                )
        }
        return PrinterResponse(
            result,
            message!!
        )
    }

    private fun sendBroadCast(displayMessage: String) {
        this.reprint = false
        this.fromBroadCast = false
        val intent = Intent(Constants.INTENT_ACTION_REPRINT_UI)
        intent.putExtra(Constants.KEY_REPRINT_MESSAGE, displayMessage)
        contextWeakReference.get()
            ?.let { LocalBroadcastManager.getInstance(it).sendBroadcast(intent) }
    }

    override fun feedPaper(numberOfEmptyLines: Int) {
        try {
            this.service.feedPaper(numberOfEmptyLines)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}