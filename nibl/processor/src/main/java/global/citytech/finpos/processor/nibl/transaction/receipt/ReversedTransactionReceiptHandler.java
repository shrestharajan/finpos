package global.citytech.finpos.processor.nibl.transaction.receipt;

import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.EmvTags;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.TransactionInfo;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Unique Shakya on 10/5/2020.
 */
public class ReversedTransactionReceiptHandler {

    private TransactionRequest transactionRequest;
    private ReadCardResponse readCardResponse;
    private TerminalRepository terminalRepository;

    public ReversedTransactionReceiptHandler(TransactionRequest transactionRequest,
                                             ReadCardResponse readCardResponse,
                                             TerminalRepository terminalRepository) {
        this.transactionRequest = transactionRequest;
        this.readCardResponse = readCardResponse;
        this.terminalRepository = terminalRepository;
    }

    public ReceiptLog prepare(String batchNumber, String stan, String invoiceNumber,
                              ReasonForReversal reasonForReversal) {
        return new ReceiptLog.Builder()
                .withRetailer(prepareRetailerInfo())
                .withPerformance(preparePerformanceInfo())
                .withTransaction(prepareTransactionInfo(batchNumber, stan, invoiceNumber, reasonForReversal))
                .withEmvTags(prepareEmvTags())
                .build();
    }

    private Retailer prepareRetailerInfo() {
        return new Retailer.Builder()
                .withRetailerLogo(this.terminalRepository.findTerminalInfo().getMerchantPrintLogo())
                .withRetailerName(this.terminalRepository.findTerminalInfo().getMerchantName())
                .withRetailerAddress(this.terminalRepository.findTerminalInfo().getMerchantAddress())
                .withMerchantId(this.terminalRepository.findTerminalInfo().getMerchantID())
                .withTerminalId(this.terminalRepository.findTerminalInfo().getTerminalID())
                .build();
    }

    private Performance preparePerformanceInfo() {
        return new Performance.Builder()
                .withStartDateTime(
                        this.readCardResponse
                                .getCardDetails()
                                .getTransactionInitializeDateTime()
                )
                .build();
    }

    private TransactionInfo prepareTransactionInfo(String batchNumber, String stan, String invoiceNumber,
                                                   ReasonForReversal reasonForReversal) {
        return new TransactionInfo.Builder()
                .withApprovalCode("")
                .withTransactionCurrencyName(this.terminalRepository.getTerminalTransactionCurrencyName())
                .withBatchNumber(batchNumber)
                .withCardHolderName(!StringUtils.isEmpty(readCardResponse.getCardDetails().getCardHolderName())?readCardResponse.getCardDetails().getCardHolderName().trim():"")
                .withCardNumber(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .withCardScheme(readCardResponse.getCardDetails().getCardSchemeLabel())
                .withExpireDate(readCardResponse.getCardDetails().getExpiryDate())
                .withPurchaseAmount(this.retrieveAmount())
                .withBaseAmount(this.retrieveBaseAmount())
                .withPurchaseType(this.transactionRequest.getTransactionType().getPrintName())
                .withRrn("")
                .withStan(stan)
                .withInvoiceNumber(invoiceNumber)
                .withTransactionMessage(reasonForReversal.getMessage())
                .withSignatureRequired(false)
                .withDebitAcknowledgementRequired(false)
                .withTransactionStatus("DECLINED")
                .build();
    }

    private String retrieveBaseAmount() {
        if (this.transactionRequest.getTransactionType() == TransactionType.TIP_ADJUSTMENT)
            return StringUtils.formatAmountTwoDecimal(readCardResponse.getCardDetails().getAmount());
        return "";
    }

    private String retrieveAmount() {
        if (this.transactionRequest.getTransactionType() == TransactionType.TIP_ADJUSTMENT)
            return StringUtils.formatAmountTwoDecimal(readCardResponse.getCardDetails().getCashBackAmount());
        else
            return StringUtils.formatAmountTwoDecimal(readCardResponse.getCardDetails().getAmount());
    }

    private EmvTags prepareEmvTags() {
        return EmvTags.Builder.defaultBuilder()
                .withAc(this.getFromTagCollection(IccData.APPLICATION_CRYPTOGRAM.getTag()))
                .withResponseCode("")
                .withAid(this.getFromTagCollection(IccData.DF_NAME.getTag()))
                .withCardType(this.readCardResponse.getCardDetails().getCardType().getPosEntryMode())
                .withCid(this.getFromTagCollection(IccData.CID.getTag()))
                .withCvm(this.getFromTagCollection(IccData.CVM.getTag()))
                .withKernelId(this.getFromTagCollection(IccData.KERNEL_ID.getTag()))
                .withTvr(this.getFromTagCollection(IccData.TVR.getTag()))
                .withTsi(this.getFromTagCollection(IccData.TSI.getTag()))
                .withIin(this.getFromTagCollection(IccData.IIN.getTag()))
                .build();
    }

    private String getFromTagCollection(int tag) {
        try {
            return this.readCardResponse.getCardDetails().getTagCollection().get(tag).getData();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
