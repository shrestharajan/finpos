package global.citytech.finpos.processor.nibl.reconciliation.check;

import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reconciliation.check.CheckReconciliationResponseParameter;

/**
 * Created by Unique Shakya on 8/11/2021.
 */
public class CheckReconciliationResponseModel implements CheckReconciliationResponseParameter, UseCase.Response {

    private boolean reconciliationRequired;

    public CheckReconciliationResponseModel(boolean reconciliationRequired) {
        this.reconciliationRequired = reconciliationRequired;
    }

    public boolean isReconciliationRequired() {
        return reconciliationRequired;
    }
}
