package global.citytech.finposframework.hardware.network

import global.citytech.finposframework.hardware.common.DeviceResponse
import global.citytech.finposframework.hardware.common.DeviceService

/**
 * Created by Rishav Chudal on 6/7/20.
 */
interface NetworkService: DeviceService {
    fun enableNetwork(network: NetworkType): DeviceResponse
    fun disableNetwork(network: NetworkType): DeviceResponse
    fun setDataTransmissionNetwork(network: NetworkType): DeviceResponse
    fun setAPN(network: NetworkType): DeviceResponse
}