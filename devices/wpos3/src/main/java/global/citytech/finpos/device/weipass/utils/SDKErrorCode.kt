package global.citytech.finpos.device.weipass.utils

/**
 * Created by Rishav Chudal on 6/9/20.
 */
enum class SDKErrorCode(
    val errorCode: Int,
    val description: String
) {
    /**
     * PICC SDK Error Code
     */
    TRANSPROCESS_PICC_TRANS_FAILED_EXCEPTION(0x040200FF, "Non-receiving transactions abnormal"),
    TRANSPROCESS_PICC_TRANS_PRE_PROCESS_FAILED(0x04020001, "Non- pick-up pre-processing failed"),
    TRANSPROCESS_PICC_TRANS_APPLICATION_SELECT_FAILED(
        0x04020002,
        "Non-connected app selection failed"
    ),
    TRANSPROCESS_PICC_TRANS_READ_RECORD_FAILED(0x04020003, "Non-receiving read app records failed"),
    TRANSPROCESS_PICC_TRANS_CARD_AUTHENTICATE_FAILED(
        0x04020004,
        "Non-receiving card data authentication failed"
    ),
    TRANSPROCESS_PICC_TRANS_PARAMETER_NULL(
        0x04020010,
        "Non-connected trading parameters are short"
    ),
    TRANSPROCESS_PICC_TRANS_PARAMETER_TERMINAL_SETTING_NULL(
        0x04020011,
        "Non-connected terminal parameters are empty(EMVParam)"
    ),
    TRANSPROCESS_PICC_TRANS_PARAMETER_TERMINALSETTING_ERROR(
        0x04020012,
        "Non-connected terminal parameters are empty(EMVParam)"
    ),
    TRANSPROCESS_PICC_TRANS_NO_APPLICATION(
        0x04020013,
        "Non-connected app selection, no application"
    ),
    TRANSPROCESS_PICC_TRANS_APPLICATION_BLOCK(0x04020014, "Non-connected card app locked"),
    TRANSPROCESS_PICC_TRANS_CARD_BLOCK(0x04020015, "Non-connected cards are locked"),
    TRANSPROCESS_PICC_TRANS_USE_OTHER_INTERFACE(
        0x04020016,
        "Non-receiving transactions are abnormal，try again"
    ),
    TRANSPROCESS_PICC_TRANS_NO_DETECT_CARD(0x04020017, "No non-receiving cards detected"),
    TRANSPROCESS_CONTACTLESSTRANS_ONLINE_FAILED(
        0x04020018,
        "Non-connected online transactions failed"
    ),
    TRANSPROCESS_PICC_TRANS_TRANS_DENIAL(0x04020019, "Non-connected process rejected"),
    TRANSPROCESS_PICC_TRANS_USE_CONTRACT(0x04020020, "Please use the contact card to trade"),
    TRANSPROCESS_PICC_TRANS_TRY_AGAIN(
        0x04020021,
        "Non-receiving transactions abnormal，try again"
    ),
    TRANSPROCESS_PICC_TRANS_OTHER_ERROR(
        0x04020023,
        "Other errors in non-receiving transactions"
    ),

    /**
     * ICC SDK Error Code
     */
    TRANSPROCESS_ICC_TRANS_FAILED(0x040100FF, "Contact deals failed"),
    TRANSPROCESS_ICC_TRANS_APPSEL_FAILED(0x04010001, "Contact trading app selection failed"),
    TRANSPROCESS_ICC_TRANS_READAPPDATA_FAILED(
        0x04010002,
        "Contact trading reading application records failed"
    ),
    TRANSPROCESS_ICC_TRANS_CARDAUTH_FAILED(
        0x04010003,
        "Contact transaction card data authentication failed"
    ),
    TRANSPROCESS_ICC_TRANS_PROCCONTACTTRANS_FAILED(
        0x04010004,
        "Contact transaction process failed"
    ),
    TRANSPROCESS_ICC_TRANS_ONLINE_FAILED(0x04010005, "Contact transaction online failed"),
    TRANSPROCESS_GETPANFROMEMVCARD_GETPAN_FAILED(
        0x04030001,
        "Contact transaction reading card number failed"
    ),
    TRANSPROCESS_ICC_TRANS_APPLICATION_BLOCK(
        0x04010011,
        "Contact transaction application blocked"
    ),

    /**
     * Other Error Code
     */
    OUTCOME_FAILED(0x050100ff, "Get outcome failed")

}