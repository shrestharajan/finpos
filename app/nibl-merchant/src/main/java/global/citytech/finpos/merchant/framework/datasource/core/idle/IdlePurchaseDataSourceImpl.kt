package global.citytech.finpos.merchant.framework.datasource.core.idle

import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.data.datasource.core.idle.IdlePurchaseDataSource
import global.citytech.finpos.merchant.framework.datasource.core.ApplicationRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.TransactionAuthenticatorImpl
import global.citytech.finpos.merchant.framework.datasource.device.*
import global.citytech.finpos.merchant.domain.model.purchase.PurchaseRequestEntity
import global.citytech.finpos.merchant.domain.model.purchase.PurchaseResponseEntity
import global.citytech.finpos.merchant.framework.datasource.core.TransactionRepositoryImpl
import global.citytech.finpos.merchant.presentation.data.mapToModel
import global.citytech.finpos.merchant.presentation.data.mapToPurchaseResponseUiModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.purchase.PurchaseRequestItem
import global.citytech.finpos.merchant.presentation.utils.NotificationHandler

import global.citytech.finposframework.switches.ProcessorManager
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest
import io.reactivex.Observable

/**
 * Created by Rishav Chudal on 11/24/20.
 */
class IdlePurchaseDataSourceImpl: IdlePurchaseDataSource {
    override fun purchase(
        configurationItem: ConfigurationItem,
        purchaseRequestItem: PurchaseRequestItem
    ): Observable<PurchaseResponseEntity> {
        Class.forName(BuildConfig.PROCESSOR_CLASS_NAME)
        val terminalRepository = TerminalRepositoryImpl(configurationItem)
        val transactionRepository =
            TransactionRepositoryImpl(
                NiblMerchant.INSTANCE
            )
        val transactionRequest = prepareTransactionRequest(purchaseRequestItem)
        val deviceController = DeviceControllerImpl(NiblMerchant.INSTANCE)
        val cardReader = CardSourceImpl(NiblMerchant.getActivityContext())
        val transactionAuthenticator =
            TransactionAuthenticatorImpl()
        val printerServiceImpl = PrinterSourceImpl(NiblMerchant.getActivityContext())
        val applicationRepository = ApplicationRepositoryImpl(NiblMerchant.INSTANCE)
        val purchaseRequestEntity = PurchaseRequestEntity(
            readCardService = cardReader,
            deviceController = deviceController,
            printerService = printerServiceImpl,
            transactionAuthenticator = transactionAuthenticator,
            transactionRepository = transactionRepository,
            transactionRequest = transactionRequest,
            applicationRepository = applicationRepository,
            ledService = LedSourceImpl(NiblMerchant.INSTANCE),
            soundService = SoundSourceImpl(NiblMerchant.INSTANCE)
        )
        val idlePurchaseRequester =
            ProcessorManager.getInterface(
                terminalRepository,
                NotificationHandler
            ).idlePurchaseRequester
        return Observable.fromCallable {
            (idlePurchaseRequester.execute(purchaseRequestEntity.mapToModel()).mapToPurchaseResponseUiModel())
        }
    }

    private fun prepareTransactionRequest(
        purchaseRequestItem: PurchaseRequestItem
    ): TransactionRequest {
        val transactionRequest = TransactionRequest(
            purchaseRequestItem.transactionType
        )
        transactionRequest.amount = purchaseRequestItem.amount!!
        return transactionRequest

    }
}