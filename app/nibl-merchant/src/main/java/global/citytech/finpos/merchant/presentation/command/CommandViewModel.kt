package global.citytech.finpos.merchant.presentation.command

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import global.citytech.common.data.Response
import global.citytech.common.extensions.hasInternetConnection
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.*
import global.citytech.finpos.merchant.data.repository.core.configuration.ConfigurationRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.Command
import global.citytech.finpos.merchant.domain.model.app.CommandType
import global.citytech.finpos.merchant.domain.model.app.Status
import global.citytech.finpos.merchant.domain.model.logon.LogonResponseEntity
import global.citytech.finpos.merchant.domain.model.app.*
import global.citytech.finpos.merchant.domain.usecase.app.*
import global.citytech.finpos.merchant.domain.usecase.core.configuration.CoreConfigurationUseCase
import global.citytech.finpos.merchant.framework.datasource.app.*
import global.citytech.finpos.merchant.framework.datasource.core.configuration.ConfigurationDataSourceImpl
import global.citytech.finpos.merchant.presentation.alarms.SettlementAlarmSetter
import global.citytech.finpos.merchant.presentation.model.*
import global.citytech.finpos.merchant.presentation.model.banner.TerminalBannersResponse
import global.citytech.finpos.merchant.presentation.model.command.execution.CommandExecutionAcknowledgmentRequest
import global.citytech.finpos.merchant.presentation.model.merchanttransactionlog.MerchantTransactionLogResponse
import global.citytech.finpos.merchant.presentation.model.setting.TerminalSettingRequest
import global.citytech.finpos.merchant.presentation.model.setting.TerminalSettingResponse
import global.citytech.finpos.merchant.service.ActivityLogPushService
import global.citytech.finpos.merchant.service.TransactionPushService
import global.citytech.finpos.merchant.utils.ApiConstant
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finpos.merchant.utils.AppUtility.savePushNotificationConfiguration
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.exceptions.PosException
import global.citytech.finposframework.utility.StringUtils
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Unique Shakya on 9/21/2021.
 */
class CommandViewModel(val context: Application) : BaseAndroidViewModel(context) {

    private val TAG = CommandViewModel::class.java.name
    val finishCommandTask by lazy { MutableLiveData<Boolean>() }
    val startDashActivity by lazy { MutableLiveData<Boolean>() }
    val commandToExecute by lazy { MutableLiveData<Command>() }
    var originalCommandList: MutableList<Command> = mutableListOf()
    val startSettlementActivity by lazy { MutableLiveData<Boolean>() }

    private val commandQueueUseCase = CommandQueueUseCase(
        CommandQueueRepositoryImpl(
            CommandQueueDataSourceImpl(context)
        )
    )

    private var localDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(),
            PreferenceManager
        )
    )

    private val qrPaymentUseCase = QrPaymentUseCase(
        QrPaymentRepositoryImpl(
            QrPaymentDataSourceImpl(context)
        )
    )

    private val mobileNfcPaymentUseCase = MobilePaymentUseCase(
        MobileNfcPaymentRepositoryImpl(
            MobileNfcPaymentDataSourceImpl(context)
        )
    )

    private val appNotificationUseCase = AppNotificationUseCase(
        AppNotificationRepositoryImpl(
            AppNotificationDataSourceImpl(context)
        )
    )

    private var localRepository = LocalRepositoryImpl(
        LocalDatabaseSourceImpl(),
        PreferenceManager
    )

    private val configurationUseCase: CoreConfigurationUseCase =
        CoreConfigurationUseCase(
            ConfigurationRepositoryImpl(
                ConfigurationDataSourceImpl()
            )
        )

    fun retrieveCommands() {
        isLoading.value = true
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<MutableList<Command>> {
                it.onNext(commandQueueUseCase.getAllCommands())
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onCommandsReceived(it!!)
                }, {
                    printLog(TAG, "Error in retrieving command " + it.message)
                    isLoading.value = false
                    finishCommandTask.value = true
                })
        )
    }

    private fun onCommandsReceived(it: MutableList<Command>) {
        originalCommandList = it
        performCommands()
    }

    private fun performCommands() {
        if (originalCommandList.isEmpty()) {
            printLog(TAG, "::: COMMAND ORIGINAL LIST IS EMPTY :::")
            isLoading.value = false
            finishCommandTask.value = true
        } else {
            printLog(TAG, "::: COMMAND ORIGINAL LIST IS NOT EMPTY :::")
            val command = originalCommandList[0]
            printLog(TAG, "::: COMMAND ORIGINAL LIST [0] $command:::")
            if (command.status == Status.OPEN)
                commandToExecute.value = command
            else
                sendCommandExecuteAcknowledgment(command, command.status, command.remarks)
        }
    }

    fun performKeyExchange() {
        compositeDisposable.add(localDataUseCase.getWholeConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.mapToPresentation() }
            .subscribe({
                performLogon(it)
            }, {
                printLog(TAG, "KEY EXCHANGE FROM COMMAND >>> EXCEPTION" + it.message.toString())
                onCommandExecutionFailure(it.localizedMessage)
            })
        )
    }

    private fun performLogon(it: ConfigurationItem?) {
        printLog(TAG, "::: PERFORMING LOGON FROM COMMAND :::")
        compositeDisposable.add(
            configurationUseCase.logOn(it!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    ActivityLogPushService.pushActivityLog()
                    onLogonNext(it)
                }, {
                    printLog(TAG, "::: LOGON FROM COMMAND >>> EXCEPTION ${it.localizedMessage}")
                    ActivityLogPushService.pushActivityLog()
                    onCommandExecutionFailure(it.localizedMessage)
                })
        )
    }

    private fun onLogonNext(logonResponseEntity: LogonResponseEntity) {
        printLog(TAG, "::: LOGON FROM COMMAND >>> IS SUCCESS?? ${logonResponseEntity.isSuccess}")
        if (logonResponseEntity.isSuccess)
            onCommandExecutionSuccess()
        else
            onCommandExecutionFailure()
    }

    fun isTerminalConfigured(): Boolean = localDataUseCase.isTerminalConfigured(false)

    fun onCommandExecutionSuccess() {
        printLog(TAG, "::: COMMAND EXECUTION SUCCESS :::")
        commandQueueUseCase.updateCommandStatus(commandToExecute.value!!, Status.SUCCESS)
        sendCommandExecuteAcknowledgment(commandToExecute.value!!, Status.SUCCESS)
    }

    fun onCommandExecutionFailure(errorMessage: String = "") {
        printLog(TAG, "::: COMMAND EXECUTION FAILURE :::")
        commandQueueUseCase.updateCommandStatus(
            command = commandToExecute.value!!,
            status = Status.FAILED,
            remarks = errorMessage
        )
        sendCommandExecuteAcknowledgment(commandToExecute.value!!, Status.FAILED, errorMessage)
    }

    private fun sendCommandExecuteAcknowledgment(
        commandToAcknowledge: Command,
        status: Status,
        remarks: String = ""
    ) {
        printLog(TAG, "::: COMMAND EXEC ACK SENDING :::")
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Boolean> {
                onCommandExecutionAcknowledgmentSubscribe(it, commandToAcknowledge, status, remarks)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    printLog(TAG, "::: COMMAND EXEC ACK ON NEXT >> IS SUCCESS?? $it")
                    if (it)
                        handleAcknowledgeSuccess(commandToAcknowledge)
                    else
                        handleAcknowledgeFailure(commandToAcknowledge)
                }, {
                    printLog(
                        TAG,
                        "::: COMMAND EXEC ACK ON NEXT 1 >> EXCEPTION?? ${it.localizedMessage}"
                    )
                    handleAcknowledgeFailure(commandToAcknowledge)
                })
        )
    }

    private fun handleAcknowledgeFailure(commandToAcknowledge: Command) {
        commandQueueUseCase.removeCommand(commandToAcknowledge)
        originalCommandList.remove(commandToAcknowledge)
        performNextCommand(commandToAcknowledge)

    }

    private fun performNextCommand(previousCommand: Command) {
        if (previousCommand.commandType == CommandType.SWITCH_CONFIG_UPDATE
            || previousCommand.commandType == CommandType.BANNER_UPDATE
            || previousCommand.commandType == CommandType.SYNC_SETTINGS
            || previousCommand.commandType == CommandType.TRANSACTION_LOG_PUSH
        ) {
            startDashActivity.postValue(true)
        } else {
            performCommands()
        }
    }

    private fun handleAcknowledgeSuccess(commandToAcknowledge: Command) {
        commandQueueUseCase.removeCommand(commandToAcknowledge)
        originalCommandList.remove(commandToAcknowledge)
        performNextCommand(commandToAcknowledge)
    }

    private fun onCommandExecutionAcknowledgmentSubscribe(
        emitter: ObservableEmitter<Boolean>,
        commandToAcknowledge: Command,
        status: Status,
        remarks: String
    ) {
        val acknowledgmentRequest = CommandExecutionAcknowledgmentRequest(
            serialNumber = NiblMerchant.INSTANCE.iPlatformManager.serialNumber,
            terminalCommandId = commandToAcknowledge.id,
            status = status.name,
            remarks = remarks
        )
        val jsonRequest = Jsons.toJsonObj(acknowledgmentRequest)
        val jsonResponse =
            NiblMerchant.INSTANCE.iPlatformManager.post(
                ApiConstant.COMMAND_EXECUTION_ACK_URL,
                jsonRequest,
                false
            )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        emitter.onNext(response.code == "0")
        emitter.onComplete()
    }

    fun fetchBanners() {
        isLoading.value = true
        compositeDisposable.add(
            postRequestForBanners()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        onTerminalBannersResponseReceived(it)
                    },
                    {
                        printLog(TAG, "FETCH BANNER FROM COMMAND >>> EXCEPTION ::: " + it.message)
                        onErrorTerminalBannersResponse(it)
                    }
                )
        )
    }

    private fun postRequestForBanners(): Observable<TerminalBannersResponse> {
        return Observable.fromCallable {
            val configRequest =
                ConfigRequest(
                    AppInfo("NiblAdmin", 1),
                    DeviceInfo(NiblMerchant.INSTANCE.iPlatformManager.serialNumber)
                )
            val jsonRequest = Jsons.toJsonObj(configRequest)
            val jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.post(
                ApiConstant.CONFIG_URL,
                jsonRequest,
                false
            )
            val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
            val responseJson = Jsons.toJsonObj(response.data)
            val configResponse = Jsons.fromJsonToObj(responseJson, ConfigResponse::class.java)
            TerminalBannersResponse(configResponse.logo!!)
        }
    }

    private fun onTerminalBannersResponseReceived(it: TerminalBannersResponse) {
        printLog(TAG, "TerminalBannersResponse data ::: ".plus(Jsons.toJsonObj(it.logo.banners)))
        isLoading.value = false
        localDataUseCase.setBanners(it.logo.banners!!)
        sendCommandExecuteAcknowledgment(commandToExecute.value!!, Status.SUCCESS)
    }

    private fun onErrorTerminalBannersResponse(it: Throwable) {
        printLog(TAG, "Terminal Banners Response Error ::: ".plus(it.message))
        isLoading.value = false
        sendCommandExecuteAcknowledgment(
            commandToExecute.value!!, Status.FAILED, it.localizedMessage
        )
    }

    fun syncSettings() {
        isLoading.value = true
        compositeDisposable.add(
            postSyncSettings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        onTerminalSettingResponseReceived(it)
                    },
                    {
                        printLog(TAG, "SYNC SETTING FROM COMMAND >>> EXCEPTION ::: " + it.message)
                        onErrorTerminalSettingResponse(it)
                    }
                )
        )
    }


    fun performSettlement() {
        //isLoading.value = true
        startSettlementActivity.value = true
    }

    fun triggerTransactionLogPush() {
        isLoading.value = true
        getMerchantTransactionLogs()
    }

    private fun getMerchantTransactionLogs() {
        compositeDisposable.add(
            localRepository.getMerchantTransactionLog().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    updateMerchantTransactionLogsToFinpos(it)
                }, {
                    isLoading.value = false
                    printLog(TAG, "Fetching Merchant Transaction log error ::: " + it.message)
                })
        )
    }

    private fun updateMerchantTransactionLogsToFinpos(merchantTransactionLogs: List<MerchantTransactionLog>?) {
        if (context.hasInternetConnection()) {
            TransactionPushService.isLogPushInProgress = true
            compositeDisposable.add(
                Observable.create(ObservableOnSubscribe<MerchantTransactionLogResponse> {
                    onSubscribe(merchantTransactionLogs, it)
                })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onNext, this::onError)
            )
        } else {
            isLoading.value = false
        }
    }

    private fun onNext(response: MerchantTransactionLogResponse) {
        isLoading.value = false
        TransactionPushService.isLogPushInProgress = false
        if (response.data != null) {
            compositeDisposable.add(
                localRepository.nukeMerchantTransactionLog()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        commandQueueUseCase.removeCommand(commandToExecute.value!!)
                        performNextCommand(commandToExecute.value!!)
                        printLog(TAG, "Merchant Transaction Log Database Cleared")
                    }, {
                        printLog(TAG, "Updating Merchant Transaction log  error ::: " + it.message)
                    })
            )
        } else {
            printLog(TAG, "Some Error occurred during pushing merchant transaction log.")
        }

    }

    private fun onError(throwable: Throwable) {
        printLog(TAG, throwable.message.toString())
        isLoading.value = false
        TransactionPushService.isLogPushInProgress = false
    }

    private fun onSubscribe(
        merchantTransactionLogs: List<MerchantTransactionLog>?,
        emitter: ObservableEmitter<MerchantTransactionLogResponse>
    ) {
        val jsonRequest = Jsons.toJsonObj(merchantTransactionLogs)
        printLog(TAG, "Merchant Transaction Log Request::: $jsonRequest", true)
        val jsonResponse =
            NiblMerchant.INSTANCE.iPlatformManager.post(
                ApiConstant.UPDATE_MERCHANT_TRANSACTION_LOG_URL,
                jsonRequest,
                true
            )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            prepareResponse(emitter, response)
        } else {
            if (response.code == "401")
                emitter.onError(PosException(PosError.DEVICE_ERROR_OPERATOR_LOGIN_REQUIRED))
            else
                emitter.onError(Exception("Error Code :: ${response.code}. ${response.message}"))
        }
        emitter.onComplete()
    }

    private fun prepareResponse(
        emitter: ObservableEmitter<MerchantTransactionLogResponse>,
        data: Any
    ) {
        val responseJson = Jsons.toJsonObj(data)
        val response = Jsons.fromJsonToObj(responseJson, MerchantTransactionLogResponse::class.java)
        emitter.onNext(response)
    }

    private fun postSyncSettings(): Observable<TerminalSettingResponse> {
        return Observable.fromCallable {
            val jsonRequest =
                Jsons.toJsonObj(TerminalSettingRequest(NiblMerchant.INSTANCE.iPlatformManager.serialNumber))
            val jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.post(
                ApiConstant.SYNC_SETTING,
                jsonRequest,
                false
            )
            val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
            val responseJson = Jsons.toJsonObj(response.data)
            Jsons.fromJsonToObj(responseJson, TerminalSettingResponse::class.java)
        }
    }

    private fun onTerminalSettingResponseReceived(it: TerminalSettingResponse) {
        isLoading.value = false
        localDataUseCase.saveTerminalSetting(it)
        it.qrBillNumber?.let { qrBillNumber ->
            qrPaymentUseCase.saveBillingInvoiceNumber(
                qrBillNumber
            )
        }
        Log.d(TAG, "::Nfc bill number :: " + it.nfcBillNumber.toString())
        it.nfcBillNumber?.let { mobileNfcPaymentUseCase.saveNfcPaymentIdentifiers(it) }
        if (it.enableAutoSettlement && !it.settlementTime.isNullOrEmpty()) {
            setSettlementTimeAlarm(it.settlementTime)
        } else {
            PreferenceManager.storeSettlementTime("")
            NiblMerchant.INSTANCE.cancelRescheduledSettlement()
        }
        localDataUseCase.saveDebugModeStatus(it.enableDebugLog)
        addQrDisclaimerNotification(it)
        it.pushNotificationConfiguration?.let { it1 -> savePushNotificationConfiguration(it1) }

        localDataUseCase.saveGreenPinStatus(it.enableGreenPin)
        localDataUseCase.saveSoundModeStatus(it.isSoundEnabled)
        sendCommandExecuteAcknowledgment(commandToExecute.value!!, Status.SUCCESS)
    }

    private fun setSettlementTimeAlarm(reconcileTime: String?) {
        reconcileTime?.let {
            val settlementAlarmSetter = SettlementAlarmSetter(context.applicationContext)
            settlementAlarmSetter.setRepeatingAlarm(it)
            this.localDataUseCase.storeSettlementTime(reconcileTime)
        }
    }


    private fun addQrDisclaimerNotification(terminalSettings: TerminalSettingResponse) {
        if (terminalSettings.showQrDisclaimer) {
            appNotificationUseCase.addNotification(
                AppNotification(
                    title = "",
                    body = context.getString(R.string.msg_qr_disclaimer_notification),
                    timeStamp = StringUtils.dateTimeStamp(),
                    action = Action.QR_DISCLAIMER,
                    read = false,
                    type = Type.WARNING
                )
            )
        } else {
            appNotificationUseCase.removeNotificationByAction(Action.QR_DISCLAIMER)
        }
    }

    private fun onErrorTerminalSettingResponse(it: Throwable) {
        printLog(TAG, "Terminal Setting Response Error ::: ".plus(it.message))
        isLoading.value = false
        sendCommandExecuteAcknowledgment(
            commandToExecute.value!!,
            Status.FAILED,
            it.localizedMessage
        )
    }

    fun handleUnknownCommand() {
        sendCommandExecuteAcknowledgment(
            commandToExecute.value!!,
            Status.FAILED,
            "Not Implemented : App V" + BuildConfig.VERSION_NAME
        )
    }
}