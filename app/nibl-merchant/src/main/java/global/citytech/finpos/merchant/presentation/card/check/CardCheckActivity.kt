package global.citytech.finpos.merchant.presentation.card.check

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityCardCheckBinding

/**
 * Created by Unique Shakya on 11/5/2020.
 */
class CardCheckActivity : AppBaseActivity<ActivityCardCheckBinding, CardCheckViewModel>() {

    companion object {
        const val REQUEST_CODE = 2273
    }

    override fun getBindingVariable(): Int = BR.cardCheckViewModel

    override fun getLayout(): Int = R.layout.activity_card_check

    override fun getViewModel(): CardCheckViewModel =
        ViewModelProviders.of(this)[CardCheckViewModel::class.java]

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.initObservers()
        this.getViewModel().checkForCardPresent()
    }

    private fun initObservers() {
        this.getViewModel().cardPresent.observe(this, Observer {
            if (it) {
                this.getViewModel().playAlertSound()
                Handler().postDelayed({ this.getViewModel().checkForCardPresent() }, 1000)
            } else {
                this.returnCardRemovedResult()
            }
        })
    }

    private fun returnCardRemovedResult() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onBackPressed() {

    }
}