package global.citytech.payment.sdk.utils;

import java.nio.charset.StandardCharsets;

/**
 * Created by BikashShrestha on 6/4/19.
 */
public class HexUtils {

    public static byte[] hex2byte(String s) {
        if (s == null) {
            return new byte[1];
        } else {
            return s.length() % 2 == 0 ? hex2byte(s.getBytes(StandardCharsets.UTF_8), 0, s.length() >> 1) : hex2byte("0" + s);
        }
    }

    private static byte[] hex2byte(byte[] b, int offset, int len) {
        byte[] d = new byte[len];

        for (int i = 0; i < len * 2; ++i) {
            int shift = i % 2 == 1 ? 0 : 4;
            d[i >> 1] = (byte) (d[i >> 1] | Character.digit((char) b[offset + i], 16) << shift);
        }

        return d;
    }

    public static String byte2hex(byte[] bs) {
        return byte2hex(bs, 0, bs.length);
    }

    public static String byte2hex(byte[] bs, int off, int length) {
        if (bs.length > off && bs.length >= off + length) {
            StringBuilder sb = new StringBuilder(length * 2);
            byte2hexAppend(bs, off, length, sb);
            return sb.toString().toUpperCase();
        } else {
            throw new IllegalArgumentException();
        }
    }

    private static void byte2hexAppend(byte[] bs, int off, int length, StringBuilder sb) {
        if (bs.length > off && bs.length >= off + length) {
            sb.ensureCapacity(sb.length() + length * 2);

            for (int i = off; i < off + length; ++i) {
                sb.append(Character.forDigit(bs[i] >>> 4 & 15, 16));
                sb.append(Character.forDigit(bs[i] & 15, 16));
            }

        } else {
            throw new IllegalArgumentException();
        }
    }

}
