package global.citytech.finposframework.listeners;

import java.io.Serializable;

public interface CashInAmountConfirmationListener extends Serializable {
    void onResult(boolean confirmed, String amount);
}
