package global.citytech.finpos.processor.nibl.receipt.duplicatereceipt;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.duplicatereceipt.DuplicateReceiptRequestParameter;

/**
 * Created by Unique Shakya on 12/15/2020.
 */
public class DuplicateReceiptRequestModel implements UseCase.Request, DuplicateReceiptRequestParameter {

    private TransactionRepository transactionRepository;
    private PrinterService printerService;
    private String stan;

    public DuplicateReceiptRequestModel(TransactionRepository transactionRepository, PrinterService printerService) {
        this.transactionRepository = transactionRepository;
        this.printerService = printerService;
    }
    public DuplicateReceiptRequestModel(TransactionRepository transactionRepository, PrinterService printerService,String stan) {
        this.transactionRepository = transactionRepository;
        this.printerService = printerService;
        this.stan = stan;
    }

    public String getStan() {
        return stan;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }
}
