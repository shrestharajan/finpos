package global.citytech.finpos.neps.transaction;

import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.usecases.transaction.receipt.Performance;
import global.citytech.finposframework.usecases.transaction.receipt.Retailer;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.EmvTags;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.TransactionInfo;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
import global.citytech.finposframework.utility.CvmResultsParser;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Rishav Chudal on 7/6/20.
 */
public class TransactionReceiptHandler {
    private TransactionRequest transactionRequest;
    private PurchaseRequest purchaseRequest;
    private ReadCardResponse readCardResponse;
    private IsoMessageResponse isoMessageResponse;

    public TransactionReceiptHandler(
            TransactionRequest transactionRequest,
            PurchaseRequest purchaseRequest,
            ReadCardResponse readCardResponse,
            IsoMessageResponse isoMessageResponse
    ) {
        this.transactionRequest = transactionRequest;
        this.purchaseRequest = purchaseRequest;
        this.readCardResponse = readCardResponse;
        this.isoMessageResponse = isoMessageResponse;
    }

    public ReceiptLog prepare() {
        return new ReceiptLog.Builder()
                .withRetailer(prepareRetailerInfo())
                .withPerformance(preparePerformanceInfo())
                .withTransaction(prepareTransactionInfo())
                .withEmvTags(prepareEmvTags())
                .build();
    }

    private Retailer prepareRetailerInfo() {
        return new Retailer.Builder()
                .withRetailerName(
                        this.transactionRequest
                                .getEmvParameterRequest()
                                .getMerchantName()
                )
                .withRetailerAddress("Kamal Pokhari, Kathmandu")
                .withMerchantId(this.transactionRequest
                        .getEmvParameterRequest()
                        .getMerchantIdentifier()
                )
                .withTerminalId(this.transactionRequest
                        .getEmvParameterRequest()
                        .getTerminalId()
                )
                .build();
    }

    private Performance preparePerformanceInfo() {
        return new Performance.Builder()
                .withStartDateTime(
                        this.readCardResponse
                                .getCardDetails()
                                .getTransactionInitializeDateTime()
                )
                .withEndDateTime(
                        String.valueOf(
                                this.isoMessageResponse
                                        .getMsg()
                                        .getDataElementByIndex(12).get().getData()

                        )
                )
                .build();
    }

    private TransactionInfo prepareTransactionInfo() {
        boolean isApproved = this.isApprovedByActionCode(this.isoMessageResponse.getMsg().getDataElementByIndex(39).get().getValueAsInt());
        return new TransactionInfo.Builder()
                .withApprovalCode((String) this.isoMessageResponse.getMsg().getDataElementByIndex(38).get().getData())
                .withBatchNumber("123456")
                .withCardHolderName(!StringUtils.isEmpty(readCardResponse.getCardDetails().getCardHolderName())?readCardResponse.getCardDetails().getCardHolderName().trim():"")
                .withCardNumber(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .withCardScheme(readCardResponse.getCardDetails().getCardSchemeLabel())
                .withExpireDate(readCardResponse.getCardDetails().getExpiryDate())
                .withPurchaseAmount(StringUtils.formatAmountTwoDecimal(readCardResponse.getCardDetails().getAmount())) // handle differently for tip adjustment
                .withPurchaseType(this.transactionRequest.getTransactionType().getPrintName())
                .withRrn((String) this.isoMessageResponse.getMsg().getDataElementByIndex(37).get().getData())
                .withStan((String) this.isoMessageResponse.getMsg().getDataElementByIndex(11).get().getData())
                .withTransactionMessage(this.retrieveTransactionMessage(isApproved))
                .withTransactionStatus(this.retrieveTransactionStatus(isApproved))
                .build();
    }

    private String retrieveTransactionMessage(boolean isApproved) {
        if (isApproved)
            return CvmResultsParser.retrieveCvmResults(readCardResponse.getCardDetails().getTagCollection().get(0x9F34).getData()).getCvm().getCardHolderVerification().getCardHolderVerificationEn();
        else
            return "DECLINED BY ISSUING BANK";
    }

    private String retrieveTransactionStatus(boolean isApproved) {
        if (isApproved)
            return "APPROVED";
        else
            return "DECLINED";
    }

    private boolean isApprovedByActionCode(int responseCode) {
        return responseCode == 0;
    }

    private EmvTags prepareEmvTags() {
        return EmvTags.Builder.defaultBuilder()
                .withAc(this.getFromTagCollection(IccData.APPLICATION_CRYPTOGRAM.getTag()))
                .withAid(this.getFromTagCollection(IccData.DF_NAME.getTag()))
                .withCardType(this.retrieveCardType())
                .withCid(this.getFromTagCollection(IccData.CID.getTag()))
                .withCvm(this.getFromTagCollection(IccData.CVM.getTag()))
                .withKernelId(this.getFromTagCollection(IccData.KERNEL_ID.getTag()))
                .withResponseCode((String) this.isoMessageResponse.getMsg().getDataElementByIndex(39).get().getData())
                .withTvr(this.getFromTagCollection(IccData.TVR.getTag()))
                .build();
    }

    private String getFromTagCollection(int tag) {
        try {
            return this.readCardResponse.getCardDetails().getTagCollection().get(tag).getData();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String retrieveCardType() {
        switch (this.readCardResponse.getCardDetails().getCardType()) {
            case ICC:
                return TransactionReceiptLabels.CARD_TYPE_CONTACT;
            case PICC:
                return TransactionReceiptLabels.CARD_TYPE_CONTACTLESS;
            case MANUAL:
                return TransactionReceiptLabels.CARD_TYPE_MANUAL;
            case MAG:
                return TransactionReceiptLabels.CARD_TYPE_MAGNETIC;
            default:
                return "";
        }
    }
}
