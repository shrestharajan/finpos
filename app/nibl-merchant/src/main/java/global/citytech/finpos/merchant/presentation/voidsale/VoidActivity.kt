package global.citytech.finpos.merchant.presentation.voidsale

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import global.citytech.common.Constants
import global.citytech.common.extensions.defaultOrEmptyValue
import global.citytech.common.extensions.getBundle
import global.citytech.common.extensions.hideKeyboard
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityBaseTransactionBinding
import global.citytech.finpos.merchant.datasource.core.voidsale.VoidSaleUtil
import global.citytech.finpos.merchant.presentation.admin.login.LoginDialog
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.TransactionItem
import global.citytech.finpos.merchant.presentation.payment.PaymentSdkSingleton
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionActivity
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finpos.merchant.utils.*
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.utility.StringUtils
import global.citytech.payment.sdk.api.PaymentResult
import kotlinx.android.synthetic.main.activity_base_transaction.*
import kotlinx.android.synthetic.main.layout_void.*
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Created by Rishav Chudal on 9/18/20.
 */
class VoidActivity : BaseTransactionActivity<ActivityBaseTransactionBinding, VoidViewModel>(),
    LoginDialog.LoginDialogListener {

    private lateinit var viewModel: VoidViewModel
    private lateinit var transactionItem: TransactionItem
    private lateinit var configurationItem: ConfigurationItem
    private lateinit var mode: String

    private lateinit var loginDialog: LoginDialog
    private lateinit var textWatcher: TextWatcher

    companion object {
        const val MODE = "mode"
        const val CASH = "cash"
        const val SALE = "sale"
        const val INVOICE = "invoice_number"
        const val FROM_TRANSACTION_DETAIL = "from_transaction_detail"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        PaymentSdkSingleton.getInstance().isFromPaymentSdk =
            intent.getBooleanExtra(Constants.EXTRA_FROM_CHECKOUT, false)
        mode = if (PaymentSdkSingleton.getInstance().isFromPaymentSdk)
            SALE
        else
            getBundle()?.getString(MODE)!!
        loadVoidLayout()
        initViews()
        initBaseViews()
        this.viewModel = getViewModel()
        initObservers()
        initTransaction()

        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
    }

    private fun initTransaction() {
        val invoiceNumber: String?
        getBundle()?.let {
            fromTransactionDetail = it.getBoolean(FROM_TRANSACTION_DETAIL, false)
        }
        if (PaymentSdkSingleton.getInstance().isFromPaymentSdk) {
            getViewModel().checkoutAdditionalData = intent.getStringExtra(Constants.EXTRA_ADDITIONAL_DATA)
            val rrn = intent.getStringExtra(Constants.EXTRA_REFERENCE_NUMBER)
            getViewModel().retrieveInvoiceNumberFromRrn(rrn)
        } else {
            invoiceNumber = getBundle()?.getString(INVOICE)
            invoiceNumber?.let {
                et_void_invoice.setText(invoiceNumber)
                et_void_invoice.isEnabled = false;
                btn_void_confirm.performClick()
            }
        }
    }

    private fun loadVoidLayout() {
        layout_void.visibility = View.VISIBLE
    }

    private fun initViews() {

        tv_void_title.text =
            if (mode == CASH) {
                resources.getString(R.string.title_void_cash)
            } else {
                resources.getString(R.string.title_void_sale)
            }

        textWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                ll_void_txn_detail.visibility = View.GONE
                btn_void_confirm.text = resources.getString(R.string.action_next)
                btn_void_confirm.enableMe()
            }

            override fun afterTextChanged(s: Editable?) {

            }
        }
        et_void_invoice.addTextChangedListener(textWatcher)
        et_void_invoice.requestFocus()
        iv_void_back.setOnClickListener {
            this.hideKeyboard(et_void_invoice)
            updatePaymentCancelled()
            onBackPressed()
        }

        btn_void_cancel.setOnClickListener {
            this.hideKeyboard(et_void_invoice)
            btn_void_cancel.handleDebounce()
            updatePaymentCancelled()
            returnToDashboard()
        }

        btn_void_confirm.setOnlineClickListener {
            hideKeyboard(et_void_invoice)
            if (ll_void_txn_detail.visibility == View.VISIBLE) {
                initiateVoidSale()
            } else {
                btn_void_confirm.text = resources.getString(R.string.action_next)
                this.viewModel.validateData(
                    StringUtils.ofRequiredLength(
                        et_void_invoice.text.toString(),
                        6
                    ), mode
                )
            }
        }
    }

    private fun hideVoidLayout() {
        layout_void.visibility = View.GONE
    }

    private fun initObservers() {

        this.viewModel.transactionItem.observe(this, Observer {
            this.transactionItem = it
        })
        this.viewModel.configurationItem.observe(this, Observer {
            this.configurationItem = it
            hideKeyboard(et_void_invoice)
            showTransactionDetail()
        })

        this.viewModel.invalidInvoiceNumber.observe(this, Observer {
            if (it) {
                showToast(getString(R.string.error_msg_invalid_invoice_number), Toast.LENGTH_SHORT)
            }
        })

        this.viewModel.transactionFound.observe(this, Observer {
            if (!it) {
                if (PaymentSdkSingleton.getInstance().isFromPaymentSdk) {
                    updatePaymentFailedIntent(
                        PaymentResult.VOID_TRANSACTION_NOT_FOUND.resultCode,
                        getString(R.string.error_msg_transaction_not_found)
                    )
                    returnToDashboard()
                } else {
                    showToast(
                        getString(R.string.error_msg_transaction_not_found),
                        Toast.LENGTH_SHORT
                    )
                }
            }
        })

        viewModel.invoiceNumberFromRrn.observe(this, Observer {
            et_void_invoice.setText(it)
            et_void_invoice.isEnabled = false;
            btn_void_confirm.performClick()
        })

        viewModel.invalidRrnFromCheckout.observe(this, Observer {
            if (it) {
                updatePaymentFailedIntent(
                    PaymentResult.VOID_INVALID_RRN.resultCode,
                    getString(R.string.msg_invalid_rrn)
                )
                returnToDashboard()
            }
        })

        observeTransactionAlreadyVoidedLiveData()
        initBaseLiveDataObservers()
    }

    private fun observeTransactionAlreadyVoidedLiveData() {
        this.viewModel.transactionAlreadyVoided.observe(
            this,
            Observer {
                if (it) {
                    if (PaymentSdkSingleton.getInstance().isFromPaymentSdk) {
                        updatePaymentFailedIntent(
                            PaymentResult.VOID_TRANSACTION_ALREADY_VOID.resultCode,
                            getString(R.string.msg_transaction_already_voided)
                        )
                        returnToDashboard()
                    } else {
                        showToast(
                            getString(R.string.msg_transaction_already_voided),
                            Toast.LENGTH_SHORT
                        )
                    }
                }
            })
    }

    private fun showTransactionDetail() {
        val txnType =
            Jsons.fromJsonToObj(transactionItem.transactionType, TransactionType::class.java)
        tv_void_txn_type.text = txnType.displayName

        val maskedCardNumber = transactionItem.pan?.maskCardNumber()
        maskedCardNumber.apply {
            rl_transaction_detail_card_number.visibility = if (this.isNullOrEmpty()) View.GONE else View.VISIBLE
            tv_void_card_number.text = this.defaultOrEmptyValue()
        }

        val formattedExpiryDate = transactionItem.expiryDate?.formatExpiryDate()
        formattedExpiryDate.apply {
            rl_transaction_detail_expiry_date.visibility = if (this.isNullOrEmpty()) View.GONE else View.VISIBLE
            tv_void_expiry.text = this.defaultOrEmptyValue()
        }

        displayAmount = VoidSaleUtil.getTotalAmount(transactionItem, this.getViewModel())
        tv_void_amount.text = transactionItem.currencyName.plus(" ").plus(displayAmount)
        ll_void_txn_detail.visibility = View.VISIBLE
        btn_void_confirm.text = resources.getString(R.string.action_confirm)
        modifyViewIfTransactionItemIsEmiTransaction()
    }

    private fun modifyViewIfTransactionItemIsEmiTransaction() {
        if (transactionItem.isEmiTransaction) {
            tv_title_card_view.text = getString(R.string.title_emi_transaction)
            btn_void_confirm.disableMe()
        }
    }

    private fun initiateVoidSale() {
        showMerchantLoginDialog(
            getString(R.string.msg_merchant_password),
            getViewModel().getMerchantCredential()
        )
    }

    private fun showMerchantLoginDialog(pageTitle: String, validCredential: String) {
        this.loginDialog = LoginDialog.newInstance(
            pageTitle = pageTitle,
            validCredential = validCredential
        )
        this.loginDialog.isCancelable = false
        this.loginDialog.show(
            supportFragmentManager,
            LoginDialog.TAG
        )
    }

    override fun onLoginDialogCorrectCredentialEntered() {
        hideKeyboard(et_void_invoice)
        hideVoidLayout()
        this.showProgressScreen("Validating Invoice Transaction...")
        this.amount = transactionItem.amount?.toBigDecimal()!!
            .divide(BigDecimal.valueOf(100), 2, RoundingMode.CEILING)
        showHeaderView(displayAmount)
        this.viewModel.doVoidSale(
            configurationItem,
            amount,
            getTransactionType()
        )
    }

    override fun onLoginDialogCancelButtonClicked() {
        hideKeyboard(et_void_invoice)
    }

    override fun getTransactionType(): TransactionType {
        return when (mode) {
            CASH -> TransactionType.CASH_VOID
            SALE -> TransactionType.VOID
            else -> TransactionType.VOID
        }
    }

    override fun getTransactionViewModel(): BaseTransactionViewModel {
        return getViewModel()
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayout(): Int = R.layout.activity_base_transaction

    override fun getViewModel(): VoidViewModel =
        ViewModelProviders.of(this)[VoidViewModel::class.java]

    override fun onPositiveButtonClick() {
        this.showProgressScreen("Printing Customer Copy...")
        this.viewModel.printCustomerCopy()
    }

    override fun onNegativeButtonClick() = checkForCardPresent()

    override fun onManualButtonClicked() {
        showToast("Manual Button Clicked", Toast.LENGTH_SHORT)
        this.viewModel.doCleanUp(BaseTransactionViewModel.Task.MANUAL_TRANSACTION)
    }

    override fun onCancelButtonClicked() {
        showToast("Cancel Button Clicked", Toast.LENGTH_SHORT)
        updatePaymentCancelled()
        this.viewModel.doCleanUp(BaseTransactionViewModel.Task.RETURN_TO_DASHBOARD)
    }

    override fun onAmountActivityResult(data: Intent?) {
        // not needed as amount is not entered in void sale therefore never invoked in case of void sale
    }

    override fun onManualActivityResult(data: Intent?) {
        // not needed as manual data is not entered in void sale therefore never invoked in case of void sale
    }

    override fun onBackPressed() {
        if (ll_void_txn_detail.visibility == View.VISIBLE) {
            et_void_invoice.setText("")
            ll_void_txn_detail.visibility = View.GONE
        } else {
            updatePaymentCancelled()
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        et_void_invoice.removeTextChangedListener(textWatcher)
    }
}