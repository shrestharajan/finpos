package global.citytech.finpos.merchant.presentation.ministatement

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import global.citytech.common.Base64
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityMiniStatementBinding
import global.citytech.finpos.merchant.presentation.alertdialogs.MiniStatementAlertDialog
import global.citytech.finpos.merchant.presentation.alertdialogs.MiniStatementDialogListener
import global.citytech.finpos.merchant.presentation.alertdialogs.PosEntryMode
import global.citytech.finpos.merchant.presentation.greenpin.GreenPinBaseTransactionActivity
import global.citytech.finpos.merchant.presentation.greenpin.GreenPinBaseTransactionViewModel
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.usecases.transaction.data.StatementItems
import kotlinx.android.synthetic.main.activity_green_pin.*
import kotlinx.android.synthetic.main.ministatement_list.*

class MiniStatementActivity :
    GreenPinBaseTransactionActivity<ActivityMiniStatementBinding, MiniStatementViewModel>(),
    View.OnClickListener, MiniStatementDialogListener {

    private lateinit var viewModel: MiniStatementViewModel
    private lateinit var configurationItem: ConfigurationItem
    private lateinit var miniStatementAlertDialog: MiniStatementAlertDialog
    private var bankLogo: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel()
        initBaseViews()
        initObservers()
        viewModel.getConfigurationItem()
        showPosEntrySelectionDialog()
        observeBankDisplayImage()
        viewModel.miniStatementResult.observe(this, {
            if (it) {
                showMiniStatementDialog(getViewModel().miniStatementData)
                Log.d("ministatementag",it.toString())

            }
        })


    }

    override fun getBindingVariable(): Int = BR.miniStatementViewModel

    override fun getLayout(): Int = R.layout.activity_mini_statement

    override fun getViewModel(): MiniStatementViewModel =
        ViewModelProviders.of(this)[MiniStatementViewModel::class.java]

    override fun onManualButtonClicked() {
        TODO("Not yet implemented")
    }

    override fun onCancelButtonClicked() {
        this.viewModel.shouldDispose = true
        this.viewModel.doCleanUp(GreenPinBaseTransactionViewModel.Task.RETURN_TO_DASHBOARD)
    }

    override fun getGreenPinBaseTransactionViewModel(): GreenPinBaseTransactionViewModel =
        this.viewModel


    override fun onAmountActivityResult(data: Intent?) {
        TODO("Not yet implemented")
    }

    override fun onManualActivityResult(data: Intent?) {
        TODO("Not yet implemented")
    }

    override fun getTransactionType(): TransactionType = TransactionType.MINI_STATEMENT

    override fun onClick(p0: View?) {
    }

    private fun showPosEntrySelectionDialog() {
        Glide.with(this).load(PosEntryMode.ACCEPT_ALL.loaderImage).into(image)
    }

    private fun initObservers() {
        this.viewModel.configurationItem.observe(this, {
            this.configurationItem = it
            this.viewModel.generateMiniStatementRequest(configurationItem, getTransactionType())
        })
    }

    private fun showMiniStatementDialog(miniStatementData: List<StatementItems>) {
        miniStatementAlertDialog = getViewModel().bankDisplayImage.value?.let {
            MiniStatementAlertDialog(
                miniStatementData,
                it, this
            )
        }!!
        miniStatementAlertDialog.isCancelable = false
        miniStatementAlertDialog.show(
            supportFragmentManager,
            MiniStatementAlertDialog.TAG
        )
    }

    override fun onPrintMiniStatementClicked() {
        if(!miniStatementAlertDialog.isButtonClicked){
            this.viewModel.printMiniStatement()
        }
    }

    override fun onCancelMiniStatementClicked() {
        returnToDashboard()
    }

    private fun observeBankDisplayImage() {
        getViewModel().bankDisplayImage.observe(this, Observer {
            this.bankLogo = Base64.encodeToString(it, false)
            Glide.with(this).load(it).into(iv_bank_logo_mini_statement)
        })

        getViewModel().defaultBankDisplayImage.observe(this, Observer {
            if (it)
                iv_bank_logo_mini_statement.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_full_finpos_logo
                    )
                )
        })
    }



}