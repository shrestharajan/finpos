package global.citytech.finposframework.listeners;


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/06/24 - 12:31 PM
 */

import java.io.Serializable;

public interface CardConfirmationListener extends Serializable {

    void onResult(boolean confirmed);

}
