package global.citytech.finpos.merchant.presentation.dashboard.cashmode

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.finpos.merchant.presentation.dashboard.base.BaseDashboardViewModel
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.usecases.TransactionType

/**
 * Created by Unique Shakya on 1/6/2021.
 * Modified by Rishav Chudal on 05/13/2021.
 */

class CashModeViewModel(context: Application): BaseDashboardViewModel(context) {

    private val logger = Logger(CashModeViewModel::class.java.name)

    val showCashAdvanceMenuLiveData by lazy { MutableLiveData<Boolean>() }

    override fun proceedWithEnabledTransactions(enabledTransactions: List<TransactionType>) {
       showCashAdvanceMenuLiveData.value = enabledTransactions.contains(TransactionType.CASH_ADVANCE)
    }

}