package global.citytech.finpos.merchant.presentation.transactions.qr

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.domain.model.app.QrPayment
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.APPROVED_PAYMENT_MOBILE
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.TRANSACTION_VOID_SALE
import global.citytech.finpos.merchant.presentation.utils.StringUtils
import global.citytech.finpos.merchant.utils.retrieveCurrencyName
import global.citytech.finposframework.usecases.TransactionType
import kotlinx.android.synthetic.main.item_transaction.view.*

class QrTransactionAdapter(
    private var qrPayments: MutableList<QrPayment>,
    private var listener: Listener
) :
    RecyclerView.Adapter<QrTransactionAdapter.QrTransactionViewHolder>() {
    lateinit var context: Context

    class QrTransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QrTransactionViewHolder {
        context = parent.context
        return QrTransactionViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_transaction, parent, false)
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: QrTransactionViewHolder, position: Int) {
        val qrPayment = qrPayments[position]
        holder.itemView.ll_txn_initiator.visibility = View.VISIBLE
        holder.itemView.tv_txn_type.text =
            "Type: ${
                if (qrPayment.transactionType.lowercase() == TRANSACTION_VOID_SALE) {
                    TransactionType.VOID.displayName
                } else {
                    qrPayment.transactionType
                }
            }"
        holder.itemView.tv_txn_initiator.setTypeface(holder.itemView.tv_txn_initiator.typeface, Typeface.BOLD)
        holder.itemView.tv_txn_initiator.text =
            if (qrPayment.paymentInitiator.equals("fonepay", true)){
                " fonePAY"
            }else{
                " Nepal Pay QR"
            }

        holder.itemView.tv_card_number.setTypeface(holder.itemView.tv_card_number.typeface, Typeface.BOLD)
        holder.itemView.tv_card_number.text = "Reference # :".plus("\n").plus(qrPayment.referenceNumber)
        holder.itemView.tv_invoice_number.visibility = View.GONE
        holder.itemView.tv_txn_time.text = qrPayment.transactionTime
        holder.itemView.tv_amount.text =
            "${retrieveCurrencyName(qrPayment.transactionCurrency)} ${
                StringUtils.decimalFormatterHigherCurrency(
                    qrPayment.transactionAmount
                )
            }"
        holder.itemView.setOnClickListener {
            listener.onItemClicked(qrPayment)
        }

        val drawableId =
            if (qrPayment.transactionStatus == APPROVED_PAYMENT_MOBILE) {
                if (qrPayment.transactionType.lowercase() == TRANSACTION_VOID_SALE) {
                    R.drawable.ic_check_circle_red_64dp
                } else {
                    R.drawable.ic_check_circle_green_64dp
                }
            } else {
                R.drawable.ic_failure_red_64dp
            }

        holder.itemView.iv_transaction_icon.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                drawableId
            )
        )
    }

    override fun getItemCount() = qrPayments.size

    fun update(transactions: MutableList<QrPayment>) {
        this.qrPayments = transactions
        notifyDataSetChanged()
    }

    fun updateAll(qrPayments: MutableList<QrPayment>) {
        this.qrPayments = qrPayments
        notifyDataSetChanged()
    }

    interface Listener {
        fun onItemClicked(qrPayment: QrPayment)
    }
}