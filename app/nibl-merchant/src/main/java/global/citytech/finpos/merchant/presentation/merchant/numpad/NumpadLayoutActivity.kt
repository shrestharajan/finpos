package global.citytech.finpos.merchant.presentation.merchant.numpad

import android.os.Bundle
import android.widget.RadioButton
import androidx.lifecycle.ViewModelProviders
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.databinding.ActivityNumpadLayoutBinding
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import kotlinx.android.synthetic.main.activity_numpad_layout.*

class NumpadLayoutActivity : AppBaseActivity<ActivityNumpadLayoutBinding, NumpadLayoutViewModel>() {
    private var selectedText = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObservers()
        initViews()
    }

    private fun initObservers(){
        getViewModel().numpadLayoutType.observe(this) {
            if (it.equals(rb_new_layout.text.toString())){
                rb_new_layout.isChecked = true
            }else{
                rb_old_layout.isChecked = true
            }
        }
    }

    private fun initViews(){
        getViewModel().getNumpadLayoutType(getString(R.string.title_old_layout))

        iv_back.setOnClickListener{
            getViewModel().saveNumpadLayoutType(selectedText)
            finish()
        }

        rg_numpad_layout.setOnCheckedChangeListener { radioGroup, checkId ->
            val radioButton = radioGroup.findViewById(checkId) as RadioButton
            selectedText = radioButton.text.toString()
        }
    }

    override fun getBindingVariable(): Int = BR.numpadLayoutViewModel

    override fun getLayout(): Int = R.layout.activity_numpad_layout

    override fun getViewModel(): NumpadLayoutViewModel = ViewModelProviders
        .of(this)[NumpadLayoutViewModel::class.java]

    override fun onPause() {
        super.onPause()
        getViewModel().saveNumpadLayoutType(selectedText)
    }
}