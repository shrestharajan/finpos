package global.citytech.finpos.merchant.presentation.mobilenfcpayment.receipt

import global.citytech.finposframework.hardware.io.printer.Printable
import global.citytech.finposframework.hardware.io.printer.PrinterRequest
import global.citytech.finposframework.hardware.io.printer.PrinterResponse
import global.citytech.finposframework.hardware.io.printer.PrinterService
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion
import global.citytech.finposframework.utility.ReceiptUtils.*
import global.citytech.finposframework.utility.StringUtils

class MobileNfcTransactionReceiptPrintHandler(val printerService: PrinterService) {

    fun printReceipt(
        mobileNfcPaymentReceipt: MobileNfcPaymentReceipt,
        receiptVersion: ReceiptVersion
    ): PrinterResponse {
        val printerRequest = preparePrinterRequest(mobileNfcPaymentReceipt, receiptVersion)
        return printerService.print(printerRequest)
    }

    private fun preparePrinterRequest(
        mobileNfcPaymentReceipt: MobileNfcPaymentReceipt,
        receiptVersion: ReceiptVersion
    ): PrinterRequest {
        val printableList = mutableListOf<Printable>()
        if (!StringUtils.isEmpty(mobileNfcPaymentReceipt.retailer.retailerLogo))
            addBase64Image(printableList, mobileNfcPaymentReceipt.retailer.retailerLogo)
        addSingleColumnString(
            printableList,
            mobileNfcPaymentReceipt.retailer.retailerName,
            MobileNfcPaymentReceiptStyle.RETAILER_NAME.getStyle()
        )
        addSingleColumnString(
            printableList,
            mobileNfcPaymentReceipt.retailer.retailerAddress,
            MobileNfcPaymentReceiptStyle.RETAILER_ADDRESS.getStyle()
        )
        addSingleColumnString(printableList, "  ", MobileNfcPaymentReceiptStyle.DIVIDER.getStyle())
        addDoubleColumnString(
            printableList,
            this.prepareDateString(mobileNfcPaymentReceipt.performance.startDateTime),
            this.prepareTimeString(mobileNfcPaymentReceipt.performance.startDateTime),
            MobileNfcPaymentReceiptStyle.DATE_TIME.getStyle()
        )
        addDoubleColumnString(
            printableList,
            this.merchantId(mobileNfcPaymentReceipt.retailer.merchantId),
            this.terminalId(mobileNfcPaymentReceipt.retailer.terminalId),
            MobileNfcPaymentReceiptStyle.MID_TID.getStyle()
        )
        addDoubleColumnString(
            printableList,
            MobileNfcPaymentReceiptLabel.REFERENCE_NUMBER,
            mobileNfcPaymentReceipt.mobileNfcTransactionDetail.referenceNumber,
            MobileNfcPaymentReceiptStyle.REFERENCE_NUMBER.getStyle()
        )
        addSingleColumnString(printableList, "  ", MobileNfcPaymentReceiptStyle.DIVIDER.getStyle())
        addSingleColumnString(
            printableList,
            mobileNfcPaymentReceipt.mobileNfcTransactionDetail.transactionType,
            MobileNfcPaymentReceiptStyle.TRANSACTION_TYPE.getStyle()
        )
        addSingleColumnString(
            printableList,
            prepareAmountString(mobileNfcPaymentReceipt),
            MobileNfcPaymentReceiptStyle.TRANSACTION_AMOUNT.getStyle()
        )
        addSingleColumnString(
            printableList,
            mobileNfcPaymentReceipt.mobileNfcTransactionDetail.transactionResult,
            MobileNfcPaymentReceiptStyle.TRANSACTION_RESULT.getStyle()
        )
        addSingleColumnString(printableList, "  ", MobileNfcPaymentReceiptStyle.DIVIDER.getStyle())
        addDoubleColumnString(
            printableList,
            MobileNfcPaymentReceiptLabel.PAYMENT_INITIATOR,
            mobileNfcPaymentReceipt.mobileNfcTransactionDetail.paymentInitiator,
            MobileNfcPaymentReceiptStyle.PAYMENT_INITIATOR.getStyle()
        )
        addDoubleColumnString(
            printableList,
            MobileNfcPaymentReceiptLabel.INITIATOR_ID,
            mobileNfcPaymentReceipt.mobileNfcTransactionDetail.initiatorId,
            MobileNfcPaymentReceiptStyle.INITIATOR_ID.getStyle()
        )
        addDoubleColumnString(
            printableList,
            if (mobileNfcPaymentReceipt.mobileNfcTransactionDetail.transactionType.toLowerCase() == "sale")
                MobileNfcPaymentReceiptLabel.APPROVAL_CODE else MobileNfcPaymentReceiptLabel.PROCESSING_NUMBER,
            mobileNfcPaymentReceipt.mobileNfcTransactionDetail.approvalCode,
            MobileNfcPaymentReceiptStyle.APPROVAL_CODE.getStyle()
        )
        addSingleColumnString(printableList, "  ", MobileNfcPaymentReceiptStyle.DIVIDER.getStyle())
        addSingleColumnString(
            printableList,
            mobileNfcPaymentReceipt.thankYouMessage,
            MobileNfcPaymentReceiptStyle.THANK_YOU_MESSAGE.getStyle()
        )
        addSingleColumnString(
            printableList,
            receiptVersion.versionLabel,
            MobileNfcPaymentReceiptStyle.RECEIPT_VERSION.getStyle()
        )
        addSingleColumnString(printableList, "  ", MobileNfcPaymentReceiptStyle.DIVIDER.getStyle())
        return PrinterRequest(printableList)
    }

    private fun prepareAmountString(mobileNfcPaymentReceipt: MobileNfcPaymentReceipt): String {
        val stringBuilder = StringBuilder()
        stringBuilder.append(mobileNfcPaymentReceipt.mobileNfcTransactionDetail.transactionCurrency)
        stringBuilder.append("\t")
        stringBuilder.append(mobileNfcPaymentReceipt.mobileNfcTransactionDetail.transactionAmount)
        return stringBuilder.toString()
    }

    private fun terminalId(terminalId: String): String {
        return this.formatLabelWithValue(MobileNfcPaymentReceiptLabel.TERMINAL_ID, terminalId)
    }

    private fun merchantId(merchantId: String): String {
        return this.formatLabelWithValue(MobileNfcPaymentReceiptLabel.MERCHANT_ID, merchantId)
    }

    private fun prepareTimeString(startDateTime: String): String {
        if (StringUtils.isEmpty(startDateTime) || startDateTime.length < 15) return ""
        return formatLabelWithValue(MobileNfcPaymentReceiptLabel.TIME, startDateTime.substring(10))
    }

    private fun prepareDateString(startDateTime: String): String {
        if (StringUtils.isEmpty(startDateTime) || startDateTime.length < 18) return ""
        return formatLabelWithValue(
            MobileNfcPaymentReceiptLabel.DATE,
            startDateTime.substring(0, 10)
        )
    }

    private fun formatLabelWithValue(label: String, value: String): String {
        val stringBuilder = StringBuilder()
        stringBuilder.append(label)
        stringBuilder.append(": ")
        stringBuilder.append(value)
        return stringBuilder.toString()
    }
}