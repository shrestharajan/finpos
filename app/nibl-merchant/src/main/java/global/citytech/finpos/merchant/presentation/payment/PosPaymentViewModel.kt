package global.citytech.finpos.merchant.presentation.payment

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.datasource.device.DeviceConfigurationSource
import global.citytech.finpos.merchant.data.repository.app.AppNotificationRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.AutoReversalQueueRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.reconciliation.ReconciliationRepositoryImpl
import global.citytech.finpos.merchant.data.repository.device.DeviceRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.Action
import global.citytech.finpos.merchant.domain.model.app.AppNotification
import global.citytech.finpos.merchant.domain.model.app.Type
import global.citytech.finpos.merchant.domain.model.device.DeviceResponse
import global.citytech.finpos.merchant.domain.model.device.DeviceResult
import global.citytech.finpos.merchant.domain.model.device.LoadParameterRequest
import global.citytech.finpos.merchant.domain.repository.device.DeviceRepository
import global.citytech.finpos.merchant.domain.usecase.app.AppNotificationUseCase
import global.citytech.finpos.merchant.domain.usecase.app.CoreAutoReversalQueueUseCase
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.core.reconciliation.CoreReconciliationUseCase
import global.citytech.finpos.merchant.domain.usecase.device.DeviceUseCase
import global.citytech.finpos.merchant.framework.datasource.app.*
import global.citytech.finpos.merchant.framework.datasource.core.reconciliation.ReconciliationDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.device.DeviceConfigurationSourceImpl
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.utils.retrieveCurrencyName
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.usecases.TransactionType
import global.citytech.finposframework.utility.StringUtils
import global.citytech.payment.sdk.api.PaymentResult
import global.citytech.payment.sdk.core.PaymentRequest
import global.citytech.payment.sdk.core.PurchaseType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Unique Shakya on 8/18/2021.
 */
class PosPaymentViewModel(val context: Application) : BaseAndroidViewModel(context) {

    val paymentResultLiveData by lazy { MutableLiveData<PaymentResult>() }
    val autoReversalPresent by lazy { MutableLiveData<Boolean>() }
    val settlementTimeElapsed by lazy { MutableLiveData<Boolean>() }
    val configurationItem by lazy { MutableLiveData<ConfigurationItem>() }
    var paymentRequest: PaymentRequest? = null
    lateinit var paymentTransactionType: TransactionType
    var deviceConfigurationSource: DeviceConfigurationSource
            = DeviceConfigurationSourceImpl(context)
    var deviceRepository: DeviceRepository
            = DeviceRepositoryImpl(deviceConfigurationSource)
    var deviceConfigurationUseCase = DeviceUseCase(deviceRepository)

    private val coreAutoReversalQueueUseCase = CoreAutoReversalQueueUseCase(
        AutoReversalQueueRepositoryImpl(
            AutoReversalQueueDataSourceImpl(context)
        )
    )

    private val coreReconciliationUseCase = CoreReconciliationUseCase(
        ReconciliationRepositoryImpl(
            ReconciliationDataSourceImpl()
        )
    )

    private val appNotificationUseCase = AppNotificationUseCase(
        AppNotificationRepositoryImpl(
            AppNotificationDataSourceImpl(context.applicationContext)
        )
    )

    private val localDataUseCase: LocalDataUseCase = LocalDataUseCase(
        LocalRepositoryImpl(
            LocalDatabaseSourceImpl(), PreferenceManager
        )
    )

    fun checkPaymentRequestValidityAndProceed(intentData: String?) {
        if (Jsons.isValidJsonString(intentData))
            retrievePaymentRequest(intentData)
        else
            paymentResultLiveData.value = PaymentResult.INVALID_DATA
    }

    private fun retrievePaymentRequest(intentData: String?) {
        try {
            paymentRequest = Jsons.fromJsonToObj(intentData, PaymentRequest::class.java)
            onPaymentRequestRetrieved()
        } catch (e: Exception) {
            e.printStackTrace()
            paymentResultLiveData.value = PaymentResult.INVALID_DATA
        }
    }

    private fun onPaymentRequestRetrieved() {
        paymentTransactionType = when (paymentRequest!!.purchaseType) {
            PurchaseType.PURCHASE -> TransactionType.PURCHASE
            PurchaseType.VOID -> TransactionType.VOID
            else -> TransactionType.PURCHASE
        }
    }

    fun checkForAutoReversal() {
        isLoading.value = true
        if (coreAutoReversalQueueUseCase.isQueueEmpty()) {
            Logger.getLogger("AutoReversal").log("::: AUTO REVERSAL ::: QUEUE IS EMPTY")
            autoReversalPresent.value = false
        } else {
            Logger.getLogger("AutoReversal").log("::: AUTO REVERSAL ::: QUEUE IS NOT EMPTY")
            autoReversalPresent.value = coreAutoReversalQueueUseCase.isActive()
        }
        isLoading.value = false
    }

    fun checkSettlementTime() {
        isLoading.value = true
        compositeDisposable.add(
            coreReconciliationUseCase.checkReconciliationRequired(
                configurationItem.value!!
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    settlementTimeElapsed.value = it
                    isLoading.value = false
                }, {
                    settlementTimeElapsed.value = false
                    isLoading.value = false
                })
        )
    }

    fun addSettlementPendingNotification() {
        appNotificationUseCase.addNotification(
            AppNotification(
                title = context.getString(R.string.title_settlement_pending),
                body = context.getString(R.string.msg_settlement_time_elapsed),
                action = Action.SETTLEMENT,
                read = false,
                type = Type.WARNING,
                timeStamp = StringUtils.dateTimeStamp()
            )
        )
    }

    fun getInvoiceDetail(): String {
        return when (paymentRequest!!.purchaseType!!) {
            PurchaseType.PURCHASE -> "Billing Amount:  ${paymentRequest!!.transactionAmount}"
            PurchaseType.VOID -> "Retrieval Reference Number: ${paymentRequest!!.retrievalReferenceNumber} \n Billed Amount: ${paymentRequest!!.transactionAmount}"
        }
    }

    fun getAmount(): String {
        return "${retrieveCurrencyName(DeviceConfiguration.get().currencyCode!!)} ${StringUtils.formatAmountTwoDecimal(
            paymentRequest!!.transactionAmount
        )}"
    }

    fun getConfigurationItem() {
        compositeDisposable.add(localDataUseCase.getWholeConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.mapToPresentation() }
            .subscribe(
                { configurationItem.value = it },
                {
                    paymentResultLiveData.value = PaymentResult.POS_PAYMENT_SERVICE_NOT_AVAILABLE
                })
        )
    }

    fun initializeSdk() {
        isLoading.value = true
        compositeDisposable.add(
            deviceConfigurationUseCase.initSdk()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::onSdkInitializedResponse,
                    this::onErrorInSdkInitialize
                )
        )
    }

    private fun onSdkInitializedResponse(deviceResponse: DeviceResponse) {
        if (deviceResponse.result == DeviceResult.SUCCESS) {
            configureTerminal()
        } else {
            paymentResultLiveData.value = PaymentResult.POS_NOT_CONFIGURED
        }
    }

    private fun onErrorInSdkInitialize(throwable: Throwable) {
        isLoading.value = false
        paymentResultLiveData.value = PaymentResult.POS_NOT_CONFIGURED
    }

    private fun configureTerminal() {
        isLoading.value = true
        compositeDisposable.add(
            deviceConfigurationUseCase.loadTerminalParameters(
                LoadParameterRequest(false)
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::onConfigureTerminalResponse,
                    this::onConfigureTerminalError
                )
        )
    }

    private fun onConfigureTerminalResponse(deviceResponse: DeviceResponse) {
        isLoading.value = false
        NiblMerchant.INSTANCE.isTerminalConfigured.value = deviceResponse.result == DeviceResult.SUCCESS
    }

    private fun onConfigureTerminalError(throwable: Throwable){
        isLoading.value = false
        paymentResultLiveData.value = PaymentResult.POS_NOT_CONFIGURED
    }
}