package global.citytech.crypto;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

import javax.crypto.Cipher;

import global.citytech.crypto.utils.Base64;

/**
 * Created by Rishav Chudal on 6/17/21.
 * Updated by Bikash Shrestha on 6/20/21.
 */
class PosCryptoImpl {

    private static final String DEFAULT_DATA = "";

    public String generatePublicKeyAndDecrypt(String publicKey, String data) {
        String decryptedData = DEFAULT_DATA;
        try {
            byte[] dateBytes = getBase64DecodedToBytesFromString(data);
            PublicKey generatedPublicKey = generatePublicKey(publicKey);
            decryptedData = decryptData(generatedPublicKey, dateBytes);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return decryptedData;
    }

    private PublicKey generatePublicKey(String publicKey) throws Exception {
        byte[] base64DecodedPublicKey = getBase64DecodedToBytesFromString(publicKey);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        EncodedKeySpec encodedKeySpec = new X509EncodedKeySpec(base64DecodedPublicKey);
        return keyFactory.generatePublic(encodedKeySpec);
    }

    private String encryptData(PrivateKey privateKey, byte[] dataInBytes)
            throws Exception {
        Cipher cryptoCipher = Cipher.getInstance("RSA");
        cryptoCipher.init(Cipher.ENCRYPT_MODE, privateKey);
        byte[] enBytes = null;
        for (int i = 0; i < dataInBytes.length; i += 64) {
            int toIndex = i + 64;
            if (dataInBytes.length < toIndex)
                toIndex = dataInBytes.length;
            byte[] doFinal = cryptoCipher.doFinal(Arrays.copyOfRange(dataInBytes, i, toIndex));
            enBytes = concatByte(enBytes, doFinal);
        }
        return getBase64EncodedToStringFromBytes(enBytes);
    }

    private byte[] concatByte(byte[] a, byte[] b) {
        if (a == null)
            return b;
        byte[] c = new byte[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }

    public String generatePrivateKeyAndEncrypt(String privateKey, String data) {
        String encryptedData = DEFAULT_DATA;
        try {
            byte[] dateBytes = data.getBytes();
            PrivateKey generatePrivateKey = generatePrivateKey(privateKey);
            encryptedData = encryptData(generatePrivateKey, dateBytes);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return encryptedData;
    }

    private PrivateKey generatePrivateKey(String privateKey) throws Exception {
        byte[] privateKeyBytes = getBase64DecodedToBytesFromString(privateKey);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        EncodedKeySpec encodedKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
        return keyFactory.generatePrivate(encodedKeySpec);
    }

    private String decryptData(PublicKey publicKey, byte[] dataInBytes) throws Exception {
        Cipher cryptoCipher = Cipher.getInstance("RSA");
        cryptoCipher.init(Cipher.DECRYPT_MODE, publicKey);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < dataInBytes.length; i += 128) {
            int toIndex = i + 128;
            if (dataInBytes.length < toIndex)
                toIndex = dataInBytes.length;
            byte[] doFinal = cryptoCipher.doFinal(Arrays.copyOfRange(dataInBytes, i, toIndex));
            sb.append(new String(doFinal));
        }
        return sb.toString();
    }

    private byte[] getBase64DecodedToBytesFromString(String data) {
        return Base64.decode(data);
    }

    private String getBase64EncodedToStringFromBytes(byte[] data) {
        return Base64.encodeToString(data, false);
    }
}
