package global.citytech.finpos.device.weipass.utils

import com.wiseasy.emvprocess.Process

/**
 * Created by Rishav Chudal on 4/9/20.
 */
object WeipassConstant {
    const val LOGGER_TAG: String = "finPos-cashier-WPos3"
    const val SUCCESS_SDK_INIT = "Success in SDK Initialization"
    const val FAILURE_SDK_INIT = "Failure in SDK Initialization"
    const val SUCCESS_KEY_INJECT = "Success in KEY Injection"
    const val FAILURE_KEY_INJECT = "Failure in KEY Injection"
    const val SUCCESS_KEY_CHECK = "Success, KEY Exist"
    const val FAILURE_KEY_CHECK = "Failure, KEY Does Not Exist"
    const val SUCCESS_KEY_ERASE = "Success in KEY Erase"
    const val FAILURE_KEY_ERASE = "Failure in KEY Erase"
    const val FAILURE_INVALID_PACKAGE_NAME = "Provided Package Name is Not Valid"
    const val FAILURE_INVALID_KEY = "Provided Key is Not Valid"
    const val FAILURE_EMV_PARAM_INJECT = "Failure in EMV Param Injection"
    const val SUCCESS_EMV_PARAM_INJECT = "Success in EMV Param Injection"
    const val TRANS_TYPE_SALE = Process.TYPE_SALE
    const val EMPTY_STRING = ""
    const val SUCCESS_EMV_KEYS_INJECTION = "Success in Emv Keys Injection"
    const val FAILURE_EMV_KEYS_INJECTION = "Failure in Emv Keys Injection"
    const val EMPTY_EMV_KEYS_LIST = "Emv Keys List is Empty"
    const val SUCCESS_EMV_KEYS_ERASE = "Success in Erasing All EMV Keys"
    const val FAILURE_EMV_KEYS_ERASE = "Failure in Erasing All EMV Keys"
    const val SUCCESS_REVOKED_KEYS_INJECTION = "Success in Revoked Keys Injection"
    const val FAILURE_REVOKED_KEYS_INJECTION = "Failure in Revoked Keys Injection"
    const val EMPTY_REVOKED_KEYS_LIST = "Revoked Keys List is Empty"
    const val SUCCESS_REVOKED_KEYS_ERASE = "Success in Erasing Revoked Keys"
    const val FAILURE_REVOKED_KEYS_ERASE = "Failure in Erasing Revoked Keys"
    const val EMPTY_AID_LIST = "AID List is Empty"
    const val SUCCESS_AID_INJECTION = "Success in AID Injection"
    const val FAILURE_AID_INJECTION = "Failure in AID Injection"
    const val FAILURE_AID_ERASE = "Failure in Erasing All AID Data"
    const val SUCCESS_AID_ERASE = "Success in Erasing All AID Data"
    const val NO_CARD_DETECTED = "No Card Detected"
    const val SLOT_OPEN_EMPTY = "Slots to open is emtpy"
    const val DETECT_ERROR_CASE_1 = "Read Card Failed"
    const val DETECT_ERROR_CASE_2 = "Got Card Magnetic Track Data, Data Encrypt failed"
    const val MAG_CARD_DETECTED = "Magnetic Card Detected"
    const val SWIPE_CARD_AGAIN = "SWIPE CARD AGAIN"
    const val DETECT_CARD_TIMEOUT = "Detect Card Timeout"
    const val DETECT_CARD_CANCEL = "Detect Card Cancelled"
    const val ICC_CARD_DETECTED = "ICC Card Detected"
    const val PICC_CARD_DETECTED = "PICC Card Detected"
    const val DEFAULT_CARD_READ_TIMEOUT = 30
    const val MAX_CARD_READ_TIMEOUT = 60
    const val NO_SUCH_SLOT_TO_OPEN = "No Such Slot to Open"
    const val CARD_TYPE_MAG_PRIOR_TO_CL = 0x40
    const val LED_ACTION_FAILURE = "Led Action Failure"
    const val LED_ACTION_SUCCESS = "Led Action Success"
    const val NETWORK_ENABLED_SUCCESS = "Network Enabled Success"
    const val NETWORK_DISABLED_SUCCESS = "Network Disabled Success"
    const val SETTING_DATA_TRANSMISSION_NETWORK_SUCCESS =
        "Setting Data Transmission Network Success"
    const val SETTING_APN_SUCCESS = "Setting APN Success"
    const val SDK_ENABLE_ADB_MODE = 0
    const val SDK_DISABLE_ADB_MODE = 1
    const val ADB_MODE_ENABLING_FAILED = "ADB Mode Enabling Failed"
    const val ADB_MODE_DISABLING_FAILED = "ADB Mode Disabling Failed"
    const val ADB_MODE_SETTINGS_APPLIED = "ADB Mode Settings Applied"
    const val FACTORY_RESET_FAILED = "Factory Reset Failed"
    const val SYSTEM_UPDATE_FAILED = "System Update Failed"
    const val SYSTEM_UPDATE_SUCCESS = "System Update Success"
    const val OTG_MODE_ENABLING_FAILED = "OTG Mode Enabling failed"
    const val OTG_MODE_DISABLING_FAILED = "OTG Mode Disabling failed"
    const val OTG_MODE_SETTINGS_APPLIED = "OTG Mode Settings Applied"
    const val MICROSD_MODE_ENABLING_FAILED = "MicroSD Mode Enabling failed"
    const val MICROSD_MODE_DISABLING_FAILED = "MicroSD Mode Disabling failed"
    const val MICROSD_MODE_SETTINGS_APPLIED = "MicroSD Mode Settings Applied"
    const val HOME_BUTTON_ENABLING_FAILED = "Home Button Enabling Failed"
    const val HOME_BUTTON_DISABLING_FAILED = "Home Button Disabling Failed"
    const val HOME_BUTTON_SETTINGS_APPLIED = "Home Button Settings Applied"
    const val POWER_BUTTON_ENABLING_FAILED = "Power Button Enabling Failed"
    const val POWER_BUTTON_DISABLING_FAILED = "Power Button Disabling Failed"
    const val POWER_BUTTON_SETTINGS_APPLIED = "Power Button Settings Applied"
    const val MSG_READ_CARD_FAIL = "Read Card Fail"
    const val MSG_APP_SELECTION_CANCELLED = "User Cancelled The Application Selection"
    const val MSG_PIN_ENTRY_CANCELLED = "User Cancelled The Pin Entry"
    const val MSG_PINPAD_FAILURE = "PinPad Failure"
    const val MSG_PINPAD_TIMEOUT = "PinPad Timeout"
    const val MSG_PINPAD_BYPASS = "PinPad Bypass"
    const val MSG_PINPAD_PAN_ERROR = "PinPad PAN Error"
    const val MSG_PROCESSING_ERROR = "Processing Error"
    const val MINM_PIN_LENGTH_DEFAULT = 4
    const val MAXM_PIN_LENGTH_DEFAULT = 12
    const val DEFAULT_PINPAD_TIMEOUT = 30
    const val MSG_CALLBACK_AMOUNT = "Emv Amount Callback"
    const val TAG_5F28 = 0x5F28
    const val TAG_9F42 = 0x9F42
    const val TAG_DF01_WPOS_ISSUER_SCRIPT_RESULTS = 0xDF01
    const val MSG_READ_CARD_SUCCESS = "Read Card Success"
    const val INVALID_CARD = "Invalid Card"
    const val MSG_MAG_CARD_READ_SUCCESS = "Magnetic Card Read Success"
    const val MSG_MAG_CARD_READ_FAIL = "Magnetic Card Read Failure"
    const val MSG_CONTACT_FALLBACK = "CONTACT FALLBACK TO MAGNETIC"
    const val MSG_APPLICATION_BLOCKED = "APPLICATION IS BLOCKED"
    const val MSG_CARD_BLOCKED = "CARD IS BLOCKED"
    const val MSG_OFFLINE_APPROVED = "OFFLINE APPROVED"
    const val MSG_OFFLINE_DECLINED = "OFFLINE DECLINED"
    const val MSG_ONLINE_APPROVED = "ONLINE APPROVED"
    const val MSG_ONLINE_DECLINED = "ONLINE DECLINED"
    const val MSG_ONLINE_FAIL_OFFLINE_APPROVED = "ONLINE FAIL OFFLINE APPROVED"
    const val MSG_ONLINE_FAIL_OFFLINE_DECLINED = "ONLINE FAIL OFFLINE DECLINED"
    const val MSG_CL_FALLBACK = "CONTACTLESS FALLBACK TO CONTACT";
    const val MSG_WAVE_AGAIN = "PLEASE WAVE AGAIN";
    const val MSG_CL_WAVE_FAIL = "CONTACTLESS WAVE FAIL"
    const val MSG_CL_READ_FAIL = "CONTACTLESS READ FAIL"
    const val MSG_CL_SECOND_PHASE_SUCCESS = "CONTACTLESS SECOND PHASE SUCCESS"
    const val MAX_BITMAP_HEIGHT_FOR_PRINTING = 5000
    const val MSG_ONLINE_SYNC_API_FAILED = "ONLINE SYNC API FAILED"
    const val MSG_ICC_READER_CARD_DETECT_ERROR = " Error in detecting card in Icc reader"

    /**
     * Printer Status if called via SDK api "getPrinterStatus"
     */
    const val SDK_API_getPrinterStatus_PRINTER_STATUS_OK = 0x00
    const val SDK_API_getPrinterStatus_PRINTER_PARAMETER_ERROR = 0x01
    const val SDK_API_getPrinterStatus_PRINTER_NOT_AVAILABLE = 0x06
    const val SDK_API_getPrinterStatus_PRINTER_OUT_OF_PAPER = 0x8A
    const val SDK_API_getPrinterStatus_PRINTER_OVER_HEAT = 0x8B

    /**
     * Printer Status if called via SDK api "getDeviceStatus"
     */
    const val SDK_API_getDeviceStatus_PRINTER_STATUS_OK = 0x00
    const val SDK_API_getDeviceStatus_PRINTER_STATUS_UNKNOWN = 0x01
    const val SDK_API_getDeviceStatus_PRINTER_STATUS_OUT_OF_PAPER = 0x10
    const val SDK_API_getDeviceStatus_PRINTER_STATUS_NOT_EXIST = 0xFF

    /**
     * Magnetic Reader Status if called via SDK api "getDeviceStatus"
     */
    const val SDK_MAG_READER_OK = 0x00
    const val SDK_MAG_READER_UNKNOWN = 0x01
    const val SDK_MAG_READER_NOT_EXIST = 0xFF

    /**
     * Contact Reader Status if called via SDK api "getDeviceStatus"
     */
    const val SDK_CONTACT_READER_OK = 0x00
    const val SDK_CONTACT_READER_UNKNOWN = 0x01
    const val SDK_CONTACT_READER_NOT_EXIST = 0xFF

    /**
     * Contactless Reader Status if called via SDK api "getDeviceStatus"
     */
    const val SDK_CONTACTLESS_READER_OK = 0x00
    const val SDK_CONTACTLESS_READER_UNKNOWN = 0x01
    const val SDK_CONTACTLESS_READER_NOT_EXIST = 0xFF

    /**
     * Led Values
     */
    const val SDK_LED_ACTION_ON = 1
    const val SDK_LED_ACTION_OFF = 0

    /**
     * SDK Specified Transaction Type
     */
    const val SDK_TRANS_PURCHASE_AND_PURCHASE_ADVICE = 0x02;
    const val SDK_TRANS_PURCHASE_WITH_CASHBACK = 0x04;
    const val SDK_TRANS_PREAUTH_AND_PREAUTH_EXTN = 0x02;
    const val SDK_TRANS_REFUND = 0x09;
    const val SDK_TRANS_CASH_ADVANCE = 0x01;
    const val SDK_TRANS_AUTH_VOID = 0x09;

    /**
     * DF8129 Outcome Messages
     */
    const val MSG_DF8129_APPROVED = "APPROVED"
    const val MSG_DF8129_DECLINED = "DECLINED"
    const val MSG_DF8129_ONLINE_REQUEST = "ONLINE REQUEST"
    const val MSG_DF8129_END_APPLICATIION = "END APPLICATION"
    const val MSG_DF8129_TRY_ANOTHER_INTERFACE = "TRY ANOTHER INTERFACE"
    const val MSG_DF8129_TRY_AGAIN = "TRY AGAIN"

    /**DF8116 Outcome Messages*/
    const val MSG_DF8116_SEE_PHONE = "SEE PHONE"

    /**
     *  Value obtained on parsing the outdata on card detect error in magnetic reader.
     */
    const val MAGNETIC_CARD_DETECT_ERROR_OUTDATA_VALUE = "80"

    /**
     *  Value obtained on parsing the outdata on card detect error in icc reader.
     */
    const val ICC_CARD_DETECT_ERROR_OUTDATA_VALUE = "82"
}