package global.citytech.finpos.device.weipass.card.read

import android.app.Activity
import android.content.Context
import com.wiseasy.emvprocess.Process.*
import com.wiseasy.emvprocess.TransProcess
import com.wiseasy.emvprocess.TransResult
import com.wiseasy.emvprocess.bean.OnlineResult
import com.wiseasy.emvprocess.bean.TransInfoBean
import global.citytech.finpos.device.weipass.R
import global.citytech.finpos.device.weipass.card.WeipassTransInfoBeanReadCard
import global.citytech.finpos.device.weipass.card.handler.ProcessInterfaceHandler
import global.citytech.finpos.device.weipass.card.handler.WeipassStateHandler
import global.citytech.finpos.device.weipass.detect.DetectCardRequest
import global.citytech.finpos.device.weipass.detect.DetectCardResponse
import global.citytech.finpos.device.weipass.detect.WeipassDetectCardServiceImpl
import global.citytech.finpos.device.weipass.led.WeipassLedServiceImpl
import global.citytech.finpos.device.weipass.sound.WeipassSoundServiceImpl
import global.citytech.finpos.device.weipass.utils.*
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_APPLICATION_BLOCKED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_CARD_BLOCKED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_CL_FALLBACK
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_DF8129_APPROVED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_DF8129_DECLINED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_DF8129_ONLINE_REQUEST
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_ONLINE_SYNC_API_FAILED
import global.citytech.finpos.device.weipass.utils.WeipassConstant.MSG_WAVE_AGAIN
import global.citytech.finpos.device.weipass.utils.WeipassConstant.SWIPE_CARD_AGAIN
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.exceptions.PosException
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.*
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService
import global.citytech.finposframework.hardware.io.led.LedAction
import global.citytech.finposframework.hardware.io.led.LedLight
import global.citytech.finposframework.hardware.io.led.LedRequest
import global.citytech.finposframework.hardware.io.sound.Sound
import global.citytech.finposframework.notifier.Notifier
import global.citytech.finposframework.utility.HexString
import java.lang.ref.WeakReference

/**
 * Created by Rishav Chudal on 6/6/20.
 */
abstract class WeipassReadCardTemplate(val context: Context, val notifier: Notifier) {
    private var contextWeakReference: WeakReference<Context> = WeakReference(context)
    private var weipassLedServiceImpl: WeipassLedServiceImpl = WeipassLedServiceImpl()
    private var weipassSoundServiceImpl: WeipassSoundServiceImpl =
        WeipassSoundServiceImpl(contextWeakReference.get()!!)
    private lateinit var readCardRequest: ReadCardRequest
    private lateinit var detectCardResponse: DetectCardResponse
    private lateinit var readCardResponse: ReadCardResponse

    private var iccThread: ICCThread? = null

    private var contactLessThread: PICCThread? = null

    /**
     * ReadCardDetails
     */
    fun executeReadCard(readCardRequest: ReadCardRequest): ReadCardResponse {
        this.readCardRequest = readCardRequest
        if (this.readCardRequest.packageName.isNullOrEmptyOrBlank()) {
            return onInvalidPackageName()
        }
        initializeTransactionThreads()
        WeipassStateHandler.clearPropertyInstances()
        WeipassStateHandler.contextWeakReference = contextWeakReference
        WeipassStateHandler.readCardRequest = this.readCardRequest
        WeipassStateHandler.transState = WeipassTransState.READ_CARD
        return doDetectCard()
    }

    private fun onInvalidPackageName(): ReadCardResponse {
        prepareReadCardResponse(
            null,
            Result.FAILURE,
            WeipassConstant.FAILURE_INVALID_PACKAGE_NAME
        )
        return this.readCardResponse
    }

    private fun initializeTransactionThreads() {
        this.iccThread = ICCThread(Runnable {
            processICCTransaction()
        }, contextWeakReference.get()!!.getString(R.string.contact_thread_name))

        this.contactLessThread = PICCThread(Runnable {
            processPICCTransaction()
        }, contextWeakReference.get()!!.getString(R.string.picc_thread_name))
    }

    private fun doDetectCard(): ReadCardResponse {
        this.detectCardResponse = WeipassDetectCardServiceImpl(context, notifier)
            .detectCard(prepareDetectCardRequest())
        return when (this.detectCardResponse.result) {
            Result.SUCCESS -> manageOnDetectCardResponseDeviceResultSuccess()
            Result.TIMEOUT -> manageOnDetectCardResponseDeviceResultTimeout()
            Result.WAVE_AGAIN -> manageOnDetectCardResponseResultWaveAgain()
            Result.SWIPE_AGAIN -> manageOnDetectCardResponseSwipeAgain()
            Result.ICC_CARD_DETECT_ERROR -> manageOnDetectCardResponseIccCardDetectError()
            else -> manageForOtherDetectCardResponseDeviceResult()
        }
    }

    private fun prepareDetectCardRequest() = DetectCardRequest(
        this.readCardRequest.cardTypes,
        WeipassDeviceUtils.getCardReadTimeoutOrDefault(this.readCardRequest.cardReadTimeOut),
        this.readCardRequest.packageName!!
    )

    private fun manageOnDetectCardResponseDeviceResultTimeout(): ReadCardResponse {
        prepareReadCardResponse(
            null,
            Result.TIMEOUT,
            WeipassConstant.DETECT_CARD_TIMEOUT
        )
        return this.readCardResponse
    }

    private fun manageForOtherDetectCardResponseDeviceResult(): ReadCardResponse {
        prepareReadCardResponse(
            null,
            this.detectCardResponse.result,
            this.detectCardResponse.message
        )
        return this.readCardResponse
    }

    private fun manageOnDetectCardResponseResultWaveAgain(): ReadCardResponse {
        val cardDetails = CardDetails()
        cardDetails.cardType = CardType.PICC
        prepareReadCardResponse(
            cardDetails,
            this.detectCardResponse.result,
            this.detectCardResponse.message
        )
        return this.readCardResponse
    }

    private fun manageOnDetectCardResponseSwipeAgain(): ReadCardResponse {
        val cardDetails = CardDetails()
        cardDetails.cardType = CardType.MAG
        prepareReadCardResponse(
            cardDetails,
            Result.SWIPE_AGAIN,
            SWIPE_CARD_AGAIN
        )
        return this.readCardResponse
    }

    private fun manageOnDetectCardResponseIccCardDetectError(): ReadCardResponse {
        val cardDetails = CardDetails()
        cardDetails.cardType = CardType.ICC
        prepareReadCardResponse(
            cardDetails,
            this.detectCardResponse.result,
            this.detectCardResponse.message
        )
        return this.readCardResponse
    }

    private fun manageOnDetectCardResponseDeviceResultSuccess(): ReadCardResponse {
        return when (this.detectCardResponse.cardDetected) {
            CardType.MAG -> manageForSlotDetectedMagnetic(this.detectCardResponse.outData!!)
            CardType.ICC -> manageForSlotDetectedICC()
            CardType.PICC -> manageForSlotDetectedPICC()
            else -> manageForSlotDetectedOther()
        }

    }

    private fun manageForSlotDetectedMagnetic(outData: ByteArray): ReadCardResponse {
        weipassSoundServiceImpl.playSound(Sound.SUCCESS)
        val cardDetails =
            CardDetails()
        return prepareReadCardResponseForMagneticCardData(cardDetails, outData)
    }

    private fun prepareReadCardResponseForMagneticCardData(
        cardDetails: CardDetails,
        outData: ByteArray
    ): ReadCardResponse {
        val trackData = TransProcess.getInstance().unpackTrackData(outData)
        if (trackData != null && !(trackData.track2.isNullOrEmptyOrBlank())) {
            cardDetails.cardType = CardType.MAG
            cardDetails.primaryAccountNumber = MagneticCardUtil.getMagPan(trackData.track2)
            cardDetails.expiryDate = MagneticCardUtil.getMagExpiryDate(trackData.track2)
            cardDetails.cardHolderName = MagneticCardUtil.getCardHolderName(trackData.track1)
            cardDetails.trackTwoData = trackData.track2
            cardDetails.amount = this.readCardRequest.amount
            cardDetails.cashBackAmount = this.readCardRequest.cashBackAmount
            prepareReadCardResponse(
                cardDetails,
                Result.SUCCESS,
                WeipassConstant.MSG_MAG_CARD_READ_SUCCESS
            )
            return readCardResponse
        } else {
            WeipassLogger.log("TrackData in Magnetic was null. So Swipe Again...")
            return manageOnDetectCardResponseSwipeAgain()
        }
    }

    private fun manageForSlotDetectedOther(): ReadCardResponse {
        prepareReadCardResponse(
            null,
            Result.FAILURE,
            WeipassConstant.NO_CARD_DETECTED
        )
        return readCardResponse
    }

    private fun prepareReadCardResponse(
        cardDetails: CardDetails?,
        result: Result,
        message: String
    ) {
        WeipassLogger.log("WeipassReadCardTemplate ::: Result ::: ".plus(result))
        WeipassLogger.log("WeipassReadCardTemplate ::: Message ::: ".plus(message))
        this.readCardResponse =
            ReadCardResponse(
                cardDetails,
                result,
                message
            )
        if (result != Result.WAVE_AGAIN || result != Result.FAILURE) {
            this.readCardResponse.piccCvm = EmvUtils
                .checkForPICCCvm(WeipassStateHandler.CURRENT_CARD_TYPE!!)
        }
        if (result != Result.SUCCESS)
            this.checkWithTransactionOutcome(this.readCardResponse.cardDetails)
        WeipassStateHandler.readCardResponse = this.readCardResponse
    }

    private fun checkWithTransactionOutcome(cardDetails: CardDetails?) {
        if (cardDetails != null && cardDetails.cardType == CardType.PICC) {
            try {
                val outComeData = TransResult.getOutCome()
                if (outComeData != null) {
                    when (outComeData.dF8129_Msg) {
                        WeipassConstant.MSG_DF8129_TRY_ANOTHER_INTERFACE -> {
                            this.readCardResponse = ReadCardResponse(
                                cardDetails,
                                Result.PICC_FALLBACK_TO_ICC,
                                "TRY ANOTHER INTERFACE"
                            )
                        }
                        WeipassConstant.MSG_DF8129_TRY_AGAIN -> {
                            this.readCardResponse =
                                ReadCardResponse(cardDetails, Result.WAVE_AGAIN, "TRY AGAIN")
                        }
                        WeipassConstant.MSG_DF8129_END_APPLICATIION -> {
                            this.readCardResponse =
                                ReadCardResponse(cardDetails, Result.FAILURE, "END APPLICATION")
                        }
                    }
                    if (outComeData.dF8116_Msg == WeipassConstant.MSG_DF8116_SEE_PHONE)
                        this.readCardResponse =
                            ReadCardResponse(cardDetails, Result.SEE_PHONE, "SEE PHONE")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun manageForSlotDetectedICC(): ReadCardResponse {
        weipassSoundServiceImpl.playSound(Sound.SUCCESS)
        WeipassLogger.log("Process For First Phase Contact Read Card Response...")
        initICCTransaction()
        WeipassStateHandler.awaitServiceCountDownLatch()
        this.readCardResponse = WeipassStateHandler.readCardResponse!!
        return this.readCardResponse
    }

    private fun initICCTransaction() {
        WeipassLogger.log("initContactTransaction")
        try {
            this.iccThread!!.setUncaughtExceptionHandler(this::manageICCThreadUncaughtException)
            WeipassLogger.log("Starting Contact Thread for Transaction...")
            this.iccThread!!.start()
        } catch (ex: Exception) {
            WeipassLogger.log("Exception Caught in Contact Trans...")
        }
    }

    private fun manageICCThreadUncaughtException(thread: Thread, ex: Throwable) {
        WeipassLogger.log("Thread name ::: ".plus(thread.name))
        WeipassLogger.log("Thread Exception ::: ".plus(ex.message))
        ex.printStackTrace()
        manageForICCThreadException()
        releaseTransactionThread()
        releaseLatches()
    }

    private fun manageForICCThreadException() {
        val cardDetails =
            CardDetails()
        cardDetails.cardType = CardType.ICC
        prepareReadCardResponse(
            cardDetails,
            Result.PROCESSING_ERROR,
            WeipassConstant.MSG_PROCESSING_ERROR
        )
    }

    private fun processICCTransaction() {
        val processInterfaceHandler = ProcessInterfaceHandler()
        val sdkApiResult = TransProcess.getInstance().doContactTransAsyncOnline(
            (contextWeakReference.get() as Activity),
            prepareSdkCommonTransInfo(CardType.ICC),
            processInterfaceHandler
        )
        WeipassLogger.log("doContactTransAsyncOnline ::: result (DEC) ::: ".plus(sdkApiResult))
        WeipassLogger.log(
            "doContactTransAsyncOnline ::: result (HEX) ::: "
                .plus(Integer.toHexString(sdkApiResult))
        )
        proceedForICCTransactionStatus(sdkApiResult)
    }

    private fun manageForSlotDetectedPICC(): ReadCardResponse {
        WeipassLogger.log("Process For First Phase Contactless Read Card Response...")
        initContactlessTransaction()
        WeipassStateHandler.awaitServiceCountDownLatch()
        this.readCardResponse = WeipassStateHandler.readCardResponse!!
        return this.readCardResponse
    }

    private fun initContactlessTransaction() {
        WeipassLogger.log("initContactlessTransaction")
        try {
            this.contactLessThread!!.setUncaughtExceptionHandler(
                this::manageContactlessThreadUncaughtException
            )
            WeipassLogger.log("Starting Contactless Thread for Transaction...")
            this.contactLessThread!!.start()
        } catch (ex: Exception) {
            WeipassLogger.log("Exception Caught in Contactless Trans...".plus(ex.message))
        }
    }

    private fun manageContactlessThreadUncaughtException(thread: Thread, ex: Throwable) {
        WeipassLogger.log("Thread name ::: ".plus(thread.name))
        WeipassLogger.log("Thread Exception ::: ".plus(ex.message))
        ex.printStackTrace()
        manageForContactlessThreadException()
        releaseTransactionThread()
        releaseLatches()
    }

    private fun manageForContactlessThreadException() {
        val cardDetails =
            CardDetails()
        cardDetails.cardType = CardType.PICC
        prepareReadCardResponse(
            cardDetails,
            Result.PROCESSING_ERROR,
            WeipassConstant.MSG_PROCESSING_ERROR
        )
    }

    private fun processPICCTransaction() {
        val processInterfaceHandler = ProcessInterfaceHandler()
        val sdkApiResult = TransProcess.getInstance().doCLTransAsyncOnline(
            (contextWeakReference.get() as Activity),
            prepareSdkCommonTransInfo(CardType.PICC),
            processInterfaceHandler
        )
        WeipassLogger.log("doContactTransAsyncOnline ::: result (DEC) ::: ".plus(sdkApiResult))
        WeipassLogger.log(
            "doContactTransAsyncOnline ::: result (HEX) ::: "
                .plus(Integer.toHexString(sdkApiResult))
        )
        proceedForPICCTransactionStatus(sdkApiResult)
    }

    private fun proceedForPICCTransactionStatus(sdkApiResult: Int) {
        val transState = WeipassStateHandler.transState!!
        if (WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler)) {
            continueProcessingPICCTransaction(sdkApiResult)
        } else {
            releaseTransactionThread()
            releaseLatches()
        }
    }

    private fun continueProcessingPICCTransaction(sdkApiResult: Int) {
        WeipassTransStateHandler.checkForSdkErrorCode(sdkApiResult, WeipassStateHandler)
        println("::: WEIPASS STATE HANDLER TRANS STATE === " + WeipassStateHandler.transState!!)
        when (WeipassStateHandler.transState!!) {
            WeipassTransState.FAILURE -> manageForPICCFailure()
            WeipassTransState.PROCESSING_ERROR -> manageForPICCProcessingError()
            WeipassTransState.PICC_FALLBACK -> manageForPICCFallback()
            WeipassTransState.APPLICATION_BLOCKED -> manageForPICCApplicationBlock()
            WeipassTransState.PICC_WAVE_AGAIN -> manageForPICCWaveAgain()
            else -> manageForPICCTransactionAnalyze(sdkApiResult)
        }
    }

    private fun manageForPICCFailure() {
        WeipassLogger.log("Contactless Failure Case...")
        turnLedAndPlaySound(LedLight.ALL, LedAction.OFF, null)
        turnLedAndPlaySound(LedLight.RED, LedAction.ON, null)
        prepareReadCardResponse(
            null,
            Result.FAILURE,
            WeipassConstant.MSG_CL_READ_FAIL
        )
        releaseTransactionThread()
        releaseLatches()
    }

    private fun manageForPICCProcessingError() {
        WeipassLogger.log("Contactless Processing Error Case...")
        val cardDetails =
            CardDetails()
        cardDetails.cardType = CardType.PICC
        prepareReadCardResponse(
            cardDetails,
            Result.PROCESSING_ERROR,
            WeipassConstant.MSG_PROCESSING_ERROR
        )
        releaseTransactionThread()
        releaseLatches()
    }

    private fun manageForPICCFallback() {
        WeipassLogger.log("Contactless Fallback to Contact Case...")
        val cardDetails =
            CardDetails()
        cardDetails.cardType = CardType.PICC
        prepareReadCardResponse(
            cardDetails,
            Result.PICC_FALLBACK_TO_ICC,
            MSG_CL_FALLBACK
        )
        releaseTransactionThread()
        releaseLatches()
    }

    private fun manageForPICCApplicationBlock() {
        WeipassLogger.log("Contactless Application Block Case...")
        val cardDetails =
            CardDetails()
        cardDetails.cardType = CardType.PICC
        prepareReadCardResponse(
            cardDetails,
            Result.APPLICATION_BLOCKED,
            MSG_APPLICATION_BLOCKED
        )
        releaseTransactionThread()
        releaseLatches()
    }

    private fun manageForPICCWaveAgain() {
        WeipassLogger.log("Contactless Wave Again Case...")
        val cardDetails =
            CardDetails()
        cardDetails.cardType = CardType.PICC
        prepareReadCardResponse(
            cardDetails,
            Result.WAVE_AGAIN,
            MSG_WAVE_AGAIN
        )
        releaseTransactionThread()
        releaseLatches()
    }

    private fun manageForPICCTransactionAnalyze(sdkApiResult: Int) {
        val tag8A = EmvUtils.retrieveFromTLV(EmvTagList.TAG_8A_AUTHORISATION_RESPONSE_CODE.tag)
        if (tag8A != null && !tag8A.data.isNullOrEmptyOrBlank()) {
            WeipassLogger.debug("::: TAG 8A in MANAGE FOR CONTACTLESS TRANSACTION :::".plus(tag8A.data))
            analyzeTag8AResult(tag8A)
        } else {
            analyzeOutComeForTransResult(sdkApiResult)
        }
    }

    private fun analyzeOutComeForTransResult(sdkApiResult: Int) {
        if (sdkApiResult == 0 &&
            EmvUtils.getOutComeDF8129Msg().equals(MSG_DF8129_APPROVED, true)
        ) {
            WeipassLogger.log("analyzeOutComeForTransResult ::: Offline Approved")
            this.manageForOfflineApprovedTransaction(null)
        } else if ((sdkApiResult == 0) &&
            (EmvUtils.getOutComeDF8129Msg().equals(MSG_DF8129_ONLINE_REQUEST, true))
        ) {
            WeipassLogger.log("analyzeOutComeForTransResult ::: Online Approved")
            this.manageForOnlineApprovedTransaction(null)
        } else if (sdkApiResult != 0 &&
            EmvUtils.getOutComeDF8129Msg().equals(MSG_DF8129_DECLINED, true)
        ) {
            WeipassLogger.log("analyzeOutComeForTransResult ::: Offline Declined")
            this.manageForOfflineDeclinedTransaction(null)
        } else {
            this.manageForDefaultCaseInTransaction(null)
        }
    }

    private fun prepareSdkCommonTransInfo(cardType: CardType): TransInfoBean {
        WeipassLogger.log("Prepare Read Card TransInfoBean...")
        val transactionType =
            WeipassDeviceUtils.getKernelTransType(this.readCardRequest.transactionType)
        WeipassLogger.log("SDK TransType ::: ".plus(transactionType))
        return WeipassTransInfoBeanReadCard(
            context,
            transactionType,
            readCardRequest,
            cardType
        )
            .buildSdkCommonTransInfo()
    }

    private fun proceedForICCTransactionStatus(sdkApiResult: Int) {
        val transState = WeipassStateHandler.transState!!
        WeipassLogger.debug("SDK trans state ::: ".plus(transState))
        if (WeipassTransStateHandler.doProceedFurther(transState, WeipassStateHandler)) {
            continueProcessingICCTransaction(sdkApiResult)
        } else {
            releaseTransactionThread()
            releaseLatches()
        }
    }

    private fun continueProcessingICCTransaction(sdkApiResult: Int) {
        WeipassTransStateHandler.checkForSdkErrorCode(sdkApiResult, WeipassStateHandler)
        val transState = WeipassStateHandler.transState!!
        WeipassLogger.debug("SDK trans state ::: ".plus(transState))
        when (transState) {
            WeipassTransState.FAILURE -> manageForICCFailure()
            WeipassTransState.PROCESSING_ERROR -> manageForICCProcessingError()
            WeipassTransState.ICC_FALLBACK -> manageForICCFallback()
            WeipassTransState.APPLICATION_BLOCKED -> manageForICCApplicationBlock()
            WeipassTransState.CARD_BLOCKED -> manageForICCCardBlock()
            WeipassTransState.ONLINE_SYNC_API_FAILED -> manageForICCOnlineSyncApiFailed()
            else -> manageForICCTransactionAnalyze(sdkApiResult)
        }
    }

    private fun manageForICCFailure() {
        WeipassLogger.log("Contact Failure Case...")
        prepareReadCardResponse(
            null,
            Result.FAILURE,
            WeipassConstant.MSG_READ_CARD_FAIL
        )
        releaseTransactionThread()
        releaseLatches()
    }

    private fun manageForICCCardBlock() {
        WeipassLogger.log("Contact Application Block Case...")
        val cardDetails = CardDetails()
        cardDetails.cardType = CardType.ICC
        prepareReadCardResponse(
            cardDetails,
            Result.CARD_BLOCKED,
            MSG_CARD_BLOCKED
        )
        releaseTransactionThread()
        releaseLatches()
    }

    private fun manageForICCProcessingError() {
        WeipassLogger.log("Contact Processing Error Case...")
        prepareReadCardResponse(
            null,
            Result.PROCESSING_ERROR,
            WeipassConstant.MSG_PROCESSING_ERROR
        )
        releaseTransactionThread()
        releaseLatches()
    }

    private fun manageForICCFallback() {
        WeipassLogger.log("Contact Fallback Case...")
        val cardDetails = CardDetails()
        cardDetails.cardType = CardType.ICC
        prepareReadCardResponse(
            cardDetails,
            Result.ICC_FALLBACK_TO_MAGNETIC,
            WeipassConstant.MSG_CONTACT_FALLBACK
        )
        releaseTransactionThread()
        releaseLatches()
    }

    private fun manageForICCApplicationBlock() {
        WeipassLogger.log("Contact Application Block Case...")
        val cardDetails = CardDetails()
        cardDetails.cardType = CardType.ICC
        prepareReadCardResponse(
            cardDetails,
            Result.APPLICATION_BLOCKED,
            MSG_APPLICATION_BLOCKED
        )
        releaseTransactionThread()
        releaseLatches()
    }

    private fun manageForICCOnlineSyncApiFailed() {
        WeipassLogger.log("Contact Online Sync Api Failed Case...")
        val cardDetails =
            CardDetails()
        cardDetails.transactionState = TransactionState.OFFLINE_DECLINED
        EmvUtils.prepareCardDetailsForTransaction(cardDetails)
        addTagDF01IfPresent(cardDetails)
        prepareReadCardResponse(
            cardDetails,
            Result.FAILURE,
            MSG_ONLINE_SYNC_API_FAILED
        )
        releaseTransactionThread()
        releaseLatches()
    }

    private fun manageForICCTransactionAnalyze(sdkApiResult: Int) {
        val tag8A = EmvUtils.retrieveFromTLV(EmvTagList.TAG_8A_AUTHORISATION_RESPONSE_CODE.tag)
        if (tag8A != null) {
            manageICCScriptResults()
            analyzeTag8AResult(tag8A)
        } else {
            manageForICCFailure()
        }
    }

    private fun manageICCScriptResults() {
        if (WeipassStateHandler.isOnlineProcess && WeipassStateHandler.doProcessScript) {
            WeipassLogger.log("Collecting Issuer Scripts Results...")
            val issuerScripResult = EmvUtils.getIssuerScriptResults()
            if (!issuerScripResult.isNullOrEmptyOrBlank()) {
                WeipassStateHandler.issuerScriptResults = issuerScripResult
            }
        }
    }

    private fun analyzeTag8AResult(tag8A: EmvTag) {
        val arcResponseCode = HexString.hexToString(tag8A.data).substring(0, 2)
        WeipassLogger.log("ARC Response Code ::: ".plus(arcResponseCode))
        when (arcResponseCode) {
            ARC_OFFLINEAPPROVED -> manageForOfflineApprovedTransaction(tag8A)
            ARC_OFFLINEDECLINED -> manageForOfflineDeclinedTransaction(tag8A)
            ARC_ONLINEFAILOFFLINEAPPROVED -> manageForOnlineFailOfflineApprovedTransaction(tag8A)
            ARC_ONLINEFAILOFFLINEDECLINED -> manageForOnlineFailOfflineDeclinedTransaction(tag8A)
            ARC_APPROVAL_00,
            ARC_APPROVAL_10,
            ARC_APPROVAL_11 -> manageForOnlineApprovedTransaction(tag8A)
            else -> manageForDefaultCaseInTransaction(tag8A)
        }
    }

    private fun manageForOfflineApprovedTransaction(tag8A: EmvTag?) {
        WeipassLogger.log(" Transaction Status ::: Offline Approved")
        WeipassStateHandler.transState = WeipassTransState.OFFLINE_APPROVED
        prepareReadCardResponseBasedOnCase(tag8A, WeipassStateHandler.transState!!)
    }

    private fun manageForOfflineDeclinedTransaction(tag8A: EmvTag?) {
        WeipassLogger.log(" Transaction Status ::: Offline Declined")
        WeipassStateHandler.transState = WeipassTransState.OFFLINE_DECLINED
        prepareReadCardResponseBasedOnCase(tag8A, WeipassStateHandler.transState!!)
    }

    private fun manageForOnlineFailOfflineApprovedTransaction(tag8A: EmvTag?) {
        WeipassLogger.log(" Transaction Status ::: Online Fail Offline Approved")
        WeipassStateHandler.transState = WeipassTransState.ONLINE_FAIL_OFFLINE_APPROVED
        prepareReadCardResponseBasedOnCase(tag8A, WeipassStateHandler.transState!!)
    }

    private fun manageForOnlineFailOfflineDeclinedTransaction(tag8A: EmvTag?) {
        WeipassLogger.log(" Transaction Status ::: Online Fail Offline Declined")
        WeipassStateHandler.transState = WeipassTransState.ONLINE_FAIL_OFFLINE_DECLINED
        prepareReadCardResponseBasedOnCase(tag8A, WeipassStateHandler.transState!!)
    }

    private fun manageForOnlineApprovedTransaction(tag8A: EmvTag?) {
        WeipassLogger.log("Transaction Status ::: Online Approved")
        WeipassStateHandler.transState = WeipassTransState.ONLINE_APPROVED
        prepareReadCardResponseBasedOnCase(tag8A, WeipassStateHandler.transState!!)
    }

    private fun manageForDefaultCaseInTransaction(tag8A: EmvTag?) {
        WeipassLogger.log("Transaction Status ::: Online Declined")
        WeipassStateHandler.transState = WeipassTransState.ONLINE_DECLINED
        prepareReadCardResponseBasedOnCase(tag8A, WeipassStateHandler.transState!!)
    }

    private fun prepareReadCardResponseBasedOnCase(tag8A: EmvTag?, transState: WeipassTransState) {
        val cardDetails =
            CardDetails()
        cardDetails.transactionState = WeipassDeviceUtils
            .getTransactionStateBasedOnWeipassTransState(transState)
        EmvUtils.prepareCardDetailsForTransaction(cardDetails)
        addTag8AIfPresent(tag8A, cardDetails)
        addTagDF01IfPresent(cardDetails)
        prepareReadCardResponse(
            cardDetails,
            WeipassDeviceUtils.getResultBasedOnWeipassTransState(transState),
            WeipassDeviceUtils.getResponseBasedOnWeipassTransState(transState)
        )
        releaseTransactionThread()
        releaseLatches()
    }

    private fun addTag8AIfPresent(tag8A: EmvTag?, cardDetails: CardDetails) {
        if (tag8A != null && tag8A.size != 0) {
            cardDetails.tagCollection!!.add(tag8A)
        }
    }

    private fun addTagDF01IfPresent(cardDetails: CardDetails) {
        if (WeipassStateHandler.doProcessScript) {
            val tag: Int = WeipassConstant.TAG_DF01_WPOS_ISSUER_SCRIPT_RESULTS
            val data = WeipassStateHandler.issuerScriptResults
            WeipassLogger.debug("Issuer Script Results ::: $data")
            if (!data.isNullOrEmptyOrBlank()) {
                val emvTag =
                    EmvTag(
                        tag,
                        data!!,
                        data.length / 2
                    )
                cardDetails.tagCollection!!.add(emvTag)
            }
        }
    }

    private fun releaseTransactionThread() {
        if (this.contactLessThread != null && this.contactLessThread!!.isAlive) {
            WeipassLogger.log("Releasing Contactless Transaction Thread...")
            this.contactLessThread!!.interrupt()
        }

        if (this.iccThread != null && this.iccThread!!.isAlive) {
            WeipassLogger.log("Releasing Contact Transaction Thread...")
            this.iccThread!!.interrupt()
        }
    }

    private fun releaseLatches() {
        WeipassLogger.log("Releasing All Countdown Latches...")
        WeipassStateHandler.releaseAllCountdownLatches()
    }

    /**
     * Read Emv
     */
    fun executeReadEmv(readCardRequest: ReadCardRequest): ReadCardResponse {
        WeipassStateHandler.contextWeakReference = this.contextWeakReference
        this.readCardRequest = readCardRequest
        WeipassStateHandler.CURRENT_CARD_TYPE = this.readCardRequest.cardTypes[0]
        WeipassStateHandler.readCardRequest = this.readCardRequest
        WeipassStateHandler.transState = WeipassTransState.READ_EMV
        WeipassStateHandler.releaseKernelCountDownLatch()
        WeipassStateHandler.awaitServiceCountDownLatch()
        this.readCardResponse = WeipassStateHandler.readCardResponse!!
        return this.readCardResponse
    }

    private fun getCardSummary(cardDetails: CardDetails): CardSummary {
        return CardSummary(
            cardSchemeLabel = cardDetails.cardSchemeLabel,
            primaryAccountNumber = cardDetails.primaryAccountNumber,
            cardHolderName = cardDetails.cardHolderName,
            expiryDate = cardDetails.expiryDate
        )
    }

    /**
     * Process Emv
     */
    fun executeProcessEmv(readCardRequest: ReadCardRequest): ReadCardResponse {
        WeipassStateHandler.contextWeakReference = this.contextWeakReference
        this.readCardRequest = readCardRequest
        WeipassStateHandler.CURRENT_CARD_TYPE = readCardRequest.cardDetailsBeforeEmvNext!!.cardType
        WeipassStateHandler.readCardRequest = this.readCardRequest
        WeipassStateHandler.cardSummary =
            getCardSummary(this.readCardRequest.cardDetailsBeforeEmvNext!!)
        WeipassStateHandler.transState = WeipassTransState.PROCESS_EMV
        return manageResponseBasedOnSlotType(WeipassStateHandler.CURRENT_CARD_TYPE!!)
    }

    private fun manageResponseBasedOnSlotType(cardType: CardType): ReadCardResponse {
        return when (cardType) {
            CardType.ICC -> manageForContactProcessEmv()
            CardType.PICC -> manageForContactlessProcessEmv()
            else -> throw PosException(
                PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD
            )
        }
    }


    private fun manageForContactProcessEmv(): ReadCardResponse {
        WeipassLogger.log("Process Emv for Contact...")
        WeipassStateHandler.releaseKernelCountDownLatch()
        WeipassStateHandler.awaitServiceCountDownLatch()
        this.readCardResponse = WeipassStateHandler.readCardResponse!!
        return this.readCardResponse
    }

    private fun manageForContactlessProcessEmv(): ReadCardResponse {
        return EmvUtils.prepareReadCardResponseForContactless(this.readCardRequest)
    }

    /**
     * Setting EMV Online Result
     */
    fun executeEmvOnlineResult(
        responseCode: Int, onlineResult: ReadCardService.OnlineResult,
        field55Data: String
    ): ReadCardResponse {
        WeipassStateHandler.contextWeakReference = this.contextWeakReference
        this.readCardRequest = WeipassStateHandler.readCardRequest!!
        WeipassStateHandler.CURRENT_CARD_TYPE =
            this.readCardRequest.cardDetailsBeforeEmvNext!!.cardType
        WeipassStateHandler.transState = WeipassTransState.SET_ONLINE_RESULT
        setOnlineResultBasedOn(responseCode, onlineResult, field55Data, true)
        WeipassStateHandler.releaseKernelCountDownLatch()
        WeipassStateHandler.awaitServiceCountDownLatch()
        this.readCardResponse = WeipassStateHandler.readCardResponse!!
        return this.readCardResponse
    }

    private fun setOnlineResultBasedOn(
        responseCode: Int,
        onlineResult: ReadCardService.OnlineResult,
        field55Data: String,
        isEmvProcess: Boolean
    ) {
        when (onlineResult) {
            ReadCardService.OnlineResult.ONLINE_APPROVED -> runOnlineApproveResult(
                field55Data,
                isEmvProcess
            )
            ReadCardService.OnlineResult.UNABLE_TO_GO_ONLINE -> runOfflineResult(
                isEmvProcess
            )
            else -> runOnlineDeclineResult(field55Data, isEmvProcess)
        }
    }

    private fun runOnlineApproveResult(
        field55Data: String,
        isEmvProcess: Boolean
    ) {
        WeipassLogger.log("runOnlineApproveResult...")
        if (isEmvProcess) {
            val onlineResult = OnlineResult()
            onlineResult.transStatus = ONLINE_APPROVAL
            if (!field55Data.isNullOrEmptyOrBlank()) {
                setField55DataInSDK(onlineResult, field55Data)
            }
            EmvUtils.clearEmvLogHistory()
            WeipassStateHandler.onlineResult = onlineResult
        } else {
            this.manageForOnlineApprovedTransaction(null)
        }
    }

    private fun setField55DataInSDK(
        onlineResult: OnlineResult,
        field55Data: String
    ) {
        onlineResult.dE39 = retrieveTag8AFromData(field55Data)
        val tag91Data: String = EmvUtils.getValueFromTLV(
            EmvTagList.TAG_91_ISSUER_AUTHENTICATION_DATA,
            field55Data
        )
        onlineResult.tag91Data = tag91Data
        val tag71Data: String = EmvUtils.getValueFromTLV(
            EmvTagList.TAG_71_ISSUER_SCRIPT_71,
            field55Data
        )
        onlineResult.tag71Data = tag71Data
        val tag72Data: String = EmvUtils.getValueFromTLV(
            EmvTagList.TAG_72_ISSUER_SCRIPT_72,
            field55Data
        )
        onlineResult.tag72Data = tag72Data
        doCheckForScriptProcessing(tag71Data, tag72Data)
    }

    private fun retrieveTag8AFromData(field55Data: String): String {
        var tag8A: String = EmvUtils.getValueFromTLV(
            EmvTagList.TAG_8A_AUTHORISATION_RESPONSE_CODE,
            field55Data
        )
        if (!tag8A.isNullOrEmptyOrBlank()) {
            tag8A = HexString.hexToString(tag8A)
        }
        WeipassLogger.debug("retrieveTag8AFromData ::: $tag8A")
        return tag8A
    }

    private fun doCheckForScriptProcessing(
        scriptData1: String,
        scriptData2: String
    ) {
        WeipassStateHandler.doProcessScript =
            !scriptData1.isNullOrEmptyOrBlank() || !scriptData2.isNullOrEmptyOrBlank()
    }

    private fun runOfflineResult(isEmvProcess: Boolean) {
        WeipassLogger.log("runOfflineResult...")
        if (isEmvProcess) {
            val onlineResult = OnlineResult()
            onlineResult.transStatus = ONLINE_FAILED
            WeipassStateHandler.onlineResult = onlineResult
        } else {
            this.manageForDefaultCaseInTransaction(null)
        }
    }

    private fun runOnlineDeclineResult(
        field55Data: String,
        isEmvProcess: Boolean
    ) {
        WeipassLogger.log("runOnlineDeclineResult...")
        if (isEmvProcess) {
            val onlineResult = OnlineResult()
            onlineResult.transStatus = ONLINE_DECLINE
            if (!field55Data.isNullOrEmptyOrBlank()) {
                setField55DataInSDK(onlineResult, field55Data)
            }
            WeipassStateHandler.onlineResult = onlineResult
        } else {
            this.manageForDefaultCaseInTransaction(null)
        }
    }

    /**
     * LED and Sound
     */
    private fun turnLedAndPlaySound(
        ledLight: LedLight,
        ledAction: LedAction,
        sound: Sound?
    ) {
        if (WeipassStateHandler.CURRENT_CARD_TYPE == CardType.PICC) {
            weipassLedServiceImpl.doTurnLedWith(
                LedRequest(
                    ledLight,
                    ledAction
                )
            )
            weipassSoundServiceImpl.playSound(sound)
        }
    }

    /**
     * Check ICC Status
     */
    fun executeIccCardStatus(): ReadCardResponse {
        var readCardResponse =
            ReadCardResponse(
                null,
                Result.ICC_CARD_NOT_PRESENT,
                WeipassConstant.EMPTY_STRING
            )
        WeipassDeviceUtils.callForSdkBreakOffCommand(this.context)
        WeipassLogger.log("Checking for Icc Card Present In Reader...")
        if (WeipassDeviceUtils.isICCPresentInDevice()) {
            readCardResponse =
                ReadCardResponse(
                    null,
                    Result.ICC_CARD_PRESENT,
                    WeipassConstant.EMPTY_STRING
                )
        }
        return readCardResponse
    }

    /**
     * Clean Up
     */
    fun executeCleanUp() {
        WeipassDeviceUtils.interruptAliveThread(
            contextWeakReference.get()!!
                .getString(R.string.picc_thread_name)
        )
        WeipassDeviceUtils.interruptAliveThread(
            contextWeakReference.get()!!
                .getString(R.string.contact_thread_name)
        )
        WeipassDeviceUtils.cleanUp(
            context,
            WeipassStateHandler,
            weipassLedServiceImpl
        )
    }

    /**
     * Contact Thread
     */
    class ICCThread constructor(
        target: Runnable,
        name: String
    ) : Thread(target, name)

    /**
     * Contactless Thread
     */
    private class PICCThread constructor(
        target: Runnable,
        name: String
    ) : Thread(target, name)

}