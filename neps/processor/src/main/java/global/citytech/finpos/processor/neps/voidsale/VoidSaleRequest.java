package global.citytech.finpos.processor.neps.voidsale;

import global.citytech.finpos.processor.neps.transaction.TransactionRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.io.sound.SoundService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;

/**
 * Created by Saurav Ghimire on 4/19/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */


public class VoidSaleRequest extends TransactionRequest {

    public static final class Builder {
        private global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest;
        private TransactionRepository transactionRepository;
        private ReadCardService readCardService;
        private DeviceController deviceController;
        private TransactionAuthenticator transactionAuthenticator;
        private PrinterService printerService;
        private ApplicationRepository applicationRepository;
        private LedService ledService;
        private SoundService soundService;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withTransactionRequest(global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest) {
            this.transactionRequest = transactionRequest;
            return this;
        }

        public Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public Builder withReadCardService(ReadCardService readCardService) {
            this.readCardService = readCardService;
            return this;
        }

        public Builder withDeviceController(DeviceController deviceController) {
            this.deviceController = deviceController;
            return this;
        }

        public Builder withTransactionAuthenticator(TransactionAuthenticator transactionAuthenticator) {
            this.transactionAuthenticator = transactionAuthenticator;
            return this;
        }

        public Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public Builder withLedService(LedService ledService) {
            this.ledService = ledService;
            return this;
        }

        public Builder withSoundService(SoundService soundService) {
            this.soundService = soundService;
            return this;
        }

        public VoidSaleRequest build() {
            VoidSaleRequest voidSaleRequestModel = new VoidSaleRequest();
            voidSaleRequestModel.transactionAuthenticator = this.transactionAuthenticator;
            voidSaleRequestModel.transactionRepository = this.transactionRepository;
            voidSaleRequestModel.readCardService = this.readCardService;
            voidSaleRequestModel.deviceController = this.deviceController;
            voidSaleRequestModel.transactionRequest = this.transactionRequest;
            voidSaleRequestModel.printerService = this.printerService;
            voidSaleRequestModel.applicationRepository = this.applicationRepository;
            voidSaleRequestModel.ledService = this.ledService;
            voidSaleRequestModel.soundService = this.soundService;
            return voidSaleRequestModel;
        }
    }
}
