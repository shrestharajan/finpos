package global.citytech.finpos.neps.iso8583;

/**
 * Created by Rishav Chudal on 1/7/22.
 */
public enum PosConditionCode {
    ATTENDANT_TERMINAL("00"),
    UNATTENDANT_TERMINAL("02");

    private final String value;

    PosConditionCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
