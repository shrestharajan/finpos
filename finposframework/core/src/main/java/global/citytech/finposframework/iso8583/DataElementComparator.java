package global.citytech.finposframework.iso8583;

import java.io.Serializable;
import java.util.Comparator;

/** User: Surajchhetry Date: 2/14/20 Time: 7:17 AM */
public class DataElementComparator implements Comparator<DataElement>, Serializable {

  public int compare(DataElement a, DataElement b) {
    return a.getIndex() - b.getIndex();
  }
}
