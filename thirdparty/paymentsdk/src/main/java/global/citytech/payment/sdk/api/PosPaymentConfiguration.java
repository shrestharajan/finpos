package global.citytech.payment.sdk.api;


/*
 * @author BikashShrestha
 * @project pos
 * @created 2021/06/18 - 11:47 AM
 */

public class PosPaymentConfiguration {

    private final String applicationIdentifier;
    private final String vendorName;
    private final String posPublicKey;
    private final String vendorPrivateKey;

    public PosPaymentConfiguration(String applicationIdentifier, String vendorName,
                                   String posPublicKey, String vendorPrivateKey) {
        this.applicationIdentifier = applicationIdentifier;
        this.vendorName = vendorName;
        this.posPublicKey = posPublicKey;
        this.vendorPrivateKey = vendorPrivateKey;
    }

    public String getApplicationIdentifier() {
        return applicationIdentifier;
    }

    public String getVendorName() {
        return vendorName;
    }

    public String getPosPublicKey() {
        return posPublicKey;
    }

    public String getVendorPrivateKey() {
        return vendorPrivateKey;
    }
}
