package global.citytech.finpos.merchant.domain.repository.app

import global.citytech.finpos.merchant.domain.model.app.*
import global.citytech.finpos.merchant.domain.model.device.DeviceResponse
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.presentation.model.ConfigResponse
import global.citytech.finpos.merchant.presentation.model.banner.Banner
import global.citytech.finpos.merchant.presentation.model.setting.TerminalSettingResponse
import global.citytech.finpos.merchant.presentation.purchase.PaymentItems
import global.citytech.finposframework.switches.posmode.PosMode
import global.citytech.finposframework.usecases.TransactionType
import io.reactivex.Observable


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 * Update by Bikash Shrestha on 11/09/2021
 */
interface LocalRepository {
    fun getHost(): Observable<List<Host>>
    fun getMerchant(): Observable<List<Merchant>>
    fun getLogo(): Observable<List<Logo>>
    fun getAidParams(): Observable<List<AidParam>>
    fun getCardScheme(): Observable<List<CardScheme>>
    fun getEmvKeys(): Observable<List<EmvKey>>
    fun getEmvParams(): Observable<List<EmvParam>>
    fun getMerchantTransactionLog(): Observable<List<MerchantTransactionLog>>
    fun getActivityLog(): Observable<List<ActivityLog>>
    fun getTransactionLogByStan(stan:String): Observable<TransactionLog>
    fun getTransactionLogByInvoice(invoiceNumber:String, ignoreBatchNumber: Boolean): Observable<TransactionLog>
    fun getTransactionLogByRRN(rrn:String): Observable<TransactionLog>
    fun getTransactionLogs(): Observable<List<TransactionLog>>
    fun getTransactionLogs(limit:Int,offset:Int): Observable<List<TransactionLog>>
    fun searchTransactionLogs(searchParam:String,limit: Int, offset: Int): Observable<List<TransactionLog>>
    fun countTransactionLogs():Int
    fun countSearchTransactionLogs(searchParam: String): Int
    fun saveMerchantTransactionLog(merchantTransactionLog: MerchantTransactionLog):Observable<DatabaseReponse>
    fun nukeActivityLog():Observable<DatabaseReponse>
    fun nukeMerchantTransactionLog():Observable<DatabaseReponse>
    fun deleteMerchantTransactionLogByStans(stans:List<String>):Observable<DatabaseReponse>
    fun saveConfigResponseAndInjectKey(configResponse: ConfigResponse): Observable<DeviceResponse>
    fun saveSettlementActivePref(value: Boolean)
    fun getSettlementActivePref(defaultValue: Boolean): Boolean
    fun saveTerminalConfigPref(value: Boolean)
    fun getTerminalConfigPref(defaultValue: Boolean): Boolean
    fun saveDebugModePreference(isDebugMode: Boolean)
    fun saveSoundModePreference(isDebugMode: Boolean)
    fun getDebugModePreference(defaultValue: Boolean): Boolean
    fun getSoundModePreference(defaultValue: Boolean): Boolean
    fun clearPreferences()
    fun isTransactionPresent(invoiceNumber: String): Observable<Boolean>
    fun isTransactionPresentByAuthCode(authCode: String): Observable<Boolean>
    fun isTransactionPresentByRrn(rrn: String): Observable<Boolean>
    fun setCardMonitoringPreference(cardMonitoring: Boolean)
    fun getCardMonitoringPreference(defaultValue: Boolean): Boolean
    fun setForceSettlementPreference(forceSettlement: Boolean)
    fun getForceSettlementPreference(defaultValue: Boolean): Boolean
    fun savePosMode(posMode: PosMode)
    fun getPosMode(defaultPosMode: PosMode): PosMode
    fun saveTipLimit(value: Int)
    fun saveNumpadLayoutType(numpadLayoutType: String)
    fun getNumpadLayoutType(defaultValue: String): String
    fun retrieveTipLimit(): Int
    fun getAdminPassword(): Observable<String>
    fun getEnabledTransactionsForThePosMode(posMode: PosMode): Observable<List<TransactionType>>
    fun getSettlementTime(defaultValue: String): String
    fun storeSettlementTime(settlementTime: String)
    fun getLastSuccessSettlementTimeInMillis(defaultValue: Long): Long
    fun updateLastSuccessSettlementTimeInMillis(settledTime: Long)
    fun updateReconciliationBatchNumber(batchNumber: Int)
    fun getExistingSettlementBatchNumber(): Observable<String>
    fun getTipAdjustedTransactionLogOfInvoiceNumber(invoiceNumber: String): Observable<TransactionLog>
    fun getDeviceConfiguration(): DeviceConfiguration
    fun saveCardConfirmationRequiredPref(value: Boolean)
    fun getCardConfirmationRequiredPref(defaultValue: Boolean): Boolean
    fun saveVatRefundSupportedPref(value: Boolean)
    fun getVatRefundSupportedPref(defaultValue: Boolean): Boolean
    fun saveShouldShufflePinPadPref(value: Boolean)
    fun getShouldShufflePinPadPref(defaultValue: Boolean): Boolean
    fun saveMerchantCredential(value: String)
    fun getMerchantCredential(): String
    fun getBanners(): List<Banner>
    fun setBanners(banners: List<Banner>)
    fun deleteTransactionByStan(stan: String): Observable<Boolean>
    fun saveTerminalSettings(terminalSettingResponse: TerminalSettingResponse)
    fun savePaymentInitiatorItems(paymentItems: MutableList<PaymentItems>)
    fun updateShowQrDisclaimerSetting(showDisclaimer: Boolean)
    fun getShowQrDisclaimerSetting(): Boolean
    fun getTerminalSettings(): TerminalSettingResponse
    fun getPaymentInitiatorItems(): ArrayList<PaymentItems>
    fun isDisclaimerShown(): Boolean
    fun saveDisclaimerShownStatusPref(value: Boolean)
    fun getInvoiceNumberByRrn(rrn: String): Observable<String>
    fun insertToActivityLog(activityLog: ActivityLog): Observable<Boolean>
    fun addToDisclaimers(disclaimer: Disclaimer): Observable<Boolean>
    fun addListToDisclaimers(disclaimers: List<Disclaimer>): Observable<Boolean>
    fun retrieveAllDisclaimers(): Observable<List<Disclaimer>>
    fun retrieveDisclaimerById(disclaimerId: String): Observable<Disclaimer>
    fun updateRemarksById(disclaimerId: String, remarks: String): Observable<Boolean>
    fun deleteById(disclaimerId: String): Observable<Boolean>
    fun deleteMerchantTransactionLogsOfGivenCountFromInitial(count: Int): Observable<DatabaseReponse>
    fun saveAppDownloadExtra(value: String)
    fun getAppDownloadExtra():String
    fun getStoredAppVersion(): Int
    fun saveAppVersion(version: Int)
    fun isSwitchConfigActive(): Boolean
    fun setSwitchConfigActive(active: Boolean)
    fun isBroadcastAcknowledgeActive(): Boolean
    fun setBroadcastAcknowledgeActive(active: Boolean)
    fun isAppResetActive(active: Boolean):Boolean
    fun setAppResetActive(active: Boolean)
    fun nukeAllDatabaseTable():Observable<DatabaseReponse>
    fun getAmountPerTransaction(): Observable<Long>
    fun getAmountLengthLimit(): Int
    fun saveAmountLengthLimit(value: Int)
    fun getMerchantTransactionLogsOfGivenCount(count: Int): Observable<List<MerchantTransactionLog>>
    fun getFonePayLoginState(defaultValue: Boolean): Boolean
    fun saveFonePayLoginState(value: Boolean)
    fun saveGreenPinStatus(value: Boolean)
    fun getGreenPinStatus() : Boolean
}