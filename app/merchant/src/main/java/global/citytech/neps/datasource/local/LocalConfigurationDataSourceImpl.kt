package global.citytech.neps.datasource.local

import global.citytech.neps.App
import global.citytech.neps.data.datasource.local.LocalDataSource
import global.citytech.neps.data.db.AppDatabase
import global.citytech.neps.domain.model.*
import io.reactivex.Observable


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
class LocalConfigurationDataSourceImpl :
    LocalDataSource {
    override fun getHost(): Observable<List<Host>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(App.INSTANCE).getHostDao().getHostList()
        }
    }

    override fun getMerchant(): Observable<List<Merchant>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(App.INSTANCE).getMerchantDao().getMerchantList()
        }
    }

    override fun getLogo(): Observable<List<Logo>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(App.INSTANCE).getLogoDao().getLogoList()
        }
    }

    override fun getAidParams(): Observable<List<AidParam>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(App.INSTANCE).getAidParamDao().getAidParamList()
        }
    }

    override fun getCardScheme(): Observable<List<CardScheme>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(App.INSTANCE).getCardSchemeDao().getCardScheme()
        }
    }

    override fun getEmvKeys(): Observable<List<EmvKey>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(App.INSTANCE).getEmvKeyDao().getEmvKeys()
        }
    }

    override fun getEmvParams(): Observable<List<EmvParam>> {
        return Observable.fromCallable {
            AppDatabase.getInstance(App.INSTANCE).getEmvParamDao().getEmvParams()
        }
    }

}