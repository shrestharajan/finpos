package global.citytech.finpos.merchant.domain.model.app

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import global.citytech.finpos.merchant.domain.model.app.TransactionConfig.Companion.TABLE_NAME

/**
 * Created by Unique Shakya on 9/24/2020.
 */

@Entity(tableName = TABLE_NAME)
data class TransactionConfig(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_STAN)
    var stan: Long? = 0,
    @ColumnInfo(name = COLUMN_RRN)
    var rrn: Long? = 0,
    @ColumnInfo(name = COLUMN_INVOICE_NUMBER)
    var invoiceNumber: Long? = 0,
    @ColumnInfo(name = COLUMN_RECONCILIATION_BATCH_NUMBER)
    var reconciliationBatchNumber: Long? = 0
) {

    companion object {
        const val TABLE_NAME = "transaction_configs"
        const val COLUMN_STAN = "stan"
        const val COLUMN_RRN = "rrn"
        const val COLUMN_INVOICE_NUMBER = "invoice_number"
        const val COLUMN_RECONCILIATION_BATCH_NUMBER = "reconciliation_batch_number"
    }
}