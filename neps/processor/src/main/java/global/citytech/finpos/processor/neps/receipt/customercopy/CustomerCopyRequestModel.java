package global.citytech.finpos.processor.neps.receipt.customercopy;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.receipt.customercopy.CustomerCopyRequestParameter;

/**
 * Created by Saurav Ghimire on 1/26/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class CustomerCopyRequestModel implements UseCase.Request, CustomerCopyRequestParameter {
    private TransactionRepository transactionRepository;
    private PrinterService printerService;

    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public static final class Builder {
        TransactionRepository transactionRepository;
        PrinterService printerService;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public CustomerCopyRequestModel build() {
            CustomerCopyRequestModel customerCopyRequestModel = new CustomerCopyRequestModel();
            customerCopyRequestModel.transactionRepository = this.transactionRepository;
            customerCopyRequestModel.printerService = this.printerService;
            return customerCopyRequestModel;
        }
    }
}
