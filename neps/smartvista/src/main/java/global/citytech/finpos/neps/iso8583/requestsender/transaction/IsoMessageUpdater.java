package global.citytech.finpos.neps.iso8583.requestsender.transaction;

import global.citytech.finposframework.iso8583.Iso8583Msg;

public class IsoMessageUpdater {
    CardTypeISOMessageUpdater cardTypeISOMessageUpdater;
    TransactionTypeISOMessageUpdater transactionTypeISOMessageUpdater;

    public IsoMessageUpdater(CardTypeISOMessageUpdater cardTypeISOMessageUpdater, TransactionTypeISOMessageUpdater transactionTypeISOMessageUpdater) {
        this.cardTypeISOMessageUpdater = cardTypeISOMessageUpdater;
        this.transactionTypeISOMessageUpdater = transactionTypeISOMessageUpdater;
    }

    Iso8583Msg update(TransactionIsoRequest transactionIsoRequest, Iso8583Msg iso8583Msg) {
        iso8583Msg = cardTypeISOMessageUpdater.update(transactionIsoRequest, iso8583Msg);
        iso8583Msg = transactionTypeISOMessageUpdater.update(transactionIsoRequest, iso8583Msg);
        return iso8583Msg;
    }
}
