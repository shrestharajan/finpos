package global.citytech.finpos.nibl.admin

import android.app.Application
import android.content.ServiceConnection
import global.citytech.tms.sdk.IPlatformManager

class NiblAdmin : Application() {
    companion object {
        lateinit var INSTANCE: NiblAdmin
    }

    lateinit var iPlatformManager: IPlatformManager
    lateinit var serviceConnection: ServiceConnection

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }

    fun unbindService() {
        unbindService(serviceConnection)
    }
}