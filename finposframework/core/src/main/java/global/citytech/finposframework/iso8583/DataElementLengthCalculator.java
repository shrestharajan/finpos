package global.citytech.finposframework.iso8583;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import global.citytech.finposframework.utility.EncodingUtils;

/***
 * @author Surajchhetry
 * @since 1.0
 */
public class DataElementLengthCalculator {
    private DataElementLengthCalculator() {
    }

    public static byte[] lengthAsByte(DataElement dataElement, DataElementConfig deConfig) {
        int length = calculateLength(dataElement, deConfig);
        try (ByteArrayOutputStream lengthInByte = new ByteArrayOutputStream()) {
            if (deConfig.getLengthConfig().getLengthType() == LengthConfig.LengthType.LLVAR) {
                if (deConfig.getLengthConfig().getEncodingType() == EncodingType.BCD)
                    lengthInByte.write(EncodingUtils.hex2Bytes(String.valueOf(toLLVarLengthFormat(length))));
                else {
                    lengthInByte.write(EncodingUtils.asciiRepresentationAsByte(toLLVarLengthFormat(length)));
                }
            } else {
                if (deConfig.getLengthConfig().getEncodingType() == EncodingType.BCD) {
                    lengthInByte.write(EncodingUtils.hex2Bytes(String.valueOf(toLLLVarLengthFormat(length))));
                } else {
                    lengthInByte.write(EncodingUtils.asciiRepresentationAsByte(toLLLVarLengthFormat(length)));
                }
            }
            return lengthInByte.toByteArray();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static String toLLVarLengthFormat(int length) {
        if (length <= 9)
            return String.format("0%d", length);
        return String.format("%d", length);
    }

    private static String toLLLVarLengthFormat(int length) {
        if (length <= 9)
            return String.format("%03d", length);
        if (length > 9 && length <= 99)
            return String.format("0%d", length);
        return String.format("%d", length);
    }

    private static int calculateLength(DataElement dataElement, DataElementConfig deConfig) {
        int length = dataElement.getData().toString().length();
        // Data content is z type so instead of creating new data type just made it static data type.
        if (dataElement.getIndex() == DataElement.TRACK_II_DATA) {
            return length;
        }
        if (dataElement.getIndex() == DataElement.PRIMARY_ACCOUNT_NUMBER) { // TODO temporary solution for now, change later
            return length;
        }
        if (deConfig.getDataConfig().getEncodingType() == EncodingType.BCD)
            length = length / 2;
        return length;
    }
}
