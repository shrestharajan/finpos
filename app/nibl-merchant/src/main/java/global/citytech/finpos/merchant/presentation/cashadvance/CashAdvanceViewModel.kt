package global.citytech.finpos.merchant.presentation.cashadvance

import android.app.Application
import global.citytech.finpos.merchant.data.repository.core.cashadvance.CashAdvanceRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.core.cashadvance.CashAdvanceDataSourceImpl
import global.citytech.finpos.merchant.domain.model.cashadvance.CashAdvanceResponseEntity
import global.citytech.finpos.merchant.domain.usecase.core.cashadvance.CoreCashAdvanceUseCase
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.cashadvance.CashAdvanceRequestItem
import global.citytech.finpos.merchant.presentation.model.cashadvance.ManualCashAdvanceRequestItem
import global.citytech.finpos.merchant.presentation.purchase.BaseTransactionViewModel
import global.citytech.finposframework.usecases.TransactionType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal

/**
 * Created by Unique Shakya on 2/4/2021.
 */
class CashAdvanceViewModel(application: Application) : BaseTransactionViewModel(application) {

    private val cashAdvanceUseCase =
        CoreCashAdvanceUseCase(CashAdvanceRepositoryImpl(CashAdvanceDataSourceImpl()))

    fun performCashAdvance(
        configurationItem: ConfigurationItem,
        amount: BigDecimal,
        transactionType: TransactionType
    ) {
        isLoading.value = true
        compositeDisposable.add(
            cashAdvanceUseCase.performCashAdvance(
                configurationItem, prepareCashAdvanceRequest(
                    amount,
                    transactionType
                )
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    if (!shouldDispose) {
                        handleCashAdvanceResponse(it)
                    } else {
                        completePreviousTask()
                    }
                }, {
                    it.printStackTrace()
                    isLoading.value = false
                    if (!shouldDispose) {
                        transactionComplete.value = false
                        message.value = it.message
                    } else {
                        completePreviousTask()
                    }
                })
        )
    }

    private fun handleCashAdvanceResponse(cashAdvanceResponseEntity: CashAdvanceResponseEntity) {
        showTransactionConfirmationDialog(cashAdvanceResponseEntity)
    }

    private fun showTransactionConfirmationDialog(it: CashAdvanceResponseEntity) {
        prepareBase64UrlToDisplayQr(
            it.isApproved!!,
            it.message!!,
            it.shouldPrintCustomerCopy!!,
            it.stan!!
        )
    }

    private fun prepareCashAdvanceRequest(
        amount: BigDecimal,
        transactionType: TransactionType
    ): CashAdvanceRequestItem {
        return CashAdvanceRequestItem(amount, transactionType)
    }

    fun performManualCashAdvance(
        configurationItem: ConfigurationItem,
        amount: BigDecimal,
        transactionType: TransactionType,
        cardNumber: String,
        expiryDate: String
    ) {
        isLoading.value = true
        compositeDisposable.add(
            this.cashAdvanceUseCase.performManualCashAdvance(
                configurationItem,
                prepareManualCashAdvanceRequest(amount, transactionType, cardNumber, expiryDate)
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    handleCashAdvanceResponse(it)
                }, {
                    it.printStackTrace()
                    isLoading.value = false
                    transactionComplete.value = false
                    message.value = it.message
                })
        )
    }

    private fun prepareManualCashAdvanceRequest(
        amount: BigDecimal,
        transactionType: TransactionType,
        cardNumber: String,
        expiryDate: String
    ): ManualCashAdvanceRequestItem {
        return ManualCashAdvanceRequestItem(
            amount,
            transactionType,
            cardNumber,
            expiryDate
        )
    }
}