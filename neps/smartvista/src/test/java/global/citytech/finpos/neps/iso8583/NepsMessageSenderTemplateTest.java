package global.citytech.finpos.neps.iso8583;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import global.citytech.finposframework.iso8583.BitMap;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.DefaultMessageParser;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.SpecInfo;

import static org.junit.Assert.*;

/**
 * Created by Saurav Ghimire on 1/26/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class NepsMessageSenderTemplateTest {

    SpecInfo specInfo = new NepsSpecInfo();
    DefaultMessageParser messageParser;

    @Before
    public void setup(){
        messageParser = new DefaultMessageParser(specInfo);
    }

    @Test
    public void testSplitDataFroMStream(){
        BitMap bitMap = new BitMap();
        bitMap.set(2);
        bitMap.set(3);
        bitMap.set(4);
        bitMap.set(7);
        bitMap.set(11);
        bitMap.set(12);
        bitMap.set(37);
        bitMap.set(39);
        bitMap.set(41);
        bitMap.set(49);
        bitMap.set(55);
        List<DataElement> elements ;

        String hexData = "31363434333833303031323332383735343030303030303030303030303030303538373830313236313234373431303030303035323130313236313234373430303030333030373836363331393133343132333430323735323400048A023936";

        Iso8583Msg message = messageParser.parse(hextToByte(hexData));
        System.out.println("ISO MESSAGE :::: "+message.getPrintableData());

    }

    public byte[] hextToByte(String str){
        byte[] val = new byte[str.length() / 2];
        for (int i = 0; i < val.length; i++) {
            int index = i * 2;
            int j = Integer.parseInt(str.substring(index, index + 2), 16);
            val[i] = (byte) j;
        }
        return val;
    }
}