package global.citytech.finpos.processor.neps.logon;

import global.citytech.finposframework.switches.logon.LogOnResponseParameter;
import global.citytech.finposframework.supports.UseCase;

/** User: Surajchhetry Date: 2/28/20 Time: 6:03 PM */
public final class LogOnResponseModel implements UseCase.Response, LogOnResponseParameter {
  private String debugRequestMessage;
  private String debugResponseMessage;
  private boolean success;

  public LogOnResponseModel(Builder builder) {
    this.debugRequestMessage = builder.debugRequestMessage;
    this.debugResponseMessage = builder.debugResponseMessage;
    this.success = builder.success;
  }

  public String getDebugRequestMessage() {
    return debugRequestMessage;
  }

  public String getDebugResponseMessage() {
    return debugResponseMessage;
  }

  public boolean isSuccess() {
    return success;
  }

  @Override
  public String toString() {
    return "LogOnResponseModel{"
        + "debugRequestMessage='"
        + debugRequestMessage
        + '\''
        + ", debugResponseMessage='"
        + debugResponseMessage
        + '\''
        + ", success="
        + success
        + '}';
  }

  public static final class Builder {
    private String debugRequestMessage;
    private String debugResponseMessage;
    private boolean success;
    private String pekKey;

    private Builder() {}

    public static Builder createDefaultBuilder() {
      return new Builder();
    }

    public Builder debugRequestMessage(String debugRequestString) {
      this.debugRequestMessage = debugRequestString;
      return this;
    }

    public Builder debugResponseMessage(String debugResponseString) {
      this.debugResponseMessage = debugResponseString;
      return this;
    }


    public Builder success(boolean success) {
      this.success = success;
      return this;
    }

    public LogOnResponseModel build() {
      return new LogOnResponseModel(this);
    }
  }
}
