package global.citytech.finposframework.supports;

import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.comm.Request;

/** User: Surajchhetry Date: 2/19/20 Time: 5:21 PM */
public interface MessagePayload {

  byte[] getAsBytes();
  Iso8583Msg getRequestMsg();
  Request getRequestPacket();
  void setRequestPacket(Request requestPacket);

  class Default implements MessagePayload {

    private byte[] message;
    private Iso8583Msg requestMsg;
    private Request requestPackage;

    public Default(byte[] message, Iso8583Msg requestMsg) {
      this.message = message;
      this.requestMsg = requestMsg;
    }

    @Override
    public byte[] getAsBytes() {
      return message;
    }

    @Override
    public Iso8583Msg getRequestMsg() {
      return requestMsg;
    }

    @Override
    public Request getRequestPacket() {
      return requestPackage;
    }

    @Override
    public void setRequestPacket(Request requestPacket) {
      this.requestPackage = requestPacket;
    }
  }

  interface Builder {
    // TODO : Iso8583Msg msg
    MessagePayload build();
  }
}
