package global.citytech.finpos.processor.neps.batchupload;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import global.citytech.finpos.neps.iso8583.requestsender.batchupload.BatchUploadISORequest;
import global.citytech.finpos.neps.iso8583.requestsender.batchupload.BatchUploadRequestSender;
import global.citytech.finpos.processor.neps.transaction.NepsIsoMessageReceiptHandler;
import global.citytech.finpos.processor.neps.transaction.TransactionType;
import global.citytech.finpos.processor.neps.transaction.TransactionUtils;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.emv.emvparam.EmvParametersRequest;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.log.AppState;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.batchupload.BatchUploadRequester;
import global.citytech.finposframework.usecases.activity.ActivityLog;
import global.citytech.finposframework.usecases.activity.ActivityLogStatus;
import global.citytech.finposframework.usecases.activity.ActivityLogType;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.TransactionLog;
import global.citytech.finposframework.utility.AmountUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.StackTraceUtil;
import global.citytech.finposframework.utility.StringUtils;

/**
 * Created by Saurav Ghimire on 6/14/21.
 * sauravnghimire@gmail.com
 */


public class BatchUploadUseCase implements UseCase<BatchUploadRequestModel, BatchUploadResponseModel>,
        BatchUploadRequester<BatchUploadRequestModel, BatchUploadResponseModel> {

    private final Logger logger = Logger.getLogger(BatchUploadUseCase.class.getName());
    private final TerminalRepository terminalRepository;
    private final Notifier notifier;
    private BatchUploadRequestModel batchUploadRequestModel;
    private String currentReconciliationBatch;
    private RequestContext batchTransactionRequestContext;


    public BatchUploadUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        this.terminalRepository = terminalRepository;
        this.notifier = notifier;
    }

    @Override
    public BatchUploadResponseModel execute(BatchUploadRequestModel requestModel) {
        notifier.notify(Notifier.EventType.BATCH_UPLOADING, "Please wait...");
        batchUploadRequestModel = requestModel;
        currentReconciliationBatch = terminalRepository.getReconciliationBatchNumber();
        List<TransactionLog> transactionLogs = batchUploadRequestModel.getTransactionRepository()
                .getTransactionLogsByBatchNumber(currentReconciliationBatch);
        try {
            processTransactionLogsForBatchUpload(transactionLogs);
        } catch (FinPosException finPosException) {
            return onFinPosExceptionOccurred(finPosException);
        } catch (Exception exception) {
            return onOtherExceptionOccurred(exception);
        }
        return new BatchUploadResponseModel(Result.SUCCESS, "SUCCESS");
    }

    private void processTransactionLogsForBatchUpload(List<TransactionLog> transactionLogs) {
        int totalCount = transactionLogs.size();
        int currentCount = 1;
        for (TransactionLog transactionLog: transactionLogs) {
            if (isTransactionNotNeededForBatchUpload(transactionLog)) {
                logger.debug("::: Not Uploading Void Sale or Void Transaction :::");
                totalCount--;
                continue;
            }
            notifyBatchUploadProgress(currentCount, totalCount);
            sendIndividualBatchUploadRequest(transactionLog, currentCount, totalCount);
            currentCount++;
        }
    }

    private boolean isTransactionNotNeededForBatchUpload(TransactionLog transactionLog) {
        return this.isVoidedSaleTransaction(transactionLog) ||
                this.isVoidTransaction(transactionLog) ||
                this.isVoidedTipAdjustment(transactionLog);
    }

    private boolean isVoidedSaleTransaction(TransactionLog transactionLog) {
        return transactionLog.getTransactionType() ==
                global.citytech.finposframework.usecases.TransactionType.PURCHASE &&
                transactionLog.isTransactionVoided();
    }

    private boolean isVoidTransaction(TransactionLog transactionLog) {
        return transactionLog.getTransactionType() ==
                global.citytech.finposframework.usecases.TransactionType.VOID;
    }

    private boolean isVoidedTipAdjustment(TransactionLog transactionLog) {
        return transactionLog.getTransactionType() ==
                global.citytech.finposframework.usecases.TransactionType.TIP_ADJUSTMENT &&
                transactionLog.isTransactionVoided();
    }

    private void notifyBatchUploadProgress(int currentCount, int totalCount) {
        notifier.notify(
                Notifier.EventType.BATCH_UPLOAD_PROGRESS,
                retrieveBatchUploadMessage(currentReconciliationBatch, currentCount, totalCount)
        );
    }

    private void sendIndividualBatchUploadRequest(
            TransactionLog transactionLog,
            int currentTransactionLogCount,
            int totalTransactionLogCount
    ) {
        batchTransactionRequestContext = prepareRequestContextForTransaction(transactionLog);
        BatchUploadRequestSender batchUploadRequestSender = new BatchUploadRequestSender(
                batchTransactionRequestContext
        );
        IsoMessageResponse isoMessageResponse = batchUploadRequestSender.send();
        logger.debug("::: Batch Upload Response Received :::");
        logger.debug("::: DE-39 ::: " +
                IsoMessageUtils.retrieveFromDataElementsAsString(
                        isoMessageResponse,
                        DataElement.RESPONSE_CODE
                )
        );
        printDebugReceiptIfDebugMode(isoMessageResponse);
        printBatchUploadReceiptIfAppStateIsDebug(
                transactionLog,
                isoMessageResponse,
                batchUploadRequestModel.getPrinterService(),
                currentTransactionLogCount == 1,
                currentTransactionLogCount == totalTransactionLogCount,
                currentReconciliationBatch
        );
        terminalRepository.incrementSystemTraceAuditNumber();
    }

    private RequestContext prepareRequestContextForTransaction(TransactionLog transactionLog) {
        ConnectionParam connectionParam = new ConnectionParam(
                terminalRepository.findPrimaryHost(),
                terminalRepository.findSecondaryHost()
        );
        String stan = terminalRepository.getSystemTraceAuditNumber();
        RequestContext requestContext = new RequestContext(
                stan,
                terminalRepository.findTerminalInfo(),
                connectionParam
        );
        requestContext.setRequest(prepareBatchUploadRequest(transactionLog));
        return requestContext;
    }

    private BatchUploadISORequest prepareBatchUploadRequest(TransactionLog transactionLog) {
        TransactionType transactionType = TransactionUtils.mapPurchaseTypeWithTransactionType(transactionLog.getTransactionType());
        CardDetails cardDetails = transactionLog.getReadCardResponse().getCardDetails();
        EmvParametersRequest emvParametersRequest = batchUploadRequestModel.getApplicationRepository().retrieveEmvParameterRequest();
        return BatchUploadISORequest.Builder.newInstance()
                .pan(cardDetails.getPrimaryAccountNumber())
                .originalProcessingCode(transactionType.getProcessingCode())
                .transactionAmount(transactionLog.getTransactionAmount())
                .localTxnDateAndTime(transactionLog.getTransactionDate().concat(transactionLog.getTransactionTime()))
                .expirationDate(cardDetails.getExpiryDate())
                .posEntryMode(transactionLog.getPosEntryMode())
                .posConditionCode(transactionLog.getPosConditionCode())
                .originalRrn(transactionLog.getRrn())
                .invoiceNumber(transactionLog.getInvoiceNumber())
                .track2Data(transactionLog.getReadCardResponse().getCardDetails().getTrackTwoData())
                .emvData(transactionLog.getReadCardResponse().getCardDetails().getIccDataBlock())
                .currencyCode(emvParametersRequest.getTransactionCurrencyCode())
                .pinBlock(transactionLog.getReadCardResponse().getCardDetails().getPinBlock())
                .build();
    }

    private void printBatchUploadReceiptIfAppStateIsDebug(TransactionLog transactionLog, IsoMessageResponse isoMessageResponse,
                                                          PrinterService printerService, boolean shouldPrintBatchUploadHeader,
                                                          boolean shouldFeedPaperAfterFinish, String batchNumber) {
        if (!AppState.getInstance().isDebug()) {
            return;
        }
        BatchUploadReceipt batchUploadReceipt = BatchUploadReceipt.Builder.newInstance()
                .batchNumber(batchNumber)
                .amount(transactionLog.getReceiptLog().getAmountWithCurrency())
                .approvalCode(transactionLog.getAuthCode())
                .cardNumber(StringUtils.encodeAccountNumber(transactionLog.getReadCardResponse().getCardDetails().getPrimaryAccountNumber()))
                .cardScheme(transactionLog.getReadCardResponse().getCardDetails().getCardScheme().getCardScheme())
                .invoiceNumber(transactionLog.getInvoiceNumber())
                .responseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
                .transactionType(transactionLog.getTransactionType().getPrintName())
                .transactionDate(transactionLog.getTransactionDate())
                .transactionTime(transactionLog.getTransactionTime())
                .build();
        new BatchUploadReceiptPrintHandler(printerService).printBatchUploadReceipt(batchUploadReceipt,
                shouldPrintBatchUploadHeader, shouldFeedPaperAfterFinish);
    }

    private void printDebugReceiptIfDebugMode(IsoMessageResponse isoMessageResponse) {
        boolean isDebugVersion = this.terminalRepository.findTerminalInfo().isDebugModeEnabled();
        if (isDebugVersion) {
            PrinterService printerService = batchUploadRequestModel.getPrinterService();
            ReceiptHandler.IsoMessageReceiptHandler isoMessageReceiptHandler
                    = new NepsIsoMessageReceiptHandler(printerService);
            isoMessageReceiptHandler.printIsoMessageReceipt(isoMessageResponse);
        }
    }

    private String retrieveBatchUploadMessage(String batchNumber, int count, int totalSize) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Batch Number: ");
        stringBuilder.append(batchNumber);
        stringBuilder.append("\n");
        stringBuilder.append("Uploading .. ");
        stringBuilder.append("\n");
        stringBuilder.append("[ ");
        stringBuilder.append(count);
        stringBuilder.append(" / ");
        stringBuilder.append(totalSize);
        stringBuilder.append(" ]");
        return stringBuilder.toString();
    }

    private BatchUploadResponseModel onFinPosExceptionOccurred(FinPosException finPosException) {
        logger.error(finPosException.getDetailMessage());
        updateActivityLogOnFinPosException(finPosException);
        return new BatchUploadResponseModel(Result.FAILURE, "FAILURE");
    }

    private BatchUploadResponseModel onOtherExceptionOccurred(Exception exception) {
        logger.error(exception.getLocalizedMessage());
        updateActivityLog(StackTraceUtil.getStackTraceAsString(exception));
        return new BatchUploadResponseModel(Result.FAILURE, "FAILURE");
    }

    private void updateActivityLogOnFinPosException(FinPosException finPosException) {
        try {
            BatchUploadISORequest request
                    = (BatchUploadISORequest) batchTransactionRequestContext.getRequest();
            TerminalInfo terminalInfo = terminalRepository.findTerminalInfo();
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(terminalRepository.getSystemTraceAuditNumber())
                    .withType(ActivityLogType.BATCH_ADVICE)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(currentReconciliationBatch)
                    .withAmount(AmountUtils.toLowerCurrencyFromBigDecimalInLong(request.getTransactionAmount()))
                    .withResponseCode("")
                    .withIsoRequest("")
                    .withIsoResponse(finPosException.getIsoResponse())
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(
                            finPosException.getLocalizedMessage() +
                                    " : " +
                                    finPosException.getDetailMessage()
                    )
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .build();
            this.terminalRepository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update " +
                    ActivityLogType.BATCH_ADVICE.name()
                    + " activity log :: "
                    + e.getMessage());
        }
    }

    private void updateActivityLog(String remarks) {
        try {
            BatchUploadISORequest request
                    = (BatchUploadISORequest) batchTransactionRequestContext.getRequest();
            TerminalInfo terminalInfo = terminalRepository.findTerminalInfo();
            ActivityLog activityLog = ActivityLog.Builder.newInstance()
                    .withStan(terminalRepository.getSystemTraceAuditNumber())
                    .withType(ActivityLogType.BATCH_ADVICE)
                    .withMerchantId(terminalInfo.getMerchantID())
                    .withTerminalId(terminalInfo.getTerminalID())
                    .withBatchNumber(currentReconciliationBatch)
                    .withAmount(AmountUtils.toLowerCurrencyFromBigDecimalInLong(request.getTransactionAmount()))
                    .withResponseCode("")
                    .withIsoRequest("")
                    .withIsoResponse("")
                    .withStatus(ActivityLogStatus.FAILED)
                    .withRemarks(remarks)
                    .withPosDateTime(new Timestamp(new Date().getTime()))
                    .build();
            this.terminalRepository.updateActivityLog(activityLog);
        } catch (Exception e) {
            this.logger.log("Unable to update " +
                    ActivityLogType.BATCH_ADVICE.name()
                    + " activity log :: "
                    + e.getMessage());
        }
    }
}
