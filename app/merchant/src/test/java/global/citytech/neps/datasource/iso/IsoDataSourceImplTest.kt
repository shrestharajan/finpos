package global.citytech.neps.datasource.iso

import global.citytech.neps.datasource.core.configuration.ConfigurationDataSourceImpl
import global.citytech.neps.presentation.model.ConfigurationItem
import io.mockk.MockKAnnotations
import org.junit.Before
import org.junit.Test

class IsoDataSourceImplTest{
    lateinit var  SUT : ConfigurationDataSourceImpl


    @Before
    fun setUp(){
        MockKAnnotations.init(this)
        SUT =
            ConfigurationDataSourceImpl()
    }

    @Test
    fun logOn(){

        val configurationItem = ConfigurationItem()
        SUT.logOn(configurationItem)
    }

}