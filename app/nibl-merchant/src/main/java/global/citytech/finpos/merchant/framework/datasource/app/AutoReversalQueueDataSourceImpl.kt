package global.citytech.finpos.merchant.framework.datasource.app

import android.content.Context
import global.citytech.easydroid.core.preference.SecurePreference
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.data.datasource.app.autoreversal.AutoReversalQueueDataSource
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.usecases.transaction.data.AutoReversal
import global.citytech.finposframework.utility.StringUtils

/**
 * Created by Unique Shakya on 6/22/2021.
 */
class AutoReversalQueueDataSourceImpl(context: Context) : AutoReversalQueueDataSource {

    private var securePreference: SecurePreference
    private val logger: Logger

    companion object {
        const val PREFERENCE_NAME = "AUTO-REVERSAL-PREFERENCE"
        const val PREFERENCE_KEY = "AUTO-REVERSAL-KEY"
    }

    init {
        securePreference = SecurePreference(context.applicationContext, PREFERENCE_NAME)
        logger = Logger.getLogger(AutoReversalQueueDataSourceImpl::class.java.name)
    }

    override fun isQueueEmpty(): Boolean {
        return getSize().toInt() == 0
    }

    override fun isActive(): Boolean {
        if (isQueueEmpty())
            return false
        return getRecentRequest()!!.status == AutoReversal.Status.ACTIVE
    }

    override fun getSize(): Long {
        return getAllQueueRequests().size.toLong()
    }

    override fun getAllQueueRequests(): MutableList<AutoReversal> {
        val data = getAsString()
        var list = emptyList<AutoReversal>().toMutableList()
        data?.let {
            list = Jsons.fromJsonToList<AutoReversal>(data, Array<AutoReversal>::class.java)
                .toMutableList()
        }
        logger.log("::: AUTO REVERSAL >>> ALL QUEUE REQUESTS SIZE::: ${list.size}")
        list.sortWith(AutoReversalListComparator())
        return list
    }

    override fun getRecentRequest(): AutoReversal? {
        if (isQueueEmpty())
            return null
        return getAllQueueRequests()[0]
    }

    override fun removeRequest(autoReversal: AutoReversal) {
        val list = getAllQueueRequests()
        if (list.isNotEmpty()) {
            val copyList = copy(list)
            copyList.remove(autoReversal)
            copyList.sortWith(AutoReversalListComparator())
            update(copyList)
        }
    }

    override fun addRequest(autoReversal: AutoReversal) {
        var list = getAllQueueRequests()
        if (list.isEmpty())
            list = mutableListOf()
        val copyList = copy(list)
        copyList.add(autoReversal)
        copyList.sortWith(AutoReversalListComparator())
        update(copyList)
    }

    override fun incrementRetryCount(autoReversal: AutoReversal) {
        val list = getAllQueueRequests()
        if (list.isNotEmpty()){
            val copyList = copy(list)
            val newAutoReversal = AutoReversal(autoReversal.transactionType,
            autoReversal.reversalRequest, autoReversal.retryCount + 1,
            autoReversal.status, StringUtils.dateTimeStamp())
            copyList.remove(autoReversal)
            copyList.add(newAutoReversal)
            copyList.sortWith(AutoReversalListComparator())
            update(copyList)
        }
    }

    override fun changeStatus(autoReversal: AutoReversal, status: AutoReversal.Status) {
        val list = getAllQueueRequests()
        if (list.isNotEmpty()){
            val copyList = copy(list)
            val newAutoReversal = AutoReversal(autoReversal.transactionType,
                autoReversal.reversalRequest, autoReversal.retryCount,
                status, StringUtils.dateTimeStamp())
            copyList.remove(autoReversal)
            copyList.add(newAutoReversal)
            copyList.sortWith(AutoReversalListComparator())
            update(copyList)
        }
    }

    override fun clear() {
        securePreference.clear()
    }

    private fun copy(list: MutableList<AutoReversal>): MutableList<AutoReversal> {
        val copyList = mutableListOf<AutoReversal>()
        list.forEach {
            copyList.add(it)
        }
        return copyList.toMutableList()
    }

    private fun getAsString(): String? {
        return securePreference.retrieveData(PREFERENCE_KEY, null)
    }

    private fun update(list: MutableList<AutoReversal>) {
        val data = Jsons.toJsonList(list)
        logger.log("::: AUTO REVERSAL >>> UPDATE ::: ${list.size}")
        securePreference.saveData(PREFERENCE_KEY, data)
    }

    internal class AutoReversalListComparator: Comparator<AutoReversal> {
        override fun compare(p0: AutoReversal?, p1: AutoReversal?): Int {
            val timeStamp1 = p0!!.timeStamp.toLong()
            val timeStamp2 = p1!!.timeStamp.toLong()
            return (timeStamp2 - timeStamp1).toInt()
        }
    }
}