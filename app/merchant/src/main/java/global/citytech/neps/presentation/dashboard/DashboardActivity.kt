package global.citytech.neps.presentation.dashboard

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.common.extensions.openActivity
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.neps.BR
import global.citytech.neps.R
import global.citytech.neps.databinding.ActivityDashboardBinding
import global.citytech.neps.presentation.amount.AmountActivity
import global.citytech.neps.presentation.idle.IdleActivity
import global.citytech.neps.presentation.merchant.MerchantActivity
import global.citytech.neps.presentation.model.ConfigurationItem
import global.citytech.neps.presentation.purchase.PurchaseActivity
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppBaseActivity<ActivityDashboardBinding, DashboardViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        initObserver()
        getViewModel().getConfiguration()
    }

    private fun initObserver() {
        getViewModel().configuration.observe(this, Observer { configurationItem ->
            handleConfig(configurationItem)
        })
    }

    private fun handleConfig(configurationItem: ConfigurationItem?) {
        tv_terminal_id.text = "TERMINAL ID : ${configurationItem!!.merchants!![0].terminalId}"
    }

    private fun initViews() {
        btn_merchant_menu.setOnClickListener {
            openActivity(MerchantActivity::class.java)
            finish()
        }

        btn_purchase.setOnClickListener{
            openActivity(PurchaseActivity::class.java)
            finish()
        }

        iv_back.setOnClickListener {
            returnToIdlePage()
        }
    }

    private fun returnToIdlePage() {
        openActivity(IdleActivity::class.java)
        finish()
    }

    override fun onBackPressed() {
        returnToIdlePage()
    }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getViewModel(): DashboardViewModel =
        ViewModelProviders.of(this)[DashboardViewModel::class.java]

    override fun getLayout(): Int = R.layout.activity_dashboard
}
