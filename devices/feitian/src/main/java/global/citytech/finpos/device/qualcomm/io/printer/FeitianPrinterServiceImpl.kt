package global.citytech.finpos.device.qualcomm.io.printer

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import com.ftpos.library.smartpos.errcode.ErrCode
import com.ftpos.library.smartpos.printer.AlignStyle
import com.ftpos.library.smartpos.printer.OnPrinterCallback
import com.ftpos.library.smartpos.printer.Printer
import global.citytech.common.printer.BarCodeUtils
import global.citytech.common.printer.BitmapGenerator
import global.citytech.common.printer.FontManager
import global.citytech.finpos.device.qualcomm.utils.FeitianLogger
import global.citytech.finposframework.exceptions.PosError
import global.citytech.finposframework.exceptions.PosException
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.printer.*
import global.citytech.finposframework.utility.StringUtils
import java.util.*
import java.util.concurrent.CountDownLatch

/**
 * Created by Rishav Chudal on 4/12/21.
 */
class FeitianPrinterServiceImpl constructor(
    val context: Context
) : PrinterService {

    private var printer: Printer
    private lateinit var countDownLatch: CountDownLatch
    private var printResponse: PrinterResponse? = null

    init {
        printer = Printer.getInstance(context.applicationContext)
        FontManager.setArialFontAsCustomTypeFaceFromAssets(
            context.applicationContext.assets
        )
    }


    override fun print(printerRequest: PrinterRequest?): PrinterResponse {
        countDownLatch = CountDownLatch(1)
        val result = printer.open()
        if (result != ErrCode.ERR_SUCCESS ) {
            if(result == ErrCode.ERR_LOW_BATTERY){
                return PrinterResponse(
                    Result.FAILURE,
                    "Low Battery!\nFailed to open printer"
                )
            }
            return PrinterResponse(
                Result.FAILURE,
                "Failed to open printer")
        }
        printer.startCaching()
        printer.setGray(3)
        printList(printerRequest!!.printableList!!)
        printer.print(object : OnPrinterCallback {
            override fun onSuccess() {
                printResponse = PrinterResponse(
                    Result.SUCCESS,
                    "Success"
                )
                countDownLatch.countDown()
            }

            override fun onError(errorCode: Int) {
                handleErrorInPrint(errorCode)
                countDownLatch.countDown()
            }
        })
        try {
            countDownLatch.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        if (printerRequest.feedPaperAfterFinish)
            feedPaper(5)
        return printResponse!!
    }

    private fun printList(printList: List<Printable>) {
        val bitmaps: MutableList<Bitmap> = ArrayList()
        for (printable in printList) {
            require(printable.isAllow()) { "Invalid Object in List" }
            if (printable.isImage()) {
                bitmaps.add(printable.getData() as Bitmap)
            } else if (printable.isQrCode()) {
                if (bitmaps.size > 0) {
                    val bitmap: Bitmap = BitmapGenerator.mergeBitmapsVertically(bitmaps)
                    printBitmapImage(bitmap)
                    bitmaps.clear()
                }
                printer.setAlignStyle(AlignStyle.PRINT_STYLE_CENTER)
                printer.printQRCodeEx(
                    (printable.getData() as PrintMessage).value!!.toByteArray(),
                    prepareBundleWithWidth((printable as PrintMessage).getStyle())
                )
            } else if (printable.isBase64Image()) {
                val bitmap = BitmapGenerator.base64ToImage(printable.getData() as String)
                bitmap?.let {
                    bitmaps.add(it)
                }
            } else if (printable.isBarCode()) {
                val bitmap = BitmapGenerator.resize(
                    BarCodeUtils.generateBarCode(printable.getData() as String)
                )
                bitmaps.add(bitmap)
            } else {
                val bitmap = generateBitmap(printable.getData() as PrintMessage)
                if (bitmap != null) bitmaps.add(bitmap)
            }
        }
        val bitmap: Bitmap = BitmapGenerator.mergeBitmapsVertically(bitmaps)
        printBitmapImage(bitmap)
    }

    private fun prepareBundleWithWidth(style: Style): Bundle {
        val bundle = Bundle()
        bundle.putInt("height", style.qrWidth)
        bundle.putInt("width", style.qrWidth)
        return bundle
    }

    private fun printBitmapImage(bitmap: Bitmap) {
        printer.printBmp(bitmap)
    }

    private fun generateBitmap(printMessage: PrintMessage): Bitmap? {
        return if (printMessage.hasMultiString()) {
            if (printMessage.textList == null && printMessage.textList!!.isEmpty()
            ) null else BitmapGenerator.fromMultiColumn(
                printMessage.textList,
                printMessage.getStyle(),
                printMessage.columnWidths
            )
        } else {
            if (StringUtils.isEmpty(printMessage.label)) BitmapGenerator.fromText(
                printMessage.value, printMessage.getStyle(),
                PrinterUtils.getAlignmentForBitmap(printMessage.getStyle().getAlignment()),
                BitmapGenerator.PAPER_WIDTH
            ) else BitmapGenerator.fromDoubleColumn(
                printMessage.label,
                printMessage.value, printMessage.getStyle(),
                PrinterUtils.getAlignmentForBitmap(printMessage.getStyle().getAlignment())
            )
        }
    }

    override fun feedPaper(numberOfEmptyLines: Int) {
        val result = printer.open()
        if (result == ErrCode.ERR_SUCCESS) printer.feed(numberOfEmptyLines * 16)
    }

    private fun handleErrorInPrint(error: Int) {
        FeitianLogger.logStatic("Printer Error Code ::: ".plus(error))
        when(error) {
            ErrCode.ERR_PRINTER_NO_PAPER -> {
                printResponse = PrinterResponse(
                    Result.PRINTER_ERROR,
                    PosError.PRINTER_OUT_OF_PAPER.errorMessage
                )
            }
            ErrCode.ERR_PRINTER_TEMPERATURE -> {
                printResponse = PrinterResponse(
                    Result.PRINTER_ERROR,
                    PosError.DEVICE_ERROR_PRINTER_OVERHEAT.errorMessage
                )
            }
            else ->{
                printResponse = PrinterResponse(
                    Result.PRINTER_ERROR,
                    PosError.DEVICE_ERROR_PRINTER_ERROR.errorMessage
                )
            }
        }
    }
}