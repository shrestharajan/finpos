package global.citytech.finpos.nibl.iso8583.requestsender;

import org.junit.Test;

import global.citytech.finpos.nibl.NIBLHostConfig;
import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
import global.citytech.finpos.nibl.iso8583.requestsender.tip.S2MTipAdjustmentRequest;
import global.citytech.finpos.nibl.iso8583.requestsender.tip.TipAdjustmentRequestSender;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Unique Shakya on 4/26/2021.
 */
public class TipAdjustmentRequestSenderTest {

    @Test
    public void sendTest() {
        try {
            String terminalId = "99991002";
            String acquirerId = "405635000000002";
            TerminalInfo terminalInfo = new TerminalInfo(terminalId, acquirerId, "", "Citytech", "Kamaladi");
            HostInfo primaryHost = NIBLHostConfig.getPrimaryConfig();
            HostInfo secondaryHost = NIBLHostConfig.getSecondaryConfig();
            ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);
            RequestContext context =
                    new RequestContext(
                            "000405", terminalInfo, connectionParam);
            S2MTipAdjustmentRequest s2MTipAdjustmentRequest = S2MTipAdjustmentRequest.Builder.newInstance()
                    .pan("4249720020000234")
                    .amount(100)
                    .localTime("142322")
                    .localDate("0404")
                    .expireDate("1222")
                    .posEntryMode(PosEntryMode.ICC)
                    .posConditionCode("00")
                    .track2Data("4249720020000234D1222062")
                    .originalRrn("100000000012")
                    .originalAuthCode("123456")
                    .originalResponseCode("00")
                    .pinData("")
                    .tipAmount(1)
                    .iccDataBlock("")
                    .originalAmount(100)
                    .originalInvoiceNumber("100023")
                    .additionalData("")
                    .build();
            context.setRequest(s2MTipAdjustmentRequest);

            TipAdjustmentRequestSender requestSender = new TipAdjustmentRequestSender(new NIBLSpecInfo(),
                    context);
            IsoMessageResponse isoMessageResponse = requestSender.send();
            assertNotNull(isoMessageResponse);
        } catch (FinPosException fe) {
            fe.printStackTrace();
        }
    }
}
