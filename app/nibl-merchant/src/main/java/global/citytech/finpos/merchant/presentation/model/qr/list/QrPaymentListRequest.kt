package global.citytech.finpos.merchant.presentation.model.qr.list


data class QrPaymentListRequest(
    val terminalInfos: List<QrTerminalInfos>
) {
}