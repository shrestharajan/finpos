package global.citytech.finpos.merchant.presentation.greenpin

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.os.Handler
import android.widget.Button
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.BuildConfig
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityGreenPinBinding
import global.citytech.finpos.merchant.presentation.alertdialogs.PosEntryMode
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finposframework.listeners.OtpConfirmationListener
import global.citytech.finposframework.usecases.TransactionType
import kotlinx.android.synthetic.main.activity_green_pin.*
import kotlinx.android.synthetic.main.layout_otp_pin_block.button0
import kotlinx.android.synthetic.main.layout_otp_pin_block.button1
import kotlinx.android.synthetic.main.layout_otp_pin_block.button2
import kotlinx.android.synthetic.main.layout_otp_pin_block.button3
import kotlinx.android.synthetic.main.layout_otp_pin_block.button4
import kotlinx.android.synthetic.main.layout_otp_pin_block.button5
import kotlinx.android.synthetic.main.layout_otp_pin_block.button6
import kotlinx.android.synthetic.main.layout_otp_pin_block.button7
import kotlinx.android.synthetic.main.layout_otp_pin_block.button8
import kotlinx.android.synthetic.main.layout_otp_pin_block.button9
import kotlinx.android.synthetic.main.layout_otp_pin_block.et_inputCode1
import kotlinx.android.synthetic.main.layout_otp_pin_block.et_inputCode2
import kotlinx.android.synthetic.main.layout_otp_pin_block.et_inputCode3
import kotlinx.android.synthetic.main.layout_otp_pin_block.et_inputCode4
import kotlinx.android.synthetic.main.layout_otp_pin_block.et_inputCode5
import kotlinx.android.synthetic.main.layout_otp_pin_block.et_inputCode6
import kotlinx.android.synthetic.main.layout_otp_pin_block.img_btn_clear
import kotlinx.android.synthetic.main.layout_otp_pin_block.iv_back
import kotlinx.android.synthetic.main.layout_otp_pin_block.mb_submit


class GreenPinActivity :
    GreenPinBaseTransactionActivity<ActivityGreenPinBinding, GreenPinViewModel>(),
    View.OnClickListener {

    private lateinit var editTexts: Array<EditText>
    private lateinit var viewModel: GreenPinViewModel
    private lateinit var configurationItem: ConfigurationItem
    private lateinit var currentEditText: EditText
    private var previousIndex: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel()
        initBaseViews()
        initViews()
        initObservers()
        viewModel.getConfigurationItem()
        editTexts = if (!AppUtility.isAppVariantNIBL()) {
            arrayOf(
                et_inputCode1,
                et_inputCode2,
                et_inputCode3,
                et_inputCode4,
                et_inputCode5,
                et_inputCode6
            )
        } else {
            arrayOf(
                et_inputCode1, et_inputCode2, et_inputCode3, et_inputCode4
            )
        }
        setupEditTexts()
        showPosEntrySelectionDialog()
    }


    private fun initViews() {
        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
        img_btn_clear.setOnClickListener(this)
        mb_submit.setOnClickListener(this)
        et_inputCode1.requestFocus()
        et_inputCode1.showSoftInputOnFocus = false
        et_inputCode2.showSoftInputOnFocus = false
        et_inputCode3.showSoftInputOnFocus = false
        et_inputCode4.showSoftInputOnFocus = false
        et_inputCode5.showSoftInputOnFocus = false
        et_inputCode6.showSoftInputOnFocus = false


    }

    private fun setupEditTexts() {
        for (i in editTexts.indices) {
            editTexts[i].setCustomSelectionActionModeCallback(object : ActionMode.Callback {
                override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
                    return false
                }

                override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
                    return false
                }

                override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
                    return false
                }

                override fun onDestroyActionMode(mode: ActionMode) {}
            })

            editTexts[i].addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (p0?.length == 1 && i < editTexts.indices.last) {
                        editTexts[i + 1].requestFocus()
                    }

                }

                override fun afterTextChanged(p0: Editable?) {
                    if (!AppUtility.isAppVariantNIBL()) {
                        et_inputCode1.backgroundTintList =
                            ContextCompat.getColorStateList(
                                this@GreenPinActivity,
                                R.color.colorPrimary
                            )
                        et_inputCode2.backgroundTintList =
                            ContextCompat.getColorStateList(
                                this@GreenPinActivity,
                                R.color.colorPrimary
                            )
                        et_inputCode3.backgroundTintList =
                            ContextCompat.getColorStateList(
                                this@GreenPinActivity,
                                R.color.colorPrimary
                            )
                        et_inputCode4.backgroundTintList =
                            ContextCompat.getColorStateList(
                                this@GreenPinActivity,
                                R.color.colorPrimary
                            )
                        et_inputCode5.backgroundTintList =
                            ContextCompat.getColorStateList(
                                this@GreenPinActivity,
                                R.color.colorPrimary
                            )
                        et_inputCode6.backgroundTintList =
                            ContextCompat.getColorStateList(
                                this@GreenPinActivity,
                                R.color.colorPrimary
                            )
                        et_inputCode6.setSelection(et_inputCode6.text.length)

                    } else {
                        et_inputCode1.backgroundTintList =
                            ContextCompat.getColorStateList(
                                this@GreenPinActivity,
                                R.color.colorPrimary
                            )
                        et_inputCode2.backgroundTintList =
                            ContextCompat.getColorStateList(
                                this@GreenPinActivity,
                                R.color.colorPrimary
                            )
                        et_inputCode3.backgroundTintList =
                            ContextCompat.getColorStateList(
                                this@GreenPinActivity,
                                R.color.colorPrimary
                            )
                        et_inputCode4.backgroundTintList =
                            ContextCompat.getColorStateList(
                                this@GreenPinActivity,
                                R.color.colorPrimary
                            )
                        et_inputCode4.setSelection(et_inputCode6.text.length)
                    }

                }
            })
        }

        if (!AppUtility.isAppVariantNIBL()) {
            et_inputCode5.visibility = View.VISIBLE
            et_inputCode6.visibility = View.VISIBLE
        } else {
            et_inputCode5.visibility = View.GONE
            et_inputCode6.visibility = View.GONE
        }
    }


    override fun onClick(view: View?) {
        return when (view!!.id) {
            R.id.img_btn_clear -> if (getViewModel().otp.value != null && getViewModel().otp.value!!.length > 0) {
                manageForClearButtonClicked()
            } else {
            }
            R.id.mb_submit -> {
                if (!AppUtility.isAppVariantNIBL()) {
                    onSubmitButtonClicked()
                } else {
                    onSubmitButtonClickedForNibl()
                }
            }
            else -> {
                if (!AppUtility.isAppVariantNIBL()) {
                    getViewModel().onNumericButtonPressed((view as Button).text.toString(), 6)
                } else {
                    getViewModel().onNumericButtonPressed((view as Button).text.toString(), 4)
                }
            }

        }
    }

    private fun initObservers() {
        this.viewModel.configurationItem.observe(this, Observer {
            this.configurationItem = it
            this.viewModel.generateOtpRequest(configurationItem, getTransactionType())
        })

        viewModel.otp.observe(this, Observer { otp ->
            for (i in editTexts.indices) {
                if (i < otp.length) {
                    editTexts[i].setText(otp[i].toString())
                } else {
                    editTexts[i].text.clear()
                }
            }
        })
        viewModel.dismissActivity.observe(this, Observer {
            if (it) {
                returnToDashboard()
            }
        })

        viewModel.doReturnToDashboard.observe(this, Observer {
            if (it) {
                returnToDashboard()
            }

        })
    }

    private fun manageForClearButtonClicked() {
        viewModel.onClearButtonClicked()
        moveCursorToPreviousEditText()
    }

    private fun moveCursorToPreviousEditText() {
        currentEditText = getCurrentEditText()
        previousIndex = editTexts.indexOf(currentEditText) - 1

        if (previousIndex >= 0) {
            val previousEditText = editTexts[previousIndex]
            previousEditText.requestFocus()
            previousEditText.setSelection(previousEditText.text.length)
        }

    }


    private fun getCurrentEditText(): EditText {
        val focusedView = currentFocus
        return focusedView as EditText
    }

    private fun showPosEntrySelectionDialog() {
        Glide.with(this).load(PosEntryMode.ACCEPT_ALL.loaderImage).into(image)
    }

    private fun onSubmitButtonClicked() {
        if (TextUtils.isEmpty(getViewModel().otp.value)) {
            showToast(getString(R.string.enter_otp))
            et_inputCode1.backgroundTintList = ContextCompat.getColorStateList(this, R.color.red)
            et_inputCode2.backgroundTintList = ContextCompat.getColorStateList(this, R.color.red)
            et_inputCode3.backgroundTintList = ContextCompat.getColorStateList(this, R.color.red)
            et_inputCode4.backgroundTintList = ContextCompat.getColorStateList(this, R.color.red)
            et_inputCode5.backgroundTintList = ContextCompat.getColorStateList(this, R.color.red)
            et_inputCode6.backgroundTintList = ContextCompat.getColorStateList(this, R.color.red)
        } else if (getViewModel().otp.value?.length!! < 6) {
            showToast(getString(R.string.valid_digit))
        } else {
            submitOtp(getViewModel().otp.value)
            Handler().postDelayed({
                // This code will run after the specified delay
                showProgressScreen("Please do not remove your card...")
            }, 1000)
        }

    }

    private fun onSubmitButtonClickedForNibl() {
        if (TextUtils.isEmpty(getViewModel().otp.value)) {
            showToast(getString(R.string.enter_otp))
            et_inputCode1.backgroundTintList = ContextCompat.getColorStateList(this, R.color.red)
            et_inputCode2.backgroundTintList = ContextCompat.getColorStateList(this, R.color.red)
            et_inputCode3.backgroundTintList = ContextCompat.getColorStateList(this, R.color.red)
            et_inputCode4.backgroundTintList = ContextCompat.getColorStateList(this, R.color.red)
        } else if (getViewModel().otp.value?.length!! < 4) {
            showToast(getString(R.string.valid_four_digit))

        } else {
            submitOtp(getViewModel().otp.value)
        }

    }


    override fun onManualButtonClicked() {
        TODO("Not yet implemented")
    }

    override fun onCancelButtonClicked() {
        this.viewModel.shouldDispose = true
        this.viewModel.doCleanUp(GreenPinBaseTransactionViewModel.Task.RETURN_TO_DASHBOARD)
    }


    override fun getViewModel(): GreenPinViewModel =
        ViewModelProviders.of(this)[GreenPinViewModel::class.java]

    override fun onAmountActivityResult(data: Intent?) {
        TODO("Not yet implemented")
    }

    override fun onManualActivityResult(data: Intent?) {
        TODO("Not yet implemented")
    }

    override fun getTransactionType(): TransactionType = TransactionType.GREEN_PIN


    override fun getBindingVariable(): Int = BR.greenPinViewModel

    override fun getLayout(): Int = R.layout.activity_green_pin

    override fun getGreenPinBaseTransactionViewModel(): GreenPinBaseTransactionViewModel =
        this.viewModel

}

