package global.citytech.finpos.processor.neps.cashadvance;

import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.ProcessingCode;
import global.citytech.finpos.neps.iso8583.requestsender.cashadvance.CashAdvanceIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.cashadvance.ManualCashAdvanceRequestSender;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.processor.neps.transaction.ManualTransactionUseCase;
import global.citytech.finpos.processor.neps.transaction.TransactionResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
import global.citytech.finposframework.utility.DateUtils;

/**
 * Created by Saurav Ghimire on 2/2/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class ManualCashAdvanceUseCase extends ManualTransactionUseCase {
    public ManualCashAdvanceUseCase(TerminalRepository terminalRepository, Notifier notifier) {
        super(terminalRepository, notifier);
    }

    @Override
    protected TransactionType getOriginalTransactionType() {
        return TransactionType.CASH_ADVANCE;
    }

    @Override
    protected TransactionType getTransactionType() {
        return TransactionType.CASH_ADVANCE;
    }


    @Override
    protected NepsMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context) {
        return new ManualCashAdvanceRequestSender(context);
    }


    @Override
    protected TransactionResponse prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository, boolean shouldPrint) {
        return CashAdvanceResponse.Builder.createDefaultBuilder()
                .approved(transactionApproved)
                .stan(stan)
                .shouldPrintCustomerCopy(shouldPrint)
                .debugRequestMessage(isoMessageResponse.getDebugRequestString())
                .debugResponseMessage(isoMessageResponse.getDebugResponseString())
                .message(this.retrieveMessage(isoMessageResponse, transactionApproved, applicationRepository))
                .build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.CASH_ADVANCE.getCode();
    }

    @Override
    protected TransactionResponse prepareErrorResponse(String responseMessage, boolean shouldPrint) {
        return CashAdvanceResponse.Builder.createDefaultBuilder()
                .stan(this.stan)
                .message(responseMessage)
                .approved(false)
                .shouldPrintCustomerCopy(shouldPrint)
                .build();
    }

    @Override
    protected String getClassName() {
        return ManualCashAdvanceUseCase.class.getName();
    }

    @Override
    protected TransactionIsoRequest prepareIsoRequest(ReadCardResponse readCardResponse, TransactionRequest transactionRequest) {
        return CashAdvanceIsoRequest.Builder.newInstance()
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .transactionAmount(readCardResponse.getCardDetails().getAmount())
                .localDateTime(DateUtils.yyMMddHHmmssDate())
                .currencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .pinBlock(shouldSkipPinBlockFromCard(readCardResponse) ? "" : readCardResponse.getCardDetails().getPinBlock().getPin())
                .cvv(readCardResponse.getCardDetails().getCvv())
                .build();
    }
}
