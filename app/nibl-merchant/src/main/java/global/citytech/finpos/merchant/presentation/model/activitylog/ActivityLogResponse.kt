package global.citytech.finpos.merchant.presentation.model.activitylog


/*
 * @author BikashShrestha
 * @project finpos
 * @created 2021/11/08 - 5:59 PM
*/

data class ActivityLogResponse(
    var code: String? = null,
    var data: Any? = null,
    var message: String? = null
)