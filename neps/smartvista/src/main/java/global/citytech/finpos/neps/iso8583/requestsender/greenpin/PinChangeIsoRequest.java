package global.citytech.finpos.neps.iso8583.requestsender.greenpin;

import java.math.BigDecimal;

import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.voidsale.VoidSaleIsoRequest;
import global.citytech.finposframework.hardware.io.cards.PinBlock;
import global.citytech.finposframework.switches.PosEntryMode;

public class PinChangeIsoRequest extends TransactionIsoRequest {
    private String stan;
    private PosEntryMode posEntryMode;
    private String posConditionCode;
//    private String newPin;

    public String getStan() {
        return stan;
    }

    public PosEntryMode getPosEntryMode() {
        return posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }


    public static final class Builder{
        private String pan;
        private BigDecimal transactionAmount;
        private String stan;
        private String localDateTime;
        private String expirationDate;
        private PosEntryMode posEntryMode;
        private String posConditionCode;
        private String track2Data;
        private String additionalData;
        private String pinBlock;
        private String emvData;
        private String currencyCode;
//        private String newPin;


        private Builder(){
        }

        public static Builder newInstance(){
            return new Builder();
        }

        public PinChangeIsoRequest.Builder stan(String stan) {
            this.stan = stan;
            return this;
        }

        public PinChangeIsoRequest.Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public PinChangeIsoRequest.Builder transactionAmount(BigDecimal transactionAmount) {
            this.transactionAmount = transactionAmount;
            return this;
        }

        public PinChangeIsoRequest.Builder localDateTime(String localDataTime) {
            this.localDateTime = localDataTime;
            return this;
        }

        public PinChangeIsoRequest.Builder expirationDate(String expirationDate){
            this.expirationDate = expirationDate;
            return this;
        }

        public PinChangeIsoRequest.Builder track2Data(String track2Data) {
            this.track2Data = track2Data;
            return this;
        }

        public PinChangeIsoRequest.Builder additionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public PinChangeIsoRequest.Builder pinBlock(String pinBlock) {
            this.pinBlock = pinBlock;
            return this;
        }

        public PinChangeIsoRequest.Builder emvData(String emvData) {
            this.emvData = emvData;
            return this;
        }

        public PinChangeIsoRequest.Builder posEntryMode(PosEntryMode posEntryMode) {
            this.posEntryMode = posEntryMode;
            return this;
        }

        public PinChangeIsoRequest.Builder posConditionCode(String posConditionCode) {
            this.posConditionCode = posConditionCode;
            return this;
        }

        public PinChangeIsoRequest.Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

//        public PinChangeIsoRequest.Builder newPin(String newPin) {
//            this.newPin = newPin;
//            return this;
//        }

        public PinChangeIsoRequest build() {
            PinChangeIsoRequest pinChangeIsoRequest = new PinChangeIsoRequest();
            pinChangeIsoRequest.pan = this.pan;
            pinChangeIsoRequest.transactionAmount = this.transactionAmount;
            pinChangeIsoRequest.stan = this.stan;
            pinChangeIsoRequest.localDateTime = this.localDateTime;
            pinChangeIsoRequest.expiryDate = this.expirationDate;
            pinChangeIsoRequest.posEntryMode = this.posEntryMode;
            pinChangeIsoRequest.posConditionCode = this.posConditionCode;
            pinChangeIsoRequest.track2Data = this.track2Data;
            pinChangeIsoRequest.additionalData = this.additionalData;
            pinChangeIsoRequest.pinBlock = this.pinBlock;
            pinChangeIsoRequest.emvData = this.emvData;
            pinChangeIsoRequest.currencyCode= this.currencyCode;
//            pinChangeIsoRequest.newPin=this.newPin;
            return pinChangeIsoRequest;
        }
    }
}
