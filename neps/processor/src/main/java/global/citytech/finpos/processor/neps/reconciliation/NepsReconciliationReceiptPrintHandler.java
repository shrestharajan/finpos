package global.citytech.finpos.processor.neps.reconciliation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import global.citytech.finposframework.hardware.io.printer.Printable;
import global.citytech.finposframework.hardware.io.printer.PrinterRequest;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.usecases.CardSchemeType;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.utility.HelperUtils;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finpos.processor.neps.receipt.ReceiptUtils.addBase64Image;
import static global.citytech.finpos.processor.neps.receipt.ReceiptUtils.addDoubleColumnString;
import static global.citytech.finpos.processor.neps.receipt.ReceiptUtils.addMultiColumnString;
import static global.citytech.finpos.processor.neps.receipt.ReceiptUtils.addSingleColumnString;

public class NepsReconciliationReceiptPrintHandler {
    private final PrinterService printerService;

    public NepsReconciliationReceiptPrintHandler(PrinterService printerService) {
        this.printerService = printerService;
    }


    public void printReceipt(
            NepsReconciliationReceipt nepsReconciliationReceipt,
            ReceiptVersion receiptVersion
    ) {
        PrinterRequest printerRequest = this.preparePrinterRequest(
                nepsReconciliationReceipt,
                receiptVersion
        );
        printerRequest.setFeedPaperAfterFinish(false);
        this.printerService.print(printerRequest);
    }

    private PrinterRequest preparePrinterRequest(
            NepsReconciliationReceipt nepsReconciliationReceipt,
            ReceiptVersion receiptVersion
    ) {
        List<Printable> printableList = new ArrayList<>();
        if (nepsReconciliationReceipt.getRetailer().getRetailerLogo() != null)
            addBase64Image(printableList, nepsReconciliationReceipt.getRetailer().getRetailerLogo());
        addSingleColumnString(printableList, nepsReconciliationReceipt.getRetailer().getRetailerName(), ReconciliationReceiptStyle.RETAILER_NAME.getStyle());
        addSingleColumnString(printableList, nepsReconciliationReceipt.getRetailer().getRetailerAddress(), ReconciliationReceiptStyle.RETAILER_ADDRESS.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
        if (receiptVersion == ReceiptVersion.DUPLICATE_COPY)
            addSingleColumnString(printableList, receiptVersion.getVersionLabel(), ReconciliationReceiptStyle.RECEIPT_VERSION.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.SETTLEMENT_REPORT, ReconciliationReceiptStyle.RECONCILIATION_REPORT.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
        addDoubleColumnString(printableList, ReconciliationReceiptLabels.TERMINAL_ID, nepsReconciliationReceipt.getRetailer().getTerminalId(), ReconciliationReceiptStyle.TERMINAL_ID.getStyle());
        addDoubleColumnString(printableList, ReconciliationReceiptLabels.MERCHANT_ID, nepsReconciliationReceipt.getRetailer().getMerchantId(), ReconciliationReceiptStyle.MERCHANT_ID.getStyle());
        addDoubleColumnString(printableList, ReconciliationReceiptLabels.DATE_TIME, this.receiptFormatDate(nepsReconciliationReceipt.getPerformance().getStartDateTime()), ReconciliationReceiptStyle.START_DATE_TIME.getStyle());
        addDoubleColumnString(printableList, ReconciliationReceiptLabels.BATCH_NUMBER, nepsReconciliationReceipt.getReconciliationBatchNumber(), ReconciliationReceiptStyle.BATCH_NUMBER.getStyle());
        addDoubleColumnString(printableList, ReconciliationReceiptLabels.HOST, nepsReconciliationReceipt.getHost(), ReconciliationReceiptStyle.HOST.getStyle());
        addDoubleColumnString(printableList, ReconciliationReceiptLabels.APPLICATION_VERSION, nepsReconciliationReceipt.getApplicationVersion(), ReconciliationReceiptStyle.APPLICATION_VERSION.getStyle());
        addSingleColumnString(printableList, "  ", ReconciliationReceiptStyle.DIVIDER.getStyle());
        addTotalsTable(printableList, nepsReconciliationReceipt);
        addCumulativeTotalsTable(printableList, nepsReconciliationReceipt);
        addSingleColumnString(printableList, nepsReconciliationReceipt.getReconciliationResult(), ReconciliationReceiptStyle.RECONCILIATION_RESULT.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
        return new PrinterRequest(printableList);
    }

    private String receiptFormatDate(String startDateTime) {
        try {
            Date date = new SimpleDateFormat("yyMMddHHmmss").parse(startDateTime);
            return new SimpleDateFormat("yyyy/MM/dd, HH:mm:ss").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDateTime;
    }

    private void addTotalsTable(List<Printable> printableList, NepsReconciliationReceipt nepsReconciliationReceipt) {
        NepsReconciliationTotals niblSettlementTotals = (NepsReconciliationTotals) nepsReconciliationReceipt.getReconciliationTotals();
        for (Map.Entry<CardSchemeType, NepsCardSchemeTotals> niblCardSchemeTotals :niblSettlementTotals.getTotalsMap().entrySet()) {
            addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
            addSingleColumnString(printableList, niblCardSchemeTotals.getKey().getCardScheme(), ReconciliationReceiptStyle.CARD_SCHEME.getStyle());
            addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
            addMultiColumnString(printableList, this.prepareSalesRow(niblCardSchemeTotals.getValue()), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
            addMultiColumnString(printableList, this.prepareSalesRefundRow(niblCardSchemeTotals.getValue()), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
            addMultiColumnString(printableList, this.prepareVoidRow(niblCardSchemeTotals.getValue()), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
            addSingleColumnString(printableList, ReconciliationReceiptLabels.DIVIDER, ReconciliationReceiptStyle.DIVIDER.getStyle());
            addMultiColumnString(printableList, this.prepareTotalsRow(niblCardSchemeTotals.getValue()), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
            addSingleColumnString(printableList, ReconciliationReceiptLabels.DIVIDER, ReconciliationReceiptStyle.DIVIDER.getStyle());
        }
    }

    private List<String> prepareTotalsRow(NepsCardSchemeTotals value) {
        List<String> strings = new ArrayList<>();
        strings.add(ReconciliationReceiptLabels.TOTAL);
        strings.add(receiptFormatCount(value.getTotalCount()));
        strings.add(ReconciliationReceiptLabels.CURRENCY_NAME);
        strings.add(this.receiptFormatAmount(value.getTotalAmount()));
        return strings;
    }

    private List<String> prepareVoidRow(NepsCardSchemeTotals value) {
        List<String> strings = new ArrayList<>();
        strings.add(ReconciliationReceiptLabels.VOID);
        strings.add(receiptFormatCount(value.getVoidCount()));
        strings.add(ReconciliationReceiptLabels.CURRENCY_NAME);
        strings.add(receiptFormatAmount(value.getVoidAmount()));
        return strings;
    }

    private List<String> prepareSalesRefundRow(NepsCardSchemeTotals value) {
        List<String> strings = new ArrayList<>();
        strings.add(ReconciliationReceiptLabels.REFUND);
        strings.add(receiptFormatCount(value.getSalesRefundCount()));
        strings.add(ReconciliationReceiptLabels.CURRENCY_NAME);
        strings.add(receiptFormatAmount(value.getSalesRefundAmount()));
        return strings;
    }

    private List<String> prepareSalesRow(NepsCardSchemeTotals value) {
        List<String> strings = new ArrayList<>();
        strings.add(ReconciliationReceiptLabels.SALE);
        strings.add(receiptFormatCount(value.getSalesCount()));
        strings.add(ReconciliationReceiptLabels.CURRENCY_NAME);
        strings.add(receiptFormatAmount(value.getSalesAmount()));
        return strings;
    }

    private void addCumulativeTotalsTable(
            List<Printable> printableList,
            NepsReconciliationReceipt reconciliationReceipt
    ) {
        NepsReconciliationTotals settlementTotals = (NepsReconciliationTotals)
                reconciliationReceipt.getReconciliationTotals();
        addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.CUMULATIVE, ReconciliationReceiptStyle.CUMULATIVE.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.LINE_BREAK, ReconciliationReceiptStyle.LINE_BREAK.getStyle());
        addMultiColumnString(printableList, prepareCumulativeSalesRow(settlementTotals.getTransactionCurrencyName(), settlementTotals), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
        addMultiColumnString(printableList, prepareCumulativeSalesRefundRow(settlementTotals.getTransactionCurrencyName(), settlementTotals), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
        addMultiColumnString(printableList, prepareCumulativeVoidRow(settlementTotals.getTransactionCurrencyName(), settlementTotals), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.DIVIDER, ReconciliationReceiptStyle.DIVIDER.getStyle());
        addMultiColumnString(printableList, this.prepareCumulativeTotalsRow(settlementTotals.getTransactionCurrencyName(), settlementTotals), ReconciliationReceiptStyle.TOTALS.columnWidths(), ReconciliationReceiptStyle.TOTALS.getStyle());
        addSingleColumnString(printableList, ReconciliationReceiptLabels.DIVIDER, ReconciliationReceiptStyle.DIVIDER.getStyle());
    }

    private List<String> prepareCumulativeSalesRow(
            String transactionCurrencyName,
            NepsReconciliationTotals reconciliationTotals
    ) {
        List<String> strings = new ArrayList<>();
        strings.add(ReconciliationReceiptLabels.SALE);
        strings.add(this.receiptFormatCount(reconciliationTotals.getSalesCount()));
        strings.add(transactionCurrencyName);
        strings.add(this.receiptFormatAmount(reconciliationTotals.getSalesAmount()));
        return strings;
    }

    private List<String> prepareCumulativeSalesRefundRow(
            String transactionCurrencyName,
            NepsReconciliationTotals reconciliationTotals
    ) {
        List<String> strings = new ArrayList<>();
        strings.add(ReconciliationReceiptLabels.REFUND);
        strings.add(this.receiptFormatCount(reconciliationTotals.getSalesRefundCount()));
        strings.add(transactionCurrencyName);
        strings.add(this.receiptFormatAmount(reconciliationTotals.getSalesRefundAmount()));
        return strings;
    }

    private List<String> prepareCumulativeVoidRow(
            String transactionCurrencyName,
            NepsReconciliationTotals reconciliationTotals
    ) {
        List<String> strings = new ArrayList<>();
        strings.add(ReconciliationReceiptLabels.VOID);
        strings.add(this.receiptFormatCount(reconciliationTotals.getVoidCount()));
        strings.add(transactionCurrencyName);
        strings.add(this.receiptFormatAmount(reconciliationTotals.getVoidAmount()));
        return strings;
    }

    private List<String> prepareCumulativeTotalsRow(
            String transactionCurrencyName,
            NepsReconciliationTotals reconciliationTotals
    ) {
        List<String> strings = new ArrayList<>();
        strings.add(ReconciliationReceiptLabels.TOTAL);
        strings.add(this.receiptFormatCount(reconciliationTotals.getTotalCountReceipt()));
        strings.add(transactionCurrencyName);
        strings.add(this.receiptFormatAmount(reconciliationTotals.getTotalAmountReceipt()));
        return strings;
    }

    private String receiptFormatCount(long count) {
        return StringUtils.ofRequiredLength(String.valueOf(count), 4);
    }

    private String receiptFormatAmount(long salesAmount) {
        return StringUtils.formatAmountTwoDecimal(HelperUtils.fromIsoAmount(String.valueOf(salesAmount)));
    }

}
