package global.citytech.finpos.neps.transaction;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;


public class IccCardPurchaseProcessor implements PurchaseProcessor {

    ReadCardService readCardService;
    TransactionRepository transactionRepository;
    ApplicationRepository applicationRepository;

    public IccCardPurchaseProcessor(
            ReadCardService readCardService,
            TransactionRepository transactionRepository,
            ApplicationRepository applicationRepository
    ) {
        this.readCardService = readCardService;
        this.transactionRepository = transactionRepository;
        this.applicationRepository = applicationRepository;
    }

    @Override
    public ReadCardResponse purchase(String stan, PurchaseRequest purchaseRequest) {
        ReadCardRequest readCardRequest = this.prepareReadCardRequest(stan, purchaseRequest);
        ReadCardResponse readCardResponse = readCardService.processEMV(readCardRequest);
        if (readCardResponse.getResult() != Result.FAILURE) {
            if (readCardResponse.getResult() == Result.PROCESSING_ERROR) {
                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
            } else if (readCardResponse.getResult() == Result.PINPAD_ERROR) {
                throw new PosException(PosError.DEVICE_ERROR_PINPAD_ERROR);
            } else if (readCardResponse.getResult() == Result.USER_CANCELLED) {
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            } else if (readCardResponse.getResult() == Result.TIMEOUT) {
                throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
            }
            readCardResponse.getCardDetails().setTransactionInitializeDateTime(DateUtils.yyMMddHHmmssSSSDate());
            return readCardResponse;
        } else {
            throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_READ_CARD);
        }
    }

    private ReadCardRequest prepareReadCardRequest(String stan, PurchaseRequest purchaseRequest) {
        ReadCardRequest readCardRequest =  new ReadCardRequest(
                TransactionUtils.getCardTypesFromTransaction(purchaseRequest.getTransactionType()),
                purchaseRequest.getTransactionType(),
                this.retrieveTransactionCode(purchaseRequest.getTransactionType()),
                this.applicationRepository.retrieveEmvParameterRequest()
        );
        readCardRequest.setStan(stan);
        readCardRequest.setAmount(purchaseRequest.getAmount());
        readCardRequest.setCashBackAmount(purchaseRequest.getAdditionalAmount());
        readCardRequest.setPinpadRequired(
                this.retrievePinRequired(
                        purchaseRequest.getTransactionType(),
                        purchaseRequest
                                .getCardDetails()
                                .getCardScheme().getCardSchemeId()
                )
        );
        readCardRequest.setPrimaryAccountNumber(purchaseRequest.getCardDetails().getPrimaryAccountNumber());
        readCardRequest.setCardDetailsBeforeEmvNext(purchaseRequest.getCardDetails());
        readCardRequest.setTransactionDate(this.retrieveTransactionDate());
        readCardRequest.setTransactionTime(this.retrieveTransactionTime());
        return readCardRequest;
    }

    private String retrieveTransactionCode(TransactionType transactionType) {
        switch (transactionType) {
            case PURCHASE:
                return "00";
            default:
                return "00";
        }
    }

    private boolean retrievePinRequired(TransactionType transactionType, String cardScheme) {
        TransactionSetTableVerifier verifier = new TransactionSetTableVerifier(
                this.applicationRepository,
                cardScheme,
                TransactionUtils.mapPurchaseTypeWithTransactionType(transactionType));
        return verifier.isPinVerificationNecessary();
    }



    private String retrieveTransactionTime() {
        return DateUtils.hhmmssTime();
    }

    private String retrieveTransactionDate() {
        return DateUtils.yyMMddDate();
    }


}