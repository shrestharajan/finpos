package global.citytech.finposframework.switches.receipt.duplicatereceipt;

import global.citytech.finposframework.switches.ResponseParameter;

/**
 * Created by Unique Shakya on 12/15/2020.
 */
public interface DuplicateReceiptResponseParameter extends ResponseParameter {
}
