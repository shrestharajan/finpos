//package global.citytech.finpos.processor.nibl.purchases;
//
//import java.util.List;
//import java.util.concurrent.CountDownLatch;
//
//import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
//import global.citytech.finpos.nibl.iso8583.ProcessingCode;
//import global.citytech.finpos.nibl.iso8583.requestsender.purchase.IccPurchaseRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.purchase.MagPurchaseRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.purchase.PICCPurchaseRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.purchase.PurchaseRequest;
//import global.citytech.finpos.nibl.iso8583.requestsender.purchase.PurchaseRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
//import global.citytech.finpos.processor.nibl.reversal.ReversalRequestModel;
//import global.citytech.finpos.processor.nibl.reversal.ReversalUseCase;
//import global.citytech.finpos.processor.nibl.transaction.NiblTransactionReceiptPrintHandler;
//import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
//import global.citytech.finpos.processor.nibl.transaction.ReversedTransactionReceiptHandler;
//import global.citytech.finpos.processor.nibl.transaction.TransactionUseCase;
//import global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor;
//import global.citytech.finpos.processor.nibl.transaction.card.MagneticCardProcessor;
//import global.citytech.finpos.processor.nibl.transaction.card.PICCCardProcessor;
//import global.citytech.finposframework.exceptions.FinPosException;
//import global.citytech.finposframework.exceptions.PosError;
//import global.citytech.finposframework.exceptions.PosException;
//import global.citytech.finposframework.hardware.io.cards.CardDetails;
//import global.citytech.finposframework.hardware.io.cards.CardType;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
//import global.citytech.finposframework.hardware.io.led.LedAction;
//import global.citytech.finposframework.hardware.io.led.LedLight;
//import global.citytech.finposframework.hardware.io.led.LedRequest;
//import global.citytech.finposframework.hardware.io.led.LedService;
//import global.citytech.finposframework.iso8583.DataElement;
//import global.citytech.finposframework.iso8583.IsoMessageResponse;
//import global.citytech.finposframework.iso8583.RequestContext;
//import global.citytech.finposframework.listeners.CardConfirmationListener;
//import global.citytech.finposframework.notifier.Notifier;
//import global.citytech.finposframework.repositories.ApplicationRepository;
//import global.citytech.finposframework.repositories.TerminalRepository;
//import global.citytech.finposframework.repositories.TransactionRepository;
//import global.citytech.finposframework.supports.UseCase;
//import global.citytech.finposframework.switches.purchase.PurchaseRequester;
//import global.citytech.finposframework.usecases.MagneticSwipe;
//import global.citytech.finposframework.usecases.PICCWave;
//import global.citytech.finposframework.usecases.TransactionType;
//import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
//import global.citytech.finposframework.usecases.transaction.TransactionLog;
//import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
//import global.citytech.finposframework.usecases.transaction.data.Bin;
//import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
//import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
//import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
//import global.citytech.finposframework.utility.HelperUtils;
//import global.citytech.finposframework.utility.IsoMessageUtils;
//import global.citytech.finposframework.utility.JsonUtils;
//import global.citytech.finposframework.utility.StringUtils;
//
//
//public class PurchaseUseCase extends TransactionUseCase implements UseCase<PurchaseRequestModel, PurchaseResponseModel>,
//        PurchaseRequester<PurchaseRequestModel, PurchaseResponseModel> {
//    static CountDownLatch countDownLatch;
//    PurchaseRequestModel purchaseRequestModel;
//    ReadCardResponse readCardResponse;
//    String stan;
//    String invoiceNumber;
//    TransactionRequest transactionRequest;
//    String batchNumber;
//    private global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest;
//    private PurchaseRequest isoPurchaseRequest;
//
//    public PurchaseUseCase(TerminalRepository repository, Notifier notifier) {
//        super(repository, notifier);
//    }
//
//    @Override
//    protected String getClassName() {
//        return PurchaseUseCase.class.getName();
//    }
//
//    @Override
//    public PurchaseResponseModel execute(PurchaseRequestModel request) {
//        this.purchaseRequestModel = request;
//        try {
//            logger.log(":: REACHED PURCHASE USE CASE ::");
//            stan = this.terminalRepository.getSystemTraceAuditNumber();
//            invoiceNumber = this.terminalRepository.getInvoiceNumber();
//            batchNumber = this.terminalRepository.getReconciliationBatchNumber();
//            transactionRequest = request.getTransactionRequest();
//            this.transactionRepository = request.getTransactionRepository();
//            if (!this.isDeviceReady(request.getDeviceController(), transactionRequest.getTransactionType()))
//                throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
//            this.initTransaction(request.getApplicationRepository(), request.getTransactionRequest());
//            ReadCardRequest readCardRequest = prepareReadCardRequest(stan, request.getApplicationRepository(),
//                    transactionRequest);
//            this.notifier.notify(Notifier.EventType.STARTING_READ_CARD);
//            readCardResponse = this.detectCard(readCardRequest, request.getReadCardService(),
//                    request.getLedService());
//            this.notifier.notify(Notifier.EventType.READING_CARD, "Reading Card..");
//            this.logger.debug("::: PURCHASE USE CASE ::: CARD TYPE === " + readCardResponse.getCardDetails().getCardType());
//            this.logger.debug("::: PURCHASE USE CASE ::: READ CARD RESPONSE === " + readCardResponse.toString());
//            boolean isCardConfirmationRequired;
//            switch (readCardResponse.getCardDetails().getCardType()) {
//                case MAG:
//                    isCardConfirmationRequired = true;
//                    this.cardProcessor = new MagneticCardProcessor(
//                            request.getTransactionRepository(),
//                            request.getTransactionAuthenticator(),
//                            request.getApplicationRepository(),
//                            request.getReadCardService(),
//                            this.notifier,
//                            this.terminalRepository
//                    );
//                    break;
//                case ICC:
//                    isCardConfirmationRequired = true;
//                    this.cardProcessor = new IccCardProcessor(request.getReadCardService(),
//                            request.getTransactionRepository(), request.getApplicationRepository(),
//                            request.getTransactionAuthenticator(), notifier, this.terminalRepository);
//                    break;
//                case PICC:
//                    isCardConfirmationRequired = false;
//                    this.cardProcessor = new PICCCardProcessor(request.getTransactionRepository(),
//                            request.getApplicationRepository(), request.getReadCardService(), this.notifier,
//                            request.getTransactionAuthenticator(), request.getLedService(), request.getSoundService(),
//                            this.terminalRepository);
//                    break;
//                default:
//                    isCardConfirmationRequired = true;
//                    break;
//            }
//            readCardResponse = this.cardProcessor.validateReadCardResponse(readCardRequest, readCardResponse);
//            this.logger.debug("::: CARD TYPE AFTER VALIDATE READ CARD RESPONSE === " + readCardResponse.getCardDetails().getCardType());
//            this.logger.debug("::: FALLBACK TO ICC VALIDATE READ CARD RESPONSE === " + readCardResponse.getFallbackIccToMag());
//            this.logger.debug("::: CARD SCHEME AFTER VALIDATE READ CARD RESPONSE ::: " + readCardResponse.getCardDetails().getCardScheme());
//            this.checkForCardTypeChange(readCardResponse.getCardDetails().getCardType(), request);
//            if (!this.isValidTransaction(
//                    request.getApplicationRepository(),
//                    request.getTransactionRequest(),
//                    readCardResponse)
//            ) {
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
//            }
//            validateIfEmiTransaction(readCardResponse);
//            if (!this.isTransactionAuthorizationSuccess(request.getTransactionAuthenticator(),
//                    request.getApplicationRepository(), transactionRequest.getTransactionType(),
//                    readCardResponse))
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
//            purchaseRequest =
//                    preparePurchaseCardRequest(transactionRequest, readCardResponse.getCardDetails(),
//                            readCardResponse.getFallbackIccToMag());
//            if (isCardConfirmationRequired) {
//                final boolean[] isConfirm = {false};
//                countDownLatch = new CountDownLatch(1);
//                this.notifier.notify(Notifier.EventType.CARD_CONFIRMATION_EVENT,
//                        readCardResponse.getCardDetails(),
//                        (CardConfirmationListener) confirmed -> {
//                            isConfirm[0] = confirmed;
//                            countDownLatch.countDown();
//                        });
//                countDownLatch.await();
//                if (!isConfirm[0]) {
//                    throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
//                }
//            }
//            return processCardAndProceed();
//        } catch (PosException ex) {
//            ex.printStackTrace();
//            throw ex;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        } finally {
//            request.getReadCardService().cleanUp();
//            request.getLedService().doTurnLedWith(new LedRequest(LedLight.ALL, LedAction.OFF));
//            PICCWave.getInstance().clear();
//            MagneticSwipe.getInstance().clear();
//        }
//    }
//
//
//    private PurchaseResponseModel processCardAndProceed() {
//        readCardResponse = this.cardProcessor.processCard(stan, purchaseRequest);
//        this.logger.debug("::: FALLBACK TO ICC VALIDATE READ CARD RESPONSE === " + readCardResponse.getFallbackIccToMag());
//        logger.debug("::: READ CARD RESPONSE ::: " + readCardResponse.toString());
//        this.notifier.notify(Notifier.EventType.TRANSMITTING_REQUEST, "Transmitting Request..");
//        try {
//            IsoMessageResponse purchaseIsoMessageResponse = this.sendIsoMessage(stan, invoiceNumber, readCardResponse, purchaseRequestModel);
//            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait..");
//            InvalidResponseHandlerResponse invalidResponseHandlerResponse = this.checkForInvalidResponseFromHost(purchaseIsoMessageResponse);
//            if (invalidResponseHandlerResponse.isInvalid())
//                return this.handleInvalidResponseFromHost(transactionRequest,
//                        invalidResponseHandlerResponse, batchNumber, stan, invoiceNumber,
//                        readCardResponse, purchaseRequestModel);
//            boolean isPurchaseApproved = this.isTransactionApprovedByActionCode(purchaseIsoMessageResponse);
//            this.notifier.notify(Notifier.EventType.PROCESSING_ISSUER_SCRIPT, "Processing..");
//            readCardResponse = this.cardProcessor.processOnlineResult(readCardResponse, purchaseIsoMessageResponse);
//            if (this.isTransactionDeclineByCardEvenIfSuccessOnSwitch(purchaseIsoMessageResponse, readCardResponse))
//                return this.handleApprovedTransactionDeclinedByCard(stan, batchNumber, invoiceNumber,
//                        readCardResponse, purchaseRequestModel, transactionRequest);
//            if (this.isForceContactCardPromptActionCode(readCardResponse, purchaseIsoMessageResponse))
//                return this.handleForceContactCardPromptFromHost(purchaseRequestModel);
//            this.notifyTransactionCompletion(stan, batchNumber, invoiceNumber, isPurchaseApproved,
//                    transactionRequest, purchaseRequestModel.getTransactionRepository(), purchaseIsoMessageResponse,
//                    readCardResponse, purchaseRequestModel.getApplicationRepository(), purchaseRequestModel.getLedService());
//            boolean shouldPrintReceipt = this.printReceipt(batchNumber, stan, invoiceNumber,
//                    purchaseRequestModel.getPrinterService(),
//                    purchaseRequestModel.getTransactionRequest(),
//                    purchaseRequestModel.getTransactionRepository(),
//                    readCardResponse,
//                    purchaseIsoMessageResponse,
//                    purchaseRequestModel.getApplicationRepository()
//            );
//            return this.prepareResponse(stan, purchaseIsoMessageResponse, isPurchaseApproved,
//                    purchaseRequestModel.getApplicationRepository(), shouldPrintReceipt);
//        } catch (FinPosException e) {
//            if (this.isResponseNotReceivedException(e)) {
//                return this.handleTimeoutTransaction(transactionRequest, batchNumber, stan,
//                        invoiceNumber, readCardResponse, purchaseRequestModel);
//            } else if (e.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT) {
//                logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED UNABLE TO CONNECT");
//                transactionRepository.removeAutoReversal(this.autoReversal);
//                throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
//            } else {
//                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//            }
//        }
//    }
//
//    private PurchaseResponseModel handleForceContactCardPromptFromHost(PurchaseRequestModel request) {
//        this.updateTransactionIds(false);
//        request.getReadCardService().cleanUp();
//        ForceContactCardPurchaseUseCase forceContactCardPurchaseUseCase = new ForceContactCardPurchaseUseCase(
//                this.terminalRepository,
//                this.notifier
//        );
//        return forceContactCardPurchaseUseCase.execute(request);
//    }
//
//    private PurchaseResponseModel handleApprovedTransactionDeclinedByCard(String stan, String batchNumber,
//                                                                          String invoiceNumber,
//                                                                          ReadCardResponse readCardResponse,
//                                                                          PurchaseRequestModel request,
//                                                                          TransactionRequest transactionRequest) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, ReasonForReversal.Type.DECLINED_BY_CARD.getDescription());
//        this.performReversal(stan, readCardResponse, request);
//        boolean shouldPrint = !this.approvePrintOnly(request.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest, readCardResponse,
//                    this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.DECLINED_BY_CARD,
//                            ReasonForReversal.Type.DECLINED_BY_CARD.getDescription()));
//            new NiblTransactionReceiptPrintHandler(request.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            request.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PurchaseResponseModel.Builder.createDefaultBuilder()
//                .isApproved(false)
//                .message(ReasonForReversal.Type.DECLINED_BY_CARD.getDescription())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private void checkForCardTypeChange(CardType cardType, PurchaseRequestModel request) {
//        switch (cardType) {
//            case MAG:
//                this.cardProcessor = new global.citytech.finpos.processor.nibl.transaction.card.MagneticCardProcessor(
//                        request.getTransactionRepository(),
//                        request.getTransactionAuthenticator(),
//                        request.getApplicationRepository(),
//                        request.getReadCardService(),
//                        this.notifier,
//                        this.terminalRepository
//                );
//                break;
//            case ICC:
//                this.cardProcessor = new global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor(request.getReadCardService(),
//                        request.getTransactionRepository(), request.getApplicationRepository(),
//                        request.getTransactionAuthenticator(), notifier, this.terminalRepository);
//                break;
//            case PICC:
//                this.cardProcessor = new PICCCardProcessor(request.getTransactionRepository(),
//                        request.getApplicationRepository(), request.getReadCardService(), this.notifier,
//                        request.getTransactionAuthenticator(), request.getLedService(), request.getSoundService(),
//                        this.terminalRepository);
//                break;
//            default:
//                break;
//        }
//    }
//
//    private PurchaseResponseModel handleInvalidResponseFromHost(TransactionRequest transactionRequest,
//                                                                InvalidResponseHandlerResponse invalidResponseHandlerResponse,
//                                                                String batchNumber, String stan, String invoiceNumber,
//                                                                ReadCardResponse readCardResponse, PurchaseRequestModel request) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, invalidResponseHandlerResponse.getMessage());
//        this.performReversal(stan, readCardResponse, request);
//        boolean shouldPrint = !this.approvePrintOnly(request.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest,
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.INVALID_RESPONSE,
//                            invalidResponseHandlerResponse.getMessage()));
//            new NiblTransactionReceiptPrintHandler(request.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            request.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PurchaseResponseModel.Builder.createDefaultBuilder()
//                .isApproved(false)
//                .message(invalidResponseHandlerResponse.getMessage())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private PurchaseResponseModel handleTimeoutTransaction(TransactionRequest transactionRequest,
//                                                           String batchNumber, String stan,
//                                                           String invoiceNumber, ReadCardResponse readCardResponse,
//                                                           PurchaseRequestModel request) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, "TIMEOUT");
//        this.setUnableToGoOnlineResultToCard(readCardResponse, request.getReadCardService());
//        this.performReversal(stan, readCardResponse, request);
//        boolean shouldPrint = !this.approvePrintOnly(request.getApplicationRepository());
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest,
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.TIMEOUT, "Timeout"));
//            new NiblTransactionReceiptPrintHandler(request.getPrinterService())
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            request.getTransactionRepository().updateReceiptLog(receiptLog);
//        }
//        return PurchaseResponseModel.Builder.createDefaultBuilder()
//                .message("TIMEOUT")
//                .isApproved(false)
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private PurchaseResponseModel prepareResponse(
//            String stan,
//            IsoMessageResponse response,
//            boolean isPurchaseApproved,
//            ApplicationRepository applicationRepository,
//            boolean shouldPrintReceipt) {
//        PurchaseResponseModel.Builder builder = PurchaseResponseModel.Builder.createDefaultBuilder();
//        builder.isApproved(isPurchaseApproved);
//        builder.debugRequestMessage(response.getDebugRequestString());
//        builder.debugResponseMessage(response.getDebugResponseString());
//        builder.stan(stan);
//        builder.message(this.retrieveMessage(response, isPurchaseApproved, applicationRepository));
//        builder.shouldPrintCustomerCopy(shouldPrintReceipt);
//        return builder.build();
//    }
//
//    private IsoMessageResponse sendIsoMessage(String stan, String invoiceNumber, ReadCardResponse readCardResponse, PurchaseRequestModel requestModel) {
//
//        RequestContext context = this.prepareTemplateRequestContext(stan);
//        logger.debug(String.format(" Request Context ::%s", context));
//        this.isoPurchaseRequest = preparePurchaseRequest(invoiceNumber, readCardResponse);
//        context.setRequest(this.isoPurchaseRequest);
//        this.autoReversal = new AutoReversal(TransactionType.PURCHASE,
//                JsonUtils.toJsonObj(prepareReversalRequest(stan, readCardResponse, requestModel)),
//                0,
//                AutoReversal.Status.ACTIVE,
//                StringUtils.dateTimeStamp());
//        logger.log("::: AUTO REVERSAL ::: SAVING AUTO REVERSAL ");
//        transactionRepository.saveAutoReversal(this.autoReversal);
//        PurchaseRequestSender sender;
//        switch (readCardResponse.getCardDetails().getCardType()) {
//            case MAG:
//                sender = new MagPurchaseRequestSender(new NIBLSpecInfo(), context);
//                break;
//            case ICC:
//            default:
//                sender = new IccPurchaseRequestSender(new NIBLSpecInfo(), context);
//                break;
//            case PICC:
//                sender = new PICCPurchaseRequestSender(new NIBLSpecInfo(), context);
//                break;
//        }
//        IsoMessageResponse isoMessageResponse = sender.send();
//        logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED TRANSACTION SUCCESS ");
//        transactionRepository.removeAutoReversal(this.autoReversal);
//        return isoMessageResponse;
//    }
//
//    private PurchaseRequest preparePurchaseRequest(String invoiceNumber, ReadCardResponse readCardResponse) {
//        PurchaseRequest request = new PurchaseRequest();
//        request.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        request.setTransactionAmount(readCardResponse.getCardDetails().getAmount());
//        request.setPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()));
//        request.setPosConditionCode("00"); //TODO Change later
//        request.setTrack2Data(readCardResponse.getCardDetails().getTrackTwoData());
//        if (readCardResponse.getCardDetails().getPinBlock().getOfflinePin() ||
//                StringUtils.isEmpty(readCardResponse.getCardDetails().getPinBlock().getPin()) ||
//                readCardResponse.getCardDetails().getPinBlock().getPin().equalsIgnoreCase("offline"))
//            request.setPinBlock("");
//        else
//            request.setPinBlock(readCardResponse.getCardDetails().getPinBlock().getPin());
//        request.setEmvData(readCardResponse.getCardDetails().getIccDataBlock());
//        request.setExpireDate(readCardResponse.getCardDetails().getExpiryDate());
//        request.setCardSequenceNumber(readCardResponse.getCardDetails().getPrimaryAccountNumberSerialNumber());
//        request.setLocalTime(HelperUtils.getDefaultLocaleTimeHhMmSs());
//        request.setLocalDate(HelperUtils.getDefaultLocaleDateWithoutYear());
//        request.setInvoiceNumber(invoiceNumber);
//        this.logger.debug("::: FALLBACK TO ICC PURCHASE REQUEST === " + readCardResponse.getFallbackIccToMag());
//        request.setFallbackFromIccToMag(readCardResponse.getFallbackIccToMag());
//        return request;
//    }
//
//    private global.citytech.finposframework.usecases.transaction.data.PurchaseRequest preparePurchaseCardRequest(
//            TransactionRequest transactionRequest,
//            CardDetails cardDetails,
//            boolean fallbackIccToMag) {
//        global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest
//                = new global.citytech.finposframework.usecases.transaction.data.PurchaseRequest(
//                transactionRequest.getTransactionType(),
//                transactionRequest.getAmount(),
//                transactionRequest.getAdditionalAmount()
//        );
//        cardDetails.setAmount(transactionRequest.getAmount());
//        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
//        purchaseRequest.setCardDetails(cardDetails);
//        purchaseRequest.setFallbackIccToMag(fallbackIccToMag);
//        return purchaseRequest;
//    }
//
//    private void performReversal(
//            String stan,
//            ReadCardResponse readCardResponse,
//            PurchaseRequestModel purchaseRequestModel
//    ) {
//        ReversalUseCase reversalUseCase = new ReversalUseCase(
//                this.prepareReversalRequest(stan, readCardResponse, purchaseRequestModel),
//                this.terminalRepository,
//                this.notifier
//        );
//        try {
//            reversalUseCase.execute(
//                    prepareReversalRequestModel(
//                            purchaseRequestModel,
//                            readCardResponse
//                    )
//            );
//            logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED REVERSAL SUCCESS ");
//            this.transactionRepository.removeAutoReversal(this.autoReversal);
//        } catch (FinPosException e) {
//            e.printStackTrace();
//            logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL NOT REMOVED REVERSAL EXCEPTION ");
//        }
//    }
//
//    private NiblReversalRequest prepareReversalRequest(
//            String stan,
//            ReadCardResponse readCardResponse,
//            PurchaseRequestModel purchaseRequestModel
//    ) {
//        NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
//        niblReversalRequest.setProcessingCode(ProcessingCode.PURCHASE.getCode());
//        niblReversalRequest.setAmount(
//                purchaseRequestModel.getTransactionRequest().getAmount()
//        );
//        niblReversalRequest.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        niblReversalRequest.setExpiryDate(readCardResponse.getCardDetails().getExpiryDate());
//        niblReversalRequest.setPosEntryMode(this.isoPurchaseRequest.getPosEntryMode());
//        niblReversalRequest.setPosConditionCode(this.isoPurchaseRequest.getPosConditionCode());
//        niblReversalRequest.setTrack2Data(this.isoPurchaseRequest.getTrack2Data());
//        niblReversalRequest.setPinBlock(this.isoPurchaseRequest.getPinBlock());
//        niblReversalRequest.setInvoiceNumber(this.isoPurchaseRequest.getInvoiceNumber());
//        niblReversalRequest.setStan(stan);
//        return niblReversalRequest;
//    }
//
//    private ReversalRequestModel prepareReversalRequestModel(
//            PurchaseRequestModel purchaseRequestModel,
//            ReadCardResponse readCardResponse
//    ) {
//        return ReversalRequestModel.Builder.newInstance()
//                .withTransactionRequest(
//                        purchaseRequestModel.getTransactionRequest()
//                )
//                .withTransactionRepository(
//                        purchaseRequestModel.getTransactionRepository()
//                )
//                .withPrinterService(
//                        purchaseRequestModel.getPrinterService()
//                )
//                .withApplicationRepository(
//                        purchaseRequestModel.getApplicationRepository()
//                )
//                .withReadCardResponse(readCardResponse)
//                .build();
//    }
//
//    private void notifyTransactionCompletion(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean purchaseApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository,
//            LedService ledService
//    ) {
//        this.updateTransactionLog(
//                stan,
//                batchNumber,
//                invoiceNumber,
//                purchaseApproved,
//                transactionRequest,
//                transactionRepository,
//                isoMessageResponse,
//                readCardResponse,
//                applicationRepository
//        );
//        this.updateTransactionIds(purchaseApproved);
//        this.notifyTransactionStatus(
//                purchaseApproved,
//                isoMessageResponse,
//                applicationRepository,
//                ledService,
//                readCardResponse.getCardDetails().getCardType() == CardType.PICC
//        );
//    }
//
//    private void updateTransactionLog(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean purchaseApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository
//    ) {
//        this.logger.log("::: NIBL ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
//        this.logger.log("::: NIBL ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
//        this.logger.log("STAN >>> " + stan);
//        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
//                .withStan(stan)
//                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
//                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
//                .withInvoiceNumber(invoiceNumber)
//                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
//                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
//                .withTransactionType(TransactionType.PURCHASE)
//                .withOriginalTransactionType(TransactionType.PURCHASE)
//                .withTransactionAmount(transactionRequest.getAmount())
//                .withOriginalTransactionAmount(transactionRequest.getAmount())
//                .withTransactionDate(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_DATE))
//                .withTransactionTime(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_TIME))
//                .withTransactionStatus(purchaseApproved ? "APPROVED" : "DECLINED")
//                .withPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
//                .withOriginalPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
//                .withPosConditionCode(this.isoPurchaseRequest.getPosConditionCode())
//                .withOriginalPosConditionCode("")
//                .withReconcileStatus("")
//                .withReconcileTime("")
//                .withReconcileDate("")
//                .withReconcileBatchNo(batchNumber)
//                .withReadCardResponse(readCardResponse)
//                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse,
//                        applicationRepository, batchNumber, stan, invoiceNumber))
//                .withAuthorizationCompleted(true)
//                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
//                .withTransactionVoided(false)
//                .withTipAdjusted(false)
//                .withIsEmiTransaction(purchaseRequestModel.getTransactionRequest().isEmiTransaction())
//                .withEmiInfo(purchaseRequestModel.getTransactionRequest().getEmiInfo())
//                .build();
//        transactionRepository.updateTransactionLog(transactionLog);
//    }
//
//    private void validateIfEmiTransaction(ReadCardResponse readCardResponse) {
//        boolean isEmiTransaction = purchaseRequestModel.getTransactionRequest().isEmiTransaction();
//        if (isEmiTransaction) {
//            CardDetails cardDetails = readCardResponse.getCardDetails();
//            String primaryAccountNumber = cardDetails.getPrimaryAccountNumber();
//            List<Bin> transactionBins = transactionRepository.getTransactionBins();
//            validateForAllowedBin(primaryAccountNumber, transactionBins);
//        }
//    }
//}