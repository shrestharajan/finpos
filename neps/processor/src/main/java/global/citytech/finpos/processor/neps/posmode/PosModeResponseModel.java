package global.citytech.finpos.processor.neps.posmode;

import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.posmode.PosMode;
import global.citytech.finposframework.switches.posmode.PosModeResponseParameter;

/**
 * Created by Unique Shakya on 1/20/2021.
 */
public class PosModeResponseModel implements PosModeResponseParameter, UseCase.Response {

    private PosMode posMode;

    public PosModeResponseModel(PosMode posMode) {
        this.posMode = posMode;
    }

    public PosMode getPosMode() {
        return posMode;
    }
}
