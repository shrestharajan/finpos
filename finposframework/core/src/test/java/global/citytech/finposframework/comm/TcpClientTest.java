package global.citytech.finposframework.comm;

import org.junit.Assert;
import org.junit.Test;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finposframework.exceptions.FinPosException;

/** User: Surajchhetry Date: 2/20/20 Time: 11:23 AM */
public class TcpClientTest {

  private Logger LOGGER = Logger.getLogger(TcpClientTest.class.getName());

  @Test(expected = FinPosException.class)
  public void dialTest() {
    HostInfo primaryHost =
        HostInfo.Builder.createDefaultBuilder("127.0.0.1", 0)
            .addEventListener(
                (event, context) -> {
                  LOGGER.log(context.get("MSG").toString());
                })
            .build();
    HostInfo secondaryHost =
        HostInfo.Builder.createDefaultBuilder("127.0.0.1", 0)
            .addEventListener(
                (event, context) -> {
                  LOGGER.log(context.get("MSG").toString());
                })
            .build();
    ConnectionParam connectionParam = new ConnectionParam(primaryHost, secondaryHost);

    TcpClient tcpClient = new TcpClient(connectionParam);
    //   when(tcpClient.dial()).thenReturn()
    HostInfo result = tcpClient.dial();
    Assert.assertNull(result);
  }
}
