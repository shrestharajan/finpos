package global.citytech.finposframework.switches.receipt.customercopy;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 9/2/2020.
 */
public interface CustomerCopyRequester<T extends CustomerCopyRequestParameter,
        E extends CustomerCopyResponseParameter>
        extends Request<T, E> {
}
