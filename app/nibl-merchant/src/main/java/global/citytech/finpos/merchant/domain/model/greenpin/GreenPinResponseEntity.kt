package global.citytech.finpos.merchant.domain.model.greenpin

data class
GreenPinResponseEntity(
    val stan:String? = null,
    val message: String = "",
    val isApproved: Boolean
)