package global.citytech.finpos.processor.neps.transaction;

import java.util.concurrent.CountDownLatch;

import global.citytech.finpos.neps.iso8583.NepsMessageSenderTemplate;
import global.citytech.finpos.neps.iso8583.PosConditionCode;
import global.citytech.finpos.neps.iso8583.SmartVistaActionCode;
import global.citytech.finpos.neps.iso8583.requestsender.transaction.TransactionIsoRequest;
import global.citytech.finpos.neps.iso8583.requestsender.voidsale.VoidSaleIsoRequest;
import global.citytech.finpos.processor.neps.NepsConstant;
import global.citytech.finpos.processor.neps.cardprocessor.CardProcessor;
import global.citytech.finpos.processor.neps.reversal.ReversalRequest;
import global.citytech.finpos.processor.neps.reversal.ReversalUseCase;
import global.citytech.finposframework.comm.ConnectionParam;
import global.citytech.finposframework.comm.HostInfo;
import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.exceptions.PosError;
import global.citytech.finposframework.exceptions.PosException;
import global.citytech.finposframework.hardware.io.cards.CardDetails;
import global.citytech.finposframework.hardware.io.cards.CardType;
import global.citytech.finposframework.hardware.io.cards.IccData;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.listeners.CardConfirmationListener;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.notifier.Notifier;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TerminalRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.PosEntryMode;
import global.citytech.finposframework.switches.transaction.TransactionRequester;
import global.citytech.finposframework.usecases.TransactionStatusFromHost;
import global.citytech.finposframework.usecases.TransactionType;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.terminal.TerminalInfo;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandler;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
import global.citytech.finposframework.usecases.transaction.ReceiptHandler;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;
import global.citytech.finposframework.usecases.transaction.TransactionAuthorizer;
import global.citytech.finposframework.usecases.transaction.TransactionInitializer;
import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
import global.citytech.finposframework.usecases.transaction.data.TransactionAuthorizationResponse;
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
import global.citytech.finposframework.utility.DateUtils;
import global.citytech.finposframework.utility.IsoMessageUtils;
import global.citytech.finposframework.utility.JsonUtils;
import global.citytech.finposframework.utility.PosResponse;
import global.citytech.finposframework.utility.PosResult;
import global.citytech.finposframework.utility.StringUtils;

import static global.citytech.finposframework.utility.TMSConfigurationConstants.APPROVED_PRINT_ONLY;

/**
 * Created by Saurav Ghimire on 2/2/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public abstract class TransactionExecutorTemplate implements UseCase<TransactionRequest, TransactionResponse>, TransactionRequester<TransactionRequest, TransactionResponse> {

    static CountDownLatch countDownLatch;

    protected ReadCardResponse readCardResponse;
    protected CardProcessor cardProcessor;
    protected String stan;
    protected String invoiceNumber;

    protected String batchNumber;
    protected global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest;
    protected ApplicationRepository applicationRepository;
    protected Logger logger;
    protected TransactionRequest request;
    protected Notifier notifier;
    protected AutoReversal autoReversal;

    protected TerminalRepository terminalRepository;
    protected boolean isCardConfirmationRequired;

    private NepsTransactionValidator transactionValidator;
    protected ReceiptLog receiptLog;
    protected String currencyCode;

    protected TransactionExecutorTemplate(TerminalRepository terminalRepository, Notifier notifier) {
        this.terminalRepository = terminalRepository;
        this.notifier = notifier;
        this.logger = Logger.getLogger(getClassName());
    }

    protected abstract void retrieveCardResponse();

    protected abstract void processCard();

    protected abstract String getClassName();

    protected abstract TransactionIsoRequest prepareIsoRequest(ReadCardResponse readCardResponse, global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest);

    protected abstract NepsMessageSenderTemplate getRequestSender(ReadCardResponse readCardResponse, RequestContext context);

    protected abstract void processOnlineResult(IsoMessageResponse isoMessageResponse);

    protected abstract void updateTransactionLog(boolean transactionApproved, global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest, TransactionRepository transactionRepository, IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse);

    protected abstract void updateTransactionIdsAndNotifyTransactionStatus(boolean transactionApproved, IsoMessageResponse isoMessageResponse, TransactionRequest request, ReadCardResponse readCardResponse);

    protected abstract TransactionResponse prepareResponse(IsoMessageResponse isoMessageResponse, boolean transactionApproved, ApplicationRepository applicationRepository, boolean shouldPrint);

    protected abstract String getProcessingCode();

    protected abstract TransactionResponse prepareErrorResponse(String responseMessage, boolean shouldPrint);

    protected abstract TransactionResponse promptToPerformIccTransaction();

    @Override
    public TransactionResponse execute(TransactionRequest request) {
        setUpRequiredTransactionFields(request);
        checkIfDeviceReady();
        this.initTransaction(
                request.getApplicationRepository(),
                transactionRequest
        );
        this.retrieveCardResponse();
        return checkTransactionValidity();
    }

    protected TransactionResponse checkTransactionValidity() {
        try {
            if (!validTransaction(
                    request.getApplicationRepository(),
                    transactionRequest,
                    readCardResponse
            )) {
                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
            }
            return proceedValidTransaction();
        } catch (PosException ex) {
            throw ex;
        } catch (Exception exception) {
            exception.printStackTrace();
            throw new PosException(PosError.DEVICE_ERROR_PINPAD_TIMEOUT);
        }
    }

    private TransactionResponse proceedValidTransaction() throws InterruptedException {
        checkTransactionAuthorizationAndProceed();
        return proceedAuthorizedTransaction();
    }

    private TransactionResponse proceedAuthorizedTransaction() throws InterruptedException {
        if (isCardConfirmationRequired) {
            final boolean[] isConfirm = {false};
            countDownLatch = new CountDownLatch(1);
            this.notifier.notify(Notifier.EventType.CARD_CONFIRMATION_EVENT,
                    readCardResponse.getCardDetails(),
                    new CardConfirmationListener() {
                        @Override
                        public void onResult(boolean confirmed) {
                            isConfirm[0] = confirmed;
                            countDownLatch.countDown();
                        }
                    });
            countDownLatch.await();
            if (!isConfirm[0]) {
                throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
            }
        }
        this.processCard();
        this.notifier.notify(Notifier.EventType.TRANSMITTING_REQUEST,
                Notifier.EventType.TRANSMITTING_REQUEST.getDescription());
        return performTransactionWithHost();
    }

    private TransactionResponse performTransactionWithHost() {
        try {
            VoidSaleIsoRequest reversalIsoRequest = prepareReversalIsoRequest();
            this.autoReversal = new AutoReversal(request.transactionRequest.getTransactionType(),
                    JsonUtils.toJsonObj(reversalIsoRequest),
                    0,
                    AutoReversal.Status.ACTIVE,
                    StringUtils.dateTimeStamp(),
                    readCardResponse);
            request.transactionRepository.saveAutoReversal(autoReversal);
            IsoMessageResponse isoMessageResponse = this.sendIsoMessage();
//            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait..");
            return checkForInvalidResponseAndProceed(isoMessageResponse);
        } catch (FinPosException ex) {
            return this.handleFinposExceptionForTransaction(ex.getType());
        }

    }

    protected TransactionResponse checkForInvalidResponseAndProceed(IsoMessageResponse isoMessageResponse) {
        InvalidResponseHandlerResponse invalidResponseHandlerResponse = this.checkForInvalidResponseFromHost(isoMessageResponse);
        if (invalidResponseHandlerResponse.isInvalid()) {
            request.transactionRepository.removeAutoReversal(autoReversal);
            return this.handleInvalidResponseFromHost(invalidResponseHandlerResponse);
        } else {
            return proceedValidResponseFromHost(isoMessageResponse);
        }
    }

    private TransactionResponse proceedValidResponseFromHost(IsoMessageResponse isoMessageResponse) {
        if (isIccTransactionRequired(isoMessageResponse)) {
            request.transactionRepository.removeAutoReversal(autoReversal);
            return promptToPerformIccTransaction();
        }
        boolean transactionApproved = this.transactionApprovedByActionCode(isoMessageResponse);
        if (isCardTypeNonChipOrNonEmv(readCardResponse)) {
            request.transactionRepository.removeAutoReversal(autoReversal);
            return finalizeCompletedTransaction(isoMessageResponse, transactionApproved);
        } else {
            return processIsoResponseToChipOrEmvCard(isoMessageResponse, transactionApproved);
        }
    }

    protected boolean isIccTransactionRequired(IsoMessageResponse isoMessageResponse) {
        int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(
                isoMessageResponse,
                DataElement.RESPONSE_CODE
        );
        CardDetails cardDetails = readCardResponse.getCardDetails();
        if (cardDetails == null) {
            return false;
        }
        return (actionCode == SmartVistaActionCode.ACTION_CODE_123.getActionCode()
                && cardDetails.getCardType() == CardType.PICC);
    }


    protected boolean isCardTypeNonChipOrNonEmv(ReadCardResponse readCardResponse) {
        boolean isNonChipOrNonEmv = false;
        CardDetails cardDetails = readCardResponse.getCardDetails();
        if (cardDetails != null) {
            isNonChipOrNonEmv = cardDetails.getCardType() == CardType.MAG ||
                    cardDetails.getCardType() == CardType.MANUAL;
        }
        return isNonChipOrNonEmv;
    }

    protected TransactionResponse processIsoResponseToChipOrEmvCard(
            IsoMessageResponse isoMessageResponse,
            boolean transactionApproved
    ) {
        processOnlineResult(isoMessageResponse);
        return checkIfApprovedTransactionDeclinedByCardAndProceed(
                isoMessageResponse,
                transactionApproved
        );
    }

    private TransactionResponse checkIfApprovedTransactionDeclinedByCardAndProceed(
            IsoMessageResponse isoMessageResponse,
            boolean transactionApproved
    ) {
        request.transactionRepository.removeAutoReversal(autoReversal);
        if (this.isTransactionDeclineByCardEvenIfSuccessOnSwitch(isoMessageResponse))
            return this.handleApprovedTransactionDeclinedByCard();
        else {
            return finalizeCompletedTransaction(isoMessageResponse, transactionApproved);
        }
    }

    protected TransactionResponse finalizeCompletedTransaction(
            IsoMessageResponse isoMessageResponse,
            boolean transactionApproved
    ) {
        this.notifyTransactionCompletion(transactionApproved,
                transactionRequest, request, isoMessageResponse, readCardResponse);

        String message = retrieveMessage(isoMessageResponse, transactionApproved, request.getApplicationRepository());
        this.notifier.notify(Notifier.EventType.PUSH_MERCHANT_LOG, this.stan, message, transactionApproved);

        boolean shouldPrint = this.prepareReceiptLog(isoMessageResponse, transactionApproved);

        this.printReceipt(shouldPrint);
        return this.prepareResponse(isoMessageResponse, transactionApproved,
                request.getApplicationRepository(), shouldPrint);

    }

    private void checkTransactionAuthorizationAndProceed() {
        if (!this.isTransactionAuthorizationSuccess(request.getTransactionAuthenticator(),
                request.getApplicationRepository(), transactionRequest.getTransactionType(),
                readCardResponse)) {
            throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
        }
    }

    protected void checkIfDeviceReady() {
        DeviceController deviceController = request.deviceController;
        TransactionType transactionType = transactionRequest.getTransactionType();
        PosResponse deviceReadyResponse = deviceController.isReady(TransactionUtils
                .getAllowedCardEntryModes(transactionType));
        if (deviceReadyResponse.getPosResult() == PosResult.SUCCESS) {
            return;
        }
        throw new PosException(deviceReadyResponse.getPosError());
    }

    protected void setUpRequiredTransactionFields(TransactionRequest request) {
        this.request = request;
        stan = this.terminalRepository.getSystemTraceAuditNumber();
        invoiceNumber = this.terminalRepository.getInvoiceNumber();
        batchNumber = this.terminalRepository.getReconciliationBatchNumber();
        transactionRequest = request.getTransactionRequest();
        applicationRepository = request.getApplicationRepository();
    }

    protected TransactionResponse handleApprovedTransactionDeclinedByCard() {
        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, ReasonForReversal.Type.DECLINED_BY_CARD.getDescription(), Notifier.TransactionType.CARD.name());
        this.receiptLog = new ReversedTransactionReceiptHandler(transactionRequest, readCardResponse,
                this.terminalRepository)
                .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.DECLINED_BY_CARD,
                        ReasonForReversal.Type.DECLINED_BY_CARD.getDescription()));
        this.performAutoReversal();
        boolean shouldPrint = !this.approvePrintOnly(request.applicationRepository);
        if (shouldPrint) {
            new NepsTransactionReceiptPrintHandler(request.getPrinterService())
                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
            request.getTransactionRepository().updateReceiptLog(receiptLog);
        }
        return prepareErrorResponse(ReasonForReversal.Type.DECLINED_BY_CARD.getDescription(), shouldPrint);
    }

    protected boolean isTransactionDeclineByCardEvenIfSuccessOnSwitch(IsoMessageResponse isoMessageResponse) {
        return transactionValidator.isTransactionDeclineByCardEvenIfSuccessOnSwitch(
                isoMessageResponse,
                readCardResponse
        );
    }

    protected TransactionResponse handleInvalidResponseFromHost(InvalidResponseHandlerResponse invalidResponseHandlerResponse) {
        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, invalidResponseHandlerResponse.getMessage(), Notifier.TransactionType.CARD.name());
        this.receiptLog = new ReversedTransactionReceiptHandler(transactionRequest,
                readCardResponse, this.terminalRepository)
                .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.INVALID_RESPONSE,
                        invalidResponseHandlerResponse.getMessage()));
        this.performAutoReversal();
        boolean shouldPrint = !this.approvePrintOnly(request.getApplicationRepository());
        if (shouldPrint) {
            new NepsTransactionReceiptPrintHandler(request.getPrinterService())
                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
            request.getTransactionRepository().updateReceiptLog(receiptLog);
        }
        return prepareErrorResponse(invalidResponseHandlerResponse.getMessage(), shouldPrint);
    }

    protected InvalidResponseHandlerResponse checkForInvalidResponseFromHost(IsoMessageResponse isoMessageResponse) {
        InvalidResponseHandler invalidResponseHandler = new NepsInvalidResponseHandler();
        return invalidResponseHandler.isInvalidResponse(isoMessageResponse);
    }



    protected TransactionResponse handleFinposExceptionForTransaction(FinPosException.ExceptionType type) {
        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, type.getMessage(), Notifier.TransactionType.CARD.name());
        this.setUnableToGoOnlineResultToCard();
        this.receiptLog = new ReversedTransactionReceiptHandler(transactionRequest,
                readCardResponse, this.terminalRepository)
                .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.TIMEOUT, type.getMessage()));
        this.performAutoReversal();
        boolean shouldPrint = !this.approvePrintOnly(request.getApplicationRepository());
        if (shouldPrint) {
            new NepsTransactionReceiptPrintHandler(request.getPrinterService())
                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
            request.getTransactionRepository().updateReceiptLog(receiptLog);
        }
        return prepareErrorResponse(type.getMessage(), shouldPrint);
    }

    protected boolean unableToGoOnlineException(FinPosException e) {
        return e.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT ||
                e.getType() == FinPosException.ExceptionType.READ_TIME_OUT ||
                e.getType() == FinPosException.ExceptionType.CONNECTION_ERROR;
    }

    private void performAutoReversal() {
        ReversalUseCase reversalUseCase = new ReversalUseCase(
                this.terminalRepository,
                prepareReversalIsoRequest(),
                this.notifier
        );
        try {
            reversalUseCase.execute(
                    prepareReversalRequest()

            );
            request.transactionRepository.removeAutoReversal(autoReversal);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected ReversalRequest prepareReversalRequest() {
        return ReversalRequest.Builder
                .newInstance()
                .withTransactionRequest(this.transactionRequest)
                .withApplicationRepository(this.request.applicationRepository)
                .withPrinterService(this.request.printerService)
                .withTransactionRepository(this.request.transactionRepository)
                .withProcessingCode(getProcessingCode())
                .withStan(stan)
                .withTrack2Data(this.readCardResponse.getCardDetails().getTrackTwoData())
                .withCurrencyCode(this.currencyCode)
                .withReceiptLog(this.receiptLog)
                .build();
    }

    private void setUnableToGoOnlineResultToCard() {
        if (request.transactionRequest.getTransactionType() == TransactionType.VOID)
            return;
        if (this.isArqcTransaction(readCardResponse)) {
            ReadCardResponse afterSetOnlineResultResponse = request.readCardService.setOnlineResult(
                    -99, ReadCardService.OnlineResult.UNABLE_TO_GO_ONLINE,
                    "", false);
            readCardResponse.getCardDetails().setIccDataBlock(afterSetOnlineResultResponse
                    .getCardDetails().getIccDataBlock());
            readCardResponse.getCardDetails().setTagCollection(afterSetOnlineResultResponse
                    .getCardDetails().getTagCollection());
            readCardResponse.getCardDetails().setTransactionState(afterSetOnlineResultResponse
                    .getCardDetails().getTransactionState());
        }
    }

    private boolean isArqcTransaction(ReadCardResponse readCardResponse) {
        try {
            return readCardResponse
                    .getCardDetails()
                    .getTagCollection()
                    .get(IccData.CID.getTag())
                    .getData()
                    .equals(NepsConstant.ARQC_TRANSACTION_CODE);
        } catch (Exception e) {
            return false;
        }
    }

    protected void initTransaction(ApplicationRepository applicationRepository, global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest) {
        TransactionInitializer transactionInitializer
                = new NepsTransactionInitializer(applicationRepository);
        global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequestAfterInit
                = transactionInitializer.initialize(transactionRequest);
        this.validateRequest(transactionRequestAfterInit, applicationRepository);
    }

    private void validateRequest(global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequestAfterInit, ApplicationRepository applicationRepository) {
        transactionValidator = new NepsTransactionValidator(applicationRepository);
        transactionValidator.validateRequest(transactionRequestAfterInit);
    }


    protected boolean validTransaction(ApplicationRepository applicationRepository, global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest, ReadCardResponse readCardResponse) {
        transactionValidator = new NepsTransactionValidator(applicationRepository);
        return transactionValidator.isValidTransaction(transactionRequest, readCardResponse,
                readCardResponse.getCardDetails().getCardScheme().getCardSchemeId());
    }

    protected boolean isTransactionAuthorizationSuccess(TransactionAuthenticator transactionAuthenticator, ApplicationRepository applicationRepository, TransactionType transactionType, ReadCardResponse readCardResponse) {
        TransactionAuthorizer transactionAuthorizer = new NepsTransactionAuthorizer(transactionAuthenticator,
                applicationRepository);
        TransactionAuthorizationResponse transactionAuthorizationResponse = transactionAuthorizer
                .authorizeUser(transactionType, readCardResponse.getCardDetails().getCardScheme().getCardSchemeId());
        return transactionAuthorizationResponse.isSuccess();
    }

    protected RequestContext prepareTemplateRequestContext(String stan) {
        HostInfo primaryHostInfo = this.terminalRepository.findPrimaryHost();
        HostInfo secondaryHostInfo = this.terminalRepository.findSecondaryHost();
        TerminalInfo terminalInfo = this.terminalRepository.findTerminalInfo();
        ConnectionParam connectionParam = new ConnectionParam(primaryHostInfo, secondaryHostInfo);
        return new RequestContext(stan, terminalInfo, connectionParam);
    }

    protected boolean transactionApprovedByActionCode(IsoMessageResponse isoMessageResponse) {
        int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, 39);
        return SmartVistaActionCode.getByActionCode(actionCode).getTransactionStatusFromHost() == TransactionStatusFromHost.ONLINE_APPROVED;
    }

    private boolean prepareReceiptLog(IsoMessageResponse isoMessageResponse, boolean isApproved) {
        printDebugReceiptIfDebugMode(request.getPrinterService(), isoMessageResponse);
        boolean approvePrintOnly = this.approvePrintOnly(applicationRepository);
        boolean shouldPrintReceipt = isApproved || !approvePrintOnly;
        this.receiptLog = new TransactionReceiptHandler(transactionRequest, readCardResponse,
                isoMessageResponse, terminalRepository, request.getApplicationRepository())
                .prepare(batchNumber, stan, invoiceNumber);
        return shouldPrintReceipt;
    }


    protected boolean printReceipt(boolean shouldPrintReceipt) {
        if (shouldPrintReceipt) {
            ReceiptHandler.TransactionReceiptHandler receiptHandler
                    = new NepsTransactionReceiptPrintHandler(request.getPrinterService());
            request.getTransactionRepository().updateReceiptLog(receiptLog);
            receiptHandler.printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
        }
        return shouldPrintReceipt;
    }

    protected boolean approvePrintOnly(ApplicationRepository applicationRepository) {
        String approvedPrintOnly = applicationRepository
                .retrieveFromAdditionalDataEmvParameters(APPROVED_PRINT_ONLY);
        if (StringUtils.isEmpty(approvedPrintOnly))
            return false;
        return Boolean.parseBoolean(approvedPrintOnly);
    }


    protected void printDebugReceiptIfDebugMode(PrinterService printerService, IsoMessageResponse isoMessageResponse) {
        boolean isDebugVersion = this.terminalRepository.findTerminalInfo().isDebugModeEnabled();
        if (isDebugVersion) {
            ReceiptHandler.IsoMessageReceiptHandler isoMessageReceiptHandler
                    = new NepsIsoMessageReceiptHandler(printerService);
            isoMessageReceiptHandler.printIsoMessageReceipt(isoMessageResponse);
        }
    }

    protected String retrieveMessage(IsoMessageResponse isoMessageResponse, boolean purchaseApproved, ApplicationRepository applicationRepository) {
        if (purchaseApproved)
            return this.approvedMessage(isoMessageResponse);
        else
            return this.declinedMessage(isoMessageResponse, applicationRepository);
    }

    protected String declinedMessage(IsoMessageResponse isoMessageResponse, ApplicationRepository applicationRepository) {
        int actionCode = IsoMessageUtils.retrieveFromDataElementsAsInt(isoMessageResponse, 39);
        if (actionCode < 0)
            throw new IllegalArgumentException("Invalid Action Code");
        String messageText = applicationRepository.getMessageTextByActionCode(actionCode);
        if (StringUtils.isEmpty(messageText))
            messageText = SmartVistaActionCode.getByActionCode(actionCode).getDescription();
        return messageText;
    }

    protected String approvedMessage(IsoMessageResponse isoMessageResponse) {
        return "Approval Code : " + IsoMessageUtils.retrieveFromDataElementsAsString(
                isoMessageResponse, DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE);
    }

    protected void notifyTransactionCompletion(boolean transactionApproved, global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest, TransactionRequest request, IsoMessageResponse isoMessageResponse, ReadCardResponse readCardResponse) {
        this.updateTransactionLog(
                transactionApproved,
                transactionRequest,
                request.getTransactionRepository(),
                isoMessageResponse,
                readCardResponse
        );
        this.updateTransactionIdsAndNotifyTransactionStatus(
                transactionApproved,
                isoMessageResponse,
                request,
                readCardResponse
        );
    }

    protected IsoMessageResponse sendIsoMessage() {
        RequestContext context = this.prepareTemplateRequestContext(stan);
        this.logger.debug(String.format(" Request Context ::%s", context));
        TransactionIsoRequest transactionIsoRequest = prepareIsoRequest(readCardResponse, transactionRequest);
        context.setRequest(transactionIsoRequest);
        NepsMessageSenderTemplate sender = getRequestSender(readCardResponse, context);
        IsoMessageResponse isoMessageResponse = sender.send();
        return isoMessageResponse;
    }

    protected VoidSaleIsoRequest prepareReversalIsoRequest() {
        return VoidSaleIsoRequest.Builder
                .newInstance()
                .stan(stan)
                .transactionAmount(transactionRequest.getAmount())
                .localDateTime(DateUtils.yyMMddHHmmssDate())
                .processingCode(getProcessingCode())
                .track2Data(readCardResponse.getCardDetails().getTrackTwoData())
                .emvData(readCardResponse.getCardDetails().getIccDataBlock() == null ? "" : readCardResponse.getCardDetails().getIccDataBlock())
                .pan(readCardResponse.getCardDetails().getPrimaryAccountNumber())
                .currencyCode(transactionRequest.getEmvParameterRequest().getTransactionCurrencyCode())
                .posEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
                .posConditionCode(PosConditionCode.ATTENDANT_TERMINAL.getValue())
                .build();
    }


    public boolean shouldSkipPinBlockFromCard(ReadCardResponse readCardResponse) {
        return readCardResponse.getCardDetails().getPinBlock().getOfflinePin() ||
                StringUtils.isEmpty(readCardResponse.getCardDetails().getPinBlock().getPin()) ||
                readCardResponse.getCardDetails().getPinBlock().getPin().equalsIgnoreCase("offline");
    }


    protected PurchaseRequest preparePurchaseRequest(global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest, CardDetails cardDetails) {
        PurchaseRequest purchaseRequest = new PurchaseRequest(
                transactionRequest.getTransactionType(),
                transactionRequest.getAmount(),
                transactionRequest.getAdditionalAmount()
        );
        cardDetails.setAmount(transactionRequest.getAmount());
        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
        purchaseRequest.setCardDetails(cardDetails);
        return purchaseRequest;
    }


    protected void updateTransactionIds(boolean purchaseApproved) {
        this.terminalRepository.incrementSystemTraceAuditNumber();
        this.terminalRepository.incrementRetrievalReferenceNumber();
        if (purchaseApproved)
            this.terminalRepository.incrementInvoiceNumber();
    }

    protected PosEntryMode getPosEntryMode(CardType cardType) {
        if (cardType == null) {
            return PosEntryMode.UNSPECIFIED;
        }
        switch (cardType) {
            case ICC:
                return PosEntryMode.ICC;
            case PICC:
                return PosEntryMode.PICC;
            case MAG:
                return PosEntryMode.MAGNETIC_STRIPE;
            case MANUAL:
                return PosEntryMode.MANUAL;
            case UNKNOWN:
            default:
                return PosEntryMode.UNSPECIFIED;
        }
    }

    protected ReceiptLog getReceiptLog(
            global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest,
            ReadCardResponse readCardResponse,
            IsoMessageResponse isoMessageResponse,
            ApplicationRepository applicationRepository,
            String batchNumber,
            String stan,
            String invoiceNumber
    ) {
        return new TransactionReceiptHandler(
                transactionRequest,
                readCardResponse,
                isoMessageResponse,
                terminalRepository,
                applicationRepository
        ).prepare(batchNumber, stan, invoiceNumber);
    }

}
