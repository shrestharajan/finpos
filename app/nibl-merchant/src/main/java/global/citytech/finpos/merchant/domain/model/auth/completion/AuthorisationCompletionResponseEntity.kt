package global.citytech.finpos.merchant.domain.model.auth.completion


/**
 * Created by Saurav Ghimire on 2/4/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
data class AuthorisationCompletionResponseEntity(
    val stan: String? = null,
    val message: String? = null,
    val isApproved: Boolean? = false,
    val shouldPrintCustomerCopy: Boolean? = false

)
