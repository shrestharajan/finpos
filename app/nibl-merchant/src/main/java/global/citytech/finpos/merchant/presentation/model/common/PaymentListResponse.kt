package global.citytech.finpos.merchant.presentation.model.common


data class PaymentListResponse(
    val limit: Int,
    val offset: Int,
    val totalRecord: Long,
    val data: List<Any>
) {
}
