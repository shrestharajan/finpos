package global.citytech.finposframework.switches.batchupload;

import global.citytech.finposframework.switches.RequestParameter;

/**
 * Created by Unique Shakya on 5/11/2021.
 */
public interface BatchUploadRequestParameter extends RequestParameter {
}
