package global.citytech.finpos.processor.neps.greenpin;

import global.citytech.finpos.processor.neps.transaction.TransactionRequest;
import global.citytech.finposframework.hardware.io.cards.read.ReadCardService;
import global.citytech.finposframework.hardware.io.led.LedService;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.hardware.io.sound.SoundService;
import global.citytech.finposframework.repositories.ApplicationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.usecases.controllers.DeviceController;
import global.citytech.finposframework.usecases.transaction.TransactionAuthenticator;

public class GreenPinRequest extends TransactionRequest {
    public static  class Builder {
        private TransactionRepository transactionRepository;
        private ReadCardService readCardService;
        private DeviceController deviceController;
        private TransactionAuthenticator transactionAuthenticator;
        private PrinterService printerService;
        private ApplicationRepository applicationRepository;
        private global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest;
        private LedService ledService;
        private SoundService soundService;


        public static GreenPinRequest.Builder newInstance() {
            return new GreenPinRequest.Builder();
        }

        public GreenPinRequest.Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public GreenPinRequest.Builder withReadCardService(ReadCardService readCardService) {
            this.readCardService = readCardService;
            return this;
        }

        public GreenPinRequest.Builder withDeviceController(DeviceController deviceController) {
            this.deviceController = deviceController;
            return this;
        }

        public GreenPinRequest.Builder withTransactionAuthenticator(TransactionAuthenticator transactionAuthenticator) {
            this.transactionAuthenticator = transactionAuthenticator;
            return this;
        }

        public GreenPinRequest.Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public GreenPinRequest.Builder withApplicationRepository(ApplicationRepository applicationRepository) {
            this.applicationRepository = applicationRepository;
            return this;
        }

        public GreenPinRequest.Builder withTransactionRequest(global.citytech.finposframework.usecases.transaction.data.TransactionRequest transactionRequest) {
            this.transactionRequest = transactionRequest;
            return this;
        }

        public GreenPinRequest.Builder withLedService(LedService ledService) {
            this.ledService = ledService;
            return this;
        }

        public GreenPinRequest.Builder withSoundService(SoundService soundService) {
            this.soundService = soundService;
            return this;
        }

        public GreenPinRequest build() {
            GreenPinRequest GreenPinRequest = new GreenPinRequest();
            GreenPinRequest.applicationRepository = this.applicationRepository;
            GreenPinRequest.transactionRequest = this.transactionRequest;
            GreenPinRequest.transactionRepository = this.transactionRepository;
            GreenPinRequest.readCardService = this.readCardService;
            GreenPinRequest.transactionAuthenticator = this.transactionAuthenticator;
            GreenPinRequest.deviceController = this.deviceController;
            GreenPinRequest.printerService = this.printerService;
            GreenPinRequest.ledService = this.ledService;
            GreenPinRequest.soundService = this.soundService;
            return GreenPinRequest;
        }
    }

}
