package global.citytech.finposframework.switches.tipadjustment;

import global.citytech.finposframework.switches.ResponseParameter;

/**
 * Created by Unique Shakya on 5/4/2021.
 */
public interface TipAdjustmentResponseParameter extends ResponseParameter {
}
