package global.citytech.finposframework.repositories;

public class EmvKeyRetrieveResponse {
    private String data;

    public EmvKeyRetrieveResponse(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }
}
