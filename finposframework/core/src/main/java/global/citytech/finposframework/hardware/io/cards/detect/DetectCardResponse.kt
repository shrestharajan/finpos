package global.citytech.finposframework.hardware.io.cards.detect

import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.cards.CardType

/**
 * Created by Rishav Chudal on 4/7/21.
 */
open class DetectCardResponse constructor(
    val result: Result,
    val message: String,
    val cardDetected: CardType
)