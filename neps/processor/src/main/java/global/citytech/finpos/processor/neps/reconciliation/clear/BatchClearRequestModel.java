package global.citytech.finpos.processor.neps.reconciliation.clear;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.ReconciliationRepository;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reconciliation.clear.BatchClearRequestParameter;

public class BatchClearRequestModel implements BatchClearRequestParameter, UseCase.Request {

    private final ReconciliationRepository reconciliationRepository;
    private final PrinterService printerService;
    private final TransactionRepository transactionRepository;

    public BatchClearRequestModel(ReconciliationRepository reconciliationRepository, PrinterService printerService, TransactionRepository transactionRepository) {
        this.reconciliationRepository = reconciliationRepository;
        this.printerService = printerService;
        this.transactionRepository = transactionRepository;
    }

    public ReconciliationRepository getReconciliationRepository() {
        return reconciliationRepository;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }
}
