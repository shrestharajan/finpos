package global.citytech.finposframework.hardware.io.cards

import global.citytech.finposframework.usecases.CardSchemeType
import java.math.BigDecimal


/**
 * Created by Rishav Chudal on 6/8/20.
 */
class CardDetails {
    var cardType: CardType? = null
    var tagCollection: EMVTagCollection? = null
    var aid: String? = null
    var tag5F28: String? = null
    var tag9F42: String? = null
    var primaryAccountNumber: String? = null
    var expiryDate: String? = null
    var trackTwoData: String? = null
    var cardHolderName: String? = null
    var primaryAccountNumberSerialNumber: String? = null
    var cardScheme: CardSchemeType? = null
    var cardSchemeLabel: String? = null
    var pinBlock: PinBlock? =
        PinBlock(
            "Offline",
            true
        )
    var iccDataBlock: String? = null
    var amount: BigDecimal? = null
    var cashBackAmount: BigDecimal? = null
    var transactionAcceptedByCard: Boolean? = null
    var transactionState: TransactionState? = null
    var transactionInitializeDateTime: String? = null
    var cvv: String? = null


    override fun toString(): String {
        return "CardDetails(slotType=$cardType, emvTagCollection=$tagCollection, aid=$aid, tag5F28=$tag5F28, tag9F42=$tag9F42, pan=$primaryAccountNumber, expiryDate=$expiryDate, trackTwoData=$trackTwoData, cardHolderName=$cardHolderName, panSerialNumber=$primaryAccountNumberSerialNumber, cardScheme=$cardScheme, cardSchemeLabel = $cardSchemeLabel, pinBlock=$pinBlock, iccData=$iccDataBlock, amount=$amount, additionalAmount=$cashBackAmount, transactionAcceptedByCard=$transactionAcceptedByCard, transactionState=$transactionState, transactionInitDateTime=$transactionInitializeDateTime)"
    }


}