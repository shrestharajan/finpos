package global.citytech.finposframework.iso8583;

import org.junit.Test;

import global.citytech.finposframework.log.Logger;;

import static org.junit.Assert.assertEquals;

/** User: Surajchhetry Date: 2/19/20 Time: 1:25 PM */
public class Iso8583MsgTest {

  private Logger logger = Logger.getLogger(Iso8583MsgTest.class.getName());

  @Test
  public void testMTIGeneration() {
    MTI mti =
        new MTI(
            MTI.Version.Version_1987,
            MTI.MessageClass.Network_Management,
            MTI.MessageFunction.Request,
            MTI.Originator.Acquirer);
    assertEquals("0800", mti.toString());
  }

  @Test
  public void testLogonMessageGenerate() {
    MTI mti =
        new MTI(
            MTI.Version.Version_1987,
            MTI.MessageClass.Network_Management,
            MTI.MessageFunction.Request,
            MTI.Originator.Acquirer);
    Iso8583Msg msg = new Iso8583Msg(mti);
    msg.addField(new DataElement(3,  "990000"));
    msg.addField(new DataElement(7, "1111111111"));
    msg.addField(new DataElement(11, "000001"));
    msg.addField(new DataElement(24, "811"));
    msg.addField(new DataElement(41, "41234027"));
    msg.addField(new DataElement(42, "100512345678910"));
    BitMap bitMap = msg.getBitMap();
    assertEquals("2220010000C00000", bitMap.asHexValue());
  }


}
