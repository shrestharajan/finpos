package global.citytech.finpos.merchant.domain.model.receipt;

import global.citytech.finposframework.hardware.common.Result;
import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.TransactionRepository;

/**
 * Created by Saurav Ghimire on 1/26/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public class CustomerCopyResponseEntity {
    private final Result result;
    private final String message;

    public CustomerCopyResponseEntity(Result result, String message) {
        this.result = result;
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }
}
