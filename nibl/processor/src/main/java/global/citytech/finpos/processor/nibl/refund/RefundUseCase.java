//package global.citytech.finpos.processor.nibl.refund;
//
//import java.util.concurrent.CountDownLatch;
//
//import global.citytech.finpos.nibl.iso8583.NIBLSpecInfo;
//import global.citytech.finpos.nibl.iso8583.ProcessingCode;
//import global.citytech.finpos.nibl.iso8583.requestsender.refund.IccRefundRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.refund.MagRefundRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.refund.PICCRefundRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.refund.RefundRequest;
//import global.citytech.finpos.nibl.iso8583.requestsender.refund.RefundRequestSender;
//import global.citytech.finpos.nibl.iso8583.requestsender.reversal.NiblReversalRequest;
//import global.citytech.finpos.processor.nibl.reversal.ReversalRequestModel;
//import global.citytech.finpos.processor.nibl.reversal.ReversalUseCase;
//import global.citytech.finpos.processor.nibl.transaction.NiblTransactionReceiptPrintHandler;
//import global.citytech.finpos.processor.nibl.transaction.ReasonForReversal;
//import global.citytech.finpos.processor.nibl.transaction.ReversedTransactionReceiptHandler;
//import global.citytech.finpos.processor.nibl.transaction.TransactionUseCase;
//import global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor;
//import global.citytech.finpos.processor.nibl.transaction.card.MagneticCardProcessor;
//import global.citytech.finpos.processor.nibl.transaction.card.PICCCardProcessor;
//import global.citytech.finposframework.exceptions.FinPosException;
//import global.citytech.finposframework.exceptions.PosError;
//import global.citytech.finposframework.exceptions.PosException;
//import global.citytech.finposframework.hardware.io.cards.CardDetails;
//import global.citytech.finposframework.hardware.io.cards.CardType;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardRequest;
//import global.citytech.finposframework.hardware.io.cards.read.ReadCardResponse;
//import global.citytech.finposframework.hardware.io.led.LedAction;
//import global.citytech.finposframework.hardware.io.led.LedLight;
//import global.citytech.finposframework.hardware.io.led.LedRequest;
//import global.citytech.finposframework.hardware.io.led.LedService;
//import global.citytech.finposframework.iso8583.DataElement;
//import global.citytech.finposframework.iso8583.IsoMessageResponse;
//import global.citytech.finposframework.iso8583.RequestContext;
//import global.citytech.finposframework.listeners.CardConfirmationListener;
//import global.citytech.finposframework.notifier.Notifier;
//import global.citytech.finposframework.repositories.ApplicationRepository;
//import global.citytech.finposframework.repositories.TerminalRepository;
//import global.citytech.finposframework.repositories.TransactionRepository;
//import global.citytech.finposframework.supports.UseCase;
//import global.citytech.finposframework.switches.refund.RefundRequester;
//import global.citytech.finposframework.usecases.MagneticSwipe;
//import global.citytech.finposframework.usecases.PICCWave;
//import global.citytech.finposframework.usecases.TransactionType;
//import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;
//import global.citytech.finposframework.usecases.transaction.TransactionLog;
//import global.citytech.finposframework.usecases.transaction.data.AutoReversal;
//import global.citytech.finposframework.usecases.transaction.data.PurchaseRequest;
//import global.citytech.finposframework.usecases.transaction.data.TransactionRequest;
//import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion;
//import global.citytech.finposframework.usecases.transaction.receipt.transaction.ReceiptLog;
//import global.citytech.finposframework.utility.HelperUtils;
//import global.citytech.finposframework.utility.IsoMessageUtils;
//import global.citytech.finposframework.utility.JsonUtils;
//import global.citytech.finposframework.utility.StringUtils;
//
///**
// * Created by Unique Shakya on 9/28/2020.
// */
//public class RefundUseCase extends TransactionUseCase implements UseCase<RefundRequestModel, RefundResponseModel>,
//        RefundRequester<RefundRequestModel, RefundResponseModel> {
//
//    static CountDownLatch countDownLatch;
//    RefundRequestModel request;
//    ReadCardResponse readCardResponse;
//    String stan;
//    String invoiceNumber;
//    TransactionRequest transactionRequest;
//    String batchNumber;
//    private global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest;
//    private RefundRequest isoRefundRequest;
//
//    public RefundUseCase(TerminalRepository terminalRepository, Notifier notifier) {
//        super(terminalRepository, notifier);
//    }
//
//    @Override
//    protected String getClassName() {
//        return RefundUseCase.class.getName();
//    }
//
//    @Override
//    public RefundResponseModel execute(RefundRequestModel request) {
//        try {
//            this.request = request;
//            logger.log(":: REACHED PURCHASE USE CASE ::");
//            stan = this.terminalRepository.getSystemTraceAuditNumber();
//            invoiceNumber = this.terminalRepository.getInvoiceNumber();
//            batchNumber = this.terminalRepository.getReconciliationBatchNumber();
//            this.transactionRepository = request.transactionRepository;
//            if (!this.isDeviceReady(request.deviceController, request.transactionRequest.getTransactionType()))
//                throw new PosException(PosError.DEVICE_ERROR_NOT_READY);
//            this.initTransaction(request.applicationRepository, request.transactionRequest);
//            ReadCardRequest readCardRequest = prepareReadCardRequest(stan, request.applicationRepository,
//                    request.transactionRequest);
//            this.notifier.notify(Notifier.EventType.STARTING_READ_CARD);
//            ReadCardResponse readCardResponse = this.detectCard(readCardRequest, request.readCardService,
//                    request.ledService);
//            this.notifier.notify(Notifier.EventType.READING_CARD, "Reading Card..");
//            boolean isCardConfirmationRequired;
//            switch (readCardResponse.getCardDetails().getCardType()) {
//                case MAG:
//                    isCardConfirmationRequired = true;
//                    this.cardProcessor = new MagneticCardProcessor(
//                            request.transactionRepository,
//                            request.transactionAuthenticator,
//                            request.applicationRepository,
//                            request.readCardService,
//                            this.notifier,
//                            this.terminalRepository
//                    );
//                    break;
//                case ICC:
//                    isCardConfirmationRequired = true;
//                    this.cardProcessor = new IccCardProcessor(request.readCardService,
//                            request.transactionRepository, request.applicationRepository,
//                            request.transactionAuthenticator, this.notifier,
//                            this.terminalRepository);
//                    break;
//                case PICC:
//                    isCardConfirmationRequired = false;
//                    this.cardProcessor = new PICCCardProcessor(request.transactionRepository,
//                            request.applicationRepository, request.readCardService, this.notifier,
//                            request.transactionAuthenticator, request.ledService, request.soundService,
//                            this.terminalRepository);
//                    break;
//                default:
//                    isCardConfirmationRequired = false;
//            }
//            readCardResponse = this.cardProcessor.validateReadCardResponse(readCardRequest, readCardResponse);
//            this.checkForCardTypeChange(readCardResponse.getCardDetails().getCardType(), request);
//            if (!this.isValidTransaction(request.applicationRepository, request.transactionRequest, readCardResponse))
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_ALLOWED);
//            if (!this.isTransactionAuthorizationSuccess(request.transactionAuthenticator, request.applicationRepository,
//                    request.transactionRequest.getTransactionType(), readCardResponse))
//                throw new PosException(PosError.DEVICE_ERROR_TRANSACTION_NOT_AUTHORIZED);
//            purchaseRequest =
//                    preparePurchaseCardRequest(request.transactionRequest, readCardResponse.getCardDetails(),
//                            readCardResponse.getFallbackIccToMag());
//
//            if (isCardConfirmationRequired) {
//                final boolean[] isConfirm = {false};
//                countDownLatch = new CountDownLatch(1);
//                this.notifier.notify(Notifier.EventType.CARD_CONFIRMATION_EVENT,
//                        readCardResponse.getCardDetails(),
//                        (CardConfirmationListener) confirmed -> {
//                            isConfirm[0] = confirmed;
//                            countDownLatch.countDown();
//                        });
//                countDownLatch.await();
//                if (!isConfirm[0]) {
//                    throw new PosException(PosError.DEVICE_ERROR_USER_CANCELLED);
//                }
//            }
//            return processCardAndProceed();
//
//        } catch (PosException ex) {
//            ex.printStackTrace();
//            throw ex;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//        } finally {
//            request.readCardService.cleanUp();
//            request.ledService.doTurnLedWith(new LedRequest(LedLight.ALL, LedAction.OFF));
//            PICCWave.getInstance().clear();
//            MagneticSwipe.getInstance().clear();
//        }
//    }
//
//    private RefundResponseModel processCardAndProceed() {
//        readCardResponse = this.cardProcessor.processCard(stan, purchaseRequest);
//        logger.debug("::: READ CARD RESPONSE ::: " + readCardResponse.toString());
//        this.notifier.notify(Notifier.EventType.TRANSMITTING_REQUEST, "Transmitting Request..");
//        try {
//            IsoMessageResponse purchaseIsoMessageResponse = this.sendIsoMessage(stan,
//                    request.transactionRequest.getOriginalRetrievalReferenceNumber(), invoiceNumber,
//                    readCardResponse, request);
//            this.notifier.notify(Notifier.EventType.RESPONSE_RECEIVED, "Please wait..");
//            InvalidResponseHandlerResponse invalidResponseHandlerResponse = this.checkForInvalidResponseFromHost(purchaseIsoMessageResponse);
//            if (invalidResponseHandlerResponse.isInvalid())
//                return this.handleInvalidResponseFromHost(request.transactionRequest,
//                        invalidResponseHandlerResponse, batchNumber, stan, invoiceNumber,
//                        readCardResponse, request);
//            boolean isPurchaseApproved = this.isTransactionApprovedByActionCode(purchaseIsoMessageResponse);
//            this.notifier.notify(Notifier.EventType.PROCESSING_ISSUER_SCRIPT, "Processing..");
//            readCardResponse = this.cardProcessor.processOnlineResult(readCardResponse, purchaseIsoMessageResponse);
//            if (this.isTransactionDeclineByCardEvenIfSuccessOnSwitch(purchaseIsoMessageResponse, readCardResponse))
//                return this.handleApprovedTransactionDeclinedByCard(stan, batchNumber, invoiceNumber,
//                        readCardResponse, request, request.transactionRequest);
//            if (this.isForceContactCardPromptActionCode(readCardResponse, purchaseIsoMessageResponse))
//                return this.handleForceContactCardPromptFromHost(request);
//            this.notifyTransactionCompletion(stan, batchNumber, invoiceNumber, isPurchaseApproved,
//                    request.transactionRequest, request.transactionRepository, purchaseIsoMessageResponse,
//                    readCardResponse, request.applicationRepository, request.ledService);
//            boolean shouldPrint = this.printReceipt(batchNumber, stan, invoiceNumber, request.printerService, request.transactionRequest,
//                    request.transactionRepository, readCardResponse, purchaseIsoMessageResponse, request.applicationRepository);
//            return this.prepareResponse(stan, purchaseIsoMessageResponse, isPurchaseApproved, request.applicationRepository, shouldPrint);
//        } catch (FinPosException e) {
//            e.printStackTrace();
//            if (this.isResponseNotReceivedException(e)) {
//                return this.handleTimeoutTransaction(request.transactionRequest, batchNumber,
//                        stan, invoiceNumber, readCardResponse, request);
//            } else if (e.getType() == FinPosException.ExceptionType.UNABLE_TO_CONNECT) {
//                logger.log("::: AUTO REVERSAL ::: AUTO REVERSAL REMOVED UNABLE TO CONNECT");
//                transactionRepository.removeAutoReversal(this.autoReversal);
//                throw new PosException(PosError.DEVICE_ERROR_UNABLE_TO_CONNECT);
//            } else {
//                throw new PosException(PosError.DEVICE_ERROR_PROCESSING_ERROR);
//            }
//        }
//    }
//
//    private RefundResponseModel handleForceContactCardPromptFromHost(RefundRequestModel request) {
//        this.updateTransactionIds(false);
//        request.readCardService.cleanUp();
//        ForceContactCardRefundUseCase forceContactCardRefundUseCase = new ForceContactCardRefundUseCase(
//                this.terminalRepository,
//                this.notifier
//        );
//        return forceContactCardRefundUseCase.execute(request);
//    }
//
//    private RefundResponseModel handleApprovedTransactionDeclinedByCard(String stan, String batchNumber,
//                                                                        String invoiceNumber,
//                                                                        ReadCardResponse readCardResponse,
//                                                                        RefundRequestModel request,
//                                                                        TransactionRequest transactionRequest) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, ReasonForReversal.Type.DECLINED_BY_CARD.getDescription());
//        this.performReversal(stan, readCardResponse, request);
//        boolean shouldPrint = !this.approvePrintOnly(request.applicationRepository);
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest, readCardResponse,
//                    this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.DECLINED_BY_CARD,
//                            ReasonForReversal.Type.DECLINED_BY_CARD.getDescription()));
//            new NiblTransactionReceiptPrintHandler(request.printerService)
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            request.transactionRepository.updateReceiptLog(receiptLog);
//        }
//        return RefundResponseModel.Builder.createDefaultBuilder()
//                .isApproved(false)
//                .message(ReasonForReversal.Type.DECLINED_BY_CARD.getDescription())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private void checkForCardTypeChange(CardType cardType, RefundRequestModel request) {
//        switch (cardType) {
//            case MAG:
//                this.cardProcessor = new global.citytech.finpos.processor.nibl.transaction.card.MagneticCardProcessor(
//                        request.transactionRepository,
//                        request.transactionAuthenticator,
//                        request.applicationRepository,
//                        request.readCardService,
//                        this.notifier,
//                        this.terminalRepository
//                );
//                break;
//            case ICC:
//                this.cardProcessor = new global.citytech.finpos.processor.nibl.transaction.card.IccCardProcessor(request.readCardService,
//                        request.transactionRepository, request.applicationRepository,
//                        request.transactionAuthenticator, this.notifier,
//                        this.terminalRepository);
//                break;
//            case PICC:
//                this.cardProcessor = new PICCCardProcessor(request.transactionRepository,
//                        request.applicationRepository, request.readCardService, this.notifier,
//                        request.transactionAuthenticator, request.ledService, request.soundService,
//                        this.terminalRepository);
//                break;
//        }
//    }
//
//    private RefundResponseModel handleTimeoutTransaction(TransactionRequest transactionRequest,
//                                                         String batchNumber, String stan, String invoiceNumber,
//                                                         ReadCardResponse readCardResponse, RefundRequestModel request) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, "TIMEOUT");
//        this.setUnableToGoOnlineResultToCard(readCardResponse, request.readCardService);
//        this.performReversal(stan, readCardResponse, request);
//        boolean shouldPrint = !this.approvePrintOnly(request.applicationRepository);
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest,
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.TIMEOUT, "Timeout"));
//            new NiblTransactionReceiptPrintHandler(request.printerService)
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            request.transactionRepository.updateReceiptLog(receiptLog);
//        }
//        return RefundResponseModel.Builder.createDefaultBuilder()
//                .message("TIMEOUT")
//                .isApproved(false)
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private RefundResponseModel handleInvalidResponseFromHost(TransactionRequest transactionRequest,
//                                                              InvalidResponseHandlerResponse invalidResponseHandlerResponse,
//                                                              String batchNumber, String stan, String invoiceNumber,
//                                                              ReadCardResponse readCardResponse, RefundRequestModel request) {
//        this.notifier.notify(Notifier.EventType.TRANSACTION_DECLINED, invalidResponseHandlerResponse.getMessage());
//        this.performReversal(stan, readCardResponse, request);
//        boolean shouldPrint = !this.approvePrintOnly(request.applicationRepository);
//        if (shouldPrint) {
//            ReceiptLog receiptLog = new ReversedTransactionReceiptHandler(transactionRequest,
//                    readCardResponse, this.terminalRepository)
//                    .prepare(batchNumber, stan, invoiceNumber, new ReasonForReversal(ReasonForReversal.Type.INVALID_RESPONSE,
//                            invalidResponseHandlerResponse.getMessage()));
//            new NiblTransactionReceiptPrintHandler(request.printerService)
//                    .printTransactionReceipt(receiptLog, ReceiptVersion.MERCHANT_COPY);
//            request.transactionRepository.updateReceiptLog(receiptLog);
//        }
//        return RefundResponseModel.Builder.createDefaultBuilder()
//                .isApproved(false)
//                .message(invalidResponseHandlerResponse.getMessage())
//                .shouldPrintCustomerCopy(shouldPrint)
//                .build();
//    }
//
//    private IsoMessageResponse sendIsoMessage(String stan, String originalRetrievalReferenceNumber,
//                                              String invoiceNumber, ReadCardResponse readCardResponse, RefundRequestModel request) {
//        RequestContext requestContext = this.prepareTemplateRequestContext(stan);
//        requestContext.setRequest(this.prepareRefundRequest(originalRetrievalReferenceNumber, invoiceNumber, readCardResponse));
//        RefundRequestSender refundRequestSender;
//        switch (readCardResponse.getCardDetails().getCardType()) {
//            case MAG:
//                refundRequestSender = new MagRefundRequestSender(new NIBLSpecInfo(), requestContext);
//                break;
//            case ICC:
//                refundRequestSender = new IccRefundRequestSender(new NIBLSpecInfo(), requestContext);
//                break;
//            case PICC:
//                refundRequestSender = new PICCRefundRequestSender(new NIBLSpecInfo(), requestContext);
//                break;
//            default:
//                throw new PosException(PosError.DEVICE_ERROR_NO_SUCH_CARD_TYPE);
//        }
//        this.autoReversal = new AutoReversal(
//                TransactionType.REFUND,
//                JsonUtils.toJsonObj(prepareReversalRequest(stan, readCardResponse, request)),
//                0,
//                AutoReversal.Status.ACTIVE,
//                StringUtils.dateTimeStamp()
//        );
//        this.transactionRepository.saveAutoReversal(this.autoReversal);
//        IsoMessageResponse isoMessageResponse = refundRequestSender.send();
//        this.transactionRepository.removeAutoReversal(this.autoReversal);
//        return isoMessageResponse;
//    }
//
//    private RefundRequest prepareRefundRequest(String originalRetrievalReferenceNumber, String invoiceNumber,
//                                               ReadCardResponse readCardResponse) {
//        this.isoRefundRequest = new RefundRequest();
//        this.isoRefundRequest.setPan(readCardResponse.getCardDetails().getPrimaryAccountNumber());
//        this.isoRefundRequest.setTransactionAmount(readCardResponse.getCardDetails().getAmount());
//        this.isoRefundRequest.setPosConditionCode("00"); //TODO Change later
//        this.isoRefundRequest.setEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()));
//        this.isoRefundRequest.setTrack2Data(readCardResponse.getCardDetails().getTrackTwoData());
//        if (readCardResponse.getCardDetails().getPinBlock().getOfflinePin() ||
//                StringUtils.isEmpty(readCardResponse.getCardDetails().getPinBlock().getPin()) ||
//                readCardResponse.getCardDetails().getPinBlock().getPin().equalsIgnoreCase("offline"))
//            isoRefundRequest.setPinBlock("");
//        else
//            isoRefundRequest.setPinBlock(readCardResponse.getCardDetails().getPinBlock().getPin());
//        this.isoRefundRequest.setEmvData(readCardResponse.getCardDetails().getIccDataBlock());
//        this.isoRefundRequest.setExpireDate(readCardResponse.getCardDetails().getExpiryDate());
//        this.isoRefundRequest.setCardSequenceNumber(readCardResponse.getCardDetails().getPrimaryAccountNumberSerialNumber());
//        this.isoRefundRequest.setLocalTime(HelperUtils.getDefaultLocaleTimeHhMmSs());
//        this.isoRefundRequest.setLocalDate(HelperUtils.getDefaultLocaleDateWithoutYear());
//        this.isoRefundRequest.setOriginalRetrievalReferenceNumber(originalRetrievalReferenceNumber);
//        this.isoRefundRequest.setInvoiceNumber(invoiceNumber);
//        this.isoRefundRequest.setFallbackIccToMag(readCardResponse.getFallbackIccToMag());
//        return this.isoRefundRequest;
//    }
//
//    private PurchaseRequest preparePurchaseCardRequest(TransactionRequest transactionRequest, CardDetails cardDetails,
//                                                       boolean fallbackIccToMag) {
//        global.citytech.finposframework.usecases.transaction.data.PurchaseRequest purchaseRequest
//                = new global.citytech.finposframework.usecases.transaction.data.PurchaseRequest(
//                transactionRequest.getTransactionType(),
//                transactionRequest.getAmount(),
//                transactionRequest.getAdditionalAmount()
//        );
//        cardDetails.setAmount(transactionRequest.getAmount());
//        cardDetails.setCashBackAmount(transactionRequest.getAdditionalAmount());
//        purchaseRequest.setCardDetails(cardDetails);
//        purchaseRequest.setFallbackIccToMag(fallbackIccToMag);
//        return purchaseRequest;
//    }
//
//    private RefundResponseModel prepareResponse(String stan, IsoMessageResponse response, boolean isPurchaseApproved,
//                                                ApplicationRepository applicationRepository,
//                                                boolean shouldPrint) {
//        RefundResponseModel.Builder builder = RefundResponseModel.Builder.createDefaultBuilder();
//        builder.isApproved(isPurchaseApproved);
//        builder.stan(stan);
//        builder.debugRequestMessage(response.getDebugRequestString());
//        builder.debugResponseMessage(response.getDebugResponseString());
//        builder.message(this.retrieveMessage(response, isPurchaseApproved, applicationRepository));
//        builder.shouldPrintCustomerCopy(shouldPrint);
//        return builder.build();
//    }
//
//    private void performReversal(
//            String stan,
//            ReadCardResponse readCardResponse,
//            RefundRequestModel refundRequestModel
//    ) {
//        ReversalUseCase reversalUseCase = new ReversalUseCase(
//                this.prepareReversalRequest(stan, readCardResponse, refundRequestModel),
//                this.terminalRepository,
//                this.notifier
//        );
//        try {
//            reversalUseCase.execute(
//                    prepareReversalRequestModel(
//                            refundRequestModel,
//                            readCardResponse
//                    )
//            );
//            this.transactionRepository.removeAutoReversal(this.autoReversal);
//        } catch (FinPosException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private NiblReversalRequest prepareReversalRequest(
//            String stan,
//            ReadCardResponse readCardResponse,
//            RefundRequestModel refundRequestModel
//    ) {
//        NiblReversalRequest niblReversalRequest = new NiblReversalRequest();
//        niblReversalRequest.setProcessingCode(ProcessingCode.REFUND.getCode());
//        niblReversalRequest.setAmount(
//                refundRequestModel.transactionRequest.getAmount()
//        );
//        niblReversalRequest.setPan(this.isoRefundRequest.getPan());
//        niblReversalRequest.setExpiryDate(this.isoRefundRequest.getExpireDate());
//        niblReversalRequest.setPosEntryMode(this.isoRefundRequest.getEntryMode());
//        niblReversalRequest.setPosConditionCode(this.isoRefundRequest.getPosConditionCode());
//        niblReversalRequest.setTrack2Data(this.isoRefundRequest.getTrack2Data());
//        niblReversalRequest.setPinBlock(this.isoRefundRequest.getPinBlock());
//        niblReversalRequest.setInvoiceNumber(this.isoRefundRequest.getInvoiceNumber());
//        niblReversalRequest.setStan(stan);
//        return niblReversalRequest;
//    }
//
//    private ReversalRequestModel prepareReversalRequestModel(
//            RefundRequestModel refundRequestModel,
//            ReadCardResponse readCardResponse
//    ) {
//        return ReversalRequestModel.Builder.newInstance()
//                .withTransactionRequest(
//                        refundRequestModel.transactionRequest
//                )
//                .withTransactionRepository(
//                        refundRequestModel.transactionRepository
//                )
//                .withPrinterService(
//                        refundRequestModel.printerService
//                )
//                .withApplicationRepository(
//                        refundRequestModel.applicationRepository
//                )
//                .withReadCardResponse(readCardResponse)
//                .build();
//    }
//
//    private void notifyTransactionCompletion(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean purchaseApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository,
//            LedService ledService) {
//        this.updateTransactionLog(
//                stan,
//                batchNumber,
//                invoiceNumber,
//                purchaseApproved,
//                transactionRequest,
//                transactionRepository,
//                isoMessageResponse,
//                readCardResponse,
//                applicationRepository
//        );
//        this.updateTransactionIds(purchaseApproved);
//        this.notifyTransactionStatus(
//                purchaseApproved,
//                isoMessageResponse,
//                applicationRepository,
//                ledService,
//                readCardResponse.getCardDetails().getCardType() == CardType.PICC
//        );
//    }
//
//    private void updateTransactionLog(
//            String stan,
//            String batchNumber,
//            String invoiceNumber,
//            boolean purchaseApproved,
//            TransactionRequest transactionRequest,
//            TransactionRepository transactionRepository,
//            IsoMessageResponse isoMessageResponse,
//            ReadCardResponse readCardResponse,
//            ApplicationRepository applicationRepository
//    ) {
//        this.logger.log("::: NIBL ::: BATCH NUMBER UPDATE TRANSACTION LOG ::: " + batchNumber);
//        this.logger.log("::: NIBL ::: INVOICE NUMBER UPDATE TRANSACTION LOG ::: " + invoiceNumber);
//        TransactionLog transactionLog = TransactionLog.Builder.newInstance()
//                .withStan(stan)
//                .withTerminalId(terminalRepository.findTerminalInfo().getTerminalID())
//                .withMerchantId(terminalRepository.findTerminalInfo().getMerchantID())
//                .withInvoiceNumber(invoiceNumber)
//                .withRrn(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.RETRIEVAL_REFERENCE_NUMBER))
//                .withAuthCode(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE))
//                .withTransactionType(TransactionType.REFUND)
//                .withOriginalTransactionType(TransactionType.REFUND)
//                .withTransactionAmount(transactionRequest.getAmount())
//                .withOriginalTransactionAmount(transactionRequest.getAmount())
//                .withTransactionDate(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_DATE))
//                .withTransactionTime(IsoMessageUtils.retrieveFromDataElementsAsString(
//                        isoMessageResponse,
//                        DataElement.LOCAL_TIME))
//                .withTransactionStatus(purchaseApproved ? "APPROVED" : "DECLINED")
//                .withPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
//                .withOriginalPosEntryMode(getPosEntryMode(readCardResponse.getCardDetails().getCardType()))
//                .withPosConditionCode(this.isoRefundRequest.getPosConditionCode())
//                .withOriginalPosConditionCode("")
//                .withReconcileStatus("")
//                .withReconcileTime("")
//                .withReconcileDate("")
//                .withReconcileBatchNo(batchNumber)
//                .withReadCardResponse(readCardResponse)
//                .withOriginalTransactionReferenceNumber(isoRefundRequest.getOriginalRetrievalReferenceNumber())
//                .withReceiptLog(getReceiptLog(transactionRequest, readCardResponse, isoMessageResponse,
//                        applicationRepository, batchNumber, stan, invoiceNumber))
//                .withAuthorizationCompleted(true)
//                .withResponseCode(IsoMessageUtils.retrieveFromDataElementsAsString(isoMessageResponse, DataElement.RESPONSE_CODE))
//                .withTransactionVoided(false)
//                .withTipAdjusted(false)
//                .build();
//        transactionRepository.updateTransactionLog(transactionLog);
//    }
//}
