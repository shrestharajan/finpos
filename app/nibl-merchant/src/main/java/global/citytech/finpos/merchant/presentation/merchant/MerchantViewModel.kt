package global.citytech.finpos.merchant.presentation.merchant

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.receipt.DetailReportRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.receipt.DuplicateReceiptRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.receipt.DuplicateReconciliationReceiptRepositoryImpl
import global.citytech.finpos.merchant.data.repository.core.receipt.SummaryReportRepositoryImpl
import global.citytech.finpos.merchant.domain.model.receipt.detailreport.DetailReportResponseEntity
import global.citytech.finpos.merchant.domain.model.receipt.summaryreport.SummaryReportResponseEntity
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.core.receipt.CoreDetailReportUseCase
import global.citytech.finpos.merchant.domain.usecase.core.receipt.CoreDuplicateReceiptUseCase
import global.citytech.finpos.merchant.domain.usecase.core.receipt.CoreDuplicateReconciliationReceiptUseCase
import global.citytech.finpos.merchant.domain.usecase.core.receipt.CoreSummaryReportUseCase
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.core.receipt.DetailReportDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.receipt.DuplicateReceiptDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.receipt.DuplicateReconciliationReceiptDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.receipt.SummaryReportDataSourceImpl
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finposframework.hardware.common.Result

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Unique Shakya on 8/12/2020.
 */
class MerchantViewModel(val context: Application) : BaseAndroidViewModel(context) {

    private val TAG = MerchantViewModel::class.java.name
    val logonResponse by lazy { MutableLiveData<Boolean>() }
    val exchangeKeysResponse by lazy { MutableLiveData<Boolean>() }
    val successMessage by lazy { MutableLiveData<String>() }
    val failureMessage by lazy { MutableLiveData<String>() }
    val loadingMessage by lazy { MutableLiveData<String>() }
    val configurationItem by lazy { MutableLiveData<ConfigurationItem>() }

    var localDataUseCase: LocalDataUseCase =
        LocalDataUseCase(
            LocalRepositoryImpl(
                LocalDatabaseSourceImpl(),
                PreferenceManager
            )
        )


    var duplicateReceiptUseCase = CoreDuplicateReceiptUseCase(
        DuplicateReceiptRepositoryImpl(
            DuplicateReceiptDataSourceImpl()
        )
    )

    var duplicateReconciliationReceiptUseCase = CoreDuplicateReconciliationReceiptUseCase(
        DuplicateReconciliationReceiptRepositoryImpl(
            DuplicateReconciliationReceiptDataSourceImpl()
        )
    )

    var detailReportUseCase = CoreDetailReportUseCase(
        DetailReportRepositoryImpl(
            DetailReportDataSourceImpl()
        )
    )

    var summaryReportUseCase = CoreSummaryReportUseCase(
        SummaryReportRepositoryImpl(
            SummaryReportDataSourceImpl()
        )
    )

    fun getConfigurationItem() {
        compositeDisposable.add(localDataUseCase.getWholeConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.mapToPresentation() }
            .subscribe({ configurationItem.value = it }, {
                printLog(TAG, it.message.toString())
                message.value = it.message
            })
        )
    }


    fun duplicateReceipt() {
        printLog(TAG, "Printing Duplicate Receipt")
        loadingMessage.value = "Printing Duplicate Receipt"
        compositeDisposable.add(
            duplicateReceiptUseCase.print()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    loadingMessage.value = ""
                    if (it.result == Result.SUCCESS)
                        successMessage.value = it.message
                    else if (it.result != Result.CANCEL)
                        failureMessage.value = it.message
                }, {
                    printLog(TAG, it.message.toString())
                    it.printStackTrace()
                    loadingMessage.value = ""
                    failureMessage.value = it.message
                })
        )
    }

    fun duplicateReconciliationReceipt() {
        printLog(TAG, "Printing Duplicate Settlement Receipt")
        loadingMessage.value = "Printing Duplicate Settlement Receipt"
        compositeDisposable.add(
            duplicateReconciliationReceiptUseCase.printReceipt()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    loadingMessage.value = ""
                    if (it.result == Result.SUCCESS)
                        successMessage.value = it.message
                    else if (it.result != Result.CANCEL)
                        failureMessage.value = it.message
                }, {
                    printLog(TAG, it.message.toString())
                    it.printStackTrace()
                    loadingMessage.value = ""
                    failureMessage.value = it.message
                })
        )
    }

    fun detailReport(configurationItem: ConfigurationItem) {
        loadingMessage.value = "Printing Detail Report"
        compositeDisposable.add(
            detailReportUseCase.printDetailReport(configurationItem)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onDetailResponseEntityReceived(it)
                }, {
                    onErrorInDetailReportPrint(it)
                })
        )
    }

    private fun onDetailResponseEntityReceived(it: DetailReportResponseEntity) {
        isLoading.value = false
        if (it.result == Result.SUCCESS || it.result == Result.CANCEL) {
            return
        }
        message.value = it.message
    }

    private fun onErrorInDetailReportPrint(it: Throwable) {
        printLog(TAG, it.message.toString())
        isLoading.value = false
        it.printStackTrace()
        failureMessage.value = it.message
    }

    fun summaryReport(configurationItem: ConfigurationItem) {
        loadingMessage.value = "Printing Summary Report"
        compositeDisposable.add(
            summaryReportUseCase.print(configurationItem)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onSummaryReportResponseEntityReceived(it)
                }, {
                    onErrorInSummaryReportPrint(it)
                })
        )
    }

    private fun onSummaryReportResponseEntityReceived(it: SummaryReportResponseEntity) {
        isLoading.value = false
        if (it.result == Result.SUCCESS || it.result == Result.CANCEL) {
            return
        }
        message.value = it.message
    }

    private fun onErrorInSummaryReportPrint(it: Throwable) {
        printLog(TAG, it.message.toString())
        isLoading.value = false
        it.printStackTrace()
        failureMessage.value = it.message
    }

    fun getSoundModeStatus(): Boolean {
        return localDataUseCase.isSoundMode(false)
    }

    fun saveSoundModePreference(isSoundModeOn: Boolean) {
        localDataUseCase.saveSoundModeStatus(isSoundModeOn)
    }

}