package global.citytech.common.extensions

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import global.citytech.easydroid.core.utils.Jsons
import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.*

fun Any.toJsonString(): String {
    return Jsons.toJsonObj(this)
}
fun <T> String.toJsonObj(obj: Class<T>): T {
    return Jsons.fromJsonToObj(this, obj)
}

fun String.isAlphaNumeric(): Boolean {
    return this.isNotEmpty() && this.matches(Regex("^[a-zA-Z0-9]*\$"))
}

fun String.toBitmap(): Bitmap {
    val decodedString: ByteArray = global.citytech.common.Base64.decode(this)
    return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
}

fun Double.rupeesToIsoAmountFormat(): Long {
    val paisaInString : String
    var paisaInLong = 0L
    try {
        val rupeesInString = this.toString()
        if (rupeesInString.isEmpty()) {
            paisaInLong = 0L
        } else {
            val index: Int = rupeesInString.indexOf(".")
            if (index >= 0) {
                val gap: Int = rupeesInString.length - index - 1
                if (gap == 0) {
                    paisaInString = rupeesInString.plus("00")
                } else if (gap == 1) {
                    paisaInString = rupeesInString.replace(".", "") + "0"
                } else if (gap == 2) {
                    paisaInString = rupeesInString.replace(".", "")
                } else {
                    paisaInString = rupeesInString
                        .substring(0, index + 3)
                        .replace(".", "")
                }
            } else {
                paisaInString = rupeesInString.plus("00")
            }
            paisaInLong = paisaInString.toLong()
            val isoAmount = String.format(Locale.US, "%012d", paisaInLong)
            paisaInLong = isoAmount.toLong()
        }
    } catch (exception: Exception) {
        exception.printStackTrace()
    }
    return paisaInLong
}

fun BigDecimal.rupeesToIsoAmountFormat(): Long {
    val paisaInString : String
    var paisaInLong = 0L
    try {
        val rupeesInString = this.toString()
        if (rupeesInString.isEmpty()) {
            paisaInLong = 0L
        } else {
            val index: Int = rupeesInString.indexOf(".")
            if (index >= 0) {
                val gap: Int = rupeesInString.length - index - 1
                if (gap == 0) {
                    paisaInString = rupeesInString.plus("00")
                } else if (gap == 1) {
                    paisaInString = rupeesInString.replace(".", "") + "0"
                } else if (gap == 2) {
                    paisaInString = rupeesInString.replace(".", "")
                } else {
                    paisaInString = rupeesInString
                        .substring(0, index + 3)
                        .replace(".", "")
                }
            } else {
                paisaInString = rupeesInString.plus("00")
            }
            paisaInLong = paisaInString.toLong()
            val isoAmount = String.format(Locale.US, "%012d", paisaInLong)
            paisaInLong = isoAmount.toLong()
        }
    } catch (exception: Exception) {
        exception.printStackTrace()
    }
    return paisaInLong
}

fun Long.toCurrencyFormat(): Double {
    var amountInDouble: Double = 0.0
    try {
        val df = DecimalFormat("#0.00")
        amountInDouble = df.format(this / 100.00).toDouble()
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
    return amountInDouble
}

fun Long.toBigDecimalCurrencyFormat(): BigDecimal {
    var amountInBigDecimal = BigDecimal.ZERO
    try {
        val df = DecimalFormat("#0.00")
        amountInBigDecimal = df.format(this/100.00).toBigDecimal()
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
    return amountInBigDecimal
}

fun String?.isNullOrEmptyOrBlank(): Boolean {
    var boolean = true
    if ((this != null) && !(this.isEmpty() || this.isBlank())) {
        boolean = false
    }
    return boolean;
}

fun String?.defaultOrEmptyValue(): String {
    var defaultValue = ""
    try {
        if (!this.isNullOrEmptyOrBlank()) {
            defaultValue = this!!
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
    return defaultValue
}

fun String.removeCharactersIfNotADigit(): String {
    var formattedData = this
    val charArray = formattedData.toCharArray()
    for (c in charArray) {
        if (!c.isDigit()) {
            formattedData = formattedData.replace(c.toString(), "")
        }
    }
    return formattedData
}

fun String.removePaddedLettersFromTheCardNumber(): String {
    var formattedData = this
    val charArray = formattedData.toCharArray()
    for (c in charArray) {
        if (c.isLetter() && !c.toString().equals("X", true)) {
            formattedData = formattedData.replace(c.toString(), "")
        }
    }
    return formattedData
}