plugins {
    id("java-library")
    id("kotlin")
}

dependencies {
    implementation (fileTree("dir" to "libs", "include" to listOf("*.jar") ))
    implementation ("org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlinVersion}")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
    kotlinOptions.jvmTarget = "1.8"
}


