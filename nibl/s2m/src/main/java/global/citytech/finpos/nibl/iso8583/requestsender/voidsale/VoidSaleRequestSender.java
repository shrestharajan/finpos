package global.citytech.finpos.nibl.iso8583.requestsender.voidsale;

import java.math.BigDecimal;

import global.citytech.finposframework.log.Logger;;

import global.citytech.finpos.nibl.iso8583.NIBLMessagePayloadBuilder;
import global.citytech.finpos.nibl.iso8583.NIBLMessageSenderTemplate;
import global.citytech.finpos.nibl.iso8583.PosEntryModeConfig;
import global.citytech.finpos.nibl.iso8583.ProcessingCode;
import global.citytech.finpos.nibl.iso8583.requestsender.TransactionIsoRequest;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.iso8583.RequestContext;
import global.citytech.finposframework.iso8583.SpecInfo;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.utility.HelperUtils;

/***
 * @author Surajchhetry
 */
public class  VoidSaleRequestSender extends NIBLMessageSenderTemplate {

    private Logger logger = Logger.getLogger(VoidSaleRequestSender.class.getName());

    public VoidSaleRequestSender(SpecInfo specInfo, RequestContext context) {
        super(specInfo, context);
    }

    @Override
    protected MessagePayload prepareRequestPayload() {
        MTI mti =
                new MTI(
                        MTI.Version.Version_1987,
                        MTI.MessageClass.Financial,
                        MTI.MessageFunction.Request,
                        MTI.Originator.Acquirer);
        TransactionIsoRequest voidSaleRequest = (TransactionIsoRequest) this.context.getRequest();
        Iso8583Msg msg = this.getTemplateIsoMessage(mti);
        msg.addField(
                new DataElement(
                        DataElement.PRIMARY_ACCOUNT_NUMBER,
                        voidSaleRequest.getPan()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.AMOUNT,
                        HelperUtils.toISOAmount(BigDecimal.ZERO, 12)
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.LOCAL_TIME,
                        voidSaleRequest.getLocalTime()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.LOCAL_DATE,
                        voidSaleRequest.getLocalDate()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.PAN_EXPIRE_DATE,
                        voidSaleRequest.getExpireDate()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.POS_ENTRY_MODE,
                        PosEntryModeConfig.getValue(voidSaleRequest.getPosEntryMode()
                        )
                )
        ); // Based on transaction source like ICC,Manual
        msg.addField(
                new DataElement(
                        DataElement.POINT_OF_SERVICE_CONDITIONAL_CODE,
                        voidSaleRequest.getPosConditionCode()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.RETRIEVAL_REFERENCE_NUMBER,
                        voidSaleRequest.getRetrievalReferenceNumber()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.AUTHORIZATION_IDENTIFICATION_RESPONSE,
                        voidSaleRequest.getApprovalCode()
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.PRIVATE_USE_FIELD_60,
                        HelperUtils.toISOAmount(voidSaleRequest.getOriginalAmount(), 12)
                )
        );
        msg.addField(
                new DataElement(
                        DataElement.PRIVATE_USE_FIELD_62,
                        voidSaleRequest.getInvoiceNumber()
                )
        );
        MessagePayload.Builder builder = new NIBLMessagePayloadBuilder(this.specInfo, msg);
        logger.debug("==== Request ====");
        logger.debug(msg.getPrintableData());
        return builder.build();
    }

    @Override
    protected String getProcessingCode() {
        return ProcessingCode.VOID_SALE.getCode();
    }

    @Override
    protected String getFunctionCode() {
        return "0011";
    }
}
