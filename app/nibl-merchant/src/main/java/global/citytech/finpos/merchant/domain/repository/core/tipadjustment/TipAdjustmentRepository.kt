package global.citytech.finpos.merchant.domain.repository.core.tipadjustment

import global.citytech.finpos.merchant.domain.model.tip.TipAdjustmentResponseEntity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.tipadjustment.TipAdjustmentRequestItem
import global.citytech.finpos.processor.nibl.transaction.tipadjustment.TipAdjustmentResponseModel
import io.reactivex.Observable

/**
 * Created by Unique Shakya on 5/4/2021.
 */
interface TipAdjustmentRepository {

    fun performTipAdjustment(
        configurationItem: ConfigurationItem,
        tipAdjustmentRequestItem: TipAdjustmentRequestItem
    ): Observable<TipAdjustmentResponseEntity>
}