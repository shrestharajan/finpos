package global.citytech.finpos.merchant.domain.utils

enum class Currency(private var currencyCode: String,private var currencyName: String){

    NPR("524", "NPR"),
    USD("840","USD");

    fun getCurrencyCode():String{
        return currencyCode
    }

    fun getCurrencyName():String{
        return currencyName
    }

}