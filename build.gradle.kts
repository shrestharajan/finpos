// Top-level build file where you can add configuration options common to all sub-projects/modules.


buildscript {
    val kotlin_version by extra("1.3.72")
    extra.set("junit","junit:junit:4.12")

    repositories {
        google()
        jcenter()
        maven { setUrl("https://maven.fabric.io/public") }
    }
    dependencies {
        var kotlinVersion: String by extra
        kotlinVersion = "1.3.61"
        classpath(AppPath.gradle)
        classpath(AppPath.kotlin)
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.61")
    }
}

allprojects {

    repositories {
        google()
        jcenter()
        maven {
            setUrl("http://repo.drose.com.np/artifactory/libs-release")
        }
        flatDir {
            dirs("libs")
        }


    }
}

tasks {
    val clean by registering(Delete::class) {
        delete(buildDir)
    }
}