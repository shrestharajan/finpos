package global.citytech.finpos.merchant.presentation.transactions.qr

import android.app.Application
import androidx.lifecycle.MutableLiveData
import global.citytech.common.data.Response
import global.citytech.easydroid.core.mvvm.BaseAndroidViewModel
import global.citytech.easydroid.core.utils.Jsons
import global.citytech.finpos.merchant.NiblMerchant
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.data.repository.app.LocalRepositoryImpl
import global.citytech.finpos.merchant.data.repository.app.QrPaymentRepositoryImpl
import global.citytech.finpos.merchant.domain.model.app.QrPayment
import global.citytech.finpos.merchant.domain.usecase.app.LocalDataUseCase
import global.citytech.finpos.merchant.domain.usecase.app.QrPaymentUseCase
import global.citytech.finpos.merchant.framework.datasource.app.DeviceConfiguration
import global.citytech.finpos.merchant.framework.datasource.app.LocalDatabaseSourceImpl
import global.citytech.finpos.merchant.framework.datasource.app.PreferenceManager
import global.citytech.finpos.merchant.framework.datasource.app.QrPaymentDataSourceImpl
import global.citytech.finpos.merchant.framework.datasource.core.TerminalRepositoryImpl
import global.citytech.finpos.merchant.framework.datasource.device.PrinterSourceImpl
import global.citytech.finpos.merchant.presentation.alertdialogs.TransactionConfirmation
import global.citytech.finpos.merchant.presentation.dynamicqr.nqrsale.DynamicNQrActivity
import global.citytech.finpos.merchant.presentation.dynamicqr.receipt.DynamicQrReceiptGenerator
import global.citytech.finpos.merchant.presentation.dynamicqr.receipt.DynamicQrTransactionReceiptPrintHandler
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.presentation.model.mapToPresentation
import global.citytech.finpos.merchant.presentation.model.qr.voidqr.QrVoidRequest
import global.citytech.finpos.merchant.presentation.model.qr.voidqr.QrVoidResponse
import global.citytech.finpos.merchant.utils.AppUtility
import global.citytech.finpos.merchant.utils.AppUtility.printLog
import global.citytech.finpos.merchant.utils.SoundUtils
import global.citytech.finpos.merchant.utils.VoiceMessageUtils
import global.citytech.finposframework.hardware.common.Result
import global.citytech.finposframework.hardware.io.printer.PrinterResponse
import global.citytech.finposframework.log.Logger
import global.citytech.finposframework.repositories.TerminalRepository
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion
import global.citytech.finposframework.utility.StringUtils
import io.reactivex.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class QrTransactionDetailViewModel(val context: Application) : BaseAndroidViewModel(context) {

    val TAG = QrTransactionDetailViewModel::class.java.name
    var configurationItem: ConfigurationItem? = null
    val transactionConfirmationLiveData by lazy { MutableLiveData<TransactionConfirmation>() }
    val finishActivity by lazy { MutableLiveData<Boolean>() }
    var updateTransactionConfirmation = false
    var voidedQrPayment: QrPayment? = null

    private val printerService = PrinterSourceImpl(context)
    private val localDataUseCase =
        LocalDataUseCase(LocalRepositoryImpl(LocalDatabaseSourceImpl(), PreferenceManager))
    private var terminalRepository: TerminalRepository? = null
    private val qrPaymentUseCase = QrPaymentUseCase(
        QrPaymentRepositoryImpl(
            QrPaymentDataSourceImpl(context)
        )
    )

    fun getConfigurationItem() {
        compositeDisposable.add(localDataUseCase.getWholeConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.mapToPresentation() }
            .subscribe({
                configurationItem = it
                terminalRepository = TerminalRepositoryImpl(configurationItem)
            }, {
                printLog(TAG, "Get Configuration error::: " + it.message.toString())
            })
        )
    }

    fun printDuplicateReceipt(qrPayment: QrPayment) {
        isLoading.value = true
        compositeDisposable.add(
            onPrintDuplicateReceiptSubscribe(qrPayment)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onDuplicatePrinterResponseReceiptReceived(it)
                }, {
                    printLog(TAG, "PRINT DUPLICATE RECEIPT ::: "+it.message.toString())
                    isLoading.value = false
                    it.printStackTrace()
                    message.value = it.message
                })
        )
    }

    private fun onDuplicatePrinterResponseReceiptReceived(it: PrinterResponse) {
        isLoading.value = false
        if (it.result == Result.SUCCESS || it.result == Result.CANCEL) {
            return
        }
        message.value = it.message
    }

    private fun onPrintDuplicateReceiptSubscribe(
        qrPayment: QrPayment
    ): Observable<PrinterResponse> {
        val qrReceipt = DynamicQrReceiptGenerator(terminalRepository!!).generate(qrPayment)
        val qrTransactionReceiptPrintHandler =
            DynamicQrTransactionReceiptPrintHandler(printerService)
        return Observable.fromCallable {
            qrTransactionReceiptPrintHandler.printReceipt(
                qrReceipt,
                ReceiptVersion.DUPLICATE_COPY
            )
        }
    }

    fun performVoid(originalQrPayment: QrPayment) {
        isLoading.value = true
        compositeDisposable.add(Observable.create(
            ObservableOnSubscribe<QrVoidResponse> {
                onQrVoidSubscribe(it, originalQrPayment)
            }
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                val isApproved = true
                updateTransactionConfirmation = false
                transactionConfirmationLiveData.value = TransactionConfirmation.Builder()
                    .amount(StringUtils.formatAmountTwoDecimal(originalQrPayment.transactionAmount))
                    .imageId(if (isApproved) R.drawable.approved else R.drawable.declined)
                    .message(if (isApproved) "Approval Code: ".plus(it.requestNumber) else "DECLINED")
                    .title(if (isApproved) "APPROVED" else "DECLINED")
                    .positiveLabel("")
                    .negativeLabel("")
                    .qrImageString("")
                    .transactionStatus("")
                    .build()

                storeTransactionLog(originalQrPayment, isApproved, it)
                playSound(isApproved, originalQrPayment.transactionAmount.toDouble())
            }, {
                printLog(TAG, "PERFORM VOID ::: "+it.message.toString())
                isLoading.value = false
                message.value = "Could not perform void. Please try again."
            })
        )
    }

    private fun storeTransactionLog(
        originalQrPayment: QrPayment,
        isApproved: Boolean,
        qrVoidResponse: QrVoidResponse
    ) {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<QrPayment> {
                val responseDateTimeStamp = qrVoidResponse.localDateTime
                val transactionStatus = if (isApproved) "APPROVED" else "DECLINED"
                val voidQrPayment = QrPayment(
                    merchantId = originalQrPayment.merchantId,
                    terminalId = originalQrPayment.terminalId,
                    referenceNumber = originalQrPayment.referenceNumber,
                    invoiceNumber = originalQrPayment.invoiceNumber,
                    transactionType = "VOID SALE",
                    transactionDate = responseDateTimeStamp.substring(0, 10),
                    transactionTime = responseDateTimeStamp.substring(10),
                    transactionCurrency = originalQrPayment.transactionCurrency,
                    transactionAmount = originalQrPayment.transactionAmount,
                    transactionStatus = transactionStatus,
                    approvalCode = qrVoidResponse.requestNumber,
                    paymentInitiator = originalQrPayment.paymentInitiator,
                    initiatorId = originalQrPayment.initiatorId
                )
                qrPaymentUseCase.updateQrPaymentVoided(originalQrPayment)
                qrPaymentUseCase.addQrPayment(voidQrPayment)
                it.onNext(voidQrPayment)
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    voidedQrPayment = it
                    printQrReceipt(ReceiptVersion.MERCHANT_COPY)
                }, {
                    printLog(TAG, "STORE TRANSACTION LOG ::: "+it.message.toString())
                    isLoading.value = false
                    it.printStackTrace()
                })
        )
    }

    fun printQrReceipt(receiptVersion: ReceiptVersion) {
        if (receiptVersion == ReceiptVersion.CUSTOMER_COPY)
            isLoading.value = true
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<PrinterResponse> {
                val dynamicReceipt =
                    DynamicQrReceiptGenerator(terminalRepository!!).generate(
                        voidedQrPayment!!
                    )
                val printerResponse =
                    DynamicQrTransactionReceiptPrintHandler(printerService).printReceipt(
                        dynamicReceipt,
                        receiptVersion
                    )
                it.onNext(printerResponse)
                it.onComplete()
            }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    updateTransactionConfirmation = true
                    if (receiptVersion == ReceiptVersion.MERCHANT_COPY) {
                        val transactionConfirmation = transactionConfirmationLiveData.value
                        transactionConfirmation!!.positiveLabel = "PRINT CUSTOMER COPY"
                        transactionConfirmation.negativeLabel = "RETURN TO TRANSACTIONS"
                        transactionConfirmationLiveData.value = transactionConfirmation
                    } else {
                        finishActivity.value = true
                    }
                }, {
                    printLog(TAG, "QR RECEIPT ::: "+it.message.toString())
                    isLoading.value = false
                    it.printStackTrace()
                })
        )
    }

    private fun onQrVoidSubscribe(
        it: ObservableEmitter<QrVoidResponse>,
        originalQrPayment: QrPayment
    ) {
        val qrVoidRequest = QrVoidRequest(
            originalQrPayment.merchantId,
            originalQrPayment.terminalId,
            originalQrPayment.referenceNumber,
            StringUtils.formatAmountTwoDecimal(originalQrPayment.transactionAmount)
        )
        val jsonRequest = Jsons.toJsonObj(qrVoidRequest)
        val jsonResponse = NiblMerchant.INSTANCE.iPlatformManager.post(
            "/device-payment-processor-endpoints/void/",
            jsonRequest,
            true
        )
        val response = Jsons.fromJsonToObj(jsonResponse, Response::class.java)
        if (response.data != null) {
            val responseDataJson = Jsons.toJsonObj(response.data)
            it.onNext(Jsons.fromJsonToObj(responseDataJson, QrVoidResponse::class.java))
        } else {
            it.onError(Exception("Error Code :: ${response.code}. ${response.message}"))
        }
    }

    fun getMerchantCredential(): String = localDataUseCase.getMerchantCredential()

    private fun playSound(isApproved: Boolean, amount: Double) {
        if (isApproved){
            /** APPROVED */
            if (localDataUseCase.isSoundMode(false))
                SoundUtils.play(getApplication<Application?>().applicationContext, amount,"radha", AppUtility.getCurrencyCode()
                )
            Logger.getLogger(DynamicNQrActivity::class.simpleName)
                .debug("transaction approved")
        }else{
            /** DECLINED */
            if (localDataUseCase.isSoundMode(false))
                VoiceMessageUtils.playFail(getApplication<Application?>().applicationContext)
            Logger.getLogger(DynamicNQrActivity::class.simpleName)
                .debug("transaction declined")
        }
    }
}