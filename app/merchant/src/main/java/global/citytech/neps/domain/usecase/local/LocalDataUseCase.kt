package global.citytech.neps.domain.usecase.local

import global.citytech.neps.domain.model.Host
import global.citytech.neps.domain.model.Logo
import global.citytech.neps.domain.model.Merchant
import global.citytech.neps.domain.repository.LocalRepository
import io.reactivex.Observable
import io.reactivex.functions.BiFunction


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
data class HostMerchant(val hostList: List<Host>, val merchantList: List<Merchant>)
data class Configuration(
    val hostList: List<Host>,
    val merchantList: List<Merchant>,
    val logos: List<Logo>
)

class LocalDataUseCase(private val configurationRepository: LocalRepository) {

    fun getHosts(): Observable<List<Host>> = configurationRepository.getHost()
    fun getMerchants(): Observable<List<Merchant>> = configurationRepository.getMerchant()
    fun getLogos(): Observable<List<Logo>> = configurationRepository.getLogo()

    fun getConfiguration(): Observable<HostMerchant> =
        Observable.zip(getHosts(), getMerchants()
            , BiFunction { t1, t2 -> map(t1, t2) })

    fun getWholeConfiguration(): Observable<Configuration> =
        Observable.zip(getConfiguration(), getLogos()
            , BiFunction { t1, t2 -> map(t1, t2) })

    private fun map(hostList: List<Host>, merchantList: List<Merchant>): HostMerchant =
        HostMerchant(
            hostList,
            merchantList
        )

    private fun map(hostMerchant: HostMerchant, logos: List<Logo>): Configuration =
        Configuration(
            hostMerchant.hostList,
            hostMerchant.merchantList,
            logos
        )

}