package global.citytech.finposframework.switches.transactiontype;

import global.citytech.finposframework.switches.Request;

/**
 * Created by Unique Shakya on 12/1/2020.
 */
public interface TransactionTypeRequester<T extends TransactionTypeRequestParameter,
        E extends TransactionTypeResponseParameter> extends Request<T, E> {
}
