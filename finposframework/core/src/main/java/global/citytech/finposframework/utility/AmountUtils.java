package global.citytech.finposframework.utility;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Locale;

public class AmountUtils {

    private AmountUtils() {
    }

    public static String toLowerCurrency(String higherCurrency) {
        String lowerCurrency = "";
        long lowerCurrencyLong = 0;
        if (StringUtils.isEmpty(higherCurrency)) {
            return "";
        } else {
            int indexOfPeriod = higherCurrency.indexOf(".");
            if (indexOfPeriod >= 0) {
                int gap = higherCurrency.length() - indexOfPeriod - 1;
                if (gap == 0)
                    lowerCurrency = higherCurrency + "00";
                else if (gap == 1)
                    lowerCurrency = higherCurrency.replace(".", "") + "0";
                else if (gap == 2)
                    lowerCurrency = higherCurrency.replace(".", "");
                else
                    lowerCurrency = higherCurrency.substring(0, indexOfPeriod + 3).replace(".", "");
            } else
                lowerCurrency = higherCurrency + "00";
            lowerCurrencyLong = parseLong(lowerCurrency);
            if (lowerCurrencyLong == 0)
                return "";
        }
        return String.valueOf(lowerCurrencyLong);
    }

    public static String toISOFormattedLowerCurrency(String higherCurrency) {
        String convertedLowerCurrency = toLowerCurrency(higherCurrency);
        long lowerCurrencyInLong = parseLong(convertedLowerCurrency);
        return String.format(Locale.getDefault(), "%012d", lowerCurrencyInLong);
    }

    public static long toLongLowerCurrency(double higherCurrency){
        return Long.parseLong(String.valueOf((int)(higherCurrency * 100)));
    }

    public static double toLowerCurrency(double higherCurrency) {
        return higherCurrency * 100.00;
    }

    public static String toHigherCurrency(double lowerCurrency) {
        double higherCurrency = lowerCurrency / 100.00;
        return String.format(Locale.US, "%.2f", higherCurrency);
    }

    public static double toHigherCurrencyDouble(double lowerCurrency) {
        return lowerCurrency / 100.00;
    }

    public static long parseLong(String sLong) {
        if (StringUtils.isEmpty(sLong)) {
            return 0;
        }
        long result = 0;
        try {
            result = Long.parseLong(sLong);
        } catch (NumberFormatException e) {
            return 0;
        }
        return result;
    }

    public static double addAmounts(double... amount) {
        long total = 0;
        for (double anAmount : amount) {
            String number = toLowerCurrency(String.valueOf(anAmount));
            total = total + parseLong(number);
        }
        return (double) total / 100.00;
    }

    public static String formatAmountTwoDecimal(double amount) {
        try {
            DecimalFormat df = new DecimalFormat("0.00");
            return df.format(amount);
        } catch (Exception e) {
            e.printStackTrace();
            return String.valueOf(amount);
        }
    }

    public static long toLowerCurrencyInLong(double rupees) {
        double paisas = rupees * 100.00;
        String paisasInString = String.format(Locale.US, "%.0f", paisas);
        long paisasInLong = 0;
        try {
            paisasInLong = Long.parseLong(paisasInString);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return paisasInLong;
    }

    public static long toLowerCurrencyFromBigDecimalInLong(BigDecimal rupees) {
        BigDecimal paisas = rupees.multiply(BigDecimal.valueOf(100.00));
        String paisasInString = String.format(Locale.US, "%.0f", paisas);
        long paisasInLong = 0;
        try {
            paisasInLong = Long.parseLong(paisasInString);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return paisasInLong;
    }
}
