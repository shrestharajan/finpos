package global.citytech.finpos.neps.iso8583;

import global.citytech.finposframework.switches.FunctionCode;

public class FunctionCodeConfig {

    public static String getValue(FunctionCode functionCode) {
        switch (functionCode) {
            case LOG_ON:
                return "801";
            case KEY_EXCHANGE:
                return "811";
            case TRANSACTION:
                return "200";
            case REVERSAL:
                return "400";
            case RECONCILICATION:
                return "504";
            case GREEN_PIN:
                return "100";
            default:
                return "000";
        }
    }
}
