package global.citytech.finpos.merchant.presentation.dynamicqr.receipt

import global.citytech.finposframework.hardware.io.printer.Printable
import global.citytech.finposframework.hardware.io.printer.PrinterRequest
import global.citytech.finposframework.hardware.io.printer.PrinterResponse
import global.citytech.finposframework.hardware.io.printer.PrinterService
import global.citytech.finposframework.usecases.transaction.receipt.ReceiptVersion
import global.citytech.finposframework.utility.ReceiptUtils.*
import global.citytech.finposframework.utility.StringUtils

class DynamicQrTransactionReceiptPrintHandler(val printerService: PrinterService) {

    fun printReceipt(
        dynamicQrReceipt: DynamicQrReceipt,
        receiptVersion: ReceiptVersion
    ): PrinterResponse {
        val printerRequest = preparePrinterRequest(dynamicQrReceipt, receiptVersion)
        return printerService.print(printerRequest)
    }

    private fun preparePrinterRequest(
        dynamicQrReceipt: DynamicQrReceipt,
        receiptVersion: ReceiptVersion
    ): PrinterRequest {
        val printableList = mutableListOf<Printable>()
        if (!StringUtils.isEmpty(dynamicQrReceipt.retailer.retailerLogo))
            addBase64Image(printableList, dynamicQrReceipt.retailer.retailerLogo)
        addSingleColumnString(
            printableList,
            dynamicQrReceipt.retailer.retailerName,
            DynamicQrReceiptStyle.RETAILER_NAME.getStyle()
        )
        addSingleColumnString(
            printableList,
            dynamicQrReceipt.retailer.retailerAddress,
            DynamicQrReceiptStyle.RETAILER_ADDRESS.getStyle()
        )
        addSingleColumnString(printableList, "  ", DynamicQrReceiptStyle.DIVIDER.getStyle())
        addDoubleColumnString(
            printableList,
            this.prepareDateString(dynamicQrReceipt.performance.startDateTime),
            this.prepareTimeString(dynamicQrReceipt.performance.startDateTime),
            DynamicQrReceiptStyle.DATE_TIME.getStyle()
        )
        addDoubleColumnString(
            printableList,
            this.merchantId(dynamicQrReceipt.retailer.merchantId),
            this.terminalId(dynamicQrReceipt.retailer.terminalId),
            DynamicQrReceiptStyle.MID_TID.getStyle()
        )
        addDoubleColumnString(
            printableList,
            DynamicQrReceiptLabel.REFERENCE_NUMBER,
            dynamicQrReceipt.qrTransactionDetail.referenceNumber,
            DynamicQrReceiptStyle.REFERENCE_NUMBER.getStyle()
        )
        addSingleColumnString(printableList, "  ", DynamicQrReceiptStyle.DIVIDER.getStyle())
        addSingleColumnString(
            printableList,
            dynamicQrReceipt.qrTransactionDetail.transactionType,
            DynamicQrReceiptStyle.TRANSACTION_TYPE.getStyle()
        )
        addSingleColumnString(
            printableList,
            prepareAmountString(dynamicQrReceipt),
            DynamicQrReceiptStyle.TRANSACTION_AMOUNT.getStyle()
        )
        addSingleColumnString(
            printableList,
            dynamicQrReceipt.qrTransactionDetail.transactionResult,
            DynamicQrReceiptStyle.TRANSACTION_RESULT.getStyle()
        )
        addSingleColumnString(printableList, "  ", DynamicQrReceiptStyle.DIVIDER.getStyle())
        addDoubleColumnString(
            printableList,
            DynamicQrReceiptLabel.PAYMENT_INITIATOR,
            dynamicQrReceipt.qrTransactionDetail.paymentInitiator,
            DynamicQrReceiptStyle.PAYMENT_INITIATOR.getStyle()
        )
        addDoubleColumnString(
            printableList,
            DynamicQrReceiptLabel.INITIATOR_ID,
            dynamicQrReceipt.qrTransactionDetail.initiatorId,
            DynamicQrReceiptStyle.INITIATOR_ID.getStyle()
        )
        addDoubleColumnString(
            printableList,
            if (dynamicQrReceipt.qrTransactionDetail.transactionType.toLowerCase() == "sale")
                DynamicQrReceiptLabel.APPROVAL_CODE else DynamicQrReceiptLabel.PROCESSING_NUMBER,
            dynamicQrReceipt.qrTransactionDetail.approvalCode,
            DynamicQrReceiptStyle.APPROVAL_CODE.getStyle()
        )
        addQRCodeImage(
            printableList,
            dynamicQrReceipt.qrCodeString,
            200,
            DynamicQrReceiptStyle.QR_CODE.getStyle()
        )
        addSingleColumnString(printableList, "  ", DynamicQrReceiptStyle.DIVIDER.getStyle())
        addSingleColumnString(
            printableList,
            dynamicQrReceipt.thankYouMessage,
            DynamicQrReceiptStyle.THANK_YOU_MESSAGE.getStyle()
        )
        addSingleColumnString(
            printableList,
            receiptVersion.versionLabel,
            DynamicQrReceiptStyle.RECEIPT_VERSION.getStyle()
        )
        addSingleColumnString(printableList, "  ", DynamicQrReceiptStyle.DIVIDER.getStyle())
        return PrinterRequest(printableList)
    }

    private fun prepareAmountString(dynamicQrReceipt: DynamicQrReceipt): String {
        val stringBuilder = StringBuilder()
        stringBuilder.append(dynamicQrReceipt.qrTransactionDetail.transactionCurrency)
        stringBuilder.append("\t")
        stringBuilder.append(dynamicQrReceipt.qrTransactionDetail.transactionAmount)
        return stringBuilder.toString()
    }

    private fun terminalId(terminalId: String): String {
        return this.formatLabelWithValue(DynamicQrReceiptLabel.TERMINAL_ID, terminalId)
    }

    private fun merchantId(merchantId: String): String {
        return this.formatLabelWithValue(DynamicQrReceiptLabel.MERCHANT_ID, merchantId)
    }

    private fun prepareTimeString(startDateTime: String): String {
        if (StringUtils.isEmpty(startDateTime) || startDateTime.length < 15) return ""
        return formatLabelWithValue(DynamicQrReceiptLabel.TIME, startDateTime.substring(10))
    }

    private fun prepareDateString(startDateTime: String): String {
        if (StringUtils.isEmpty(startDateTime) || startDateTime.length < 18) return ""
        return formatLabelWithValue(DynamicQrReceiptLabel.DATE, startDateTime.substring(0, 10))
    }

    private fun formatLabelWithValue(label: String, value: String): String {
        val stringBuilder = StringBuilder()
        stringBuilder.append(label)
        stringBuilder.append(": ")
        stringBuilder.append(value)
        return stringBuilder.toString()
    }
}