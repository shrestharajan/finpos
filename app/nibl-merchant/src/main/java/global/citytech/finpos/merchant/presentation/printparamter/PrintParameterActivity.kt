package global.citytech.finpos.merchant.presentation.printparamter

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import global.citytech.finpos.merchant.BR
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.databinding.ActivityPrintParameterBinding
import global.citytech.finpos.merchant.presentation.base.AppBaseActivity
import global.citytech.finpos.merchant.presentation.model.ConfigurationItem
import global.citytech.finpos.merchant.utils.MessageConfig
import global.citytech.finposframework.switches.receipt.tmslogs.TmsLogsParameter
import kotlinx.android.synthetic.main.activity_print_parameter.*

class PrintParameterActivity : AppBaseActivity<ActivityPrintParameterBinding,PrintParameterViewModel>() {

    private val parameters = arrayListOf<TmsLogsParameter>()
//    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        initObservers()
        this.getViewModel().checkDeviceConfigured()
    }

    private fun initViews() {
        iv_back.setOnClickListener {
            finish()
        }
        cb_switch_param.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked){
                parameters.add(TmsLogsParameter.SWITCH_PARAMETERS)
            }else{
                parameters.remove(TmsLogsParameter.SWITCH_PARAMETERS)
            }
        }
        cb_aid.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked){
                parameters.add(TmsLogsParameter.AID)
            }else{
                parameters.remove(TmsLogsParameter.AID)
            }
        }
        cb_emv.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked){
                parameters.add(TmsLogsParameter.EMV_KEYS)
            }else{
                parameters.remove(TmsLogsParameter.EMV_KEYS)
            }
        }
        cb_card_scheme.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked){
                parameters.add(TmsLogsParameter.CARD_SCHEME)
            }else{
                parameters.remove(TmsLogsParameter.CARD_SCHEME)
            }
        }

        btn_print.setOnClickListener {
            showProgressScreen(getString(R.string.title_printing_terminal_parameters))
            getViewModel().validateParametersToPrint(parameters)
        }

    }


    private fun initObservers() {
        this.getViewModel().terminalConfigured.observe(
            this,
            Observer {
                onObserveTerminalConfigured(it)
            }
        )

        this.getViewModel().configurationItem.observe(
            this,
            Observer {
                initiateParametersPrint(it, parameters)
            }
        )

        this.getViewModel().message.observe(
            this,
            Observer {
                displayMessage(it)
            }
        )

        this.getViewModel().printSuccess.observe(
            this,
            Observer {
                onObservePrintSuccess(it)
            }
        )

        this.getViewModel().isLoading.observe(
            this,
            Observer {
                onObserveLoading(it)
            }
        )
    }

    private fun displayMessage(message: String?) {
        if (!message.isNullOrEmpty()) {
            val messageConfig = MessageConfig.Builder()
                .message(message)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick { }
            showNormalMessage(messageConfig)
        }
    }

    private fun displayMessageAndExit(message: String?) {
        if (!message.isNullOrEmpty()) {
            val messageConfig = MessageConfig.Builder()
                .message(message)
                .positiveLabel(getString(R.string.title_ok))
                .onPositiveClick {
                    finish()
                }
            showNormalMessage(messageConfig)
        }
    }

    private fun onObserveTerminalConfigured(configured: Boolean) {
        if (!configured) {
            displayMessageAndExit(getString(R.string.msg_not_configured))
        }
    }

    private fun onObservePrintSuccess(printSuccess: Boolean) {
        cb_switch_param.isChecked = false
        cb_aid.isChecked = false
        cb_emv.isChecked = false
        cb_card_scheme.isChecked = false
    }

    private fun onObserveLoading(isLoading: Boolean) {
        if (!isLoading) {
            hideProgressScreen()
        }
    }

    private fun initiateParametersPrint(
        configurationItem: ConfigurationItem,
        parameters: ArrayList<TmsLogsParameter>
    ) {
        this.getViewModel().printParameters(
            configurationItem,
            parameters
        )
    }

    private fun showProgressScreen(message: String) {
        showProgress(message)
    }

    private fun hideProgressScreen() {
        hideProgress()
    }

    override fun getBindingVariable(): Int = BR.printParameterViewModel

    override fun getLayout(): Int  = R.layout.activity_print_parameter

    override fun getViewModel(): PrintParameterViewModel = ViewModelProviders.of(this)[PrintParameterViewModel::class.java]
}
