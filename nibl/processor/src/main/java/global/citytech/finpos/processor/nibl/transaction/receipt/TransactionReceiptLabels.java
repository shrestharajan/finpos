package global.citytech.finpos.processor.nibl.transaction.receipt;

public class TransactionReceiptLabels {

    private TransactionReceiptLabels() {
    }

    public static final String SIGNATURE_MESSAGE_CUSTOMER = "CUSTOMER SIGN BELOW";
    public static final String SIGNATURE_LINE = "_______________________";
    public static final String DEBIT_ACCOUNT_ACKNOWLEDGEMENT = "DEBIT MY ACCOUNT FOR THE AMOUNT";
    public static final String APPROVAL_CODE_LABEL = "APPROVAL CODE: ";
    public static final String THANK_YOU_MESSAGE = "THANK YOU FOR USING NIMB";
    public static final String RETAIN_RECEIPT = "PLEASE RETAIN YOUR RECEIPT";
    public static final String CUSTOMER_COPY = "** CUSTOMER COPY **";
    public static final String AMOUNT_LABEL = "AMOUNT";
    public static final String TIP_AMOUNT_LABEL = "TIP";
    public static final String TOTAL_AMOUNT_LABEL = "TOTAL";
    public static final String DATE_LABEL = "DATE";
    public static final String TIME_LABEL = "TIME";
    public static final String TERMINAL_ID_LABEL = "TID";
    public static final String MERCHANT_ID_LABEL = "MID";
    public static final String BATCH_NUMBER_LABEL = "BATCH NUMBER: ";
    public static final String STAN_LABEL = "TRACE NO";
    public static final String RRN_LABEL = "RRN";
    public static final String INVOICE_NUMBER_LABEL = "INVOICE NUMBER: ";
    public static final String CARD_NUMBER = "Card No: ";
    public static final String CARD_TYPE = "Card Type: ";
    public static final String AID = "AID: ";
    public static final String DIVIDER = "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --";

    public static final String CARD_TYPE_ICC = "CHIP";
    public static final String CARD_TYPE_MAGNETIC = "SWIPED";
    public static final String CARD_TYPE_MANUAL = "MANUAL";
    public static final String CARD_TYPE_PICC = "TAPPED";

    public static final String TAG_TVR = "TVR";
    public static final String TAG_TSI = "TSI";
    public static final String TAG_IIN = "IIN";
    public static final String EMI_TRANSACTION_LABEL = "** EMI TRANSACTION **";
    public static final String EMI_MONTHLY_EMI_AMOUNT_LABEL = "Monthly EMI Amount";
    public static final String EMI_INTEREST_RATE_LABEL = "Interest Rate";
    public static final String EMI_TIME_PERIOD_LABEL = "Time Period";
}
