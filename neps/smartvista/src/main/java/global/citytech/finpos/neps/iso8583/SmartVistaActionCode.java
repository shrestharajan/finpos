package global.citytech.finpos.neps.iso8583;

import global.citytech.finposframework.usecases.TransactionStatusFromHost;

/**
 * Created by Saurav Ghimire on 1/21/21.
 * Citytech
 * saurav.ghimire@citytech.global
 */
public enum  SmartVistaActionCode {

    ACTION_CODE_000(0,"Successful Transaction",TransactionStatusFromHost.ONLINE_APPROVED),
    ACTION_CODE_001(1,"Approve with ID",TransactionStatusFromHost.ONLINE_APPROVED),
    ACTION_CODE_002(2,"ATM performed a partial dispense",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_003(3,"Successful Transaction",TransactionStatusFromHost.ONLINE_APPROVED),
    ACTION_CODE_005(5,"System Error",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_020(20,"Successful transaction",TransactionStatusFromHost.ONLINE_APPROVED),
    ACTION_CODE_095(95,"Reconcile Error",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_100(100,"Do not honor transaction",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_101(101,"Expired Card",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_103(103,"Call Issuer",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_104(104,"Card is restricted",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_105(105,"Call security",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_106(106,"Excessive pin failures",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_107(107,"Call Issuer",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_109(109,"Invalid merchant ID",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_110(110,"Cannot process amount",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_111(111,"Invalid account - retry",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_116(116,"Insufficient funds - retry",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_117(117,"Incorrect Pin",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_118(118,"Forced post, no account on file",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_119(119,"Transaction not permitted by law",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_120(120,"Not permitted",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_121(121,"Withdrawal limit exceeded - retry",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_123(123,"Limit reached for total number of transactions in cycle",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_125(125,"Bad Card",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_126(126,"Pin processing error",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_127(127,"Pin processing error",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_128(128,"Pin processing error",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_200(200,"Invalid card",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_201(201,"Card expired",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_202(202,"Invalid card",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_203(203,"Call security",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_204(204,"Account restricted",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_205(205,"Call security",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_206(206,"Invalid Pin",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_208(208,"Lost Card",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_209(209,"Stolen Card",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_901(901,"Invalid payment parameters",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_902(902,"Invalid transaction - retry",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_903(903,"Transaction needs to be entered again",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_904(904,"The message received was not within standards",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_905(905,"Issuing institution is unknown",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_907(907,"Issuer inoperative",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_908(908,"Issuing institution is unknown",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_909(909,"System malfunction",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_910(910,"Issuer inoperative",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_911(911,"SmartVista FE has no knowledge of any attempt to either authorize or deny the transaction.",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_912(912,"Time out waiting for response",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_913(913,"Duplicate transaction received",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_914(914,"Issuer inoperativeCould not find the original transaction",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_915(915,"Amount being reversed is greater than original, or no amount being reversed.",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_916(916,"Debts not found",TransactionStatusFromHost.ONLINE_DECLINED),
    ACTION_CODE_920(920,"Pin processing error",TransactionStatusFromHost.ONLINE_DECLINED);


    private int actionCode;
    private String description;
    private TransactionStatusFromHost transactionStatusFromHost;

    SmartVistaActionCode(int actionCode, String description, TransactionStatusFromHost transactionStatusFromHost) {
        this.actionCode = actionCode;
        this.description = description;
        this.transactionStatusFromHost = transactionStatusFromHost;
    }

    public TransactionStatusFromHost getTransactionStatusFromHost() {
        return transactionStatusFromHost;
    }

    public int getActionCode() {
        return actionCode;
    }

    public String getDescription() {
        return description;
    }

    public static SmartVistaActionCode getByActionCode(int actionCode) {
        SmartVistaActionCode[] dataTypes = values();
        SmartVistaActionCode[] var2 = dataTypes;
        int var3 = dataTypes.length;

        for (int var4 = 0; var4 < var3; ++var4) {
            SmartVistaActionCode dataType = var2[var4];
            if (dataType.getActionCode() == actionCode) {
                return dataType;
            }
        }
        throw new IllegalArgumentException("No such action code");
    }


}
