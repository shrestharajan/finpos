package global.citytech.finpos.merchant.presentation.transactions.qr

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import global.citytech.common.Constants
import global.citytech.common.extensions.hideKeyboard
import global.citytech.finpos.merchant.R
import global.citytech.finpos.merchant.domain.model.app.QrPayment
import global.citytech.finpos.merchant.domain.usecase.mobile.MobileNfcPaymentConst.INITIATOR_QR
import global.citytech.finpos.merchant.presentation.base.AppBaseFragment
import global.citytech.finpos.merchant.presentation.transactions.TransactionsViewModel
import global.citytech.finposframework.log.Logger
import kotlinx.android.synthetic.main.fragment_qr_transactions.*


class QrTransactionFragment : AppBaseFragment(), QrTransactionAdapter.Listener {

    override fun getLayoutId(): Int = R.layout.fragment_qr_transactions

    private val logger: Logger = Logger.getLogger(QrTransactionFragment::class.java.name)

    private val adapter = QrTransactionAdapter(mutableListOf(), this)
    private var qrPayments = mutableListOf<QrPayment>()
    lateinit var textWatcher: TextWatcher
    lateinit var onEditActionListener: TextView.OnEditorActionListener
    var pageNumber = 1
    var searchPageNumber = 1
    lateinit var scrollListener: RecyclerView.OnScrollListener
    var transactionsCount = 0
    var searchTransactionsCount = 0
    val pageSize = Constants.DEFAULT_PAGE_SIZE
    var searchParam = ""
    var alreadySwipedForRefresh = false

//    companion object {
//
//        private var instance: QrTransactionFragment? = null
//
//        fun getInstance(): QrTransactionFragment {
//            if (instance == null)
//                instance = QrTransactionFragment()
//            return instance as QrTransactionFragment
//        }
//    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()

        hideSoftKeyboard(requireActivity(),requireView())
    }

    override fun onResume() {
        super.onResume()
        logger.log("getQrTransaction :::  fromOnCreate")
        pageNumber = 1
        getViewModel().getTransactionsEitherQrOrMobilePay(pageNumber, pageSize, false, INITIATOR_QR)
        println("::: QR TRANSACTION FRAGMENT >>> ON RESUME CALLED <<<")
    }

    private fun initObservers() {
        getViewModel().transactionAvailableEitherQrOrMobilePay.observe(viewLifecycleOwner, Observer {
            swipe_refresh_layout_fragment_qr.isRefreshing = false
            alreadySwipedForRefresh = false
            if (it) {
                et_reference_number.visibility = View.GONE
                tv_no_txn.visibility = View.INVISIBLE
                cv_rv_wrapper.visibility = View.VISIBLE
                rv_transactions.visibility - View.VISIBLE
            } else {
                et_reference_number.visibility = View.GONE
                tv_no_txn.visibility = View.VISIBLE
                cv_rv_wrapper.visibility = View.INVISIBLE
                rv_transactions.visibility - View.INVISIBLE
            }
        })

        getViewModel().noInternet.observe(viewLifecycleOwner){
            if (it){
                tv_no_txn.text  = getString(R.string.no_internet_transactions)
            }else{
                tv_no_txn.text  = getString(R.string.title_no_txns)
            }
        }

        getViewModel().paymentsEitherQrOrMobilePay.observe(viewLifecycleOwner, Observer {
            if (isResumed){
                this.qrPayments = Gson().fromJson(Gson().toJson(it),
                    object : TypeToken<List<QrPayment>>() {}.type)
                adapter.updateAll(qrPayments)
            }
        })

        getViewModel().isPaginatingEitherQrOrMobilePay.observe(viewLifecycleOwner, Observer {
            if (it) {
                iv_progress_pagination.visibility = View.VISIBLE
            } else {
                iv_progress_pagination.visibility = View.GONE
            }
        })

        getViewModel().paymentCountEitherQrOrMobilePay.observe(viewLifecycleOwner, Observer {
            transactionsCount = it
        })

        getViewModel().searchedPaymentCountEitherQrOrMobilePay.observe(viewLifecycleOwner, Observer {
            searchTransactionsCount = it
        })

        getViewModel().isLoading.observe(viewLifecycleOwner, Observer {
            if (it) {
                showProgress()
            } else {
                hideProgress()
            }
        })
    }

    private fun initViews() {
        swipe_refresh_layout_fragment_qr.setOnRefreshListener {
            if (alreadySwipedForRefresh) {
                logger.log("Already Swipe Refresh Layout Refreshing...")
                return@setOnRefreshListener
            }
            alreadySwipedForRefresh = true
            pageNumber = 1
            getViewModel().getTransactionsEitherQrOrMobilePay(pageNumber, pageSize, true, INITIATOR_QR)
        }

        textWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // do nothing
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchParam = s.toString()
                if ((s.toString()).isEmpty()) {
                    pageNumber = 1
                    logger.log("getQrTransactions from text change listener")
                    getViewModel().getTransactionsEitherQrOrMobilePay(pageNumber, pageSize, true, INITIATOR_QR)
                    return
                } else {
                    searchPageNumber = 1
                    pageNumber = 1
                    adapter.update(qrPayments.filter { it.invoiceNumber.contains(s!!) }
                        .reversed()
                        .toMutableList())

                    getViewModel().searchTransactionsEitherQrOrMobilePay(s.toString(), searchPageNumber, pageSize, initiator =  INITIATOR_QR)
                }
            }

            override fun afterTextChanged(s: Editable?) {
                // do nothing
            }
        }

        onEditActionListener = object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_NUMPAD_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    activity?.hideKeyboard(et_reference_number);
                }
                return false;
            }
        }

        Glide.with(this).load("file:///android_asset/images/loading.gif")
            .into(iv_progress_pagination)

        et_reference_number.setOnEditorActionListener(onEditActionListener)

        val layoutManager = LinearLayoutManager(activity)
        rv_transactions.layoutManager = layoutManager
        scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if ((dy > 0)) { //check for scroll down
                    val visibleItemCount = layoutManager.getChildCount();
                    val totalItemCount = layoutManager.getItemCount();
                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
                    var availablePages = transactionsCount / pageSize
                    var availableSearchPages = searchTransactionsCount / pageSize
                    if (transactionsCount % pageSize != 0) {
                        availablePages++
                    }
                    if (searchTransactionsCount % pageSize != 0) {
                        availableSearchPages++
                    }
                    if (et_reference_number.text.toString().isEmpty()) {
                        if (((visibleItemCount + pastVisiblesItems) >= (totalItemCount - 5)) && !(getViewModel().isPaginatingEitherQrOrMobilePay.value)!! && (++pageNumber <= availablePages)) {
                            logger.log("getQrTransactions from on scroll listener")
                            getViewModel().getTransactionsEitherQrOrMobilePay(pageNumber, pageSize, initiator = INITIATOR_QR)
                        }
                    } else {
                        if (((visibleItemCount + pastVisiblesItems) >= (totalItemCount - 5)) && !(getViewModel().isPaginatingEitherQrOrMobilePay.value)!! && (++searchPageNumber <= availableSearchPages)) {
                            getViewModel().searchTransactionsEitherQrOrMobilePay(
                                searchParam = searchParam,
                                pageNumber = searchPageNumber,
                                pageSize = pageSize,
                                initiator = INITIATOR_QR
                            )
                        }
                    }
                }
            }

        }
        rv_transactions.addOnScrollListener(scrollListener)
        rv_transactions.adapter = adapter
    }

    fun getViewModel() = ViewModelProviders.of(requireActivity())[TransactionsViewModel::class.java]

    override fun onDestroyView() {
        et_reference_number.removeTextChangedListener(textWatcher)
        et_reference_number.setOnEditorActionListener(null)
        rv_transactions.removeOnScrollListener(scrollListener)
        rv_transactions.adapter = null
        super.onDestroyView()
    }

    override fun onItemClicked(qrPayment: QrPayment) {
        val intent = QrTransactionDetailActivity.getLaunchIntent(requireActivity(), qrPayment)
        startActivity(intent)
    }
}