package global.citytech.finpos.merchant.presentation.alertdialogs

import android.app.Activity
import android.app.AlertDialog
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import global.citytech.finpos.device.weipass.utils.isNullOrEmptyOrBlank
import global.citytech.finpos.merchant.R

/**
 * Created by Unique Shakya on 5/5/2021.
 */
class SliderDialog constructor(
    private var activity: Activity,
    private var listener: Listener,
    private var sliderAttributes: SliderAttributes
) {

    private lateinit var tvTitle: AppCompatTextView
    private lateinit var tvMessage: AppCompatTextView
    private lateinit var tvConfiguredTip: AppCompatTextView
    private lateinit var etValue: AppCompatEditText
    private lateinit var btnConfirm: AppCompatButton
    private lateinit var btnCancel: AppCompatButton

    private var alertDialog: AlertDialog

    private var sliderValue: Int = 0

    init {
        val alertDialogBuilder = AlertDialog.Builder(activity)
        val inflater = activity.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_slider, null)
        alertDialogBuilder.setView(dialogView)
        initViews(dialogView!!)
        setViewAttributes()
        handleClickEvents()
        this.alertDialog = alertDialogBuilder.create()
        this.alertDialog.setCancelable(false)
    }

    private fun setViewAttributes() {
        tvTitle.text = sliderAttributes.title
        tvMessage.text = "Range is between "
            .plus(sliderAttributes.minLimit)
            .plus(" and ")
            .plus(sliderAttributes.maxLimit)
        tvConfiguredTip.text = sliderAttributes.defaultConfigurationMessage
            .plus(" ")
            .plus(sliderAttributes.configuredValue)
    }

    fun show() {
        this.alertDialog.show()
    }

    fun hide() {
        this.alertDialog.dismiss()
    }

    private fun handleClickEvents() {
        btnConfirm.setOnClickListener {
            this.validateTipLimit(etValue.text.toString())
        }

        btnCancel.setOnClickListener {
            this.listener.onCancel()
            hide()
        }
    }

    private fun validateTipLimit(value: String) {
        if (value.isNullOrEmptyOrBlank())
            showToast(sliderAttributes.emptyInputMessage)
        else {
            if (value.toInt() >= sliderAttributes.minLimit && value.toInt() <= sliderAttributes.maxLimit) {
                this.listener.onValueConfirmed(value.toInt())
                this.hide()
            } else {
                showToast(sliderAttributes.invalidInputMessage)
            }
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    private fun initViews(dialogView: View) {
        tvTitle = dialogView.findViewById(R.id.tv_title)
        tvMessage = dialogView.findViewById(R.id.tv_message)
        tvConfiguredTip = dialogView.findViewById(R.id.tv_configured_tip)
        etValue = dialogView.findViewById(R.id.et_value)
        btnConfirm = dialogView.findViewById(R.id.btn_confirm)
        btnCancel = dialogView.findViewById(R.id.btn_cancel)
    }

    interface Listener {
        fun onValueConfirmed(value: Int)
        fun onCancel()
    }
}