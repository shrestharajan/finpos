package global.citytech.finpos.processor.nibl.reconciliation;

import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.switches.reconciliation.ReconciliationResponseParameter;

/**
 * Created by Unique Shakya on 9/22/2020.
 */
public class ReconciliationResponseModel implements UseCase.Response, ReconciliationResponseParameter {

    private String debugRequestString;
    private String debutResponseString;
    private String message;
    private boolean isSuccess;

    public String getDebugRequestString() {
        return debugRequestString;
    }

    public String getDebutResponseString() {
        return debutResponseString;
    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public static final class Builder {
        private String debugRequestString;
        private String debutResponseString;
        private String message;
        private boolean isSuccess;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withDebugRequestString(String debugRequestString) {
            this.debugRequestString = debugRequestString;
            return this;
        }

        public Builder withDebutResponseString(String debutResponseString) {
            this.debutResponseString = debutResponseString;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder withIsSuccess(boolean isSuccess) {
            this.isSuccess = isSuccess;
            return this;
        }

        public ReconciliationResponseModel build() {
            ReconciliationResponseModel reconciliationResponseModel = new ReconciliationResponseModel();
            reconciliationResponseModel.isSuccess = this.isSuccess;
            reconciliationResponseModel.message = this.message;
            reconciliationResponseModel.debugRequestString = this.debugRequestString;
            reconciliationResponseModel.debutResponseString = this.debutResponseString;
            return reconciliationResponseModel;
        }
    }
}
