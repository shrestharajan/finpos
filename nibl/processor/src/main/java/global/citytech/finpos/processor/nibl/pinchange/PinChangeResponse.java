package global.citytech.finpos.processor.nibl.pinchange;
import global.citytech.finpos.processor.nibl.transaction.TransactionResponseModel;

public class PinChangeResponse extends TransactionResponseModel {
    private String debugRequestMessage;
    private String debugResponseMessage;
    private String message;
    private String stan;
    private boolean approved;
    private boolean shouldPrintCustomerCopy;

    public String getDebugRequestMessage() {
        return debugRequestMessage;
    }

    public String getDebugResponseMessage() {
        return debugResponseMessage;
    }


    public String getMessage() {
        return message;
    }

    public String getStan() {
        return stan;
    }


    public boolean isApproved() {
        return approved;
    }

    public boolean isShouldPrintCustomerCopy() {
        return shouldPrintCustomerCopy;
    }

    public static final class Builder {
        private String debugRequestMessage;
        private String debugResponseMessage;
        private String message;
        private String stan;
        private boolean approved;
        private boolean shouldPrintCustomerCopy;

        private Builder() {
        }

        public static PinChangeResponse.Builder newInstance() {
            return new PinChangeResponse.Builder();
        }

        public PinChangeResponse.Builder withDebugRequestMessage(String debugRequestMessage) {
            this.debugRequestMessage = debugRequestMessage;
            return this;
        }

        public PinChangeResponse.Builder withDebugResponseMessage(String debugResponseMessage) {
            this.debugResponseMessage = debugResponseMessage;
            return this;
        }

        public PinChangeResponse.Builder withShouldPrintCustomerCopy(boolean shouldPrintCustomerCopy) {
            this.shouldPrintCustomerCopy = shouldPrintCustomerCopy;
            return this;
        }
        public PinChangeResponse.Builder withApproved(boolean approved) {
            this.approved = approved;
            return this;
        }

        public PinChangeResponse.Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public PinChangeResponse.Builder withStan(String stan) {
            this.stan = stan;
            return this;
        }


        public PinChangeResponse build() {
            PinChangeResponse PinChangeResponse = new PinChangeResponse();
            PinChangeResponse.debugRequestMessage = this.debugRequestMessage;
            PinChangeResponse.debugResponseMessage = this.debugResponseMessage;
            PinChangeResponse.approved = this.approved;
            PinChangeResponse.message = this.message;
            PinChangeResponse.shouldPrintCustomerCopy = this.shouldPrintCustomerCopy;
            PinChangeResponse.stan = this.stan;
            return PinChangeResponse;
        }
    }

}
