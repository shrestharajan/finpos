package global.citytech.neps.domain.model

import android.content.ContentValues
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "card_scheme")
data class CardScheme(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_ID)
    var id: String,
    @ColumnInfo(name = COLUMN_CARD_SCHEME_ID)
    var cardSchemeId: String? = null,
    @ColumnInfo(name = COLUMN_ATTRIBUTE)
    var attribute: String? = null,
    @ColumnInfo(name = COLUMN_DISPLAY_LABEL)
    var displayValue: String? = null,
    @ColumnInfo(name = COLUMN_PRINT_LABEL)
    var printLabel: String? = null,
    @ColumnInfo(name = COLUMN_VALUE)
    var value: String? = null

) {
    companion object {
        const val COLUMN_ID = "id"
        const val COLUMN_CARD_SCHEME_ID = "card_scheme_id"
        const val COLUMN_ATTRIBUTE = "attribute"
        const val COLUMN_DISPLAY_LABEL = "display_label"
        const val COLUMN_PRINT_LABEL = "print_Label"
        const val COLUMN_VALUE = "value"

        fun fromContentValues(values: ContentValues): CardScheme {
            values.let {
                val cardScheme = CardScheme(values.getAsString(COLUMN_ID))
                if (values.containsKey(COLUMN_ATTRIBUTE)) {
                    cardScheme.attribute = values.getAsString(COLUMN_ATTRIBUTE)
                }
                if (values.containsKey(COLUMN_CARD_SCHEME_ID)) {
                    cardScheme.cardSchemeId = values.getAsString(COLUMN_CARD_SCHEME_ID)
                }
                if (values.containsKey(COLUMN_PRINT_LABEL)) {
                    cardScheme.printLabel = values.getAsString(COLUMN_PRINT_LABEL)
                }
                if (values.containsKey(COLUMN_DISPLAY_LABEL)) {
                    cardScheme.displayValue = values.getAsString(COLUMN_DISPLAY_LABEL)
                }
                if (values.containsKey(COLUMN_VALUE)) {
                    cardScheme.value = values.getAsString(COLUMN_VALUE)
                }
                return cardScheme
            }

        }
    }
}