package global.citytech.finposframework.hardware

import global.citytech.finposframework.hardware.common.DeviceService
import global.citytech.finposframework.notifier.Notifier

/**
 * Created by Rishav Chudal on 4/8/20.
 */
interface DeviceFactory {
    fun getDevice(
        context: Any?,
        deviceServiceType: DeviceServiceType,
        notifier: Notifier
    ): DeviceService?
}