package global.citytech.finpos.merchant.presentation.mobilenfcpayment.receipt

data class MobileNfcTransactionDetail(
    val referenceNumber: String,
    val transactionType: String,
    val transactionCurrency: String,
    val transactionAmount: String,
    val transactionResult: String,
    val paymentInitiator: String,
    val initiatorId: String,
    val approvalCode: String
)
