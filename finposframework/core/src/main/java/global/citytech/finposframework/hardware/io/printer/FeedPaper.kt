package global.citytech.finposframework.hardware.io.printer

/**
 * Created by Rishav Chudal on 6/19/20.
 */
class FeedPaper constructor(noOfEmptyLines: Int):
    Printable {
    private val numberOfEmptyLines: Int = noOfEmptyLines
    private val isQrCode = false
    private val isAllow = true
    private val isImage = false
    private val isFeedPaper = true
    private val isBase64Image = false
    private val isBarCode = false

    override fun isAllow(): Boolean {
        return isAllow
    }

    override fun isImage(): Boolean {
        return isImage
    }

    override fun isQrCode(): Boolean {
        return isQrCode
    }

    override fun isFeedPaper(): Boolean {
        return isFeedPaper
    }

    override fun isBase64Image(): Boolean {
        return isBase64Image;
    }

    override fun isBarCode(): Boolean {
        return isBarCode
    }

    override fun getData(): Int? {
        return numberOfEmptyLines
    }
}