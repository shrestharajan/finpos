package global.citytech.finposframework.comm;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import global.citytech.finposframework.exceptions.FinPosException;
import global.citytech.finposframework.log.Logger;
import global.citytech.finposframework.supports.Context;
import global.citytech.finposframework.supports.Optional;
import global.citytech.finposframework.utility.HexDump;

public class NonCloseableTcpClient {

    private static final int CONNECTION_TIMEOUT = 60;
    private Logger LOGGER = Logger.getLogger(TcpClient.class.getName());
    private DataOutputStream writer;
    private DataInputStream reader;
    private Socket client = null;
    private final ConnectionParam param;
    private Socket socket;

    private static NonCloseableTcpClient instance = null;

    public static NonCloseableTcpClient getInstance(ConnectionParam connectionParam) {
        if (instance == null)
            instance = new NonCloseableTcpClient(connectionParam);
        return instance;
    }

    private NonCloseableTcpClient(ConnectionParam param) {
        this.param = param;
    }

    public HostInfo dial() {
        Optional<Socket> result;
        try {
            result = this.dialPrimaryHost();
            if (result.isPresent()) {
                this.config(result.get());
                return param.getPrimaryHost();
            }
        } catch (Exception e) {
            LOGGER.log(e.getMessage());
        }
        try {
            if (param.hasSecondaryConnection()) {
                result = this.dialSecondaryHost();
                if (result.isPresent()) {
                    this.config(result.get());
                    return param.getSecondaryHost();
                }
            }
        } catch (Exception e) {
            LOGGER.log(e.getMessage());
        }
        socket = null;
        throw new FinPosException(FinPosException.ExceptionType.UNABLE_TO_CONNECT);
    }

    private void config(Socket socket) {
        try {
            this.client = socket;
            this.reader = new DataInputStream(this.client.getInputStream());
            this.writer = new DataOutputStream(this.client.getOutputStream());
        } catch (IOException e) {
            throw new FinPosException(FinPosException.ExceptionType.CONNECTION_ERROR, e.getMessage());
        }
    }

    public boolean write(Request data) {
        try {
            LOGGER.debug(HexDump.dumpHexString(data.getData()));
            this.writer.write(data.getData());
            this.writer.flush();
            return true;
        } catch (IOException e) {
            socket = null;
            throw new FinPosException(FinPosException.ExceptionType.WRITE_EXCEPTION, e.getMessage());
        }
    }

    public Response read(int numberOfBytes) {
        try {
            if (numberOfBytes <= 0)
                throw new FinPosException(
                        FinPosException.ExceptionType.INVALID_PARAMETER_SUPPLIED,
                        " numberOfBytes should be greater than zero.");
            byte[] destination = new byte[numberOfBytes];
            int size = this.reader.read(destination);
            if (size > 0) {
                return new Response(destination);
            }
            socket = null;
            throw new FinPosException(FinPosException.ExceptionType.READ_TIME_OUT, "No data available");
        } catch (IOException e) {
            throw new FinPosException(
                    FinPosException.ExceptionType.READ_TIME_OUT, e.getLocalizedMessage());
        }
    }

    private Optional<Socket> dialPrimaryHost() {
        Context context = new Context.Factory().createContextWithValue();
        context.add("MSG", "Dialing to primary host...");
        param.getPrimaryHost().raiseEvent(ConnectionEvent.CONNECTING, context);
        try {
            Optional<Socket> result = this.connectWithHost(param.getPrimaryHost(), context);
            if (result.isPresent()) {
                context.add("MSG", "Connected with primary host...");
                param.getPrimaryHost().raiseEvent(ConnectionEvent.CONNECTED, context);
                return result;
            }
        } catch (FinPosException e) {
            throw e;
        }
        throw new FinPosException(FinPosException.ExceptionType.UNABLE_TO_CONNECT);
    }

    private Optional<Socket> dialSecondaryHost() {
        Context context = new Context.Factory().createContextWithValue();
        context.add("MSG", "Dialing to secondary host...");
        param.getSecondaryHost().raiseEvent(ConnectionEvent.CONNECTING, context);
        try {
            Optional<Socket> result = this.connectWithHost(param.getSecondaryHost(), context);
            if (result.isPresent()) {
                context.add("MSG", "Connected with secondary host...");
                param.getSecondaryHost().raiseEvent(ConnectionEvent.CONNECTED, context);
                return result;
            }
        } catch (FinPosException e) {
            throw e;
        }
        throw new FinPosException(FinPosException.ExceptionType.UNABLE_TO_CONNECT);
    }

    private Optional<Socket> connectWithHost(HostInfo host, Context context) {
        //  Result.Builder<Socket> resultBuilder = Result.Builder.createDefaultResultBuilder();
        int dialCount = 1;
        while (true) {
            try {
                if (dialCount > (host.getRetryLimit())) break;
                context.add("MSG", "Dialing  # " + dialCount);
                host.raiseEvent(ConnectionEvent.REDIALING, context);
                dialCount++;
                Socket socket;
                if (host.isSecureConnection())
                    socket = this.retrieveSecureSocket(host);
                else
                    socket = this.retrieveInsecureSocket(host);
                if (socket.isConnected()) {
                    // return resultBuilder.success(socket).build();
                    return Optional.of(socket);
                }

//                if (!socket.isClosed()) socket.close();
//                socket = null;
            } catch (IOException e) {
                LOGGER.log(e.getMessage());
                throw new FinPosException(FinPosException.ExceptionType.UNABLE_TO_CONNECT, e.getMessage());
            }
        }
        throw new FinPosException(FinPosException.ExceptionType.UNABLE_TO_CONNECT);
        //  return resultBuilder.error(ErrorCode.UNABLE_TO_CONNECT).build();
    }

    private byte[] retrieveSslCertificate(TlsCertificate tlsCertificate) {
        String certificateString = tlsCertificate.getCertificate();
        String privateKeyPEM = certificateString
                .replace("-----BEGIN CERTIFICATE-----", "")
                .replaceAll("\n", "")
                .replace("-----END CERTIFICATE-----", "");
        return Base64.decode(privateKeyPEM);
    }

    private Socket retrieveInsecureSocket(HostInfo host) throws IOException {
        if (socket != null)
            return socket;
        socket = new Socket();
        InetAddress addr = InetAddress.getByName(host.getIp());
        SocketAddress address = new InetSocketAddress(addr, host.getPort());
        socket.connect(address, 0);
        socket.setSoTimeout(0);
        return socket;
    }

    private Socket retrieveSecureSocket(HostInfo hostInfo) throws IOException {
        try {
            SSLSocketFactory sslSocketFactory = this.prepareSslSocketFactory(hostInfo.getTlsCertificate());
            if (sslSocketFactory == null)
                throw new IOException("Unable to connect");
            SSLSocket sslSocket = (SSLSocket) sslSocketFactory.createSocket(hostInfo.getIp(), hostInfo.getPort());
            sslSocket.setSoTimeout(hostInfo.getTimeOut() * 1000);
            sslSocket.startHandshake();
            sslSocket.setSoTimeout(hostInfo.getTimeOut() * 1000);
            return sslSocket;
        } catch (Exception e) {
            e.printStackTrace();
            throw new IOException("Unable to connect");
        }
    }

    private SSLSocketFactory prepareSslSocketFactory(TlsCertificate tlsCertificate) throws Exception {
        KeyStore keyStore = this.prepareKeyStore(tlsCertificate);
        TrustManagerFactory trustManagerFactory = this.prepareTrustManagerFactory(keyStore);
        SSLContext sslContext = this.prepareSslContext(trustManagerFactory, tlsCertificate);
        SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
        return sslSocketFactory;
    }

    private SSLContext prepareSslContext(TrustManagerFactory trustManagerFactory, TlsCertificate tlsCertificate) throws Exception {
        SSLContext sslContext = SSLContext.getInstance(this.getTlsVersion(tlsCertificate.getTlsVersion()));
        sslContext.init(null, trustManagerFactory.getTrustManagers(), null);
        return sslContext;
    }

    private String getTlsVersion(String tlsVersion) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("TLSv");
        stringBuilder.append(tlsVersion);
        return stringBuilder.toString();
    }

    private KeyStore prepareKeyStore(TlsCertificate tlsCertificate) throws Exception {
        CertificateFactory certificateFactory = CertificateFactory.getInstance(tlsCertificate.getCertificateFactoryType());
        InputStream certificateInputStream = new BufferedInputStream(new ByteArrayInputStream(this.retrieveSslCertificate(tlsCertificate)));
        Certificate certificate = certificateFactory.generateCertificate(certificateInputStream);
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(null, null);
        keyStore.setCertificateEntry(tlsCertificate.getCertificateKey().toLowerCase(), certificate);
        certificateInputStream.close();
        return keyStore;
    }

    private TrustManagerFactory prepareTrustManagerFactory(KeyStore keyStore) throws Exception {
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm);
        trustManagerFactory.init(keyStore);
        return trustManagerFactory;
    }

    public void close() {
        LOGGER.log("Closing all stream...");
        closeWrite();
        closeReader();
        closeClient();
        instance = null;
    }

    private void closeClient() {
        try {
            if (client != null) {
                client.close();
                client = null;
            }
        } catch (Exception ignored) {}
    }

    private void closeReader() {
        try {
            if (this.reader != null) {
                this.reader.close();
                this.reader = null;
            }
        } catch (Exception ignored) {}
    }

    private void closeWrite() {
        try {
            if (this.writer != null) {
                this.writer.close();
                this.writer = null;
            }
        } catch (Exception ignored) {}
    }

}
