package global.citytech.finpos.processor.nibl.transaction;

import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import global.citytech.finpos.processor.nibl.transaction.responsehandler.NiblInvalidResponseHandler;
import global.citytech.finposframework.iso8583.DataElement;
import global.citytech.finposframework.iso8583.Iso8583Msg;
import global.citytech.finposframework.iso8583.IsoMessageResponse;
import global.citytech.finposframework.iso8583.MTI;
import global.citytech.finposframework.supports.MessagePayload;
import global.citytech.finposframework.supports.Optional;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandler;
import global.citytech.finposframework.usecases.transaction.InvalidResponseHandlerResponse;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Unique Shakya on 10/2/2020.
 */
public class NiblInvalidResponseHandlerTest {

    @Test
    public void validResponseTest() {
        IsoMessageResponse isoMessage = PowerMockito.mock(IsoMessageResponse.class);
        Iso8583Msg requestIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MessagePayload requestMessagePayload = PowerMockito.mock(MessagePayload.class);
        Iso8583Msg responseIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MTI requestMti = PowerMockito.mock(MTI.class);
        MTI responseMti = PowerMockito.mock(MTI.class);

        PowerMockito.when(requestMti.getVersion()).thenReturn(0);
        PowerMockito.when(requestMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(requestMti.getSubClass()).thenReturn(0);
        PowerMockito.when(requestMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(responseMti.getVersion()).thenReturn(0);
        PowerMockito.when(responseMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(responseMti.getSubClass()).thenReturn(1);
        PowerMockito.when(responseMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(requestIso8583Msg.getMti()).thenReturn(requestMti);
        PowerMockito.when(responseIso8583Msg.getMti()).thenReturn(responseMti);

        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(requestMessagePayload.getRequestMsg()).thenReturn(requestIso8583Msg);

        PowerMockito.when(isoMessage.getMsg()).thenReturn(responseIso8583Msg);
        PowerMockito.when(isoMessage.getRequestPayload()).thenReturn(requestMessagePayload);

        InvalidResponseHandler invalidResponseHandler = new NiblInvalidResponseHandler();
        InvalidResponseHandlerResponse responseHandlerResponse = invalidResponseHandler.isInvalidResponse(isoMessage);
        assertFalse(responseHandlerResponse.isInvalid());
    }

    @Test
    public void invalidMtiVersionTest() {
        IsoMessageResponse isoMessage = PowerMockito.mock(IsoMessageResponse.class);
        Iso8583Msg requestIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MessagePayload requestMessagePayload = PowerMockito.mock(MessagePayload.class);
        Iso8583Msg responseIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MTI requestMti = PowerMockito.mock(MTI.class);
        MTI responseMti = PowerMockito.mock(MTI.class);

        PowerMockito.when(requestMti.getVersion()).thenReturn(0);
        PowerMockito.when(requestMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(requestMti.getSubClass()).thenReturn(0);
        PowerMockito.when(requestMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(responseMti.getVersion()).thenReturn(1);
        PowerMockito.when(responseMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(responseMti.getSubClass()).thenReturn(1);
        PowerMockito.when(responseMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(requestIso8583Msg.getMti()).thenReturn(requestMti);
        PowerMockito.when(responseIso8583Msg.getMti()).thenReturn(responseMti);

        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(requestMessagePayload.getRequestMsg()).thenReturn(requestIso8583Msg);

        PowerMockito.when(isoMessage.getMsg()).thenReturn(responseIso8583Msg);
        PowerMockito.when(isoMessage.getRequestPayload()).thenReturn(requestMessagePayload);

        InvalidResponseHandler invalidResponseHandler = new NiblInvalidResponseHandler();
        InvalidResponseHandlerResponse responseHandlerResponse = invalidResponseHandler.isInvalidResponse(isoMessage);
        assertTrue(responseHandlerResponse.isInvalid());
    }

    @Test
    public void invalidMtiMessageClassTest() {
        IsoMessageResponse isoMessage = PowerMockito.mock(IsoMessageResponse.class);
        Iso8583Msg requestIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MessagePayload requestMessagePayload = PowerMockito.mock(MessagePayload.class);
        Iso8583Msg responseIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MTI requestMti = PowerMockito.mock(MTI.class);
        MTI responseMti = PowerMockito.mock(MTI.class);

        PowerMockito.when(requestMti.getVersion()).thenReturn(0);
        PowerMockito.when(requestMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(requestMti.getSubClass()).thenReturn(0);
        PowerMockito.when(requestMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(responseMti.getVersion()).thenReturn(0);
        PowerMockito.when(responseMti.getMessageClass()).thenReturn(4);
        PowerMockito.when(responseMti.getSubClass()).thenReturn(1);
        PowerMockito.when(responseMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(requestIso8583Msg.getMti()).thenReturn(requestMti);
        PowerMockito.when(responseIso8583Msg.getMti()).thenReturn(responseMti);

        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(requestMessagePayload.getRequestMsg()).thenReturn(requestIso8583Msg);

        PowerMockito.when(isoMessage.getMsg()).thenReturn(responseIso8583Msg);
        PowerMockito.when(isoMessage.getRequestPayload()).thenReturn(requestMessagePayload);

        InvalidResponseHandler invalidResponseHandler = new NiblInvalidResponseHandler();
        InvalidResponseHandlerResponse responseHandlerResponse = invalidResponseHandler.isInvalidResponse(isoMessage);
        assertTrue(responseHandlerResponse.isInvalid());
    }

    @Test
    public void invalidMtiMessageSubClassTest() {
        IsoMessageResponse isoMessage = PowerMockito.mock(IsoMessageResponse.class);
        Iso8583Msg requestIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MessagePayload requestMessagePayload = PowerMockito.mock(MessagePayload.class);
        Iso8583Msg responseIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MTI requestMti = PowerMockito.mock(MTI.class);
        MTI responseMti = PowerMockito.mock(MTI.class);

        PowerMockito.when(requestMti.getVersion()).thenReturn(0);
        PowerMockito.when(requestMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(requestMti.getSubClass()).thenReturn(0);
        PowerMockito.when(requestMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(responseMti.getVersion()).thenReturn(0);
        PowerMockito.when(responseMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(responseMti.getSubClass()).thenReturn(0);
        PowerMockito.when(responseMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(requestIso8583Msg.getMti()).thenReturn(requestMti);
        PowerMockito.when(responseIso8583Msg.getMti()).thenReturn(responseMti);

        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(requestMessagePayload.getRequestMsg()).thenReturn(requestIso8583Msg);

        PowerMockito.when(isoMessage.getMsg()).thenReturn(responseIso8583Msg);
        PowerMockito.when(isoMessage.getRequestPayload()).thenReturn(requestMessagePayload);

        InvalidResponseHandler invalidResponseHandler = new NiblInvalidResponseHandler();
        InvalidResponseHandlerResponse responseHandlerResponse = invalidResponseHandler.isInvalidResponse(isoMessage);
        assertTrue(responseHandlerResponse.isInvalid());
    }

    @Test
    public void mismatchPanTest() {
        IsoMessageResponse isoMessage = PowerMockito.mock(IsoMessageResponse.class);
        Iso8583Msg requestIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MessagePayload requestMessagePayload = PowerMockito.mock(MessagePayload.class);
        Iso8583Msg responseIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MTI requestMti = PowerMockito.mock(MTI.class);
        MTI responseMti = PowerMockito.mock(MTI.class);

        PowerMockito.when(requestMti.getVersion()).thenReturn(0);
        PowerMockito.when(requestMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(requestMti.getSubClass()).thenReturn(0);
        PowerMockito.when(requestMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(responseMti.getVersion()).thenReturn(0);
        PowerMockito.when(responseMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(responseMti.getSubClass()).thenReturn(1);
        PowerMockito.when(responseMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(requestIso8583Msg.getMti()).thenReturn(requestMti);
        PowerMockito.when(responseIso8583Msg.getMti()).thenReturn(responseMti);

        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000235")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(requestMessagePayload.getRequestMsg()).thenReturn(requestIso8583Msg);

        PowerMockito.when(isoMessage.getMsg()).thenReturn(responseIso8583Msg);
        PowerMockito.when(isoMessage.getRequestPayload()).thenReturn(requestMessagePayload);

        InvalidResponseHandler invalidResponseHandler = new NiblInvalidResponseHandler();
        InvalidResponseHandlerResponse responseHandlerResponse = invalidResponseHandler.isInvalidResponse(isoMessage);
        assertTrue(responseHandlerResponse.isInvalid());
    }

    @Test
    public void panNotPresentTest() {
        IsoMessageResponse isoMessage = PowerMockito.mock(IsoMessageResponse.class);
        Iso8583Msg requestIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MessagePayload requestMessagePayload = PowerMockito.mock(MessagePayload.class);
        Iso8583Msg responseIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MTI requestMti = PowerMockito.mock(MTI.class);
        MTI responseMti = PowerMockito.mock(MTI.class);

        PowerMockito.when(requestMti.getVersion()).thenReturn(0);
        PowerMockito.when(requestMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(requestMti.getSubClass()).thenReturn(0);
        PowerMockito.when(requestMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(responseMti.getVersion()).thenReturn(0);
        PowerMockito.when(responseMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(responseMti.getSubClass()).thenReturn(1);
        PowerMockito.when(responseMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(requestIso8583Msg.getMti()).thenReturn(requestMti);
        PowerMockito.when(responseIso8583Msg.getMti()).thenReturn(responseMti);

        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.empty());
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(requestMessagePayload.getRequestMsg()).thenReturn(requestIso8583Msg);

        PowerMockito.when(isoMessage.getMsg()).thenReturn(responseIso8583Msg);
        PowerMockito.when(isoMessage.getRequestPayload()).thenReturn(requestMessagePayload);

        InvalidResponseHandler invalidResponseHandler = new NiblInvalidResponseHandler();
        InvalidResponseHandlerResponse responseHandlerResponse = invalidResponseHandler.isInvalidResponse(isoMessage);
        assertFalse(responseHandlerResponse.isInvalid());
    }

    @Test
    public void mismatchProcessingCodeTest() {
        IsoMessageResponse isoMessage = PowerMockito.mock(IsoMessageResponse.class);
        Iso8583Msg requestIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MessagePayload requestMessagePayload = PowerMockito.mock(MessagePayload.class);
        Iso8583Msg responseIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MTI requestMti = PowerMockito.mock(MTI.class);
        MTI responseMti = PowerMockito.mock(MTI.class);

        PowerMockito.when(requestMti.getVersion()).thenReturn(0);
        PowerMockito.when(requestMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(requestMti.getSubClass()).thenReturn(0);
        PowerMockito.when(requestMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(responseMti.getVersion()).thenReturn(0);
        PowerMockito.when(responseMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(responseMti.getSubClass()).thenReturn(1);
        PowerMockito.when(responseMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(requestIso8583Msg.getMti()).thenReturn(requestMti);
        PowerMockito.when(responseIso8583Msg.getMti()).thenReturn(responseMti);

        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000090")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(requestMessagePayload.getRequestMsg()).thenReturn(requestIso8583Msg);

        PowerMockito.when(isoMessage.getMsg()).thenReturn(responseIso8583Msg);
        PowerMockito.when(isoMessage.getRequestPayload()).thenReturn(requestMessagePayload);

        InvalidResponseHandler invalidResponseHandler = new NiblInvalidResponseHandler();
        InvalidResponseHandlerResponse responseHandlerResponse = invalidResponseHandler.isInvalidResponse(isoMessage);
        assertTrue(responseHandlerResponse.isInvalid());
    }

    @Test
    public void mismatchAmountTest() {
        IsoMessageResponse isoMessage = PowerMockito.mock(IsoMessageResponse.class);
        Iso8583Msg requestIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MessagePayload requestMessagePayload = PowerMockito.mock(MessagePayload.class);
        Iso8583Msg responseIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MTI requestMti = PowerMockito.mock(MTI.class);
        MTI responseMti = PowerMockito.mock(MTI.class);

        PowerMockito.when(requestMti.getVersion()).thenReturn(0);
        PowerMockito.when(requestMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(requestMti.getSubClass()).thenReturn(0);
        PowerMockito.when(requestMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(responseMti.getVersion()).thenReturn(0);
        PowerMockito.when(responseMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(responseMti.getSubClass()).thenReturn(1);
        PowerMockito.when(responseMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(requestIso8583Msg.getMti()).thenReturn(requestMti);
        PowerMockito.when(responseIso8583Msg.getMti()).thenReturn(responseMti);

        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000110000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(requestMessagePayload.getRequestMsg()).thenReturn(requestIso8583Msg);

        PowerMockito.when(isoMessage.getMsg()).thenReturn(responseIso8583Msg);
        PowerMockito.when(isoMessage.getRequestPayload()).thenReturn(requestMessagePayload);

        InvalidResponseHandler invalidResponseHandler = new NiblInvalidResponseHandler();
        InvalidResponseHandlerResponse responseHandlerResponse = invalidResponseHandler.isInvalidResponse(isoMessage);
        assertTrue(responseHandlerResponse.isInvalid());
    }

    @Test
    public void amountNotPresentTest() {
        IsoMessageResponse isoMessage = PowerMockito.mock(IsoMessageResponse.class);
        Iso8583Msg requestIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MessagePayload requestMessagePayload = PowerMockito.mock(MessagePayload.class);
        Iso8583Msg responseIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MTI requestMti = PowerMockito.mock(MTI.class);
        MTI responseMti = PowerMockito.mock(MTI.class);

        PowerMockito.when(requestMti.getVersion()).thenReturn(0);
        PowerMockito.when(requestMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(requestMti.getSubClass()).thenReturn(0);
        PowerMockito.when(requestMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(responseMti.getVersion()).thenReturn(0);
        PowerMockito.when(responseMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(responseMti.getSubClass()).thenReturn(1);
        PowerMockito.when(responseMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(requestIso8583Msg.getMti()).thenReturn(requestMti);
        PowerMockito.when(responseIso8583Msg.getMti()).thenReturn(responseMti);

        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.empty());
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(requestMessagePayload.getRequestMsg()).thenReturn(requestIso8583Msg);

        PowerMockito.when(isoMessage.getMsg()).thenReturn(responseIso8583Msg);
        PowerMockito.when(isoMessage.getRequestPayload()).thenReturn(requestMessagePayload);

        InvalidResponseHandler invalidResponseHandler = new NiblInvalidResponseHandler();
        InvalidResponseHandlerResponse responseHandlerResponse = invalidResponseHandler.isInvalidResponse(isoMessage);
        assertFalse(responseHandlerResponse.isInvalid());
    }

    @Test
    public void mismatchStanTest() {
        IsoMessageResponse isoMessage = PowerMockito.mock(IsoMessageResponse.class);
        Iso8583Msg requestIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MessagePayload requestMessagePayload = PowerMockito.mock(MessagePayload.class);
        Iso8583Msg responseIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MTI requestMti = PowerMockito.mock(MTI.class);
        MTI responseMti = PowerMockito.mock(MTI.class);

        PowerMockito.when(requestMti.getVersion()).thenReturn(0);
        PowerMockito.when(requestMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(requestMti.getSubClass()).thenReturn(0);
        PowerMockito.when(requestMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(responseMti.getVersion()).thenReturn(0);
        PowerMockito.when(responseMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(responseMti.getSubClass()).thenReturn(1);
        PowerMockito.when(responseMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(requestIso8583Msg.getMti()).thenReturn(requestMti);
        PowerMockito.when(responseIso8583Msg.getMti()).thenReturn(responseMti);

        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000012")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(requestMessagePayload.getRequestMsg()).thenReturn(requestIso8583Msg);

        PowerMockito.when(isoMessage.getMsg()).thenReturn(responseIso8583Msg);
        PowerMockito.when(isoMessage.getRequestPayload()).thenReturn(requestMessagePayload);

        InvalidResponseHandler invalidResponseHandler = new NiblInvalidResponseHandler();
        InvalidResponseHandlerResponse responseHandlerResponse = invalidResponseHandler.isInvalidResponse(isoMessage);
        assertTrue(responseHandlerResponse.isInvalid());
    }

    @Test
    public void mismatchNiiTest() {
        IsoMessageResponse isoMessage = PowerMockito.mock(IsoMessageResponse.class);
        Iso8583Msg requestIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MessagePayload requestMessagePayload = PowerMockito.mock(MessagePayload.class);
        Iso8583Msg responseIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MTI requestMti = PowerMockito.mock(MTI.class);
        MTI responseMti = PowerMockito.mock(MTI.class);

        PowerMockito.when(requestMti.getVersion()).thenReturn(0);
        PowerMockito.when(requestMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(requestMti.getSubClass()).thenReturn(0);
        PowerMockito.when(requestMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(responseMti.getVersion()).thenReturn(0);
        PowerMockito.when(responseMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(responseMti.getSubClass()).thenReturn(1);
        PowerMockito.when(responseMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(requestIso8583Msg.getMti()).thenReturn(requestMti);
        PowerMockito.when(responseIso8583Msg.getMti()).thenReturn(responseMti);

        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "1111")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(requestMessagePayload.getRequestMsg()).thenReturn(requestIso8583Msg);

        PowerMockito.when(isoMessage.getMsg()).thenReturn(responseIso8583Msg);
        PowerMockito.when(isoMessage.getRequestPayload()).thenReturn(requestMessagePayload);

        InvalidResponseHandler invalidResponseHandler = new NiblInvalidResponseHandler();
        InvalidResponseHandlerResponse responseHandlerResponse = invalidResponseHandler.isInvalidResponse(isoMessage);
        assertTrue(responseHandlerResponse.isInvalid());
    }

    @Test
    public void mismatchTerminalIdTest() {
        IsoMessageResponse isoMessage = PowerMockito.mock(IsoMessageResponse.class);
        Iso8583Msg requestIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MessagePayload requestMessagePayload = PowerMockito.mock(MessagePayload.class);
        Iso8583Msg responseIso8583Msg = PowerMockito.mock(Iso8583Msg.class);
        MTI requestMti = PowerMockito.mock(MTI.class);
        MTI responseMti = PowerMockito.mock(MTI.class);

        PowerMockito.when(requestMti.getVersion()).thenReturn(0);
        PowerMockito.when(requestMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(requestMti.getSubClass()).thenReturn(0);
        PowerMockito.when(requestMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(responseMti.getVersion()).thenReturn(0);
        PowerMockito.when(responseMti.getMessageClass()).thenReturn(2);
        PowerMockito.when(responseMti.getSubClass()).thenReturn(1);
        PowerMockito.when(responseMti.getTransactionOriginator()).thenReturn(0);

        PowerMockito.when(requestIso8583Msg.getMti()).thenReturn(requestMti);
        PowerMockito.when(responseIso8583Msg.getMti()).thenReturn(responseMti);

        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(requestIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029901")));

        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(2)).thenReturn(Optional.of(new DataElement(2, "4249720020000234")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(3)).thenReturn(Optional.of(new DataElement(3, "000000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(4)).thenReturn(Optional.of(new DataElement(4, "000000100000")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(11)).thenReturn(Optional.of(new DataElement(11, "000011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(24)).thenReturn(Optional.of(new DataElement(24, "0011")));
        PowerMockito.when(responseIso8583Msg.getDataElementByIndex(41)).thenReturn(Optional.of(new DataElement(41, "99029902")));

        PowerMockito.when(requestMessagePayload.getRequestMsg()).thenReturn(requestIso8583Msg);

        PowerMockito.when(isoMessage.getMsg()).thenReturn(responseIso8583Msg);
        PowerMockito.when(isoMessage.getRequestPayload()).thenReturn(requestMessagePayload);

        InvalidResponseHandler invalidResponseHandler = new NiblInvalidResponseHandler();
        InvalidResponseHandlerResponse responseHandlerResponse = invalidResponseHandler.isInvalidResponse(isoMessage);
        assertTrue(responseHandlerResponse.isInvalid());
    }
}
