package global.citytech.finpos.device.weipass.system

import global.citytech.finpos.device.weipass.utils.WeipassLogger
import java.lang.reflect.Method

/**
 * Created by Rishav Chudal on 6/7/20.
 */
class WangPosManager {
    companion object {
        private var WangPosManager: Any? = null
        private var WangPosManagerClass: Class<*>? = null

        fun getInstance(): Any? {
            if (WangPosManager == null) {
                try {
                    WangPosManagerClass = Class.forName("android.os.WangPosManager")
                    WangPosManager = WangPosManagerClass!!.newInstance()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            return WangPosManager
        }

        @Throws(IllegalAccessException::class)
        fun <T> getMethod(
            methodName: String,
            type: Class<T>?
        ): Method? {
            return try {
                getInstance()
                WangPosManagerClass!!.getMethod(methodName, type)
            } catch (e: Exception) {
                WeipassLogger.log("Exception in getMethod ::: " + e.message)
                throw IllegalAccessException("Unable to get Method : $methodName")
            }
        }
    }
}