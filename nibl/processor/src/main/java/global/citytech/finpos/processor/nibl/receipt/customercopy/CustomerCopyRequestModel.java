package global.citytech.finpos.processor.nibl.receipt.customercopy;

import global.citytech.finposframework.hardware.io.printer.PrinterService;
import global.citytech.finposframework.repositories.TransactionRepository;
import global.citytech.finposframework.switches.receipt.customercopy.CustomerCopyRequestParameter;
import global.citytech.finposframework.supports.UseCase;
import global.citytech.finposframework.usecases.transaction.data.StatementList;

/**
 * Created by Unique Shakya on 9/2/2020.
 */
public class CustomerCopyRequestModel implements UseCase.Request, CustomerCopyRequestParameter {

    private TransactionRepository transactionRepository;
    private PrinterService printerService;
    private StatementList statementList;


    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }

    public PrinterService getPrinterService() {
        return printerService;
    }

    public StatementList getStatementList(){
        return statementList;
    }

    public static final class Builder {
        TransactionRepository transactionRepository;
        PrinterService printerService;
        private StatementList statementList;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withTransactionRepository(TransactionRepository transactionRepository) {
            this.transactionRepository = transactionRepository;
            return this;
        }

        public Builder withPrinterService(PrinterService printerService) {
            this.printerService = printerService;
            return this;
        }

        public Builder withMiniStatementDate(StatementList statementList){
            this.statementList = statementList;
            return this;
        }


        public CustomerCopyRequestModel build() {
            CustomerCopyRequestModel customerCopyRequestModel = new CustomerCopyRequestModel();
            customerCopyRequestModel.transactionRepository = this.transactionRepository;
            customerCopyRequestModel.printerService = this.printerService;
            customerCopyRequestModel.statementList = this.statementList;
            return customerCopyRequestModel;
        }
    }
}
