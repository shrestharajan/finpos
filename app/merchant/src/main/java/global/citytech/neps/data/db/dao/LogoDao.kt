package global.citytech.neps.data.db.dao

import android.database.Cursor
import androidx.room.*
import global.citytech.neps.domain.model.Logo


/**
 * Created by Saurav Ghimire on 3/27/20.
 * Citytech
 * saurav.ghimire@citytech.global
 */
@Dao
interface LogoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(logos: List<Logo>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(logo: Logo): Long

    @Query("SELECT * FROM logo")
    fun get(): Cursor

    @Query("SELECT * FROM logo")
    fun getLogoList(): List<Logo>

    @Query("SELECT * FROM logo where id = :id")
    fun getById(id: Long): Cursor?

    @Update
    fun update(logo: Logo):Int

    @Query("DELETE FROM logo WHERE id = :id")
    fun deleteById(id: Long): Int


}