package global.citytech.finpos.merchant.presentation.model.ministatement

import global.citytech.finposframework.usecases.TransactionType
import java.math.BigDecimal

data class MiniStatementRequestItem(
    val transactionType: TransactionType? = null,
    val amount: BigDecimal? = null
)